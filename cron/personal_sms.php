<?php
ini_set('max_execution_time', 300);
require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/model/Message.php");

mysqli_query($my_db,'set names utf8');

if ( mysqli_connect_errno() ) {
    echo mysqli_connect_error();
    exit;
}

date_default_timezone_set("Asia/Seoul");

# 날짜 계산
$message_model  = Message::Factory();
$cur_date 		= date('Y-m-d');
$exp_date 		= date('Y-m')."-23";
$exp_date_w 	= date('w', strtotime($exp_date));

if($exp_date_w == '0'){
    $exp_date = date('Y-m-d', strtotime("{$exp_date} +1 day"));
}elseif($exp_date_w == '6'){
    $exp_date = date('Y-m-d', strtotime("{$exp_date} +2 day"));
}

$send_date = date('Y-m-d', strtotime("{$exp_date} -1 day"));

if($cur_date == $send_date)
{
    $sms_content_sql    = "SELECT k_discription FROM kind WHERE k_code='sms' ORDER BY k_no DESC LIMIT 1";
    $sms_content_query  = mysqli_query($my_db, $sms_content_sql);
    $sms_content_result = mysqli_fetch_assoc($sms_content_query);

    $c_info         = "5"; # 개인경비 잔액확인 문자
    $sms_title      = "개인경비 잔액확인 문자";
    $sms_msg        = isset($sms_content_result['k_discription']) ? addslashes($sms_content_result['k_discription']) : "";
    $send_name      = "와이즈플래닛";
    $send_phone     = "02-830-1912";
    $cur_datetime   = date('YmdHis');
    $send_data_list = [];

    if(!!$sms_msg)
    {
        $sms_msg_len    = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
        $msg_type       = ($sms_msg_len > 90) ? "L" : "S";

        $ins_per_sms_sql = "";
        $comma = "";
        $cm_id = 1;

        # 개인경비 중 법인카드 사용자 추출
        $per_mon    = date('Y-m', strtotime('-1 month'));
        $per_s_date = $per_mon."-01";
        $per_e_date = $per_mon."-31";

        $personal_exp_list_sql   = "SELECT s.hp, s.s_name FROM staff s WHERE s.s_no IN(SELECT DISTINCT pe.s_no FROM personal_expenses pe WHERE (pe.payment_date >= '{$per_s_date}' AND pe.payment_date <= '{$per_e_date}') AND display='1' AND (pe.wd_method='1' OR pe.wd_method='6')) AND s.staff_state='1' AND s.hp IS NOT NULL";
        $personal_exp_list_query = mysqli_query($my_db, $personal_exp_list_sql);
        while($personal_exp = mysqli_fetch_assoc($personal_exp_list_query))
        {
            $dest_phone  = $personal_exp['hp'];
            $per_msg     = str_replace("%name%", $personal_exp['s_name'], $sms_msg);

            if(empty($dest_phone)){
                continue;
            }

            $msg_id 	 = "W{$cur_datetime}".sprintf('%04d', $cm_id++);
            $send_data_list[$msg_id] = array(
                "msg_id"        => $msg_id,
                "msg_type"      => $msg_type,
                "sender"        => $send_name,
                "sender_hp"     => $send_phone,
                "receiver"      => $personal_exp['s_name'],
                "receiver_hp"   => $dest_phone,
                "title"         => $sms_title,
                "content"       => $per_msg,
                "cinfo"         => $c_info
            );
        }

        if(!empty($send_data_list))
        {
            $message_model->sendMessage($send_data_list);
        }
    }
}


















