<?php
ini_set('max_execution_time', 500);
require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/helper/_message.php");
require_once("/home/master/wplanet/v1/inc/model/Message.php");

mysqli_query($my_db,'set names utf8');

if ( mysqli_connect_errno() ) {
    echo mysqli_connect_error();
    exit;
}

date_default_timezone_set("Asia/Seoul");

# 보내는 템플릿 부분 확인
$kakao_model            = Message::Factory();
$kakao_model->setCharset("utf8mb4");
$kakao_model->setMainInit("EASY_SEND_FILTER_LOG", "MSG_ID");

$cm_idx                 = 1;
$c_info                 = "88";
$cur_datetime           = date("YmdHis");
$cur_s_date             = date('Y-m-d')." 00:00:00";
$cur_e_date             = date('Y-m-d')." 23:59:59";
$prd_name_option        = getCertPrdNameTermInfo();
$send_kakao_list        = [];
$upd_reserve_kakao_list = [];

$crm_reservation_sql = "
    SELECT 
        cr.cr_no, 
        cr.sms_type, 
        cr.out_name as dest_name, 
        cr.out_hp as dest_phone, 
        ct.title,
        ct.send_key,
        ct.temp_key,
        ct.btn_key,
        ct.btn_content,
        ct.send_name,
        ct.send_phone,
        cr.exp_date,
        ct.subject,
        ct.content,
        ccp.prd_no,
        ccp.prd_type
    FROM crm_cert_reservation as cr 
    LEFT JOIN crm_template as ct ON ct.t_no = cr.temp_no
    LEFT JOIN crm_cert_product as ccp ON ccp.no=cr.cert_prd_no
    WHERE cr.cr_state='1' AND ct.active='1' AND cr.exp_date BETWEEN '{$cur_s_date}' AND '{$cur_e_date}' 
    ORDER BY cr.exp_date ASC
";
$crm_reservation_query = mysqli_query($my_db, $crm_reservation_sql);
while($crm_reserve = mysqli_fetch_assoc($crm_reservation_query))
{
    $cr_no          = $crm_reserve['cr_no'];
    $temp_type      = $crm_reserve['sms_type'];
    $send_name      = $crm_reserve['send_name'];
    $send_phone     = $crm_reserve['send_phone'];
    $dest_name      = $crm_reserve['dest_name'];
    $dest_phone     = $crm_reserve['dest_phone'];
    $title          = $crm_reserve['title'];
    $content        = $crm_reserve['content'];
    $temp_key       = $crm_reserve['temp_key'];
    $btn_key        = $crm_reserve['btn_key'];
    $btn_content    = $crm_reserve['btn_content'];
    $exp_date       = $crm_reserve['exp_date'];
    $exp_datetime   = date("YmdHis", strtotime($crm_reserve['exp_date']));
    $sms_term       = $crm_reserve['exp_date'];
    $prd_name       = $prd_name_option[$crm_reserve['prd_no']][$crm_reserve['prd_type']]['title'];
    $prd_term       = $prd_name_option[$crm_reserve['prd_no']][$crm_reserve['prd_type']]['term'];

    #데이터 변환
    $msg_body_conv = str_replace("#{고객성명}", $dest_name, $content);
    $msg_body_conv = str_replace("#{고객명}", $dest_name, $msg_body_conv);
    $msg_body_conv = str_replace("#{고객님}", $dest_name, $msg_body_conv);
    $msg_body_conv = str_replace("#{제품}", $prd_name, $msg_body_conv);
    $msg_body_conv = str_replace("#{개월}", $prd_term, $msg_body_conv);
    $msg_id 	   = "W{$cur_datetime}".sprintf('%04d', $cm_idx++);

    $send_kakao_list            = [];
    $send_kakao_list[$msg_id]   = array(
        "msg_id"        => $msg_id,
        "msg_type"      => $temp_type,
        "sender"        => $send_name,
        "sender_hp"     => $send_phone,
        "receiver"      => $dest_name,
        "receiver_hp"   => $dest_phone,
        "reserved_time" => $exp_datetime,
        "title"         => $title,
        "message"       => $msg_body_conv,
        "temp_key"      => $temp_key,
        "btn_key"       => $btn_key,
        "btn_content"   => $btn_content,
        "cinfo"         => $c_info,
    );

    if($kakao_model->sendKakaoMessage($send_kakao_list)){
        $upd_reserve_kakao_list[$cr_no] = $msg_id;
    }
}

# 확인후 저장
if(!empty($upd_reserve_kakao_list))
{
    foreach($upd_reserve_kakao_list as $r_no => $send_id){
        $upd_sql = "UPDATE crm_cert_reservation SET cr_state='2', send_id='{$send_id}' WHERE cr_no='{$r_no}'";
        mysqli_query($my_db, $upd_sql);
    }
}