<?php
ini_set('max_execution_time', 500);
require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/helper/_message.php");
require_once("/home/master/wplanet/v1/inc/model/Message.php");
require_once("/home/master/wplanet/v1/inc/model/Custom.php");

mysqli_query($my_db,'set names utf8');

if ( mysqli_connect_errno() ) {
    echo mysqli_connect_error();
    exit;
}

date_default_timezone_set("Asia/Seoul");

# r_state 전송중 추가, req_date => exp_date로 변경, 상품타입 및 정보 저장
$cur_datetime   = date('Y-m-d H:i:s');
$cur_date       = date('Y-m-d');
$crm_model      = Message::Factory();
$crm_model->setCrmCertReservationInit();
$cert_prd_model = Message::Factory();
$cert_prd_model->setMainInit("crm_cert_product", "no");
$temp_model     = Custom::Factory();
$temp_model->setMainInit("crm_template", "t_no");

$cert_temp_no_list = getCertTempNoList();

$cert_next_sql  = "
    SELECT
        `cc`.`no` as cert_no,
        `ccp`.`no` AS cert_prd_no,
        `ccp`.prd_no,
        `ccp`.prd_type,
        `ccp`.sms_term,
        `ccp`.base_date,
        `ccp`.next_date,
        `cc`.`name`,
        `cc`.hp,
        `cc`.set_time,
        `cc`.last_date
    FROM crm_cert_product AS `ccp`
    LEFT JOIN crm_cert AS `cc` ON `cc`.`no`=`ccp`.cert_no
    WHERE `ccp`.next_date='{$cur_date}' AND `ccp`.active='1' AND `cc`.status='1'
";
$cert_next_query  = mysqli_query($my_db, $cert_next_sql);
while($cert_next = mysqli_fetch_assoc($cert_next_query))
{
    $send_date_list = [];
    $send_name      = $cert_next['name'];
    $send_phone     = $cert_next['hp'];
    $cert_no        = $cert_next['cert_no'];
    $cert_prd_no    = $cert_next['cert_prd_no'];
    $exp_date       = "{$cert_next['next_date']} {$cert_next['set_time']}:00:00";
    $next_date      = date("Y-m-d", strtotime("{$exp_date} +{$cert_next['sms_term']} months"));
    $last_date      = $cert_next['last_date'];
    $temp_no        = $cert_temp_no_list[$cert_next['prd_no']][$cert_next['prd_type']];
    $temp_item      = $temp_model->getItem($temp_no);

    $chk_sql        = "SELECT count(cr_no) as cnt FROM crm_cert_reservation WHERE cert_no='{$cert_no}' AND cert_prd_no='{$cert_prd_no}' AND cr_state IN(1,2,3)";
    $chk_query      = mysqli_query($my_db, $chk_sql);
    $chk_result     = mysqli_fetch_assoc($chk_query);

    if($chk_result['cnt'] > 0){
        continue;
    }

    $ins_data = array(
        "cr_state"      => '1',
        "out_name"      => $send_name,
        "out_hp"        => $send_phone,
        "cert_no"       => $cert_no,
        "cert_prd_no"   => $cert_prd_no,
        "temp_no"       => $temp_no,
        "sms_type"      => $temp_item['temp_type'],
        "exp_date"      => $exp_date,
        "regdate"       => $cur_datetime
    );

    if($crm_model->insert($ins_data))
    {
        $cert_prd_model->update(array("no" => $cert_prd_no, "active" => 3));

        if($next_date <= $last_date)
        {
            $cert_prd_data = array(
                "cert_no"   => $cert_no,
                "prd_no"    => $cert_next['prd_no'],
                "prd_type"  => $cert_next['prd_type'],
                "base_date" => $cert_next['base_date'],
                "next_date" => $next_date,
                "sms_term"  => $cert_next['sms_term'],
                "active"    => "1",
            );
            $cert_prd_model->insert($cert_prd_data);
        }
    }
}


