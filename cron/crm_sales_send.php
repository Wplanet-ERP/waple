<?php
ini_set('max_execution_time', 500);
require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/model/Message.php");

mysqli_query($my_db,'set names utf8');

if ( mysqli_connect_errno() ) {
    echo mysqli_connect_error();
    exit;
}

date_default_timezone_set("Asia/Seoul");

# 보내는 템플릿 부분 확인
$sms_model              = Message::Factory();
$kakao_model            = Message::Factory();
$my_db->set_charset("utf8mb4");
$kakao_model->setCharset("utf8mb4");
$cm_idx                 = 1;
$c_info                 = "88";
$cur_datetime           = date("YmdHis");
$cur_s_date             = date('Y-m-d')." 00:00:00";
$cur_e_date             = date('Y-m-d')." 23:59:59";
$send_sms_list          = [];
$send_kakao_list        = [];
$upd_reserve_sms_list   = [];
$upd_reserve_kakao_list = [];

$crm_reservation_sql = "
    SELECT 
        cr.r_no, 
        ct.temp_type,
        cr.send_name as dest_name, 
        cr.send_phone as dest_phone, 
        cs.title,
        ct.send_key,
        ct.temp_key,
        ct.btn_key,
        ct.btn_content,
        ct.send_name,
        ct.send_phone,
        cr.exp_date,
        cr.`account`,
        ct.subject,
        ct.content,
        cr.order_number
    FROM crm_sales_reservation as cr 
    LEFT JOIN crm_set as cs ON cs.crm_no = cr.crm_no
    LEFT JOIN crm_template as ct ON ct.t_no = cs.t_no
    WHERE cr.r_state='1' AND ct.active='1' AND cr.exp_date BETWEEN '{$cur_s_date}' AND '{$cur_e_date}' 
    ORDER BY cr.exp_date ASC
";
$crm_reservation_query = mysqli_query($my_db, $crm_reservation_sql);
while($crm_reserve = mysqli_fetch_assoc($crm_reservation_query))
{
    $r_no           = $crm_reserve['r_no'];
    $temp_type      = $crm_reserve['temp_type'];
    $title          = $crm_reserve['title'];
    $send_name      = $crm_reserve['send_name'];
    $send_phone     = $crm_reserve['send_phone'];
    $dest_name      = $crm_reserve['dest_name'];
    $dest_phone     = $crm_reserve['dest_phone'];
    $content        = $crm_reserve['content'];
    $account        = $crm_reserve['account'];
    $temp_key       = $crm_reserve['temp_key'];
    $btn_key        = $crm_reserve['btn_key'];
    $btn_content    = $crm_reserve['btn_content'];
    $exp_date       = $crm_reserve['exp_date'];
    $exp_datetime   = date("YmdHis", strtotime($crm_reserve['exp_date']));
    $order_number   = $crm_reserve['order_number'];

    #데이터 변환
    $msg_body_conv = str_replace("#{고객성명}", $dest_name, $content);
    $msg_body_conv = str_replace("#{고객명}", $dest_name, $msg_body_conv);
    $msg_body_conv = str_replace("#{고객님}", $dest_name, $msg_body_conv);
    $msg_id 	   = "W{$cur_datetime}".sprintf('%04d', $cm_idx++);

    if($temp_type == "AT")
    {
        $send_kakao_list            = [];
        $send_kakao_list[$msg_id]   = array(
            "msg_id"        => $msg_id,
            "msg_type"      => $temp_type,
            "sender"        => $send_name,
            "sender_hp"     => $send_phone,
            "receiver"      => $dest_name,
            "receiver_hp"   => $dest_phone,
            "reserved_time" => $exp_datetime,
            "title"         => $title,
            "message"       => $msg_body_conv,
            "temp_key"      => $temp_key,
            "btn_key"       => $btn_key,
            "btn_content"   => $btn_content,
            "cinfo"         => $c_info,
        );

        if($kakao_model->sendKakaoMessage($send_kakao_list)){
            $upd_reserve_kakao_list[$r_no] = $msg_id;
        }
    }
    elseif($temp_type == "SMS")
    {
        $sms_phone_chk_sql      = "SELECT is_agree FROM account_refuse WHERE hp='{$dest_phone}' ORDER BY regdate DESC LIMIT 1";
        $sms_phone_chk_query    = mysqli_query($my_db, $sms_phone_chk_sql);
        $sms_phone_chk_result   = mysqli_fetch_assoc($sms_phone_chk_query);
        if(isset($sms_phone_chk_result['is_agree']) && $sms_phone_chk_result['is_agree'] == 'N'){
            $del_reserve_list[$order_number] = $order_number;
            continue;
        }

        $sms_title      = $crm_reserve['subject'];
        $sms_msg        = $msg_body_conv;
        $sms_msg_len 	= mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
        $msg_type 		= ($sms_msg_len > 90) ? "L" : "S";

        $send_sms_list[$msg_id] = array(
            "msg_id"        => $msg_id,
            "msg_type"      => $msg_type,
            "sender"        => $send_name,
            "sender_hp"     => $send_phone,
            "receiver"      => $dest_name,
            "receiver_hp"   => $dest_phone,
            "reserved_time" => $exp_datetime,
            "title"         => $sms_title,
            "content"       => $sms_msg,
            "cinfo"         => $c_info,
        );

        $upd_reserve_sms_list[$r_no] = $msg_id;
    }
}

# 확인후 저장
if(!empty($send_sms_list))
{
    if($sms_model->sendMessage($send_sms_list))
    {
        foreach($upd_reserve_sms_list as $r_no => $send_id){
            $upd_sql = "UPDATE crm_sales_reservation SET r_state='2', send_id='{$send_id}' WHERE r_no='{$r_no}'";
            mysqli_query($my_db, $upd_sql);
        }
    }
}

if(!empty($upd_reserve_kakao_list))
{
    foreach($upd_reserve_kakao_list as $r_no => $send_id){
        $upd_sql = "UPDATE crm_sales_reservation SET r_state='2', send_id='{$send_id}' WHERE r_no='{$r_no}'";
        mysqli_query($my_db, $upd_sql);
    }
}

if(!empty($del_reserve_list))
{
    foreach($del_reserve_list as $ord_no){
        $upd_sql = "UPDATE crm_sales_reservation SET r_state='5' WHERE order_number='{$ord_no}' AND exp_date >= '{$cur_s_date}'";
        mysqli_query($my_db, $upd_sql);
    }
}