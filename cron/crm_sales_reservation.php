<?php
ini_set('max_execution_time', 500);
require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/model/Message.php");

mysqli_query($my_db,'set names utf8');

if ( mysqli_connect_errno() ) {
    echo mysqli_connect_error();
    exit;
}

date_default_timezone_set("Asia/Seoul");

# r_state 전송중 추가, req_date => exp_date로 변경, 상품타입 및 정보 저장
$cur_datetime   = date('Y-m-d H:i:s');
$prev_date      = date('Y-m-d', strtotime("-1 days"));
$date_where     = "stock_date = '{$prev_date}'";
$crm_model      = Message::Factory();
$crm_model->setCrmSalesReservationInit();

$crm_set_sql    = "SELECT crm_no, income_type, income_value, dp_company, send_type, send_time, send_value, send_e_date, exp_hour, exp_min FROM crm_set WHERE crm_state='2' and send_method='1'";
$crm_set_query  = mysqli_query($my_db, $crm_set_sql);
while($crm_set = mysqli_fetch_assoc($crm_set_query))
{
    $crm_no      = $crm_set['crm_no'];
    $send_type   = $crm_set['send_type'];
    $send_e_date = $crm_set['send_e_date'];
    $send_value  = $crm_set['send_value'];
    $exp_hour    = $crm_set['exp_hour'];
    $exp_min     = $crm_set['exp_min'];
    $crm_where   = "";

    if($crm_set['income_type'] == '2'){
        $crm_where = "prd_no IN(SELECT prd_no FROM product_cms_relation WHERE option_no IN({$crm_set['income_value']}) AND display='1')";
    }else{
        $crm_where = "prd_no IN({$crm_set['income_value']})";
    }

    if(!empty($crm_set['dp_company'])){
        $crm_where .= " AND dp_c_no IN({$crm_set['dp_company']})";
    }

    # 주문 검색
    $crm_order_sql   = "SELECT recipient, REPLACE(recipient_hp,'-','') as phone, order_number, stock_date, `account` FROM work_cms WHERE {$date_where} AND {$crm_where} AND delivery_state='4' AND unit_price > 0 AND recipient_hp like '010%' group by recipient_hp";
    $crm_order_query = mysqli_query($my_db, $crm_order_sql);
    while($crm_order = mysqli_fetch_assoc($crm_order_query))
    {
        $send_name      = $crm_order['recipient'];
        $send_phone     = $crm_order['phone'];
        $order_number   = $crm_order['order_number'];
        $stock_date     = $crm_order['stock_date'];

        $send_date_list = [];
        $chk_sql        = "SELECT count(r_no) as cnt FROM crm_sales_reservation WHERE send_phone='{$send_phone}' AND crm_no='{$crm_no}'";
        $chk_query      = mysqli_query($my_db, $chk_sql);
        $chk_result     = mysqli_fetch_assoc($chk_query);

        if($chk_result['cnt'] < 1){
            $send_date_list[] = date('Y-m-d', strtotime("{$stock_date} +{$crm_set['send_value']} day"))." {$exp_hour}:{$exp_min}";
        }

        if($send_date_list)
        {
            foreach($send_date_list as $exp_date)
            {
                $ins_data = array(
                    "r_state"       => '1',
                    "send_name"     => $send_name,
                    "send_phone"    => $send_phone,
                    "order_number"  => $order_number,
                    "account"       => $crm_order['account'],
                    "crm_no"        => $crm_no,
                    "income_type"   => $crm_set['income_type'],
                    "income_value"  => $crm_set['income_value'],
                    "exp_date"      => $exp_date,
                    "regdate"       => $cur_datetime
                );
                $crm_model->insert($ins_data);
            }
        }
    }
}


