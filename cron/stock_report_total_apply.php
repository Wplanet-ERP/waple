<?php
ini_set('max_execution_time', '3000');
ini_set('memory_limit', '2G');

require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/helper/work_cms.php");
require_once("/home/master/wplanet/v1/inc/model/ProductCmsStock.php");
require_once("/home/master/wplanet/v1/inc/model/ProductCmsUnit.php");
require_once("/home/master/wplanet/v1/inc/model/WorkCms.php");

$stock_mon      = date("Y-m", strtotime("-1 months"));
$stock_mon_day  = date("t", strtotime($stock_mon));
$stock_s_date   = "{$stock_mon}-01";
$stock_e_date   = "{$stock_mon}-{$stock_mon_day}";
$tmp_log_c_no   = "2809";

$return_unit_model  = WorkCms::Factory();
$return_unit_model->setMainInit("work_cms_return_unit", "u_no");

# 회수리스트 단품 변경
$change_single_unit_list    = getChangeSingleUnitList();
$chk_sub_unit_list          = array_keys($change_single_unit_list);
$chk_unit_text              = implode(",", $chk_sub_unit_list);
$total_return_list          = [];

$return_unit_sql    = "SELECT * FROM work_cms_return_unit WHERE `option` IN({$chk_unit_text})";
$return_unit_query  = mysqli_query($my_db, $return_unit_sql);
while($return_unit = mysqli_fetch_assoc($return_unit_query))
{
    $change_unit_data       = $change_single_unit_list[$return_unit['option']];
    $total_return_list[]    = array("u_no" => $return_unit['u_no'], "option" => $change_unit_data["option"], "sku" => $change_unit_data["sku"]);
}

if(!empty($total_return_list)){
    $return_unit_model->multiUpdate($total_return_list);
}

# 입고/반출리스트 처리
$unit_model         = ProductCmsUnit::Factory();
$total_stock_model  = ProductCmsStock::Factory();
$total_stock_model->setStockReport();
$total_stock_list   = [];
$cur_report_date    = date("Y-m-d H:i:s");
$report_memo        = "타계정[반출], 이동[반출] 의 택배 발송건, 판매반출 차감";

$chk_stock_sql = "
    SELECT 
        prd_unit,
        SUM(stock_qty) AS total_qty
    FROM product_cms_stock_report 
    WHERE (confirm_state IN('6','10') AND `type`='3') AND (regdate BETWEEN '{$stock_s_date}' AND '{$stock_e_date}')
    AND log_c_no='{$tmp_log_c_no}'
    GROUP BY prd_unit
";
$chk_stock_query    = mysqli_query($my_db, $chk_stock_sql);
while($chk_stock = mysqli_fetch_assoc($chk_stock_query))
{
    $unit_item      = $unit_model->getItem($chk_stock['prd_unit'], $tmp_log_c_no);
    $unit_type      = ($unit_item['type'] == "1") ? "상품" : "부속품";

    $total_stock_list[$chk_stock['prd_unit']] = array(
        'regdate'       => $stock_e_date,
        'report_type'   => "2",
        'type'          => "3",
        'state'         => "기간판매반출",
        'confirm_state' => "9",
        'brand'         => $unit_item['brand_name'],
        'prd_kind'      => $unit_type,
        'prd_name'      => $unit_item['option_name'],
        'prd_unit'      => $chk_stock['prd_unit'],
        'sup_c_no'      => $unit_item['sup_c_no'],
        'log_c_no'      => $tmp_log_c_no,
        'option_name'   => $unit_item['option_name'],
        'sku'           => $unit_item['sku'],
        'memo'          => $report_memo,
        'stock_type'    => $unit_item['warehouse'],
        'stock_qty'     => $chk_stock['total_qty']*-1,
        'reg_s_no'      => "167",
        'report_date'   => $cur_report_date,
    );
}

if(!empty($total_stock_list)){
    $total_stock_model->multiInsert($total_stock_list);
}

?>
