<?php
ini_set('max_execution_time', 500);

require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/helper/work_cms.php");
require_once("/home/master/wplanet/v1/inc/model/ProductCmsStock.php");
require_once("/home/master/wplanet/v1/inc/model/CommerceOrder.php");

# 이전달꺼 데이터 다시 적용
$sch_move_date      = date('Y-m', strtotime("-2 months"));
$sch_prev_date      = date('Y-m', strtotime("-1 months {$sch_move_date}"));
$stock_init_model   = ProductCmsStock::Factory();
$stock_init_model->setMainInit("product_cms_stock_confirm", "no");
$stock_ins_model    = ProductCmsStock::Factory();

# 단품 처리
$change_single_unit_list    = getChangeSingleUnitList();
$chk_single_unit_list       = array_keys($change_single_unit_list);

$commerce_order_tmp_list    = [];
$commerce_order_list        = [];
$commerce_order_sql         = "
    SELECT 
        co.option_no,
        co.total_price,
        co.quantity
    FROM commerce_order `co`
    LEFT JOIN commerce_order_set `cos` ON `cos`.no=`co`.set_no
    WHERE DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1' AND (SELECT pcsr.confirm_state FROM product_cms_stock_report pcsr WHERE pcsr.ord_no=co.`no`) IN(1,2)
";
$commerce_order_query = mysqli_query($my_db, $commerce_order_sql);
while($commerce_order = mysqli_fetch_assoc($commerce_order_query))
{
    $total_price = $commerce_order['total_price'];
    $quantity    = $commerce_order['quantity'];

    if(isset($commerce_order_tmp_list[$commerce_order['option_no']])){
        $commerce_order_tmp_list[$commerce_order['option_no']]['total_price'] += $total_price;
        $commerce_order_tmp_list[$commerce_order['option_no']]['total_qty']   += $quantity;
    }else{
        $commerce_order_tmp_list[$commerce_order['option_no']] = array(
            "total_price"   => $total_price,
            "total_qty"     => $quantity
        );
    }
}

if(!empty($commerce_order_tmp_list))
{
    foreach($commerce_order_tmp_list as $key => $comm_tmp)
    {
        $ord_qty = $comm_tmp['total_qty'] > 0 ? $comm_tmp['total_qty'] : 1;
        $commerce_order_list[$key] = round($comm_tmp['total_price']/$ord_qty, 1);
    }
}

foreach($commerce_order_list as $key => $org_price)
{
    $upd_sql = "UPDATE product_cms_stock_confirm SET org_price='{$org_price}' WHERE prd_unit='{$key}' AND base_mon='{$sch_move_date}' AND base_type='start'";
    mysqli_query($my_db, $upd_sql);

    if(in_array($key, $chk_single_unit_list))
    {
        $new_key = $change_single_unit_list[$key]['option'];
        $commerce_order_list[$new_key] = $org_price;

        $upd_sub_sql = "UPDATE product_cms_stock_confirm SET org_price='{$org_price}' WHERE prd_unit='{$new_key}' AND base_mon='{$sch_move_date}' AND base_type='start'";
        mysqli_query($my_db, $upd_sub_sql);
    }
}

# 새로 등록된 구성품등 새로 생성
$with_log_c_no  = '2809';
$stock_init_sql = "
    SELECT
        *
    FROM (
        SELECT
            prd.`no`,
            pcsw.`no` as ware_no,
            prd.`brand`,
            (SELECT c.c_name FROM company c WHERE c.c_no=prd.brand) as brand_name,
            pcsw.warehouse,
            (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=prd.`no` AND pcum.log_c_no='{$with_log_c_no}' LIMIT 1) AS base_sku, 
		    (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=prd.`no` AND pcum.log_c_no=pcsw.log_c_no LIMIT 1) AS ware_sku, 
            prd.option_name,
            pcsw.log_c_no,
            (SELECT c.c_name FROM company c WHERE c.c_no=pcsw.log_c_no) AS log_c_name
        FROM product_cms_unit AS prd
        LEFT JOIN product_cms_stock_warehouse AS pcsw ON 1=1
        WHERE is_stock='1'
        GROUP BY prd.`no`, pcsw.log_c_no, pcsw.warehouse
    ) as main
    WHERE (SELECT COUNT(pcsc.`no`) FROM product_cms_stock_confirm pcsc WHERE pcsc.prd_unit=main.`no` AND pcsc.base_mon='{$sch_move_date}' AND pcsc.warehouse=main.warehouse AND pcsc.log_c_no=main.log_c_no AND pcsc.`base_type`='start') = 0
    ORDER BY `no` ASC, warehouse ASC
";
$stock_init_query = mysqli_query($my_db, $stock_init_sql);
while($stock_init = mysqli_fetch_assoc($stock_init_query))
{
    $org_price  = isset($commerce_order_list[$stock_init['no']]) ? $commerce_order_list[$stock_init['no']] : 0;
    $sku        = !empty($stock_init['ware_sku']) ? $stock_init['ware_sku'] : $stock_init['base_sku'];

    if($stock_init['log_c_name'] == "와이즈플래닛 미디어커머스"){
        $stock_init['log_c_name'] = "와이즈미디어커머스";
    }

    $stock_chk_data  = array(
        "prd_unit"      => $stock_init['no'],
        "prd_key"       => $stock_init['no']."-".$stock_init['ware_no'],
        "option_name"   => addslashes(trim($stock_init['option_name'])),
        "sku"           => addslashes(trim($sku)),
        "brand"         => $stock_init['brand'],
        "brand_name"    => $stock_init['brand_name'],
        "base_type"     => "start",
        "base_mon"      => $sch_move_date,
        "log_c_no"      => $stock_init['log_c_no'],
        "log_company"   => $stock_init['log_c_name'],
        "warehouse"     => $stock_init['warehouse'],
        "qty"           => 0,
        "org_price"     => $org_price,
        "base_price"    => 0,
    );
    $stock_init_model->insert($stock_chk_data);
}

# 회수리스트 처리
$return_list      = [];
$return_prd_sql   = "SELECT `option`, SUM(quantity) as qty FROM work_cms_return_unit WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_return wcr WHERE wcr.return_state='6' AND DATE_FORMAT(wcr.return_date,'%Y-%m')='{$sch_move_date}') AND `type` IN(1,2,3,4) GROUP BY `option`";
$return_prd_query = mysqli_query($my_db, $return_prd_sql);
while($return_prd = mysqli_fetch_assoc($return_prd_query))
{
    if(!isset($return_list[$return_prd['option']])){
        $return_list[$return_prd['option']] = 0;
    }
    
    $return_list[$return_prd['option']] += $return_prd['qty'];
}

# 쿼리용 날짜
$sch_move_s_date    = $sch_move_date."-01";
$end_day            = date('t', strtotime($sch_move_date));
$sch_move_e_date    = $sch_move_date."-".$end_day;

# 재고이동 리스트
$stock_move_list   = [];
$stock_move_sql     = "SELECT prd_unit, in_warehouse, out_warehouse, in_qty, out_qty FROM product_cms_stock_transfer pcst WHERE (pcst.move_date BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}')";
$stock_move_query   = mysqli_query($my_db, $stock_move_sql);
while($stock_move = mysqli_fetch_assoc($stock_move_query))
{
    if(!isset($stock_move_list[$stock_move['prd_unit']][$stock_move['in_warehouse']])){
        $stock_move_list[$stock_move['prd_unit']][$stock_move['in_warehouse']] = array("in" => 0, "out" => 0);
    }

    if(!isset($stock_move_list[$stock_move['prd_unit']][$stock_move['out_warehouse']])){
        $stock_move_list[$stock_move['prd_unit']][$stock_move['out_warehouse']] = array("in" => 0, "out" => 0);
    }
    
    $stock_move_list[$stock_move['prd_unit']][$stock_move['in_warehouse']]["in"]   += $stock_move['in_qty'];
    $stock_move_list[$stock_move['prd_unit']][$stock_move['out_warehouse']]["out"] += $stock_move['out_qty'];
}

# 입고/반출 리스트
$stock_list         = [];
$stock_report_sql   = "SELECT * FROM product_cms_stock_report pcsr WHERE (pcsr.regdate BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}') AND pcsr.confirm_state > 0 AND pcsr.state NOT IN('기간반품입고','기간판매반출','기간이동입고','기간이동반출')  AND move_no='0'";
$stock_report_query = mysqli_query($my_db, $stock_report_sql);
while($stock_report = mysqli_fetch_assoc($stock_report_query))
{
    $stock_kind = "";
    switch($stock_report['confirm_state']){
        case "1":
        case "2":
            $stock_kind = "in_point";
            break;
        case "3":
            $stock_kind = "in_move";
            break;
        case "4":
            $stock_kind = "in_etc";
            break;
        case "5":
            $stock_kind = "in_return";
            break;
        case "6":
            $stock_kind = "out_move";
            break;
        case "8":
            $stock_kind = "out_etc";
            break;
        case "9":
            $stock_kind = "out_point";
            break;
        case "10":
            $stock_kind = "out_return";
            break;

    }

    if(!empty($stock_kind))
    {
        if(!isset($stock_list[$stock_report['prd_unit']][$stock_report['stock_type']])){
            $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']] = array(
                "in_point"      => 0,
                "in_move"       => 0,
                "in_etc"        => 0,
                "in_return"     => 0,
                "out_point"     => 0,
                "out_move"      => 0,
                "out_return"    => 0,
                "out_etc"       => 0,
            );
        }

        $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']][$stock_kind] += $stock_report['stock_qty'];
    }
}

# 재고자산수불부 쿼리
$product_receipt_sql = "
    SELECT
        *
    FROM product_cms_stock_confirm as rs 
    WHERE `base_type`='start' AND base_mon='{$sch_move_date}'
    ORDER BY rs.option_name ASC, rs.warehouse ASC
";
$product_receipt_query  = mysqli_query($my_db, $product_receipt_sql);
$stock_chk_out_ins      = [];
while($product_receipt = mysqli_fetch_assoc($product_receipt_query))
{
    $stock_list_data                        = isset($stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];
    $move_list_data                         = isset($stock_move_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_move_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];

    $product_receipt['in_base_qty']         = $product_receipt['qty'];
    $product_receipt['in_point_qty']        = !empty($stock_list_data) ? $stock_list_data['in_point'] : 0;
    $product_receipt['in_move_sub_qty']     = !empty($stock_list_data) ? $stock_list_data['in_move'] : 0;
    $product_receipt['in_org_move_qty']     = !empty($move_list_data) && isset($move_list_data["in"]) ? $move_list_data["in"] : 0;
    $product_receipt['in_move_qty']         = $product_receipt['in_org_move_qty'] + $product_receipt['in_move_sub_qty'];
    $product_receipt['in_etc_qty']          = !empty($stock_list_data) ? $stock_list_data['in_etc'] : 0;
    $product_receipt['in_return_sub_qty']   = ($product_receipt['warehouse'] == "2-3 검수대기창고(반품건)" && isset($return_list[$product_receipt['prd_unit']])) ? $return_list[$product_receipt['prd_unit']] : 0;
    $product_receipt['in_return_org_qty']   = !empty($stock_list_data) ? $stock_list_data['in_return'] : 0;
    $product_receipt['in_return_qty']       = $product_receipt['in_return_org_qty'] + $product_receipt['in_return_sub_qty'];

    $product_receipt['in_base_price']       = $product_receipt['base_price'] * $product_receipt['in_base_qty'];
    $product_receipt['in_point_price']      = $product_receipt['org_price'] * $product_receipt['in_point_qty'];
    $product_receipt['in_move_price']       = $product_receipt['org_price'] * $product_receipt['in_move_qty'];
    $product_receipt['in_return_price']     = $product_receipt['org_price'] * $product_receipt['in_return_qty'];
    $product_receipt['in_etc_price']        = $product_receipt['org_price'] * $product_receipt['in_etc_qty'];
    $product_receipt['in_qty']              = $product_receipt['in_point_qty'] + $product_receipt['in_move_qty'] + $product_receipt['in_return_qty'] + $product_receipt['in_etc_qty'];
    $product_receipt['in_price']            = $product_receipt['in_point_price'] + $product_receipt['in_move_price'] + $product_receipt['in_return_price'] + $product_receipt['in_etc_price'];

    $product_receipt['out_point_qty']       = !empty($stock_list_data) ? $stock_list_data['out_point']*-1 : 0;
    $product_receipt['out_org_move_qty']    = !empty($move_list_data) && isset($move_list_data["out"]) ? $move_list_data["out"] : 0;
    $product_receipt['out_move_sub_qty']    = !empty($stock_list_data) ? $stock_list_data['out_move']*-1 : 0;
    $product_receipt['out_move_qty']        = $product_receipt['out_org_move_qty'] + $product_receipt['out_move_sub_qty'];
    $product_receipt['out_return_qty']      = !empty($stock_list_data) ? $stock_list_data['out_return']*-1 : 0;
    $product_receipt['out_etc_qty']         = !empty($stock_list_data) ? $stock_list_data['out_etc']*-1 : 0;
    $product_receipt['out_qty']             = $product_receipt['out_point_qty'] + $product_receipt['out_move_qty'] + $product_receipt['out_return_qty'] + $product_receipt['out_etc_qty'];
    $product_receipt['out_base_qty']        = $product_receipt['in_base_qty'] + $product_receipt['in_qty'] - $product_receipt['out_qty'];

    # 판매반출 금액 계산
    $sales_price        = ($product_receipt['in_base_qty']+$product_receipt['in_qty'] > 0) ? (($product_receipt['in_base_price']+$product_receipt['in_price']) / ($product_receipt['in_base_qty']+$product_receipt['in_qty'])) : 0;

    $stock_chk_out_ins[] = array(
        "prd_unit"      => $product_receipt['prd_unit'],
        "prd_key"       => $product_receipt['prd_key'],
        "option_name"   => addslashes(trim($product_receipt['option_name'])),
        "sku"           => addslashes(trim($product_receipt['sku'])),
        "brand"         => $product_receipt['brand'],
        "brand_name"    => $product_receipt['brand_name'],
        "base_type"     => "end",
        "base_mon"      => $sch_move_date,
        "log_c_no"      => $product_receipt['log_c_no'],
        "log_company"   => $product_receipt['log_company'],
        "warehouse"     => $product_receipt['warehouse'],
        "qty"           => (int)$product_receipt['out_base_qty'],
        "org_price"     => $product_receipt['org_price'],
        "base_price"    => round($sales_price),
    );
}

if(!empty($stock_chk_out_ins))
{
    $stock_ins_model->setConfirmInitData($stock_chk_out_ins);
}