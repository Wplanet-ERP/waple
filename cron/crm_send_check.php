<?php
ini_set('max_execution_time', 500);
require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/model/Message.php");

mysqli_query($my_db,'set names utf8');

if ( mysqli_connect_errno() ) {
    echo mysqli_connect_error();
    exit;
}

date_default_timezone_set("Asia/Seoul");

# 확인후 crm 상태값 변경
$cur_time           = date('Y-m-d H').":00:00";
$crm_sales_model    = Message::Factory();
$crm_sales_model->setCrmSalesReservationInit();

$crm_cert_model    = Message::Factory();
$crm_cert_model->setCrmCertReservationInit();

$send_check_sql     = "SELECT cr.r_no, DATE_FORMAT(cr.exp_date, '%Y%m') as table_date, cr.send_id FROM crm_sales_reservation cr WHERE cr.r_state='2' AND exp_date < '{$cur_time}'";
$send_check_query   = mysqli_query($my_db, $send_check_sql);
while($send_check = mysqli_fetch_assoc($send_check_query))
{
    $easy_chk_sql       = "SELECT STATUS, CALL_STATUS, SEND_TIME FROM EASY_SEND_LOG WHERE MSG_ID='{$send_check['send_id']}' LIMIT 1";
    $easy_chk_query     = mysqli_query($my_db, $easy_chk_sql);
    $easy_chk_result    = mysqli_fetch_assoc($easy_chk_query);
    $easy_status        = $easy_chk_result['STATUS'];
    $easy_call_status   = $easy_chk_result['CALL_STATUS'];
    $easy_send_time     = $easy_chk_result['SEND_TIME'];
    $easy_upd_data      = [];

    if(!empty($easy_status))
    {
        $easy_upd_data = array("r_no" => $send_check['r_no'], "send_date" => $easy_send_time);

        if ($easy_status == "2") {
            $easy_upd_data["r_state"] = "3";
        } elseif($easy_status == "3" && $easy_call_status == "K001") {
            $easy_upd_data["r_state"] = "2";
        } else {
            $easy_upd_data["r_state"] = "4";
        }
    }else{
        $easy_upd_data = array("r_no" => $send_check['r_no'], "r_state" => "5");
    }

    $crm_sales_model->update($easy_upd_data);
}

$cert_send_check_sql     = "SELECT ccr.cr_no, DATE_FORMAT(ccr.exp_date, '%Y%m') as table_date, ccr.send_id FROM crm_cert_reservation ccr WHERE ccr.cr_state='2' AND exp_date < '{$cur_time}'";
$cert_send_check_query   = mysqli_query($my_db, $cert_send_check_sql);
while($cert_send_check = mysqli_fetch_assoc($cert_send_check_query))
{
    $easy_filter_chk_sql       = "SELECT STATUS, CALL_STATUS, SEND_TIME FROM EASY_SEND_FILTER_LOG WHERE MSG_ID='{$cert_send_check['send_id']}' LIMIT 1";
    $easy_filter_chk_query     = mysqli_query($my_db, $easy_filter_chk_sql);
    $easy_filter_chk_result    = mysqli_fetch_assoc($easy_filter_chk_query);
    $easy_filter_status        = $easy_filter_chk_result['STATUS'];
    $easy_filter_call_status   = $easy_filter_chk_result['CALL_STATUS'];
    $easy_filter_send_time     = $easy_filter_chk_result['SEND_TIME'];
    $easy_filter_upd_data      = [];

    if(!empty($easy_filter_status))
    {
        $easy_filter_upd_data = array("cr_no" => $cert_send_check['cr_no'], "send_date" => $easy_filter_send_time);

        if ($easy_filter_status == "2") {
            $easy_filter_upd_data["cr_state"] = "3";
        } elseif($easy_filter_status == "3" && $easy_filter_status == "K001") {
            $easy_filter_upd_data["cr_state"] = "2";
        } else {
            $easy_filter_upd_data["cr_state"] = "4";
        }
    }else{
        $easy_filter_upd_data = array("cr_no" => $cert_send_check['cr_no'], "cr_state" => "5");
    }

    $crm_cert_model->update($easy_filter_upd_data);
}
