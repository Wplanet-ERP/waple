<?php
ini_set('max_execution_time', 300);
require_once("/home/master/wplanet/libs/dbconfig.php");

mysqli_query($my_db,'set names utf8');

if ( mysqli_connect_errno() ) {
    echo mysqli_connect_error();
    exit;
}

date_default_timezone_set("Asia/Seoul");

// 업무상태가 재택근무인 리스트
$sch_cur_date             = date('Y-m-d')." 00:00:00";
$working_state_list_sql   = "SELECT s_no, (SELECT team FROM staff s WHERE s.s_no = ws.s_no) as team FROM working_state ws WHERE `no` IN(SELECT MAX(NO) FROM working_state ws GROUP BY s_no) AND `state`=2 AND regdate < '{$sch_cur_date}'";
$working_state_list_query = mysqli_query($my_db, $working_state_list_sql);
$working_state_list       = [];

while($working_state = mysqli_fetch_assoc($working_state_list_query))
{
    $working_state_list[] = array('team' => $working_state['team'], 's_no' => $working_state['s_no']);
}

$ins_working_state_sql = "INSERT INTO working_state(`s_no`, `team`, `regdate`,`state`) VALUES ";
$comma = "";

if(!!$working_state_list)
{
    $cur_date = date('Y-m-d H:i:s');
    foreach($working_state_list as $data)
    {
        $s_no  = $data['s_no'];
        $team  = $data['team'];
        $state = 1;

        $ins_working_state_sql .= $comma."('{$s_no}', '{$team}', '{$cur_date}', '{$state}')";
        $comma = " , ";
    }

    mysqli_query($my_db, $ins_working_state_sql);
}

$sch_prev_date = date("Y-m-d", strtotime("-1 days"))." 00:00:00";
$asset_reservation_sql   = "SELECT ar.as_r_no FROM asset_reservation AS ar LEFT JOIN `asset` AS a ON a.as_no=ar.as_no WHERE a.share_type='3' AND ar.work_state='2' AND r_e_date < '{$sch_prev_date}'";
$asset_reservation_query = mysqli_query($my_db, $asset_reservation_sql);
$asset_reservation_list  = [];
while($asset_reservation = mysqli_fetch_assoc($asset_reservation_query))
{
    if(isset($asset_reservation['as_r_no']) && !empty($asset_reservation['as_r_no'])){
        $asset_reservation_list[$asset_reservation['as_r_no']] = "UPDATE asset_reservation SET work_state='10' WHERE as_r_no='{$asset_reservation['as_r_no']}'";
    }
}

if(!empty($asset_reservation_list)){
    foreach($asset_reservation_list as $upd_sql){
        if(!empty($upd_sql)){
            mysqli_query($my_db, $upd_sql);
        }
    }
}