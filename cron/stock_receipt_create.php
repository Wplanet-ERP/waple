<?php
ini_set('max_execution_time', 500);
require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/helper/work_cms.php");
require_once("/home/master/wplanet/v1/inc/model/ProductCmsStock.php");
require_once("/home/master/wplanet/v1/inc/model/CommerceOrder.php");

$sch_move_date  = date("Y-m", strtotime("-1 months"));
$sch_prev_date  = date('Y-m', strtotime("-1 months {$sch_move_date}"));

$stock_model            = ProductCmsStock::Factory();
$commerce_order_model   = CommerceOrder::Factory();

# 단품처리
$change_single_unit_list    = getChangeSingleUnitList();
$chk_single_unit_list       = array_keys($change_single_unit_list);

# 발주관련 데이터(기본 가격 설정)
$commerce_order_tmp_list    = [];
$commerce_order_list        = [];
$commerce_order_sql         = "
    SELECT 
        co.set_no,
        co.option_no,
        co.unit_price, 
        co.quantity,
        co.supply_price,
        co.total_price,
        co.vat,
        co.group,
        (SELECT SUM(sub.supply_price) FROM commerce_order sub WHERE sub.set_no=co.set_no AND sub.`group`=co.`group`) as sum_sup_price,
        (SELECT SUM(sub.price) FROM commerce_order_etc sub WHERE sub.set_no=co.set_no AND sub.group_no=co.`group`) as sum_etc_price,
        (SELECT g.usd_rate FROM commerce_order_group g WHERE g.set_no=co.set_no AND g.group_no=co.`group`) as group_usd_rate,
        `cos`.sup_loc_type
    FROM commerce_order `co`
    LEFT JOIN commerce_order_set `cos` ON `cos`.no=`co`.set_no
    WHERE DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1' AND (SELECT pcsr.confirm_state FROM product_cms_stock_report pcsr WHERE pcsr.ord_no=co.`no`) IN(1,2)
";
$commerce_order_query = mysqli_query($my_db, $commerce_order_sql);
while($commerce_order = mysqli_fetch_assoc($commerce_order_query))
{
    $total_price    = $commerce_order['total_price'];
    $quantity       = $commerce_order['quantity'];

    if(isset($commerce_order_tmp_list[$commerce_order['option_no']])){
        $commerce_order_tmp_list[$commerce_order['option_no']]['total_price'] += $total_price;
        $commerce_order_tmp_list[$commerce_order['option_no']]['total_qty']   += $quantity;
    }else{
        $commerce_order_tmp_list[$commerce_order['option_no']] = array(
            "total_price"   => $total_price,
            "total_qty"     => $quantity
        );
    }
}

if(!empty($commerce_order_tmp_list))
{
    foreach($commerce_order_tmp_list as $key => $comm_tmp)
    {
        $ord_qty    = $comm_tmp['total_qty'] > 0 ? $comm_tmp['total_qty'] : 1;
        $ord_price  = round($comm_tmp['total_price']/$ord_qty, 1);
        $commerce_order_list[$key] = $ord_price;

        if(in_array($key, $chk_single_unit_list)){
            $new_key = $change_single_unit_list[$key]['option'];
            $commerce_order_list[$new_key] = $ord_price;
        }
    }
}

# 기초 재고 리스트 만들기
$product_warehouse_sql = "
    SELECT
        *
    FROM product_cms_stock_confirm main
    WHERE main.base_mon='{$sch_prev_date}' AND main.base_type='end'
        AND (SELECT COUNT(pcsc.`no`) FROM product_cms_stock_confirm pcsc WHERE pcsc.prd_unit=main.prd_unit AND pcsc.base_mon='{$sch_move_date}' AND pcsc.log_c_no=main.log_c_no AND pcsc.warehouse=main.warehouse AND pcsc.`base_type`='start') = 0
    ORDER BY prd_unit ASC, warehouse ASC
";
$product_warehouse_query = mysqli_query($my_db, $product_warehouse_sql);
$stock_chk_data     = [];
$stock_chk_in_stock = [];
while($product_warehouse = mysqli_fetch_assoc($product_warehouse_query))
{
    $stock_org_price    = isset($commerce_order_list[$product_warehouse['prd_unit']]) ? $commerce_order_list[$product_warehouse['prd_unit']] : $product_warehouse['org_price'];

    $stock_chk_data[]   = array(
        "prd_unit"      => $product_warehouse['prd_unit'],
        "prd_key"       => $product_warehouse['prd_key'],
        "option_name"   => addslashes(trim($product_warehouse['option_name'])),
        "sku"           => addslashes(trim($product_warehouse['sku'])),
        "brand"         => $product_warehouse['brand'],
        "brand_name"    => $product_warehouse['brand_name'],
        "base_type"     => "start",
        "base_mon"      => $sch_move_date,
        "log_c_no"      => $product_warehouse['log_c_no'],
        "log_company"   => $product_warehouse['log_company'],
        "warehouse"     => $product_warehouse['warehouse'],
        "qty"           => (int)$product_warehouse['qty'],
        "org_price"     => $stock_org_price,
        "base_price"    => $product_warehouse['base_price'],
    );
}

if(!empty($stock_chk_data)){
    $stock_model->setConfirmInitData($stock_chk_data);
}