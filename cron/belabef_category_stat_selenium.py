import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime, timedelta
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from bs4 import BeautifulSoup

chrome_options=Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')

driver = webdriver.Chrome('/usr/local/bin/chromedriver', options=chrome_options)

#페이지 로그인
driver.implicitly_wait(3)
driver.get("https://drbodylab.imweb.me/login")

id=driver.find_element_by_name('uid')
id.send_keys("planetmc@daum.net")

pw = driver.find_element_by_name('passwd')
pw.send_keys("Wise2023!!")

submit_btn = driver.find_element_by_class_name('btn-block')
submit_btn.send_keys('\n')

try:
    #날짜계산
    time2 = datetime.now()
    now = time2
    cur_date = now.strftime('%Y-%m-%d')
    cur_date_time = now.strftime('%Y-%m-%d %H:%M')
    cur_date_hour = now.strftime('%H')
    cur_date_weekday = now.weekday()
    dateDict = {0: '월요일', 1:'화요일', 2:'수요일', 3:'목요일', 4:'금요일', 5:'토요일', 6:'일요일'}

    prev = time2 + timedelta(days=-1)
    prev_date = prev.strftime('%Y-%m-%d')

    if cur_date_hour == '00':
        cur_date = prev_date
        cur_date_hour = '24'
    elif cur_date_hour == '0':
        cur_date = prev_date
        cur_date_hour = '24'

    # Total URL 이동
    time.sleep(5)
    total_url = "https://drbodylab.imweb.me/admin/stat/shopping_old?orderby=&sort=&target=hour&date_range_type=custom&start_date="+cur_date+"&end_date="+cur_date+"&filter_order_channel=all"
    driver.get(total_url)
    time.sleep(5)

    html = driver.page_source
    soup = BeautifulSoup(html, 'html.parser')
    total_tr_data = soup.find("div",{"class": "table-responsive"}).find("tbody").find_all("tr")
    total_tr_idx  = int(cur_date_hour)+1
    total_td_data = total_tr_data[total_tr_idx].find_all("td")
    imweb_total_price = total_td_data[8].text.replace("원","")

    # Category Url 이동
    time.sleep(5)
    crawling_url = "https://drbodylab.imweb.me/admin/stat/shopping_old?orderby=category1&sort=&target=category&date_range_type=custom&start_date="+cur_date+"&end_date="+cur_date+"&filter_order_channel=all"
    driver.get(crawling_url)
    time.sleep(5)

    # 구글 Sheet 접근
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    json_file_name = '/home/master/wplanet/cron/scenic-dynamo-281305-f448443dbbd5.json'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(json_file_name, scope)
    gc = gspread.authorize(credentials)

    spreadsheet_url = 'https://docs.google.com/spreadsheets/d/1nDrFAjrtiPWtf1afrM3kSPNQ_XNjhD_8-KS048j2rGA/edit#gid=701425108'

    # 스프레스시트 문서 가져오기
    doc = gc.open_by_url(spreadsheet_url)

    # 시트 선택하기
    worksheet = doc.worksheet('아임웹 카테고리통계')

    time.sleep(5)
    # STAT 데이터
    html = driver.page_source
    soup = BeautifulSoup(html, 'html.parser')
    tr_data = soup.find("div",{"class": "table-responsive"}).find("tbody").find_all("tr")

    doc_data_list = []
    nuzam_total_list = []
    nuzam_base_list = []
    nuzam_double_list = []
    nuzam_cover_list = []
    for i in range(len(tr_data)):
        td_data = tr_data[i].find_all("td")

        if td_data and len(td_data) > 1:
            title = td_data[0].text
            if title == '닥터피엘':
                qty = td_data[3].text
                price = td_data[4].text.replace("원","")
                cancle_price = td_data[5].text.replace("원","")
                total_price = td_data[6].text.replace("원","")

                if cur_date_hour == '01':
                    doc_data_list.insert(0, [cur_date_time, dateDict[cur_date_weekday], title, qty, price, cancle_price, total_price, total_price, '', imweb_total_price, '=H3/J3'])
                else:
                    doc_data_list.insert(0, [cur_date_time, dateDict[cur_date_weekday], title, qty, price, cancle_price, total_price, '=G3-G4', '', imweb_total_price, '=H3/J3'])

            elif title == '누잠 TOTAL':
                qty = td_data[3].text
                price = td_data[4].text.replace("원","")
                cancle_price = td_data[5].text.replace("원","")
                total_price = td_data[6].text.replace("원","")

                if cur_date_hour == '01':
                    nuzam_total_list.insert(0, [cur_date_time, dateDict[cur_date_weekday], title, qty, price, cancle_price, total_price, total_price, '', imweb_total_price, '=H3/J3'])
                else:
                    nuzam_total_list.insert(0, [cur_date_time, dateDict[cur_date_weekday], title, qty, price, cancle_price, total_price, '=G3-G4', '', imweb_total_price, '=H3/J3'])

            elif title == '누잠 매트리스':
                qty = td_data[3].text
                price = td_data[4].text.replace("원","")
                cancle_price = td_data[5].text.replace("원","")
                total_price = td_data[6].text.replace("원","")

                if cur_date_hour == '01':
                    nuzam_base_list.insert(0, [cur_date_time, dateDict[cur_date_weekday], title, qty, price, cancle_price, total_price, total_price, '', imweb_total_price, '=H3/J3'])
                else:
                    nuzam_base_list.insert(0, [cur_date_time, dateDict[cur_date_weekday], title, qty, price, cancle_price, total_price, '=G3-G4', '', imweb_total_price, '=H3/J3'])

            elif title == '누잠 더블업':
                qty = td_data[3].text
                price = td_data[4].text.replace("원","")
                cancle_price = td_data[5].text.replace("원","")
                total_price = td_data[6].text.replace("원","")

                if cur_date_hour == '01':
                    nuzam_double_list.insert(0, [cur_date_time, dateDict[cur_date_weekday], title, qty, price, cancle_price, total_price, total_price, '', imweb_total_price, '=H3/J3'])
                else:
                    nuzam_double_list.insert(0, [cur_date_time, dateDict[cur_date_weekday], title, qty, price, cancle_price, total_price, '=G3-G4', '', imweb_total_price, '=H3/J3'])

            elif title == '누잠 이불':
                qty = td_data[3].text
                price = td_data[4].text.replace("원","")
                cancle_price = td_data[5].text.replace("원","")
                total_price = td_data[6].text.replace("원","")

                if cur_date_hour == '01':
                    nuzam_cover_list.insert(0, [cur_date_time, dateDict[cur_date_weekday], title, qty, price, cancle_price, total_price, total_price, '', imweb_total_price, '=H3/J3'])
                else:
                    nuzam_cover_list.insert(0, [cur_date_time, dateDict[cur_date_weekday], title, qty, price, cancle_price, total_price, '=G3-G4', '', imweb_total_price, '=H3/J3'])

    if doc_data_list:
        print("pass")
        #worksheet.insert_rows(doc_data_list, 3, value_input_option='USER_ENTERED')

    nuzam_spreadsheet_url = 'https://docs.google.com/spreadsheets/d/1dCeGYRZggfzJcNXhCu_oPB3b1sF8PYY4vD_4KDZUnhI/edit#gid=0'
    nuzam_doc = gc.open_by_url(nuzam_spreadsheet_url)

    if nuzam_total_list:
        nuzam_worksheet = nuzam_doc.worksheet('누잠 Total')
        nuzam_worksheet.insert_rows(nuzam_total_list, 3, value_input_option='USER_ENTERED')

    if nuzam_base_list:
        nuzam_worksheet = nuzam_doc.worksheet('누잠 매트리스')
        nuzam_worksheet.insert_rows(nuzam_base_list, 3, value_input_option='USER_ENTERED')

    if nuzam_double_list:
        nuzam_worksheet = nuzam_doc.worksheet('누잠 더블업')
        nuzam_worksheet.insert_rows(nuzam_double_list, 3, value_input_option='USER_ENTERED')

    if nuzam_cover_list:
        nuzam_worksheet = nuzam_doc.worksheet('누잠 이불')
        nuzam_worksheet.insert_rows(nuzam_cover_list, 3, value_input_option='USER_ENTERED')
except:
    print('fail')
finally:
    print('Finish')
    driver.quit()