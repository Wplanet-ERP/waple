<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', 500);
require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/model/WapleChat.php");
require_once("/home/master/wplanet/v1/inc/model/Message.php");
require_once("/home/master/wplanet/v1/inc/model/Advertising.php");
require_once("/home/master/wplanet/v1/inc/helper/advertising.php");

date_default_timezone_set("Asia/Seoul");

$cur_date       = date('Y-m-d');
$cur_datetime   = date('Y-m-d H:i:s');

$chat_model = WapleChat::Factory();
$chat_model->setMainInit("waple_chat_content", "wcc_no");

$chat_schedule_sql   = "SELECT * FROM waple_chat_content_schedule WHERE exp_date= '{$cur_date}' AND status='1'";
$chat_schedule_query = mysqli_query($my_db, $chat_schedule_sql);
$chat_ins_data       = [];
while($chat_schedule = mysqli_fetch_assoc($chat_schedule_query))
{
    $chat_ins_data[] = array(
        "wc_no"         => $chat_schedule['wc_no'],
        "s_no"          => $chat_schedule['s_no'],
        "content"       => $chat_schedule['content'],
        "file_path"     => $chat_schedule['file_path'],
        "file_name"     => $chat_schedule['file_name'],
        "alert_type"    => $chat_schedule['alert_type'],
        "alert_check"   => $chat_schedule['alert_check'],
        "regdate"       => $cur_datetime
    );

    $upd_sql = "UPDATE waple_chat_content_schedule SET status='2' WHERE wcs_no='{$chat_schedule['wcs_no']}'";
    mysqli_query($my_db, $upd_sql);
}

if(!empty($chat_ins_data)){
    $chat_model->multiInsert($chat_ins_data);
}

$message_model      = Message::Factory();
$event_sms_model    = Advertising::Factory();
$event_sms_model->setMainInit("advertising_event_sms", "aes_no");
$event_option       = getEventOption();
$send_data_list     = [];
$sms_change_list    = [];
$sms_cm_date        = date("YmdHis");
$cm_idx             = 1;
$event_date         = "{$cur_date} 11:00:00";
$event_datetime     = date("YmdHis", strtotime($event_date));
$send_name          = "와이즈미디어커머스";
$send_phone         = "02-830-1912";

$chk_event_sql      = "SELECT aes.*, ae.event_no, ae.event_e_date FROM advertising_event_sms AS aes LEFT JOIN advertising_event ae ON ae.ae_no=aes.ae_no WHERE aes.sms_state='1' AND aes.sms_date='{$event_date}'";
$chk_event_query    = mysqli_query($my_db, $chk_event_sql);
while($chk_event = mysqli_fetch_assoc($chk_event_query))
{
    $sms_title      = "이벤트 종료 예정 알림";
    $event_name     = $event_option[$chk_event["event_no"]];
    $event_day      = date("Y-m-d", strtotime($chk_event["event_e_date"]));
    $event_hour     = date("H", strtotime($chk_event["event_e_date"]));
    $event_time     = date("i", strtotime($chk_event["event_e_date"]));
    $event_end_time = $event_day." ".$event_hour.":".$event_time;
    $content        = "[이벤트 종료 예정 알림]\r\n`{$event_name}`\r\n{$event_end_time}에 종료 됩니다.\r\n\r\n와플의 커머스 이벤트 관리페이지에서\r\n이벤트 연장 또는 추가 설정 바랍니다.";
    $sms_msg        = addslashes($content);
    $sms_msg_len    = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
    $msg_type       = ($sms_msg_len > 90) ? "L" : "S";
    $msg_id         = "W{$sms_cm_date}".sprintf('%04d', $cm_idx++);

    $send_data_list[$msg_id] = array(
        "msg_id"        => $msg_id,
        "msg_type"      => $msg_type,
        "sender"        => $send_name,
        "sender_hp"     => $send_phone,
        "receiver_hp"   => $chk_event['hp'],
        "reserved_time" => $event_datetime,
        "title"         => $sms_title,
        "content"       => $sms_msg,
        "cinfo"         => 77
    );

    $sms_change_list[] = array("aes_no" => $chk_event['aes_no'], "sms_state" => 2);
}

if(!empty($send_data_list)) {
    $message_model->sendMessage($send_data_list);
}

if(!empty($sms_change_list)) {
    $event_sms_model->multiUpdate($sms_change_list);
}