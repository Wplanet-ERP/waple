<?php
ini_set('max_execution_time', 500);

require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/helper/work_cms.php");

# 이전달꺼 데이터 적용
$sch_move_date      = date('Y-m', strtotime("-1 months"));

# 쿼리용 날짜
$sch_move_s_date    = $sch_move_date."-01";
$end_day            = date('t', strtotime($sch_move_date));
$sch_move_e_date    = $sch_move_date."-".$end_day;

# 단품처리
$change_single_unit_list    = getChangeSingleUnitList();
$change_sub_unit_list       = getChangeSubUnitList();
$chk_single_unit_list       = array_keys($change_sub_unit_list);
$chk_sub_unit_list          = array_keys($change_single_unit_list);

# 회수리스트 처리
$return_list      = [];
$return_prd_sql   = "SELECT `option`, SUM(quantity) as qty FROM work_cms_return_unit WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_return wcr WHERE wcr.return_state='6' AND DATE_FORMAT(wcr.return_date,'%Y-%m')='{$sch_move_date}') AND `type` IN(1,2,3,4) GROUP BY `option`";
$return_prd_query = mysqli_query($my_db, $return_prd_sql);
while($return_prd = mysqli_fetch_assoc($return_prd_query))
{
    if(!isset($return_list[$return_prd['option']])){
        $return_list[$return_prd['option']] = 0;
    }

    $return_list[$return_prd['option']] += $return_prd['qty'];
}

# 입고/반출 리스트
$stock_list         = [];
$stock_report_sql   = "
    SELECT 
       * 
    FROM product_cms_stock_report pcsr 
    WHERE (pcsr.regdate BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}') AND pcsr.confirm_state > 0
      AND pcsr.state NOT IN('기간반품입고','기간판매반출','기간이동입고','기간이동반출')  AND move_no='0'
";
$stock_report_query = mysqli_query($my_db, $stock_report_sql);
while($stock_report = mysqli_fetch_assoc($stock_report_query))
{
    $stock_kind = "";
    switch($stock_report['confirm_state']){
        case "1":
        case "2":
            $stock_kind = "in_point";
            break;
        case "3":
            $stock_kind = "in_move";
            break;
        case "4":
            $stock_kind = "in_etc";
            break;
        case "5":
            $stock_kind = "in_return";
            break;
        case "6":
            $stock_kind = "out_move";
            break;
        case "8":
            $stock_kind = "out_etc";
            break;
        case "9":
            $stock_kind = "out_point";
            break;
        case "10":
            $stock_kind = "out_return";
            break;

    }

    if(!empty($stock_kind))
    {
        if(!isset($stock_list[$stock_report['prd_unit']][$stock_report['stock_type']])){
            $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']] = array(
                "in_point"      => 0,
                "in_move"       => 0,
                "in_etc"        => 0,
                "in_return"     => 0,
                "out_point"     => 0,
                "out_move"      => 0,
                "out_return"    => 0,
                "out_etc"       => 0,
            );
        }

        $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']][$stock_kind] += $stock_report['stock_qty'];
    }
}

# 재고자산수불부 쿼리
$product_receipt_sql = "
    SELECT
        *
    FROM product_cms_stock_confirm as rs 
    WHERE `base_type`='start' AND base_mon='{$sch_move_date}'
    AND log_c_no = '2809'
    ORDER BY rs.option_name ASC, rs.warehouse ASC
";
$product_receipt_query  = mysqli_query($my_db, $product_receipt_sql);
$stock_price_data       = [];
while($product_receipt = mysqli_fetch_assoc($product_receipt_query))
{
    if(!isset($stock_price_data[$product_receipt['prd_unit']])){
        $stock_price_data[$product_receipt['prd_unit']] = array(
            "total_price"   => 0,
            "total_qty"     => 0
        );
    }

    $stock_list_data                        = isset($stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];

    $product_receipt['in_base_qty']         = $product_receipt['qty'];
    $product_receipt['in_point_qty']        = !empty($stock_list_data) ? $stock_list_data['in_point'] : 0;
    $product_receipt['in_etc_qty']          = !empty($stock_list_data) ? $stock_list_data['in_etc'] : 0;
    $product_receipt['in_return_sub_qty']   = ($product_receipt['warehouse'] == "2-3 검수대기창고(반품건)" && isset($return_list[$product_receipt['prd_unit']])) ? $return_list[$product_receipt['prd_unit']] : 0;
    $product_receipt['in_return_org_qty']   = !empty($stock_list_data) ? $stock_list_data['in_return'] : 0;
    $product_receipt['in_return_qty']       = $product_receipt['in_return_org_qty'] + $product_receipt['in_return_sub_qty'];

    $product_receipt['in_base_price']       = $product_receipt['base_price'] * $product_receipt['in_base_qty'];
    $product_receipt['in_point_price']      = $product_receipt['org_price'] * $product_receipt['in_point_qty'];
    $product_receipt['in_return_price']     = $product_receipt['org_price'] * $product_receipt['in_return_qty'];
    $product_receipt['in_etc_price']        = $product_receipt['org_price'] * $product_receipt['in_etc_qty'];
    $product_receipt['in_qty']              = $product_receipt['in_base_qty'] + $product_receipt['in_point_qty'] + $product_receipt['in_return_qty'] + $product_receipt['in_etc_qty'];
    $product_receipt['in_price']            = $product_receipt['in_base_price'] + $product_receipt['in_point_price'] + $product_receipt['in_return_price'] + $product_receipt['in_etc_price'];

    $prd_unit = $product_receipt['prd_unit'];
    $chk_unit = $prd_unit;
    if(in_array($prd_unit, $chk_sub_unit_list)){
        $chk_unit = $change_single_unit_list[$prd_unit]['option'];
    }

    $stock_price_data[$chk_unit]["total_qty"]   += $product_receipt['in_qty'];
    $stock_price_data[$chk_unit]["total_price"] += $product_receipt['in_price'];
}


foreach($stock_price_data as $prd_unit => $total_data)
{
    $avg_price  = ($total_data['total_qty'] > 0) ? round($total_data['total_price']/$total_data['total_qty'], 1) : 0;
    $upd_sql    = "UPDATE product_cms_stock_confirm SET avg_price='{$avg_price}' WHERE base_mon='{$sch_move_date}' ANd prd_unit='{$prd_unit}'";

    if($avg_price > 0)
    {
        if(mysqli_query($my_db, $upd_sql))
        {
            if(in_array($prd_unit, $chk_single_unit_list))
            {
                $sub_prd        = $change_sub_unit_list[$prd_unit]['option'];
                $sub_upd_sql    = "UPDATE product_cms_stock_confirm SET avg_price='{$avg_price}' WHERE base_mon='{$sch_move_date}' ANd prd_unit='{$sub_prd}'";

                mysqli_query($my_db, $sub_upd_sql);
            }
        }
    }
}