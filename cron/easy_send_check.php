<?php
require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/model/Message.php");

mysqli_query($my_db,'set names utf8');

if ( mysqli_connect_errno() ) {
    echo mysqli_connect_error();
    exit;
}

date_default_timezone_set("Asia/Seoul");

# 날짜 계산
$message_model  = Message::Factory();
$cur_datetime   = date("Y-m-d H:i:s", strtotime("-5 mins"));
$check_sql      = "SELECT * FROM EASY_SEND_LOG WHERE ((STATUS='1' AND REPORT_TIME IS NULL) OR (STATUS='3' AND CALL_STATUS='M001') OR (STATUS='4' AND (CALL_STATUS='K000' OR CALL_STATUS='R000'))) AND SEND_TIME <= '{$cur_datetime}'";
$check_query    = mysqli_query($my_db, $check_sql);
while($chk_result = mysqli_fetch_assoc($check_query)){
    $message_model->checkMessage(array(array("msg_id" => $chk_result['MSG_ID'])));
}

$cert_message_model = Message::Factory();
$cert_message_model->setMainInit("EASY_SEND_FILTER_LOG", "MSG_ID");
$check_cert_sql     = "SELECT * FROM EASY_SEND_FILTER_LOG WHERE ((STATUS='1' AND REPORT_TIME IS NULL) OR (STATUS='3' AND CALL_STATUS='M001') OR (STATUS='4' AND (CALL_STATUS='K000' OR CALL_STATUS='R000'))) AND SEND_TIME <= '{$cur_datetime}'";
$check_cert_query   = mysqli_query($my_db, $check_cert_sql);
while($check_cert_result = mysqli_fetch_assoc($check_cert_query)){
    $cert_message_model->checkMessage(array(array("msg_id" => $check_cert_result['MSG_ID'])));
}