<?php
ini_set('max_execution_time', '3000');
ini_set('memory_limit', '2G');

require_once("/home/master/wplanet/libs/dbconfig.php");
require_once("/home/master/wplanet/v1/inc/helper/product_cms_stock.php");
require_once("/home/master/wplanet/v1/inc/model/ProductCmsStock.php");

$stock_mon          = date("Y-m", strtotime("-1 months"));
$stock_mon_day      = date("t", strtotime($stock_mon));
$stock_s_date       = "{$stock_mon}-01";
$stock_e_date       = "{$stock_mon}-{$stock_mon_day}";
$cur_date           = date('Y-m-d');
$result             = true;
$chk_group_option   = getStockStateGroupOption();
$chk_type_option    = getStockInsertTypeOption();
$chk_subject_option = getStockInsertSubjectOption();
$chk_company_option = getStockMatchCompanyOption();
$ins_stock_model    = ProductCmsStock::Factory();
$upd_stock_model    = ProductCmsStock::Factory();
$ins_stock_model->setStockReport();
$upd_stock_model->setStockReport();

# 택배리스트 처리
$work_cms_sql                   = "SELECT * FROM work_cms WHERE (stock_date BETWEEN '{$stock_s_date}' AND '{$stock_e_date}') AND delivery_state='4' AND log_c_no IN(1113,2809)";
$work_cms_query                 = mysqli_query($my_db, $work_cms_sql);
$product_cms_unit_list          = [];
$product_cms_unit_title_list    = [];
$warehouse_option               = $ins_stock_model->getWarehouseBaseList();
$tmp_log_c_no                   = '2809';
while ($work_cms = mysqli_fetch_assoc($work_cms_query))
{
    $order_number   = $work_cms['order_number'];
    $dp_c_no        = $work_cms['dp_c_no'];
    $dp_c_name      = $work_cms['dp_c_name'];
    $prd_no         = $work_cms['prd_no'];
    $quantity       = $work_cms['quantity'];
    $log_c_no       = $work_cms['log_c_no'];
    $stock_date     = $work_cms['stock_date'];
    $chk_group      = ($log_c_no == $tmp_log_c_no && isset($chk_type_option[$dp_c_no])) ? $chk_type_option[$dp_c_no] : 1;
    $chk_subject    = isset($chk_subject_option[$dp_c_no]) ? $chk_subject_option[$dp_c_no] : 0;

    $product_cms_unit_sql = "
        SELECT 
            pcu.no, 
            (SELECT c.c_name FROM company `c` WHERE `c`.c_no=pcu.brand) AS brand,
            pcu.sup_c_no, 
            pcu.option_name, 
            pcum.sku, 
            pcum.warehouse, 
            pcr.quantity, 
            (SELECT p.title FROM product_cms p WHERE p.prd_no='{$prd_no}') as prd_name, 
            pcu.type as prd_kind  
        FROM product_cms_unit pcu     
        LEFT JOIN product_cms_relation pcr ON  pcr.option_no = pcu.no
        LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=pcu.no AND pcum.log_c_no='{$tmp_log_c_no}'
        WHERE pcr.prd_no ='{$prd_no}' AND pcr.display = 1
    ";
    $product_cms_unit_query = mysqli_query($my_db, $product_cms_unit_sql);
    while ($product_cms_unit = mysqli_fetch_assoc($product_cms_unit_query))
    {
        # 기본값 처리
        if(!isset($product_cms_unit_title_list[$product_cms_unit['no']][$log_c_no]))
        {
            $chk_unit_sku = $product_cms_unit['sku'];
            if(empty($product_cms_unit['sku']) || $log_c_no != '2809'){
                $other_unit_chk_sql     = "SELECT * FROM product_cms_unit_management WHERE log_c_no='{$log_c_no}' AND prd_unit='{$product_cms_unit['no']}' LIMIT 1";
                $other_unit_chk_query   = mysqli_query($my_db, $other_unit_chk_sql);
                $other_unit_chk_result  = mysqli_fetch_assoc($other_unit_chk_query);

                if(isset($other_unit_chk_result['sku'])){
                    $chk_unit_sku = $other_unit_chk_result['sku'];
                }
            }

            $product_cms_unit_title_list[$product_cms_unit['no']][$log_c_no] = array(
                'brand'         => $product_cms_unit['brand'],
                'sup_c_no'      => $product_cms_unit['sup_c_no'],
                'prd_kind'      => ($product_cms_unit['prd_kind'] == '1') ? "상품" : "부속품",
                'option_name'   => $product_cms_unit['option_name'],
                'sku'           => $chk_unit_sku,
                'prd_name'      => $product_cms_unit['prd_name']
            );
        }

        $unit_warehouse = "";
        if($log_c_no == $tmp_log_c_no){
            $unit_warehouse = $product_cms_unit['warehouse'];
        }elseif($log_c_no == '1113'){
            $chk_group      = "1";
            $unit_warehouse = "사내창고";
        } else{
            foreach($warehouse_option[$log_c_no] as $warehouse){
                $unit_warehouse = $warehouse;
                break;
            }
        }

        if(!isset($product_cms_unit_list[$stock_date][$log_c_no][$chk_group][$chk_subject][$unit_warehouse][$product_cms_unit['no']])){
            $product_cms_unit_list[$stock_date][$log_c_no][$chk_group][$chk_subject][$unit_warehouse][$product_cms_unit['no']] = 0;
        }
        $product_cms_unit_list[$stock_date][$log_c_no][$chk_group][$chk_subject][$unit_warehouse][$product_cms_unit['no']] += ($product_cms_unit['quantity'] * $quantity);

        # 이동반출 처리
        if($chk_group == "3")
        {
            $move_chk_group = 5;
            $move_log_c_no  = $dp_c_no;
            $move_warehouse = "";

            if($move_log_c_no == $tmp_log_c_no){
                $move_warehouse = $product_cms_unit['warehouse'];
            }else{
                foreach($warehouse_option[$move_log_c_no] as $warehouse){
                    $move_warehouse = $warehouse;
                    break;
                }
            }

            if(!isset($product_cms_unit_title_list[$product_cms_unit['no']][$move_log_c_no]))
            {
                $chk_unit_sku = $product_cms_unit['sku'];
                if(empty($product_cms_unit['sku']) || $move_log_c_no != '2809'){
                    $other_unit_chk_sql     = "SELECT * FROM product_cms_unit_management WHERE log_c_no='{$move_log_c_no}' AND prd_unit='{$product_cms_unit['no']}' LIMIT 1";
                    $other_unit_chk_query   = mysqli_query($my_db, $other_unit_chk_sql);
                    $other_unit_chk_result  = mysqli_fetch_assoc($other_unit_chk_query);

                    if(isset($other_unit_chk_result['sku'])){
                        $chk_unit_sku = $other_unit_chk_result['sku'];
                    }
                }

                $product_cms_unit_title_list[$product_cms_unit['no']][$move_log_c_no] = array(
                    'sup_c_no'      => $product_cms_unit['sup_c_no'],
                    'prd_kind'      => ($product_cms_unit['prd_kind'] == '1') ? "상품" : "부속품",
                    'option_name'   => $product_cms_unit['option_name'],
                    'sku'           => $chk_unit_sku,
                    'prd_name'      => $product_cms_unit['prd_name']
                );
            }

            if(!isset($product_cms_unit_list[$stock_date][$move_log_c_no][$move_chk_group][$chk_subject][$move_warehouse][$product_cms_unit['no']])){
                $product_cms_unit_list[$stock_date][$move_log_c_no][$move_chk_group][$chk_subject][$move_warehouse][$product_cms_unit['no']] = 0;
            }
            $product_cms_unit_list[$stock_date][$move_log_c_no][$move_chk_group][$chk_subject][$move_warehouse][$product_cms_unit['no']] += ($product_cms_unit['quantity'] * $quantity);
        }
    }
}

$ins_stock_data = [];
$upd_stock_data = [];
$total_cnt      = 0;
$reg_s_no       = "167";
$report_date    = date("Y-m-d H:i:s");
foreach($product_cms_unit_list as $stock_date => $prd_unit_data)
{
    foreach ($prd_unit_data as $chk_log_c_no => $type_data)
    {
        foreach ($type_data as $chk_group => $group_data)
        {
            $chk_state_name         = $chk_group_option[$chk_group]['state'];
            $chk_confirm_name       = $chk_group_option[$chk_group]['confirm'];
            foreach ($group_data as $chk_subject => $subject_data)
            {
                foreach ($subject_data as $unit_warehouse => $unit_data)
                {
                    foreach ($unit_data as $unit_no => $stock)
                    {
                        $title_list         = $product_cms_unit_title_list[$unit_no][$chk_log_c_no];
                        $sup_c_no           = isset($title_list['sup_c_no']) ? $title_list['sup_c_no'] : 0;
                        $option_name        = isset($title_list['option_name']) ? $title_list['option_name'] : "";
                        $sku                = isset($title_list['sku']) ? $title_list['sku'] : "";
                        $prd_kind           = isset($title_list['prd_kind']) ? $title_list['prd_kind'] : "";
                        $prd_name           = isset($title_list['prd_name']) ? $title_list['prd_name'] : "";
                        $brand              = isset($title_list['brand']) ? $title_list['brand'] : "";
                        $stock_qty          = ($chk_group == '5') ? $stock : -$stock;

                        $chk_stock_sql      = "SELECT `no` FROM product_cms_stock_report WHERE `report_type`='2' AND `type`='3' AND log_c_no='{$chk_log_c_no}' AND prd_unit='{$unit_no}' AND regdate='{$stock_date}' AND stock_date='{$stock_date}' AND `state`='{$chk_state_name}' AND confirm_state='{$chk_confirm_name}' AND stock_type='{$unit_warehouse}' AND log_subject='{$chk_subject}' AND memo LIKE '%택배 리스트'";
                        $chk_stock_query    = mysqli_query($my_db, $chk_stock_sql);
                        $chk_stock_result   = mysqli_fetch_assoc($chk_stock_query);

                        if(isset($chk_stock_result['no']))
                        {
                            $upd_stock_data[] = array(
                                'no'        => $chk_stock_result['no'],
                                "stock_qty" => $stock_qty,
                            );

                        }else{
                            $ins_stock_data[] = array(
                                "regdate"           => $stock_date,
                                "stock_date"        => $stock_date,
                                "report_type"       => 2,
                                "type"              => 3,
                                "client"            => "와이즈플래닛컴퍼니",
                                "doc_no"            => 1,
                                "order"             => 1,
                                "state"             => $chk_state_name,
                                "confirm_state"     => $chk_confirm_name,
                                "log_subject"       => $chk_subject,
                                "brand"             => $brand,
                                "prd_kind"          => $prd_kind,
                                "prd_unit"          => $unit_no,
                                "sup_c_no"          => $sup_c_no,
                                "log_c_no"          => $chk_log_c_no,
                                "option_name"       => $option_name,
                                "sku"               => $sku,
                                "prd_name"          => $prd_name,
                                "stock_type"        => $unit_warehouse,
                                "stock_qty"         => $stock_qty,
                                "memo"              => "{$stock_date} 택배 리스트",
                                "reg_s_no"          => $reg_s_no,
                                "report_date"       => $report_date
                            );
                        }
                        $total_cnt++;
                    }
                }
            }
        }
    }
}

# 퀵/화물 리스트
$work_cms_quick_sql             = "SELECT *, (SELECT COUNT(*) FROM work_cms_quick_delivery AS wd WHERE wd.order_number=w.order_number) AS deli_cnt, (SELECT lm.subject FROM logistics_management lm WHERE lm.doc_no=w.doc_no) as doc_subject FROM work_cms_quick AS w WHERE (run_date BETWEEN '{$stock_s_date}' AND '{$stock_e_date}') AND quick_state='4' AND delivery_method='1' ORDER BY run_date";
$work_cms_quick_query           = mysqli_query($my_db, $work_cms_quick_sql);
$quick_product_cms_unit_list    = [];
$quick_doc_run_date_list        = [];
while($work_cms_quick = mysqli_fetch_assoc($work_cms_quick_query))
{
    if($work_cms_quick['deli_cnt'] > 0)
    {
        $order_number   = $work_cms_quick['order_number'];
        $doc_subject    = $work_cms_quick['doc_subject'];
        $doc_no         = $work_cms_quick['doc_no'];
        $doc_run_c_no   = !empty($work_cms_quick['doc_subject']) && isset($chk_company_option[$work_cms_quick['doc_subject']]) ? $chk_company_option[$work_cms_quick['doc_subject']] : "";
        $run_c_no       = !empty($work_cms_quick['run_c_no']) ? $work_cms_quick['run_c_no'] : $doc_run_c_no;
        $log_c_no       = $tmp_log_c_no;
        $prd_no         = $work_cms_quick['prd_no'];
        $quantity       = $work_cms_quick['quantity'];
        $chk_group      = isset($chk_type_option[$run_c_no]) ? $chk_type_option[$run_c_no] : 1;
        $chk_subject    = isset($chk_subject_option[$run_c_no]) ? $chk_subject_option[$run_c_no] : 0;

        $quick_doc_run_date_list[$work_cms_quick['doc_no']] = $work_cms_quick['run_date'];
        if($work_cms_quick['prd_type'] == '1')
        {
            $quick_product_cms_unit_sql = "
                SELECT 
                    pcu.no,
                    (SELECT c.c_name FROM company `c` WHERE `c`.c_no=pcu.brand) AS brand,
                    pcu.sup_c_no,
                    pcu.option_name, 
                    pcum.sku, 
                    pcum.warehouse, 
                    pcr.quantity, 
                    (SELECT p.title FROM product_cms p WHERE p.prd_no='{$prd_no}') as prd_name, 
                    pcu.type as prd_kind
                FROM product_cms_unit pcu     
                LEFT JOIN product_cms_relation pcr ON  pcr.option_no = pcu.no
                LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=pcu.no AND pcum.log_c_no='{$tmp_log_c_no}'
                WHERE pcr.prd_no ='{$prd_no}' AND pcr.display = 1
            ";
        }
        else
        {
            $quick_product_cms_unit_sql = "
                SELECT 
                    pcu.no, 
                    (SELECT c.c_name FROM company `c` WHERE `c`.c_no=pcu.brand) AS brand,
                    pcu.sup_c_no, 
                    pcu.option_name, 
                    pcum.sku, 
                    pcum.warehouse,
                    '1' as quantity,
                    pcu.type as prd_kind  
                FROM product_cms_unit pcu
                LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=pcu.no AND pcum.log_c_no='{$tmp_log_c_no}'
                WHERE pcu.no ='{$prd_no}'
            ";
        }
        $quick_product_cms_unit_query = mysqli_query($my_db, $quick_product_cms_unit_sql);
        while ($quick_product_cms_unit = mysqli_fetch_assoc($quick_product_cms_unit_query))
        {
            if(!isset($product_cms_unit_title_list[$quick_product_cms_unit['no']][$log_c_no]))
            {
                $chk_unit_sku = $quick_product_cms_unit['sku'];
                if(empty($quick_product_cms_unit['sku']) || $log_c_no != '2809'){
                    $other_unit_chk_sql     = "SELECT * FROM product_cms_unit_management WHERE log_c_no='{$log_c_no}' AND prd_unit='{$quick_product_cms_unit['no']}' LIMIT 1";
                    $other_unit_chk_query   = mysqli_query($my_db, $other_unit_chk_sql);
                    $other_unit_chk_result  = mysqli_fetch_assoc($other_unit_chk_query);

                    if(isset($other_unit_chk_result['sku'])){
                        $chk_unit_sku = $other_unit_chk_result['sku'];
                    }
                }

                $product_cms_unit_title_list[$quick_product_cms_unit['no']][$log_c_no] = array(
                    'brand'         => $quick_product_cms_unit['brand'],
                    'sup_c_no'      => $quick_product_cms_unit['sup_c_no'],
                    'prd_kind'      => ($quick_product_cms_unit['prd_kind'] == '1') ? "상품" : "부속품",
                    'option_name'   => $quick_product_cms_unit['option_name'],
                    'sku'           => $chk_unit_sku,
                    'prd_name'      => ($work_cms_quick['prd_type'] == '1') ? $quick_product_cms_unit['prd_name'] : $quick_product_cms_unit['option_name']
                );
            }

            $unit_warehouse = "";
            if($log_c_no == $tmp_log_c_no){
                $unit_warehouse = $quick_product_cms_unit['warehouse'];
            }elseif($log_c_no == '1113'){
                $chk_group      = "1";
                $unit_warehouse = "사내창고";
            } else{
                foreach($warehouse_option[$log_c_no] as $warehouse){
                    $unit_warehouse = $warehouse;
                    break;
                }
            }

            if(!isset($quick_product_cms_unit_list[$doc_no][$log_c_no][$chk_group][$chk_subject][$unit_warehouse][$quick_product_cms_unit['no']])){
                $quick_product_cms_unit_list[$doc_no][$log_c_no][$chk_group][$chk_subject][$unit_warehouse][$quick_product_cms_unit['no']] = 0;
            }
            $quick_product_cms_unit_list[$doc_no][$log_c_no][$chk_group][$chk_subject][$unit_warehouse][$quick_product_cms_unit['no']] += ($quick_product_cms_unit['quantity'] * $quantity);

            # 이동반출 처리
            if($chk_group == "3")
            {
                $move_chk_group = 5;
                $move_log_c_no  = $run_c_no;
                $move_warehouse = "";

                if($move_log_c_no == $tmp_log_c_no){
                    $move_warehouse = $quick_product_cms_unit['warehouse'];
                }else{
                    foreach($warehouse_option[$move_log_c_no] as $warehouse){
                        $move_warehouse = $warehouse;
                        break;
                    }
                }

                if(!isset($product_cms_unit_title_list[$quick_product_cms_unit['no']][$move_log_c_no]))
                {
                    $chk_unit_sku = $quick_product_cms_unit['sku'];
                    if(empty($quick_product_cms_unit['sku']) || $move_log_c_no != '2809'){
                        $other_unit_chk_sql     = "SELECT * FROM product_cms_unit_management WHERE log_c_no='{$move_log_c_no}' AND prd_unit='{$quick_product_cms_unit['no']}' LIMIT 1";
                        $other_unit_chk_query   = mysqli_query($my_db, $other_unit_chk_sql);
                        $other_unit_chk_result  = mysqli_fetch_assoc($other_unit_chk_query);

                        if(isset($other_unit_chk_result['sku'])){
                            $chk_unit_sku = $other_unit_chk_result['sku'];
                        }
                    }

                    $product_cms_unit_title_list[$quick_product_cms_unit['no']][$move_log_c_no] = array(
                        'sup_c_no'      => $quick_product_cms_unit['sup_c_no'],
                        'prd_kind'      => ($quick_product_cms_unit['prd_kind'] == '1') ? "상품" : "부속품",
                        'option_name'   => $quick_product_cms_unit['option_name'],
                        'sku'           => $chk_unit_sku,
                        'prd_name'      => $quick_product_cms_unit['prd_name']
                    );
                }

                if(!isset($quick_product_cms_unit_list[$doc_no][$move_log_c_no][$move_chk_group][$chk_subject][$move_warehouse][$quick_product_cms_unit['no']])){
                    $quick_product_cms_unit_list[$doc_no][$move_log_c_no][$move_chk_group][$chk_subject][$move_warehouse][$quick_product_cms_unit['no']] = 0;
                }
                $quick_product_cms_unit_list[$doc_no][$move_log_c_no][$move_chk_group][$chk_subject][$move_warehouse][$quick_product_cms_unit['no']] += ($quick_product_cms_unit['quantity'] * $quantity);
            }
        }
    }
}

if(!empty($quick_product_cms_unit_list))
{
    foreach($quick_product_cms_unit_list as $doc_no => $quick_prd_unit_data)
    {
        foreach ($quick_prd_unit_data as $chk_log_c_no => $quick_type_data)
        {
            foreach ($quick_type_data as $chk_group => $group_data)
            {
                $chk_state_name         = $chk_group_option[$chk_group]['state'];
                $chk_confirm_name       = $chk_group_option[$chk_group]['confirm'];
                foreach ($group_data as $chk_subject => $subject_data)
                {
                    foreach ($subject_data as $unit_warehouse => $unit_data)
                    {
                        foreach ($unit_data as $unit_no => $stock)
                        {
                            $title_list         = $product_cms_unit_title_list[$unit_no][$chk_log_c_no];
                            $sup_c_no           = isset($title_list['sup_c_no']) ? $title_list['sup_c_no'] : 0;
                            $option_name        = isset($title_list['option_name']) ? $title_list['option_name'] : "";
                            $sku                = isset($title_list['sku']) ? $title_list['sku'] : "";
                            $prd_kind           = isset($title_list['prd_kind']) ? $title_list['prd_kind'] : "";
                            $prd_name           = isset($title_list['prd_name']) ? $title_list['prd_name'] : "";
                            $brand              = isset($title_list['brand']) ? $title_list['brand'] : "";
                            $run_date           = $quick_doc_run_date_list[$doc_no];
                            $stock_qty          = ($chk_group == '5') ? $stock : -$stock;

                            $chk_stock_sql      = "SELECT `no` FROM product_cms_stock_report WHERE `report_type`='2' AND `type`='3' AND log_c_no='{$chk_log_c_no}' AND prd_unit='{$unit_no}' AND `state`='{$chk_state_name}' AND confirm_state='{$chk_confirm_name}' AND stock_type='{$unit_warehouse}' AND log_subject='{$chk_subject}' AND log_doc_no='{$doc_no}' AND memo LIKE '%출고요청 리스트'";
                            $chk_stock_query    = mysqli_query($my_db, $chk_stock_sql);
                            $chk_stock_result   = mysqli_fetch_assoc($chk_stock_query);

                            if(isset($chk_stock_result['no']))
                            {
                                $upd_stock_data[] = array(
                                    'no'        => $chk_stock_result['no'],
                                    "stock_qty" => $stock_qty,
                                );

                            }else{
                                $ins_stock_data[] = array(
                                    "regdate"           => $run_date,
                                    "stock_date"        => $run_date,
                                    "report_type"       => 2,
                                    "type"              => 3,
                                    "client"            => "와이즈플래닛컴퍼니",
                                    "doc_no"            => 1,
                                    "order"             => 1,
                                    "state"             => $chk_state_name,
                                    "confirm_state"     => $chk_confirm_name,
                                    "log_doc_no"        => $doc_no,
                                    "log_subject"       => $chk_subject,
                                    "brand"             => $brand,
                                    "prd_kind"          => $prd_kind,
                                    "prd_unit"          => $unit_no,
                                    "sup_c_no"          => $sup_c_no,
                                    "log_c_no"          => $chk_log_c_no,
                                    "option_name"       => $option_name,
                                    "sku"               => $sku,
                                    "prd_name"          => $prd_name,
                                    "stock_type"        => $unit_warehouse,
                                    "stock_qty"         => $stock_qty,
                                    "memo"              => addslashes("{$run_date} 입/출고요청 리스트"),
                                    "reg_s_no"          => $reg_s_no,
                                    "report_date"       => $report_date
                                );
                            }
                            $total_cnt++;
                        }
                    }
                }
            }
        }
    }
}

$result = false;
if(!empty($upd_stock_data))
{
    if($upd_stock_model->multiUpdate($upd_stock_data)){
        $result = true;
    }
}

if(!empty($ins_stock_data))
{
    if($ins_stock_model->multiInsert($ins_stock_data)){
        $result = true;
    }
}

$chat_ins_sql = "";
if($total_cnt > 0)
{
    if($result){
        $chat_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$reg_s_no}', content='{$stock_mon}월 택배리스트 입고/반출데이터 적용완료', alert_type='14', alert_check='STOCK_CRON_SUCCESS', regdate=now()";
    }else {
        $chat_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$reg_s_no}', content='{$stock_mon}월 택배리스트 입고/반출데이터 적용실패', alert_type='14', alert_check='STOCK_CRON_FAIL', regdate=now()";
    }
}else{
    $chat_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$reg_s_no}', content='{$stock_mon}월 택배리스트 입고/반출데이터 적용실패', alert_type='14', alert_check='STOCK_CRON_FAIL', regdate=now()";
}

if(!empty($chat_ins_sql)){
    mysqli_query($my_db, $chat_ins_sql);
}

?>
