import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from bs4 import BeautifulSoup

chrome_options=Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')

driver = webdriver.Chrome('/usr/local/bin/chromedriver', options=chrome_options)

# 날짜계산
time2 = datetime.now()
cur_date = time2.strftime('%Y-%m-%d %H:00:00')

# 구글시트
scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
json_file_name = '/home/master/wplanet/cron/scenic-dynamo-281305-f448443dbbd5.json'
credentials = ServiceAccountCredentials.from_json_keyfile_name(json_file_name, scope)
gc = gspread.authorize(credentials)
spreadsheet_url = 'https://docs.google.com/spreadsheets/d/10srPXb-pCnKQ89XdsMYUBygERaujdlbZtKCFYLAHtI0/edit#gid=2002371597'
doc = gc.open_by_url(spreadsheet_url)

# 네이버 메인
try:
    total_main_img_list = []
    for nav in range(8):
        driver.implicitly_wait(3)
        driver.get('https://www.naver.com/')
        driver.implicitly_wait(3)

        iframe_content = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID, "ad_timeboard_tgtLREC")))
        driver.switch_to.frame(iframe_content)

        main_html = driver.page_source
        main_soup = BeautifulSoup(main_html, 'html.parser')
        main_AdWrap = main_soup.find('div', {"class": "defaultBanner"})
        main_check = 1

        if not main_AdWrap:
            main_AdWrap = main_soup.find('a', {"id": "ac_banner_a"})
            main_check = 0

        main_img_data = main_AdWrap.find("img")

        if main_img_data:
            main_image = main_img_data.get('src')

            if main_check:
                main_titleWrap = main_soup.find('a', {"class": "sideA"})
                main_titleSpan = main_titleWrap.find('span', {"class": "blind"})
                main_title = main_titleSpan.text
                total_main_img_list.append([main_image, main_title])
            else:
                main_title = main_img_data.get('alt')
                total_main_img_list.append([main_image, main_title])

        driver.switch_to.default_content()

    dup_remove_main_img_list = list(set([tuple(l) for l in total_main_img_list]))

    # 스프레스시트 문서 가져오기
    doc = gc.open_by_url(spreadsheet_url)

    main_img_list = []
    if dup_remove_main_img_list:
        for x, y in dup_remove_main_img_list:
            main_img_list.insert(0, [cur_date, y, f'=IMAGE("{x}")'])

    if main_img_list:
        worksheet = doc.worksheet('네이버 메인광고')
        worksheet.format('A3:C3', {"borders": {"top": {"style": "SOLID", "width": "2"}}})
        worksheet.insert_rows(main_img_list, 3, value_input_option='USER_ENTERED')

except Exception as e:
    print('error')
    print(str(e))
finally:
    print('Naver Finish')

# 네이버 M광고
try:
    total_img_list = []
    for nav in range(10):
        # 크롤링
        driver.implicitly_wait(3)
        driver.get('https://m.naver.com/')
        driver.implicitly_wait(3)

        main_html = driver.page_source
        main_soup = BeautifulSoup(main_html, 'html.parser')
        ad_div_data = main_soup.find("div",{"class": "ad_main_search_home"})

        if ad_div_data:
            a_link_data = ad_div_data.find("a")
            img_data = ad_div_data.find("img")

            link = a_link_data.get('href')
            image = img_data.get('src')
            title = img_data.get('alt')

            total_img_list.append([image,title])

    # 시트 선택하기
    worksheet = doc.worksheet('네이버M메인광고')

    rows_data_list = []
    if total_img_list:
        dup_remove_img_list = list(set([tuple(l) for l in total_img_list]))

        if dup_remove_img_list:
            idx = 1
            for x, y in dup_remove_img_list:
                if idx == 1:
                    worksheet.format('A3:C3', {"borders": {"top": {"style": "SOLID", "width": "2"}}})

                rows_data_list.insert(0, [cur_date, y, f'=IMAGE("{x}")'])
                idx += 1


    if rows_data_list:
        worksheet.insert_rows(rows_data_list, 3, value_input_option='USER_ENTERED')

except Exception as e:
    print('error')
    print(str(e))
finally:
    print('Naver M finish')


# 다음광고
try:
    total_main_img_list = []
    total_sub_img_list = []
    for nav in range(5):
        driver.implicitly_wait(3)
        driver.get('https://www.daum.net/')

        #메인광고
        main_iframe = driver.find_element_by_id('adMain').find_element_by_tag_name('iframe')
        driver.switch_to.frame(main_iframe)

        main_html = driver.page_source
        main_soup = BeautifulSoup(main_html, 'html.parser')
        main_adWrap = main_soup.find('div', {"class": "cm_ad"})
        if main_adWrap:
            main_a_link_data = main_adWrap.find("a")
            main_img_data = main_adWrap.find("img")

            main_link = main_a_link_data.get('href')
            main_image = main_img_data.get('src')
            main_title = main_img_data.get('alt')

            total_main_img_list.append([main_image, main_title])

        driver.switch_to.default_content()

        # 서브 광고
        driver.implicitly_wait(3)
        sub_iframe = driver.find_element_by_class_name('kakao_ad_area').find_element_by_tag_name('iframe')
        driver.switch_to.frame(sub_iframe)

        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        sub_adWrap = soup.find('div', {"class": "cm_ad"})
        sub_adWrap2 = soup.find('div', {"class": "link_ad"})
        sub_adWrap3 = soup.find('div', {"id": "kakaoAdWrap"})

        if sub_adWrap:
            sub_a_link_data = sub_adWrap.find("a")
            sub_img_data = sub_adWrap.find("img")

            sub_link = sub_a_link_data.get('href')
            sub_image = sub_img_data.get('src')
            sub_title = sub_img_data.get('alt')

            total_sub_img_list.append([sub_image, sub_title])

        elif sub_adWrap2:
            sub_a_link_data = sub_adWrap2.find("a")
            sub_img_data = sub_adWrap2.find("img")

            sub_link = sub_a_link_data.get('href')
            sub_image = sub_img_data.get('src')
            sub_title = sub_img_data.get('alt')

            total_sub_img_list.append([sub_image, sub_title])

        elif sub_adWrap3:
            sub_a_link_data = sub_adWrap3.find("a")
            sub_img_data = sub_adWrap3.find("img")

            sub_link = sub_a_link_data.get('href')
            sub_image = sub_img_data.get('src')
            sub_title = sub_img_data.get('alt')

            total_sub_img_list.append([sub_image, sub_title])

        driver.switch_to.default_content()


    dup_remove_main_img_list = list(set([tuple(l) for l in total_main_img_list]))
    dup_remove_sub_img_list = list(set([tuple(l) for l in total_sub_img_list]))

    main_img_list = []
    sub_img_list = []
    if dup_remove_main_img_list:
        for x, y in dup_remove_main_img_list:
            main_img_list.insert(0,[cur_date, y, f'=IMAGE("{x}")'])

    if dup_remove_sub_img_list:
        for x, y in dup_remove_sub_img_list:
            sub_img_list.insert(0,[cur_date, y, f'=IMAGE("{x}")'])

    if main_img_list:
        # 시트 선택하기
        worksheet = doc.worksheet('다음 PC 메인')
        worksheet.format('A3:C3', {"borders": {"top": {"style": "SOLID", "width": "2"}}})
        worksheet.insert_rows(main_img_list, 3, value_input_option='USER_ENTERED')

    if sub_img_list:
        # 시트 선택하기
        worksheet = doc.worksheet('다음 1200*600')
        worksheet.format('A3:C3', {"borders": {"top": {"style": "SOLID", "width": "2"}}})
        worksheet.insert_rows(sub_img_list, 3, value_input_option='USER_ENTERED')
except:
    print('fail')
finally:
    print('Daum Finish')
    driver.quit()