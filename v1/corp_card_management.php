<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/corporation.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');
require('inc/model/MyCompany.php');
require('inc/model/Corporation.php');

// 접근 권한
if (!permissionNameCheck($session_permission, "재무관리자") && !permissionNameCheck($session_permission, "대표")){
    $smarty->display('access_error.html');
    exit;
}

# 프로세스 처리
$proc = (isset($_POST['process'])) ? $_POST['process'] : "";

if($proc == 'display')
{
    $cc_no      = isset($_POST['cc_no']) ?$_POST['cc_no'] : "";
    $type       = isset($_POST['type']) ?$_POST['type'] : "";
    $search_url = isset($_POST['search_url']) ?$_POST['search_url'] : "";
    $card_model = Corporation::Factory();
    $card_model->setCorpCard();

    if(empty($cc_no) || empty($type)) {
        exit("<script>alert('다시 시도해 주세요');location.href='corp_card_management.php?{$search_url}';</script>");
    }

    $upd_data = array("cc_no" => $cc_no, "display" => $type);

    if(!$card_model->update($upd_data)) {
        exit("<script>alert('변경에 실패했습니다');location.href='corp_card_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('변경했습니다');location.href='corp_card_management.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "61";
$nav_title   = "법인카드 관리";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색용 리스트
$team_model             = Team::Factory();
$sch_team_name_list     = $team_model->getTeamFullNameList();

# 검색 초기화 및 조건 생성
$add_where          = "1=1";
$cur_date           = date('Y-m');
$exp_date           = date('Y-m',strtotime("+3 month"));
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_card_kind      = isset($_GET['sch_card_kind']) ? $_GET['sch_card_kind'] : "";
$sch_card_type      = isset($_GET['sch_card_type']) ? $_GET['sch_card_type'] : "";
$sch_card_type_exc  = isset($_GET['sch_card_type_exc']) ? $_GET['sch_card_type_exc'] : "1";
$sch_card_num       = isset($_GET['sch_card_num']) ? $_GET['sch_card_num'] : "";
$sch_share_card     = isset($_GET['sch_share_card']) ? $_GET['sch_share_card'] : "";
$sch_team           = isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_expired        = isset($_GET['sch_expired']) ? $_GET['sch_expired'] : "";
$sch_display        = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";

if(!empty($sch_my_c_no))
{
    $add_where .= " AND `cc`.my_c_no = '{$sch_my_c_no}'";
    $smarty->assign('sch_my_c_no', $sch_my_c_no);
}

if(!empty($sch_card_kind))
{
    $add_where .= " AND `cc`.card_kind LIKE '%{$sch_card_kind}%'";
    $smarty->assign('sch_card_kind', $sch_card_kind);
}

if(!empty($sch_card_type))
{
    $add_where .= " AND `cc`.card_type = '{$sch_card_type}'";
    $smarty->assign('sch_card_type', $sch_card_type);
}

if(!empty($sch_card_type_exc))
{
    $add_where .= " AND `cc`.card_type != '3'";
    $smarty->assign("sch_card_type_exc", $sch_card_type_exc);
}

if(!empty($sch_card_num))
{
    $add_where .= " AND `cc`.card_num LIKE '%{$sch_card_num}%'";
    $smarty->assign('sch_card_num', $sch_card_num);
}

if(!empty($sch_share_card))
{
    $add_where .= " AND `cc`.share_card LIKE '%{$sch_share_card}%'";
    $smarty->assign('sch_share_card', $sch_share_card);
}

if(!empty($sch_team)) {
    if ($sch_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        $add_where          .= " AND `cc`.team IN ({$sch_team_code_where})";
    }
    $smarty->assign("sch_team", $sch_team);
}

if(!empty($sch_manager))
{
    $add_where .= " AND `cc`.manager IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_manager}%')";
    $smarty->assign('sch_manager', $sch_manager);
}

if(!empty($sch_expired))
{
    if($sch_expired == '1'){
        $add_where .= " AND DATE_FORMAT(`cc`.expired_date, '%Y-%m') BETWEEN '{$cur_date}' AND '{$exp_date}'";
    }elseif($sch_expired == '2'){
        $add_where .= " AND DATE_FORMAT(`cc`.expired_date, '%Y-%m') < '{$cur_date}'";
    }
    $smarty->assign('sch_expired', $sch_expired);
}

if(!empty($sch_display))
{
    $add_where .= " AND `cc`.display='{$sch_display}'";
    $smarty->assign('sch_display', $sch_display);
}

# 전체 게시물 수
$corp_card_total_sql	= "SELECT count(cc_no) as cnt FROM corp_card `cc` WHERE {$add_where}";
$corp_card_total_query	= mysqli_query($my_db, $corp_card_total_sql);
$corp_card_total_result = mysqli_fetch_array($corp_card_total_query);
$corp_card_total        = $corp_card_total_result['cnt'];

# 페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($corp_card_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist	= pagelist($pages, "corp_card_management.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $corp_card_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

# 가이드 리스트 쿼리
$corp_card_sql  = "
    SELECT
        `cc`.cc_no,
        `cc`.`my_c_no`,
        `cc`.card_kind,
        `cc`.card_num,
        `cc`.card_type,
        `cc`.share_card,
        (SELECT t.team_name FROM team t WHERE t.team_code=(SELECT sub_t.team_code_parent FROM team sub_t WHERE sub_t.team_code=`cc`.team LIMIT 1)) AS parent_group_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=`cc`.team) AS group_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`cc`.manager) AS manager_name,
        DATE_FORMAT(`cc`.expired_date, '%m/%y') AS expired_my,
        DATE_FORMAT(`cc`.expired_date, '%Y-%m') AS expired_ym_chk,
        `cc`.display,
        `cc`.memo
    FROM corp_card `cc`
    WHERE {$add_where}
    ORDER BY cc_no DESC
    LIMIT {$offset}, {$num}
";
$corp_card_query    = mysqli_query($my_db, $corp_card_sql);
$corp_card_list     = [];
$card_type_option   = getCardTypeOption();
$my_company_model   = MyCompany::Factory();
$my_company_option  = $my_company_model->getNameList();
while($corp_card = mysqli_fetch_assoc($corp_card_query))
{
    $corp_card['my_c_name']      = isset($my_company_option[$corp_card['my_c_no']]) ? $my_company_option[$corp_card['my_c_no']] : "";
    $corp_card['card_type_name'] = isset($card_type_option[$corp_card['card_type']]) ? $card_type_option[$corp_card['card_type']] : "";

    if($cur_date > $corp_card['expired_ym_chk']){
        $corp_card['expired_comment'] = "만료";
    } elseif($cur_date <= $corp_card['expired_ym_chk'] && $corp_card['expired_ym_chk'] <= $exp_date) {
        $corp_card['expired_comment'] = "만료임박";
    }

    $corp_card_list[] = $corp_card;
}

$smarty->assign('page_type_option', getPageTypeOption('4'));
$smarty->assign('my_company_option', $my_company_option);
$smarty->assign("sch_team_list", $sch_team_name_list);
$smarty->assign('is_expired_option', getIsExpiredOption());
$smarty->assign('card_type_option', $card_type_option);
$smarty->assign('display_option', getDisplayOption());
$smarty->assign('corp_card_list', $corp_card_list);

$smarty->display('corp_card_management.html');

?>
