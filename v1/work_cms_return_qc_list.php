<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_return.php');
require('inc/helper/manufacturer.php');
require('inc/model/MyQuick.php');
require('inc/model/ProductCmsUnit.php');

# Navigation & My Quick
$nav_prd_no  = "162";
$nav_title   = "커머스 Q/C 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색처리
$add_where          = "1=1 AND wcr.return_state='6'";
$add_sup_where      = "";
$add_c_no           = 1113;
$sch_r_no           = isset($_GET['sch_r_no']) ? $_GET['sch_r_no'] : "";
$sch_prev_ord_no    = isset($_GET['sch_prev_ord_no']) ? $_GET['sch_prev_ord_no'] : "";
$sch_stock_date     = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : "";
$sch_recipient      = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp   = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_return_type    = isset($_GET['sch_return_type']) ? $_GET['sch_return_type'] : "4";
$sch_delivery_no    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
$sch_sup_c_no       = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_option         = isset($_GET['sch_option']) ? $_GET['sch_option'] : "";
$sch_mi_state       = isset($_GET['sch_mi_state']) ? $_GET['sch_mi_state'] : "";

if(!empty($sch_r_no)) {
    $add_where .= " AND wcr.r_no='{$sch_r_no}'";
    $smarty->assign("sch_r_no", $sch_r_no);
}

if(!empty($sch_prev_ord_no)) {
    $add_where .= " AND wcr.parent_order_number='{$sch_prev_ord_no}'";
    $smarty->assign("sch_prev_ord_no", $sch_prev_ord_no);
}

if(!empty($sch_stock_date)) {
    $add_where .= " AND (SELECT w.stock_date FROM work_cms w WHERE wcr.parent_order_number=w.order_number AND stock_date != '' LIMIT 1)='{$sch_stock_date}'";
    $smarty->assign("sch_stock_date", $sch_stock_date);
}

if(!empty($sch_recipient)) {
    $add_where .= " AND wcr.recipient='{$sch_recipient}'";
    $smarty->assign("sch_recipient", $sch_recipient);
}

if(!empty($sch_recipient_hp)) {
    $add_where .= " AND wcr.recipient_hp LIKE '%{$sch_recipient_hp}%'";
    $smarty->assign("sch_recipient_hp", $sch_recipient_hp);
}

if(!empty($sch_return_type)) {
    $add_where .= " AND wcr.return_type='{$sch_return_type}'";
    $smarty->assign("sch_return_type", $sch_return_type);
}

if(!empty($sch_delivery_no)) {
    $add_where .= " AND (wcr.return_delivery_no='{$sch_delivery_no}' OR wcr.return_delivery_no2='{$sch_delivery_no}')";
    $smarty->assign("sch_delivery_no", $sch_delivery_no);
}

if(!empty($sch_sup_c_no)) {
    $add_where      .= " AND wcr.order_number IN(SELECT DISTINCT wcru.order_number FROM work_cms_return_unit wcru WHERE wcru.`option` IN(SELECT `no` FROM product_cms_unit pcu WHERE pcu.sup_c_no='{$sch_sup_c_no}') AND wcru.quantity > 0)";
    $add_sup_where  .= " AND pcu.sup_c_no='{$sch_sup_c_no}'";
    $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
}

if(!empty($sch_option)) {
    $add_where .= " AND wcr.order_number IN(SELECT DISTINCT wcru.order_number FROM work_cms_return_unit wcru WHERE sku LIKE '%{$sch_option}%' AND wcru.quantity > 0)";
    $smarty->assign("sch_option", $sch_option);
}

if(!empty($sch_mi_state)) {
    if($sch_mi_state == "99"){
        $add_where .= " AND wcr.order_number NOT IN(SELECT DISTINCT mi.order_number FROM manufacturer_inspection mi WHERE mi.work_state = '6')";
    }else{
        $add_where .= " AND wcr.order_number IN(SELECT DISTINCT mi.order_number FROM manufacturer_inspection mi WHERE mi.work_state='{$sch_mi_state}')";
    }
    $smarty->assign("sch_mi_state", $sch_mi_state);
}


# 페이징처리
$return_qc_total_sql    = "SELECT COUNT(DISTINCT order_number) as cnt FROM work_cms_return wcr WHERE {$add_where}";
$return_qc_total_query  = mysqli_query($my_db, $return_qc_total_sql);
$return_qc_total_result = mysqli_fetch_assoc($return_qc_total_query);
$return_qc_total        = isset($return_qc_total_result['cnt']) ? $return_qc_total_result['cnt'] : 0;

$page       = isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 20;
$num 		= $page_type;
$offset 	= ($page-1) * $num;
$pagenum 	= ceil($return_qc_total/$num);

if ($page >= $pagenum){$page = $pagenum;}
if ($page <= 0){$page = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($page, "work_cms_return_qc_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $return_qc_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$return_qc_sql = "
    SELECT
        wcr.r_no,
        wcr.order_number,
        wcr.parent_order_number,
        (SELECT w.stock_date FROM work_cms w WHERE wcr.parent_order_number=w.order_number LIMIT 1) as stock_date,
        wcr.recipient,   
        wcr.recipient_hp,
        IF(wcr.zip_code, CONCAT('[',wcr.zip_code,']'), '') as postcode,
        wcr.recipient_addr,
        wcr.return_type,
        wcr.req_memo,
        wcr.return_delivery_type,
        wcr.return_delivery_no,
        wcr.return_delivery_no2,
        (SELECT mi.mi_no FROM manufacturer_inspection mi WHERE mi.order_number=wcr.order_number) as mi_no,
        (SELECT mi.work_state FROM manufacturer_inspection mi WHERE mi.order_number=wcr.order_number) as mi_state
    FROM work_cms_return wcr
    WHERE {$add_where}
    GROUP BY order_number
    ORDER BY r_no DESC 
    LIMIT {$offset}, {$num}
";
$return_qc_query    = mysqli_query($my_db, $return_qc_sql);
$return_qc_list     = [];
$return_type_option = getReturnTypeOption();
$mi_state_option    = getWorkStateOption();
while($return_qc = mysqli_fetch_assoc($return_qc_query))
{
    if (isset($return_qc['recipient_hp']) && !empty($return_qc['recipient_hp'])) {
        $f_hp = substr($return_qc['recipient_hp'], 0, 4);
        $e_hp = substr($return_qc['recipient_hp'], 7, 15);
        $return_qc['recipient_sc_hp'] = $f_hp . "***" . $e_hp;
    }

    $return_qc['return_type_name']  = isset($return_type_option[$return_qc['return_type']]) ? $return_type_option[$return_qc['return_type']] : "";
    $return_qc['mi_state_name']     = isset($mi_state_option[$return_qc['mi_state']]) ? $mi_state_option[$return_qc['mi_state']] : "";

    $return_unit_sql    = "
        SELECT 
            wcr.type, 
            wcr.option, 
            wcr.sku, 
            (SELECT c.c_name FROM company c WHERE c.c_no=pcu.sup_c_no) as sup_c_name,
            wcr.quantity 
        FROM work_cms_return_unit wcr 
        LEFT JOIN product_cms_unit pcu ON pcu.`no`=wcr.option
        WHERE wcr.order_number = '{$return_qc['order_number']}' AND wcr.quantity > 0 {$add_sup_where} 
    ";
    $return_unit_query  = mysqli_query($my_db, $return_unit_sql);
    $return_unit_list   = [];
    while($return_unit  = mysqli_fetch_assoc($return_unit_query))
    {
        $return_unit_list[] = array("option_info" => "[{$return_unit['sup_c_name']}] {$return_unit['sku']}::{$return_unit['quantity']}");
    }

    $return_qc['return_unit_list'] = $return_unit_list;

    $return_qc_list[] = $return_qc;
}

# 공급업체
$unit_model			= ProductCmsUnit::Factory();
$sch_sup_c_list 	= $unit_model->getDistinctUnitCompanyData('sup_c_no');
$sch_mi_state_option= array("6" => "진행완료", "99" => "미완료");

$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("sup_c_list", $sch_sup_c_list);
$smarty->assign("sch_mi_state_option", $sch_mi_state_option);
$smarty->assign("return_qc_list", $return_qc_list);

$smarty->display('work_cms_return_qc_list.html');
?>