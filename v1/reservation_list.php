<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');

$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

$proc=(isset($_POST['process']))?$_POST['process']:"";

if ($proc == "save_send_yn") {
	$r_no = (isset($_POST['r_no'])) ? $_POST['r_no']:"";
	$send_yn = (isset($_POST['send_yn'])) ? $_POST['send_yn']:"";

	$sql = "
		update reservation set
					 send_yn = '{$send_yn}'
		 where r_no= '{$r_no}'";

	if ($r_no != '')
		mysqli_query($my_db, $sql);

	//exit("<script>/*alert('발송정보를 저장 하였습니다');*/history.back();</script>");
	exit;
}

# Navigation & My Quick
$nav_prd_no  = "120";
$nav_title   = "캠페인 알림 요청 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query", $save_query);

// 선정상태 텍스트
$a_state_text=array(1=>'선정',2=>'탈락',3=>'선정취소');
foreach($a_state_text as $key => $value) {
	$state_stmt[]=array
		(
			"no"=>$key,
			"name"=>$value
		);
	$smarty->assign("select",$state_stmt);
}

// 프로모션 구분 텍스트
$kind_name=array('','체험단','기자단','배송체험');

// 지역 분류 가져오기
$sql="select k_name,k_name_code from kind where k_code='location' and k_parent is null";
$sql=mysqli_query($my_db,$sql);
while($result=mysqli_fetch_array($sql)) {
	$location[]=array(
		"location_name"=>trim($result['k_name']),
		"location_code"=>trim($result['k_name_code'])
	);
	$smarty->assign("location",$location);
}

// 단순히 상태값을 텍스트 표현하기 위해 배열에 담은 정보
$state_name=array(1=>'접수대기', 2=>'접수완료',3=>'모집중',4=>'진행중',5=>'마감');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";

$search_kind = isset($_GET['skind']) ? $_GET['skind']:"";
$f_location1_get = isset($_GET['f_location1']) ? $_GET['f_location1']:"";
$f_location2_get = isset($_GET['f_location2']) ? $_GET['f_location2']:"";
$search_stype = isset($_GET['stype']) ? $_GET['stype']:"";
$search_ssend = isset($_GET['ssend']) ? $_GET['ssend']:"";
$search_scompany = isset($_GET['scompany']) ? $_GET['scompany']:"";
$search_scno = isset($_GET['scno']) ? $_GET['scno']:"";

if(!empty($search_kind)) {
	$add_where .= " AND p.kind = '".$search_kind."'";
	$smarty->assign("skind", $search_kind);
}

if(!empty($f_location1_get)) {
	$add_where .= " AND p.c_no in (select c_no from company where location1='".$f_location1_get."')";
	$smarty->assign("f_location1", $f_location1_get);
}

if(!empty($f_location2_get)) {
	$add_where .= " AND p.c_no in (select c_no from company where location2='".$f_location2_get."')";
	$smarty->assign("f_location2", $f_location2_get);
}

if(!empty($search_stype)) {
	if ($search_stype != "all")
		$add_where .= " AND r.{$search_stype} > ''";
	$smarty->assign("stype", $search_stype);
}

if(!empty($search_ssend)) {
	if ($search_ssend != "all")
		$add_where .= " AND r.send_yn = '{$search_ssend}'";
	$smarty->assign("ssend", $search_ssend);
}

if(!empty($search_scompany)) {
	if(empty($search_scno))
		$add_where.=" AND p.company like '%".$search_scompany."%'";
	$smarty->assign("scompany",$search_scompany);
}

if(!empty($search_scno)) {
	$add_where.=" AND p.c_no = '".$search_scno."'";
	$smarty->assign("scno",$search_scno);
}

// 2차 분류에 값이 있는 경우 2차 지역 분류 가져오기(2차)
if($f_location2_get != "" || $f_location1_get != ""){
	$sql="select k_name,k_name_code,k_parent from kind where k_code='location' and k_parent='".$f_location1_get."'";
	$sql=mysqli_query($my_db,$sql);
	while($result=mysqli_fetch_array($sql)) {
		$editlocation[]=array(
			"location_name"=>trim($result['k_name']),
			"location_code"=>trim($result['k_name_code']),
			"location_parent"=>trim($result['k_parent'])
		);
		$smarty->assign("editlocation",$editlocation);
	}
}

// 정렬순서 토글 & 필드 지정
$add_orderby.=" r.r_no DESC";

// 페이에 따른 limit 설정
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num = 20;
$offset = ($pages-1) * $num;

$sql="
	SELECT p.p_state AS p_state, p.kind AS kind, p.promotion_code AS promotion_code, p.c_no AS c_no, p.name AS staff_name, p.pres_reward AS pres_reward,
		(select k_name from kind k where k.k_name_code=(select location1 from company c where c.c_no=p.c_no)) as location1,
		(select k_name from kind k where k.k_name_code=(select location2 from company c where c.c_no=p.c_no)) as location2,
		r.r_no,
		r.naver_id,
		r.hp,
		r.send_yn,
		r.regdate
	FROM promotion p, reservation r
	WHERE
		p.p_no=r.p_no
		{$add_where}
	ORDER BY
		$add_orderby
	";

//echo $sql;


/// 전체 게시물 수
$cnt_sql = "SELECT count(*) FROM (".$sql.") AS cnt";
$cnt_query= mysqli_query($my_db, $cnt_sql);
$cnt_data=mysqli_fetch_array($cnt_query);
$total_num = $cnt_data[0];

$smarty->assign(array(
	"total_num"=>number_format($total_num)
));

$smarty->assign("total_num",$total_num);
$pagenum = ceil($total_num/$num);


// 페이징
if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$url = "skind=".$search_kind."&f_location1=".$f_location1_get."&f_location2=".$f_location2_get."&stype=".$search_stype."&ssend=".$search_ssend."&scompany=".$search_scompany."&scno=".$search_scno;
$page=pagelist($pages, "reservation_list.php", $pagenum, $url);
$smarty->assign("pagelist",$page);//
$noi = $totalnum - ($pages-1)*$num;
$offset = ($pages-1) * $num;
$smarty->assign("totalnum",$totalnum);


// 리스트 쿼리 결과(데이터)
$sql .= "LIMIT $offset,$num";

	$query=mysqli_query($my_db,$sql);
	while($data=mysqli_fetch_array($query)) {

		$regdate = substr($data['regdate'], 0, 16);

		/* c_no로 업체명 가져오기 Start */
		$company_sql="select c_name from company where c_no='".$data['c_no']."'";
		$company_query=mysqli_query($my_db,$company_sql);
		$company_data=mysqli_fetch_array($company_query); //$company_data['c_name']
		/* c_no로 업체명 가져오기 End */

		$arr_hp = explode('-', $data['hp']);

		if (permissionNameCheck($session_permission, "서비스운영") || permissionNameCheck($session_permission, "마스터관리자"))
			$hp = $data['hp'];
		else {
			if ($data['hp'])
				$hp = $arr_hp[0]."-****-".$arr_hp[2];
		}

		$stmt[]=array
			(
				"i"=>$i,
				"r_no"=>$data['r_no'],
				"p_no"=>$data['p_no'],
				"state_name"=>$state_name[$data['p_state']],
				"kind"=>$data['kind'],
				"kind_name"=>$kind_name[$data['kind']],
				"promotion_code"=>$data['promotion_code'],
				"location1"=>$data['location1'],
				"location2"=>$data['location2'],
				"company"=>$company_data['c_name'],
				"naver_id"=>$data['naver_id'],
				"hp"=>$hp,
				"send_yn"=>$data['send_yn'],
				"regdate"=>$regdate
			);
		$smarty->assign("blogger",$stmt);
		$i++;
	}

	$smarty->display('reservation_list.html');
?>
