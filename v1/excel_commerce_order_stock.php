<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_price.php');
require('inc/helper/product_cms_stock.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");


//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "입고반출 No")
	->setCellValue('B1', "입고완료일")
	->setCellValue('C1', "확정 입고형태")
	->setCellValue('D1', "업체명/브랜드명")
	->setCellValue('E1', "공급업체")
	->setCellValue('F1', "구성품목명")
	->setCellValue('G1', "물류업체")
	->setCellValue('H1', "재고관리코드(SKU)")
	->setCellValue('I1', "수량")
	->setCellValue('J1', "총 입고액")
	->setCellValue('K1', "단가(VAT포함)")
	->setCellValue('L1', "공급가")
	->setCellValue('M1', "부가세")
	->setCellValue('N1', "메모")
	->setCellValue('O1', "발주번호")
	->setCellValue('P1', "발주제목")
	->setCellValue('Q1', "원가(VAT별도)")
;

# 검색조건건
$add_where          = "1=1 AND is_order='1'";
$sch_reg_mon        = isset($_GET['sch_reg_mon']) ? $_GET['sch_reg_mon'] : "";
$sch_reg_s_date     = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : date("Y-m")."-01";
$sch_reg_e_date     = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : date("Y-m")."-".date("t");
$sch_stock_no       = isset($_GET['sch_stock_no']) ? $_GET['sch_stock_no'] : "";
$sch_confirm_state  = isset($_GET['sch_confirm_state']) ? $_GET['sch_confirm_state'] : "";
$sch_sup_c_no       = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_log_company    = isset($_GET['sch_log_company']) ? $_GET['sch_log_company'] : "";
$sch_option_name    = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_prd_sku        = isset($_GET['sch_prd_sku']) ? $_GET['sch_prd_sku'] : "";
$yet_commerce       = isset($_GET['yet_commerce']) ? $_GET['yet_commerce'] : "";

# 발주 조건
$sch_multi_state    = isset($_GET['sch_multi_state']) ? $_GET['sch_multi_state'] : "";
$sch_team           = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no           = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_title          = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_order_count    = isset($_GET['sch_order_count']) ? $_GET['sch_order_count'] : "";
$sch_c_name         = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_month          = isset($_GET['sch_month']) ? $_GET['sch_month'] : "";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_reg_date_type  = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "";

$url_check_str  = $_SERVER['QUERY_STRING'];
$url_chk_result = false;
if(empty($url_check_str)){
	$url_check_team_where   = getTeamWhere($my_db, $session_team);
	$url_check_sql    		=  "SELECT count(*) as cnt FROM product_cms_stock_report as pcsr LEFT JOIN product_cms_unit as pcu ON pcu.no = pcsr.prd_unit WHERE is_order='1' AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$url_check_team_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$url_check_team_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$url_check_team_where})))";
	$url_check_query  		= mysqli_query($my_db, $url_check_sql);
	$url_check_result 		= mysqli_fetch_assoc($url_check_query);

	if($url_check_result['cnt'] == 0){
		$sch_team       = "all";
		$url_chk_result = true;
	}
}

if($yet_commerce == '1')
{
	$add_where .= " AND (ord_no = 0 OR ord_no IS NULL)";
	if (!empty($sch_sup_c_no)) {
		$add_where .= " AND pcsr.sup_c_no='{$sch_sup_c_no}'";
	}

	if (!empty($sch_log_company)) {
		$add_where .= " AND pcsr.log_c_no='{$sch_log_company}'";
	}

	if (!empty($sch_team))
	{
		if ($sch_team != "all") {
			$sch_team_code_where = getTeamWhere($my_db, $sch_team);
			$add_where       .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})))";
		}
	}else{
		$sch_team_code_where = getTeamWhere($my_db, $session_team);
		$add_where       .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})))";
	}

	if (!empty($sch_s_no))
	{
		if ($sch_s_no != "all") {
			$add_where .= " AND pcu.ord_s_no ='{$sch_s_no}'";
		}
	}else{
		if($sch_team == $session_team){
			$add_where .= " AND pcu.ord_s_no ='{$session_s_no}'";
		}
	}
}
else
{
	if(!empty($sch_reg_mon)) {
		$sch_reg_s_date = $sch_reg_mon."-01";
		$sch_reg_e_date = $sch_reg_mon."-31";
	}

	if (!empty($sch_reg_s_date)) {
		$reg_s_date = $sch_reg_s_date." 00:00:00";
		$add_where .= " AND pcsr.regdate >='{$reg_s_date}'";
	}

	if (!empty($sch_reg_e_date)) {
		$reg_e_date = $sch_reg_e_date." 23:59:59";
		$add_where .= " AND pcsr.regdate <= '{$reg_e_date}'";
	}

	if (!empty($sch_stock_no)) {
		$add_where .= " AND pcsr.no='{$sch_stock_no}'";
	}

	if(!empty($sch_multi_state)){
		$add_where .= " AND pcsr.confirm_state IN(1,2)";
	}

	if (!empty($sch_confirm_state)) {
		$add_where .= " AND pcsr.confirm_state='{$sch_confirm_state}'";
	}

	if (!empty($sch_sup_c_no)) {
		$add_where .= " AND pcsr.sup_c_no='{$sch_sup_c_no}'";
	}

	if (!empty($sch_log_company)) {
		$add_where .= " AND pcsr.log_c_no='{$sch_log_company}'";
	}

	if (!empty($sch_option_name)) {
		$add_where .= " AND pcu.option_name LIKE '%{$sch_option_name}%'";
	}

	if (!empty($sch_prd_sku)) {
		$add_where .= " AND pcsr.sku = '{$sch_prd_sku}'";
	}

	if (!empty($sch_team))
	{
		if ($sch_team != "all") {
			$sch_team_code_where = getTeamWhere($my_db, $sch_team);
			$add_where       .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})))";
		}
	}else{
		$sch_team_code_where = getTeamWhere($my_db, $session_team);
		$add_where       .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})))";
	}

	if (!empty($sch_s_no))
	{
		if ($sch_s_no != "all") {
			$add_where .= " AND pcu.ord_s_no ='{$sch_s_no}'";
		}
	}else{
		if($sch_team == $session_team){
			$add_where .= " AND pcu.ord_s_no ='{$session_s_no}'";
		}
	}

	if (!empty($sch_my_c_no)) {
		if ($sch_my_c_no != "all") {
			$add_where       .= " AND pcsr.ord_no IN(SELECT co.no FROM commerce_order co WHERE co.set_no IN(SELECT cos.`no` FROM commerce_order_set cos WHERE cos.my_c_no ='{$sch_my_c_no}'))";
		}
	}

	if (!empty($sch_title)) {
		$add_where       .= " AND pcsr.ord_no IN(SELECT co.no FROM commerce_order co WHERE co.set_no IN(SELECT cos.`no` FROM commerce_order_set cos WHERE cos.title LIKE '%{$sch_title}%'))";
	}

	if (!empty($sch_order_count)) {
		$add_where       .= " AND pcsr.ord_no IN(SELECT co.no FROM commerce_order co WHERE co.set_no IN(SELECT cos.`no` FROM commerce_order_set cos WHERE cos.order_count ='{$sch_order_count}'))";
	}

	if (!empty($sch_c_name)) {
		$add_where       .= " AND pcsr.ord_no IN(SELECT co.no FROM commerce_order co WHERE co.set_no IN(SELECT cos.`no` FROM commerce_order_set cos WHERE cos.c_name LIKE '%{$sch_c_name}%'))";
	}

	if (!empty($sch_month)) {
		$add_where       .= " AND pcsr.ord_no IN(SELECT co.no FROM commerce_order co WHERE co.set_no IN(SELECT cos.`no` FROM commerce_order_set cos WHERE DATE_FORMAT(cos.req_date, '%Y-%m') BETWEEN '{$sch_month}' AND '{$sch_month}'))";
	}

	if (!empty($sch_no)) {
		$add_where       .= " AND pcsr.ord_no ='{$sch_no}'";
	}
}

# 발주 처리
$commerce_order_list            = [];
$stock_report_total_sql         = "
    SELECT 
        pcsr.no,
        pcsr.ord_no,
        pcsr.stock_qty,
        pcsr.confirm_state
    FROM product_cms_stock_report pcsr 
    LEFT JOIN product_cms_unit as pcu ON pcu.no = pcsr.prd_unit
    WHERE {$add_where}";
$stock_report_total_query       = mysqli_query($my_db, $stock_report_total_sql);
while($stock_report_total_result  = mysqli_fetch_array($stock_report_total_query))
{
	if($stock_report_total_result['ord_no'] > 0
		&& ($stock_report_total_result['confirm_state'] == 1 || $stock_report_total_result['confirm_state'] == 2 || $stock_report_total_result['confirm_state'] == 4))
	{
		$commerce_order_sql     = "
            SELECT 
                co.no, 
                co.set_no, 
                cos.title, 
                cos.state, 
                cos.sup_c_name, 
                cos.order_count, 
                co.unit_price, 
                co.quantity, 
                co.supply_price, 
                co.total_price,
                co.vat, 
                co.group, 
                cos.sup_loc_type 
            FROM commerce_order co 
            LEFT JOIN commerce_order_set cos ON cos.no=co.set_no 
            WHERE co.`no`='{$stock_report_total_result['ord_no']}' AND co.`type`='supply'
        ";
		$commerce_order_query   = mysqli_query($my_db, $commerce_order_sql);
		$commerce_order_result  = mysqli_fetch_assoc($commerce_order_query);

		if($commerce_order_result['no'])
		{
			$commerce_is_usd            = ($commerce_order_result['sup_loc_type'] == '2') ? true : false;
			$total_price_text           = getNumberFormatPrice($commerce_order_result['total_price']);
			$unit_price_text            = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['unit_price']) : getNumberFormatPrice($commerce_order_result['unit_price']);
			$supply_price_text          = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['supply_price']) : getNumberFormatPrice($commerce_order_result['supply_price']);
			$org_price                  = $commerce_order_result['total_price']/$commerce_order_result['quantity'];

			$commerce_order_list[$commerce_order_result['no']] = array(
				"set_no"            => $commerce_order_result['set_no'],
				"ord_name"          => $commerce_order_result['sup_c_name']." [{$commerce_order_result['order_count']}차]",
				"ord_title"         => $commerce_order_result['title'],
				"vat_price"         => $commerce_order_result['vat'],
				"ord_state"         => $commerce_order_result['state'],
				"total_price"       => $total_price_text,
				"unit_price"        => $unit_price_text,
				"supply_price"      => $supply_price_text,
				"sup_loc_type"      => $commerce_order_result['sup_loc_type'],
				"commerce_is_usd"   => $commerce_is_usd,
				"commerce_type"     => ($commerce_is_usd) ? "$" : "₩",
				"org_price"         => ($stock_report_total_result['confirm_state'] == '1') ? round($org_price) : 0,
			);
		}
	}
}

# 입고/반출 리스트
$report_sql = "
    SELECT
        pcsr.no,
        pcsr.regdate,
        pcsr.prd_unit,
        pcsr.confirm_state,
        (SELECT c.c_name FROM company c WHERE c.c_no=pcu.brand) as c_name,
        pcsr.sup_c_no,
        (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.sup_c_no) as sup_c_name,
		(SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.log_c_no) as log_c_name,          
        pcu.option_name,
        pcsr.sku,
        pcsr.stock_qty,
        pcsr.ord_no,
        pcsr.is_order,
        pcsr.memo
    FROM product_cms_stock_report as pcsr
    LEFT JOIN product_cms_unit as pcu ON pcu.no = pcsr.prd_unit
    WHERE {$add_where}
    ORDER BY pcsr.no DESC
";
$report_query = mysqli_query($my_db, $report_sql);
$idx = 2;
$confirm_state_option = getConfirmStateOption();
while($report = mysqli_fetch_assoc($report_query))
{
	$confirm_state_name = $confirm_state_option[$report['confirm_state']];
	if(isset($commerce_order_list[$report['ord_no']]))
	{
		$report = array_merge($report, $commerce_order_list[$report['ord_no']]);
	}

	$total_price  = ($report['ord_no'] > 0) ? $report['total_price'] : "";
	$unit_price   = ($report['ord_no'] > 0) ? $report['unit_price'] : "";
	$supply_price = ($report['ord_no'] > 0) ? $report['supply_price'] : "";
	$vat_price	  = ($report['ord_no'] > 0) ? number_format($report['vat_price']) : "";
	$org_price	  = ($report['ord_no'] > 0) ? number_format($report['org_price']) : "";
	$ord_no 	  = ($report['ord_no'] > 0) ? $report['ord_no'] : "";
	$ord_title 	  = ($report['ord_no'] > 0) ? $report['ord_name'] : "";

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$idx}", $report['no'])
        ->setCellValue("B{$idx}", $report['regdate'])
        ->setCellValue("C{$idx}", $confirm_state_name)
        ->setCellValue("D{$idx}", $report['c_name'])
        ->setCellValue("E{$idx}", $report['sup_c_name'])
        ->setCellValue("F{$idx}", $report['option_name'])
        ->setCellValue("G{$idx}", $report['log_c_name'])
        ->setCellValue("H{$idx}", $report['sku'])
        ->setCellValue("I{$idx}", number_format($report['stock_qty']))
        ->setCellValue("J{$idx}", $total_price)
        ->setCellValue("K{$idx}", $unit_price)
        ->setCellValue("L{$idx}", $supply_price)
        ->setCellValue("M{$idx}", $vat_price)
        ->setCellValue("N{$idx}", $report['memo'])
        ->setCellValue("O{$idx}", $ord_no)
        ->setCellValue("P{$idx}", $ord_title)
        ->setCellValue("Q{$idx}", $org_price)
    ;

    $idx++;

}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');
$background_color = "00262626";

$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');


$objPHPExcel->getActiveSheet()->getStyle("A1:Q{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:Q{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:Q{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("M2:M{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("Q2:Q{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setWrapText(TRUE);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setWrapText(TRUE);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setWrapText(TRUE);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(12);

$objPHPExcel->getActiveSheet()->setTitle('입고관리 내역');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date('Y-m-d')."_입고관리 내역.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
