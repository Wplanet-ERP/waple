<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/personal_expenses.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');
require('inc/model/Kind.php');

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "재무관리자") && !permissionNameCheck($session_permission, "마스터관리자")){
    $smarty->display('access_error.html');
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "60";
$nav_title   = "개인경비(지출종합표)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색용 리스트
$team_model             = Team::Factory();
$sch_team_name_list     = $team_model->getTeamFullNameList();
$kind_model             = Kind::Factory();
$kind_account_2_list    = $kind_model->getKindChildList("account_code");


# 검색쿼리
$add_where          = " pe.display = '1' AND (pe.state = '4' OR pe.state = '5') AND pe.account_code != '0'";
$sch_c_no 		    = isset($_GET['sch_c_no']) ? $_GET['sch_c_no'] : "";
$sch_team           = isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$sch_share_card     = isset($_GET['sch_share_card']) ? $_GET['sch_share_card'] : "";
$sch_req_s_name     = isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : "";
$sch_month 	        = isset($_GET['sch_month']) ? $_GET['sch_month'] : date("Y-m", strtotime("-1 month")); // 월간
$sch_account_code   = isset($_GET['sch_account_code']) ? $_GET['sch_account_code'] : "";

if (!empty($sch_c_no)) {
	if($sch_c_no == 'default'){
		$add_where .= " AND pe.my_c_no = 1";
	}elseif($sch_c_no == 'commerce'){
		$add_where .= " AND pe.my_c_no = 3";
	}
	$smarty->assign("sch_c_no", $sch_c_no);
}

if(!empty($sch_team)) {
    if ($sch_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        $add_where          .= " AND pe.team IN ({$sch_team_code_where})";
    }
    $smarty->assign("sch_team", $sch_team);
}

if(!empty($sch_share_card)) {
    $add_where .= " AND ``cc``.share_card LIKE '%{$sch_share_card}%'";
    $smarty->assign('sch_share_card', $sch_share_card);
}

if(!empty($sch_req_s_name)) {
    $add_where .= " AND `pe`.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_req_s_name}%')";
    $smarty->assign('sch_req_s_name', $sch_req_s_name);
}

if (!empty($sch_month)) {
	$add_where .= " AND DATE_FORMAT(pe.payment_date, '%Y-%m') = '{$sch_month}'";
	$smarty->assign("sch_month", $sch_month);
}

if (!empty($sch_account_code)) {
	$add_where .= " AND pe.account_code='{$sch_account_code}'";
	$smarty->assign("sch_account_code", $sch_account_code);
}

#검색 URL
$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

# 개인경비 종합 리스트 쿼리
$personal_expenses_sql = "
	SELECT
		pe.my_c_no,
		(SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=pe.my_c_no) as my_c_name,
		pe.team as share_group,
        (SELECT t.team_name FROM team t WHERE t.team_code=pe.team) as share_group_name,
		(SELECT t.priority FROM team t WHERE t.team_code=pe.team) AS team_priority,
		pe.s_no,
		(SELECT s_name FROM staff s where s.s_no=pe.s_no) AS s_name,
		(SELECT k_name_code FROM kind WHERE k_name_code = (SELECT k_parent FROM kind WHERE k_code = 'account_code' AND k_name_code=pe.account_code)) AS parent_account_code,
		(SELECT k_name FROM kind WHERE k_name_code = (SELECT k_parent FROM kind WHERE k_code = 'account_code' AND k_name_code=pe.account_code)) AS parent_account_code_name,
		pe.money,
		`cc`.card_type,		
		`cc`.share_card
	FROM personal_expenses pe
	LEFT JOIN corp_card `cc` ON `cc`.cc_no = pe.corp_card_no
	WHERE {$add_where}
	ORDER BY my_c_no ASC, team_priority ASC, parent_account_code ASC, s_name ASC
";
$personal_expenses_query = mysqli_query($my_db, $personal_expenses_sql);

# 공용
$corp_card_list     = [];
$corp_team_count    = "";
$corp_comp_count    = "";

#개인
$per_card_list      = [];

# 종합
$parent_account_total_list  = [];
$parent_account_total_sum   = 0;
$total_num                  = 0;
$total_pe_money             = 0;
while($pe_result = mysqli_fetch_assoc($personal_expenses_query))
{
	#개인, 공용 구분
	$card_type_chk = isset($pe_result['card_type']) && ($pe_result['card_type'] == '2') ? '2' : '1';

	if($card_type_chk == '1'){
	    $share_group        = $pe_result['share_group'];
        $share_group_name   = $pe_result['share_group_name'];
        if(empty($pe_result['share_group'])){
            $chk_share_sql      = "SELECT `cc`.team, (SELECT t.team_name FROM team t WHERE t.team_code=`cc`.team) as team_name FROM corp_card `cc` WHERE card_type='3' AND manager='{$pe_result['s_no']}' AND display='1' LIMIT 1";
            $chk_share_query    = mysqli_query($my_db, $chk_share_sql);
            $chk_share_result   = mysqli_fetch_assoc($chk_share_query);

            $share_group        = isset($chk_share_result['team']) ? $chk_share_result['team'] : $pe_result['share_group'];
            $share_group_name   = isset($chk_share_result['team_name']) ? $chk_share_result['team_name'] : $pe_result['share_group_name'];
        }

		if(!isset($per_card_list[$pe_result['my_c_no']][$share_group][$pe_result['s_no']][$pe_result['parent_account_code']])){
            $per_card_list[$pe_result['my_c_no']][$share_group][$pe_result['s_no']][$pe_result['parent_account_code']] = array(
				'my_c_name' => $pe_result['my_c_name'],
				'team_name' => $share_group_name,
				's_name' 	=> $pe_result['share_card'],
				'code_name' => $pe_result['parent_account_code_name'],
				'money' 	=> 0,
			);
		}

        $per_card_list[$pe_result['my_c_no']][$share_group][$pe_result['s_no']][$pe_result['parent_account_code']]['money'] += $pe_result['money'];
	}
	elseif($card_type_chk == '2')
    {
	    $share_group        = "public";
	    $share_group_name   = "공용";
        if(!isset($corp_card_list[$pe_result['my_c_no']][$share_group][$pe_result['share_card']][$pe_result['parent_account_code']])){
            $corp_card_list[$pe_result['my_c_no']][$share_group][$pe_result['share_card']][$pe_result['parent_account_code']] = array(
                'my_c_name' 	=> $pe_result['my_c_name'],
                'share_group' 	=> $share_group_name,
                'share_card' 	=> $pe_result['share_card'],
                'code_name' 	=> $pe_result['parent_account_code_name'],
                'money' 		=> 0,
            );
        }

        $corp_card_list[$pe_result['my_c_no']][$share_group][$pe_result['share_card']][$pe_result['parent_account_code']]['money'] += $pe_result['money'];
	}

	# 계정 과목별 총계
    $parent_account_total_list[$pe_result['parent_account_code']]['label'] = $pe_result['parent_account_code_name'];
    $parent_account_total_list[$pe_result['parent_account_code']]['money'] += $pe_result['money'];

    $total_pe_money += $pe_result['money'];
}

#개인 카운트 및 합계
$per_team_data_list  = [];
$per_staff_data_list = [];
if($per_card_list)
{
    foreach ($per_card_list as $my_c_no => $per_card_comp_data){
        foreach ($per_card_comp_data as $team => $per_card_team_data){
            foreach ($per_card_team_data as $s_no => $per_card_staff_data){
                foreach ($per_card_staff_data as $per_card_acc_data){
                    $per_team_data_list[$my_c_no][$team]['total'] += $per_card_acc_data['money'];
                    $per_team_data_list[$my_c_no][$team]['count']++;

                    $per_staff_data_list[$my_c_no][$team][$s_no] += $per_card_acc_data['money'];
                    $total_num++;
				}
            }
        }
	}
}

#공용 카운트 및 합계
$corp_share_group_data_list = [];
$corp_share_card_data_list  = [];
if($corp_card_list)
{
    foreach ($corp_card_list as $my_c_no => $corp_card_comp_data){
        foreach ($corp_card_comp_data as $share_group => $corp_share_group_data){
            foreach ($corp_share_group_data as $share_card => $corp_share_card_data){
                foreach ($corp_share_card_data as $corp_card_acc_data){
                    $corp_share_group_data_list[$my_c_no][$share_group]['total'] += $corp_card_acc_data['money'];
                    $corp_share_group_data_list[$my_c_no][$share_group]['count']++;

                    $corp_share_card_data_list[$my_c_no][$share_group][$share_card] += $corp_card_acc_data['money'];
                    $total_num++;
                }
            }
        }
    }
}

$smarty->assign("sch_my_c_list", getPeMyCompanyOption());
$smarty->assign("sch_team_list", $sch_team_name_list);
$smarty->assign("kind_account_2_list", $kind_account_2_list);
$smarty->assign("total_num", $total_num);
$smarty->assign("total_pe_money", $total_pe_money);
$smarty->assign("per_card_list", $per_card_list);
$smarty->assign("per_team_data_list", $per_team_data_list);
$smarty->assign("per_staff_data_list", $per_staff_data_list);
$smarty->assign("corp_card_list", $corp_card_list);
$smarty->assign("corp_share_group_data_list", $corp_share_group_data_list);
$smarty->assign("corp_share_card_data_list", $corp_share_card_data_list);
$smarty->assign("parent_account_total_list", $parent_account_total_list);

$smarty->display('personal_expenses_result_list.html');

?>
