<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/deposit.php');
require ('api/SlackHook.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Corporation.php');
require('inc/model/MyCompany.php');
require('inc/model/Product.php');
require('inc/model/Team.php');
require('inc/model/Work.php');
require('inc/model/Deposit.php');

# 접근 권한
if (!(permissionNameCheck($session_permission,"마케터") || permissionNameCheck($session_permission,"재무관리자") || permissionNameCheck($session_permission,"대표") || permissionNameCheck($session_permission,"마스터관리자") || permissionNameCheck($session_permission, "외주관리자") || $session_team == '00244')){
	$smarty->display('access_error.html');
	exit;
}

# Model 설정
$deposit_model = Deposit::Factory();

# 프로세스 처리
$proc = (isset($_POST['process'])) ? $_POST['process'] : "";

if($proc == "del_manager_record")
{
	$dp_no 			 = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$search_url_get	 = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$upd_data 		 = array("dp_no" => $dp_no, "display" => '2');
	$deposit_item	 = $deposit_model->getItem($dp_no);
	$deposit_w_no	 = $deposit_item['w_no'];

	if (!$deposit_model->update($upd_data)){
		exit("<script>alert('dp_no = {$dp_no} 입금리스트 레코드를 삭제가 실패하였습니다.');location.href='deposit_list.php?{$search_url_get}';</script>");
	} else {

		if($deposit_w_no > 0)
		{
			$work_model	= Work::Factory();
			$work_item	= $work_model->getItem($deposit_w_no);
			$work_prd_no= $work_item['prd_no'];

			$work_data  = array("w_no" => $deposit_w_no, "dp_no" => 'NULL');
			if($work_prd_no == '260'){
				$work_data['work_state']  	 = "8";
				$work_data['linked_table'] 	 = "NULL";
				$work_data['linked_shop_no'] = "NULL";
			}
			$work_item	= $work_model->update($work_data);
		}

		$confirm_sql = "UPDATE `deposit_confirm` dc SET dc.dp_no = NULL, dc.work_state='1' WHERE dc.dp_no = '{$dp_no}';";
		mysqli_query($my_db, $confirm_sql);

		exit("<script>alert('dp_no = {$dp_no} 입금리스트 레코드를 삭제하였습니다.');location.href='deposit_list.php?{$search_url_get}';</script>");
	}
}
elseif ($proc == "f_dp_state")
{
	$dp_no 	  		= (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$value 	  		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data 	 	= array("dp_no" => $dp_no, "dp_state" => $value);
	$deposit_item 	= $deposit_model->getItem($dp_no);

	if($value == '2')
	{
		if(empty($deposit_item['run_s_no'])){
			$upd_data['run_s_no'] = $session_s_no;
			$upd_data['run_team'] = $session_team;
		}

		if(empty($deposit_item['dp_date'])){
			$upd_data['dp_date'] 		 = date("Y-m-d");
			$upd_data['incentive_month'] = date("Y-m");
		}

		if(empty($deposit_item['dp_money_vat'])){
			$upd_data["dp_money"] 		= $deposit_item['supply_cost'];
			$upd_data["dp_money_vat"] 	= $deposit_item['cost'];
		}
	}

    if (!$deposit_model->update($upd_data)) {
        echo "입금상태 저장에 실패 하였습니다.";
    }else{
        if($value == '2')
        {
            $checkDeposit = $deposit_model->checkDepositSlack($dp_no);
            if($checkDeposit['result'])
            {
                $slack_title = "[입금완료] {$checkDeposit['dp_subject']}";
                $dp_url      = "https://work.wplanet.co.kr/v1/deposit_list.php?sch_dp_no={$dp_no}";
                $channel     = "U018T52JM8D";
                $message     = "{$slack_title} 입금완료 처리되었습니다.\r\n{$dp_url}";
                $slack_type  = 'deposit';
                sendSlackMessage($channel, $message, $slack_type);
            }
        }
        echo "입금상태가 저장 되었습니다.";
    }
	exit;
}
elseif($proc == "confirm_deposit")
{
	$dp_no 			 = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$dpc_no 		 = (isset($_POST['dpc_no'])) ? $_POST['dpc_no'] : "";
	$search_url_get	 = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$cur_date	 	 = date("Y-m-d");
	$upd_data 		 = array("dp_no" => $dp_no, "dp_state" => "2", "dp_date" => $cur_date, "confirm_no" => $dpc_no, "run_s_no" => $session_s_no, "run_team" => $session_team, "confirm_date" => $cur_date);
	$back_data 		 = array("dp_no" => $dp_no, "dp_state" => "1", "dp_date" => "NULL", "confirm_no" => "NULL", "run_s_no" => "NULL", "run_team" => "NULL", "confirm_date" => "NULL");

	# 완료금액 비어 있을 경우
	$deposit_result 	= $deposit_model->getItem($dp_no);
	if($deposit_result['dp_money_vat'] == 0)
	{
		$upd_data['dp_money'] 	  = $deposit_result['supply_cost'];
		$upd_data['dp_money_vat'] = $deposit_result['cost'];
		$back_data['dp_money'] 	  = 0;
		$back_data['dp_money_vat']= 0;
	}

	if (!$deposit_model->update($upd_data)){
		exit("<script>alert('입금확인 완료처리에 실패했습니다.');location.href='deposit_list.php?{$search_url_get}';</script>");
	} else
	{
		$upd_confirm_data = array("dpc_no" => $dpc_no, "work_state" => "6", "dp_no" => $dp_no, "run_date" => $cur_date, "run_s_no" => $session_s_no, "run_team" => $session_team);

		if(!empty($deposit_result['w_no']))
		{
			$work_model  = Work::Factory();
			$work_item   = $work_model->getItem($deposit_result['w_no']);
			if(isset($work_item['w_no']) && !empty($work_item['w_no']))
			{
				$upd_confirm_data['c_no']   = $work_item['c_no'];
				$upd_confirm_data['c_name'] = $work_item['c_name'];
			}
		}

		if($deposit_model->updateConfirm($upd_confirm_data)){
			exit("<script>alert('입금확인 완료처리에 성공했습니다.');location.href='deposit_list.php?{$search_url_get}';</script>");
		}else{
			$deposit_model->update($back_data);
			exit("<script>alert('입금확인 완료처리에 실패했습니다.');location.href='deposit_list.php?{$search_url_get}';</script>");
		}
	}
}
elseif ($proc == "f_dp_date")
{
	$dp_no 	  = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$value 	  = (isset($_POST['val'])) ? $_POST['val'] : "";
	$dp_month = date("Y-m", strtotime($value));

	$deposit_result = $deposit_model->getItem($dp_no);
	$upd_data		= array("dp_no"	=> $dp_no, "dp_date" => $value, "incentive_month" => $dp_month);

	if (!$deposit_model->update($upd_data))
		echo "입금완료일 저장에 실패 하였습니다.";
	else{
		echo "입금완료일이 저장 되었습니다.";
		echo "<script>location.reload();</script>";
	}
	exit;
}
elseif ($proc == "f_dp_money_vat")
{
	$dp_no 	 = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$value 	 = (isset($_POST['val'])) ? $_POST['val'] : "";
	$value 	 = str_replace(",","",trim($value)); // 컴마 제거하기


	#법인회사 개인회사 구분하여 부가세 자동계산 Start
	$deposit_result = $deposit_model->getItem($dp_no);
	$company_model  = Company::Factory();
	$company_result = $company_model->getItem($deposit_result['c_no']);
	$license_type 	= $company_result['license_type'];
	$vat_choice		= $deposit_result['vat_choice'];

	$value_no_vat 	= ""; // VAT 미포함
	if($vat_choice == '3') {
		$value_no_vat = $value;
	}
	else
	{
		if($license_type == '1' || $license_type == '2'){ // 법인회사 or 개인회사 (부가세처리)
			$value_no_vat = $value / 1.1;
		}elseif($license_type == '3'){ // 개인 (소득세처리)
			$value_no_vat = $value / 0.967;
		}else{ // 업체등록이 안되어 있는 경우(입금리스트는 무조건 부가세처리로 함)
			$value_no_vat = $value / 1.1;
		}
	}

	$upd_data = array("dp_no" => $dp_no, "dp_money" => $value_no_vat, "dp_money_vat" => $value);

	if (!$deposit_model->update($upd_data))
		echo "입금 완료액 저장에 실패 하였습니다.";
	else{
		echo "입금 완료액이 저장 되었습니다.";
		echo "<script>location.reload();</script>";
	}
	exit;
}
elseif ($proc == "f_cost")
{
	$dp_no 	 = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$value 	 = (isset($_POST['val'])) ? $_POST['val'] : "";
	$value 	 = str_replace(",","",trim($value)); // 컴마 제거하기

	#법인회사 개인회사 구분하여 부가세 자동계산 Start
	$deposit_result = $deposit_model->getItem($dp_no);
	$company_model  = Company::Factory();
	$company_result = $company_model->getItem($deposit_result['c_no']);
	$license_type 	= $company_result['license_type'];
	$vat_choice		= $deposit_result['vat_choice'];

	$value_no_vat 	= ""; // VAT 미포함
	$value_vat 		= 0; // VAT

	if($vat_choice == '3') {
		$value_no_vat = $value;
	}
	else
	{
		if($license_type == '1' || $license_type == '2'){ // 법인회사 or 개인회사 (부가세처리)
			$value_no_vat = $value / 1.1;
			$value_vat	  = $value-$value_no_vat;
		}elseif($license_type == '3'){ // 개인 (소득세처리)
			$value_no_vat = $value / 0.967;
			$value_vat	  = $value-$value_no_vat;
		}else{ // 업체등록이 안되어 있는 경우(입금리스트는 무조건 부가세처리로 함)
			$value_no_vat = $value / 1.1;
			$value_vat	  = $value-$value_no_vat;
		}
	}

	$upd_data = array("dp_no" => $dp_no, "supply_cost" => $value_no_vat, "supply_cost_vat" => $value_vat, "cost" => $value);

	if (!$deposit_model->update($upd_data))
		echo "입금 완료액 저장에 실패 하였습니다.";
	else{
		echo "입금 완료액이 저장 되었습니다.";
		echo "<script>location.reload();</script>";
	}
	exit;
}
elseif ($proc == "f_dp_tax_state")
{
	$dp_no 	  = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$value 	  = (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data = array("dp_no" => $dp_no, "dp_tax_state" => $value);

    if (!$deposit_model->update($upd_data)) {
        echo "계산서 발행상태 저장에 실패 하였습니다.";
    }else {
        if($value == '2')
        {
            $checkDeposit = $deposit_model->checkDepositSlack($dp_no);
            if($checkDeposit['result'])
            {
                $slack_title = "[발행완료] {$checkDeposit['dp_subject']}";
                $dp_url      = "https://work.wplanet.co.kr/v1/deposit_list.php?sch_dp_no={$dp_no}";
                $channel     = "U018T52JM8D";
                $message     = "{$slack_title} 계산서 발행완료 처리되었습니다.\r\n{$dp_url}";
                $slack_type  = 'deposit';
                sendSlackMessage($channel, $message, $slack_type);
            }
        }
        echo "계산서 발행상태가 저장 되었습니다.";
    }
	exit;
}
elseif ($proc == "f_dp_account")
{
	$dp_no 	  = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$value 	  = (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data = array("dp_no" => $dp_no, "dp_account" => $value);

	if (!$deposit_model->update($upd_data))
		echo " 입금받는계좌 변경에 실패 하였습니다.";
	else
		echo "입금받는계좌 변경에 성공했습니다.";
	exit;
}
elseif ($proc == "modify_select_state")
{
	$dp_list 		= (isset($_POST['select_dp_no_list'])) ? $_POST['select_dp_no_list'] : "";
	$select_state 	= (isset($_POST['select_state'])) ? $_POST['select_state'] : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	if($dp_list)
	{
		$chk_dp_sql 	= "SELECT * FROM deposit WHERE dp_no IN({$dp_list})";
		$chk_dp_query	= mysqli_query($my_db, $chk_dp_sql);
		$upd_dp_data 	= [];
		while($chk_dp = mysqli_fetch_array($chk_dp_query))
		{
			$upd_tmp_data = array("dp_no" => $chk_dp['dp_no'], "dp_state" => $select_state);
			if($select_state == '2')
			{
				if(empty($chk_dp['dp_date'])){
					$upd_tmp_data['dp_date'] 		 = date("Y-m-d");
					$upd_tmp_data['incentive_month'] = date("Y-m");
				}

				if(empty($chk_dp['run_s_no'])){
					$upd_tmp_data['run_s_no'] = $session_s_no;
					$upd_tmp_data['run_team'] = $session_team;
				}

				if(empty($chk_dp['dp_money_vat'])){
					$upd_tmp_data["dp_money"] 		= $chk_dp['supply_cost'];
					$upd_tmp_data["dp_money_vat"]	= $chk_dp['cost'];
				}
			}

			$upd_dp_data[] = $upd_tmp_data;
		}

		if(!empty($upd_dp_data)){
			if($deposit_model->multiUpdate($upd_dp_data)){
				exit ("<script>alert('입금상태 변경을 완료했습니다.');location.href='deposit_list.php?{$search_url}';</script>");
			}else{
				exit ("<script>alert('입금상태 변경에 실패했습니다.');location.href='deposit_list.php?{$search_url}';</script>");
			}
		}
	}
	exit ("<script>alert('입금상태 변경에 실패했습니다.');location.href='deposit_list.php?{$search_url}';</script>");
}
elseif ($proc == "modify_select_date")
{
	$dp_list 		= (isset($_POST['select_dp_no_list'])) ? $_POST['select_dp_no_list'] : "";
	$select_date 	= (isset($_POST['select_date'])) ? $_POST['select_date'] : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$select_month	= date("Y-m", strtotime($select_date));

	if($dp_list)
	{
		$chk_dp_sql 	= "SELECT * FROM deposit WHERE dp_no IN({$dp_list}) AND dp_state='2'";
		$chk_dp_query	= mysqli_query($my_db, $chk_dp_sql);
		$upd_dp_data 	= [];
		while($chk_dp = mysqli_fetch_array($chk_dp_query))
		{
			$upd_dp_data[] = array("dp_no" => $chk_dp['dp_no'], "dp_date" => $select_date, "incentive_month" => $select_month);
		}

		if(!empty($upd_dp_data)){
			if($deposit_model->multiUpdate($upd_dp_data)){
				exit ("<script>alert('입금완료일 변경을 완료했습니다.');location.href='deposit_list.php?{$search_url}';</script>");
			}else{
				exit ("<script>alert('입금완료일 변경에 실패했습니다.');location.href='deposit_list.php?{$search_url}';</script>");
			}
		}
	}
	exit ("<script>alert('입금완료일 변경에 실패했습니다.');location.href='deposit_list.php?{$search_url}';</script>");
}
elseif ($proc == "modify_select_confirm")
{
	$dp_list 	= (isset($_POST['select_dp_no_list'])) ? $_POST['select_dp_no_list'] : "";
	$search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	if($dp_list)
	{
		$chk_dp_sql 	= "SELECT * FROM deposit WHERE dp_no IN({$dp_list}) AND dp_state='1' AND c_no != '4026'";
		$chk_dp_query 	= mysqli_query($my_db, $chk_dp_sql);
		$upd_dp_data 	= [];
		$dp_date		= date("Y-m-d");
		$dp_month		= date("Y-m");
		while ($chk_dp = mysqli_fetch_array($chk_dp_query))
		{
			$upd_dp_data[] = array("dp_no" => $chk_dp['dp_no'], "dp_state" => 2, "dp_date" => $dp_date, "incentive_month" => $dp_month, "dp_money" => $chk_dp['supply_cost'], "dp_money_vat" => $chk_dp['cost'],"run_s_no" => $session_s_no, "run_team" => $session_team);
		}

		if(!empty($upd_dp_data)){
			if($deposit_model->multiUpdate($upd_dp_data)){
				exit ("<script>alert('입금완료 일괄 처리했습니다.');location.href='deposit_list.php?{$search_url}';</script>");
			}else{
				exit ("<script>alert('입금완료 일괄처리에 실패했습니다.');location.href='deposit_list.php?{$search_url}';</script>");
			}
		}
	}
	exit ("<script>alert('입금완료 일괄처리에 실패했습니다.');location.href='deposit_list.php?{$search_url}';</script>");
}

# Navigation & My Quick
$nav_prd_no  = "62";
$nav_title   = "입금리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# Option Model 설정
$team_model 	= Team::Factory();
$mycomp_model 	= MyCompany::Factory();
$product_model 	= Product::Factory();
$corp_model 	= Corporation::Factory();


# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where 			= " dp.display = '1'";

$sch				= isset($_GET['sch']) ? $_GET['sch']:"Y";
$sch_dp_quick 		= isset($_GET['sch_dp_quick']) ? $_GET['sch_dp_quick'] : "";
$sch_dp_state 		= isset($_GET['sch_dp_state']) ? $_GET['sch_dp_state'] : "";
$sch_dp_method 		= isset($_GET['sch_dp_method']) ? $_GET['sch_dp_method'] : "";
$sch_dp_no 			= isset($_GET['sch_dp_no']) ? $_GET['sch_dp_no'] : "";
$sch_s_dp_date 		= isset($_GET['sch_s_dp_date']) ? $_GET['sch_s_dp_date'] : "";
$sch_e_dp_date 		= isset($_GET['sch_e_dp_date']) ? $_GET['sch_e_dp_date'] : "";
$sch_dp_tax_state 	= isset($_GET['sch_dp_tax_state']) ? $_GET['sch_dp_tax_state'] : "";
$sch_team 			= isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$sch_s_no 			= isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$sch_dp_etc 		= isset($_GET['sch_dp_etc']) ? $_GET['sch_dp_etc'] : "";
$sch_my_c_no 		= isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_dp_account		= isset($_GET['sch_dp_account']) ? $_GET['sch_dp_account'] : "";
$sch_prd_no			= isset($_GET['sch_prd_no']) ? $_GET['sch_prd_no'] : "";
$sch_c_name 		= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_c_no 			= isset($_GET['sch_c_no'])?$_GET['sch_c_no']:"";
$sch_d_my_c_no 		= isset($_GET['sch_d_my_c_no']) ? $_GET['sch_d_my_c_no'] : "";
$sch_d_c_name		= isset($_GET['sch_d_c_name']) ? $_GET['sch_d_c_name'] : "";
$sch_d_c_equal		= isset($_GET['sch_d_c_equal']) ? $_GET['sch_d_c_equal'] : "";
$sch_subject 		= isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
$sch_bk_name 		= isset($_GET['sch_bk_name']) ? $_GET['sch_bk_name'] : "";
$sch_mod_type 		= isset($_GET['sch_mod_type']) ? $_GET['sch_mod_type'] : "";

if(!empty($sch)) {
	$smarty->assign("sch", $sch);
}
$smarty->assign("sch_dp_quick", $sch_dp_quick);

if($sch_dp_quick == '8'){
	$sch_dp_account = '1';
}

if (!empty($sch_dp_no)) {
	$add_where .= " AND dp.dp_no = '{$sch_dp_no}'";
	$smarty->assign("sch_dp_no", $sch_dp_no);
}

if (!empty($sch_dp_state)) {
	$add_where .= " AND dp.dp_state = '{$sch_dp_state}'";
	$smarty->assign("sch_dp_state", $sch_dp_state);
}

if (!empty($sch_dp_method)) {
	$add_where .= " AND dp.dp_method = '{$sch_dp_method}'";
	$smarty->assign("sch_dp_method", $sch_dp_method);
}

if (!empty($sch_s_dp_date)) {
	$add_where .= " AND dp.dp_date >= '{$sch_s_dp_date}'";
	$smarty->assign("sch_s_dp_date", $sch_s_dp_date);
}

if (!empty($sch_e_dp_date)) {
	$add_where .= " AND dp.dp_date <= '{$sch_e_dp_date}'";
	$smarty->assign("sch_e_dp_date", $sch_e_dp_date);
}

if (!empty($sch_dp_tax_state)) {
	$add_where .= " AND dp.dp_tax_state = '{$sch_dp_tax_state}'";
	$smarty->assign("sch_dp_tax_state", $sch_dp_tax_state);
}

#팀 체크
if (!empty($sch_team))
{
	$sch_team_code_where = getTeamWhere($my_db, $sch_team);
	if($sch_team_code_where){
		$add_where .= " AND team IN ({$sch_team_code_where})";
	}
	$smarty->assign("sch_team", $sch_team);
}

if (!empty($sch_s_no)) {
	if($sch_s_no == "알수없음"){
		$add_where .= " AND (s_no = '{$sch_s_no}' or s_no = 0) ";
	}else{
		$add_where .= " AND s_no IN (SELECT sub.s_no FROM staff sub WHERE sub.s_name like '%{$sch_s_no}%')";
	}

	$smarty->assign("sch_s_no", $sch_s_no);
}

if (!empty($sch_dp_etc)) {
	$add_where .= " AND dp.c_no in (SELECT c_no FROM company WHERE o_registration = '' OR o_registration IS NULL OR tx_company = '' OR tx_company IS NULL OR tx_company_number <= '' OR tx_company_number IS NULL OR tx_company_ceo <= '' OR tx_company_ceo IS NULL) ";
	$smarty->assign("sch_dp_etc", $sch_dp_etc);
}

if (!empty($sch_my_c_no)) {
	$add_where .= " AND dp.my_c_no = '{$sch_my_c_no}'";
	$smarty->assign("sch_my_c_no", $sch_my_c_no);
}

if (!empty($sch_dp_account)) {
	$add_where .= " AND dp.dp_account = '{$sch_dp_account}'";
	$smarty->assign("sch_dp_account", $sch_dp_account);
}

if(!empty($sch_prd_no)) { // 상품
	$add_where .= " AND (SELECT w.prd_no FROM work w WHERE w.w_no=dp.w_no)='{$sch_prd_no}'";
	$smarty->assign("sch_prd_no", $sch_prd_no);
}

if (!empty($sch_c_name)) {
	$add_where .= " AND dp.c_name like '%{$sch_c_name}%'";
	$smarty->assign("sch_c_name", $sch_c_name);
}

if($sch_c_no == 'null') { // c_no is null
	$add_where.=" AND dp.c_no ='0'";
	$smarty->assign("sch_c_no", $sch_c_no);
}

if (!empty($sch_subject)) {
	$add_where .= " AND dp.dp_subject like '%{$sch_subject}%'";
	$smarty->assign("sch_subject", $sch_subject);
}

if (!empty($sch_bk_name)) {
	$add_where .= " AND dp.bk_name like '%{$sch_bk_name}%'";
	$smarty->assign("sch_bk_name", $sch_bk_name);
}

if (!empty($sch_mod_type)) {
	$add_where .= " AND dp.mod_type = '{$sch_mod_type}'";
	$smarty->assign("sch_mod_type", $sch_mod_type);
}

if(!empty($sch_d_my_c_no))
{
	$add_where .= " AND (SELECT (SELECT c.my_c_no FROM company c WHERE c.c_no=w.c_no) FROM work w WHERE w.w_no = dp.w_no) = '{$sch_d_my_c_no}'";
	$smarty->assign("sch_d_my_c_no", $sch_d_my_c_no);
}

if (!empty($sch_d_c_name))
{
	if(!empty($sch_d_c_equal)){
		$add_where .= " AND (SELECT w.c_name FROM work w WHERE w.w_no = dp.w_no) = '{$sch_d_c_name}'";
		$smarty->assign("sch_d_c_equal", $sch_d_c_equal);
	}else{
		$add_where .= " AND (SELECT w.c_name FROM work w WHERE w.w_no = dp.w_no) like '%{$sch_d_c_name}%'";
	}

	$smarty->assign("sch_d_c_name", $sch_d_c_name);
}

if ($sch_dp_state == "2")
	$add_orderby = " dp.dp_date desc ";
else {
	if (permissionNameCheck($session_permission, "마케터"))
		$add_orderby = " case when s_no = 0 then 99999999 + dp_no else dp_no end desc ";
	else
		$add_orderby = " dp.dp_no desc ";
}


# 출금 게시물 수
$cnt_sql 	= "SELECT count(dp_no) as cnt FROM deposit dp WHERE {$add_where}";
$result		= mysqli_query($my_db, $cnt_sql);
$cnt_data	= mysqli_fetch_array($result);
$total_num 	= $cnt_data['cnt'];

# 페이지 계산
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($total_num/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$search_url = getenv("QUERY_STRING");
$pagelist 	= pagelist($pages, "deposit_list.php", $pagenum, $search_url);

$smarty->assign("total_num", $total_num);
$smarty->assign("search_url", $search_url);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);


# 출금 리스트 쿼리
$deposit_sql = "
	SELECT
		*,
		(SELECT s.s_name FROM staff s WHERE s.s_no=dp.s_no) AS s_name,
		(SELECT t.team_name FROM team t WHERE t.team_code=dp.team) AS t_name,
		(SELECT title FROM product prd WHERE prd.prd_no=(SELECT w.prd_no FROM work w WHERE w.w_no = dp.w_no)) AS product_title,
		(SELECT w.prd_no FROM work w WHERE w.w_no = dp.w_no) AS prd_no,
		(SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=dp.my_c_no) AS my_c_name,
		(SELECT mc.kind_color FROM my_company mc WHERE mc.my_c_no=dp.my_c_no) AS my_c_kind_color,
		(SELECT s_name FROM staff s WHERE s.s_no=dp.reg_s_no) AS reg_s_name,
		(SELECT s_name FROM staff s WHERE s.s_no=dp.run_s_no) AS run_s_name,
	   	(SELECT cp.my_c_no FROM company cp WHERE cp.c_no = (SELECT w.c_no FROM work w WHERE w.w_no = dp.w_no)) as d_my_c_no,
		(SELECT w.c_name FROM work w WHERE w.w_no = dp.w_no) as d_c_name,
		(SELECT concat(o_registration, '||', tx_company, '||', tx_company_number, '||', tx_company_ceo) FROM company WHERE c_no = dp.c_no) tx_info,
		(SELECT COUNT(*) FROM linked_comment lc WHERE lc.linked_table='deposit' AND lc.linked_no=dp.dp_no) as comment_cnt
	FROM deposit dp
	WHERE {$add_where}
	ORDER BY {$add_orderby}
	LIMIT {$offset}, {$num}
";
$deposit_query 	= mysqli_query($my_db, $deposit_sql);
$deposit_list 	= [];
$dp_mod_type_option = getDpModTypeOption();
$dp_method_option 	= getDpMethodOption();
while($deposit_result = mysqli_fetch_array($deposit_query))
{
	$tx_info_msg = "";
	$tx_info_chk = [];
	$tx_info_val = $deposit_result['tx_info'];
	$tx_info 	 = explode("||", $tx_info_val);

	if (!$tx_info[0])
		$tx_info_chk[] = "사업자 (파일)";
	if (!$tx_info[1])
		$tx_info_chk[] = "업체명";
	if (!$tx_info[2])
		$tx_info_chk[] = "번호";
	if (!$tx_info[3])
		$tx_info_chk[] = "대표자";

	if (count($tx_info_chk) > 0) {
		$tx_info_chk_msg = implode(", ", $tx_info_chk);
		$tx_info_msg 	 = "자료없음 ({$tx_info_chk_msg})";
	}

	if(!empty($deposit_result['regdate'])) {
		$regdate_day_value 	= date("Y/m/d",strtotime($deposit_result['regdate']));
		$regdate_time_value = date("H:i",strtotime($deposit_result['regdate']));
	}else{
		$regdate_day_value 	= '';
		$regdate_time_value = '';
	}

	$deposit_result['is_cs_editable'] = false;
	if($deposit_result['prd_no'] == '260' && $session_team == '00244'){
		$deposit_result['is_cs_editable'] = true;
	}

	$deposit_result['s_name'] 		 	= $deposit_result['s_name'] ? $deposit_result['s_name'] : "<span style=\"color:red\">알수없음</span>";
	$deposit_result['regdate_day']   	= $regdate_day_value;
	$deposit_result['regdate_time']  	= $regdate_time_value;
	$deposit_result['dp_check_msg']  	= $tx_info_msg;
	$deposit_result['mod_type_name'] 	= (isset($deposit_result['mod_type']) && !empty($deposit_result['mod_type'])) ? $dp_mod_type_option[$deposit_result['mod_type']] : "";
	$deposit_result['dp_method_name'] 	= $dp_method_option[$deposit_result['dp_method']];
	$deposit_result['is_confirm_editable'] = ($deposit_result["prd_no"] == "260" && $session_team == '00244') ? true : false;

	$deposit_list[] = $deposit_result;
}

# 월정산 데이터
$deposit_mon_total_sql 	= "SELECT c_no, c_name, SUM(cost) as total FROM deposit wd WHERE dp_method='3' AND dp_state='1' AND display='1' GROUP BY c_no ORDER BY c_name ASC";
$deposit_mon_total_query 	= mysqli_query($my_db, $deposit_mon_total_sql);
$deposit_mon_total_list 	= [];
while($deposit_mon_total = mysqli_fetch_array($deposit_mon_total_query)){
	$deposit_mon_total_list[$deposit_mon_total['c_no']] = $deposit_mon_total;
}
$smarty->assign("deposit_mon_total_list", $deposit_mon_total_list);

# 공급가액, 출금 요청액, 출금 완료액 총합
$deposit_price_sql = "
	SELECT
		SUM(dp.supply_cost) AS total_supply_cost,
		SUM(dp.cost) AS total_cost,
		SUM(dp.dp_money) AS total_dp_money,
		SUM(dp.dp_money_vat) AS total_dp_money_vat
	FROM deposit dp
	WHERE {$add_where}
";
$deposit_price_query  = mysqli_query($my_db, $deposit_price_sql);
$deposit_price_result = mysqli_fetch_array($deposit_price_query);

$smarty->assign("total_supply_cost", number_format($deposit_price_result['total_supply_cost']));
$smarty->assign("total_cost", number_format($deposit_price_result['total_cost']));
$smarty->assign("total_dp_money", number_format($deposit_price_result['total_dp_money']));
$smarty->assign("total_dp_money_vat", number_format($deposit_price_result['total_dp_money_vat']));

$corp_account_option = $corp_model->getAccountList('1');
$corp_account_option[1][5] = "기업-지출";

$smarty->assign("sch_team_list", $team_model->getTeamFullNameList());
$smarty->assign("sch_mod_type_option", $dp_mod_type_option);
$smarty->assign("my_company_list", $mycomp_model->getList());
$smarty->assign("sch_my_company_option", $mycomp_model->getNameList());
$smarty->assign("sch_corp_account_option", $corp_account_option);
$smarty->assign("sch_dp_quick_option", getDpQuickOption());
$smarty->assign("sch_dp_state_option", getDpStateOption());
$smarty->assign("sch_dp_method_option", $dp_method_option);
$smarty->assign("sch_dp_tax_state_option", getDpTaxOption());
$smarty->assign("sch_dp_etc_option", getDpEtcOption());
$smarty->assign("sch_dp_product_option", $product_model->getDepositWorkProduct());
$smarty->assign("page_type_option", getPageTypeOption(3));
$smarty->assign("deposit_list", $deposit_list);

$smarty->display('deposit_list.html');


/* Slack API START */
function sendSlackMessage($channel, $message, $type)
{
    $slackHook = new SlackHook();
    $slackHook->setTokenUsername($type);
    $slackHook->setChannel($channel);
    $slackHook->setMessage($message);
    $slackHook->send();
}
/* Slack API END */
?>
