<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_price.php');

$proc = (isset($_POST['process'])) ? $_POST['process'] : "";

if($proc == "add_order_withdraw")
{
    if(empty($_POST['wd_supply_price_new']) || empty($_POST['wd_vat_price_new']))
    {
        exit("<script>alert('필수 데이터가 없습니다. 지급완료 상세내역 추가에 실패했습니다.');history.back();</script>");
    }

    $set_no          = $_POST['set_no'];
    $supply_cost 	 = str_replace(",","",trim($_POST['wd_supply_price_new']));
    $supply_cost_vat = str_replace(",","",trim($_POST['wd_vat_price_new']));
    $wd_money_vat 	 = $supply_cost + $supply_cost_vat;

    $withdraw_sql = "
        INSERT INTO withdraw SET
            my_c_no     = (SELECT my_c_no FROM commerce_order_set WHERE `no`='{$set_no}'),
            wd_subject  = CONCAT('[발주 입금]',(SELECT sup_c_name FROM commerce_order_set WHERE `no`='{$set_no}'), ' ', (SELECT order_count FROM commerce_order_set WHERE `no`='{$set_no}'), '차'),
            wd_state    = '2',
            wd_method   = '1',
            c_no        = (SELECT c_no FROM commerce_order_set WHERE `no`='{$set_no}'),
            c_name      = (SELECT c_name FROM commerce_order_set WHERE `no`='{$set_no}'),
            s_no        = (SELECT s_no FROM company WHERE c_no=(SELECT c_no FROM commerce_order_set WHERE `no`='{$set_no}')),
            team        = (SELECT team FROM staff WHERE s_no=(SELECT s_no FROM company WHERE c_no=(SELECT c_no FROM commerce_order_set WHERE `no`='{$set_no}'))),
            supply_cost = '{$supply_cost}',
            vat_choice      = '1',
            supply_cost_vat = '{$supply_cost_vat}',
            cost            = '{$wd_money_vat}',
            wd_tax_state    = '1',
            regdate         = now(),
            reg_s_no        = '{$session_s_no}'
	";

    if (!mysqli_query($my_db, $withdraw_sql)){
        exit("<script>alert('지급 요청에 실패 하였습니다.'); history.back();</script>");
    }else{
        $withdraw_query  = mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
        $withdraw_result = mysqli_fetch_array($withdraw_query);
        $last_wd_no 	 = $withdraw_result[0];

        $ins_sql = "
            INSERT INTO commerce_order SET 
                `type`  = 'withdraw', 
                set_no  = '{$set_no}', 
                supply_price = '{$supply_cost}', 
                vat     = '{$supply_cost_vat}',  
                price   = '{$wd_money_vat}', 
                wd_no   = '{$last_wd_no}',
                memo    =".addslashes($_POST['wd_memo_new'])."
            ";

        if (!mysqli_query($my_db, $ins_sql))
            echo("<script>alert('출금번호 저장에 실패하였습니다.');</script>");
        else
            echo("<script>alert('지급이 요청 되었습니다.');</script>");
    }

    exit ("<script>location.href='commerce_order_iframe_withdraw.php?no={$set_no}';</script>");
}
elseif($proc == "withdraw_delete")
{
    $set_no = (isset($_POST['set_no'])) ? $_POST['set_no'] : "";
    $co_no 	= (isset($_POST['co_no'])) ? $_POST['co_no'] : "";
    $wd_no  = (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";

    if(empty($set_no) || empty($co_no) || empty($wd_no)){
        echo("<script>alert('삭제에 실패 하였습니다.');</script>");
    }
    else
    {
        $del_sql = "DELETE FROM commerce_order WHERE `no` = '{$co_no}'";

        if (!mysqli_query($my_db, $del_sql)) {
            echo("<script>alert('삭제에 실패 하였습니다.');</script>");
        } else {
            $withdraw_del_sql = "DELETE FROM withdraw WHERE wd_no={$wd_no}";

            if (!mysqli_query($my_db, $withdraw_del_sql)) {
                echo("<script>alert('출금 삭제에 실패 하였습니다');</script>");
            }
        }
    }

    exit ("<script>location.href='commerce_order_iframe_withdraw.php?no={$set_no}';</script>");
}
elseif($proc == "f_supply_price")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";
    $value  = str_replace(",", "", trim($value));

    $comm_sql 	  = "SELECT vat FROM commerce_order co WHERE co.no='{$no}'";
    $comm_query   = mysqli_query($my_db, $comm_sql);
    $comm_result  = mysqli_fetch_assoc($comm_query);
    $total_price  = $value+$comm_result['vat'];

    $sql    = "UPDATE commerce_order co SET co.supply_price='{$value}', co.price='{$total_price}' WHERE co.no = '{$no}'";

    if (!mysqli_query($my_db, $sql))
        echo "공급가 저장에 실패 하였습니다.";
    else
        echo "공급가가 저장 되었습니다.";
    exit;
}
elseif($proc == "f_vat")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";
    $value  = str_replace(",", "", trim($value));

    $comm_sql 	  = "SELECT supply_price FROM commerce_order co WHERE co.no='{$no}'";
    $comm_query   = mysqli_query($my_db, $comm_sql);
    $comm_result  = mysqli_fetch_assoc($comm_query);
    $total_price  = $value+$comm_result['supply_price'];

    $sql    = "UPDATE commerce_order co SET co.vat = '{$value}', co.price='{$total_price}' WHERE co.no = '{$no}'";

    if (!mysqli_query($my_db, $sql))
        echo "부가세 저장에 실패 하였습니다.";
    else
        echo "부가세가 저장 되었습니다.";
    exit;
}
elseif($proc == "f_memo")
{
	$no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql    = "UPDATE commerce_order co SET co.memo = '" . addslashes($value) . "' WHERE co.no = '{$no}'";

    if (!mysqli_query($my_db, $sql))
        echo "비고 저장에 실패 하였습니다.";
    else
        echo "비고가 저장 되었습니다.";
    exit;
}

# 발주서 지급내역
$no = isset($_GET['no']) ? $_GET['no'] : "";
$sch_supply_date = isset($_GET['sch_supply_date']) ? $_GET['sch_supply_date'] : date('Y-m', strtotime('-1 months'));

$add_sup_where = "set_no='{$no}' AND `type`='supply'";
if(!empty($sch_supply_date))
{
    $sch_supply_s_date = $sch_supply_date."-01";
    $sch_supply_e_date = $sch_supply_date."-31";
    $add_sup_where    .= " AND sup_date BETWEEN '{$sch_supply_s_date}' AND '{$sch_supply_e_date}'";
    $smarty->assign("sch_supply_date", $sch_supply_date);
}
$smarty->assign("no", $no);

$commerce_order_set_sql="
		SELECT
			cos.no,
			cos.state,
		    (SELECT c.license_type FROM company c WHERE c.c_no=cos.sup_c_no) as license_type,
		    sup_c_no,
		    sup_loc_type,
			cos.req_s_no
		FROM commerce_order_set cos
		WHERE cos.no='{$no}'
";
$commerce_order_set_query = mysqli_query($my_db, $commerce_order_set_sql);
$commerce_order_set       = mysqli_fetch_array($commerce_order_set_query);

$commerce_order_sql = "
    SELECT
        co.no,
        co.type,
        co.option_no,
        co.supply_price,
        co.vat,
        co.memo,
        co.wd_no,
        (SELECT regdate FROM withdraw WHERE wd_no=co.wd_no) AS wd_regdate,
        (SELECT wd_money FROM withdraw WHERE wd_no=co.wd_no) AS wd_money,
        (SELECT wd_date FROM withdraw WHERE wd_no=co.wd_no) AS wd_date
    FROM commerce_order co
    WHERE co.set_no='{$no}' AND co.`type` IN('request', 'withdraw')
    ORDER BY `no` ASC
";
$commerce_order_query = mysqli_query($my_db, $commerce_order_sql);
$commerce_order_list  = [];
$usd_currency         = getCurrency("usd");
$request_total        = 0;
$withdraw_total       = 0;
while($commerce_order_result = mysqli_fetch_array($commerce_order_query))
{
    if($commerce_order_result['type'] == 'request'){
        $request_total += $commerce_order_result['supply_price']+$commerce_order_result['vat'];
    }

    if($commerce_order_result['type'] == 'withdraw'){
        $withdraw_total += $commerce_order_result['wd_money'];

        $wd_money_usd 	= calculateCurrency($usd_currency, $commerce_order_result['wd_money']);
        $commerce_order_result['wd_regdate']   = date("Y-m-d", strtotime($commerce_order_result['wd_regdate']));
        $commerce_order_result['wd_money_usd'] = $wd_money_usd;
        $commerce_order_list[] = $commerce_order_result;
    }
}

$rest_price = $request_total - $withdraw_total;
$request_total_usd = $withdraw_total_usd = $rest_price_usd = [];
if($commerce_order_set['license_type'] == '4')
{
    $request_total_usd 	= calculateCurrency($usd_currency, $request_total);
    $withdraw_total_usd = calculateCurrency($usd_currency, $withdraw_total);
    $rest_price_usd 	= calculateCurrency($usd_currency, $rest_price);
}

$mon_supply_sql = "SELECT co.option_no, (SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_name, SUM(co.quantity) as sup_qty, SUM(co.supply_price) as sup_price, SUM(co.vat) as vat_price FROM commerce_order co WHERE {$add_sup_where} GROUP BY option_no";
$mon_supply_query = mysqli_query($my_db, $mon_supply_sql);
$mon_sup_price_total = 0;
$mon_vat_price_total = 0;
$mon_total_price     = 0;
$mon_supply_list     = [];
while($mon_supply = mysqli_fetch_assoc($mon_supply_query))
{
    $mon_supply['total_price'] = $mon_supply['sup_price'] + $mon_supply['vat_price'];
    $mon_supply_list[] = $mon_supply;

    $mon_sup_price_total += $mon_supply['sup_price'];
    $mon_vat_price_total += $mon_supply['vat_price'];
    $mon_total_price     += $mon_supply['total_price'];
}


$smarty->assign("request_total", $request_total);
$smarty->assign("request_total_usd", $request_total_usd);
$smarty->assign("withdraw_total", $withdraw_total);
$smarty->assign("withdraw_total_usd", $withdraw_total_usd);
$smarty->assign("rest_price", $rest_price);
$smarty->assign("rest_price_usd", $rest_price_usd);
$smarty->assign("mon_sup_price_total", $mon_sup_price_total);
$smarty->assign("mon_vat_price_total", $mon_vat_price_total);
$smarty->assign("mon_total_price", $mon_total_price);
$smarty->assign('mon_supply_list', $mon_supply_list);
$smarty->assign('commerce_order_set', $commerce_order_set);
$smarty->assign('commerce_order_list', $commerce_order_list);
$smarty->display('commerce_order_iframe_withdraw.html');

?>
