<?php
require('../inc/common.php');
require('../inc/data_state.php');
require('ckadmin.php');


$reject = false;
if($session_staff_state == '2'){ // 프리랜서의 경우 기본 접근제한으로 설정
	$reject = true;
}

for($i=0 ; $i < sizeof($access_accept_list) ; $i++){ // 접근허용 리스트
	if($session_s_no == $access_accept_list[$i][1]){
		if($_GET['sch_prd'] == $access_accept_list[$i][2] || $access_accept_list[$i][2] == 'all'){
			$reject = false;
		}
	}
}

if($reject){
	for($i=0 ; $i < sizeof($access_reject_list) ; $i++){ // 접근제한 리스트
		if($session_s_no == $access_reject_list[$i][1]){
			if($_GET['sch_prd'] == $access_reject_list[$i][2] || $access_reject_list[$i][2] == 'all'){
				$reject = true;
			}
		}
	}
}
if($reject && !!$_GET['sch_prd']){
	$smarty->display('access_error.html');
	exit;
}


$proc=(isset($_POST['process']))?$_POST['process']:"";


/////////////////////////* 자동 저장 처리 부분 *//////////////////////

if ($proc == "f_task_run_regdate") { //업무완료일 자동저장

	$w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE work w SET w.task_run_regdate = NULL WHERE w.w_no = '" . $w_no . "'";
	}else{
		$sql = "UPDATE work w SET w.task_run_regdate = '" . $value . "' WHERE w.w_no = '" . $w_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "업무완료일 저장에 실패 하였습니다.";
	else
		echo "업무완료일이 저장 되었습니다.";

	exit;
}elseif ($proc == "f_work_state") { //진행상태 자동저장
	$w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if($value == '6'){
		$task_run_date_sql = "SELECT w.task_run_regdate FROM work w WHERE w.w_no = '" . $w_no . "'";

		$task_run_date=mysqli_fetch_array(mysqli_query($my_db, $task_run_date_sql));

		if(!$task_run_date[0]){
			$sql = "UPDATE work w SET w.task_run_regdate = NOW(), w.work_state = '" . $value . "' WHERE w.w_no = '" . $w_no . "'";
		}else{
			$sql = "UPDATE work w SET w.work_state = '" . $value . "' WHERE w.w_no = '" . $w_no . "'";
		}
	}else{
		$sql = "UPDATE work w SET w.work_state = '" . $value . "' WHERE w.w_no = '" . $w_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "진행상태 저장에 실패 하였습니다.";
	else{
		echo "진행상태가 저장 되었습니다.";
	}

	exit;

}elseif ($proc == "f_k_name_code") { //업무진행구분 자동저장
	$w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE work w SET w.k_name_code = NULL WHERE w.w_no = '" . $w_no . "'";
	}else{
		$sql = "UPDATE work w SET w.k_name_code = '" . $value . "' WHERE w.w_no = '" . $w_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "업무진행구분 저장에 실패 하였습니다.";
	else
		echo "업무진행구분이 저장 되었습니다.";

	exit;

}elseif ($proc == "f_task_run") { //업무진행 자동저장
	$w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	$sql = "UPDATE work w SET w.task_run = '" . addslashes($value) . "' WHERE w.w_no = '" . $w_no . "'";

	if (!mysqli_query($my_db, $sql))
		echo "업무진행 저장에 실패 하였습니다.";
	else{
		echo "업무진행이 저장 되었습니다.";
		//echo "<script>location.reload();</script>";
	}

	exit;

}elseif ($proc == "f_task_run_dday") {
	$w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE work w SET w.task_run_dday = NULL WHERE w.w_no = '" . $w_no . "'";
	}else{
		$sql = "UPDATE work w SET w.task_run_dday = '" . $value . "' WHERE w.w_no = '" . $w_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "완료 예정일 저장에 실패 하였습니다.";
	else
		echo "완료 예정일이 저장 되었습니다.";

	exit;

/////////////////////////* fom action 처리 부분 *//////////////////////

} elseif ($proc == "down_file") {
	$w_no = isset($_POST['w_no']) ? $_POST['w_no'] : "";
	$folder = isset($_POST['folder']) ? $_POST['folder'] : "";

	if ($folder == "out_task_req"){
		$sql="SELECT task_req_file_origin, task_req_file_read FROM work w WHERE w_no = '{$w_no}'";
		$result = mysqli_query($my_db, $sql);
		$file_info = mysqli_fetch_array($result);

		$filename = htmlspecialchars($file_info['task_req_file_origin']);
		$string = $file_info['task_req_file_read'];
	} else if ($folder == "out_task_run"){
		$sql="SELECT task_run_file_origin, task_run_file_read FROM work w WHERE w_no = '{$w_no}'";
		$result = mysqli_query($my_db, $sql);
		$file_info = mysqli_fetch_array($result);

		$filename = htmlspecialchars($file_info['task_run_file_origin']);
		$string = $file_info['task_run_file_read'];
	}

	echo "File UP Name : $string<br>";
	echo "File Downloading...<br>File DN Name : $filename<br>";
	exit("<script>location.href='../popup/file_download.php?file_dn_name=".urlencode($filename)."&file_up_name=".$string."';target='_blank';</script>");

} elseif($proc=="del_file") {
	$w_no = isset($_POST['w_no']) ? $_POST['w_no'] : "";
	$folder = isset($_POST['folder']) ? $_POST['folder'] : "";
	$search_url_get = isset($_POST['search_url']) ? $_POST['search_url'] : "";

	if ($folder == "out_task_req") {
		$sql="SELECT task_req_file_origin, task_req_file_read FROM work w WHERE w_no = '{$w_no}'";
		$result = mysqli_query($my_db, $sql);
		$file_info = mysqli_fetch_array($result);

		$string = "uploads/".$file_info['task_req_file_read'];
		$del_sql="UPDATE work w SET task_req_file_origin = '', task_req_file_read = '' WHERE w_no = '{$w_no}'";
	} else if ($folder == "out_task_run") {
		$sql="SELECT task_run_file_origin, task_run_file_read FROM work w WHERE w_no = '{$w_no}'";
		$result = mysqli_query($my_db, $sql);
		$file_info = mysqli_fetch_array($result);

		$string = "uploads/".$file_info['task_run_file_read'];
		$del_sql="UPDATE work w SET task_run_file_origin = '', task_run_file_read = '' WHERE w_no = '{$w_no}'";
	}

	unlink($string);
	$my_db->query($del_sql);

	exit("<script>alert('해당 파일을 삭제 하였습니다.');location.href='work_list.php?{$search_url_get}';</script>");

} else if ($proc == "save_task_run_file") {
	$w_no = isset($_POST['w_no']) ? $_POST['w_no'] : "";
	//$f_task_run = isset($_POST['f_task_run']) ? $_POST['f_task_run'] : "";
	$search_url_get = isset($_POST['search_url']) ? $_POST['search_url'] : "";

	$files = $_FILES["file2"];
	image_check($files);
	$save_name = store_image($files, "out_task_run");

	$add_set = "";
	if (!empty($save_name[0])) {
		$add_set .= "task_run_file_read = '" . $save_name[0] . "'";
	}
	if(!empty($files["name"][0])) {
		$add_set.=", task_run_file_origin='".addslashes($files["name"][0])."'";
	}

	//$sql = "UPDATE work SET task_run = '{$f_task_run}', task_run_regdate = now(), task_run_s_no = '{$session_s_no}' {$add_set} WHERE w_no = '{$w_no}'";
	$sql = "UPDATE work SET {$add_set} WHERE w_no = '{$w_no}'";

	if(!mysqli_query($my_db,$sql)){
		exit("<script>alert('파일 업로드에 실패 하였습니다');location.href='work_list.php?{$search_url_get}';</script>");
	}

	exit ("<script>alert('파일 업로드를 완료하였습니다.');location.href='work_list.php?{$search_url_get}';</script>");

}elseif($proc=="f_work_state_save_4") {
	$w_no=(isset($_POST['w_no']))?$_POST['w_no']:"";

	$search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

		$sql="update work w set
						w.work_state = '4'
					where
						w.w_no='".$w_no."'
					";

	mysqli_query($my_db,$sql);

	exit("<script>history.back();</script>");
	//exit("<script>location.href='work_list.php?$search_url_get';</script>");

}elseif($proc=="f_work_state_save_6") {
	$w_no=(isset($_POST['w_no']))?$_POST['w_no']:"";
	$f_task_run_regdate_post=isset($_POST['f_task_run_regdate'.$w_no])?$_POST['f_task_run_regdate'.$w_no]:"";

	$search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

	$add_set="";
	if(!empty($f_task_run_regdate_post)) {
		$add_set.=", task_run_regdate='".$f_task_run_regdate_post."'";
	}else{
		$add_set.=", task_run_regdate = now()";
	}

	$sql="update work w set
					w.work_state = '6'
					$add_set
				where
					w.w_no='".$w_no."'
				";

	mysqli_query($my_db,$sql);

	exit("<script>history.back();</script>");
	//exit("<script>location.href='work_list.php?$search_url_get';</script>");

} else {

	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where="1=1";

	$set_get=isset($_GET['sch_set'])?$_GET['sch_set']:"";

	$q_sch_get=isset($_GET['q_sch'])?$_GET['q_sch']:"";

	$sch_prd_g1_get=isset($_GET['sch_prd_g1'])?$_GET['sch_prd_g1']:"";
	$sch_prd_g2_get=isset($_GET['sch_prd_g2'])?$_GET['sch_prd_g2']:"";
	$sch_prd_get=isset($_GET['sch_prd'])?$_GET['sch_prd']:"";

	$sch_task_run_regdate_get=isset($_GET['sch_task_run_regdate'])?$_GET['sch_task_run_regdate']:"";
	$sch_work_state_get=isset($_GET['sch_work_state'])?$_GET['sch_work_state']:"";

	$sch_dp_c_no_get=isset($_GET['sch_dp_c_no'])?$_GET['sch_dp_c_no']:"";
	$sch_wd_c_no_get=isset($_GET['sch_wd_c_no'])?$_GET['sch_wd_c_no']:"$session_c_no";
	$sch_wd_pay_date_get=isset($_GET['sch_wd_pay_date'])?$_GET['sch_wd_pay_date']:"";

	$sch_c_name_get=isset($_GET['sch_c_name'])?$_GET['sch_c_name']:"";
	$sch_t_keyword_get=isset($_GET['sch_t_keyword'])?$_GET['sch_t_keyword']:"";
	$sch_r_keyword_get=isset($_GET['sch_r_keyword'])?$_GET['sch_r_keyword']:"";

	$sch_w_no_get=isset($_GET['sch_w_no'])?$_GET['sch_w_no']:"";
	$sch_dp_no_get=isset($_GET['sch_dp_no'])?$_GET['sch_dp_no']:"";
	$sch_wd_no_get=isset($_GET['sch_wd_no'])?$_GET['sch_wd_no']:"";


	//QUICK SEARCH
	if(!empty($q_sch_get)) { // 퀵서치
		if($q_sch_get == "1"){ //요청안된건
			$add_where.=" AND (w.work_state='1' OR w.work_state='2')";
		}elseif($q_sch_get == "2"){ //요청중인건
			$add_where.=" AND w.work_state='3'";
		}elseif($q_sch_get == "3"){ //진행중인건
			$add_where.=" AND (w.work_state='4' OR w.work_state='5')";
		}elseif($q_sch_get == "4"){ //진행완료건
			$add_where.=" AND w.work_state='6'";
		}elseif($q_sch_get == "5"){ //연장여부 확인요청건
			// D-10일 ~ +5일
			$add_where.=" AND w.work_state='6' AND w.extension_date <= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL 10 DAY), '%Y-%m-%d') AND w.extension_date >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -5 DAY), '%Y-%m-%d') AND (w.extension='2' OR w.extension='0' OR w.extension IS NULL)";
		}
		$smarty->assign("q_sch",$q_sch_get);
	}


	// 상품에 따른 그룹 코드 설정
	for ($arr_i = 1 ; $arr_i < count($product_list) ; $arr_i++ ){
		if($product_list[$arr_i]['prd_no'] == $sch_prd_get){
			$sch_prd_g1_get = $product_list[$arr_i]['g1_code'];
			$sch_prd_g2_get = $product_list[$arr_i]['g2_code'];
			break;
		}
	}

	$smarty->assign("sch_prd_g1",$sch_prd_g1_get);
	$smarty->assign("sch_prd_g2",$sch_prd_g2_get);



	if(!empty($sch_prd_get)) { // 상품
		$add_where.=" AND w.prd_no='".$sch_prd_get."'";
	}else{
		if($session_staff_state == '1'){ // active 계정
			if($sch_prd_g2_get){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
				$add_where.=" AND (SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)='".$sch_prd_g2_get."'";
			}elseif($sch_prd_g1_get){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
				$add_where.=" AND (SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))='".$sch_prd_g1_get."'";
			}
		}elseif($session_staff_state == '2'){ // freelancer 계정
			$add_or_where="";
			for($i=0 ; $i < sizeof($access_accept_list) ; $i++){ // 접근허용 리스트
				if($session_s_no == $access_accept_list[$i][1]){
					if(!!$add_or_where){
						$add_or_where.=" OR w.prd_no='".$access_accept_list[$i][2]."'";
					}else{
						$add_or_where.=" w.prd_no='".$access_accept_list[$i][2]."'";
					}
				}
			}
			if(!!$add_or_where)
				$add_where.=" AND (".$add_or_where.")";
		}
	}
	$smarty->assign("sch_prd",$sch_prd_get); // 전체 업무내역에 자주하는 업무가 나타게 하기위해 empty 밖에서 처리함

	if(!empty($sch_task_run_regdate_get)) { // 업무완료일
		$add_where.=" AND w.task_run_regdate like '".$sch_task_run_regdate_get."%'";
		$smarty->assign("sch_task_run_regdate",$sch_task_run_regdate_get);
	}

	if(!empty($sch_work_state_get)&&empty($q_sch_get)) { // 진행상태 [퀵서치를 안한경우]
		$add_where.=" AND w.work_state='".$sch_work_state_get."'";
		$smarty->assign("sch_work_state",$sch_work_state_get);
	}


	if($sch_dp_no_get == 'null') { // dp_no is null
		$add_where.=" AND w.dp_no is null";
		$smarty->assign("sch_dp_no_get",$sch_dp_no_get);
	}

	if($sch_wd_no_get == 'null') { // wd_no is null
		$add_where.=" AND w.wd_no is null";
		$smarty->assign("sch_wd_no_get",$sch_wd_no_get);
	}

	if(!empty($sch_dp_c_no_get)) { // 입금업체(입금처)
		if($sch_dp_c_no_get != "null"){
			$add_where.=" AND w.dp_c_no='".$sch_dp_c_no_get."'";
		}else{
			$add_where.=" AND (w.dp_c_no IS NULL OR w.dp_c_no ='0')";
		}

		$smarty->assign("sch_dp_c_no",$sch_dp_c_no_get);
	}

	// 외주 작업자(지급처)
	$add_where.=" AND w.wd_c_no='".$sch_wd_c_no_get."'";
	$smarty->assign("sch_wd_c_no",$sch_wd_c_no_get);

	if(!empty($sch_wd_pay_date_get)) { // 지급일
		$add_where.=" AND (SELECT wd_date FROM withdraw wd where wd.wd_no=w.wd_no) = '".$sch_wd_pay_date_get."'";
		$smarty->assign("sch_wd_pay_date",$sch_wd_pay_date_get);
	}

	if(!empty($sch_c_name_get)) { // 업체명
		$add_where.=" AND w.c_name like '%".$sch_c_name_get."%'";
		$smarty->assign("sch_c_name",$sch_c_name_get);
	}
	if(!empty($sch_t_keyword_get)) { // 타겟키워드 및 타겟채널
		$add_where.=" AND w.t_keyword like '%".$sch_t_keyword_get."%'";
		$smarty->assign("sch_t_keyword",$sch_t_keyword_get);
	}
	if(!empty($sch_r_keyword_get)) { // 노출키워드
		$add_where.=" AND w.r_keyword like '%".$sch_r_keyword_get."%'";
		$smarty->assign("sch_r_keyword",$sch_r_keyword_get);
	}
	if(!empty($sch_w_no_get)) { // 업무번호
		$add_where.=" AND w.w_no ='".$sch_w_no_get."'";
		$smarty->assign("sch_w_no",$sch_w_no_get);
	}

	// 상품 구분 가져오기(1차)
	$prd_g1_sql="
	    SELECT
	        k_name,k_name_code
	    FROM kind
	    WHERE
	        k_code='product' AND (k_parent='0' OR k_parent is null)
	    ORDER BY
	        priority ASC
	    ";
	$prd_g1_result=mysqli_query($my_db,$prd_g1_sql);

	while($prd_g1=mysqli_fetch_array($prd_g1_result)) {
		$prd_g1_list[]=array(
			"k_name"=>trim($prd_g1['k_name']),
			"k_name_code"=>trim($prd_g1['k_name_code'])
		);
	}
	$smarty->assign("prd_g1_list",$prd_g1_list);


	// 상품 구분 가져오기(2차)
	if(empty($sch_prd_get) && $sch_prd_g2_get){ // 상품 선택 없이 상품 그룹2로 검색시 그룹2 가져오기
		$prd_g2_sql="
		    SELECT
		        k_name,k_name_code,k_parent
		    FROM kind
		    WHERE
		        k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code='".$sch_prd_g2_get."')
		    ORDER BY
		        priority ASC
		    ";
	}elseif(empty($sch_prd_get) && $sch_prd_g1_get){ // 상품 선택 없이 상품 그룹1로 검색시 그룹2 가져오기
		$prd_g2_sql="
				SELECT
						k_name,k_name_code,k_parent
				FROM kind
				WHERE
						k_code='product' AND k_parent='".$sch_prd_g1_get."'
				ORDER BY
						priority ASC
				";
	}else{ // 그 외 상품에 따른 그룹2 가져오기
		$prd_g2_sql="
		    SELECT
		        k_name,k_name_code,k_parent
		    FROM kind
		    WHERE
		        k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='".$sch_prd_get."'))
		    ORDER BY
		        priority ASC
		    ";
	}

	$prd_g2_result=mysqli_query($my_db,$prd_g2_sql);
	while($prd_g2=mysqli_fetch_array($prd_g2_result)) {
		$prd_g2_list[]=array(
			"k_name"=>trim($prd_g2['k_name']),
			"k_name_code"=>trim($prd_g2['k_name_code'])
		);
	}
	$smarty->assign("prd_g2_list",$prd_g2_list);


	// 상품 목록 가져오기
	if(empty($sch_prd_get) && $sch_prd_g2_get){ // 상품 선택 없이 상품 그룹2로 검색시 상품 가져오기
		$prd_sql="
		    SELECT
		        title,prd_no,k_name_code
		    FROM product
		    WHERE
		        k_name_code='".$sch_prd_g2_get."'
		    ORDER BY
		        priority ASC
		    ";
	}else{
		$prd_sql="
		    SELECT
		        title,prd_no,k_name_code
		    FROM product
		    WHERE
		        k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='".$sch_prd_get."')
		    ORDER BY
		        priority ASC
		    ";
	}

	$prd_result=mysqli_query($my_db,$prd_sql);
	while($prd=mysqli_fetch_array($prd_result)) {
		$sch_prd_list[]=array(
	      "w_no"=>trim($work_array['w_no']),
				"title"=>trim($prd['title']),
				"prd_no"=>trim($prd['prd_no']),
				"k_name_code"=>trim($prd['k_name_code'])
		);
	}
	$smarty->assign("sch_prd_list",$sch_prd_list);


	// 상품정보 - 업무요청 내용, 입/출금 설정 가져오기 (1:미설정, 2:입금, 3:출금, 4:입금+출금) && work time mehtod 가져오기 (0:미사용, 1:사용(기본값 변경불가), 2:사용(기본값 변경가능)) && work time 기본값 가져오기
	$prd_info_sql = "SELECT
												prd.kind,
												prd.work_format,
												prd.equally_vat,
												prd.wd_dp_state
										FROM product prd
										WHERE prd.prd_no = '{$sch_prd_get}'
									";
	$prd_info_result = mysqli_query($my_db, $prd_info_sql);

	if(!!$prd_info_result)
		$product_data = mysqli_fetch_array($prd_info_result);

	$work_kind = $product_data['kind'];
	$work_format = $product_data['work_format'];
	$work_equally_vat = $product_data['equally_vat'];

	$smarty->assign("work_kind", $work_kind);
	$smarty->assign("work_format", $work_format);
	$smarty->assign("work_equally_vat", $work_equally_vat);

	$smarty->assign("wd_dp_state", $product_data['wd_dp_state']);


	// 정렬순서
	$add_orderby.=" w_no desc ";

	// 페이지에 따른 limit 설정
	$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
	$smarty->assign("page",$pages);

	// 페이지당 리스트수
	$od_list_num = isset($_GET['od_list_num']) ?intval($_GET['od_list_num']) : 10;
	$smarty->assign("od_list_num",$od_list_num);
	$num = $od_list_num;
	$offset = ($pages-1) * $num;


	// 리스트 쿼리
	$work_sql="
		SELECT
			*,
			$add_select
			(SELECT k_name FROM kind WHERE k_name_code=w.k_name_code) AS k_name,
			(SELECT regdate FROM withdraw wd where wd.wd_no=w.wd_no) as wd_regdate,
			(SELECT wd_date FROM withdraw wd where wd.wd_no=w.wd_no) as wd_date,
			(SELECT regdate FROM deposit dp where dp.dp_no=w.dp_no) as dp_regdate,
			(SELECT dp_date FROM deposit dp where dp.dp_no=w.dp_no) as dp_date,
			(SELECT s_name FROM staff s where s.s_no=w.task_req_s_no) as task_req_s_name,
			(SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name,
			(SELECT staff_state FROM staff s where s.s_no=w.task_run_s_no) as task_run_staff_state,
			(SELECT s_name from staff s where s.s_no=w.s_no) as s_no_name,
			(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1_name,
			(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2_name,
			(SELECT title from product prd where prd.prd_no=w.prd_no) as prd_no_name,
			(SELECT prd.wd_dp_state FROM product prd	WHERE prd.prd_no = w.prd_no) as wd_dp_state
		FROM
			work w
		WHERE
			$add_where
		ORDER BY
			$add_orderby
		";

	//echo $work_sql; //exit;
	$smarty->assign("query",$work_sql);

	// 전체 게시물 수
	$cnt_sql = "SELECT count(*) FROM (".$work_sql.") AS cnt";
	$cnt_query= mysqli_query($my_db, $cnt_sql);
	if(!!$cnt_query)
		$cnt_data = mysqli_fetch_array($cnt_query);
	$total_num = $cnt_data[0];

	$smarty->assign(array(
		"total_num"=>number_format($total_num)
	));

	$smarty->assign("total_num",$total_num);
	$pagenum = ceil($total_num/$num);


	// 페이징
	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	// 검색 조건
	$search_url = "sch_prd_g1=".$sch_prd_g1_get.
								"&sch_prd_g2=".$sch_prd_g2_get.
								"&sch_prd=".$sch_prd_get.
								"&q_sch=".$q_sch_get.
								"&sch_w_no=".$sch_w_no_get.
								"&sch_task_run_regdate=".$sch_task_run_regdate_get.
								"&sch_work_state=".$sch_work_state_get.
								"&sch_wd_no=".$sch_wd_no_get.
								"&sch_wd_c_no=".$sch_wd_c_no_get.
								"&sch_dp_no=".$sch_dp_no_get.
								"&sch_dp_c_no=".$sch_dp_c_no_get.
								"&sch_c_name=".$sch_c_name_get.
								"&sch_t_keyword=".$sch_t_keyword_get.
								"&sch_r_keyword=".$sch_r_keyword_get.
								"&od_list_num=".$od_list_num;

	$smarty->assign("search_url",$search_url);
	$page=pagelist($pages, "work_list.php", $pagenum, $search_url);
	$smarty->assign("pagelist",$page);

	// 리스트 쿼리 결과(데이터)
	$work_sql .= "LIMIT $offset,$num";
	$result= mysqli_query($my_db, $work_sql);
	if(!!$result)
	while($work_array = mysqli_fetch_array($result)){

		if(!empty($work_array['regdate'])) {
			$regdate_day_value = date("Y/m/d",strtotime($work_array['regdate']));
			$regdate_time_value = date("H:i",strtotime($work_array['regdate']));
		}else{
			$regdate_day_value="";
			$regdate_time_value="";
		}

		if(!empty($work_array['wd_price'])){
			$wd_price_value=number_format($work_array['wd_price']);
		}else{
			$wd_price_value="";
		}
		if(!empty($work_array['wd_price_vat'])){
			$wd_price_vat_value=number_format($work_array['wd_price_vat']);
		}else{
			$wd_price_vat_value="";
		}
		if(!empty($work_array['wd_regdate'])) {
			$wd_regdate_day_value = date("Y/m/d",strtotime($work_array['wd_regdate']));
			$wd_regdate_time_value = date("H:i",strtotime($work_array['wd_regdate']));
		}else{
			$wd_regdate_day_value="";
			$wd_regdate_time_value="";
		}

		if(!empty($work_array['dp_price'])){
			$dp_price_value=number_format($work_array['dp_price']);
		}else{
			$dp_price_value="";
		}
		if(!empty($work_array['dp_price_vat'])){
			$dp_price_vat_value=number_format($work_array['dp_price_vat']);
		}else{
			$dp_price_vat_value="";
		}
		if(!empty($work_array['dp_regdate'])) {
			$dp_regdate_day_value = date("Y/m/d",strtotime($work_array['dp_regdate']));
			$dp_regdate_time_value = date("H:i",strtotime($work_array['dp_regdate']));
		}else{
			$dp_regdate_day_value="";
			$dp_regdate_time_value="";
		}

		// 입금업체 이름 찾기
		$dp_c_name2 = "";
		for ($arr_i = 1 ; $arr_i < count($dp_company_list) ; $arr_i++ ){
			if($work_array['dp_c_no'] == $dp_company_list[$arr_i][1] && $work_array['prd_no'] == $dp_company_list[$arr_i][2]){
				$dp_c_name2 = $dp_company_list[$arr_i][3];
			}
		}

		// 외주 작업자 이름 찾기
		$wd_c_no_name2 = "";
		for ($arr_i = 1 ; $arr_i < count($wd_company_list) ; $arr_i++ ){
			if($work_array['wd_c_no'] == $wd_company_list[$arr_i][1] && $work_array['prd_no'] == $wd_company_list[$arr_i][2]){
				$wd_c_no_name2 = $wd_company_list[$arr_i][3];
			}
		}


		// 입금업체 리스트에 존재여부 확인
		$is_dp_company_list=false;
		for($arr_i = 0 ; $arr_i < count($dp_company_list) ; $arr_i++) {
			if($dp_company_list[$arr_i][1] == $work_array['dp_c_no']){
				if($dp_company_list[$arr_i][2] == $work_array['prd_no']){
					$is_dp_company_list=true;
				}
			}
		}


		$work_lists[] = array(
			"w_no"=>$work_array['w_no'],
			"task_run_regdate"=>isset($work_array['task_run_regdate'])?date("Y-m-d",strtotime($work_array['task_run_regdate'])):"",
			"work_state"=>$work_array['work_state'],
			"wd_regdate_day"=>$wd_regdate_day_value,
			"wd_regdate_time"=>$wd_regdate_time_value,
			"dp_regdate_day"=>$dp_regdate_day_value,
			"dp_regdate_time"=>$dp_regdate_time_value,
			"c_name"=>$work_array['c_name'],
			"s_no"=>$work_array['s_no'],
			"s_no_name"=>$work_array['s_no_name'],
			"k_prd1"=>$work_array['k_prd1'],
			"k_prd1_name"=>$work_array['k_prd1_name'],
			"k_prd2"=>$work_array['k_prd2'],
			"k_prd2_name"=>$work_array['k_prd2_name'],
			"prd_no"=>$work_array['prd_no'],
			"prd_no_name"=>$work_array['prd_no_name'],
			"wd_dp_state"=>$work_array['wd_dp_state'],
			"t_keyword"=>$work_array['t_keyword'],
			"k_name_code"=>$work_array['k_name_code'],
			"k_name"=>$work_array['k_name'],
			"r_keyword"=>$work_array['r_keyword'],
			"set_title"=>$work_array['set_title'],
			"set_tag_count"=>$work_array['set_tag_count'],
			"task_req"=>htmlspecialchars($work_array['task_req']),
			"task_req_s_no"=>$work_array['task_req_s_no'],
			"task_req_dday"=>$work_array['task_req_dday'],
			"task_req_s_name"=>$work_array['task_req_s_name'],
			"task_req_file_origin"=>htmlspecialchars($work_array['task_req_file_origin']),
			"task_run"=>$work_array['task_run'],
			"task_run_dday"=>$work_array['task_run_dday'],
			"task_run_s_no"=>$work_array['task_run_s_no'],
			"is_task_run_staff_list"=>$is_task_run_staff_list,
			"task_run_regdate_day"=>isset($work_array['task_run_regdate'])?date("Y/m/d",strtotime($work_array['task_run_regdate'])):"",
			"task_run_regdate_time"=>isset($work_array['task_run_regdate'])?date("H:i",strtotime($work_array['task_run_regdate'])):"",
			"task_run_s_name"=>$work_array['task_run_s_name'],
			"task_run_file_origin"=>htmlspecialchars($work_array['task_run_file_origin']),
			"dp_price"=>$dp_price_value,
			"dp_price_vat"=>$dp_price_vat_value,
			"dp_c_no"=>$work_array['dp_c_no'],
			"dp_c_name"=>$work_array['dp_c_name'],
			"is_dp_company_list"=>$is_dp_company_list,
			"dp_c_name2"=>$dp_c_name2,
			"dp_no"=>$work_array['dp_no'],
			"wd_price"=>$wd_price_value,
			"wd_price_vat"=>$wd_price_vat_value,
			"wd_c_no"=>$work_array['wd_c_no'],
			"wd_c_name"=>$work_array['wd_c_name'],
			"wd_c_no_name2"=>$wd_c_no_name2,
			"wd_no"=>$work_array['wd_no'],
			"wd_pay_date"=>isset($work_array['wd_date'])?date("Y-m-d",strtotime($work_array['wd_date'])):"",
			"dp_pay_date"=>isset($work_array['dp_date'])?date("Y-m-d",strtotime($work_array['dp_date'])):"",
			"regdate_day"=>$regdate_day_value,
			"regdate_time"=>$regdate_time_value
		);
		$smarty->assign(array(
			"work_list"=>$work_lists
		));
	}
	$smarty->display('out_company/work_list.html');
}

function image_check($files) {
	//카운트 수
	$count=count($files["name"]);

	//이미지 파일
	for ($i = 0; $i < $count; $i++) {
		$o_saveName = $files["name"];
		$tmp_filename = explode(".",$o_saveName[$i]);
		$ext=strtolower($tmp_filename[1]);

		//빈 파일 체크 if문 start
		if($o_saveName!="" || $o_saveName!=null)
		{
			 //파일 업로드시 에러
			/* if ($files["error"][$i] > 0)
			 {
				 $msg = "Error: " . $files["error"][$i] . "<br />";
				 $url = "history.go(-1)";
				 echo "<script language='javascript'>alert('$msg');$url;</script>";
				 exit;
			 }*/

			 //파일 이미지 체크
			 $img_check=false;
			 /*if (($files["type"][$i] == "image/gif")|| ($files["type"][$i] == "image/jpeg")|| ($files["type"][$i] == "image/pjpeg"))
			 {
				 $image_check=true;
			 }//if문*/

			if (($ext == "gif") || ($ext == "jpeg") || ($ext == "jpg") || ($etc == "pdf "))
			 {
				 $img_check=true;
			 }//if문

			 if ($img_check=false) {
					$url = "history.go(-1)";
					echo "<script language='javascript'>alert('확장자 올바르지 않습니다.');$url;</script>";
					exit;
			 }

			 //파일 사이즈 체크
			 if ($files["size"][$i] > 10*1024*1024) // webmatrix의 경우 다음 설정 확인 C:\Program Files (x86)\IIS Express\PHP\v5.5\php.ini / upload_max_filesize = 10M / post_max_size = 10M
			 {
					exit("<script>alert('10MB 이상 파일 업로드를 하실 수 없습니다.');location.href='work_list.php?{$_POST['search_url']}';</script>");
			 }

		 }//빈 파일 체크 if문 end
	}//for문
}//이미지 확인 함수 끝


function store_image($files, $folder) {
	//print_r($_POST);echo "<br>";
	$count=count($files["name"]);

	for ($i = 0; $i < $count; $i++) {

		$o_saveName = $files["name"];

		if($o_saveName[$i]){
			$tmp_filename = explode(".",$o_saveName[$i]);
			//print_r($ext);
			if ($tmp_filename[0] != '')
				$md5filename = md5(time().$tmp_filename[0]);
			else
				$md5filename = "";

			$ext=$tmp_filename[1];
			$r_saveName[] = $folder."/".$md5filename.".".$ext;

			move_uploaded_file($files["tmp_name"][$i], "uploads/". $r_saveName[$i] );
		}
	}

	return $r_saveName;
}

function task_req_dday_count_alert($task_req_dday_cnt, $prd_no, $value){
	if(!!$value)
		for($i = 0 ; $i < count($GLOBALS['task_req_dday_count_list']) ; $i++) {
			if($GLOBALS['task_req_dday_count_list'][$i][0] == $prd_no){
				if($GLOBALS['task_req_dday_count_list'][$i][1] <= $task_req_dday_cnt){
					echo "<script>alert('요청하신 희망완료일 {$value}에 {$task_req_dday_cnt}건이 요청중에 있습니다.\\n희망완료일이 몰릴 경우 스케줄조정이 불가피하므로\\n가능하다면 다른날로 변경 부탁드립니다.');</script>";
				}
			}
		}
}

?>
