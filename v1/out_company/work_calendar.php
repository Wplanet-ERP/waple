<?php
require('../inc/common.php');
require('../inc/data_state.php');
require('ckadmin.php');

$proc=(isset($_POST['process']))?$_POST['process']:"";
//$sch_get=(isset($_GET['sch']))?$_GET['sch']:"";


if ($proc == "f_task_run_regdate") { //업무완료일 자동저장

	/////////////////////////* fom action 처리 부분 *//////////////////////

} else {

	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where="1=1";
	$sch_prd_g1_get=isset($_GET['sch_prd_g1'])?$_GET['sch_prd_g1']:"";
	$sch_prd_g2_get=isset($_GET['sch_prd_g2'])?$_GET['sch_prd_g2']:"";
	$sch_prd_get=isset($_GET['sch_prd'])?$_GET['sch_prd']:"";
	$sch_date_type_get=isset($_GET['sch_date_type'])?$_GET['sch_date_type']:"1";
	$sch_view_type_get=isset($_GET['sch_view_type'])?$_GET['sch_view_type']:"4";
	$sch_wd_c_no_get=isset($_GET['sch_wd_c_no'])?$_GET['sch_wd_c_no']:$session_c_no;
	$sch_run_c_no_get=$session_c_no;

	// 달력 정보 가져오기 [START]
	$sch_date_get=isset($_GET['sch_date'])?$_GET['sch_date']:$today; // get 값이 없는 경우 오늘을 기준으로 함
	$smarty->assign("sch_date",$sch_date_get);

	$day_token=explode("-",$sch_date_get); // 들어온 날짜를 년,월,일로 분할해 변수로 저장합니다.
	$base_year=$day_token[0]; // 지정 년도
	$base_month=$day_token[1]; // 지정된 월
	$base_day=$day_token[2]; // 지정된 일

	$smarty->assign("base_year",$base_year);
	$smarty->assign("base_month",$base_month);
	$smarty->assign("base_day",$base_day);

	//echo $base_year."<br>";
	//echo $base_month."<br>";
	//echo $base_day."<br>";

	$day_length = date("t",mktime(0,0,0,$base_month,$base_day,$base_year)); // 지정된 달은 몇일까지 있을까요?
	$first_week = date("N",mktime(0,0,0,$base_month,1,$base_year)); // 지정된 달의 첫날은 무슨요일일까요? 1~7 월~일
	$first_empty_week = $first_week%7; // 지정된 달 1일 앞의 공백 숫자.

	$smarty->assign("day_length",$day_length);
	$smarty->assign("first_week",$first_week);
	$smarty->assign("first_empty_week",$first_empty_week);

	//echo $day_length."<br>";
	//echo $first_week."<br>";
	//echo $first_empty_week."<br>";

	$week_length=($day_length+$first_empty_week)/7; $week_length=ceil($week_length); $week_length=$week_length; // 지정된 달은 총 몇주로 라인을 그어야 하나?

	$smarty->assign("week_length",$week_length);

	//echo $week_length."<br>";

	// 날짜와 요일 정보 담기
	$date_i = 1;
	for($i = 1 ; $i <= $first_empty_week ; $i++){
		$calendar_date[]=array(
				'day'=>'',
				'date'=>'',
				'week'=>''
		);
	}
	while($date_i <= $day_length) {
		if($date_i < 10)
			$cal_day = '0'.$date_i;
		else {
			$cal_day = $date_i;
		}
		$calendar_date[]=array(
				'day'=>$cal_day,
				'date'=>date('Y-m-d', strtotime($base_year.'-'.$base_month.'-'.$date_i)),
				'week'=>date('w', strtotime($base_year.'-'.$base_month.'-'.$date_i))
		);
		$date_i++;
	}
	//print_r($calendar_date);
	$smarty->assign("calendar_date",$calendar_date);


	$next_day= date("Y-m-d",mktime(0,0,0,$base_month,$base_day+1,$base_year)); // 다음날
	$prev_dday= date("Y-m-d",mktime(0,0,0,$base_month,$base_day-1,$base_year)); // 이전날
	$next_month= date("Y-m-d",mktime(0,0,0,$base_month+1,$base_day,$base_year)); // 다음달
	$prev_month= date("Y-m-d",mktime(0,0,0,$base_month-1,$base_day,$base_year)); // 지난달
	$next_year= date("Y-m-d",mktime(0,0,0,$base_month,$base_day,$base_year+1)); // 내년
	$prev_year= date("Y-m-d",mktime(0,0,0,$base_month,$base_day,$base_year-1)); // 작년

	$smarty->assign("next_day",$next_day);
	$smarty->assign("prev_dday",$prev_dday);
	$smarty->assign("next_month",$next_month);
	$smarty->assign("prev_month",$prev_month);
	$smarty->assign("next_year",$next_year);
	$smarty->assign("prev_year",$prev_year);

	//echo $next_day."<br>";
	//echo $prev_dday."<br>";
	//echo $next_month."<br>";
	//echo $prev_month."<br>";
	//echo $next_year."<br>";
	//echo $prev_year."<br>";
	// 달력 정보 가져오기 [END]



	// 상품에 따른 그룹 코드 설정
	for ($arr_i = 1 ; $arr_i < count($product_list) ; $arr_i++ ){
		if($product_list[$arr_i]['prd_no'] == $sch_prd_get){
			$sch_prd_g1_get = $product_list[$arr_i]['g1_code'];
			$sch_prd_g2_get = $product_list[$arr_i]['g2_code'];
			break;
		}
	}

	$smarty->assign("sch_prd_g1",$sch_prd_g1_get);
	$smarty->assign("sch_prd_g2",$sch_prd_g2_get);



	if(!empty($sch_wd_c_no_get)) { // 외주 작업자
		if($sch_wd_c_no_get != "all"){
			$add_where.=" AND w.wd_c_no='".$sch_wd_c_no_get."'";
		}
		$smarty->assign("sch_wd_c_no",$sch_wd_c_no_get);
	}

	if(!empty($sch_prd_get)) { // 상품
		$add_where.=" AND w.prd_no='".$sch_prd_get."'";
	}else{
		if($sch_prd_g2_get){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
			$add_where.=" AND (SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)='".$sch_prd_g2_get."'";
		}elseif($sch_prd_g1_get){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
			$add_where.=" AND (SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))='".$sch_prd_g1_get."'";
		}
	}
	$smarty->assign("sch_prd",$sch_prd_get); // 전체 업무내역에 자주하는 업무가 나타게 하기위해 empty 밖에서 처리함

	if(!empty($sch_date_type_get)) { // 조회일정
		$where_s_date = $base_year."-".$base_month."-01"; // where 조건 지정 월 시작날짜
		$where_e_date = date("Y-m", strtotime("+1 month", strtotime($where_s_date))); // where 조건 지정 월 시작날짜의 다음달

		if($sch_date_type_get == '1'){ // 예정 완료일
			$add_where.=" AND DATE_FORMAT(w.task_run_dday, '%Y-%m-%d') BETWEEN '$where_s_date' AND '$where_e_date' ";
		}elseif($sch_date_type_get == '2'){ // 업무 완료일
			$add_where.=" AND DATE_FORMAT(w.task_run_regdate, '%Y-%m-%d') BETWEEN '$where_s_date' AND '$where_e_date' ";
		}elseif($sch_date_type_get == '3'){ // 희망 완료일
			$add_where.=" AND DATE_FORMAT(w.task_req_dday, '%Y-%m-%d') BETWEEN '$where_s_date' AND '$where_e_date' ";
		}

		$smarty->assign("sch_date_type",$sch_date_type_get);
	}

	$smarty->assign("sch_view_type",$sch_view_type_get);


	// 상품 구분 가져오기(1차)
	$prd_g1_sql="
	    SELECT
	        k_name,k_name_code
	    FROM kind
	    WHERE
	        k_code='product' AND (k_parent='0' OR k_parent is null)
	    ORDER BY
	        priority ASC
	    ";
	$prd_g1_result=mysqli_query($my_db,$prd_g1_sql);

	while($prd_g1=mysqli_fetch_array($prd_g1_result)) {
		$prd_g1_list[]=array(
			"k_name"=>trim($prd_g1['k_name']),
			"k_name_code"=>trim($prd_g1['k_name_code'])
		);
	}
	$smarty->assign("prd_g1_list",$prd_g1_list);


	// 상품 구분 가져오기(2차)
	if(empty($sch_prd_get) && $sch_prd_g2_get){ // 상품 선택 없이 상품 그룹2로 검색시 그룹2 가져오기
		$prd_g2_sql="
		    SELECT
		        k_name,k_name_code,k_parent
		    FROM kind
		    WHERE
		        k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code='".$sch_prd_g2_get."')
		    ORDER BY
		        priority ASC
		    ";
	}elseif(empty($sch_prd_get) && $sch_prd_g1_get){ // 상품 선택 없이 상품 그룹1로 검색시 그룹2 가져오기
		$prd_g2_sql="
				SELECT
						k_name,k_name_code,k_parent
				FROM kind
				WHERE
						k_code='product' AND k_parent='".$sch_prd_g1_get."'
				ORDER BY
						priority ASC
				";
	}else{ // 그 외 상품에 따른 그룹2 가져오기
		$prd_g2_sql="
		    SELECT
		        k_name,k_name_code,k_parent
		    FROM kind
		    WHERE
		        k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='".$sch_prd_get."'))
		    ORDER BY
		        priority ASC
		    ";
	}

	$prd_g2_result=mysqli_query($my_db,$prd_g2_sql);
	while($prd_g2=mysqli_fetch_array($prd_g2_result)) {
		$prd_g2_list[]=array(
			"k_name"=>trim($prd_g2['k_name']),
			"k_name_code"=>trim($prd_g2['k_name_code'])
		);
	}
	$smarty->assign("prd_g2_list",$prd_g2_list);

	// 상품 목록 가져오기
	if(empty($sch_prd_get) && $sch_prd_g2_get){ // 상품 선택 없이 상품 그룹2로 검색시 상품 가져오기
		$prd_sql="
		    SELECT
		        title,prd_no,k_name_code
		    FROM product
		    WHERE
		        k_name_code='".$sch_prd_g2_get."'
		    ORDER BY
		        priority ASC
		    ";
	}else{
		$prd_sql="
		    SELECT
		        title,prd_no,k_name_code
		    FROM product
		    WHERE
		        k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='".$sch_prd_get."')
		    ORDER BY
		        priority ASC
		    ";
	}

	$prd_result=mysqli_query($my_db,$prd_sql);
	while($prd=mysqli_fetch_array($prd_result)) {
		$sch_prd_list[]=array(
			"w_no"=>trim($work_array['w_no']),
			"title"=>trim($prd['title']),
			"prd_no"=>trim($prd['prd_no']),
			"k_name_code"=>trim($prd['k_name_code'])
		);
	}
	$smarty->assign("sch_prd_list",$sch_prd_list);


	// 정렬순서 토글 & 필드 지정
	$add_orderby="";

	if($q_sch_get == "5"){ // 퀵서치의 연장여부 확인 요청건은 \연장여부 마감일순으로 정렬
		$add_orderby.=" extension_date asc";
	}else{
		//$add_orderby.=" w_no desc";
		$add_orderby.=" regdate desc";
	}

	// 리스트 쿼리
	$work_sql="
			SELECT "
			."w_no, " // w_no
			."work_state, " // 진행상태
			."task_req_dday, " // 희망 완료일
			."task_run_dday, " // 예정 완료일
			."task_run_regdate, " // 업무 완료일
			."c_name, " // 업체명
			."s_no, " // 마케팅 담당자
			."(SELECT s_name FROM staff s where s.s_no=w.s_no) as s_name, " // 마케팅 담당자
			."prd_no, " // 상품명
			."(SELECT title from product prd where prd.prd_no=w.prd_no) as prd_no_name, " // 상품명
			."t_keyword, " // 타겟키워드
			."task_run_s_no, " // 업무처리 담당자
			."(SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name " // 업무처리 담당자
			."
		FROM
			work w
		WHERE
			$add_where
			AND (w.work_state = '3' OR w.work_state = '4' OR w.work_state = '5' OR w.work_state = '6')
		ORDER BY
			$add_orderby
		";

	//echo $work_sql;// exit;
	$smarty->assign("query",$work_sql);

	// 전체 게시물 수
	$cnt_sql = "SELECT count(*) FROM (".$work_sql.") AS cnt";
	$cnt_query= mysqli_query($my_db, $cnt_sql);
	$cnt_data=mysqli_fetch_array($cnt_query);
	$total_num = $cnt_data[0];

	$smarty->assign(array(
		"total_num"=>number_format($total_num)
	));

	$smarty->assign("total_num",$total_num);

	// 검색 조건
	$search_url = "sch_prd_g1=".$sch_prd_g1_get.
								"&sch_prd_g2=".$sch_prd_g2_get.
								"&sch_prd=".$sch_prd_get.
								"&sch_date_type=".$sch_date_type_get.
								"&sch_view_type=".$sch_view_type_get.
								"&sch_date=".$sch_date_get.
								"&sch_wd_c_no=".$sch_wd_c_no_get.
								"&sch_task_req=".$sch_task_req_get.
								"&sch_task_run=".$sch_task_run_get;

	$smarty->assign("search_url",$search_url);

	// 리스트 쿼리 결과(데이터)
	$result= mysqli_query($my_db, $work_sql);
	while($work_array = mysqli_fetch_array($result)){

		if(!empty($work_array['price'])){
			$price_value=number_format($work_array['price']);
		}else{
			$price_value="";
		}
		if(!empty($work_array['price_vat'])){
			$price_vat_value=number_format($work_array['price_vat']);
		}else{
			$price_vat_value="";
		}
		if(!empty($work_array['regdate'])) {
			$regdate_day_value = date("Y/m/d",strtotime($work_array['regdate']));
			$regdate_time_value = date("H:i",strtotime($work_array['regdate']));
		}else{
			$regdate_day_value="";
			$regdate_time_value="";
		}


		$req_run_intval = "";

		if(!empty($work_array['task_run_regdate'])){
			$task_run_regdate_val = date("Y-m-d",strtotime($work_array['task_run_regdate']));
		}else{
			$task_run_regdate_val = "";
		}

		//예정 완료일 - 희망 완료일
		if($sch_date_type_get == '1' && $work_array['task_run_dday'] && $work_array['task_req_dday']){
			$s_day = date("Y-m-d", strtotime($work_array['task_run_dday']));
			$e_day = date("Y-m-d", strtotime($work_array['task_req_dday']));

			$req_run_intval = intval((strtotime($s_day) - strtotime($e_day)) / 86400);
		//업무 완료일 - 희망 완료일
		}else if($sch_date_type_get == '2' && $work_array['task_run_regdate'] && $work_array['task_req_dday']){
			$s_day = date("Y-m-d", strtotime($work_array['task_run_regdate']));
			$e_day = date("Y-m-d", strtotime($work_array['task_req_dday']));

			$req_run_intval = intval((strtotime($s_day) - strtotime($e_day)) / 86400);
		}

		$work_lists[] = array(
			"w_no"=>$work_array['w_no'],
			"work_state"=>$work_array['work_state'],
			"task_req_dday"=>$work_array['task_req_dday'],
			"task_run_dday"=>$work_array['task_run_dday'],
			"task_run_regdate"=>$task_run_regdate_val,
			"req_run_intval"=>$req_run_intval,
			"c_name"=>$work_array['c_name'],
			"s_no"=>$work_array['s_no'],
			"s_name"=>$work_array['s_name'],
			"prd_no"=>$work_array['prd_no'],
			"prd_no_name"=>$work_array['prd_no_name'],
			"t_keyword"=>is_null($work_array['t_keyword']) ? "Empty data" : ($work_array['t_keyword'] != "" ? $work_array['t_keyword'] : "Empty data"),
			"task_run_s_no"=>is_null($work_array['task_run_s_no']) ? "Empty data" : ($work_array['task_run_s_no'] != "" ? $work_array['task_run_s_no'] : "Empty data"),
			"task_run_s_name"=>is_null($work_array['task_run_s_name']) ? "Empty data" : ($work_array['task_run_s_name'] != "" ? $work_array['task_run_s_name'] : "Empty data")
		);
	}

	$smarty->assign(array(
		"work_list"=>$work_lists
	));
	$smarty->display('out_company/work_calendar.html');
}

?>
