<?php
require('../inc/common.php');
require('../inc/data_state.php');
require('ckadmin.php');

// 접근 권한
/*
if (!$session_c_no){
	$smarty->display('access_error.html');
	exit;
}
*/
$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

$proc=(isset($_POST['process']))?$_POST['process']:"";




/* # 외주 작업자 000님의 진행중인 업무내역 # [Start]*/
$add_where = " 1 = 1";

$add_where .= " AND w.wd_c_no = '" . $session_c_no . "' "; // 외주 작업자
$add_where .= " AND (w.work_state = '4' OR w.work_state = '5') "; // 진행상태 :: 접수완료, 진행상태 :: 진행중
$add_where .= " AND (w.task_req_dday <= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL 4 DAY), '%Y-%m-%d') OR w.task_req_dday IS NULL OR w.task_req_dday = '')
							"; // 희망완료일 4일 이내

$add_orderby = "w.task_req_dday ASC";

// 업무 리스트 쿼리
$work_task_run_sql = "
							SELECT
								w.w_no,
								w.work_state,
								w.c_name,
								w.prd_no,
								(SELECT title from product prd where prd.prd_no=w.prd_no) as prd_name,
								w.t_keyword,
								w.wd_c_name,
								w.task_req_dday
							FROM work w
							WHERE $add_where
							ORDER BY $add_orderby
							";

// 업무 리스트 쿼리 결과(데이터)
//echo "$work_task_run_sql<br>"; //exit;

$result= mysqli_query($my_db, $work_task_run_sql);

while($work_task_run_array = mysqli_fetch_array($result)){

	// 희망완료일 D-Day
	$awd_date_d_day = "";
	$task_req_typeB = "";
	if($work_task_run_array['task_req_dday'])
		$task_req_d_day = intval((strtotime(date("Y-m-d",time()))-strtotime($work_task_run_array['task_req_dday'])) / 86400);

	if(!!$work_task_run_array['task_req_dday'])
		$task_req_typeB = date("m/d",strtotime($work_task_run_array['task_req_dday']));

	$work_task_run[] = array(
		"w_no"=>$work_task_run_array['w_no'],
		"work_state"=>$work_task_run_array['work_state'],
		"c_name"=>$work_task_run_array['c_name'],
		"prd_no"=>$work_task_run_array['prd_no'],
		"prd_name"=>$work_task_run_array['prd_name'],
		"t_keyword"=>$work_task_run_array['t_keyword'],
		"wd_c_name"=>$work_task_run_array['wd_c_name'],
		"task_req"=>$task_req_typeB,
		"task_req_d_day"=>$task_req_d_day
	);
}
$smarty->assign("work_task_run", $work_task_run);
/* # 외주 작업자 0000님의 진행중인 업무내역 # [End]*/

$smarty->display('out_company/main.html');
?>
