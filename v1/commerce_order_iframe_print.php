<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_price.php');

# 발주서 발주내역
$no = isset($_GET['no']) ? $_GET['no'] : "";

$commerce_order_set_sql="
		SELECT
			cos.no,
			cos.state,
		    cos.my_c_no,
            cos.sup_c_no,
            cos.sup_loc_type,
		    cos.req_s_no,
			cos.req_date,
            cos.sup_c_name,
		    cos.order_count,
		    cos.tel,
            cos.fax,
		    cos.reference,
		    cos.delivery_date,
		    cos.pay_method,
		    cos.sup_location,
		    cos.remark
		FROM commerce_order_set cos
		WHERE cos.no='{$no}'
";
$commerce_order_set_query = mysqli_query($my_db, $commerce_order_set_sql);
$commerce_order_set       = mysqli_fetch_array($commerce_order_set_query);

$commerce_order_set['is_new'] = false;
if($commerce_order_set['req_date'] >= '2025-01-01'){
    $commerce_order_set['is_new'] = true;
}

$comm_my_company_sql    = "SELECT * FROM my_company WHERE my_c_no='{$commerce_order_set['my_c_no']}'";
$comm_my_company_query  = mysqli_query($my_db, $comm_my_company_sql);
$comm_my_company        = mysqli_fetch_assoc($comm_my_company_query);

$comm_company_sql       = "SELECT * FROM company WHERE c_no='{$commerce_order_set['sup_c_no']}'";
$comm_company_query     = mysqli_query($my_db, $comm_company_sql);
$comm_company           = mysqli_fetch_assoc($comm_company_query);
$comm_company['tel']    = !empty($commerce_order_set['tel']) ? $commerce_order_set['tel'] : str_replace("없음--","",$comm_company['tel']);
$comm_company['fax']    = !empty($commerce_order_set['fax']) ? $commerce_order_set['fax'] : str_replace("--","", $comm_company['fax']);

$comm_staff_sql         = "SELECT * FROM staff WHERE s_no='{$commerce_order_set['req_s_no']}'";
$comm_staff_query       = mysqli_query($my_db, $comm_staff_sql);
$comm_staff             = mysqli_fetch_assoc($comm_staff_query);

# 발주서 발주내역 쿼리
$commerce_order_sql = "
    SELECT
        co.no,
        co.priority,
        co.stock_no,
        co.option_no,
        (SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_name,
        (SELECT pcu.priority FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_priority,
        co.quantity,
        co.unit_price,
        co.supply_price,
        co.vat,
        co.memo,
        co.sup_date,
        co.limit_date
    FROM commerce_order co
    WHERE co.set_no='{$no}' AND co.type='request'
    ORDER BY co.priority ASC, co.`no` ASC
";
$commerce_order_query   = mysqli_query($my_db, $commerce_order_sql);
$commerce_order_list    = [];
$commerce_is_usd        = ($commerce_order_set['sup_loc_type'] == '2') ? true : false;
$commerce_price_type    = ($commerce_is_usd) ? "$" : "₩";

$request_price_total        = 0;
$request_sup_price_total    = 0;
$request_vat_price_total    = 0;

while($commerce_order_result = mysqli_fetch_assoc($commerce_order_query))
{
    $request_price_total     += $commerce_order_result['supply_price'] + $commerce_order_result['vat'];
    $request_sup_price_total += $commerce_order_result['supply_price'];
    $request_vat_price_total += $commerce_order_result['vat'];

    $commerce_order_result['price']        = $commerce_order_result['supply_price'] + $commerce_order_result['vat'];
    $commerce_order_result['unit_price']   = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['unit_price']) : getNumberFormatPrice($commerce_order_result['unit_price']);
    $commerce_order_result['supply_price'] = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['supply_price']) : getNumberFormatPrice($commerce_order_result['supply_price']);
    $commerce_order_result['vat']          = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['vat']) : getNumberFormatPrice($commerce_order_result['vat']);
    $commerce_order_result['price']        = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['price']) : getNumberFormatPrice($commerce_order_result['price']);

    $commerce_order_list[] = $commerce_order_result;
}

$request_price_total     = ($commerce_is_usd) ? getUsdFormatPrice($request_price_total) : getNumberFormatPrice($request_price_total);
$request_sup_price_total = ($commerce_is_usd) ? getUsdFormatPrice($request_sup_price_total) : getNumberFormatPrice($request_sup_price_total);
$request_vat_price_total = ($commerce_is_usd) ? getUsdFormatPrice($request_vat_price_total) : getNumberFormatPrice($request_vat_price_total);

$smarty->assign("comm_my_company", $comm_my_company);
$smarty->assign("comm_company", $comm_company);
$smarty->assign("comm_staff", $comm_staff);
$smarty->assign("commerce_order_set", $commerce_order_set);
$smarty->assign("commerce_order_list", $commerce_order_list);
$smarty->assign("commerce_is_usd", $commerce_is_usd);
$smarty->assign("commerce_price_type", $commerce_price_type);
$smarty->assign("request_price_total", $request_price_total);
$smarty->assign("request_sup_price_total", $request_sup_price_total);
$smarty->assign("request_vat_price_total", $request_vat_price_total);

$smarty->display('commerce_order_iframe_print.html');

?>
