<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

# Create new PHPExcel object & SET document
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
	->setLastModifiedBy("Maarten Balliauw")
	->setTitle("Office 2007 XLSX Test Document")
	->setSubject("Office 2007 XLSX Test Document")
	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	->setKeywords("office 2007 openxml php")
	->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

# 상단타이틀 설정
$work_sheet			= $objPHPExcel->setActiveSheetIndex(0);
$header_title_list  = array("은행명", "계좌번호", "출금요청액(VAT/소득세포함)", "예금주", "보내는사람");
$head_idx           = 0;
foreach(range('A', 'E') as $columnID){
	$work_sheet->setCellValue("{$columnID}1", $header_title_list[$head_idx]);
	$head_idx++;
}

$is_search  = false;
if(permissionNameCheck($session_permission, "재무관리자") || permissionNameCheck($session_permission, "마스터관리자")
	|| ($session_team == '00222' || $session_team == '00244' || $session_team == '00211' || $session_team == '00221' || $session_s_no == '62'))
{
	$is_search = true;
}

$base_wd_method = 1;
if(permissionNameCheck($session_permission, "외주관리자")){
	$base_wd_method = "";
}

# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where 			= " 1=1 AND wd.my_c_no IN(1,3) AND (wd.bk_name != '' AND wd.bk_name IS NOT NULL) AND (wd.bk_num != '' AND wd.bk_num IS NOT NULL) AND wd.display='1' ";
$sch_wd_no 			= isset($_GET['sch_wd_no']) ? $_GET['sch_wd_no'] : "";
$sch_w_req_date 	= isset($_GET['sch_w_req_date']) ? $_GET['sch_w_req_date'] : "";
$sch_wd_state 		= isset($_GET['sch_wd_state']) ? $_GET['sch_wd_state'] : "";
$sch_wd_method 		= isset($_GET['sch_wd_method']) ? $_GET['sch_wd_method'] : $base_wd_method;
$sch_s_wd_date 		= isset($_GET['sch_s_wd_date']) ? $_GET['sch_s_wd_date'] : "";
$sch_e_wd_date 		= isset($_GET['sch_e_wd_date']) ? $_GET['sch_e_wd_date'] : "";
$sch_wd_tax_state 	= isset($_GET['sch_wd_tax_state']) ? $_GET['sch_wd_tax_state'] : "";
$sch_reg_s_no 		= isset($_GET['sch_reg_s_no']) ? $_GET['sch_reg_s_no'] : "";
$sch_wd_etc 		= isset($_GET['sch_wd_etc']) ? $_GET['sch_wd_etc'] : "";
$sch_my_c_no 		= isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_wd_account		= isset($_GET['sch_wd_account']) ? $_GET['sch_wd_account'] : "";
$sch_prd_no			= isset($_GET['sch_prd_no'])?$_GET['sch_prd_no']:"";
$sch_c_name 		= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_c_no 			= isset($_GET['sch_c_no'])?$_GET['sch_c_no']:"";
$sch_subject 		= isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
$sch_w_my_c_no 		= isset($_GET['sch_w_my_c_no']) ? $_GET['sch_w_my_c_no'] : "";
$sch_w_c_no 		= isset($_GET['sch_w_c_no'])?$_GET['sch_w_c_no']:"";
$sch_w_c_name 		= isset($_GET['sch_w_c_name']) ? $_GET['sch_w_c_name'] : "";
$sch_w_c_equal 		= isset($_GET['sch_w_c_equal']) ? $_GET['sch_w_c_equal'] : "";
$sch_w_team 		= isset($_GET['sch_w_team']) ? $_GET['sch_w_team'] : "";
$sch_w_s_name 		= isset($_GET['sch_w_s_name']) ? $_GET['sch_w_s_name'] : "";
$sch_is_inspection 	= isset($_GET['sch_is_inspection']) ? $_GET['sch_is_inspection'] : "";
$sch_is_approve 	= isset($_GET['sch_is_approve']) ? $_GET['sch_is_approve'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

if(!empty($sch_brand)) {
	$add_where .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
	$add_where .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
	$add_where .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

if(!$is_search && !isset($_GET['sch_w_s_name'])){
	$sch_w_s_name = $session_name;
}

if (!empty($sch_wd_no)) {
	$add_where .= " AND wd.wd_no='{$sch_wd_no}'";
}

if (!empty($sch_w_req_date)) {
	$add_where .= " AND wd.regdate like '%{$sch_w_req_date}%'";
}

if (!empty($sch_wd_state)) {
	$add_where .= " AND wd.wd_state='{$sch_wd_state}'";
}

if (!empty($sch_wd_method)) {
	$add_where .= " AND wd.wd_method='{$sch_wd_method}'";
}

if (!empty($sch_s_wd_date)) {
	$add_where .= " AND wd.wd_date >= '{$sch_s_wd_date}'";
}

if (!empty($sch_e_wd_date)) {
	$add_where .= " AND wd.wd_date <= '{$sch_e_wd_date}'";
}

if (!empty($sch_wd_tax_state)) {
	$add_where .= " AND wd.wd_tax_state='{$sch_wd_tax_state}'";
}

if (!empty($sch_reg_s_no)) {
	$add_where .= " AND wd.reg_s_no IN (SELECT sub.s_no FROM staff sub WHERE sub.s_name like '%{$sch_reg_s_no}%')";
}

if (!empty($sch_wd_etc)) {
	if ($sch_wd_etc == "1") {
		$add_where .= " AND  ";
	} elseif ($sch_wd_etc == "2") {
		$add_where .= " AND  ";
	}
}

if (!empty($sch_my_c_no)) {
	$add_where .= " AND wd.my_c_no = '{$sch_my_c_no}'";
}

if (!empty($sch_wd_account)) {
	$add_where .= " AND wd.wd_account = '{$sch_wd_account}'";
}

if(!empty($sch_prd_no)) { // 상품
	$add_where .= " AND `w`.prd_no='{$sch_prd_no}'";
}

if($sch_c_no == 'null') { // c_no is null
	$add_where .= " AND wd.c_no ='0'";
}

if (!empty($sch_c_name))
{
	$add_where .= " AND wd.c_name like '%{$sch_c_name}%'";
}

if (!empty($sch_subject)) {
	$add_where .= " AND wd.wd_subject like '%{$sch_subject}%'";
}

if($sch_w_c_no) {
	$add_where .= " AND `w`.c_no = '0' AND `w`.task_run_regdate >= '2020-01-01 00:00:00'";
}
else
{
	if(!empty($sch_w_my_c_no)) {
		$add_where .= " AND (SELECT cp.my_c_no FROM company cp WHERE cp.c_no =`w`.c_no) = '{$sch_w_my_c_no}'";
	}

	if (!empty($sch_w_c_name))
	{
		if(!empty($sch_w_c_equal)){
			$add_where .= " AND `w`.c_name = '{$sch_w_c_name}'";
		}else{
			$add_where .= " AND `w`.c_name LIKE '%{$sch_w_c_name}%'";
		}
	}
}

if(!$is_search){
	$sch_w_team = $session_team;
}

if(!empty($sch_w_team))
{
	if ($sch_w_team != "all")
	{
		if($sch_w_team == "00257"){
			$team_where_1 	= getTeamWhere($my_db, $sch_w_team);
			$team_where_2 	= getTeamWhere($my_db, "00252");
			$team_where 	= $team_where_1.",".$team_where_2;
		}else{
			$team_where = getTeamWhere($my_db, $sch_w_team);
		}

		$add_where .= " AND wd.team IN({$team_where})";
	}
}

if(!empty($sch_w_s_name))
{
	$add_where .= " AND wd.s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_w_s_name}%')";
}

if (!empty($sch_is_inspection))
{
	$add_where .= " AND wd.is_inspection='{$sch_is_inspection}'";
}

if (!empty($sch_is_approve))
{
	$add_where .= " AND wd.is_approve='{$sch_is_approve}'";
}

# 출금리스트 쿼리
$withdraw_sql = "
	SELECT
		wd.wd_no,
	   	wd.my_c_no,
		wd.wd_state,
		wd.wd_method,
		wd.bk_title,
		wd.bk_name,
		wd.bk_num,
	   	w.c_name as w_c_name,
	    SUM(wd.cost) as total_price
	FROM withdraw wd
	LEFT JOIN `work` AS w ON w.wd_no=wd.wd_no
	WHERE {$add_where}
	GROUP BY wd.bk_name, wd.bk_num
	ORDER BY wd.my_c_no ASC, wd.bk_name ASC
	LIMIT 2000
";
$withdraw_query = mysqli_query($my_db, $withdraw_sql);
$wd_idx = 2;
while($withdraw_array = mysqli_fetch_array($withdraw_query))
{
	$my_c_name = $withdraw_array['my_c_no'] == '1' ? "(주)와이즈플래닛컴" : $withdraw_array['w_c_name'];

	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue("A{$wd_idx}", $withdraw_array['bk_title'])
		->setCellValueExplicit("B{$wd_idx}", $withdraw_array['bk_num'], PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValue("C{$wd_idx}", $withdraw_array['total_price'])
		->setCellValue("D{$wd_idx}", $withdraw_array['bk_name'])
		->setCellValue("E{$wd_idx}", $my_c_name)
	;
	$wd_idx++;
}
$wd_idx--;

$objPHPExcel->getActiveSheet()->getStyle("A1:E{$wd_idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:E{$wd_idx}")->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:E1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00E3E3E3');
$objPHPExcel->getActiveSheet()->getStyle("A1:E1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:E{$wd_idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:E{$wd_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("C2:C{$wd_idx}")->getNumberFormat()->setFormatCode("#,##0");

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

$objPHPExcel->getActiveSheet()->setTitle('대량이체 리스트');
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename = iconv('UTF-8','EUC-KR',date("Ymd")."_대량이체 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
