<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");


//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "no")
	->setCellValue('B1', "와이즈 구성품명")
	->setCellValue('C1', "수량")
	->setCellValue('D1', "공급완료일")
	->setCellValue('E1', "공급완료월")
	->setCellValue('F1', "입고단가(VAT별도)")
	->setCellValue('G1', "입고단가(VAT포함)")
	->setCellValue('H1', "공급가")
	->setCellValue('I1', "부가세")
	->setCellValue('J1', "총 입고액")
	->setCellValue('K1', "유통기한(까지)")
;

# 검색조건
$commerce_order_no        = isset($_GET['set_no']) ? $_GET['set_no'] : "";
$commerce_order_set_sql   = "SELECT * FROM commerce_order_set WHERE `no`='{$commerce_order_no}' LIMIT 1";
$commerce_order_set_query = mysqli_query($my_db, $commerce_order_set_sql);
$commerce_order_set       = mysqli_fetch_assoc($commerce_order_set_query);
$commerce_order_set_title = $commerce_order_set['sup_c_name']." [{$commerce_order_set['order_count']}]차";

$commerce_order_sql="
		SELECT
			co.no,
			(SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_name,
			(SELECT pcu.priority FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_priority,
			co.quantity,
			co.unit_price,
			co.supply_price,
			co.total_price,
			co.vat,
			co.sup_date,
			co.limit_date
		FROM commerce_order co
		WHERE co.set_no='{$commerce_order_set['no']}' AND co.type='supply'
		ORDER BY sup_date ASC, option_priority ASC
	";

$commerce_order_query	= mysqli_query($my_db, $commerce_order_sql);
$idx = 2;
if(!!$commerce_order_query)
{
    while($commerce_order = mysqli_fetch_array($commerce_order_query))
    {
		$sup_month = date('Y-m', strtotime($commerce_order['sup_date']));
		$not_vat   = $commerce_order['unit_price'] - ($commerce_order['unit_price']*0.1);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $commerce_order['no'])
            ->setCellValue("B{$idx}", $commerce_order['option_name'])
            ->setCellValue("C{$idx}", number_format($commerce_order['quantity']))
            ->setCellValue("D{$idx}", $commerce_order['sup_date'])
            ->setCellValue("E{$idx}", $sup_month)
            ->setCellValue("F{$idx}", number_format($not_vat))
            ->setCellValue("G{$idx}", number_format($commerce_order['unit_price']))
            ->setCellValue("H{$idx}", number_format($commerce_order['supply_price']))
            ->setCellValue("I{$idx}", number_format($commerce_order['vat_price']))
            ->setCellValue("J{$idx}", number_format($commerce_order['total_price']))
            ->setCellValue("K{$idx}", $commerce_order['limit_date'])
		;

        $idx++;
    }
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');
$background_color = "00262626";

$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');


$objPHPExcel->getActiveSheet()->getStyle("A1:K{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:K{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("B2:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);

$objPHPExcel->getActiveSheet()->setTitle('입고완료 내역');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',"입고완료 내역.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
