<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');

$wsu_no=isset($_GET['wsu_no']) ? $_GET['wsu_no'] : "";
$smarty->assign("wsu_no",$wsu_no);

// GET : 위의 wsu_no 번호가 없으면 등록(write)으로, 없으면 수정(modify)으로 mode에 저장
$mode=(!$wsu_no) ? "write":"modify";

// POST : form action 을 통해 넘겨받은 process 가 있는지 체크하고 있으면 proc에 저장
$proc=isset($_POST['process']) ? $_POST['process'] : "";

// 담당자리스트
$staff_sql="select s_no,s_name from staff where staff_state = '1'";
$staff_query=mysqli_query($my_db,$staff_sql);

while($staff_data=mysqli_fetch_array($staff_query)) {
	$staffs[]=array(
		'no'=>$staff_data['s_no'],
		'name'=>$staff_data['s_name']
	);
}

$smarty->assign("staff",$staffs);



if ($proc == "f_title") { // 업무세트명 자동저장
	$wsu_no = (isset($_POST['wsu_no'])) ? $_POST['wsu_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE work_set_unit SET title = NULL WHERE wsu_no = '" . $wsu_no . "'";
	}else{
		$sql = "UPDATE work_set_unit SET title = '" . addslashes($value) . "' WHERE wsu_no = '" . $wsu_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "업무세트명 저장에 실패 하였습니다.";
	else
		echo "업무세트명이 저장 되었습니다.";

	exit;

}else if ($proc == "f_s_no") { // 업무세트 담당자 자동저장
	$wsu_no = (isset($_POST['wsu_no'])) ? $_POST['wsu_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE work_set_unit SET s_no = NULL WHERE wsu_no = '" . $wsu_no . "'";
	}else{
		$sql = "UPDATE work_set_unit SET s_no = '" . $value . "' WHERE wsu_no = '" . $wsu_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "업무세트 담당자 저장에 실패 하였습니다.";
	else
		echo "업무세트 담당자가 저장 되었습니다.";

	exit;

}else if ($proc == "f_priority") { // 순번 자동저장
	$r_no = (isset($_POST['r_no'])) ? $_POST['r_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE  work_set_unit_relation SET priority = NULL WHERE r_no = '" . $r_no . "'";
	}else{
		$sql = "UPDATE  work_set_unit_relation SET priority = '" . $value . "' WHERE r_no = '" . $r_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "순번 변경에 실패 하였습니다.";
	else{
		echo "순번이 변경 되었습니다.";
		echo "<script>location.reload();</script>";
	}

	exit;

}else if ($proc == "f_prd") { // 업무 세트 리스트
    $r_no = (isset($_POST['r_no'])) ? $_POST['r_no'] : "";
    $value = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($value)){
        $sql = "UPDATE  work_set_unit_relation SET prd_no = NULL, category=NULL WHERE r_no = '" . $r_no . "'";
    }else{
        $sql = "UPDATE  work_set_unit_relation SET prd_no = '" . $value . "', category=NULL WHERE r_no = '" . $r_no . "'";
    }

    if (!mysqli_query($my_db, $sql))
        echo "업무 저장에 실패 하였습니다.";
    else
        echo "업무가 저장 되었습니다.";

    exit;

}else if ($proc == "f_prd_kind") { // 업무 세트 리스트
    $r_no = (isset($_POST['r_no'])) ? $_POST['r_no'] : "";
    $value = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($value)){
        $sql = "UPDATE  work_set_unit_relation SET category=NULL WHERE r_no = '{$r_no}'";
    }else{
        $sql = "UPDATE  work_set_unit_relation SET category = '{$value}' WHERE r_no = '{$r_no}'";
    }

    if (!mysqli_query($my_db, $sql))
        echo "세부카테고리 저장에 실패 하였습니다.";
    else
        echo "세부카테고리가 저장 되었습니다.";

    exit;

}elseif($proc=="del_relation") {  // 삭제
	$wsu_no=(isset($_POST['wsu_no']))?$_POST['wsu_no']:"";
	$r_no = (isset($_POST['r_no'])) ? $_POST['r_no'] : "";

	$relation_sql = "DELETE FROM  work_set_unit_relation WHERE r_no = '". $r_no ."'";
	if(!mysqli_query($my_db, $relation_sql)){
		exit("<script>alert('r_no = {$r_no} 삭제에 실패 하였습니다.');location.href='work_set_unit_regist.php?wsu_no=$wsu_no';</script>");
	}else{
		exit("<script>location.href='work_set_unit_regist.php?wsu_no=$wsu_no';</script>");
	}

}elseif($proc=="create_relation") {  // 추가
	$wsu_no=(isset($_POST['wsu_no']))?$_POST['wsu_no']:"";
	$f_priority=(isset($_POST['f_priority']))?$_POST['f_priority']:"";
	$f_prd=(isset($_POST['f_prd']))?$_POST['f_prd']:"";
	$f_prd_kind=(isset($_POST['f_prd_kind']))?$_POST['f_prd_kind']:"";

	$relation_sql="INSERT into  work_set_unit_relation set
					wsu_no = '{$wsu_no}',
					priority = (SELECT priority FROM product WHERE prd_no='{$f_prd}'),
					prd_no = '{$f_prd}',
					category='{$f_prd_kind}'
				";
	//echo $relation_sql; exit;

	if(!mysqli_query($my_db, $relation_sql)){
		exit("<script>alert('wsu_no = {$wsu_no} 추가에 실패 하였습니다.');location.href='work_set_unit_regist.php?wsu_no=$wsu_no';</script>");
	}else{
		exit("<script>location.href='work_set_unit_regist.php?wsu_no=$wsu_no';</script>");
	}
}

if($mode=="modify") {

	// work set 리스트 쿼리
	$work_set_unit_sql = "
								SELECT
									w_s.wsu_no,
									w_s.title,
									w_s.s_no
								FROM work_set_unit w_s
								WHERE w_s.wsu_no='".$wsu_no."'
								";

	//echo "<br><br>".$work_set_unit_sql."<br><br>";

	$work_set_unit_result = mysqli_query($my_db, $work_set_unit_sql);
	$work_set_unit_data = mysqli_fetch_array($work_set_unit_result);

	$smarty->assign(
		array(
			"wsu_no"=>$work_set_unit_data['wsu_no'],
			"title"=>$work_set_unit_data['title'],
			"s_no"=>$work_set_unit_data['s_no']
		)
	);


	// 상품 구분 가져오기(1차)
	$prd_g1_sql="
			SELECT
					k_name,k_name_code
			FROM kind
			WHERE
					k_code='product' AND (k_parent='0' OR k_parent is null) AND display='1'
			ORDER BY
					priority ASC
			";
	$prd_g1_result=mysqli_query($my_db,$prd_g1_sql);

	while($prd_g1=mysqli_fetch_array($prd_g1_result)) {
		$prd_g1_list[]=array(
			"k_name"=>trim($prd_g1['k_name']),
			"k_name_code"=>trim($prd_g1['k_name_code'])
		);
		$smarty->assign("prd_g1_list",$prd_g1_list);
	}


	//  work_set_unit_relation 리스트 쿼리
	$realtion_sql = "
								SELECT
									w_s_R_s.r_no,
									w_s_R_s.wsu_no,
									w_s_R_s.priority,
									(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w_s_R_s.prd_no))) AS k_prd1,
									(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w_s_R_s.prd_no))) AS k_prd1_name,
									(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w_s_R_s.prd_no)) AS k_prd2,
									(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w_s_R_s.prd_no)) AS k_prd2_name,
									w_s_R_s.prd_no,
									(SELECT title FROM product prd WHERE prd.prd_no=w_s_R_s.prd_no) AS prd_name,
									w_s_R_s.category
								FROM
									 work_set_unit_relation w_s_R_s
								WHERE
									w_s_R_s.wsu_no='".$wsu_no."'
								ORDER BY
									w_s_R_s.priority ASC, w_s_R_s.prd_no ASC
								";


	$realtion_result = mysqli_query($my_db, $realtion_sql);
	while($realtion_array = mysqli_fetch_array($realtion_result)){
		// 상품 구분 가져오기(2차)
		$prd_g2_sql="
					SELECT
							k_name,k_name_code,k_parent
					FROM kind
					WHERE
							k_code='product' AND display='1' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='".$realtion_array['prd_no']."'))
					ORDER BY
							priority ASC
					";

		$prd_g2_result=mysqli_query($my_db,$prd_g2_sql);
		while($prd_g2=mysqli_fetch_array($prd_g2_result)) {
			$prd_g2_list[]=array(
				"r_no"=>trim($realtion_array['r_no']),
				"k_name"=>trim($prd_g2['k_name']),
				"k_name_code"=>trim($prd_g2['k_name_code']),
				"k_parent"=>trim($prd_g2['k_parent'])
			);
			$smarty->assign("prd_g2_list",$prd_g2_list);
		}

		// 상품 목록 가져오기
		$prd_sql="
					SELECT
							title,prd_no,k_name_code,description,work_format,suggested_price,market_price,min_price,max_price
					FROM product
					WHERE
							k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='".$realtion_array['prd_no']."')
					ORDER BY
							priority ASC
					";

		$prd_result  = mysqli_query($my_db,$prd_sql);
		while($prd=mysqli_fetch_array($prd_result)) {
			$prd_list[]=array(
				"r_no"=>trim($realtion_array['r_no']),
				"title"=>trim($prd['title']),
				"prd_no"=>trim($prd['prd_no']),
				"k_name_code"=>trim($prd['k_name_code'])
			);
			$smarty->assign("prd_list",$prd_list);
		}


        // 상품 목록 가져오기
        $k_parent 	 = sprintf('%05d', $realtion_array['prd_no']);
        $prd_kind_sql = "SELECT k_name_code, k_name FROM kind WHERE k_code='work_task_run' AND k_parent='{$k_parent}' AND display='1' ORDER BY priority";
        $prd_kind_result=mysqli_query($my_db,$prd_kind_sql);
        while($prd_kind=mysqli_fetch_array($prd_kind_result)) {
            $prd_kind_list[]=array(
                "r_no"=>$realtion_array['r_no'],
                "title"=>trim($prd_kind['k_name']),
                "k_name_code"=>trim($prd_kind['k_name_code'])
            );
            $smarty->assign("prd_kind_list",$prd_kind_list);
        }


		$realtion[] = array(
			"r_no"=>$realtion_array['r_no'],
			"wsu_no"=>$realtion_array['wsu_no'],
			"priority"=>$realtion_array['priority'],
			"k_prd1"=>$realtion_array['k_prd1'],
			"k_prd1_name"=>$realtion_array['k_prd1_name'],
			"k_prd2"=>$realtion_array['k_prd2'],
			"k_prd2_name"=>$realtion_array['k_prd2_name'],
			"prd_no"=>$realtion_array['prd_no'],
			"prd_name"=>$realtion_array['prd_name'],
			"category"=>$realtion_array['category']
		);
	}
	$smarty->assign("realtion", $realtion);


} else if ($proc=="write") {
	$add_set = "";

	if(!empty($_POST['f_title'])){
		$add_set.=" title = '".addslashes($_POST['f_title'])."',";
	}

	if(!empty($_POST['f_s_no'])){
		$add_set.=" s_no = '".$_POST['f_s_no']."'";
	}

	$sql="INSERT INTO work_set_unit SET
													regdate = now(),
													".$add_set."
													";
	//echo "<br><br><br>".$sql;exit;

	if (mysqli_query($my_db, $sql)){
		$result= mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
		$okr_obj_array=mysqli_fetch_array($result);
		$last_wsu_no = $okr_obj_array[0];

		exit("<script>location.href='work_set_unit_regist.php?wsu_no=$last_wsu_no';</script>");
	} else {
		exit("<script>alert('등록에 실패 하였습니다');location.href='work_set_unit_regist.php';</script>");
	}

// form action 이 수정일 경우
} elseif($proc=="modify") {
	$supply_cost_value = str_replace(",","",trim($_POST['f_supply_cost'])); // 컴마 제거하기
	$supply_cost_vat_value = str_replace(",","",trim($_POST['f_supply_cost_vat'])); // 컴마 제거하기
	$cost_value = str_replace(",","",trim($_POST['f_cost'])); // 컴마 제거하기
	$wd_money_value = str_replace(",","",trim($_POST['f_wd_money'])); // 컴마 제거하기

	$add_set = "";

	if(!empty($_POST['f_wd_date'])){
		$add_set.="wd_date = '".$_POST['f_wd_date']."',";
	}else if(empty($_POST['f_wd_date'])){
		$add_set.="wd_date = NULL,";
	}
	if(!empty($_POST['f_incentive_date'])){
		$add_set.="incentive_date = '".$_POST['f_incentive_date']."',";
	}
	if(!empty($_POST['f_cost_info'])){
		$add_set.="cost_info = '".addslashes($_POST['f_cost_info'])."',";
	}
	if(!empty($_POST['f_wd_tax_date'])){
		$add_set.="wd_tax_date = '".$_POST['f_wd_tax_date']."',";
	}else if(empty($_POST['f_wd_tax_date'])){
		$add_set.="wd_tax_date = NULL,";
	}
	if(!empty($_POST['f_wd_tax_item'])){
		$add_set.="wd_tax_item = '".addslashes($_POST['f_wd_tax_item'])."',";
	}

	$files=$_FILES["file"];
	if($files){
		image_check($files);
		$save_name=store_image($files, "evaluation");

		if(!empty($save_name[0])) {
			$add_set.="file_read='".$save_name[0]."', ";
		}
		if(!empty($_FILES["file"]["name"][0])) {
			$add_set.="file_origin='".addslashes($_FILES["file"]["name"][0])."', ";
		}
	}

	$sql="UPDATE evaluation SET
				wd_subject = '".addslashes($_POST['f_wd_subject'])."',
				wd_state = '".$_POST['f_wd_state']."',
				wd_method = '".$_POST['f_wd_method']."',
				c_no = '".$_POST['f_c_no']."',
				c_name = '".addslashes($_POST['f_c_name'])."',
				s_no = '".$_POST['s_no']."',
				supply_cost = '".$supply_cost_value."',
				vat_choice = '".$_POST['f_vat_choice']."',
				supply_cost_vat = '".$supply_cost_vat_value."',
				cost = '".$cost_value."',
				wd_money = '".$wd_money_value."',
				".$add_set."
				wd_tax_state = '".$_POST['f_wd_tax_state']."'
		WHERE
			wsu_no = '".$_POST['wsu_no']."'
		";
	//echo "<br><br><br>".$sql;exit;
	$my_db->query($sql);

	alert("수정하였습니다.","okr_list.php");

}

// 템플릿에 해당 입력값 던져주기
$smarty->assign(
	array(
		"proc"=>$proc,
		"mode"=>$mode,
		"idx"=>$idx
	)
);
$smarty->display('work_set_unit_regist.html');

?>
