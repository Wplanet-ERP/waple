<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/asset.php');
require('inc/model/MyQuick.php');
require('inc/model/Asset.php');
require('inc/model/Kind.php');
require('inc/model/Message.php');
require('inc/model/MyCompany.php');
require('inc/model/Staff.php');

# 프로세스 처리
$asset_model = Asset::Factory();
$process     = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'asset_state')
{
    $as_no  = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $value  = isset($_POST['val']) ? $_POST['val'] : "";

    if(!!$as_no)
    {
        $upd_data = array("as_no" => $as_no, "asset_state" => $value);

        if (!$asset_model->update($upd_data))
            echo "진행상태 저장에 실패 하였습니다.";
        else
            echo "진행상태가 저장 되었습니다.";
        exit;
    }

    echo "진행상태 저장에 실패 하였습니다.";
    exit;
}
elseif($process == 'memo')
{
    $as_no  = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $value  = isset($_POST['val']) ? addslashes(trim($_POST['val'])) : "";

    if(!!$as_no)
    {
        $upd_data = array("as_no" => $as_no, "memo" => $value);

        if (!$asset_model->update($upd_data))
            echo "관리자 메모 저장에 실패 하였습니다.";
        else
            echo "관리자 메모가 저장 되었습니다.";
        exit;
    }

    echo "관리자 메모 저장에 실패 하였습니다.";
    exit;
}
elseif($process == 'asset_repair')
{
    $search_url  = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $as_no       = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $task_req    = isset($_POST['task_req']) ? addslashes($_POST['task_req']) : "";
    $asset       = $asset_model->getItem($as_no);
    $as_name     = $asset['name'];
    $cur_date    = date('Y-m-d H:i:s');
    $work_state  = '5';
    $run_s_no    = "260";
    $run_name    = "이지윤";
    $run_team    = "00211";

    if($asset['object_type'] != '1'){
        exit("<script>alert('유형자산만 수리요청 하실 수 있습니다');location.href='asset_management.php?{$search_url}';</script>");
    }

    $staff_model    = Staff::Factory();
    $req_staff      = $staff_model->getStaff($session_s_no);
    $req_s_no       = isset($req_staff['s_no']) ? $req_staff['s_no'] : "";
    $req_name       = isset($req_staff['s_name']) ? $req_staff['s_name'] : "";
    $req_team       = isset($req_staff['team']) ? $req_staff['team'] : "";
    $req_c_no       = isset($req_staff['my_c_no']) ? $req_staff['my_c_no'] : "";

    $ins_sql = "
        INSERT INTO asset_reservation SET
            `work_state`    = '5',
            `as_no`         = '{$as_no}',
            `management`    = '{$asset['management']}',
            `manager`       = '{$asset['manager']}',
            `sub_manager`   = '{$asset['sub_manager']}',
            `regdate`       = '{$cur_date}',
            `req_no`        = '{$req_s_no}',
            `req_name`      = '{$req_name}',
            `task_req`      = '{$task_req}',
            `req_date`      = '{$cur_date}',
            `run_no`        = '{$run_s_no}',
            `run_name`      = '{$run_name}'
    ";

    if (mysqli_query($my_db, $ins_sql))
    {
        $last_as_r_no = mysqli_insert_id($my_db);
        $work_c_no = $work_c_name = $work_s_no = $work_team = "";
        if($req_c_no == '3'){
            $work_c_name ="와이즈플래닛_미디어커머스";
            $work_c_no   = "1113";
            $work_s_no   = "1";
            $work_team   = "00200";
        }else{
            $work_c_name = "와이즈플래닛 컴퍼니";
            $work_c_no   = "1580";
            $work_s_no   = "1";
            $work_team   = "00200";
        }

        $work_sql = "INSERT INTO `work` SET work_state='3', c_no='{$work_c_no}', c_name='{$work_c_name}', s_no='{$work_s_no}', team='{$work_team}', prd_no='227', k_name_code='02301', task_req='{$task_req}', task_req_s_no='{$req_s_no}', task_req_team='{$req_team}', task_run_s_no='{$run_s_no}', task_run_team='{$run_team}', linked_no = '{$last_as_r_no}', linked_table = 'asset', regdate='{$cur_date}'";
        mysqli_query($my_db, $work_sql);

        $upd_asset_state_sql = "UPDATE asset SET asset_state='3' WHERE as_no='{$as_no}'";
        mysqli_query($my_db, $upd_asset_state_sql);

        if(!empty($as_name))
        {
            $message_model  = Message::Factory();
            $send_data_list = [];

            $sms_phone      = "02-830-1912";
            $sender         = "와이즈플래닛";
            $sms_name       = "와이즈플래닛 자산관리";
            $sms_msg        = "{$session_name}님이 {$as_name} 자산수리를 요청하였습니다.";
            $sms_msg_len    = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
            $msg_type 		= ($sms_msg_len > 90) ? "L" : "S";
            $msg_id 	    = "W".date("YmdHis").sprintf('%04d', "1");
            $receiver_staff = $staff_model->getStaff($asset['manager']);

            $send_data_list[$msg_id] = array(
                "msg_id"        => $msg_id,
                "msg_type"      => $msg_type,
                "sender"        => $sender,
                "sender_hp"     => $sms_phone,
                "receiver"      => $receiver_staff['s_name'],
                "receiver_hp"   => $receiver_staff['hp'],
                "title"         => $sms_name,
                "content"       => $sms_msg,
                "cinfo"         => "3",
            );

            $message_model->sendMessage($send_data_list);
        }

        exit("<script>alert('수리 요청에 성공했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }else {
        exit("<script>alert('수리 요청에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "71";
$nav_title   = "자산 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 자산관리 상품 처리
$add_where      = "1=1";
$sch_asset_g1   = isset($_GET['sch_asset_g1']) ? $_GET['sch_asset_g1'] : "";
$sch_asset_g2   = isset($_GET['sch_asset_g2']) ? $_GET['sch_asset_g2'] : "";
$sch_asset      = isset($_GET['sch_asset']) ? $_GET['sch_asset'] : "";
$sch            = isset($_GET['sch']) ? $_GET['sch'] : "";

$smarty->assign("sch_asset_g1", $sch_asset_g1);
$smarty->assign("sch_asset_g2", $sch_asset_g2);

if(!empty($sch)) {
    $smarty->assign("sch", $sch);
}

if (!empty($sch_asset)) {
    $add_where .= " AND as.name like '%{$sch_asset}%'";
    $smarty->assign("sch_asset", $sch_asset);
}

if($sch_asset_g2){
    $add_where .= " AND as.k_name_code ='{$sch_asset_g2}'";
}elseif($sch_asset_g1){
    $add_where .= " AND (SELECT k_parent FROM kind k WHERE k.k_name_code=`as`.k_name_code)='{$sch_asset_g1}'";
}

$kind_model       = Kind::Factory();
$asset_group_list = $kind_model->getKindGroupList("asset");
$asset_g1_list = $asset_g2_list = [];
foreach($asset_group_list as $key => $asset_data)
{
    if(!$key){
        $asset_g1_list = $asset_data;
    }else{
        $asset_g2_list[$key] = $asset_data;
    }
}
$sch_asset_g2_list = isset($asset_g2_list[$sch_asset_g1]) ? $asset_g2_list[$sch_asset_g1] : [];

$smarty->assign("asset_g1_list", $asset_g1_list);
$smarty->assign("asset_g2_list", $sch_asset_g2_list);


$sch_asset_state    = isset($_GET['sch_asset_state']) ? $_GET['sch_asset_state'] : "";
$sch_my_c_name      = isset($_GET['sch_my_c_name']) ? $_GET['sch_my_c_name'] : "";
$sch_keyword        = isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";
$sch_as_no          = isset($_GET['sch_as_no']) ? $_GET['sch_as_no'] : "";
$sch_management     = isset($_GET['sch_management']) ? $_GET['sch_management'] : "";
$sch_manager_name   = isset($_GET['sch_manager_name']) ? $_GET['sch_manager_name'] : "";
$sch_description    = isset($_GET['sch_description']) ? $_GET['sch_description'] : "";
$sch_use_type       = isset($_GET['sch_use_type']) ? $_GET['sch_use_type'] : "";
$sch_asset_loc      = isset($_GET['sch_asset_loc']) ? $_GET['sch_asset_loc'] : "";
$sch_object_type    = isset($_GET['sch_object_type']) ? $_GET['sch_object_type'] : "";
$sch_share_type     = isset($_GET['sch_share_type']) ? $_GET['sch_share_type'] : "";
$sch_dp_c_name      = isset($_GET['sch_dp_c_name']) ? $_GET['sch_dp_c_name'] : "";
$sch_manufacturer   = isset($_GET['sch_manufacturer']) ? $_GET['sch_manufacturer'] : "";
$sch_get_s_date     = isset($_GET['sch_get_s_date']) ? $_GET['sch_get_s_date'] : "";
$sch_get_e_date     = isset($_GET['sch_get_e_date']) ? $_GET['sch_get_e_date'] : "";
$sch_expire_s_date  = isset($_GET['sch_expire_s_date']) ? $_GET['sch_expire_s_date'] : "";
$sch_expire_e_date  = isset($_GET['sch_expire_e_date']) ? $_GET['sch_expire_e_date'] : "";
$sch_notice         = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$sch_memo           = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
$sch_use_pw         = isset($_GET['sch_use_pw']) ? $_GET['sch_use_pw'] : "";

if(!empty($sch_asset_state))
{
    $add_where .= " AND `as`.asset_state = '{$sch_asset_state}'";
    $smarty->assign('sch_asset_state', $sch_asset_state);
}

if(!empty($sch_my_c_name))
{
    $add_where .= " AND `as`.my_c_no IN(SELECT mc.my_c_no FROM my_company mc WHERE mc.c_name LIKE '%{$sch_my_c_name}%')";
    $smarty->assign('sch_my_c_name', $sch_my_c_name);
}

if(!empty($sch_keyword))
{
    $add_where .= " AND `as`.keyword like '%{$sch_keyword}%'";
    $smarty->assign('sch_keyword', $sch_keyword);
}

if(!empty($sch_as_no))
{
    $add_where .= " AND `as`.as_no = '{$sch_as_no}'";
    $smarty->assign('sch_as_no', $sch_as_no);
}

if(!empty($sch_management))
{
    $add_where .= " AND `as`.management LIKE '%{$sch_management}%'";
    $smarty->assign('sch_management', $sch_management);
}

if(!empty($sch_manager_name))
{
    $add_where .= " AND (`as`.manager_name LIKE '%{$sch_manager_name}%' OR `as`.sub_manager_name LIKE '%{$sch_manager_name}%')";
    $smarty->assign('sch_manager_name', $sch_manager_name);
}

if(!empty($sch_description))
{
    $add_where .= " AND `as`.description LIKE '%{$sch_description}%'";
    $smarty->assign('sch_description', $sch_description);
}

if(!empty($sch_use_type))
{
    $add_where .= " AND `as`.use_type = '{$sch_use_type}'";
    $smarty->assign('sch_use_type', $sch_use_type);
}

if(!empty($sch_asset_loc))
{
    $add_where .= " AND `as`.asset_loc LIKE '%{$sch_asset_loc}%'";
    $smarty->assign('sch_asset_loc', $sch_asset_loc);
}

if(!empty($sch_object_type))
{
    $add_where .= " AND `as`.object_type = '{$sch_object_type}'";
    $smarty->assign('sch_object_type', $sch_object_type);
}

if(!empty($sch_share_type))
{
    $add_where .= " AND `as`.share_type = '{$sch_share_type}'";
    $smarty->assign('sch_share_type', $sch_share_type);
}

if(!empty($sch_dp_c_name))
{
    $add_where .= " AND `as`.dp_c_name LIKE '%{$sch_dp_c_name}%'";
    $smarty->assign('sch_dp_c_name', $sch_dp_c_name);
}

if(!empty($sch_manufacturer))
{
    $add_where .= " AND `as`.manufacturer LIKE '%{$sch_manufacturer}%'";
    $smarty->assign('sch_manufacturer', $sch_manufacturer);
}

if(!empty($sch_get_s_date))
{
    $add_where .= " AND `as`.get_date >= '{$sch_get_s_date}'";
    $smarty->assign('sch_get_s_date', $sch_get_s_date);
}

if(!empty($sch_get_e_date))
{
    $add_where .= " AND `as`.get_date <= '{$sch_get_e_date}'";
    $smarty->assign('sch_get_e_date', $sch_get_e_date);
}

if(!empty($sch_expire_s_date))
{
    $add_where .= " AND `as`.expire_date >= '{$sch_expire_s_date}'";
    $smarty->assign('sch_expire_s_date', $sch_expire_s_date);
}

if(!empty($sch_expire_e_date))
{
    $add_where .= " AND `as`.expire_date <= '{$sch_expire_e_date}'";
    $smarty->assign('sch_expire_e_date', $sch_expire_e_date);
}

if(!empty($sch_notice))
{
    $add_where .= " AND `as`.notice LIKE '%{$sch_notice}%'";
    $smarty->assign('sch_notice', $sch_notice);
}

if(!empty($sch_memo))
{
    $add_where .= " AND `as`.memo LIKE '%{$sch_memo}%'";
    $smarty->assign('sch_memo', $sch_memo);
}

if(!empty($sch_use_pw))
{
    if($sch_use_pw == '1'){
        $add_where .= " AND `as`.pw_date != ''";
    }elseif($sch_use_pw == '2'){
        $add_where .= " AND (`as`.pw_date is null OR `as`.pw_date = '')";
    }

    $smarty->assign('sch_use_pw', $sch_use_pw);
}

# 전체 게시물 수
$asset_total_sql	= "SELECT count(as_no) as cnt FROM (SELECT `as`.as_no FROM asset `as` WHERE {$add_where}) AS cnt";
$asset_total_query	= mysqli_query($my_db, $asset_total_sql);
$asset_total_result = mysqli_fetch_array($asset_total_query);
$asset_total        = $asset_total_result['cnt'];

# 페이징 처리
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($asset_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list  = pagelist($pages, "asset_management.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $asset_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$asset_sql  = "
    SELECT 
        *,
        DATE_FORMAT(`as`.regdate, '%Y-%m-%d') as reg_date,
        DATE_FORMAT(`as`.regdate, '%H:%i') as reg_time,
        (SELECT mc.c_name FROM my_company as mc WHERE mc.my_c_no = `as`.my_c_no) as my_c_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=`as`.k_name_code)) AS k_asset1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=`as`.k_name_code) AS k_asset2_name,
        DATE_FORMAT(`as`.get_date, '%Y/%m/%d') as get_date,
        DATE_FORMAT(`as`.expire_date, '%Y/%m/%d') as expire_date,
        (SELECT req_name FROM asset_reservation ar WHERE ar.as_no = `as`.as_no AND ar.work_state='2' AND `as`.asset_state='5' ORDER BY ar.as_r_no DESC LIMIT 1) as use_info,
        (SELECT count(as_r_no) FROM asset_reservation ar WHERE ar.as_no = `as`.as_no AND ar.work_state='2' AND ar.req_no='{$session_s_no}') as ar_cnt
    FROM asset `as` 
    WHERE {$add_where} 
    ORDER BY `as`.as_no DESC
    LIMIT {$offset}, {$num}
";
$asset_query    = mysqli_query($my_db, $asset_sql);
$asset_list     = [];
$is_admin       = permissionNameCheck($session_permission, '재무관리자');

# 자산 Option
$asset_state_option         = getAssetStateOption();
$asset_use_type_option      = getAssetUseTypeOption();
$asset_share_type_option    = getAssetShareTypeOption();
$asset_object_type_option   = getAssetObjectTypeOption();
while($asset = mysqli_fetch_assoc($asset_query))
{
    $asset_state     = isset($asset['asset_state']) && !empty($asset['asset_state']) ? $asset['asset_state'] : "";
    $use_type        = isset($asset['use_type']) && !empty($asset['use_type']) ? $asset['use_type'] : "";
    $share_type      = isset($asset['share_type']) && !empty($asset['share_type']) ? $asset['share_type'] : "";
    $object_type     = isset($asset['object_type']) && !empty($asset['object_type']) ? $asset['object_type'] : "";
    $non_description = !empty($asset['non_description']) ? ($is_admin ? $asset['non_description'] : "*******************************") : "";
    $img_paths       = $asset['img_path'];
    $img_names       = $asset['img_name'];
    $file_paths      = $asset['file_path'];
    $file_names      = $asset['file_name'];

    if(!empty($img_paths) && !empty($img_names))
    {
        $img_paths_arr = explode(',', $img_paths);
        $img_names_arr = explode(',', $img_names);
        $asset['img_paths'] = $img_paths_arr;
        $asset['img_names'] = $img_names_arr;
    }else{
        $asset['img_paths'] = "";
        $asset['img_names'] = "";
    }

    if(!empty($file_paths) && !empty($file_names))
    {
        $file_paths_arr = explode(',', $file_paths);
        $file_names_arr = explode(',', $file_names);
        $asset['file_paths'] = $file_paths_arr;
        $asset['file_names'] = $file_names_arr;
    }else{
        $asset['file_paths'] = "";
        $asset['file_names'] = "";
    }

    $asset['asset_state_name']  = !empty($asset_state) ? $asset_state_option[$asset_state]['label'] : "";
    $asset['asset_state_color'] = !empty($asset_state) ? $asset_state_option[$asset_state]['color'] : "black";
    $asset['use_type_name']     = !empty($use_type) ? $asset_use_type_option[$use_type] : "";
    $asset['share_type_name']   = !empty($share_type) ? $asset_share_type_option[$share_type] : "";
    $asset['object_type_name']  = !empty($object_type) ? $asset_object_type_option[$object_type] : "";
    $asset['description']       = str_replace("\r\n","<br />", $asset['description']);
    $asset['non_description']   = $non_description;

    $asset_use_person_list = [];
    $asset_use_person_sql = "SELECT DISTINCT req_name FROM asset_reservation WHERE as_no='{$asset['as_no']}' AND work_state='2'";
    $asset_use_person_query = mysqli_query($my_db, $asset_use_person_sql);
    while($asset_use = mysqli_fetch_assoc($asset_use_person_query))
    {
        $asset_use_person_list[] = $asset_use['req_name'];
    }

    $asset['show_permission'] = '1';
    if(($asset['use_type'] == '1' && $asset['object_type'] == '3') && (!in_array($session_name, $asset_use_person_list) && $session_s_no != $asset['manager'] && $session_s_no != $asset['sub_manager'])) {
        $asset['show_permission'] = '2';
    }

    $asset_list[] = $asset;
}

$my_comp_model = MyCompany::Factory();

$smarty->assign("my_company_list", $my_comp_model->getList());
$smarty->assign('asset_state_option', $asset_state_option);
$smarty->assign('asset_use_type_option', $asset_use_type_option);
$smarty->assign('asset_share_type_option', $asset_share_type_option);
$smarty->assign('asset_object_type_option', $asset_object_type_option);
$smarty->assign('asset_define_option', getDefineOption());
$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign('is_admin', $is_admin);
$smarty->assign('asset_list', $asset_list);

$smarty->display('asset_management.html');
?>
