<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/LunarToSola.lib.php'); // 양력 <-> 음력 계산기
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/staff.php');
require('inc/model/MyCompany.php');

# 초기 설정
$s_no	= isset($_GET['s_no']) ? $_GET['s_no'] : "";
$proc	= isset($_POST['process']) ? $_POST['process'] : "";

$regist_mode    = "write";
$submit_btn 	= "등록하기";
$delete_btn     = "삭제하기";

$strength_arr_keys     = array("1","2","3","4","5");
$strength_keyword_list = array_fill_keys($strength_arr_keys, "");

$is_editable = false;
if(permissionNameCheck($session_permission,"마스터관리자") || permissionNameCheck($session_permission,"재무관리자")){
    $is_editable = true;
}
$smarty->assign("is_editable", $is_editable);

# Process 작업
if($proc == "write")
{
	$emails[] 	= $_POST['f_email_0'];
	$emails[] 	= $_POST['f_email_1'];
	$email		= implode('@',$emails);

	# 중복 ID 체크
	$staff_chk_sql		="SELECT count(s_no) as chk FROM staff WHERE id='".trim($_POST['f_id'])."'";
    $staff_chk_query	= mysqli_query($my_db, $staff_chk_sql);
    $staff_chk_result	= mysqli_fetch_array($staff_chk_query);

	if($staff_chk_result['chk'] > 0)
	{
		exit("<script>alert('이미 등록된 담당자 아이디 입니다');history.back();</script>");
	}

    # 퍼미션 값 정리
	$permission_val = isset($_POST['f_permission_1']) ? $_POST['f_permission_1'] : "0";
	for( $i = 2 ; $i < sizeof($permission) ; $i++) {
		$permission_val .= isset($_POST['f_permission_'.$i]) ? $_POST['f_permission_'.$i] : "0";
	}

	# 팀 리스트정리
	$f_team_list_post = implode(",", $_POST['f_team']);
	$f_team_post 	  = $_POST['f_team_default'];

	# 패스워드 정리
    $f_password = substr(md5($_POST['staff_pw_new']),8,16);

    $add_c_type = "";
    if($_POST['f_my_c_type'] != '1'){
        $add_c_type = ",my_c_no='0', team=0, team_list=null, my_c_type='{$_POST['f_my_c_type']}', partner_c_no='{$_POST['f_partner_c_no']}'";
    }else{
        $add_c_type = ",my_c_no='{$_POST['f_my_c_no']}', team= '{$f_team_post}', team_list='{$f_team_list_post}', my_c_type='{$_POST['f_my_c_type']}', partner_c_no=null";
    }

    $add_cert = "";
    if($_POST['f_cert_no1'] &&  $_POST['f_cert_no2']){
        $cert_no  = trim($_POST['f_cert_no1'])."-".trim($_POST['f_cert_no2']);
        $add_cert =",cert_no = HEX(AES_ENCRYPT('".addslashes(trim($cert_no))."', '{$aeskey}'))";
    }

	$ins_sql = "
		INSERT INTO staff SET
			`id` 		= '{$_POST['f_id']}',
			`pass` 		= '{$f_password}',
			staff_state = '{$_POST['f_staff_state']}',
			`rar`       = '".addslashes(trim($_POST['f_rar']))."',
			s_name 		= '{$_POST['f_s_name']}',
			`addr` 	    = '{$_POST['f_addr']}',
			`position` 	= '{$_POST['f_position']}',
			`pos_permission` = '{$_POST['f_pos_permission']}',
			slack_id 	= '{$_POST['f_slack_id']}',
			email 		= '{$email}',
			tel 		= '{$_POST['f_tel']}',
			extension 	= '{$_POST['f_extension']}',
			hp 			= '{$_POST['f_hp']}',
			birthday 	= '{$_POST['f_birthday']}',
			sola_lunar 	= '{$_POST['f_sola_lunar']}',
			employment_date = '{$_POST['f_employment_date']}',
			regdate 	= now(),
			permission 	= '{$permission_val}'
			{$add_c_type}
			{$add_cert}
	";

    if($my_db->query($ins_sql)){
        exit("<script>alert('수정하였습니다.');location.href='staff_list.php';</script>");
    }else{
        exit("<script>alert('등록에 실패했습니다');history.back();</script>");
    }
}
elseif($proc=="modify")
{
	$emails[]	= $_POST['f_email_0'];
	$emails[]	= $_POST['f_email_1'];
	$email		= implode('@',$emails);

	$add_set    = "";
    $add_c_type = "";
	if(permissionNameCheck($session_permission,"마스터관리자") || permissionNameCheck($session_permission,"재무관리자"))
	{
		$permission_val = isset($_POST['f_permission_1']) ? $_POST['f_permission_1'] : "0";
		for( $i = 2 ; $i < sizeof($permission) ; $i++) {
			$permission_val .= isset($_POST['f_permission_'.$i]) ? $_POST['f_permission_'.$i] : "0";
		}

		$add_set = "permission = '{$permission_val}', ";
	}

	if(isset($_POST['f_staff_state'])){
        $add_set .= "staff_state = '{$_POST['f_staff_state']}',";
    }

    if(isset($_POST['f_pos_permission'])){
        $add_set .= "pos_permission = '{$_POST['f_pos_permission']}',";
    }

    if($_POST['f_my_c_type']){
        $f_team_post = $_POST['f_team_default'];
        $f_team_list_post = implode(",", $_POST['f_team']);
        if($_POST['f_my_c_type'] != '1'){
            $add_c_type = "my_c_no='0', team=0, team_list=null, my_c_type='{$_POST['f_my_c_type']}', partner_c_no='{$_POST['f_partner_c_no']}',";
        }else{
            $add_c_type = "team= '{$f_team_post}', team_list='{$f_team_list_post}', my_c_type='{$_POST['f_my_c_type']}', partner_c_no=null,";
        }
    }

    $add_cert = "";
    if($_POST['f_cert_no1'] &&  $_POST['f_cert_no2']){
        $cert_no  = trim($_POST['f_cert_no1'])."-".trim($_POST['f_cert_no2']);
        $add_cert = "cert_no = HEX(AES_ENCRYPT('".addslashes(trim($cert_no))."', '{$aeskey}')),";
    }

    if(isset($_POST['f_birthday'])){
        $add_set .= "birthday='{$_POST['f_birthday']}',";
    }

    if(isset($_POST['f_employment_date'])){
        $add_set .= "employment_date='{$_POST['f_employment_date']}',";
    }

    if(isset($_POST['f_retirement_date'])){
        if(!empty($_POST['f_retirement_date'])){
            $add_set .= "retirement_date='{$_POST['f_retirement_date']}',";
        }else{
            $add_set .= "retirement_date=NULL,";
        }

    }

    if(isset($_POST['f_sola_lunar'])){
        $add_set .= "sola_lunar='{$_POST['f_sola_lunar']}',";
    }

    $upd_sql = "
		UPDATE staff SET
			s_name 			= '{$_POST['f_s_name']}',
			`rar` 			= '".addslashes(trim($_POST['f_rar']))."',
			slack_id 		= '{$_POST['f_slack_id']}',
			email		 	= '{$email}',
		    {$add_set}
			{$add_c_type}
			{$add_cert}
			tel 			= '{$_POST['f_tel']}',
			extension 		= '{$_POST['f_extension']}',
			hp 				= '{$_POST['f_hp']}'
		WHERE
			s_no='{$_POST['s_no']}'
	";


	if($my_db->query($upd_sql))
	{
		if($session_s_no == '28')
		{
            if(isset($_POST['f_team_leader']) && !empty($_POST['f_team_leader']))
            {
                $prev_team_upd_sql = "UPDATE team SET team_leader=NULL WHERE team_leader='{$_POST['s_no']}'";
                mysqli_query($my_db, $prev_team_upd_sql);

                foreach($_POST['f_team_leader'] as $f_team_val)
                {
                    $team_upd_sql = "UPDATE team SET team_leader='{$_POST['s_no']}' WHERE team_code='{$f_team_val}'";
                    mysqli_query($my_db, $team_upd_sql);
                }
            }else{
                $team_upd_sql = "UPDATE team SET team_leader=NULL WHERE team_leader='{$_POST['s_no']}'";
                mysqli_query($my_db, $team_upd_sql);
            }
		}

        exit("<script>alert('수정하였습니다.');location.href='staff_regist.php?s_no={$_POST['s_no']}';</script>");
	}else{
        exit("<script>alert('오류가 발생했습니다. 다시 시도해 주세요');location.href='staff_regist.php?s_no={$_POST['s_no']}';</script>");
	}

}
elseif($proc == "f_s_memo")
{
	$s_no		= (isset($_POST['s_no'])) ? $_POST['s_no'] : "";
	$value		= (isset($_POST['f_s_memo'])) ? addslashes($_POST['f_s_memo']) : "";
	$upd_sql	= "UPDATE staff SET s_memo='{$value}', s_memo_date = now() WHERE s_no='{$s_no}'";

	if(mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('메모를 등록하였습니다');location.href='staff_regist.php?s_no={$s_no}';</script>");
	}else{
        exit("<script>alert('메모를 등록에 실패했습니다');location.href='staff_regist.php?s_no={$s_no}';</script>");
	}
}
elseif($proc == "pw_reset")
{
	$s_no				= (isset($_POST['s_no'])) ? $_POST['s_no'] : "";
	$f_staff_pw			= (isset($_POST['staff_pw'])) ? $_POST['staff_pw'] : "";
	$f_staff_pw_new		= (isset($_POST['staff_pw_new'])) ? $_POST['staff_pw_new'] : "";
	$f_staff_pw_confirm = (isset($_POST['staff_pw_confirm'])) ? $_POST['staff_pw_confirm'] : "";

	$getpwd		  	 = substr(md5($f_staff_pw),8,16);
	$pass_chk_sql 	 = "SELECT pass FROM staff WHERE s_no='{$s_no}'";
    $pass_chk_query  = mysqli_query($my_db, $pass_chk_sql);
    $pass_chk_result = mysqli_fetch_array($pass_chk_query);

    $pw_result_msg  = "";

	if($pass_chk_result['pass'] == substr(md5($f_staff_pw),8,16))
	{
		if($f_staff_pw_new == $f_staff_pw_confirm)
		{
			$conv_password  = substr(md5($f_staff_pw_new),8,16);
			$upd_sql 		= "UPDATE staff SET pass='{$conv_password}' WHERE s_no='{$s_no}'";

			if(mysqli_query($my_db, $upd_sql)){
                $pw_result_msg = "비밀번호가 변경되었습니다.";
			}else{
                $pw_result_msg = "비밀번호가 변경에 실패했습니다.";
			}
		}else{
            $pw_result_msg = "변경 비밀번호 확인이 일치하지 않습니다.";
		}
	}
	else if(permissionNameCheck($session_permission, "마스터관리자") || permissionNameCheck($session_permission, "재무관리자"))
	{
		if($f_staff_pw_new == $f_staff_pw_confirm){
            $conv_password = substr(md5($f_staff_pw_new),8,16);
            $upd_sql 	  = "UPDATE staff SET pass='{$conv_password}' WHERE s_no='{$s_no}'";

            if(mysqli_query($my_db, $upd_sql)){
                $pw_result_msg = "비밀번호가 변경되었습니다.";
            }else{
                $pw_result_msg = "비밀번호가 변경에 실패했습니다.";
            }
		}else{
            $pw_result_msg = "변경 비밀번호 확인이 일치하지 않습니다.";
		}
	}else{
        $pw_result_msg = "비밀번호가 일치하지 않습니다.";
	}

    exit("<script>alert('{$pw_result_msg}');location.href='staff_regist.php?s_no={$s_no}';</script>");
}
elseif($proc=="insert_strength")
{
	$s_no 			   = isset($_POST['s_no']) ? $_POST['s_no'] : "";
	$strength_keywords = isset($_POST['strength_keyword']) ? $_POST['strength_keyword'] : "";

	if(!$s_no || !$strength_keywords){
		exit("<script>alert('데이터 오류가 발생했습니다.');location.href='staff_regist.php?s_no={$s_no}';</script>");
	}

	$ins_sql  = "INSERT INTO staff_strength(`s_no`, `keyword_no`, `keyword`, `priority`) VALUES";
	$priority = 1;
	$comma	  = "";

	foreach($strength_keywords as $strength_keyword)
	{
		$ins_sql .= $comma."('{$s_no}', (SELECT k_no FROM staff_strength_keyword WHERE keyword='{$strength_keyword}'), '{$strength_keyword}', '{$priority}')";
		$comma    = " , ";
		$priority++;
	}

	if(!mysqli_query($my_db, $ins_sql))
	{
		exit("<script>alert('강점키워드 등록에 실패했습니다.');location.href='staff_regist.php?s_no={$s_no}';</script>");
	}else{
		exit("<script>alert('강점키워드 등록에 성공했습니다.');location.href='staff_regist.php?s_no={$s_no}';</script>");
	}

}
elseif($proc=="update_strength")
{
	$s_no 			   = isset($_POST['s_no']) ? $_POST['s_no'] : "";
	$strength_keywords = isset($_POST['strength_keyword']) ? $_POST['strength_keyword'] : "";

	if(!$s_no || !$strength_keywords){
		exit("<script>alert('데이터 오류가 발생했습니다.');location.href='staff_regist.php?s_no={$s_no}';</script>");
	}

	$update_sql_list = [];
	$priority 		 = 1;
	foreach($strength_keywords as $strength_keyword)
	{
		$update_sql_list[] = "UPDATE staff_strength SET keyword_no=(SELECT k_no FROM staff_strength_keyword WHERE keyword='{$strength_keyword}'), keyword='{$strength_keyword}' WHERE s_no='{$s_no}' AND priority='{$priority}'";
		$priority++;
	}

	if(!empty($update_sql_list))
	{
		try{
			foreach($update_sql_list as $update_sql){
				$my_db->autocommit(false);
				if(!$my_db->query($update_sql)){
					throw new Exception('query 오류');
				}
			}
			$my_db->commit();
		}catch(Exception $e){
			$my_db->rollback();
			exit("<script>alert('강점키워드 변경에 실패했습니다.');location.href='staff_regist.php?s_no={$s_no}';</script>");
		}
		exit("<script>alert('강점키워드 변경에 성공했습니다.');location.href='staff_regist.php?s_no={$s_no}';</script>");
	}else{
		exit("<script>alert('데이터 없습니다');location.href='staff_regist.php?s_no={$s_no}';</script>");
	}
}
elseif($proc=="add_profile_img")
{
    $s_no  		 = isset($_POST['s_no']) ? $_POST['s_no'] : "";
    $profile_img = isset($_FILES['profile_img']) ? $_FILES['profile_img'] : "";

    if(!$s_no){
        exit("<script>alert('데이터 오류가 발생했습니다.');location.href='staff_list.php';</script>");
	}elseif(!$profile_img){
        exit("<script>alert('데이터 오류가 발생했습니다.');location.href='staff_regist.php?s_no={$s_no}';</script>");
    }

    $profile_path = add_store_file($profile_img, "staff");
    $upd_sql	  = "UPDATE staff SET profile_img='{$profile_path}' WHERE s_no='{$s_no}'";

    if(mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('프로필 이미지를 등록하였습니다');location.href='staff_regist.php?s_no={$s_no}';</script>");
    }else{
        exit("<script>alert('프로필 이미지 등록에 실패했습니다');location.href='staff_regist.php?s_no={$s_no}';</script>");
    }

}
elseif($proc=="update_mbti")
{
    $s_no 	= isset($_POST['s_no']) ? $_POST['s_no'] : "";
    $mbti   = isset($_POST['f_mbti']) ? $_POST['f_mbti'] : "";

    if(!$s_no || !$mbti){
        exit("<script>alert('데이터 오류가 발생했습니다.');location.href='staff_regist.php?s_no={$s_no}';</script>");
    }

    $upd_sql = "UPDATE staff SET mbti='{$mbti}' WHERE s_no='{$s_no}'";

    if(mysqli_query($my_db, $upd_sql))
    {
        exit("<script>alert('MBTI 변경에 성공했습니다.');location.href='staff_regist.php?s_no={$s_no}';</script>");
    }else{
        exit("<script>alert('MBTI 변경에 실패했습니다');location.href='staff_regist.php?s_no={$s_no}';</script>");
    }
}
elseif($proc == "deactivate_staff")
{
    $retire_s_no 	    = isset($_POST['s_no']) ? $_POST['s_no'] : "";

    # 퇴사자 알림
    $staff_chk_sql      = "SELECT s_no, s_name, team, (SELECT t.team_leader FROM team t WHERE t.team_code=s.team) as t_leader FROM staff s WHERE staff_state = '1' AND s_no='{$retire_s_no}'";
    $staff_chk_query    = mysqli_query($my_db, $staff_chk_sql);
    $staff_chk_result   = mysqli_fetch_assoc($staff_chk_query);

    if(!isset($staff_chk_result['s_no'])){
        exit("<script>alert('이미 퇴사 처리 됬거나 없는 Staff 번호 입니다.');location.href='staff_regist.php?s_no={$s_no}';</script>");
    }

    $retire_s_name      = $staff_chk_result['s_name'];

    # 윤영님 알림
    $master_chk_sql     = "SELECT COUNT(wcc_no) as cnt FROM waple_chat_content WHERE wc_no='2' AND s_no='18' AND alert_type='31' AND alert_check='RETIRE_{$retire_s_no}'";
    $master_chk_query   = mysqli_query($my_db, $master_chk_sql);
    $master_chk_result  = mysqli_fetch_assoc($master_chk_query);
    if($master_chk_result['cnt'] == 0)
    {
        $master_chk_msg = "[와플 보안 주의]\r\n퇴사자 {$retire_s_name}에 대한 와플 외의 권한을 반드시 해지 해 주시기 바랍니다.\r\nex) 외부 권한 = 페이스북, 구글ADS, e카운트, 은행/카드사 등 와플 권한(하드코딩 부분) = 매출및비용관리, UTM";
        $master_chk_msg = addslashes($master_chk_msg);
        $chk_ins_sql    = "INSERT INTO waple_chat_content SET wc_no='2', s_no='18', content='{$master_chk_msg}', alert_type='31', alert_check='RETIRE_{$retire_s_no}', regdate=now()";
        mysqli_query($my_db, $chk_ins_sql);
    }

     # 팀리더 알림
    $t_leader_s_no  = $staff_chk_result['t_leader'];
    if($t_leader_s_no > 0)
    {
        $chat_chk_sql    = "SELECT COUNT(wcc_no) as cnt FROM waple_chat_content WHERE wc_no='2' AND s_no='{$t_leader_s_no}' AND alert_type='31' AND alert_check='RETIRE_{$retire_s_no}'";
        $chat_chk_query  = mysqli_query($my_db, $chat_chk_sql);
        $chat_chk_result = mysqli_fetch_assoc($chat_chk_query);

        if($chat_chk_result['cnt'] == 0)
        {
            $retire_chk_msg = "[와플 보안 주의]\r\n퇴사자 {$retire_s_name}에 대한 와플 외의 권한을 반드시 해지 해 주시기 바랍니다.\r\nex) 외부 권한 = 페이스북, 구글ADS, e카운트, 은행/카드사 등 와플 권한(하드코딩 부분) = 매출및비용관리, UTM";
            $retire_chk_msg = addslashes($retire_chk_msg);
            $chk_ins_sql    = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$t_leader_s_no}', content='{$retire_chk_msg}', alert_type='31', alert_check='RETIRE_{$retire_s_no}', regdate=now()";
            mysqli_query($my_db, $chk_ins_sql);
        }
    }

    # 자산승인 반납요청 알림
    $retire_asset_manager_sql   = "SELECT asr.req_name, asr.manager, (SELECT s.s_name FROM staff s WHERE s.s_no=asr.manager) as manager_name, asr.sub_manager, (SELECT s.s_name FROM staff s WHERE s.s_no=asr.sub_manager) as sub_manager_name FROM asset_reservation as asr LEFT JOIN asset as `a` ON a.as_no=asr.as_no WHERE asr.work_state='2' AND asr.req_no='{$retire_s_no}' AND a.share_type IN(1,2)";
    $retire_asset_manager_query = mysqli_query($my_db, $retire_asset_manager_sql);
    $retire_asset_manager_list  = [];
    while($retire_asset_manager = mysqli_fetch_assoc($retire_asset_manager_query))
    {
        $retire_asset_manager_list[$retire_asset_manager['manager']] = array("manager" => $retire_asset_manager['manager'], "manager_name" => $retire_asset_manager['manager_name']);
        if($retire_asset_manager['sub_manager'] > 0){
            $retire_asset_manager_list[$retire_asset_manager['sub_manager']] = array("manager" => $retire_asset_manager['sub_manager'], "manager_name" => $retire_asset_manager['sub_manager_name']);
        }
    }

    if(!empty($retire_asset_manager_list))
    {
        foreach($retire_asset_manager_list as $as_manager_data)
        {
            $as_manager          = $as_manager_data['manager'];
            $as_manager_name     = $as_manager_data['manager_name'];
            $retire_asset_sql    = "SELECT count(as_r_no) as cnt, req_name FROM asset_reservation as asr LEFT JOIN asset as `a` ON a.as_no=asr.as_no WHERE asr.work_state='2' AND asr.req_no='{$retire_s_no}' AND a.share_type IN(1,2) AND (asr.manager='{$as_manager}' OR asr.sub_manager='{$as_manager}')";
            $retire_asset_query  = mysqli_query($my_db, $retire_asset_sql);
            $retire_asset_result = mysqli_fetch_assoc($retire_asset_query);

            if($retire_asset_result['cnt'] > 0)
            {
                $chat_chk_sql    = "SELECT COUNT(wcc_no) as cnt FROM waple_chat_content WHERE wc_no='2' AND s_no='{$as_manager}' AND alert_type='32' AND alert_check='RETIRE_ASSET_{$retire_s_no}'";
                $chat_chk_query  = mysqli_query($my_db, $chat_chk_sql);
                $chat_chk_result = mysqli_fetch_assoc($chat_chk_query);

                if($chat_chk_result['cnt'] == 0)
                {
                    $retire_asset_chk_msg = "[와플 자산 반납 안내]\r\n퇴사자 {$retire_s_name}에 대한 사용중인 와플자산 {$retire_asset_result['cnt']}건을 모두 반납처리 해 주세요.\r\n※중요 보안 대상의 계정 자산의 경우 비밀번호 변경을 진행해주세요.";
                    $retire_asset_chk_msg.= "\r\nhttps://work.wplanet.co.kr/v1/asset_reservation.php?sch_work_state=2&sch_manager={$as_manager_name}&sch_req_name={$retire_s_name}";
                    $retire_asset_chk_msg = addslashes($retire_asset_chk_msg);
                    $chk_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$as_manager}', content='{$retire_asset_chk_msg}', alert_type='32', alert_check='RETIRE_ASSET_{$retire_s_no}', regdate=now()";
                    mysqli_query($my_db, $chk_ins_sql);
                }
            }
        }
    }

    $upd_sql = "UPDATE staff SET staff_state='3' WHERE s_no='{$retire_s_no}'";
    if(mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('퇴사 처리에 성공했습니다.');location.href='staff_regist.php?s_no={$retire_s_no}';</script>");
    }else{
        exit("<script>alert('퇴사 처리에 실패했습니다');location.href='staff_regist.php?s_no={$retire_s_no}';</script>");
    }
}
elseif(!empty($s_no))
{
    $regist_mode = "modify";
    $submit_btn  = "수정하기";
    $delete_btn  = "";
    $to_year     = date("Y");

    $team_leader_total_list = getTeamLeader($my_db);
    $team_leader_list       = isset($team_leader_total_list[$session_s_no]) ? $team_leader_total_list[$session_s_no] : [];
    $aes_unhex = "(SELECT AES_DECRYPT(UNHEX(s.cert_no), '{$aeskey}')) AS cert_no";
    $staff_sql = "
		SELECT
			*,
			(SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=s.my_c_no) as my_c_name,
			(SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=s.partner_c_no) as partner_c_name,
			(SELECT team_name FROM team where team_code=s.team) as team_name,
			{$aes_unhex},
			(SELECT GROUP_CONCAT(ss.keyword SEPARATOR ' ∙ ') FROM staff_strength AS ss WHERE ss.s_no = s.s_no GROUP BY ss.s_no ORDER BY ss.priority ASC) as strength_keyword,
			(SELECT GROUP_CONCAT(t.team_code) FROM team AS t WHERE t.team_leader=s.s_no group by team_leader) as t_leader
		FROM staff s
		WHERE s.s_no='{$s_no}'"
    ;
    $staff_query = mysqli_query($my_db, $staff_sql);
    $staff = mysqli_fetch_array($staff_query);

    if(!isset($staff['s_no']) || empty($staff['s_no'])){
        exit("<script>alert('데이터가 없습니다');location.href='staff_list.php';</script>");
	}

    # 데이터 변환( Email, 권한, 음력생일)
    $emails = explode("@", $staff['email']);

    $f_permissions = [];
    for($i = -1 ; $i <= strlen($staff['permission']) ; $i++) {
        $f_permissions[] = array(
            'val' => $staff['permission'][$i]
        );
    }

    $luna_birthday = "";
    if($staff['sola_lunar'] == '2'){ // 음력의 경우 금년 생일 계산
        $birthday_m_d = date("md", strtotime($staff['birthday']));
        $Sola           = LunarToSola($to_year.$birthday_m_d); // 올해 음력 생일을 양력으로 계산
        $luna_birthday  = $Sola['month'] . "월 " . $Sola['day'] . "일";
    }

    #팀리스트
    $f_team_chk_list = [];
    $f_team_list = [];
    if(isset($staff['team_list']) && !empty($staff['team_list']))
    {
        $f_team_list_val = explode(",", $staff['team_list']);
        $f_team_key 	 = array_search($staff['team'], $f_team_list_val);
        array_splice($f_team_list_val, $f_team_key, 1);
        $f_team_chk_list[] = $staff['s_no'].'_'.$staff['team'];

        if(!empty($f_team_list_val))
        {
            foreach ($f_team_list_val as $team_code){
                $f_team_sql = "SELECT depth FROM team WHERE team_code='{$team_code}' LIMIT 1";
                $f_team_query = mysqli_query($my_db, $f_team_sql);
                $f_team_result = mysqli_fetch_assoc($f_team_query);
                $f_team_depth = isset($f_team_result['depth']) ? $f_team_result['depth'] : 0;
                $f_team_list[$team_code] = getTeamFullName($my_db, $f_team_depth, $team_code);
                $f_team_chk_list[] = $staff['s_no'].'_'.$team_code;
            }
        }
    }

    $f_team_leader = [];
    if(!empty($staff['t_leader']))
    {
    	$f_team_leader = explode(',', $staff['t_leader']);
	}

    $is_leader = false;

    if(!empty($team_leader_list) && in_array($staff['team'], $team_leader_list))
    {
        $is_leader = true;
    }

    $prd_req_list = [];
    $prd_run_list = [];
    if(!empty($f_team_chk_list))
    {
        foreach($f_team_chk_list as $f_team_chk)
        {
            $prd_staff_sql   = "SELECT prd_no, title, task_req_staff, task_run_staff, (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=p.k_name_code)) AS prd1_name, (SELECT k_name FROM kind k WHERE k.k_name_code=p.k_name_code) AS prd2_name, display FROM product as p WHERE (FIND_IN_SET('{$f_team_chk}',task_req_staff) > 0 OR FIND_IN_SET('{$f_team_chk}',task_run_staff) > 0) ORDER BY prd_no ASC";
            $prd_staff_query = mysqli_query($my_db, $prd_staff_sql);
            while($prd_staff_result = mysqli_fetch_assoc($prd_staff_query))
            {
                if(!empty($prd_staff_result['task_req_staff'])){
                    $prd_staff_req_list = explode(',', $prd_staff_result['task_req_staff']);
                    if(in_array($f_team_chk, $prd_staff_req_list)){
                        $req_display_state = "";
                        if($prd_staff_result['display'] == '2')
                          $req_display_state = " [OFF]";

                        $prd_req_list[$prd_staff_result['prd_no']] = $prd_staff_result['prd1_name']." > ".$prd_staff_result['prd2_name']." > ".$prd_staff_result['title'].$req_display_state;
                    }
                }

                if(!empty($prd_staff_result['task_run_staff']))
                {
                    $prd_staff_run_list = explode(',', $prd_staff_result['task_run_staff']);
                    if(in_array($f_team_chk, $prd_staff_run_list)){
                      $run_display_state = "";
                      if($prd_staff_result['display'] == '2')
                        $run_display_state = " [OFF]";
                        $prd_run_list[$prd_staff_result['prd_no']] = $prd_staff_result['prd1_name']." > ".$prd_staff_result['prd2_name']." > ".$prd_staff_result['title'].$run_display_state;
                    }
                }
            }
        }
    }

    if(!empty($staff['cert_no'])){
        $f_cert_no = explode('-', $staff['cert_no']);
        $f_cert_no2 = "*******";
        if($session_s_no == '73' || $session_s_no == '233'){
            $f_cert_no2 = $f_cert_no[1];
        }
        $smarty->assign("f_cert_no_1", $f_cert_no[0]);
        $smarty->assign("f_cert_no_2", $f_cert_no2);
    }

    $smarty->assign(
        array(
            "s_no"			=> $staff['s_no'],
            "f_permission"	=> $f_permissions,
            "f_staff_state"	=> $staff['staff_state'],
            "f_rar"	        => stripslashes($staff['rar']),
            "f_id"			=> $staff['id'],
            "f_s_name"		=> $staff['s_name'],
            "f_my_c_no"		=> $staff['my_c_no'],
            "f_my_c_name"	=> $staff['my_c_name'],
            "f_my_c_type"	=> $staff['my_c_type'],
            "f_partner_c_no"    => $staff['partner_c_no'],
            "f_partner_c_name"  => $staff['partner_c_name'],
            "f_team"		=> $staff['team'],
            "f_team_name"	=> $staff['team_name'],
            "f_team_list"	=> $f_team_list,
            "f_email"		=> $staff['email'],
            "f_email_0"		=> $emails[0],
            "f_email_1"		=> $emails[1],
            "f_addr"	    => $staff['addr'],
            "f_position"	=> $staff['position'],
            "f_pos_permission" => $staff['pos_permission'],
            "f_slack_id"	=> $staff['slack_id'],
            "f_tel"			=> $staff['tel'],
            "f_extension"	=>  $staff['extension'],
            "f_hp"			=> $staff['hp'],
            "f_birthday"	=> $staff['birthday'],
            "f_sola_lunar"	=> $staff['sola_lunar'],
            "f_employment_date" => $staff['employment_date'],
            "f_retirement_date" => $staff['retirement_date'],
            "f_s_memo"		=> stripslashes($staff['s_memo']),
            "f_s_memo_day"	=> date("Y/m/d",strtotime($staff['s_memo_date'])),
            "f_s_memo_time"	=> date("H:i",strtotime($staff['s_memo_date'])),
            "f_strength_keyword" => $staff['strength_keyword'],
            "f_profile_img"	=> $staff['profile_img'],
			"luna_birthday" => $luna_birthday,
			"f_team_leader" => $f_team_leader,
			"f_mbti"		=> $staff['mbti'],
			"is_leader"	 	=> $is_leader,
            "f_prd_req_list"=> $prd_req_list,
            "f_prd_run_list"=> $prd_run_list
        )
    );

    #계정등록 퍼미션 체크
	if(!permissionNameCheck($session_permission,"대표") && !permissionNameCheck($session_permission,"마스터관리자") && !permissionNameCheck($session_permission,"재무관리자") && $session_s_no !=  $staff['s_no'] && !$is_leader){
        $smarty->display('access_error.html');
        exit;
	}

    $strength_keyword_sql    = "SELECT keyword, priority FROM staff_strength WHERE s_no = '{$s_no}' ORDER BY priority ASC";
    $strength_keyword_query  = mysqli_query($my_db, $strength_keyword_sql);
    while($strength_keyword_result = mysqli_fetch_assoc($strength_keyword_query))
    {
        $priority = $strength_keyword_result['priority'];
        if(isset($strength_keyword_list[$priority]))
        {
            $strength_keyword_list[$priority] = $strength_keyword_result['keyword'];
        }
    }


}else{
    # 등록시 퍼미션 체크
    if (!(permissionNameCheck($session_permission,"마스터관리자") || permissionNameCheck($session_permission,"재무관리자")))
    {
        $smarty->display('access_error.html');
        exit;
    }
}

$my_company_model   = MyCompany::Factory();
$my_company_option  = $my_company_model->getNameList();

$smarty->assign("s_no", $s_no);
$smarty->assign("regist_mode", $regist_mode);
$smarty->assign("submit_btn", $submit_btn);
$smarty->assign("delete_btn", $delete_btn);
$smarty->assign("to_year", $to_year);
$smarty->assign("company_addr_option", getCompanyAddrOption());
$smarty->assign("my_company_option", $my_company_option);
$smarty->assign("pos_permission_option", getPosPermissionOption());
$smarty->assign("strength_keyword_list", $strength_keyword_list);

// 템플릿에 해당 입력값 던져주기
$smarty->display('staff_regist.html');

?>
