<?php
require('inc/common.php');
require('ckadmin.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

# 변수 설정
$pj_no      = isset($_GET['pj_no']) ? $_GET['pj_no'] : "";
$del        = isset($_GET['del']) ? $_GET['del'] : "";
$list_url   = "project_staff_regist_input_iframe.php";
$editable   = false;
$project_staff_list  = [];

if(!empty($del)){
    $smarty->assign("del", $del);
}

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'project_output_staff')
{
    $pj_no   = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_r_no = isset($_POST['pj_r_no']) ? $_POST['pj_r_no'] : "";
    $out_sql = "UPDATE `project_input_report` SET active='2' WHERE pj_r_no='{$pj_r_no}'";

    if(!mysqli_query($my_db, $out_sql)){
        alert("투입인력에서 삭제처리에 실패했습니다", "{$list_url}?pj_no={$pj_no}");
    }else{
        alert("투입인력에서 삭제처리 했습니다", "{$list_url}?pj_no={$pj_no}&del=1");
    }
}
elseif($process == 'pir_s_date')
{
    $pj_r_no = isset($_POST['pj_r_no']) ? $_POST['pj_r_no'] : "";
    $value   = isset($_POST['value']) ? $_POST['value'] : "";
    $upd_sql = "UPDATE `project_input_report` SET pir_s_date='{$value}' WHERE pj_r_no='{$pj_r_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "투입시작날짜 저장에 실패 하였습니다.";
    else
        echo "투입시작날짜가 변경 되었습니다.";
    exit;
}
elseif($process == 'pir_e_date')
{
    $pj_r_no = isset($_POST['pj_r_no']) ? $_POST['pj_r_no'] : "";
    $value   = isset($_POST['value']) ? $_POST['value'] : "";
    $upd_sql = "UPDATE `project_input_report` SET pir_e_date='{$value}' WHERE pj_r_no='{$pj_r_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "투입종료날짜 저장에 실패 하였습니다.";
    else
        echo "투입종료날짜가 저장 되었습니다.";
    exit;
}
elseif($process == 'resource')
{
    $pj_r_no    = isset($_POST['pj_r_no']) ? $_POST['pj_r_no'] : "";
    $pj_s_no    = isset($_POST['pj_s_no']) ? $_POST['pj_s_no'] : "";
    $pir_s_date = isset($_POST['pir_s_date']) ? $_POST['pir_s_date'] : "";
    $pir_e_date = isset($_POST['pir_e_date']) ? $_POST['pir_e_date'] : "";
    $resource   = isset($_POST['value']) ? $_POST['value'] : "";

    /**
     *  기존 사업전체 투입률 검색
     *  $res_sql    = "SELECT SUM(pir.resource) as total FROM project_input_report as pir WHERE pir.pj_r_no!='{$pj_r_no}' AND pir.pj_s_no = '{$pj_s_no}' AND pir.pj_no NOT IN(SELECT pj.pj_no FROM project pj WHERE pj.state='3') AND pir.active='1' AND ((pir.pir_s_date BETWEEN '{$pir_s_date}' AND '{$pir_e_date}' OR pir.pir_e_date BETWEEN '{$pir_s_date}' AND '{$pir_e_date}') OR (pir.pir_s_date <= '{$pir_s_date}' AND pir.pir_e_date >= '{$pir_e_date}'))";
     */
    $res_sql    = "SELECT SUM(pir.resource) as total FROM project_input_report as pir WHERE pir.pj_r_no!='{$pj_r_no}' AND pir.pj_s_no = '{$pj_s_no}' AND pir.pj_no=(SELECT sub.pj_no FROM project_input_report as sub WHERE sub.pj_r_no='{$pj_r_no}' LIMIT 1)";
    $res_query  = mysqli_query($my_db, $res_sql);
    $res_result = mysqli_fetch_assoc($res_query);
    $res_total  = isset($res_result['total']) && !empty($res_result['total']) ? $res_result['total'] : 0;
    $res_total += $resource;

    $out_sql  = "UPDATE `project_input_report` SET resource='{$resource}' WHERE pj_r_no='{$pj_r_no}'";

    if($res_total <= 100)
    {
        if(!mysqli_query($my_db, $out_sql)){
            $data = array("result" => '2', "msg" => "리소스 저장에 실패했습니다");
        }else{
            $data = array("result" => '1', "msg" => "리소스를 저장했습니다");
        }
    }else{
        $data = array("result" => '3', "msg" => "투입리소스가 가용리소스를 초과 합니다.");
    }

    echo json_encode($data, JSON_UNESCAPED_UNICODE);
    exit;
}
else
{
    $project_staff_sql     = "SELECT *,(SELECT ps.s_name FROM project_staff ps WHERE ps.pj_s_no = pir.pj_s_no) as s_name, (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=pir.my_c_no) as my_c_name FROM project_input_report `pir` WHERE pir.pj_no='{$pj_no}' AND pir.active='1'";
    $project_staff_query   = mysqli_query($my_db, $project_staff_sql);
    $project_staff_list    = [];
    while($project_staff = mysqli_fetch_assoc($project_staff_query))
    {
        $project_staff_list[] = $project_staff;
    }

    if($pj_no)
    {
        $project_sql = "SELECT * FROM project WHERE pj_no='{$pj_no}'";
        $project_query = mysqli_query($my_db, $project_sql);
        $project       = mysqli_fetch_assoc($project_query);

        if($project['team'] == $session_team || $session_s_no == '62'){
            $editable = true;
        }

        $smarty->assign('project', $project);
    }
}

$smarty->assign("project_staff_list", $project_staff_list);
$smarty->assign("editable", $editable);

$smarty->display('project_staff_regist_input_iframe.html');
?>
