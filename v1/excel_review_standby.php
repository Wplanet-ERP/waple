<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
),
),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "체크상태")
->setCellValue('B1', "완료보고일")
->setCellValue('C1', "포스팅URL")
->setCellValue('D1', "포스팅제목")
->setCellValue('E1', "포스팅 후 한마디")
->setCellValue('F1', "CODE")
->setCellValue('G1', "구분")
->setCellValue('H1', "업체명")
->setCellValue('I1', "담당자")
->setCellValue('J1', "리뷰마감")
->setCellValue('k1', "카페아이디")
->setCellValue('l1', "닉네임")
->setCellValue('m1', "이름")
->setCellValue('n1', "메모")
->setCellValue('o1', "마지막 평가");

$kind=array(
				'name'=>array(
					'',
					'체험단',
					'기자단',
					'배송체험'
				),
				'url'=>array(
					'',
					'consumer',
					'press',
					'delivery'
				)
			);


// all 이 아니면? kind 포함시키기 : 기본검색으로 지정하지 않았을 경우 where 절을 출력하기 위해서 사용.
if($page_code!="all") {
	$add_where=" where kind=".$page_code." ";
	$_GET['skind']=$page_code;
}

$kind=array(
				'name'=>array(
					'',
					'체험단',
					'기자단',
					'배송체험'
				),
				'url'=>array(
					'',
					'consumer',
					'press',
					'delivery'
				)
			);

$check=array(
				'name'=>array(
					'',
					'체크대기',
					'체크완료',
					'체크취소'
				),
				'url'=>array(
					'',
					'consumer',
					'press',
					'delivery'
				)
			);



// all 이 아니면? kind 포함시키기 : 기본검색으로 지정하지 않았을 경우 where 절을 출력하기 위해서 사용.
if($page_code!="all") {
	$add_where=" where kind=".$page_code." ";
	$_GET['skind']=$page_code;
}




// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";
$field=array();
$keyword=array();
$where=array();

if (isset($_GET['chk_status'])){
$search_status=$_GET['chk_status'];                     
}

$search_post_title=isset($_GET['post_title'])?$_GET['post_title']:"";

if(!empty($search_status)) {
	$field[]="r_status";
	$keyword[]=$search_status;
	$where[]="(status='".$search_status."')";
}

if(!empty($search_post_title)) {
	$field[]="post_title";
	$keyword[]=$search_post_title;
	$where[]="post_title like '%".$search_post_title."%'";
}

if(sizeof($where)>0) {
	$add_where=" where ";
	for($i=0;$i<sizeof($where);$i++) {
		$add_where.=$where[$i].(($i<(sizeof($where)-1))?" and ":"");
		$smarty->assign($field[$i],$keyword[$i]);
	}
}




// 정렬순서 토글 & 필드 지정
$add_orderby="";
$order=isset($_GET['od'])?$_GET['od']:"";
$order_type=isset($_GET['by'])?$_GET['by']:"";
$toggle=$order_type?"asc":"desc";
$order_field=array('','reg_sdate','reg_edate','awd_date','exp_edate','pres_reward');
if($order && $order<6) {
	$add_orderby.="$order_field[$order] $toggle,";
	$smarty->assign("order",$order);
	$smarty->assign("order_type",$order_type);
}
$add_orderby.=" status asc";

// 리스트 내용
$i=2;
$ttamount=0;
//$rs = mysqli_query($my_db, "SELECT s_no,state,id,s_name,team,position,email,tel,hp FROM staff");
$report_sql="
				select *,
				(select kind from promotion p where p.p_no=r.p_no) as kind,
				(select company from promotion p where p.p_no=r.p_no) as company,
				(select name from promotion p where p.p_no=r.p_no) as staff_name,
				(select reg_edate from promotion p where p.p_no=r.p_no) as reg_edate,
				(select cafe_id from blog b where b.b_no=r.b_no) as cafe_id,
				(select nick from blog b where b.b_no=r.b_no) as nick,
				(select username from blog b where b.b_no=r.b_no) as username
				from 
					report r
				left join 
					promotion p
					on r.p_no=p.p_no
				$add_where 
				order by $add_orderby
				";
//exit($promotion_sql);
//echo "<br><br><br><br><br><br><br><br>";
//echo $report_sql;

$report_query= mysqli_query($my_db, $report_sql);
while($dt = mysqli_fetch_array($report_query)){
	// Add some data
	switch ($dt[kind]) {
		case '1' :
					 $kind_name = "체험단";
					 break;
		case '2' :
					 $kind_name = "기자단";
					 break;
		case '3' :
					 $kind_name = "배송체험";
					 break;
	}switch ($dt[status]) {
		case '1' :
					 $status_name = "체크대기";
					 break;
		case '2' :
					 $status_name = "체크완료";
					 break;
	}
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$i, $status_name)
	->setCellValue('B'.$i, $dt[reg_edate])
	->setCellValue('C'.$i, $dt[post_url])
	->setCellValue('D'.$i, $dt[post_title])
	->setCellValue('E'.$i, $dt[memo])
	->setCellValue('F'.$i, $dt[code])
	->setCellValue('G'.$i, $kind_name)
	->setCellValue('H'.$i, $dt[company])
	->setCellValue('I'.$i, $dt[staff_name])
	->setCellValue('J'.$i, $dt[reg_edate])
	->setCellValue('K'.$i, $dt[cafe_id])
	->setCellValue('L'.$i, $dt[nick])
	->setCellValue('M'.$i, $dt[username])
	->setCellValue('N'.$i, $dt[r_memo])
	->setCellValue('O'.$i, $dt[reload]);
	$i = $i+1;
}
/*$reports[] = array(
		"no"=>$report_array['r_no'],
		"post_url"=>$report_array['post_url'],
		"post_title"=>$report_array['post_title'],
		"memo"=>$report_array['memo'],
		"r_date_ymd"=>date("Y.m.d",strtotime($report_array['r_datetime'])),
		"r_date_hi"=>date("H:i",strtotime($report_array['r_datetime'])),
		"p_no"=>$report_array['p_no'],
		"c_no"=>$report_array['c_no'],
		/*"reg_start"=>date("n/d(".$datey[date("w",strtotime($report_array['reg_sdate']))].")",strtotime($report_array['reg_sdate'])),
		"reg_end"=>date("n/d(".$datey[date("w",strtotime($report_array['reg_edate']))].")",strtotime($report_array['reg_edate'])),
		"awd_date"=>$report_array['awd_date'],
		"award"=>date("n/d(".$datey[date("w",strtotime($report_array['awd_date']))].")",strtotime($report_array['awd_date'])),
		"pres_reward"=>$pres_reward,
		"awd_complete"=>(($report_array['p_state']>3)?"10/10":"-"),
		"reg_num"=>$report_array['reg_num'],
		"s_no"=>$report_array['s_no'],
		"a_no"=>$report_array['a_no'],
		"b_no"=>$report_array['b_no'],
		"p_memo"=>$report_array['p_memo'],
		"kind"=>$report_array['kind'],
		"kind_name"=>$kind['name'][$report_array['kind']],
		"company"=>$report_array['company'],
		"staff_name"=>$report_array['staff_name'],
		"reg_edate"=>$report_array['reg_edate'],
		"cafe_id"=>$report_array['cafe_id'],
		"nick"=>$report_array['nick'],
		"username"=>$report_array['username'],
		"p_memo"=>$report_array['p_memo'],
		"r_memo"=>$report_array['r_memo'],
		"limit_yn"=>$report_array['limit_yn'],
		"reload"=>((empty($report_array['reload']))?"____/__/__":date("Y/m/d H:i:s",strtotime($report_array['reload']))),
	);*/

$objPHPExcel->getActiveSheet()->getStyle('A1:O'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('00F3F3F6');


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setTitle('리뷰 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="review_standby_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;