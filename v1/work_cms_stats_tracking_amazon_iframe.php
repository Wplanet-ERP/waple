<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/work_sales.php');

# 검색조건
$add_where          = "1=1 AND log_c_no='5711'";
$prev_month         = date('Y-m', strtotime("-1 months"));
$start_month        = date('Y-m', strtotime("{$prev_month} -3 months"));
$sch_log_type       = isset($_GET['sch_log_type']) ? $_GET['sch_log_type'] : "amazon";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_run_s_month    = isset($_GET['sch_run_s_month']) ? $_GET['sch_run_s_month'] : $start_month;
$sch_run_e_month    = isset($_GET['sch_run_e_month']) ? $_GET['sch_run_e_month'] : $prev_month;

if(empty($sch_run_s_month) || empty($sch_run_e_month))
{
    echo "날짜 검색 값이 없으면 차트 생성이 안됩니다.";
    exit;
}

$smarty->assign("sch_log_type", $sch_log_type);
$smarty->assign("sch_run_s_month", $sch_run_s_month);
$smarty->assign("sch_run_e_month", $sch_run_e_month);

if(!empty($sch_run_s_month)) {
    $add_where          .= " AND settle_month >= '{$sch_run_s_month}'";
}

if(!empty($sch_run_e_month)){
    $add_where         .= " AND settle_month <= '{$sch_run_e_month}'";
}

# 사용 Array
$track_total_list    = [];
$track_delivery_list = [];
$track_etc_list      = [];
$track_gubun_option  = getSettleGubunOption();
$track_etc_name_list = $track_gubun_option;
unset($track_etc_name_list[2]);

# 브랜드 옵션
if(!empty($sch_brand)) {
    $add_where         .= " AND c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $add_where         .= " AND c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $add_where         .= " AND c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

# 날짜 생성
$all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_run_s_month}' AND '{$sch_run_e_month}'";
$all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
$all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y/%m')";
$add_key_column  = "REPLACE(settle_month,'-','')";

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key
";
$all_date_query         = mysqli_query($my_db, $all_date_sql);
$track_date_name_list   = [];
while($all_date = mysqli_fetch_assoc($all_date_query))
{
    $track_date_name_list[$all_date['chart_key']] = $all_date['chart_title'];
    foreach($track_etc_name_list as $etc_key => $etc_name){
        $track_etc_list[$etc_key][$all_date['chart_key']] = 0;
    }

    $track_total_list[$all_date['chart_key']]       = 0;
    $track_delivery_list[$all_date['chart_key']]    = 0;
}

# 물류비 데이터 생성
$work_track_sql = "
    SELECT
        settle_gubun,
        {$add_key_column} as key_month,
        SUM(settle_price_vat) as total_value
    FROM work_cms_settlement 
    WHERE {$add_where}
    GROUP BY settle_gubun, key_month
    ORDER BY settle_gubun, key_month
";
$work_track_query = mysqli_query($my_db, $work_track_sql);
while($work_track = mysqli_fetch_assoc($work_track_query))
{
    if($work_track['settle_gubun'] == "2"){
        $track_delivery_list[$work_track['key_month']] += $work_track['total_value'];
    }else{
        $track_etc_list[$work_track['settle_gubun']][$work_track['key_month']] += $work_track['total_value'];
    }

    $track_total_list[$work_track['key_month']] += $work_track['total_value'];
}


$chart_idx = 0;
$track_etc_chart_list = [];
foreach($track_etc_name_list as $chart_key => $chart_label)
{
    $track_etc_chart_list[$chart_idx] = array(
        "title" => $chart_label,
        "type"  => "line",
        "data"  => array()
    );

    foreach($track_date_name_list as $date_key => $date_name)
    {
        $total_value = isset($track_etc_list[$chart_key][$date_key]) ? $track_etc_list[$chart_key][$date_key] : 0;
        $track_etc_chart_list[$chart_idx]['data'][] = $total_value;
    }
    $chart_idx++;
}

$smarty->assign("track_date_name_list", $track_date_name_list);
$smarty->assign("track_date_chart_name_list", json_encode($track_date_name_list));
$smarty->assign("track_date_cnt", count($track_date_name_list));
$smarty->assign("track_delivery_cnt", count($track_date_name_list)+1);

$smarty->assign("track_total_list", $track_total_list);
$smarty->assign("track_total_chart_list", json_encode($track_total_list));

$smarty->assign("track_delivery_list", $track_delivery_list);
$smarty->assign("track_delivery_chart_list", json_encode($track_delivery_list));

$smarty->assign("track_etc_list", $track_etc_list);
$smarty->assign("track_etc_name_list", $track_etc_name_list);
$smarty->assign("track_etc_chart_name_list", json_encode($track_etc_name_list));
$smarty->assign("track_etc_chart_list", json_encode($track_etc_chart_list));

$smarty->display('work_cms_stats_tracking_amazon_iframe.html');
?>
