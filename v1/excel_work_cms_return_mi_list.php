<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');
require('inc/helper/manufacturer.php');
require('inc/model/Manufacturer.php');


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

# 상단 타이블
$lfcr = chr(10);
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "no{$lfcr}작성일")
    ->setCellValue('B1', "AS 입고")
    ->setCellValue('C1', "진행상태")
    ->setCellValue('D1', "AS 출고")
    ->setCellValue('E1', "이전 주문번호")
    ->setCellValue('F1', "발송처리일")
    ->setCellValue('G1', "회수자명{$lfcr}회수자 전화")
    ->setCellValue('H1', "공급업체")
    ->setCellValue('I1', "상품명(SKU)::수량")
    ->setCellValue('J1', "요청메세지")
    ->setCellValue('K1', "제조사 검수 제목")
    ->setCellValue('L1', "작성자")
    ->setCellValue('M1', "개선 여부")
    ->setCellValue('N1', "와이즈 확인{$lfcr}(확인담당자)")
;

# 검색조건
$add_where          = "1=1";
$sch_mi_no          = isset($_GET['sch_mi_no']) ? $_GET['sch_mi_no'] : "";
$sch_work_state     = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_parent_ord_no  = isset($_GET['sch_parent_ord_no']) ? $_GET['sch_parent_ord_no'] : "";
$sch_stock_date     = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : "";
$sch_recipient      = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp   = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_sup_c_no       = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_option         = isset($_GET['sch_option']) ? $_GET['sch_option'] : "";
$sch_title          = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_is_improve     = isset($_GET['sch_is_improve']) ? $_GET['sch_is_improve'] : "";
$sch_run_state      = isset($_GET['sch_run_state']) ? $_GET['sch_run_state'] : "";
$sch_is_as_delivery = isset($_GET['sch_is_as_delivery']) ? $_GET['sch_is_as_delivery'] : "";

if(!empty($sch_mi_no)) {
    $add_where .= " AND mi.mi_no='{$sch_mi_no}'";
}

if(!empty($sch_work_state)) {
    $add_where .= " AND mi.work_state='{$sch_work_state}'";
}

if(!empty($sch_parent_ord_no)) {
    $add_where .= " AND mi.parent_order_number LIKE '%{$sch_parent_ord_no}%'";
}

if(!empty($sch_stock_date)) {
    $add_where .= " AND mi.stock_date = '{$sch_stock_date}'";
}

if(!empty($sch_recipient)) {
    $add_where .= " AND mi.recipient LIKE '%{$sch_recipient}%'";
}

if(!empty($sch_recipient_hp)) {
    $add_where .= " AND mi.recipient_hp LIKE '%{$sch_recipient_hp}%'";
}

if(!empty($sch_sup_c_no)) {
    $add_where .= " AND mi.mi_no IN(SELECT DISTINCT sub.mi_no FROM manufacturer_inspection_unit sub WHERE sub.sup_c_no ='{$sch_sup_c_no}')";
}

if(!empty($sch_option)) {
    $add_where .= " AND mi.mi_no IN(SELECT DISTINCT sub.mi_no FROM manufacturer_inspection_unit sub WHERE sub.sku LIKE '%{$sch_option}%')";
}

if(!empty($sch_title)) {
    $add_where .= " AND mi.ins_title LIKE '%{$sch_title}%'";
}

if(!empty($sch_is_improve)) {
    $add_where .= " AND mi.is_improve='{$sch_is_improve}'";
}

if(!empty($sch_run_state)) {
    $add_where .= " AND mi.run_state='{$sch_run_state}'";
}

if(!empty($sch_is_as_delivery)) {
    $add_where .= " AND mi.is_as_delivery='{$sch_is_as_delivery}'";
}

# 리스트 쿼리
$return_mi_sql = "
    SELECT
        *,
        DATE_FORMAT(mi.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(mi.regdate, '%H:%i') as reg_hour,
        (SELECT sub.c_name FROM company sub WHERE sub.c_no=mi.add_c_no) as add_c_name,
        (SELECT sub.s_name FROM staff sub WHERE sub.s_no=mi.reg_s_no) as reg_s_name,
        (SELECT sub.s_name FROM staff sub WHERE sub.s_no=mi.run_s_no) as run_s_name
    FROM manufacturer_inspection mi
    WHERE {$add_where}
    ORDER BY mi_no DESC
";
$return_mi_query    = mysqli_query($my_db, $return_mi_sql);
$return_mi_list     = [];
$work_state_option  = getWorkStateOption();
$run_state_option   = getMiRunStateOption();
$is_improve_option  = getIsImproveOption();
$idx  = 2;
while($return_mi = mysqli_fetch_assoc($return_mi_query))
{
    $recipient_sc_hp = "";
    if (isset($return_mi['recipient_hp']) && !empty($return_mi['recipient_hp'])) {
        $f_hp = substr($return_mi['recipient_hp'], 0, 4);
        $e_hp = substr($return_mi['recipient_hp'], 7, 15);
        $recipient_sc_hp = $f_hp . "***" . $e_hp;
    }

    $is_as_in           = ($return_mi['is_as_in'] == '1') ? "√" : "";
    $is_as_out          = ($return_mi['is_as_out'] == '1') ? "√" : "";
    $work_state_name    = $work_state_option[$return_mi['work_state']];
    $is_improve_name    = $return_mi['is_improve'] == '1' ? $is_improve_option[$return_mi['is_improve']] : "";
    if(!empty($is_improve_name) && !empty($return_mi['exp_date'])){
        $is_improve_name .= $lfcr."({$return_mi['exp_date']})";
    }

    $run_state_name     = $run_state_option[$return_mi['run_state']];
    if(!empty($run_state_name) && !empty($return_mi['run_s_name'])){
        $run_state_name .=  $lfcr."({$return_mi['run_s_name']})";
    }
    $stock_date = ($return_mi['stock_date'] == '0000-00-00') ? "" : $return_mi['stock_date'];

    $return_mi_unit_sql = "
        SELECT
            miu.sku,
            (SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=miu.sup_c_no) as sup_c_name,
            miu.quantity
        FROM manufacturer_inspection_unit miu
        WHERE miu.mi_no = '{$return_mi['mi_no']}'
    ";
    $return_mi_unit_query   = mysqli_query($my_db, $return_mi_unit_sql);
    $return_mi_sup_list     = "";
    $return_mi_unit_list    = "";
    while($return_unit  = mysqli_fetch_assoc($return_mi_unit_query))
    {
        $sup_info  = $return_unit['sup_c_name'];
        $unit_info = "{$return_unit['sku']}::{$return_unit['quantity']}";

        $return_mi_sup_list  .= !empty($return_mi_sup_list) ?  $lfcr.$sup_info : $sup_info;
        $return_mi_unit_list .= !empty($return_mi_unit_list) ?  $lfcr.$unit_info : $unit_info;
    }

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$idx}", $return_mi['mi_no'].$lfcr.$return_mi['reg_day'].$lfcr.$return_mi['reg_hour'])
        ->setCellValue("B{$idx}", $is_as_in)
        ->setCellValue("C{$idx}", $work_state_name)
        ->setCellValue("D{$idx}", $is_as_out)
        ->setCellValueExplicit("E{$idx}", $return_mi['parent_order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
        ->setCellValue("F{$idx}", $return_mi['stock_date'])
        ->setCellValue("G{$idx}", $return_mi['recipient'].$lfcr.$recipient_sc_hp)
        ->setCellValue("H{$idx}", $return_mi_sup_list)
        ->setCellValue("I{$idx}", $return_mi_unit_list)
        ->setCellValue("J{$idx}", $return_mi['req_memo'])
        ->setCellValue("K{$idx}", $return_mi['ins_title'])
        ->setCellValue("L{$idx}", $return_mi['reg_s_name'])
        ->setCellValue("M{$idx}", $is_improve_name)
        ->setCellValue("N{$idx}", $run_state_name)
    ;

    $idx++;
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00343a40');
$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:N{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A2:N{$idx}")->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("A1:A{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("G1:G{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("N1:N{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setWrapText(true);

// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);

$excel_title = "MI 리스트";
$objPHPExcel->getActiveSheet()->setTitle($excel_title);
$objPHPExcel->getActiveSheet()->getStyle("A1:N{$idx}")->applyFromArray($styleArray);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$excel_title}.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
