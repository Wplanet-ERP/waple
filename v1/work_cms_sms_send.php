<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_message.php');
require('inc/model/WorkCms.php');
require('inc/model/Message.php');

# Model Init
$cms_model          = WorkCms::Factory();
$message_model      = Message::Factory();
$message_model->setCharset("utf8mb4");

# Process
$process            = isset($_POST['process']) ? $_POST['process'] : "";
$cs_temp_list       = getCsSendTempList();
$send_phone_list    = getWapleSendPhoneList();

if($process == 'new_sms_send')
{
    $content    = (isset($_POST['temp_content'])) ? $_POST['temp_content'] : "";
    $send_phone = (isset($_POST['send_phone'])) ? $_POST['send_phone'] : "";
    $send_name  = isset($send_phone_list[$send_phone]) ? $send_phone_list[$send_phone] : "와이즈미디어커머스";
    $dest_name  = (isset($_POST['dest_name'])) ? $_POST['dest_name'] : "";
    $dest_phone = (isset($_POST['dest_phone'])) ? $_POST['dest_phone'] : "";
    $msg_body   = str_replace("#{고객명}", $dest_name, $content);
    $result     = false;

    $sms_msg        = $msg_body;
    $sms_msg_len 	= mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
    $msg_type 		= ($sms_msg_len > 90) ? "L" : "S";
    $c_info         = 100;
    $cur_datetime   = date("YmdHis");
    $msg_id 	    = "CS{$cur_datetime}0001";

    $send_list[$msg_id] = array(
        "msg_id"        => $msg_id,
        "msg_type"      => $msg_type,
        "sender"        => $send_name,
        "sender_hp"     => $send_phone,
        "receiver"      => $dest_name,
        "receiver_hp"   => $dest_phone,
        "title"         => "와플 CS",
        "content"       => $sms_msg,
        "cinfo"         => $c_info,
    );

    if($message_model->sendMessage($send_list)){
        $result = true;
    }

    if($result){
        exit ("<script>alert('메세지를 전송했습니다'); location.href='work_cms_sms_send.php';</script>");
    }else{
        exit ("<script>alert('메세지 전송에 실패했습니다'); location.href='work_cms_sms_send.php';</script>");
    }
}

$order_number       = isset($_GET['ord_no']) ? trim($_GET['ord_no']) : "";
$send_phone         = "1668-3620";
$receiver_name      = "";
$receiver_phone     = "";

if(!empty($order_number)){
    $order_item     = $cms_model->getOrderItem($order_number);
    $receiver_name  = $order_item['recipient'];
    $receiver_phone = str_replace("-", "", $order_item['recipient_hp']);

}

$smarty->assign("send_phone", $send_phone);
$smarty->assign("send_phone_list", $send_phone_list);
$smarty->assign("receiver_name", $receiver_name);
$smarty->assign("receiver_phone", $receiver_phone);
$smarty->assign("cs_temp_list", $cs_temp_list);

$smarty->display('work_cms_sms_send.html');
?>
