<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Company.php');

# Navigation & My Quick
$nav_prd_no  = "236";
$nav_title   = "커머스 상품별 판매";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$kind_model                 = Kind::Factory();
$company_model              = Company::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$dp_company_list            = $company_model->getDpList();
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

# 검색조건
$add_where              = "1=1 AND `w`.delivery_state='4'";
$today_val              = date('Y-m-d');
$month_val              = date('Y-m', strtotime('-3 months'))."-01";
$sch_order_type         = isset($_GET['sch_order_type']) ? $_GET['sch_order_type'] : "unit_price";
$sch_brand_g1           = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2           = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand              = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_order_date_type    = isset($_GET['sch_order_date_type']) ? $_GET['sch_order_date_type'] : "3";
$sch_order_s_date       = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : $month_val;
$sch_order_e_date       = isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : $today_val;
$sch_price_type         = isset($_GET['sch_price_type']) ? $_GET['sch_price_type'] : "";
$sch_dp_company         = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";
$sch_dp_company         = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";
$search_url             = getenv("QUERY_STRING");

if(empty($search_url)){
    $search_url = "sch_order_type=unit_price&sch_order_date_type=3";
}

if(empty($sch_order_s_date) || empty($sch_order_e_date))
{
    echo "날짜 검색 값이 없으면 검색 생성이 안됩니다.";
    exit;
}

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_price_type)){

    if($sch_price_type == '1'){
        $add_where .= " AND w.unit_price > 0";
    }elseif($sch_price_type == '2'){
        $add_where .= " AND w.unit_price = 0";
    }
    $smarty->assign("sch_price_type", $sch_price_type);
}

if(!empty($sch_dp_company)){
    $add_where .= " AND w.dp_c_no = '{$sch_dp_company}'";
    $smarty->assign("sch_dp_company", $sch_dp_company);
}

# 브랜드 옵션
if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("search_url", $search_url);
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 날짜 처리
$all_date_where     = "";
$all_date_title     = "";
$all_date_key       = "";
$add_key_column     = "";

if($sch_order_date_type == '4') //연간
{
    $sch_s_year      = date("Y", strtotime($sch_order_s_date));
    $sch_e_year      = date("Y", strtotime($sch_order_e_date));

    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y')";
    $add_key_column  = "DATE_FORMAT(`w`.order_date, '%Y')";
}
elseif($sch_order_date_type == '3') //월간
{
    $sch_s_month     = date("Y-m", strtotime($sch_order_s_date));
    $sch_e_month     = date("Y-m", strtotime($sch_order_e_date));
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m')";
    $add_key_column  = "DATE_FORMAT(`w`.order_date, '%Y%m')";
}
elseif($sch_order_date_type == '2') //주간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_order_s_date}' AND '{$sch_order_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";
    $add_key_column  = "DATE_FORMAT(DATE_SUB(`w`.order_date, INTERVAL(IF(DAYOFWEEK(`w`.order_date)=1,8,DAYOFWEEK(`w`.order_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_order_s_date}' AND '{$sch_order_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";
    $add_key_column  = "DATE_FORMAT(`w`.order_date, '%Y%m%d')";
}
$smarty->assign("sch_order_type", $sch_order_type);
$smarty->assign("sch_order_date_type", $sch_order_date_type);
$smarty->assign("sch_order_s_date", $sch_order_s_date);
$smarty->assign("sch_order_e_date", $sch_order_e_date);

# 날짜 생성
$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
	    DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
$total_all_date_list = [];
$cms_date_name_list  = [];
while($all_date = mysqli_fetch_assoc($all_date_query))
{
    $total_all_date_list[$all_date['date_key']] = array("s_date" => date("Y-m-d",strtotime($all_date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($all_date['date_key']))." 23:59:59");
    $cms_date_name_list[$all_date['chart_key']] = $all_date['chart_title'];
}

$cms_product_temp_qty_total     = [];
$cms_product_temp_price_total   = [];
$cms_product_name_list          = [];

$cms_product_tmp_qty_list    = [];
$cms_product_tmp_price_list  = [];
$cms_product_qty_list        = [];
$cms_product_price_list      = [];
$cms_product_qty_date_list   = [];
$cms_product_price_date_list = [];

foreach($total_all_date_list as $date_data)
{
    $chk_prd_rank_sql   = "SELECT {$add_key_column} as key_date, prd_no, (SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no) as prd_name, SUM(quantity) as total_qty, SUM(unit_price) as total_price  FROM work_cms w WHERE {$add_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}') GROUP BY prd_no";
    $chk_prd_rank_query = mysqli_query($my_db, $chk_prd_rank_sql);
    while($chk_prd_rank = mysqli_fetch_assoc($chk_prd_rank_query))
    {
        $cms_product_temp_qty_total[$chk_prd_rank['prd_no']]   += $chk_prd_rank['total_qty'];
        $cms_product_temp_price_total[$chk_prd_rank['prd_no']] += $chk_prd_rank['total_price'];
        $cms_product_name_list[$chk_prd_rank['prd_no']]         = $chk_prd_rank['prd_name'];

        if(!isset($cms_product_tmp_qty_list[$chk_prd_rank['prd_no']])){
            foreach($cms_date_name_list as $key_date => $key_label){
                $cms_product_tmp_qty_list[$chk_prd_rank['prd_no']][$key_date]    = 0;
                $cms_product_tmp_price_list[$chk_prd_rank['prd_no']][$key_date]  = 0;
            }
        }

        $cms_product_tmp_qty_list[$chk_prd_rank['prd_no']][$chk_prd_rank['key_date']]    += $chk_prd_rank['total_qty'];
        $cms_product_tmp_price_list[$chk_prd_rank['prd_no']][$chk_prd_rank['key_date']]  += $chk_prd_rank['total_price'];
    }
}

# 정렬
$ord_type       = "total";
$ord_type_by    = isset($_GET['ord_type_by']) && !empty($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

if($ord_type_by == "1"){
    asort($cms_product_temp_qty_total);
    asort($cms_product_temp_price_total);
}else{
    arsort($cms_product_temp_qty_total);
    arsort($cms_product_temp_price_total);
}
$smarty->assign("ord_type", $ord_type);
$smarty->assign("ord_type_by", $ord_type_by);

foreach($cms_product_temp_qty_total as $prd_no => $best_value)
{
    foreach($cms_date_name_list as $key_date => $key_label){
        $cms_product_qty_list[$prd_no][$key_date] = $cms_product_tmp_qty_list[$prd_no][$key_date];
        $cms_product_qty_date_list[$key_date]    += $cms_product_tmp_qty_list[$prd_no][$key_date];
    }
}

foreach($cms_product_temp_price_total as $prd_no => $best_value)
{
    foreach($cms_date_name_list as $key_date => $key_label){
        $cms_product_price_list[$prd_no][$key_date] = $cms_product_tmp_price_list[$prd_no][$key_date];
        $cms_product_price_date_list[$key_date]    += $cms_product_tmp_price_list[$prd_no][$key_date];
    }
}

$cols_cnt = count($cms_date_name_list)+2;

$smarty->assign("sch_order_type_option", getDateTypeOption());
$smarty->assign("dp_company_list", $dp_company_list);
$smarty->assign("cms_price_type_option", getCmsPriceType());
$smarty->assign("cols_cnt", $cols_cnt);
$smarty->assign("cms_date_name_list", $cms_date_name_list);
$smarty->assign("cms_product_name_list", $cms_product_name_list);
$smarty->assign("cms_product_qty_list", $cms_product_qty_list);
$smarty->assign("cms_product_price_list", $cms_product_price_list);
$smarty->assign("cms_product_qty_date_list", $cms_product_qty_date_list);
$smarty->assign("cms_product_price_date_list", $cms_product_price_date_list);

$smarty->display('media_commerce_sales_product.html');
?>
