<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_cms.php');
require('inc/helper/work_return.php');
require('inc/helper/deposit.php');
require('inc/helper/withdraw.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

$nowdate    = date("Y-m-d H:i:s");
$lfcr       = chr(10);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O1:P1');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Q1:R1');

# 상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "택배리스트")
	->setCellValue('A2', "w_no")
	->setCellValue('B2', "발송진행상태")
	->setCellValue('C2', "주문일시")
	->setCellValue('D2', "주문번호")
	->setCellValue('E2', "수령자명")
	->setCellValue('F2', "택배사{$lfcr}운송장번호")
	->setCellValue('G2', "업체명/브랜드명")
	->setCellValue('H2', "커머스 상품명")
	->setCellValue('I2', "수량")
	->setCellValue('J2', "구매처")
	->setCellValue('K2', "상품별 결제금액")
	->setCellValue('L2', "상품별 택배비")
	->setCellValue('M2', "환불처리")
	->setCellValue('N2', "입금확인")
	->setCellValue('O1', "부가세리스트")
	->setCellValue('O2', "승인일{$lfcr}매출인식일{$lfcr}전산확정일")
	->setCellValue('P2', "부가세")
	->setCellValue('Q1', "정산리스트")
	->setCellValue('Q2', "지급일{$lfcr}예정일{$lfcr}환불일")
	->setCellValue('R2', "정산금액")
	->setCellValue('S1', "회수리스트")
	->setCellValue('S2', "교환(발송) / 회수 / 환불내역 / 동봉금")
;

# 상품 검색
$sch_prd_g1	    = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	    = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	    = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_prd_name   = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";

$add_where      = "1=1";
$add_date_where = "1=1";
if (!empty($sch_prd) && $sch_prd != "0") { // 상품
    $add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

if(!empty($sch_prd_name)){
    $add_date_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no from product_cms prd_cms where prd_cms.title like '%{$sch_prd_name}%')";
}

# 검색 처리
$base_mon           = date("Y-m", strtotime("-1 months"));
$base_s_date        = $base_mon."-01";
$base_e_day         = date("t", strtotime($base_s_date));
$base_e_date        = $base_mon."-".$base_e_day;
$sch_w_no 			= isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
$sch_order_s_date   = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : $base_s_date;
$sch_order_e_date 	= isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : $base_e_date;
$sch_order_number 	= isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_is_refund      = isset($_GET['sch_is_refund']) ? $_GET['sch_is_refund'] : "";
$sch_is_deposit     = isset($_GET['sch_is_deposit']) ? $_GET['sch_is_deposit'] : "";
$sch_c_no 		    = isset($_GET['sch_c_no']) ? $_GET['sch_c_no'] : "";
$sch_dp_c_no 		= isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_is_vat         = isset($_GET['sch_is_vat']) ? $_GET['sch_is_vat'] : "";
$sch_is_settle      = isset($_GET['sch_is_settle']) ? $_GET['sch_is_settle'] : "";
$sch_is_return      = isset($_GET['sch_is_return']) ? $_GET['sch_is_return'] : "";
$sch_is_return_fee  = isset($_GET['sch_is_return_fee']) ? $_GET['sch_is_return_fee'] : "";

if(!empty($sch_w_no)){
    $add_where .= " AND w.w_no='{$sch_w_no}'";
}

if(!empty($sch_order_s_date) && !empty($sch_order_e_date))
{
    $sch_ord_s_datetime = $sch_order_s_date." 00:00:00";
    $sch_ord_e_datetime = $sch_order_e_date." 23:59:59";

    $add_date_where .= " AND (w.order_date BETWEEN '{$sch_ord_s_datetime}' AND '{$sch_ord_e_datetime}')";
}

if(!empty($sch_order_number)){
    $add_date_where .= " AND w.order_number = '{$sch_order_number}'";
}

if(!empty($sch_is_refund)){
    if($sch_is_refund == '1'){
        $add_where .= " AND refund_cnt > 0";
    }else{
        $add_where .= " AND refund_cnt = 0";
    }
}

if(!empty($sch_is_deposit)){
    if($sch_is_deposit == '1'){
        $add_where .= " AND deposit_cnt > 0";
    }else{
        $add_where .= " AND deposit_cnt = 0";
    }
}

if(!empty($sch_c_no)){
    $add_date_where .= " AND w.c_no='{$sch_c_no}'";
}

if(!empty($sch_dp_c_no)){
    $add_date_where .= " AND w.dp_c_no='{$sch_dp_c_no}'";
}

if(!empty($sch_is_vat)){
    if($sch_is_vat == '1'){
        $add_where .= " AND vat_cnt > 0";
    }else{
        $add_where .= " AND(origin_ord_no != '' AND origin_ord_no IS NOT NULL) AND vat_cnt = 0";
    }
}

if(!empty($sch_is_settle)){
    if($sch_is_settle == '1'){
        $add_where .= " AND settle_cnt > 0";
    }else{
        $add_where .= " AND (origin_ord_no != '' AND origin_ord_no IS NOT NULL) AND settle_cnt = 0";
    }
}

if(!empty($sch_is_return)){
    if($sch_is_return == '1'){
        $add_where .= " AND return_cnt > 0";
    }else{
        $add_where .= " AND return_cnt = 0";
    }
}

if(!empty($sch_is_return_fee)){
    if($sch_is_return_fee == '1'){
        $add_where .= " AND return_fee > 0";
    }else{
        $add_where .= " AND (return_fee = 0 OR return_fee IS NULL)";
    }
}
$add_orderby    = "w.w_no DESC";


# 주문번호 뽑아내기
$cms_ord_total_sql = "
    SELECT 
        * 
    FROM 
    (
        SELECT
            *,
            (SELECT COUNT(wk.w_no) FROM `work` wk WHERE wk.prd_no='229' AND wk.linked_table='work_cms' AND wk.linked_shop_no=w.shop_ord_no) as refund_cnt,
            (SELECT COUNT(wk.w_no) FROM `work` wk WHERE wk.prd_no='260' AND wk.work_state='6' AND wk.linked_table='work_cms' AND wk.linked_shop_no=w.order_number) as deposit_cnt,
            (SELECT COUNT(wcv.no) FROM work_cms_vat wcv WHERE wcv.order_number=w.ord_no) as vat_cnt,
            (SELECT GROUP_CONCAT(wcv.notice SEPARATOR ' ') FROM work_cms_vat wcv WHERE wcv.order_number=w.ord_no GROUP BY ord_no) as vat_notice,             
            (SELECT COUNT(wcs.no) FROM work_cms_settlement wcs WHERE wcs.order_number=w.ord_no) as settle_cnt,
            (SELECT GROUP_CONCAT(wcs.notice SEPARATOR ' ') FROM work_cms_settlement wcs WHERE wcs.order_number=w.ord_no GROUP BY ord_no) as settle_notice,
            (SELECT COUNT(wcr.r_no) FROM work_cms_return wcr WHERE wcr.parent_order_number=w.order_number) as return_cnt,
            (SELECT SUM(wcr.return_delivery_fee) FROM work_cms_return wcr WHERE wcr.parent_order_number=w.order_number) as return_fee
        FROM 
        (
            SELECT
                w.w_no,
                w.delivery_state,
                w.order_number,
                w.origin_ord_no,
                IF(w.origin_ord_no='', w.order_number, w.origin_ord_no) as ord_no,
                w.recipient,
                w.c_no,
                w.c_name,
                w.order_date,
                w.prd_no,
                w.quantity,
                w.dp_c_no,
                w.dp_c_name,
                w.shop_ord_no,
                SUM(w.unit_price) as ord_price,
                SUM(w.unit_delivery_price) as ord_delivery_price
            FROM work_cms w
            WHERE {$add_date_where}
            GROUP BY ord_no
        ) as w
    ) AS w
    WHERE {$add_where} AND w.order_number is not null
    GROUP BY order_number ORDER BY {$add_orderby}
";
$cms_ord_total_query = mysqli_query($my_db, $cms_ord_total_sql);
$order_number_list   = [];
while($order_number = mysqli_fetch_assoc($cms_ord_total_query)){
    $order_number_list[] =  "'".$order_number['order_number']."'";
}
$order_numbers   = implode(',', $order_number_list);
$add_where_group = !empty($order_numbers) ? "w.order_number IN({$order_numbers})" : "w.order_number=''";


# 배송리스트 쿼리
$work_cms_sql = "
    SELECT
        *,
        DATE_FORMAT(w.order_date, '%Y-%m-%d') as ord_date,
        DATE_FORMAT(w.order_date, '%H:%i') as ord_time,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
        (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name
    FROM
    (
        SELECT
            w.w_no,
            w.delivery_state,
            w.order_number,
            w.origin_ord_no,
            w.recipient,
            w.c_no,
            w.c_name,
            w.order_date,
            w.prd_no,
            w.quantity,
            w.dp_c_no,
            w.dp_c_name,
            w.shop_ord_no,
            w.unit_price,
            w.unit_delivery_price
        FROM work_cms w
        WHERE {$add_where_group}
    ) as w
    ORDER BY {$add_orderby}
    LIMIT 10000
";
$work_cms_query	= mysqli_query($my_db, $work_cms_sql);
$work_cms_list  = $deposit_list = $delivery_list = $trade_list = $return_list = $vat_list = $vat_chk_list = $settle_list = $settle_chk_list = [];
$dp_state_option        = getDpStateOption();
$wd_state_option        = getWdStateOption();
$delivery_state_option  = getDeliveryStateOption();
$return_state_option    = getReturnStateOption();
if(!!$work_cms_query)
{
    while($work_array = mysqli_fetch_array($work_cms_query))
    {
        $work_array['delivery_state_name'] = $delivery_state_option[$work_array['delivery_state']];

        $refund_list = [];
        if(!empty($work_array['shop_ord_no']))
        {
            $refund_sql    = "SELECT w_no, wd_no, wd_price_vat, (SELECT wd.wd_state FROM withdraw wd WHERE wd.wd_no=w.wd_no) as wd_state FROM work w WHERE prd_no='229' AND linked_table='work_cms' AND linked_shop_no='{$work_array['shop_ord_no']}'";
            $refund_query  = mysqli_query($my_db, $refund_sql);
            while($refund = mysqli_fetch_assoc($refund_query))
            {
                $price = isset($refund['wd_price_vat']) && !empty($refund['wd_price_vat']) ? $refund['wd_price_vat']  : 0;
                $state = isset($refund['wd_state']) && !empty($refund['wd_state']) ? "[".$wd_state_option[$refund['wd_state']]."]" : "[대기]";
                $refund_list[$refund['w_no']] = array('price' => $price, 'state' => $state);
            }
        }
        $work_array['refund_list'] = $refund_list;

        if(!isset($work_cms_list[$work_array['order_number']]))
        {
            # 입금확인
            $deposit_sql   = "SELECT w_no, dp_no, dp_price_vat, (SELECT dp.dp_state FROM deposit dp WHERE dp.dp_no=w.dp_no) as dp_state FROM work w WHERE prd_no='260' AND work_state='6' AND linked_table='work_cms' AND linked_shop_no='{$work_array['order_number']}'";
            $deposit_query = mysqli_query($my_db, $deposit_sql);
            while($deposit = mysqli_fetch_assoc($deposit_query))
            {
                $dp_price = isset($deposit['dp_price_vat']) && !empty($deposit['dp_price_vat']) ? $deposit['dp_price_vat']  : 0;
                $dp_state = isset($deposit['dp_state']) && !empty($deposit['dp_state']) ? "[".$dp_state_option[$deposit['dp_state']]."]" : "[대기]";
                $deposit_list[$work_array['order_number']][$deposit['dp_no']] = array('price' => $dp_price, 'state' => $dp_state);
            }

            # 운송장 리스트
            $delivery_sql    = "SELECT `no`, delivery_no, delivery_type FROM work_cms_delivery WHERE order_number='{$work_array['order_number']}' GROUP BY delivery_no";
            $delivery_query  = mysqli_query($my_db, $delivery_sql);
            while ($delivery = mysqli_fetch_assoc($delivery_query)) {
                $delivery_list[$work_array['order_number']][$delivery['no']] = array('delivery_no' => $delivery['delivery_no'], 'delivery_type' => $delivery['delivery_type']);
            }

            # 교환 주문번호 체크(CMS)
            $trade_sql    = "SELECT order_number, delivery_state FROM work_cms WHERE parent_order_number = '{$work_array['order_number']}' GROUP BY order_number ORDER BY w_no";
            $trade_query  = mysqli_query($my_db, $trade_sql);
            while($trade = mysqli_fetch_assoc($trade_query))
            {
                $trade_list[$work_array['order_number']][] = array('ord_no' => $trade['order_number'], 'delivery_state' => "[".$delivery_state_option[$trade['delivery_state']]."]");
            }

            # 회수 리스트
            $return_sql    = "SELECT order_number, return_state, return_delivery_fee FROM work_cms_return WHERE parent_order_number = '{$work_array['order_number']}' GROUP BY order_number ORDER BY r_no";
            $return_query  = mysqli_query($my_db, $return_sql);
            while($return = mysqli_fetch_assoc($return_query))
            {
                $return_list[$work_array['order_number']][] = array("ord_no" => $return['order_number'], "return_state" => "[".$return_state_option[$return['return_state']]."]", "return_fee" => $return['return_delivery_fee']);
            }
        }

        # 부가세 리스트
        if(!isset($vat_chk_list[$work_array['origin_ord_no']]) && !empty($work_array['origin_ord_no']))
        {
            $vat_chk_list[$work_array['origin_ord_no']] = 1;
            $vat_sql    = "SELECT vat_date, (card_price+cash_price+etc_price+tax_price) as vat_price, notice FROM work_cms_vat WHERE order_number = '{$work_array['origin_ord_no']}' ORDER BY vat_price DESC";
            $vat_query  = mysqli_query($my_db, $vat_sql);
            while($vat = mysqli_fetch_assoc($vat_query))
            {
                $vat_list[$work_array['order_number']][] = array("vat_date" => $vat['vat_date'], "vat_price" => $vat['vat_price'], "notice" => $vat['notice']);
            }
        }

        # 정산 리스트
        if(!isset($settle_chk_list[$work_array['origin_ord_no']]) && !empty($work_array['origin_ord_no']))
        {
            $settle_chk_list[$work_array['origin_ord_no']] = 1;
            $settle_sql    = "SELECT settle_date, settle_price, notice FROM work_cms_settlement WHERE order_number = '{$work_array['origin_ord_no']}' ORDER BY settle_price DESC";
            $settle_query  = mysqli_query($my_db, $settle_sql);
            while($settle = mysqli_fetch_assoc($settle_query))
            {
                $settle_list[$work_array['order_number']][] = array("settle_date" => $settle['settle_date'], "settle_price" => $settle['settle_price'], "notice" => $settle['notice']);
            }
        }

        $work_cms_list[$work_array['order_number']][] = $work_array;
    }
}

$idx        = 3;
$work_sheet = $objPHPExcel->setActiveSheetIndex(0);
foreach($work_cms_list as $work_cms)
{
    $ord_idx = 0;
    $row_cnt = count($work_cms);
    foreach($work_cms as $cms_prd)
    {
        $work_sheet
            ->setCellValue("A{$idx}", $cms_prd['w_no'])
            ->setCellValue("B{$idx}", $cms_prd['delivery_state_name'])
            ->setCellValue("G{$idx}", $cms_prd['c_name'])
            ->setCellValue("H{$idx}", $cms_prd['prd_name'])
            ->setCellValue("I{$idx}", $cms_prd['quantity'])
            ->setCellValue("J{$idx}", $cms_prd['dp_c_name'])
            ->setCellValue("K{$idx}", $cms_prd['unit_price'])
            ->setCellValue("L{$idx}", $cms_prd['unit_delivery_price'])
        ;

        if (isset($cms_prd['refund_list']))
        {
            $refund_text = "";
            foreach($cms_prd['refund_list'] as $refund_data){
                $refund_text .= !empty($refund_text) ? $lfcr.$refund_data['price'] : $refund_data['price'];
                $refund_text .= !empty($refund_text) ? $lfcr.$refund_data['state'] : $refund_data['state'];
            }

            $work_sheet->setCellValue("M{$idx}", $refund_text);
        }

        if($ord_idx == 0)
        {
            $start_idx  = $idx;
            $end_idx    = $idx+$row_cnt-1;

            $work_sheet->mergeCells("C{$start_idx}:C{$end_idx}");
            $work_sheet->mergeCells("D{$start_idx}:D{$end_idx}");
            $work_sheet->mergeCells("E{$start_idx}:E{$end_idx}");
            $work_sheet->mergeCells("F{$start_idx}:F{$end_idx}");
            $work_sheet->mergeCells("N{$start_idx}:N{$end_idx}");
            $work_sheet->mergeCells("O{$start_idx}:P{$end_idx}");
            $work_sheet->mergeCells("Q{$start_idx}:R{$end_idx}");
            $work_sheet->mergeCells("S{$start_idx}:S{$end_idx}");

            $work_sheet
                ->setCellValue("C{$idx}", $cms_prd['ord_date'].$lfcr.$cms_prd['ord_time'])
                ->setCellValueExplicit("D{$idx}", $cms_prd['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("E{$idx}", $cms_prd['recipient'])
            ;

            if (isset($delivery_list[$cms_prd['order_number']]))
            {
                $delivery_text = "";
                foreach($delivery_list[$cms_prd['order_number']] as $delivery_data){
                    $delivery_text .= !empty($delivery_text) ? $lfcr.$delivery_data['delivery_type'] : $delivery_data['delivery_type'];
                    $delivery_text .= !empty($delivery_text) ? $lfcr.$delivery_data['delivery_no'] : $delivery_data['delivery_no'];
                }

                $work_sheet->setCellValue("F{$idx}", $delivery_text);
            }

            if (isset($deposit_list[$cms_prd['order_number']]))
            {
                $deposit_text = "";
                foreach($deposit_list[$cms_prd['order_number']] as $deposit_data){
                    $deposit_text .= !empty($deposit_text) ? $lfcr.$deposit_data['price'] : $deposit_data['price'];
                    $deposit_text .= !empty($deposit_text) ? $lfcr.$deposit_data['state'] : $deposit_data['state'];
                }

                $work_sheet->setCellValue("N{$idx}", $deposit_text);
            }

            if (isset($vat_list[$cms_prd['order_number']]))
            {
                $vat_text = "";
                foreach($vat_list[$cms_prd['order_number']] as $vat_data){
                    $vat_text .= !empty($vat_text) ? $lfcr.$vat_data['vat_date']." ".$vat_data['vat_price'] : $vat_data['vat_date']." ".$vat_data['vat_price'];
                }
                $work_sheet->setCellValue("O{$idx}", $vat_text);
            }

            if (isset($settle_list[$cms_prd['order_number']]))
            {
                $settle_text = "";
                foreach($settle_list[$cms_prd['order_number']] as $settle_data){
                    $settle_text .= !empty($settle_text) ? $lfcr.$settle_data['settle_date']." ".$settle_data['settle_price'] : $settle_data['settle_date']." ".$settle_data['settle_price'];
                }
                $work_sheet->setCellValue("Q{$idx}", $settle_text);
            }

            $trade_text = "";
            if (isset($trade_list[$cms_prd['order_number']]))
            {
                foreach($trade_list[$cms_prd['order_number']] as $trade_data){
                    $trade_text .= !empty($trade_text) ? $lfcr."발송 : {$trade_data['ord_no']} {$trade_data['delivery_state']}" : "발송 : {$trade_data['ord_no']} {$trade_data['delivery_state']}";
                }
            }

            if (isset($return_list[$cms_prd['order_number']]))
            {
                foreach($return_list[$cms_prd['order_number']] as $return_data){
                    $trade_text .= !empty($trade_text) ? $lfcr."회수 : {$return_data['ord_no']} {$return_data['return_state']}" : "회수 : {$return_data['ord_no']} {$return_data['return_state']}";

                    if($return_data['return_fee'] > 0){
                        $trade_text .= $return_data['return_fee'];
                    }
                }
            }

            if(!empty($trade_text)){
                $work_sheet->setCellValue("S{$idx}", $trade_text);
            }
        }

        $ord_idx++;
        $idx++;
    }
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');
$objPHPExcel->getActiveSheet()->getStyle("A1:S2")->getFont()->setColor($fontColor);

$objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:S2")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00262626');
$objPHPExcel->getActiveSheet()->getStyle("A1:S2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("D3:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("H3:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("K3:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("C2:C{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("M2:M{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("Q2:Q{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("S2:S{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->applyFromArray(
    array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('rgb' => '000000')
            )
        )
    )
);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(40);

$objPHPExcel->getActiveSheet()->setTitle("커머스 배송리스트(정산검토)");


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_커머스 배송리스트(정산검토).xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
