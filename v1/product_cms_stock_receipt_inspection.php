<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', 3000);

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/model/CommerceOrder.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');
include_once('Classes/PHPExcel.php');

$unit_model     = ProductCmsUnit::Factory();
$file_name      = $_FILES["inspection_file"]["tmp_name"];

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$excel_stock_list   = [];
$not_option_list    = [];
$with_log_c_no      = '2809';

for ($i = 9; $i < $totalRow; $i++)
{
    $prd_code           = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));
    $warehouse_val      = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));
    $in_base_qty        = (int)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));
    $in_point_qty       = (int)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));
    $in_move_qty        = (int)trim(addslashes($objWorksheet->getCell("V{$i}")->getCalculatedValue()));
    $in_return_qty      = (int)trim(addslashes($objWorksheet->getCell("Z{$i}")->getCalculatedValue()));
    $in_etc_qty         = (int)trim(addslashes($objWorksheet->getCell("R{$i}")->getCalculatedValue()));
    $out_base_qty       = (int)trim(addslashes($objWorksheet->getCell("BB{$i}")->getCalculatedValue()));
    $out_point_qty      = (int)trim(addslashes($objWorksheet->getCell("AH{$i}")->getCalculatedValue()));
    $out_move_qty       = (int)trim(addslashes($objWorksheet->getCell("AN{$i}")->getCalculatedValue()));
    $out_return_qty     = (int)trim(addslashes($objWorksheet->getCell("AR{$i}")->getCalculatedValue()));
    $out_etc_qty        = (int)trim(addslashes($objWorksheet->getCell("AX{$i}")->getCalculatedValue()));
    $in_base_price      = (int)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));
    $in_point_price     = (int)trim(addslashes($objWorksheet->getCell("O{$i}")->getCalculatedValue()));
    $in_move_price      = (int)trim(addslashes($objWorksheet->getCell("W{$i}")->getCalculatedValue()));
    $in_return_price    = (int)trim(addslashes($objWorksheet->getCell("AA{$i}")->getCalculatedValue()));
    $in_etc_price       = (int)trim(addslashes($objWorksheet->getCell("S{$i}")->getCalculatedValue()));
    $out_base_price     = (int)trim(addslashes($objWorksheet->getCell("BC{$i}")->getCalculatedValue()));
    $out_point_price    = (int)trim(addslashes($objWorksheet->getCell("AI{$i}")->getCalculatedValue()));
    $out_move_price     = (int)trim(addslashes($objWorksheet->getCell("AO{$i}")->getCalculatedValue()));
    $out_return_price   = (int)trim(addslashes($objWorksheet->getCell("AS{$i}")->getCalculatedValue()));
    $out_etc_price      = (int)trim(addslashes($objWorksheet->getCell("AY{$i}")->getCalculatedValue()));

    $prd_item           = $unit_model->getSkuItem($prd_code, $with_log_c_no);

    if(!empty($prd_item))
    {
        $log_c_no           = ($warehouse_val == '99. 사내창고') ? "1113" : "2809";
        $log_company        = ($warehouse_val == '99. 사내창고') ? "와이즈미디어커머스" : "위드플레이스";
        $warehouse          = ($warehouse_val == '99. 사내창고') ? "사내창고" : $warehouse_val;

        $excel_stock_list[$prd_item['no']][$log_c_no][$warehouse] = array(
            'prd_unit'          => $prd_item['prd_unit'],
            'brand'             => $prd_item['brand'],
            "brand_name"        => $prd_item['brand_name'],
            "option_name"       => $prd_item['option_name'],
            "log_company"       => $log_company,
            "warehouse"         => $warehouse,
            "sku"               => $prd_code,
            "in_base_qty"       => $in_base_qty,
            "in_point_qty"      => $in_point_qty,
            "in_move_qty"       => $in_move_qty,
            "in_return_qty"     => $in_return_qty,
            "in_etc_qty"        => $in_etc_qty,
            "out_base_qty"      => $out_base_qty,
            "out_point_qty"     => $out_point_qty,
            "out_move_qty"      => $out_move_qty,
            "out_return_qty"    => $out_return_qty,
            "out_etc_qty"       => $out_etc_qty,
            "in_base_price"     => $in_base_price,
            "in_point_price"    => $in_point_price,
            "in_move_price"     => $in_move_price,
            "in_return_price"   => $in_return_price,
            "in_etc_price"      => $in_etc_price,
            "out_base_price"    => $out_base_price,
            "out_point_price"   => $out_point_price,
            "out_move_price"    => $out_move_price,
            "out_return_price"  => $out_return_price,
            "out_etc_price"     => $out_etc_price,
        );
    }else{
        $not_option_list[$prd_code] = $prd_code;
    }
}

# 검색
$add_confirm_where  = "1=1 AND `base_type`='start'";
$sch_move_date      = isset($_POST['inspection_month']) ? $_POST['inspection_month'] : date('Y-m', strtotime('-1 months'));

if(!empty($sch_move_date))
{
    $add_confirm_where  .=  " AND rs.base_mon='{$sch_move_date}'";
}

# 회수리스트 처리
$return_list      = [];
$return_prd_sql   = "SELECT `option`, SUM(quantity) as qty FROM work_cms_return_unit WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_return wcr WHERE wcr.return_state='6' AND DATE_FORMAT(wcr.return_date,'%Y-%m')='{$sch_move_date}') AND `type` IN(1,2,3,4) GROUP BY `option`";
$return_prd_query = mysqli_query($my_db, $return_prd_sql);
while($return_prd = mysqli_fetch_assoc($return_prd_query))
{
    $return_list[$return_prd['option']] += $return_prd['qty'];
}

# 쿼리용 날짜
$sch_move_s_date    = $sch_move_date."-01";
$end_day            = date('t', strtotime($sch_move_date));
$sch_move_e_date    = $sch_move_date."-".$end_day;

# 재고이동 리스트
$stock_move_list   = [];
$stock_move_sql     = "SELECT prd_unit, in_warehouse, out_warehouse, in_qty, out_qty FROM product_cms_stock_transfer pcst WHERE (pcst.move_date BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}')";
$stock_move_query   = mysqli_query($my_db, $stock_move_sql);
while($stock_move = mysqli_fetch_assoc($stock_move_query))
{
    $stock_move_list[$stock_move['prd_unit']][$stock_move['in_warehouse']]["in"]   += $stock_move['in_qty'];
    $stock_move_list[$stock_move['prd_unit']][$stock_move['out_warehouse']]["out"] += $stock_move['out_qty'];
}

# 입고/반출 리스트
$stock_list         = [];
$stock_report_sql   = "SELECT * FROM product_cms_stock_report pcsr WHERE (pcsr.regdate BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}') AND pcsr.confirm_state > 0";
$stock_report_query = mysqli_query($my_db, $stock_report_sql);
while($stock_report = mysqli_fetch_assoc($stock_report_query))
{
    $stock_kind = "";
    switch($stock_report['confirm_state']){
        case "1":
        case "2":
            $stock_kind = "in_point";
            break;
        case "3":
            $stock_kind = "in_move";
            break;
        case "4":
            $stock_kind = "in_etc";
            break;
        case "5":
            $stock_kind = "in_return";
            break;
        case "6":
            $stock_kind = "out_move";
            break;
        case "8":
            $stock_kind = "out_etc";
            break;
        case "9":
            $stock_kind = "out_point";
            break;
        case "10":
            $stock_kind = "out_return";
            break;

    }

    if(!empty($stock_kind))
    {
        if(!isset($stock_list[$stock_report['prd_unit']][$stock_report['stock_type']])){
            $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']] = array(
                "in_point"      => 0,
                "in_move"       => 0,
                "in_etc"        => 0,
                "in_return"     => 0,
                "out_point"     => 0,
                "out_move"      => 0,
                "out_return"    => 0,
                "out_etc"       => 0,
            );
        }

        $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']][$stock_kind] += $stock_report['stock_qty'];
    }
}

# 재고자산수불부 쿼리
$product_receipt_sql = "
    SELECT
        *
    FROM product_cms_stock_confirm as rs 
    WHERE {$add_confirm_where}
    ORDER BY rs.option_name ASC, rs.warehouse ASC
";
$product_receipt_query  = mysqli_query($my_db, $product_receipt_sql);
$product_receipt_list   = [];
while($product_receipt = mysqli_fetch_assoc($product_receipt_query))
{
    $stock_list_data                        = isset($stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];
    $move_list_data                         = isset($stock_move_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_move_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];

    $product_receipt['in_base_qty']         = $product_receipt['qty'];
    $product_receipt['in_point_qty']        = !empty($stock_list_data) ? $stock_list_data['in_point'] : 0;
    $product_receipt['in_move_sub_qty']     = !empty($stock_list_data) ? $stock_list_data['in_move'] : 0;
    $product_receipt['in_org_move_qty']     = !empty($move_list_data) && isset($move_list_data["in"]) ? $move_list_data["in"] : 0;
    $product_receipt['in_move_qty']         = $product_receipt['in_org_move_qty'] + $product_receipt['in_move_sub_qty'];
    $product_receipt['in_etc_qty']          = !empty($stock_list_data) ? $stock_list_data['in_etc'] : 0;
    $product_receipt['in_return_sub_qty']   = ($product_receipt['warehouse'] == "2-3 검수대기창고(반품건)" && isset($return_list[$product_receipt['prd_unit']])) ? $return_list[$product_receipt['prd_unit']] : 0;
    $product_receipt['in_return_org_qty']   = !empty($stock_list_data) ? $stock_list_data['in_return'] : 0;
    $product_receipt['in_return_qty']       = $product_receipt['in_return_org_qty'] + $product_receipt['in_return_sub_qty'];

    $product_receipt['in_base_price']       = $product_receipt['base_price'] * $product_receipt['in_base_qty'];
    $product_receipt['in_point_price']      = $product_receipt['org_price'] * $product_receipt['in_point_qty'];
    $product_receipt['in_move_price']       = $product_receipt['org_price'] * $product_receipt['in_move_qty'];
    $product_receipt['in_return_price']     = $product_receipt['org_price'] * $product_receipt['in_return_qty'];
    $product_receipt['in_etc_price']        = $product_receipt['org_price'] * $product_receipt['in_etc_qty'];
    $product_receipt['in_qty']              = $product_receipt['in_point_qty'] + $product_receipt['in_move_qty'] + $product_receipt['in_return_qty'] + $product_receipt['in_etc_qty'];
    $product_receipt['in_price']            = $product_receipt['in_point_price'] + $product_receipt['in_move_price'] + $product_receipt['in_return_price'] + $product_receipt['in_etc_price'];

    $product_receipt['out_point_qty']       = !empty($stock_list_data) ? $stock_list_data['out_point']*-1 : 0;
    $product_receipt['out_org_move_qty']    = !empty($move_list_data) && isset($move_list_data["out"]) ? $move_list_data["out"] : 0;
    $product_receipt['out_move_sub_qty']    = !empty($stock_list_data) ? $stock_list_data['out_move']*-1 : 0;
    $product_receipt['out_move_qty']        = $product_receipt['out_org_move_qty'] + $product_receipt['out_move_sub_qty'];
    $product_receipt['out_return_qty']      = !empty($stock_list_data) ? $stock_list_data['out_return']*-1 : 0;
    $product_receipt['out_etc_qty']         = !empty($stock_list_data) ? $stock_list_data['out_etc']*-1 : 0;
    $product_receipt['out_qty']             = $product_receipt['out_point_qty'] + $product_receipt['out_move_qty'] + $product_receipt['out_return_qty'] + $product_receipt['out_etc_qty'];
    $product_receipt['out_base_qty']        = $product_receipt['in_base_qty'] + $product_receipt['in_qty'] - $product_receipt['out_qty'];

    # 판매반출 금액 계산
    $sales_price                            = ($product_receipt['in_base_qty']+$product_receipt['in_qty'] > 0) ? (($product_receipt['in_base_price']+$product_receipt['in_price']) / ($product_receipt['in_base_qty']+$product_receipt['in_qty'])) : 0;
    $product_receipt['out_point_price']     = $sales_price * $product_receipt['out_point_qty'];
    $product_receipt['out_move_price']      = $sales_price * $product_receipt['out_move_qty'];
    $product_receipt['out_return_price']    = $sales_price * $product_receipt['out_return_qty'];
    $product_receipt['out_etc_price']       = $sales_price * $product_receipt['out_etc_qty'];
    $product_receipt['out_price']           = $product_receipt['out_point_price'] + $product_receipt['out_move_price'] + $product_receipt['out_return_price'] + $product_receipt['out_etc_price'];
    $product_receipt['out_base_price']      = $product_receipt['in_base_price'] + $product_receipt['in_price'] - $product_receipt['out_price'];

    $product_receipt_list[$product_receipt['prd_unit']][$product_receipt['log_c_no']][$product_receipt['warehouse']] = $product_receipt;
}

$base_in_stock_not_matching_list    = [];
$base_in_price_not_matching_list    = [];
$base_out_stock_not_matching_list   = [];
$base_out_price_not_matching_list   = [];

$sales_in_stock_not_matching_list   = [];
$sales_in_price_not_matching_list   = [];
$sales_out_stock_not_matching_list  = [];
$sales_out_price_not_matching_list  = [];

$move_in_stock_not_matching_list    = [];
$move_in_price_not_matching_list    = [];
$move_out_stock_not_matching_list   = [];
$move_out_price_not_matching_list   = [];

$return_in_stock_not_matching_list  = [];
$return_in_price_not_matching_list  = [];
$return_out_stock_not_matching_list = [];
$return_out_price_not_matching_list = [];

$etc_in_stock_not_matching_list     = [];
$etc_in_price_not_matching_list     = [];
$etc_out_stock_not_matching_list    = [];
$etc_out_price_not_matching_list    = [];

foreach($excel_stock_list as $prd_unit => $logData)
{
    foreach($logData as $log_c_no => $warehouseData)
    {
        foreach($warehouseData as $warehouse => $stockData)
        {
            $receiptData = $product_receipt_list[$prd_unit][$log_c_no][$warehouse];

            $notMatchData = !empty($receiptData) ? $receiptData : $stockData;

            if($stockData['in_base_qty'] != $receiptData['in_base_qty']){
                $notMatchData['excel_in_base_qty']      = $stockData['in_base_qty'];
                $notMatchData['receipt_in_base_qty']    = $receiptData['in_base_qty'];
                $base_in_stock_not_matching_list[]      = $notMatchData;
            }

            if($stockData['in_point_qty'] != $receiptData['in_point_qty']){
                $notMatchData['excel_in_point_qty']     = $stockData['in_point_qty'];
                $notMatchData['receipt_in_point_qty']   = $receiptData['in_point_qty'];
                $sales_in_stock_not_matching_list[]     = $notMatchData;
            }

            if($stockData['in_move_qty'] != $receiptData['in_move_qty']){
                $notMatchData['excel_in_move_qty']      = $stockData['in_move_qty'];
                $notMatchData['receipt_in_move_qty']    = $receiptData['in_move_qty'];
                $move_in_stock_not_matching_list[]      = $notMatchData;
            }

            if($stockData['in_return_qty'] != $receiptData['in_return_qty']){
                $notMatchData['excel_in_return_qty']    = $stockData['in_return_qty'];
                $notMatchData['receipt_in_return_qty']  = $receiptData['in_return_qty'];
                $return_in_stock_not_matching_list[]    = $notMatchData;
            }

            if($stockData['in_etc_qty'] != $receiptData['in_etc_qty']){
                $notMatchData['excel_in_etc_qty']       = $stockData['in_etc_qty'];
                $notMatchData['receipt_in_etc_qty']     = $receiptData['in_etc_qty'];
                $etc_in_stock_not_matching_list[]       = $notMatchData;
            }

            if($stockData['out_base_qty'] != $receiptData['out_base_qty']){
                $notMatchData['excel_out_base_qty']     = $stockData['out_base_qty'];
                $notMatchData['receipt_out_base_qty']   = $receiptData['out_base_qty'];
                $base_out_stock_not_matching_list[]     = $notMatchData;
            }

            if($stockData['out_point_qty'] != $receiptData['out_point_qty']){
                $notMatchData['excel_out_point_qty']    = $stockData['out_point_qty'];
                $notMatchData['receipt_out_point_qty']  = $receiptData['out_point_qty'];
                $sales_out_stock_not_matching_list[]    = $notMatchData;
            }

            if($stockData['out_move_qty'] != $receiptData['out_move_qty']){
                $notMatchData['excel_out_move_qty']     = $stockData['out_move_qty'];
                $notMatchData['receipt_out_move_qty']   = $receiptData['out_move_qty'];
                $move_out_stock_not_matching_list[]     = $notMatchData;
            }

            if($stockData['out_return_qty'] != $receiptData['out_return_qty']){
                $notMatchData['excel_out_return_qty']   = $stockData['out_return_qty'];
                $notMatchData['receipt_out_return_qty'] = $receiptData['out_return_qty'];
                $return_out_stock_not_matching_list[]   = $notMatchData;
            }

            if($stockData['out_etc_qty'] != $receiptData['out_etc_qty']){
                $notMatchData['excel_out_etc_qty']      = $stockData['out_etc_qty'];
                $notMatchData['receipt_out_etc_qty']    = $receiptData['out_etc_qty'];
                $etc_out_stock_not_matching_list[]      = $notMatchData;
            }
        }
    }
}

$smarty->assign("not_option_list", $not_option_list);
$smarty->assign("base_in_stock_not_matching_list", $base_in_stock_not_matching_list);
$smarty->assign("base_in_stock_cnt", count($base_in_stock_not_matching_list));
$smarty->assign("sales_in_stock_not_matching_list", $sales_in_stock_not_matching_list);
$smarty->assign("sales_in_stock_cnt", count($sales_in_stock_not_matching_list));
$smarty->assign("move_in_stock_not_matching_list", $move_in_stock_not_matching_list);
$smarty->assign("move_in_stock_cnt", count($move_in_stock_not_matching_list));
$smarty->assign("return_in_stock_not_matching_list", $return_in_stock_not_matching_list);
$smarty->assign("return_in_stock_cnt", count($return_in_stock_not_matching_list));
$smarty->assign("etc_in_stock_not_matching_list", $etc_in_stock_not_matching_list);
$smarty->assign("etc_in_stock_cnt", count($etc_in_stock_not_matching_list));

$smarty->assign("sales_out_stock_not_matching_list", $sales_out_stock_not_matching_list);
$smarty->assign("sales_out_stock_cnt", count($sales_out_stock_not_matching_list));
$smarty->assign("move_out_stock_not_matching_list", $move_out_stock_not_matching_list);
$smarty->assign("move_out_stock_cnt", count($move_out_stock_not_matching_list));
$smarty->assign("return_out_stock_not_matching_list", $return_out_stock_not_matching_list);
$smarty->assign("return_out_stock_cnt", count($return_out_stock_not_matching_list));
$smarty->assign("etc_out_stock_not_matching_list", $etc_out_stock_not_matching_list);
$smarty->assign("etc_out_stock_cnt", count($etc_out_stock_not_matching_list));

$smarty->display('product_cms_stock_receipt_inspection.html');
?>
