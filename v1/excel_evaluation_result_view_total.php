<?php
ini_set("max_execution_time", 3600);

require('inc/common.php');
require('ckadmin.php');
require('inc/model/Evaluation.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
	->setLastModifiedBy("Maarten Balliauw")
	->setTitle("Office 2007 XLSX Test Document")
	->setSubject("Office 2007 XLSX Test Document")
	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	->setKeywords("office 2007 openxml php")
	->setCategory("Evaluation Result");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

function setBorder($sheet, $row)
{
	$sheet->getStyle($row)->applyFromArray(
		array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)
		)
	);
}

$eng_abc  = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
$eng_abcc = [];
$eng_list = $eng_abc;
foreach($eng_abc as $a){
    foreach($eng_abc as $b){
        $eng_list[] = $a.$b;
        $eng_abcc[] = $a.$b;
    }
}

foreach($eng_abcc as $aa){
    foreach($eng_abc as $b){
        $eng_list[] = $aa.$b;
    }
}

$sch_ev_no 	        = isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$ev_system_model 	= Evaluation::Factory();
$evaluation_system 	= $ev_system_model->getItem($sch_ev_no);
$ev_system_list	    = $ev_system_model->getEvSystemMultiRaterList($session_s_no);
$eval_year			= date("Y", strtotime($evaluation_system['ev_s_date']));

$ev_result_list			= [];
$ev_result_self_list	= [];
$ev_result_text_list	= [];
$ev_result_total_list	= [];
$ev_result_total_data   = [];
$ev_prev_total_list		= [];
$ev_result_rec_total_list	= [];
$ev_simple_question_list	= [];

$add_where          = "1=1 AND er.ev_no='{$sch_ev_no}' AND er.is_complete='1' AND (er.rec_is_review='1' AND er.eval_is_review='1') AND esr.evaluation_state IN(1,2,3)";
$add_self_where     = "1=1 AND sub.ev_no='{$sch_ev_no}' AND sub.is_complete='1' AND (sub.rec_is_review='1' AND sub.eval_is_review='1') AND sub.receiver_s_no <> sub.evaluator_s_no AND sub.receiver_s_no=er.receiver_s_no";
$add_text_where     = "1=1 AND er.ev_no='{$sch_ev_no}' AND er.is_complete='1' AND (er.rec_is_review='1' AND er.eval_is_review='1') AND er.receiver_s_no <> er.evaluator_s_no AND esr.evaluation_state IN(4,5)";


# 평가지 항목
$evaluation_unit_sql    = "SELECT * FROM evaluation_unit WHERE ev_u_set_no='{$evaluation_system['ev_u_set_no']}' AND active='1' AND evaluation_state NOT IN(99,100) ORDER BY `order`, page ASC";
$evaluation_unit_query  = mysqli_query($my_db,$evaluation_unit_sql);
$ev_unit_question_list  = [];
$ev_self_default        = [];
$page_idx               = 0;
$page_chk               = 1;
while($evaluation_unit = mysqli_fetch_array($evaluation_unit_query)) {

    if($page_chk == $evaluation_unit['page']){
        $page_idx++;
    }else{
        $page_chk = $evaluation_unit['page'];
        $page_idx = 1;
    }

    $ev_question_subject = $evaluation_unit['page']."-".$page_idx.". ".$evaluation_unit['question'];
    $ev_unit_question_list[$evaluation_unit['ev_u_no']] = array(
        "kind"   	=> $evaluation_unit['kind'],
        "subject"   => $ev_question_subject,
        "state"    	=> $evaluation_unit['evaluation_state'],
        "question"  => $evaluation_unit['question']
    );

    if($evaluation_unit['evaluation_state'] == '1' || $evaluation_unit['evaluation_state'] == '2' || $evaluation_unit['evaluation_state'] == '3'){
        $ev_self_default[$evaluation_unit['ev_u_no']] = array('self_count' => 0, 'self_result' => 0);
    }
}

# 자기평가 점수 리스트
$ev_self_sql 	= "
    SELECT 
        esr.ev_u_no,
        esr.evaluation_value,
        er.receiver_s_no,
        (SELECT t.team_name FROM team t WHERE t.team_code=er.receiver_team) AS t_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no=er.receiver_s_no) AS s_name,
        (SELECT s.`position` FROM staff s WHERE s.s_no=er.receiver_s_no) AS s_position,
        (SELECT COUNT(sub.ev_r_no) FROM evaluation_relation sub WHERE {$add_self_where}) AS eval_cnt 
    FROM evaluation_system_result esr 
    LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no 
    WHERE {$add_where} AND esr.receiver_s_no=esr.evaluator_s_no
    ORDER BY s_name
";
$ev_self_query		= mysqli_query($my_db, $ev_self_sql);
$ev_self_total_tmp	= [];
while($ev_self = mysqli_fetch_assoc($ev_self_query)){
    if(!isset($ev_result_self_list[$ev_self['receiver_s_no']])){
        $ev_result_self_list[$ev_self['receiver_s_no']] = array(
            "t_name" 		=> $ev_self['t_name'],
            "s_name" 		=> $ev_self['s_name'],
            "s_position"	=> $ev_self['s_position'],
            "eval_cnt"		=> $ev_self['eval_cnt']
        );
    }
    $ev_result_self_list[$ev_self['receiver_s_no']][$ev_self['ev_u_no']] = !empty($ev_self['evaluation_value']) ? $ev_self['evaluation_value'] : 0;

    if(!isset($ev_self_total_tmp[$ev_self['receiver_s_no']])){
        $ev_self_total_tmp[$ev_self['receiver_s_no']] = array("total" => 0, "cnt" => 0);
    }

    $ev_self_total_tmp[$ev_self['receiver_s_no']]['total'] += !empty($ev_self['evaluation_value']) ? $ev_self['evaluation_value'] : 0;
    $ev_self_total_tmp[$ev_self['receiver_s_no']]['cnt']++;
}

if(!empty($ev_self_total_tmp)) {
    foreach($ev_self_total_tmp as $self_s_no => $self_data) {
        $ev_result_self_list[$self_s_no]['avg'] = round($self_data['total']/$self_data['cnt'], 2);
    }
}

# 평가의견 리스트
$ev_text_sql 	= "SELECT * FROM evaluation_system_result esr LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no WHERE {$add_text_where} ORDER BY esr.ev_u_no";
$ev_text_query	= mysqli_query($my_db, $ev_text_sql);
while($ev_text = mysqli_fetch_assoc($ev_text_query)){
    $ev_result_text_list[$ev_text['receiver_s_no']][$ev_text['ev_u_no']][] = $ev_text['evaluation_value'];
}

# 현평가 전체평균 리스트
$ev_result_total_sql 	= "
    SELECT
        ev_no,
        ev_u_no,
        AVG(avg_sqrt) as avg_sqrt
    FROM
    (
        SELECT 
            esr.ev_no,
            esr.ev_u_no, 
            esr.receiver_s_no,
            AVG(esr.`sqrt`) AS avg_sqrt
        FROM evaluation_system_result as esr
        LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
        WHERE esr.ev_no='{$sch_ev_no}' AND er.is_complete='1' AND (er.rec_is_review='1' AND er.eval_is_review='1') AND esr.evaluation_state IN(1,2,3) AND esr.evaluator_s_no <> esr.receiver_s_no
        GROUP BY esr.receiver_s_no, esr.ev_u_no
        ORDER BY esr.receiver_s_no, esr.ev_u_no
    ) as ev_result
    GROUP BY ev_u_no
    ORDER BY ev_u_no
";
$ev_result_total_query 	= mysqli_query($my_db, $ev_result_total_sql);
$ev_result_total_avg 	= 0;
$ev_result_total_cnt 	= 0;
while($ev_result_total = mysqli_fetch_assoc($ev_result_total_query)){
    $ev_total_avg = round($ev_result_total['avg_sqrt'], 2);

    $ev_result_total_list[$sch_ev_no]["subject"] = $eval_year." 전체평균";
    $ev_result_total_list[$sch_ev_no][$ev_result_total['ev_u_no']] = $ev_total_avg;
    $ev_result_total_data[$sch_ev_no][] = $ev_total_avg;
    $ev_result_total_avg 	+= $ev_total_avg;
    $ev_result_total_cnt++;
}

$ev_result_total_list[$sch_ev_no]['avg'] = 0;
if($ev_result_total_avg > 0 && $ev_result_total_cnt > 0){
    $ev_result_total_list[$sch_ev_no]['avg'] = round($ev_result_total_avg/$ev_result_total_cnt, 2);
}

# 평가 평균
$ev_result_sql = "
		SELECT 
			esr.ev_u_no, 
			esr.receiver_s_no,
			AVG(esr.`sqrt`) AS avg_sqrt
		FROM evaluation_system_result as esr
		LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
		WHERE {$add_where} AND esr.evaluator_s_no <> esr.receiver_s_no
		GROUP BY esr.receiver_s_no, esr.ev_u_no
		ORDER BY esr.receiver_s_no, esr.ev_u_no
	";
$ev_result_query 	= mysqli_query($my_db, $ev_result_sql);
$ev_result_tmp_list	= [];
$ev_result_tmp_value= [];
while($ev_result = mysqli_fetch_assoc($ev_result_query))
{
    $ev_avg_sqrt = round($ev_result['avg_sqrt'],2);

    $ev_result_list[$ev_result['receiver_s_no']][$ev_result['ev_u_no']] = $ev_avg_sqrt;
    $ev_result_tmp_value[$ev_result['receiver_s_no']][] = $ev_avg_sqrt;
    $ev_result_tmp_list[$ev_result['receiver_s_no']]['total'] += $ev_avg_sqrt;
    $ev_result_tmp_list[$ev_result['receiver_s_no']]['cnt']++;
}

foreach($ev_result_self_list as $rec_s_no => $self_data)
{
    $ev_self_result_data = $ev_result_tmp_value[$rec_s_no];

    foreach($ev_system_list as $ev_no => $ev_subject)
    {
        if($ev_no < $sch_ev_no)
        {
            $rec_ev_system 	= $ev_system_model->getItem($ev_no);
            $rec_cur_year	= date("Y", strtotime($rec_ev_system['ev_s_date']));
            $rec_total_sql  = "
                SELECT 
                    esr.ev_no,
                    esr.ev_u_no, 
                    AVG(esr.`sqrt`) AS avg_sqrt
                FROM evaluation_system_result as esr
                LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
                WHERE esr.ev_no='{$ev_no}' AND er.receiver_s_no='{$rec_s_no}' AND er.is_complete='1' AND (er.rec_is_review='1' AND er.eval_is_review='1') AND esr.evaluation_state IN(1,2,3) AND esr.evaluator_s_no <> esr.receiver_s_no
                GROUP BY esr.ev_u_no
                ORDER BY esr.ev_u_no
            ";
            $rec_total_query 	= mysqli_query($my_db, $rec_total_sql);
            $rec_total_list 	= [];
            $rec_total_avg		= 0;
            $rec_total_cnt		= 0;
            while($rec_total = mysqli_fetch_assoc($rec_total_query))
            {
                if($rec_total_cnt == 0 ){
                    $rec_total_list["subject"] = $rec_cur_year." 평가결과";
                }

                $rec_u_avg = round($rec_total['avg_sqrt'], 2);
                $rec_total_list[$rec_total['ev_u_no']] 	= $rec_u_avg;

                $rec_total_avg += $rec_u_avg;
                $rec_total_cnt++;
            }

            if(!empty($rec_total_list))
            {
                if($rec_total_avg > 0 && $rec_total_cnt > 0){
                    $rec_total_list['avg'] = round($rec_total_avg/$rec_total_cnt, 2);
                }

                $ev_result_rec_total_list[$rec_s_no][$ev_no] = $rec_total_list;
            }
        }
    }
}

if($ev_result_tmp_list){
    foreach ($ev_result_tmp_list as $rec_s_no => $result_data){
        $ev_result_list[$rec_s_no]['avg'] = round($result_data['total']/$result_data['cnt'], 2);
    }
}

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

if(!empty($ev_result_self_list))
{
    $sheetIdx = 0;
    foreach ($ev_result_self_list as $rec_s_no => $self_data)
    {
        if($sheetIdx > 0){
            $objPHPExcel->createSheet($sheetIdx);
        }

        $sheetName = $self_data['s_name']." 평가결과";
        $workSheet = $objPHPExcel->setActiveSheetIndex($sheetIdx);
        $workSheet->getDefaultStyle()->getFont()
            ->setName('맑은 고딕')
            ->setSize(10);

        $workSheet->mergeCells('A1:F1');
        $workSheet->getStyle('A1:F1')->getFont()->setSize(18);
        $workSheet->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $workSheet->getColumnDimension('A')->setWidth(15);
        $workSheet->getColumnDimension('B')->setWidth(45);
        $workSheet->getColumnDimension('C')->setWidth(20);
        $workSheet->getColumnDimension('D')->setWidth(20);

        // 상단 타이틀
        $workSheet->setCellValue('A1', $evaluation_system['subject']);

        // 기본 정보
        $workSheet->setCellValue('A3', '■ 기본정보');
        $workSheet->setCellValue('A4', '피평가자명');
        $workSheet->setCellValue('B4', $self_data['s_name']);
        $workSheet->setCellValue('A5', '직책');
        $workSheet->setCellValue('B5', $self_data['s_position']);
        $workSheet->setCellValue('A6', '소속');
        $workSheet->setCellValue('B6', $self_data['t_name']);
        $workSheet->setCellValue('A7', '평가 진행 기간');
        $workSheet->setCellValue('B7', $evaluation_system['ev_s_date']." ~ ".$evaluation_system['ev_e_date']);
        $workSheet->setCellValue('A8', '평가자 수');
        $workSheet->setCellValue('B8', $self_data['eval_cnt']);
        $workSheet->getStyle("A4:A8")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF002060');
        $workSheet->getStyle("A4:A8")->getFont()->setColor($fontColor);
        $workSheet->getStyle("A1:A10")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $workSheet->getStyle("B1:B8")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $workSheet->setCellValue('A10', '■ 평가항목 및 평정표');
        $workSheet->setCellValue('A11', '구분');
        $workSheet->setCellValue('B11', '질문');
        $workSheet->setCellValue('C11', '자기평가(원점수)');
        $workSheet->setCellValue('D11', "{$eval_year} 평가결과");

        $alpha_idx  = 4;
        $last_alpha = "E";
        foreach($ev_result_total_list as $ev_no => $ev_result_total)
        {
            $alpha = $eng_list[$alpha_idx];
            $workSheet->setCellValue("{$alpha}11", "{$ev_result_total['subject']}");
            $workSheet->getColumnDimension("{$alpha}")->setWidth(20);
            $alpha_idx++;
        }

        if(isset($ev_result_rec_total_list[$rec_s_no]))
        {
            foreach($ev_result_rec_total_list[$rec_s_no] as $ev_no => $ev_rec_total)
            {
                $alpha = $eng_list[$alpha_idx];
                $workSheet->setCellValue("{$alpha}11", "{$ev_rec_total['subject']}");
                $workSheet->getColumnDimension("{$alpha}")->setWidth(20);
                $last_alpha = $alpha;
                $alpha_idx++;
            }
        }

        $workSheet->getStyle("A11:{$last_alpha}11")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C9C9C9');
        $workSheet->getStyle("A11:{$last_alpha}11")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $workSheet->getStyle("A11:{$last_alpha}11")->getFont()->setBold(true);


        $result_data = $ev_result_list[$rec_s_no];
        $idx = 12;
        foreach($ev_unit_question_list as $ev_u_no => $unit_data)
        {
            if($unit_data['state'] != '4' && $unit_data['state'] != '5')
            {
                $workSheet->setCellValue("A{$idx}", "{$unit_data['kind']}");
                $workSheet->setCellValue("B{$idx}", "{$unit_data['subject']}");
                $workSheet->setCellValue("C{$idx}", "{$self_data[$ev_u_no]}");
                $workSheet->setCellValue("D{$idx}", "{$result_data[$ev_u_no]}");

                $alpha_idx = 4;
                foreach($ev_result_total_list as $ev_no => $ev_result_total)
                {
                    $alpha = $eng_list[$alpha_idx];
                    $workSheet->setCellValue("{$alpha}{$idx}", "{$ev_result_total[$ev_u_no]}");
                    $alpha_idx++;
                }

                if(isset($ev_result_rec_total_list[$rec_s_no]))
                {
                    foreach($ev_result_rec_total_list[$rec_s_no] as $ev_no => $ev_rec_total)
                    {
                        $alpha = $eng_list[$alpha_idx];
                        $workSheet->setCellValue("{$alpha}{$idx}", "{$ev_rec_total[$ev_u_no]}");
                        $alpha_idx++;
                    }
                }

                $workSheet->getStyle("A{$idx}:B{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF002060');
                $workSheet->getStyle("A{$idx}:B{$idx}")->getFont()->setColor($fontColor);
                $workSheet->getStyle("A{$idx}:A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $workSheet->getStyle("B{$idx}:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                $workSheet->getStyle("E{$idx}:{$last_alpha}{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C9C9C9');
                $workSheet->getStyle("C{$idx}:{$last_alpha}{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $workSheet->getStyle("E{$idx}:{$last_alpha}{$idx}")->getFont()->setBold(true);

                $idx++;
            }
        }

        $workSheet->mergeCells("A{$idx}:B{$idx}");
        $workSheet->setCellValue("A{$idx}", "평균");
        $workSheet->getStyle("A{$idx}")->getFont()->setBold(true);
        $workSheet->getStyle("A{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C9C9C9');
        $workSheet->getStyle("A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $workSheet->setCellValue("C{$idx}", $self_data['avg']);
        $workSheet->setCellValue("D{$idx}", $result_data['avg']);

        $alpha_idx = 4;
        foreach($ev_result_total_list as $ev_no => $ev_result_total)
        {
            $alpha = $eng_list[$alpha_idx];
            $workSheet->setCellValue("{$alpha}{$idx}", "{$ev_result_total["avg"]}");
            $alpha_idx++;
        }

        if(isset($ev_result_rec_total_list[$rec_s_no]))
        {
            foreach($ev_result_rec_total_list[$rec_s_no] as $ev_no => $ev_rec_total)
            {
                $alpha = $eng_list[$alpha_idx];
                $workSheet->setCellValue("{$alpha}{$idx}", "{$ev_rec_total["avg"]}");
                $alpha_idx++;
            }
        }

        $workSheet->getStyle("E{$idx}:{$last_alpha}{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C9C9C9');
        $workSheet->getStyle("C{$idx}:{$last_alpha}{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $workSheet->getStyle("E{$idx}:{$last_alpha}{$idx}")->getFont()->setBold(true);

        $idx += 2;
        $workSheet->setCellValue("A{$idx}", '■ 평가의견');
        $idx++;

        if(isset($ev_result_text_list[$rec_s_no]))
        {
            $ev_text_data = $ev_result_text_list[$rec_s_no];

            foreach ($ev_text_data as $ev_u_no => $ev_text)
            {
                $workSheet->mergeCells("A{$idx}:{$last_alpha}{$idx}");
                $workSheet->setCellValue("A{$idx}", $ev_unit_question_list[$ev_u_no]['subject']);
                $workSheet->getStyle("A{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF002060');
                $workSheet->getStyle("A{$idx}")->getFont()->setColor($fontColor);
                $workSheet->getStyle("A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $workSheet->getRowDimension($idx)->setRowHeight(20);
                $idx++;

                if(!empty($ev_text))
                {
                    foreach($ev_text as $text)
                    {
                        $workSheet->mergeCells("A{$idx}:{$last_alpha}{$idx}");
                        $workSheet->setCellValue("A{$idx}", $text);
                        $workSheet->getStyle("A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                        $workSheet->getRowDimension($idx)->setRowHeight(20);
                        $workSheet->getStyle("A{$idx}")->getAlignment()->setWrapText(true);
                        $idx++;
                    }
                }
            }
        }


        $workSheet->getStyle("A1:{$last_alpha}{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $workSheet->getPageSetup()->setFitToPage(true);
        $workSheet->setTitle($sheetName);

        $sheetIdx++;
    }
}

$objPHPExcel->setActiveSheetIndex(0);
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="evaluation_result_'.$today.'.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
