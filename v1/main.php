<?php
ini_set("default_socket_timeout", 30);

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_date.php');
require('inc/helper/asset.php');
require('inc/helper/okr.php');
require('inc/model/Asset.php');
require('inc/model/Custom.php');
require('inc/model/Calendar.php');
require('inc/model/Product.php');
require('inc/model/Work.php');
require('inc/model/Team.php');
require('inc/restapi.php');
require('inc/LunarToSola.lib.php'); # 양력 <-> 음력 계산기

// NAVER API [START]
$config = parse_ini_file("inc/n_account.ini");
$api 	= new RestApi($config['BASE_URL'], $config['API_KEY'], $config['SECRET_KEY'], $config['CUSTOMER_ID']);
// NAVER API [END]

# Model Init
$work_model 	= Work::Factory();
$product_model 	= Product::Factory();
$to_do_model 	= Custom::Factory();
$to_do_model->setMainInit("to_do_list", "td_no");
$custom_model 	= Custom::Factory();
$reserve_model 	= Asset::Factory();
$reserve_model->setMainInit("asset_reservation", "as_r_no");

# 프로세스 처리
$process = (isset($_POST['process'])) ? $_POST['process'] : "";
if($process == "save_extension") #연장,미연장 처리
{
	$w_no		 	= (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
	$f_extension 	= isset($_POST['f_extension']) ? $_POST['f_extension'] : "";

	$work_item   	= $work_model->getItem($w_no);
	$product_item 	= $product_model->getItem($work_item['prd_no']);

	$work_model->update(array("w_no" => $w_no, "extension" => $f_extension));

	if($f_extension == '1') # 연장시 복제하여 신규로 요청하기
	{
		$extension_date = $work_item['extension_date'];

		$task_req_s_no_value 	= $session_s_no;
		$task_req_s_team_value 	= $session_team;
		$work_state_value 		= '3';

		# 외주관리자가 연장시 업무요청자는 업체 담당자로 하고 접수완료로 처리함
		if(permissionNameCheck($session_permission,"외주관리자")){
			$task_req_s_no_value 	= $work_item['s_no'];
			$task_req_s_team_value 	= $work_item['team'];
			$work_state_value 		= '4';
		}

		$duplicate_data = array(
			"regdate" 		=> date("Y-m-d H:i:s"),
			"work_state"	=> $work_state_value,
			"c_no"			=> $work_item['c_no'],
			"c_name"		=> $work_item['c_name'],
			"s_no"			=> $work_item['s_no'],
			"team"			=> $work_item['team'],
			"prd_no"		=> $work_item['prd_no'],
			"t_keyword"		=> addslashes($work_item['t_keyword']),
			"r_keyword"		=> addslashes($work_item['r_keyword']),
			"wd_price"		=> $work_item['wd_price'],
			"wd_price_vat"	=> $work_item['wd_price_vat'],
			"wd_c_name"		=> $work_item['wd_c_name'],
			"wd_c_no"		=> $work_item['wd_c_no'],
			"extension_date"=> date("Y-m-d", strtotime("{$extension_date} +1 months")),
			"task_req"		=> "연장 ({$w_no})",
			"task_req_s_no"	=> $task_req_s_no_value,
			"task_req_team"	=> $task_req_s_team_value,
			"work_time"		=> $product_item['work_time_default'],
		);

		if($product_model->getTaskRunStaffCnt($product_item['prd_no']) == '1')
		{
			$duplicate_data['task_run_s_no'] = $work_item['task_run_s_no'];
			$duplicate_data['task_run_team'] = $work_item['task_run_team'];
		}

		if($work_model->insert($duplicate_data)) {
			exit("<script>location.href='main.php';</script>");
		}else {
			exit("<script>alert('연장 요청에 실패 하였습니다');location.href='main.php';</script>");
		}
	}
	elseif($f_extension == '3')
	{
		exit("<script>alert('w_no = {$w_no} 미연장 하였습니다');location.href='main.php';</script>");
	}
}
elseif($process == "monthly_keyword_cnt") #월간 키워드 검색
{
	$f_keyword_new_arr 		= explode("\n", $_POST['f_keyword_new']); // 줄바꿈 기준 array 적용
	$f_keyword_new 			= str_replace(" ", "", $_POST['f_keyword_new']); // 공백 제거
	$f_keyword_new_trim_arr = explode("\n", $f_keyword_new); // 공백 제거 후 줄바꿈 기준 array 적용

    echo "<table cellpadding='0' cellspacing='0' id='keyword-table' class='keyword-table'>";
	echo "<thead>";
	echo "<tr>";
	echo "<th class='first' style='min-width:180px;max-width:180px;background-color: #dedede; color: #585858;'>키워드</th>";
	echo "<th style='min-width:72px;max-width:72px;background-color: #dedede; color: #585858;'>PC</th>";
	echo "<th style='min-width:72px;max-width:72px;background-color: #dedede; color: #585858;'>Mobile</th>";
	echo "<th style='min-width:90px;max-width:90px;background-color: #dedede; color: #585858;'>Total</th>";
	echo "</tr>";
	echo "</thead>";
	echo "<tbody>";

	$hintKeywords_5 = "";
	$cnt_i 			= 0;
	for($arr_5=0; $arr_5 < count($f_keyword_new_arr); $arr_5++)
	{
		$f_keyword_new_trim_arr[$arr_5] = strtoupper($f_keyword_new_trim_arr[$arr_5]);

		if($hintKeywords_5 == ""){
			$hintKeywords_5 = $f_keyword_new_trim_arr[$arr_5];
		}else{
			$hintKeywords_5 = $hintKeywords_5.",".$f_keyword_new_trim_arr[$arr_5];
		}
		$cnt_i++;

		# 5개 단위로 묶어서 검색량 조회
		if(($arr_5+1)%5 == 0 || $arr_5 == (count($f_keyword_new_arr)-1))
		{
			$keyword_list 	= array("hintKeywords" => $hintKeywords_5);
			$response 		= $api->GET('/keywordstool', str_replace(' ','',$keyword_list));
			$hintKeywords_5 = ""; // 초기화

			for($arr_j=0; $arr_j < count($f_keyword_new_arr); $arr_j++)
			{
				if($arr_j < ($arr_5-($cnt_i-1)) || $arr_j > $arr_5)
					continue;

				$responseCount = (isset($response["keywordList"]) && !empty($response["keywordList"])) ? count($response["keywordList"]) : 0;
				for($arr_i=0 ; $arr_i < $responseCount; $arr_i++)
				{
					if($response["keywordList"][$arr_i]["relKeyword"] == $f_keyword_new_trim_arr[$arr_j])
					{
						echo "<tr>";
						echo "<td class='first'>{$f_keyword_new_arr[$arr_j]}</td>";
						echo "<td style='text-align: right;'>{$response["keywordList"][$arr_i]["monthlyPcQcCnt"]}</td>";
						echo "<td style='text-align: right;'>{$response["keywordList"][$arr_i]["monthlyMobileQcCnt"]}</td>";
						$total_value = (int)$response["keywordList"][$arr_i]["monthlyPcQcCnt"] + (int)$response["keywordList"][$arr_i]["monthlyMobileQcCnt"];
						echo "<td style='text-align: right;'>{$total_value}</td>";
						echo "</tr>";
						break;
					}
				}
			}
			$cnt_i=0;
			usleep(1000000);
		}
	}
	echo "</tbody>";
	echo "</table>";
    echo "<div class='copy-table'><input type='button' class='copy-table-btn' value='표 복사' onclick='copyKeywordTable();' /></div>";
	exit;
}
elseif($process == "save_okr") #OKR 완료율, 메모 저장
{
	$no 		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$type 		= (isset($_POST['type'])) ? $_POST['type'] : "";

	$custom_model->setMainInit("okr_kr", "ok_no");
	if (!$custom_model->update(array("ok_no" => $no, $type => addslashes($value))))
		echo "저장에 실패 하였습니다.";
	else
		echo "저장 되었습니다.";
	exit;
}
elseif($process == "save_to_do_list") #000님의 할일(MY TO_O_LIST) :: 진행상태, 날짜, 중요, 할일 저장
{
	$no 		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$type 		= (isset($_POST['type'])) ? $_POST['type'] : "";

	$success_msg 	= "날짜가 저장 되었습니다.";
	$fail_msg 		= "날짜 저장에 실패 하였습니다.";
	if($type == "state"){
		$success_msg 	= "진행상태가 저장 되었습니다.";
		$fail_msg 		= "진행상태 저장에 실패 하였습니다.";
	}
	elseif($type == "important"){
		if($value == '1')
			$success_msg = "중요함으로 설정 되었습니다.";
		elseif($value == '2')
			$success_msg = "중요함 설정이 해지 되었습니다.";

		$fail_msg 		= "중요함 설정에 실패 하였습니다.";
	}
	elseif($type == "contents"){
		$success_msg 	= "할일이 저장 되었습니다.";
		$fail_msg 		= "할일 저장에 실패 하였습니다.";
	}

	if (!$to_do_model->update(array("td_no" => $no, $type => addslashes($value))))
		echo $fail_msg;
	else
		echo $success_msg;

	if($type == "state")
		exit("<script>location.href='main.php';</script>");
	else
		exit;
}
elseif($process == "add_today_to_do_list") #000님의 할일(MY TO_O_LIST) 생성
{
	if (!$to_do_model->insert(array("td_date" => date("Y-m-d"), "s_no" => $session_s_no))){
		exit("<script>alert('할일을 생성하지 못했습니다. 반복 발생시 관리자에게 문의해 주세요.');location.href='main.php';</script>");
	}else{
		exit("<script>location.href='main.php';</script>");
	}
}
elseif($process == "del_to_do_list") #000님의 할일(MY TO_O_LIST) 삭제
{
	$td_no	= (isset($_POST['td_no'])) ? $_POST['td_no'] : "";

	if (!$to_do_model->delete($td_no)){
		exit("<script>alert('할일을 삭제하지 못했습니다. 반복 발생시 관리자에게 문의해 주세요.');location.href='main.php';</script>");
	}else{
		exit("<script>location.href='main.php';</script>");
	}
}
elseif($process == "f_working_state") #000님의 촤근 완료된 업무내역 :: 업무평가 저장
{
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	$custom_model->setMainInit("working_state", "no");
	if (!$custom_model->insert(array("s_no" => $session_s_no, "team" => $session_team, "state" => $value, "regdate" => date("Y-m-d H:i:s"))))
		echo "업무상태 저장에 실패 하였습니다.";
	else
		echo "업무상태가 저장 되었습니다.";

	exit("<script>location.href='main.php';</script>");
}
elseif($process == "new_asset_reservation")
{
	$new_as_no  	= isset($_POST['new_as_no']) ? $_POST['new_as_no'] : "";
	$new_as_name  	= isset($_POST['new_as_name']) ? $_POST['new_as_name'] : "";
	$new_work_state = isset($_POST['new_work_state']) ? $_POST['new_work_state'] : "";
	$new_management = isset($_POST['new_management']) ? $_POST['new_management'] : "";
	$new_manager 	= isset($_POST['new_manager']) ? $_POST['new_manager'] : "";
	$new_sub_manager= isset($_POST['new_sub_manager']) ? $_POST['new_sub_manager'] : "";
	$new_r_s_day	= isset($_POST['new_r_s_day']) ? $_POST['new_r_s_day'] : date('Y-m-d');
	$new_r_e_day	= isset($_POST['new_r_e_day']) ? $_POST['new_r_e_day'] : date('Y-m-d');
	$new_r_s_hour 	= isset($_POST['new_r_s_hour']) ? $_POST['new_r_s_hour'] : "";
	$new_r_s_min	= isset($_POST['new_r_s_min']) ? $_POST['new_r_s_min'] : "";
	$new_r_e_hour 	= isset($_POST['new_r_e_hour']) ? $_POST['new_r_e_hour'] : "";
	$new_r_e_min 	= isset($_POST['new_r_e_min']) ? $_POST['new_r_e_min'] : "";
	$new_task_req 	= isset($_POST['new_task_req']) ? $_POST['new_task_req'] : "";
	$new_regdate	= date('Y-m-d H:i:s');

	$insert_data = array(
		"work_state"  	=> $new_work_state,
		"as_no"  		=> $new_as_no,
		"management"  	=> $new_management,
		"manager"  		=> $new_manager,
		"sub_manager"  	=> $new_sub_manager,
		"task_req"  	=> addslashes(trim($new_task_req)),
		"regdate"  		=> $new_regdate,
		"req_no"  		=> $session_s_no,
		"req_name" 		=> $session_name,
		"req_date" 		=> $new_regdate,
		"r_s_date" 		=> $new_r_s_day." ".$new_r_s_hour.":".$new_r_s_min.":00",
		"r_e_date" 		=> $new_r_e_day." ".$new_r_e_hour.":".$new_r_e_min.":00",
	);

	if($new_work_state == '2')
	{
		$insert_data["run_no"] 	 = $session_s_no;
		$insert_data["run_name"] = $session_name;
		$insert_data["run_date"] = $new_regdate;
	}

	if($reserve_model->insert($insert_data)){
		exit("<script>alert('예약성공 했습니다');location.href='main.php?sch_as_no={$new_as_no}&sch_r_date={$new_r_s_day}';</script>");
	}else {
		exit("<script>alert('오류가 발생했습니다 다시 시도해 주세요.');location.href='main.php?sch_as_no={$new_as_no}&sch_r_date={$new_r_s_day}';</script>");
	}
}
elseif($process == 'cancel_asset_reservation')
{
    $as_r_no    	= isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $sch_as_no  	= isset($_POST['sch_as_no']) ? $_POST['sch_as_no'] : "";
    $r_s_day		= isset($_POST['new_r_s_day']) ? date('Y-m-d',strtotime($_POST['new_r_s_day'])) : "";
    $cancel_data 	= array("as_r_no" => $as_r_no, "work_state" => 4);

    if($reserve_model->update($cancel_data)){
        exit("<script>alert('예약취소 했습니다');location.href='main.php?sch_as_no={$sch_as_no}&sch_r_date={$r_s_day}';</script>");
    }else {
        exit("<script>alert('예약취소에 실패했습니다');location.href='main.php?sch_as_no={$sch_as_no}&sch_r_date={$r_s_day}';</script>");
    }
}
elseif($process == 'modify_asset_reservation')
{
    $as_r_no    = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $sch_as_no  = isset($_POST['sch_as_no']) ? $_POST['sch_as_no'] : "";
    $r_s_day    = isset($_POST['new_r_s_day']) ? date('Y-m-d',strtotime($_POST['new_r_s_day'])) : "";
    $r_s_hour   = isset($_POST['new_r_s_hour']) ? $_POST['new_r_s_hour'] : "";
    $r_s_min    = isset($_POST['new_r_s_min']) ? $_POST['new_r_s_min'] : "";
    $r_e_day    = isset($_POST['new_r_e_day']) ? date('Y-m-d',strtotime($_POST['new_r_e_day'])) : "";
    $r_e_hour   = isset($_POST['new_r_e_hour']) ? $_POST['new_r_e_hour'] : "";
    $r_e_min    = isset($_POST['new_r_e_min']) ? $_POST['new_r_e_min'] : "";
    $task_req   = isset($_POST['new_task_req']) ? addslashes($_POST['new_task_req']) : "";
    $r_s_date 	= $r_s_day." ".$r_s_hour.":".$r_s_min.":00";
    $r_e_date 	= $r_e_day." ".$r_e_hour.":".$r_e_min.":00";
	$upd_data 	= array("as_r_no" => $as_r_no, "task_req" => $task_req, "r_s_date" => $r_s_date, "r_e_date" => $r_e_date);

    if($reserve_model->update($upd_data)){
        exit("<script>alert('예약변경 했습니다');location.href='main.php?sch_as_no={$sch_as_no}&sch_r_date={$r_s_day}';</script>");
    }else {
        exit("<script>alert('예약변경에 실패했습니다');location.href='main.php?sch_as_no={$sch_as_no}&sch_r_date={$r_s_day}';</script>");
    }
}
elseif($process == 'check_holiday')
{
	$chk_cs_no 		= isset($_POST['chk_cs_no']) ? $_POST['chk_cs_no'] : "";
	$chk_cs_model 	= Calendar::Factory();
	$chk_cs_model->setScheduleTable();
	$chk_cs_item 	= $chk_cs_model->getItem($chk_cs_no);

	$chk_ins_sql = "INSERT INTO calendar_schedule_comment SET cal_no='{$chk_cs_item['cal_no']}', cs_no='{$chk_cs_no}', comment='노무수령거부통지를 확인했습니다.', team='{$session_team}', s_no='{$session_s_no}', regdate=now()";
	mysqli_query($my_db, $chk_ins_sql);

	exit("<script>location.href='main.php';</script>");
}

# 현재시간
$cur_year		= date("Y");
$cur_month		= date("Y-m");
$cur_month_val	= date("m");
$cur_date 	  	= date("Y-m-d");
$cur_md			= date("m/d");
$cur_s_datetime = "{$cur_date} 00:00:00";
$cur_e_datetime = "{$cur_date} 23:59:59";
$cur_s_month	= "{$cur_month_val}-01";
$cur_e_day		= date("t", strtotime($cur_date));
$cur_e_month 	= "{$cur_month_val}-{$cur_e_day}";
$cur_hour 	 	= date('H');
$cur_min_cal 	= ceil(date('i')/10)*10;
$cur_min_tmp 	= ($cur_min_cal == '60') ? '00' : $cur_min_cal;
$cur_min     	= sprintf('%02d', $cur_min_tmp);

/* 연차 촉진체크 Start  */
$chk_holiday_sql	= "SELECT `cs`.cs_no, COUNT(*) as cal_cnt, (SELECT COUNT(*) FROM calendar_schedule_comment css WHERE css.cs_no=`cs`.cs_no) as comm_cnt FROM calendar_schedule `cs` WHERE `cs`.cs_s_date BETWEEN '{$cur_s_datetime}' AND '{$cur_e_datetime}' AND cal_id='hr_boost' ANd cal_no='7' AND `cs`.cs_s_no='{$session_s_no}'";
$chk_holiday_query	= mysqli_query($my_db, $chk_holiday_sql);
$chk_holiday_result	= mysqli_fetch_assoc($chk_holiday_query);
$chk_holiday 		= false;
$chk_cs_no 			= "";

if($chk_holiday_result['cal_cnt'] > 0 && $chk_holiday_result['comm_cnt'] == 0){
	$chk_holiday 	= true;
	$chk_cs_no		= $chk_holiday_result['cs_no'];
}
$smarty->assign("chk_holiday", $chk_holiday);
$smarty->assign("chk_cs_no", $chk_cs_no);
/* 연차 촉진체크 END  */

/* 와이즈 공지사항 Start  */
$board_notice_sql = "
	SELECT
		brd_n.no,
		brd_n.b_id,
		brd_n.title,
		brd_n.contents,
		(SELECT s_name FROM staff s where s.s_no=brd_n.s_no) AS s_name,
		brd_n.regdate,
		brd_n.main_view,
		brd_n.popup_view,
		brd_n.main_slider
	FROM board_notice brd_n
	WHERE brd_n.display = '1' AND (brd_n.main_view = '1' OR brd_n.popup_view = '1' OR brd_n.main_slider='1')
";
$board_notice_query 		= mysqli_query($my_db, $board_notice_sql);
$board_notice_main_list 	= [];
$board_notice_popup_list 	= [];
$board_notice_slider_list 	= [];
$check_img_str 				= "<img alt=";
while($board_notice = mysqli_fetch_array($board_notice_query))
{
	$notice_ago = 0;
	$notice_ago = (strtotime($cur_date) - strtotime(date("Y-m-d", strtotime($board_notice['regdate'])))) / (60 * 60 * 24);
	$board_notice['notice_ago'] = $notice_ago;
	$board_notice['url'] = "board/board_notice_detail.php?no={$board_notice['no']}&b_id={$board_notice['b_id']}";

	if($board_notice['main_view'] == '1')
	{
		$board_notice_main_list[] = $board_notice;
	}

	if($board_notice['popup_view'] == '1')
	{
		$board_notice_popup_list[] = $board_notice;
	}

	if($board_notice['main_slider'] == '1')
	{
		if(strpos($board_notice['contents'], $check_img_str) !== false)
		{
			$content = explode($check_img_str, $board_notice['contents']);

			if(isset($content[1])){
				$src_contents = explode("src=\"",$board_notice['contents']);
				if(isset($src_contents[1])){
					$src_content = explode("\"",$src_contents[1]);
					if(isset($src_content[0]) && !empty($src_content[0])){
						$board_notice_slider_list[] = array(
							"b_no" => $board_notice['no'],
							"link" => "board/board_notice_detail.php?no={$board_notice['no']}&b_id=wp_down00",
							"img"  => $src_content[0]
						);
					}
				}
			}
		}
	}
}
$smarty->assign("board_notice_main_list", $board_notice_main_list);
$smarty->assign("board_notice_popup_list", $board_notice_popup_list);
$smarty->assign("board_notice_slider_list", $board_notice_slider_list);
/* 와이즈 공지사항 End */

/* 와플 chat 팝업 START */
$waple_chat_popup_sql = "
	SELECT
        wcc.wcc_no,
        wc.wc_no,
        wc.title,
        wcc.content
    FROM waple_chat_content wcc
    LEFT JOIN waple_chat as wc ON wc.wc_no=wcc.wc_no
    WHERE wc.display='1' AND wcc.display='1' AND wc.`type`='4' AND wcc.s_no='{$session_s_no}' AND wcc.view_type='2'
	ANd (SELECT COUNT(wcr.wcr_no) FROM waple_chat_read wcr WHERE wcr.wcc_no=wcc.wcc_no AND wcr.read_s_no='{$session_s_no}') = 0
    ORDER BY wcc.regdate ASC, wcc.wcc_no ASC
";
$waple_chat_popup_query = mysqli_query($my_db, $waple_chat_popup_sql);
$waple_chat_popup_list	= [];
while($waple_chat_popup = mysqli_fetch_assoc($waple_chat_popup_query))
{
	$chat_content 		= "";
	$chat_content_tmp 	= str_replace("\r\n", "<br/>", $waple_chat_popup['content']);
	if(strpos($chat_content_tmp, "http") !== false)
	{
		$content_val  = explode("<br/>", $chat_content_tmp);
		$content_list = [];

		foreach($content_val as $content_tmp){
			if(strpos($content_tmp, "http") !== false){
				$content_list[] = "<a href='{$content_tmp}' target='_blank' style='color: blue;' class='chat-content-link'>{$content_tmp}</a>";
			}else{
				$content_list[] = $content_tmp;
			}
		}

		$chat_content = implode("<br/>", $content_list);
	}

	$read_sql = "INSERT INTO waple_chat_read SET wcc_no='{$waple_chat_popup['wcc_no']}', read_s_no='{$session_s_no}', read_date=now()";
	mysqli_query($my_db, $read_sql);

	$waple_chat_popup_list[] = $chat_content;
}
$smarty->assign("waple_chat_popup_cnt", count($waple_chat_popup_list));
$smarty->assign("waple_chat_popup_list", $waple_chat_popup_list);
/* 와플 chat 팝업 END */


/* 공용설비 예약시스템 Start */
$sch_asset_no = isset($_GET['sch_as_no']) ? $_GET['sch_as_no'] : "";
$sch_r_date   = (isset($_GET['sch_r_date']) && !empty($_GET['sch_r_date'])) ? date('Y-m-d', strtotime($_GET['sch_r_date'])) : $cur_date;

# 공용설비 자산 예약시간 체크(30분이내)
$ok_asset_share_s_time = date('Y-m-d H:i').":00";
$ok_asset_share_e_time = date('Y-m-d H:i', strtotime('+ 30 min')).":00";

$asset_share_list = [];
$asset_share_reservation_list = [];

# 공용설비 자산 정보
$new_asset_name    = "";
$new_asset_form    = "";
$new_work_state	   = "1";
$new_management	   = "";
$new_manager	   = "";
$new_sub_manager   = "";
$asset_share_sql   = "SELECT `as`.as_no, `as`.name, `as`.form, `as`.management, `as`.use_type, `as`.manager, `as`.sub_manager, (SELECT count(`asr`.`as_r_no`) as cnt FROM asset_reservation `asr` WHERE `asr`.as_no =`as`.as_no AND `asr`.work_state IN('1','2') AND ((`asr`.r_s_date >= '{$ok_asset_share_s_time}' AND `asr`.r_s_date <= '{$ok_asset_share_e_time}') OR (`asr`.r_e_date >= '{$ok_asset_share_s_time}' AND `asr`.r_e_date <= '{$ok_asset_share_e_time}') OR (`asr`.r_s_date < '{$ok_asset_share_s_time}' AND `asr`.r_e_date >= '{$ok_asset_share_e_time}'))) as cur_cnt FROM asset `as` WHERE `as`.share_type='3' AND `as`.asset_state='1' ORDER BY `as`.priority";
$asset_share_query = mysqli_query($my_db, $asset_share_sql);
while($asset_share = mysqli_fetch_assoc($asset_share_query))
{
	if(empty($sch_asset_no)){
		$sch_asset_no = $asset_share['as_no'];
	}

	if($sch_asset_no == $asset_share['as_no'])
	{
		$new_asset_name = $asset_share['name'];
		$new_asset_form = $asset_share['form'];
		$new_management = $asset_share['management'];
		$new_manager 	= $asset_share['manager'];
		$new_sub_manager= $asset_share['sub_manager'];

		if(isset($asset_share['use_type']) && $asset_share['use_type'] == '2'){
			$new_work_state = '2';
		}
	}

	$asset_share_list[$asset_share['as_no']] = array('label' => $asset_share['name'], 'cur_cnt' => ($asset_share['cur_cnt'] > 0) ? false: true);
}

# 공용설비 자산 예약 정보
$asset_share_reservation_sql   = "
	SELECT
		`asr`.as_r_no,
		DATE_FORMAT(`asr`.r_s_date, '%Y%m%d') as r_s_day,
        DATE_FORMAT(`asr`.r_s_date, '%H:%i') as r_s_hour,
        DATE_FORMAT(`asr`.r_e_date, '%Y%m%d') as r_e_day,
        DATE_FORMAT(`asr`.r_e_date, '%H:%i') as r_e_hour,
        req_name
	FROM asset_reservation `asr`
	WHERE as_no='{$sch_asset_no}' AND work_state IN('1','2','10')
	AND (DATE_FORMAT(r_s_date, '%Y-%m-%d')<='{$sch_r_date}' AND DATE_FORMAT(r_e_date, '%Y-%m-%d')>='{$sch_r_date}')
	ORDER BY r_s_date ASC";

$asset_share_reservation_query = mysqli_query($my_db, $asset_share_reservation_sql);
$asset_color = array("#E0F2F7","#CEECF5","#A9E2F3","#81DAF5","#58D3F7","#E0F2F7","#CEECF5","#A9E2F3","#81DAF5","#58D3F7");
$color_i	 = 0;

while($asset_share_reservation = mysqli_fetch_assoc($asset_share_reservation_query))
{
	$s_date_title = $asset_share_reservation['r_s_hour'];
	$e_date_title = $asset_share_reservation['r_e_hour'];

	if($asset_share_reservation['r_s_day'] != $asset_share_reservation['r_e_day']){
		$r_s_day = date("m/d", strtotime($asset_share_reservation['r_s_day']));
		$r_e_day = date("m/d", strtotime($asset_share_reservation['r_e_day']));
		$title = $r_s_day." ".$s_date_title." ~ ".$r_e_day." ".$e_date_title." ".$asset_share_reservation['req_name'];
	}else{
		$title = $s_date_title." ~ ".$e_date_title." ".$asset_share_reservation['req_name'];
	}

	$asset_share_reservation_list[] = array('color' => $asset_color[$color_i], 'title' => $title, 'as_r_no' => $asset_share_reservation['as_r_no']);
	$color_i++;
}

$asset_share_total_num = !empty($asset_share_list) ? count($asset_share_list) : "0";
$asset_share_reservation_total_num = !empty($asset_share_reservation_list) ? count($asset_share_reservation_list) : "0";

$smarty->assign("sch_as_no", $sch_asset_no);
$smarty->assign("sch_r_date", $sch_r_date);
$smarty->assign("sch_prev_date", date('Y-m-d', strtotime("{$sch_r_date} -1 day")));
$smarty->assign("sch_next_date", date('Y-m-d', strtotime("{$sch_r_date} +1 day")));
$smarty->assign("new_asset_form", $new_asset_form);
$smarty->assign("new_as_name", $new_asset_name);
$smarty->assign("new_work_state", $new_work_state);
$smarty->assign("new_management", $new_management);
$smarty->assign("new_manager", $new_manager);
$smarty->assign("new_sub_manager", $new_sub_manager);
$smarty->assign("cur_hour", $cur_hour);
$smarty->assign("cur_min", $cur_min);
$smarty->assign("asset_hour_list", getHourOption());
$smarty->assign("asset_min_list", getMinOption());
$smarty->assign("asset_share_list", $asset_share_list);
$smarty->assign("asset_share_total_num", $asset_share_total_num);
$smarty->assign("asset_share_reservation_list", $asset_share_reservation_list);
$smarty->assign("asset_share_reservation_total_num", $asset_share_reservation_total_num);
/* 공용설비 예약시스템 End */


/* 자주하는 자산 무형(계정)리스트 START */
$session_non_sql 	= "SELECT count(asr.as_r_no) as cnt FROM asset_reservation asr WHERE (SELECT a.object_type FROM asset a WHERE a.as_no=asr.as_no)=3 AND asr.work_state='2' AND asr.req_no='{$session_s_no}'";
$session_non_query 	= mysqli_query($my_db, $session_non_sql);
$session_non_result = mysqli_fetch_assoc($session_non_query);
$session_non_count  = isset($session_non_result['cnt']) ? $session_non_result['cnt'] : 0;

$smarty->assign('session_non_count', $session_non_count);
$non_object_asset_list  	= [];

$staff_quick_asset_sql     	= "SELECT quick_asset FROM staff WHERE s_no='{$session_s_no}'";
$staff_quick_asset_query   	= mysqli_query($my_db, $staff_quick_asset_sql);
$staff_quick_asset_result  	= mysqli_fetch_assoc($staff_quick_asset_query);
$non_object_button_total   	= 1;

if(isset($staff_quick_asset_result['quick_asset']) && !empty($staff_quick_asset_result['quick_asset']))
{
	$asset_state_option = getAssetStateNameOption();

	$staff_quick_asset    	= explode(',',$staff_quick_asset_result['quick_asset']);
	$non_object_asset_sql	= "SELECT `a`.as_no, `a`.asset_state, `a`.object_url, `a`.object_site, `a`.object_id, `a`.object_pw, `a`.notice  FROM asset `a` WHERE as_no IN({$staff_quick_asset_result['quick_asset']}) AND display='1'";
	$non_object_asset_query = mysqli_query($my_db, $non_object_asset_sql);

	foreach($staff_quick_asset as $data){
		$non_object_asset_list[$data] = "";
	}

	while($non_object_asset = mysqli_fetch_assoc($non_object_asset_query))
	{
		if($non_object_button_total <= 10)
		{
			$non_object_asset['asset_state_name'] = isset($asset_state_option[$non_object_asset['asset_state']]) ? $asset_state_option[$non_object_asset['asset_state']] : "";
			$non_object_asset_list[$non_object_asset['as_no']] = $non_object_asset;
		}
		$non_object_button_total++;
	}

	$non_object_asset_list = array_filter($non_object_asset_list);
}

$non_object_asset_total = !empty($non_object_asset_list) ? count($non_object_asset_list) : 0;
$smarty->assign("non_object_asset_list", $non_object_asset_list);
$smarty->assign("non_object_button_total", $non_object_button_total);
$smarty->assign("non_object_asset_total", $non_object_asset_total);
/* 자주하는 자산 무형(계정)리스트 END */


/* 와플 알림톡 START */
$waple_chat_cnt_sql = "
	SELECT
	   	COUNT(wcc.wcc_no) as content_cnt,
		(SELECT COUNT(DISTINCT wcr.wcc_no) FROM waple_chat_read wcr WHERE wcr.read_s_no='{$session_s_no}') AS read_cnt 
	FROM waple_chat_content wcc 
    LEFT JOIN waple_chat as wc ON wc.wc_no=wcc.wc_no 
    WHERE wc.display='1' AND wcc.view_type='1' AND wcc.display='1' AND ((wc.`type`='1') OR (wc.`type`='2' AND wcc.s_no='{$session_s_no}') OR (wc.`type`='3' AND wcc.wc_no IN(SELECT wcu.wc_no FROM waple_chat_user as wcu WHERE wcu.user='{$session_s_no}') AND wcc.s_no != '{$session_s_no}'))
";
$waple_chat_cnt_query  	= mysqli_query($my_db, $waple_chat_cnt_sql);
$waple_chat_cnt_result 	= mysqli_fetch_assoc($waple_chat_cnt_query);
$waple_chat_new_cnt 	= $waple_chat_cnt_result['content_cnt']-$waple_chat_cnt_result['read_cnt'];

$waple_chat_sql = "
	SELECT
	    wc.wc_no,
        wc.title, 
        wcc.content, 
        DATE_FORMAT(wcc.regdate, '%Y-%m-%d %H:%i') AS regdate, 
        IF(wc.type='2','WAPLE',(SELECT s.s_name FROM staff AS s WHERE s.s_no=wcc.s_no)) AS s_name
    FROM waple_chat_content wcc 
    LEFT JOIN waple_chat as wc ON wc.wc_no=wcc.wc_no 
    WHERE wc.display='1' AND wcc.display='1' AND ((wc.`type`='1') OR (wc.`type`='2' AND wcc.s_no='{$session_s_no}') OR (wcc.wc_no IN(SELECT wcu.wc_no FROM waple_chat_user as wcu WHERE wcu.user='{$session_s_no}') AND wcc.s_no != '{$session_s_no}'))
    AND (SELECT COUNT(DISTINCT wcr.wcc_no) FROM waple_chat_read wcr WHERE wcr.wcc_no=wcc.wcc_no AND wcr.read_s_no='{$session_s_no}') = 0
	ORDER BY regdate DESC, wcc.wcc_no DESC LIMIT 10
";
$waple_chat_query 	= mysqli_query($my_db, $waple_chat_sql);
$waple_chat_list  	= [];
while($waple_chat_result = mysqli_fetch_assoc($waple_chat_query))
{
	$waple_chat_list[] = $waple_chat_result;
}
$smarty->assign("waple_chat_new_cnt", $waple_chat_new_cnt);
$smarty->assign("waple_chat_list", $waple_chat_list);
/* 와플 알림톡 END */


/* 000님의 업무상태는 Start */
$working_state_max_sql   = "SELECT max(regdate) as reg FROM working_state GROUP BY s_no";
$working_state_max_query = mysqli_query($my_db, $working_state_max_sql);
$working_state_max_list  = [];
while($working_state_max_result = mysqli_fetch_assoc($working_state_max_query))
{
	$working_state_max_list[] = "'{$working_state_max_result['reg']}'";
}
$working_state_max_list = ($working_state_max_list != "") ? implode(',', $working_state_max_list) : "";

$working_state_sql = "
	SELECT
		ws.s_no,
		(SELECT s_name FROM staff s WHERE s.s_no=ws.s_no) AS s_name,
		ws.state,
	    ws.regdate
	FROM working_state ws
	WHERE ws.regdate IN ({$working_state_max_list}) ORDER BY s_name ASC, regdate ASC
";
$working_state_query	= mysqli_query($my_db,$working_state_sql);
$working_state_list 	= [];
while($working_state_result = mysqli_fetch_array($working_state_query))
{
	$working_state_list[$working_state_result['s_no']] = array(
		's_name'	=> $working_state_result['s_name'],
		'state'		=> $working_state_result['state'],
		'regdate'	=> $working_state_result['regdate'],
	);
}
$my_working_state 		= $working_state_list[$session_s_no]['state'];
$my_working_regdate 	= $working_state_list[$session_s_no]['regdate'];
$working_state_option	= array("1" => "일반업무중", "2" => "재택근무중", "3" => "외근중", "4" => "대체휴일중", "5" => "휴가중");
$interval_days 			= (strtotime($cur_date) - strtotime(date("Y-m-d", strtotime($my_working_regdate)))) / 86400; // 시:분:초 제거 후 날짜 차이 비교

if($my_working_state > 1 && $interval_days >= 1){
	echo "<script>alert('{$session_name}님의 업무상태가 +{$interval_days}일째 {$working_state_option[$my_working_state]} 입니다.\\n현재 업무상태가 다를경우 업무상태를 반드시 수정해주세요.');</script>";
}
$smarty->assign("working_state_option" , $working_state_option);
$smarty->assign("my_working_state" , $my_working_state);
$smarty->assign("working_state_list", $working_state_list);
/* 000님의 업무상태는 End */


/* 와이즈플래닛 최근 생일자 및 입사기념일 Start */
$lunar_s_data 		= SolaToLunar($cur_year.str_replace('-','' ,$cur_s_month));
$lunar_e_data 		= SolaToLunar($cur_year.str_replace('-','', $cur_e_month));
$cur_lunar_s_month	= "{$lunar_s_data['month']}-{$lunar_s_data['day']}";
$cur_lunar_e_month	= "{$lunar_e_data['month']}-{$lunar_e_data['day']}";

$staff_birthday_sql = "
	SELECT
		s.s_name,
		(DATE_FORMAT(s.birthday, '%m/%d')) AS birthday
	FROM staff s
	WHERE 
		(
	    	(DATE_FORMAT(s.birthday, '%m-%d') BETWEEN '{$cur_s_month}' AND '{$cur_e_month}' AND sola_lunar='1') OR 
	  		(DATE_FORMAT(s.birthday, '%m-%d') BETWEEN '{$cur_lunar_s_month}' AND '{$cur_lunar_e_month}' AND sola_lunar='2')
		)
	  	AND s.staff_state='1'
	ORDER BY (DATE_FORMAT(s.birthday, '%m/%d')) ASC
";
$staff_birthday_query		= mysqli_query($my_db,$staff_birthday_sql);
$cur_month_birthday_list	= [];
while($staff_birthday = mysqli_fetch_array($staff_birthday_query)) {
	$cur_month_birthday_list[] = array(
		's_name'	=> $staff_birthday['s_name'],
		'birthday'	=> $staff_birthday['birthday']
	);
}

$staff_employment_sql = "
	SELECT
		s.s_name,
		(DATE_FORMAT(s.employment_date, '%Y')) AS employment_year,
		(DATE_FORMAT(s.employment_date, '%m/%d')) AS employment_date
	FROM staff s
	WHERE (DATE_FORMAT(s.employment_date, '%m-%d')) BETWEEN '{$cur_s_month}' AND '{$cur_e_month}' AND s.staff_state='1'
	ORDER BY (DATE_FORMAT(s.employment_date, '%m/%d')) ASC
";
$staff_employment_query 	= mysqli_query($my_db, $staff_employment_sql);
$cur_month_employment_list 	= []; // 입사일
while($staff_employment = mysqli_fetch_array($staff_employment_query))
{
	$employ_th = $cur_year - $staff_employment['employment_year'];

	$cur_month_employment_list[] = array(
		's_name'		  => $staff_employment['s_name'],
		'employment_date' => $staff_employment['employment_date'],
		'employ_th'		  => $employ_th
	);
}
$smarty->assign("cur_month", $cur_month);
$smarty->assign("cur_month_birthday_list", $cur_month_birthday_list);
$smarty->assign("cur_month_employment_list", $cur_month_employment_list);
/* 와이즈플래닛 최근 생일자 및 입사기념일 END */


# 본인 입사기념일 및 생일 START
$self_staff_sql 	= "SELECT DATE_FORMAT(birthday, '%m/%d') as birthday, employment_date as join_date, DATE_FORMAT(employment_date, '%m/%d') as employment_date FROM staff WHERE s_no='{$session_s_no}' AND staff_state='1'";
$self_staff_query 	= mysqli_query($my_db, $self_staff_sql);
$self_staff_result 	= mysqli_fetch_assoc($self_staff_query);

$my_birthday 		= $self_staff_result['birthday'];
$my_employment_date = $self_staff_result['employment_date'];
$my_join_date		= date("Y-m-d", strtotime("{$self_staff_result['join_date']} +1days"));
$is_birthday_today  = ($my_birthday == $cur_md) ? true : false;
$is_employment_today= ($my_employment_date == $cur_md) ? true : false;
$is_join_today		= ($my_join_date == $cur_date) ? true : false;

$smarty->assign("is_birthday_today", $is_birthday_today);
$smarty->assign("is_employment_today", $is_employment_today);
$smarty->assign("is_join_today", $is_join_today);
/* 와이즈플래닛 최근 생일자 및 입사기념일 End */


/* 000님의 진행중인 OKR Start */
$okr_obj_sql = "
	SELECT
		oo.oo_no,
		oo.state,
		oo.obj_name,
		oo.sdate,
		oo.edate,
		oo.s_no,
		(SELECT s_name FROM staff s where s.s_no=oo.s_no) AS s_name,
		round((SELECT AVG(ok.complete_rate) FROM okr_kr ok WHERE ok.oo_no=oo.oo_no),1) AS avg_complete_rate
	FROM okr_obj AS oo
	WHERE display='1' AND `type`='base' AND oo.s_no = '{$session_s_no}' AND oo.state = '2'
	ORDER BY oo.edate IS NULL ASC, oo.edate ASC
";
$okr_obj_query 		= mysqli_query($my_db, $okr_obj_sql);
$okr_obj_list		= [];
$okr_state_option	= getOkrStateOption();
while($okr_obj 	= mysqli_fetch_array($okr_obj_query))
{
	$obj_d_day 				= intval((strtotime(date("Y-m-d", time())) - strtotime($okr_obj['edate'])) / 86400);
	$okr_obj['obj_d_day'] 	= $obj_d_day;
	$okr_obj['state_name'] 	= $okr_state_option[$okr_obj['state']];

	$okr_kr_sql = "
		SELECT
			ok.ok_no,
			ok.oo_no,
			ok.kr_name,
			ok.complete_rate,
			ok.kr_result
		FROM okr_kr AS ok
		WHERE ok.oo_no='{$okr_obj['oo_no']}'
	";
	$okr_kr_query 	= mysqli_query($my_db, $okr_kr_sql);
	$okr_kr_list 	= [];
	while($okr_kr = mysqli_fetch_assoc($okr_kr_query))
	{
		$okr_kr['kr_result'] = htmlspecialchars($okr_kr['kr_result']);
		$okr_kr_list[$okr_kr['ok_no']] = $okr_kr;
	}
	$okr_obj['okr_kr_list'] = $okr_kr_list;

	$okr_obj_list[$okr_obj['oo_no']] = $okr_obj;
}
$smarty->assign("okr_obj_list", $okr_obj_list);
/* 000님의 진행중인 OKR END */


/* 000님의 할일(MY TO_O_LIST) START */
$to_do_sql = "
	SELECT
		td_no,
		`state`,
		td_date,
		important,
		contents,
		s_no
	FROM to_do_list td
	WHERE td.s_no = '{$session_s_no}' AND (td.td_date = '{$cur_date}' OR (td.td_date BETWEEN '' AND '{$cur_date}' AND ((td.state <> '2' AND td.state <> '4') OR td.state IS NULL)))
	ORDER BY td_date ASC, important ASC, td_no ASC
";
$to_do_query		= mysqli_query($my_db, $to_do_sql);
$to_do_list			= [];
$to_do_state_option = array("" => "", "1" => "진행중", "2" => "완료", "3" => "보류", "4" => "중단");
while($to_do_result = mysqli_fetch_array($to_do_query))
{
	$to_do_result['contents'] = htmlspecialchars($to_do_result['contents']);
	$to_do_list[] = $to_do_result;
}
$smarty->assign("to_do_state_option", $to_do_state_option);
$smarty->assign("to_do_list", $to_do_list);
/* 000님의 할일(MY TO_O_LIST) End */


/* 000님의 업무처리현황 Start */
$team_model         = Team::Factory();
$team_all_list      = $team_model->getTeamAllList();
$staff_all_list     = $team_all_list['staff_list'];
$staff_all_list['00221']['167'] = "임태형";
$team_name_list     = $team_model->getActiveTeamList();

# 업무처리 검색
$chk_work_team			= $session_s_no == "1" && !isset($_GET['sch_team']) ? "" : (($session_team != "00236") ? $session_team : "00221");
$sch_team				= isset($_GET['sch_team']) ? $_GET['sch_team'] : $chk_work_team;
$sch_team_s_month		= date("Y-m", strtotime("-4 months"));
$sch_team_e_month		= $cur_month;
$sch_team_s_date		= "{$sch_team_s_month}-01 00:00:00";
$sch_team_e_date		= "{$cur_month}-{$cur_e_day} 23:59:59";

$add_work_team_where 	= "work_state='6' AND task_run_regdate BETWEEN '{$sch_team_s_date}' AND '{$sch_team_e_date}'";
$work_run_name_list		= $team_name_list;
$team_key_column		= "task_run_team";
$team_key_name			= "업무처리부서";
if (!empty($sch_team))
{
	$work_run_name_list  = $staff_all_list[$sch_team];
	$team_key_column	 = "task_run_s_no";
	$team_key_name		 = "업무처리자";
	$sch_team_code_where = getTeamWhere($my_db, $sch_team);
	if($sch_team == "00221"){
		$sch_team_code_where .= ",00236";
	}
	$add_work_team_where .= " AND `w`.task_run_team IN ({$sch_team_code_where})";
}

$work_team_date_list 		= [];
$work_team_data_list  		= [];
$work_team_date_total_list  = [];
$all_date_sql = "
	SELECT
		DATE_FORMAT(`allday`.Date, '%Y%m') as chart_key,
		DATE_FORMAT(`allday`.Date, '%Y/%m') as chart_title
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_team_s_month}' AND '{$sch_team_e_month}'
	GROUP BY chart_key
	ORDER BY chart_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($date = mysqli_fetch_array($all_date_query))
{
	$work_team_date_list[$date['chart_key']]        = $date['chart_title'];
	$work_team_date_total_list[$date['chart_key']]  = 0;

	foreach($work_run_name_list as $run_key => $run_name){
		$work_team_data_list[$date['chart_key']][$run_key] = 0;
	}
}

$work_team_sql = "
	SELECT
		DATE_FORMAT(`w`.task_run_regdate, '%Y%m') as key_date,
	    {$team_key_column} as key_column,
	    COUNT(w_no) as work_cnt
	FROM `work` AS `w`
	WHERE {$add_work_team_where}
	GROUP BY key_date, key_column
	ORDER BY key_date
";
$work_team_query = mysqli_query($my_db, $work_team_sql);
while($work_team_result = mysqli_fetch_assoc($work_team_query))
{
	$work_team_data_list[$work_team_result['key_date']][$work_team_result['key_column']] += $work_team_result['work_cnt'];
	$work_team_date_total_list[$work_team_result['key_date']] += $work_team_result['work_cnt'];
}

$work_team_chart_list = [];
foreach($work_run_name_list as $run_key => $run_name)
{
	$work_team_chart_init_data = array(
		"title" => $run_name,
		"type"  => "bar",
		"data"  => array(),
	);

	foreach($work_team_date_list as $key_date => $date_label){
		$work_team_chart_init_data["data"][] = $work_team_data_list[$key_date][$run_key];
	}

	$work_team_chart_list[] = $work_team_chart_init_data;
}

$smarty->assign("sch_team", $sch_team);
$smarty->assign("team_key_name", $team_key_name);
$smarty->assign("team_name_list", $team_name_list);
$smarty->assign("work_team_date_list", $work_team_date_list);
$smarty->assign("work_team_data_list", $work_team_data_list);
$smarty->assign("work_team_data_name_list", $work_run_name_list);
$smarty->assign("work_team_date_total_list", $work_team_date_total_list);
$smarty->assign("work_team_chart_list", json_encode($work_team_chart_list));
$smarty->assign("work_team_chart_name_list", json_encode($work_team_date_list));
$smarty->assign("work_team_chart_run_name_list", json_encode($work_run_name_list));
/* 000님의 업무처리현황 END */


/* # 월간 매출통계 # [Start]*/
$deposit_s_month	= "{$cur_year}-{$cur_s_month}";
$deposit_e_month	= "{$cur_year}-{$cur_e_month}";
$add_deposit_where  = " dp.display='1' AND (dp.dp_date BETWEEN '{$deposit_s_month}' AND '{$deposit_e_month}') AND dp.my_c_no <> '3'";
$add_deposit_where .= " AND ((dp.dp_method = '2' AND (dp.dp_state = '2' OR dp.dp_state = '3' OR dp.dp_state = '4')) "; // 현금일 경우 입금완료 or 부분입금 or 환불완료
$add_deposit_where .= " OR (dp.dp_method = '1' AND (dp.dp_state = '1' OR dp.dp_state = '2' OR dp.dp_state = '4')) "; // 카드일 경우 입금대기 or 입금완료 or 부분입금 or 환불완료
$add_deposit_where .= " OR (dp.dp_method = '3' AND dp.dp_state = '2')) "; // 월정산 경우 입금완료

# 대표 & 재무관리자 제외처리
if(!permissionNameCheck($session_permission, '대표') && !permissionNameCheck($session_permission, '재무관리자')){
	$add_deposit_where .= " AND dp.s_no NOT IN('1')";
}

# 입금 리스트 쿼리
$deposit_sql = "
	SELECT
		DATE_FORMAT(dp.dp_date, '%Y-%m') AS result_ym,
		(SELECT t.team_name FROM team t WHERE t.team_code=dp.team) AS t_name,
		dp.s_no,
		(SELECT s.s_name FROM staff s WHERE s.s_no=dp.s_no) AS s_name,
		SUM(dp_money) AS sum_dp_money
	FROM deposit dp
	WHERE {$add_deposit_where} 
	GROUP BY result_ym, dp.s_no, dp.team
	HAVING sum_dp_money > 0
	ORDER BY sum_dp_money DESC
";
$deposit_query 				= mysqli_query($my_db, $deposit_sql);
$deposit_list  				= [];
$deposit_chart_list  		= [];
$deposit_chart_name_list  	= [];
while($deposit_array = mysqli_fetch_array($deposit_query)){
	$deposit_list[] = array(
		"t_name"		=> $deposit_array['t_name'],
		"s_no"			=> $deposit_array['s_no'],
		"s_name"		=> $deposit_array['s_name'],
		"sum_dp_money"	=> $deposit_array['sum_dp_money']
	);

	$deposit_chart_list[] 		= (int)($deposit_array['sum_dp_money']/1000);
	$deposit_chart_name_list[] 	= $deposit_array['s_name'];
}
$smarty->assign("deposit_list", $deposit_list);
$smarty->assign("deposit_chart_list", json_encode($deposit_chart_list));
$smarty->assign("deposit_chart_name_list", json_encode($deposit_chart_name_list));
/* # 월간 매출통계 # [End]*/


/* # 주간 커머스 제품별 & 브랜드별 판매수 통계 # [Start]*/
$week_prev_date 		= date("Y-m-d", strtotime("-6 day"));
$week_comm_s_date 		= "{$week_prev_date} 00:00:00";
$week_comm_e_date 		= "{$cur_date} 23:59:59";
$week_ord_where  		= " w_c.delivery_state = '4' AND w_c.dp_c_no NOT IN('2007','2008') AND (w_c.order_date BETWEEN '{$week_comm_s_date}' AND '{$week_comm_e_date}')";
$day_name_option		= getDayShortOption();
$week_date_name_list		= [];
$week_ord_chart_date_list 	= [];
for($week_idx=6; $week_idx>=0; $week_idx--)
{
	$chk_week_date 	= date("Y-m-d", strtotime("-{$week_idx} day"));
	$chk_week_key  	= date("Ymd", strtotime($chk_week_date));

	$week_date_name_list[$chk_week_key] = array("date" => date("m/d", strtotime($chk_week_date)), "week" => $day_name_option[date("w", strtotime($chk_week_date))]);
	$week_ord_chart_date_list[] 		= date("m/d", strtotime($chk_week_date)).$day_name_option[date("w", strtotime($chk_week_date))];
}

//대표&재무관리자 제외처리
if(!permissionNameCheck($session_permission, '대표') && !permissionNameCheck($session_permission, '재무관리자')){
	$week_ord_where .= " AND w_c.s_no NOT IN('1')";
}

$week_ord_sql = "
	SELECT
		w_c.team,
		(SELECT t.team_name FROM team AS t WHERE t.team_code=w_c.team) as t_name,
		w_c.s_no,
		(SELECT s.s_name FROM staff s WHERE s.s_no=w_c.s_no) AS s_name,
		w_c.prd_no,
		(SELECT prd.title FROM product_cms prd WHERE prd.prd_no=w_c.prd_no) AS prd_name,
	   	DATE_FORMAT(w_c.order_date, '%Y%m%d') AS key_date,
		quantity,
	    (SELECT prd.k_name_code FROM product_cms prd WHERE prd.prd_no=w_c.prd_no) AS k_name_code,
		(SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product_cms prd WHERE prd.prd_no=w_c.prd_no)) AS k_name,
	   	unit_price
	FROM `work_cms` w_c
	WHERE {$week_ord_where}
	ORDER BY team ASC, s_no ASC, prd_no ASC, key_date ASC
";
$week_ord_query 				= mysqli_query($my_db, $week_ord_sql);
$week_ord_team_name_list 		= [];
$week_ord_staff_name_list 		= [];
$week_ord_prd_name_list 		= [];
$week_ord_brand_name_list 		= [];
$week_ord_product_data_list		= [];
$week_ord_brand_data_list 		= [];
$week_ord_product_chart_list	= [];
$week_ord_brand_chart_list  	= [];
$week_ord_brand_sort_list  		= [];
while($work_cm_array = mysqli_fetch_array($week_ord_query))
{
	if(!isset($week_ord_team_name_list[$work_cm_array['team']])){
		$week_ord_team_name_list[$work_cm_array['team']] = $work_cm_array['t_name'];
	}

	if(!isset($week_ord_staff_name_list[$work_cm_array['team']][$work_cm_array['s_no']])){
		$week_ord_staff_name_list[$work_cm_array['team']][$work_cm_array['s_no']] = $work_cm_array['s_name'];
	}

	if(!isset($week_ord_prd_name_list[$work_cm_array['team']][$work_cm_array['s_no']][$work_cm_array['prd_no']])){
		$week_ord_prd_name_list[$work_cm_array['team']][$work_cm_array['s_no']][$work_cm_array['prd_no']] = $work_cm_array['prd_name'];
	}

	if(!isset($week_ord_brand_name_list[$work_cm_array['k_name_code']])){
		$week_ord_brand_name_list[$work_cm_array['k_name_code']] = $work_cm_array['k_name'];
	}

	$unit_price = floor($work_cm_array['unit_price']/1000);

	$week_ord_product_data_list[$work_cm_array['team']][$work_cm_array['s_no']][$work_cm_array['prd_no']][$work_cm_array['key_date']] += $work_cm_array['quantity'];
	$week_ord_brand_data_list[$work_cm_array['k_name_code']][$work_cm_array['key_date']] += $unit_price;

	$week_ord_product_chart_list[$work_cm_array['key_date']] 	+= $work_cm_array['quantity'];
	$week_ord_brand_chart_list[$work_cm_array['key_date']] 		+= $unit_price;
	$week_ord_brand_sort_list[$work_cm_array['k_name_code']] 	+= $unit_price;
}

arsort($week_ord_brand_sort_list);

$smarty->assign("week_date_name_list", $week_date_name_list);
$smarty->assign("week_ord_team_name_list", $week_ord_team_name_list);
$smarty->assign("week_ord_staff_name_list", $week_ord_staff_name_list);
$smarty->assign("week_ord_prd_name_list", $week_ord_prd_name_list);
$smarty->assign("week_ord_brand_name_list", $week_ord_brand_name_list);
$smarty->assign("week_ord_product_data_list", $week_ord_product_data_list);
$smarty->assign("week_ord_brand_sort_list", $week_ord_brand_sort_list);
$smarty->assign("week_ord_brand_data_list", $week_ord_brand_data_list);
$smarty->assign("week_ord_chart_date_list", json_encode($week_ord_chart_date_list));
$smarty->assign("week_ord_product_chart_list", json_encode($week_ord_product_chart_list));
$smarty->assign("week_ord_brand_chart_list", json_encode($week_ord_brand_chart_list));
/* # 주간 커머스 제품별 & 브랜드별 통계 # [End]*/

$smarty->display('main.html');
?>
