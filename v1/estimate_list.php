<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/incentive.php');
require('inc/helper/work.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');

# Process 처리
$proc = (isset($_POST['process']))?$_POST['process']:"";

if ($proc == "f_p_state")
{
	$est_wp_no 	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	$upd_sql 	= "UPDATE estimate_work_package ewp SET ewp.state = '{$value}' WHERE ewp.est_wp_no = '{$est_wp_no}'";

	if (!mysqli_query($my_db, $upd_sql))
		echo "진행상태 저장에 실패 하였습니다.";
	else{
		echo "진행상태가 저장 되었습니다.";
	}
	exit;
}
elseif ($proc == "f_p_subject")
{
	$est_wp_no 	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	
	$upd_sql 	= "UPDATE estimate_work_package ewp SET ewp.subject = '{$value}' WHERE ewp.est_wp_no = '{$est_wp_no}'";

	if (!mysqli_query($my_db, $upd_sql))
		echo "견적서명 저장에 실패 하였습니다.";
	else{
		echo "견적서명이 저장 되었습니다.";
	}
	exit;
}
elseif ($proc == "f_p_memo")
{
	$est_wp_no 	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	
	$upd_sql 	= "UPDATE estimate_work_package ewp SET ewp.memo = '{$value}' WHERE ewp.est_wp_no = '{$est_wp_no}'";

	if (!mysqli_query($my_db, $upd_sql))
		echo "마케터 메모 저장에 실패 하였습니다.";
	else
		echo "마케터 메모가 저장 되었습니다.";
	exit;
}
elseif ($proc == "f_w_no")
{
	$est_w_no 	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$est_wp_no 	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	if(empty($value)){
		$upd_sql = "UPDATE estimate_work ew SET ew.w_no = NULL WHERE ew.est_w_no = '{$est_w_no}'";
	}else{
		$upd_sql = "UPDATE estimate_work ew SET ew.w_no = '{$value}', ew.prd_no=(SELECT prd_no FROM work WHERE w_no='{$value}') WHERE ew.est_w_no = '{$est_w_no}'";
	}

	if (!mysqli_query($my_db, $upd_sql))
		echo "w_no 저장에 실패 하였습니다.";
	else{
		echo "w_no가 저장 되었습니다.";
		echo "<script>location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>";
	}
	exit;
}
elseif ($proc == "f_work_state")
{
	$est_w_no 	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	$upd_sql 	= "UPDATE work w SET w.work_state = '{$value}' WHERE w.w_no=(SELECT ew.w_no FROM estimate_work ew WHERE ew.est_w_no = '{$est_w_no}')";

	if (!mysqli_query($my_db, $upd_sql))
		echo "진행상태 저장에 실패 하였습니다.";
	else{
		echo "진행상태가 저장 되었습니다.";
	}
	exit;
}
elseif ($proc == "f_prd")
{
	$est_w_no 	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$est_wp_no	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";

	$upd_sql 	= "UPDATE estimate_work ew SET ew.prd_no = '{$value}' WHERE ew.est_w_no = '{$est_w_no}'";

	if (!mysqli_query($my_db, $sql))
		echo "상품 변경에 실패 하였습니다.";
	else{
		echo "상품이 변경 되었습니다.";
		echo "<script>location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>";
	}
	exit;
}
elseif ($proc == "f_t_keyword_or_cnt")
{
	$est_w_no 	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$upd_sql = "UPDATE estimate_work ew SET ew.t_keyword_or_cnt = NULL WHERE ew.est_w_no = '{$est_w_no}'";
	}else{
		$upd_sql = "UPDATE estimate_work ew SET ew.t_keyword_or_cnt = '{$value}' WHERE ew.est_w_no = '{$est_w_no}'";
	}

	if (!mysqli_query($my_db, $upd_sql))
		echo "타겟키워드 및 수량 저장에 실패 하였습니다.";
	else{
		echo "타겟키워드 및 수량이 저장 되었습니다.";
	}
	exit;
}
elseif ($proc == "f_price")
{
	$est_w_no 	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$value 		= str_replace(",","",trim($value));

	$upd_sql 	= "UPDATE estimate_work ew SET ew.price = '{$value}' WHERE ew.est_w_no = '{$est_w_no}'";

	if (!mysqli_query($my_db, $upd_sql))
		echo "총 판매가 저장에 실패 하였습니다.";
	else{
		echo "총 판매가가 저장 되었습니다.";
	}
	exit;
}
elseif ($proc == "f_out_price")
{
	$est_w_no	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$value 		= str_replace(",","",trim($value));

	$upd_sql 	= "UPDATE estimate_work ew SET ew.out_price = '{$value}' WHERE ew.est_w_no = '{$est_w_no}'";

	if (!mysqli_query($my_db, $upd_sql))
		echo "총 외주비 단가 저장에 실패 하였습니다.";
	else{
		echo "총 외주비 단가가 저장 되었습니다.";
	}
	exit;
}
elseif ($proc == "f_memo")
{
	$est_w_no 	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$upd_sql = "UPDATE estimate_work ew SET ew.memo = NULL WHERE ew.est_w_no = '{$est_w_no}'";
	}else{
		$upd_sql = "UPDATE estimate_work ew SET ew.memo = '{$value}' WHERE ew.est_w_no = '{$est_w_no}'";
	}

	if (!mysqli_query($my_db, $upd_sql))
		echo "마케터 메모 저장에 실패 하였습니다.";
	else
		echo "마케터 메모가 저장 되었습니다.";
	exit;
}
elseif ($proc == "check_f_t_keyword_new")
{
	$f_t_keyword_new = str_replace(" ", "", $_POST['f_t_keyword_new']);
	$f_prd_no_new 	 = (isset($_POST['f_prd_no_new'])) ? $_POST['f_prd_no_new'] : "";

	$keyword_sql = "
		SELECT 
		   COUNT(est_wp_no) AS cnt
		FROM estimate_work_package ewp
		WHERE prd_no = '{$f_prd_no_new}'
			AND extension_date >= date(now())
			AND REPLACE(t_keyword, ' ', '') = '{$f_t_keyword_new}'
			AND work_state < 7
			AND NOT(extension ='1' OR extension = '3')
	";

	$keyword_query  = mysqli_query($my_db, $keyword_sql);
	$keyword_result = mysqli_fetch_array($keyword_query);
	$keyword_cnt 	= $keyword_result['cnt'];
	echo $keyword_cnt;
	exit;
}
elseif($proc == "del_estimate")
{
	$est_w_no	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$est_wp_no	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$del_sql 	= "DELETE FROM estimate_work_package WHERE est_wp_no = '{$est_wp_no}'";

	if(mysqli_query($my_db, $del_sql)) {
		exit("<script>alert('est_w_no = {$est_w_no} 견적서를 삭제하였습니다.');location.href='estimate_list.php?{$search_url}';</script>");
	}else{
		exit("<script>alert('est_w_no = {$est_w_no} 견적서 삭제에 실패 하였습니다.');location.href='estimate_list.php?{$search_url}';</script>");
	}
}
elseif($proc == "del_estimate_work")
{
	$est_w_no	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$est_wp_no	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$estimate_sql 	= "SELECT w_no FROM estimate_work WHERE est_w_no = '{$est_w_no}'";
	$estimate_query = mysqli_query($my_db, $estimate_sql);
	$estimate_work 	= mysqli_fetch_array($estimate_query);

	if($estimate_work['w_no'])
	{
		$work_sql = "DELETE FROM `work` WHERE w_no = '{$estimate_work['w_no']}'";
		if(!mysqli_query($my_db, $work_sql)){
			exit("<script>alert('w_no = {$estimate_work['w_no']} 업무 삭제에 실패 하였습니다.');</script>");
		}
	}

	$upd_sql = "DELETE FROM estimate_work WHERE est_w_no = '{$est_w_no}'";

	if(mysqli_query($my_db, $upd_sql)) {
		exit("<script>location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
	}else{
		exit("<script>alert('est_w_no = $est_w_no 견적서 업무 삭제에 실패 하였습니다.');location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
	}
}
elseif($proc == "duplicate_package")
{
	$est_w_no	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$est_wp_no	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$duplicate_package_sql = "
		INSERT IGNORE INTO estimate_work_package(regdate, state, c_no, c_name, subject, s_no,memo)
		(SELECT NOW(), '1', c_no, c_name, CONCAT(subject,'_사본'), s_no, memo FROM estimate_work_package WHERE est_wp_no = '{$est_wp_no}')
	";

	if(!mysqli_query($my_db, $duplicate_package_sql)) {
		exit("<script>alert('견적서 복제에 실패 하였습니다. 개발 담당자에게 문의해 주세요.');location.href='estimate_list.php?{$search_url}';</script>");
	} else {
		$estimate_work_package_query	= mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
		$estimate_work_package_result	= mysqli_fetch_array($estimate_work_package_query);
		$last_est_wp_no 				= $estimate_work_package_result[0];

		$estimate_work_sql 			= "SELECT est_w_no FROM estimate_work ew WHERE est_wp_no = '{$est_wp_no}'";
		$estimate_work_query		= mysqli_query($my_db,$estimate_work_sql);
		while($estimate_work_array 	= mysqli_fetch_array($estimate_work_query)) 
		{
			$duplicate_estimate_work_sql = "
				INSERT IGNORE INTO estimate_work(est_wp_no, regdate, prd_no, t_keyword_or_cnt, price, out_price, memo)
				(SELECT '{$last_est_wp_no}', NOW(), prd_no, t_keyword_or_cnt, price, out_price, memo FROM estimate_work WHERE est_w_no = '{$estimate_work_array['est_w_no']}')
			";

			if(!mysqli_query($my_db, $duplicate_estimate_work_sql)) {
				exit("<script>alert('견적서 복제에 실패 하였습니다. 개발 담당자에게 문의해 주세요.');location.href='estimate_list.php?{$search_url}';</script>");
			}
		}

		exit("<script>location.href='estimate_list.php?{$search_url}&open_est_wp_no={$last_est_wp_no}';</script>");
	}
}
elseif($proc == "duplicate") 
{
	$est_w_no	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$est_wp_no	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$duplicate_sql = "
		INSERT IGNORE INTO estimate_work (est_wp_no, regdate, prd_no, t_keyword_or_cnt, price, out_price, memo)
		(SELECT est_wp_no, NOW(), prd_no, CONCAT(t_keyword_or_cnt,'_사본'), price, out_price, memo FROM estimate_work WHERE est_w_no = '{$est_w_no}')
	";

	if(mysqli_query($my_db, $duplicate_sql)) {
		exit("<script>location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
	} else {
		exit("<script>alert('견적서 업무 복제에 실패 하였습니다');location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
	}
} 
elseif($proc == "add_estimate_work_package")
{
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$ins_sql	= "
		INSERT INTO estimate_work_package SET
			`state` = '{$_POST['f_state_new']}',
			c_no 	= '{$_POST['f_c_no_new']}',
			c_name 	= '{$_POST['f_c_name_new']}',
			subject = '{$_POST['f_subject_new']}',
			s_no 	= '{$_POST['s_no_new']}',
			memo 	= '{$_POST['f_memo_new']}',
			regdate = now()
	";

	if(mysqli_query($my_db, $ins_sql)) {
		exit("<script>alert('견적서를 제작하었습니다');location.href='estimate_list.php?{$search_url}';</script>");
	} else {
		exit("<script>alert('견적서 제작에 실패 하였습니다');location.href='estimate_list.php?{$search_url}';</script>");
	}

} 
elseif($proc == "add_estimate_work")
{
	$est_wp_no	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$add_set 	= "";
	if(!empty($_POST['f_price_new'])){
		$add_set .= "price = '".str_replace(",","",trim($_POST['f_price_new']))."',";
	}
	if(!empty($_POST['f_out_price_new'])){
		$add_set .= "out_price = '".str_replace(",","",trim($_POST['f_out_price_new']))."',";
	}

	$ins_sql = "
		INSERT INTO estimate_work SET
			est_wp_no 		 = '{$_POST['est_wp_no']}',
			prd_no 			 = '{$_POST['f_prd']}',
			t_keyword_or_cnt = '{$_POST['f_t_keyword_or_cnt_new']}',
			{$add_set}
			regdate 		 = now()
	";

	if(mysqli_query($my_db, $ins_sql)) {
		exit("<script>location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
	} else {
		exit("<script>alert('견적서 업무 추가에 실패 하였습니다');location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
	}

} 
elseif($proc == "add_work")
{
	$est_w_no	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$est_wp_no	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$f_prd		= (isset($_POST['f_prd'])) ? $_POST['f_prd'] : "";

	$add_set 	= "";
	if($f_prd == '7' || $f_prd == '30'){ // 연관, 자동완성
		$add_set .= "t_keyword = '".trim($_POST['f_t_keyword_new'])."',";
		$add_set .= "r_keyword = '".trim($_POST['f_r_keyword_new'])."',";
	}elseif($f_prd == '9' || $f_prd == '4' || $f_prd == '5' || $f_prd == '6' || $f_prd == '8' || $f_prd == '10'){
		$add_set .= "t_keyword = '".trim($_POST['f_t_keyword_or_cnt'])."',";
	}

	$work_ins_sql = "
		INSERT INTO `work` SET
			work_state 	= '3',
			c_no 		= (SELECT ewp.c_no FROM estimate_work_package ewp WHERE ewp.est_wp_no='{$est_wp_no}'),
			c_name 		= (SELECT ewp.c_name FROM estimate_work_package ewp WHERE ewp.est_wp_no='{$est_wp_no}'),
			s_no 		= (SELECT ewp.s_no FROM estimate_work_package ewp WHERE ewp.est_wp_no='{$est_wp_no}'),
			prd_no 		= '{$f_prd}',
			task_req 	= '{$_POST['f_task_req_new']}',
			{$add_set}
			regdate 	= now()
	";

	if(mysqli_query($my_db, $work_ins_sql))
	{
		$last_w_no 	= mysqli_insert_id($my_db);
		$files 		= $_FILES["f_task_req_file_new"];
		if ($files)
		{
			$save_name = add_store_file($files, "out_task_req");

			$add_set = "";
			if (!empty($save_name)) {
				$add_set .= ", task_req_file_read = '{$save_name}'";
			}
			if(!empty($files["name"][0])) {
				$add_set.=", task_req_file_origin='".addslashes($files["name"][0])."'";
			}

			$upd_sql = "UPDATE `work` SET task_req_regdate = now(), task_req_s_no = '{$session_s_no}' {$add_set} WHERE w_no = '{$last_w_no}'";
			mysqli_query($my_db, $upd_sql);
		}

		$estimate_work_sql = "UPDATE estimate_work ew SET ew.w_no = '{$last_w_no}' WHERE ew.est_w_no = '{$est_w_no}'";

		if (mysqli_query($my_db, $estimate_work_sql)){
			exit("<script>location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
		}else{
			exit("<script>alert('업무요청에 실패 하였습니다');location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
		}
	} else {
		exit("<script>alert('업무요청에 실패 하였습니다');location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
	}
} 
elseif($proc == "extension_work")
{
	$est_w_no	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$est_wp_no	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$extension_w_no = (isset($_POST['extension_w_no'])) ? $_POST['extension_w_no'] : "";
	$f_prd		= (isset($_POST['f_prd'])) ? $_POST['f_prd'] : "";

	$upd_sql 	= "UPDATE `work` w SET w.extension = '1' WHERE w.w_no = '{$extension_w_no}'";

	if (!mysqli_query($my_db, $upd_sql))
		echo "연장여부 저장에 실패 하였습니다.";

	$work_sql	= "SELECT w.extension_date FROM work w WHERE w_no = '{$extension_w_no}'";
	$work_query	= mysqli_query($my_db, $work_sql);
	$work 		= mysqli_fetch_array($work_query);

	$extension_date = $work['extension_date'];

	$duplicate_sql = "
		INSERT IGNORE INTO `work` (regdate, work_state, c_no, c_name, s_no, prd_no, t_keyword, r_keyword, price, price_vat, out_c_name, out_c_no, extension_date, task_req, task_req_regdate, task_req_s_no)
		(SELECT NOW(), '3', c_no, c_name, s_no, prd_no, t_keyword, r_keyword, price, price_vat, out_c_name, out_c_no, DATE_ADD('{$extension_date}', INTERVAL 1 MONTH), '연장 ({$extension_w_no})', NOW(), '{$session_s_no}' FROM work WHERE w_no = '{$extension_w_no}')
	";

	if(mysqli_query($my_db, $duplicate_sql)){
		$work_query	 = mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
		$work_result = mysqli_fetch_array($work_query);
		$last_w_no   = $work_result[0];

		$estimate_work_sql = "UPDATE estimate_work ew SET ew.w_no = '{$last_w_no}' WHERE ew.est_w_no = '{$est_w_no}'";

		if (mysqli_query($my_db, $estimate_work_sql)){
			exit("<script>location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
		}else{
			exit("<script>alert('연장에 실패 하였습니다');location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
		}
	} else {
		exit("<script>alert('연장에 실패 하였습니다');location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
	}
}
elseif($proc == "add_incentive")
{
	$est_w_no	= (isset($_POST['est_w_no'])) ? $_POST['est_w_no'] : "";
	$est_wp_no	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$dp_no		= (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$total_price_value		= (isset($_POST['total_price'])) ? $_POST['total_price'] : "";
	$total_out_price_value	= (isset($_POST['total_out_price'])) ? $_POST['total_out_price'] : "";
	$dp_money_value			= (isset($_POST['dp_money'])) ? $_POST['dp_money'] : "";
	$total_work_price_value = (isset($_POST['total_work_price'])) ? $_POST['total_work_price'] : "";
	$nat_sales_price_value	= (isset($_POST['nat_sales_price'])) ? $_POST['nat_sales_price'] : "";


	$estimate_package_sql = "
		INSERT INTO incentive SET
			dp_date 		= (SELECT dp.dp_date FROM deposit dp WHERE dp.dp_no='{$dp_no}'),
			incentive_date 	= (SELECT dp.incentive_date FROM deposit dp WHERE dp.dp_no='{$dp_no}'),
			`state` 		= '2',
			est_wp_no 		= '{$est_wp_no}',
			dp_no 			= (SELECT ewp.dp_no FROM estimate_work_package ewp WHERE ewp.est_wp_no='{$est_wp_no}'),
			c_no 			= (SELECT ewp.c_no FROM estimate_work_package ewp WHERE ewp.est_wp_no='{$est_wp_no}'),
			c_name 			= (SELECT ewp.c_name FROM estimate_work_package ewp WHERE ewp.est_wp_no='{$est_wp_no}'),
			subject 		= (SELECT ewp.subject FROM estimate_work_package ewp WHERE ewp.est_wp_no='{$est_wp_no}'),
			s_no 			= (SELECT ewp.s_no FROM estimate_work_package ewp WHERE ewp.est_wp_no='{$est_wp_no}'),
			price 			= '{$total_price_value}',
			out_price 		= '{$total_out_price_value}',
			dp_money 		= '{$dp_money_value}',
			total_work_price= '{$total_work_price_value}',
			nat_sales_price = '{$nat_sales_price_value}'
	";

	if(!mysqli_query($my_db, $estimate_package_sql)) {
		exit("<script>alert('정산요청에 실패 하였습니다.(인센티브 저장 실패) 개발 담당자에게 문의해 주세요.');location.href='estimate_list.php?{$search_url}';</script>");
	} else {
		$incentive_query	= mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
		$incentive_result	= mysqli_fetch_array($incentive_query);
		$last_i_no 			= $incentive_result[0];

		$estimate_package_sql = "UPDATE estimate_work_package ewp SET ewp.i_no = '{$last_i_no}' WHERE ewp.est_wp_no = '{$est_wp_no}'";

		if (!mysqli_query($my_db, $estimate_package_sql))
			exit("<script>alert('인센티브 업무 복제에 실패 하였습니다. 개발 담당자에게 문의해 주세요.');location.href='estimate_list.php?{$search_url}';</script>");


		$estimate_work_sql 	 = "SELECT est_w_no FROM estimate_work ew WHERE est_wp_no = '{$est_wp_no}'";
		$estimate_work_query = mysqli_query($my_db, $estimate_work_sql);
		while($estimate_work_array=mysqli_fetch_array($estimate_work_query))
		{
			$duplicate_work_sql = "
				INSERT IGNORE INTO incentive_work (est_w_no, est_wp_no, regdate, w_no, prd_no, t_keyword_or_cnt, price, out_price)
				(SELECT est_w_no, est_wp_no, regdate, w_no, prd_no, t_keyword_or_cnt, price, out_price FROM estimate_work WHERE est_w_no = '{$estimate_work_array['est_w_no']}'
			";

			if(!mysqli_query($my_db, $duplicate_work_sql)) {
				exit("<script>alert('인센티브 업무 복제에 실패 하였습니다. 개발 담당자에게 문의해 주세요.');location.href='estimate_list.php?{$search_url}';</script>");
			}
		}

		exit("<script>location.href='estimate_list.php?$search_url&open_est_wp_no={$est_wp_no}';</script>");
	}
} 
elseif($proc == "save_dp_no")
{
	$est_wp_no	= (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$dp_no		= (isset($_POST['dp_no'])) ? $_POST['dp_no']:"";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$upd_sql 	= "UPDATE estimate_work_package ewp SET ewp.dp_no = '{$dp_no}' WHERE ewp.est_wp_no = '{$est_wp_no}'";

	if (mysqli_query($my_db, $upd_sql)){
		exit("<script>location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
	}else{
		exit("<script>alert('입금 찾기 설정에 실패 하였습니다');location.href='estimate_list.php?{$search_url}&open_est_wp_no={$est_wp_no}';</script>");
	}
} 
else 
{
	# Navigation & My Quick
	$nav_prd_no  = "16";
	$nav_title   = "견적서 리스트";
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_title", $nav_title);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	# Model 설정
	$kind_model 	= Kind::Factory();
	$prd_g1_list 	= $kind_model->getKindParentList("product");

	# 직원가져오기 배열
	$staff_list = [];
	$add_where_permission 	= str_replace("0", "_", $permission_name['마케터']);
	if(permissionNameCheck($session_permission,"재무관리자") || permissionNameCheck($session_permission,"대표") || permissionNameCheck($session_permission,"마스터관리자"))
	{
		$staff_sql	 = "SELECT s_no,s_name FROM staff WHERE staff_state < '3' AND permission LIKE '{$add_where_permission}'";
		$staff_query = mysqli_query($my_db, $staff_sql);
		while($staff_data = mysqli_fetch_array($staff_query))
		{
			$staff_list[] = array(
				'no'	=> $staff_data['s_no'],
				'name'	=> $staff_data['s_name']
			);
		}
	}
	elseif($session_s_no == '22')
	{
		$add_where_permission_2 = str_replace("0", "_", $permission_name['인큐베이팅']);
		$staff_sql	 = "SELECT s_no,s_name FROM staff WHERE staff_state < '3' AND permission LIKE '{$add_where_permission}' AND permission LIKE '{$add_where_permission_2}'";
		$staff_query = mysqli_query($my_db, $staff_sql);

		while($staff_data = mysqli_fetch_array($staff_query))
		{
			$staff_list[] = array(
				'no'	=> $staff_data['s_no'],
				'name'	=> $staff_data['s_name']
			);
		}
	}
	elseif(permissionNameCheck($session_permission,"마케터"))
	{
		$staff_list[] = array(
			'no'	=> $session_s_no,
			'name'	=> $session_name
		);
	}
	$smarty->assign("staff_list", $staff_list);

	#검색 쿼리
	$add_where		 = "1=1";
	$add_where_noset = "1=1";
	
	$sch_est_wp_no_get 	= (isset($_GET['sch_est_wp_no'])) ? $_GET['sch_est_wp_no'] : "";
	$sch_state_get		= (isset($_GET['sch_state'])) ? $_GET['sch_state'] : "";
	$sch_s_no_get		= (isset($_GET['sch_s_no'])) ? $_GET['sch_s_no'] : "";
	$sch_c_name_get		= (isset($_GET['sch_c_name'])) ? $_GET['sch_c_name'] : "";
	$sch_memo_get		= (isset($_GET['sch_memo'])) ? $_GET['sch_memo'] : "";
	$open_est_wp_no_get	= (isset($_GET['open_est_wp_no'])) ? $_GET['open_est_wp_no'] : "";
	$sch_incentive_date_get  = (isset($_GET['sch_incentive_date'])) ? $_GET['sch_incentive_date'] : "";
	$sch_incentive_state_get = (isset($_GET['sch_incentive_state'])) ? $_GET['sch_incentive_state'] : "";


	if(!empty($sch_est_wp_no_get)) {
		$add_where .= " AND ewp.est_wp_no='{$sch_est_wp_no_get}'";
		$smarty->assign("sch_est_wp_no", $sch_est_wp_no_get);
	}
	
	if(!empty($sch_state_get)) {
		$add_where .= " AND ewp.state='{$sch_state_get}'";
		$smarty->assign("sch_state", $sch_state_get);
	}

	if (!empty($sch_incentive_date_get)) {
		$add_where .= " AND (SELECT dp.incentive_date FROM deposit dp WHERE dp.dp_no=ewp.dp_no)='{$sch_incentive_date_get}'";
		$smarty->assign("sch_incentive_date", $sch_incentive_date_get);
	}

	if(!empty($sch_incentive_state_get)) {
		if($sch_incentive_state_get == '1')
			$add_where .= " AND ewp.i_no IS NULL";
		else
			$add_where .= " AND (SELECT i.state FROM incentive i WHERE i.i_no=ewp.i_no)='{$sch_incentive_state_get}'";
		$smarty->assign("sch_incentive_state", $sch_incentive_state_get);
	}

	if(!empty($sch_s_no_get)) {
		if($sch_s_no_get != "all"){
			$add_where .= " AND ewp.s_no='{$sch_s_no_get}'";
		}
		$smarty->assign("sch_s_no", $sch_s_no_get);
	}elseif(empty($sch_s_no_get) && permissionNameCheck($session_permission, "마케터") && !permissionNameCheck($session_permission, "대표")){
		$add_where .= " AND ewp.s_no='{$session_s_no}'";
	}

	if(!empty($sch_c_name_get)) {
		$add_where 		 .= " AND ewp.c_name like '%{$sch_c_name_get}%'";
		$add_where_noset .= " AND c_name like '%{$sch_c_name_get}%'";
		$smarty->assign("sch_c_name", $sch_c_name_get);
	}

	if(!empty($sch_memo_get)) {
		$add_where .= " AND ewp.memo like '%{$sch_memo_get}%'";
		$smarty->assign("sch_memo", $sch_memo_get);
	}

	if(!empty($open_est_wp_no_get)) {
		$smarty->assign("open_est_wp_no", $open_est_wp_no_get);
	}else if(!empty($sch_est_wp_no_get)) {
		$smarty->assign("open_est_wp_no", $sch_est_wp_no_get);
	}

	// 견적서에 설정되지 않은 입금 내역 가져오기
	$deposit_sql = "
		SELECT
			dp.dp_no,
			dp.dp_date,
			dp.c_name,
			dp.s_no,
			(SELECT s_name FROM staff s WHERE s.s_no=dp.s_no) AS s_no_name,
			dp.dp_money
		FROM deposit dp
		LEFT JOIN estimate_work_package ewp ON dp.dp_no = ewp.dp_no
		WHERE ewp.dp_no IS NULL AND (dp.c_no <> '0' OR dp.c_no <> NULL) AND dp.s_no='{$session_s_no}'
		ORDER BY dp.dp_date DESC
	";
	$deposit_query = mysqli_query($my_db, $deposit_sql);
	$deposit_not_set_list = [];
	while($deposit_result = mysqli_fetch_array($deposit_query))
	{
		$deposit_not_set_list[] = array(
			"dp_no"		=> trim($deposit_result['dp_no']),
			"dp_date"	=> trim($deposit_result['dp_date']),
			"c_name"	=> trim($deposit_result['c_name']),
			"s_no"		=> trim($deposit_result['s_no']),
			"s_no_name"	=> trim($deposit_result['s_no_name']),
			"dp_money"	=> number_format(trim($deposit_result['dp_money']))
		);
	}
	$smarty->assign("prd_g1_list", $prd_g1_list);
	$smarty->assign("deposit_not_set_list", $deposit_not_set_list);


	// 견적서에 설정되지 않은 업무 내역 가져오기
	$work_sql = "
		SELECT
			w.w_no,
			DATE_FORMAT(w.regdate, '%Y-%m-%d') AS regdate,
			w.work_state,
			w.c_name,
			(SELECT s_name FROM staff s WHERE s.s_no=w.s_no) AS s_no_name,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1_name,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2_name,
			(SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_no_name,
			w.t_keyword
		FROM work w
		LEFT JOIN estimate_work ew ON w.w_no = ew.w_no
		WHERE {$add_where_noset} AND ew.w_no IS NULL AND w.s_no='{$session_s_no}'
		ORDER BY w.w_no DESC, w.prd_no DESC, w.regdate DESC
	";
	$work_query = mysqli_query($my_db,$work_sql);
	$work_not_set_list = [];
	while($work_array=mysqli_fetch_array($work_query))
	{
		$work_not_set_list[] = array(
			"w_no"			=> trim($work_array['w_no']),
			"regdate"		=> trim($work_array['regdate']),
			"work_state"	=> trim($work_array['work_state']),
			"c_name"		=> trim($work_array['c_name']),
			"s_no_name"		=> trim($work_array['s_no_name']),
			"k_prd1_name"	=> trim($work_array['k_prd1_name']),
			"k_prd2_name"	=> trim($work_array['k_prd2_name']),
			"prd_no_name"	=> trim($work_array['prd_no_name']),
			"t_keyword"		=> trim($work_array['t_keyword'])
		);
	}
	$smarty->assign("work_not_set_list", $work_not_set_list);

	// 상품정보 - 업무요청 기본 양식 가져오기
	$work_format_sql 	= "SELECT prd_no, work_format FROM product WHERE display='1'";
	$work_format_query	= mysqli_query($my_db, $work_format_sql);
	$work_format_list	= [];
	while($work_format_data = mysqli_fetch_array($work_format_query))
	{
		$work_format_list[] = array(
			"prd_no"		=> $work_format_data['prd_no'],
			"work_format"	=> $work_format_data['work_format']
		);
	}
	$smarty->assign("work_format_list", $work_format_list);


	# 리스트 쿼리
	$estimate_work_package_sql = "
		SELECT
			est_wp_no,
			regdate,
			state,
			dp_no,
			(SELECT dp.dp_date FROM deposit dp WHERE dp.dp_no=ewp.dp_no) AS dp_date,
			(SELECT dp.incentive_date FROM deposit dp WHERE dp.dp_no=ewp.dp_no) AS incentive_date,
			(SELECT dp.dp_money FROM deposit dp WHERE dp.dp_no=ewp.dp_no) AS dp_money,
			i_no,
			c_no,
			c_name,
			(SELECT i.state FROM incentive i WHERE i.i_no=ewp.i_no)  AS incentive_state,
			subject,
			s_no,
			(SELECT s_name FROM staff s WHERE s.s_no=ewp.s_no) AS s_no_name,
			(SELECT SUM(ew.price) FROM estimate_work ew WHERE ew.est_wp_no=ewp.est_wp_no) AS total_price,
			(SELECT SUM(ew.out_price) FROM estimate_work ew WHERE ew.est_wp_no=ewp.est_wp_no) AS total_out_price,
			memo,
			(SELECT COUNT(*) FROM estimate_work ew WHERE ew.est_wp_no=ewp.est_wp_no) AS mw_cnt
		FROM estimate_work_package ewp
		WHERE {$add_where}
		ORDER BY est_wp_no DESC
	";

	// 전체 게시물 수
	$cnt_sql 	= "SELECT count(*) FROM ({$estimate_work_package_sql}) AS cnt";
	$cnt_query	= mysqli_query($my_db, $cnt_sql);
	$cnt_data	= mysqli_fetch_array($cnt_query);
	$total_num	= $cnt_data[0];

	# 페이징
	$pages   = isset($_GET['page']) ? intval($_GET['page']) : 1;
	$num 	 = 10;
	$offset  = ($pages-1) * $num;
	$pagenum = ceil($total_num/$num);

	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	$search_url = getenv("QUERY_STRING");
	$pagelist   = pagelist($pages, "estimate_list.php", $pagenum, $search_url);

	$smarty->assign("total_num", $total_num);
	$smarty->assign("search_url",$search_url);
	$smarty->assign("pagelist", $pagelist);


	// 리스트 쿼리 결과(데이터)
	$estimate_work_package_sql  .= "LIMIT {$offset}, {$num}";
	$estimate_work_package_query = mysqli_query($my_db, $estimate_work_package_sql);
	while($estimate_work_package_array = mysqli_fetch_array($estimate_work_package_query))
	{
		$total_work_price_value  = 0;
		$package_price_value 	 = (!empty($estimate_work_package_array['price'])) ? number_format($estimate_work_package_array['price']) : "";
		$package_out_price_value = (!empty($estimate_work_package_array['out_price'])) ? number_format($estimate_work_package_array['out_price']) : "";
		$regdate_day_value  	 = (!empty($estimate_work_package_array['regdate'])) ? date("Y/m/d",strtotime($estimate_work_package_array['regdate'])) : "";
		$regdate_time_value 	 = (!empty($estimate_work_package_array['regdate'])) ? date("H:i",strtotime($estimate_work_package_array['regdate'])) : "";

		$nat_salse_price_value  = 0; // 인센기준 매출액 초기화
		$total_not6_money_comma = 0; // 예정업무 예상 외주비 최화
		if($estimate_work_package_array['dp_no'])
		{
			$nat_salse_price_sql = "
				SELECT
					(SELECT dp.dp_money FROM deposit dp WHERE dp.dp_no=ewp.dp_no) AS dp_money,
					(SELECT SUM(ew2.out_price) FROM estimate_work ew2 WHERE ew2.w_no IN (SELECT w.w_no FROM `work` w WHERE w.work_state!='6' AND w.w_no IN (SELECT ew.w_no FROM estimate_work ew WHERE ew.w_no IS NOT NULL AND ew.est_wp_no=ewp.est_wp_no))) AS w_state_not6_money,
					(SELECT SUM(ew.out_price) FROM estimate_work ew WHERE (ew.w_no='' OR ew.w_no IS NULL) AND ew.est_wp_no=ewp.est_wp_no) AS ew_out_price
				FROM estimate_work_package ewp
				WHERE ewp.est_wp_no='{$estimate_work_package_array['est_wp_no']}'
			";
			$nat_salse_price_query = mysqli_query($my_db, $nat_salse_price_sql);
			$nat_dp_money = $nat_w_state_6_money = $nat_w_state_not6_money = $nat_ew_out_price = 0;
			while($nat_salse_price_array = mysqli_fetch_array($nat_salse_price_query))
			{
				$nat_dp_money 			= !empty($nat_salse_price_array['dp_money']) ? $nat_salse_price_array['dp_money'] : 0;
				$nat_w_state_6_money 	= !empty($nat_salse_price_array['w_state_6_money']) ? $nat_salse_price_array['w_state_6_money'] : 0;
				$nat_w_state_not6_money = !empty($nat_salse_price_array['w_state_not6_money']) ? $nat_salse_price_array['w_state_not6_money'] : 0;
				$nat_ew_out_price 		= !empty($nat_salse_price_array['ew_out_price']) ? $nat_salse_price_array['ew_out_price'] : 0;
			}

			$nat_salse_price_value 		 = $nat_dp_money-$nat_w_state_6_money-$nat_w_state_not6_money-$nat_ew_out_price;
			$nat_salse_price_value_comma = number_format($nat_salse_price_value);
			$total_not6_money_comma 	 = number_format($nat_w_state_not6_money+$nat_ew_out_price);
		}else{
			$nat_salse_price_value = "";
			$nat_salse_price_value_comma = "";
		}

		// 견적서 업무 리스트 쿼리
		$estimate_work_sql = "
			SELECT
				est_w_no,
				est_wp_no,
				w_no,
				(SELECT work_state FROM work w WHERE w.w_no=ew.w_no) AS work_state,
				(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=ew.prd_no))) AS k_prd1,
				(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=ew.prd_no))) AS k_prd1_name,
				(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=ew.prd_no)) AS k_prd2,
				(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=ew.prd_no)) AS k_prd2_name,
				prd_no,
				(SELECT title from product prd where prd.prd_no=ew.prd_no) as prd_no_name,
				t_keyword_or_cnt,
				(SELECT t_keyword FROM work w WHERE w.w_no=ew.w_no) AS wrok_t_keyword,
				(SELECT r_keyword FROM work w WHERE w.w_no=ew.w_no) AS wrok_r_keyword,
				(SELECT task_req FROM work w WHERE w.w_no=ew.w_no) AS task_req,
				(SELECT task_run FROM work w WHERE w.w_no=ew.w_no) AS task_run,
				(SELECT task_req_file_origin FROM work w WHERE w.w_no=ew.w_no) AS task_req_file_origin,
				(SELECT task_req_file_read FROM work w WHERE w.w_no=ew.w_no) AS task_req_file_read,
				(SELECT task_run_file_origin FROM work w WHERE w.w_no=ew.w_no) AS task_run_file_origin,
				(SELECT task_run_file_read FROM work w WHERE w.w_no=ew.w_no) AS task_run_file_read,
				price,
				out_price,
				(SELECT price FROM work w WHERE w.w_no=ew.w_no) AS work_price,
				memo
			FROM estimate_work ew
			WHERE ew.est_wp_no='{$estimate_work_package_array['est_wp_no']}'
			ORDER BY k_prd1 ASC, k_prd2 ASC, prd_no ASC
		";
		$estimate_work_query = mysqli_query($my_db, $estimate_work_sql);
		while($estimate_work_array = mysqli_fetch_array($estimate_work_query))
		{
			$price_value 	 = (!empty($estimate_work_array['price'])) ? number_format($estimate_work_array['price']) : "";
			$out_price_value = (!empty($estimate_work_array['out_price'])) ? number_format($estimate_work_array['out_price']) : "";

			// 상품 구분 가져오기(2차)
			$prd_g2_sql="
		        SELECT
		            k_name,k_name_code,k_parent
		        FROM kind
		        WHERE k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='{$estimate_work_array['prd_no']}'))
		        ORDER BY priority ASC
			";

			$prd_g2_query 	= mysqli_query($my_db,$prd_g2_sql);
			while($prd_g2 = mysqli_fetch_array($prd_g2_query))
			{
				$prd_g2_list[] = array(
		      		"est_w_no"		=> trim($estimate_work_array['est_w_no']),
					"k_name"		=> trim($prd_g2['k_name']),
					"k_name_code"	=> trim($prd_g2['k_name_code']),
					"k_parent"		=> trim($prd_g2['k_parent'])
				);
			}
			$smarty->assign("prd_g2_list", $prd_g2_list);

			$prd_sql = "
		        SELECT
		            title,prd_no,k_name_code
		        FROM product
		        WHERE k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='{$estimate_work_array['prd_no']}')
		        ORDER BY priority ASC
			";
			$prd_query = mysqli_query($my_db,$prd_sql);
			while($prd = mysqli_fetch_array($prd_query))
			{
				$prd_list[] = array(
					"est_w_no"		=> trim($estimate_work_array['est_w_no']),
					"title"			=> trim($prd['title']),
					"prd_no"		=> trim($prd['prd_no']),
					"k_name_code"	=> trim($prd['k_name_code'])
				);
			}
			$smarty->assign("prd_list", $prd_list);

			// 총 실제 외주비 합계
			$total_work_price_value += $estimate_work_array['work_price'];
			$work_price_value = !empty($estimate_work_array['w_no']) ? number_format($estimate_work_array['work_price']) : "";

			// 지식인, 연관검색어, 자동완성 검색어 연장건 유무 확인
			$extension_d_day 	= "";
			$extension_w_no 	= "";
			if($estimate_work_array['prd_no'] == '7' || $estimate_work_array['prd_no'] == '9' || $estimate_work_array['prd_no'] == '30')
			{
				$extension_sql = "
			        SELECT
			            w.w_no,
						w.extension_date
			        FROM work w
			        WHERE w.prd_no='{$estimate_work_array['prd_no']}' AND w.c_no='{$estimate_work_package_array['c_no']}' AND w.s_no='{$estimate_work_package_array['s_no']}' AND
						REPLACE(w.t_keyword,' ','')=REPLACE('{$estimate_work_array['t_keyword_or_cnt']}',' ','') AND w.work_state='6' AND (w.extension IS NULL OR w.extension = '0' OR w.extension = '2') AND
						w.extension_date <= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL 30 DAY), '%Y-%m-%d') AND w.extension_date >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -3 DAY), '%Y-%m-%d')
					ORDER BY w.w_no DESC LIMIT 1
				";
				$extension_query = mysqli_query($my_db,$extension_sql);
				$extension_array = mysqli_fetch_array($extension_query);

				$extension_d_day = intval((strtotime(date("Y-m-d",time()))-strtotime($extension_array['extension_date'])) / 86400);
				$extension_w_no  = $extension_array['w_no'];

			}

			$estimate_work_lists[] = array(
				"est_w_no"			=> $estimate_work_array['est_w_no'],
				"est_wp_no"			=> $estimate_work_array['est_wp_no'],
				"w_no"				=> $estimate_work_array['w_no'],
				"work_state"		=> $estimate_work_array['work_state'],
				"k_prd1"			=> $estimate_work_array['k_prd1'],
				"k_prd1_name"		=> $estimate_work_array['k_prd1_name'],
				"k_prd2"			=> $estimate_work_array['k_prd2'],
				"k_prd2_name"		=> $estimate_work_array['k_prd2_name'],
				"prd_no"			=> $estimate_work_array['prd_no'],
				"prd_no_name"		=> $estimate_work_array['prd_no_name'],
				"t_keyword_or_cnt"	=> $estimate_work_array['t_keyword_or_cnt'],
				"wrok_t_keyword"	=> $estimate_work_array['wrok_t_keyword'],
				"wrok_r_keyword"	=> $estimate_work_array['wrok_r_keyword'],
				"task_req"			=> $estimate_work_array['task_req'],
				"task_run"			=> $estimate_work_array['task_run'],
				"price"				=> $price_value,
				"out_price"			=> $out_price_value,
				"work_price"		=> $work_price_value,
				"memo"				=> $estimate_work_array['memo'],
				"extension_w_no"	=> $extension_w_no,
				"extension_date"	=> $extension_array['extension_date'],
				"extension_d_day"	=> $extension_d_day,
				"task_req_file_origin"	=> $estimate_work_array['task_req_file_origin'],
				"task_run_file_origin"	=> $estimate_work_array['task_run_file_origin'],
			);
		}

		$estimate_lists[] = array(
			"est_wp_no"			=> $estimate_work_package_array['est_wp_no'],
			"regdate"			=> $estimate_work_package_array['regdate'],
			"regdate_day"		=> $regdate_day_value,
			"regdate_time"		=> $regdate_time_value,
			"state"				=> $estimate_work_package_array['state'],
			"dp_no"				=> $estimate_work_package_array['dp_no'],
			"dp_date"			=> $estimate_work_package_array['dp_date'],
			"incentive_date"	=> $estimate_work_package_array['incentive_date'],
			"dp_money_comma"	=> number_format($estimate_work_package_array['dp_money']),
			"dp_money"			=> $estimate_work_package_array['dp_money'],
			"i_no"				=> $estimate_work_package_array['i_no'],
			"c_no"				=> $estimate_work_package_array['c_no'],
			"c_name"			=> $estimate_work_package_array['c_name'],
			"incentive_state"	=> $estimate_work_package_array['incentive_state'],
			"subject"			=> $estimate_work_package_array['subject'],
			"s_no"				=> $estimate_work_package_array['s_no'],
			"s_no_name"			=> $estimate_work_package_array['s_no_name'],
			"memo"				=> $estimate_work_package_array['memo'],
			"mw_cnt"			=> $estimate_work_package_array['mw_cnt'],
			"nat_sales_price_comma"	 => $nat_salse_price_value_comma,
			"nat_sales_price"		 => $nat_salse_price_value,
			"total_not6_money_comma" => $total_not6_money_comma,
			"total_price_comma"		 => number_format($estimate_work_package_array['total_price']),
			"total_price"			 => $estimate_work_package_array['total_price'],
			"total_out_price_comma"	 => number_format($estimate_work_package_array['total_out_price']),
			"total_out_price"		 => $estimate_work_package_array['total_out_price'],
			"total_work_price_comma" => number_format($total_work_price_value),
			"total_work_price"		 => $total_work_price_value
		);
	}
	$smarty->assign("estimate_list", $estimate_lists);
	$smarty->assign("estimate_work_list", $estimate_work_lists);

	$smarty->assign("work_state_option", getWorkStateOption());
	$smarty->assign("incentive_state_option", getEstimateIncStateOption());

	$smarty->display('estimate_list.html');
}
?>
