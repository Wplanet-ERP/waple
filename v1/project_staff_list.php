<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/project.php');
require('inc/model/MyQuick.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "22";
$nav_title   = "내부 투입인력 현황";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$sch_date       = isset($_GET['sch_date']) ? $_GET['sch_date'] : date('Y-m-d');
$sch_active     = isset($_GET['sch_active']) ? $_GET['sch_active'] : "1";
$sch_s_name     = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
$sch_info       = isset($_GET['sch_info']) ? $_GET['sch_info'] : "";

$add_where          = "1=1";
$add_report_where   = "1=1";

if (!empty($sch_date))
{
    $add_report_where .= " AND (pir.pir_s_date <= '{$sch_date}' AND pir.pir_e_date >= '{$sch_date}')";
    $smarty->assign('sch_date', $sch_date);
}

if (!empty($sch_active)) {
    $add_where .= " AND ps.active = '{$sch_active}'";
    $smarty->assign('sch_active', $sch_active);
}

if (!empty($sch_s_name)) {
    $add_where .= " AND ps.s_name like '%{$sch_s_name}%'";
    $smarty->assign('sch_s_name', $sch_s_name);
}

if (!empty($sch_info)) {
    $add_where .= " AND ps.info like '%{$sch_info}%'";
    $smarty->assign('sch_info', $sch_info);
}

// 전체 게시물 수
$pj_total_sql	= "SELECT count(pj_s_no) FROM (SELECT `ps`.pj_s_no FROM project_staff `ps` WHERE {$add_where}) AS cnt";
$pj_total_query	= mysqli_query($my_db, $pj_total_sql);
if(!!$pj_total_query)
    $pj_total_result = mysqli_fetch_array($pj_total_query);

$pj_staff_total = $pj_total_result[0];

$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 10;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($pj_staff_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "project_staff_list.php", $pagenum, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $pj_staff_total);
$smarty->assign("pagelist", $page);

$project_staff_sql = "
    SELECT
        ps.my_c_no,
        (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=ps.my_c_no) as my_c_name,
        ps.pj_s_no,
        ps.s_no,
        ps.s_name,
        ps.active,
        ps.info
    FROM project_staff ps
    WHERE {$add_where}
    ORDER BY s_name ASC
    LIMIT {$offset}, {$num}
";
$project_staff_query   = mysqli_query($my_db, $project_staff_sql);
$project_staff_list    = [];
$pj_s_no_list          = [];
$active_option         = getActiveOption();
while($project_staff = mysqli_fetch_assoc($project_staff_query))
{
    $project_staff['active_name']  = isset($active_option[$project_staff['active']]) ? $active_option[$project_staff['active']] : "";

    $project_staff['confirm']   = 0;
    $project_staff['unconfirm'] = 0;
    $project_staff['confirm_resource']  = 0;
    $project_staff['total_resource']    = 0;
    $project_staff['unconfirm']         = 0;

    $project_staff_list[$project_staff['pj_s_no']] = $project_staff;
    $pj_s_no_list[$project_staff['pj_s_no']] = $project_staff['pj_s_no'];
}

$pj_s_nos = !empty($pj_s_no_list) ? implode(",", $pj_s_no_list): "";

$project_report_sql = "
    SELECT
      pir.pj_s_no,
      (SELECT p.state FROM project p WHERE p.pj_no = pir.pj_no) as state,
      SUM(CASE WHEN p.state='2' THEN pir.resource END) as confirm,
      SUM(CASE WHEN p.state='1' THEN pir.resource END) as unconfirm
    FROM project_input_report `pir`
    LEFT JOIN project `p` ON `pir`.pj_no = `p`.pj_no
    WHERE {$add_report_where} AND p.display='1' AND pir.pj_s_no IN({$pj_s_nos}) AND pir.active='1'
    GROUP BY pj_s_no
";
$project_report_query   = mysqli_query($my_db, $project_report_sql);

while($project_report = mysqli_fetch_assoc($project_report_query))
{
    $confirm    = isset($project_report['confirm']) && !empty($project_report['confirm']) ? $project_report['confirm'] : 0;
    $unconfirm  = isset($project_report['unconfirm']) && !empty($project_report['unconfirm']) ? $project_report['unconfirm'] : 0;
    $total      = $confirm+$unconfirm;

    $confirm_resource = isset($project_staff_list[$project_report['pj_s_no']]) ? $project_staff_list[$project_report['pj_s_no']]['confirm_resource'] + $confirm : $confirm;
    $total_resource   = isset($project_staff_list[$project_report['pj_s_no']]) ? $project_staff_list[$project_report['pj_s_no']]['total_resource'] + $total : $total;

    $project_staff_list[$project_report['pj_s_no']]['confirm_resource'] = $confirm_resource;
    $project_staff_list[$project_report['pj_s_no']]['total_resource']   = $total_resource;
}

$pj_active_staff_total_sql      = "SELECT count(pj_s_no) as cnt FROM project_staff ps WHERE ps.active='1'";
$pj_active_staff_total_query    = mysqli_query($my_db, $pj_active_staff_total_sql);
$pj_active_staff_total_result   = mysqli_fetch_assoc($pj_active_staff_total_query);
$pj_active_staff_total          = isset($pj_active_staff_total_result['cnt']) && !empty($pj_active_staff_total_result['cnt']) ? $pj_active_staff_total_result['cnt'] : 0;

$pj_input_staff_total_sql       = "SELECT count(DISTINCT pj_s_no) as cnt FROM project_input_report `pir` LEFT JOIN project `p` ON `pir`.pj_no = `p`.pj_no  WHERE {$add_report_where} AND pir.pj_s_no IN((SELECT ps.pj_s_no FROM project_staff ps WHERE {$add_where}))";
$pj_input_staff_total_query     = mysqli_query($my_db, $pj_input_staff_total_sql);
$pj_input_staff_total_result    = mysqli_fetch_assoc($pj_input_staff_total_query);
$pj_input_staff_total           = isset($pj_input_staff_total_result['cnt']) && !empty($pj_input_staff_total_result['cnt']) ? $pj_input_staff_total_result['cnt'] : 0;

$pj_input_staff_per = 0;
if(!empty($project_staff_list) && $pj_active_staff_total > 0) {
    $pj_input_staff_per = round(($pj_input_staff_total/$pj_active_staff_total)*100, 1);
}

$staff_deactivate_sql   = "SELECT * FROM staff s WHERE s_no IN(SELECT ps.s_no FROM project_staff ps WHERE ps.active='1') AND staff_state='3'";
$staff_deactivate_query = mysqli_query($my_db, $staff_deactivate_sql);
$staff_deactivate_list  = [];
while($staff_deactivate_result = mysqli_fetch_assoc($staff_deactivate_query))
{
    $staff_deactivate_list[] = $staff_deactivate_result;
}

$smarty->assign("active_option", $active_option);
$smarty->assign("project_staff_list", $project_staff_list);
$smarty->assign("pj_input_staff_per", $pj_input_staff_per);
$smarty->assign("staff_deactivate_list", $staff_deactivate_list);

$smarty->display('project_staff_list.html');
?>
