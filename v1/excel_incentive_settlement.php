<?php
ini_set('max_execution_time', 60);
ini_set('memory_limit', '1024M');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/deposit.php');
require('inc/helper/withdraw.php');
require('inc/helper/incentive.php');
require('inc/model/Team.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$lfcr = chr(10);
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "No")
	->setCellValue('B1', "진행상태")
	->setCellValue('C1', "업체명")
	->setCellValue('D1', "부서{$lfcr}담당자")
	->setCellValue('E1', "업무명")
	->setCellValue('F1', "입/출금명")
	->setCellValue('G1', "업무진행내용")
	->setCellValue('H1', "입/출금{$lfcr}구분")
	->setCellValue('I1', "입/출금방식")
	->setCellValue('J1', "입/출금액{$lfcr}(VAT 별도)")
	->setCellValue('K1', "입/출금액{$lfcr}(VAT 포함)")
	->setCellValue('L1', "입/출금업체")
	->setCellValue('M1', "입/출금완료일")
	->setCellValue('N1', "인센티브{$lfcr}정산 년/월")
	->setCellValue('O1', "인센확인{$lfcr}확인날짜")
	->setCellValue('P1', "메모")
;

# 기본 검색조건
$team_model     	 = Team::Factory();
$incentive_data_list = $team_model->getIncentiveTeamAndStaffList();
$team_full_name_list = $incentive_data_list["team_list"];
$team_where_list     = $incentive_data_list["team_where"];
$team_title          = "전체";
$default_team_where  = implode(',', $team_where_list);

$add_dp_where   = "1=1 AND main.display='1' AND main.dp_state='2' AND main.team IN({$default_team_where})";
$add_wd_where   = "1=1 AND main.display='1' AND main.wd_state='3' AND main.team IN({$default_team_where})";
$add_where      = "1=1";

# 기본 function
$inc_state_option       = getIncStateOption();
$inc_type_option        = getIncTypeOption();
$inc_sort_option        = getIncSortOption();
$inc_method_option      = [];
$deposit_state_option   = getDpStateOption();
$withdraw_state_option  = getWdStateOption();
$deposit_method_option  = getDpMethodOption();
$withdraw_method_option = getWdMethodOption();

# 검색조건
$sch_dp_wd_no		    = isset($_GET['sch_dp_wd_no']) ? $_GET['sch_dp_wd_no'] : "";
$sch_s_dp_wd_date 		= isset($_GET['sch_s_dp_wd_date']) ? $_GET['sch_s_dp_wd_date'] : "";
$sch_e_dp_wd_date 		= isset($_GET['sch_e_dp_wd_date']) ? $_GET['sch_e_dp_wd_date'] : "";
$sch_incentive_month    = isset($_GET['sch_incentive_month']) ? $_GET['sch_incentive_month'] : "";
$sch_incentive_state    = isset($_GET['sch_incentive_state']) ? $_GET['sch_incentive_state'] : "1";
$sch_incentive_type     = isset($_GET['sch_incentive_type']) ? $_GET['sch_incentive_type'] : "withdraw";
$sch_incentive_method   = isset($_GET['sch_incentive_method']) ? $_GET['sch_incentive_method'] : "";
$sch_manager_team       = isset($_GET['sch_manager_team']) ? $_GET['sch_manager_team'] : "";
$sch_manager	        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_work_company       = isset($_GET['sch_work_company']) ? $_GET['sch_work_company'] : "";
$sch_work_name          = isset($_GET['sch_work_name']) ? $_GET['sch_work_name'] : "";
$sch_work_no            = isset($_GET['sch_work_no']) ? $_GET['sch_work_no'] : "";
$sch_subject            = isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
$sch_company            = isset($_GET['sch_company']) ? $_GET['sch_company'] : "";
$sch_memo               = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";

if(!empty($sch_dp_wd_no)){
	$add_dp_where .= " AND main.dp_no = '{$sch_dp_wd_no}'";
	$add_wd_where .= " AND main.wd_no = '{$sch_dp_wd_no}'";
}

if (!empty($sch_s_dp_wd_date)) {
	if($sch_incentive_type == 'deposit'){
		$add_dp_where .= " AND main.dp_date >= '{$sch_s_dp_wd_date}'";
	}elseif($sch_incentive_type == 'withdraw'){
		$add_wd_where .= " AND main.wd_date >= '{$sch_s_dp_wd_date}'";
	}else{
		$add_dp_where .= " AND main.dp_date >= '{$sch_s_dp_wd_date}'";
		$add_wd_where .= " AND main.wd_date >= '{$sch_s_dp_wd_date}'";
	}
}

if (!empty($sch_e_dp_wd_date)) {
	if($sch_incentive_type == 'deposit'){
		$add_dp_where .= " AND main.dp_date <= '{$sch_e_dp_wd_date}'";
	}elseif($sch_incentive_type == 'withdraw'){
		$add_wd_where .= " AND main.wd_date <= '{$sch_e_dp_wd_date}'";
	}else{
		$add_dp_where .= " AND main.dp_date <= '{$sch_e_dp_wd_date}'";
		$add_wd_where .= " AND main.wd_date <= '{$sch_e_dp_wd_date}'";
	}
}

if(!empty($sch_incentive_month))
{
	$add_dp_where .= " AND main.incentive_month = '{$sch_incentive_month}'";
	$add_wd_where .= " AND main.incentive_month = '{$sch_incentive_month}'";
}

if(!empty($sch_incentive_state))
{
	$add_dp_where .= " AND main.incentive_state = '{$sch_incentive_state}'";
	$add_wd_where .= " AND main.incentive_state = '{$sch_incentive_state}'";
}

if(!empty($sch_incentive_type))
{
	if($sch_incentive_type == 'deposit'){
		$inc_method_option = $deposit_method_option;
		$add_wd_where .= " AND 1!=1";
	}elseif($sch_incentive_type == 'withdraw'){
		$inc_method_option = $withdraw_method_option;
		$add_dp_where .= " AND 1!=1";
	}

	if(!empty($sch_incentive_method))
	{
		$add_where .= " AND method = '{$sch_incentive_method}'";
	}
}

if (!empty($sch_manager_team))
{
	if ($sch_manager_team != "all") {
		$sch_team_code_where = getTeamWhere($my_db, $sch_manager_team);
		$add_dp_where   .= " AND main.`team` IN({$sch_team_code_where})";
		$add_wd_where 	.= " AND main.`team` IN({$sch_team_code_where})";
	}
}

if (!empty($sch_manager))
{
	$add_dp_where  .= " AND main.`s_no` IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_manager}%')";
	$add_wd_where  .= " AND main.`s_no` IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_manager}%')";
}

if(!empty($sch_work_company))
{
	$add_dp_where .= " AND w.c_name LIKE '%{$sch_work_company}%'";
	$add_wd_where .= " AND w.c_name LIKE '%{$sch_work_company}%'";
}

if(!empty($sch_work_name))
{
	$add_dp_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product p WHERE p.title LIKE '%{$sch_work_name}%' AND p.display='1')";
	$add_wd_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product p WHERE p.title LIKE '%{$sch_work_name}%' AND p.display='1')";
}

if(!empty($sch_work_no))
{
	$add_dp_where .= " AND w.w_no = '{$sch_work_no}'";
	$add_wd_where .= " AND w.w_no = '{$sch_work_no}'";
}

if(!empty($sch_subject))
{
	$add_dp_where .= " AND main.dp_subject LIKE '%{$sch_subject}%'";
	$add_wd_where .= " AND main.wd_subject LIKE '%{$sch_subject}%'";
}

if(!empty($sch_company))
{
	$add_dp_where .= " AND main.c_name LIKE '%{$sch_company}%'";
	$add_wd_where .= " AND main.c_name LIKE '%{$sch_company}%'";
}

if(!empty($sch_memo))
{
	$add_dp_where .= " AND main.manager_memo LIKE '%{$sch_memo}%'";
	$add_wd_where .= " AND main.manager_memo LIKE '%{$sch_memo}%'";
}

# 정렬
$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";
$add_order_by   = "regdate DESC";

if(!empty($ord_type))
{
	if($ord_type_by == '1'){
		$orderby_val = "ASC";
	}else{
		$orderby_val = "DESC";
	}

	$add_order_by = "{$ord_type} {$orderby_val}, regdate DESC";
}


# 쿼리문
$incentive_sql = "
    SELECT
       *
    FROM
    (
        (
            SELECT
                main.w_no,
                main.dp_no AS main_no,
                main.dp_state AS state,
                main.dp_method AS method,
                main.vat_choice AS vat_type,
                w.c_name AS work_company,
                (SELECT s_name FROM staff s WHERE s.s_no=main.s_no) AS s_name,
                (SELECT team_name FROM team t WHERE t.team_code=main.team) AS t_name,
                (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) AS prd_title,
                main.dp_subject as subject,
                w.task_run,
                'deposit' AS type,
                main.supply_cost,
                main.cost,
                main.dp_money AS confirm_price,
                main.dp_money_vat AS confirm_price_vat,
                main.dp_date AS confirm_date,     
                main.c_name AS confirm_company,
                main.incentive_state,
                main.incentive_month,
                main.incentive_date,
                main.manager_memo,
                main.regdate
            FROM deposit main
            LEFT JOIN work w ON w.w_no=main.w_no
            WHERE {$add_dp_where}
        )
        UNION
        (
            SELECT
                main.w_no,
                main.wd_no AS main_no,
                main.wd_state as state,
                main.wd_method AS method,
                main.vat_choice AS vat_type,
                w.c_name AS work_company,
                (SELECT s_name FROM staff s WHERE s.s_no=main.s_no) AS s_name,
                (SELECT team_name FROM team t WHERE t.team_code=main.team) AS t_name,
                (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) AS prd_title,
                main.wd_subject as subject,
                w.task_run,
                'withdraw' AS type,
                main.supply_cost,
                main.cost,
                '0' AS confirm_price,
                main.wd_money AS confirm_price_vat,
                main.wd_date AS confirm_date,
                main.c_name AS confirm_company,
                main.incentive_state,
                main.incentive_month,
                main.incentive_date,
                main.manager_memo,
                main.regdate
            FROM withdraw main
            LEFT JOIN work w ON w.w_no=main.w_no
            WHERE {$add_wd_where}
        )
    ) AS main_result
    WHERE {$add_where}
    ORDER BY {$add_order_by}
";
$incentive_query = mysqli_query($my_db, $incentive_sql);
$idx = 2;
while($incentive = mysqli_fetch_assoc($incentive_query))
{
	$incentive['type_name']  = $inc_type_option[$incentive['type']]['title'];
	$type_name = "";
	if($incentive['type'] == 'deposit'){
		$incentive['state_name']    = $deposit_state_option[$incentive['state']];
		$incentive['method_name']   = $deposit_method_option[$incentive['method']];
		$type_name = "입금";
	}else{
		$incentive['state_name']    = $withdraw_state_option[$incentive['state']];
		$incentive['method_name']   = $withdraw_method_option[$incentive['method']];
		$type_name = "출금";
	}

	if($incentive['incentive_date'] >= '1970-01-01'){
		$incentive['incentive_date'] = date("Y-m-d", strtotime("{$incentive['incentive_date']}"));
	}else{
		$incentive['incentive_date'] = '';
	}

	if($incentive['type'] == 'deposit')
	{
		if($incentive['method'] == '3'){
			$incentive['view_price']     = $incentive['supply_cost'];
			$incentive['view_price_vat'] = $incentive['cost'];
		}else{
			$incentive['view_price']     = $incentive['confirm_price'];
			$incentive['view_price_vat'] = $incentive['confirm_price_vat'];
		}
	}
	elseif($incentive['type'] == 'withdraw')
	{
		if($incentive['method'] == '3' || $incentive['method'] == '4'){
			$incentive['view_price']     = $incentive['supply_cost'];
			$incentive['view_price_vat'] = $incentive['cost'];
		} elseif($incentive['vat_type'] == '2'){
			$incentive['view_price']     = $incentive['supply_cost'];
			$incentive['view_price_vat'] = $incentive['confirm_price_vat'];
		}else{
			$incentive['view_price']     = round($incentive['confirm_price_vat']/1.1,0);
			$incentive['view_price_vat'] = $incentive['confirm_price_vat'];
		}

	}else{
		$incentive['view_price']        = $incentive['confirm_price'];
		$incentive['view_price_vat']    = $incentive['confirm_price_vat'];
	}


	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue("A{$idx}", $incentive['main_no'])
		->setCellValue("B{$idx}", $incentive['state_name'])
		->setCellValue("C{$idx}", $incentive['work_company'])
		->setCellValue("D{$idx}", $incentive['t_name'].$lfcr.$incentive['s_name'])
		->setCellValue("E{$idx}", $incentive['prd_title'])
		->setCellValue("F{$idx}", $incentive['subject'])
		->setCellValue("G{$idx}", $incentive['task_run'])
		->setCellValue("H{$idx}", $incentive['type_name'])
		->setCellValue("I{$idx}", $incentive['method_name'])
		->setCellValue("J{$idx}", number_format($incentive['view_price'], 0))
		->setCellValue("K{$idx}", number_format($incentive['view_price_vat'], 0))
		->setCellValue("L{$idx}", $incentive['confirm_company'])
		->setCellValue("M{$idx}", $incentive['confirm_date'].$lfcr.$type_name)
		->setCellValue("N{$idx}", $incentive['incentive_month'])
		->setCellValue("O{$idx}", $incentive['incentive_date'])
		->setCellValue("P{$idx}", $incentive['manager_memo'])
	;

	$idx++;
}
$idx--;


$objPHPExcel->getActiveSheet()->getStyle("A1:P{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:P{$idx}")->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:P{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');
$objPHPExcel->getActiveSheet()->getStyle("A1:P1")->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("A1:P1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:P1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFC0C0C0');
$objPHPExcel->getActiveSheet()->getStyle("A2:P{$idx}")->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("C2:C{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("E2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("J2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("D1:D{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getStyle("H1")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("J1")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("K1")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("M2:M{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("N1:N{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("O1")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("E1:E{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(40);

$objPHPExcel->getActiveSheet()->setTitle('인센티브 정산리스트');
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_인센티브리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
