<?php
ini_set('display_errors', '300');
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/model/Custom.php');
require('inc/model/ProductCmsStock.php');
require('inc/model/Message.php');
require('Classes/PHPExcel.php');

# 재고현황 Post
$stock_file         = $_FILES["stock_file"];
$log_c_no           = isset($_POST['stock_warehouse']) ? $_POST['stock_warehouse'] : 2809;
$stock_date         = isset($_POST['stock_date']) ? $_POST['stock_date'] : date('Y-m-d');
$regdate            = date('Y-m-d H:i:s');

# 재고 알림 메세지
$message_sql = "
    SELECT 
        pcu.`no`, 
        pcum.qty_alert, 
        REPLACE(pcum.qty_hp,'-','') as qty_hp, 
        (SELECT REPLACE(s.hp,'-','') FROM staff s WHERE s.s_no=pcu.ord_s_no) as manager_hp, 
        (SELECT REPLACE(s.hp,'-','') FROM staff s WHERE s.s_no=pcu.ord_sub_s_no) as sub_manager_hp, 
        (SELECT REPLACE(s.hp,'-','') FROM staff s WHERE s.s_no=pcu.ord_third_s_no) as third_manager_hp
    FROM product_cms_unit as pcu 
    LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=pcu.`no` AND pcum.log_c_no='{$log_c_no}'
    WHERE pcu.display='1' AND pcum.qty_alert > 0
";
$message_query      = mysqli_query($my_db, $message_sql);
$message_list       = [];
while($message_result = mysqli_fetch_assoc($message_query)){
    if($message_result['qty_alert'] > 1){
        $message_list[$message_result['no']] = array(
            'qty'               => $message_result['qty_alert'],
            'hp'                => $message_result['qty_hp'],
            'manager_hp'        => $message_result['manager_hp'],
            'sub_manager_hp'    => $message_result['sub_manager_hp'],
            'third_manager_hp'  => $message_result['third_manager_hp']
        );
    }
}

/**
# 재고현황 데이터 확인
$stock_date_chk_sql    = "SELECT count(`no`) as cnt FROM product_cms_stock WHERE stock_date='{$stock_date}' AND stock_c_no='{$log_c_no}'";
$stock_date_chk_query  = mysqli_query($my_db, $stock_date_chk_sql);
$stock_date_chk_result = mysqli_fetch_assoc($stock_date_chk_query);

if(isset($stock_date_chk_result['cnt']) && $stock_date_chk_result['cnt'] > 0) {
    exit("<script>alert('기준일이 같은 날짜로 등록된 자료가 있어 업로드 할 수 없습니다.');location.href='waple_upload_management.php';</script>");
}
*/

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($stock_file['name']) && !empty($stock_file['name'])){
    $upload_file_path = add_store_file($stock_file, "upload_management");
    $upload_file_name = $stock_file['name'];
}

$upload_data  = array(
    "upload_type"   => 1,
    "upload_kind"   => $log_c_no,
    "upload_date"   => $stock_date,
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $session_s_no,
    "regdate"       => $regdate,
);
$upload_model->insert($upload_data);

# 재고 현황 처리
$stock_model     = ProductCmsStock::Factory();
$stock_qty_list  = [];
$stock_data_list = [];
$file_no         = $upload_model->getInsertId();
$excel_file_path = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;

if($log_c_no == '2809')
{
    $excelReader = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
    $excelReader->setReadDataOnly(true);

    $excel = $excelReader->load($excel_file_path);
    $excel->setActiveSheetIndex(0);
    $objWorksheet = $excel->getActiveSheet();
    $totalRow     = $objWorksheet->getHighestRow();

    $stock_prd_cell = $stock_prd_sku_cell = $stock_center_cell = $warehouse_cell = $qty_cell = "";
    $eng_abc        = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
    for ($i = 2; $i < $totalRow; $i++)
    {
        if($i == "2")
        {
            $highestColumn  = $objWorksheet->getHighestColumn();
            $titleRowData   = $objWorksheet->rangeToArray("A{$i}:".$highestColumn.$i, NULL, TRUE, FALSE);

            foreach ($titleRowData[0] as $key => $title)
            {
                switch($title){
                    case "대표코드":
                        $stock_prd_cell = $eng_abc[$key];
                        break;
                    case "상품코드":
                        $stock_prd_sku_cell = $eng_abc[$key];
                        break;
                    case "물류센터":
                        $stock_center_cell = $eng_abc[$key];
                        break;
                    case "창고":
                        $warehouse_cell = $eng_abc[$key];
                        break;
                    case "재고수량":
                        $qty_cell = $eng_abc[$key];
                        break;
                }
            }
            continue;
        }

        if(empty($stock_prd_cell) || empty($stock_prd_sku_cell) || empty($stock_center_cell) || empty($warehouse_cell) || empty($qty_cell)){
            echo "변환되지 않는 필드 값이 있습니다. 개발팀 임태형에게 문의해주세요!";
            exit;
        }

        $stock_prd_type = (string)trim(addslashes($objWorksheet->getCell("{$stock_prd_cell}{$i}")->getValue())); //품목분류
        $stock_prd_sku  = (string)trim(addslashes($objWorksheet->getCell("{$stock_prd_sku_cell}{$i}")->getValue())); //재고관리코드(SKU)
        $stock_center   = (string)trim(addslashes($objWorksheet->getCell("{$stock_center_cell}{$i}")->getValue())); //물류센터
        $warehouse      = (string)trim(addslashes($objWorksheet->getCell("{$warehouse_cell}{$i}")->getValue())); //창고
        $qty            = (string)trim(addslashes($objWorksheet->getCell("{$qty_cell}{$i}")->getValue())); //재고수량

        # 와이즈 구성상품 확인
        $product_sql    = "
            SELECT 
                pcu.no, 
                pcu.option_name, 
                pcu.sup_c_no, 
                pcu.ord_s_no,  
                pcum.log_c_no, 
                pcum.warehouse, 
                pcu.expiration 
            FROM product_cms_unit as pcu 
            LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=pcu.no AND pcum.log_c_no='{$log_c_no}'
            WHERE pcum.sku = '{$stock_prd_sku}'
        ";
        $product_query  = mysqli_query($my_db, $product_sql);
        $product_data   = mysqli_fetch_array($product_query);
        $prd_unit       = isset($product_data['no']) ? $product_data['no'] : "";
        $prd_name       = isset($product_data['option_name']) ? $product_data['option_name'] : "";
        $sup_c_no       = isset($product_data['sup_c_no']) ? $product_data['sup_c_no'] : "";
        $ord_s_no       = isset($product_data['ord_s_no']) ? $product_data['ord_s_no'] : "";
        $stock_c_no     = isset($product_data['log_c_no']) ? $product_data['log_c_no'] : "";
        $warehouse_val  = isset($product_data['warehouse']) ? $product_data['warehouse'] : "";

        if(empty($prd_unit)){
            $upload_model->delete($file_no);
            del_file($upload_file_path);
            
            echo "등록된 구성품이 없는 SKU : {$stock_prd_sku} 입니다.<br/>";
            exit;
        }

        if(!empty($warehouse_val) && (strpos($warehouse, "정상") !== false || strpos($warehouse, "세이프인") !== false) && $qty != 0)
        {
            if(!isset($stock_qty_list[$prd_unit])){
                $stock_qty_list[$prd_unit]['prd_name'] = $prd_name;
                $stock_qty_list[$prd_unit]['qty'] = $qty;
            }else{
                $stock_qty_list[$prd_unit]['qty'] += $qty;
            }
        }

        $stock_data_list[] = array(
            "stock_date"    => $stock_date,
            "prd_unit"      => $prd_unit,
            "sup_c_no"      => $sup_c_no,
            "ord_s_no"      => $ord_s_no,
            "prd_name"      => $prd_name,
            "stock_c_no"    => $stock_c_no,
            "stock_prd_type"=> $stock_prd_type,
            "stock_prd_sku" => $stock_prd_sku,
            "stock_center"  => $stock_center,
            "warehouse"     => $warehouse,
            "qty"           => $qty,
            "regdate"       => $regdate,
            "reg_s_no"      => $session_s_no,
            "file_no"       => $file_no,
        );
    }
}
elseif ($log_c_no == '5711' || $log_c_no == '5795' || $log_c_no == '5885')
{
    $csv_file_path  = fopen($excel_file_path, "r");
    $file_idx       = 0;
    $warehouse      = "아마존(미국) 창고";
    $stock_prd_type = "상품";
    $stock_center   = "";

    switch ($log_c_no){
        case "5711":
            $warehouse  = "아마존_닥터피엘(미국) 창고";
            break;
        case "5795":
            $warehouse  = "아마존_아이레놀(일본) 창고";
            break;
        case "5885":
            $warehouse  = "아마존_아이레놀(미국) 창고";
            break;
    }

    while ($csv_file_data = fgetcsv($csv_file_path, 2048, ","))
    {
        $file_idx++;

        if($file_idx < 2){
            continue;
        }

        $stock_prd_sku  = $csv_file_data[1]; //재고관리코드(SKU)
        $main_qty       = $csv_file_data[6];
        $sub_qty        = $csv_file_data[58];
        $total_qty      = $main_qty+$sub_qty;

        if(!isset($stock_data_list[$stock_prd_sku]))
        {
            # 와이즈 구성상품 확인
            $product_sql    = "
                SELECT 
                    pcu.no, 
                    pcu.option_name, 
                    pcu.sup_c_no, 
                    pcu.ord_s_no,  
                    pcum.log_c_no, 
                    pcum.warehouse, 
                    pcu.expiration 
                FROM product_cms_unit as pcu 
                LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=pcu.no AND pcum.log_c_no='{$log_c_no}'
                WHERE pcum.sku = '{$stock_prd_sku}'
            ";
            $product_query  = mysqli_query($my_db, $product_sql);
            $product_data   = mysqli_fetch_array($product_query);
            $prd_unit       = isset($product_data['no']) ? $product_data['no'] : "";
            $prd_name       = isset($product_data['option_name']) ? $product_data['option_name'] : "";
            $sup_c_no       = isset($product_data['sup_c_no']) ? $product_data['sup_c_no'] : "";
            $ord_s_no       = isset($product_data['ord_s_no']) ? $product_data['ord_s_no'] : "";
            $stock_c_no     = isset($product_data['log_c_no']) ? $product_data['log_c_no'] : "";

            if(empty($prd_unit)){
                continue;
            }

            $stock_data_list[$stock_prd_sku] = array(
                "stock_date"    => $stock_date,
                "prd_unit"      => $prd_unit,
                "sup_c_no"      => $sup_c_no,
                "ord_s_no"      => $ord_s_no,
                "prd_name"      => $prd_name,
                "stock_c_no"    => $stock_c_no,
                "stock_prd_type"=> $stock_prd_type,
                "stock_prd_sku" => $stock_prd_sku,
                "stock_center"  => $stock_center,
                "warehouse"     => $warehouse,
                "qty"           => $total_qty,
                "regdate"       => $regdate,
                "reg_s_no"      => $session_s_no,
                "file_no"       => $file_no,
            );
        }
        else{
            $stock_data_list[$stock_prd_sku]['qty'] += $total_qty;
        }
    }
}

# 재고 체크
$message_chk_list   = [];
if(!empty($message_list))
{
    foreach($message_list as $pcu_no => $msg_data)
    {
        $chk_qty = 0;
        if(isset($stock_qty_list[$pcu_no]))
        {
            $stock_data = $stock_qty_list[$pcu_no];
            $chk_qty = $stock_data['qty'];
            $msg_qty = $msg_data['qty'];

            if($chk_qty != 0 && $chk_qty <= $msg_qty)
            {
                if(!empty($msg_data['hp'])){
                    $message_chk_list[$pcu_no][$msg_data['hp']] = array(
                        "pcu_no"    => $pcu_no,
                        "pcu_name"  => $stock_data['prd_name'],
                        "pcu_qty"   => $chk_qty,
                        "hp"        => $msg_data['hp']
                    );
                }

                if(!empty($msg_data['manager_hp'])){
                    $message_chk_list[$pcu_no][$msg_data['manager_hp']] = array(
                        "pcu_no"    => $pcu_no,
                        "pcu_name"  => $stock_data['prd_name'],
                        "pcu_qty"   => $chk_qty,
                        "hp"        => $msg_data['manager_hp']
                    );
                }

                if(!empty($msg_data['sub_manager_hp'])){
                    $message_chk_list[$pcu_no][$msg_data['sub_manager_hp']] = array(
                        "pcu_no"    => $pcu_no,
                        "pcu_name"  => $stock_data['prd_name'],
                        "pcu_qty"   => $chk_qty,
                        "hp"        => $msg_data['sub_manager_hp']
                    );
                }

                if(!empty($msg_data['third_manager_hp'])){
                    $message_chk_list[$pcu_no][$msg_data['third_manager_hp']] = array(
                        "pcu_no"    => $pcu_no,
                        "pcu_name"  => $stock_data['prd_name'],
                        "pcu_qty"   => $chk_qty,
                        "hp"        => $msg_data['third_manager_hp']
                    );
                }
            }
        }
    }
}

if (!$stock_model->multiInsert($stock_data_list)){
    $upload_model->delete($file_no);
    del_file($upload_file_path);
    
    echo "재고 반영에 실패하였습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영";
    exit;
}else{
    if(!empty($message_chk_list))
    {
        $cm_id          = 1;
        $cur_datetime	= date("YmdHis");
        $sms_title      = "재고알림 입니다";
        $send_name      = "와이즈플래닛";
        $send_phone     = "070-7873-1373";
        $c_info         = "8"; # 재고관리
        $send_data_list = [];

        foreach($message_chk_list as $pcu_no => $msg_chk_data)
        {
            foreach($msg_chk_data as $msg_chk_detail)
            {
                $msg_id 	    = "W{$cur_datetime}".sprintf('%04d', $cm_id++);
                $receiver_hp    = $msg_chk_detail['hp'];
                $content        = "[재고알림]\r\n재고가 부족합니다.\r\n재고수량을 확인해주세요.\r\n품목명 : {$msg_chk_detail['pcu_name']}\r\n재고수량: ".number_format($msg_chk_detail['pcu_qty']);
                $link           = "https://work.wplanet.co.kr/v1/product_cms_stock_stats.php?sch_ord_s_no=&sch_sup_c_no=&sch_option={$pcu_no}";
                $content       .= "\r\n".$link;
                $sms_msg        = addslashes($content);
                $sms_msg_len    = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
                $msg_type       = ($sms_msg_len > 90) ? "L" : "S";

                $send_data_list[$msg_id] = array(
                    "msg_id"        => $msg_id,
                    "msg_type"      => $msg_type,
                    "sender"        => $send_name,
                    "sender_hp"     => $send_phone,
                    "receiver_hp"   => $receiver_hp,
                    "title"         => $sms_title,
                    "content"       => $sms_msg,
                    "cinfo"         => $c_info,
                );
            }
        }

        if(!empty($send_data_list)){
            $message_model = Message::Factory();
            $message_model->sendMessage($send_data_list);
        }
    }

    exit("<script>alert('재고가 반영 되었습니다.');location.href='product_cms_stock_list.php?sch_file_no={$file_no}';</script>");
}

?>
