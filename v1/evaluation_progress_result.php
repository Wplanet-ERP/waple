<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/evaluation.php');
require('inc/model/Evaluation.php');
require('inc/model/Staff.php');

# Model Init
$staff_model 	    = Staff::Factory();
$ev_system_model 	= Evaluation::Factory();

# 평가하기 시작
$ev_no		    = isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$evaluator_s_no = isset($_GET['evaluator_s_no']) ? $_GET['evaluator_s_no'] : "";

if(empty($ev_no)){
    exit("<script type='text/javascript'>alert('선택된 평가지가 없습니다.');window.close();</script>");
}

$ev_system_item		= $ev_system_model->getItem($ev_no);
$cur_date           = date("Y-m-d");

if(empty($ev_system_item)){
    exit("<script type='text/javascript'>alert('없는 평가지입니다.');window.close();</script>");
}

if(!($ev_system_item['admin_s_no'] == $session_s_no || permissionNameCheck($session_permission, "대표") || $session_s_no == '28'))
{
    if($ev_system_item['ev_state'] == '1'){
        echo "아직 평가일이 아닙니다.";
        exit;
    }elseif($ev_system_item['ev_state'] == '3' || $ev_system_item['ev_state'] == '4'){
        echo "평가기간이 종료되었습니다.";
        exit;
    }    elseif($ev_system_item['ev_state'] != '2'){
        echo "평가 진행중이 아닙니다.";
        exit;
    }

    if(strtotime($ev_system_item['ev_s_date']) > strtotime($cur_date)){
        echo "아직 평가일이 아닙니다.";
        exit;
    }elseif(strtotime($ev_system_item['ev_e_date']) < strtotime($cur_date)){
        echo "평가기간이 종료되었습니다.";
        exit;
    }
}

$evaluator_staff = $staff_model->getStaff($evaluator_s_no);

$smarty->assign("ev_no", $ev_no);
$smarty->assign("evaluator_staff", $evaluator_staff);
$smarty->assign("evaluation_system", $ev_system_item);

# 평가지 문항
$evaluation_unit_sql    = "SELECT * FROM evaluation_unit WHERE ev_u_set_no='{$ev_system_item['ev_u_set_no']}' AND active='1' AND evaluation_state NOT IN(99,100) ORDER BY `order`, page ASC";
$evaluation_unit_query  = mysqli_query($my_db, $evaluation_unit_sql);
$ev_unit_question_list  = [];
$page_idx               = 0;
$page_chk               = 1;
while($evaluation_unit = mysqli_fetch_array($evaluation_unit_query))
{
    if($page_chk == $evaluation_unit['page']){
        $page_idx++;
    }else{
        $page_chk = $evaluation_unit['page'];
        $page_idx = 1;
    }

    $ev_unit_question_list[$evaluation_unit['ev_u_no']] =
        array(
            'subject'   => $evaluation_unit['page']."_".$page_idx,
            "type"      => $evaluation_unit['evaluation_state'],
            "question"  => $evaluation_unit['question']
        );
}

# 평가 시스템 관계 쿼리
$evaluation_relation_sql = "
    SELECT 
        esr.ev_r_no,
        esr.receiver_s_no,
        esr.ev_u_no,
        esr.evaluation_value,
        (SELECT s_name FROM staff s where s.s_no=esr.receiver_s_no) as rec_s_name
    FROM evaluation_system_result esr
    LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
    WHERE esr.ev_no='{$ev_no}' AND esr.evaluator_s_no = '{$session_s_no}' AND er.rec_is_review='1' AND er.eval_is_review='1'
    ORDER BY esr.ev_r_no
";
$evaluation_relation_query  = mysqli_query($my_db, $evaluation_relation_sql);
$evaluation_result_list     = [];
while($evaluation_relation = mysqli_fetch_assoc($evaluation_relation_query))
{
    if(!isset($evaluation_result_list[$evaluation_relation['receiver_s_no']]))
    {
        $evaluation_result_list[$evaluation_relation['receiver_s_no']] = array(
            "rec_name"  => $evaluation_relation['rec_s_name'],
            "ev_r_no"   => $evaluation_relation['ev_r_no'],
        );

        foreach($ev_unit_question_list as $ev_u_no => $subject)
        {
            $evaluation_result_list[$evaluation_relation['receiver_s_no']][$ev_u_no] = 0;
        }
    }

    $evaluation_result_list[$evaluation_relation['receiver_s_no']][$evaluation_relation['ev_u_no']] = $evaluation_relation['evaluation_value'];
}

$smarty->assign("total_td_cnt", count($ev_unit_question_list)+1);
$smarty->assign("ev_unit_question_list", $ev_unit_question_list);
$smarty->assign("evaluation_result_list", $evaluation_result_list);

$smarty->display('evaluation_progress_result.html');

?>
