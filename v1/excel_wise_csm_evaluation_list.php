<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/wise_csm.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "no")
	->setCellValue('B1', "작성일")
	->setCellValue('C1', "종류")
	->setCellValue('D1', "1.고객센터의 온라인상담과 ARS 상담 서비스에 만족 하셨나요?")
	->setCellValue('E1', "2.상담시 안내 받으신 정보가 원하시는 정확한 안내가 되셨나요?")
	->setCellValue('F1', "3.베라베프 상담채널을 다른사람에게 추천할 의향이 있으신가요?")
	->setCellValue('G1', "4.상담 요청후 상담사와 연결까지 소요시간이 적절하였습니까?")
	->setCellValue('H1', "평균점수")
	->setCellValue('I1', "5.베라베프 고객센터를 이용하면서 좋았던 점이나 바라는 점이 있다면 무엇인가요?")
	->setCellValue('J1', "피평가자")
;


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where          = "1=1";
$sch_csm_no         = isset($_GET['sch_csm_no']) ? $_GET['sch_csm_no'] : "";
$sch_regdate        = isset($_GET['sch_regdate']) ? $_GET['sch_regdate'] : "";
$sch_ev_type        = isset($_GET['sch_ev_type']) ? $_GET['sch_ev_type'] : "";
$sch_question_type  = isset($_GET['sch_question_type']) ? $_GET['sch_question_type'] : "1";
$sch_s_score        = isset($_GET['sch_s_score']) ? $_GET['sch_s_score'] : "1";
$sch_e_score        = isset($_GET['sch_e_score']) ? $_GET['sch_e_score'] : "5";
$sch_q5             = isset($_GET['sch_q5']) ? $_GET['sch_q5'] : "";
$sch_q5_not_null    = isset($_GET['sch_q5_not_null']) ? $_GET['sch_q5_not_null'] : "";
$sch_s_no           = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";

if(!empty($sch_csm_no))
{
	$add_where .= " AND `ce`.csm_e_no = '{$sch_csm_no}'";
}

if(!empty($sch_regdate))
{
	$add_where .= " AND `ce`.regdate = '{$sch_regdate}'";
}

if(!empty($sch_ev_type))
{
	if($sch_ev_type == '2'){
		$add_where .= " AND `ce`.type = '1'";
	}elseif($sch_ev_type == '3'){
		$add_where .= " AND `ce`.type = '2'";
	}
}

if(!empty($sch_q5))
{
	$add_where .= " AND `ce`.q5_text like '%{$sch_q5}%'";
}

if(!empty($sch_q5_not_null))
{
	$add_where .= " AND `ce`.q5_text != ''";
}

if(!empty($sch_s_no))
{
	$add_where .= " AND `ce`.s_no = '{$sch_s_no}'";
}

switch($sch_question_type)
{
	case 1:
		$add_where .= " AND ((`ce`.q1_score >= {$sch_s_score} AND `ce`.q1_score <= {$sch_e_score}) 
            OR (`ce`.q2_score >= {$sch_s_score} AND `ce`.q2_score <= {$sch_e_score})
            OR (`ce`.q3_score >= {$sch_s_score} AND `ce`.q3_score <= {$sch_e_score})
            OR (`ce`.q4_score >= {$sch_s_score} AND `ce`.q4_score <= {$sch_e_score}))
        ";
		break;
	case 2:
		$add_where .= " AND (`ce`.q1_score >= {$sch_s_score} AND `ce`.q1_score <= {$sch_e_score})";
		break;
	case 3:
		$add_where .= " AND (`ce`.q2_score >= {$sch_s_score} AND `ce`.q2_score <= {$sch_e_score})";
		break;
	case 4:
		$add_where .= " AND (`ce`.q3_score >= {$sch_s_score} AND `ce`.q3_score <= {$sch_e_score})";
		break;
	case 5:
		$add_where .= " AND (`ce`.q4_score >= {$sch_s_score} AND `ce`.q4_score <= {$sch_e_score})";
		break;
}

// 리스트 쿼리
$csm_ev_sql  = "
    SELECT 
        *,
        (SELECT s.s_name FROM staff s WHERE s.s_no = ce.s_no LIMIT 1) as s_name
    FROM csm_evaluation `ce`
    WHERE {$add_where}
    ORDER BY `ce`.csm_e_no DESC
";
$csm_ev_query = mysqli_query($my_db, $csm_ev_sql);
$evaluation_type_option = getEvaluationTypeOption();
for ($i = 2; $csm_ev = mysqli_fetch_assoc($csm_ev_query);  $i++)
{
	$score_avg = round((($csm_ev['q1_score'] + $csm_ev['q2_score'] + $csm_ev['q3_score'] +$csm_ev['q4_score'])/4), 1);
	$type_name = $evaluation_type_option[($csm_ev['type']+1)];

	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $csm_ev['csm_e_no'])
		->setCellValue('B'.$i, $csm_ev['regdate'])
		->setCellValue('C'.$i, $type_name)
		->setCellValue('D'.$i, $csm_ev['q1_score'])
		->setCellValue('E'.$i, $csm_ev['q2_score'])
		->setCellValue('F'.$i, $csm_ev['q3_score'])
		->setCellValue('G'.$i, $csm_ev['q4_score'])
		->setCellValue('H'.$i, $score_avg)
		->setCellValue('I'.$i, $csm_ev['q5_text'])
		->setCellValue('J'.$i, $csm_ev['s_name']);
}

if($i > 1)
	$i = $i-1;

$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$i)->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00c4bd97');

$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$i)->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('I2:I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(60);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
$objPHPExcel->getActiveSheet()->setTitle('고객만족도 평가 리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_고객만족도 평가 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
