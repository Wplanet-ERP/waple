<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', 3000);

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/product_cms.php');
require('inc/helper/product_cms_stock.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/CommerceOrder.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');

# Navigation & My Quick
$nav_prd_no  = "147";
$nav_title   = "재고자산수불부";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);


# 프로세스 처리
$process    = isset($_POST['process']) ? $_POST['process'] : "";

# 모델생성
$company_model  = Company::Factory();
$product_model  = ProductCmsUnit::Factory();
$stock_model    = ProductCmsStock::Factory();
$stock_model->setMainInit("product_cms_stock_confirm", "no");
$report_model   = ProductCmsStock::Factory();
$report_model->setStockReport();

if($process == "f_in_base_qty")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";
    $value  = str_replace(",","",trim($value));

    if (!$stock_model->update(array("no" => $no, "qty" => $value))) {
        echo "기초재고 저장에 실패 하였습니다.";
    }else{
        echo "기초재고 저장에 성공했습니다.";
    }
    exit;
}
elseif($process == "f_out_base_qty")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";
    $value  = str_replace(",","",trim($value));

    $end_confirm = $stock_model->getEndItem($no);

    if (!$stock_model->update(array("no" => $end_confirm['no'], "qty" => $value))) {
        echo "기말재고 저장에 실패 하였습니다.";
    }else{
        echo "기말재고 저장에 성공했습니다.";
    }
    exit;
}
elseif($process == "f_apply_out_point_stock")
{
    $chk_month          = (isset($_POST['chk_month'])) ? $_POST['chk_month'] : "";
    $chk_log_c_no       = (isset($_POST['chk_log_c_no'])) ? $_POST['chk_log_c_no'] : "";
    $search_url         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $chk_month_val      = date("Y-m", strtotime($chk_month));
    $chk_month_e_day    = date("t", strtotime($chk_month_val));
    $chk_month_s_date   = "{$chk_month_val}-01";
    $chk_month_e_date   = "{$chk_month_val}-{$chk_month_e_day}";
    $chk_stock_sql      = "SELECT *, SUM(stock_qty) as total_qty FROM product_cms_stock_report pcsr WHERE `state`='기간판매반출' AND log_c_no='{$chk_log_c_no}' AND (pcsr.regdate BETWEEN '{$chk_month_s_date}' AND '{$chk_month_e_date}') AND pcsr.confirm_state > 0 GROUP BY prd_unit";
    $chk_stock_query    = mysqli_query($my_db, $chk_stock_sql);
    $ins_data_list      = [];
    $report_date        = date("Y-m-d H:i:s");
    while($chk_stock = mysqli_fetch_assoc($chk_stock_query))
    {
        $ins_data_list[] = array(
            "report_type"   => "3",
            "regdate"       => $chk_month_e_date,
            "type"          => $chk_stock['type'],
            "is_order"      => $chk_stock['is_order'],
            "state"         => "판매반출",
            "confirm_state" => $chk_stock['confirm_state'],
            "brand"         => $chk_stock['brand'],
            "prd_kind"      => $chk_stock['prd_kind'],
            "prd_unit"      => $chk_stock['prd_unit'],
            "sup_c_no"      => $chk_stock['sup_c_no'],
            "log_c_no"      => $chk_stock['log_c_no'],
            "option_name"   => $chk_stock['option_name'],
            "sku"           => $chk_stock['sku'],
            "prd_name"      => $chk_stock['prd_name'],
            "stock_type"    => $chk_stock['stock_type'],
            "stock_qty"     => $chk_stock['total_qty'],
            "memo"          => $chk_stock['memo'],
            "reg_s_no"      => $session_s_no,
            "report_date"   => $report_date,
        );
    }

    if(!empty($ins_data_list)){
        if($report_model->multiInsert($ins_data_list)){
            exit("<script>alert('외부창고 판매반출 데이터를 적용시켰습니다.');location.href='product_cms_stock_receipt_list.php?{$search_url}';</script>");
        }
    }

    exit("<script>alert('외부창고 판매반출 데이터 적용에 실패했습니다.');location.href='product_cms_stock_receipt_list.php?{$search_url}';</script>");
}
elseif($process == "apply_commerce_price")
{
    $chk_month                  = (isset($_POST['price_month'])) ? $_POST['price_month'] : date("Y-m", strtotime("-1 months"));
    $search_url                 = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $chk_prev_month             = date("Y-m", strtotime("{$chk_month} -1 months"));
    $sch_move_date              = $chk_month;
    $change_single_unit_list    = getChangeSingleUnitList();
    $chk_single_unit_list       = array_keys($change_single_unit_list);
    $commerce_order_tmp_list    = [];
    $chk_confirm_list           = [];

    # 바꾸고자 하는 달 SKU 전체
    $chk_confirm_sql            = "
        SELECT
            pcsc.prd_unit,
            (SELECT sub.org_price FROM product_cms_stock_confirm as sub WHERE sub.base_mon='{$chk_prev_month}' AND sub.prd_unit=pcsc.prd_unit LIMIT 1) as parent_org_price
        FROM product_cms_stock_confirm as pcsc
        WHERE base_mon='{$sch_move_date}'
        GROUP BY prd_unit
    ";
    $chk_confirm_query = mysqli_query($my_db, $chk_confirm_sql);
    while($chk_confirm = mysqli_fetch_assoc($chk_confirm_query)){
        $chk_confirm_list[$chk_confirm['prd_unit']] = $chk_confirm['parent_org_price'];
    }

    $commerce_order_sql         = "
        SELECT 
            co.set_no,
            co.option_no,
            co.unit_price, 
            co.quantity,
            co.supply_price,
            co.total_price,
            co.vat,
            co.group,
            (SELECT SUM(sub.supply_price) FROM commerce_order sub WHERE sub.set_no=co.set_no AND sub.`group`=co.`group`) as sum_sup_price,
            (SELECT SUM(sub.price) FROM commerce_order_etc sub WHERE sub.set_no=co.set_no AND sub.group_no=co.`group`) as sum_etc_price,
            (SELECT g.usd_rate FROM commerce_order_group g WHERE g.set_no=co.set_no AND g.group_no=co.`group`) as group_usd_rate,
            `cos`.sup_loc_type
        FROM commerce_order `co`
        LEFT JOIN commerce_order_set `cos` ON `cos`.no=`co`.set_no
        WHERE DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1' AND (SELECT pcsr.confirm_state FROM product_cms_stock_report pcsr WHERE pcsr.ord_no=co.`no`) IN(1,2)
    ";
    $commerce_order_query = mysqli_query($my_db, $commerce_order_sql);
    while($commerce_order = mysqli_fetch_assoc($commerce_order_query))
    {
        $total_price    = $commerce_order['total_price'];
        $quantity       = $commerce_order['quantity'];

        if(isset($commerce_order_tmp_list[$commerce_order['option_no']])){
            $commerce_order_tmp_list[$commerce_order['option_no']]['total_price'] += $total_price;
            $commerce_order_tmp_list[$commerce_order['option_no']]['total_qty']   += $quantity;
        }else{
            $commerce_order_tmp_list[$commerce_order['option_no']] = array(
                "total_price"   => $total_price,
                "total_qty"     => $quantity
            );
        }
    }

    if(!empty($commerce_order_tmp_list))
    {
        foreach($commerce_order_tmp_list as $key => $comm_tmp)
        {
            $ord_qty    = $comm_tmp['total_qty'] > 0 ? $comm_tmp['total_qty'] : 1;
            $ord_price  = round($comm_tmp['total_price']/$ord_qty, 1);
            $chk_confirm_list[$key] = $ord_price;

            if(in_array($key, $chk_single_unit_list)){
                $new_key = $change_single_unit_list[$key]['option'];
                $chk_confirm_list[$new_key] = $ord_price;
            }
        }
    }

    $result = false;
    if(!empty($chk_confirm_list)){
        foreach($chk_confirm_list as $key => $org_price)
        {
            $org_price = round($org_price, 1);
            $upd_sql = "UPDATE product_cms_stock_confirm SET org_price='{$org_price}' WHERE prd_unit='{$key}' AND base_mon='{$sch_move_date}'";
            mysqli_query($my_db, $upd_sql);
        }

        $result = true;
    }

    if($result){
        exit("<script>alert('확정원가 적용했습니다.');location.href='product_cms_stock_receipt_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('확정원가 적용에 실패했습니다.');location.href='product_cms_stock_receipt_list.php?{$search_url}';</script>");
    }
}

# 검색
$add_confirm_where      = "1=1 AND rs.`base_type`='start'";
$add_report_where       = "1=1";
$add_confirm_rs_where   = "(log_except_stock IS NULL OR log_except_stock = 0)";
$sch_cur_month          = date('Y-m');
$sch_chk_month          = date('Y-m', strtotime("{$sch_cur_month} -1 months"));
$sch_staff_type         = isset($_GET['sch_staff_type']) ? $_GET['sch_staff_type'] : "1";
$sch_move_date          = isset($_GET['sch_move_date']) ? $_GET['sch_move_date'] : $sch_chk_month;
$sch_brand              = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_option_name        = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_unit_type          = isset($_GET['sch_unit_type']) ? $_GET['sch_unit_type'] : "";
$sch_log_c_no           = isset($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "";
$sch_warehouse          = isset($_GET['sch_warehouse']) ? $_GET['sch_warehouse'] : "";
$sch_warehouse_all      = isset($_GET['sch_warehouse_all']) ? $_GET['sch_warehouse_all'] : "";
$sch_sku                = isset($_GET['sch_sku']) ? $_GET['sch_sku'] : "";
$sch_not_empty          = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : 1;
$sch_is_commerce        = isset($_GET['sch_is_commerce']) ? $_GET['sch_is_commerce'] : "";
$sch_only_qty           = isset($_GET['sch_only_qty']) ? $_GET['sch_only_qty'] : "";
$sch_not_empty_except   = isset($_GET['sch_not_empty_except']) ? $_GET['sch_not_empty_except'] : "";
$check_sum              = isset($_GET['check_sum']) ? $_GET['check_sum'] : "";
$check_not_move_stock   = isset($_GET['check_not_move_stock']) ? $_GET['check_not_move_stock'] : "";
$search_url             = getenv("QUERY_STRING");
$sch_brand_url          = "";
$with_log_c_no          = "2809";
$change_sum_unit_list   = getChangeSubUnitList();

if(empty($search_url))
{
    if($sch_staff_type == 1){
        $sch_only_qty       = 1;
    } elseif($sch_staff_type == 2){

    } elseif($sch_staff_type == 3){
        $sch_warehouse_all  = 1;
        $sch_log_c_no       = $with_log_c_no;
    }
}

$smarty->assign('sch_staff_type', $sch_staff_type);
$smarty->assign('sch_not_empty', $sch_not_empty);
$smarty->assign('sch_warehouse_all', $sch_warehouse_all);
$smarty->assign('sch_only_qty', $sch_only_qty);
$smarty->assign('sch_not_empty_except', $sch_not_empty_except);
$smarty->assign('check_sum', $check_sum);
$smarty->assign('check_not_move_stock', $check_not_move_stock);
$smarty->assign("search_url", $search_url);

if(!empty($sch_brand)){
    $add_confirm_where  .= " AND rs.brand = '{$sch_brand}'";
    $smarty->assign("sch_brand", $sch_brand);

    $sch_brand_sql      = "SELECT c.brand as brand_g2, (SELECT k.k_parent FROM kind k WHERE k.k_name_code=c.brand) as brand_g1 FROM company `c` WHERE c.c_no='{$sch_brand}' LIMIT 1";
    $sch_brand_query    = mysqli_query($my_db, $sch_brand_sql);
    $sch_brand_result   = mysqli_fetch_assoc($sch_brand_query);
    $sch_brand_url      = "&sch_brand_g1={$sch_brand_result['brand_g1']}&sch_brand_g2={$sch_brand_result['brand_g2']}&sch_brand={$sch_brand}";
}
$smarty->assign("sch_brand_url", $sch_brand_url);

if(!empty($sch_option_name)){
    $add_confirm_where  .= " AND rs.option_name LIKE '%{$sch_option_name}%'";
    $smarty->assign("sch_option_name", $sch_option_name);
}

if(!empty($sch_unit_type)){
    $add_report_where       .= " AND pcsr.prd_unit IN(SELECT pcu.`no` FROM product_cms_unit pcu WHERE pcu.`type`= '{$sch_unit_type}')";
    $add_confirm_rs_where   .= " AND unit_type='{$sch_unit_type}'";
    $smarty->assign("sch_unit_type", $sch_unit_type);
}

if(!empty($sch_log_c_no)){
    $add_confirm_where  .= " AND rs.log_c_no = '{$sch_log_c_no}'";
    $add_report_where   .= " AND pcsr.log_c_no = '{$sch_log_c_no}'";
    $smarty->assign("sch_log_c_no", $sch_log_c_no);
}

if(!empty($sch_warehouse)){
    $add_confirm_where  .= " AND rs.warehouse LIKE '%{$sch_warehouse}%'";
    $smarty->assign("sch_warehouse", $sch_warehouse);
}

if(!empty($sch_sku)){
    $add_confirm_where  .= " AND rs.sku = '{$sch_sku}'";
    $smarty->assign("sch_sku", $sch_sku);
}

if(!empty($sch_move_date)){
    $add_confirm_where  .=  " AND rs.base_mon='{$sch_move_date}'";
    $smarty->assign("sch_move_date", $sch_move_date);
}

if(!empty($sch_is_commerce)){
    if($sch_is_commerce == '1'){
        $add_confirm_where  .=  " AND rs.prd_unit IN(SELECT DISTINCT co.option_no FROM commerce_order co LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no` WHERE cos.display='1' AND DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1')";
    }else{
        $add_confirm_where  .=  " AND rs.prd_unit NOT IN(SELECT DISTINCT co.option_no FROM commerce_order co LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no` WHERE cos.display='1' AND DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1')";
    }

    $smarty->assign("sch_is_commerce", $sch_is_commerce);
}

# 재고자산수불부 total 변수
$total_in_base_qty = $total_in_base_price = $total_out_base_qty = $total_out_base_price = 0;
$total_in_point_qty = $total_in_point_price = $total_in_move_qty = $total_in_move_price = $total_in_return_qty = $total_in_return_price = $total_in_etc_qty = $total_in_etc_price = $total_in_qty = $total_in_price = 0;
$total_out_point_qty = $total_out_point_price = $total_out_move_qty = $total_out_move_price = $total_out_return_qty = $total_out_return_price = $total_out_etc_qty = $total_out_etc_price = $total_out_qty = $total_out_price = $total_db_end_qty = 0;

# 회수리스트 처리
$return_list      = [];
$return_prd_sql   = "SELECT `option`, SUM(quantity) as qty FROM work_cms_return_unit WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_return wcr WHERE wcr.return_state='6' AND DATE_FORMAT(wcr.return_date,'%Y-%m')='{$sch_move_date}') AND `type` IN(1,2,3,4) GROUP BY `option`";
$return_prd_query = mysqli_query($my_db, $return_prd_sql);
while($return_prd = mysqli_fetch_assoc($return_prd_query))
{
    $return_list[$return_prd['option']] += $return_prd['qty'];
}

# 쿼리용 날짜
$sch_move_s_date    = $sch_move_date."-01";
$end_day            = date('t', strtotime($sch_move_date));
$sch_move_e_date    = $sch_move_date."-".$end_day;

# 재고이동 리스트
$stock_move_list   = [];
if($check_not_move_stock != "1")
{
    $stock_move_sql     = "SELECT prd_unit, in_warehouse, out_warehouse, in_qty, out_qty FROM product_cms_stock_transfer pcst WHERE (pcst.move_date BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}')";
    $stock_move_query   = mysqli_query($my_db, $stock_move_sql);
    while($stock_move = mysqli_fetch_assoc($stock_move_query))
    {
        $stock_move_list[$stock_move['prd_unit']][$stock_move['in_warehouse']]["in"]   += $stock_move['in_qty'];
        $stock_move_list[$stock_move['prd_unit']][$stock_move['out_warehouse']]["out"] += $stock_move['out_qty'];
    }
}


# 입고/반출 리스트
$stock_list         = [];
$stock_report_sql   = "SELECT * FROM product_cms_stock_report pcsr WHERE {$add_report_where} AND (pcsr.regdate BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}') AND pcsr.confirm_state > 0";
$stock_report_query = mysqli_query($my_db, $stock_report_sql);
$stock_dev_list     = [];
while($stock_report = mysqli_fetch_assoc($stock_report_query))
{
    $stock_kind     = "";
    $stock_dev_kind = "";
    switch($stock_report['confirm_state']){
        case "1":
        case "2":
            $stock_kind = "in_point";
            break;
        case "3":
            if($stock_report['state'] == "기간이동입고"){
                $stock_dev_kind = "in_move";
            }else{
                $stock_kind = "in_move";
            }
            break;
        case "4":
            $stock_kind = "in_etc";
            break;
        case "5":
            if($stock_report['state'] == "기간반품입고"){
                $stock_dev_kind = "in_return";
            }else{
                $stock_kind = "in_return";
            }
            break;
        case "6":
            if($stock_report['state'] == "기간이동반출"){
                $stock_dev_kind = "out_move";
            }else{
                $stock_kind = "out_move";
            }
            break;
        case "8":
            $stock_kind = "out_etc";
            break;
        case "9":
            if($stock_report['state'] == "기간판매반출"){
                $stock_dev_kind = "out_point";
            }else{
                $stock_kind = "out_point";
            }
            break;
        case "10":
            $stock_kind = "out_return";
            break;
        case "11":
            $stock_dev_kind = "in_base";
            break;
        case "12":
            $stock_dev_kind = "out_base";
            break;
    }

    if(!empty($stock_kind))
    {
        if(!isset($stock_list[$stock_report['prd_unit']][$stock_report['stock_type']])){
            $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']] = array(
                "in_point"      => 0,
                "in_move"       => 0,
                "in_etc"        => 0,
                "in_return"     => 0,
                "out_point"     => 0,
                "out_move"      => 0,
                "out_return"    => 0,
                "out_etc"       => 0,
            );
        }

        $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']][$stock_kind] += $stock_report['stock_qty'];
    }

    if(!empty($stock_dev_kind))
    {
        if(!isset($stock_dev_list[$stock_report['prd_unit']][$stock_report['stock_type']])){
            $stock_dev_list[$stock_report['prd_unit']][$stock_report['stock_type']] = array(
                "in_base"   => 0,
                "out_base"  => 0,
                "in_move"   => 0,
                "in_return" => 0,
                "out_move"  => 0,
                "out_point" => 0,
            );
        }
        $stock_dev_list[$stock_report['prd_unit']][$stock_report['stock_type']][$stock_dev_kind] += $stock_report['stock_qty'];
    }
}

# 실시간 확정원가
$commerce_order_list        = [];
$commerce_order_tmp_list    = [];
$commerce_order_sql         = "
    SELECT 
        co.set_no,
        co.option_no,
        co.unit_price, 
        co.quantity,
        co.supply_price,
        co.total_price,
        co.vat,
        co.group,
        (SELECT SUM(sub.supply_price) FROM commerce_order sub WHERE sub.set_no=co.set_no AND sub.`group`=co.`group`) as sum_sup_price,
        (SELECT SUM(sub.price) FROM commerce_order_etc sub WHERE sub.set_no=co.set_no AND sub.group_no=co.`group`) as sum_etc_price,
        (SELECT g.usd_rate FROM commerce_order_group g WHERE g.set_no=co.set_no AND g.group_no=co.`group`) as group_usd_rate,
        `cos`.sup_loc_type
    FROM commerce_order `co`
    LEFT JOIN commerce_order_set `cos` ON `cos`.no=`co`.set_no
    WHERE DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1' AND (SELECT pcsr.confirm_state FROM product_cms_stock_report pcsr WHERE pcsr.ord_no=co.`no`) IN(1,2)
";
$commerce_order_query = mysqli_query($my_db, $commerce_order_sql);
while($commerce_order = mysqli_fetch_assoc($commerce_order_query))
{
    $total_price    = $commerce_order['total_price'];
    $quantity       = $commerce_order['quantity'];

    if(isset($commerce_order_tmp_list[$commerce_order['option_no']])){
        $commerce_order_tmp_list[$commerce_order['option_no']]['total_price'] += $total_price;
        $commerce_order_tmp_list[$commerce_order['option_no']]['total_qty']   += $quantity;
    }else{
        $commerce_order_tmp_list[$commerce_order['option_no']] = array(
            "total_price"   => $total_price,
            "total_qty"     => $quantity
        );
    }
}

if(!empty($commerce_order_tmp_list))
{
    foreach($commerce_order_tmp_list as $key => $comm_tmp)
    {
        $ord_qty = $comm_tmp['total_qty'] > 0 ? $comm_tmp['total_qty'] : 1;
        $commerce_order_list[$key] = round($comm_tmp['total_price']/$ord_qty, 1);
    }
}


# 재고자산수불부 쿼리
$unit_type_option    = getUnitTypeOption();
$is_sum_view_qty     = ($sch_move_date < $sch_chk_month) ? true : false;
$product_receipt_sql = "
    SELECT
        *
    FROM 
    (
        SELECT
            *,
            (SELECT pcu.`type` FROM product_cms_unit pcu WHERE pcu.`no`=rs.prd_unit LIMIT 1) as unit_type,
            (SELECT pcum.is_except_stock FROM product_cms_unit_management pcum WHERE pcum.prd_unit=rs.prd_unit AND pcum.log_c_no=rs.log_c_no LIMIT 1) as log_except_stock
        FROM product_cms_stock_confirm as rs
        WHERE {$add_confirm_where}
    ) as rs
    WHERE {$add_confirm_rs_where} 
    ORDER BY rs.option_name ASC, rs.warehouse ASC
";
$product_receipt_query      = mysqli_query($my_db, $product_receipt_sql);
$product_receipt_list       = [];
$product_receipt_tmp_list   = [];
$product_unit_data_list     = [];
while($product_receipt = mysqli_fetch_assoc($product_receipt_query))
{
    $stock_list_data                        = isset($stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];
    $stock_dev_data                         = isset($stock_dev_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_dev_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];
    $move_list_data                         = isset($stock_move_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_move_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];
    $live_org_price                         = isset($commerce_order_list[$product_receipt['prd_unit']]) ? $commerce_order_list[$product_receipt['prd_unit']] : 0;

    if($check_sum == "1" && isset($change_sum_unit_list[$product_receipt['prd_unit']]))
    {
        $chk_prd_unit               =  $change_sum_unit_list[$product_receipt['prd_unit']]['option'];
        $chk_prd_sku                =  $change_sum_unit_list[$product_receipt['prd_unit']]['sku'];

        $live_org_price                 = $commerce_order_list[$chk_prd_unit];
        $product_receipt["prd_key"]     = str_replace($product_receipt['prd_unit'], $chk_prd_unit, $product_receipt['prd_key']);
        $product_receipt["option_name"] = $chk_prd_sku;
        $product_receipt["sku"]         = $chk_prd_sku;
    }

    $product_receipt['unit_type_name']      = $unit_type_option[$product_receipt['unit_type']];
    $product_receipt['in_base_qty']         = $product_receipt['qty'];
    $product_receipt['in_base_qty_dev']     = !empty($stock_dev_data) ? $stock_dev_data['in_base'] : 0;
    $product_receipt['in_base_qty_diff']    = $product_receipt['in_base_qty_dev']-$product_receipt['in_base_qty'];
    $product_receipt['in_point_qty']        = !empty($stock_list_data) ? $stock_list_data['in_point'] : 0;
    $product_receipt['in_move_sub_qty']     = !empty($stock_list_data) ? $stock_list_data['in_move'] : 0;
    $product_receipt['in_org_move_qty']     = !empty($move_list_data) && isset($move_list_data["in"]) ? $move_list_data["in"] : 0;
    $product_receipt['in_move_qty']         = $product_receipt['in_org_move_qty'] + $product_receipt['in_move_sub_qty'];
    $product_receipt['in_move_qty_dev']     = !empty($stock_dev_data) ? $stock_dev_data['in_move'] : 0;
    $product_receipt['in_move_qty_diff']    = $product_receipt['in_move_qty_dev']-$product_receipt['in_move_qty'];
    $product_receipt['in_etc_qty']          = !empty($stock_list_data) ? $stock_list_data['in_etc'] : 0;
    $product_receipt['in_return_sub_qty']   = ($product_receipt['warehouse'] == "2-3 검수대기창고(반품건)" && isset($return_list[$product_receipt['prd_unit']])) ? $return_list[$product_receipt['prd_unit']] : 0;
    $product_receipt['in_return_org_qty']   = !empty($stock_list_data) ? $stock_list_data['in_return'] : 0;
    $product_receipt['in_return_qty']       = $product_receipt['in_return_org_qty'] + $product_receipt['in_return_sub_qty'];
    $product_receipt['in_return_qty_dev']   = !empty($stock_dev_data) ? $stock_dev_data['in_return'] : 0;
    $product_receipt['in_return_qty_diff']  = $product_receipt['in_return_qty_dev']-$product_receipt['in_return_qty'];

    $product_receipt['live_org_price']      = $live_org_price;
    $product_receipt['in_base_price']       = $product_receipt['base_price'] * $product_receipt['in_base_qty'];
    $product_receipt['in_point_price']      = $product_receipt['org_price'] * $product_receipt['in_point_qty'];
    $product_receipt['in_move_price']       = $product_receipt['avg_price'] * $product_receipt['in_move_qty'];
    $product_receipt['in_return_price']     = $product_receipt['org_price'] * $product_receipt['in_return_qty'];
    $product_receipt['in_etc_price']        = $product_receipt['org_price'] * $product_receipt['in_etc_qty'];
    $product_receipt['in_qty']              = $product_receipt['in_point_qty'] + $product_receipt['in_move_qty'] + $product_receipt['in_return_qty'] + $product_receipt['in_etc_qty'];
    $product_receipt['in_price']            = $product_receipt['in_point_price'] + $product_receipt['in_move_price'] + $product_receipt['in_return_price'] + $product_receipt['in_etc_price'];

    $product_receipt['out_point_qty']       = !empty($stock_list_data) ? $stock_list_data['out_point']*-1 : 0;
    $product_receipt['out_point_qty_dev']   = !empty($stock_dev_data) ? $stock_dev_data['out_point']*-1 : 0;
    $product_receipt['out_point_qty_diff']  = $product_receipt['out_point_qty_dev']-$product_receipt['out_point_qty'];
    $product_receipt['out_org_move_qty']    = !empty($move_list_data) && isset($move_list_data["out"]) ? $move_list_data["out"] : 0;
    $product_receipt['out_move_sub_qty']    = !empty($stock_list_data) ? $stock_list_data['out_move']*-1 : 0;
    $product_receipt['out_move_qty']        = $product_receipt['out_org_move_qty'] + $product_receipt['out_move_sub_qty'];
    $product_receipt['out_move_qty_dev']    = !empty($stock_dev_data) ? $stock_dev_data['out_move']*-1 : 0;
    $product_receipt['out_move_qty_diff']   = $product_receipt['out_move_qty_dev']-$product_receipt['out_move_qty'];

    $product_receipt['out_return_qty']      = !empty($stock_list_data) ? $stock_list_data['out_return']*-1 : 0;
    $product_receipt['out_etc_qty']         = !empty($stock_list_data) ? $stock_list_data['out_etc']*-1 : 0;
    $product_receipt['out_qty']             = $product_receipt['out_point_qty'] + $product_receipt['out_move_qty'] + $product_receipt['out_return_qty'] + $product_receipt['out_etc_qty'];
    $product_receipt['out_base_qty']        = $product_receipt['in_base_qty'] + $product_receipt['in_qty'] - $product_receipt['out_qty'];
    $product_receipt['out_base_qty_dev']    = !empty($stock_dev_data) ? $stock_dev_data['out_base'] : 0;
    $product_receipt['out_base_qty_diff']   = $product_receipt['out_base_qty_dev']-$product_receipt['out_base_qty'];

    # 판매반출 금액 계산
    $sales_price                            = ($product_receipt['in_base_qty']+$product_receipt['in_qty'] > 0) ? (($product_receipt['in_base_price']+$product_receipt['in_price']) / ($product_receipt['in_base_qty']+$product_receipt['in_qty'])) : 0;
    $product_receipt['out_point_price']     = $sales_price * $product_receipt['out_point_qty'];
    $product_receipt['out_move_price']      = $product_receipt['avg_price'] * $product_receipt['out_move_qty'];
    $product_receipt['out_return_price']    = $sales_price * $product_receipt['out_return_qty'];
    $product_receipt['out_etc_price']       = $sales_price * $product_receipt['out_etc_qty'];
    $product_receipt['out_price']           = $product_receipt['out_point_price'] + $product_receipt['out_move_price'] + $product_receipt['out_return_price'] + $product_receipt['out_etc_price'];
    $product_receipt['out_base_price']      = $product_receipt['in_base_price'] + $product_receipt['in_price'] - $product_receipt['out_price'];

    if($sch_not_empty_except == '1' && ($product_receipt['in_qty'] == 0 && $product_receipt['out_qty'] == 0)){
        continue;
    }
    elseif($sch_not_empty == '1' && (
            ($product_receipt['in_base_price'] == 0 && $product_receipt['in_base_qty'] == 0)
            && ($product_receipt['in_point_price'] == 0 && $product_receipt['in_point_qty'] == 0)
            && ($product_receipt['in_move_price'] == 0 && $product_receipt['in_move_qty'] == 0 && $product_receipt['in_move_qty_dev'] == 0)
            && ($product_receipt['in_return_price'] == 0 && $product_receipt['in_return_qty'] == 0)
            && ($product_receipt['in_etc_price'] == 0 && $product_receipt['in_etc_qty'] == 0)
            && ($product_receipt['in_price'] == 0 && $product_receipt['in_qty'] == 0)
            && ($product_receipt['out_point_price'] == 0 && $product_receipt['out_point_qty'] == 0)
            && ($product_receipt['out_move_price'] == 0 && $product_receipt['out_move_qty'] == 0 && $product_receipt['out_move_sub_qty'] == 0)
            && ($product_receipt['out_return_price'] == 0 && $product_receipt['out_return_qty'] == 0)
            && ($product_receipt['out_etc_price'] == 0 && $product_receipt['out_etc_qty'] == 0)
            && ($product_receipt['out_price'] == 0 && $product_receipt['out_qty']  == 0)
            && ($product_receipt['out_base_price'] == 0 && $product_receipt['out_base_qty']  == 0)
        )
    ){
        continue;
    }

    if($sch_warehouse_all == "1")
    {
        $sum_prd_unit = ($check_sum == "1" && isset($change_sum_unit_list[$product_receipt['prd_unit']])) ? $change_sum_unit_list[$product_receipt['prd_unit']]['option'] : $product_receipt['prd_unit'];

        if(!isset($product_receipt_list[$sum_prd_unit]))
        {
            $product_receipt_list[$sum_prd_unit] = array(
                "no"                => $product_receipt['no'],
                "prd_key"           => $product_receipt['prd_key'],
                "brand_name"        => $product_receipt['brand_name'],
                "option_name"       => $product_receipt["option_name"],
                "warehouse"         => "통합창고",
                "unit_type"         => $product_receipt['unit_type'],
                "unit_type_name"    => $unit_type_option[$product_receipt['unit_type']],
                "log_c_no"          => $product_receipt["log_c_no"],
                "log_company"       => $product_receipt["log_company"],
                "sku"               => $product_receipt["sku"],
                "sch_sku"           => urlencode($product_receipt["sku"]),
                "live_org_price"    => $live_org_price,
                "org_price"         => $product_receipt["org_price"],
                "in_base_qty"       => 0,
                "in_base_price"     => 0,
                "in_point_qty"      => 0,
                "in_point_price"    => 0,
                "in_move_qty"       => 0,
                "in_org_move_qty"   => 0,
                "in_move_sub_qty"   => 0,
                "in_move_price"     => 0,
                "in_return_qty"     => 0,
                "in_return_price"   => 0,
                "in_etc_qty"        => 0,
                "in_etc_price"      => 0,
                "in_qty"            => 0,
                "in_price"          => 0,
                "out_point_qty"     => 0,
                "out_point_price"   => 0,
                "out_move_qty"      => 0,
                "out_org_move_qty"  => 0,
                "out_move_sub_qty"  => 0,
                "out_move_price"    => 0,
                "out_return_qty"    => 0,
                "out_return_price"  => 0,
                "out_etc_qty"       => 0,
                "out_etc_price"     => 0,
                "out_qty"           => 0,
                "out_price"         => 0,
                "out_base_qty"      => 0,
                "out_base_price"    => 0,
            );
        }
        $product_receipt_list[$sum_prd_unit]["in_base_qty"]      += $product_receipt["in_base_qty"];
        $product_receipt_list[$sum_prd_unit]["in_base_price"]    += $product_receipt["in_base_price"];
        $product_receipt_list[$sum_prd_unit]["in_point_qty"]     += $product_receipt["in_point_qty"];
        $product_receipt_list[$sum_prd_unit]["in_point_price"]   += $product_receipt["in_point_price"];
        $product_receipt_list[$sum_prd_unit]["in_move_qty"]      += $product_receipt["in_move_qty"];
        $product_receipt_list[$sum_prd_unit]["in_org_move_qty"]  += $product_receipt["in_org_move_qty"];
        $product_receipt_list[$sum_prd_unit]["in_move_sub_qty"]  += $product_receipt["in_move_sub_qty"];
        $product_receipt_list[$sum_prd_unit]["in_move_price"]    += $product_receipt["in_move_price"];
        $product_receipt_list[$sum_prd_unit]["in_return_qty"]    += $product_receipt["in_return_qty"];
        $product_receipt_list[$sum_prd_unit]["in_return_price"]  += $product_receipt["in_return_price"];
        $product_receipt_list[$sum_prd_unit]["in_etc_qty"]       += $product_receipt["in_etc_qty"];
        $product_receipt_list[$sum_prd_unit]["in_etc_price"]     += $product_receipt["in_etc_price"];
        $product_receipt_list[$sum_prd_unit]["in_qty"]           += $product_receipt["in_qty"];
        $product_receipt_list[$sum_prd_unit]["in_price"]         += $product_receipt["in_price"];
        $product_receipt_list[$sum_prd_unit]["out_point_qty"]    += $product_receipt["out_point_qty"];
        $product_receipt_list[$sum_prd_unit]["out_point_price"]  += $product_receipt["out_point_price"];
        $product_receipt_list[$sum_prd_unit]["out_move_qty"]     += $product_receipt["out_move_qty"];
        $product_receipt_list[$sum_prd_unit]["out_org_move_qty"] += $product_receipt["out_org_move_qty"];
        $product_receipt_list[$sum_prd_unit]["out_move_sub_qty"] += $product_receipt["out_move_sub_qty"];
        $product_receipt_list[$sum_prd_unit]["out_move_price"]   += $product_receipt["out_move_price"];
        $product_receipt_list[$sum_prd_unit]["out_return_qty"]   += $product_receipt["out_return_qty"];
        $product_receipt_list[$sum_prd_unit]["out_return_price"] += $product_receipt["out_return_price"];
        $product_receipt_list[$sum_prd_unit]["out_etc_qty"]      += $product_receipt["out_etc_qty"];
        $product_receipt_list[$sum_prd_unit]["out_etc_price"]    += $product_receipt["out_etc_price"];
        $product_receipt_list[$sum_prd_unit]["out_qty"]          += $product_receipt["out_qty"];
        $product_receipt_list[$sum_prd_unit]["out_price"]        += $product_receipt["out_price"];
        $product_receipt_list[$sum_prd_unit]["out_base_qty"]     += $product_receipt["out_base_qty"];
        $product_receipt_list[$sum_prd_unit]["out_base_price"]   += $product_receipt["out_base_price"];
    }
    else
    {
        $product_receipt["sch_sku"] = urlencode($product_receipt["sku"]);
        $product_receipt_tmp_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] = $product_receipt;
    }

    $total_in_base_qty      += $product_receipt['in_base_qty'];
    $total_in_base_price    += $product_receipt['in_base_price'];
    $total_in_point_qty     += $product_receipt['in_point_qty'];
    $total_in_point_price   += $product_receipt['in_point_price'];
    $total_in_move_qty      += $product_receipt['in_move_qty'];
    $total_in_move_price    += $product_receipt['in_move_price'];
    $total_in_return_qty    += $product_receipt['in_return_qty'];
    $total_in_return_price  += $product_receipt['in_return_price'];
    $total_in_etc_qty       += $product_receipt['in_etc_qty'];
    $total_in_etc_price     += $product_receipt['in_etc_price'];
    $total_in_qty           += $product_receipt['in_qty'];
    $total_in_price         += $product_receipt['in_price'];
    $total_out_point_qty    += $product_receipt['out_point_qty'];
    $total_out_point_price  += $product_receipt['out_point_price'];
    $total_out_move_qty     += $product_receipt['out_move_qty'];
    $total_out_move_price   += $product_receipt['out_move_price'];
    $total_out_return_qty   += $product_receipt['out_return_qty'];
    $total_out_return_price += $product_receipt['out_return_price'];
    $total_out_etc_qty      += $product_receipt['out_etc_qty'];
    $total_out_etc_price    += $product_receipt['out_etc_price'];
    $total_out_price        += $product_receipt['out_price'];
    $total_out_qty          += $product_receipt['out_qty'];

    if($is_sum_view_qty){
        $chk_end_qty_sql    = "SELECT qty FROM product_cms_stock_confirm WHERE base_mon='{$sch_move_date}' AND base_type='end' AND prd_unit='{$product_receipt['prd_unit']}' AND warehouse='{$product_receipt['warehouse']}'";
        $chk_end_qty_query  = mysqli_query($my_db, $chk_end_qty_sql);
        $chk_end_qty_result = mysqli_fetch_assoc($chk_end_qty_query);
        $total_db_end_qty  += isset($chk_end_qty_result['qty']) ? $chk_end_qty_result['qty'] : 0;
    }
}

if(!empty($product_receipt_tmp_list))
{
    $product_receipt_convert_list = [];
    foreach($product_receipt_tmp_list as $prd_unit => $unit_data)
    {
        foreach($unit_data as $warehouse => $warehouse_data)
        {
            if($check_sum == "1")
            {
                $chk_prd_unit       = isset($change_sum_unit_list[$prd_unit]) ? $change_sum_unit_list[$prd_unit]['option'] : $prd_unit;
                $chk_convert_data   = $product_receipt_tmp_list[$chk_prd_unit][$warehouse];

                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["no"]              = isset($chk_convert_data['no']) && !empty($chk_convert_data['no']) ? $chk_convert_data["no"] : $warehouse_data["no"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["prd_key"]         = isset($chk_convert_data['prd_key']) && !empty($chk_convert_data['prd_key']) ? $chk_convert_data["prd_key"] : $warehouse_data["prd_key"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["warehouse"]       = isset($chk_convert_data['warehouse']) && !empty($chk_convert_data['warehouse']) ? $chk_convert_data["warehouse"] : $warehouse_data["warehouse"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["brand_name"]      = isset($chk_convert_data['brand_name']) && !empty($chk_convert_data['brand_name']) ? $chk_convert_data["brand_name"] : $warehouse_data["brand_name"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["option_name"]     = isset($chk_convert_data['option_name']) && !empty($chk_convert_data['option_name']) ? $chk_convert_data["option_name"] : $warehouse_data["option_name"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["unit_type"]       = isset($chk_convert_data['unit_type']) && !empty($chk_convert_data['unit_type']) ? $chk_convert_data["unit_type"] : $warehouse_data["unit_type"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["unit_type_name"]  = isset($chk_convert_data['unit_type_name']) && !empty($chk_convert_data['unit_type_name']) ? $chk_convert_data["unit_type_name"] : $warehouse_data["unit_type_name"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["log_c_no"]        = isset($chk_convert_data['log_c_no']) && !empty($chk_convert_data['log_c_no']) ? $chk_convert_data["log_c_no"] : $warehouse_data["log_c_no"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["log_company"]     = isset($chk_convert_data['log_company']) && !empty($chk_convert_data['log_company']) ? $chk_convert_data["log_company"] : $warehouse_data["log_company"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["sku"]             = isset($chk_convert_data['sku']) && !empty($chk_convert_data['sku']) ? $chk_convert_data["sku"] : $warehouse_data["sku"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["sch_sku"]         = isset($chk_convert_data['sch_sku']) && !empty($chk_convert_data['sch_sku']) ? $chk_convert_data["sch_sku"] : $warehouse_data["sch_sku"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["live_org_price"]  = isset($chk_convert_data['live_org_price']) && !empty($chk_convert_data['live_org_price']) ? $chk_convert_data["live_org_price"] : $warehouse_data["live_org_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["org_price"]       = isset($chk_convert_data['org_price']) && !empty($chk_convert_data['org_price']) ? $chk_convert_data["org_price"] : $warehouse_data["org_price"];

                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_base_qty"]      += $warehouse_data["in_base_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_base_price"]    += $warehouse_data["in_base_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_point_qty"]     += $warehouse_data["in_point_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_point_price"]   += $warehouse_data["in_point_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_move_qty"]      += $warehouse_data["in_move_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_org_move_qty"]  += $warehouse_data["in_org_move_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_move_sub_qty"]  += $warehouse_data["in_move_sub_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_move_price"]    += $warehouse_data["in_move_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_return_qty"]    += $warehouse_data["in_return_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_return_price"]  += $warehouse_data["in_return_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_etc_qty"]       += $warehouse_data["in_etc_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_etc_price"]     += $warehouse_data["in_etc_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_qty"]           += $warehouse_data["in_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["in_price"]         += $warehouse_data["in_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_point_qty"]    += $warehouse_data["out_point_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_point_price"]  += $warehouse_data["out_point_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_move_qty"]     += $warehouse_data["out_move_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_org_move_qty"] += $warehouse_data["out_org_move_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_move_sub_qty"] += $warehouse_data["out_move_sub_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_move_price"]   += $warehouse_data["out_move_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_return_qty"]   += $warehouse_data["out_return_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_return_price"] += $warehouse_data["out_return_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_etc_qty"]      += $warehouse_data["out_etc_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_etc_price"]    += $warehouse_data["out_etc_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_qty"]          += $warehouse_data["out_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_price"]        += $warehouse_data["out_price"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_base_qty"]     += $warehouse_data["out_base_qty"];
                $product_receipt_convert_list[$chk_prd_unit][$warehouse]["out_base_price"]   += $warehouse_data["out_base_price"];
            }
            else
            {
                $product_receipt_list[] = $warehouse_data;
            }
        }
    }

    if($check_sum == "1" && !empty($product_receipt_convert_list)){
        foreach($product_receipt_convert_list as $prd_unit => $convert_data)
        {
            foreach($convert_data as $warehouse => $conv_warehouse_data)
            {
                $product_receipt_list[] = $conv_warehouse_data;
            }
        }
    }
}


$ord_type       = isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";

if(!empty($ord_type) && !empty($ord_type_by))
{
    if($ord_type_by == "1"){
        $product_receipt_list = array_sort($product_receipt_list, $ord_type);
    }elseif($ord_type_by == "2"){
        $product_receipt_list = array_rsort($product_receipt_list, $ord_type);
    }
}
$smarty->assign("ord_type", $ord_type);
$smarty->assign("ord_type_by", $ord_type_by);

if($is_sum_view_qty){
    $total_out_base_qty = $total_db_end_qty;
}else{
    $total_out_base_qty = $total_in_base_qty+$total_in_qty-$total_out_qty;
}
$total_out_base_price   = $total_in_base_price+$total_in_price-$total_out_price;

$sch_log_company_list   = $company_model->getLogisticsList();
$log_company_total_list = $company_model->getReceiptLogisticsList();
$log_company_list       = $log_company_total_list[1];
$contract_company_list  = $log_company_total_list[2];
$sch_log_c_name         = !empty($sch_log_c_no) ? $sch_log_company_list[$sch_log_c_no] : "";

# 마지막 재고자산수불부 달
$last_receipt_month_sql     = "SELECT MAX(base_mon) as max_month FROM product_cms_stock_confirm";
$last_receipt_month_query   = mysqli_query($my_db, $last_receipt_month_sql);
$last_receipt_month_result  = mysqli_fetch_assoc($last_receipt_month_query);
$last_receipt_month         = $last_receipt_month_result['max_month'];

$smarty->assign("sch_log_c_name", $sch_log_c_name);
$smarty->assign("total_in_base_qty", $total_in_base_qty);
$smarty->assign("total_in_base_price", $total_in_base_price);
$smarty->assign("total_in_point_qty", $total_in_point_qty);
$smarty->assign("total_in_point_price", $total_in_point_price);
$smarty->assign("total_in_move_qty", $total_in_move_qty);
$smarty->assign("total_in_move_price", $total_in_move_price);
$smarty->assign("total_in_return_qty", $total_in_return_qty);
$smarty->assign("total_in_return_price", $total_in_return_price);
$smarty->assign("total_in_etc_qty", $total_in_etc_qty);
$smarty->assign("total_in_etc_price", $total_in_etc_price);
$smarty->assign("total_in_qty", $total_in_qty);
$smarty->assign("total_in_price", $total_in_price);
$smarty->assign("total_out_base_qty", $total_out_base_qty);
$smarty->assign("total_out_base_price", $total_out_base_price);
$smarty->assign("total_out_point_qty", $total_out_point_qty);
$smarty->assign("total_out_point_price", $total_out_point_price);
$smarty->assign("total_out_move_qty", $total_out_move_qty);
$smarty->assign("total_out_move_price", $total_out_move_price);
$smarty->assign("total_out_return_qty", $total_out_return_qty);
$smarty->assign("total_out_return_price", $total_out_return_price);
$smarty->assign("total_out_etc_qty", $total_out_etc_qty);
$smarty->assign("total_out_etc_price", $total_out_etc_price);
$smarty->assign("total_out_qty", $total_out_qty);
$smarty->assign("total_out_price", $total_out_price);
$smarty->assign("sch_reg_s_date", $sch_move_s_date);
$smarty->assign("sch_reg_e_date", $sch_move_e_date);
$smarty->assign("receipt_month", date("Y-m", strtotime("-1 months")));
$smarty->assign("sch_logistics_company_list", $sch_log_company_list);
$smarty->assign("logistics_company_list", $log_company_list);
$smarty->assign("contract_company_list", $contract_company_list);
$smarty->assign("brand_option", $product_model->getBrandData());
$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("last_receipt_month", $last_receipt_month);
$smarty->assign("sch_unit_type_option", $unit_type_option);
$smarty->assign("product_receipt_list", $product_receipt_list);

$smarty->display('product_cms_stock_receipt_list.html');
?>
