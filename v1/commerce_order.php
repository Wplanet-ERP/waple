<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_price.php');
require('inc/helper/commerce_order.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/MyCompany.php');
require('inc/model/Team.php');
require('inc/model/CommerceOrder.php');

# Model 설정
$comm_set_model = CommerceOrder::Factory();

# 프로세스 처리
$process 		= (isset($_POST['process'])) ? $_POST['process'] : "";

if ($process == "f_state")
{
	$no		 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	$comm_item	= $comm_set_model->getItem($no);
	$upd_data 	= array("state" => $value, "no" => $no);

	if($value == '3'){
		if(empty($comm_item['req_date'])){
			$upd_data['req_date'] = date("Y-m-d");
		}
	}

	if($comm_set_model->update($upd_data))
	{
		if(isset($upd_data['req_date'])){
			echo "진행상태와 발주요청일이 저장 되었습니다.";
			echo "<script>setTimeout(function() {location.reload();}, 800);</script>";
		}else{
			echo "진행상태가 저장 되었습니다.";
			echo "<script>setTimeout(function() {location.reload();}, 800);</script>";
		}
	}else{
		echo "진행상태 저장에 실패 하였습니다.";
	}
	exit;
}
elseif ($process == "delete")
{
	$no 		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

	if($comm_set_model->update(array("no" => $no, "display" => '2'))) {
		echo("<script>alert('삭제 하였습니다');</script>");
	} else {
		echo("<script>alert('삭제에 실패 하였습니다');</script>");
	}
	exit("<script>location.href='commerce_order.php?{$search_url}';</script>");
}
elseif ($process == "duplicate")
{
	$no 		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
	$req_date	= date('Y-m-d');
	$comm_set	= $comm_set_model->getItem($no);
	$ord_count  = $comm_set_model->getMaxOrderCount($comm_set['sup_c_no'], $req_date);
	$child_cnt	= $comm_set_model->getChildCount($no);


	$dup_sql = "
		INSERT IGNORE INTO commerce_order_set (my_c_no, state, c_no, c_name, s_no, team, log_c_no, sup_c_no, sup_c_name, sup_loc_type, title, order_count, req_date, req_s_no, req_team, regdate)
			SELECT my_c_no, '1', c_no, c_name, s_no, team, log_c_no, sup_c_no, sup_c_name, sup_loc_type, CONCAT(title,'_복제'), '{$ord_count}', '{$req_date}', '{$session_s_no}', '{$session_team}', NOW()
			FROM commerce_order_set cos
			WHERE no = '{$no}'
	";

	if(mysqli_query($my_db, $dup_sql))
	{
		if($child_cnt > 0)
		{
			$sql_query	= mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
			$sql_result	= mysqli_fetch_array($sql_query);
			$last_no 	= $sql_result[0];

			$child_sql = "
				INSERT IGNORE INTO commerce_order(type, set_no, option_no, quantity, unit_price, supply_price, vat, price, priority, memo, regdate)
					SELECT 'request', '{$last_no}', option_no, quantity, unit_price, supply_price, vat, price, priority, memo, NOW()
					FROM commerce_order
					WHERE set_no = '{$no}' AND `type`='request'
			";

			if(mysqli_query($my_db, $child_sql)) {
				echo("<script>alert('복제 하였습니다');</script>");
			} else {
				echo("<script>alert('복제에 실패 하였습니다');</script>");
			}
		}else{
			echo("<script>alert('복제 하였습니다');</script>");
		}
	} else {
		echo("<script>alert('복제에 실패 하였습니다');</script>");
	}

	exit("<script>location.href='commerce_order.php?{$search_url}';</script>");
}
elseif($process == "add_req_record")
{
	$search_url 	= isset($_POST['search_url']) ? $_POST['search_url'] : "";
	$new_c_no 		= isset($_POST['f_c_no_new']) ? $_POST['f_c_no_new'] : "";
	$new_sup_c_no 	= isset($_POST['f_sup_c_no_new']) ? $_POST['f_sup_c_no_new'] : "";
	$new_log_c_no 	= isset($_POST['f_log_c_no_new']) ? $_POST['f_log_c_no_new'] : "";

	$company_model 		= Company::Factory();
	$company_item  		= $company_model->getItem($new_c_no);
	$sup_company_item  	= $company_model->getItem($new_sup_c_no);
	$req_date			= date('Y-m-d');

	$ins_data   = array(
		"state" 		=> isset($_POST['f_state_new']) ? $_POST['f_state_new'] : 1,
		"c_no" 			=> $new_c_no,
		"c_name" 		=> $company_item['c_name'],
		"my_c_no" 		=> $company_item['my_c_no'],
		"team" 			=> $_POST['f_team_new'],
		"s_no" 			=> $_POST['f_s_no_new'],
		"log_c_no"		=> $new_log_c_no,
		"sup_c_no"		=> $new_sup_c_no,
		"sup_c_name"	=> $sup_company_item['c_name'],
		"sup_loc_type"	=> isset($_POST['f_sup_loc_type_new']) ? $_POST['f_sup_loc_type_new'] : "",
		"order_count"	=> $comm_set_model->getMaxOrderCount($new_sup_c_no, $req_date),
		"req_team"		=> $_POST['f_req_team_new'],
		"req_s_no"		=> $_POST['f_req_s_no_new'],
		"req_date"		=> $req_date,
		"regdate"		=> date('Y-m-d H:i:s'),
	);

	if($comm_set_model->insert($ins_data)) {
		echo("<script>alert('발주 구성을 추가 하였습니다');</script>");

	} else {
		echo("<script>alert('발주 구성 추가에 실패 하였습니다');</script>");
	}

	exit("<script>location.href='commerce_order.php?{$search_url}';</script>");
}
elseif ($process == "f_req_date")
{
    $no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    if ($comm_set_model->update(array("no" => $no, "req_date" => $value))){
        echo "발주요청일이 저장 되었습니다.";
    }else{
        echo "발주요청일 저장에 실패 하였습니다.";
    }
    exit;
}
elseif ($process == "f_sup_end_date")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($comm_set_model->update(array("no" => $no, "sup_end_date" => $value))){
		echo "공급완료일이 저장 되었습니다.";
	}else{
		echo "공급완료일 저장에 실패 하였습니다.";
	}
	exit;
}
elseif ($process == "f_memo")
{
    $no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($comm_set_model->update(array("no" => $no, "memo" => empty($value) ? "NULL" : addslashes($value)))){
		echo "메모가 저장 되었습니다.";
	}else{
		echo "메모 저장에 실패 하였습니다.";
	}
	exit;
}
else
{
	# Navigation & My Quick
	$nav_prd_no  = "37";
	$nav_title   = "발주관리";
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_title", $nav_title);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$company_model			= Company::Factory();
	$log_company_list 		= $company_model->getLogisticsList();
	$add_log_company_list 	= $log_company_list;
	$is_global				= false;

	if($session_s_no == "9" || $session_s_no == "237" || $session_s_no == "298"){
		$is_global = true;
	}
	$smarty->assign("is_global", $is_global);

	$add_where		 	= " `cos`.display = '1'";
    $sch			 	= isset($_GET['sch'])?$_GET['sch']:"Y";
    $sch_req_s_date  	= isset($_GET['sch_req_s_date']) ? $_GET['sch_req_s_date'] : "";
    $sch_req_e_date  	= isset($_GET['sch_req_e_date']) ? $_GET['sch_req_e_date'] : "";
    $sch_state 	  	 	= isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
    $sch_team 	  	 	= isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
    $sch_s_no 	  	 	= isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
    $sch_sup_c_no 	 	= isset($_GET['sch_sup_c_no'])?$_GET['sch_sup_c_no']:"";
	$sch_no		 	 	= isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
	$sch_my_c_no 	 	= isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
	$sch_c_name 	 	= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
	$sch_title		 	= isset($_GET['sch_title'])?$_GET['sch_title']:"";
	$sch_order_count 	= isset($_GET['sch_order_count'])?$_GET['sch_order_count']:"";
	$sch_month 		 	= isset($_GET['sch_month']) ? $_GET['sch_month'] : "";
	$sch_loc_type	 	= isset($_GET['sch_loc_type']) ? $_GET['sch_loc_type'] : "";
	$sch_log_company 	= isset($_GET['sch_log_company']) ? $_GET['sch_log_company'] : "";
	$sch_req_s_name	 	= isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : "";
	$sch_memo	 		= isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
	$smarty->assign("sch", $sch);

	# 기본 리스트
	$unit_model			= ProductCmsUnit::Factory();
	$brand_list			= $unit_model->getDistinctUnitOrdCompanyData("brand");
    $sup_ord_c_list 	= $unit_model->getDistinctUnitOrdCompanyLocData("sup_c_no");
    $sup_c_list			= $unit_model->getDistinctUnitCompanyData("sup_c_no");

    $add_brand_list	  	= $brand_list[$session_s_no];
    $add_sup_c_list   	= $sup_ord_c_list[$session_s_no];
    $sup_c_count_list 	= [];
    $sch_sup_c_list 	= $sup_c_list;

	$team_model         = Team::Factory();
	$team_all_list      = $team_model->getTeamAllList();
	$staff_team_list    = $team_all_list['staff_list'];
	$sch_staff_list 	= $staff_team_list['all'];
	$team_full_name_list= $team_model->getTeamFullNameList();
	$team_name_list     = $team_all_list['team_name_list'];

    if(!empty($add_sup_c_list)){
    	foreach($add_sup_c_list as $key => $sup_c){
    		$sup_c_sql = "SELECT count(*) as cnt FROM commerce_order_set cos WHERE cos.sup_c_no='{$key}' AND cos.req_s_no='{$session_s_no}' AND `state`='2' AND display='1'";
            $sup_c_query = mysqli_query($my_db, $sup_c_sql);
            while($sup_c_result = mysqli_fetch_assoc($sup_c_query)){
            	$sup_c_count_list[$key] = $sup_c_result['cnt'];
			}
		}
	}

	#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
	$url_check_str  = $_SERVER['QUERY_STRING'];
	$url_chk_result = false;
	if(empty($url_check_str)){
		$url_check_team_where   = getTeamWhere($my_db, $session_team);
		$url_check_sql    		= "SELECT count(`no`) as cnt FROM commerce_order_set `cos` WHERE `cos`.display = '1' AND (cos.req_team IN ({$url_check_team_where}) OR `cos`.sup_c_no IN(SELECT DISTINCT sup_c_no FROM product_cms_unit WHERE ord_s_no IN(SELECT s.s_no FROM staff s WHERE team='{$session_team}') OR ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team='{$session_team}') OR ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team='{$session_team}')))";
		$url_check_query  		= mysqli_query($my_db, $url_check_sql);
		$url_check_result 		= mysqli_fetch_assoc($url_check_query);

		if($url_check_result['cnt'] == 0){
			$sch_team 	= "all";
			$url_chk_result = true;
		}
	}

	if (!empty($sch_state)) {
		$add_where .= " AND `cos`.state ='{$sch_state}'";
		$smarty->assign("sch_state", $sch_state);
	}

    $sch_team_code_where = "";
    if (!empty($sch_team))
    {
        if ($sch_team != "all") {
            $sch_team_code_where = getTeamWhere($my_db, $sch_team);
            $add_where 		 .= " AND (`cos`.req_team IN({$sch_team_code_where}) OR `cos`.sup_c_no IN(SELECT DISTINCT sup_c_no FROM product_cms_unit WHERE ord_s_no IN(SELECT s.s_no FROM staff s WHERE team='{$sch_team}') OR ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team='{$sch_team}') OR ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team='{$sch_team}')))";
            $sch_sup_c_list   = $unit_model->getTeamSupList($sch_team);
            $sch_staff_list   = $staff_team_list[$sch_team];
        }
        $smarty->assign("sch_req_team", $sch_team);
    }else{
        $sch_team_code_where = getTeamWhere($my_db, $session_team);
        $add_where 		 .= " AND (cos.req_team IN ({$sch_team_code_where}) OR `cos`.sup_c_no IN(SELECT DISTINCT sup_c_no FROM product_cms_unit WHERE ord_s_no IN(SELECT s.s_no FROM staff s WHERE team='{$session_team}') OR ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team='{$session_team}') OR ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team='{$session_team}')))";
        $sch_sup_c_list   = $unit_model->getTeamSupList($session_team);
        $sch_staff_list   = $staff_team_list[$session_team];
        $smarty->assign("sch_req_team", $session_team);
    }

    if (!empty($sch_s_no))
    {
        if ($sch_s_no != "all") {
            $add_where 		 .= " AND (`cos`.req_s_no = '{$sch_s_no}' OR `cos`.sup_c_no IN(SELECT DISTINCT sup_c_no FROM product_cms_unit WHERE ord_s_no='{$sch_s_no}' OR ord_sub_s_no='{$sch_s_no}' OR ord_third_s_no='{$sch_s_no}'))";
        }
        $smarty->assign("sch_req_s_no",$sch_s_no);
    }else{
        if($sch_team == $session_team){
            $add_where		 .=" AND (`cos`.req_s_no = '{$session_s_no}' OR `cos`.sup_c_no IN(SELECT DISTINCT sup_c_no FROM product_cms_unit WHERE ord_s_no='{$sch_s_no}' OR ord_sub_s_no='{$sch_s_no}' OR ord_third_s_no='{$sch_s_no}'))";
            $smarty->assign("sch_req_s_no", $session_s_no);
        }
	}

    if (!empty($sch_sup_c_no)) {
        $add_where .= " AND `cos`.sup_c_no ='{$sch_sup_c_no}'";
        $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
    }

    if (!empty($sch_no)) {
        $add_where .= " AND `cos`.`no`='{$sch_no}'";
        $smarty->assign("sch_no", $sch_no);
    }

	if (!empty($sch_my_c_no)) {
		if ($sch_my_c_no != "all") {
			$add_where .= " AND `cos`.my_c_no = '{$sch_my_c_no}'";
		}
		$smarty->assign("sch_my_c_no", $sch_my_c_no);
	}

	if (!empty($sch_c_name)) {
		$add_where .= " AND `cos`.c_name LIKE '%{$sch_c_name}%'";
		$smarty->assign("sch_c_name", $sch_c_name);
	}

	if (!empty($sch_title)) {
		$add_where .= " AND `cos`.title LIKE '%{$sch_title}%'";
		$smarty->assign("sch_title", $sch_title);
	}

	if (!empty($sch_order_count)) {
		$add_where .= " AND `cos`.order_count = '{$sch_order_count}'";
		$smarty->assign("sch_order_count", $sch_order_count);
	}

	if (!empty($sch_month)) {
		$add_where .= " AND DATE_FORMAT(`cos`.req_date, '%Y-%m') BETWEEN '{$sch_month}' AND '{$sch_month}' ";
		$smarty->assign("sch_month", $sch_month);
	}

    if (!empty($sch_req_s_date)) {
        $req_s_date = $sch_req_s_date." 00:00:00";
        $add_where .= " AND `cos`.req_date >='{$req_s_date}'";
        $smarty->assign("sch_req_s_date", $sch_req_s_date);
    }

    if (!empty($sch_req_e_date)) {
        $req_e_date = $sch_req_e_date." 23:59:59";
        $add_where .= " AND `cos`.req_date <= '{$req_e_date}'";
        $smarty->assign("sch_req_e_date", $sch_req_e_date);
    }

    if(!empty($sch_loc_type)) {
		$add_where .= " AND `cos`.sup_loc_type='{$sch_loc_type}'";
		$smarty->assign("sch_loc_type", $sch_loc_type);
	}

	if(!empty($sch_req_s_name)) {
		$add_where .= " AND `cos`.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_req_s_name}%')";
		$smarty->assign("sch_req_s_name", $sch_req_s_name);
	}

	if(!empty($sch_log_company)) {
		$add_where .= " AND `cos`.log_c_no='{$sch_log_company}'";
		$smarty->assign("sch_log_company", $sch_log_company);
	}

	if(!empty($sch_memo)) {
		$add_where .= " AND `cos`.memo LIKE '%{$sch_memo}%'";
		$smarty->assign("sch_memo", $sch_memo);
	}

	// 정렬순서 토글 & 필드 지정
	$add_orderby = "";
	$order		 = isset($_GET['od'])?$_GET['od']:"";
	$order_type	 = isset($_GET['by'])?$_GET['by']:"";

	if($order_type == '2'){
		$toggle = "DESC";
	}else{
		$toggle = "ASC";
	}

	$order_field = array('','sup_c_name','order_count');

	if($order && $order<3) {
		$add_orderby .= " ISNULL($order_field[$order]) ASC, $order_field[$order] $toggle";
		$smarty->assign("order",$order);
		$smarty->assign("order_type",$order_type);
	}else{
		$add_orderby = "`cos`.`no` DESC";
	}

	# 전체 게시물 수
	$cnt_sql 	= "SELECT count(*) AS total_count FROM (SELECT `cos`.`no` FROM commerce_order_set `cos` WHERE {$add_where}) AS cnt";
	$result		= mysqli_query($my_db, $cnt_sql);
	$cnt_data	= mysqli_fetch_array($result);
	$total_num 	= $cnt_data['total_count'];

	$page_type	= (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
    $page 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= $page_type;
    $offset 	= ($page-1) * $num;
	$page_num 	= ceil($total_num/$num);

	# 페이징
	if ($page >= $page_num){$page = $page_num;}
	if ($page <= 0){$page = 1;}

	if(empty($url_check_str) && $url_chk_result){
		$search_url = "sch_team=all";
	}else{
		$search_url = getenv("QUERY_STRING");
	}
	$page_list	= pagelist($page, "commerce_order.php", $page_num, $search_url);

	$smarty->assign("search_url", $search_url);
	$smarty->assign("total_num", $total_num);
	$smarty->assign("ord_page_type", $page_type);
	$smarty->assign("page_list", $page_list);

	# 리스트 쿼리
	$commerce_order_set_sql = "
		SELECT
			`cos`.`no`,
			`cos`.my_c_no,
			(SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=`cos`.my_c_no) as my_c_name,
		    `cos`.sup_loc_type,
			`cos`.state,
			`cos`.c_no,
			`cos`.c_name,
			(SELECT t2.team_name FROM team t2 WHERE t2.team_code=(SELECT t.team_code_parent FROM team t WHERE t.team_code=`cos`.team)) AS team_parent_name,
			(SELECT t.team_name FROM team t WHERE t.team_code=`cos`.team) AS team_name,
			`cos`.s_no,
			(SELECT s_name FROM staff s where s.s_no=`cos`.s_no) AS s_name,
			`cos`.sup_c_no,
			`cos`.sup_c_name,
		   	`cos`.log_c_no,
		    (SELECT c.c_name FROM company c WHERE c.c_no=`cos`.log_c_no) as log_c_name,
			`cos`.title,
			`cos`.order_count,
			`cos`.req_s_no,
			(SELECT s_name FROM staff s where s.s_no=`cos`.req_s_no) AS req_s_name,
			(SELECT t.team_name FROM team t WHERE t.team_code=`cos`.req_team) AS req_team_name,
			`cos`.req_date,
			`cos`.sup_end_date,
			(SELECT SUM(quantity) FROM commerce_order co WHERE co.`type`='supply' AND co.set_no=`cos`.`no`) AS sup_quantity_sum,
			(SELECT SUM(quantity) FROM commerce_order co WHERE co.`type`='request' AND co.set_no=`cos`.`no`) AS req_quantity_sum,
			(SELECT SUM(co.supply_price+co.vat) FROM commerce_order co WHERE co.`type`='request' AND co.set_no=`cos`.`no`) AS req_price_sum,
			`cos`.memo,
			`cos`.regdate
		FROM commerce_order_set `cos`
		WHERE {$add_where}
		ORDER BY {$add_orderby}
		LIMIT {$offset}, {$num}
	";
	$commerce_order_set_query 	= mysqli_query($my_db, $commerce_order_set_sql);
	$commerce_order_set_list  	= [];
	$my_company_model       	= MyCompany::Factory();
	$my_company_list        	= $my_company_model->getList();
	while($commerce_order_array = mysqli_fetch_array($commerce_order_set_query))
	{
		$wd_money_sum_sql 		= "SELECT SUM(wd_money) as wd_money_sum FROM withdraw WHERE wd_no IN (SELECT sub_c.wd_no FROM commerce_order sub_c WHERE sub_c.`type`='withdraw' AND sub_c.set_no='{$commerce_order_array['no']}')";
		$wd_money_sum_query 	= mysqli_query($my_db, $wd_money_sum_sql);
		$wd_money_sum_result 	= mysqli_fetch_assoc($wd_money_sum_query);
		$commerce_order_array['wd_money_sum'] = $wd_money_sum_result['wd_money_sum'];

		$commerce_order_array['kind_color']			= $my_company_list[$commerce_order_array['my_c_no']]['color'];
		$commerce_order_array["wd_money_sum"] 		= ($commerce_order_array['sup_loc_type'] == '2') ? getUsdFormatPrice($commerce_order_array['wd_money_sum']) : getNumberFormatPrice($commerce_order_array['wd_money_sum']);
		$commerce_order_array["req_price_sum"] 		= ($commerce_order_array['sup_loc_type'] == '2') ? getUsdFormatPrice($commerce_order_array['req_price_sum']) : getNumberFormatPrice($commerce_order_array['req_price_sum']);
		$commerce_order_array['sup_quantity_sum']	= number_format($commerce_order_array['sup_quantity_sum']);
		$commerce_order_array['req_quantity_sum']	= number_format($commerce_order_array['req_quantity_sum']);
		$commerce_order_array['memo']				= htmlspecialchars($commerce_order_array['memo']);

		$commerce_order_array['is_new'] = false;
		if($commerce_order_array['req_date'] >= '2025-01-01'){
			$commerce_order_array['is_new'] = true;
		}

		$commerce_order_set_list[] = $commerce_order_array;
	}

    $smarty->assign("brand_list", $add_brand_list);
	$smarty->assign("add_sup_c_list", $add_sup_c_list);
    $smarty->assign("sup_c_count_list", $sup_c_count_list);
    $smarty->assign("sch_sup_c_list", $sch_sup_c_list);
    $smarty->assign("my_company_list", $my_company_list);
    $smarty->assign("sch_staff_list", $sch_staff_list);
    $smarty->assign("sch_team_list", $team_full_name_list);
    $smarty->assign("team_name_list", $team_name_list);
    $smarty->assign("sch_work_state_list", getCommerceWorkStateOption());
    $smarty->assign("location_type_option", getLocationTypeOption());
    $smarty->assign("page_type_option", getPageTypeOption('5'));
	$smarty->assign("log_company_list", $log_company_list);
	$smarty->assign("commerce_order_set_list", $commerce_order_set_list);

	$smarty->display('commerce_order.html');
}
?>
