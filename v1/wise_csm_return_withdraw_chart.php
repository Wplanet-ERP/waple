<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_date.php');
require('inc/helper/wise_csm.php');
require('inc/model/Staff.php');

# 사용 변수들
$staff_model        = Staff::Factory();
$cs_staff_list      = $staff_model->getActiveTeamStaff("00244");
$cs_staff_list['309'] = "이채하";
$date_name_option   = getDateChartOption();

# 날짜별 검색
$add_where      = "w.prd_no='229' AND w.work_state='6' AND wd.wd_state='3' AND wd.wd_date IS NOT NULL AND wd.display='1'";
$today_s_w		= date('w')-1;
$base_s_date    = date('Y-m-d', strtotime("-{$today_s_w} day"));
$sch_date_type  = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "3";
$sch_date       = isset($_GET['sch_date']) ? $_GET['sch_date'] : date('Y-m');
$sch_s_month    = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("-3 months"));
$sch_e_month    = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week     = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d', strtotime("{$base_s_date} -2 weeks"));
$sch_e_week     = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d", strtotime("{$base_s_date} +6 day"));
$sch_s_date     = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d', strtotime("-10 day"));
$sch_e_date     = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');
$sch_s_no       = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_date', $sch_date);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);

if(!empty($sch_s_no)) {
    $add_where  .= " AND w.task_run_s_no='{$sch_s_no}'";
    $smarty->assign('sch_s_no', $sch_s_no);
}

# 전체 기간 조회 및 누적데이터 조회
$all_date_where   = "";
$all_date_key	  = "";
$add_date_where   = "";
$add_date_column  = "";
$stats_list       = [];

$add_return_date_where  = "";
$add_return_date_column = "";
$sch_s_datetime         = "";
$sch_e_datetime         = "";

if($sch_date_type == '3') //월간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $sch_s_datetime  = "{$sch_s_month}-01";
    $sch_e_day       = date("t", strtotime($sch_e_month));
    $sch_e_datetime  = "{$sch_e_month}-{$sch_e_day}";
    $add_key_column  = "DATE_FORMAT(wd.wd_date, '%Y%m')";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}')";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $sch_s_datetime  = $sch_s_week;
    $sch_e_datetime  = $sch_e_week;
    $add_key_column  = "DATE_FORMAT(DATE_SUB(wd.wd_date, INTERVAL(IF(DAYOFWEEK(wd.wd_date)=1,8,DAYOFWEEK(wd.wd_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '$sch_e_date')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $sch_s_datetime  = $sch_s_date;
    $sch_e_datetime  = $sch_e_date;
    $add_key_column  = "DATE_FORMAT(wd.wd_date, '%Y%m%d')";
}
$add_where  .= " AND (wd.wd_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}')";


# 기본 STAT 리스트 Init 및 x key 및 타이틀 설정
$x_label_list       = [];
$x_table_th_list    = [];
$picker_list        = [];
$withdraw_brand_list= [];

$picker_sql         = "SELECT DISTINCT w.c_no, w.c_name FROM `work` AS w LEFT JOIN withdraw AS wd ON wd.wd_no=w.wd_no WHERE {$add_where} ORDER BY c_no";
$picker_query       = mysqli_query($my_db, $picker_sql);
while($picker_result = mysqli_fetch_assoc($picker_query)){
    $picker_list[$picker_result['c_no']] = $picker_result['c_name'];
}


# 날짜 체크
$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	GROUP BY chart_key
	ORDER BY chart_key, date_key
";
if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_name_option[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        $x_label_list[$date['chart_key']] = "'{$chart_title}'";

        if(!isset($x_table_th_list[$date['chart_key']]))
        {
           if($sch_date_type == '3'){
                $chart_s_date_val   = $date['chart_key']."01";
                $chart_s_date       = date("Y-m-d", strtotime("{$chart_s_date_val}"));
                $chart_e_day        = DATE('t', strtotime($chart_s_date));
                $chart_e_date_val   = $date['chart_key'].$chart_e_day;
                $chart_e_date       = date("Y-m-d", strtotime("{$chart_e_date_val}"));
            }elseif($sch_date_type == '2'){
                $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_e_date = date("Y-m-d", strtotime("{$chart_s_date} +6 days"));
                $chart_mon    = date("Y-m", strtotime($chart_s_date));
            }else{
                $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_e_date = date("Y-m-d", strtotime("{$date['date_key']}"));
            }

            $x_table_th_list[$date['chart_key']] = array("title" => $chart_title, "s_date" => $chart_s_date, "e_date" => $chart_e_date);
        }

        foreach($picker_list as $c_no => $label){
            $withdraw_brand_list[$date['chart_key']][$c_no] = 0;
        }
    }
}

# Y 컬럼 Label
$y_label_list   = [];
$y_label_title  = [];
foreach($picker_list as $c_no => $c_name)
{
    $y_label_list[$c_no]  = $c_name;
    $y_label_title[$c_no] = $c_name;
    $y_label_title['all'] = "합산";
}

# 커머스 환불 브랜드별
$withdraw_brand_sql = "
    SELECT
        w.c_no,
        {$add_key_column} as key_date,
        SUM(wd.wd_money) as total_price
    FROM work AS w
    LEFT JOIN withdraw AS wd ON wd.wd_no=w.wd_no
    WHERE {$add_where} 
    GROUP BY key_date, w.c_no
    ORDER BY key_date ASC
";
$withdraw_brand_query = mysqli_query($my_db, $withdraw_brand_sql);
while($withdraw_brand = mysqli_fetch_assoc($withdraw_brand_query))
{
    $withdraw_brand_list[$withdraw_brand['key_date']][$withdraw_brand['c_no']] += $withdraw_brand['total_price'];
}

$full_data = [];
foreach($withdraw_brand_list as $sales_date => $stats_withdraw_data)
{
    $date_price_sum = 0;
    if($stats_withdraw_data)
    {
        $price_sum = 0;
        foreach ($stats_withdraw_data as $key => $price)
        {
            $full_data["each"]["line"][$key]['title']   = $picker_list[$key];
            $full_data["each"]["line"][$key]['data'][]  = $price;

            $full_data["each"]["bar"][$key]['title']    = $picker_list[$key];
            $full_data["each"]["bar"][$key]['data'][]   = $price;

            $price_sum      += $price;
            $date_price_sum += $price;
        }
    }

    $full_data["sum"]["line"]["all"]['title']   = "합산";
    $full_data["sum"]["line"]["all"]['data'][] = $date_price_sum;
    $full_data["sum"]["bar"]["all"]['title']    = "합산";
    $full_data["sum"]["bar"]["all"]['data'][]   = $date_price_sum;
}

$smarty->assign('cs_staff_list', $cs_staff_list);
$smarty->assign('x_label_list', implode(',', $x_label_list));
$smarty->assign('x_table_th_list', json_encode($x_table_th_list));
$smarty->assign('legend_list', json_encode($y_label_list));
$smarty->assign('y_label_title', json_encode($y_label_title));
$smarty->assign('picker_list', $picker_list);
$smarty->assign('full_data', json_encode($full_data));

$smarty->display('wise_csm_return_withdraw_chart.html');
?>
