<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/manufacturer.php');
require('inc/model/Manufacturer.php');
require('inc/model/WorkCms.php');
require('inc/model/WorkCmsReturn.php');

# 공급업체 체크
if($session_partner == "dewbell"){
    $add_c_no   = "2305";
    $add_c_name = "듀벨";
}elseif($session_partner == "seongill"){
    $add_c_no   = "2304";
    $add_c_name = "성일화학";
}elseif($session_partner == "pico"){
    $add_c_no   = "2772";
    $add_c_name = "주식회사 피코그램";
}elseif($session_partner == "paino"){
    $add_c_no   = "2472";
    $add_c_name = "(주)파이노";
}else{
    exit("접근할 수 없는 URL 입니다.");
}
$smarty->assign("add_c_no", $add_c_no);
$smarty->assign("add_c_name", $add_c_name);

$editor_folder_name = folderCheck('cms_return_inspection_content');
$smarty->assign('editor_path', 'cms_return_inspection_content/'.$editor_folder_name);

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$manu_model     = Manufacturer::Factory();
$cms_model      = WorkCms::Factory();
$return_model   = WorkCmsReturn::Factory();

if($process == "add_inspection")
{
    $r_ord_no   = isset($_POST['order_number']) ? $_POST['order_number'] : "";
    $prev_type  = isset($_POST['prev_type']) ? $_POST['prev_type'] : "MI";
    $prev_url   = ($prev_type == "QC") ? "out_return_qc_list.php" : "out_return_mi_list.php";
    $regdate    = date("Y-m-d H:i:s");

    if(empty($r_ord_no)){
        exit("<script>alert('검수결과 저장에 실패했습니다. 회수 주문번호가 없습니다.');location.href='out_return_qc_list.php'</script>");
    }

    # 회수 주문
    $return_item = $return_model->getOrderItem($r_ord_no);
    $order_item  = $cms_model->getOrderItem($return_item['parent_order_number']);

    if(empty($return_item['r_no'])){
        exit("<script>alert('검수결과 저장에 실패했습니다. 존재하지 않는 회수주문입니다.');location.href='out_return_qc_list.php'</script>");
    }

    # 등록된 검수인지 확인
    $inspection_sql     = "SELECT COUNT(*) as cnt FROM manufacturer_inspection WHERE order_number='{$r_ord_no}' AND add_c_no='{$add_c_no}'";
    $inspection_query   = mysqli_query($my_db, $inspection_sql);
    $inspection_result  = mysqli_fetch_assoc($inspection_query);

    if($inspection_result['cnt'] > 0){
        exit("<script>alert('검수결과 저장에 실패했습니다. 이미 작성된 검수결과입니다.');location.href='out_return_qc_list.php'</script>");
    }

    $return_unit_sql    = "SELECT wcr.order_number, wcr.option, wcr.sku, wcr.quantity, pcu.sup_c_no FROM work_cms_return_unit wcr LEFT JOIN product_cms_unit pcu ON pcu.`no`=wcr.option WHERE wcr.order_number='{$r_ord_no}' AND pcu.sup_c_no='{$add_c_no}' AND wcr.quantity > 0";
    $return_unit_query  = mysqli_query($my_db, $return_unit_sql);
    $return_unit_list   = [];
    $mi_unit_data       = [];
    while($return_unit_data  = mysqli_fetch_assoc($return_unit_query)) {
        $return_unit_list[] = $return_unit_data;
    }

    if(empty($return_unit_list)){
        exit("<script>alert('검수결과 저장에 실패했습니다. 작성 권한이 없는 회수 주문입니다.');location.href='out_return_qc_list.php'</script>");
    }

    $ins_title      = isset($_POST['ins_title']) ? $_POST['ins_title'] : "";
    $is_improve     = isset($_POST['is_improve']) ? $_POST['is_improve'] : "2";
    $exp_date       = isset($_POST['exp_date']) ? $_POST['exp_date'] : "";
    $improve_run    = isset($_POST['improve_run']) ? $_POST['improve_run'] : "";
    $content        = isset($_POST['content']) ? $_POST['content'] : "";

    $mi_data = array(
        "order_number"          => $r_ord_no,
        "parent_order_number"   => $order_item['order_number'],
        "stock_date"            => $order_item['stock_date'],
        "work_state"            => "6",
        "add_c_no"              => $add_c_no,
        "recipient"             => $return_item['recipient'],
        "recipient_hp"          => $return_item['recipient_hp'],
        "recipient_addr"        => $return_item['recipient_addr'],
        "zip_code"              => $return_item['zip_code'],
        "req_memo"              => $return_item['req_memo'],
        "ins_title"             => addslashes(trim($ins_title)),
        "reg_s_no"              => $session_s_no,
        "reg_s_name"            => $session_name,
        "regdate"               => $regdate,
        "is_improve"            => $is_improve,
        "exp_date"              => !empty($exp_date) ? $exp_date : "NULL",
        "improve_run"           => addslashes(trim($improve_run)),
        "content"               => addslashes(trim($content)),
    );

    # 파일첨부
    $file_name = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_path = isset($_POST['file_path']) ? $_POST['file_path'] : "";

    $add_file_column = "";
    if(!empty($file_path))
    {
        $file_reads  = move_store_files($file_path, "dropzone_tmp", "cms_return_inspection");
        $file_names  = implode(',', $file_name);
        $file_paths  = implode(',', $file_reads);

        $mi_data['file_path'] = $file_paths;
        $mi_data['file_name'] = $file_names;
    }

    if($manu_model->insert($mi_data))
    {
        $new_mi_no      = $manu_model->getInsertId();
        $mi_unit_data   = [];
        foreach ($return_unit_list as $return_unit){
            $mi_unit_data[] = array(
                "mi_no"         => $new_mi_no,
                "order_number"  => $r_ord_no,
                "sup_c_no"      => $return_unit['sup_c_no'],
                "option_no"     => $return_unit['option'],
                "sku"           => $return_unit['sku'],
                "quantity"      => $return_unit['quantity'],
                "regdate"       => $regdate
            );
        }

        $manu_model->setMainInit("manufacturer_inspection_unit", "u_no");
        $manu_model->multiInsert($mi_unit_data);

        exit("<script>alert('검수결과 저장에 성공했습니다');location.href='{$prev_url}';</script>");
    }else{
        exit("<script>alert('검수결과 저장에 실패했습니다');location.href='out_return_mi_regist.php?r_ord_no={$r_ord_no}&prev_type={$prev_type}';</script>");
    }
}
elseif($process == "mod_inspection")
{
    $mi_no      = isset($_POST['mi_no']) ? $_POST['mi_no'] : "";
    $prev_type  = isset($_POST['prev_type']) ? $_POST['prev_type'] : "MI";
    $prev_url   = ($prev_type == "QC") ? "out_return_qc_list.php" : "out_return_mi_list.php";

    if(empty($mi_no)){
        exit("<script>alert('검수결과 저장에 실패했습니다. 고유번호가 없습니다.');location.href='out_return_qc_list.php'</script>");
    }

    # 회수 주문
    $ma_item = $manu_model->getItem($mi_no);
    if($ma_item['add_c_no'] != $add_c_no){
        exit("<script>alert('작성 권한이 없는 회수주문입니다.');location.href='out_return_qc_list.php'</script>");
    }

    if($ma_item['work_state'] == '6'){
        exit("<script>alert('완료된 검수결과는 수정할 수 없습니다.');location.href='out_return_qc_list.php'</script>");
    }

    if($ma_item['run_state'] > 0){
        exit("<script>alert('와이즈 확인 진행중인 건은 수정할 수 없습니다.');location.href='out_return_qc_list.php'</script>");
    }

    $ins_title      = isset($_POST['ins_title']) ? $_POST['ins_title'] : "";
    $is_improve     = isset($_POST['is_improve']) ? $_POST['is_improve'] : "2";
    $exp_date       = isset($_POST['exp_date']) ? $_POST['exp_date'] : "";
    $improve_run    = isset($_POST['improve_run']) ? $_POST['improve_run'] : "";
    $content        = isset($_POST['content']) ? $_POST['content'] : "";

    $mi_data = array(
        "mi_no"         => $mi_no,
        "work_state"    => "6",
        "ins_title"     => addslashes(trim($ins_title)),
        "is_improve"    => $is_improve,
        "exp_date"      => !empty($exp_date) ? $exp_date : "NULL",
        "improve_run"   => addslashes(trim($improve_run)),
        "content"       => addslashes(trim($content)),
    );

    # 파일 처리
    $file_origin_path   = isset($_POST['file_origin_path']) ? $_POST['file_origin_path'] : "";
    $file_origin_name   = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_names         = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_paths         = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_chk           = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    $move_file_path = "";
    $move_file_name = "";
    if($file_chk)
    {
        if(!empty($file_paths) && !empty($file_names))
        {
            $move_file_paths = move_store_files($file_paths, "dropzone_tmp", "cms_return_inspection");
            $move_file_name  = implode(',', $file_names);
            $move_file_path  = implode(',', $move_file_paths);
        }

        if(!empty($file_origin_path) && !empty($file_origin_name))
        {
            if(!empty($file_paths) && !empty($file_names)){
                $del_files = array_diff(explode(',', $file_origin_path), $file_paths);
            }else{
                $del_files = explode(',', $file_origin_path);
            }

            if(!empty($del_files))
            {
                del_files($del_files);
            }
        }

        if(!empty($move_file_path) && !empty($move_file_name)){
            $mi_data['file_path'] = $move_file_path;
            $mi_data['file_name'] = $move_file_name;
        }else{
            $mi_data['file_path'] = "NULL";
            $mi_data['file_name'] = "NULL";
        }
    }

    if($manu_model->update($mi_data)){
        exit("<script>alert('검수결과 저장에 성공했습니다');location.href='{$prev_url}';</script>");
    }else{
        exit("<script>alert('검수결과 저장에 실패했습니다');location.href='out_return_mi_regist.php?mi_no={$mi_no}&prev_type={$prev_type}';</script>");
    }
}

# 등록/수정 페이지 시작
$mi_no      = isset($_GET['mi_no']) ? $_GET['mi_no'] : "";
$prev_type  = isset($_GET['prev_type']) ? $_GET['prev_type'] : "";
$prev_url   = ($prev_type == "QC") ? "out_return_qc_list.php" : "out_return_mi_list.php";
$r_ord_no   = isset($_GET['r_ord_no']) ? $_GET['r_ord_no'] : "";

$smarty->assign("prev_type", $prev_type);
$smarty->assign("prev_url", $prev_url);

if(empty($mi_no) && empty($r_ord_no)){
    exit("<script>alert('잘못된 접근입니다.');location.href='out_return_qc_list.php'</script>");
}

# 제조사 검수 번호로 접근시
$ma_item    = [];
if(!empty($mi_no))
{
    $ma_item    = $manu_model->getItem($mi_no);
    $r_ord_no   = $ma_item['order_number'];

    if($ma_item['add_c_no'] != $add_c_no){
        exit("<script>alert('작성 권한이 없는 회수주문입니다.');location.href='out_return_qc_list.php'</script>");
    }
}

# 회수주문 관련 확인 및 회수주문 적용
if(empty($r_ord_no)){
    exit("<script>alert('회수 주문번호가 없습니다.');location.href='out_return_qc_list.php'</script>");
}

$return_item = $return_model->getOrderItem($r_ord_no);

if(empty($return_item['r_no'])){
    exit("<script>alert('존재하지 않는 회수주문입니다.');location.href='out_return_qc_list.php'</script>");
}

# 등록된 검수인지 확인
if(empty($ma_item))
{
    $inspection_sql     = "SELECT * FROM manufacturer_inspection WHERE order_number='{$r_ord_no}' AND add_c_no='{$add_c_no}'";
    $inspection_query   = mysqli_query($my_db, $inspection_sql);
    $inspection_result  = mysqli_fetch_assoc($inspection_query);
    $ma_item            = $inspection_result;
    $mi_no              = $ma_item['mi_no'];
}

if(!empty($ma_item))
{
    $inspection_unit_sql    = "SELECT miu.option_no, miu.sku, miu.quantity FROM manufacturer_inspection_unit miu WHERE miu.mi_no='{$mi_no}'";
    $inspection_unit_query  = mysqli_query($my_db, $inspection_unit_sql);
    $inspection_unit_list   = [];
    while($inspection_unit  = mysqli_fetch_assoc($inspection_unit_query)) {
        $inspection_unit_list[] = $inspection_unit;
    }

    if (isset($ma_item['recipient_hp']) && !empty($ma_item['recipient_hp'])) {
        $f_hp = substr($ma_item['recipient_hp'], 0, 4);
        $e_hp = substr($ma_item['recipient_hp'], 7, 15);
        $ma_item["recipient_sc_hp"] = $f_hp . "***" . $e_hp;
    }

    $file_paths = $ma_item['file_path'];
    $file_names = $ma_item['file_name'];
    if(!empty($file_paths) && !empty($file_names))
    {
        $ma_item['file_paths'] = explode(',', $file_paths);
        $ma_item['file_names'] = explode(',', $file_names);
        $ma_item['file_origin_path'] = $file_paths;
        $ma_item['file_origin_name'] = $file_names;
    }

    $ma_item['process']     = "mod_inspection";
    $ma_item['cur_date']    = date("Y-m-d", strtotime("{$ma_item['regdate']}"));
    $ma_item['unit_list']   = $inspection_unit_list;
}
else
{
    $return_unit_sql    = "SELECT wcr.sku, wcr.quantity FROM work_cms_return_unit wcr LEFT JOIN product_cms_unit pcu ON pcu.`no`=wcr.option WHERE wcr.order_number='{$r_ord_no}' AND pcu.sup_c_no='{$add_c_no}' AND wcr.quantity > 0";
    $return_unit_query  = mysqli_query($my_db, $return_unit_sql);
    $return_unit_list   = [];
    while($return_unit  = mysqli_fetch_assoc($return_unit_query)) {
        $return_unit_list[] = $return_unit;
    }

    if(empty($return_unit_list)){
        exit("<script>alert('작성 권한이 없는 회수주문입니다.');location.href='out_return_qc_list.php'</script>");
    }

    $recipient_sc_hp    = "";
    if (isset($return_item['recipient_hp']) && !empty($return_item['recipient_hp'])) {
        $f_hp = substr($return_item['recipient_hp'], 0, 4);
        $e_hp = substr($return_item['recipient_hp'], 7, 15);
        $recipient_sc_hp = $f_hp . "***" . $e_hp;
    }

    $ma_item = array(
        "process"           => "add_inspection",
        "order_number"      => $r_ord_no,
        "add_c_no"          => $add_c_no,
        "unit_list"         => $return_unit_list,
        "recipient"         => $return_item['recipient'],
        "recipient_sc_hp"   => $recipient_sc_hp,
        "recipient_addr"    => $return_item['recipient_addr'],
        "zip_code"          => $return_item['zip_code'],
        "req_memo"          => $return_item['req_memo'],
        "reg_s_name"        => $session_name,
        "cur_date"          => date("Y-m-d"),
        "file_names"        => "",
        "file_paths"        => "",
    );
}

$file_count = !empty($ma_item['file_paths']) ? count($ma_item['file_paths']) : 0;
$smarty->assign("file_count", $file_count);

$smarty->assign("is_improve_option", getIsImproveOption());
$smarty->assign($ma_item);

$smarty->display('out_return_mi_regist.html');
?>
