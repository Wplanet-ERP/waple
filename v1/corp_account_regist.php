<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/company.php');
require('inc/helper/corporation.php');
require('inc/model/MyCompany.php');
require('inc/model/Corporation.php');

// 접근 권한
if (!permissionNameCheck($session_permission, "재무관리자")){
    $smarty->display('access_error.html');
    exit;
}

# 처음 변수 구성
$corp_model     = Corporation::Factory();
$co_no          = isset($_GET['co_no']) ? $_GET['co_no'] : "";
$corp_account   = [];
$title		    = "등록하기";

$process        = isset($_POST['process']) ? $_POST['process'] : "";
$regist_url     = "corp_account_regist.php";
$list_url       = "corp_account_management.php";

$sch_array_key  = array("sch_reg_name","sch_type","sch_my_c_no","sch_name","sch_display","sch_memo");
foreach($sch_array_key as $sch_key)
{
    $sch_val   = isset($_GET[$sch_key]) && !empty($_GET[$sch_key]) ? $_GET[$sch_key]: "";
    if(!!$sch_val)
    {
        $search_url .= "&{$sch_key}={$sch_val}";
    }
}
$smarty->assign("search_url", $search_url);


if($process == 'new_account') # 생성
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $ins_data = array(
        "type"      => isset($_POST['type']) ? $_POST['type'] : "",
        "my_c_no"   => isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "",
        "reg_s_no"  => isset($_POST['reg_s_no']) ? $_POST['reg_s_no'] : $session_s_no,
        "name"      => isset($_POST['name']) ? $_POST['name'] : "",
        "bank"      => isset($_POST['bank']) ? $_POST['bank'] : "",
        "account"   => isset($_POST['account']) ? $_POST['account'] : "",
        "holder"    => isset($_POST['holder']) ? $_POST['holder'] : "",
        "memo"      => isset($_POST['memo']) ? addslashes(trim($_POST['memo'])) : "",
        "regdate"   => date('Y-m-d H:i:s')
    );

    if($corp_model->insert($ins_data)){
        exit("<script>alert('입금/출금계좌 등록에 성공했습니다');location.href='corp_account_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('입금/출금계좌 등록에 실패했습니다');location.href='corp_account_regist.php?co_no={$co_no}&{$search_url}';</script>");
    }
}
elseif($process == 'modify_account') # 수정
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_data = array(
        "co_no"     => isset($_POST['co_no']) ? $_POST['co_no'] : "",
        "type"      => isset($_POST['type']) ? $_POST['type'] : "",
        "my_c_no"   => isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "",
        "name"      => isset($_POST['name']) ? $_POST['name'] : "",
        "bank"      => isset($_POST['bank']) ? $_POST['bank'] : "",
        "account"   => isset($_POST['account']) ? $_POST['account'] : "",
        "holder"    => isset($_POST['holder']) ? $_POST['holder'] : "",
        "memo"      => isset($_POST['memo']) ? addslashes(trim($_POST['memo'])) : ""
    );

    if($corp_model->update($upd_data)){
        exit("<script>alert('입금/출금계좌 수정에 성공했습니다');location.href='corp_account_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('입금/출금계좌 수정에 실패했습니다');location.href='corp_account_regist.php?co_no={$co_no}&{$search_url}';</script>");
    }
}
elseif($co_no)
{
    $title = "수정하기";
    $corp_account_sql   = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=ca.reg_s_no) as reg_s_name FROM corp_account `ca` WHERE `ca`.co_no = {$co_no} LIMIT 1";
    $corp_account_query = mysqli_query($my_db, $corp_account_sql);
    $corp_account 		= mysqli_fetch_assoc($corp_account_query);
}

if(!isset($corp_account['reg_s_name'])){
    $corp_account['reg_s_no']   = $session_s_no;
    $corp_account['reg_s_name'] = $session_name;
}

$my_company_model = MyCompany::Factory();
$my_company_model->setList();
$my_company_name_list = $my_company_model->getNameList();
$account_type_option  = getAccountTypeOption();

$smarty->assign("title", $title);
$smarty->assign($corp_account);
$smarty->assign('my_company_option', $my_company_name_list);
$smarty->assign('account_type_option', $account_type_option);
$smarty->assign('display_option', getDisplayOption());
$smarty->assign('bank_option', getBkTitleOption());

$smarty->display('corp_account_regist.html');
?>
