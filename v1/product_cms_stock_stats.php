<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/product_cms.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/ProductCmsUnit.php');

# Navigation & My Quick
$nav_prd_no  = "41";
$nav_title   = "재고 현황(통계)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 데이터
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$unit_model                 = ProductCmsUnit::Factory();
$sch_sup_c_list             = $unit_model->getDistinctUnitCompanyData('sup_c_no');
$company_model			    = Company::Factory();
$log_company_list     	    = $company_model->getLogisticsList();
$unit_type_option           = getUnitTypeOption();
$change_sum_unit_list       = getChangeSubUnitList();

# 기준일
$last_stock_sql     = "SELECT * FROM (SELECT DISTINCT stock_c_no, stock_date FROM product_cms_stock ORDER BY stock_date DESC) as date_result GROUP BY stock_c_no";
$last_stock_query   = mysqli_query($my_db, $last_stock_sql);
$last_stock_list    = [];
$last_chk_date      = "";
while($last_stock_result  = mysqli_fetch_assoc($last_stock_query)){
    if($last_stock_result['stock_c_no'] == '2809'){
        $last_chk_date = $last_stock_result['stock_date'];
    }
    $last_stock_list[$last_stock_result['stock_c_no']] = $last_stock_result['stock_date'];
}

# 검색조건
$add_where          = "1=1 AND IF(pcs.stock_c_no=2809,(pcs.warehouse LIKE '%정상%' OR pcs.warehouse LIKE '%세이프인%'), (pcs.warehouse LIKE '%창고%'))";
$check_sum          = isset($_GET['check_sum']) ? $_GET['check_sum'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_sup_c_no	    = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_option_name    = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_unit_type      = isset($_GET['sch_unit_type']) ? $_GET['sch_unit_type'] : "1";
$sch_log_company    = isset($_GET['sch_log_company']) ? $_GET['sch_log_company'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where      .= " AND `pcu`.brand = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where      .= " AND `pcu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

    $add_where      .= " AND `pcu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign('check_sum', $check_sum);
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

if(!empty($sch_sup_c_no)) {
    $add_where   .= " AND pcs.sup_c_no='{$sch_sup_c_no}'";
    $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
}

if(!empty($sch_option_name)) {
    $add_where   .= " AND pcu.option_name LIKE '%{$sch_option_name}%'";
    $smarty->assign("sch_option_name", $sch_option_name);
}

if (!empty($sch_unit_type)) {
    $add_where .= " AND pcu.`type` = '{$sch_unit_type}'";
    $smarty->assign("sch_unit_type", $sch_unit_type);
}

if (!empty($sch_log_company)) {
    $add_where .= " AND pcs.stock_c_no = '{$sch_log_company}'";
    $smarty->assign("sch_log_company", $sch_log_company);
}

$stock_date_sql     = "";
$stock_idx          = 1;
$stock_cnt          = count($last_stock_list);
$prev_month         = date("Y-m", strtotime("{$last_chk_date} -1 months"));
$chk_report_s_date  = $prev_month."-01";
$end_day            = date('t', strtotime($prev_month));
$chk_report_e_date  = $prev_month."-".$end_day;
$add_group_check    = ($check_sum == "1") ? "group_no, stock_c_no" : "prd_unit, stock_c_no";
foreach($last_stock_list as $log_c_no => $stock_date)
{
    $stock_date_sql .=
        empty($stock_date_sql) ?
            "
                SELECT 
                    gr.brand,
                    gr.prd_unit,
                    gr.group_no,
                    gr.stock_prd_sku AS sku,
                    gr.option_name,
                    gr.sup_c_no,
                    gr.stock_c_no,
                    SUM(gr.qty) AS qty,
                    gr.qty_alert,
                    gr.report_qty
                FROM (
            "
        : "";

    $stock_date_sql .= "(
        SELECT 
            pcs.*, 
            pcu.brand,  
            pcu.group_no,  
            pcu.`type` as unit_type,
            pcu.option_name,
            (SELECT SUM(pcum.qty_alert) FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcs.prd_unit AND pcum.log_c_no=pcs.stock_c_no) as qty_alert,
            (SELECT SUM(stock_qty) FROM product_cms_stock_report pcsr WHERE pcsr.prd_unit=pcs.prd_unit AND pcsr.state='판매반출' AND pcsr.confirm_state='9' AND pcsr.log_c_no=pcs.stock_c_no AND (pcsr.regdate BETWEEN '{$chk_report_s_date}' AND '{$chk_report_e_date}')) AS report_qty
        FROM product_cms_stock pcs 
        LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcs.prd_unit 
        WHERE {$add_where} AND stock_c_no='{$log_c_no}' AND stock_date='{$stock_date}'
    )";

    if($stock_idx == $stock_cnt){
        $stock_date_sql .= ") AS gr GROUP BY prd_unit, stock_c_no";
    }else{
        $stock_date_sql .= " UNION ";
    }

    $stock_idx++;
}
$smarty->assign("chk_report_s_date", $chk_report_s_date);
$smarty->assign("chk_report_e_date", $chk_report_e_date);

# 정렬기능
$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "exp_day";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "1";

$add_order    = "";
if(!empty($ord_type))
{
    if($ord_type_by == '1'){
        $orderby_val = "ASC";
    }else{
        $orderby_val = "DESC";
    }

    if($ord_type == "exp_day"){
        $add_order = "day_null DESC, {$ord_type} {$orderby_val}";
    }else{
        $add_order = "{$ord_type} {$orderby_val}";
    }
}
$add_order .= empty($add_order) ? "prd_unit ASC, stock_c_no ASC" : ",prd_unit ASC, stock_c_no ASC";

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);

# 리스트 쿼리
$stock_stats_sql      = "
    SELECT
        rs.*,
        (SELECT c.c_name FROM company c WHERE c.c_no=rs.brand) as brand_name,
        (SELECT c.c_name FROM company c WHERE c.c_no=rs.sup_c_no) as sup_c_name,
        (SELECT c.c_name FROM company c WHERE c.c_no=rs.stock_c_no) as stock_c_name,
        SUM(qty) as total_qty,
        IF(day_qty IS NULL OR day_qty=0, '1', '2') as day_null,
        (SUM(qty)/day_qty) as exp_day
    FROM
    (
        SELECT 
            pcs.brand,
            pcs.prd_unit,
            pcs.group_no,
            (SELECT pcu.option_name FROM product_cms_unit AS pcu WHERE pcu.no=pcs.group_no ) AS group_name,
            pcs.sku,
            pcs.option_name,
            pcs.sup_c_no,
            pcs.stock_c_no,
            SUM(pcs.qty) AS qty,
            (SELECT SUM(co.quantity) FROM commerce_order co LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no` WHERE co.option_no=pcs.prd_unit AND `cos`.sup_c_no=pcs.sup_c_no AND `cos`.display='1' AND `cos`.state='2' AND `co`.`type` IN('request','supply') ) AS co_qty,
            SUM(pcs.qty_alert) AS qty_alert,
            SUM(pcs.report_qty) AS report_qty,
            (SUM(pcs.report_qty)*-1) as ord_report_qty,
            ((SUM(pcs.report_qty)*-1)/{$end_day}) as day_qty
        FROM ({$stock_date_sql}) as pcs
        GROUP BY {$add_group_check}
    ) AS rs
    GROUP BY prd_unit, stock_c_no
    ORDER BY {$add_order}
";
$stock_stats_query = mysqli_query($my_db, $stock_stats_sql);
$stock_stats_list  = [];
while($stock_stats = mysqli_fetch_assoc($stock_stats_query))
{
    # 미입고수량 체크
    $co_sql   = "SELECT co.option_no, co.`type`, co.quantity FROM commerce_order co WHERE co.set_no IN(SELECT cos.no FROM commerce_order_set cos WHERE cos.log_c_no='{$stock_stats['stock_c_no']}' AND cos.sup_c_no='{$stock_stats['sup_c_no']}' AND `state`='2' AND display='1') AND co.option_no='{$stock_stats['prd_unit']}' AND co.type IN('request','supply')";
    $co_query = mysqli_query($my_db, $co_sql);
    $co_list  = [];
    while($co_result = mysqli_fetch_assoc($co_query)){
        $co_list[$co_result['type']] += $co_result['quantity'];
    }

    $stock_stats['yet_qty'] = 0;
    if(!empty($co_list)){
        $req_qty = isset($co_list['request']) ? $co_list['request'] : 0;
        $sup_qty = isset($co_list['supply']) ? $co_list['supply'] : 0;

        $yet_qty = ($req_qty > 0) ? $req_qty-$sup_qty : 0;
        if(!empty($yet_qty) && $yet_qty > 0){
            $stock_stats['yet_qty'] = $yet_qty;
        }
    }

    $stock_stats['option_name'] = ($check_sum == "1") ? $stock_stats['group_name'] : $stock_stats['option_name'];

    $stock_stats_list[$stock_stats['prd_unit']][$stock_stats['stock_c_no']] = $stock_stats;
}

# 마지막 업로드일
$last_upload_sql    = "SELECT upload_date FROM upload_management WHERE upload_type='1' ORDER BY regdate DESC LIMIT 1";
$last_upload_query  = mysqli_query($my_db, $last_upload_sql);
$last_upload_result = mysqli_fetch_assoc($last_upload_query);
$last_upload_date   = isset($last_upload_result['upload_date']) ? $last_upload_result['upload_date'] : "";

$smarty->assign('last_upload_date', $last_upload_date);
$smarty->assign("sch_sup_c_list", $sch_sup_c_list);
$smarty->assign('unit_type_option', $unit_type_option);
$smarty->assign('log_company_list', $log_company_list);
$smarty->assign('stock_stats_list', $stock_stats_list);

$smarty->display('product_cms_stock_stats.html');
?>
