<?php
ini_set('display_errors', -1);
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_price.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');
require('inc/model/ProductCmsUnit.php');

# Navigation & My Quick
$nav_prd_no  = "38";
$nav_title   = "커머스 발주원가 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 변수, 리스트
$unit_model 			= ProductCmsUnit::Factory();
$team_model				= Team::Factory();
$total_sup_c_list 		= $unit_model->getDistinctUnitCompanyData("sup_c_no");
$sch_sup_c_list 		= $total_sup_c_list;
$sch_brand_list 		= $unit_model->getDistinctUnitCompanyData("brand");
$team_full_name_list	= $team_model->getTeamFullNameList();
$team_all_list			= $team_model->getTeamAllList();
$staff_team_list		= $team_all_list['staff_list'];
$sch_staff_list 		= $staff_team_list['all'];

# 검색쿼리
$add_where 		 = " 1=1 ";
$sch_no 	   	 = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_team 	  	 = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no 	  	 = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$sch_sup_c_no 	 = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no']:"";
$sch_brand  	 = isset($_GET['sch_brand']) ? $_GET['sch_brand']:"";
$sch_option_name = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_prev_month	 = date("Y-m", strtotime("-3 months"));
$sch_prev_s_date = "{$sch_prev_month}-01";
$sch_prev_e_day	 = date("t", strtotime($sch_prev_month));
$sch_prev_e_date = "{$sch_prev_month}-{$sch_prev_e_day}";

#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
$url_check_str  = $_SERVER['QUERY_STRING'];
if(empty($url_check_str)){
	$url_check_team_where   = getTeamWhere($my_db, $session_team);
	$url_check_sql    		=  "SELECT count(*) as cnt FROM product_cms_unit_cost_control as pcucc LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcucc.prd_unit WHERE (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$url_check_team_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$url_check_team_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$url_check_team_where})))";
	$url_check_query  		= mysqli_query($my_db, $url_check_sql);
	$url_check_result 		= mysqli_fetch_assoc($url_check_query);

	if($url_check_result['cnt'] == 0){
		$sch_team = "all";
	}
}

if (!empty($sch_no)) {
	$add_where .= " AND pcucc.prd_unit = '{$sch_no}'";
	$smarty->assign("sch_no", $sch_no);
}

$sch_team_code_where = "";
if (!empty($sch_team))
{
	if ($sch_team != "all") {
		$sch_team_code_where = getTeamWhere($my_db, $sch_team);
		$add_where 		 .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where})))";
		$sch_sup_c_list   = $unit_model->getTeamSupList($sch_team);
		$sch_staff_list   = $staff_team_list[$sch_team];
	}
	$smarty->assign("sch_team", $sch_team);
}else{
	$sch_team_code_where = getTeamWhere($my_db, $session_team);
	$add_where 		 .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$sch_team_code_where})))";
	$sch_sup_c_list   = $unit_model->getTeamSupList($session_team);
	$sch_staff_list   = $staff_team_list[$session_team];
	$smarty->assign("sch_team", $session_team);
}

if (!empty($sch_s_no))
{
	if ($sch_s_no != "all") {
		$add_where 		 .= " AND (pcu.ord_s_no = '{$sch_s_no}' OR pcu.ord_sub_s_no='{$sch_s_no}' OR pcu.ord_third_s_no='{$sch_s_no}')";
	}
	$smarty->assign("sch_s_no", $sch_s_no);
}else{
	if($sch_team == $session_team){
		$add_where		 .=" AND (pcu.ord_s_no = '{$session_s_no}' OR pcu.ord_sub_s_no = '{$session_s_no}' OR pcu.ord_third_s_no = '{$session_s_no}')";
		$smarty->assign("sch_s_no", $session_s_no);
	}
}

if (!empty($sch_sup_c_no)) {
	$add_where .= " AND pcucc.sup_c_no = '{$sch_sup_c_no}'";
	$smarty->assign("sch_sup_c_no", $sch_sup_c_no);
}

if (!empty($sch_brand)) {
	$add_where .= " AND pcu.brand = '{$sch_brand}'";
	$smarty->assign("sch_brand", $sch_brand);
}

if (!empty($sch_option_name)) {
	$add_where .= " AND pcu.option_name like '%{$sch_option_name}%'";
	$smarty->assign("sch_option_name", $sch_option_name);
}

# 리스트 쿼리
$product_cms_unit_cost_sql = "
	SELECT
	   	pcucc.cc_no,
		pcucc.prd_unit,
		pcu.brand,
		pcu.ord_s_no,
		pcu.ord_sub_s_no,
		pcu.ord_third_s_no,
		pcucc.sup_c_no,
		(SELECT s.s_name FROM staff s WHERE pcu.ord_s_no=s.s_no) as ord_s_name,
		pcu.option_name,
	    pcucc.cost_date,
	    pcucc.sup_price_vat,
	    (SELECT c.license_type FROM company c WHERE pcu.sup_c_no=c.c_no) as license_type,
	    COUNT(pcucc.cc_no) as cc_cnt
	FROM product_cms_unit_cost_control as pcucc
	LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcucc.prd_unit
	WHERE {$add_where} AND pcu.is_control='1'
	GROUP BY pcucc.prd_unit
	ORDER BY pcucc.prd_unit ASC
";
$product_cms_unit_cost_query 	= mysqli_query($my_db, $product_cms_unit_cost_sql);
$product_cms_unit_cost_tmp_list = [];
$product_cms_unit_sort_list  	= [];
$product_cms_unit_per_list  	= [];
$product_cms_unit_price_list  	= [];
$product_cms_unit_stock_list	= [];
$product_cms_unit_cost_list 	= [];
while($product_cms_unit_cost = mysqli_fetch_assoc($product_cms_unit_cost_query))
{
	$prd_unit		= $product_cms_unit_cost['prd_unit'];
	$is_usd_price	= ($product_cms_unit_cost['license_type'] == '4') ? true : false;
	$sup_price 		= ($is_usd_price) ? getUsdFormatPrice($product_cms_unit_cost['sup_price_vat']) : getKrwFormatPrice($product_cms_unit_cost['sup_price_vat']);
	$prev_per		= 0;
	$prev_price		= 0;
	$prev_price_text= "0";
	$prev_stock		= 0;

	$unit_cost_data = array(
		"cc_no" 			=> $product_cms_unit_cost['cc_no'],
		"prd_unit" 			=> $prd_unit,
		"brand" 	  		=> $sch_brand_list[$product_cms_unit_cost['brand']],
		"sup_c_name"		=> $sch_sup_c_list[$product_cms_unit_cost['sup_c_no']],
		"ord_s_no"			=> $product_cms_unit_cost['ord_s_no'],
		"ord_s_name"		=> $product_cms_unit_cost['ord_s_name'],
		"ord_sub_s_no"		=> $product_cms_unit_cost['ord_sub_s_no'],
		"ord_third_s_no"	=> $product_cms_unit_cost['ord_third_s_no'],
		"option_name" 		=> $product_cms_unit_cost['option_name'],
		"cost_date"			=> $product_cms_unit_cost['cost_date'],
		"is_usd_price"		=> $is_usd_price,
		"sup_price"			=> $sup_price,
		"prev_price"		=> $prev_price,
		"prev_price_text" 	=> $prev_price_text,
		"prev_per"		 	=> $prev_per,
		"prev_stock"		=> $prev_stock
	);

	if($product_cms_unit_cost['cc_cnt'] > 1)
	{
		$chk_sql 	 = "SELECT pcucc.cc_no, pcucc.prd_unit, pcucc.sup_price_vat, pcucc.cost_date FROM product_cms_unit_cost_control as pcucc WHERE prd_unit='{$prd_unit}' ORDER BY cost_date DESC, cc_no DESC LIMIT 2";
		$chk_query 	 = mysqli_query($my_db, $chk_sql);
		$chk_idx 	 = 0;
		$first_price = 0;
		while($chk_result = mysqli_fetch_assoc($chk_query))
		{
			$sup_price_vat = $chk_result["sup_price_vat"];
			if($chk_idx == 0){
				$unit_cost_data['cc_no'] 	 = $chk_result['cc_no'];
				$unit_cost_data['cost_date'] = $chk_result['cost_date'];
				$unit_cost_data['sup_price'] = ($is_usd_price) ? getUsdFormatPrice($sup_price_vat) : getKrwFormatPrice($sup_price_vat);
				$first_price = $sup_price_vat;
			}elseif($chk_idx == 1){
				$prev_price 		= $first_price-$sup_price_vat;
				$prev_price_text	= ($first_price-$sup_price_vat)*-1;
				$prev_per   		= round((1-($sup_price_vat/$first_price))*100, 2);
				break;
			}
			$chk_idx++;
		}
	}

	# 반출 합계 계산
	$prev_move_sql     	= "SELECT SUM(out_qty) as out_total FROM product_cms_stock_transfer pcst WHERE pcst.prd_unit='{$prd_unit}' AND (pcst.move_date BETWEEN '{$sch_prev_s_date}' AND '{$sch_prev_e_date}')";
	$prev_move_query	= mysqli_query($my_db, $prev_move_sql);
	$prev_move_result 	= mysqli_fetch_assoc($prev_move_query);
	$prev_move_val		= isset($prev_move_result["out_total"]) ? $prev_move_result["out_total"] : 0;

	$prev_stock_sql   	= "SELECT move_no, stock_qty FROM product_cms_stock_report pcsr WHERE pcsr.prd_unit='{$prd_unit}' AND (pcsr.regdate BETWEEN '{$sch_prev_s_date}' AND '{$sch_prev_e_date}') AND pcsr.confirm_state IN(6,8,9,10) AND pcsr.state NOT IN('기간반품입고','기간판매반출')";
	$prev_stock_query 	= mysqli_query($my_db, $prev_stock_sql);
	$prev_stock_val		= 0;
	while($prev_stock_result 	= mysqli_fetch_assoc($prev_stock_query)){
		if($prev_stock_result['move_no'] == 0){
			$prev_stock_val += $prev_stock_result['stock_qty'];
		}
	}
	$prev_stock	= $prev_move_val+($prev_stock_val*-1);

	$unit_cost_data["prev_stock"] 		= $prev_stock;
	$unit_cost_data["prev_price"] 		= $prev_price;
	$unit_cost_data["prev_price_text"] 	= $prev_price_text;
	$unit_cost_data["prev_per"] 		= $prev_per;

	$product_cms_unit_cost_tmp_list[$prd_unit]	= $unit_cost_data;
	$product_cms_unit_per_list[$prd_unit] 	 	= $prev_per;
	$product_cms_unit_price_list[$prd_unit] 	= $prev_price;
	$product_cms_unit_stock_list[$prd_unit] 	= $prev_stock;
}

$ord_type       = isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";

if(!empty($ord_type) && !empty($ord_type_by))
{
	if($ord_type == "prev_price")
	{
		if($ord_type_by == "ASC") {
			asort($product_cms_unit_price_list);
		} elseif($ord_type_by == "DESC") {
			arsort($product_cms_unit_price_list);
		}

		foreach($product_cms_unit_price_list as $unit_no => $unit_price){
			$product_cms_unit_cost_list[] = $product_cms_unit_cost_tmp_list[$unit_no];
		}
	}
	elseif($ord_type == "prev_per")
	{
		if($ord_type_by == "ASC") {
			asort($product_cms_unit_per_list);
		} elseif($ord_type_by == "DESC") {
			arsort($product_cms_unit_per_list);
		}

		foreach($product_cms_unit_per_list as $unit_no => $unit_price){
			$product_cms_unit_cost_list[] = $product_cms_unit_cost_tmp_list[$unit_no];
		}
	}
	elseif($ord_type == "prev_stock")
	{
		if($ord_type_by == "ASC") {
			asort($product_cms_unit_stock_list);
		} elseif($ord_type_by == "DESC") {
			arsort($product_cms_unit_stock_list);
		}

		foreach($product_cms_unit_stock_list as $unit_no => $unit_price){
			$product_cms_unit_cost_list[] = $product_cms_unit_cost_tmp_list[$unit_no];
		}
	}
}else{
	$product_cms_unit_cost_list = $product_cms_unit_cost_tmp_list;
}
$smarty->assign("ord_type", $ord_type);
$smarty->assign("ord_type_by", $ord_type_by);
$smarty->assign("sch_prev_month", $sch_prev_month);

$smarty->assign("product_cms_unit_cost_list", $product_cms_unit_cost_list);
$smarty->assign("sch_sup_c_list", $sch_sup_c_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

$smarty->display('product_cms_unit_cost_list.html');

?>
