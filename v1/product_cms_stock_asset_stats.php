<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');

$proc = (isset($_POST['process'])) ? $_POST['process'] : "";

if ($proc == "f_is_order") {
    $no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE product_cms_stock_report SET is_order = '{$value}' WHERE `no` = '{$no}'";

    if (mysqli_query($my_db, $sql)){
        echo "발주서 입고가 저장 되었습니다.";
    }else{
        echo "발주서 입고 저장에 실패 하였습니다.";
    }

    exit;
}

# Navigation & My Quick
$nav_prd_no  = "46";
$nav_title   = "재고 자산 현황";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$add_where      = "1=1 AND (pcs.warehouse LIKE '%정상창고' OR pcs.warehouse LIKE '%세이프인')";
$add_label_where= "";

$sch_ord_s_no	= isset($_GET['sch_ord_s_no']) ? $_GET['sch_ord_s_no'] : $session_s_no;
$sch_sup_c_no	= isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_center	    = isset($_GET['sch_center']) ? $_GET['sch_center'] : "";
$sch_warehouse	= isset($_GET['sch_warehouse']) ? $_GET['sch_warehouse'] : "";
$sch_s_date 	= isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date("Y-m", strtotime("-6 month")); // 월간
$sch_e_date 	= isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date("Y-m"); // 월간

$unit_model     = ProductCmsUnit::Factory();
$stock_model    = ProductCmsStock::Factory();
$sup_c_list     = $unit_model->getDistinctUnitCompanyData('sup_c_no');
$sup_ord_c_list = $unit_model->getDistinctUnitOrdCompanyData("sup_c_no");
$ord_s_list     = $unit_model->getOrdStaffData();
$warehouse_list = $stock_model->getWarehouseList();
$center_list    = $stock_model->getStockCenterList();
$sch_sup_c_list = $sup_c_list;

if(!empty($sch_ord_s_no))
{
    if(isset($sup_ord_c_list[$sch_ord_s_no])){
        $sch_sup_c_list     = isset($sup_ord_c_list[$sch_ord_s_no]) ? $sup_ord_c_list[$sch_ord_s_no] : $sup_c_list;
        $add_where         .= " AND pcs.ord_s_no='{$sch_ord_s_no}'";
        $add_label_where   .= "pcs.ord_s_no='{$sch_ord_s_no}' AND ";
        $smarty->assign("sch_ord_s_no", $sch_ord_s_no);
    }
}

if(!empty($sch_sup_c_no))
{
    $add_where   .= " AND pcs.sup_c_no='{$sch_sup_c_no}'";
    $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
}

if(!empty($sch_center))
{
    $add_where   .= " AND pcs.stock_center='{$sch_center}'";
    $smarty->assign("sch_center", $sch_center);
}

if(!empty($sch_warehouse))
{
    $add_where   .= " AND pcs.warehouse='{$sch_warehouse}'";
    $smarty->assign("sch_warehouse", $sch_warehouse);
}

if(!empty($sch_s_date) && !empty($sch_e_date))
{
    $smarty->assign("sch_s_date", $sch_s_date);
    $smarty->assign("sch_e_date", $sch_e_date);
}


# Y LABEL 체크
$stats_list     = [];
$sup_c_init     = [];
$y_label_sql    = "SELECT DISTINCT sup_c_no, REPLACE((SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=pcs.sup_c_no), '(주)','') as sup_c_name FROM product_cms_stock pcs WHERE {$add_label_where} DATE_FORMAT(stock_date, '%Y-%m') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ORDER BY sup_c_name";
$y_label_query  = mysqli_query($my_db, $y_label_sql);

while($y_label = mysqli_fetch_assoc($y_label_query))
{
    $y_label_list[$y_label['sup_c_no']] = $y_label['sup_c_name'];
    $sup_c_init[$y_label['sup_c_no']] = array('title' => $y_label['sup_c_name'], "stock_date" => "", 'total_qty' => 0);
}

# X LABEL
$all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
$all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
$all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m')";
$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as `Date` FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	GROUP BY chart_key
	ORDER BY chart_key
";
$x_label_list = [];
$x_table_th_list = [];
$last_stock_date_list = [];
if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    while($date = mysqli_fetch_array($all_date_query))
    {
        $stats_list[$date['chart_key']]      = $sup_c_init;
        $chart_title                         = $date['chart_title'];
        $x_label_list[$date['chart_key']]    = "'".$chart_title."'";
        $x_table_th_list[$date['chart_key']] = $chart_title;

        $check_last_s_date = $chart_title."-01";
        $check_last_e_date = $chart_title."-31";
        $check_last_date_sql    = "SELECT DISTINCT stock_date FROM product_cms_stock WHERE stock_date BETWEEN '{$check_last_s_date}' AND '{$check_last_e_date}' ORDER BY stock_date DESC LIMIT 1";
        $check_last_date_query  = mysqli_query($my_db, $check_last_date_sql);
        $check_last_date_result = mysqli_fetch_assoc($check_last_date_query);
        $check_last_date        = isset($check_last_date_result['stock_date']) ? $check_last_date_result['stock_date'] : "";

        if(!empty($check_last_date)){
            $last_stock_date_list[] = "'".$check_last_date."'";
        }
    }
}

# 특정 날짜만 재고 확인
$last_stock_date = implode(',', $last_stock_date_list);
$stats_sql = "
    SELECT
        sup_c_no,
        DATE_FORMAT(stock_date, '%Y%m') as stock_month,
        stock_date,
        SUM(qty) as total_qty
    FROM product_cms_stock as pcs
    WHERE {$add_where} AND stock_date IN({$last_stock_date})
    GROUP BY stock_date, sup_c_no
    ORDER BY stock_date ASC
";
$stats_query = mysqli_query($my_db, $stats_sql);
$total_qty_list = [];
while($stats = mysqli_fetch_assoc($stats_query))
{
    $stats_list[$stats["stock_month"]][$stats["sup_c_no"]]["total_qty"] += $stats['total_qty'];
    $stats_list[$stats["stock_month"]][$stats["sup_c_no"]]["stock_date"] = $stats['stock_date'];
    $total_qty_list[$stats['stock_month']] = 0;
}

$full_data = [];
foreach($stats_list as $stock_date => $stock_date_data)
{
    if($stock_date)
    {
        $total_qty_sum  = 0;

        foreach ($stock_date_data as $key => $stock_data)
        {
            $stock_unit_date  = $stock_data["stock_date"];
            $stock_unit_total = $stock_data["total_qty"];
            $stock_unit_total_price = 0;

            if ($stock_unit_total > 0)
            {
                $stock_month_qty_sql = "SELECT pcs.prd_unit, SUM(pcs.qty) as stock_qty FROM product_cms_stock as pcs WHERE {$add_where} AND pcs.stock_date='{$stock_unit_date}' GROUP BY prd_unit";
                $stock_month_qty_query = mysqli_query($my_db, $stock_month_qty_sql);
                $stock_month_qty_list = [];
                while($stock_month_qty_result = mysqli_fetch_assoc($stock_month_qty_query))
                {
                    $stock_month_qty_list[$stock_month_qty_result['prd_unit']] = $stock_month_qty_result['stock_qty'];
                }

                $stock_chk_sql = "SELECT pcsr.prd_unit, (SELECT co.quantity FROM commerce_order co WHERE co.no=pcsr.ord_no and co.stock_no=pcsr.no) as unit_qty, (SELECT co.unit_price FROM commerce_order co WHERE co.no=pcsr.ord_no and co.stock_no=pcsr.no) as price FROM product_cms_stock_report pcsr WHERE pcsr.state='발주입고' AND pcsr.regdate <= '{$stock_unit_date}' AND pcsr.sup_c_no='{$key}' ORDER BY regdate DESC";
                $stock_chk_query = mysqli_query($my_db, $stock_chk_sql);
                while ($stock_chk_result = mysqli_fetch_assoc($stock_chk_query))
                {
                    $stock_chk_total = 0;
                    $stock_chk_price = (!isset($stock_chk_result['price']) || empty($stock_chk_result['price'])) ? 0 : $stock_chk_result['price'];
                    $stock_month_qty = isset($stock_month_qty_list[$stock_chk_result['prd_unit']]) ? $stock_month_qty_list[$stock_chk_result['prd_unit']] : 0;
                    $stock_unit_qty  =  (!isset($stock_chk_result['unit_qty']) || empty($stock_chk_result['unit_qty'])) ? 0 : $stock_chk_result['unit_qty'];
                    $stock_chk_qty   = $stock_month_qty-$stock_unit_qty;

                    if($stock_chk_qty > 0){
                        $stock_chk_total = $stock_unit_qty * $stock_chk_price;
                        $stock_unit_total -= $stock_unit_qty;
                        $stock_month_qty_list[$stock_chk_result['prd_unit']] -= $stock_unit_qty;
                    }elseif($stock_month_qty > 0){
                        $stock_chk_total = $stock_month_qty * $stock_chk_price;
                        $stock_unit_total -= $stock_month_qty;
                        $stock_month_qty_list[$stock_chk_result['prd_unit']] -= $stock_month_qty;
                    }

                    $stock_unit_total_price += $stock_chk_total;

                    if ($stock_unit_total < 0) {
                        break;
                    }
                }
            }

            $full_data["each"][$key]['title'] = $stock_data["title"];
            $full_data["each"][$key]['data'][] = round($stock_unit_total_price, 2);

            $total_qty_sum += $stock_unit_total_price;
        }

        $full_data["sum"]['total']['title']  = "합산";
        $full_data["sum"]['total']['data'][] = $total_qty_sum;
    }else{
        $full_data = [];
    }
}


$smarty->assign("sch_ord_s_list", $ord_s_list);
$smarty->assign("sch_sup_c_list", $sch_sup_c_list);
$smarty->assign("sch_warehouse_list", $warehouse_list);
$smarty->assign("sch_center_list", $center_list);

$smarty->assign('full_data', json_encode($full_data));
$smarty->assign('x_label_list', implode(',', $x_label_list));
$smarty->assign('x_table_th_list', json_encode($x_table_th_list));
$smarty->assign('legend_list', json_encode($y_label_list));
$smarty->assign('picker_list', $y_label_list);


# 30일 이내 미입고처리건
$prev_month_day = date('Y-m-d', strtotime('-180 days'));
$stock_report_yet_sql = "SELECT pcsr.no, pcsr.is_order, pcsr.memo, pcsr.regdate, pcsr.sku, pcsr.stock_type, pcsr.stock_qty, (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.log_c_no) as log_c_name, (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.sup_c_no) as sup_c_name from product_cms_stock_report pcsr WHERE pcsr.ord_no=0 AND pcsr.state='정상입고' AND pcsr.regdate >= '2022-02-22' AND pcsr.regdate > '{$prev_month_day}' AND pcsr.is_order='1'";
$stock_report_yet_query = mysqli_query($my_db, $stock_report_yet_sql);

$yet_stock_report_list = [];
while($stock_report = mysqli_fetch_assoc($stock_report_yet_query))
{
    $yet_stock_report_list[] = $stock_report;
}

$smarty->assign('yet_stock_report_list', $yet_stock_report_list);

$smarty->display('product_cms_stock_asset_stats.html');
?>
