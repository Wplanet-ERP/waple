<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');
require('inc/model/Evaluation.php');

// 접근 제한
if(!(permissionNameCheck($session_permission, "대표") || $session_s_no == '28')){ // 대표 및 김영경 외 접근불가
	$smarty->display('access_error.html');
	exit;
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);


$ev_no 		= isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$ev_model 	= Evaluation::Factory();
$ev_item	= $ev_model->getItem($ev_no);

$sheet = $objPHPExcel->setActiveSheetIndex(0);
$sheet->getDefaultStyle()->getFont()
->setName('맑은 고딕')
->setSize(9);

//상단타이틀
$sheet->setCellValue('A1', "ev_result_no");
$sheet->setCellValue('B1', "평가받는사람");
$sheet->setCellValue('C1', "평가하는사람");
$sheet->setCellValue('D1', "질문");
$sheet->setCellValue('E1', "평가내용(원본)");
$sheet->setCellValue('F1', "평가내용(확정)");

// 평가 시스템 쿼리
$evaluation_system_sql = "
	SELECT
	    `esr`.ev_result_no,
	    `esr`.receiver_s_no,
		(SELECT s.s_name FROM staff s WHERE s.s_no=esr.receiver_s_no) as re_s_name,
		(SELECT s.s_name FROM staff s WHERE s.s_no=esr.evaluator_s_no) as ev_s_name,
		(SELECT eu.question FROM evaluation_unit eu WHERE eu.ev_u_no=`esr`.ev_u_no) as question,
		`esr`.evaluation_value
	FROM evaluation_system_result as `esr`
	WHERE `esr`.ev_no='{$ev_no}' AND `esr`.evaluation_state='5' AND (evaluation_value IS NOT NULL AND evaluation_value != '')
	ORDER BY re_s_name ASC, ev_s_name ASC
";
$evaluation_system_result = mysqli_query($my_db, $evaluation_system_sql);
$idx = 2;
$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(20);
while($er = mysqli_fetch_array($evaluation_system_result))
{
	$ev_s_name = ($er['receiver_s_no'] == '28') ? "" : $er['ev_s_name'];
    $objPHPExcel->getActiveSheet()
		->setCellValue("A{$idx}", $er['ev_result_no'])
		->setCellValue("B{$idx}", $er['re_s_name'])
		->setCellValue("C{$idx}", $ev_s_name)
		->setCellValue("D{$idx}", $er['question'])
		->setCellValue("E{$idx}", $er['evaluation_value'])
	;

	$objPHPExcel->getActiveSheet()->getRowDimension($idx)->setRowHeight(20);
    $idx++;
}
$idx--;

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);

$objPHPExcel->getActiveSheet()->getStyle("A1:F{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("E2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
$objPHPExcel->getActiveSheet()->setTitle('평가의견 리스트');

$excel_title = str_replace('/', '', $ev_item['subject']);

$objPHPExcel->setActiveSheetIndex(0);
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="'.$excel_title.'_평가의견 리스트.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
