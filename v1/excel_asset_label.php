<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");


//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "no")
	->setCellValue('B1', "management")
	->setCellValue('C1', "name")
	->setCellValue('D1', "qr_code")
;

# 검색조건
$as_s_no = isset($_GET['as_s_no']) ? $_GET['as_s_no'] : "";
$as_e_no = isset($_GET['as_e_no']) ? $_GET['as_e_no'] : "";

$asset_sql = "
	SELECT
		as_no,
		management,
		`name`,
		qr_code
	FROM asset
	WHERE (as_no >='{$as_s_no}' AND as_no <= '{$as_e_no}') AND display='1' AND qr_code is not null AND qr_code != ''
	ORDER BY as_no ASC
";

$result	= mysqli_query($my_db, $asset_sql);
$idx = 2;
if(!!$result)
{
    while($asset = mysqli_fetch_array($result))
    {
		$file_name    = str_replace('uploads/qr_code/','',$asset['qr_code']);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$idx, $asset['as_no'])
            ->setCellValue('B'.$idx, $asset['management'])
            ->setCellValue('C'.$idx, $asset['name'])
            ->setCellValue('D'.$idx, $file_name)
		;

        $idx++;
    }
}
$idx--;

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');


$objPHPExcel->getActiveSheet()->getStyle("A1:D{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A1:A'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('B1:B'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('D1:D'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$idx)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(45);

$objPHPExcel->getActiveSheet()->setTitle('sheet 1');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',"자산 라벨정보.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
