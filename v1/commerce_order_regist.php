<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/commerce_order.php');
require('inc/model/Company.php');
require('inc/model/CommerceOrder.php');

# Model 설정
$comm_set_model = CommerceOrder::Factory();

# 기본 변수 및 프로세스 처리
$no	  			= isset($_GET['no']) ? $_GET['no'] : "";
$process 		= (isset($_POST['process'])) ? $_POST['process'] : "";

if ($process == "f_state")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	$comm_item	= $comm_set_model->getItem($no);
	$upd_data 	= array("state" => $value, "no" => $no);

	if($value == '3'){
		if(empty($comm_item['req_date'])){
			$upd_data['req_date'] = date("Y-m-d");
		}
	}

	if ($comm_set_model->update($upd_data)) {
		echo "진행상태가 저장 되었습니다.";
	}else{
		echo "진행상태 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_title")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($comm_set_model->update(array("no" => $no, "title" => $value))){
		echo "발주제목이 저장 되었습니다.";
	}else{
		echo "발주제목 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_order_count")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($comm_set_model->update(array("no" => $no, "order_count" => $value))){
		echo "차수가 저장 되었습니다.";
	}else{
		echo "차수 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_log_c_no")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($comm_set_model->update(array("no" => $no, "log_c_no" => $value))){
		echo "물류업체가 저장 되었습니다.";
	}else{
		echo "물류업체ㅔ에 실패 하였습니다.";
	}
	exit;
}
else
{
	$search_url = getenv("QUERY_STRING");
	if(substr($search_url, 0, 2) == 'no'){ // 첫번째 값(no) 제거하기
		if(strpos($search_url,"&"))
			$search_url = substr($search_url, strpos($search_url,"&")+1);
		else
			$search_url = "";
	}

	$sch_stock_link = isset($_GET['stock_link']) ? $_GET['stock_link'] : "";
	if($sch_stock_link == '1'){
		$smarty->assign("sch_stock_link", $sch_stock_link);
	}

	if(empty($no)){
		exit("<script>alert('발주서 번호가 없습니다.');location.href='commerce_order.php?{$search_url}';</script>");
	}

	# 발주서 정보 Start
	$commerce_order_set_sql="
		SELECT
			cos.no,
			cos.state,
			cos.title,
			cos.order_count,
		   	cos.log_c_no,
		   	cos.sup_c_no,
			cos.sup_c_name,
		   	cos.sup_loc_type,
			cos.req_s_no
		FROM commerce_order_set cos
		WHERE cos.no='{$no}'
	";

	$commerce_order_set_query  = mysqli_query($my_db, $commerce_order_set_sql);
	$commerce_order_set_result = mysqli_fetch_array($commerce_order_set_query);

	$smarty->assign($commerce_order_set_result);
}

$company_model		= Company::Factory();
$log_company_list 	= $company_model->getLogisticsList();

$smarty->assign("search_url", $search_url);
$smarty->assign("work_state_list", getCommerceWorkStateOption());
$smarty->assign("location_type_option", getLocationTypeOption());
$smarty->assign("log_company_list", $log_company_list);

$smarty->display('commerce_order_regist.html');
?>
