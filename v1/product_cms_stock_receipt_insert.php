<?php
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');

# 파일 변수
$receipt_month  = (isset($_POST['receipt_month'])) ? $_POST['receipt_month'] : date('Y-m', strtotime('-1 months'));
$search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
$file_name      = $_FILES["receipt_file"]["tmp_name"];

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$stock_confirm  = ProductCmsStock::Factory();
$unit_model     = ProductCmsUnit::Factory();
$upd_data       = [];
$empty_sku      = [];
$with_log_c_no  = '2809';

for ($i = 4; $i <= $totalRow; $i++)
{
    $check          = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 체크
    $product_name   = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 상품명
    $warehouse      = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 물류창고
    $qty            = (int)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 수량

    if(empty($check)){
        break;
    }

    $unit_item = $unit_model->getSkuItem($product_name, $with_log_c_no);

    if(!empty($unit_item))
    {
        $chk_sql    = "SELECT `no` FROM product_cms_stock_confirm WHERE `base_type`='start' AND base_mon = '{$receipt_month}' AND sku='{$product_name}' AND warehouse='{$warehouse}'";
        $chk_query  = mysqli_query($my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        if(isset($chk_result['no']) && !empty($chk_result['no'])){
            $upd_data[] = array('no' => $chk_result['no'], 'qty' => $qty);
        }else{
            if($qty > 0){
                echo "수불부 등록안된 구성품 입니다. {$product_name}";
                exit;
            }
        }

    }else{
        if($qty > 0){
            $empty_sku[$product_name] = $product_name;
        }
    }
}

$stock_confirm->setMainInit("product_cms_stock_confirm", 'no');

if (!$stock_confirm->multiUpdate($upd_data)){
    echo "기초재고 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 임태형<br/>";
    exit;
}else{
    exit("<script>alert('기초재고가 반영 되었습니다.');location.href='product_cms_stock_receipt_list.php?{$search_url}';</script>");
}

?>
