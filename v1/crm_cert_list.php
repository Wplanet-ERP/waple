<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_message.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "224";
$nav_title   = "필터 교체 알림 서비스";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 옵션
$sch_status_option      = getCertStatusOption();
$sch_prd_option         = getCertPrdOption();
$sch_prd_detail_option  = getCertPrdDetailOption();

$add_where      = "1=1 AND `ccp`.active != 2";
$sch_no         = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_status     = isset($_GET['sch_status']) ? $_GET['sch_status'] : "";
$sch_name       = isset($_GET['sch_name']) ? $_GET['sch_name'] : "";
$sch_hp         = isset($_GET['sch_hp']) ? $_GET['sch_hp'] : "";
$sch_prd_no     = isset($_GET['sch_prd_no']) ? $_GET['sch_prd_no'] : "";
$sch_option     = isset($_GET['sch_option']) ? $_GET['sch_option'] : "";


if(!empty($sch_no)){
    $add_where .= " AND `cc`.`no`='{$sch_no}'";
    $smarty->assign("sch_no", $sch_no);
}

if(!empty($sch_status)){
    $add_where .= " AND `cc`.`status`='{$sch_status}'";
    $smarty->assign("sch_status", $sch_status);
}

if(!empty($sch_name)){
    $add_where .= " AND `cc`.`name` LIKE '%{$sch_name}%'";
    $smarty->assign("sch_name", $sch_name);
}

if(!empty($sch_hp)){
    $add_where .= " AND `cc`.`hp` LIKE '%{$sch_hp}%'";
    $smarty->assign("sch_hp", $sch_hp);
}

if(!empty($sch_prd_no)){
    $add_where .= " AND `ccp`.`prd_no`='{$sch_prd_no}'";
    $smarty->assign("sch_prd_no", $sch_prd_no);
}

if(!empty($sch_option)){
    $sch_option_tmp = explode("-", $sch_option);
    $prd_no_tmp     = $sch_option_tmp[0];
    $option_no_tmp  = isset($sch_option_tmp[1]) ? $sch_option_tmp[1] : 1;

    $add_where .= " AND `ccp`.`prd_no`='{$prd_no_tmp}' AND `ccp`.prd_type='{$option_no_tmp}'";
    $smarty->assign("sch_option", $sch_option);
}

# 전체 게시물 수
$crm_cert_cnt_sql       = "SELECT COUNT(DISTINCT `cc`.no) as cnt FROM crm_cert AS `cc` LEFT JOIN crm_cert_product AS `ccp` ON `cc`.`no`=`ccp`.cert_no WHERE {$add_where} GROUP BY `cc`.no";
$crm_cert_cnt_query     = mysqli_query($my_db, $crm_cert_cnt_sql);
$crm_cert_cnt_result    = mysqli_fetch_array($crm_cert_cnt_query);
$crm_cert_total 	    = $crm_cert_cnt_result['cnt'];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($crm_cert_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = (!empty($new_am_no)) ? "" : getenv("QUERY_STRING");
$page_list	= pagelist($pages, "crm_cert_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $crm_cert_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# cert_no 그룹처리
$cert_group_sql     = "SELECT DISTINCT cc.`no` FROM crm_cert AS `cc` LEFT JOIN crm_cert_product AS `ccp` ON `cc`.`no`=`ccp`.cert_no WHERE {$add_where} ORDER BY `ccp`.`no` ASC LIMIT {$offset}, {$num}";
$cert_group_query   = mysqli_query($my_db, $cert_group_sql);
$cert_group_list    = [];
while($cert_group = mysqli_fetch_assoc($cert_group_query)){
    $cert_group_list[] = $cert_group['no'];
}

if(!empty($cert_group_list)){
    $add_group_text = implode(",", $cert_group_list);
    $add_where     .= " AND `cc`.no IN({$add_group_text})";
}else{
    $add_where     .= " AND 1!=1";
}

# 리스트 쿼리
$crm_cert_sql = "
    SELECT
        `cc`.no as cert_no,
        `cc`.status as cert_status,
        `cc`.cert_key,
        `cc`.`update`,
        `cc`.`name`,
        `cc`.`hp`,
        `cc`.last_date,
        `cc`.set_time,
        `ccp`.no as cert_prd_no, 
        `ccp`.prd_no,
        `ccp`.prd_type,   
        `ccp`.base_date,
        `ccp`.next_date,
        `ccp`.sms_term,
        `ccp`.active as prd_status
    FROM crm_cert AS `cc`
    LEFT JOIN crm_cert_product AS `ccp` ON `cc`.`no`=`ccp`.cert_no
    WHERE {$add_where}
    ORDER BY `cc`.`no` ASC, `ccp`.`no` DESC
";
$crm_cert_query = mysqli_query($my_db, $crm_cert_sql);
$crm_cert_list  = [];
while($crm_cert = mysqli_fetch_assoc($crm_cert_query))
{
    $crm_cert['cert_status_name']   = $sch_status_option[$crm_cert['cert_status']];
    $crm_cert['prd_name']           = $sch_prd_option[$crm_cert['prd_no']];
    $crm_cert['prd_detail_name']    = $sch_prd_detail_option[$crm_cert['prd_no']][$crm_cert['prd_type']]['title'];
    $crm_cert['base_date']          = ($crm_cert['base_date'] == "0000-00-00") ? "" : $crm_cert['base_date'];

    if(isset($crm_cert['hp']) && !empty($crm_cert['hp'])){
        $f_hp2  = substr($crm_cert['hp'],0,4);
        $e_hp2  = substr($crm_cert['hp'],7,15);
        $crm_cert['sch_hp'] = $f_hp2."***".$e_hp2;
    }

    $crm_cert_list[$crm_cert['cert_no']][] = $crm_cert;
}

$smarty->assign("sch_status_option", $sch_status_option);
$smarty->assign("sch_prd_option", $sch_prd_option);
$smarty->assign("sch_prd_detail_option", $sch_prd_detail_option);
$smarty->assign("crm_cert_list", $crm_cert_list);

$smarty->display('crm_cert_list.html');
?>