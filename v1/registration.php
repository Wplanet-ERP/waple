<?php
session_start();
require('inc/comm.php');
require('inc/data_state.php');

// GET : registration.php?promotion_no= 로 넘어온 프로모션 고유번호(p_no)를 체크하고 있으면 code에 저장
$code	= isset($_GET['promotion_no']) ? $_GET['promotion_no'] : "";

// POST : form action 을 통해 넘겨받은 process 가 있는지 체크하고 있으면 proc에 저장
$proc	= isset($_POST['process']) ? $_POST['process'] : "";

if($proc == "reserve") // 종료후 알림 요청
{
	$p_no 	= $_POST['code'];

	$hps[] 	= trim($_POST['hp_0']);
	$hps[] 	= trim($_POST['hp_1']);
	$hps[] 	= trim($_POST['hp_2']);
	$hp 	= implode("-",$hps);

	if ($hps[1] == "" && $hps[2] == "") {
        $hp = "";
    }

	$naver_id = addslashes($_POST['naver_id']);

	if ($naver_id) {
		$sql 	= "SELECT count(r_no) as chk FROM reservation WHERE p_no = '{$p_no}' AND naver_id = '{$naver_id}'";
		$result = mysqli_query($my_db, $sql);
		$data 	= mysqli_fetch_array($result);

		if ($data['chk'] > 0) {
			exit("<script>alert('이미 신청된 네이버 아이디 입니다.');history.back();</script>");
		}
	}

	if ($hp) {
		$sql 	= "SELECT count(r_no) as chk FROM reservation WHERE p_no = '{$p_no}' AND hp = '{$hp}'";
		$result = mysqli_query($my_db, $sql);
		$data 	= mysqli_fetch_array($result);

		if ($data['chk'] > 0) {
			exit("<script>alert('이미 신청된 휴대폰 번호 입니다.');history.back();</script>");
		}
	}

	$query = "
		INSERT INTO reservation SET
			 p_no = '{$p_no}',
			 naver_id = '{$naver_id}',
			 hp = '{$hp}',
			 ip = '".$_SERVER['REMOTE_ADDR']."',
			 regdate = now()
	";

	$my_db->query($query);

	alert("신청되었습니다." ,"registration.php?promotion_no=".$_POST['code']);

}
elseif($proc=="write") //등록시
{
	$blog_url_val 	= str_replace("https:","http:", $_POST['blog_url']);
	$select_sql		= "SELECT count(*) FROM application WHERE blog_url= '".addslashes($blog_url_val)."' AND p_no='{$_POST['code']}'";
	$select_query	= mysqli_query($my_db,$select_sql);
	$select_data	= mysqli_fetch_array($select_query);

	if ($select_data[0] != 0 ){
        alert("이미 신청 하셨습니다.","registration.php?promotion_no={$_POST['code']}");
	}

	$hps[] 	= $_POST['hp_0'];
	$hps[] 	= $_POST['hp_1'];
	$hps[] 	= $_POST['hp_2'];
	$hp		= implode("-",$hps);

	$emails[]	= trim(addslashes($_POST['email_0']));
	$emails[]	= trim(addslashes($_POST['email_1']));
	$email		= implode("@",$emails);

	// 체험신청 타입별 추가 쿼리 선언
	switch($_POST['kind']) {
		case 2 :
		case 4 :
			$add_insert[]="bk_title = '".$_POST['bk_title']."',";

			$bk_name_filter = addslashes($_POST['bk_num']);
			$bk_name_filter = preg_replace("/[^0-9]/i","",$bk_name_filter);

			$add_insert[]	= "bk_num = '".$bk_name_filter."',";
			$add_insert[]	= "bk_name = '".addslashes($_POST['bk_name'])."',";
			$add_insert[]	= "bk_jumin = '".addslashes($_POST['bk_jumin'])."',";
			$add_insert_cnt	= sizeof($add_insert);
			break;
		case 3 :
		case 5 :
			$zipcode = $_POST['zipcode'];
			$add_insert[]	= "zipcode = '".$zipcode."',";
			$add_insert[]	= "address1 = '".addslashes($_POST['address1'])."',";
			$add_insert[]	= "address2 = '".addslashes($_POST['address2'])."',";
			$add_insert[]	= "delivery_memo = '".addslashes($_POST['delivery_memo'])."',";
			$add_insert_cnt	= sizeof($add_insert);
			break;
		default :
			$add_insert = "";
			$add_insert_cnt=0;
	}

	$add_query="";
	if($add_insert_cnt!=0) {
		for($i=0;$i<$add_insert_cnt;$i++) {
			$add_query.=$add_insert[$i].PHP_EOL;
		}
	}

	// 답변이 존재하는 것만 추가 쿼리 선언
	if($_POST['opt_a_answer'] != "") {
        $add_query .= "opt_a_answer = '".addslashes($_POST['opt_a_answer'])."',";
    }

    if($_POST['opt_b_answer'] != "") {
        $add_query .= "opt_b_answer = '".addslashes($_POST['opt_b_answer'])."',";
    }

    if($_POST['opt_c_answer'] != "") {
        $add_query .= "opt_c_answer = '".addslashes($_POST['opt_c_answer'])."',";
    }

	if($_POST['opt_d_answer'] != "") {
		$add_query .= "opt_d_answer = '".addslashes($_POST['opt_d_answer'])."',";
	}

	if($_POST['opt_e_answer'] != "") {
		$add_query .= "opt_e_answer = '".addslashes($_POST['opt_e_answer'])."',";
	}

	$query = "
		INSERT INTO application SET
			p_no = '{$_POST['code']}',
			c_no = '{$_POST['c_no']}',
			kind = '{$_POST['kind']}',
			blog_url = '".addslashes($blog_url_val)."',
			nick = '".addslashes($_POST['nick'])."',
			username = '".addslashes($_POST['username'])."',
			hp = '{$hp}',
			email = '{$email}',
			cafe_id = '".addslashes($_POST['cafe_id'])."',
			{$add_query}
			memo = '".addslashes($_POST['memo'])."',
			regdate = '".date("Y-m-d H:i:s")."',
			ip = '".ip2long($_SERVER['REMOTE_ADDR'])."'
	";

	if(isset($_POST['f_visit_num'])){
		$query .= ", visit_num = '{$_POST['f_visit_num']}'";
	}

	$my_db->query($query);
	alert("신청이 완료되었습니다.","registration.php?promotion_no=".$_POST['code']);
}
else
{
    if(!$code) {
        exit("<script>alert('해당 프로모션이 존재하지 않습니다(1)');</script>");
    }

    $app_sql = "select * from promotion where p_no='{$code}'";

    $app_query = mysqli_query($my_db, $app_sql);
    $app_data = mysqli_fetch_array($app_query);

    if(!$app_data)
        exit("<script>alert('해당 프로모션이 존재하지 않습니다(2)');</script>");

    if (in_array($app_data['p_state'], array('4', '5')) || $app_data['p_state1']  === '1') {
        //echo "<script>alert('해당 프로모션의 신청단 모집기간이 종료되었습니다. (3)');</script>";
        $is_p_close = true;
    } else {
        $is_p_close = false;
    }

    $arr_1 = array('(0)','(1)','(2)','(3)','(4)','(5)','(6)');
    $arr_2 = array('(일)','(월)','(화)','(수)','(목)','(금)','(토)');

    $reg_sdate = date("Y-m-d (w)", strtotime($app_data['reg_sdate']));
    $reg_edate = date("Y-m-d (w)", strtotime($app_data['reg_edate']));

    $reg_sdate = str_replace($arr_1, $arr_2, $reg_sdate);
    $reg_edate = str_replace($arr_1, $arr_2, $reg_edate);

    $init_blog_url = "http://blog.naver.com/";
    //인스타그램일 경우 방문자 변경
    if($app_data['channel'] == '2'){
        $init_blog_url = "http://www.instagram.com/";
    	$smarty->assign('visit_num', $insta_visit_num);
    }elseif($app_data['channel'] == '7'){
		$init_blog_url = "http://in.naver.com/";
	}elseif($app_data['channel'] == '8'){
		$init_blog_url = "http://www.youtube.com/";
	}

    $promotion_title = $app_data['title'] ? $app_data['title'] : $app_data['company'];
    $smarty->assign(array(
        "is_p_close" 	=> $is_p_close,
        "no"			=> $app_data['p_no'],
        "s_no"			=> $app_data['s_no'],
        "c_no"			=> $app_data['c_no'],
        "title"			=> $promotion_title,
        "company"		=> $app_data['company'],
        "kind"			=> $app_data['kind'],
        "channel"		=> $app_data['channel'],
        "pres_reason"	=> $app_data['pres_reason'],
        "opt_a_title"	=> $app_data['opt_a_title'],
        "opt_a_desc"	=> nl2br($app_data['opt_a_desc']),
        "opt_a_use"		=> $app_data['opt_a_use'],
        "opt_b_title"	=> $app_data['opt_b_title'],
        "opt_b_desc"	=> nl2br($app_data['opt_b_desc']),
        "opt_b_use"		=> $app_data['opt_b_use'],
        "opt_c_title"	=> $app_data['opt_c_title'],
        "opt_c_desc"	=> nl2br($app_data['opt_c_desc']),
        "opt_c_use"		=> $app_data['opt_c_use'],
		"opt_d_title"	=> $app_data['opt_d_title'],
		"opt_d_desc"	=> nl2br($app_data['opt_d_desc']),
		"opt_d_use"		=> $app_data['opt_d_use'],
		"opt_e_title"	=> $app_data['opt_e_title'],
		"opt_e_desc"	=> nl2br($app_data['opt_e_desc']),
		"opt_e_use"		=> $app_data['opt_e_use'],
        "p_state"		=> $app_data['p_state'],
        "reg_sdate"		=> $reg_sdate,
        "reg_edate"		=> $reg_edate,
        "exp_sdate"		=> $app_data['exp_sdate'],
        "exp_edate"		=> $app_data['exp_edate'],
        "remark_memo"	=> nl2br($app_data['remark_memo']),
        "init_blog_url"	=> $init_blog_url
    ));

    $smarty->display('registration.html');
}

?>
