<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/project.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

# 변수 설정
$search_url    = isset($_POST['search_url']) ? $_POST['search_url'] : "";
$list_url      = "project_staff_list_input_iframe.php";
$project_list  = [];

# 프로세스 처리
$process       = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'project_input_staff')
{
    $pj_no          = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_s_no_list   = isset($_POST['pj_s_no_list']) ? $_POST['pj_s_no_list'] : [];

    if(empty($pj_no)){
        alert("투입인력 처리에 실패했습니다. 프로젝트 번호가 없습니다.", "{$list_url}?{$search_url}");
    }

    $result = true;
    if($pj_s_no_list)
    {
        $regdate    = date('Y-m-d H:i:s');
        foreach($pj_s_no_list as $pj_s_no)
        {
            $my_c_no = "";
            if($pj_s_no)
            {
                $pj_staff_sql   = "SELECT * FROM project_staff WHERE pj_s_no='{$pj_s_no}' LIMIT 1";
                $pj_staff_query = mysqli_query($my_db, $pj_staff_sql);
                $pj_staff       = mysqli_fetch_assoc($pj_staff_query);

                if(isset($pj_staff['pj_s_no'])){
                    $my_c_no = $pj_staff['my_c_no'];
                }

                $ins_sql = "INSERT INTO `project_input_report` SET active='1', pj_no='{$pj_no}', pj_s_no='{$pj_s_no}', my_c_no='{$my_c_no}', regdate='{$regdate}', pir_s_date=(SELECT p.pj_s_date FROM project p WHERE p.pj_no='{$pj_no}' LIMIT 1), pir_e_date=(SELECT p.pj_e_date FROM project p WHERE p.pj_no='{$pj_no}' LIMIT 1)";

                if(!mysqli_query($my_db, $ins_sql)){
                    $result = false;
                }
            }
        }
    }

    if(!$result){
        alert("투입인력 추가 처리 실패했습니다",  "{$list_url}?{$search_url}");
    }else{
        alert("투입인력 추가 했습니다",  "{$list_url}?{$search_url}");
    }
}
elseif($process == 'project_output_staff')
{
    $pj_no   = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_r_no = isset($_POST['pj_r_no']) ? $_POST['pj_r_no'] : "";
    $pj_r_no = isset($_POST['pj_r_no']) ? $_POST['pj_r_no'] : "";
    $out_sql = "UPDATE `project_input_report` SET active='2' WHERE pj_r_no='{$pj_r_no}'";

    if(!mysqli_query($my_db, $out_sql)){
        alert("투입인력에서 삭제처리에 실패했습니다", "{$list_url}?{$search_url}");
    }else{
        alert("투입인력에서 삭제처리 했습니다",  "{$list_url}?{$search_url}");
    }
}
elseif($process == 'pir_s_date')
{
    $pj_r_no = isset($_POST['pj_r_no']) ? $_POST['pj_r_no'] : "";
    $value   = isset($_POST['value']) ? $_POST['value'] : "";
    $upd_sql = "UPDATE `project_input_report` SET pir_s_date='{$value}' WHERE pj_r_no='{$pj_r_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "투입시작날짜 저장에 실패 하였습니다.";
    else
        echo "투입시작날짜가 변경 되었습니다.";
    exit;
}
elseif($process == 'pir_e_date')
{
    $pj_r_no = isset($_POST['pj_r_no']) ? $_POST['pj_r_no'] : "";
    $value   = isset($_POST['value']) ? $_POST['value'] : "";
    $upd_sql = "UPDATE `project_input_report` SET pir_e_date='{$value}' WHERE pj_r_no='{$pj_r_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "투입종료날짜 저장에 실패 하였습니다.";
    else
        echo "투입종료날짜가 저장 되었습니다.";
    exit;
}
elseif($process == 'resource')
{
    $pj_r_no    = isset($_POST['pj_r_no']) ? $_POST['pj_r_no'] : "";
    $pj_s_no    = isset($_POST['pj_s_no']) ? $_POST['pj_s_no'] : "";
    $pir_s_date = isset($_POST['pir_s_date']) ? $_POST['pir_s_date'] : "";
    $pir_e_date = isset($_POST['pir_e_date']) ? $_POST['pir_e_date'] : "";
    $resource   = isset($_POST['value']) ? $_POST['value'] : "";


    /**
     *  기존 사업전체 투입률 검색
     *  $res_sql    = "SELECT SUM(pir.resource) as total FROM project_input_report as pir WHERE pir.pj_r_no!='{$pj_r_no}' AND pir.pj_s_no = '{$pj_s_no}' AND pir.pj_no NOT IN(SELECT pj.pj_no FROM project pj WHERE pj.state='3') AND pir.active='1' AND ((pir.pir_s_date BETWEEN '{$pir_s_date}' AND '{$pir_e_date}' OR pir.pir_e_date BETWEEN '{$pir_s_date}' AND '{$pir_e_date}') OR (pir.pir_s_date <= '{$pir_s_date}' AND pir.pir_e_date >= '{$pir_e_date}'))";
     */

    $res_sql    = "SELECT SUM(pir.resource) as total FROM project_input_report as pir WHERE pir.pj_r_no!='{$pj_r_no}' AND pir.pj_s_no = '{$pj_s_no}' AND pir.pj_no = (SELECT sub.pj_no FROM project_input_report as sub WHERE sub.pj_r_no='{$pj_r_no}' LIMIT 1)";
    $res_query  = mysqli_query($my_db, $res_sql);
    $res_result = mysqli_fetch_assoc($res_query);
    $res_total  = isset($res_result['total']) && !empty($res_result['total']) ? $res_result['total'] : 0;
    $res_total += $resource;

    $out_sql  = "UPDATE `project_input_report` SET resource='{$resource}' WHERE pj_r_no='{$pj_r_no}'";

    if($res_total <= 100)
    {
        if(!mysqli_query($my_db, $out_sql)){
            $data = array("result" => '2', "msg" => "리소스 저장에 실패했습니다");
        }else{
            $data = array("result" => '1', "msg" => "리소스를 저장했습니다");
        }
    }else{
        $data = array("result" => '3', "msg" => "투입리소스가 가용리소스를 초과 합니다.");
    }

    echo json_encode($data, JSON_UNESCAPED_UNICODE);
    exit;
}
else
{
    $add_where      = "`p`.display = '1'";
    $sch_pj_name    = isset($_GET['sch_pj_name']) ? $_GET['sch_pj_name'] : "";
    $sch_manager    = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
    $sch_agency     = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
    $ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "pj_s_date";
    $ori_ord_type   = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "pj_s_date";

    if(!empty($sch_pj_name)) {
        $add_where .= " AND p.pj_name like '%{$sch_pj_name}%'";
        $smarty->assign("sch_pj_name", $sch_pj_name);
    }

    if(!empty($sch_manager))
    {
        $add_where .= " AND p.manager IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_manager}%')";
        $smarty->assign("sch_manager", $sch_manager);
    }

    if(!empty($sch_agency)) {
        $add_where .= " AND p.agency like '%{$sch_agency}%'";
        $smarty->assign("sch_agency", $sch_agency);
    }

    $add_orderby = "";
    $ord_type_by = "";
    if(!empty($ord_type))
    {
        $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

        if(!empty($ord_type_by))
        {
            $orderby_val = "DESC";

            if($ori_ord_type == $ord_type)
            {
                if($ord_type_by == '1'){
                    $orderby_val = "ASC";
                }elseif($ord_type_by == '2'){
                    $orderby_val = "DESC";
                }
            }

            $add_orderby .= "{$ord_type} {$orderby_val}, ";
        }
    }
    $add_orderby .= "pj_no DESC";

    $smarty->assign('ord_type', $ord_type);
    $smarty->assign('ord_type_by', $ord_type_by);

    $pj_total_sql		= "SELECT count(pj_no) FROM (SELECT `p`.pj_no FROM project `p` WHERE {$add_where}) AS cnt";
    $pj_total_query	= mysqli_query($my_db, $pj_total_sql);
    if(!!$pj_total_query)
        $pj_total_result = mysqli_fetch_array($pj_total_query);

    $pj_staff_total = $pj_total_result[0];

    $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= 5;
    $offset 	= ($pages-1) * $num;
    $pagenum 	= ceil($pj_staff_total/$num);

    if ($pages >= $pagenum){$pages = $pagenum;}
    if ($pages <= 0){$pages = 1;}

    $search_url = getenv("QUERY_STRING");
    $page		= pagelist($pages, "project_staff_list_input_iframe.php", $pagenum, $search_url);
    $smarty->assign("total_num", $pj_staff_total);
    $smarty->assign("pagelist", $page);

    $project_sql = "
          SELECT 
              *, 
              (SELECT s.s_name FROM staff s WHERE s.s_no = p.manager) as manager_name,
              (SELECT COUNT(pir.pj_r_no) FROM project_input_report as pir WHERE pir.pj_no = p.pj_no AND pir.active='1') as total_pj_staff,
              (SELECT SUM(pir.resource) FROM project_input_report as pir WHERE pir.pj_no = p.pj_no AND pir.active='1') as total_pj_resource
          FROM project `p` 
          WHERE {$add_where} 
          ORDER BY {$add_orderby}
          LIMIT {$offset}, {$num}
     ";

    $project_query   = mysqli_query($my_db, $project_sql);
    $project_list    = [];
    $state_option    = getProjectStateOption();
    $cal_state_option= getCalStateOption();
    while($project = mysqli_fetch_assoc($project_query))
    {
        $project['state_name']          = isset($state_option[$project['state']]) ? $state_option[$project['state']] : "";
        $project['cal_state_name']      = isset($cal_state_option[$project['cal_state']]) ? $cal_state_option[$project['cal_state']] : "";
        $project['total_pj_staff']      = isset($project['total_pj_staff']) && !empty($project['total_pj_staff']) ? $project['total_pj_staff'] : '0';
        $project['total_pj_resource']   = isset($project['total_pj_resource']) && !empty($project['total_pj_resource']) ? $project['total_pj_resource'] : '0';

        $project_staff_sql     = "SELECT *,(SELECT ps.s_name FROM project_staff ps WHERE ps.pj_s_no = pir.pj_s_no) as s_name FROM project_input_report `pir` WHERE pir.pj_no='{$project['pj_no']}' AND pir.active='1'";
        $project_staff_query   = mysqli_query($my_db, $project_staff_sql);
        $project_staff_list    = [];
        while($project_staff = mysqli_fetch_assoc($project_staff_query))
        {
            $project_staff_list[] = $project_staff;
        }

        $project['staff_list'] = $project_staff_list;
        $project['editable']   = ($project['team'] == $session_team) ? true : false;
        $project_list[] = $project;
    }
}

$smarty->assign("project_list", $project_list);

$smarty->display('project_staff_list_input_iframe.html');
?>
