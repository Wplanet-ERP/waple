<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/WorkCmsSettle.php');
require('inc/model/Kind.php');

# Navigation & My Quick
$nav_prd_no  = "165";
$nav_title   = "커머스 부가세 및 정산 현황";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$sch_brand_total_list       = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

# 검색조건
$prev_month         = date('Y-m', strtotime("-1 months"));
$sch_status_month   = isset($_GET['sch_status_month']) ? $_GET['sch_status_month'] : $prev_month;
$smarty->assign('sch_status_month', $sch_status_month);

# 브랜드 검색
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$check_not_empty    = isset($_GET['check_not_empty']) ? $_GET['check_not_empty'] : "";
$sch_brand_url      = "";
$add_where          = "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1   = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2   = $brand_parent_list[$sch_brand]["brand_g2"];
    $sch_brand_url  = "sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}&sch_brand={$sch_brand}";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1   = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
    $sch_brand_url  = "sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}";
}
elseif(!empty($sch_brand_g1)){
    $sch_brand_url  = "sch_brand_g1={$sch_brand_g1}";
}

if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_url", $sch_brand_url);
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 부가세 정산 판매처 리스트
$settle_model       = WorkCmsSettle::Factory();
$dp_company_list    = $settle_model->getSettleVatCompanyList($sch_status_month);
$dp_except_list     = getNotApplyDpList();
$sales_status_list  = [];
$total_price_list   = array(
    "total_price"   => 0,
    "card_price"    => 0,
    "cash_price"    => 0,
    "etc_price"     => 0,
    "tax_price"     => 0,
    "sales_price"   => 0,
    "settle_price"  => 0,
);

foreach($dp_company_list as $dp_c_no => $dp_c_name){
    if(in_array($dp_c_no, $dp_except_list)){
        continue;
    }
    
    $sales_status_list[$dp_c_no] = array(
        "c_name"        => $dp_c_name,
        "total_price"   => 0,
        "card_price"    => 0,
        "cash_price"    => 0,
        "etc_price"     => 0,
        "tax_price"     => 0,
        "sales_price"   => 0,
        "settle_price"  => 0,
        "settle_per"    => 0,
    );
}

# 부가세 계산
$sales_status_vat_sql = "
    SELECT 
        vat_dp_c_no AS dp_c_no,
        SUM(card_price) AS total_card_price,
        SUM(cash_price) AS total_cash_price,
        SUM(etc_price) AS total_etc_price,
        SUM(tax_price) AS total_tax_price
    FROM work_cms_vat w
    WHERE vat_month='{$sch_status_month}'
    {$add_where}
    GROUP BY dp_c_no
";
$sales_status_vat_query = mysqli_query($my_db, $sales_status_vat_sql);
while($sales_status_vat = mysqli_fetch_assoc($sales_status_vat_query))
{
    $total_price = $sales_status_vat['total_card_price']+$sales_status_vat['total_cash_price']+$sales_status_vat['total_etc_price']+$sales_status_vat['total_tax_price'];

    $sales_status_list[$sales_status_vat['dp_c_no']]['card_price']  += $sales_status_vat['total_card_price'];
    $sales_status_list[$sales_status_vat['dp_c_no']]['cash_price']  += $sales_status_vat['total_cash_price'];
    $sales_status_list[$sales_status_vat['dp_c_no']]['etc_price']   += $sales_status_vat['total_etc_price'];
    $sales_status_list[$sales_status_vat['dp_c_no']]['tax_price']   += $sales_status_vat['total_tax_price'];
    $sales_status_list[$sales_status_vat['dp_c_no']]['total_price'] += $total_price;

    $total_price_list['card_price']  += $sales_status_vat['total_card_price'];
    $total_price_list['cash_price']  += $sales_status_vat['total_cash_price'];
    $total_price_list['etc_price']   += $sales_status_vat['total_etc_price'];
    $total_price_list['tax_price']   += $sales_status_vat['total_tax_price'];
    $total_price_list['total_price'] += $total_price;
}

# 정산 계산
$sales_status_settle_sql = "
    SELECT 
        settle_dp_c_no AS dp_c_no,
        SUM(sales_price) AS total_sales_price,
        SUM(settle_price_vat) AS total_settle_price
    FROM work_cms_settlement w 
    WHERE settle_month='{$sch_status_month}'
    {$add_where}
    GROUP BY dp_c_no
";
$sales_status_settle_query = mysqli_query($my_db, $sales_status_settle_sql);
while($sales_status_settle = mysqli_fetch_assoc($sales_status_settle_query))
{
    $sales_status_list[$sales_status_settle['dp_c_no']]['sales_price']     += $sales_status_settle['total_sales_price'];
    $sales_status_list[$sales_status_settle['dp_c_no']]['settle_price']    += $sales_status_settle['total_settle_price'];
    $sales_status_list[$sales_status_settle['dp_c_no']]['settle_per']       = ($sales_status_settle['total_settle_price']/$sales_status_settle['total_sales_price'])*100;

    $total_price_list['sales_price']     += $sales_status_settle['total_sales_price'];
    $total_price_list['settle_price']    += $sales_status_settle['total_settle_price'];
}

if(isset($sales_status_list[0]))
{
    $chk_result = true;
    foreach($sales_status_list[0] as $chk_price){
        if($chk_price != 0){
            $chk_result = false;
        }
    }

    if($chk_result){
        unset($sales_status_list[0]);
    }
}

if($check_not_empty == "1"){
    foreach($sales_status_list as $dp_c_no => $sales_status){
        if($sales_status['total_price'] == 0 && $sales_status['settle_price'] == 0){
            unset($sales_status_list[$dp_c_no]);
        }
    }
}

$smarty->assign("check_not_empty", $check_not_empty);
$smarty->assign("total_price_list", $total_price_list);
$smarty->assign("sales_status_list", $sales_status_list);

$smarty->display('work_cms_sales_vat_settle_status.html');
?>
