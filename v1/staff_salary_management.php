<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/staff.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');

// 접근 권한
if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자")) {
    $smarty->display('access_company_error.html');
    exit;
}

# 프로세스 처리
$process  = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "add_new_salary")
{
    $search_url           = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $new_s_no             = isset($_POST['new_s_no']) ? $_POST['new_s_no'] : "";
    $new_state            = isset($_POST['new_state']) ? $_POST['new_state'] : "";
    $new_salary           = isset($_POST['new_salary']) ? str_replace(',', '', addslashes(trim($_POST['new_salary']))) : "";
    $new_contract_s_date  = isset($_POST['new_contract_s_date']) ? $_POST['new_contract_s_date'] : "";
    $new_contract_e_date  = isset($_POST['new_contract_e_date']) ? $_POST['new_contract_e_date'] : "";
    $new_renewal_date     = date('Y-m-d', strtotime("{$new_contract_e_date} +1 day"));
    $new_notice           = isset($_POST['new_notice']) ? addslashes(trim($_POST['new_notice'])) : "";
    $aes_salary           = "HEX(AES_ENCRYPT('{$new_salary}', '{$aeskey}'))";


    $staff_sql = "SELECT s_name, my_c_no FROM staff WHERE s_no='{$new_s_no}'";
    $staff_query = mysqli_query($my_db, $staff_sql);
    $staff_result = mysqli_fetch_assoc($staff_query);

    if(isset($staff_result['s_name']) && !empty($staff_result['s_name']))
    {
        $new_s_name  = $staff_result['s_name'];
        $new_my_c_no = $staff_result['my_c_no'];
        $ins_sql     = "INSERT INTO staff_salary SET my_c_no='{$new_my_c_no}', s_no='{$new_s_no}', s_name='{$new_s_name}', `state`='{$new_state}', salary={$aes_salary}, contract_s_date='{$new_contract_s_date}', contract_e_date='{$new_contract_e_date}', renewal_date='{$new_renewal_date}', notice='{$new_notice}', regdate=now()";

        if(!mysqli_query($my_db, $ins_sql)){
            exit("<script>alert('추가등록에 실패했습니다');location.href='staff_salary_management.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('추가등록 되었습니다');location.href='staff_salary_management.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('해당계정이 존재하지 않습니다. 추가등록에 실패했습니다');location.href='staff_salary_management.php?{$search_url}';</script>");
    }
}
elseif($process == "extension_contract")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $ss_no      = isset($_POST['ss_no']) ? $_POST['ss_no'] : "";
    $ins_sql    = "INSERT INTO staff_salary(`my_c_no`,`s_no`,`s_name`,`state`,`salary`,`contract_s_date`,`contract_e_date`,`renewal_date`, regdate) (SELECT my_c_no, s_no, s_name, state, salary, DATE_ADD(contract_s_date, INTERVAL 1 YEAR), DATE_ADD(contract_e_date, INTERVAL 1 YEAR), DATE_ADD(renewal_date, INTERVAL 1 YEAR), now() FROM staff_salary WHERE ss_no='{$ss_no}' LIMIT 1)";

    if(!mysqli_query($my_db, $ins_sql)){
        exit("<script>alert('연장에 실패했습니다');location.href='staff_salary_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('연장 되었습니다');location.href='staff_salary_management.php?{$search_url}';</script>");
    }
}elseif($process == "end_contract")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $ss_no      = isset($_POST['ss_no']) ? $_POST['ss_no'] : "";

    $ins_sql    = "UPDATE staff_salary SET state='2' WHERE ss_no='{$ss_no}'";
//    $ins_sql    = "UPDATE staff_salary SET state='2', contract_e_date=now() WHERE ss_no='{$ss_no}'";

    if(!mysqli_query($my_db, $ins_sql)){
        exit("<script>alert('종료에 실패했습니다');location.href='staff_salary_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('종료 되었습니다');location.href='staff_salary_management.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "76";
$nav_title   = "연봉계약 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 연봉계약 리스트
$add_where      = "1=1";
$sch_my_c_no    = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_s_name     = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
$sch_state      = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_dday       = isset($_GET['sch_dday']) ? $_GET['sch_dday'] : "";
$sch_notice     = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";

if(!empty($sch_my_c_no)) {
    $add_where .= " AND ss.my_c_no='{$sch_my_c_no}'";
    $smarty->assign("sch_my_c_no", $sch_my_c_no);
}

if(!empty($sch_s_name)) {
    $add_where .= " AND ss.s_name like '%{$sch_s_name}%'";
    $smarty->assign("sch_s_name", $sch_s_name);
}

if(!empty($sch_state)) {
    $add_where .= " AND ss.state='{$sch_state}'";
    $smarty->assign("sch_state", $sch_state);
}

if(!empty($sch_dday)) {
    $cur_date  = date('Y-m-d');
    $dday_date = date('Y-m-d', strtotime("+{$sch_dday} days"));
    $add_where .= " AND ss.renewal_date BETWEEN '{$cur_date}' AND '{$dday_date}' AND ss.state='1'";
    $smarty->assign("sch_dday", $sch_dday);
}

if(!empty($sch_notice)) {
    $add_where .= " AND ss.notice like '%{$sch_notice}%'";
    $smarty->assign("sch_notice", $sch_notice);
}

# 전체 게시물 수
$staff_salary_total_sql     = "SELECT count(ss_no) as cnt FROM staff_salary ss WHERE {$add_where}";
$staff_salary_total_query   = mysqli_query($my_db, $staff_salary_total_sql);
$staff_salary_total_result  = mysqli_fetch_array($staff_salary_total_query);
$staff_salary_total         = $staff_salary_total_result['cnt'];

# 페이징 처리
$pages      = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num        = 20;
$pagenum    = ceil($staff_salary_total/$num);
if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}
$offset     = ($pages-1) * $num;

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "staff_salary_management.php", $pagenum, $search_url);

$smarty->assign("total_num", $staff_salary_total);
$smarty->assign("search_url", $search_url);
$smarty->assign("pagelist", $pagelist);

# 리스트 쿼리
$staff_salary_sql = "
    SELECT
        *,
        (AES_DECRYPT(UNHEX(ss.salary), '{$aeskey}')) AS conv_salary,
        DATE_FORMAT(`ss`.`contract_s_date`, '%Y.%m.%d') as cont_s_date,
        DATE_FORMAT(`ss`.`contract_e_date`, '%Y.%m.%d') as cont_e_date,
        DATE_FORMAT(`ss`.`renewal_date`, '%Y.%m.%d') as re_date
    FROM staff_salary ss
    WHERE {$add_where}
    ORDER BY ss.ss_no DESC
    LIMIT {$offset},{$num}
";
$staff_salary_query     = mysqli_query($my_db, $staff_salary_sql);
$staff_salary_list      = [];
$my_company_option      = getStaffMyCompanyOption();
$contract_state_option  = getContractStateOption();
while($staff_salary = mysqli_fetch_assoc($staff_salary_query))
{
    $staff_salary['my_c_name']  = isset($my_company_option[$staff_salary['my_c_no']]) ? $my_company_option[$staff_salary['my_c_no']] : "";
    $staff_salary['state_name'] = isset($contract_state_option[$staff_salary['state']]) ? $contract_state_option[$staff_salary['state']] : "";

    if($staff_salary['state'] == '1')
    {
        $cur_date = date('Y-m-d');
        $end_date = $staff_salary['renewal_date'];
        $s_date = new DateTime($cur_date);
        $e_date = new DateTime($end_date);
        $intval = $s_date->diff($e_date);

        $staff_salary['dday'] = $intval->days;
    }else{
        $staff_salary['dday'] = 0;
    }

    $staff_salary_list[] = $staff_salary;
}

$smarty->assign("contract_state_option", $contract_state_option);
$smarty->assign("my_company_option", $my_company_option);
$smarty->assign("staff_salary_list", $staff_salary_list);

$smarty->display('staff_salary_management.html');
?>
