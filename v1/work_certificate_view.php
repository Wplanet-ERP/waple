<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work.php');
require('inc/helper/work_extra.php');
require('inc/model/WorkCertificate.php');

# 경영지원실 권한 부여
$super_editable = false;
$name_editable  = false;
$my_editable    = false;

if($session_team == "00211") {
    $super_editable = true;
}

if($session_s_no == "62") {
    $name_editable = true;
}

$process = isset($_POST['process']) ? $_POST['process'] : "";
$certificate_model = WorkCertificate::Factory();

if($process == "save_certificate")
{
    $wc_no      = isset($_POST['wc_no']) ? $_POST['wc_no'] : "";
    $doc_no     = isset($_POST['doc_no']) ? $_POST['doc_no'] : $certificate_model->getNewDocNo();
    $work_state = isset($_POST['work_state']) ? $_POST['work_state'] : '1';
    $doc_type   = isset($_POST['doc_type']) ? $_POST['doc_type'] : '';
    $req_s_no   = isset($_POST['req_s_no']) ? $_POST['req_s_no'] : '';
    $req_team   = isset($_POST['req_team']) ? $_POST['req_team'] : '';
    $req_name   = isset($_POST['req_name']) ? addslashes(trim($_POST['req_name'])) : '';
    $req_date   = isset($_POST['req_date']) ? $_POST['req_date'] : date('Y-m-d');
    $purpose    = isset($_POST['purpose']) ? $_POST['purpose'] : '';
    $cert_no_1      = isset($_POST['cert_no_1']) ? $_POST['cert_no_1'] : '';
    $cert_no_2_1    = isset($_POST['cert_no_2_1']) ? $_POST['cert_no_2_1'] : '';
    $cert_no_2_2    = isset($_POST['cert_no_2_2']) ? $_POST['cert_no_2_2'] : '';
    $cert_hide      = isset($_POST['cert_hide']) ? $_POST['cert_hide'] : '2';
    $cert_no        = (!empty($cert_no_1) && !empty($cert_no_2_1) && !empty($cert_no_2_2)) ? $cert_no_1."-".$cert_no_2_1.$cert_no_2_2 : "";
    $req_addr       = isset($_POST['req_addr']) ? addslashes(trim($_POST['req_addr'])) : '';
    $req_position   = isset($_POST['req_position']) ? addslashes(trim($_POST['req_position'])) : '';
    $work_s_date    = isset($_POST['work_s_date']) ? $_POST['work_s_date'] : '';
    $work_e_date    = isset($_POST['work_e_date']) ? $_POST['work_e_date'] : '';
    $work_position  = isset($_POST['work_position']) ? $_POST['work_position'] : '';
    $company_addr   = isset($_POST['company_addr']) ? addslashes(trim($_POST['company_addr'])) : '';
    $company_ceo    = isset($_POST['company_ceo']) ? addslashes(trim($_POST['company_ceo'])) : '';
    $company_name   = isset($_POST['company_name']) ? addslashes(trim($_POST['company_name'])) : '';
    $company_seal   = isset($_POST['company_seal']) ? $_POST['company_seal'] : '';
    $w_no           = isset($_POST['w_no']) ? $_POST['w_no'] : '';

    $add_cert = "";
    if(!empty($cert_no)){
        $add_cert = ", cert_no = HEX(AES_ENCRYPT('".addslashes(trim($cert_no))."', '{$aeskey}'))";
    }

    $add_run  = "";
    if($work_state == '6'){
        $add_run = ", run_date=now() ";
    }

    $add_seal  = "";
    if($super_editable){
        $add_seal = ", company_seal='{$company_seal}'";
    }

    if(!empty($wc_no))
    {
        $cert_sql = "
            UPDATE work_certificate SET
                work_state  = '{$work_state}',
                req_s_no    = '{$req_s_no}',
                req_team    = '{$req_team}',
                req_name    = '{$req_name}',
                doc_type    = '{$doc_type}',
                purpose     = '{$purpose}',
                req_addr        = '{$req_addr}',
                req_position    = '{$req_position}',
                req_date        = '{$req_date}',
                work_s_date     = '{$work_s_date}',
                work_e_date     = '{$work_e_date}',
                w_no            = '{$w_no}',
                work_position   = '{$work_position}',
                company_addr    = '{$company_addr}',
                company_ceo     = '{$company_ceo}',
                company_name    = '{$company_name}',
                cert_hide       = '{$cert_hide}'                       
                {$add_cert}
                {$add_run}
                {$add_seal}
            WHERE wc_no='{$wc_no}'
        ";
    }
    else
    {
        $cert_sql = "
            INSERT INTO work_certificate SET 
                doc_no      = '{$doc_no}',
                work_state  = '{$work_state}',
                req_s_no    = '{$req_s_no}',
                req_team    = '{$req_team}',
                req_name    = '{$req_name}',
                doc_type    = '{$doc_type}',
                purpose     = '{$purpose}',
                req_addr        = '{$req_addr}',
                req_position    = '{$req_position}',
                req_date        = '{$req_date}',
                work_s_date     = '{$work_s_date}',
                work_e_date     = '{$work_e_date}',
                work_position   = '{$work_position}',
                company_addr    = '{$company_addr}',
                company_ceo     = '{$company_ceo}',
                company_name    = '{$company_name}',
                regdate         = now(),
                cert_hide       = '{$cert_hide}'
                {$add_cert}
                {$add_run}
                {$add_seal}
        ";

        if($super_editable){
            $cert_sql .= ", run_s_no='{$session_s_no}', run_team='{$session_team}'";
        }
    }

    if(mysqli_query($my_db, $cert_sql))
    {
        $move_wc_no = empty($wc_no) ? mysqli_insert_id($my_db) : $wc_no;

        if(!empty($w_no)){
            $upd_work_sql = "UPDATE `work` SET work_state='{$work_state}' WHERE w_no='{$w_no}'";
            mysqli_query($my_db, $upd_work_sql);
        }

        if($super_editable){
            exit("<script>alert('증명서가 저장되었습니다.');location.href='work_certificate_view.php?wc_no={$move_wc_no}'</script>");
        }else{
            exit("<script>alert('증명서가 저장되었습니다.');location.href='work_list.php?sch_prd_g1=01014&sch_prd_g2=01022&sch_prd=62'</script>");
        }
    }else{
        exit("<script>alert('증명서 저장에 실패했습니다.');history.back();</script>");
    }
}
elseif($process == "run_certificate")
{
    $wc_no      = isset($_POST['wc_no']) ? $_POST['wc_no'] : "";
    $doc_no     = isset($_POST['doc_no']) ? $_POST['doc_no'] : $certificate_model->getNewDocNo();
    $work_state = '6';
    $doc_type   = isset($_POST['doc_type']) ? $_POST['doc_type'] : '';
    $req_s_no   = isset($_POST['req_s_no']) ? $_POST['req_s_no'] : '';
    $req_team   = isset($_POST['req_team']) ? $_POST['req_team'] : '';
    $req_name   = isset($_POST['req_name']) ? addslashes(trim($_POST['req_name'])) : '';
    $req_date   = isset($_POST['req_date']) ? $_POST['req_date'] : date("Y-m-d");
    $purpose    = isset($_POST['purpose']) ? $_POST['purpose'] : '';
    $cert_no_1      = isset($_POST['cert_no_1']) ? $_POST['cert_no_1'] : '';
    $cert_no_2_1    = isset($_POST['cert_no_2_1']) ? $_POST['cert_no_2_1'] : '';
    $cert_no_2_2    = isset($_POST['cert_no_2_2']) ? $_POST['cert_no_2_2'] : '';
    $cert_hide      = isset($_POST['cert_hide']) ? $_POST['cert_hide'] : '2';
    $cert_no        = (!empty($cert_no_1) && !empty($cert_no_2_1) && !empty($cert_no_2_2)) ? $cert_no_1."-".$cert_no_2_1.$cert_no_2_2 : "";
    $req_addr       = isset($_POST['req_addr']) ? addslashes(trim($_POST['req_addr'])) : '';
    $req_position   = isset($_POST['req_position']) ? addslashes(trim($_POST['req_position'])) : '';
    $work_s_date    = isset($_POST['work_s_date']) ? $_POST['work_s_date'] : '';
    $work_e_date    = isset($_POST['work_e_date']) ? $_POST['work_e_date'] : '';
    $work_position  = isset($_POST['work_position']) ? $_POST['work_position'] : '';
    $company_addr   = isset($_POST['company_addr']) ? addslashes(trim($_POST['company_addr'])) : '';
    $company_ceo    = isset($_POST['company_ceo']) ? addslashes(trim($_POST['company_ceo'])) : '';
    $company_name   = isset($_POST['company_name']) ? addslashes(trim($_POST['company_name'])) : '';
    $company_seal   = isset($_POST['company_seal']) ? $_POST['company_seal'] : '';
    $w_no           = isset($_POST['w_no']) ? $_POST['w_no'] : '';

    $add_cert = "";
    if(!empty($cert_no)){
        $add_cert = ", cert_no = HEX(AES_ENCRYPT('".addslashes(trim($cert_no))."', '{$aeskey}'))";
    }

    $add_seal  = "";
    if($super_editable){
        $add_seal = ", company_seal='{$company_seal}'";
    }

    if(!empty($wc_no))
    {
        $cert_sql = "
            UPDATE work_certificate SET
                work_state  = '{$work_state}',
                req_s_no    = '{$req_s_no}',
                req_team    = '{$req_team}',
                req_name    = '{$req_name}',
                doc_type    = '{$doc_type}',
                purpose     = '{$purpose}',
                req_addr        = '{$req_addr}',
                req_position    = '{$req_position}',
                req_date        = '{$req_date}',
                work_s_date     = '{$work_s_date}',
                work_e_date     = '{$work_e_date}',
                w_no            = '{$w_no}',
                work_position   = '{$work_position}',
                company_addr    = '{$company_addr}',
                company_ceo     = '{$company_ceo}',
                company_name    = '{$company_name}',
                run_date        = now(),
                cert_hide       = '{$cert_hide}'                
                {$add_cert}
                {$add_seal}
            WHERE wc_no='{$wc_no}'
        ";
    }
    else
    {
        $cert_sql = "
            INSERT INTO work_certificate SET 
                doc_no      = '{$doc_no}',
                work_state  = '{$work_state}',
                req_s_no    = '{$req_s_no}',
                req_team    = '{$req_team}',
                req_name    = '{$req_name}',
                doc_type    = '{$doc_type}',
                purpose     = '{$purpose}',
                req_addr        = '{$req_addr}',
                req_position    = '{$req_position}',
                req_date        = '{$req_date}',
                work_s_date     = '{$work_s_date}',
                work_e_date     = '{$work_e_date}',
                work_position   = '{$work_position}',
                company_addr    = '{$company_addr}',
                company_ceo     = '{$company_ceo}',
                company_name    = '{$company_name}',
                run_date        = now(),
                regdate         = now(),
                cert_hide       = '{$cert_hide}'
                {$add_cert}
                {$add_seal}
        ";

        if($super_editable){
            $cert_sql .= ", run_s_no='{$session_s_no}', run_team='{$session_team}'";
        }
    }

    if(mysqli_query($my_db, $cert_sql))
    {
        $move_wc_no = empty($wc_no) ? mysqli_insert_id($my_db) : $wc_no;

        if(!empty($w_no)){
            $upd_work_sql = "UPDATE `work` SET work_state='6', task_run_regdate=now() WHERE w_no='{$w_no}'";
            mysqli_query($my_db, $upd_work_sql);
        }

        exit("<script>alert('증명서가 발행되었습니다.');location.href='work_certificate_print.php?wc_no={$move_wc_no}'</script>");
    }else{
        exit("<script>alert('증명서 발행에 실패했습니다.');history.back();</script>");
    }
}


$wc_no               = isset($_GET['wc_no']) ? $_GET['wc_no'] : "";
$certificate_view    = [];
$aes_unhex           = "(SELECT AES_DECRYPT(UNHEX(wc.cert_no), '{$aeskey}')) AS cert_no";
$company_seal_option = getCertCompanySealOption();
if(!empty($wc_no))
{
    $certificate_sql = "
        SELECT 
            *,
            (SELECT t.team_name FROM team t WHERE t.team_code=wc.req_team) as req_team_name,
            (SELECT s.my_c_no FROM staff s WHERE s.s_no=wc.req_s_no) as my_c_no,
            {$aes_unhex}
        FROM work_certificate as wc
        WHERE wc_no = '{$wc_no}'
    ";
    $certificate_query = mysqli_query($my_db, $certificate_sql);
    while($certificate_result = mysqli_fetch_assoc($certificate_query))
    {
        if(!empty($certificate_result['cert_no'])){
            $cert_nos = explode('-', $certificate_result['cert_no']);
            $certificate_result['cert_no_1'] = $cert_nos[0];

            $cert_no_2_1 = $cert_no_2_2 = "";
            if(isset($cert_nos[1]))
            {
                $cert_no_2_1 = substr($cert_nos[1], 0, 1);
                $cert_no_2_2 = substr($cert_nos[1], 1);
            }

            $certificate_result['cert_no_2_1'] = $cert_no_2_1;
            $certificate_result['cert_no_2_2'] = $cert_no_2_2;
        }

        if($certificate_result['work_state'] == '6'){
            $certificate_result['company_seal_name']  = $company_seal_option[$certificate_result['company_seal']]['seal_file'];
        }

        if($certificate_result['req_s_no'] == $session_s_no){
            $my_editable = true;
        }

        $certificate_view = $certificate_result;
    }
}
else
{
    $certificate_view['req_date']    = date('Y-m-d');
    $certificate_view['work_e_date'] = date('Y-m-d');
    $certificate_view['cert_hide'] = '1';
}

if(!$super_editable && !$name_editable && !$my_editable){
    exit("<script>alert('조회 권한이 없습니다.');location.href='main.php'</script>");
}

$smarty->assign("work_state_option", getWorkStateOption());
$smarty->assign("work_state_color_option", getWorkStateOptionColor());
$smarty->assign("doc_type_option", getCertDocTypeOption());
$smarty->assign("company_seal_option", $company_seal_option);
$smarty->assign("super_editable", $super_editable);
$smarty->assign("name_editable", $name_editable);
$smarty->assign("my_editable", $my_editable);
$smarty->assign($certificate_view);

$smarty->display('work_certificate_view.html');
?>
