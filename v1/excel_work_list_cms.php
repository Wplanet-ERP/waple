<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_cms.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$nowdate = date("Y-m-d H:i:s");

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "w_no")
	->setCellValue('B3', "작성일")
	->setCellValue('C3', "발송진행상태")
	->setCellValue('D3', "주문일시")
	->setCellValue('E3', "주문번호")
	->setCellValue('F3', "품목번호")
	->setCellValue('G3', "수령자명")
	->setCellValue('H3', "수령자전화")
	->setCellValue('I3', "우편번호")
	->setCellValue('J3', "수령지")
	->setCellValue('K3', "배송메모")
	->setCellValue('L3', "배송처리일")
	->setCellValue('M3', "택배사")
	->setCellValue('N3', "운송자번호")
	->setCellValue('O3', "업체명/브랜드명")
	->setCellValue('P3', "커머스 상품명")
	->setCellValue('Q3', "업무요청자")
	->setCellValue('R3', "수량")
	->setCellValue('S3', "특이사항")
	->setCellValue('T3', "주문요약정보")
	->setCellValue('U3', "주문처리자")
	->setCellValue('V3', "결제금액")
	->setCellValue('W3', "결제일시")
	->setCellValue('X3', "구매처")
	->setCellValue('Y3', "관리자메모")
	->setCellValue('Z3', "구성품")
;


# 상품 검색
$add_where  = "1=1";
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd    = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

if (!empty($sch_prd) && $sch_prd != "0") {
    $add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){
        $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){
        $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

# 브랜드 검색
$sch_brand_g1           = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2           = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand              = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

if(!empty($sch_brand)) {
    $add_where      .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

# 날짜 검색
$today_val              = date('Y-m-d');
$week_val               = date('Y-m-d', strtotime('-1 weeks'));

$sch_w_no 			    = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
$sch_reg_s_date         = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : $week_val;
$sch_reg_e_date         = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : $today_val;
$sch_stock_date 	    = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : "";
$sch_delivery_state     = isset($_GET['sch_delivery_state']) ? $_GET['sch_delivery_state'] : "";
$sch_order_s_date 	    = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : "";
$sch_order_e_date 	    = isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : "";
$sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_recipient 		    = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp 	    = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_delivery_no 	    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
$sch_delivery_no_null   = isset($_GET['sch_delivery_no_null']) ? $_GET['sch_delivery_no_null'] : "";
$sch_dp_c_no 		    = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_prd_name 		    = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_notice             = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$sch_task_req           = isset($_GET['sch_task_req']) ? $_GET['sch_task_req'] : "";
$sch_ord_bundle 	    = isset($_GET['sch_ord_bundle']) ? $_GET['sch_ord_bundle'] : "";
$sch_delivery_price     = isset($_GET['sch_delivery_price']) ? $_GET['sch_delivery_price'] : "";
$sch_unit_price         = isset($_GET['sch_unit_price']) ? $_GET['sch_unit_price'] : "";
$sch_coupon_price       = isset($_GET['sch_coupon_price']) ? $_GET['sch_coupon_price'] : "";
$sch_coupon_type        = isset($_GET['sch_coupon_type']) ? $_GET['sch_coupon_type'] : "";
$sch_subscription       = isset($_GET['sch_subscription']) ? $_GET['sch_subscription'] : "";
$sch_wise_dp 	        = isset($_GET['sch_wise_dp']) ? $_GET['sch_wise_dp'] : "";
$sch_order_type	        = isset($_GET['sch_order_type']) ? $_GET['sch_order_type'] : "";
$sch_log_c_no	        = isset($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "";
$sch_run_s_name	        = isset($_GET['sch_run_s_name']) ? $_GET['sch_run_s_name'] : "";
$sch_logistics          = isset($_GET['sch_logistics']) ? $_GET['sch_logistics'] : "";
$sch_option_sku	        = isset($_GET['sch_option_sku']) ? $_GET['sch_option_sku'] : "";
$sch_file_no	        = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";

if(empty(getenv("QUERY_STRING"))){
    $sch_ord_bundle = 1;
}

$excel_title = isset($_GET['excel_title']) ? "{$sch_reg_s_date} 취소건 리스트" : "커머스 판매 리스트";

if(!empty($sch_w_no)){
    $add_where .= " AND w.w_no='{$sch_w_no}'";
}

if(!empty($sch_reg_s_date) || !empty($sch_reg_e_date))
{

    if(!empty($sch_reg_s_date)){
        $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
        $add_where .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
    }

    if(!empty($sch_reg_e_date)){
        $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
        $add_where .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
    }
}

if(!empty($sch_stock_date)){
    $add_where .= " AND w.stock_date = '{$sch_stock_date}'";
}

if(!empty($sch_delivery_state)){
    $add_where .= " AND w.delivery_state = '{$sch_delivery_state}'";
}

if(!empty($sch_order_s_date) || !empty($sch_order_e_date))
{
    if(!empty($sch_order_s_date)){
        $sch_ord_s_datetime = $sch_order_s_date." 00:00:00";
        $add_where .= " AND w.order_date >= '{$sch_ord_s_datetime}'";
    }

    if(!empty($sch_order_e_date)){
        $sch_ord_e_datetime = $sch_order_e_date." 23:59:59";
        $add_where .= " AND w.order_date <= '{$sch_ord_e_datetime}'";
    }
}

if(!empty($sch_order_number)){
    $add_where .= " AND w.order_number = '{$sch_order_number}'";
}

if(!empty($sch_recipient)){
    $add_where .= " AND w.recipient like '%{$sch_recipient}%'";
}

if(!empty($sch_recipient_hp)){
    $add_where .= " AND w.recipient_hp like '%{$sch_recipient_hp}%'";
}

if(!empty($sch_delivery_no_null))
{
    $add_where .= " AND (SELECT COUNT(DISTINCT delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) = 0";
}else{
    if(!empty($sch_delivery_no)){
        $add_where .= " AND w.order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery wcd WHERE wcd.delivery_no='{$sch_delivery_no}')";
    }
}

if(!empty($sch_wise_dp)){
    $add_where .= " AND w.dp_c_no IN(2280, 5468, 5469, 5475, 5476, 5477, 5478 ,5479, 5480, 5481, 5482, 5939, 6002)";
}else{
    if(!empty($sch_dp_c_no)){
        $add_where .= " AND w.dp_c_no='{$sch_dp_c_no}'";
    }
}

if(!empty($sch_prd_name)){
    $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.title like '%{$sch_prd_name}%')";
}

if(!empty($sch_notice)){
    $add_where .= " AND w.notice like '%{$sch_notice}%'";
}

if(!empty($sch_task_req)){
    $add_where .= " AND w.task_req like '%{$sch_task_req}%'";
}

if(!empty($sch_delivery_price))
{
    if($sch_delivery_price == '1'){
        $add_where .= " AND w.unit_delivery_price > 0";
    }else{
        $add_where .= " AND w.unit_delivery_price = 0";
    }
}

if(!empty($sch_unit_price))
{
    if($sch_unit_price == '1'){
        $add_where .= " AND w.unit_price > 0";
    }else{
        $add_where .= " AND w.unit_price = 0";
    }
}

if(!empty($sch_coupon_price))
{
    if($sch_coupon_price == '1'){
        $add_where .= " AND (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) > 0";
    }else{
        $add_where .= " AND (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) IS NULL";
    }
}

if(!empty($sch_coupon_type))
{
    if($sch_coupon_type == '1'){
        $add_where .= " AND w.dp_c_no = 1372";
    }elseif($sch_coupon_type == '2'){
        $add_where .= " AND w.dp_c_no IN(3295,4629,5427,5588)";
    }
}

if(!empty($sch_subscription)){
    if($sch_subscription == '1'){
        $add_where .= " AND (w.subs_application_times > 0 OR w.subs_progression_times > 0)";
    }else{
        $add_where .= " AND ((w.subs_application_times = 0 OR w.subs_application_times IS NULL) AND (w.subs_progression_times = 0 OR w.subs_progression_times IS NULL))";
    }
}

if(!empty($sch_order_type)){
    $add_where .= " AND w.order_type = '{$sch_order_type}'";
}

if(!empty($sch_log_c_no)){
    $add_where .= " AND w.log_c_no='{$sch_log_c_no}'";
}

if(!empty($sch_run_s_name)){
    $add_where .= " AND w.task_run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_run_s_name}%')";
}

if(!empty($sch_option_sku))
{
    $chk_option_sql   = "SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.`option_no` IN(SELECT pcum.prd_unit FROM product_cms_unit_management pcum WHERE pcum.sku='{$sch_option_sku}') AND pcr.display='1'";
    $chk_option_query = mysqli_query($my_db, $chk_option_sql);
    $chk_option_list  = [];
    while($chk_option_result = mysqli_fetch_assoc($chk_option_query)){
        $chk_option_list[] = "'{$chk_option_result['prd_no']}'";
    }

    if(!empty($chk_option_list)){
        $chk_option_text = implode(",", $chk_option_list);
        $add_where .= " AND w.prd_no IN({$chk_option_text})";
    }
}

if(!empty($sch_file_no))
{
    $chk_file_sql   = "SELECT DISTINCT wcc.shop_ord_no FROM work_cms_coupon wcc WHERE wcc.file_no='{$sch_file_no}'";
    $chk_file_query = mysqli_query($my_db, $chk_file_sql);
    $chk_file_list  = [];
    while($chk_file_result = mysqli_fetch_assoc($chk_file_query)){
        $chk_file_list[] = "'{$chk_file_result['shop_ord_no']}'";
    }

    if(!empty($chk_file_list)){
        $chk_file_text = implode(",", $chk_file_list);
        $add_where .= " AND (w.file_no='{$sch_file_no}' OR w.shop_ord_no IN($chk_file_text))";
    }else{
        $add_where .= " AND w.file_no='{$sch_file_no}'";
    }
}

if(!empty($sch_logistics))
{
    $logistics_sql    = "SELECT order_number FROM logistics_management WHERE lm_no='{$sch_logistics}'";
    $logistics_query  = mysqli_query($my_db, $logistics_sql);
    $logistics_result = mysqli_fetch_assoc($logistics_query);
    $logistics_list   = [];

    if(isset($logistics_result['order_number']) && !empty($logistics_result['order_number']))
    {
        $logistics_tmp = explode(",", $logistics_result['order_number']);
        foreach($logistics_tmp as $log_ord){
            $logistics_list[] = "'{$log_ord}'";
        }
    }

    if(!empty($logistics_list))
    {
        $logistics_where = implode(",", $logistics_list);
        $add_where .= " AND order_number IN ({$logistics_where})";
    }
}

$add_order_by = " w.w_no DESC";

if(!empty($sch_ord_bundle))
{
    $cms_ord_sql = "SELECT DISTINCT w.order_number FROM work_cms w WHERE {$add_where} AND w.order_number is not null ORDER BY w.w_no DESC";
    $cms_ord_query = mysqli_query($my_db, $cms_ord_sql);
    $order_number_list = [];
    while ($order_number = mysqli_fetch_assoc($cms_ord_query)) {
        $order_number_list[] = "'" . $order_number['order_number'] . "'";
    }

    $order_numbers = implode(',', $order_number_list);

    $add_order_by    = " w.w_no DESC, w.prd_no ASC";
    $add_where_group = !empty($order_numbers) ? "w.order_number IN({$order_numbers})" : "1!=1";
}else{
    # w_no 번호 뽑아내기
    $cms_ord_sql        = "SELECT DISTINCT w.w_no, order_number FROM work_cms w WHERE {$add_where} AND w.w_no IS NOT NULL ORDER BY w.w_no DESC";
    $cms_ord_query      = mysqli_query($my_db, $cms_ord_sql);
    $w_no_list          = [];
    $order_number_list  = [];
    while($cms_ord = mysqli_fetch_assoc($cms_ord_query)){
        $w_no_list[]            =  "'".$cms_ord['w_no']."'";
        $order_number_list[]    = "'" . $cms_ord['order_number'] . "'";
    }
    $w_nos           = !empty($w_no_list) ? implode(',', $w_no_list) : '';
    $add_where_group = !empty($w_nos) ? "w.w_no IN({$w_nos})" : "1!=1";
    $order_numbers   = implode(',', $order_number_list);
}

$delivery_sql   = "SELECT order_number, delivery_no, delivery_type FROM work_cms_delivery w WHERE order_number IN({$order_numbers}) GROUP BY order_number ORDER BY `no` ASC";
$delivery_query = mysqli_query($my_db, $delivery_sql);
$delivery_list  = [];
while($delivery = mysqli_fetch_assoc($delivery_query))
{
    $delivery_list[$delivery['order_number']] = $delivery;
}

# 리스트 쿼리
$work_cms_sql = "
	SELECT
		w.w_no,
	    w.prd_no,
		w.shop_ord_no,
		w.delivery_state,
		DATE_FORMAT(w.regdate, '%Y-%m-%d %H:%i') as reg_date,
		DATE_FORMAT(w.order_date, '%Y-%m-%d %H:%i') as order_date,
		w.stock_date,
		w.order_number,
		w.recipient,
		w.recipient_hp,
		w.recipient_addr,
		w.zip_code,
		w.delivery_memo,
		w.c_name,
		(SELECT s.s_name FROM staff s WHERE s.s_no=w.s_no) as s_name,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
		(SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
		(SELECT s.s_name FROM staff s WHERE s.s_no=w.task_req_s_no) as task_req_s_name,
		w.task_req,
		(SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name,
		w.quantity,
		w.dp_price_vat,
		DATE_FORMAT(w.payment_date, '%Y-%m-%d %H:%i') as payment_date,
		w.dp_c_name,
		w.notice,
		w.manager_memo
	FROM work_cms w
	WHERE {$add_where_group}
	ORDER BY {$add_order_by}
    LIMIT 10000
";
$delivery_state_list = getDeliveryStateOption();
$work_cms_query	= mysqli_query($my_db, $work_cms_sql);
$idx = 4;
$lfcr = chr(10) ;
if(!!$work_cms_query)
{
    while($work_array = mysqli_fetch_array($work_cms_query))
    {
        $dp_price_vat = number_format($work_array['dp_price_vat']);
        $delivery_state = $work_array['delivery_state'] ? $delivery_state_list[$work_array['delivery_state']] : "";
        $delivery_type  = isset($delivery_list[$work_array['order_number']]) ? $delivery_list[$work_array['order_number']]['delivery_type'] : "";
        $delivery_no    = isset($delivery_list[$work_array['order_number']]) ? $delivery_list[$work_array['order_number']]['delivery_no'] : "";

        $unit_sql   = "SELECT (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='2809') as sku, pcr.quantity as qty FROM product_cms_relation as pcr WHERE pcr.prd_no ='{$work_array['prd_no']}' AND pcr.display='1'";
        $unit_query = mysqli_query($my_db, $unit_sql);
        $unit_list  = [];
        while($unit_result = mysqli_fetch_assoc($unit_query)){
            $unit_result['qty'] = $work_array['quantity']*$unit_result['qty'];
            $unit_list[] = "{$unit_result['sku']} * {$unit_result['qty']}";
        }
        $unit_text = !empty($unit_list) ? implode($lfcr, $unit_list) : "";


        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$idx, $work_array['w_no'])
            ->setCellValue('B'.$idx, $work_array['reg_date'])
            ->setCellValue('C'.$idx, $delivery_state)
            ->setCellValue('D'.$idx, $work_array['order_date'])
            ->setCellValueExplicit('E'.$idx, $work_array['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValueExplicit('F'.$idx, $work_array['shop_ord_no'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('G'.$idx, $work_array['recipient'])
            ->setCellValue('H'.$idx, $work_array['recipient_hp'])
            ->setCellValueExplicit('I'.$idx, $work_array['zip_code'],PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('J'.$idx, $work_array['recipient_addr'])
            ->setCellValueExplicit('K'.$idx, $work_array['delivery_memo'],PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('L'.$idx, $work_array['stock_date'])
            ->setCellValue('M'.$idx, $delivery_type)
            ->setCellValueExplicit('N'.$idx, $delivery_no, PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('O'.$idx, $work_array['c_name'])
            ->setCellValue('P'.$idx, $work_array['prd_name'])
            ->setCellValue('Q'.$idx, $work_array['task_req_s_name'])
            ->setCellValue('R'.$idx, $work_array['quantity'])
            ->setCellValue('S'.$idx, $work_array['notice'])
            ->setCellValue('T'.$idx, $work_array['task_req'])
            ->setCellValue('U'.$idx, $work_array['task_run_s_name'])
            ->setCellValue('V'.$idx, $dp_price_vat)
            ->setCellValue('W'.$idx, $work_array['payment_date'])
            ->setCellValue('X'.$idx, $work_array['dp_c_name'])
            ->setCellValue('Y'.$idx, $work_array['manager_memo'])
            ->setCellValue('Z'.$idx, $unit_text)
        ;

        $idx++;
    }
}

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $excel_title);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:Z3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A3:Z{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:Z3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC0C0C0');
$objPHPExcel->getActiveSheet()->getStyle('A3:Z3')->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("A3:Z{$idx}")->getFont()->setSize(10);

$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('I4:I'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('O4:O'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('P4:P'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Q4:Q'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('R4:R'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('S4:S'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('T4:T'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('U4:U'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('V4:V'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('W4:W'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('X4:X'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Y4:Y'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Z4:Z'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('P4:P'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('S4:S'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('T4:T'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('Y4:Y'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('Z4:Z'.$idx)->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(40);
$objPHPExcel->getActiveSheet()->getStyle("A3:Z{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->setTitle($excel_title);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$excel_title}.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
