<?php
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_date.php');
require('inc/helper/advertising.php');
require('inc/model/Advertising.php');

# 파일 변수
$file_name      = $_FILES["result_file"]["tmp_name"];
$search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

$file_path      = fopen($file_name, "r");
$file_idx       = 0;
$regdate        = date('Y-m-d H:i:s');
$result_data    = [];
$chk_am_list    = [];

while ($file_data = fgetcsv($file_path, 2048, ","))
{
    $file_idx++;
    if($file_idx < 8){
        continue;
    }

    $base_date          = $file_data[0]; // 날짜
    $base_time_val      = iconv("euc-kr", "utf-8", $file_data[1]); // 시간
    $adv_product        = iconv("euc-kr","utf-8", $file_data[2]); // 상품
    $adv_prd_type       = iconv("euc-kr","utf-8", $file_data[3]); // 상품유형
    $adv_material       = iconv("euc-kr","utf-8", $file_data[4]); // 광고소재
    $impressions        = $file_data[5]; // 노출수
    $click_cnt          = $file_data[6]; // 클릭수
    $click_per          = $file_data[7]; // 클릭율
    $adv_price          = $file_data[8]; // 광고비

    $chk_product = "";
    if(strpos($adv_product, "타임보드") !== false){
        $chk_product = 1;
    }elseif(strpos($adv_product, "스페셜DA") !== false){
        $chk_product = 2;
    }

    if(empty($chk_product)){
        echo "ROW : {$file_idx}<br/>";
        echo "등록 가능한 광고상품이 아닙니다. {$adv_product}";
        exit;
    }

    $base_time_val = str_replace("시", "", $base_time_val);
    $base_time_tmp = explode("-", $base_time_val);
    $base_s_date   = $base_date." ".$base_time_tmp[0].":00:00";
    $base_e_date   = $base_date." ".$base_time_tmp[1].":00:00";

    $chk_adv_sql    = "SELECT * FROM advertising_management WHERE adv_s_date <= '{$base_s_date}' AND adv_e_date >= '{$base_e_date}' AND `product`='{$chk_product}' AND `state` != '4'";
    $chk_adv_query  = mysqli_query($my_db, $chk_adv_sql);
    $chk_adv_cnt    = mysqli_num_rows($chk_adv_query);
    $chk_adv_result = mysqli_fetch_assoc($chk_adv_query);

    if($chk_adv_cnt > 1 || (!isset($chk_adv_result['am_no']) && empty($chk_adv_result['am_no'])))
    {
        echo "ROW : {$file_idx}<br/>";
        echo "등록된 광고를 확인할 수 없습니다. {$chk_adv_sql}";
        exit;
    }
    $am_no = $chk_adv_result['am_no'];
    $chk_am_list[$am_no] = $am_no;

    $brand = $brand_name = "";
    if(strpos($adv_material, "큐빙") !== false){
        $brand      = "3386";
        $brand_name = "닥터피엘 큐빙";
    }elseif(strpos($adv_material, "여름이불") !== false){
        $brand      = "5201";
        $brand_name = "누잠 여름이불";
    }elseif(strpos($adv_material, "겨울이불") !== false){
        $brand      = "5642";
        $brand_name = "누잠 겨울이불";
    }elseif(strpos($adv_material, "더블업") !== false){
        $brand      = "4878";
        $brand_name = "누잠 더블업토퍼";
    }elseif(strpos($adv_material, "베개") !== false){
        $brand      = "6026";
        $brand_name = "누잠 베개";
    }elseif(strpos($adv_material, "바디필로우") !== false){
        $brand      = "5810";
        $brand_name = "누잠 바디필로우";
    }elseif(strpos($adv_material, "누잠") !== false){
        $brand      = "2827";
        $brand_name = "누잠";
    }elseif(strpos($adv_material, "쌩얼크림") !== false){
        $brand      = "5759";
        $brand_name = "아이레놀 쌩얼보정 선크림";
    }elseif(strpos($adv_material, "아이레놀") !== false){
        $brand      = "2388";
        $brand_name = "아이레놀";
    }elseif(strpos($adv_material, "시티파이") !== false){
        $brand      = "4333";
        $brand_name = "시티파이";
    }elseif(strpos($adv_material, "에코호스") !== false){
        $brand      = "3303";
        $brand_name = "닥터피엘 에코호스";
    }elseif(strpos($adv_material, "여행용") !== false){
        $brand      = "5434";
        $brand_name = "닥터피엘 여행용";
    }elseif(strpos($adv_material, "퓨어팟") !== false){
        $brand      = "2863";
        $brand_name = "닥터피엘 퓨어팟";
    }elseif(strpos($adv_material, "피처형") !== false){
        $brand      = "4446";
        $brand_name = "닥터피엘 에코 피처형 정수기";
    }elseif(strpos($adv_material, "아토") !== false){
        $brand      = "5812";
        $brand_name = "아토샤워헤드";
    }elseif(strpos($adv_material, "주방용 프리미엄") !== false){
        $brand      = "5368";
        $brand_name = "닥터피엘 주방용 프리미엄";
    }elseif(strpos($adv_material, "버블세면대") !== false){
        $brand      = "5910";
        $brand_name = "버블세면대";
    }elseif(strpos($adv_material, "닥터피엘") !== false){
        $brand      = "1314";
        $brand_name = "닥터피엘";
    }else{
        echo "ROW : {$file_idx}<br/>";
        echo "{$adv_material} 없는 브랜드입니다.";
        exit;
    }

    $result_data[] = array(
        "am_no"         => $am_no,
        "adv_product"   => $adv_product,
        "adv_prd_type"  => $adv_prd_type,
        "adv_material"  => $adv_material,
        "brand"         => $brand,
        "brand_name"    => $brand_name,
        "impressions"   => $impressions,
        "click_cnt"     => $click_cnt,
        "click_per"     => $click_per,
        "adv_price"     => $adv_price,
        "regdate"       => $regdate,
        "reg_s_no"      => $session_s_no
    );
}

$advertise_model    = Advertising::Factory();
$result_model       = Advertising::Factory();
$result_model->setResultTable();

if($result_model->multiInsert($result_data))
{
    $adv_product_option = getAdvertisingProductOption();
    $adv_date_option    = getDayShortOption();
    $adv_media_option   = getAdvertisingMediaOption();

    if(!empty($chk_am_list))
    {
        foreach($chk_am_list as $chk_am_no)
        {
            $advertise_model->update(array("am_no" => $chk_am_no, "state" => "5"));

            $advertise_item     = $advertise_model->getItem($chk_am_no);
            $adv_s_date_text    = $advertise_item['adv_s_day']." (".$adv_date_option[date("w", strtotime($advertise_item['adv_s_day']))].") ".$advertise_item['adv_s_time'];
            $adv_media_name     = $adv_media_option[$advertise_item['media']];
            $adv_product_name   = $adv_product_option[$advertise_item['product']];

            $adv_application_sql    = "SELECT aa_no, s_no, s_name FROM advertising_application WHERE am_no='{$chk_am_no}'";
            $adv_application_query  = mysqli_query($my_db, $adv_application_sql);
            while($adv_application = mysqli_fetch_assoc($adv_application_query))
            {
                $adv_msg     = "[NOSP 광고 결과보고 알림] {$adv_media_name}, {$adv_product_name} {$adv_s_date_text}";
                $adv_msg    .= "\r\nhttps://work.wplanet.co.kr/v1/advertising_result_list.php?sch_no={$chk_am_no}";
                $adv_msg     = addslashes($adv_msg);
                $chk_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$adv_application['s_no']}', content='{$adv_msg}', alert_type='62', alert_check='ADVERTISE_RESULT_{$chk_am_no}', regdate=now()";
                mysqli_query($my_db, $chk_ins_sql);
            }
        }
    }

    exit("<script>alert('결과 등록에 성공했습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
}else{
    exit("<script>alert('결과 등록에 실패했습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
}
die;
?>
