<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/approval.php');
require('inc/helper/withdraw.php');
require('inc/model/MyQuick.php');
require('inc/model/Approval.php');
require('inc/model/Kind.php');
require('inc/model/Team.php');
require('inc/model/Leave.php');

$process    = isset($_POST['process']) ? $_POST['process'] : "";
$form_model = Approval::Factory();
$read_model = Approval::Factory();
$read_model->setMainInit("approval_report_read", "arr_no");

if($process == 'read_approval_report')
{
    $chk_ar_no_list     = isset($_POST['chk_ar_no_list']) ? $_POST['chk_ar_no_list'] : "";
    $chk_ar_state_list  = isset($_POST['chk_ar_state_list']) ? $_POST['chk_ar_state_list'] : "";
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $ar_no_list     = explode(',', $chk_ar_no_list);
    $ar_state_list  = explode(',', $chk_ar_state_list);

    $read_ins_list  = [];
    $read_cnt       = 0;
    if(!empty($ar_no_list))
    {
        foreach($ar_no_list as $key => $ar_no)
        {
            $ar_state   = $ar_state_list[$key];

            if($ar_state == '1'){
                continue;
            }

            $chk_sql    = "SELECT (arr_no) as cnt FROM approval_report_read WHERE read_s_no='{$session_s_no}' AND ar_no='{$ar_no}' AND ar_state='{$ar_state}'";
            $chk_query  = mysqli_query($my_db, $chk_sql);
            $chk_result = mysqli_fetch_assoc($chk_query);

            if($chk_result['cnt'] == 0){
                $read_ins_list[] = array("ar_no" => $ar_no, "ar_state" => $ar_state, "read_s_no" => $session_s_no, "read_date" => date("Y-m-d H:i:s"));
                $read_cnt++;
            }
        }
    }

    if($read_cnt > 0 && !empty($read_ins_list))
    {
        if(!$read_model->multiInsert($read_ins_list)){
            exit("<script>alert('읽음 처리에 실패 하였습니다.');location.href='approval_report_list.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('읽음 처리 성공했습니다.');location.href='approval_report_list.php?{$search_url}';</script>");
        }
    }
    exit("<script>alert('읽음 처리 성공했습니다.');location.href='approval_report_list.php?{$search_url}';</script>");
}

# Navigation & My Quick
$nav_prd_no     = "226";
$nav_title      = "결재상신함";
$quick_model    = MyQuick::Factory();
$is_my_quick    = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 변수 리스트
$team_model             = Team::Factory();
$kind_model             = Kind::Factory();
$leave_model            = Leave::Factory();
$team_all_list	        = $team_model->getTeamAllList();
$team_name_list	        = $team_all_list["team_name_list"];
$staff_all_list	        = $team_all_list["staff_list"];
$sch_staff_list         = $staff_all_list['all'];
$approval_group_list    = $kind_model->getKindGroupList("approval");
$leave_type_list	    = getLeaveTypeList();
$withdraw_state_option  = getWdStateOption();
$sch_appr_g1_list       = [];
$sch_appr_g2_list       = [];
$approval_g1_list       = [];
$approval_g2_list       = [];

foreach($approval_group_list as $key => $appr_data)
{
    if(!$key){
        $approval_g1_list = $appr_data;
    }else{
        $approval_g2_list[$key] = $appr_data;
    }
}
$sch_appr_g1_list = $approval_g1_list;

# 검색 조건
$add_where      = "1=1";
$add_read_where = "1=1";
$sch_appr_g1    = isset($_GET['sch_appr_g1']) ? $_GET['sch_appr_g1'] : "";
$sch_appr_g2    = isset($_GET['sch_appr_g2']) ? $_GET['sch_appr_g2'] : "";

if(!empty($sch_appr_g1))
{
    $add_where       .= " AND ar.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_code='approval' AND sub.k_parent='{$sch_appr_g1}')";
    $add_read_where  .= " AND ar.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_code='approval' AND sub.k_parent='{$sch_appr_g1}')";
    $sch_appr_g2_list = isset($approval_g2_list[$sch_appr_g1]) ? $approval_g2_list[$sch_appr_g1] : [];
}

if(!empty($sch_appr_g2))
{
    $add_where      .= " AND ar.k_name_code='{$sch_appr_g2}'";
    $add_read_where .= " AND ar.k_name_code='{$sch_appr_g2}'";
}

$smarty->assign("sch_appr_g1", $sch_appr_g1);
$smarty->assign("sch_appr_g2", $sch_appr_g2);
$smarty->assign("sch_appr_g1_list", $sch_appr_g1_list);
$smarty->assign("sch_appr_g2_list", $sch_appr_g2_list);

# 검색조건
$sch_doc_no     = isset($_GET['sch_doc_no']) ? $_GET['sch_doc_no'] : "";
$sch_linked_no  = isset($_GET['sch_linked_no']) ? $_GET['sch_linked_no'] : "";
$sch_ar_state   = isset($_GET['sch_ar_state']) ? $_GET['sch_ar_state'] : "";
$sch_team       = $session_team;
$sch_req_s_no   = $session_s_no;
$sch_af_no      = isset($_GET['sch_af_no']) ? $_GET['sch_af_no'] : "";
$sch_ar_title   = isset($_GET['sch_ar_title']) ? $_GET['sch_ar_title'] : "";

if(!empty($sch_doc_no)) {
    $add_where      .= " AND ar.doc_no='{$sch_doc_no}'";
    $add_read_where .= " AND ar.doc_no='{$sch_doc_no}'";
    $smarty->assign("sch_doc_no", $sch_doc_no);
}

if(!empty($sch_linked_no)) {
    $add_where      .= " AND ar.linked_no='{$sch_linked_no}'";
    $add_read_where .= " AND ar.linked_no='{$sch_linked_no}'";
    $smarty->assign("sch_linked_no", $sch_linked_no);
}

if(!empty($sch_ar_state)) {
    $add_where  .= " AND ar.ar_state='{$sch_ar_state}'";
    $smarty->assign("sch_ar_state", $sch_ar_state);
}

if (!empty($sch_team))
{
    if($sch_team != 'all')
    {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        if($sch_team_code_where){
            $add_where      .= " AND `ar`.req_team IN ({$sch_team_code_where})";
            $add_read_where .= " AND `ar`.req_team IN ({$sch_team_code_where})";
        }
        $sch_staff_list = $staff_all_list[$sch_team];
    }
    $smarty->assign("sch_team", $sch_team);
}

if(!empty($sch_req_s_no)) {
    $add_where      .= " AND ar.req_s_no = '{$sch_req_s_no}'";
    $add_read_where .= " AND ar.req_s_no = '{$sch_req_s_no}'";
    $smarty->assign("sch_req_s_no", $sch_req_s_no);
}

if(!empty($sch_af_no)) {
    $add_where      .= " AND ar.af_no='{$sch_af_no}'";
    $add_read_where .= " AND ar.af_no='{$sch_af_no}'";
    $smarty->assign("sch_af_no", $sch_af_no);
}

if(!empty($sch_ar_title)) {
    $add_where      .= " AND ar.ar_title='{$sch_ar_title}'";
    $add_read_where .= " AND ar.ar_title='{$sch_ar_title}'";
    $smarty->assign("sch_ar_title", $sch_ar_title);
}

# 페이징
$approval_report_total_sql     = "SELECT count(ar_no) as cnt FROM approval_report ar WHERE {$add_where}";
$approval_report_total_query   = mysqli_query($my_db, $approval_report_total_sql);
$approval_report_total_result  = mysqli_fetch_assoc($approval_report_total_query);
$approval_report_total         = isset($approval_report_total_result['cnt']) ? $approval_report_total_result['cnt'] : 0;

$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 10;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($approval_report_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "approval_report_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $approval_report_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 결재상신 리스트
$approval_report_sql = "
    SELECT
        ar.ar_no,
        ar.af_no,
        ar.doc_no,
        ar.ar_state,
        ar.ar_title,
        (SELECT s.s_name FROM staff s WHERE s.s_no=ar.req_s_no) as req_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=ar.req_team) as team_name,
        ar.linked_no,
        ar.linked_table,
        DATE_FORMAT(ar.req_date, '%Y/%m/%d') as req_day,
        DATE_FORMAT(ar.req_date, '%H:%i') as req_hour,
        DATE_FORMAT(ar.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(ar.regdate, '%H:%i') as reg_hour,
        (SELECT count(arr.arr_no) FROM approval_report_read arr WHERE arr.ar_no=ar.ar_no AND arr.ar_state=ar.ar_state AND arr.read_s_no='{$session_s_no}') as read_cnt,
        (SELECT count(arc.arc_no) FROM approval_report_comment arc WHERE arc.ar_no=ar.ar_no) as comm_cnt
    FROM approval_report as ar
    WHERE {$add_where}
    ORDER BY ar_no DESC
    LIMIT {$offset}, {$num}
";
$approval_report_query  = mysqli_query($my_db, $approval_report_sql);
$approval_report_list   = [];
$approval_state_option  = getApprovalStateOption();
$linked_table_option    = getLinkedTableOption();

while($approval_report = mysqli_fetch_assoc($approval_report_query))
{
    $approval_form_item                 = $form_model->getItem($approval_report['af_no']);
    $approval_report['linked_name']     = !empty($approval_report['linked_table']) && isset($linked_table_option[$approval_report['linked_table']])? $linked_table_option[$approval_report['linked_table']] : "";
    $approval_report['state_name']      = isset($approval_state_option[$approval_report['ar_state']]) ? $approval_state_option[$approval_report['ar_state']] : "";
    $approval_report['staff_name']      = $approval_report['req_name'];
    $approval_report['team_name']       = $approval_report['team_name'];
    $approval_report['appr_name']       = $approval_form_item['title'];

    $approval_report_permission_sql     = "SELECT (SELECT s.s_name FROM staff s WHERE s.s_no=arp.arp_s_no) as arp_s_name, arp_state FROM approval_report_permission arp WHERE arp.ar_no='{$approval_report['ar_no']}' AND arp.arp_type IN('approval','conference') ORDER BY arp.priority ASC";
    $approval_report_permission_query   = mysqli_query($my_db, $approval_report_permission_sql);
    $approval_report_permission_list    = [];
    $approval_current_permission_name   = "";
    while($approval_report_permission = mysqli_fetch_assoc($approval_report_permission_query))
    {
        if(empty($approval_current_permission_name) && $approval_report['ar_state'] == '2' && $approval_report_permission['arp_state'] == '1'){
            $approval_current_permission_name = $approval_report_permission['arp_s_name'];
        }
        $approval_report_permission_list[] = array('name' => $approval_report_permission['arp_s_name'], 'state' => $approval_report_permission['arp_state']);
    }

    if(!empty($approval_report_permission_list))
    {
        $approval_report['permission_name']     = $approval_report_permission_list;
        $approval_report['cur_permission_name'] = $approval_current_permission_name;
    }

    if($approval_report['ar_state'] > 1){
        $approval_report['link'] = "approval_report_view.php";
    }else{
        $approval_report['link'] = "approval_report.php";
    }

    $approval_exp_text = "";
    if($approval_form_item['is_withdraw'])
    {
        $withdraw_sql       = "SELECT * FROM withdraw WHERE wd_no='{$approval_report['wd_no']}'";
        $withdraw_query     = mysqli_query($my_db, $withdraw_sql);
        $withdraw_result    = mysqli_fetch_assoc($withdraw_query);

        if(!empty($withdraw_result)){
            $wd_state   = $withdraw_state_option[$withdraw_result['wd_state']];
            $wd_price   = number_format($withdraw_result['cost']);

            $approval_exp_text .= "({$wd_state}, ₩ {$wd_price})";
        }
    }
    $approval_report['exp_text'] = $approval_exp_text;

    $approval_report_list[] = $approval_report;
}

# New 처리
$approval_read_sql = "
    SELECT
        ar.ar_no,
        ar.ar_state,
        (SELECT count(arr.arr_no) FROM approval_report_read arr WHERE arr.ar_no=ar.ar_no AND arr.ar_state=ar.ar_state AND arr.read_s_no='{$session_s_no}') as read_cnt
    FROM approval_report as ar
    WHERE {$add_read_where}
    ORDER BY ar_no DESC
";
$approval_read_query        = mysqli_query($my_db, $approval_read_sql);
$approval_report_read_list  = getApprovalStateReadOption();
while($approval_read = mysqli_fetch_assoc($approval_read_query))
{
    if($approval_read['read_cnt'] == 0){
        $approval_report_read_list[$approval_read['ar_state']]++;
    }
}

$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("sch_team_list", $team_name_list);
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("approval_state_option", $approval_state_option);
$smarty->assign("approval_form_list", $read_model->getApprovalFormList());
$smarty->assign("approval_report_list", $approval_report_list);
$smarty->assign("approval_report_read_list", $approval_report_read_list);

$smarty->display('approval_report_list.html');
?>
