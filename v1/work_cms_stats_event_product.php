<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Custom.php');
require('inc/model/ProductCms.php');

# Navigation & My Quick
$nav_prd_no  = "219";
$nav_title   = "커머스 증정품 현황";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$sch_brand_total_list       = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

$add_where          = "`w`.delivery_state='4' AND `w`.order_number IN(SELECT order_number FROM work_cms_tmp_single) AND `w`.unit_price=0 AND (`w`.notice IS NULL OR `w`.notice='') AND p.prd_type!='2'";
$add_single_where   = "`w`.delivery_state='4' AND (`w`.order_number NOT LIKE '%_CS_%' AND `w`.order_number NOT LIKE '%_MUL_%') AND dp_c_no NOT IN(2007,2008,5468,5469,5475,5476,5477,5478,5479,5480,5481,5482,5127,5128,5134,5899,5900,5901,5939,6002)";

# 브랜드 검색
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 날짜별 검색
$today_s_w		    = date('w')-1;
$today_s_week       = date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$sch_s_year         = isset($_GET['sch_s_year']) ? $_GET['sch_s_year'] : date('Y', strtotime("-2 years"));;
$sch_e_year         = isset($_GET['sch_e_year']) ? $_GET['sch_e_year'] : date('Y');
$sch_s_month        = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("-2 months"));
$sch_e_month        = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_e_day          = date("t", strtotime($sch_e_month));
$sch_s_week         = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("{$today_s_week} -3 weeks"));
$sch_e_week         = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$today_s_week} +6 days"));
$sch_s_date         = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-8 day"));
$sch_e_date         = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');
$sch_not_empty      = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : "";

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_s_year', $sch_s_year);
$smarty->assign('sch_e_year', $sch_e_year);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);

# 날짜별 처리
$all_date_where     = "";
$all_date_key	    = "";
$chk_s_date         = "";
$chk_e_date         = "";
$add_cms_column     = "";

if($sch_date_type == '4') # 연간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y')";
    $add_cms_column     = "DATE_FORMAT(stock_date, '%Y')";

    $chk_s_date         = "{$sch_s_year}-01-01";
    $chk_e_date         = "{$sch_e_year}-12-31";
}
elseif($sch_date_type == '3') # 월간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y.%m')";
    $add_cms_column     = "DATE_FORMAT(stock_date, '%Y%m')";

    $chk_s_date         = "{$sch_s_month}-01";
    $chk_e_date         = "{$sch_e_month}-{$sch_e_day}";
}
elseif($sch_date_type == '2') # 주간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	    = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title     = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";
    $add_cms_column     = "DATE_FORMAT(DATE_SUB(stock_date, INTERVAL(IF(DAYOFWEEK(stock_date)=1,8,DAYOFWEEK(stock_date))-2) DAY), '%Y%m%d')";

    $chk_s_date         = $sch_s_week;
    $chk_e_date         = $sch_e_week;
}
else # 일간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";
    $add_cms_column     = "DATE_FORMAT(stock_date, '%Y%m%d')";

    $chk_s_date         = $sch_s_date;
    $chk_e_date         = $sch_e_date;
}

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
$all_date_query     = mysqli_query($my_db, $all_date_sql);
$date_short_option  = getDateChartOption();
$commerce_date_list = [];
while($date = mysqli_fetch_array($all_date_query))
{
    $chart_title = $date['chart_title'];

    if($sch_date_type == '1'){
        $date_convert = explode('_', $chart_title);
        $date_name = isset($date_convert[1]) ? $date_short_option[$date_convert[1]] : "";
        $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
    }

    if(!isset($commerce_date_list[$date['chart_key']]))
    {
        if($sch_date_type == '4'){
            $chart_s_date = $chart_title."-01-01";
            $chart_e_date = $chart_title."-12-31";
        }elseif($sch_date_type == '3'){
            $chart_s_date_val   = $date['chart_key']."01";
            $chart_s_date       = date("Y-m-d", strtotime("{$chart_s_date_val}"));
            $chart_e_day        = DATE('t', strtotime($chart_s_date));
            $chart_e_date_val   = $date['chart_key'].$chart_e_day;
            $chart_e_date       = date("Y-m-d", strtotime("{$chart_e_date_val}"));
        }elseif($sch_date_type == '2'){
            $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
            $chart_e_date = date("Y-m-d", strtotime("{$chart_s_date} +6 days"));
        }else{
            $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
            $chart_e_date = date("Y-m-d", strtotime("{$date['date_key']}"));
        }

        $commerce_date_list[$date['chart_key']] = array('title' => $chart_title, 's_date' => $chart_s_date, 'e_date' => $chart_e_date);
    }
}

# VIEW 테이블 및 쿼리 처리
$add_single_where  .= " AND stock_date BETWEEN '{$chk_s_date}' AND '{$chk_e_date}' ";
$view_search_sql    = "
    CREATE OR REPLACE VIEW work_cms_tmp_single 
    AS (SELECT DISTINCT order_number FROM work_cms w WHERE {$add_single_where} GROUP BY order_number HAVING COUNT(*) > 1)
";
mysqli_query($my_db, $view_search_sql);

# 리스트 검색
$work_cms_event_list        = [];
$work_cms_event_head_list   = [];
$work_cms_event_total_list  = [];
$work_cms_event_sql     = "
    SELECT
        w.prd_no,
        p.title AS prd_name,
        w.dp_c_no,
        w.dp_c_name,
        (SELECT c.priority FROM company c WHERE c.c_no=w.dp_c_no) as dp_priority,
        SUM(w.quantity) AS total_qty,
        {$add_cms_column} as key_date
    FROM work_cms w
    LEFT JOIN product_cms p ON p.prd_no=w.prd_no
    WHERE {$add_where}
    GROUP BY w.prd_no, w.dp_c_no, key_date
    ORDER BY total_qty DESC, prd_name ASC, dp_priority ASC
";
$work_cms_event_query = mysqli_query($my_db, $work_cms_event_sql);
while($work_cms_event = mysqli_fetch_assoc($work_cms_event_query))
{
    if(!isset($work_cms_event_head_list[$work_cms_event['prd_no']][$work_cms_event['dp_c_no']])){
        $work_cms_event_head_list[$work_cms_event['prd_no']][$work_cms_event['dp_c_no']] = array(
            "prd_name"  => $work_cms_event['prd_name'],
            "dp_c_name" => $work_cms_event['dp_c_name']
        );
    }

    $work_cms_event_list[$work_cms_event['prd_no']][$work_cms_event['dp_c_no']][$work_cms_event['key_date']] = $work_cms_event['total_qty'];
    $work_cms_event_total_list[$work_cms_event['prd_no']][$work_cms_event['key_date']] += $work_cms_event['total_qty'];
    $work_cms_event_total_list["total"][$work_cms_event['key_date']] += $work_cms_event['total_qty'];
}

$smarty->assign("commerce_date_cnt", count($commerce_date_list)+2);
$smarty->assign("commerce_date_list", $commerce_date_list);
$smarty->assign("work_cms_event_head_list", $work_cms_event_head_list);
$smarty->assign("work_cms_event_list", $work_cms_event_list);
$smarty->assign("work_cms_event_total_list", $work_cms_event_total_list);

$smarty->display('work_cms_stats_event_product.html');
?>
