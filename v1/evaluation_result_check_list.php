<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/evaluation.php');
require('inc/model/MyQuick.php');
require('inc/model/Staff.php');
require('inc/model/Team.php');
require('inc/model/Evaluation.php');

if($session_s_no != '1'){
	exit("<script>alert('접근 권한이 없습니다.');location.href='main.php';</script>");
}

#프로세스 처리
$process 	= (isset($_POST['process'])) ? $_POST['process'] : "";
$ev_result_model = Evaluation::Factory();
$ev_result_model->setMainInit("evaluation_system_result", "ev_result_no");

if($process == "f_score")
{
	$ev_r_no 		= isset($_POST['ev_r_no']) ? $_POST['ev_r_no'] : "";
	$ev_u_no 		= isset($_POST['ev_u_no']) ? $_POST['ev_u_no'] : "";
	$result_value	= isset($_POST['val']) ? $_POST['val'] : "";
	$result_hun		= $result_value*20;

	$upd_sql = "UPDATE evaluation_system_result SET evaluation_value='{$result_value}', hundred_points='{$result_hun}' WHERE ev_r_no='{$ev_r_no}' AND ev_u_no='{$ev_u_no}'";
	if(!mysqli_query($my_db, $upd_sql)){
		echo "저장에 실패했습니다.";
	}
	exit;
}
elseif($process == "f_apply_standard")
{
	$chk_ev_no 		= isset($_POST['chk_ev_no']) ? $_POST['chk_ev_no'] : "";

	$result_list    = [];
	$sqrt_list      = [];

	# 평가자
	$evaluator_sql = "
        SELECT 
            DISTINCT evaluator_s_no
        FROM evaluation_system_result
        WHERE ev_no='{$chk_ev_no}'
        ORDER BY evaluator_s_no ASC
    ";
	$evaluator_result = mysqli_query($my_db, $evaluator_sql);
	while($evaluator_array = mysqli_fetch_array($evaluator_result))
	{
		$unit_sql = "
            SELECT 
                DISTINCT ev_u_no
            FROM evaluation_system_result
            WHERE ev_no='{$chk_ev_no}' AND evaluator_s_no='{$evaluator_array['evaluator_s_no']}' AND (evaluation_state='1' OR evaluation_state='2' OR evaluation_state='3')
            ORDER BY ev_u_no ASC
        ";
		$unit_result = mysqli_query($my_db, $unit_sql);
		while($unit_array = mysqli_fetch_array($unit_result))
		{
			#평가자의 평가지 항목별 점수(자기평가 제외)
			$unit_value_sql = "
                SELECT
                    ev_r_no,
                    rate,
                    evaluation_value
                FROM evaluation_system_result
                WHERE ev_no='{$chk_ev_no}' AND evaluator_s_no='{$evaluator_array['evaluator_s_no']}' AND ev_u_no='{$unit_array['ev_u_no']}' AND evaluator_s_no <> receiver_s_no
                ORDER BY ev_r_no ASC
            ";
			$unit_value_result = mysqli_query($my_db,$unit_value_sql);
			$sqrt_array = [];
			while($unit_value_array=mysqli_fetch_array($unit_value_result))
			{
				$result_list[]=array(
					"ev_u_no"           => $unit_array['ev_u_no'],
					"evaluator_s_no"    => $evaluator_array['evaluator_s_no'],
					"ev_r_no"           => $unit_value_array['ev_r_no'],
					"rate"              => $unit_value_array['rate'],
					"evaluation_value"  => $unit_value_array['evaluation_value']
				);

				$sqrt_array[] = $unit_value_array['evaluation_value'];
			}

			if(!empty($sqrt_array))
			{
				$sqrt_average = array_sum(array_filter($sqrt_array))/count($sqrt_array);
				for ($i=0 , $s=0 ; $i<count($sqrt_array) ; $i++) {
					$s += pow($sqrt_array[$i] - $sqrt_average, 2);
				}

				$sqrt_list[]=array(
					"ev_u_no"           => $unit_array['ev_u_no'],
					"evaluator_s_no"    => $evaluator_array['evaluator_s_no'],
					"average"           => $sqrt_average,
					"sqrt"              => round(SQRT($s/count($sqrt_array)), 2)
				);
			}
		}
	}

	for ($idx_1=0 ; $idx_1<count($result_list) ; $idx_1++){
		for ($idx_2=0 ; $idx_2<count($sqrt_list) ; $idx_2++){
			if($result_list[$idx_1]['ev_u_no'] == $sqrt_list[$idx_2]['ev_u_no'] && $result_list[$idx_1]['evaluator_s_no'] == $sqrt_list[$idx_2]['evaluator_s_no']){
				if($sqrt_list[$idx_2]['sqrt'] > 0){
					$sqrt_value = round((($result_list[$idx_1]['evaluation_value'] - $sqrt_list[$idx_2]['average']) / $sqrt_list[$idx_2]['sqrt']) * 10 + 70, 2);
				}else{ // 표준편차가 0일 경우(피평가자의 항목이 한가지 일때)
					$sqrt_value = 70;
				}
				$result_list[$idx_1]['sqrt_value'] = $sqrt_value;
			}
		}
	}

	foreach($result_list as $result)
	{
		$upd_sql = "UPDATE evaluation_system_result SET sqrt='{$result['sqrt_value']}' WHERE ev_no='{$chk_ev_no}' AND ev_r_no='{$result['ev_r_no']}' AND evaluator_s_no='{$result['evaluator_s_no']}' AND ev_u_no='{$result['ev_u_no']}'";
		mysqli_query($my_db, $upd_sql);
	}

	exit("<script>alert('적용시켰습니다.');location.href='evaluation_result_check_list.php';</script>");
}

# 검색조건
$sch_ev_no              = 182;
$ev_system_model		= Evaluation::Factory();
$evaluation_system		= $ev_system_model->getItem($sch_ev_no);
$add_where          	= "1=1 AND (er.rec_is_review='1' AND er.eval_is_review='1') AND er.evaluator_s_no = {$session_s_no} AND esr.ev_no='{$sch_ev_no}' AND evaluation_state IN(1,2,3)";

# 문항
$evaluation_unit_sql    = "SELECT * FROM evaluation_unit WHERE ev_u_set_no='{$evaluation_system['ev_u_set_no']}' AND active='1' AND evaluation_state IN(1,2,3) ORDER BY `order`, page ASC";
$evaluation_unit_query  = mysqli_query($my_db, $evaluation_unit_sql);
$page_idx               = 0;
$page_chk               = 1;
$ev_unit_question_list	= [];
$ev_result_list			= [];
while($evaluation_unit = mysqli_fetch_array($evaluation_unit_query))
{
	if($page_chk == $evaluation_unit['page']){
		$page_idx++;
	}else{
		$page_chk = $evaluation_unit['page'];
		$page_idx = 1;
	}

	$ev_unit_question_list[$evaluation_unit['ev_u_no']] = array(
		'subject'   => $evaluation_unit['page']."_".$page_idx,
		"type"      => $evaluation_unit['evaluation_state'],
		"question"  => $evaluation_unit['question']
	);
}

$ev_result_total_chk_list 	= [];
$ev_result_total_list		= [];
$ev_result_total_chk_cnt	= 0;
$ev_result_total_sql  		= "SELECT * FROM evaluation_system_result as esr LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no WHERE esr.ev_no='{$sch_ev_no}' AND er.is_complete='1' AND (er.rec_is_review='1' AND er.eval_is_review='1') AND (esr.evaluator_s_no != esr.receiver_s_no) AND esr.evaluation_state IN(1,2,3) ";
$ev_result_total_query		= mysqli_query($my_db, $ev_result_total_sql);
while($ev_result_total_result = mysqli_fetch_assoc($ev_result_total_query))
{
	if(!isset($ev_result_total_chk_list[$ev_result_total_result['receiver_s_no']][$ev_result_total_result['evaluator_s_no']])){
		$ev_result_total_chk_list[$ev_result_total_result['receiver_s_no']][$ev_result_total_result['evaluator_s_no']] = 1;
		$ev_result_total_chk_cnt++;
	}
	$ev_result_total_list[$ev_result_total_result['ev_u_no']] += $ev_result_total_result['evaluation_value'];
}

if(!empty($ev_result_total_list))
{
	foreach($ev_result_total_list as $ev_u_no => $total_value)
	{
		$avg_value	 		 = round($total_value/$ev_result_total_chk_cnt,2);
		$ev_total_avg_list[$ev_u_no] = array(
			"avg"		=> $avg_value,
		);
	}
}


$ev_result_sql = "
		SELECT 
			esr.ev_result_no,
			esr.ev_r_no,
			esr.ev_u_no,
			esr.evaluation_state,
			esr.evaluation_value,
			esr.sqrt,
		    esr.receiver_s_no,   
		    esr.evaluator_s_no,
		    esr.rate,
		   	er.is_complete,
			(SELECT s.s_name FROM staff s WHERE s.s_no=esr.receiver_s_no) as rec_name,
			(SELECT t.team_name FROM team t WHERE t.team_code=er.receiver_team) as rec_team,
			(SELECT t.priority FROM team t WHERE t.team_code=er.receiver_team) as rec_t_priority,
			(SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as eval_name,
			(SELECT t.team_name FROM team t WHERE t.team_code=er.evaluator_team) as eval_team,   
			(SELECT t.priority FROM team t WHERE t.team_code=er.evaluator_team) as eval_t_priority,
			(SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as eval_name
		FROM evaluation_system_result as esr
		LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
		WHERE {$add_where}
		ORDER BY rec_t_priority, er.receiver_s_no ASC, rate DESC, evaluator_s_no, ev_u_no ASC
	";
$ev_result_query = mysqli_query($my_db, $ev_result_sql);
while($ev_result = mysqli_fetch_assoc($ev_result_query))
{
	if(!isset($ev_result_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']]))
	{
		$result_data = array(
			'result_no'	=> $ev_result['ev_result_no'],
			'ev_r_no'	=> $ev_result['ev_r_no'],
			'rec_team' 	=> $ev_result['rec_team'],
			'rec_name' 	=> $ev_result['rec_name'],
			'eval_team' => $ev_result['eval_team'],
			'eval_name' => $ev_result['eval_name'],
			'complete' 	=> $ev_result['is_complete'],
			'rate' 		=> round($ev_result['rate'], 2),
			'per_sqrt'	=> 0,
			'total' 	=> 0,
			'num_cnt' 	=> 0,
		);

		foreach($ev_unit_question_list as $ev_u_no => $unit_data)
		{
			$result_data[$ev_u_no] = 0;
		}

		$ev_result_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']] = $result_data;
	}

	$ev_value = (empty($ev_result['evaluation_value']) ? 0 : $ev_result['evaluation_value']);

	$ev_result_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']][$ev_result['ev_u_no']] = $ev_value;
	$ev_result_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']]['total'] += $ev_value;
	$ev_result_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']]['num_cnt']++;
}

foreach($ev_result_list as $rec_s_no => $rec_data)
{
	foreach($rec_data as $eval_s_no => $eval_data)
	{
		if($eval_data['num_cnt'] > 0)
		{
			$cal_avg = round($eval_data['total']/$eval_data['num_cnt'], 2);
			$ev_result_list[$rec_s_no][$eval_s_no]['avg'] 		= $cal_avg;
			$ev_result_list[$rec_s_no][$eval_s_no]['per_sqrt']	= round($cal_avg*($eval_data['rate']/100),2);
		}
	}
}

$ev_unit_question_cnt = count($ev_unit_question_list);

$smarty->assign("ev_system", $evaluation_system);
$smarty->assign("ev_unit_question_cnt", $ev_unit_question_cnt);
$smarty->assign("ev_unit_question_list", $ev_unit_question_list);
$smarty->assign("ev_total_avg_list", $ev_total_avg_list);
$smarty->assign("ev_result_list", $ev_result_list);

$smarty->display('evaluation_result_check_list.html');

?>
