<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_date.php');
require('inc/helper/evaluation.php');
require('inc/model/MyQuick.php');
require('inc/model/Evaluation.php');

$ev_model 	= Evaluation::Factory();
$process 	= (isset($_POST['process'])) ? $_POST['process'] : "";

if ($process == "f_ev_state")
{
	$ev_no	= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$ev_model->update(array("ev_no" => $ev_no, "ev_state" => $value)))
		echo "진행상태 저장에 실패 하였습니다.";
	else
		echo "진행상태가 저장 되었습니다.";
	exit;
}
elseif ($process == "duplicate")
{
	$ev_no		= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
	$search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	if($ev_model->duplicate($ev_no, $session_s_no))
	{
		$last_no = $ev_model->getInsertId();

		if($ev_model->duplicateRelation($ev_no, $last_no))
			exit("<script>alert('복제 하였습니다');location.href='evaluation_system.php?{$search_url}';</script>");
		else
			exit("<script>alert('복제에 실패 하였습니다');location.href='evaluation_system.php?{$search_url}';</script>");
	}
	else
	{
		exit("<script>alert('복제에 실패 하였습니다');location.href='evaluation_system.php?{$search_url}';</script>");
	}
}
elseif ($process == "delete")
{
    $ev_no 		= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if($ev_model->delete($ev_no))
		exit("<script>alert('삭제 하였습니다');location.href='evaluation_system.php?{$search_url}';</script>");
    else
		exit("<script>alert('삭제에 실패 하였습니다');location.href='evaluation_system.php?{$search_url}';</script>");
}
else
{
	# Navigation & My Quick
	$nav_prd_no  = "85";
	$nav_title   = "평가 시스템";
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_title", $nav_title);
	$smarty->assign("nav_prd_no", $nav_prd_no);


	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where 			= "1=1";
	$sch_subject 		= isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
	$sch_description 	= isset($_GET['sch_description']) ? $_GET['sch_description'] : "";

	if (!empty($sch_subject)) {
		$add_where .= " AND es.subject like '%{$sch_subject}%'";
		$smarty->assign("sch_subject", $sch_subject);
	}

	if (!empty($sch_description)) {
		$add_where .= " AND es.description like '%{$sch_description}%'";
		$smarty->assign("sch_description", $sch_description);
	}

	$cur_date = date("Y-m-d");
    if(!permissionNameCheck($session_permission, "대표") && $session_s_no != '28')
    {
        //평가 담당자 체크
        $admin_ev_sql 	= "SELECT ev_no FROM evaluation_system WHERE admin_s_no = '{$session_s_no}'";
        $admin_ev_query = mysqli_query($my_db, $admin_ev_sql);
        $ev_no_list   	= [];

        while($admin_ev = mysqli_fetch_assoc($admin_ev_query))
        {
            $ev_no_list[] = $admin_ev['ev_no'];
        }

        $relation_sql 	= "
			SELECT
				ev_no
			FROM evaluation_system
			WHERE ev_no IN(SELECT distinct ev_no FROM evaluation_relation WHERE evaluator_s_no = {$session_s_no})
			AND (
				((DATE_FORMAT(mod_s_date,'%Y-%m-%d') <= '{$cur_date}' AND DATE_FORMAT(mod_s_date,'%Y-%m-%d') >= '{$cur_date}') OR (DATE_FORMAT(mod_e_date,'%Y-%m-%d') >= '{$cur_date}' AND DATE_FORMAT(mod_e_date,'%Y-%m-%d') >= '{$cur_date}'))
				OR ((DATE_FORMAT(chk_s_date,'%Y-%m-%d') <= '{$cur_date}' AND DATE_FORMAT(chk_s_date,'%Y-%m-%d') >= '{$cur_date}') OR (DATE_FORMAT(chk_e_date,'%Y-%m-%d') >= '{$cur_date}' AND DATE_FORMAT(chk_e_date,'%Y-%m-%d') >= '{$cur_date}'))
				OR ((DATE_FORMAT(ev_s_date,'%Y-%m-%d') <= '{$cur_date}' AND DATE_FORMAT(ev_e_date,'%Y-%m-%d') >= '{$cur_date}') OR (DATE_FORMAT(ev_s_date,'%Y-%m-%d') >= '{$cur_date}' AND DATE_FORMAT(ev_e_date,'%Y-%m-%d') >= '{$cur_date}'))
			)
		";
        $relation_query = mysqli_query($my_db, $relation_sql);

        while($relation_ev = mysqli_fetch_assoc($relation_query))
        {
            $ev_no_list[] = $relation_ev['ev_no'];
        }

        if($ev_no_list){
            $ev_no_list = implode(',', $ev_no_list);
            $add_where .= " AND es.ev_no IN({$ev_no_list})";
        }else{
            $add_where .= " AND es.ev_no = ''";
        }
    }

	# 전체 게시물 수
	$ev_total_sql 		= "SELECT count(ev_no) as cnt FROM evaluation_system es WHERE {$add_where}";
	$ev_total_query		= mysqli_query($my_db, $ev_total_sql);
	$ev_total_result 	= mysqli_fetch_array($ev_total_query);
	$ev_total_num	 	= $ev_total_result['cnt'];

	# 페이징 처리
	$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num 		= 15;
	$offset 	= ($pages-1) * $num;
	$pagenum 	= ceil($ev_total_num/$num);

	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	$search_url = getenv("QUERY_STRING");
	$pagelist 	= pagelist($pages, "evaluation_system.php", $pagenum, $search_url);

	$smarty->assign("total_num", $ev_total_num);
	$smarty->assign("search_url", $search_url);
	$smarty->assign("pagelist", $pagelist);

	# 리스트 쿼리
	$evaluation_system_sql = "
		SELECT
			*,
			DATE_FORMAT(set_s_date, '%w') as set_s_day,
			DATE_FORMAT(set_e_date, '%w') as set_e_day,
			DATE_FORMAT(mod_s_date, '%w') as mod_s_day,
			DATE_FORMAT(mod_e_date, '%w') as mod_e_day,
			DATE_FORMAT(chk_s_date, '%w') as chk_s_day,
			DATE_FORMAT(chk_e_date, '%w') as chk_e_day,
			DATE_FORMAT(ev_s_date, '%w') as ev_s_day,
			DATE_FORMAT(ev_e_date, '%w') as ev_e_day,
			(SELECT s_name FROM staff s WHERE s.s_no=es.admin_s_no) AS admin_s_name,
			(SELECT subject FROM evaluation_unit_set ev_set WHERE ev_set.ev_u_set_no=es.ev_u_set_no) AS evaluation_set_name,
			(SELECT COUNT(DISTINCT er.receiver_s_no) FROM evaluation_relation er WHERE er.ev_no=es.ev_no AND er.receiver_s_no != 0) AS rec_cnt,
			(SELECT COUNT(er.evaluator_s_no) FROM evaluation_relation er WHERE er.ev_no=es.ev_no AND er.evaluator_s_no != 0) AS eval_cnt,
			(SELECT COUNT(er.evaluator_s_no) FROM evaluation_relation er WHERE er.ev_no=es.ev_no AND er.rec_is_review = 1 AND er.eval_is_review = 1) AS eval_comp_cnt,
			(SELECT SUM(er.is_complete) FROM evaluation_relation er WHERE er.ev_no=es.ev_no AND er.rec_is_review = 1 AND er.eval_is_review = 1) AS complete_cnt
		FROM evaluation_system es
		WHERE {$add_where}
		ORDER BY es.ev_no DESC
		LIMIT {$offset}, {$num}
	";
	$evaluation_system_query	= mysqli_query($my_db, $evaluation_system_sql);
	$ev_state_option			= getEvStateOption();
	$ev_state_color_option		= getEvStateColorOption();
	$date_short_name_option		= getDateChartOption();
	$evaluation_system_list		= [];
	while($evaluation_system = mysqli_fetch_array($evaluation_system_query))
	{
		$evaluation_system["ev_state_name"]		= isset($ev_state_option[$evaluation_system['ev_state']]) ? $ev_state_option[$evaluation_system['ev_state']] : "";
		$evaluation_system["ev_state_color"] 	= isset($ev_state_color_option[$evaluation_system['ev_state']]) ? $ev_state_color_option[$evaluation_system['ev_state']]['color'] : "";
		$evaluation_system["is_evaluation"]		= false;

		if($evaluation_system['ev_s_date'] <= $cur_date && $evaluation_system['ev_e_date'] >= $cur_date){
			$evaluation_system["is_evaluation"] = true;
		}

		$evaluation_system_list[] = $evaluation_system;
	}

	$smarty->assign("ev_state_option", $ev_state_option);
	$smarty->assign("date_short_name_option", $date_short_name_option);
	$smarty->assign("evaluation_system_list", $evaluation_system_list);

	$smarty->display('evaluation_system.html');
}
?>
