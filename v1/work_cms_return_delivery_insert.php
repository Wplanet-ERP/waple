<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

include_once('inc/phpExcelReader/reader.php');
include_once('inc/common.php');
require('ckadmin.php');

$data = new Spreadsheet_Excel_Reader();
// Set output Encoding.
$data->setOutputEncoding('UTF-8');

$data->read($_FILES["delivery_file"]["tmp_name"]);

$search_url  = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
$upd_sql_list = [];

$rma_date   = date('Ymd');
$lfcr       = chr(10);
for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++)
{
    $state_name     = (string)trim(addslashes($data->sheets[0]['cells'][$i][2]));   //회수상태명
    $r_order_val    = (string)trim(addslashes($data->sheets[0]['cells'][$i][4]));   //회수 고유번호
    $delivery_type  = (string)trim(addslashes($data->sheets[0]['cells'][$i][9]));  //운송장타입
    $delivery_no    = (string)trim(addslashes($data->sheets[0]['cells'][$i][10]));  //운송장번호
    $delivery_no2   = (string)trim(addslashes($data->sheets[0]['cells'][$i][11]));  //(추가)운송장번호
    $r_order_list   = explode($lfcr, $r_order_val);
    $r_order_number = $r_order_list[0];

    if(isset($upd_sql_list[$r_order_number])){
        continue;
    }

    if(!$r_order_number)
    {
        echo "ROW : {$i}<br/>";
        echo "운송장번호 반영에 실패하였습니다.<br/>고유번호가 없습니다.<br>
            주문번호    : {$r_order_number}<br>
            운송장번호  : {$delivery_no}<br>";
        exit;
    }

    if(!$delivery_no){
        echo "ROW : {$i}<br/>";
        echo "운송장번호 반영에 실패하였습니다.<br>운송장번호가 없습니다.<br>
            주문번호    : {$r_order_number}<br>
            운송장번호  : {$delivery_no}<br>";
        exit;
    }

    $ord_sql        = "SELECT count(r_no) as cnt FROM work_cms_return WHERE order_number ='{$r_order_number}'";
    $ord_query      = mysqli_query($my_db, $ord_sql);
    $ord_result     = mysqli_fetch_assoc($ord_query);
    if(!$ord_result || !isset($ord_result['cnt']))
    {
        echo "ROW : {$i}<br/>";
        echo "운송장번호 반영에 실패하였습니다.<br>매칭되는 주문번호가 없습니다.<br>
            주문번호    : {$r_order_number}<br>
            운송장번호  : {$delivery_no}<br>";
        exit;
    }

    $deli_sql    = "SELECT count(r_no) as cnt FROM work_cms_return WHERE order_number !='{$r_order_number}' AND return_delivery_no = '{$delivery_no}'";
    $deli_query  = mysqli_query($my_db, $deli_sql);
    $deli_result = mysqli_fetch_assoc($deli_query);

    if(isset($deli_result['cnt']) && $deli_result['cnt'] > 0)
    {
        echo "ROW : {$i}<br/>";
        echo "운송장번호 반영에 실패하였습니다.<br>운송장번호가 이미 존재합니다.<br>
            주문번호    : {$r_order_number}<br>
            운송장번호  : {$delivery_no}<br>";
        exit;
    }

    $add_delivery = "return_delivery_no = '{$delivery_no}'";
    if(!empty($delivery_no2))
    {
        $last_rma_ord_no_sql    = "SELECT REPLACE(order_number, '{$rma_date}_RMA_', '') as ord_no FROM work_cms_return WHERE order_number LIKE '%{$rma_date}_RMA_%' ORDER BY order_number DESC LIMIT 1";
        $last_rma_ord_no_query  = mysqli_query($my_db, $last_rma_ord_no_sql);
        $last_rma_ord_no_result = mysqli_fetch_assoc($last_rma_ord_no_query);

        $last_rma_ord_no = (isset($last_rma_ord_no_result['ord_no']) && !empty($last_rma_ord_no_result['ord_no'])) ? (int)$last_rma_ord_no_result['ord_no'] : 0;
        $ord_no          = $rma_date."_RMA_".sprintf('%04d', $last_rma_ord_no+1);

        if($state_name == '회수요청')
        {
            $ins_sql = "
                INSERT INTO work_cms_return(
                    order_number, parent_order_number, return_state, return_req_date, parent_shop_ord_no, 
                    c_no, c_name, s_no, team, prd_no, quantity, return_purpose, return_reason, return_type, bad_reason, recipient, recipient_addr, recipient_hp, 
                    dp_c_no, zip_code, return_delivery_type, return_delivery_no, req_s_no, req_team, req_memo, run_s_no, run_team, regdate
                ) 
                (
                    SELECT 
                        '{$ord_no}', parent_order_number, '3', now(), parent_shop_ord_no,
                        c_no, c_name, s_no, team, prd_no, quantity, return_purpose, return_reason, return_type, bad_reason ,recipient, recipient_addr, recipient_hp, 
                        dp_c_no, zip_code, '{$delivery_type}', '{$delivery_no2}', req_s_no, req_team, req_memo, run_s_no, run_team, now()
                    FROM work_cms_return WHERE order_number='{$r_order_number}'
                )
            ";
        }else{
            $ins_sql = "
                INSERT INTO work_cms_return(
                    order_number, parent_order_number, return_state, return_req_date, parent_shop_ord_no, 
                    c_no, c_name, s_no, team, prd_no, quantity, return_purpose, return_reason, return_type, bad_reason, recipient, recipient_addr, recipient_hp, 
                    dp_c_no, zip_code, return_delivery_type, return_delivery_no, req_s_no, req_team, req_memo, run_s_no, run_team, regdate
                ) 
                (
                    SELECT 
                        '{$ord_no}', parent_order_number, return_state, now(), parent_shop_ord_no,
                        c_no, c_name, s_no, team, prd_no, quantity, return_purpose, return_reason, return_type, bad_reason ,recipient, recipient_addr, recipient_hp, 
                        dp_c_no, zip_code, '{$delivery_type}', '{$delivery_no2}', req_s_no, req_team, req_memo, run_s_no, run_team, now()
                    FROM work_cms_return WHERE order_number='{$r_order_number}'
                )
            ";
        }

        if(mysqli_query($my_db, $ins_sql)){
            $insert_sub_sql = "INSERT INTO `work_cms_return_unit`(order_number, `option`, sku, quantity, regdate) (SELECT '{$ord_no}', `option`, sku, quantity, now() FROM work_cms_return_unit WHERE order_number='{$r_order_number}')";
            mysqli_query($my_db, $insert_sub_sql);
        }
    }

    if($state_name == '회수요청'){
        $upd_sql_list[$r_order_number] = "UPDATE work_cms_return SET return_state='3', {$add_delivery}, return_delivery_type='{$delivery_type}' WHERE order_number='{$r_order_number}'";
    }else{
        $upd_sql_list[$r_order_number] = "UPDATE work_cms_return SET {$add_delivery}, return_delivery_type='{$delivery_type}' WHERE order_number='{$r_order_number}'";
    }
}

$idx = 0;
if(!empty($upd_sql_list))
{
    foreach($upd_sql_list as $idx => $upd_sql)
    {
        if (!mysqli_query($my_db, $upd_sql)){
            echo "운송장번호 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
            echo "{$idx}번째";
            echo "SQL : ".$upd_sql."<br/>";
        }
    }
}else{
    exit("<script>alert('엑셀파일 데이터가 없습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
}

exit("<script>alert('송장번호가 반영 되었습니다.');location.href='work_cms_return_list.php?{$search_url}';</script>");

?>
