<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');
require('inc/model/ProductCms.php');

$is_freelancer  = false;
$is_editable    = false;
if($session_staff_state == '2'){
    $is_freelancer = true;
}

if($is_freelancer || ($session_s_no == '9' || $session_s_no == '102')){
    $is_editable   = true;
}
$smarty->assign("is_freelancer", $is_freelancer);
$smarty->assign("is_editable", $is_editable);

# 프로세스 처리
$process    = isset($_POST['process']) ? $_POST['process'] : "";
$cms_model  = ProductCms::Factory();

if($process == "add_product_load")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $unit_nos   = isset($_POST['unit_no']) ? $_POST['unit_no'] : "";
    $unit_names = isset($_POST['unit_name']) ? $_POST['unit_name'] : "";
    $load_data  = array(
        "brand"         => isset($_POST['f_brand_new']) ? $_POST['f_brand_new'] : "",
        "load_title"    => isset($_POST['f_load_title_new']) ? addslashes(trim($_POST['f_load_title_new'])) : "",
        "box_width"     => isset($_POST['f_box_width_new']) ? $_POST['f_box_width_new'] : 0,
        "box_length"    => isset($_POST['f_box_length_new']) ? $_POST['f_box_length_new'] : 0,
        "box_height"    => isset($_POST['f_box_height_new']) ? $_POST['f_box_height_new'] : 0,
        "box_weight"    => isset($_POST['f_box_weight_new']) ? $_POST['f_box_weight_new'] : 0,
        "box_amount"    => isset($_POST['f_box_amount_new']) ? $_POST['f_box_amount_new'] : 0,
        "single_width"  => isset($_POST['f_single_width_new']) ? $_POST['f_single_width_new'] : 0,
        "single_length" => isset($_POST['f_single_length_new']) ? $_POST['f_single_length_new'] : 0,
        "single_height" => isset($_POST['f_single_height_new']) ? $_POST['f_single_height_new'] : 0,
        "single_weight" => isset($_POST['f_single_weight_new']) ? $_POST['f_single_weight_new'] : 0,
        "single_amount" => isset($_POST['f_single_amount_new']) ? $_POST['f_single_amount_new'] : 0,
        "memo"          => isset($_POST['f_memo_new']) ? addslashes(trim($_POST['f_memo_new'])) : 0,
        "reg_s_no"      => $session_s_no,
        "regdate"       => date("Y-m-d H:i:s")
    );

    if(!$cms_model->checkLoadUnit(0, implode(",", $unit_nos))){
        exit("<script>alert('다른 적재정보에서 사용중인 구성품목입니다. 적재정보 구성품목 변경에 실패했습니다.');location.href='product_cms_load_list.php?{$search_url}';</script>");
    }

    if ($cms_model->insertLoad($load_data))
    {
        $load_no = $cms_model->getInsertId();

        if(!empty($unit_nos))
        {
            foreach($unit_nos as $idx => $unit_no)
            {
                $cms_model->insertLoadUnit(array(
                    "pl_no"     => $load_no,
                    "unit_no"   => $unit_no,
                    "sku"       => $unit_names[$idx],
                ));
            }
        }

        exit("<script>alert('적재정보가 등록되었습니다');location.href='product_cms_load_list.php?{$search_url}';</script>");
    }
    else{
        exit("<script>alert('적재정보가 등록에 실패했습니다');location.href='product_cms_load_list.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_data")
{
    $pl_no 	= (isset($_POST['pl_no'])) ? $_POST['pl_no'] : "";
    $type 	= (isset($_POST['type'])) ? $_POST['type'] : "";
    $value 	= (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if ($cms_model->updateLoad(array("pl_no" => $pl_no, "{$type}" => $value))) {
        echo "데이터가 저장 되었습니다.";
    } else {
        echo "데이터 저장에 실패 하였습니다.";
    }
    exit;
}
elseif($process == "modify_load_detail")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $load_data  = array(
        "pl_no"         => isset($_POST['f_pl_no']) ? $_POST['f_pl_no'] : "",
        "box_width"     => isset($_POST['f_box_width']) ? $_POST['f_box_width'] : 0,
        "box_length"    => isset($_POST['f_box_length']) ? $_POST['f_box_length'] : 0,
        "box_height"    => isset($_POST['f_box_height']) ? $_POST['f_box_height'] : 0,
        "box_weight"    => isset($_POST['f_box_weight']) ? $_POST['f_box_weight'] : 0,
        "box_amount"    => isset($_POST['f_box_amount']) ? $_POST['f_box_amount'] : 0,
        "single_width"  => isset($_POST['f_single_width']) ? $_POST['f_single_width'] : 0,
        "single_length" => isset($_POST['f_single_length']) ? $_POST['f_single_length'] : 0,
        "single_height" => isset($_POST['f_single_height']) ? $_POST['f_single_height'] : 0,
        "single_weight" => isset($_POST['f_single_weight']) ? $_POST['f_single_weight'] : 0,
        "single_amount" => isset($_POST['f_single_amount']) ? $_POST['f_single_amount'] : 0,
        "upd_s_no"      => $session_s_no,
        "upd_date"      => date("Y-m-d H:i:s")
    );

    if ($cms_model->updateLoad($load_data)){
        exit("<script>alert('적재정보가 변경되었습니다');location.href='product_cms_load_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('적재정보가 변경에 실패했습니다');location.href='product_cms_load_list.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_unit")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $pl_no              = isset($_POST['pl_no']) ? $_POST['pl_no'] : "";
    $mod_unit_list      = isset($_POST['mod_unit_list']) ? explode(",", $_POST['mod_unit_list']) : "";
    $mod_unit_sku_list  = isset($_POST['mod_unit_sku_list']) ? explode(",", $_POST['mod_unit_sku_list']) : "";
    $org_plu_list       = isset($_POST['org_plu_list']) ? explode(",", $_POST['org_plu_list']) : "";
    $plu_no_list        = isset($_POST['plu_no_list']) ? explode(",", $_POST['plu_no_list']) : "";
    $result             = true;

    if(!empty($mod_unit_list))
    {
        if(!$cms_model->checkLoadUnit($pl_no, $_POST['mod_unit_list'])){
            exit("<script>alert('다른 적재정보에서 사용중인 구성품목입니다. 적재정보 구성품목 변경에 실패했습니다.');location.href='product_cms_load_list.php?{$search_url}';</script>");
        }

        foreach($mod_unit_list as $idx => $mod_unit)
        {
            $plu_no     = isset($plu_no_list[$idx]) && !empty($plu_no_list[$idx]) ? $plu_no_list[$idx] : "";
            $unit_sku   = isset($mod_unit_sku_list[$idx]) && !empty($mod_unit_sku_list[$idx]) ? $mod_unit_sku_list[$idx] : "";
            if(!empty($plu_no)){
                if(!$cms_model->updateLoadUnit(array("plu_no" => $plu_no, "unit_no" => $mod_unit, "sku" => $unit_sku))){
                    $result = false;
                }
            }else{
                if(!$cms_model->insertLoadUnit(array("pl_no" => $pl_no, "unit_no" => $mod_unit, "sku" => $unit_sku))){
                    $result = false;
                }
            }
        }
    }

    $del_plu_list = array_diff($org_plu_list, $plu_no_list);
    if(!empty($del_plu_list)){
        foreach($del_plu_list as $del_plu){
            $cms_model->deleteLoadUnit($del_plu);
        }
    }

    if ($result){
        exit("<script>alert('적재정보 구성품목이 변경되었습니다');location.href='product_cms_load_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('적재정보 구성품목 변경에 실패했습니다');location.href='product_cms_load_list.php?{$search_url}';</script>");
    }
}


# Navigation & My Quick
$nav_prd_no  = "155";
$nav_title   = "적재정보 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$add_where      = "1=1";
$sch_brand      = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_no         = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_display    = isset($_GET['sch_display']) ? $_GET['sch_display'] : "";
$sch_load_title = isset($_GET['sch_load_title']) ? $_GET['sch_load_title'] : "";
$sch_memo       = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";

if(!empty($sch_brand)){
    $add_where   .= " AND `pl`.brand = '{$sch_brand}'";
    $smarty->assign('sch_brand', $sch_brand);
}

if(!empty($sch_no)){
    $add_where   .= " AND `pl`.pl_no = '{$sch_no}'";
    $smarty->assign('sch_no', $sch_no);
}

if(!empty($sch_display)){
    $add_where   .= " AND `pl`.display = '{$sch_display}'";
    $smarty->assign('sch_display', $sch_display);
}

if(!empty($sch_load_title)){
    $add_where   .= " AND `pl`.load_title LIKE '%{$sch_load_title}%'";
    $smarty->assign('sch_load_title', $sch_load_title);
}

if(!empty($sch_memo)){
    $add_where   .= " AND `pl`.memo LIKE '%{$sch_memo}%'";
    $smarty->assign('sch_memo', $sch_memo);
}

# 전체 게시물 수
$load_total_sql	    = "SELECT count(`pl_no`) as cnt FROM product_cms_load `pl` WHERE {$add_where}";
$load_total_query   = mysqli_query($my_db, $load_total_sql);
$load_total_result  = mysqli_fetch_array($load_total_query);
$load_total 	    = $load_total_result['cnt'];

$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 20;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($load_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist	= pagelist($pages, "product_cms_load_list.php", $page_num, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $load_total);
$smarty->assign("pagelist", $pagelist);


# 적재정보 리스트
$product_cms_load_sql = "
    SELECT 
        *,
        DATE_FORMAT(`pl`.regdate, '%Y-%m-%d') as reg_day,
        DATE_FORMAT(`pl`.upd_date, '%Y-%m-%d') as upd_day,
        (SELECT s_name FROM staff s WHERE s.s_no=`pl`.reg_s_no) AS reg_name,
        (SELECT s_name FROM staff s WHERE s.s_no=`pl`.upd_s_no) AS upd_name
    FROM product_cms_load `pl`
    WHERE {$add_where}
    ORDER BY `pl_no` DESC
    LIMIT {$offset}, {$num}
";
$product_cms_load_query = mysqli_query($my_db, $product_cms_load_sql);
$product_cms_load_list  = [];
$brand_option           = $cms_model->getLoadBrandOption();
while($product_cms_load = mysqli_fetch_assoc($product_cms_load_query))
{
    $load_unit_sql      = "SELECT * FROM product_cms_load_unit as plu WHERE plu.pl_no='{$product_cms_load['pl_no']}'";
    $load_unit_query    = mysqli_query($my_db, $load_unit_sql);
    $load_unit_list     = [];
    while($load_unit = mysqli_fetch_assoc($load_unit_query)){
        $load_unit_list[$load_unit['plu_no']] = $load_unit;
    }
    $load_unit_cnt  = !empty($load_unit_list) ? count($load_unit_list) : 0;
    $org_plu_list   = !empty($load_unit_list) ? implode(",", array_keys($load_unit_list)) : "";

    $product_cms_load["unit_cnt"]       = $load_unit_cnt;
    $product_cms_load["org_plu_list"]   = $org_plu_list;
    $product_cms_load["unit_list"]      = $load_unit_list;
    $product_cms_load['brand_name']     = $brand_option[$product_cms_load['brand']];
    $product_cms_load_list[]            = $product_cms_load;
}

$smarty->assign('display_option', getDisplayOption());
$smarty->assign('brand_option', $brand_option);
$smarty->assign("product_cms_load_list", $product_cms_load_list);

$smarty->display('product_cms_load_list.html');
?>
