<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/project.php');
require('inc/model/Product.php');
require('inc/model/Project.php');

# 프로세스 처리
$process    = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "add_new_withdraw")
{
    $pj_expenses_model = Project::Factory();
    $pj_expenses_model->setExpensesTable();

    $result      = false;
    $pj_no       = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_kind     = isset($_POST['pj_kind']) ? $_POST['pj_kind'] : "";
    $chk_wd_list = isset($_POST['chk_wd_no_list']) ? $_POST['chk_wd_no_list'] : "";
    $search_url  = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $cur_date    = date('Y-m-d');
    $reg_date    = date('Y-m-d H:i:s');

    $pj_name_sql    = "SELECT pj_name FROM project WHERE pj_no='{$pj_no}' LIMIT 1";
    $pj_name_query  = mysqli_query($my_db, $pj_name_sql);
    $pj_name_result = mysqli_fetch_assoc($pj_name_query);
    $pj_name        = isset($pj_name_result['pj_name']) ? $pj_name_result['pj_name'] : "";

    $ins_data       = [];

    if(empty($pj_name)){
        exit("<script>alert('사업명이 없습니다. 내부통용_사업명 변경에 실패했습니다.');location.href='project_expenses.php?{$search_url}';</script>");
    }

    $wd_list_sql    = "SELECT wd.*, (SELECT title FROM product prd WHERE prd.prd_no=w.prd_no) AS product_title FROM withdraw wd LEFT JOIN `work` as w ON w.w_no = wd.w_no WHERE wd.wd_no IN({$chk_wd_list})";
    $wd_list_query  = mysqli_query($my_db, $wd_list_sql);

    while($withdraw = mysqli_fetch_assoc($wd_list_query))
    {
        $chk_pje_sql    = "SELECT * FROM project_expenses WHERE wd_no='{$withdraw['wd_no']}' AND display='1'";
        echo $chk_pje_sql;
        $chk_pje_query  = mysqli_query($my_db, $chk_pje_sql);
        $chk_pje_result = mysqli_fetch_assoc($chk_pje_query);

        $supply_price   = $withdraw['supply_cost'];
        $tax_price      = 0;
        $total_price    = $withdraw['supply_cost'];

        if($withdraw['vat_choice'] == '1'){
            $tax_price   = $withdraw['supply_cost_vat'];
        }elseif($withdraw['vat_choice'] == '2'){
            $tax_price   = $withdraw['local_tax']+$withdraw['biz_tax'];
        }
        $total_price += $tax_price;

        if(!empty($chk_pje_result['pj_e_no']) && $chk_pje_result['pj_e_no'] > 0)
        {
            $upd_data = array(
                "pj_e_no"       => $chk_pje_result['pj_e_no'],
                "pj_no"         => $pj_no,
                "kind"          => $pj_kind,
                "pj_name"       => $pj_name,
                "wd_no"         => $withdraw['wd_no'],
                "cal_method"    => "2",
                "spend_method"  => "3",
                "req_s_no"      => $withdraw['s_no'],
                "req_team"      => $withdraw['team'],
                "supply_price"  => $supply_price,
                "tax_price"     => $tax_price,
                "total_price"   => $total_price,
                "appr_date"     => $withdraw['wd_date'],
                "c_no"          => $withdraw['c_no'],
                "c_name"        => $withdraw['c_name'],
                "memo"          => $withdraw['product_title'],
            );

            if($pj_expenses_model->update($upd_data)){
                $result = true;
            }
        }else {
            $ins_data[] = array(
                "pj_no"         => $pj_no,
                "kind"          => $pj_kind,
                "pj_name"       => $pj_name,
                "wd_no"         => $withdraw['wd_no'],
                "cal_method"    => "2",
                "spend_method"  => "3",
                "req_s_no"      => $withdraw['s_no'],
                "req_team"      => $withdraw['team'],
                "supply_price"  => $supply_price,
                "tax_price"     => $tax_price,
                "total_price"   => $total_price,
                "req_date"      => $cur_date,
                "appr_date"     => $withdraw['wd_date'],
                "c_no"          => $withdraw['c_no'],
                "c_name"        => $withdraw['c_name'],
                "memo"          => $withdraw['product_title'],
                "regdate"       => $reg_date
            );

            $result = true;
        }
    }

    if(!$pj_expenses_model->multiInsert($ins_data) && !$result){
        exit("<script>alert('사업비 지출관리 추가등록에 실패했습니다');location.href='project_expenses_withdraw_window.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('사업비 지출관리 추가등록 되었습니다');location.href='project_expenses_withdraw_window.php?{$search_url}';</script>");
    }
}

# Product Option
$product_model      = Product::Factory();
$out_product_list   = $product_model->getWithdrawWorkProduct();
unset($out_product_list[278]);

# 검색조건
$add_where 			= " wd.display = 1 AND wd.wd_state='3' AND w.w_no > 0 AND w.prd_no != 278";
$auto_pj_no         = isset($_GET['auto_pj_no']) ? $_GET['auto_pj_no'] : "";
$sch_pj_name        = isset($_GET['sch_pj_name']) ? $_GET['sch_pj_name'] : "";
$sch_pj_name_null   = isset($_GET['sch_pj_name_null']) ? $_GET['sch_pj_name_null'] : "";
$sch_wd_no   		= isset($_GET['sch_wd_no']) ? $_GET['sch_wd_no'] : "";
$sch_w_c_name    	= isset($_GET['sch_w_c_name']) ? $_GET['sch_w_c_name'] : "";
$sch_prd_no		    = isset($_GET['sch_prd_no'])?$_GET['sch_prd_no']:"";
$sch_s_wd_date 	    = isset($_GET['sch_s_wd_date']) ? $_GET['sch_s_wd_date'] : "";
$sch_e_wd_date 	    = isset($_GET['sch_e_wd_date']) ? $_GET['sch_e_wd_date'] : "";

if(!empty($auto_pj_no))
{
    $auto_pj_sql    = "SELECT pj_name FROM project WHERE pj_no='{$auto_pj_no}'";
    $auto_pj_query  = mysqli_query($my_db, $auto_pj_sql);
    $auto_pj_result = mysqli_fetch_assoc($auto_pj_query);
    $auto_pj_name   = isset($auto_pj_result['pj_name']) ? $auto_pj_result['pj_name'] : "";

    $smarty->assign("auto_pj_no", $auto_pj_no);
    $smarty->assign("auto_pj_name", $auto_pj_name);
}

if (!empty($sch_pj_name_null)){
    $add_where .= " AND wd.wd_no NOT IN(SELECT pje.wd_no FROM project_expenses as pje WHERE pje.wd_no != '')";
    $smarty->assign("sch_pj_name_null", $sch_pj_name_null);
}elseif (!empty($sch_pj_name)) {
    $add_where .= " AND wd.wd_no IN(SELECT pje.wd_no FROM project_expenses as pje WHERE pje.pj_name like '%{$sch_pj_name}%')";
    $smarty->assign("sch_pj_name", $sch_pj_name);
}

if (!empty($sch_wd_no)) {
    $add_where .= " AND wd.wd_no='{$sch_wd_no}'";
    $smarty->assign("sch_wd_no", $sch_wd_no);
}

if (!empty($sch_w_c_name)) {
    $add_where .= " AND (SELECT w.c_name FROM work w WHERE w.w_no = wd.w_no) LIKE '%{$sch_w_c_name}%'";
    $smarty->assign("sch_w_c_name", $sch_w_c_name);
}

if(!empty($sch_prd_no)) {
    $add_where.=" AND (SELECT w.prd_no FROM work w WHERE w.w_no=wd.w_no)='{$sch_prd_no}'";
    $smarty->assign("sch_prd_no", $sch_prd_no);
}

if (!empty($sch_s_wd_date)) {
    $add_where .= " AND wd.wd_date >= '{$sch_s_wd_date}'";
    $smarty->assign("sch_s_wd_date", $sch_s_wd_date);
}

if (!empty($sch_e_wd_date)) {
    $add_where .= " AND wd.wd_date <= '{$sch_e_wd_date}'";
    $smarty->assign("sch_e_wd_date", $sch_e_wd_date);
}

# 정렬추가
$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "wd_date";
$ori_ord_type   = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "wd_date";
$add_orderby    = "";
$ord_type_by    = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";
    if(!empty($ord_type_by))
    {
        $ord_type_list = [];
        if($ord_type == 'wd_date'){
            $ord_type_list[] = "wd_date";
        }
        elseif($ord_type == 'pj_kind')
        {
            $ord_type_list[] = "pj_no";
            $ord_type_list[] = "pj_kind";
        }

        $orderby_val = "";
        if($ori_ord_type == $ord_type)
        {
            if($ord_type_by == '1'){
                $orderby_val = "ASC";
            }elseif($ord_type_by == '2'){
                $orderby_val = "DESC";
            }
        }else{
            $ord_type_by = '2';
            $orderby_val = "DESC";
        }

        foreach($ord_type_list as $ord_type_val){
            $add_orderby .= "{$ord_type_val} {$orderby_val}, ";
        }
    }else{
        $add_orderby .= "wd.wd_no DESC, ";
    }
}
$add_orderby .= "wd.wd_no DESC";

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);

# 전체 게시물 수
$cnt_sql    = "SELECT count(*) as cnt FROM withdraw wd LEFT JOIN `work` as w ON w.w_no = wd.w_no WHERE {$add_where}";
$cnt_query  = mysqli_query($my_db, $cnt_sql);
$cnt_data   = mysqli_fetch_assoc($cnt_query);
$total_num  = $cnt_data['cnt'];

# 페이징 처리
$pages      = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num        = 20;
$pagenum    = ceil($total_num/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$offset = ($pages-1) * $num;
$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "project_expenses_withdraw_window.php", $pagenum, $search_url);

$smarty->assign("total_num", $total_num);
$smarty->assign("page", $pages);
$smarty->assign("search_url",$search_url);
$smarty->assign("pagelist", $pagelist);

# 리스트 쿼리
$withdraw_sql = "
    SELECT
        wd.wd_no,
        (SELECT pje.pj_no FROM project_expenses pje WHERE pje.wd_no = wd.wd_no AND pje.display='1' LIMIT 1) as pj_no,
        (SELECT pje.pj_name FROM project_expenses pje WHERE pje.wd_no = wd.wd_no AND pje.display='1' LIMIT 1) as pj_name,
        (SELECT pje.kind FROM project_expenses pje WHERE pje.wd_no = wd.wd_no AND pje.display='1' LIMIT 1) as pj_kind,
        w.c_name as w_c_name,
        (SELECT title FROM product prd WHERE prd.prd_no=w.prd_no) AS product_title,
        (SELECT s_name FROM staff s WHERE s.s_no=wd.reg_s_no) AS reg_s_name,
		DATE_FORMAT(wd.regdate, '%Y-%m-%d') as reg_day,
		wd.wd_date,
		wd.supply_cost,
        wd.c_name
    FROM withdraw wd
    LEFT JOIN `work` as w ON w.w_no = wd.w_no
    WHERE {$add_where}
    ORDER BY {$add_orderby}
    LIMIT {$offset}, {$num}
";
$withdraw_query = mysqli_query($my_db, $withdraw_sql);
$withdraw_list  = [];
while($withdraw = mysqli_fetch_array($withdraw_query))
{
    $pj_kind_name = "";
    if(!empty($withdraw['pj_kind'])){
        $pj_kind_sql    = "SELECT k_name FROM project_expenses_kind WHERE pj_k_no='{$withdraw['pj_kind']}'";
        $pj_kind_query  = mysqli_query($my_db, $pj_kind_sql);
        $pj_kind_result = mysqli_fetch_assoc($pj_kind_query);
        $pj_kind_name   = isset($pj_kind_result['k_name']) ? $pj_kind_result['k_name'] : "";
    }
    $withdraw['pj_kind_name'] = $pj_kind_name;
    $withdraw_list[] = $withdraw;
}

$pj_kind_list = [];
if(!empty($auto_pj_no))
{
    $pj_kind_sql = "SELECT * FROM project_expenses_kind WHERE pj_no='{$auto_pj_no}' ORDER BY k_priority ASC";
    $pj_kind_query = mysqli_query($my_db, $pj_kind_sql);
    while($pj_kind = mysqli_fetch_assoc($pj_kind_query))
    {
        $pj_kind_list[$pj_kind['pj_k_no']] = $pj_kind['k_name'];
    }
}
$smarty->assign("pj_kind_list", $pj_kind_list);
$smarty->assign("out_product_list", $out_product_list);
$smarty->assign("withdraw_list", $withdraw_list);

$smarty->display('project_expenses_withdraw_window.html');
?>