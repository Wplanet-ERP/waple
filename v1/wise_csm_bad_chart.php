<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Kind.php');
require('inc/model/Staff.php');

# 검색조건
$add_where              = "1=1";
$add_return_where       = "bad_reason != ''";
$sch_csm_g1             = isset($_GET['sch_csm_g1']) ? $_GET['sch_csm_g1'] : "";
$sch_csm_g2             = isset($_GET['sch_csm_g2']) ? $_GET['sch_csm_g2'] : "";
$kind_model             = Kind::Factory();
$csm_group_total_list   = $kind_model->getKindGroupList("bad_reason", 0);
$csm_g1_list = $csm_g2_list = [];
foreach($csm_group_total_list as $key => $asset_data)
{
    if(!$key){
        $csm_g1_list = $asset_data;
    }else{
        $csm_g2_list[$key] = $asset_data;
    }
}
$sch_csm_g2_list = isset($csm_g2_list[$sch_csm_g1]) ? $csm_g2_list[$sch_csm_g1] : [];

$kind_total_name_list   = $kind_model->getKindDisplayNameList("bad_reason");
$csm_title_total_list   = $kind_total_name_list['k_total_list'];
$csm_g1_title_list      = $kind_total_name_list['k_parent_list'];
$csm_g2_title_list      = $kind_total_name_list['k_child_list'];
$picker_list            = [];
if($sch_csm_g2){
    $add_where          .= " AND cbr.k_name_code ='{$sch_csm_g2}'";
    $add_return_where   .= " AND wcr.bad_reason ='{$sch_csm_g2}'";
    $add_column_where    = " AND k_name_code ='{$sch_csm_g2}'";

    $picker_list = array($sch_csm_g2 => array("title" => $csm_g2_title_list[$sch_csm_g2]));
}elseif($sch_csm_g1){
    $add_where         .= " AND (SELECT k_parent FROM kind k WHERE k.k_name_code=`cbr`.k_name_code) ='{$sch_csm_g1}'";
    $add_return_where  .= " AND (SELECT k_parent FROM kind k WHERE k.k_name_code=`wcr`.bad_reason) ='{$sch_csm_g1}'";
    $add_column_where   = " AND k_parent ='".$sch_csm_g1."'";
    $picker_list = $csm_title_total_list[$sch_csm_g1];
}else{
    $picker_list = $csm_g1_title_list;
}

$smarty->assign("sch_csm_g1", $sch_csm_g1);
$smarty->assign("sch_csm_g2", $sch_csm_g2);
$smarty->assign("csm_g1_list", $csm_g1_list);
$smarty->assign("csm_g2_list", $sch_csm_g2_list);

# 날짜별 검색
$today_s_w		 = date('w')-1;
$sch_date_type   = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "3";
$sch_date        = isset($_GET['sch_date']) ? $_GET['sch_date'] : date('Y-m');
$sch_s_month     = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m');
$sch_e_month     = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week      = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week      = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date      = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-10 day"));
$sch_e_date      = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_date', $sch_date);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);

$sch_s_no       = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";

if(!empty($sch_s_no))
{
    $add_where  .= " AND cbr.s_no='{$sch_s_no}'";
    $add_return_where   .= " AND `wcr`.req_s_no = '{$sch_s_no}'";
    $smarty->assign('sch_s_no', $sch_s_no);
}


# CHART
# 전체 기간 조회 및 누적데이터 조회
$all_date_where   = "";
$all_date_key	  = "";
$add_date_where   = "";
$add_date_column  = "";
$stats_list       = [];

$add_return_date_where  = "";
$add_return_date_column = "";

if($sch_date_type == '3') //월간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where  = " AND DATE_FORMAT(cbr.bad_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_date_column = "DATE_FORMAT(cbr.bad_date, '%Y%m')";

    $add_return_date_where   = " AND DATE_FORMAT(return_req_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_return_date_column  = "DATE_FORMAT(return_req_date, '%Y%m')";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}')";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where  = " AND DATE_FORMAT(cbr.bad_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_date_column = "DATE_FORMAT(DATE_SUB(cbr.bad_date, INTERVAL(IF(DAYOFWEEK(cbr.bad_date)=1,8,DAYOFWEEK(cbr.bad_date))-2) DAY), '%Y%m%d')";

    $add_return_date_where   = " AND DATE_FORMAT(return_req_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_return_date_column  = "DATE_FORMAT(DATE_SUB(return_req_date, INTERVAL(IF(DAYOFWEEK(return_req_date)=1,8,DAYOFWEEK(return_req_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '$sch_e_date')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_date_where  = " AND DATE_FORMAT(cbr.bad_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '$sch_e_date' ";
    $add_date_column = "DATE_FORMAT(cbr.bad_date, '%Y%m%d')";

    $add_return_date_where   = " AND DATE_FORMAT(return_req_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_return_date_column  = "DATE_FORMAT(return_req_date, '%Y%m%d')";
}

$all_date_where .= " AND DATE_FORMAT(`allday`.Date, '%Y-%m-%d') NOT IN (SELECT DATE_FORMAT(h.h_date, '%Y-%m-%d') FROM holiday_info h) AND (DATE_FORMAT(`allday`.Date, '%w') != 0 AND DATE_FORMAT(`allday`.Date, '%w') != 6)";
$add_date_where .= " AND DATE_FORMAT(cbr.bad_date, '%Y-%m-%d') NOT IN (SELECT DATE_FORMAT(h.h_date, '%Y-%m-%d') FROM holiday_info h) AND (DATE_FORMAT(cbr.bad_date, '%w') != 0 AND DATE_FORMAT(cbr.bad_date, '%w') != 6)";

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";

# 기본 STAT 리스트 Init 및 x key 및 타이틀 설정
$x_label_list       = [];
$x_table_th_list    = [];
$stats_list         = [];
if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        $x_label_list[$date['chart_key']]    = "'".$chart_title."'";
        $x_table_th_list[$date['chart_key']] = $chart_title;

        foreach($picker_list as $k_name_code => $picker_data){
            $stats_list[$date['chart_key']][$k_name_code] = 0;
        }
    }
}

# Y 컬럼 Label
$y_label_list = [];
$y_label_title = [];
foreach($picker_list as $k_name_code => $picker_title)
{
    $y_label_list[$k_name_code]  = $picker_title;
    $y_label_title[$k_name_code] = $picker_title;
    $y_label_title['all'] = "합산";
}

$stats_sql = "
    SELECT
        {$add_date_column} as key_date,
        (SELECT k.k_name_code FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cbr`.k_name_code)) as csm_g1,
        cbr.k_name_code as csm_g2,
        cbr.qty as qty
    FROM csm_bad_report as cbr
    WHERE {$add_where} 
    {$add_date_where}
    ORDER BY key_date ASC
";
$stats_query = mysqli_query($my_db, $stats_sql);
while($stats = mysqli_fetch_assoc($stats_query))
{
    $stats_key = (empty($sch_csm_g1)) ? $stats['csm_g1'] : $stats['csm_g2'];
    $stats_list[$stats['key_date']][$stats_key] +=  $stats['qty'];
}

$return_sql = "
    SELECT
        {$add_return_date_column} as key_date,
        (SELECT k.k_name_code FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`wcr`.bad_reason)) as csm_g1,
        wcr.bad_reason as csm_g2,
        1 as qty,
        DATE_FORMAT(return_req_date, '%Y%m%d') as bad_date        
    FROM work_cms_return wcr
    WHERE {$add_return_where}
    {$add_return_date_where}
    ORDER BY key_date ASC
";
$return_query = mysqli_query($my_db, $return_sql);
while($return = mysqli_fetch_assoc($return_query))
{
    $stats_key_val  = (empty($sch_csm_g1)) ? $return['csm_g1'] : $return['csm_g2'];
    $stats_key      = str_pad($stats_key_val, 5, "0", STR_PAD_LEFT);

    $stats_list[$return['key_date']][$stats_key] += $return['qty'];
}

$full_data = [];
foreach($stats_list as $bad_date => $stats_work_data)
{
    $date_qty_sum = 0;
    if($stats_work_data)
    {
        $qty_sum = 0;
        foreach ($stats_work_data as $key => $qty)
        {
            $full_data["each"]["line"][$key]['title']   = $picker_list[$key];
            $full_data["each"]["line"][$key]['data'][]  = $qty;

            $full_data["each"]["bar"][$key]['title']  = $picker_list[$key];
            $full_data["each"]["bar"][$key]['data'][] = $qty;
            $qty_sum += $qty;
            $date_qty_sum += $qty;
        }
    }

    $full_data["sum"]["line"]["all"]['title'] = "합산";
    $full_data["sum"]["line"]["all"]['data'][] = $date_qty_sum;
    $full_data["sum"]["bar"]["all"]['title']  = "합산";
    $full_data["sum"]["bar"]["all"]['data'][] = $date_qty_sum;
}

$staff_model    = Staff::Factory();
$cs_staff_list  = $staff_model->getActiveTeamStaff("00244");
$cs_staff_list['309'] = "이채하";

$smarty->assign('cs_staff_list', $cs_staff_list);
$smarty->assign('x_label_list', implode(',', $x_label_list));
$smarty->assign('x_table_th_list', json_encode($x_table_th_list));
$smarty->assign('legend_list', json_encode($y_label_list));
$smarty->assign('y_label_title', json_encode($y_label_title));
$smarty->assign('picker_list', $picker_list);
$smarty->assign('full_data', json_encode($full_data));

$smarty->display('wise_csm_bad_chart.html');
?>
