<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/staff.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');

# Process 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'staff_delete')
{
    $s_no_post  = isset($_POST['s_no']) ? $_POST['s_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $file_path  = isset($_POST['profile_img']) ? $_POST['profile_img'] : "";

    if(!empty($s_no_post))
    {
        $password_val   = "!1234!";
        $password       = substr(md5($password_val), 8, 16);

        $upd_sql      = "UPDATE staff SET pass='{$password}', email=NULL, tel=NULL, hp=NULL, extension=NULL, profile_img=NULL WHERE s_no='{$s_no_post}'";
        $upd_strength = "DELETE FROM staff_strength WHERE s_no='{$s_no_post}'";

        if(mysqli_query($my_db, $upd_sql) && mysqli_query($my_db, $upd_strength))
        {
            if($file_path)
            {
                $del_file    = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$file_path}";
                if(file_exists($del_file))
                {
                    unlink($del_file);
                }
            }

            exit("<script>alert('개인정보를 삭제했습니다');location.href='staff_list.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('개인정보 삭제에 실패했습니다');location.href='staff_list.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('개인정보 삭제에 실패했습니다');location.href='staff_list.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "74";
$nav_title   = "계정 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where          = "1=1";
$sch_staff_state    = isset($_GET['sch_staff_state']) ? $_GET['sch_staff_state'] : "1";
$sch_permission     = isset($_GET['sch_permission']) ? $_GET['sch_permission'] : "";
$sch_team		    = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_name		    = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
$sch_s_keyword	    = isset($_GET['sch_s_keyword']) ? $_GET['sch_s_keyword'] : "";
$sch_tel	        = isset($_GET['sch_tel']) ? $_GET['sch_tel'] : "";

if(!empty($sch_staff_state)) {
    $add_where.=" AND s.staff_state='{$sch_staff_state}'";
    $smarty->assign("sch_staff_state", $sch_staff_state);
}

if(!empty($sch_permission)) {
    $add_where_permission = str_replace("0", "_", $sch_permission);
    $add_where           .= " AND s.permission like '{$add_where_permission}'";
    $smarty->assign("sch_permission", $sch_permission);
}

if(!empty($sch_team)) {
  if ($sch_team != "all") {
    $sch_team_code_where = getTeamWhere($my_db, $sch_team);
    $add_where          .= " AND team IN ({$sch_team_code_where})";
  }
  $smarty->assign("sch_team", $sch_team);
} else {
  $sch_team_code_where  = getTeamWhere($my_db, $session_team);
  $add_where            .= " AND team IN ({$sch_team_code_where})";
  $smarty->assign("sch_team", $session_team);
}

if(!empty($sch_s_name)) {
    $add_where  .= " AND s.s_name LIKE '%{$sch_s_name}%'";
    $smarty->assign("sch_s_name", $sch_s_name);
}

if(!empty($sch_s_keyword)) {
    $add_where  .= " AND (SELECT GROUP_CONCAT(ss.keyword SEPARATOR ' ∙ ') FROM staff_strength AS ss WHERE ss.s_no = s.s_no GROUP BY ss.s_no) LIKE '%{$sch_s_keyword}%'";
    $smarty->assign("sch_s_keyword", $sch_s_keyword);
}

if(!empty($sch_tel)) {
    $add_where  .= " AND (tel LIKE '%{$sch_tel}%' OR hp LIKE '%{$sch_tel}%')";
    $smarty->assign("sch_tel", $sch_tel);
}

# 전체 게시물 수
$staff_cnt_sql      = "SELECT count(s_no) as cnt FROM staff s WHERE {$add_where}";
$staff_cnt_query    = mysqli_query($my_db, $staff_cnt_sql);
$staff_cnt_result   = mysqli_fetch_array($staff_cnt_query);
$total_num          = $staff_cnt_result['cnt'];

# 페이징 처리
$pages      = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num        = 20;
$offset     = ($pages-1) * $num;
$pagenum    = ceil($total_num/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "staff_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $total_num);
$smarty->assign("pagelist", $pagelist);

# 리더 체크
$team_leader_total_list = getTeamLeader($my_db);
$team_leader_list       = isset($team_leader_total_list[$session_s_no]) ? $team_leader_total_list[$session_s_no] : [];

# 리스트 쿼리
$staff_sql = "
    SELECT
        *,
        (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=s.my_c_no) AS my_c_name,
        (SELECT mc.kind_color FROM my_company mc WHERE mc.my_c_no=s.my_c_no) AS my_c_kind_color,
        (SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=s.partner_c_no) as partner_c_name,
        (SELECT GROUP_CONCAT(ss.keyword SEPARATOR ' ∙ ') FROM staff_strength AS ss WHERE ss.s_no = s.s_no GROUP BY ss.s_no ORDER BY ss.priority ASC) as strength_keyword,
        (SELECT count(cc_no) as card_cnt FROM corp_card as `cc` WHERE manager=s.s_no AND `cc`.card_type='3') as card_cnt,
        (SELECT sub.q_no FROM quick_search as sub WHERE sub.`type`='staff' AND sub.s_no='{$session_s_no}' AND sub.prd_no=s.s_no) AS quick_prd
    FROM staff s
    WHERE {$add_where}
    ORDER BY s_no DESC
    LIMIT {$offset}, {$num}
";
$staff_query = mysqli_query($my_db, $staff_sql);
$staff_list  = [];
$staff_state_option = getStaffStateOption();
while($staff_array = mysqli_fetch_array($staff_query))
{
    $team_list = !empty($staff_array['team_list']) ? explode(',', $staff_array['team_list']) : array($staff_array['team']);
    $t_label_list = [];
    foreach($team_list as $team_code)
    {
        $team_sql = "SELECT team_code, depth FROM team WHERE team_code='{$team_code}'";
        $team_query = mysqli_query($my_db, $team_sql);
        $team_result = mysqli_fetch_assoc($team_query);

        $t_code  = $team_result['team_code'];
        $t_depth = $team_result['depth'];
        $t_label_list[] = getTeamFullName($my_db, $t_depth, $t_code);
    }

    $is_leader = false;
    if(!empty($team_leader_list) && in_array($staff_array['team'], $team_leader_list))
    {
        $is_leader = true;
    }

    $person_state = false;
    if(empty($staff_array['email']) && empty($staff_array['tel']) && empty($staff_array['extension']) && empty($staff_array['hp']) && empty($staff_array['profile_img']))
    {
        $person_state = true;
    }

    $staff_array["state_name"]      = $staff_state_option[$staff_array['staff_state']];
    $staff_array["is_leader"]       = $is_leader;
    $staff_array["person_state"]    = $person_state;
    $staff_array["team_name_list"]  = $t_label_list;

    $staff_list[] = $staff_array;
}

# 팀 옵션
$team_model         = Team::Factory();
$sch_team_name_list = $team_model->getTeamFullNameList();

$smarty->assign("staff_state_option", $staff_state_option);
$smarty->assign("sch_team_list", $sch_team_name_list);
$smarty->assign("staff_list", $staff_list);

$smarty->display('staff_list.html');
?>
