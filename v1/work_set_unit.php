<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');
require('inc/model/MyQuick.php');

$proc = (isset($_POST['process'])) ? $_POST['process'] : "";


if($proc=="display_off") {
	$wsu_no=(isset($_POST['wsu_no']))?$_POST['wsu_no']:"";
	$search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

	$sql = "UPDATE work_set_unit w_s SET w_s.display = '2' WHERE w_s.wsu_no = '" . $wsu_no . "'";
	mysqli_query($my_db,$sql);

	exit("<script>location.href='work_set_unit.php?$search_url_get';</script>");

}elseif($proc=="display_on") {
	$wsu_no=(isset($_POST['wsu_no']))?$_POST['wsu_no']:"";
	$search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

	$sql = "UPDATE work_set_unit w_s SET w_s.display = '1' WHERE w_s.wsu_no = '" . $wsu_no . "'";
	mysqli_query($my_db,$sql);

	exit("<script>location.href='board_notice_list.php?$search_url_get';</script>");

} else {

	# Navigation & My Quick
	$nav_prd_no  = "13";
	$nav_title   = "업무세트 구성 관리";
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_title", $nav_title);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	// 리스트 페이지 쿼리 저장
	$save_query=http_build_query($_GET);
	$smarty->assign("save_query",$save_query);

  // 담당자리스트
	$staff_sql="SELECT s_no,s_name FROM staff WHERE staff_state = '1' AND staff_state = '1'";
	$staff_query=mysqli_query($my_db,$staff_sql);

	while($staff_data=mysqli_fetch_array($staff_query)) {
		$staffs[]=array(
			'no'=>$staff_data['s_no'],
			'name'=>$staff_data['s_name']
		);
	}

	$smarty->assign("staff",$staffs);

	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where = " 1=1";
	$add_orderby = " w_s.wsu_no DESC";

	$sch_wsu_no_get = isset($_GET['sch_wsu_no']) ? $_GET['sch_wsu_no'] : "";
	$sch_s_no_get = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
	$sch_team_get = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
	$sch_display_get = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
	$sch_title = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";


	if (!empty($sch_wsu_no_get)) {
		$add_where .= " AND w_s.wsu_no='" . $sch_wsu_no_get . "'";
		$smarty->assign("sch_wsu_no", $sch_wsu_no_get);
	}

	if (!empty($sch_s_no_get)) {
		if($sch_s_no_get != 'all'){
			$add_where .= " AND (w_s.s_no = '{$sch_s_no_get}' or w_s.s_no = 0) ";
		}
		$smarty->assign("sch_s_no",$sch_s_no_get);
	}

	if (!empty($sch_team_get)) {
		$add_where .= " AND (SELECT team FROM staff s WHERE s.s_no=w_s.s_no)='" . $sch_team_get . "'";
		$smarty->assign("sch_team", $sch_team_get);
	}

	if (!empty($sch_display_get)) {
		$add_where .= " AND w_s.display = '{$sch_display_get}'";
		$smarty->assign("sch_display", $sch_display_get);
	}

	if (!empty($sch_title)) {
		$add_where .= " AND w_s.title like '%" . $sch_title . "%'";
		$smarty->assign("sch_title", $sch_title);
	}

	// 페이지에 따른 limit 설정
	$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num = 20;
	$offset = ($pages-1) * $num;

	// work_set_unit 리스트 쿼리
	$work_set_unit_sql = "
								SELECT
									w_s.wsu_no,
									w_s.title,
									w_s.s_no,
									(SELECT s_name FROM staff s where s.s_no=w_s.s_no) AS s_name,
									(SELECT team FROM staff s WHERE s.s_no=w_s.s_no) AS team,
									w_s.regdate,
									w_s.display
								FROM work_set_unit w_s
								WHERE $add_where
								ORDER BY $add_orderby
								";

	//echo "<br><br>".$work_set_unit_sql."<br><br>";

	// 전체 게시물 수
	$cnt_sql = "SELECT count(*) FROM (".$work_set_unit_sql.") AS cnt";
	$result= mysqli_query($my_db, $cnt_sql);
	$cnt_data=mysqli_fetch_array($result);
	$total_num = $cnt_data[0];

	$smarty->assign(array(
		"total_num"=>number_format($total_num)
	));

	$smarty->assign("total_num",$total_num);
	$pagenum = ceil($total_num/$num);


	// 페이징
	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	$search_url = "sch_wsu_no=".$sch_wsu_no_get.
			"&sch_s_no=".$sch_s_no_get.
			"&sch_team=".$sch_team_get.
			"&sch_display=".$sch_display_get.
			"&sch_title=".$sch_title;

	$smarty->assign("search_url",$search_url);
	$page=pagelist($pages, "work_set_unit.php", $pagenum, $search_url);
	$smarty->assign("pagelist",$page);

	// 리스트 쿼리 결과(데이터)
	//echo "$work_set_unit_sql<br>"; //exit;
	$work_set_unit_sql .= "LIMIT $offset,$num";
	$result= mysqli_query($my_db, $work_set_unit_sql);

	while($work_set_unit_array = mysqli_fetch_array($result)){

		if(!empty($work_set_unit_array['regdate'])) {
			$regdate_day_value = date("Y/m/d",strtotime($work_set_unit_array['regdate']));
			$regdate_time_value = date("H:i",strtotime($work_set_unit_array['regdate']));
		}else{
			$regdate_day_value="";
			$regdate_time_value="";
		}

		$work_set_unit[] = array(
			"wsu_no"=>$work_set_unit_array['wsu_no'],
			"title"=>$work_set_unit_array['title'],
			"s_no"=>$work_set_unit_array['s_no'],
			"s_name"=>$work_set_unit_array['s_name'],
			"team"=>$work_set_unit_array['team'],
			"regdate_day"=>$regdate_day_value,
			"regdate_time"=>$regdate_time_value,
			"display"=>$work_set_unit_array['display'],
		);
	}
	$smarty->assign("work_set_unit", $work_set_unit);

	$smarty->display('work_set_unit.html');
}
?>
