<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "수령자명")
	->setCellValue('B1', "수령자 전화")
	->setCellValue('C1', "우편번호")
	->setCellValue('D1', "수령지")
	->setCellValue('E1', "재구매 수량")
	->setCellValue('F1', "금액 합계")
;

$today_val    = date('Y-m-d');
$months_val3  = date('Y-m-d', strtotime('-3 months'));
$months_val6  = date('Y-m-d', strtotime('-6 months'));
$year_val     = date('Y-m-d', strtotime('-1 years'));

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_get = (isset($_GET['sch']))?$_GET['sch']:"";

$add_where      = "1=1";
$add_sub_where  = "1=1";
$add_group      = "";
$add_having     = "";
$add_group_list     = [];
$btn_url_type_list  = [];

# 상품(업무) 조건
if (!empty($sch_prd) && $sch_prd != "0") { // 상품
    $add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

$sch_ord_s_date         = isset($_GET['sch_ord_s_date']) ? $_GET['sch_ord_s_date'] : $months_val3;
$sch_ord_e_date         = isset($_GET['sch_ord_e_date']) ? $_GET['sch_ord_e_date'] : $today_val;
$sch_ord_date_type      = isset($_GET['sch_ord_date_type']) ? $_GET['sch_ord_date_type'] : "3months";
$sch_recipient          = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp       = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_zipcode            = isset($_GET['sch_zipcode']) ? $_GET['sch_zipcode'] : "";
$sch_recipient_addr     = isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
$sch_same_quantity_type = isset($_GET['sch_same_quantity_type']) ? $_GET['sch_same_quantity_type'] : "1";
$sch_same_quantity_1    = isset($_GET['sch_same_quantity_1']) ? $_GET['sch_same_quantity_1'] : "3";
$sch_same_quantity      = 3;
$sch_not_sel_same       = "";
$ori_ord_type           = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "sum_price";
$ord_type 		        = isset($_GET['ord_type']) ? $_GET['ord_type'] : "sum_price";

if(empty($sch_recipient) && empty($sch_recipient_hp) && empty($sch_zipcode) && empty($sch_recipient_addr))
{
    $sch_not_sel_same   = '1';
    $sch_recipient      = '1';
    $sch_recipient_hp   = '1';
}

if($sch_ord_date_type != 'total'){
    if(!empty($sch_ord_s_date) && !empty($sch_ord_e_date)){
        $add_where .= " AND w.order_date BETWEEN '{$sch_ord_s_date}' AND '{$sch_ord_e_date}'";
    }
}

if($sch_ord_date_type != 'total'){
    if(!empty($sch_ord_s_date) && !empty($sch_ord_e_date)){
        $add_where      .= " AND (w.order_date BETWEEN '{$sch_ord_s_date}' AND '{$sch_ord_e_date}')";
    }
}

if(!empty($sch_recipient))
{
    $btn_url_type_list[] = "recipient";
    $add_group_list[]    = "recipient";
    $add_where      .= " AND (w.recipient IS NOT NULL AND w.recipient != '')";
    $add_sub_where  .= " AND sub.recipient=w.recipient";
}

if(!empty($sch_recipient_hp))
{
    $btn_url_type_list[] = "recipient_hp";
    $add_group_list[]    = "recipient_hp";
    $add_where      .= " AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '')";
    $add_sub_where  .= " AND sub.recipient_hp=w.recipient_hp";
}

if(!empty($sch_zipcode))
{
    $btn_url_type_list[] = "zip_code";
    $add_group_list[]    = "zip_code";
    $add_where      .= " AND (w.zip_code IS NOT NULL AND w.zip_code != '')";
    $add_sub_where  .= " AND sub.zip_code=w.zip_code";
}

if(!empty($sch_recipient_addr))
{
    $btn_url_type_list[] = "recipient_addr";
    $add_group_list[]    = "recipient_addr";
    $add_where      .= " AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '')";
    $add_sub_where  .= " AND sub.recipient_addr=w.recipient_addr";
}

if(!empty($add_group_list))
{
    $add_group  = implode(', ', $add_group_list);
}

switch($sch_same_quantity_type){
    case '1':
        $sch_same_quantity = $sch_same_quantity_1;
        break;
}

if(!!$add_group)
{
    $add_having  .= "ord_cnt >= {$sch_same_quantity}";
}

$sch_same_quantity_type_sub = isset($_GET['sch_same_quantity_type_sub']) ? $_GET['sch_same_quantity_type_sub'] : "";
$sch_same_quantity_sub      = isset($_GET['sch_same_quantity_sub']) ? $_GET['sch_same_quantity_sub'] : "";
if(!empty($sch_same_quantity_type_sub) && !empty($sch_same_quantity_sub))
{
    $sub_chk_sql = "
        SELECT
            (SELECT sub.w_no FROM work_cms_recent sub WHERE {$add_sub_where} ORDER BY sub.w_no DESC LIMIT 1) as w_no,
            w.recipient, w.recipient_hp, w.zip_code, w.recipient_addr, SUM(w.quantity) as prd_cnt, SUM(w.dp_price_vat) as prd_sum
        FROM work_cms_recent w
        WHERE {$add_where} 
        GROUP BY {$add_group}, prd_no
        HAVING prd_cnt > '{$sch_same_quantity_sub}' AND prd_sum > 0
    ";

    $sub_chk_query = mysqli_query($my_db, $sub_chk_sql);
    $sub_chk_list  = [];
    $ord_chk_list  = [];
    while($sub_chk_result = mysqli_fetch_assoc($sub_chk_query))
    {
        $add_recent_where = $add_where;

        if(!empty($sch_recipient))
        {
            $add_recent_where .= " AND w.recipient='{$sub_chk_result['recipient']}'";
        }

        if(!empty($sch_recipient_hp))
        {
            $add_recent_where .= " AND w.recipient_hp='{$sub_chk_result['recipient_hp']}'";
        }

        if(!empty($sch_zipcode))
        {
            $add_recent_where .= " AND w.zip_code='{$sub_chk_result['zip_code']}'";
        }

        if(!empty($sch_recipient_addr))
        {
            $add_recent_where .= " AND w.recipient_addr='{$sub_chk_result['recipient_addr']}'";
        }

        if(!isset($sub_chk_list[$sub_chk_result['w_no']]))
        {
            $sub_chk_list[$sub_chk_result['w_no']] = 1;
            $ord_chk_sql    = "SELECT DISTINCT order_number FROM work_cms_recent w WHERE {$add_recent_where}";
            $ord_chk_query  = mysqli_query($my_db, $ord_chk_sql);
            while($ord_chk_result = mysqli_fetch_assoc($ord_chk_query)){
                $ord_chk_list[] = "'".$ord_chk_result['order_number']."'";
            }
        }
    }

    if(!empty($ord_chk_list)){
        $ord_chk = implode(',', $ord_chk_list);
        $add_where .= " AND w.order_number NOT IN({$ord_chk})";
    }
}

$add_orderby = "";
$ord_type_by = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";
    $orderby_val = "DESC";
    if(!empty($ord_type_by))
    {
        if($ori_ord_type == $ord_type){
            if($ord_type_by == '1'){
                $orderby_val = "ASC";
            }elseif($ord_type_by == '2'){
                $orderby_val = "DESC";
            }
        }else{
            $ord_type_by = '2';
            $orderby_val = "DESC";
        }
    }

    $add_orderby = "{$ord_type} {$orderby_val}";
}

$mass_cms_sql = "
    SELECT
        w.recipient,
        w.recipient_hp,
        w.zip_code,
        w.recipient_addr,
        COUNT(DISTINCT w.order_number) as ord_cnt,
        SUM(w.dp_price_vat) as sum_price
    FROM  work_cms_recent w
    WHERE {$add_where}
    GROUP BY {$add_group}
    HAVING {$add_having}
    ORDER BY {$add_orderby}
		LIMIT 5000
";

$idx = 2;
$mass_cms_query = mysqli_query($my_db, $mass_cms_sql);
while($mass_cms = mysqli_fetch_assoc($mass_cms_query))
{
    $ord_cnt   = number_format($mass_cms['ord_cnt']);
    $sum_price      = number_format($mass_cms['sum_price']);
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$idx, $mass_cms['recipient'])
        ->setCellValue('B'.$idx, $mass_cms['recipient_hp'])
        ->setCellValue('C'.$idx, $mass_cms['zip_code'])
        ->setCellValue('D'.$idx, $mass_cms['recipient_addr'])
        ->setCellValue('E'.$idx, $ord_cnt)
        ->setCellValue('F'.$idx, $sum_price)
    ;

    $idx++;
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:F'.$idx)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC0C0C0');

$objPHPExcel->getActiveSheet()->getStyle('A2:F'.$idx)->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A2:A'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B2:B'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C2:C'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D2:D'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('E2:E'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('F2:F'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle('D2:D'.$idx)->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);

$objPHPExcel->getActiveSheet()->setTitle("재구매 조회");


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_재구매 조회.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
