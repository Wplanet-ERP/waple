<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "주문일시")
	->setCellValue('B3', "주문번호")
	->setCellValue('C3', "결제일시")
	->setCellValue('D3', "상품명")
	->setCellValue('E3', "수량")
	->setCellValue('F3', "결제금액")
	->setCellValue('G3', "수령자명")
	->setCellValue('H3', "수령자전화")
	->setCellValue('I3', "수령자핸드폰")
	->setCellValue('J3', "우편번호")
	->setCellValue('K3', "수령지")
	->setCellValue('L3', "배송메모")
	->setCellValue('M3', "특이사항")
	->setCellValue('N3', "참고사항2")
	->setCellValue('O3', "택배사")
	->setCellValue('P3', "운송자번호")
;

// 리스트 쿼리
$temporary_sql = "
	SELECT
		DATE_FORMAT(wct.order_date, '%Y-%m-%d %H:%i') as order_date,
		wct.order_number,
		DATE_FORMAT(wct.payment_date, '%Y-%m-%d %H:%i') as payment_date,
		(SELECT title from product_cms prd_cms where prd_cms.prd_no=wct.prd_no) as prd_name,
		wct.quantity,
		wct.dp_price_vat,
		wct.recipient,
		wct.recipient_hp,
		wct.zip_code,
		wct.recipient_addr,
		wct.delivery_memo,
		wct.notice,
		wct.dp_c_name
	FROM work_cms_temporary wct
	ORDER BY wct.t_no ASC
";
$temporary_query = mysqli_query($my_db, $temporary_sql);
$idx = 4;
if(!!$temporary_query)
{
    while($temporary = mysqli_fetch_array($temporary_query))
    {
        $dp_price_vat = number_format($temporary['dp_price_vat']);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $temporary['order_date'])
            ->setCellValueExplicit("B{$idx}", $temporary['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("C{$idx}", $temporary['payment_date'])
            ->setCellValue("D{$idx}", $temporary['prd_name'])
            ->setCellValue("E{$idx}", $temporary['quantity'])
            ->setCellValue("F{$idx}", $dp_price_vat)
            ->setCellValue("G{$idx}", $temporary['recipient'])
            ->setCellValue("H{$idx}", $temporary['recipient_hp'])
            ->setCellValue("I{$idx}", $temporary['recipient_hp'])
            ->setCellValue("J{$idx}", $temporary['zip_code'])
            ->setCellValue("K{$idx}", trim($temporary['recipient_addr']))
			->setCellValueExplicit("L{$idx}", $temporary['delivery_memo'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("M{$idx}", $temporary['notice'])
            ->setCellValue("N{$idx}", $temporary['dp_c_name'])
            ->setCellValue("O{$idx}", "")
            ->setCellValueExplicit("P{$idx}", "", PHPExcel_Cell_DataType::TYPE_STRING);

        $idx++;
    }
}

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
$objPHPExcel->getActiveSheet()->setCellValue('A1', "위드플레이스 양식");
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setSize(18);
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A3:P3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
$objPHPExcel->getActiveSheet()->getStyle('A3:P3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A3:P{$idx}")->getFont()->setSize(8);;
$objPHPExcel->getActiveSheet()->getStyle("A3:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A3:P{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("K4:K{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("L4:L{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("M4:M{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("N4:N{$idx}")->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);

$objPHPExcel->getActiveSheet()->setTitle('위드플레이스 양식');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_위드플레이스 양식.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
