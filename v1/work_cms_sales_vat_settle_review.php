<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/ProductCmsUnit.php');

# Navigation & My Quick
$nav_prd_no  = "166";
$nav_title   = "부가세 및 정산 검토";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$dp_except_list     = getNotApplyDpList();
$dp_except_text     = implode(",", $dp_except_list);
$add_cms_where      = "1=1 AND dp_c_no NOT IN({$dp_except_text})";
$add_settle_where   = "";
$add_vat_where      = "";
$prev_month         = date('Y-m', strtotime("-1 months"));
$sch_review_month   = isset($_GET['sch_review_month']) ? $_GET['sch_review_month'] : $prev_month;
$sch_c_no           = isset($_GET['sch_c_no']) ? $_GET['sch_c_no'] : "";


if(!empty($sch_review_month))
{
    $sch_review_s_date   = $sch_review_month."-01";
    $sch_review_e_day    = date("t", strtotime($sch_review_s_date));
    $sch_review_e_date   = $sch_review_month."-".$sch_review_e_day;

    $sch_review_s_datetime  = $sch_review_s_date." 00:00:00";
    $sch_review_e_datetime  = $sch_review_e_date." 23:59:59";
    $add_cms_where          .= " AND (order_date BETWEEN '{$sch_review_s_datetime}' AND '{$sch_review_e_datetime}')";

    $smarty->assign('sch_review_s_date', $sch_review_s_date);
    $smarty->assign('sch_review_e_date', $sch_review_e_date);
    $smarty->assign('sch_review_month', $sch_review_month);
}

if(!empty($sch_c_no))
{
    $add_cms_where      .= " AND c_no='{$sch_c_no}'";
    $add_settle_where   = " AND wcs.c_no='{$sch_c_no}'";
    $add_vat_where      = " AND wcv.c_no='{$sch_c_no}'";
    $smarty->assign('sch_c_no', $sch_c_no);
}

# 부가세 정산 판매처 리스트
$company_model      = Company::Factory();
$unit_model         = ProductCmsUnit::Factory();
$dp_company_list    = $company_model->getDpTotalList();
$brand_option       = $unit_model->getBrandData();

$sales_review_list  = [];
foreach($dp_company_list as $dp_c_no => $dp_c_name)
{
    $sales_review_list[$dp_c_no] = array(
        "c_name"            => $dp_c_name,
        "ord_cnt"           => 0,
        "vat_cnt"           => 0,
        "not_vat_cnt"       => 0,
        "settle_cnt"        => 0,
        "not_settle_cnt"    => 0,
        "not_settle_per"    => 0,
        "ord_price"         => 0,
        "vat_price"         => 0,
        "not_vat_price"     => 0,
        "not_vat_per"       => 0,
        "settle_price"      => 0,
        "not_settle_price"  => 0,
        "cancel_cnt"        => 0,
        "return_cnt"        => 0,
    );
}

# 부가세&정산 계산
$sales_review_vat_sql = "
    SELECT
        dp_c_no,
        dp_c_name,
        COUNT(DISTINCT ord_no) AS ord_cnt,
        SUM(ord_price) AS ord_price, 
        SUM(vat_cnt) AS total_vat_cnt,
        SUM(vat_price) AS total_vat_price,
        SUM(settle_cnt) AS total_settle_cnt,
        SUM(settle_price) AS total_settle_price,
        SUM(IF(settle_cnt > 0, 0, cancel_cnt)) AS total_cancel_cnt,
        SUM(IF(settle_cnt > 0, 0, return_cnt)) AS total_return_cnt
    FROM
    (
        SELECT
            w.dp_c_no,
            w.dp_c_name,
            w.ord_no,
            w.ord_price,
            (SELECT COUNT(DISTINCT wcv.order_number) FROM work_cms_vat wcv WHERE wcv.order_number=w.ord_no {$add_vat_where}) AS vat_cnt,
            (SELECT SUM(wcv.card_price+wcv.cash_price+wcv.etc_price+wcv.tax_price) FROM work_cms_vat wcv WHERE wcv.order_number=w.ord_no {$add_vat_where}) AS vat_price,
            (SELECT COUNT(DISTINCT wcs.order_number) FROM work_cms_settlement wcs WHERE wcs.order_number=w.ord_no {$add_settle_where}) AS settle_cnt,
            (SELECT SUM(wcs.settle_price) FROM work_cms_settlement wcs WHERE wcs.order_number=w.ord_no {$add_settle_where}) AS settle_price,
            IF(w.is_cancel > 0, 1, 0) AS cancel_cnt,      
            (SELECT COUNT(DISTINCT wcr.parent_order_number) as cnt FROM work_cms_return wcr WHERE wcr.parent_order_number=w.ord_no) AS return_cnt
        FROM
        (
            SELECT 
                IF(origin_ord_no='', order_number, origin_ord_no) AS ord_no,
                IF(delivery_state='5',1,0) as is_cancel,
                dp_c_no,
                (SELECT c.c_name FROM company c WHERE c.c_no=w.dp_c_no) as dp_c_name,
                SUM(unit_price+unit_delivery_price) AS ord_price
            FROM work_cms w 
            WHERE {$add_cms_where}
            GROUP BY ord_no
        ) AS w
    ) AS main
    GROUP BY dp_c_no
";
$sales_review_vat_query = mysqli_query($my_db, $sales_review_vat_sql);
while($sales_review = mysqli_fetch_assoc($sales_review_vat_query))
{
    $not_vat_cnt    = $sales_review['ord_cnt']-$sales_review['total_vat_cnt'];
    $not_settle_cnt = $sales_review['ord_cnt']-$sales_review['total_settle_cnt'];

    $sales_review_list[$sales_review['dp_c_no']]['dp_c_name']       = $sales_review['dp_c_name'];
    $sales_review_list[$sales_review['dp_c_no']]['ord_cnt']         = $sales_review['ord_cnt'];
    $sales_review_list[$sales_review['dp_c_no']]['vat_cnt']         = $sales_review['total_vat_cnt'];
    $sales_review_list[$sales_review['dp_c_no']]['not_vat_cnt']     = $not_vat_cnt;
    $sales_review_list[$sales_review['dp_c_no']]['settle_cnt']      = $sales_review['total_settle_cnt'];
    $sales_review_list[$sales_review['dp_c_no']]['not_settle_cnt']  = $not_settle_cnt;
    $sales_review_list[$sales_review['dp_c_no']]['cancel_cnt']      = $sales_review['total_cancel_cnt'];
    $sales_review_list[$sales_review['dp_c_no']]['return_cnt']      = $sales_review['total_return_cnt'];

    if($not_settle_cnt > 0) {
        $sales_review_list[$sales_review['dp_c_no']]['not_settle_per'] = round($not_settle_cnt/$sales_review['ord_cnt']*100, 1);
    }

    $not_vat_price    = $sales_review['ord_price'] - $sales_review['total_vat_price'];
    $not_settle_price = $sales_review['ord_price'] - $sales_review['total_settle_price'];

    $sales_review_list[$sales_review['dp_c_no']]['ord_price']       = $sales_review['ord_price'];
    $sales_review_list[$sales_review['dp_c_no']]['vat_price']       = $sales_review['total_vat_price'];
    $sales_review_list[$sales_review['dp_c_no']]['not_vat_price']   = $not_vat_price;
    $sales_review_list[$sales_review['dp_c_no']]['settle_price']    = $sales_review['total_settle_price'];
    $sales_review_list[$sales_review['dp_c_no']]['not_settle_price']= $not_settle_price;

    if($not_vat_price > 0) {
        $sales_review_list[$sales_review['dp_c_no']]['not_vat_per'] = round($not_vat_price/$sales_review['ord_price']*100, 1);
    }
}

foreach($sales_review_list as $dp_c_no => $sales_review_data)
{
    if($sales_review_data['ord_cnt'] == 0 && $sales_review_data['vat_cnt'] == 0 && $sales_review_data['settle_cnt'] == 0 && $sales_review_data['ord_price'] == 0 && $sales_review_data['vat_price'] == 0 && $sales_review_data['settle_price'] == 0){
        unset($sales_review_list[$dp_c_no]);
    }
}

$smarty->assign("brand_option", $brand_option);
$smarty->assign("sales_review_list", $sales_review_list);

$smarty->display('work_cms_sales_vat_settle_review.html');
?>
