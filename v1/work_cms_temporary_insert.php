<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
include_once('inc/model/Company.php');
include_once('inc/model/ProductCms.php');
include_once('inc/model/WorkCms.php');


# 기본 엑셀 변수 설정
$choice_c_no   = (isset($_POST['choice_c_name'])) ? $_POST['choice_c_name'] : "";
$file_name     = $_FILES["temporary_file"]["tmp_name"];

$product_model      = ProductCms::Factory();
$company_model      = Company::Factory();
$cms_model          = WorkCms::Factory();
$cms_model->setMainInit("work_cms_temporary", "order_number");
$reservation_model  = WorkCms::Factory();
$reservation_model->setMainInit("work_cms_reservation", "order_number");

$dp_company_commerce_list = $company_model->getDpTotalList();

$total_cnt          = 0;
$sbn_check_list     = [];
$sticker_check_list = [];
$yank_check_list    = [];
$task_run_s_no      = $session_s_no;
$task_run_team      = $session_team;
$regdate            = date('Y-m-d H:i:s');
$limit_s_date       = date('Y-m-d', strtotime("-6 months"))." 00:00:00";
$limit_e_date       = date('Y-m-d', strtotime("+6 months"))." 00:00:00";

/** 누잠 이벤트 */
# 누잠 리유저블백
$chk_nuzam_all_prd_list         = array('NU0028','NU0029','NU0030','NU0031','NU0032','NU0033','NU0034','NU0035','NU0036','NU0037','NU0038','NU0039','NU0040','NU0041','NU0042','NU0043','NU0044','NU0045','NU0046','NU0047','NU0048','NU0049','NU0050','NU0051','NU0052','NU0053','NU0054','NU0092','NU0093','NU0094','NU0095',"NU0366","NU0367","NU0368","NU0369","NU0370","NU0371","NU0410","NU0411","NU0412","NU0413","NU0414","NU0415","NU0416","NU0417","NU0418","NU0419","NU0420","NU0421","NU0422","NU0423","NU0424","NU0425","NU0426","NU0427","NU0428","NU0429","NU0430");
$chk_nuzam_total_prd_list       = array('NU0028','NU0029','NU0030','NU0031','NU0032','NU0033','NU0034','NU0035','NU0036','NU0037','NU0038','NU0039','NU0040','NU0041','NU0042','NU0043','NU0044','NU0045','NU0046','NU0047','NU0048','NU0049','NU0050','NU0051','NU0052','NU0053','NU0054','NU0092','NU0093','NU0094','NU0095');
$chk_nuzam_total_new_prd_list   = array("NU0366","NU0367","NU0368","NU0369","NU0370","NU0371","NU0410","NU0411","NU0412","NU0413","NU0414","NU0415","NU0416","NU0417","NU0418","NU0419","NU0420","NU0421","NU0422","NU0423","NU0424","NU0425","NU0426","NU0427","NU0428","NU0429","NU0430");

$chk_nuzam_ss_single_list       = array("NU0366","NU0367","NU0368");
$chk_nuzam_q_single_list        = array("NU0369","NU0370","NU0371");
$chk_nuzam_ss_double_list       = array("NU0410","NU0411","NU0414","NU0415","NU0413","NU0412");
$chk_nuzam_q_double_list        = array("NU0425","NU0426","NU0429","NU0430","NU0428","NU0427");
$chk_nuzam_ss_q_list            = array("NU0416","NU0417","NU0420","NU0419","NU0424","NU0422","NU0423","NU0418","NU0421");

# 롯데 상품코드
$lotte_address_order_list   = [];
$chk_lotte_prd_list         = array(
    "12828488"  => array("001" => "NU0352"),
    "12828489"  => array("001" => "NU0354"),
    "12828491"  => array("001" => "NU0367", "002" => "NU0366", "003" => "NU0368"),
    "12819299"  => array("001" => "NU0367", "002" => "NU0366", "003" => "NU0368"),
    "12828492"  => array("001" => "NU0370", "002" => "NU0369", "003" => "NU0371"),
    "12819298"  => array("001" => "NU0370", "002" => "NU0369", "003" => "NU0371"),
    "12828503"  => array("001" => "NU0112", "002" => "NU0252", "003" => "NU0326"),
    "12828504"  => array("001" => "NU0113", "002" => "NU0253", "003" => "NU0327"),
    "12828501"  => array("001" => "NU0110", "002" => "NU0250", "003" => "NU0324"),
    "12828502"  => array("001" => "NU0111", "002" => "NU0251", "003" => "NU0325"),
    "12828509"  => array("001" => "NU0347", "002" => "NU0346"),
    "12828493"  => array("001" => "NU0118", "002" => "NU0119"),
    "12828495"  => array("001" => "NU0120", "002" => "NU0121"),
    "12818729"  => array("001" => "NU0352"),
    "12825482"  => array("001" => "NU0352"),
    "12818728"  => array("001" => "NU0354"),
    "12825483"  => array("001" => "NU0354"),
    "12828505"  => array("001" => "NU0288", "002" => "NU0289"),
    "12828506"  => array("001" => "NU0290", "002" => "NU0291"),
    "12823878"  => array("001" => "NU0302", "002" => "NU0303"),
    "12828507"  => array("001" => "NU0302", "002" => "NU0303"),
    "12823879"  => array("001" => "NU0304", "002" => "NU0305"),
    "12828508"  => array("001" => "NU0304", "002" => "NU0305"),
    "12829866"  => array("001" => "NU0414", "002" => "NU0410", "003" => "NU0414"),
    "12829856"  => array("001" => "NU0429", "002" => "NU0425", "003" => "NU0429"),
    "12829879"  => array("001" => "NU0367", "002" => "NU0366", "003" => "NU0367"),
    "12829873"  => array("001" => "NU0370", "002" => "NU0369", "003" => "NU0370"),
    "12830959"  => array("001" => "NU0291"),
    "12832326"  => array("001" => "NU0288", "002" => "NU0289"),
    "12832324"  => array("001" => "NU0367", "002" => "NU0366", "003" => "NU0368"),
    "12832320"  => array("001" => "NU0354"),
    "12832321"  => array("001" => "NU0352"),
    "12832323"  => array("001" => "NU0370", "002" => "NU0369", "003" => "NU0371"),
    "12832325"  => array("001" => "NU0290", "002" => "NU0291"),
    "12830379"  => array("001" => "NU0354"),
    "12830380"  => array("001" => "NU0352"),
    "12832317"  => array("001" => "NU0352"),
    "12832318"  => array("001" => "NU0354"),
    "12831188"  => array("001" => "DP0799"),
    "12831191"  => array("001" => "DP0800"),
    "12829892"  => array("001" => "DP0733"),
    "12829894"  => array("001" => "DP0733"),
    "12831978"  => array("001" => "DP0183"),
    "12831980"  => array("001" => "DP0301"),
    "12831982"  => array("001" => "DP0220"),
    "12831983"  => array("001" => "DP0215"),
    "12831986"  => array("001" => "DP0299", "002" => "DP0218"),
    "12831987"  => array("001" => "DP0232", "002" => "DP0231"),
    "12831988"  => array("001" => "DPH001", "002" => "DPH002"),
    "12832648"  => array("001" => "DP0733"),
    "12832653"  => array("001" => "DP0734"),
    "12832651"  => array("001" => "DP540"),
    "12833448"  => array("001" => "DP0554"),
    "12833452"  => array("001" => "DP0623"),
    "12832503"  => array("001" => "NU0420", "002" => "NU0416"),
    "12833681"  => array("001" => "NU0111"),
    "12811282"  => array("001" => "NU0367", "002" => "NU0366", "003" => "NU0368"),
    "12811283"  => array("001" => "NU0370", "002" => "NU0369", "003" => "NU0371"),
    "12811337"  => array("001" => "NU0112", "002" => "NU0252", "003" => "NU0326"),
    "12811338"  => array("001" => "NU0113", "002" => "NU0253", "003" => "NU0327"),
    "12811334"  => array("001" => "NU0110", "002" => "NU0250", "003" => "NU0324"),
    "12811336"  => array("001" => "NU0111", "002" => "NU0251", "003" => "NU0325"),
    "12811278"  => array("001" => "NU0352"),
    "12811281"  => array("001" => "NU0354"),
    "12823873"  => array("001" => "NU0288", "002" => "NU0289"),
    "12823874"  => array("001" => "NU0290", "002" => "NU0291"),
    "12833886"  => array("001" => "DP0799"),
    "12833957"  => array("001" => "DP0301"),
    "12833985"  => array("001" => "DP0220"),
    "12833990"  => array("001" => "DP0554"),
    "12833960"  => array("001" => "DP0299", "002" => "DP0218"),
    "12833987"  => array("001" => "DP540"),
    "12833951"  => array("001" => "DP0183"),
    "12833983"  => array("001" => "DP0733"),
    "12833989"  => array("001" => "DP0623"),
    "12833982"  => array("001" => "DP0232", "002" => "DP0231"),
    "12833988"  => array("001" => "DP0734"),
    "12833934"  => array("001" => "DP0800"),
    "12833958"  => array("001" => "DP0215"),
    "12833986"  => array("001" => "DPH001", "002" => "DPH002"),
);

# 엑셀 파일 읽기
$excelReader =   PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);

$excel          = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet       = $excel->getActiveSheet();
$totalRow           = $objWorksheet->getHighestRow();
$excel_data_list    = [];
$event_data_list    = [];
$delivery_data      = [];
$total_price_data   = [];
$coupon_data        = [];
$discount_data      = [];
$track_data         = [];
$prev_order_number  = "";
$prev_shop_ord_no   = "";
$main_dp_c_name     = "";

$prev_week_day      = date("Y-m-d", strtotime("-1 weeks"))." 00:00:00";
$active_event_sql   = "SELECT * FROM advertising_event WHERE event_no IN('6','9','17','32','36') AND event_s_date >= '{$prev_week_day}' AND is_active='1' ORDER BY event_s_date ASC";
$active_event_query = mysqli_query($my_db, $active_event_sql);
$active_event_list  = [];
while($active_event = mysqli_fetch_assoc($active_event_query))
{
    $dp_list = explode(",", $active_event['dp_list']);
    foreach($dp_list as $dp_val)
    {
        $active_event_list[$dp_val][] = array("event_no" => $active_event["event_no"], "s_date" => $active_event['event_s_date'], "e_date" => $active_event['event_e_date']);
    }
}

# 커머스 URL 제외
$chk_url_exist_list = array("1572" => "1572","1573" => "1573", "1574" => "1574", "1558" => "1558");
$chk_url_yet_list   = [];
$chk_sep_list       = [];
$dup_ord_list       = [];
$chk_dp_ord_list    = [];


# 경추심 베개 예판
$chk_pillow_sql   = "SELECT prd_code FROM product_cms p WHERE p.prd_no IN(SELECT DISTINCT sub.prd_no FROM product_cms_relation sub WHERE sub.option_no IN(459) AND sub.display='1') AND p.display='1'";
$chk_pillow_query = mysqli_query($my_db, $chk_pillow_sql);
$chk_pillow_list  = [];
while($chk_pillow = mysqli_fetch_assoc($chk_pillow_query)){
    $chk_pillow_list[$chk_pillow['prd_code']] = $chk_pillow['prd_code'];
}

# DB 데이터 시작
for ($i = 2; $i <= $totalRow; $i++)
{
    if(($choice_c_no == '2' || $choice_c_no == '5' || $choice_c_no == '6' || $choice_c_no == '10' || $choice_c_no == '18' || $choice_c_no == '24') && $i == 2){
        continue;
    }

    # 기본 변수
    $order_type = "택배";
    $log_c_no   = 2809;

    # 변수 초기화
    $order_number = $shop_ord_no = $section_ord_no = $origin_ord_no = $account_code = $payment_type = $payment_code = $page_idx = "";
    $sender_name = $sender_hp = $recipient = $recipient_addr = $recipient_hp = $recipient_hp2 = $zip_code = $delivery_memo = $delivery_no = "";

    # 금액 초기화 & 현재날짜
    $dp_price = $dp_price_vat = $unit_price = $unit_delivery_price = $delivery_price = $coupon_price = $final_price = $discount_price = 0;
    $order_date = $payment_date = $order_date_val = $pay_date_val = "";
    $quantity = $subs_application_times = $subs_progression_times = 0;

    # 상품데이터 및 업체 처리용
    $sku = $prd_name = $prd_option = $c_no = $c_name = $s_no = $team = $prd_no = $dp_c_name_val = "";
    $dp_c_no = $dp_c_name = $manager_memo = $task_req = $task_req_s_no = $task_req_team = $notice = "";

    # 누잠 보관백 이벤트 변수
    $add_nuzam_prd      = false;
    $add_nuzam_prd_list = [];

    # 아이레놀 이벤트 여부
    $add_eye_event_prd  = false;
    $eye_event_text     = "";
    $eye_event_prd      = "";

    # 롯데 더블업 이벤트 여부
    $add_lotte_single_prd   = false;
    $add_lotte_bedding_prd  = false;
    $add_lotte_double_prd   = false;
    $add_lotte_set_prd      = false;

    # 상품정보 처리
    $product_data   = [];
    $use_out_sku    = false;

    # 누잠 옵션+옵션 상품 관련 처리
    $is_nuzam_option     = false;
    $nuzam_option_prd    = "";

    //엑셀 값
    if($choice_c_no == '1')   //아임웹_베라베프
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //쇼핑몰 주문번호
        $order_type         = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //주문타입
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //주문일자
        $payment_date       = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //결제금액
        $product_sale       = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //상품할인
        $account_sale       = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //회원할인
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AT{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AU{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue()));  //수령자 전화번호
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AY{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue()));  //우편번호
        $manager_memo       = (string)trim(addslashes($objWorksheet->getCell("BB{$i}")->getValue()));  //관리자메모
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AQ{$i}")->getValue()));  //배송메모
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("AS{$i}")->getValue()));  //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("AH{$i}")->getValue()));  //배송비
        $coupon_price       = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue()));  //쿠폰사용액
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("AK{$i}")->getValue()));  //최종결제금액
        $account_code       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   //회원코드
        $coupon_info        = (string)trim(addslashes($objWorksheet->getCell("AL{$i}")->getValue()));  //쿠폰정보
        $sender_name        = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //주문자
        $sender_hp          = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //주문자 전화번호
        $page_idx           = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //페이지 IDX
        $origin_ord_no      = $order_number;

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        if(!empty($coupon_info)){
            $task_req  .= "\r\n쿠폰정보 : {$coupon_info}";
        }

        if(!empty($product_sale) && $product_sale > 0){
            $task_req  .= "\r\n상품할인 : {$product_sale}";
            $discount_price = $product_sale;
        }

        if(!empty($account_sale) && $account_sale > 0){
            $task_req  .= "\r\n회원할인 : {$account_sale}";
            $discount_price = $account_sale;
        }

        if(strpos($sku, ",") !== false){
            $is_nuzam_option    = true;
            $sku_tmp            = explode(",", $sku);
            $sku                = isset($sku_tmp[0]) ? trim($sku_tmp[0]) : "";
            $nuzam_option_prd   = isset($sku_tmp[1]) ? trim($sku_tmp[1]) : "";
        }

        $dp_c_no        = "1372";
        $dp_c_name      = "아임웹_베라베프";
        $dp_price_val   = (int)$dp_price_val;
        $dp_price       = $dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = $dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '17')   //아임웹_닥터피엘
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //쇼핑몰 주문번호
        $order_type         = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //주문타입
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //주문일자
        $payment_date       = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //결제금액
        $product_sale       = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //상품할인
        $account_sale       = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //회원할인
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AT{$i}")->getValue())); //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AU{$i}")->getValue())); //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue())); //수령자 전화번호2
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AY{$i}")->getValue())); //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue())); //우편번호
        $manager_memo       = (string)trim(addslashes($objWorksheet->getCell("BB{$i}")->getValue())); //관리자메모
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AQ{$i}")->getValue())); //배송메모
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("AS{$i}")->getValue())); //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("AH{$i}")->getValue())); //배송비
        $coupon_price       = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue())); //쿠폰사용액
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("AK{$i}")->getValue())); //최종결제금액
        $account_code       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //회원코드
        $coupon_info        = (string)trim(addslashes($objWorksheet->getCell("AL{$i}")->getValue())); //쿠폰정보
        $sender_name        = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //주문자
        $sender_hp          = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //주문자 전화번호
        $page_idx           = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //페이지 IDX
        $origin_ord_no      = $order_number;

        if($sku == 'DP0131' || $sku == 'DP0556' || $sku == 'DP0805')
        {
            if(isset($sticker_check_list[$order_number]) && isset($sticker_check_list[$order_number][$sku])){
                continue;
            }else{
                $sticker_check_list[$order_number][$sku] = 1;
            }
        }

        if($sku == 'DP0768'){
            if(isset($yank_check_list[$order_number]) && isset($yank_check_list[$order_number][$sku])){
                continue;
            }else{
                $yank_check_list[$order_number][$sku] = 1;
            }
        }

        if($order_type != "택배"){
            $order_type = "택배";
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        if(!empty($coupon_info)){
            $task_req  .= "\r\n쿠폰정보 : {$coupon_info}";
        }

        if(!empty($product_sale) && $product_sale > 0){
            $task_req  .= "\r\n상품할인 : {$product_sale}";
            $discount_price = $product_sale;
        }

        if(!empty($account_sale) && $account_sale > 0){
            $task_req  .= "\r\n회원할인 : {$account_sale}";
            $discount_price = $account_sale;
        }

        $dp_c_no        = "5800";
        $dp_c_name      = "아임웹_닥터피엘";
        $dp_price_val   = (int)$dp_price_val;
        $dp_price       = $dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = $dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '8')   //아임웹_와이즈웰
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //쇼핑몰 주문번호
        $order_type         = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //주문타입
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //주문일자
        $payment_date       = $order_date;  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //수령자 전화번호
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue()));  //수령지
        $sender_name        = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //주문자
        $sender_hp          = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //주문자 전화번호
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //배송메모
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));  //배송비
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //최종결제금액
        $page_idx           = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //페이지 IDX
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5261";
        $dp_c_name          = "아임웹_와이즈웰";

        $task_req           = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        $dp_price           = (int)$dp_price_val / 1.1;
        $dp_price           = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat       = (int)$dp_price_val;
        $dp_price_vat       = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '20')   //아임웹_아이레놀
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //쇼핑몰 주문번호
        $order_type         = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //주문타입
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //주문일자
        $payment_date       = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //결제금액
        $product_sale       = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //상품할인
        $account_sale       = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //회원할인
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AT{$i}")->getValue())); //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AU{$i}")->getValue())); //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue())); //수령자 전화번호2
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AY{$i}")->getValue())); //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue())); //우편번호
        $manager_memo       = (string)trim(addslashes($objWorksheet->getCell("BB{$i}")->getValue())); //관리자메모
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AQ{$i}")->getValue())); //배송메모
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("AS{$i}")->getValue())); //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("AH{$i}")->getValue())); //배송비
        $coupon_price       = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue())); //쿠폰사용액
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("AK{$i}")->getValue())); //최종결제금액
        $account_code       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //회원코드
        $coupon_info        = (string)trim(addslashes($objWorksheet->getCell("AL{$i}")->getValue())); //쿠폰정보
        $sender_name        = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //주문자
        $sender_hp          = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //주문자 전화번호
        $page_idx           = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //페이지 IDX
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5958";
        $dp_c_name          = "아임웹_아이레놀";

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        if(!empty($coupon_info)){
            $task_req  .= "\r\n쿠폰정보 : {$coupon_info}";
        }

        if(!empty($product_sale) && $product_sale > 0){
            $task_req  .= "\r\n상품할인 : {$product_sale}";
            $discount_price = $product_sale;
        }

        if(!empty($account_sale) && $account_sale > 0){
            $task_req  .= "\r\n회원할인 : {$account_sale}";
            $discount_price = $account_sale;
        }

        $dp_price_val   = (int)$dp_price_val;
        $dp_price       = $dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = $dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '21')   //아임웹_채식주의
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //쇼핑몰 주문번호
        $order_type         = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //주문타입
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //주문일자
        $payment_date       = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //결제금액
        $product_sale       = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //상품할인
        $account_sale       = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //회원할인
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AT{$i}")->getValue())); //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AU{$i}")->getValue())); //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue())); //수령자 전화번호2
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AY{$i}")->getValue())); //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue())); //우편번호
        $manager_memo       = (string)trim(addslashes($objWorksheet->getCell("BB{$i}")->getValue())); //관리자메모
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AQ{$i}")->getValue())); //배송메모
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("AS{$i}")->getValue())); //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("AH{$i}")->getValue())); //배송비
        $coupon_price       = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue())); //쿠폰사용액
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("AK{$i}")->getValue())); //최종결제금액
        $account_code       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //회원코드
        $coupon_info        = (string)trim(addslashes($objWorksheet->getCell("AL{$i}")->getValue())); //쿠폰정보
        $sender_name        = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //주문자
        $sender_hp          = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //주문자 전화번호
        $page_idx           = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //페이지 IDX
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5965";
        $dp_c_name          = "아임웹_채식주의";

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        if(!empty($coupon_info)){
            $task_req  .= "\r\n쿠폰정보 : {$coupon_info}";
        }

        if(!empty($product_sale) && $product_sale > 0){
            $task_req  .= "\r\n상품할인 : {$product_sale}";
            $discount_price = $product_sale;
        }

        if(!empty($account_sale) && $account_sale > 0){
            $task_req  .= "\r\n회원할인 : {$account_sale}";
            $discount_price = $account_sale;
        }

        $dp_price_val   = (int)$dp_price_val;
        $dp_price       = $dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = $dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '22')   //아임웹_누잠
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //쇼핑몰 주문번호
        $order_type         = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //주문타입
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //주문일자
        $payment_date       = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //결제금액
        $product_sale       = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //상품할인
        $account_sale       = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //회원할인
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AT{$i}")->getValue())); //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AU{$i}")->getValue())); //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue())); //수령자 전화번호2
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AY{$i}")->getValue())); //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue())); //우편번호
        $manager_memo       = (string)trim(addslashes($objWorksheet->getCell("BB{$i}")->getValue())); //관리자메모
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AQ{$i}")->getValue())); //배송메모
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("AS{$i}")->getValue())); //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("AH{$i}")->getValue())); //배송비
        $coupon_price       = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue())); //쿠폰사용액
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("AK{$i}")->getValue())); //최종결제금액
        $account_code       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //회원코드
        $coupon_info        = (string)trim(addslashes($objWorksheet->getCell("AL{$i}")->getValue())); //쿠폰정보
        $sender_name        = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //주문자
        $sender_hp          = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //주문자 전화번호
        $page_idx           = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //페이지 IDX
        $origin_ord_no      = $order_number;
        $dp_c_no            = "6012";
        $dp_c_name          = "아임웹_누잠";

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        if(!empty($coupon_info)){
            $task_req  .= "\r\n쿠폰정보 : {$coupon_info}";
        }

        if(!empty($product_sale) && $product_sale > 0){
            $task_req  .= "\r\n상품할인 : {$product_sale}";
            $discount_price = $product_sale;
        }

        if(!empty($account_sale) && $account_sale > 0){
            $task_req  .= "\r\n회원할인 : {$account_sale}";
            $discount_price = $account_sale;
        }

        $dp_price_val   = (int)$dp_price_val;
        $dp_price       = $dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = $dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '91')   //아임웹_베라베프_v2
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $section_ord_no     = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //주문섹션번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));  //주문품목섹션번호
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //결제타입
        $payment_code       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //결제코드
        $order_type         = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //주문타입
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //주문일자
        $payment_date       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue())) ? (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue())) : (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //구매수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //품목실결제가
        $coupon_price       = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //쿠폰사용액(총 쿠폰 할인금액)
//        $product_sale       = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //상품할인
        $account_sale       = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //회원할인(총 등급할인금액)
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue())); //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue())); //수령자 전화번호
        $recipient_base     = (string)trim(addslashes($objWorksheet->getCell("AD{$i}")->getValue())); //주소
        $recipient_detail   = (string)trim(addslashes($objWorksheet->getCell("AE{$i}")->getValue())); //상세주소
        $recipient_addr     = trim($recipient_base." ".$recipient_detail);                      //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AC{$i}")->getValue())); //우편번호
        $coupon_info        = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //쿠폰정보
        $sender_name        = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //주문자
        $sender_hp          = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //주문자 전화번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AF{$i}")->getValue())); //배송메모
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //배송비
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //최종결제금액
        $page_idx           = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //페이지 IDX
//        $manager_memo      = (string)trim(addslashes($objWorksheet->getCell("AF{$i}")->getValue())); //관리자메모
        $origin_ord_no      = $order_number;
        $dp_c_no            = "1372";
        $dp_c_name          = "아임웹_베라베프";

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        if(!empty($coupon_info)){
            $task_req  .= "\r\n쿠폰정보 : {$coupon_info}";
        }

        if(!empty($product_sale) && $product_sale > 0){
            $task_req  .= "\r\n상품할인 : {$product_sale}";
            $discount_price = $product_sale;
        }

        if(!empty($account_sale) && $account_sale > 0){
            $task_req  .= "\r\n회원할인 : {$account_sale}";
            $discount_price = $account_sale;
        }

        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '92')   //아임웹_닥터피엘_v2
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $section_ord_no     = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //주문섹션번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));  //주문품목섹션번호
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //결제타입
        $payment_code       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //결제코드
        $order_type         = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //주문타입
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //주문일자
        $payment_date       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue())) ? (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue())) : (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //구매수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //품목실결제가
        $coupon_price       = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //쿠폰사용액(총 쿠폰 할인금액)
//        $product_sale       = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //상품할인
        $account_sale       = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //회원할인(총 등급할인금액)
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue())); //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue())); //수령자 전화번호
        $recipient_base     = (string)trim(addslashes($objWorksheet->getCell("AD{$i}")->getValue())); //주소
        $recipient_detail   = (string)trim(addslashes($objWorksheet->getCell("AE{$i}")->getValue())); //상세주소
        $recipient_addr     = trim($recipient_base." ".$recipient_detail);                      //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AC{$i}")->getValue())); //우편번호
        $coupon_info        = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //쿠폰정보
        $sender_name        = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //주문자
        $sender_hp          = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //주문자 전화번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AF{$i}")->getValue())); //배송메모
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //배송비
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //최종결제금액
        $page_idx           = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //페이지 IDX
//        $manager_memo      = (string)trim(addslashes($objWorksheet->getCell("AF{$i}")->getValue())); //관리자메모
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5800";
        $dp_c_name          = "아임웹_닥터피엘";

        if($sku == 'DP0131' || $sku == 'DP0556')
        {
            if(isset($sticker_check_list[$order_number]) && isset($sticker_check_list[$order_number][$sku])){
                continue;
            }else{
                $sticker_check_list[$order_number][$sku] = 1;
            }
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        if(!empty($coupon_info)){
            $task_req  .= "\r\n쿠폰정보 : {$coupon_info}";
        }

        if(!empty($product_sale) && $product_sale > 0){
            $task_req  .= "\r\n상품할인 : {$product_sale}";
            $discount_price = $product_sale;
        }

        if(!empty($account_sale) && $account_sale > 0){
            $task_req  .= "\r\n회원할인 : {$account_sale}";
            $discount_price = $account_sale;
        }

        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '93')   //아임웹_와이즈웰_v2
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $section_ord_no     = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //주문섹션번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));  //주문품목섹션번호
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //결제타입
        $payment_code       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //결제코드
        $order_type         = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //주문타입
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //주문일자
        $payment_date       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue())) ? (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue())) : (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //구매수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //품목실결제가
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue())); //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue())); //수령자 전화번호
        $recipient_base     = (string)trim(addslashes($objWorksheet->getCell("AD{$i}")->getValue())); //주소
        $recipient_detail   = (string)trim(addslashes($objWorksheet->getCell("AE{$i}")->getValue())); //상세주소
        $recipient_addr     = trim($recipient_base." ".$recipient_detail);                      //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AC{$i}")->getValue())); //우편번호
        $sender_name        = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //주문자
        $sender_hp          = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //주문자 전화번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AF{$i}")->getValue())); //배송메모
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //배송비
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //최종결제금액
        $page_idx           = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //페이지 IDX
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5261";
        $dp_c_name          = "아임웹_와이즈웰";

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '2')   # 스마트스토어_닥터피엘
    {
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //쇼핑몰 주문번호
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("BA{$i}")->getValue()));  //주문일자
        $pay_date_val       = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));   //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue())) ? (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue())) : (string)trim(addslashes($objWorksheet->getCell("AK{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));   //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));   //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AN{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AO{$i}")->getValue()));  //수령자 HP
        $recipient_addr1    = (string)trim(addslashes($objWorksheet->getCell("AR{$i}")->getValue()));  //수령지
        $recipient_addr2    = (string)trim(addslashes($objWorksheet->getCell("AS{$i}")->getValue()));  //상세 수령지
        $recipient_addr     = !empty($recipient_addr2) ? ($recipient_addr1." ".$recipient_addr2) : $recipient_addr1;
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AQ{$i}")->getValue()));  //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AT{$i}")->getValue()));  //배송메모
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue()));  //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));   //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));   //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("AH{$i}")->getValue()));  //배송비
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5427";
        $dp_c_name          = "스마트스토어_닥터피엘";

        $subs_application_times_val = (string)trim(addslashes($objWorksheet->getCell("BE{$i}")->getValue()));  //구독신청회차
        $subs_progression_times_val = (string)trim(addslashes($objWorksheet->getCell("BF{$i}")->getValue()));  //구독진행회차
        $subs_application_times     = ($subs_application_times_val > 0) ? $subs_application_times_val : 0;
        $subs_progression_times     = ($subs_progression_times_val > 0) ? $subs_progression_times_val : 0;

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false)
        {
            $order_date_convert   = str_replace('/','-', $order_date_val);
            $payment_date_convert = str_replace('/','-', $pay_date_val);

            $order_date     = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
            $payment_date   = $payment_date_convert ? date('Y-m-d H:i:s', strtotime("{$payment_date_convert} -33 hours")) : "";
        }else{
            //엑셀 시간 형식 변경
            $order_time_cal = ($order_date_val-25569)*86400;
            $order_time     = date("Y-m-d H:i", $order_time_cal);
            $order_date     = date("Y-m-d H:i", strtotime("{$order_time} -9 hour"));

            $payment_time_cal = ($pay_date_val-25569)*86400;
            $payment_time     = date("Y-m-d H:i", $payment_time_cal);
            $payment_date     = date("Y-m-d H:i", strtotime("{$payment_time} -9 hour"));
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '5')   //스마트스토어_베라베프
    {
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //쇼핑몰 주문번호
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("BF{$i}")->getValue()));  //주문일자
        $pay_date_val       = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue())) ? (string)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue())) : (string)trim(addslashes($objWorksheet->getCell("AN{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue()));  //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AQ{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AR{$i}")->getValue()));  //수령자 HP
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AS{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AW{$i}")->getValue()));  //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue()));  //배송메모
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("AZ{$i}")->getValue()));  //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //옵션정보
        $origin_ord_no      = $order_number;
        $dp_c_no            = "3295";
        $dp_c_name          = "스마트스토어_베라베프";

        $subs_application_times_val = (string)trim(addslashes($objWorksheet->getCell("BG{$i}")->getValue()));  //구독신청회차
        $subs_progression_times_val = (string)trim(addslashes($objWorksheet->getCell("BH{$i}")->getValue()));  //구독진행회차
        $subs_application_times     = ($subs_application_times_val > 0) ? $subs_application_times_val : 0;
        $subs_progression_times     = ($subs_progression_times_val > 0) ? $subs_progression_times_val : 0;

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false)
        {
            $order_date_convert   = str_replace('/','-', $order_date_val);
            $payment_date_convert = str_replace('/','-', $pay_date_val);

            $order_date     = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
            $payment_date   = $payment_date_convert ? date('Y-m-d H:i:s', strtotime("{$payment_date_convert} -33 hours")) : "";
        }else{
            //엑셀 시간 형식 변경
            $order_time_cal = ($order_date_val-25569)*86400;
            $order_time     = date("Y-m-d H:i", $order_time_cal);
            $order_date     = date("Y-m-d H:i", strtotime("{$order_time} -9 hour"));

            $payment_time_cal = ($pay_date_val-25569)*86400;
            $payment_time     = date("Y-m-d H:i", $payment_time_cal);
            $payment_date     = date("Y-m-d H:i", strtotime("{$payment_time} -9 hour"));
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '6')   //스마트스토어_누잠
    {
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //쇼핑몰 주문번호
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("BJ{$i}")->getValue())); //주문일자
        $pay_date_val       = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue())) ? (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue())) : (string)trim(addslashes($objWorksheet->getCell("AR{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("AE{$i}")->getValue())); //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AU{$i}")->getValue())); //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue())); //수령자 HP
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AW{$i}")->getValue())); //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("BA{$i}")->getValue())); //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("BB{$i}")->getValue())); //배송메모
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("BD{$i}")->getValue())); //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //옵션정보
        $origin_ord_no      = $order_number;
        $dp_c_no            = "4629";
        $dp_c_name          = "스마트스토어_누잠";

        $subs_application_times_val = (string)trim(addslashes($objWorksheet->getCell("BL{$i}")->getValue()));  //구독신청회차
        $subs_progression_times_val = (string)trim(addslashes($objWorksheet->getCell("BM{$i}")->getValue()));  //구독진행회차
        $subs_application_times     = ($subs_application_times_val > 0) ? $subs_application_times_val : 0;
        $subs_progression_times     = ($subs_progression_times_val > 0) ? $subs_progression_times_val : 0;

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false)
        {
            $order_date_convert   = str_replace('/','-', $order_date_val);
            $payment_date_convert = str_replace('/','-', $pay_date_val);

            $order_date     = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
            $payment_date   = $payment_date_convert ? date('Y-m-d H:i:s', strtotime("{$payment_date_convert} -33 hours")) : "";
        }else{
            //엑셀 시간 형식 변경
            $order_time_cal = ($order_date_val-25569)*86400;
            $order_time     = date("Y-m-d H:i", $order_time_cal);
            $order_date     = date("Y-m-d H:i", strtotime("{$order_time} -9 hour"));

            $payment_time_cal = ($pay_date_val-25569)*86400;
            $payment_time     = date("Y-m-d H:i", $payment_time_cal);
            $payment_date     = date("Y-m-d H:i", strtotime("{$payment_time} -9 hour"));
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '10')   //스마트스토어_아이레놀
    {
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //쇼핑몰 주문번호
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("BJ{$i}")->getValue()));  //주문일자
        $pay_date_val       = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue())) ? (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue())) : (string)trim(addslashes($objWorksheet->getCell("AR{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("AE{$i}")->getValue()));  //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AU{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue()));  //수령자 HP
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AW{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("BA{$i}")->getValue()));  //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("BB{$i}")->getValue()));  //배송메모
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("BD{$i}")->getValue()));  //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //옵션정보
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5588";
        $dp_c_name          = "스마트스토어_아이레놀";

        $subs_application_times_val = (string)trim(addslashes($objWorksheet->getCell("BL{$i}")->getValue()));  //구독신청회차
        $subs_progression_times_val = (string)trim(addslashes($objWorksheet->getCell("BM{$i}")->getValue()));  //구독진행회차
        $subs_application_times     = ($subs_application_times_val > 0) ? $subs_application_times_val : 0;
        $subs_progression_times     = ($subs_progression_times_val > 0) ? $subs_progression_times_val : 0;

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false)
        {
            $order_date_convert   = str_replace('/','-', $order_date_val);
            $payment_date_convert = str_replace('/','-', $pay_date_val);

            $order_date     = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
            $payment_date   = $payment_date_convert ? date('Y-m-d H:i:s', strtotime("{$payment_date_convert} -33 hours")) : "";
        }else{
            //엑셀 시간 형식 변경
            $order_time_cal = ($order_date_val-25569)*86400;
            $order_time     = date("Y-m-d H:i", $order_time_cal);
            $order_date     = date("Y-m-d H:i", strtotime("{$order_time} -9 hour"));

            $payment_time_cal = ($pay_date_val-25569)*86400;
            $payment_time     = date("Y-m-d H:i", $payment_time_cal);
            $payment_date     = date("Y-m-d H:i", strtotime("{$payment_time} -9 hour"));
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '24')   //스마트스토어_채식주의
    {
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //쇼핑몰 주문번호
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("BJ{$i}")->getValue()));  //주문일자
        $pay_date_val       = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue())) ? (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue())) : (string)trim(addslashes($objWorksheet->getCell("AR{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("AE{$i}")->getValue()));  //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AU{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue()));  //수령자 HP
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AW{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("BA{$i}")->getValue()));  //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("BB{$i}")->getValue()));  //배송메모
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("BD{$i}")->getValue()));  //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //옵션정보
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5770";
        $dp_c_name          = "스마트스토어_채식주의";

        $subs_application_times_val = (string)trim(addslashes($objWorksheet->getCell("BL{$i}")->getValue()));  //구독신청회차
        $subs_progression_times_val = (string)trim(addslashes($objWorksheet->getCell("BM{$i}")->getValue()));  //구독진행회차
        $subs_application_times     = ($subs_application_times_val > 0) ? $subs_application_times_val : 0;
        $subs_progression_times     = ($subs_progression_times_val > 0) ? $subs_progression_times_val : 0;

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false)
        {
            $order_date_convert   = str_replace('/','-', $order_date_val);
            $payment_date_convert = str_replace('/','-', $pay_date_val);

            $order_date     = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
            $payment_date   = $payment_date_convert ? date('Y-m-d H:i:s', strtotime("{$payment_date_convert} -33 hours")) : "";
        }else{
            //엑셀 시간 형식 변경
            $order_time_cal = ($order_date_val-25569)*86400;
            $order_time     = date("Y-m-d H:i", $order_time_cal);
            $order_date     = date("Y-m-d H:i", strtotime("{$order_time} -9 hour"));

            $payment_time_cal = ($pay_date_val-25569)*86400;
            $payment_time     = date("Y-m-d H:i", $payment_time_cal);
            $payment_date     = date("Y-m-d H:i", strtotime("{$payment_time} -9 hour"));
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '3')   //사방넷
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //주문일자
        $order_date         = date("Y-m-d H:i:s", strtotime($order_date_val));
        $payment_date       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));   //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   //수령자 HP
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));   //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));   //배송메모
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));   //쇼핑몰 주문번호
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   //상품명
        $prd_name_sub       = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   //옵션정보
        $dp_c_name_val      = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));   //구매처
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));   //배송비
        $origin_ord_no      = $order_number;

        if(empty($prd_name)){
            $prd_name = $prd_name_sub;
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";

        if($dp_c_name_val == "11번가(구)"){
            $dp_c_name_val = "11번가";
        }elseif($dp_c_name_val == "ESM지마켓"){
            $dp_c_name_val = "지마켓";
        }elseif($dp_c_name_val == "ESM옥션") {
            $dp_c_name_val = "옥션";
        }elseif($dp_c_name_val == "카카오스타일 (지그재그, 포스티)"){
            $dp_c_name_val = "지그재그";
        }elseif($dp_c_name_val == "현대홈쇼핑(3)"){
            $dp_c_name_val = "현대홈쇼핑(신)";
        }elseif($dp_c_name_val == "shop by" || $dp_c_name_val == "베이비빌리"){
            $dp_c_name_val = "shop by_베이비빌리";
        }elseif($dp_c_name_val == "토스 공동구매"){
            $dp_c_name_val = "토스 쇼핑";
        }elseif($dp_c_name_val == "후추"){
            $dp_c_name_val = "shop by_후추";
        }

        if(array_search($dp_c_name_val, $dp_company_commerce_list)){
            $dp_c_no    = array_search($dp_c_name_val, $dp_company_commerce_list);
            $dp_c_name  = $dp_company_commerce_list[$dp_c_no];
        }

        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
        
        // 동일한 주문 확인
        $sbn_check_result = false;
        if(!empty($sbn_check_list))
        {
            foreach ($sbn_check_list as $sbn_check) {
                if ($sbn_check['recipient'] == $recipient && $sbn_check['addr'] == $recipient_addr && $sbn_check['zipcode'] == $zip_code && $sbn_check['dp_c_name'] == $dp_c_name_val) {
                    $order_number = $sbn_check['ord_no'];

                    $sbn_check_result = true;
                }
            }
        }

        if(!$sbn_check_result)
        {
            $sbn_check_list[] = array('recipient' => $recipient, 'addr' => $recipient_addr, 'zipcode' => $zip_code, 'dp_c_name' => $dp_c_name_val, 'ord_no' => $order_number);
        }
    }
    elseif($choice_c_no == '4')   //위드플레이스 양식
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //주문일자
        $payment_date_val   = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   //결제일자
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   //상품명
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   //수량
        $dp_price_chk       = (strpos($objWorksheet->getCell("F{$i}")->getValue(),"=") === false) ? $objWorksheet->getCell("F{$i}")->getValue() : $objWorksheet->getCell("F{$i}")->getCalculatedValue();
        $dp_price_val       = (string)trim(addslashes(str_replace(",","",$dp_price_chk)));   //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   //수령자 HP
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //배송메모
        $notice             = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //특이사항
        $dp_c_name_val      = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //업체명
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //쇼핑몰 주문번호
        $origin_ord_no      = $order_number;

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false){
            $order_date_convert = str_replace('/','-', $order_date_val);
            $order_date         = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
        }elseif(strpos($order_date_val, ".") !== false){
            $order_date_convert = str_replace('.','-', $order_date_val);
            $order_date         = $order_date_convert ? date('Y-m-d', strtotime("{$order_date_convert}"))." 00:00:00" : "";
        }elseif(strpos($order_date_val, "-") !== false){
            $order_date = date("Y-m-d H:i:s", strtotime($order_date_val));
        }else{
            $order_time_cal = ($order_date_val-25569)*86400;
            $order_time     = date("Y-m-d H:i", $order_time_cal);
            $order_date     = date("Y-m-d H:i:s", strtotime("{$order_time} -9 hour"));
        }

        if(strpos($payment_date_val, "/") !== false){
            $payment_date_convert = str_replace('/','-', $payment_date_val);
            $payment_date         = $payment_date_convert ? date('Y-m-d H:i:s', strtotime("{$payment_date_convert} -33 hours")) : "";
        }elseif(strpos($payment_date_val, ".") !== false){
            $payment_date_convert = str_replace('.','-', $payment_date_val);
            $payment_date         = $payment_date_convert ? date('Y-m-d', strtotime("{$payment_date_convert}"))." 00:00:00" : "";
        }elseif(strpos($payment_date_val, "-") !== false){
            $payment_date = date("Y-m-d H:i:s", strtotime($payment_date_val));
        }else{
            $payment_time_cal = ($payment_date_val-25569)*86400;
            $payment_time     = date("Y-m-d H:i", $payment_time_cal);
            $payment_date     = date("Y-m-d H:i:s", strtotime("{$payment_time} -9 hour"));
        }

        // 구매처 null 체크
        if(!$dp_c_name_val){
            echo "ROW : {$i}<br/>";
            echo "구매처가 없습니다<br>
            주문날짜    : {$order_date}<br>
            주문번호    : {$order_number}<br>
            수령자명    : {$recipient}<br>
            구매처      : {$dp_c_name_val}<br>";
            exit;
        }

        if(array_search($dp_c_name_val, $dp_company_commerce_list)){
            $dp_c_no    = array_search($dp_c_name_val, $dp_company_commerce_list);
            $dp_c_name  = $dp_company_commerce_list[$dp_c_no];
        }

        // SKU 검색
        $product_sku_data   = $product_model->getWhereItem("title = '{$prd_name}'");
        $sku                = (isset($product_sku_data['prd_code']) && !empty($product_sku_data['prd_code'])) ? $product_sku_data['prd_code'] : "";  //상품코드
        $prd_no             = (isset($product_sku_data['prd_no']) && !empty($product_sku_data['prd_no'])) ? $product_sku_data['prd_no'] : "";        //상품번호
        $product_data       = $product_model->getItem($prd_no);

        $task_req       = "상품명 :: {$prd_name}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '11')   #올리브영(누잠_에이원비앤에이치)
    {
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //주문일자
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //배송번호
        $payment_date       = $order_date;  //결제일자
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //옵션정보
        $sku                = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //수령자 HP
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //우편번호
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //수령지
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue()));  //배송메모
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5615";
        $dp_c_name          = "올리브영(누잠_에이원비앤에이치)";

        if(empty($order_number)){
            $order_number       = $prev_order_number;
            $shop_ord_no        = $prev_shop_ord_no;
        }else{
            $prev_order_number  = $order_number;
            $prev_shop_ord_no   = $shop_ord_no;
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}\r\n외부코드 :: {$sku}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '13') # 알리익스프레스
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //주문시간
        $payment_date_val   = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //결제시간
        $sku_val            = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  //상품코드
        $prd_name_val       = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //상품명
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //수령자
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //우편번호
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue()));  //수령자 전화번호
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //최종결제금액
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //결제금액
        $delivery_no        = (string)trim(addslashes($objWorksheet->getCell("AE{$i}")->getValue())); //운송장
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5720";
        $dp_c_name          = "알리익스프레스";

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false){
            $order_date_convert = str_replace('/','-', $order_date_val);
            $order_date         = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
        }elseif(strpos($order_date_val, ".") !== false){
            $order_date_convert = str_replace('.','-', $order_date_val);
            $order_date         = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert}")) : "";
        }else{
            $order_date = $order_date_val;
        }

        if(strpos($payment_date_val, "/") !== false){
            $payment_date_convert = str_replace('/','-', $payment_date_val);
            $payment_date         = $payment_date_convert ? date('Y-m-d H:i:s', strtotime("{$payment_date_convert} -33 hours")) : "";
        }elseif(strpos($payment_date_val, ".") !== false){
            $payment_date_convert = str_replace('.','-', $payment_date_val);
            $payment_date         = $payment_date_convert ? date('Y-m-d H:i:s', strtotime("{$payment_date_convert}")) : "";
        }else{
            $payment_date = $payment_date_val;
        }

        if(empty($sku_val)){
            $sku            = "DP0300";
        } else {
            $sku_val_list   = explode("*", $sku_val);
            $sku            = trim($sku_val_list[0]);
        }

        $prd_name_list  = explode("(수량:", $prd_name_val);
        $prd_name       = trim($prd_name_list[0]);
        $quantity       = (int)str_replace("pieces)", "", str_replace("piece)", "", $prd_name_list[1]));

        if(empty($quantity)){
            $quantity = isset($sku_val_list[1]) ? (int)$sku_val_list[1] : 1;
        }

        if(!empty($delivery_no))
        {
            $track_data[] = array(
                "order_number"  => $order_number,
                "delivery_type" => "CJ대한통운",
                "delivery_no"   => $delivery_no,
                "delivery_fee"  => $delivery_price,
                "dp_c_no"       => $dp_c_no,
            );
        }

        $dp_price_val   = str_replace("₩ ", "", str_replace(",", "", $dp_price_val));
        $final_price    = str_replace("₩ ", "", str_replace(",", "", $final_price));
        $task_req       = "상품명 :: {$prd_name}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '14')    //아이레놀(공동구매)::밀리언즈, OURPICK
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    //주문번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("AD{$i}")->getValue()));   //고유번호
        $order_date         = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    //주문일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));    //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));    //수량
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));    //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));    //옵션정보
        $dp_price_val       = (int)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()))*0.6;       //상품가격
        $final_price        = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));    //최종결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));    //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));    //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));    //수령자 전화번호2
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue()));    //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));    //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));    //배송메모
        $eye_event_text     = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));    //이벤트여부
        $eye_event_prd      = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    //이벤트상품
        $origin_ord_no      = $order_number;
        $payment_date       = $order_date;
        $dp_c_no            = "5847";
        $dp_c_name          = "OURPICK";

        if(!empty($eye_event_text)){
            $add_eye_event_prd = true;
        }

        if(empty($recipient_hp) && !empty($recipient_hp2)){
            $recipient_hp = $recipient_hp2;
        }

        if(empty($recipient) || empty($recipient_hp) || empty($recipient_addr) || empty($zip_code)){
            echo "ROW : {$i}<br/>";
            echo "택배리스트 반영에 실패하였습니다.<br>주소 기본정보가 없습니다.<br>
            수령인     : {$recipient}<br>
            수령인번호  : {$recipient_hp}<br>
            주소       : {$recipient_addr}<br>
            우편번호    : {$zip_code}<br>";
            exit;
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림

        if(empty($order_number)){
            break;
        }
    }
    elseif($choice_c_no == '16')    //온더룩
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //주문번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //쇼핑몰 주문번호
        $sku                = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //수량
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //옵션정보
        $dp_price_val       = (int)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //상품가격
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //수령자 전화번호
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //배송메모
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //주문일자
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5792";
        $dp_c_name          = "온더룩 판매자센터";
        
        # 엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false) {
            $order_date_convert   = str_replace('/','-', $order_date_val);
            $order_date     = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
        }else{
            //엑셀 시간 형식 변경
            $order_time_cal = ($order_date_val-25569)*86400;
            $order_time     = date("Y-m-d H:i", $order_time_cal);
            $order_date     = date("Y-m-d H:i", strtotime("{$order_time} -9 hour"));
        }

        $payment_date   = $order_date;
        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림

        if(empty($order_number)){
            break;
        }
    }
    elseif($choice_c_no == '18')   // 기본양식
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문시간
        $payment_date_val   = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //결제시간
        $sku                = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //상품코드
        $out_sku            = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //외주상품코드
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));  //상품명
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //결제금액
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //배송비
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //수령자
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //우편번호
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //수령자 전화번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //배송메모
        $notice             = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //특이사항
        $dp_c_name_val      = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //업체명
        $origin_ord_no      = $order_number;

        if(empty($order_number)){
            break;
        }

        if(empty($sku)){
            $sku = $out_sku;
            if(!empty($out_sku)){
                $use_out_sku = true;
            }
        }

        if($dp_c_name_val == "더블앤코퍼레이션"){
            $dp_c_name_val = "주식회사 더블앤코퍼레이션";
        }elseif($dp_c_name_val == "선린"){
            $dp_c_name_val = "주식회사 선린";
        }

        if(array_search($dp_c_name_val, $dp_company_commerce_list)){
            $dp_c_no    = array_search($dp_c_name_val, $dp_company_commerce_list);
            $dp_c_name  = $dp_company_commerce_list[$dp_c_no];
        }

        $task_req       = "상품명 :: {$prd_name}\r\n주문번호 :: {$order_number}";

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false){
            $order_date_convert = str_replace('/','-', $order_date_val);
            $order_date         = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
        }elseif(strpos($order_date_val, ".") !== false){
            $order_date_convert = str_replace('.','-', $order_date_val);
            $order_date         = $order_date_convert ? date('Y-m-d', strtotime("{$order_date_convert}"))." 00:00:00" : "";
        }elseif(strpos($order_date_val, "-") !== false){
            $order_date = date("Y-m-d H:i:s", strtotime($order_date_val));
        }else{
            $order_time_cal = ($order_date_val-25569)*86400;
            $order_date     = date("Y-m-d", $order_time_cal);
        }

        if(!empty($payment_date_val)){
            if(strpos($payment_date_val, "/") !== false){
                $payment_date_convert = str_replace('/','-', $payment_date_val);
                $payment_date         = $payment_date_convert ? date('Y-m-d H:i:s', strtotime("{$payment_date_convert} -33 hours")) : "";
            }elseif(strpos($payment_date_val, ".") !== false){
                $payment_date_convert = str_replace('.','-', $payment_date_val);
                $payment_date         = $payment_date_convert ? date('Y-m-d', strtotime("{$payment_date_convert}"))." 00:00:00" : "";
            }elseif(strpos($payment_date_val, "-") !== false){
                $payment_date = date("Y-m-d H:i:s", strtotime($payment_date_val));
            }else{
                $payment_time_cal = ($payment_date_val-25569)*86400;
                $payment_date     = date("Y-m-d", $payment_time_cal);
            }
        }else{
            $payment_date = $order_date;
        }
        
        $dp_price_val   = (int)$dp_price_val;
        $dp_price       = $dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = $dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '19')   // 롯데홈쇼핑
    {
        $order_number       = "";  //주문번호
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //상품주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //주문시간
        $chk_sku            = (string)trim(addslashes($objWorksheet->getCell("AJ{$i}")->getValue()));  //상품코드
        $chk_code           = (string)trim(addslashes($objWorksheet->getCell("AK{$i}")->getValue()));  //단품코드
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("AL{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("AN{$i}")->getValue()));  //옵션정보
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("AO{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("AW{$i}")->getValue()));  //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //수령자
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AS{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AR{$i}")->getValue()));  //우편번호
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue()));  //수령자 전화번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue()));  //배송메오
        $origin_ord_no      = $shop_ord_no;
        $dp_c_no            = "4316";
        $dp_c_name          = "롯데홈쇼핑";

        if(empty($shop_ord_no)){
            break;
        }

        $order_number_tmp_list  = explode("-", $shop_ord_no);
        $order_number           = isset($order_number_tmp_list[0]) ? $order_number_tmp_list[0] : "";

        if(strpos($prd_name, "분리배송") !== false){
            $order_number_tmp_list[2]           = "001";
            $sep_order_number                   = implode("-", $order_number_tmp_list);
            $chk_sep_list[$sep_order_number]    = $shop_ord_no;
            continue;
        }

        $sku = isset($chk_lotte_prd_list[$chk_sku][$chk_code]) ? $chk_lotte_prd_list[$chk_sku][$chk_code] : "";

        if(empty($sku)){
            echo "등록안된 SKU 입니다. 상품코드 : {$chk_sku}";
            exit;
        }

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "-") !== false){
            $order_date = date("Y-m-d", strtotime($order_date_val));
        }else{
            $order_time_cal = ($order_date_val-25569)*86400;
            $order_date     = date("Y-m-d", $order_time_cal);
        }
        $payment_date   = $order_date;

        if($chk_sku == "12832318" || $chk_sku == "12832317" || $chk_sku == "12830379" || $chk_sku == "12830380"){
            $add_lotte_double_prd = true;
        } elseif($chk_sku == "12832324" || $chk_sku == "12832323" || $chk_sku == "12829879" || $chk_sku == "12829873"){
            $add_lotte_single_prd = true;
        } elseif($chk_sku == "12829866" || $chk_sku == "12829856" || $chk_sku == "12832503"){
            $add_lotte_set_prd = true;
        } elseif($chk_sku == "12832326" || $chk_sku == "12832325" || $chk_sku == "12832320" || $chk_sku == "12832321"){
            $add_lotte_bedding_prd = true;
        }

        if(strpos($prd_name, "경품") === false && !isset($lotte_address_order_list[$recipient_addr])) {
            $lotte_address_order_list[$recipient_addr] = $order_number;
        }

        $task_req       = "상품명 :: {$prd_name}\r\n주문번호 :: {$order_number}\r\n상품주문번호:{$shop_ord_no}\r\n옵션명 :: {$prd_option}";
        $dp_price_val   = (int)$dp_price_val;
        $dp_price       = $dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = $dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }
    elseif($choice_c_no == '23')   //후추 양식
    {
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("AM{$i}")->getValue()));   //주문일자
        $payment_date_val   = (string)trim(addslashes($objWorksheet->getCell("AN{$i}")->getValue()));   //결제일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));   //상품코드
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));   //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));   //옵션정보
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));   //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));   //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue()));   //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue()));   //수령자 전화번호
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AD{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AC{$i}")->getValue()));  //우편번호
        $delivery_memo      = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //배송메모
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("AK{$i}")->getValue()));   //배송비
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue()));  //쇼핑몰 주문번호
        $origin_ord_no      = $order_number;
        $dp_c_no            = "5859";
        $dp_c_name          = "shop by_후추";

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false){
            $order_date_convert = str_replace('/','-', $order_date_val);
            $order_date         = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
        }elseif(strpos($order_date_val, ".") !== false){
            $order_date_convert = str_replace('.','-', $order_date_val);
            $order_date         = $order_date_convert ? date('Y-m-d', strtotime("{$order_date_convert}"))." 00:00:00" : "";
        }elseif(strpos($order_date_val, "-") !== false){
            $order_date = date("Y-m-d H:i:s", strtotime($order_date_val));
        }else{
            $order_time_cal = ($order_date_val-25569)*86400;
            $order_time     = date("Y-m-d H:i", $order_time_cal);
            $order_date     = date("Y-m-d H:i:s", strtotime("{$order_time} -9 hour"));
        }

        if(strpos($payment_date_val, "/") !== false){
            $payment_date_convert = str_replace('/','-', $payment_date_val);
            $payment_date         = $payment_date_convert ? date('Y-m-d H:i:s', strtotime("{$payment_date_convert} -33 hours")) : "";
        }elseif(strpos($payment_date_val, ".") !== false){
            $payment_date_convert = str_replace('.','-', $payment_date_val);
            $payment_date         = $payment_date_convert ? date('Y-m-d', strtotime("{$payment_date_convert}"))." 00:00:00" : "";
        }elseif(strpos($payment_date_val, "-") !== false){
            $payment_date = date("Y-m-d H:i:s", strtotime($payment_date_val));
        }else{
            $payment_time_cal = ($payment_date_val-25569)*86400;
            $payment_time     = date("Y-m-d H:i", $payment_time_cal);
            $payment_date     = date("Y-m-d H:i:s", strtotime("{$payment_time} -9 hour"));
        }

        $task_req       = "상품명 :: {$prd_name}\r\n옵션명 :: {$prd_option}\r\n주문번호 :: {$order_number}";
        $dp_price       = (int)$dp_price_val / 1.1;
        $dp_price       = round($dp_price, -1); // 1의자리 반올림
        $dp_price_vat   = (int)$dp_price_val;
        $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    }

    if(isset($dup_ord_list[$order_number])){
        continue;
    }

    $temp_item = $cms_model->getTempWithOrderItem($order_number);
    if(!empty($temp_item) && isset($temp_item['t_no']))
    {
        $dup_ord_list[$order_number] = $order_number;
        if($temp_item['dp_c_no'] != $dp_c_no){
            $chk_dp_ord_list[$order_number] = array("ord_no" => $order_number, "org_dp" => $temp_item["dp_c_name"], "dup_dp" => $dp_c_name);
        }
        continue;
    }

    $order_item = $cms_model->getWithOrderItem($order_number);
    if(!empty($order_item) && isset($order_item['w_no']))
    {
        $dup_ord_list[$order_number] = $order_number;
        if($order_item['dp_c_no'] != $dp_c_no){
            $chk_dp_ord_list[$order_number] = array("ord_no" => $order_number, "org_dp" => $order_item["dp_c_name"], "dup_dp" => $dp_c_name);
        }
        continue;
    }

    $reserve_item = $reservation_model->getOrderItem($order_number);
    if(!empty($reserve_item) && isset($reserve_item['w_no']))
    {
        $dup_ord_list[$order_number] = $order_number;
        if($reserve_item['dp_c_no'] != $dp_c_no){
            $chk_dp_ord_list[$order_number] = array("ord_no" => $order_number, "org_dp" => $reserve_item["dp_c_name"], "dup_dp" => $dp_c_name);
        }
        continue;
    }

    // 상품코드가 없는 경우
    if(!$sku){
        echo "ROW : {$i}<br/>";
        echo "택배리스트 반영에 실패하였습니다.<br>상품코드가 없습니다.<br>
            주문날짜    : {$order_date}<br>
            주문번호    : {$order_number}<br>
            수령자명    : {$recipient}<br>
            Excel 상품명: {$prd_name}<br>";
        exit;
    }

    //주문번호 및 날짜 체크
    if($choice_c_no != 4 && $choice_c_no != 9)
    {
        if(empty($order_date) || empty($payment_date)){
            echo "ROW : {$i}<br/>";
            echo "택배리스트 반영에 실패하였습니다.<br>날짜가 이상합니다.<br>
            주문날짜    : {$order_date}<br>
            결제날짜    : {$payment_date}<br>
            주문번호    : {$order_number}<br>
            수령자명    : {$recipient}<br>";
            exit;
        }

        if($order_date < $limit_s_date || $order_date > $limit_e_date){
            echo "ROW : {$i}<br/>";
            echo "택배리스트 반영에 실패하였습니다.<br>주문날짜가 이상합니다.<br>
            주문날짜(원본)  : {$order_date_val}<br>
            주문날짜(변형)  : {$order_date}<br>
            주문번호        : {$order_number}<br>
            수령자명        : {$recipient}<br>";
            exit;
        }

        if($payment_date < $limit_s_date || $payment_date > $limit_e_date){
            echo "ROW : {$i}<br/>";
            echo "택배리스트 반영에 실패하였습니다.<br>결제날짜가 이상합니다.<br>
            결제날짜(원본)  : {$pay_date_val}<br>
            결제날짜(변형)  : {$payment_date}<br>
            주문번호        : {$order_number}<br>
            수령자명        : {$recipient}<br>";
            exit;
        }
    }

    # 상품번호 없읈 시 검색
    if(empty($prd_no))
    {
        if($choice_c_no == '11' || ($choice_c_no == '18' && $use_out_sku)){
            $product_data   = $product_model->getOutsourcingItem($dp_c_no, $sku);
        }else{
            $product_data   = $product_model->getWhereItem("prd_code = '{$sku}'");
        }

        $prd_no = (isset($product_data['prd_no']) && !empty($product_data['prd_no'])) ? $product_data['prd_no'] : "";
    }
    $product_item   = $product_model->getItem($product_data['prd_no']);

    //택배리스트 체크
    if($prd_no && $order_number)
    {
        $delivery_chk_sql   = "SELECT count(t_no) FROM work_cms_temporary WHERE order_number='{$order_number}' and prd_no ='{$prd_no}'";
        $delivery_chk_query = mysqli_query($my_db, $delivery_chk_sql);
        $delivery_chk       = mysqli_fetch_array($delivery_chk_query);

        if($delivery_chk[0] > 0){
            echo "ROW : {$i}<br/>";
            echo "택배리스트 반영에 실패하였습니다.<br>기존에 업로드한 내역이 있는지 확인하세요.<br>
            주문날짜    : {$order_date}<br>
            주문번호    : {$order_number}<br>
            상품번호    : {$shop_ord_no}<br>
            수령자명    : {$recipient}<br>
            상품코드    : {$sku}<br>";
            exit;
        }
    }else{
        echo "ROW : {$i}<br/>";
        echo "택배리스트 반영에 실패하였습니다.<br>확인되지 않은 상품 또는 주문번호가 있습니다.<br>
            주문날짜    : {$order_date}<br>
            주문번호    : {$order_number}<br>
            수령자명    : {$recipient}<br>
            상품코드    : {$sku}<br>";
        exit;
    }

    if($product_item['log_c_no'] == 0)
    {
        echo "ROW : {$i}<br/>";
        echo "택배리스트 반영에 실패하였습니다.<br>발송불가한 상품입니다.<br>
            주문날짜    : {$order_date}<br>
            주문번호    : {$order_number}<br>
            상품명      : {$product_item['title']}<br>
            상품번호    : {$product_item['prd_no']}<br>
            상품코드    : {$sku}<br>";
        exit;
    }

    $prd_cms_data  = $product_model->getWorkCmsItem($prd_no);
    if(!empty($prd_cms_data))
    {
        $c_no   = $prd_cms_data['c_no'];
        $c_name = $prd_cms_data['c_name'];
        $s_no   = $prd_cms_data['manager'];
        $team   = $prd_cms_data['team'];
        $task_req_s_no = $s_no;
        $task_req_team = $team;
    }

    if(empty($dp_c_no)){
        echo "ROW : {$i}<br/>";
        echo "택배리스트 반영에 실패하였습니다.<br>등록되지 않은 구매처입니다.<br>
            구매처    : {$dp_c_name}<br>";
        exit;
    }

    if($order_type != "택배"){
        $log_c_no       = 1113;
        $recipient      = $sender_name;
        $recipient_hp   = $sender_hp;
        $recipient_addr = "";
        $zip_code       = "";
    }

    if($choice_c_no == "1" || $choice_c_no == "17" || $choice_c_no == "20")
    {
        if(empty($main_dp_c_name)){
            $main_dp_c_name = $dp_c_name;
        }

        if(!isset($chk_url_exist_list[$page_idx]) && !isset($chk_url_yet_list[$page_idx]))
        {
            $chk_page_idx   = "idx=".$page_idx;
            $chk_url_sql    = "SELECT COUNT(*) as cnt FROM commerce_url WHERE url='{$chk_page_idx}' AND dp_c_no='{$dp_c_no}'";
            $chk_url_query  = mysqli_query($my_db, $chk_url_sql);
            $chk_url_result = mysqli_fetch_assoc($chk_url_query);
            if($chk_url_result['cnt'] > 0){
                $chk_url_exist_list[$page_idx] = $page_idx;
            }else{
                $chk_url_yet_list[$page_idx] = $page_idx;
            }
        }
    }

    $task_req        .= "\r\n구매처 :: {$dp_c_name}";
    $task_run_regdate = !empty($order_date) ? $order_date : $regdate;
    $unit_price       = $dp_price_vat;
    $recipient_hp     = str_replace("-", "", $recipient_hp);
    $recipient_hp     = str_replace("+", "", $recipient_hp);
    $recipient_hp     = str_replace("(", "", $recipient_hp);
    $recipient_hp     = str_replace(")", "", $recipient_hp);
    $recipient_hp     = str_replace(".", "", $recipient_hp);

    if(isset($active_event_list[$dp_c_no]) && in_array($sku, $chk_nuzam_all_prd_list))
    {
        foreach($active_event_list[$dp_c_no] as $event_date)
        {
            $order_date = date("Y-m-d H:i:s", strtotime($order_date));
            if($event_date['event_no'] == '9')
            {
                if($order_date >= $event_date['s_date'] && $order_date <= $event_date['e_date'])
                {
                    $add_nuzam_prd   = true;
                    if(in_array($sku, $chk_nuzam_total_prd_list)){
                        $add_nuzam_prd_list[] = array("prd_no" => 1600, "qty" => 1);
                    }else{
                        $add_nuzam_prd = false;
                    }

                    if($add_nuzam_prd){
                        break;
                    }
                }
            }
            elseif($event_date['event_no'] == '32')
            {
                if($order_date >= $event_date['s_date'] && $order_date <= $event_date['e_date'])
                {
                    $add_nuzam_prd   = true;
                    if(in_array($sku, $chk_nuzam_total_new_prd_list)){
                        $add_nuzam_prd_list[] = array("prd_no" => 1600, "qty" => 1);
                    }else{
                        $add_nuzam_prd = false;
                    }

                    if($add_nuzam_prd){
                        break;
                    }
                }
            }
            elseif($event_date['event_no'] == '36')
            {
                if($order_date >= $event_date['s_date'] && $order_date <= $event_date['e_date'])
                {
                    $add_nuzam_prd   = true;
                    if(in_array($sku, $chk_nuzam_ss_single_list)){
                        $add_nuzam_prd_list[] = array("prd_no" => 1027, "qty" => 1);
                    }
                    elseif(in_array($sku, $chk_nuzam_ss_double_list)){
                        $add_nuzam_prd_list[] = array("prd_no" => 1027, "qty" => 2);
                    }
                    elseif(in_array($sku, $chk_nuzam_q_single_list)){
                        $add_nuzam_prd_list[] = array("prd_no" => 1028, "qty" => 1);
                    }
                    elseif(in_array($sku, $chk_nuzam_q_double_list)){
                        $add_nuzam_prd_list[] = array("prd_no" => 1028, "qty" => 2);
                    }
                    elseif(in_array($sku, $chk_nuzam_ss_q_list)){
                        $add_nuzam_prd_list[] = array("prd_no" => 1027, "qty" => 1);
                        $add_nuzam_prd_list[] = array("prd_no" => 1028, "qty" => 1);
                    }
                    else{
                        $add_nuzam_prd = false;
                    }

                    if($add_nuzam_prd){
                        break;
                    }
                }
            }
        }
    }

    $order_date         = date("Y-m-d H:i:s", strtotime($order_date));

    if($choice_c_no == '3') # 사방넷
    {
        # 11번가, 오늘의집, 베네피아(결제금에서 배송비 포함되는 경우)
        if($dp_c_no == '1818' || $dp_c_no == '3337' || $dp_c_no == '5157'){
            $unit_price = $dp_price_vat-$delivery_price;
        }

        if(!isset($delivery_data[$order_number]) && !isset($delivery_data[$order_number][$order_date])){
            $delivery_data[$order_number][$order_date] = $delivery_price;
        } elseif($delivery_price > 0){
            $delivery_data[$order_number][$order_date] += $delivery_price;
        }

        if($dp_price_vat > 0){
            $total_price_data[$order_number][$order_date] += $dp_price_vat;
        }

        $excel_data_list[$order_number][$order_date][] = array(
            "t_type"                 => $choice_c_no,
            "c_no"                   => $c_no,
            "c_name"                 => $c_name,
            "log_c_no"               => $log_c_no,
            "s_no"                   => $s_no,
            "team"                   => $team,
            "prd_no"                 => $prd_no,
            "quantity"               => $quantity,
            "order_type"             => $order_type,
            "order_number"           => $order_number,
            "order_date"             => $order_date,
            "payment_date"           => $payment_date,
            "recipient"              => $recipient,
            "recipient_addr"         => $recipient_addr,
            "recipient_hp"           => $recipient_hp,
            "recipient_hp2"          => $recipient_hp2,
            "zip_code"               => $zip_code,
            "dp_price"               => $dp_price,
            "dp_price_vat"           => $dp_price_vat,
            "dp_c_no"                => $dp_c_no,
            "dp_c_name"              => $dp_c_name,
            "regdate"                => $regdate,
            "write_date"             => $regdate,
            "task_req"               => $task_req,
            "task_req_s_no"          => $task_req_s_no,
            "task_req_team"          => $task_req_team,
            "task_run_regdate"       => $task_run_regdate,
            "task_run_s_no"          => $task_run_s_no,
            "task_run_team"          => $task_run_team,
            "delivery_memo"          => $delivery_memo,
            "manager_memo"           => $manager_memo,
            "payment_type"           => $payment_type,
            "payment_code"           => $payment_code,
            "shop_ord_no"            => $shop_ord_no,
            "section_ord_no"         => $section_ord_no,
            "notice"                 => $notice,
            "subs_application_times" => $subs_application_times,
            "subs_progression_times" => $subs_progression_times,
            "delivery_price"         => $delivery_price,
            "account"                => $account_code,
            "coupon_price"           => $coupon_price,
            "final_price"            => $final_price,
            "unit_price"             => $unit_price,
            "unit_delivery_price"    => $unit_delivery_price,
            "origin_ord_no"          => $origin_ord_no,
        );
    }
    else
    {
        if(!isset($delivery_data[$order_number])){
            $delivery_data[$order_number] = $delivery_price;
        }elseif($delivery_data[$order_number] <= 0 && $delivery_price > 0){
            $delivery_data[$order_number] = $delivery_price;
        }

        if(!isset($coupon_data[$order_number])){
            $coupon_data[$order_number] = $coupon_price;
        }elseif($coupon_data[$order_number] <= 0 && $coupon_price > 0){
            $coupon_data[$order_number] = $coupon_price;
        }

        if(!isset($discount_data[$order_number])){
            $discount_data[$order_number] = $discount_price;
        }elseif($discount_data[$order_number] <= 0 && $discount_price > 0){
            $discount_data[$order_number] = $discount_price;
        }

        $coupon_price_val   = ($dp_price_vat > 0) ? $coupon_price : 0;

        if($dp_price_vat > 0){
            $total_price_data[$order_number] += $dp_price_vat;
        }

        $chk_is_wait    = 0;
        $chk_out_date   = "NULL";

        if(in_array($sku, $chk_pillow_list)
            && ($dp_c_no == "6012" || $dp_c_no == "4629")
            && ($order_date >= "2025-02-26 14:00:00")
        ){
            $chk_is_wait    = "1";
            $chk_out_date   = "2025-03-28";
        }

        # 1차 엑셀데이터 정리
        $excel_data_list[$order_number][] = array(
            "t_type"                 => $choice_c_no,
            "c_no"                   => $c_no,
            "c_name"                 => $c_name,
            "log_c_no"               => $log_c_no,
            "s_no"                   => $s_no,
            "team"                   => $team,
            "prd_no"                 => $prd_no,
            "quantity"               => $quantity,
            "order_type"             => $order_type,
            "order_number"           => $order_number,
            "order_date"             => $order_date,
            "payment_date"           => $payment_date,
            "recipient"              => $recipient,
            "recipient_addr"         => $recipient_addr,
            "recipient_hp"           => $recipient_hp,
            "recipient_hp2"          => $recipient_hp2,
            "zip_code"               => $zip_code,
            "dp_price"               => $dp_price,
            "dp_price_vat"           => $dp_price_vat,
            "dp_c_no"                => $dp_c_no,
            "dp_c_name"              => $dp_c_name,
            "regdate"                => $regdate,
            "write_date"             => $regdate,
            "task_req"               => $task_req,
            "task_req_s_no"          => $task_req_s_no,
            "task_req_team"          => $task_req_team,
            "task_run_regdate"       => $task_run_regdate,
            "task_run_s_no"          => $task_run_s_no,
            "task_run_team"          => $task_run_team,
            "delivery_memo"          => $delivery_memo,
            "manager_memo"           => $manager_memo,
            "payment_type"           => $payment_type,
            "payment_code"           => $payment_code,
            "shop_ord_no"            => $shop_ord_no,
            "section_ord_no"         => $section_ord_no,
            "notice"                 => $notice,
            "subs_application_times" => $subs_application_times,
            "subs_progression_times" => $subs_progression_times,
            "delivery_price"         => $delivery_price,
            "account"                => $account_code,
            "coupon_price"           => $coupon_price_val,
            "final_price"            => $final_price,
            "unit_price"             => $unit_price,
            "unit_delivery_price"    => $unit_delivery_price,
            "page_idx"               => !empty($page_idx) ? $page_idx : "NULL",
            "origin_ord_no"          => $origin_ord_no,
            "is_wait"                => $chk_is_wait,
            "out_date"               => $chk_out_date,
        );
    }

    if($add_nuzam_prd && !empty($add_nuzam_prd_list))
    {
        foreach($add_nuzam_prd_list as $add_nuzam_prd_data)
        {
            $chk_is_wait        = 0;
            $add_nuzam_prd_no   = $add_nuzam_prd_data['prd_no'];
            $add_nuzam_prd_qty  = isset($add_nuzam_prd_data['qty']) && !empty($add_nuzam_prd_data['qty']) ? $add_nuzam_prd_data['qty'] : 1;
            $nuzam_gift_qty     = $quantity*$add_nuzam_prd_qty;
            $nuzam_gift_item    = $product_model->getWorkCmsItem($add_nuzam_prd_no);
            $gift_prd_task_req2 = "상품명 :: {$nuzam_gift_item['title']}\r\n주문번호 :: {$order_number}\r\n이벤트 연관상품명:{$prd_name}\r\n구매처 :: {$dp_c_name}";

            $event_data_list[$order_number][] = array(
                "t_type"                 => $choice_c_no,
                "c_no"                   => $nuzam_gift_item['c_no'],
                "c_name"                 => $nuzam_gift_item['c_name'],
                "log_c_no"               => $log_c_no,
                "s_no"                   => $nuzam_gift_item['manager'],
                "team"                   => $nuzam_gift_item['team'],
                "prd_no"                 => $add_nuzam_prd_no,
                "quantity"               => $nuzam_gift_qty,
                "order_type"             => $order_type,
                "order_number"           => $order_number,
                "order_date"             => $order_date,
                "payment_date"           => $payment_date,
                "recipient"              => $recipient,
                "recipient_addr"         => $recipient_addr,
                "recipient_hp"           => $recipient_hp,
                "recipient_hp2"          => $recipient_hp2,
                "zip_code"               => $zip_code,
                "dp_price"               => 0,
                "dp_price_vat"           => 0,
                "dp_c_no"                => $dp_c_no,
                "dp_c_name"              => $dp_c_name,
                "regdate"                => $regdate,
                "write_date"             => $regdate,
                "task_req"               => $gift_prd_task_req2,
                "task_req_s_no"          => $nuzam_gift_item['manager'],
                "task_req_team"          => $nuzam_gift_item['team'],
                "task_run_regdate"       => $task_run_regdate,
                "task_run_s_no"          => $task_run_s_no,
                "task_run_team"          => $task_run_team,
                "delivery_memo"          => $delivery_memo,
                "manager_memo"           => $manager_memo,
                "payment_type"           => "",
                "shop_ord_no"            => "",
                "notice"                 => $notice,
                "subs_application_times" => 0,
                "subs_progression_times" => 0,
                "delivery_price"         => 0,
                "account"                => $account_code,
                "coupon_price"           => 0,
                "final_price"            => 0,
                "unit_price"             => 0,
                "unit_delivery_price"    => 0,
                "is_wait"                => $chk_is_wait,
            );
        }
    }

    if($add_eye_event_prd)
    {
        $eye_event_item = $product_model->getWorkCmsItem($eye_event_prd, "prd_code");

        $task_req .= "\r\n아이레놀 공동구매 {$eye_event_text}({$eye_event_prd} 증정)";
        $event_data_list[$order_number][] = array(
            "t_type"                 => $choice_c_no,
            "c_no"                   => $eye_event_item['c_no'],
            "c_name"                 => $eye_event_item['c_name'],
            "log_c_no"               => $log_c_no,
            "s_no"                   => $eye_event_item['manager'],
            "team"                   => $eye_event_item['team'],
            "prd_no"                 => $eye_event_item['prd_no'],
            "quantity"               => $quantity,
            "order_type"             => $order_type,
            "order_number"           => $order_number,
            "order_date"             => $order_date,
            "payment_date"           => $payment_date,
            "recipient"              => $recipient,
            "recipient_addr"         => $recipient_addr,
            "recipient_hp"           => $recipient_hp,
            "recipient_hp2"          => $recipient_hp2,
            "zip_code"               => $zip_code,
            "dp_price"               => 0,
            "dp_price_vat"           => 0,
            "dp_c_no"                => $dp_c_no,
            "dp_c_name"              => $dp_c_name,
            "regdate"                => $regdate,
            "write_date"             => $regdate,
            "task_req"               => $task_req,
            "task_req_s_no"          => $eye_event_item['manager'],
            "task_req_team"          => $eye_event_item['team'],
            "task_run_regdate"       => $task_run_regdate,
            "task_run_s_no"          => $task_run_s_no,
            "task_run_team"          => $task_run_team,
            "delivery_memo"          => $delivery_memo,
            "manager_memo"           => $manager_memo,
            "payment_type"           => $payment_type,
            "shop_ord_no"            => "",
            "notice"                 => $notice,
            "subs_application_times" => 0,
            "subs_progression_times" => 0,
            "delivery_price"         => 0,
            "account"                => $account_code,
            "coupon_price"           => 0,
            "final_price"            => 0,
            "unit_price"             => 0,
            "unit_delivery_price"    => 0,
        );
    }

    if($is_nuzam_option && !empty($nuzam_option_prd))
    {
        $option_product_data     = $product_model->getWhereItem("prd_code = '{$nuzam_option_prd}'");
        $nuzam_option_item       = $product_model->getWorkCmsItem($option_product_data['prd_no']);
        $nuzam_option_task_req   = "상품명 :: {$nuzam_option_item['title']}\r\n주문번호 :: {$order_number}\r\n옵션 연관상품명:{$prd_name}\r\n구매처 : {$dp_c_name}";

        $excel_data_list[$order_number][] = array(
            "t_type"                 => $choice_c_no,
            "c_no"                   => $nuzam_option_item['c_no'],
            "c_name"                 => $nuzam_option_item['c_name'],
            "log_c_no"               => $log_c_no,
            "s_no"                   => $nuzam_option_item['manager'],
            "team"                   => $nuzam_option_item['team'],
            "prd_no"                 => $nuzam_option_item['prd_no'],
            "quantity"               => $quantity,
            "order_type"             => $order_type,
            "order_number"           => $order_number,
            "order_date"             => $order_date,
            "payment_date"           => $payment_date,
            "recipient"              => $recipient,
            "recipient_addr"         => $recipient_addr,
            "recipient_hp"           => $recipient_hp,
            "recipient_hp2"          => $recipient_hp2,
            "zip_code"               => $zip_code,
            "dp_price"               => 0,
            "dp_price_vat"           => 0,
            "dp_c_no"                => $dp_c_no,
            "dp_c_name"              => $dp_c_name,
            "regdate"                => $regdate,
            "write_date"             => $regdate,
            "task_req"               => $nuzam_option_task_req,
            "task_req_s_no"          => $nuzam_option_item['manager'],
            "task_req_team"          => $nuzam_option_item['team'],
            "task_run_regdate"       => $task_run_regdate,
            "task_run_s_no"          => $task_run_s_no,
            "task_run_team"          => $task_run_team,
            "delivery_memo"          => $delivery_memo,
            "manager_memo"           => $manager_memo,
            "payment_type"           => $payment_type,
            "shop_ord_no"            => "",
            "notice"                 => $notice,
            "subs_application_times" => 0,
            "subs_progression_times" => 0,
            "delivery_price"         => 0,
            "account"                => $account_code,
            "coupon_price"           => 0,
            "final_price"            => $final_price,
            "unit_price"             => 0,
            "unit_delivery_price"    => 0,
            "page_idx"               => $page_idx,
            "origin_ord_no"          => $origin_ord_no,
        );
    }

    if($add_lotte_double_prd || $add_lotte_single_prd || $add_lotte_bedding_prd || $add_lotte_set_prd)
    {
        $nuzam_event_item_list = [];
        if($add_lotte_double_prd){
            $nuzam_event_item_list = array("1600" => 1, "1192" => 1);
        }elseif($add_lotte_single_prd){
            $nuzam_event_item_list = array("1600" => 1);
        }elseif($add_lotte_bedding_prd){
            $nuzam_event_item_list = array("1167" => 1);
        }elseif($add_lotte_set_prd){
            $nuzam_event_item_list = array("1600" => 2);
        }

        if(!empty($nuzam_event_item_list))
        {
            foreach($nuzam_event_item_list as $new_event_prd_no => $new_event_prd_qty)
            {
                $nuzam_event_item       = $product_model->getWorkCmsItem($new_event_prd_no);
                $nuzam_event_task_req   = "상품명 :: {$nuzam_event_item['title']}\r\n주문번호 :: {$order_number}\r\n이벤트 연관상품명:{$prd_name}\r\n구매처 : {$dp_c_name}";
                $nuzam_event_qty        = $quantity*$new_event_prd_qty;

                $event_data_list[$order_number][] = array(
                    "t_type"                 => $choice_c_no,
                    "c_no"                   => $nuzam_event_item['c_no'],
                    "c_name"                 => $nuzam_event_item['c_name'],
                    "log_c_no"               => $log_c_no,
                    "s_no"                   => $nuzam_event_item['manager'],
                    "team"                   => $nuzam_event_item['team'],
                    "prd_no"                 => $nuzam_event_item['prd_no'],
                    "quantity"               => $nuzam_event_qty,
                    "order_type"             => $order_type,
                    "order_number"           => $order_number,
                    "order_date"             => $order_date,
                    "payment_date"           => $payment_date,
                    "recipient"              => $recipient,
                    "recipient_addr"         => $recipient_addr,
                    "recipient_hp"           => $recipient_hp,
                    "recipient_hp2"          => $recipient_hp2,
                    "zip_code"               => $zip_code,
                    "dp_price"               => 0,
                    "dp_price_vat"           => 0,
                    "dp_c_no"                => $dp_c_no,
                    "dp_c_name"              => $dp_c_name,
                    "regdate"                => $regdate,
                    "write_date"             => $regdate,
                    "task_req"               => $nuzam_event_task_req,
                    "task_req_s_no"          => $nuzam_event_item['manager'],
                    "task_req_team"          => $nuzam_event_item['team'],
                    "task_run_regdate"       => $task_run_regdate,
                    "task_run_s_no"          => $task_run_s_no,
                    "task_run_team"          => $task_run_team,
                    "delivery_memo"          => $delivery_memo,
                    "manager_memo"           => $manager_memo,
                    "payment_type"           => $payment_type,
                    "shop_ord_no"            => "",
                    "notice"                 => $notice,
                    "subs_application_times" => 0,
                    "subs_progression_times" => 0,
                    "delivery_price"         => 0,
                    "account"                => $account_code,
                    "coupon_price"           => 0,
                    "final_price"            => 0,
                    "unit_price"             => 0,
                    "unit_delivery_price"    => 0,
                );
            }
        }
    }

    $total_cnt++;
}

$final_insert_data = [];
if($choice_c_no == '1' || $choice_c_no == '2' || $choice_c_no == '5' || $choice_c_no == '6' || $choice_c_no == '8' || $choice_c_no == '9'
    || $choice_c_no == '10' || $choice_c_no == '17' || $choice_c_no == '18' || $choice_c_no == '20' || $choice_c_no == '21' || $choice_c_no == '22' || $choice_c_no == '24'
    || $choice_c_no == '91' || $choice_c_no == '92' || $choice_c_no == '93'
)
{
    $idx = 0;
    foreach($excel_data_list as $ord_no => $insert_data)
    {
        $total_delivery_price   = isset($delivery_data[$ord_no]) ? $delivery_data[$ord_no] : 0;
        $total_price            = isset($total_price_data[$ord_no]) ? $total_price_data[$ord_no] : 0;
        $event_data             = isset($event_data_list[$ord_no]) ? $event_data_list[$ord_no] : "";

        if($total_delivery_price > 0)
        {
            $unit_deli_price = ($total_price > 0) ? $total_delivery_price/$total_price : 0;
            $chk_deli_price  = 0;
            $chk_dp_price    = 0;
            $chk_idx         = 0;
            foreach($insert_data as $prd_data)
            {
                $chk_prd_price = $prd_data['dp_price_vat'];
                if($chk_prd_price > 0)
                {
                    $chk_unit_deli_price = round($unit_deli_price*$chk_prd_price);
                    $chk_deli_price     += $chk_unit_deli_price;
                    $chk_dp_price       += $chk_prd_price;
                    $chk_add_price       = 0;
                    if($chk_dp_price == $total_price && $chk_deli_price != $total_delivery_price){
                        $chk_add_price = $total_delivery_price-$chk_deli_price;
                    }
                    $prd_data['unit_delivery_price'] = $chk_unit_deli_price+$chk_add_price;

                    $insert_data[$chk_idx] = $prd_data;
                }
                $chk_idx++;
            }
        }

        $total_coupon_price = isset($coupon_data[$ord_no]) ? $coupon_data[$ord_no] : 0;
        if($total_coupon_price > 0)
        {
            $unit_coupon_price  = ($total_price > 0) ? $total_coupon_price/$total_price : 0;
            $chk_coupon_price   = 0;
            $chk_dp_price       = 0;
            $chk_idx            = 0;
            foreach($insert_data as $prd_data)
            {
                $chk_prd_price = $prd_data['dp_price_vat'];
                if($chk_prd_price > 0)
                {
                    $chk_unit_coupon_price  = round($unit_coupon_price*$chk_prd_price);
                    $chk_coupon_price      += $chk_unit_coupon_price;
                    $chk_dp_price          += $chk_prd_price;
                    $chk_add_coupon         = 0;
                    if($chk_dp_price == $total_price && $chk_coupon_price != $total_coupon_price){
                        $chk_add_coupon = $total_coupon_price-$chk_coupon_price;
                    }
                    $prd_data['coupon_price'] = $chk_unit_coupon_price+$chk_add_coupon;

                    $insert_data[$chk_idx] = $prd_data;
                }
                $chk_idx++;
            }
        }

        $total_discount_price = isset($discount_data[$ord_no]) ? $discount_data[$ord_no] : 0;
        if($total_discount_price > 0)
        {
            $unit_discount_price    = $total_discount_price/$total_price;
            $chk_discount_price     = 0;
            $chk_dp_price           = 0;
            $chk_discount_idx       = 0;
            foreach($insert_data as $prd_data)
            {
                $chk_prd_price = $prd_data['dp_price_vat'];
                if($chk_prd_price > 0)
                {
                    $chk_unit_discount_price = round($unit_discount_price*$chk_prd_price);
                    $chk_discount_price     += $chk_unit_discount_price;
                    $chk_dp_price           += $chk_prd_price;
                    $chk_add_discount    = 0;
                    if($chk_dp_price == $total_price && $chk_discount_price != $total_discount_price){
                        $chk_add_discount = $total_discount_price-$chk_discount_price;
                    }
                    $prd_data['unit_price'] -= $chk_unit_discount_price+$chk_add_discount;

                    $insert_data[$chk_discount_idx] = $prd_data;
                }
                $chk_discount_idx++;
            }
        }

        $final_insert_data = array_merge($final_insert_data, $insert_data);

        if(!empty($event_data)){
            $final_insert_data = array_merge($final_insert_data, $event_data);
        }

        $idx++;
    }
}
elseif($choice_c_no == '3')
{
    foreach($excel_data_list as $ord_no => $date_data)
    {
        $event_data = isset($event_data_list[$ord_no]) ? $event_data_list[$ord_no] : "";
        foreach($date_data as $ord_date => $insert_data)
        {
            $total_delivery_price  = isset($delivery_data[$ord_no][$ord_date]) ? $delivery_data[$ord_no][$ord_date] : 0;
            $total_price           = isset($total_price_data[$ord_no][$ord_date]) ? $total_price_data[$ord_no][$ord_date] : 0;

            if($total_delivery_price > 0)
            {
                $unit_deli_price = $total_delivery_price/$total_price;
                $chk_deli_price  = 0;
                $chk_dp_price    = 0;
                $chk_idx         = 0;
                foreach($insert_data as $prd_data)
                {
                    $chk_prd_price = $prd_data['dp_price_vat'];
                    if($chk_prd_price > 0)
                    {
                        $chk_unit_deli_price = round($unit_deli_price*$chk_prd_price);
                        $chk_deli_price     += $chk_unit_deli_price;
                        $chk_dp_price       += $chk_prd_price;
                        $chk_add_price   = 0;
                        if($chk_dp_price == $total_price && $chk_deli_price != $total_delivery_price){
                            $chk_add_price = $total_delivery_price-$chk_deli_price;
                        }
                        $prd_data['unit_delivery_price'] = ($chk_unit_deli_price)+$chk_add_price;

                        $insert_data[$chk_idx] = $prd_data;
                    }
                    $chk_idx++;
                }
            }

            $final_insert_data = array_merge($final_insert_data, $insert_data);
        }

        if(!empty($event_data)){
            $final_insert_data = array_merge($final_insert_data, $event_data);
        }
    }
}
else
{
    foreach($excel_data_list as $ord_no => $insert_data)
    {
        if($choice_c_no == "19")
        {
            $key_idx = 0;
            foreach($insert_data as $insert_detail_data){
                if(isset($chk_sep_list[$insert_detail_data['shop_ord_no']])) {
                    $insert_data[$key_idx]["notice"] = "분리배송:{$chk_sep_list[$insert_detail_data['shop_ord_no']]}";
                }
                elseif(strpos($insert_detail_data['task_req'], "경품") !== false
                    && isset($lotte_address_order_list[$insert_detail_data['recipient_addr']])
                ){
                    $insert_data[$key_idx]["order_number"] = $lotte_address_order_list[$insert_detail_data['recipient_addr']];
                }

                $key_idx++;
            }
        }

        $final_insert_data = array_merge($final_insert_data, $insert_data);

        $event_data = isset($event_data_list[$ord_no]) ? $event_data_list[$ord_no] : "";
        if(!empty($event_data)){
            $final_insert_data = array_merge($final_insert_data, $event_data);
        }
    }
}

if(!empty($final_insert_data))
{
    if($cms_model->multiInsert($final_insert_data))
    {
        if(!empty($track_data)){
            $track_model = WorkCms::Factory();
            $track_model->setMainInit("work_cms_delivery", "no");
            $track_model->multiInsert($track_data);
        }

        if(!empty($chk_url_yet_list))
        {
            $chk_url_yet_text   = implode(",", $chk_url_yet_list);
            $waple_alert_text   = "[커머스 URL 미등록 알림]\r\n금일 주문 발송 건 중에 커머스 URL에 등록되지 않은건이 확인되었습니다.\r\n· 구매처 = {$main_dp_c_name}\r\n· idx = {$chk_url_yet_text}\r\n담당자 확인 후 커머스 URL에 등록 바랍니다.";
            
            $chk_ins_sql        = "INSERT INTO waple_chat_content SET wc_no='2', s_no='171', content='{$waple_alert_text}', alert_type='91', regdate=now()";
            mysqli_query($my_db, $chk_ins_sql);
        }

        if(!empty($chk_dp_ord_list))
        {
            foreach($chk_dp_ord_list as $chk_dp_data)
            {
                $dup_text       = "[커머스 중복주문 알림]\r\n주문번호 중복 주문이 발견되었습니다.\r\n· 주문번호 : {$chk_dp_data['ord_no']}\r\n· 원구매처: {$chk_dp_data['org_dp']}\r\n· 중복구매처 : {$chk_dp_data['dup_dp']}\r\n확인 부탁드립니다.";
                $dup_ins_sql    = "INSERT INTO waple_chat_content SET wc_no='2', s_no='102', content='{$dup_text}', alert_type='81', regdate=now()";
                mysqli_query($my_db, $dup_ins_sql);
            }
        }

        if($dup_ord_list){
            echo "택배리스트 반영되었습니다. 중복 주문번호 확인 하신 후 꺼주세요.<br/>";
            echo implode("<br/>", $dup_ord_list);
            exit;
        }else{
            exit("<script>alert('택배리스트가 반영 되었습니다.');location.href='work_cms_temporary.php';</script>");
        }
    }
}

exit("<script>alert('택배리스트 반영에 실패했습니다.');location.href='work_cms_temporary.php';</script>");

?>