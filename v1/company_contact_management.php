<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/company.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Team.php');

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$company_model  = Company::Factory();
$company_model->setMainInit('company_contact', 'cc_no');

if($process == "new_company_contact")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $new_hp     = !empty($_POST['new_hp1']) ? $_POST['new_hp1']."-".$_POST['new_hp2']."-".$_POST['new_hp3'] : "";
    $new_email  = !empty($_POST['new_email1']) ? $_POST['new_email1']."@".$_POST['new_email2'] : "";

    $ins_data = array(
        "manager"       => (isset($_POST['new_manager'])) ? addslashes(trim($_POST['new_manager'])) : "",
        "manager_team"  => (isset($_POST['new_manager_team'])) ? addslashes(trim($_POST['new_manager_team'])) : "",
        "type"          => (isset($_POST['new_type'])) ? addslashes(trim($_POST['new_type'])) : "",
        "c_date"        => (isset($_POST['new_c_date'])) ? addslashes(trim($_POST['new_c_date'])) : "",
        "task_run"      => (isset($_POST['new_task_run'])) ? addslashes(trim($_POST['new_task_run'])) : "",
        "c_name"        => (isset($_POST['new_c_name'])) ? addslashes(trim($_POST['new_c_name'])) : "",
        "brand"         => (isset($_POST['new_brand'])) ? addslashes(trim($_POST['new_brand'])) : "",
        "product"       => (isset($_POST['new_product'])) ? addslashes(trim($_POST['new_product'])) : "",
        "category"      => (isset($_POST['new_category'])) ? addslashes(trim($_POST['new_category'])) : "",
        "etc"           => (isset($_POST['new_etc'])) ? addslashes(trim($_POST['new_etc'])) : "",
        "ceo"           => (isset($_POST['new_ceo'])) ? addslashes(trim($_POST['new_ceo'])) : "",
        "address"       => (isset($_POST['new_address'])) ? addslashes(trim($_POST['new_address'])) : "",
        "homepage"      => (isset($_POST['new_homepage'])) ? addslashes(trim($_POST['new_homepage'])) : "",
        "notice"        => (isset($_POST['new_notice'])) ? addslashes(trim($_POST['new_notice'])) : "",
        "hp"            => addslashes(trim($new_hp)),
        "email"         => addslashes(trim($new_email)),
        "is_recontact"  => '1',
        "regdate"       => date("Y-m-d H:i:s"),
    );

    if(!$company_model->insert($ins_data)){
        exit("<script>alert('등록에 실패했습니다');location.href='company_contact_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('등록했습니다');location.href='company_contact_management.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_data")
{
    $upd_type = (isset($_POST['type'])) ? $_POST['type'] : "";
    $upd_data = array(
        "cc_no"     => (isset($_POST['no'])) ? $_POST['no'] : "",
        $upd_type   => (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "",
    );

    if ($company_model->update($upd_data)) {
        echo "변경됬습니다.";
    } else{
        echo "변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "mod_recontact")
{
    $chk_cc_no  = isset($_POST['chk_cc_no']) ? $_POST['chk_cc_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_data = array(
        "cc_no"         => (isset($_POST['chk_cc_no'])) ? $_POST['chk_cc_no'] : "",
        "c_date"        => date("Y-m-d"),
        "task_run"      => "NULL",
        "manager"       => $session_s_no,
        "manager_team"  => $session_team
    );

    if (!$company_model->update($upd_data)) {
        exit("<script>alert('재컨택에 실패했습니다');location.href='company_contact_management.php?{$search_url}#contact-tr-{$chk_cc_no}';</script>");
    }else{
        exit("<script>alert('재컨택 했습니다');location.href='company_contact_management.php?{$search_url}#contact-tr-{$chk_cc_no}';</script>");
    }
}
elseif($process == "del_contact")
{
    $chk_cc_no  = isset($_POST['chk_cc_no']) ? $_POST['chk_cc_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_data = array(
        "cc_no"         => (isset($_POST['chk_cc_no'])) ? $_POST['chk_cc_no'] : "",
        "display"       => '2'
    );

    if (!$company_model->update($upd_data)) {
        exit("<script>alert('삭제에 실패했습니다');location.href='company_contact_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제했습니다');location.href='company_contact_management.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "150";
$nav_title   = "파트너 컨택 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

#검색 조건
$add_where          = "1=1 AND display='1'";
$sch_cc_no          = isset($_GET['sch_cc_no']) ? $_GET['sch_cc_no'] : "";
$sch_manager_team   = isset($_GET['sch_manager_team']) ? $_GET['sch_manager_team'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_type           = isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_s_date         = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : "";
$sch_e_date         = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : "";
$sch_recontact      = isset($_GET['sch_recontact']) ? $_GET['sch_recontact'] : "";
$sch_hp1            = isset($_GET['sch_hp1']) ? $_GET['sch_hp1'] : "";
$sch_hp2            = isset($_GET['sch_hp2']) ? $_GET['sch_hp2'] : "";
$sch_email          = isset($_GET['sch_email']) ? $_GET['sch_email'] : "";
$sch_brand_company  = isset($_GET['sch_brand_company']) ? $_GET['sch_brand_company'] : "";
$warning_msg        = "";

if(!empty($sch_cc_no)) {
    $add_where  .= " AND cc.cc_no = '{$sch_cc_no}'";
    $smarty->assign("sch_cc_no", $sch_cc_no);
}

$team_model         = Team::Factory();
$team_all_list      = $team_model->getTeamAllList();
$staff_team_list    = $team_all_list['staff_list'];
$team_full_name_list= $team_model->getTeamFullNameList();
$sch_staff_list     = [];

if (!empty($sch_manager_team))
{
    if ($sch_manager_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_manager_team);
        $add_where 		 .= " AND `cc`.manager_team IN({$sch_team_code_where})";
        $sch_staff_list   = $staff_team_list[$sch_manager_team];
    }
    $smarty->assign("sch_manager_team", $sch_manager_team);
}

if (!empty($sch_manager))
{
    if ($sch_manager != "all") {
        $add_where 		 .= " AND `cc`.manager='{$sch_manager}'";
    }
    $smarty->assign("sch_manager", $sch_manager);
}

if(!empty($sch_type)) {
    $add_where  .= " AND cc.`type` = '{$sch_type}'";
    $smarty->assign("sch_type", $sch_type);
}

if(!empty($sch_s_date)){
    $sch_s_date_val = $sch_s_date."-01";
    $add_where  .= " AND cc.c_date >= '{$sch_s_date_val}'";
    $smarty->assign("sch_s_date", $sch_s_date);
}

if(!empty($sch_e_date)){
    $sch_e_date_val = $sch_e_date."-01";
    $add_where  .= " AND cc.c_date <= '{$sch_e_date_val}'";
    $smarty->assign("sch_e_date", $sch_e_date);
}

if(!empty($sch_recontact)) {
    $add_where  .= " AND cc.`is_recontact` = '{$sch_recontact}'";
    $smarty->assign("sch_recontact", $sch_recontact);
}

if(!empty($sch_hp1) || !empty($sch_hp2))
{
    if(!empty($sch_hp1) && !empty($sch_hp2)){
        $add_where  .= " AND (REPLACE(cc.hp,'-','') LIKE '%{$sch_hp1}%' OR REPLACE(cc.hp,'-','') LIKE '%{$sch_hp2}%')";
    }elseif(!empty($sch_hp1)){
        $add_where  .= " AND REPLACE(cc.hp,'-','') LIKE '%{$sch_hp1}%'";
    }elseif(!empty($sch_hp2)){
        $add_where  .= " AND REPLACE(cc.hp,'-','') LIKE '%{$sch_hp2}%'";
    }

    $smarty->assign("sch_hp1", $sch_hp1);
    $smarty->assign("sch_hp2", $sch_hp2);
}

if(!empty($sch_email)) {
    $add_where  .= " AND cc.email LIKE '%{$sch_email}%'";
    $smarty->assign("sch_email", $sch_email);
}

if(!empty($sch_brand_company)) {
    $add_where  .= " AND (cc.c_name LIKE '%{$sch_brand_company}%' OR cc.brand LIKE '%{$sch_brand_company}%')";
    $smarty->assign("sch_brand_company", $sch_brand_company);

    $chk_company_sql    = "SELECT count(c_no) as cnt FROM company c WHERE c_name LIKE '%{$sch_brand_company}%'";
    $chk_company_query  = mysqli_query($my_db, $chk_company_sql);
    $chk_company_result = mysqli_fetch_assoc($chk_company_query);
    if($chk_company_result['cnt'] > 0){
        $warning_msg = "※주의 : 유사한 파트너사가 {$chk_company_result['cnt']}개 존재합니다.";
        $smarty->assign("warning_msg", $warning_msg);
    }
}

# 전체 게시글 수
$contact_total_sql     = "SELECT count(`cc_no`) as cnt FROM `company_contact` as cc WHERE {$add_where}";
$contact_total_query   = mysqli_query($my_db, $contact_total_sql);
$contact_total_result  = mysqli_fetch_assoc($contact_total_query);
$contact_total         = isset($contact_total_result['cnt']) ? $contact_total_result['cnt'] : 0;

$page 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 20;
$num 		= $page_type;
$offset 	= ($page-1) * $num;
$page_num 	= ceil($contact_total/$num);

if ($page >= $page_num){$page = $page_num;}
if ($page <= 0){$page = 1;}

$search_url = getenv("QUERY_STRING");
$page_list  = pagelist($page, "company_contact_management.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $contact_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 증명서 발급 리스트
$contact_sql = "
    SELECT 
        *,
        (SELECT s.s_name FROM staff s WHERE s.s_no=cc.manager) as manager_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=cc.manager_team) as manager_team_name
    FROM `company_contact` as cc
    WHERE {$add_where}
    ORDER BY `cc_no` DESC
    LIMIT {$offset}, {$num}
";
$contact_query  = mysqli_query($my_db, $contact_sql);
$contact_list   = [];
$type_option    = getContactTypeOption();
$prev_date      = date("Y-m-d", strtotime("-1 years"));
while($contact_result = mysqli_fetch_assoc($contact_query))
{
    $contact_result['type_name']    = isset($type_option[$contact_result['type']]) ? $type_option[$contact_result['type']] : "";
    $contact_result['is_editable']  = (empty($contact_result['c_no']) && ($contact_result['manager'] == $session_s_no)) ? true : false;
    if(!empty($contact_result['c_date']) && empty($contact_result['c_no'])){
        $contact_result['recontact'] = ($contact_result['c_date'] <= $prev_date) ? true : false;
    }

    $contact_list[] = $contact_result;
}

$total_tel_option = array_merge(getHpOption(), getTelOption());

$smarty->assign("type_option", $type_option);
$smarty->assign("cur_date", date("Y-m-d"));
$smarty->assign("recontact_option", getReContactOption());
$smarty->assign("page_type_option", getPageTypeOption('4'));
$smarty->assign("total_tel_option", $total_tel_option);
$smarty->assign("email_option", getEmailOption());
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("session_team_name", $session_team_name);
$smarty->assign("contact_list", $contact_list);

$smarty->display('company_contact_management.html');
?>
