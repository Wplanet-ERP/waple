<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/product_cms.php');
require('inc/helper/wise_bm.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');

# 프로세스 처리
$process        = (isset($_POST['process']))?$_POST['process']:"";
$product_model  = ProductCms::Factory();

if($process == "duplicate_product")
{
    $prd_no     = (isset($_POST['prd_no']))?$_POST['prd_no']:"";
    $prd_code   = (isset($_POST['prd_code']))?$_POST['prd_code']:"";
    $search_url = (isset($_POST['search_url']))?$_POST['search_url']:"";

    $product_item   = $product_model->getItem($prd_no);
    $duplicate_data = array(
        "k_name_code"   => $product_item['k_name_code'],
        "prd_code"      => addslashes($product_item['prd_code']."_복제"),
        "priority"      => $product_item['priority'],
        "title"         => addslashes($product_item['title']."_복제"),
        "manager"       => $product_item['manager'],
        "manager_team"  => $product_item['manager_team'],
        "c_no"          => $product_item['c_no'],
        "description"   => addslashes($product_item['description']),
        "regdate"       => date("Y-m-d H:i:s"),
        "prd_memo"      => addslashes($product_item['prd_memo']),
        "prd_memo_date" => $product_item['prd_memo_date'],
        "display"       => $product_item['display'],
        "writer"        => $session_s_no,
        "writer_team"   => $session_team,
        "writer_date"   => date("Y-m-d H:i:s")
    );

    if($product_model->insert($duplicate_data))
    {
        $last_prd_no    = $product_model->getInsertId();

        if($product_model->setDuplicateRelation($last_prd_no, $prd_no)){
            exit("<script>alert('복제 되었습니다');location.href='product_cms_list.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('상품 복제 성공. relation 실패했습니다');location.href='product_cms_list.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('실패했습니다');location.href='product_cms_list.php?{$search_url}';</script>");
    }
}
elseif($process == "multi_save_display")
{
    $chk_prd_no_list    = isset($_POST['chk_prd_no_list']) ? trim($_POST['chk_prd_no_list']) : "";
    $chk_value          = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $chk_msg            = ($chk_value == "1") ? "Display ON 처리" : "Display OFF 처리";
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_sql            = "UPDATE product_cms SET display='{$chk_value}' WHERE prd_no IN({$chk_prd_no_list})";

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('{$chk_msg}에 실패했습니다');location.href='product_cms_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('{$chk_msg}에 성공했습니다');location.href='product_cms_list.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "68";
$nav_title   = "커머스 상품 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where              = "1=1";
$sch_display            = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$sch_manager            = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : $session_name;
$sch_c_name             = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_prd_code           = isset($_GET['sch_prd_code']) ? $_GET['sch_prd_code'] : "";
$sch_unit_name          = isset($_GET['sch_unit_name']) ? $_GET['sch_unit_name'] : "";
$sch_gift_date_type     = isset($_GET['sch_gift_date_type']) ? $_GET['sch_gift_date_type'] : "";
$sch_gift_date_on_off   = isset($_GET['sch_gift_date_on_off']) ? $_GET['sch_gift_date_on_off'] : "";
$sch_gift_s_date        = isset($_GET['sch_gift_s_date']) ? $_GET['sch_gift_s_date'] : "";
$sch_gift_e_date        = isset($_GET['sch_gift_e_date']) ? $_GET['sch_gift_e_date'] : "";
$sch_solo_prd           = isset($_GET['sch_solo_prd']) ? $_GET['sch_solo_prd'] : "";
$sch_combined_pack      = isset($_GET['sch_combined_pack']) ? $_GET['sch_combined_pack'] : "";
$sch_brand_search       = isset($_GET['sch_brand_search']) ? $_GET['sch_brand_search'] : "";
$sch_is_in_out          = isset($_GET['sch_is_in_out']) ? $_GET['sch_is_in_out'] : "";
$gift_type_option       = getGiftTypeOption();

#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
$url_check_str      = $_SERVER['QUERY_STRING'];
$url_chk_result     = false;
if(empty($url_check_str)){
    $url_check_sql    		= "SELECT count(*) as cnt FROM product_cms prd_cms WHERE manager in (SELECT s.s_no FROM staff s where s.s_name like '%{$session_name}%') AND display='1'";
    $url_check_query  		= mysqli_query($my_db, $url_check_sql);
    $url_check_result 		= mysqli_fetch_assoc($url_check_query);

    if($url_check_result['cnt'] == 0){
        $sch_manager    = "";
        $url_chk_result = true;
    }
}

$sch_prd_g1     = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2     = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd_name   = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_prd_type   = isset($_GET['sch_prd_type']) ? $_GET['sch_prd_type'] : "";
$kind_model     = Kind::Factory();
$cms_code       = "product_cms";
$cms_group_list = $kind_model->getKindGroupList($cms_code);
$prd_g1_list = $prd_g2_list = $sch_prd_g2_list = [];

foreach($cms_group_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}

if(!empty($sch_prd_g2) || !empty($sch_prd_g1))
{
    $sch_prd_g2_list = $prd_g2_list[$sch_prd_g1];
}

$smarty->assign("sch_prd_g1_list", $prd_g1_list);
$smarty->assign("sch_prd_g2_list", $sch_prd_g2_list);

if(!empty($sch_prd_g1)) {
    $add_where  .= " AND k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}')";
    $smarty->assign("sch_prd_g1", $sch_prd_g1);
}

if(!empty($sch_prd_g2)) {
    $add_where  .= " AND k_name_code = '{$sch_prd_g2}'";
    $smarty->assign("sch_prd_g2", $sch_prd_g2);
}

if(!empty($sch_prd_name)) {
    $add_where  .= " AND title like '%{$sch_prd_name}%'";
    $smarty->assign("sch_prd_name", $sch_prd_name);
}

if(!empty($sch_prd_type)) {
    $add_where  .= " AND prd_type = '{$sch_prd_type}'";
    $smarty->assign("sch_prd_type", $sch_prd_type);
}

if(!empty($sch_display)) {
    $add_where  .= " AND display = '{$sch_display}'";
    $smarty->assign("sch_display", $sch_display);
}

if(!empty($sch_manager)) {
    $add_where  .= " AND manager in (SELECT s.s_no FROM staff s where s.s_name like '%{$sch_manager}%')";
    $smarty->assign("sch_manager", $sch_manager);
}

if(!empty($sch_c_name)) {
    $add_where  .= " AND c_no in (SELECT c.c_no FROM company c where c.c_name like '%{$sch_c_name}%')";
    $smarty->assign("sch_c_name", $sch_c_name);
}

if(!empty($sch_prd_code)) {
    $add_where  .= " AND prd_code = '{$sch_prd_code}'";
    $smarty->assign("sch_prd_code", $sch_prd_code);
}

if(!empty($sch_unit_name)) {
    $add_where  .= " AND prd_no in (SELECT sub.prd_no FROM product_cms_relation sub WHERE sub.display='1' AND sub.option_no IN(SELECT pcu.`no` FROM product_cms_unit pcu WHERE pcu.option_name like '%{$sch_unit_name}%'))";
    $smarty->assign("sch_unit_name", $sch_unit_name);
}

if(!empty($sch_gift_date_type))
{
    $sch_gift_s_date_type = $gift_type_option[$sch_gift_date_type]['s_date'];
    $sch_gift_e_date_type = $gift_type_option[$sch_gift_date_type]['e_date'];

    if(!empty($sch_gift_date_on_off)) {
        $add_where  .= " AND `{$sch_gift_date_type}` = '{$sch_gift_date_on_off}'";
    }

    if(!empty($sch_gift_s_date)){
        $sch_gift_s_date_time = $sch_gift_s_date." 00:00:00";
        $add_where  .= " AND `{$sch_gift_s_date_type}` >= '{$sch_gift_s_date_time}'";
    }

    if(!empty($sch_gift_e_date)){
        $sch_gift_e_date_time = $sch_gift_e_date." 23:59:59";
        $add_where  .= " AND `{$sch_gift_e_date_type}` <= '{$sch_gift_e_date_time}'";
    }
}
else
{
    if(!empty($sch_gift_date_on_off)) {
        if($sch_gift_date_on_off == "1"){
            $add_where  .= " AND (gift_date_type='{$sch_gift_date_on_off}' OR sub_gift_date_type='{$sch_gift_date_on_off}')";
        }elseif($sch_gift_date_on_off == "2"){
            $add_where  .= " AND (gift_date_type='{$sch_gift_date_on_off}' AND sub_gift_date_type='{$sch_gift_date_on_off}')";
        }
    }

    if(!empty($sch_gift_s_date)){
        $sch_gift_s_date_time = $sch_gift_s_date." 00:00:00";
        $add_where  .= " AND (gift_s_date >= '{$sch_gift_s_date_time}' OR sub_gift_s_date >= '{$sch_gift_s_date_time}')";
    }

    if(!empty($sch_gift_e_date)){
        $sch_gift_e_date_time = $sch_gift_e_date." 23:59:59";
        $add_where  .= " AND (gift_e_date <= '{$sch_gift_e_date_time}' OR sub_gift_e_date <= '{$sch_gift_e_date_time}')";
    }
}
$smarty->assign("sch_gift_date_type", $sch_gift_date_type);
$smarty->assign("sch_gift_date_on_off", $sch_gift_date_on_off);
$smarty->assign("sch_gift_s_date", $sch_gift_s_date);
$smarty->assign("sch_gift_e_date", $sch_gift_e_date);

if(!empty($sch_solo_prd))
{
    if($sch_solo_prd == '1'){
        $add_where  .= " AND solo_prd != ''";
    }elseif($sch_solo_prd == '2'){
        $add_where  .= " AND (solo_prd IS NULL OR solo_prd = '')";
    }

    $smarty->assign("sch_solo_prd", $sch_solo_prd);
}

if(!empty($sch_combined_pack))
{
    if($sch_combined_pack == '1'){
        $add_where  .= " AND combined_pack = '2'";
    }elseif($sch_combined_pack == '2'){
        $add_where  .= " AND combined_pack = '1'";
    }

    $smarty->assign("sch_combined_pack", $sch_combined_pack);
}

if($sch_brand_search){
    $bm_brand_option = getBrandDetailOption();
    $bm_brand_list   = $bm_brand_option[$sch_brand_search]["brand_list"];
    $bm_brand_text   = implode(",", $bm_brand_list);

    $add_where       = "display=1 AND c_no IN({$bm_brand_text})";

    $brand_s_date   = isset($_GET['brand_s_date']) ? $_GET['brand_s_date'] : "";
    $brand_e_date   = isset($_GET['brand_e_date']) ? $_GET['brand_e_date'] : "";

    if(!empty($brand_s_date) && !empty($brand_e_date)){
        $add_where .= " AND (gift_date_type = 1";

        if($brand_s_date == "NULL"){
            $add_where .= " AND (gift_s_date IS NULL ";
        }else{
            $add_where .= " AND (gift_s_date = '{$brand_s_date}' ";
        }

        if($brand_e_date == "NULL"){
            $add_where .= " AND gift_e_date IS NULL))";
        }else{
            $add_where .= " AND gift_e_date = '{$brand_e_date}')) ";
        }
    }else{
        $add_where .= " AND gift_date_type = '2'";
    }

    if(!empty($brand_sub_s_date) && !empty($brand_sub_e_date))
    {
        $add_where .= " AND (sub_gift_date_type = 1";

        if($brand_sub_s_date == "NULL"){
            $add_where .= " AND (sub_gift_s_date IS NULL ";
        }else{
            $add_where .= " AND (sub_gift_s_date = '{$brand_sub_s_date}' ";
        }

        if($brand_sub_e_date == "NULL"){
            $add_where .= " AND sub_gift_e_date IS NULL))";
        }else{
            $add_where .= " AND sub_gift_e_date = '{$brand_sub_e_date}')) ";
        }
    }else{
        $add_where .= " AND sub_gift_date_type = '2'";
    }
}

if(!empty($sch_is_in_out) && $sch_is_in_out == "2")
{
    $add_where .= " AND prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation AS pcr WHERE pcr.option_no IN(SELECT pcu.no FROM product_cms_unit AS pcu WHERE pcu.is_in_out='2') AND pcr.display='1')";
    $smarty->assign("sch_is_in_out", $sch_is_in_out);
}

# 정렬순서 토글 & 필드 지정
$add_orderby    = "prd_no DESC";
$ord_type       = (isset($_GET['ord_type']) && !empty($_GET['ord_type'])) ? $_GET['ord_type'] : "";
$ord_type_by    = (isset($_GET['ord_type_by']) && !empty($_GET['ord_type_by'])) ? $_GET['ord_type_by'] : "";
if(!empty($ord_type))
{
    if($ord_type_by == '1'){
        $add_orderby = "{$ord_type} ASC, prd_no DESC";
    }elseif($ord_type_by == '2'){
        $add_orderby = "{$ord_type} DESC, prd_no DESC";
    }else{
        $add_orderby = "prd_no DESC";
    }
}

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);


# 전체 게시물 수 및 페이징처리
$product_cms_total_sql      = "SELECT count(prd_no) as cnt FROM product_cms prd_cms WHERE {$add_where}";
$product_cms_total_query    = mysqli_query($my_db, $product_cms_total_sql);
$product_cms_total_result   = mysqli_fetch_array($product_cms_total_query);
$product_cms_total          = $product_cms_total_result['cnt'];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum    = ceil($product_cms_total/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

if(empty($url_check_str) && $url_chk_result){
    $search_url = "sch_manager=";
}else{
    $search_url = getenv("QUERY_STRING");
}
$page_list = pagelist($pages, "product_cms_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $product_cms_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$product_cms_sql = "
    SELECT
        *,
        (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd_cms.k_name_code)) AS k_prd1,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd_cms.k_name_code)) AS k_prd1_name,
        (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=prd_cms.k_name_code) AS k_prd2,
        (SELECT k_name FROM kind k WHERE k.k_name_code=prd_cms.k_name_code) AS k_prd2_name,
        (SELECT s_name FROM staff s WHERE s.s_no=prd_cms.manager) AS manager_name,
        (SELECT team_name FROM team t WHERE t.team_code=prd_cms.manager_team) AS t_name,
        (SELECT c_name FROM company c WHERE c.c_no=prd_cms.c_no) AS c_name,
        (SELECT c_name FROM company c WHERE c.c_no=prd_cms.log_c_no) AS log_c_name,
        (SELECT s_name FROM staff s WHERE s.s_no=prd_cms.writer) AS writer_name,
        (SELECT sub.q_no FROM quick_search as sub WHERE sub.`type`='cms' AND sub.s_no='{$session_s_no}' AND sub.prd_no=prd_cms.prd_no) AS quick_prd,
        (SELECT sub.title FROM product_cms sub WHERE sub.prd_no=prd_cms.combined_product) as combined_prd_name,
        (SELECT sub.title FROM product_cms sub WHERE sub.prd_no=prd_cms.solo_prd) as solo_prd_name,
        (SELECT MAX(w.run_date) FROM work_cms_quick w WHERE w.prd_no=prd_cms.prd_no AND w.prd_type='1' AND w.quick_state='4') as quick_date
    FROM product_cms prd_cms
    WHERE {$add_where}
    ORDER BY {$add_orderby}
    LIMIT {$offset}, {$num}
";
$product_cms_query  = mysqli_query($my_db, $product_cms_sql);
$product_cms_list   = [];
$display_option     = getDisplayOption();
$prd_type_option    = getPrdTypeOption();
while($product_cms = mysqli_fetch_array($product_cms_query))
{
    if(!empty($product_cms['writer_date'])) {
        $writer_day_value = date("Y/m/d",strtotime($product_cms['writer_date']));
        $writer_time_value = date("H:i",strtotime($product_cms['writer_date']));
    }else{
        $writer_day_value = '';
        $writer_time_value = '';
    }

    $combined_txt = "";
    if($product_cms['combined_pack'] == '2'){
        $combined_txt = "합포장불가";
        if(!empty($product_cms['combined_except'])){
            $combined_txt .= "<br/>예외";
        }

        if(!empty($product_cms['combined_product'])){
            $combined_prd_txt = empty($product_cms['combined_except']) ? "<br/>(합포장 : {$product_cms['combined_product']})" : "(합포장 : {$product_cms['combined_product']})";
            $combined_txt .= $combined_prd_txt;
        }
    }

    $prd_unit_list  = [];
    $prd_unit_sql   = "SELECT CONCAT(pcu.option_name,'(',pcr.quantity,')') as prd_unit_name, pcr.prd_no FROM product_cms_relation pcr LEFT JOIN product_cms_unit pcu ON pcu.no = pcr.option_no  WHERE pcr.prd_no ='{$product_cms['prd_no']}' AND pcr.display = 1 ORDER BY pcr.priority ASC";
    $prd_unit_query = mysqli_query($my_db, $prd_unit_sql);
    while($prd_unit_result = mysqli_fetch_assoc($prd_unit_query))
    {
        $prd_unit_list[] = $prd_unit_result['prd_unit_name'];
    }

    $product_cms['prd_type_name']   = isset($prd_type_option[$product_cms['prd_type']]) ? $prd_type_option[$product_cms['prd_type']] : "";
    $product_cms['display_name']    = $display_option[$product_cms['display']];
    $product_cms['writer_day']      = $writer_day_value;
    $product_cms['writer_time']     = $writer_time_value;
    $product_cms['combined_txt']    = $combined_txt;
    $product_cms['prd_unit_list']   = $prd_unit_list;

    $product_cms_list[] = $product_cms;
}

# 입/출고 정지 구성품리스트
$product_stop_sql = "
    SELECT
        COUNT(*) as stop_cnt
    FROM product_cms AS p
    WHERE p.prd_no IN(SELECT DISTINCT prd_no FROM product_cms_relation p WHERE p.option_no IN(SELECT pcu.`no` FROM product_cms_unit AS pcu WHERE pcu.is_in_out='2') AND p.display='1')
    AND p.display='1'
";
$product_stop_query     = mysqli_query($my_db, $product_stop_sql);
$product_stop_result    = mysqli_fetch_assoc($product_stop_query);
$product_stop_cnt       = isset($product_stop_result['stop_cnt']) && !empty($product_stop_result['stop_cnt']) ? $product_stop_result['stop_cnt'] : 0;

$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("display_option", $display_option);
$smarty->assign("prd_type_option", $prd_type_option);
$smarty->assign("set_option", getSetOption());
$smarty->assign("gift_type_option", $gift_type_option);
$smarty->assign("gift_date_type_option", getGiftDateTypeOption());
$smarty->assign("product_stop_cnt", $product_stop_cnt);
$smarty->assign("product_cms_list", $product_cms_list);

$smarty->display('product_cms_list.html');



?>
