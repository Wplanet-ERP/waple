<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/helper/product_cms_stock.php');
require('inc/model/Logistics.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');
require('inc/model/Custom.php');
require('inc/model/Message.php');
require('Classes/PHPExcel.php');

# Model 설정
$logistics_model = Logistics::Factory();
$unit_model      = ProductCmsUnit::Factory();
$report_model    = ProductCmsStock::Factory();
$report_model->setStockReport();

# 입고/반출 Post
$report_file    = $_FILES["report_file"];
$log_c_no       = isset($_POST['report_warehouse']) ? $_POST['report_warehouse'] : 2809;
$tmp_log_c_no   = 2809;
$report_date    = date("Y-m-d H:i:s");
$reg_s_no       = $session_s_no;

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($report_file['name']) && !empty($report_file['name'])){
    $upload_file_path = add_store_file($report_file, "upload_management");
    $upload_file_name = $report_file['name'];
}

$upload_data  = array(
    "upload_type"   => 2,
    "upload_kind"   => $log_c_no,
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $session_s_no,
    "regdate"       => $report_date,
);
$upload_model->insert($upload_data);

$file_no         = $upload_model->getInsertId();
$excel_file_path = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;

# 엑셀 파일 처리
$excelReader    = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($excel_file_path);
$excel->setActiveSheetIndex(0);
$objWorksheet   = $excel->getActiveSheet();
$totalRow       = $objWorksheet->getHighestRow();

# 입고알림
$message_sql        = "select `no`, (SELECT s_name FROM staff WHERE s_no=ord_s_no) as s_name, (SELECT hp FROM staff WHERE s_no=ord_s_no) as hp from product_cms_unit WHERE display='1'";
$message_query      = mysqli_query($my_db, $message_sql);
$message_list       = [];
$message_name_list  = [];
$message_count_list = [];
while($message_result = mysqli_fetch_assoc($message_query)){
    $message_list[$message_result['hp']][] = $message_result['no'];
    $message_name_list[$message_result['hp']]  = $message_result['s_name'];
    $message_count_list[$message_result['hp']] = 0;
}

# 엑셀 처리
$confirm_state_option = getConfirmStateMatchOption();
$report_list        = [];
$stock_chk_list     = [];

if($log_c_no == '2809')
{
    $type_cell          = $client_cell = $doc_no_cell = $memo_cell = $priority_cell = $state_cell = $subs_state_cell = $is_confirm_cell = "";
    $brand_cell         = $prd_kind_cell = $prd_code_cell = $prd_name_cell = "";
    $good_stock_cell    = $good_qty_cell = $bad_stock_cell = $bad_qty_cell = $stock_center_cell = $total_qty_cell = "";
    $eng_abc            = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

    for ($i = 2; $i <= $totalRow; $i++)
    {
        if($i == "2"){
            $highestColumn  = $objWorksheet->getHighestColumn();
            $titleRowData   = $objWorksheet->rangeToArray("A{$i}:".$highestColumn.$i, NULL, TRUE, FALSE);

            foreach ($titleRowData[0] as $key => $title)
            {
                switch($title){
                    case "입고/반출":
                        $type_cell = $eng_abc[$key];
                        break;
                    case "거래처":
                        $client_cell = $eng_abc[$key];
                        break;
                    case "전표번호":
                        $doc_no_cell = $eng_abc[$key];
                        break;
                    case "메모":
                        $memo_cell = $eng_abc[$key];
                        break;
                    case "순번":
                        $priority_cell = $eng_abc[$key];
                        break;
                    case "입고형태":
                        $state_cell = $eng_abc[$key];
                        break;
                    case "가입고구분":
                        $subs_state_cell = $eng_abc[$key];
                        break;
                    case "확정여부":
                        $is_confirm_cell = $eng_abc[$key];
                        break;
                    case "브랜드":
                        $brand_cell = $eng_abc[$key];
                        break;
                    case "대표코드":
                        $prd_kind_cell = $eng_abc[$key];
                        break;
                    case "상품코드":
                        $prd_code_cell = $eng_abc[$key];
                        break;
                    case "상품명":
                        $prd_name_cell = $eng_abc[$key];
                        break;
                    case "정상창고":
                        $good_stock_cell = $eng_abc[$key];
                        break;
                    case "정상수량":
                        $good_qty_cell = $eng_abc[$key];
                        break;
                    case "불량창고":
                        $bad_stock_cell= $eng_abc[$key];
                        break;
                    case "물류센터":
                        $stock_center_cell = $eng_abc[$key];
                        break;
                    case "불량수량":
                        $bad_qty_cell = $eng_abc[$key];
                        break;
                    case "총수량":
                        $total_qty_cell = $eng_abc[$key];
                        break;
                }
            }
            continue;
        }

        if(
            empty($type_cell) || empty($client_cell) || empty($doc_no_cell) || empty($memo_cell) || empty($priority_cell) || empty($state_cell)
            || empty($subs_state_cell) || empty($is_confirm_cell) || empty($brand_cell) || empty($prd_kind_cell) || empty($prd_code_cell) || empty($prd_name_cell)
            || empty($good_stock_cell) || empty($good_qty_cell) || empty($bad_stock_cell) || empty($bad_qty_cell) || empty($stock_center_cell) || empty($total_qty_cell)
        )
        {
            echo "변환되지 않는 필드 값이 있습니다. 개발팀 임태형에게 문의해주세요!";
            exit;
        }

        $stock_type_list    = [];
        $log_doc_no         = "";
        $log_subject        = 0;

        $regdate_val        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue())); //일자
        $type_val           = (string)trim(addslashes($objWorksheet->getCell("{$type_cell}{$i}")->getValue())); //입고/반출
        $client             = (string)trim(addslashes($objWorksheet->getCell("{$client_cell}{$i}")->getValue())); //거래처
        $doc_no             = (string)trim(addslashes($objWorksheet->getCell("{$doc_no_cell}{$i}")->getValue())); //전표번호
        $memo               = (string)trim(addslashes($objWorksheet->getCell("{$memo_cell}{$i}")->getValue())); //메모
        $priority_val       = (string)trim(addslashes($objWorksheet->getCell("{$priority_cell}{$i}")->getValue())); //순번
        $state              = (string)trim(addslashes($objWorksheet->getCell("{$state_cell}{$i}")->getValue())); //입고형태
        $subs_state         = (string)trim(addslashes($objWorksheet->getCell("{$subs_state_cell}{$i}")->getValue())); //가입고구분
        $is_confirm         = (string)trim(addslashes($objWorksheet->getCell("{$is_confirm_cell}{$i}")->getValue())); //확정여부
        $brand              = (string)trim(addslashes($objWorksheet->getCell("{$brand_cell}{$i}")->getValue())); //브랜드
        $prd_kind           = (string)trim(addslashes($objWorksheet->getCell("{$prd_kind_cell}{$i}")->getValue())); //대표코드
        $prd_code           = (string)trim(addslashes($objWorksheet->getCell("{$prd_code_cell}{$i}")->getValue())); //상품코드
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("{$prd_name_cell}{$i}")->getValue())); //상품명
        $good_stock_type    = (string)trim(addslashes($objWorksheet->getCell("{$good_stock_cell}{$i}")->getValue())); //정상창고
        $good_qty           = (string)trim(addslashes($objWorksheet->getCell("{$good_qty_cell}{$i}")->getValue())); //정상수량
        $bad_stock_type     = (string)trim(addslashes($objWorksheet->getCell("{$bad_stock_cell}{$i}")->getValue())); //불량창고
        $stock_center       = (string)trim(addslashes($objWorksheet->getCell("{$stock_center_cell}{$i}")->getValue())); //물류센터
        $bad_qty            = (string)trim(addslashes($objWorksheet->getCell("{$bad_qty_cell}{$i}")->getValue())); //불량수량
        $total_qty          = (string)trim(addslashes($objWorksheet->getCell("{$total_qty_cell}{$i}")->getValue())); //총수량

        if(empty($regdate_val) || $regdate_val == '합계'){
            break;
        }

        $prd_unit       = 0;
        $sup_c_no       = 0;
        $type           = 0;
        $regdate        = "";
        $option_name    = "";
        $is_order       = 2;
        $priority       = !empty($priority_val) ? $priority_val : 1;
        $confirm_state  = isset($confirm_state_option[$state]) ? $confirm_state_option[$state] : 0;
        $unit_item      = $unit_model->getSkuItem($prd_code, $tmp_log_c_no);

        if(!$unit_item['prd_unit'])
        {
            echo "Row : {$i}, 재고 반영에 실패하였습니다.<br>확인되지 않은 상품코드(SKU)가 있습니다.<br>
            일자     : {$regdate_val}<br>
            입/출고  : {$type_val}<br>
            순번     : {$priority_val}<br>
            상품코드 : {$prd_code}<br>
            상품명   : {$prd_name}<br>
            ";
            exit;
        }else{
            $prd_unit       = $unit_item['prd_unit'];
            $sup_c_no       = $unit_item['sup_c_no'];
            $option_name    = $unit_item['option_name'];
        }

        // 일치하는 입/출고 타입이 아닌 경우
        if(!!$type_val)
        {
            if($type_val == "입고") {
                $type = 1;
            }elseif($type_val == "반출") {
                $type = 2;

                if($state == "내부이동" && $confirm_state == '3'){
                    $confirm_state = 6;
                }elseif($client == "쿠팡_제트배송" && $state == "기타반출"){
                    $confirm_state = 6;
                }elseif($client == "쿠팡_로켓배송" && $state == "기타반출"){
                    $confirm_state = 8;
                }

            }elseif($type_val == "택배") {
                $type = 3;
            } else {
                echo $i;
                echo "Row : {$i}, 재고 반영에 실패하였습니다.<br>정상적인 입고/반출 타입이 아닙니다.<br>
                    일자     : {$regdate_val}<br>
                    입/출고  : {$type_val}<br>
                    순번     : {$priority_val}<br>
                    상품코드 : {$prd_code}<br>
                    상품명   : {$prd_name}<br>
                ";
                exit;
            }
        }

        // 일치하는 입고 형태가 아닌 경우
        if(empty($state))
        {
            echo "Row : {$i},  재고 반영에 실패하였습니다.<br>입고/반출형태가 없습니다.<br>
                일자     : {$regdate_val}<br>
                입/출고  : {$type_val}<br>
                입고형태 : {$state}<br>
                순번     : {$priority_val}<br>
                상품코드 : {$prd_code}<br>
                상품명   : {$prd_name}<br>
            ";
            exit;
        }

        if($confirm_state == 0){
            echo "Row : {$i},  재고 반영에 실패하였습니다.<br>확정 입고/반출형태가 없습니다.<br>
                일자     : {$regdate_val}<br>
                입/출고  : {$type_val}<br>
                입고형태 : {$state}<br>
                순번     : {$priority_val}<br>
                상품코드 : {$prd_code}<br>
                상품명   : {$prd_name}<br>
            ";
            exit;
        }

        if(!$regdate_val)
        {
            echo "Row : {$i}, 재고 반영에 실패하였습니다.<br>일자가 없습니다 확인하세요.<br>
                일자     : {$regdate_val}<br>
                입/출고  : {$type_val}<br>
                순번     : {$priority_val}<br>
                상품코드 : {$prd_code}<br>
                상품명   : {$prd_name}<br>
            ";
            exit;
        }else{
            $regdate = date("Y-m-d", strtotime($regdate_val));
        }

        if(!$priority)
        {
            echo "Row : {$i}, 재고 반영에 실패하였습니다.<br>순번이 없습니다 확인하세요.<br>
                일자     : {$regdate_val}<br>
                입/출고  : {$type_val}<br>
                순번     : {$priority_val}<br>
                상품코드 : {$prd_code}<br>
                상품명   : {$prd_name}<br>
            ";
            exit;
        }

        if($good_qty != 0 && $bad_qty != 0) {
            $stock_type_list[] = array("stock_type" => $good_stock_type, "stock_qty" => $good_qty);
            $stock_type_list[] = array("stock_type" => $bad_stock_type, "stock_qty" => $bad_qty);
        }elseif($good_qty != 0){
            $stock_type_list[] = array("stock_type" => $good_stock_type, "stock_qty" => $good_qty);
        }elseif($bad_qty != 0){
            $stock_type_list[] = array("stock_type" => $bad_stock_type, "stock_qty" => $bad_qty);
        }

        $log_doc_no     = "";
        $log_subject    = "";
        if($memo)
        {
            if(strpos($memo, '$입고') !== false){
                $is_order = 1;
            }

            if(strpos($memo, '쿠팡(제트배송)') !== false){
                $confirm_state = "6";
            }elseif(strpos($memo, '쿠팡(로켓배송)') !== false){
                $confirm_state = "9";
            }

            if(strpos($memo, '$문서번호=') !== false)
            {
                $memo_list = explode('$문서번호=', $memo);

                if(isset($memo_list[1]) && !empty($memo_list[1]))
                {
                    $memo_last_list = explode("$", $memo_list[1]);
                    $log_doc_no     = isset($memo_last_list[0]) ? $memo_last_list[0] : "";
                    $logistics      = $logistics_model->getLogisticsByDoc($log_doc_no);
                    $log_subject    = $logistics['subject'];
                }
            }
        }

        $report_data = array(
            'file_no'       => $file_no,
            'regdate'       => $regdate,
            'report_type'   => "1",
            'type'          => $type,
            'client'        => $client,
            'doc_no'        => $doc_no,
            'order'         => $priority,
            'is_order'      => $is_order,
            'state'         => $state,
            'subs_state'    => $subs_state,
            'is_confirm'    => $is_confirm,
            'confirm_state' => $confirm_state,
            'brand'         => $brand,
            'prd_kind'      => $prd_kind,
            'prd_unit'      => $prd_unit,
            'sup_c_no'      => $sup_c_no,
            'log_c_no'      => $tmp_log_c_no,
            'option_name'   => $option_name,
            'sku'           => $prd_code,
            'prd_name'      => $prd_name,
            'stock_center'  => $stock_center,
            'memo'          => $memo,
            'log_doc_no'    => $log_doc_no,
            'log_subject'   => $log_subject,
            'reg_s_no'      => $reg_s_no,
            'report_date'   => $report_date,
        );

        if(!empty($stock_type_list))
        {
            foreach($stock_type_list as $stock_type_data)
            {
                $tmp_report_data = $report_data;
                $stock_type      = $stock_type_data['stock_type'];
                $stock_qty       = $stock_type_data['stock_qty'];
                if(!!$regdate)
                {
                    $stock_sql   = "SELECT count(no) FROM product_cms_stock_report WHERE prd_unit='{$prd_unit}' AND `order`='{$priority_val}' AND regdate ='{$regdate}' AND `type`='{$type}' AND stock_type='{$stock_type}' AND stock_qty='{$stock_qty}' AND stock_date IS NULL AND log_doc_no='{$log_doc_no}'";
                    $stock_query = mysqli_query($my_db, $stock_sql);
                    $stock_data  = mysqli_fetch_array($stock_query);

                    if($stock_data[0] > 0)
                    {
                        echo $stock_sql."<br/>";
                        echo "Row : {$i}, 재고 반영에 실패하였습니다.<br>기존에 업로드한 내역이 있는지 확인하세요.<br>
                        일자     : {$regdate}<br>
                        입/출고  : {$type_val}<br>
                        순번     : {$priority_val}<br>
                        상품코드 : {$prd_code}<br>
                        상품명   : {$prd_name}<br>
                    ";
                        exit;
                    }

                    $tmp_report_data['stock_type'] = $stock_type;
                    $tmp_report_data['stock_qty'] = $stock_qty;

                    $report_list[] = $tmp_report_data;
                }
            }
        }

        if($is_order == '1')
        {
            $stock_chk_list[$prd_unit][$regdate][] = array(
                'pcu_name'  => $prd_code,
                'pcu_qty'   => $good_qty
            );
        }
    }
}
elseif($log_c_no == '1113')
{
    for ($i = 2; $i <= $totalRow; $i++)
    {
        $regdate_val        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue())); //일자
        $type_val           = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue())); //입고/반출
        $state              = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue())); //입고/반출형태
        $confirm_state_val  = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue())); //확정 입고/반출형태
        $confirm_state      = isset($confirm_state_option[$state]) ? $confirm_state_option[$state] : 0;
        $option_name        = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue())); //SKU
        $good_stock_type    = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue())); //정상창고
        $good_qty           = !empty($objWorksheet->getCell("G{$i}")->getValue()) ? trim(addslashes($objWorksheet->getCell("G{$i}")->getValue())) : 0; //정상수량
        $bad_stock_type     = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue())); //불량창고
        $bad_qty            = !empty($objWorksheet->getCell("I{$i}")->getValue()) ? trim(addslashes($objWorksheet->getCell("I{$i}")->getValue())) : 0; //불량수량
        $memo               = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue())); //메모
        $stock_type_list    = [];

        if(empty($option_name)){
            continue;
        }

        $prd_unit = 0;
        $sup_c_no = 0;
        $type     = 0;
        $regdate  = $brand = $sku = "";
        $unit_item= $unit_model->getNameItem($option_name, $tmp_log_c_no);

        if(!$unit_item['no'])
        {
            echo "Row : {$i}, 재고 반영에 실패하였습니다.<br>확인되지 않은 상품코드(SKU)가 있습니다.<br>
            일자     : {$regdate_val}<br>
            입/출고  : {$type}<br>
            구성품명  : {$option_name}<br>
            ";
            exit;
        }else{
            $prd_unit = $unit_item['no'];
            $sup_c_no = $unit_item['sup_c_no'];
            $brand    = $unit_item['brand_name'];
            $sku      = $unit_item['sku'];
        }

        // 일치하는 입/출고 타입이 아닌 경우
        if($type_val == "입고") {
            $type = 1;
        }elseif($type_val == "반출") {
            $type = 2;
        }else {
            echo $i;
            echo "Row : {$i}, 재고 반영에 실패하였습니다.<br>정상적인 입고/반출 타입이 아닙니다.<br>
            일자     : {$regdate_val}<br>
            입/출고  : {$type}<br>
            구성품명  : {$option_name}<br>
            ";
            exit;
        }

        // 일치하는 입고 형태가 아닌 경우
        if(empty($state))
        {
            echo "Row : {$i},  재고 반영에 실패하였습니다.<br>입고형태가 없습니다.<br>
            일자     : {$regdate_val}<br>
            입/출고  : {$type}<br>
            구성품명  : {$option_name}<br>
            입고형태 : {$state}<br>
            ";
            exit;
        }

        if(!$regdate_val)
        {
            echo "Row : {$i}, 재고 반영에 실패하였습니다.<br>일자가 없습니다 확인하세요.<br>
            일자     : {$regdate_val}<br>
            입/출고  : {$type}<br>
            구성품명  : {$option_name}<br>
            ";
            exit;
        }else{
            $regdate = date("Y-m-d", strtotime($regdate_val));
        }

        $report_data = array(
            'file_no'       => $file_no,
            'regdate'       => $regdate,
            'report_type'   => "1",
            'type'          => $type,
            'state'         => $state,
            'confirm_state' => $confirm_state,
            'brand'         => $brand,
            'prd_kind'      => $brand,
            'prd_unit'      => $prd_unit,
            'sup_c_no'      => $sup_c_no,
            'log_c_no'      => $log_c_no,
            'option_name'   => $option_name,
            'sku'           => $sku,
            'memo'          => $memo,
            'reg_s_no'      => $reg_s_no,
            'report_date'   => $report_date,
        );

        // 동일한 입/출고 내역이 있는 경우
        if(!!$regdate)
        {
            $stock_sql   = "SELECT count(no) FROM product_cms_stock_report WHERE prd_unit='{$prd_unit}' AND regdate ='{$regdate}' AND `type`='{$type}' AND stock_date IS NULL AND log_c_no='{$log_c_no}'";
            $stock_query = mysqli_query($my_db, $stock_sql);
            $stock_data  = mysqli_fetch_array($stock_query);

            if($stock_data[0] > 0){
                echo $stock_sql."<br/>";
                echo "Row : {$i}, 재고 반영에 실패하였습니다.<br>기존에 업로드한 내역이 있는지 확인하세요.<br>
            일자     : {$regdate}<br>
            입/출고  : {$type}<br>
            구성품명  : {$option_name}<br>
            ";
                exit;
            }
        }

        if($good_qty != 0 && $bad_qty != 0) {
            $stock_type_list[] = array("stock_type" => $good_stock_type, "stock_qty" => $good_qty);
            $stock_type_list[] = array("stock_type" => $bad_stock_type, "stock_qty" => $bad_qty);
        }elseif($good_qty != 0){
            $stock_type_list[] = array("stock_type" => $good_stock_type, "stock_qty" => $good_qty);
        }elseif($bad_qty != 0){
            $stock_type_list[] = array("stock_type" => $bad_stock_type, "stock_qty" => $bad_qty);
        }

        if(!empty($stock_type_list))
        {
            foreach ($stock_type_list as $stock_type_data)
            {
                $tmp_report_data = $report_data;
                $stock_type      = $stock_type_data['stock_type'];
                $stock_qty       = $stock_type_data['stock_qty'];
                if(!!$regdate)
                {
                    $stock_sql   = "SELECT count(no) FROM product_cms_stock_report WHERE prd_unit='{$prd_unit}' AND regdate ='{$regdate}' AND `type`='{$type}' AND stock_type='{$stock_type}' AND stock_qty='{$stock_qty}' AND stock_date IS NULL";
                    $stock_query = mysqli_query($my_db, $stock_sql);
                    $stock_data  = mysqli_fetch_array($stock_query);

                    if($stock_data[0] > 0)
                    {
                        echo $stock_sql."<br/>";
                        echo "Row : {$i}, 재고 반영에 실패하였습니다.<br>기존에 업로드한 내역이 있는지 확인하세요.<br>
                        일자     : {$regdate}<br>
                        입/출고  : {$type_val}<br>
                        구성품명  : {$option_name}<br>
                    ";
                        exit;
                    }

                    $tmp_report_data['stock_type'] = $stock_type;
                    $tmp_report_data['stock_qty'] = $stock_qty;

                    $report_list[] = $tmp_report_data;
                }
            }
        }
    }
}
elseif($log_c_no == '5711' || $log_c_no == '5795' || $log_c_no == '5885')
{
    $csv_file_path          = fopen($excel_file_path, "r");
    $file_idx               = 0;
    $amazon_c_no            = $log_c_no;
    $amazon_c_name          = "아마존_닥터피엘(미국)";
    $amazon_warehouse       = "아마존_닥터피엘(미국) 창고";

    switch ($amazon_c_no){
        case "5711":
            $amazon_c_name    = "아마존_닥터피엘(미국)";
            $amazon_warehouse = "아마존_닥터피엘(미국) 창고";
            break;
        case "5795":
            $amazon_c_name    = "아마존_아이레놀(일본)";
            $amazon_warehouse = "아마존_아이레놀(일본) 창고";
            break;
        case "5885":
            $amazon_c_name    = "아마존_아이레놀(미국)";
            $amazon_warehouse = "아마존_아이레놀(미국) 창고";
            break;
    }
    $amazon_state_option    = getAmazonStateOption();
    $ins_tmp_report_list    = [];

    while ($csv_file_data = fgetcsv($csv_file_path, 2048, ","))
    {
        $file_idx++;

        if($file_idx < 1){
            continue;
        }

        $regdate_val= $csv_file_data['0'];
        $regdate    = date("Y-m-d", strtotime($regdate_val));
        $sku        = $csv_file_data['3'];
        $prd_name   = $csv_file_data['4'];
        $event_type = $csv_file_data['5'];
        $qty        = $csv_file_data['7'];

        $state = $confirm_state = $type = $qty_type = "";

        if($qty > 0){
            $type = 1;
        }elseif($qty < 0){
            $type = 2;
        }else{
            continue;
        }

        if(empty($event_type)){
            echo "{$event_type} 처리하지 않는는 Event Type 입니다";
            exit;
        }

        if(!empty($event_type) && !empty($type)) {
            $amazon_state_data  = $amazon_state_option[$type][$event_type];
            $state              = isset($amazon_state_data['state']) ? $amazon_state_data['state'] : "";
            $confirm_state      = isset($amazon_state_data['confirm_state']) ? $amazon_state_data['confirm_state'] : "";
        }

        $chk_unit_item = $unit_model->getSkuItem($sku, $amazon_c_no);

        if(empty($chk_unit_item)){
            echo "{$sku} 등록되지 않는 SKU 입니다";
            exit;
        }

        if(!isset($ins_tmp_report_list[$sku][$confirm_state]))
        {
            $ins_tmp_report_list[$sku][$confirm_state] = array(
                'file_no'       => $file_no,
                'regdate'       => $regdate,
                'report_type'   => "1",
                'type'          => $type,
                'state'         => $state,
                'confirm_state' => $confirm_state,
                'brand'         => $chk_unit_item['brand_name'],
                'prd_kind'      => ($chk_unit_item['type'] == '1') ? "상품" : "부속품",
                'prd_name'      => $prd_name,
                'prd_unit'      => $chk_unit_item['prd_unit'],
                'sup_c_no'      => $chk_unit_item['sup_c_no'],
                'log_c_no'      => $amazon_c_no,
                'option_name'   => $chk_unit_item['option_name'],
                'sku'           => $sku,
                'memo'          => "{$amazon_c_name} 입고/반출",
                'stock_type'    => $amazon_warehouse,
                'stock_qty'     => 0,
                'reg_s_no'      => $reg_s_no,
                'report_date'   => $report_date,
            );
        }

        $ins_tmp_report_list[$sku][$confirm_state]['stock_qty'] += $qty;
    }

    if(!empty($ins_tmp_report_list))
    {
        foreach($ins_tmp_report_list as $report_sku => $report_sku_data) {
            foreach($report_sku_data as $report_state => $report_state_data) {
                $report_list[] = $report_state_data;
            }
        }
    }
}

$message_chk_list   = [];
if(!empty($message_list))
{
    foreach ($message_list as $pcu_hp => $pcu_data)
    {
        if(!empty($pcu_hp))
        {
            $stock_content_list = [];

            foreach($pcu_data as $pcu_no)
            {
                if(isset($stock_chk_list[$pcu_no]))
                {
                    $stock_data = $stock_chk_list[$pcu_no];

                    if(!empty($stock_data))
                    {
                        foreach($stock_data as $st_date => $st_data)
                        {
                            foreach($st_data as $data){
                                $stock_content_list[$st_date][] = "{$data['pcu_name']} +".number_format($data['pcu_qty']);

                                $message_count_list[$pcu_hp]++;
                            }
                        }
                    }
                }
            }

            if(!empty($stock_content_list))
            {
                $str_content_imp = "";
                foreach($stock_content_list as $str_date => $st_content){
                    $str_content_imp .= "\r\n[{$str_date}]\r\n".implode("\r\n", $st_content);

                }
                $message_chk_list[$pcu_hp] = $str_content_imp;
            }
        }
    }
}

if (!$report_model->multiInsert($report_list)){
    echo "재고 반영에 실패하였습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영";
    exit;
}else{
    if(!empty($message_chk_list))
    {
        $cm_id          = 1;
        $cur_datetime	= date("YmdHis");
        $sms_title      = "재고알림 입니다";
        $send_name      = "와이즈플래닛";
        $send_phone     = "070-7873-1373";
        $c_info         = "8"; # 재고관리
        $send_data_list = [];

        foreach($message_chk_list as $pcu_hp => $msg_content)
        {
            $msg_id 	    = "W{$cur_datetime}".sprintf('%04d', $cm_id++);
            $receiver       = $message_name_list[$pcu_hp];
            $receiver_hp    = $pcu_hp;
            $msg_count      = isset($message_count_list[$pcu_hp]) ? $message_count_list[$pcu_hp] : 0;
            $content        = "[입고알림]\r\n'{$message_name_list[$pcu_hp]}'님 물류창고에 발주 입고 '{$msg_count}건'이 확인되었습니다.\r\n입고 된 제품을 발주서에 꼭 등록해주세요.\r\n등록 시 개당 입고단가 필수입력, 유통기한 선택입력\r\n";
            $link           = "발주서 등록 바로가기 : https://work.wplanet.co.kr/v1/commerce_order.php";
            $content       .= $msg_content."\r\n\r\n".$link;
            $sms_msg        = addslashes($content);
            $sms_msg_len    = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
            $msg_type       = ($sms_msg_len > 90) ? "L" : "S";

            $send_data_list[$msg_id] = array(
                "msg_id"        => $msg_id,
                "msg_type"      => $msg_type,
                "sender"        => $send_name,
                "sender_hp"     => $send_phone,
                "receiver"      => $receiver,
                "receiver_hp"   => $receiver_hp,
                "title"         => $sms_title,
                "content"       => $sms_msg,
                "cinfo"         => $c_info,
            );
        }

        if(!empty($send_data_list)){
            $message_model = Message::Factory();
            $message_model->sendMessage($send_data_list);
        }
    }

    exit("<script>alert('재고가 반영 되었습니다.');location.href='product_cms_stock_report_list.php?sch_file_no={$file_no}';</script>");
}

?>
