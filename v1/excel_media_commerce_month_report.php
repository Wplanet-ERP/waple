<?php
ini_set('display_errors', -1);

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/commerce_sales.php');
require('inc/helper/work_cms.php');
require('inc/helper/wise_bm.php');
require('inc/model/CommerceSales.php');
require('inc/model/Kind.php');
require('Classes/PHPExcel.php');

$brand_option = array(
    "1314"  => "닥터피엘",
    "5513"  => "닥터피엘(해외)",
    "3386"  => "닥터피엘 큐빙",
    "5283"  => "닥터피엘 올뉴큐빙",
    "2863"  => "닥터피엘 퓨어팟",
    "3303"  => "닥터피엘 에코호스",
    "5434"  => "닥터피엘 여행용",
    "4140"  => "닥터피엘 인텐시브 칫솔",
    "4445"  => "닥터피엘 코스트제로 정수기",
    "4446"  => "닥터피엘 에코 피처형 정수기",
    "5812"  => "아토샤워헤드",
    "2388"  => "아이레놀",
    "4440"  => "아이레놀R 페이스업리프터",
    "4809"  => "아이레놀(개기름컨트롤러)",
    "5759"  => "아이레놀 쌩얼보정 선크림",
    "5514"  => "아이레놀(해외)",
    "2827"  => "누잠매트리스",
    "4878"  => "누잠 더블업토퍼",
    "5201"  => "누잠 여름침구",
    "5642"  => "누잠 겨울침구",
    "5810"  => "누잠 바디필로우",
    "5415"  => "누잠 슬립미스트",
    "6026"  => "누잠 베개",
    "6060"  => "누잠 여름패드",
    "4333"  => "시티파이",
    "5447"  => "시티파이(스칼프랩)",
    "2402"  => "프라이웰",
    "4378"  => "닥터부헌",
    "5493"  => "목카스",
);
$commerce_fee_option        = getCommerceFeeOption();
$dp_except_list             = getNotApplyDpList();
$dp_except_text             = implode(",", $dp_except_list);
$dp_self_imweb_list         = getSelfDpImwebCompanyList();
$global_dp_all_list         = getGlobalDpCompanyList();
$global_dp_total_list       = $global_dp_all_list['total'];
$global_dp_ilenol_list      = $global_dp_all_list['ilenol'];
$doc_brand_list             = getTotalDocBrandList();
$doc_brand_text             = implode(",", $doc_brand_list);
$ilenol_brand_list          = getIlenolBrandList();
$ilenol_brand_text          = implode(",", $ilenol_brand_list);
$ilenol_global_dp_text      = implode(",", $global_dp_ilenol_list);

$cost_kind_sql      = "SELECT k_name_code, k_name, (SELECT sub.k_name FROM kind sub WHERE sub.k_name_code=k.k_parent) as k_parent_name FROM kind k WHERE k_code='cost' AND k_parent IS NOT NULL ORDER BY k_parent, priority ASC, k_name ASC";
$cost_kind_query    = mysqli_query($my_db, $cost_kind_sql);
$commerce_cost_list = [];
while($cost_kind = mysqli_fetch_assoc($cost_kind_query))
{
    $commerce_cost_list[$cost_kind['k_name_code']] = $cost_kind;
}

$sales_kind_sql      = "SELECT k_name_code, k_name, (SELECT sub.k_name FROM kind sub WHERE sub.k_name_code=k.k_parent) as k_parent_name FROM kind k WHERE k_code='sales' AND k_parent IS NOT NULL ORDER BY k_parent, priority ASC, k_name ASC";
$sales_kind_query    = mysqli_query($my_db, $sales_kind_sql);
$commerce_sales_list = [];
while($sales_kind = mysqli_fetch_assoc($sales_kind_query))
{
    $commerce_sales_list[$sales_kind['k_name_code']] = $sales_kind;
}

$dp_company_sql     = "SELECT c_no, IF(display='1', c_name, CONCAT(c_name,'[OFF]')) as c_name FROM company WHERE dp_e_type='1' AND c_no NOT IN({$dp_except_text}) ORDER BY priority, c_no ASC";
$dp_company_query   = mysqli_query($my_db, $dp_company_sql);
$dp_company_list    = [];
while($dp_company = mysqli_fetch_assoc($dp_company_query))
{
    $dp_company_list[$dp_company['c_no']] = $dp_company['c_name'];
}

# 날짜 컬럼
$sch_base_month     = isset($_GET['sch_base_month']) ? $_GET['sch_base_month'] : date("Y-m");
$base_month_title   = date("Ym", strtotime("{$sch_base_month}"));
$sch_base_e_day     = date("t", strtotime("{$sch_base_month}"));
$sch_base_s_date    = $sch_base_month."-01";
$sch_base_e_date    = $sch_base_month."-".$sch_base_e_day;
$sch_s_datetime     = $sch_base_s_date." 00:00:00";
$sch_e_datetime     = $sch_base_e_date." 23:59:59";

$all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_base_s_date}' AND '{$sch_base_e_date}'";
$all_date_title     = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";
$add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_base_s_date}' AND '{$sch_base_e_date}' ";

$add_cms_where      = "order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
$add_cms_column     = "DATE_FORMAT(order_date, '%Y%m%d')";
$sch_net_date       = $sch_base_s_date;

# 전체 리스트 Init
$commerce_date_list = [];
$all_date_sql       = "
	SELECT
 		DATE_FORMAT(`allday`.Date, '%Y-%m-%d') as chart_title,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as chart_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($date = mysqli_fetch_array($all_date_query))
{
    $commerce_date_list[$date['chart_key']] = $date['chart_title'];
}

# 필드
$eng_abc  = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
$eng_abcc = [];
$eng_list = $eng_abc;
foreach($eng_abc as $a){
    foreach($eng_abc as $b){
        $eng_list[] = $a.$b;
        $eng_abcc[] = $a.$b;
    }
}

foreach($eng_abcc as $aa){
    foreach($eng_abc as $b){
        $eng_list[] = $aa.$b;
    }
}

# Excel 생성
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$fontBlackColor = new PHPExcel_Style_Color();
$fontBlackColor->setRGB('0000000');
$background_color = "00262626";


$excel_sheet_idx        = 0;
foreach($brand_option as $brand => $brand_name)
{
    if($excel_sheet_idx > 0){
        $objPHPExcel->createSheet($excel_sheet_idx);
    }

    $cms_report_list                = [];
    $sales_report_list              = [];
    $cost_report_list               = [];
    $coupon_imweb_report_total_list = [];
    $coupon_smart_report_total_list = [];
    $cms_delivery_free_list         = [];
    $cms_delivery_free_total_list   = [];
    $sales_report_total_list        = [];
    $cost_report_total_list         = [];
    $net_report_date_list           = [];
    $cms_report_subtotal_sum_list   = [];

    foreach($commerce_date_list as $date_key => $date_title)
    {
        foreach($commerce_cost_list as $comm_g2 => $comm_g2_data)
        {
            $cost_report_list[$comm_g2][$date_key] = 0;
        }

        foreach($commerce_sales_list as $comm_g2 => $comm_g2_data)
        {
            $sales_report_list[$comm_g2][$date_key] = 0;
        }

        foreach($dp_company_list as $cms_key => $cms_title)
        {
            $cms_report_list[$cms_key][$date_key]['subtotal']  = 0;
            $cms_report_list[$cms_key][$date_key]['delivery']  = 0;
            $cms_delivery_free_list[$cms_key][$date_key]       = 0;
        }

        $coupon_imweb_report_total_list[$date_key]  = array("total" => 0, "belabef" => 0, "doc" => 0, "ilenol" => 0, "nuzam" => 0);
        $coupon_smart_report_total_list[$date_key]  = 0;
        $cms_report_subtotal_sum_list[$date_key]    = 0;

        $sales_report_total_list[$date_key]    = 0;
        $cost_report_total_list[$date_key]     = 0;
        $net_report_date_list[$date_key]       = 0;
    }

    $brand_sql          = "SELECT (SELECT sub.k_name FROM kind sub WHERE sub.k_name_code=(SELECT k.k_parent FROM kind k WHERE k.k_name_code=c.brand)) as brand_g1_name, (SELECT k.k_name FROM kind k WHERE k.k_name_code=c.brand) as brand_g2_name, c_name as brand_name FROM company c WHERE c.c_no='{$brand}'";
    $brand_query        = mysqli_query($my_db, $brand_sql);
    $brand_result       = mysqli_fetch_assoc($brand_query);
    $brand_data         = $brand_result;
    $last_filed         = "";

    # 수기 입력 데이터
    $commerce_sql  = "
        SELECT
            cr.`type`,
            cr.price as price,
            cr.k_name_code as comm_g2,
            DATE_FORMAT(sales_date, '%Y%m%d') as sales_date
        FROM commerce_report `cr`
        WHERE `cr`.display='1' AND `cr`.brand = '{$brand}'
            AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_base_s_date}' AND '{$sch_base_e_date}'
        ORDER BY sales_date ASC
    ";
    $commerce_query = mysqli_query($my_db, $commerce_sql);
    while($commerce = mysqli_fetch_assoc($commerce_query))
    {
        if($commerce['type'] == 'sales'){
            $sales_report_list[$commerce['comm_g2']][$commerce['sales_date']] += $commerce['price'];
            $sales_report_total_list[$commerce['sales_date']] += $commerce['price'];
        }elseif($commerce['type'] == 'cost'){
            $cost_report_list[$commerce['comm_g2']][$commerce['sales_date']]  += $commerce['price'];
            $cost_report_total_list[$commerce['sales_date']]  += $commerce['price'];
        }
    }

    $add_cms_tmp_where = $add_cms_where;

    if(in_array($brand, $doc_brand_list)){
        $add_cms_where  .= " AND `w`.c_no = '{$brand}' AND `w`.log_c_no != '5956'";
    }
    elseif(in_array($brand, $ilenol_brand_list)){
        $add_cms_where  .= " AND `w`.c_no = '{$brand}' AND `w`.log_c_no != '5659'";
    }
    elseif($brand == "5513"){
        $add_cms_where  .= " AND `w`.c_no IN({$doc_brand_text}) AND `w`.log_c_no = '5956'";
    }
    elseif($brand == "5514"){
        $add_cms_where  .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.log_c_no = '5659' AND `w`.dp_c_no NOT IN({$ilenol_global_dp_text})";
    }
    elseif($brand == "5979"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.c_no != '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
    }
    elseif($brand == "6044"){
        $add_cms_where      .= " AND `w`.c_no = '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
    }
    else{
        $add_cms_where  .= " AND `w`.c_no = '{$brand}'";
    }

    # 매출자동 데이터
    $cms_report_sql      = "
        SELECT 
            dp_c_no, 
            dp_c_name, 
            order_number,
            DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
            unit_price,
            (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
            unit_delivery_price,
            (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
        FROM work_cms as w
        WHERE {$add_cms_tmp_where} AND delivery_state='4' AND dp_c_no NOT IN({$dp_except_text})
    ";
    $cms_report_query = mysqli_query($my_db, $cms_report_sql);
    $cms_delivery_chk_fee_list   = [];
    $cms_delivery_chk_nuzam_list = [];
    $cms_delivery_chk_list       = [];
    $cms_delivery_chk_list       = [];
    $commerce_fee_total_list     = isset($commerce_fee_option[$brand]) ? $commerce_fee_option[$brand] : [];
    while($cms_report_result = mysqli_fetch_assoc($cms_report_query))
    {
        $commerce_fee_per_list      = !empty($commerce_fee_total_list) && isset($commerce_fee_total_list[$cms_report_result['dp_c_no']]) ? $commerce_fee_total_list[$cms_report_result['dp_c_no']] : [];
        $commerce_fee_per           = 0;

        if(!empty($commerce_fee_per_list))
        {
            foreach($commerce_fee_per_list as $fee_date => $fee_val){
                if($cms_report_result['sales_date'] >= $fee_date){
                    $commerce_fee_per = $fee_val;
                }
            }
        }

        $cms_report_fee      = $cms_report_result['unit_price']*($commerce_fee_per/100);
        $total_price         = $cms_report_result['unit_price'] - $cms_report_fee;

        $cms_report_list[$cms_report_result['dp_c_no']][$cms_report_result['sales_date']]['subtotal'] += $total_price;
        $cms_report_list[$cms_report_result['dp_c_no']][$cms_report_result['sales_date']]['delivery'] += $cms_report_result['unit_delivery_price'];

        $sales_report_total_list[$cms_report_result['sales_date']]      += $total_price;
        $cms_report_subtotal_sum_list[$cms_report_result['sales_date']] += $total_price;

        if(in_array($cms_report_result['dp_c_no'], $dp_self_imweb_list))
        {
            if($cms_report_result['dp_c_no'] == '1372'){
                $coupon_imweb_report_total_list[$cms_report_result['sales_date']]['belabef'] += $cms_report_result['coupon_price'];
            }
            elseif($cms_report_result['dp_c_no'] == '5800'){
                $coupon_imweb_report_total_list[$cms_report_result['sales_date']]['doc'] += $cms_report_result['coupon_price'];
            }
            elseif($cms_report_result['dp_c_no'] == '5958'){
                $coupon_imweb_report_total_list[$cms_report_result['sales_date']]['ilenol'] += $cms_report_result['coupon_price'];
            }
            elseif($cms_report_result['dp_c_no'] == '6012'){
                $coupon_imweb_report_total_list[$cms_report_result['sales_date']]['nuzam'] += $cms_report_result['coupon_price'];
            }

            $coupon_imweb_report_total_list[$cms_report_result['sales_date']]['total'] += $cms_report_result['coupon_price'];
            $cost_report_total_list[$cms_report_result['sales_date']] += $cms_report_result['coupon_price'];
        }

        if($cms_report_result['dp_c_no'] == '3295' || $cms_report_result['dp_c_no'] == '4629' || $cms_report_result['dp_c_no'] == '5427' || $cms_report_result['dp_c_no'] == '5588'){
            $coupon_smart_report_total_list[$cms_report_result['sales_date']] += $cms_report_result['coupon_price'];
        }

        if($cms_report_result['sales_date'] >= 20240101)
        {
            if ($cms_report_result['unit_delivery_price'] > 0) {
                $cms_delivery_chk_fee_list[$cms_report_result['order_number']] = 1;
            } elseif ($cms_report_result['unit_delivery_price'] == 0 && $cms_report_result['unit_price'] > 0) {
                $cms_delivery_chk_list[$cms_report_result['order_number']][$cms_report_result['dp_c_no']][$cms_report_result['sales_date']] = $cms_report_result['deli_cnt'];
            }
        }
    }

    foreach($cms_delivery_chk_list as $chk_ord => $chk_ord_data)
    {
        if(isset($cms_delivery_chk_fee_list[$chk_ord])){
            continue;
        }

        $chk_delivery_price = ($brand == '2827' || $brand == '4878') ? 5000 : 3000;

        foreach ($chk_ord_data as $chk_dp_c_no => $chk_dp_data)
        {
            if(in_array($chk_dp_c_no, $global_dp_total_list)){
                $chk_delivery_price = 6000;
            }

            foreach ($chk_dp_data as $chk_sales_date => $deli_cnt)
            {
                $cal_delivery_price = $chk_delivery_price * $deli_cnt;
                $cms_delivery_free_list[$chk_dp_c_no][$chk_sales_date]  += $cal_delivery_price;
                $cost_report_total_list[$chk_sales_date]                += $cal_delivery_price;
            }
        }
    }

    $net_sales_sum_list     = [];
    $net_cost_sum_list      = [];
    $expected_sales_list    = [];
    $roas_percent_list      = [];

    foreach($cost_report_total_list as $sales_date => $cost_report_total)
    {
        $sales_total        = $sales_report_total_list[$sales_date];
        $cost_total         = $cost_report_total;
        $expected_sales     = ($sales_total > 0) ? ($cost_total/$sales_total)*100 : 0;
        $roas_percent       = ($cost_total > 0) ? ($sales_total/$cost_total)*100 : 0;

        $net_sales_sum_list[$sales_date]  = $sales_total;
        $net_cost_sum_list[$sales_date]   = $cost_total;
        $expected_sales_list[$sales_date] = $expected_sales;
        $roas_percent_list[$sales_date]   = $roas_percent;
    }

    $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$brand}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$sch_net_date}' ORDER BY sales_date DESC LIMIT 1;";
    $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
    $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
    $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
    $net_parent             = isset($net_pre_percent_result['sales_date']) ? $net_pre_percent_result['sales_date'] : 0;

    $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$brand}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_base_s_date}' AND '{$sch_base_e_date}' ORDER BY sales_date ASC";
    $net_report_query       = mysqli_query($my_db, $net_report_sql);
    while($net_report = mysqli_fetch_assoc($net_report_query))
    {
        $net_report_date_list[$net_report['sales_date']] = $net_report['percent'];
    }

    $net_report_list            = [];
    $net_report_cal_list        = [];
    $net_report_profit_list     = [];
    $net_report_profit_per_list = [];

    foreach($net_report_date_list as $sales_date => $net_percent_val)
    {
        if($net_percent_val > 0 ){
            $net_pre_percent = $net_percent_val;
        }

        $net_percent = $net_pre_percent/100;
        $sales_total = round($sales_report_total_list[$sales_date],0);
        $cost_total  = $cost_report_total_list[$sales_date];
        $net_price   = $sales_total > 0 ? (int)($sales_total*$net_percent) : 0;
        $profit      = $cost_total > 0 ? ($net_price-$cost_total) : $net_price;

        $net_report_cal_list[$sales_date]    += (int)$net_price;
        $net_report_profit_list[$sales_date] += $profit;

        $net_report_list[$sales_date] = array('percent' => $net_pre_percent, 'parent' => ($net_percent_val > 0) ? 0 : $net_parent);
    }

    foreach($net_report_profit_list as $sales_date => $net_profit)
    {
        $sales_total_sum = $net_sales_sum_list[$sales_date];
        if($sales_total_sum > 0){
            $net_report_profit_per_list[$sales_date] = round($net_profit/$sales_total_sum * 100, 2);
        }else{
            $net_report_profit_per_list[$sales_date] = 0;
        }
    }

    $excel_work_sheet   = $objPHPExcel->setActiveSheetIndex($excel_sheet_idx);

    $excel_work_sheet->getColumnDimension("A")->setWidth(20);
    $excel_work_sheet->getColumnDimension("B")->setWidth(40);
    $excel_work_sheet->getColumnDimension("C")->setWidth(20);
    $excel_work_sheet->getColumnDimension("D")->setWidth(20);
    $excel_work_sheet->getColumnDimension("E")->setWidth(40);

    # 비용(자동)
    $excel_work_sheet->mergeCells("A1:E1");
    $excel_work_sheet->setCellValue("A1", "비용(자동)");
    $excel_work_sheet->getRowDimension("1")->setRowHeight(30);
    $excel_work_sheet->getStyle("A1:B1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
    $excel_work_sheet->getStyle("A1:B1")->getFont()->setColor($fontColor);
    $excel_work_sheet->getStyle("A1:B1")->getFont()->setBold(true);

    $base_eng_idx = 5;
    foreach($commerce_date_list as $date_label)
    {
        $excel_work_sheet->setCellValue("{$eng_list[$base_eng_idx]}1", $date_label);
        $excel_work_sheet->getColumnDimension("{$eng_list[$base_eng_idx]}")->setWidth(20);
        $last_filed = $eng_list[$base_eng_idx];
        $base_eng_idx++;
    }
    $base_eng_idx--;
    $excel_work_sheet->getStyle("F1:{$eng_list[$base_eng_idx]}1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
    $excel_work_sheet->getStyle("F1:{$eng_list[$base_eng_idx]}1")->getFont()->setColor($fontColor);
    $excel_work_sheet->getStyle("F1:{$eng_list[$base_eng_idx]}1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $excel_work_sheet->getStyle("F1:{$eng_list[$base_eng_idx]}1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $row_idx        = 2;
    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "쿠폰");
    $excel_work_sheet->setCellValue("E{$row_idx}", "아임웹_베라베프(쿠폰)");
    $coupon_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $excel_work_sheet->setCellValue("{$eng_list[$coupon_eng_idx]}{$row_idx}", number_format($coupon_imweb_report_total_list[$comm_date]['belabef']));
        $coupon_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "쿠폰");
    $excel_work_sheet->setCellValue("E{$row_idx}", "아임웹_닥터피엘(쿠폰)");
    $coupon_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $excel_work_sheet->setCellValue("{$eng_list[$coupon_eng_idx]}{$row_idx}", number_format($coupon_imweb_report_total_list[$comm_date]["doc"]));
        $coupon_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "쿠폰");
    $excel_work_sheet->setCellValue("E{$row_idx}", "아임웹_아이레놀(쿠폰)");
    $coupon_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $excel_work_sheet->setCellValue("{$eng_list[$coupon_eng_idx]}{$row_idx}", number_format($coupon_imweb_report_total_list[$comm_date]["ilenol"]));
        $coupon_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "쿠폰");
    $excel_work_sheet->setCellValue("E{$row_idx}", "아임웹_누잠(쿠폰)");
    $coupon_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $excel_work_sheet->setCellValue("{$eng_list[$coupon_eng_idx]}{$row_idx}", number_format($coupon_imweb_report_total_list[$comm_date]["nuzam"]));
        $coupon_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "쿠폰");
    $excel_work_sheet->setCellValue("E{$row_idx}", "스마트스토어(쿠폰)");
    $coupon_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $excel_work_sheet->setCellValue("{$eng_list[$coupon_eng_idx]}{$row_idx}", number_format($coupon_smart_report_total_list[$comm_date]));
        $coupon_eng_idx++;
    }
    $row_idx++;

    foreach($dp_company_list as $dp_c_no => $dp_c_name)
    {
        $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
        $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
        $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
        $excel_work_sheet->setCellValue("D{$row_idx}", "무료배송비");
        $excel_work_sheet->setCellValue("E{$row_idx}", $dp_c_name);

        $dp_eng_idx = 5;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $excel_work_sheet->setCellValue("{$eng_list[$dp_eng_idx]}{$row_idx}", number_format($cms_delivery_free_list[$dp_c_no][$comm_date]));
            $dp_eng_idx++;
        }
        $row_idx++;
    }

    $excel_work_sheet->mergeCells("A{$row_idx}:E{$row_idx}");
    $excel_work_sheet->setCellValue("A{$row_idx}", "비용(수기)");
    $excel_work_sheet->getRowDimension($row_idx)->setRowHeight(30);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFont()->setColor($fontColor);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFont()->setBold(true);
    $row_idx++;

    foreach($commerce_cost_list as $cost_key => $cost_kind)
    {
        $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
        $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
        $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
        $excel_work_sheet->setCellValue("D{$row_idx}", $cost_kind['k_parent_name']);
        $excel_work_sheet->setCellValue("E{$row_idx}", $cost_kind['k_name']);

        $cost_eng_idx = 5;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $excel_work_sheet->setCellValue("{$eng_list[$cost_eng_idx]}{$row_idx}", number_format($cost_report_list[$cost_key][$comm_date]));
            $cost_eng_idx++;
        }
        $row_idx++;
    }

    $excel_work_sheet->mergeCells("A{$row_idx}:E{$row_idx}");
    $excel_work_sheet->setCellValue("A{$row_idx}", "매출(자동)");
    $excel_work_sheet->getRowDimension($row_idx)->setRowHeight(30);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFont()->setColor($fontColor);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFont()->setBold(true);
    $row_idx++;

    foreach($dp_company_list as $dp_key => $dp_c_name)
    {
        $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
        $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
        $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
        $excel_work_sheet->setCellValue("D{$row_idx}", "매출");
        $excel_work_sheet->setCellValue("E{$row_idx}", $dp_c_name);

        $dp_eng_idx = 5;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $excel_work_sheet->setCellValue("{$eng_list[$dp_eng_idx]}{$row_idx}", number_format($cms_report_list[$dp_key][$comm_date]['subtotal']));
            $dp_eng_idx++;
        }
        $row_idx++;

        $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
        $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
        $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
        $excel_work_sheet->setCellValue("D{$row_idx}", "매출");
        $excel_work_sheet->setCellValue("E{$row_idx}", $dp_c_name."(배송비)");
        $dp_eng_idx = 5;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $excel_work_sheet->setCellValue("{$eng_list[$dp_eng_idx]}{$row_idx}", number_format($cms_report_list[$dp_key][$comm_date]['delivery']));
            $dp_eng_idx++;
        }
        $row_idx++;
    }

    $excel_work_sheet->mergeCells("A{$row_idx}:E{$row_idx}");
    $excel_work_sheet->setCellValue("A{$row_idx}", "매출(수기)");
    $excel_work_sheet->getRowDimension($row_idx)->setRowHeight(30);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFont()->setColor($fontColor);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFont()->setBold(true);
    $row_idx++;

    $sales_eng_idx = 5;
    foreach($commerce_sales_list as $sales_key => $sales_kind)
    {
        $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
        $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
        $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
        $excel_work_sheet->setCellValue("D{$row_idx}", $sales_kind['k_parent_name']);
        $excel_work_sheet->setCellValue("E{$row_idx}", $sales_kind['k_name']);

        $sales_eng_idx = 5;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $excel_work_sheet->setCellValue("{$eng_list[$sales_eng_idx]}{$row_idx}", number_format($sales_report_list[$sales_key][$comm_date]));
            $sales_eng_idx++;
        }
        $row_idx++;
    }

    $excel_work_sheet->mergeCells("A{$row_idx}:E{$row_idx}");
    $excel_work_sheet->setCellValue("A{$row_idx}", "Total");
    $excel_work_sheet->getRowDimension($row_idx)->setRowHeight(30);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFont()->setColor($fontColor);
    $excel_work_sheet->getStyle("A{$row_idx}:E{$row_idx}")->getFont()->setBold(true);
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "Total");
    $excel_work_sheet->setCellValue("E{$row_idx}", "지출 총 합계");
    $data_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $excel_work_sheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", number_format($net_cost_sum_list[$comm_date]));
        $data_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "Total");
    $excel_work_sheet->setCellValue("E{$row_idx}", "상품 매출 총 합계(수수료 제외)");
    $data_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $excel_work_sheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", number_format($net_sales_sum_list[$comm_date]));
        $data_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "Total");
    $excel_work_sheet->setCellValue("E{$row_idx}", "NET({$brand_data['brand_name']})");
    $data_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $net_percent = empty($net_report_list[$comm_date]['percent']) ? 0 : $net_report_list[$comm_date]['percent'];
        $excel_work_sheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $net_percent."%");
        $data_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "Total");
    $excel_work_sheet->setCellValue("E{$row_idx}", "NET(매출)");
    $data_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $excel_work_sheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", number_format($net_report_cal_list[$comm_date]));
        $data_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "Total");
    $excel_work_sheet->setCellValue("E{$row_idx}", "예산지출비용");
    $data_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $expected_sales = round($expected_sales_list[$comm_date]);
        $excel_work_sheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $expected_sales."%");
        $data_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "Total");
    $excel_work_sheet->setCellValue("E{$row_idx}", "ROAS");
    $data_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $roas = round($roas_percent_list[$comm_date]);
        $excel_work_sheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $roas."%");
        $data_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "Total");
    $excel_work_sheet->setCellValue("E{$row_idx}", "이익률");
    $data_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $profit_per = round($net_report_profit_per_list[$comm_date]);
        $excel_work_sheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $profit_per."%");
        $data_eng_idx++;
    }
    $row_idx++;

    $excel_work_sheet->setCellValue("A{$row_idx}", $brand_data['brand_g1_name']);
    $excel_work_sheet->setCellValue("B{$row_idx}", $brand_data['brand_g2_name']);
    $excel_work_sheet->setCellValue("C{$row_idx}", $brand_data['brand_name']);
    $excel_work_sheet->setCellValue("D{$row_idx}", "Total");
    $excel_work_sheet->setCellValue("E{$row_idx}", "차액 (공헌이익)");
    $data_eng_idx = 5;
    foreach($commerce_date_list as $comm_date => $date_label)
    {
        $excel_work_sheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", number_format($net_report_profit_list[$comm_date]));
        $data_eng_idx++;
    }

    $excel_work_sheet->getStyle("E2:{$last_filed}{$row_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $excel_work_sheet->getStyle("A1:E{$row_idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $excel_work_sheet->setTitle($brand_data['brand_name']);

    $excel_sheet_idx++;
}


$excel_filename=iconv('UTF-8','EUC-KR',$base_month_title."_커머스매출현황.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

?>
