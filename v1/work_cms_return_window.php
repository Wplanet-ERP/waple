<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/helper/work_cms.php');
require('inc/helper/work_return.php');
require('inc/model/Kind.php');
require('inc/model/Company.php');
require('inc/model/Work.php');
require('inc/model/WorkCms.php');
require('inc/model/WorkCmsReturn.php');
require('inc/model/Staff.php');
require('inc/model/Deposit.php');

$proc       = (isset($_POST['process'])) ? $_POST['process'] : "";
$cms_model  = WorkCms::Factory();
$rma_model  = WorkCmsReturn::Factory();

/////////////////////////* 자동 저장 처리 부분 *//////////////////////
if($proc == "new_delivery")
{
    $btn_type           = isset($_POST['btn_type']) ? $_POST['btn_type'] : "";
    $w_nos              = isset($_POST['w_nos']) ? $_POST['w_nos'] : "";
    $w_types            = isset($_POST['w_types']) ? $_POST['w_types'] : "";
    $prev_ord_no        = isset($_POST['prev_ord_number']) ? $_POST['prev_ord_number'] : "";
    $ord_date           = isset($_POST['new_ord_date']) ? $_POST['new_ord_date'] : "";
    $recipient          = isset($_POST['new_recipient']) ? $_POST['new_recipient'] : "";
    $recipient_hp       = isset($_POST['new_recipient_hp']) ? $_POST['new_recipient_hp'] : "";
    $zip_code           = isset($_POST['new_zipcode']) ? $_POST['new_zipcode'] : "";
    $recipient_addr1    = isset($_POST['new_recipient_addr1']) ? $_POST['new_recipient_addr1'] : "";
    $recipient_addr2    = isset($_POST['new_recipient_addr2']) ? $_POST['new_recipient_addr2'] : "";
    $recipient_addr     = addslashes($recipient_addr1." ".$recipient_addr2);
    $delivery_memo      = isset($_POST['new_delivery_memo']) ? addslashes($_POST['new_delivery_memo']) : "";
    $dp_c_no            = isset($_POST['new_dp_c_no']) ? $_POST['new_dp_c_no'] : "";
    $dp_c_name          = "";
    $manager_memo       = isset($_POST['new_manager_memo']) ? addslashes($_POST['new_manager_memo']) : "";
    $regdate            = date('Y-m-d H:i:s');
    $task_req_s_no      = isset($_POST['task_req_s_no']) ? $_POST['task_req_s_no'] : $session_s_no;
    $prd_no             = isset($_POST['new_prd_no']) ? $_POST['new_prd_no'] : "";
    $prev_shop_ord_no   = isset($_POST['new_shop_ord_no']) ? $_POST['new_shop_ord_no'] : "";
    $notices            = isset($_POST['new_notice']) ? $_POST['new_notice'] : "";
    $quantities         = isset($_POST['new_quantity']) ? $_POST['new_quantity'] : "";
    $prices             = isset($_POST['new_price']) ? $_POST['new_price'] : "";
    $prd_list           = [];
    $result             = false;
    $msg                = "등록에 실패했습니다";

    $staff_model        = Staff::Factory();
    $task_req_staff     = $staff_model->getItem($task_req_s_no);
    $prev_order_item    = $cms_model->getOrderItem($prev_ord_no);
    $ord_log_c_no       = ($prev_order_item['log_c_no'] == 5484) ? "2809" : $prev_order_item['log_c_no'];

    $cur_date           = date('Ymd');
    $last_cs_ord_no     = $cms_model->getLastCsNo();
    $ord_no             = $cur_date."_CS_".sprintf('%04d', $last_cs_ord_no+1);

    if($dp_c_no){
        $company_model  = Company::Factory();
        $dp_list        = $company_model->getDpList();
        $dp_c_name      = isset($dp_list[$dp_c_no]) ? $dp_list[$dp_c_no] : "";
    }

    $init_insert_data = array(
        "delivery_state"        => 10,
        "order_number"          => $ord_no,
        "stock_date"            => $ord_date,
        "order_date"            => $ord_date,
        "zip_code"              => $zip_code,
        "recipient"             => $recipient,
        "recipient_addr"        => $recipient_addr,
        "recipient_hp"          => $recipient_hp,
        "delivery_memo"         => $delivery_memo,
        "log_c_no"              => $ord_log_c_no,
        "dp_c_no"               => $dp_c_no,
        "dp_c_name"             => $dp_c_name,
        "manager_memo"          => $manager_memo,
        "regdate"               => $ord_date,
        "write_date"            => $regdate,
        "task_req_s_no"         => $task_req_s_no,
        "task_req_team"         => $task_req_staff['team'],
        "task_run_s_no"         => $session_s_no,
        "task_run_regdate"      => $regdate,
        "task_run_team"         => $session_team,
        "parent_order_number"   => $prev_ord_no
    );

    $new_deli_add_deposit   = isset($_POST['new_deli_add_deposit']) ? $_POST['new_deli_add_deposit'] : "";
    $new_deposit_name       = isset($_POST['new_deposit_name']) ? addslashes(trim($_POST['new_deposit_name'])) : "";
    $new_deposit_price      = isset($_POST['new_deposit_price']) ? str_replace(',','',$_POST['new_deposit_price']) : "";
    $chk_total_price        = 0;

    // 상품 정리
    $first_brand        = "";
    $first_brand_name   = "";
    $first_brand_staff  = "";
    $first_brand_team   = "";
    $ins_sql_list       = [];

    if(!empty($prd_no))
    {
        for($i=0; $i<count($prd_no); $i++)
        {
            if(isset($prd_no[$i]) && !empty($prd_no[$i]))
            {
                $quantity       = isset($quantities[$i]) ? $quantities[$i] : 1;
                $price          = isset($prices[$i]) ? str_replace(",","",$prices[$i]) : 0;
                $notice         = isset($notices[$i]) ? $notices[$i] : '';
                $parent_shop_no = isset($prev_shop_ord_no[$i]) ? $prev_shop_ord_no[$i] : "";
                $prd_list[] = array(
                    'prd_no'        => $prd_no[$i],
                    'quantity'      => $quantity,
                    'price'         => $price,
                    'notice'        => addslashes($notice),
                    'parent_shop_no'=> $parent_shop_no
                );

                $chk_total_price += $price;
            }
        }

        if($prd_list)
        {
            $chk_prd_price  = $new_deposit_price;
            $chk_prd_cnt    = count($prd_list);
            $prd_idx        = 1;

            foreach($prd_list as $prd_data)
            {
                $p_no            = $prd_data['prd_no'];
                $new_insert_data = $init_insert_data;

                if($p_no)
                {
                    $prd_sql            = "SELECT prd_cms.manager, (SELECT s.team FROM staff s WHERE s.s_no = prd_cms.manager) as team, prd_cms.c_no, (SELECT c.c_name FROM company c WHERE c.c_no = prd_cms.c_no) as c_name FROM product_cms prd_cms WHERE prd_cms.prd_no = '{$p_no}' ORDER BY prd_cms.prd_no DESC LIMIT 1";
                    $prd_query          = mysqli_query($my_db, $prd_sql);
                    $prd_result         = mysqli_fetch_assoc($prd_query);
                    $s_no               = isset($prd_result['manager']) ? $prd_result['manager'] : 0;
                    $team               = isset($prd_result['team']) ? $prd_result['team'] : 0;
                    $c_no               = isset($prd_result['c_no']) ? $prd_result['c_no'] : 0;
                    $c_name             = isset($prd_result['c_name']) ? $prd_result['c_name'] : "";
                    $quantity           = $prd_data['quantity'];
                    $notice             = $prd_data['notice'];
                    $parent_shop_ord_no = $prd_data['parent_shop_no'];
                    $new_shop_no        = "W".$ord_no."_".$p_no."_".$prd_idx;

                    if($prd_idx == '1'){
                        $first_brand        = $c_no;
                        $first_brand_name   = $c_name;
                        $first_brand_staff  = $s_no;
                        $first_brand_team   = $team;
                    }

                    if($chk_total_price > 0)
                    {
                        $cal_prd_per    = $prd_data['price']/$chk_total_price;
                        $cal_price      = round($new_deposit_price*$cal_prd_per, -1);
                        $chk_prd_price -= $cal_price;

                        if($chk_prd_cnt == $prd_idx){
                            $cal_price += $chk_prd_price;
                        }

                        $dp_price       = (int)$cal_price / 1.1;
                        $dp_price       = round($dp_price, -1);
                        $dp_price_vat   = (int)$cal_price;
                    }
                    else{
                        $dp_price_vat = 0;
                        $dp_price     = 0;
                    }

                    $new_insert_data['unit_price']          = $dp_price_vat;
                    $new_insert_data['dp_price']            = $dp_price;
                    $new_insert_data['dp_price_vat']        = $dp_price_vat;
                    $new_insert_data['shop_ord_no']         = $new_shop_no;
                    $new_insert_data['parent_shop_ord_no']  = $parent_shop_ord_no;
                    $new_insert_data['s_no']                = $s_no;
                    $new_insert_data['team']                = $team;
                    $new_insert_data['c_no']                = $c_no;
                    $new_insert_data['c_name']              = $c_name;
                    $new_insert_data['prd_no']              = $p_no;
                    $new_insert_data['quantity']            = $quantity;
                    $new_insert_data['notice']              = $notice;

                    $ins_sql_list[] = $new_insert_data;

                    $prd_idx++;
                }
            }
        }

        if($ins_sql_list)
        {
            if($cms_model->multiInsert($ins_sql_list)){
                $result = true;
                $msg    = "등록에 성공했습니다";
            }else{
                exit("<script>alert('등록 실패했습니다');history.back();</script>");
            }
        }
    }

    if($result)
    {
        if($new_deli_add_deposit == '1')
        {
            $new_task_req       = "주문번호 : {$ord_no}\r\n입금자명 : {$new_deposit_name}\r\n금액(택배비 포함가) : {$new_deposit_price}";
            $work_dp_c_no       = "4026";
            $work_dp_c_name     = "커머스 고객";
            $work_prd_no        = "260";

            $work_model  = Work::Factory();
            $work_data   = array(
                "work_state"        => 6,
                "c_no"              => $first_brand,
                "c_name"            => $first_brand_name,
                "s_no"              => $first_brand_staff,
                "team"              => $first_brand_team,
                "prd_no"            => $work_prd_no,
                "quantity"          => "1",
                "regdate"           => date('Y-m-d H:i:s'),
                "dp_price"          => $new_deposit_price,
                "dp_price_vat"      => $new_deposit_price,
                "dp_c_no"           => $work_dp_c_no,
                "dp_c_name"         => $work_dp_c_name,
                "task_req"          => addslashes(trim($new_task_req)),
                "task_req_s_no"     => $session_s_no,
                "task_req_team"     => $session_team,
                "task_run_s_no"     => $session_s_no,
                "task_run_team"     => $session_team,
                "task_run_regdate"  => $regdate,
                "linked_no"         => 0,
                "linked_table"      => "work_cms",
                "linked_shop_no"    => $ord_no
            );

            if ($work_model->insert($work_data))
            {
                $new_w_no           = $work_model->getInsertId();
                $new_deposit_chk    = isset($_POST['new_deposit_chk']) ? addslashes(trim($_POST['new_deposit_chk'])) : "3";
                $dp_tax_state       = ($new_deposit_chk == "3") ? "4" : "1";

                $deposit_model      = Deposit::Factory();
                $deposit_data       = array(
                    "my_c_no"         => '3',
                    "dp_subject"      => '커머스 입금',
                    "dp_state"        => '1',
                    "dp_method"       => '2',
                    "dp_account"      => '2',
                    "bk_name"         => $new_deposit_name,
                    "w_no"            => $new_w_no,
                    "c_no"            => $work_dp_c_no,
                    "c_name"          => $work_dp_c_name,
                    "s_no"            => $session_s_no,
                    "team"            => $session_team,
                    "supply_cost"     => $new_deposit_price,
                    "vat_choice"      => '3',
                    "supply_cost_vat" => '0',
                    "cost"            => $new_deposit_price,
                    "regdate"         => date('Y-m-d H:i:s'),
                    "dp_tax_state"    => $dp_tax_state,
                    "reg_s_no"        => $session_s_no,
                    "reg_team"        => $session_team
                );

                if($new_deposit_chk == '1')
                {
                    $new_deposit_chk_name   = isset($_POST['new_deposit_chk_name']) ? trim($_POST['new_deposit_chk_name']) : "";
                    $new_deposit_chk_num    = isset($_POST['new_deposit_chk_num']) ? trim($_POST['new_deposit_chk_num']) : "";
                    $new_deposit_cost_info  = "[커머스 고객 계산서 발행정보]\r\n성함 : {$new_deposit_chk_name}\r\n휴대전화 또는 사업자번호 : {$new_deposit_chk_num}";

                    $deposit_data['cost_info'] = addslashes(trim($new_deposit_cost_info));
                }
                elseif($new_deposit_chk == '2')
                {
                    $new_deposit_chk_email  = isset($_POST['new_deposit_chk_email']) ? trim($_POST['new_deposit_chk_email']) : "";
                    $new_deposit_cost_info  = "[커머스 고객 계산서 발행정보]\r\n이메일 : {$new_deposit_chk_email}";

                    $new_deposit_chk_file   = $_FILES['new_deposit_chk_file'];
                    if($new_deposit_chk_file)
                    {
                        if(isset($new_deposit_chk_file['name']) && !empty($new_deposit_chk_file['name'])){
                            $file_read          = add_store_file($new_deposit_chk_file, "deposit");
                            $file_origin        = $new_deposit_chk_file['name'];

                            $new_deposit_cost_info      .= "\r\n(파일첨부에 저장)";
                            $deposit_data['file_read']   = $file_read;
                            $deposit_data['file_origin'] = addslashes(trim($file_origin));
                        }
                    }
                    $deposit_data['cost_info'] = addslashes(trim($new_deposit_cost_info));
                }

                if ($deposit_model->insert($deposit_data))
                {
                    $new_dp_no = $deposit_model->getInsertId();
                    $work_model->update(array("w_no" => $new_w_no, 'dp_no' => $new_dp_no));

                    $cms_model->updateToArray(array('is_deposit' => 1), array("order_number" => $ord_no));
                }
            }
        }

        if($btn_type == 'is_delivery'){
            exit("<script>alert('{$msg}');opener.parent.location.reload();window.close();</script>");
        }else{
            exit("<script>alert('{$msg}');location.href='work_cms_return_window.php?w_no={$w_nos}&w_type={$w_types}&type={$btn_type}';</script>");
        }
    }else{
        exit("<script>alert('{$msg}');history.back();</script>");
    }
}
elseif($proc == "new_return")
{
    $btn_type           = isset($_POST['btn_type']) ? $_POST['btn_type'] : "";
    $prev_ord_no        = isset($_POST['prev_order_number']) ? $_POST['prev_order_number'] : "";
    $req_date           = isset($_POST['new_req_date']) ? $_POST['new_req_date'] : "";
    $dp_c_no            = isset($_POST['new_dp_c_no']) ? $_POST['new_dp_c_no'] : "";
    $dp_c_name          = isset($_POST['new_dp_c_name']) ? $_POST['new_dp_c_name'] : "";
    $recipient          = isset($_POST['new_recipient']) ? $_POST['new_recipient'] : "";
    $recipient_hp       = isset($_POST['new_recipient_hp']) ? $_POST['new_recipient_hp'] : "";
    $zip_code           = isset($_POST['new_zipcode']) ? $_POST['new_zipcode'] : "";
    $recipient_addr1    = isset($_POST['new_recipient_addr1']) ? $_POST['new_recipient_addr1'] : "";
    $recipient_addr2    = isset($_POST['new_recipient_addr2']) ? $_POST['new_recipient_addr2'] : "";
    $recipient_addr     = addslashes($recipient_addr1." ".$recipient_addr2);
    $req_memo           = isset($_POST['new_req_memo']) ? addslashes($_POST['new_req_memo']) : "";
    $cus_memo           = isset($_POST['new_cus_memo']) ? addslashes($_POST['new_cus_memo']) : "";
    $req_s_no           = isset($_POST['new_req_s_no']) ? $_POST['new_req_s_no'] : "";
    $req_team           = isset($_POST['new_req_team']) ? $_POST['new_req_team'] : "";
    $prd_no             = isset($_POST['new_prd_no']) ? $_POST['new_prd_no'] : "";
    $quantities         = isset($_POST['new_quantity']) ? $_POST['new_quantity'] : "";
    $prev_shop_ord_no   = isset($_POST['new_shop_ord_no']) ? $_POST['new_shop_ord_no'] : "";
    $return_purpose     = isset($_POST['new_return_purpose']) ? $_POST['new_return_purpose'] : "";
    $return_reason      = isset($_POST['new_return_reason']) ? $_POST['new_return_reason'] : "";
    $return_type        = isset($_POST['new_return_type']) ? $_POST['new_return_type'] : "";
    $new_bad_reason     = isset($_POST['new_bad_reason']) ? $_POST['new_bad_reason'] : "";
    $regdate            = date('Y-m-d H:i:s');
    $result             = false;
    $msg                = "등록에 실패했습니다";
    $with_log_c_no      = "2809";

    $rma_date           = date('Ymd');
    $last_rma_ord_no    = $rma_model->getLastRmaNo();
    $rma_ord_no         = $rma_date."_RMA_".sprintf('%04d', $last_rma_ord_no+1);

    $insert_sql = "INSERT INTO `work_cms_return` SET
            order_number        = '{$rma_ord_no}',
            parent_order_number = '{$prev_ord_no}',
            return_state        = '1',
            return_req_date     = '{$req_date}',
            recipient           = '{$recipient}',
            recipient_addr      = '{$recipient_addr}',
            recipient_hp        = '{$recipient_hp}',
            dp_c_no             = '{$dp_c_no}',
            zip_code            = '{$zip_code}',
            cus_memo            = '{$cus_memo}',
            req_s_no            = '{$req_s_no}',
            req_team            = '{$req_team}',
            req_memo            = '{$req_memo}',
            run_s_no            = '{$req_s_no}',
            run_team            = '{$req_team}',
            regdate             = '{$regdate}'
    ";

    $insert_sub_sql = "
        INSERT INTO `work_cms_return_unit` SET 
            order_number = '{$rma_ord_no}',
            regdate      = '{$regdate}'
    ";

    // 상품 정리
    if(!empty($prd_no))
    {
        $prd_list     = [];
        $prd_val_list = [];
        for($i=0; $i<count($prd_no); $i++)
        {
            if(isset($prd_no[$i]) && !empty($prd_no[$i]))
            {
                $shop_no_val   = isset($prev_shop_ord_no[$i]) ? $prev_shop_ord_no[$i] : "";
                $quantity_val  = isset($quantities[$i]) ? $quantities[$i] : 1;
                $r_purpose_val = isset($return_purpose[$i]) ? $return_purpose[$i] : 1;
                $r_reason_val  = isset($return_reason[$i]) ? $return_reason[$i] : 1;
                $r_type_val    = isset($return_type[$i]) ? $return_type[$i] : 1;
                $r_bad_reason  = (isset($new_bad_reason[$i]) && $r_reason_val == '11') ? $new_bad_reason[$i] : "";

                $prd_val_list[] = $prd_no[$i];
                $prd_list[$prd_no[$i]] = array('shop_ord_no' => $shop_no_val,'quantity' => $quantity_val, 'r_purpose' => $r_purpose_val, 'r_reason' => $r_reason_val, 'r_type' => $r_type_val, 'r_bad_reason' => $r_bad_reason);
            }
        }

        if(!empty($prd_list))
        {
            $change_unit_list = getChangeSingleUnitList();

            $ins_sql_list = "";
            foreach($prd_list as $p_no => $item)
            {
                $new_ins_sql     = $insert_sql;
                if($p_no)
                {
                    $c_no_sql    = "SELECT prd_cms.manager, prd_cms.manager_team, prd_cms.c_no, (SELECT c.c_name FROM company c WHERE c.c_no=prd_cms.c_no) as c_name FROM product_cms prd_cms WHERE prd_cms.prd_no = '{$p_no}' AND prd_cms.c_no != '0' ORDER BY prd_cms.c_no DESC LIMIT 1";
                    $c_no_query  = mysqli_query($my_db, $c_no_sql);
                    $c_no_result = mysqli_fetch_assoc($c_no_query);

                    $s_no        = isset($c_no_result['manager']) ? $c_no_result['manager'] : 0;
                    $team        = isset($c_no_result['manager_team']) ? $c_no_result['manager_team'] : 0;
                    $c_no        = isset($c_no_result['c_no']) ? $c_no_result['c_no'] : 0;
                    $c_name      = isset($c_no_result['c_name']) ? $c_no_result['c_name'] : "";

                    $new_ins_sql  .= ", parent_shop_ord_no='{$item['shop_ord_no']}', c_no='{$c_no}', c_name='{$c_name}', s_no='{$s_no}', team='{$team}', prd_no = '{$p_no}', quantity='{$item['quantity']}', return_purpose='{$item['r_purpose']}', return_reason='{$item['r_reason']}', return_type='{$item['r_type']}', bad_reason='{$item['r_bad_reason']}';";
                    $ins_sql_list .= $new_ins_sql;

                    $unit_sql    = "SELECT pcr.option_no, pcr.quantity, (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='{$with_log_c_no}') as sku FROM product_cms_relation pcr WHERE pcr.prd_no='{$p_no}' AND pcr.display='1'";
                    $unit_query  = mysqli_query($my_db, $unit_sql);
                    while($unit_result = mysqli_fetch_assoc($unit_query))
                    {
                        $pcu_quantity       = $item['quantity']*$unit_result['quantity'];
                        $change_unit_data   = isset($change_unit_list[$unit_result['option_no']]) ? $change_unit_list[$unit_result['option_no']] : [];
                        $change_unit_no     = !empty($change_unit_data) ? $change_unit_data['option'] : $unit_result['option_no'];
                        $change_unit_sku    = !empty($change_unit_data) ? $change_unit_data['sku'] : $unit_result['sku'];

                        $new_ins_sub_sql    = $insert_sub_sql.", `option`='{$change_unit_no}', sku='{$change_unit_sku}', quantity='{$pcu_quantity}';";
                        $ins_sql_list      .= $new_ins_sub_sql;
                    }
                }
            }

            if($ins_sql_list)
            {
                if(mysqli_multi_query($my_db, $ins_sql_list) == true){
                    $result = true;
                    $msg    = "등록에 성공했습니다";

                }else{
                    exit("<script>alert('등록 실패했습니다');history.back()';</script>");
                }
            }
        }
    }
    
    if($result){
        exit("<script>alert('{$msg}');opener.parent.location.reload();window.close();</script>");
    }else{
        exit("<script>alert('{$msg}');history.back();</script>");
    }
}
else
{
    $kind_model         = Kind::Factory();
    $bad_reason_g1_list = $kind_model->getKindParentList("bad_reason");
    $ord_model          = WorkCms::Factory();
    $ord_model->setMainInit("work_cms", "w_no");

    $w_no_val       = isset($_GET['w_no']) ? $_GET['w_no'] : "";
    $w_type_val     = isset($_GET['w_type']) ? $_GET['w_type'] : "";
    $btn_type       = isset($_GET['type']) ? $_GET['type'] : "";
    $w_no_list      = explode(',', $w_no_val);
    $w_type_list    = explode(',', $w_type_val);
    $cms_type_list  = [];
    $w_idx          = 0;
    $log_c_no       = 2809;
    foreach($w_no_list as $w_no)
    {
        $cms_type_list[$w_no] = $w_type_list[$w_idx];
        $w_idx++;
    }

    $add_where = "w.w_no IN({$w_no_val})";
    if($btn_type == "is_delivery"){
        $ord_item = $ord_model->getItem($w_no_val);
        $log_c_no = $ord_item['log_c_no'];
        if($log_c_no == "5659"){
            $add_where = "order_number = '{$ord_item['order_number']}'";
        }
    }
    $smarty->assign("log_c_no", $log_c_no);
    $smarty->assign("w_nos", $w_no_val);
    $smarty->assign("w_types", $w_type_val);

    #리스트 쿼리
    $work_cms_sql = "
		SELECT
		    w.w_no,
            w.order_number,
            w.shop_ord_no,
		    w.recipient,
		    w.recipient_hp,
		    w.recipient_addr,
		    w.zip_code,
		    w.prd_no,
		    (SELECT prd.title FROM product_cms prd WHERE prd.prd_no = w.prd_no) as prd_name,
		    w.log_c_no,
		    w.dp_c_no,
		    w.dp_c_name,
		    w.quantity,
		    w.delivery_memo
		FROM work_cms w
		WHERE {$add_where}
		ORDER BY w.dp_price_vat DESC
	";
    $work_cms_query  = mysqli_query($my_db, $work_cms_sql);
    $deli_prd_list   = [];
    $return_prd_list = [];
    $work_cms_list   = [];
    while ($work_cms = mysqli_fetch_assoc($work_cms_query))
    {
        if(empty($work_cms['shop_ord_no'])){
            $new_shop_no = "W".$work_cms['order_number']."_".$work_cms['prd_no'];
            $upd_sql = "UPDATE work_cms SET shop_ord_no='{$new_shop_no}' WHERE w_no='{$work_cms['w_no']}'";
            mysqli_query($my_db, $upd_sql);

            $work_cms['shop_ord_no'] = $new_shop_no;
        }

        if(empty($work_cms_list))
        {
            $work_cms_list = array(
                "order_number"    => $work_cms['order_number'],
                "first_shop_no"   => $work_cms['shop_ord_no'],
                "recipient"       => $work_cms['recipient'],
                "recipient_hp"    => $work_cms['recipient_hp'],
                "recipient_addr"  => htmlspecialchars($work_cms['recipient_addr']),
                "zip_code"        => $work_cms['zip_code'],
                "log_c_no"        => $work_cms['log_c_no'],
                "dp_c_no"         => $work_cms['dp_c_no'],
                "dp_c_name"       => $work_cms['dp_c_name'],
                "deli_prd_list"   => [],
                "deli_prd_cnt"    => 0,
                "return_prd_list" => [],
                "return_prd_cnt"  => 0,
            );
        }

        $unit_bad_sql   = "SELECT DISTINCT bad_reason FROM product_cms_unit as pcu WHERE pcu.`no` IN(SELECT pr.option_no FROM product_cms_relation pr WHERE pr.prd_no='{$work_cms['prd_no']}' AND pr.display='1') ORDER BY bad_reason ASC";
        $unit_bad_query = mysqli_query($my_db, $unit_bad_sql);
        $unit_bad_list  = [];
        while($unit_bad_result = mysqli_fetch_assoc($unit_bad_query))
        {
            if(isset($bad_reason_g1_list[$unit_bad_result['bad_reason']]))
            {
                $unit_bad_list[$unit_bad_result['bad_reason']] = $bad_reason_g1_list[$unit_bad_result['bad_reason']];
            }
        }

        $work_cs_type  = $cms_type_list[$work_cms['w_no']];
        $chk_unit_text = "";
        if(!empty($work_cms['prd_no'])){
            $chk_unit_sql   = "SELECT pcr.option_no, pcu.option_name, pcu.is_in_out, pcu.in_out_msg FROM product_cms_relation pcr LEFT JOIN product_cms_unit pcu ON pcu.no=pcr.option_no WHERE pcr.prd_no='{$work_cms['prd_no']}' AND pcr.display='1' AND pcu.is_in_out='2'";
            $chk_unit_query = mysqli_query($my_db, $chk_unit_sql);
            while($chk_unit = mysqli_fetch_assoc($chk_unit_query)){
                $chk_unit_text .= empty($chk_unit_text) ? "※주의 : 입/출고 정지 [{$chk_unit['in_out_msg']}]" : ", [{$chk_unit['in_out_msg']}]";
            }
        }

        $product = array(
            'w_no'        => $work_cms['w_no'],
            'prd_no'      => $work_cms['prd_no'],
            'shop_ord_no' => $work_cms['shop_ord_no'],
            'prd_name'    => $work_cms['prd_name'],
            'quantity'    => $work_cms['quantity'],
            'cs_type'     => $work_cs_type,
            'in_out_msg'  => $chk_unit_text,
            'bad_g1_list' => !empty($unit_bad_list) ? $unit_bad_list : $bad_reason_g1_list
        );

        if ($work_cs_type == '1') {
            $deli_prd_list[]    = $product;
            $return_prd_list[]  = $product;
        } elseif ($work_cs_type == '2') {
            $return_prd_list[] = $product;
        }else{
            $deli_prd_list[]    = $product;
        }
    }

    $work_cms_list['deli_prd_list']   = $deli_prd_list;
    $work_cms_list['deli_prd_cnt']    = (count($deli_prd_list) > 0) ? count($deli_prd_list) : 1;
    $work_cms_list['return_prd_list'] = $return_prd_list;
    $work_cms_list['return_prd_cnt']  = (count($return_prd_list) > 0) ? count($return_prd_list) : 1;

    // CS 등록 폼에 필요한 값들(dp_cs_list, 주문번호, 접수일)
    $new_ord_date   = date('Ymd');
    $cur_date       = date('Y-m-d');
    $req_date       = $cur_date;
    $req_hour       = date('Hi');
    $req_day_val    = date('w');

    // 배송리스트 마지막 번호
    $last_ord_no    = $cms_model->getLastCsNo();
    $new_ord_no     = $new_ord_date."_CS_".sprintf('%04d', $last_ord_no+1);

    if($req_hour >= 1630){
        $req_date = ($req_day_val == 5) ? date('Y-m-d', strtotime("+3 days")) : date('Y-m-d', strtotime("+1 days"));
    }

    $smarty->assign('btn_type', $btn_type);
    $smarty->assign('cur_date', $cur_date);
    $smarty->assign('req_date', $req_date);
    $smarty->assign('dp_cs_list', getDpCsList());
    $smarty->assign('new_ord_no', $new_ord_no);
    $smarty->assign("return_purpose_option", getReturnPurposeOption());
    $smarty->assign("return_reason_option", getReturnReasonOption());
    $smarty->assign("return_type_option", getReturnTypeOption());
    $smarty->assign("bad_reason_g1_list", $bad_reason_g1_list);
    $smarty->assign("work_cms_list", $work_cms_list);

    $smarty->display('work_cms_return_window.html');
}

?>
