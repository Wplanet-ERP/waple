<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/corporation.php');
require('inc/model/Kind.php');
require('inc/model/MyCompany.php');
require('inc/model/Corporation.php');

// 접근 권한
if (!permissionNameCheck($session_permission, "재무관리자")){
    $smarty->display('access_error.html');
    exit;
}

# Model Inint
$card_model = Corporation::Factory();
$card_model->setCorpCard();

# 법인카드 Init
$cc_no          = isset($_GET['cc_no']) ? $_GET['cc_no'] : "";
$corp_card      = [];
$share_card_g1  = "";
$title		    = "등록하기";
$corp_card_num_list = [];

$regist_url     = "corp_card_regist.php";
$list_url       = "corp_card_management.php";

$sch_page       = isset($_GET['page']) ? $_GET['page'] : "1";
$sch_page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$search_url     = "page={$sch_page}&ord_page_type={$sch_page_type}";

$sch_array_key  = array("sch_my_c_no","sch_card_kind","sch_card_num","sch_card_type","sch_share_g1","sch_share_g2","sch_share_card","sch_manager","sch_expired","sch_display");
foreach($sch_array_key as $sch_key)
{
    $sch_val   = isset($_GET[$sch_key]) && !empty($_GET[$sch_key]) ? $_GET[$sch_key]: "";
    if(!!$sch_val)
    {
        $search_url .= "&{$sch_key}={$sch_val}";
    }
}
$smarty->assign("search_url", $search_url);


# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'new_card') # 생성
{
    # 필수정보 체크 및 저장
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $my_c_no        = isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "";
    $card_kind      = isset($_POST['card_kind']) ? $_POST['card_kind'] : "";
    $card_num       = isset($_POST['card_num']) && !empty($_POST['card_num']) ? implode('-', $_POST['card_num']) : "";
    $card_type      = isset($_POST['card_type']) ? $_POST['card_type'] : "";
    $team           = isset($_POST['team']) ? $_POST['team'] : "";
    $manager        = isset($_POST['manager']) ? $_POST['manager'] : "";
    $expired_date   = isset($_POST['expired_date']) ? $_POST['expired_date']."-01" : "";
    $share_card     = isset($_POST['share_card']) ? $_POST['share_card'] : "";
    $display        = isset($_POST['display']) ? trim($_POST['display']) : "";
    $regdate        = date('Y-m-d H:i:s');

    if(empty($card_num) || empty($card_type) || empty($team) || empty($manager) || empty($expired_date) || empty($share_card))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');location.href='{$regist_url}?{$search_url}';</script>");
    }

    $card_data = array(
        'my_c_no'       => $my_c_no,
        'card_kind'     => $card_kind,
        'card_num'      => $card_num,
        'card_type'     => $card_type,
        'manager'       => $manager,
        'team'          => $team,
        'expired_date'  => $expired_date,
        'share_card'    => $share_card,
        'display'       => $display,
        'regdate'       => $regdate,
    );

    # 세부정보 저장
    $pay_date   = isset($_POST['pay_date']) ? $_POST['pay_date']: "";
    $password   = isset($_POST['password']) ? trim($_POST['password']) : "";
    $cvc        = isset($_POST['cvc']) ? trim($_POST['cvc']) : "";
    $limit_cost = isset($_POST['limit_cost']) ? trim($_POST['limit_cost']) : "";

    if(!empty($pay_date)){
        $card_data['pay_date'] = $pay_date;
    }

    if(!empty($password)){
        $card_data['password'] = $password;
    }

    if(!empty($cvc)){
        $card_data['cvc'] = $cvc;
    }

    if(!empty($limit_cost)){
        $card_data['limit_cost'] = $limit_cost;
    }

    if($card_model->insert($card_data)) {
        exit("<script>alert('법인카드 등록에 성공했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }else{
        exit("<script>alert('법인카드 등록에 실패했습니다');location.href='{$regist_url}?{$search_url}';</script>");
    }
}
elseif($process == 'modify_card') # 수정
{
    $modify_key = array('search_url', 'cc_no', 'my_c_no', 'card_kind', 'card_type', 'manager', 'team', 'expired_date', 'share_card', 'pay_date', 'password', 'cvc', 'limit_cost', 'display');
    $modify_var = array_fill_keys($modify_key, "");
    foreach ($modify_var as $key => $val) {
        $modify_var[$key] = $_POST[$key];
    }
    extract($modify_var);

    $card_num   = isset($_POST['card_num']) && !empty($_POST['card_num']) ? implode('-', $_POST['card_num']) : "";

    # 필수정보 체크 및 저장
    if(empty($cc_no) || empty($card_num) || empty($card_type) || empty($team) || empty($manager) || empty($expired_date) || empty($share_card))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');location.href='{$regist_url}?{$search_url}';</script>");
    }

    $expired_date = $expired_date."-01";

    $card_data = array(
        'cc_no'         => $cc_no,
        'my_c_no'       => $my_c_no,
        'card_kind'     => $card_kind,
        'card_num'      => $card_num,
        'card_type'     => $card_type,
        'manager'       => $manager,
        'team'          => $team,
        'expired_date'  => $expired_date,
        'share_card'    => $share_card,
        'pay_date'      => !empty($pay_date) ? $pay_date : "NULL",
        'password'      => !empty($password) ? $password : "NULL",
        'cvc'           => !empty($cvc) ? $cvc : "NULL",
        'limit_cost'    => !empty($limit_cost) ? $limit_cost : "NULL",
        'display'       => $display,
    );

    if($card_model->update($card_data)) {
        exit("<script>alert('법인카드 수정에 성공했습니다');location.href='{$regist_url}?cc_no={$cc_no}&{$search_url}';</script>");
    }else{
        exit("<script>alert('법인카드 수정에 실패했습니다');location.href='{$regist_url}?cc_no={$cc_no}&{$search_url}';</script>");
    }
}
elseif($process == 'corp_card_memo') # 관리자 메모
{
    $cc_no      = isset($_POST['cc_no']) ? $_POST['cc_no'] : "";
    $memo       = isset($_POST['memo']) ? $_POST['memo'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_data   = array("cc_no" => $cc_no, "memo" => addslashes($memo));

    if($card_model->update($upd_data)) {
        exit("<script>alert('관리자메모 수정에 성공했습니다');location.href='{$regist_url}?cc_no={$cc_no}&{$search_url}';</script>");
    }else{
        exit("<script>alert('관리자메모 수정에 실패했습니다');location.href='{$regist_url}?cc_no={$cc_no}&{$search_url}';</script>");
    }
}
elseif($cc_no)
{
    $corp_card 		            = $card_model->getCardItem($cc_no);
    $team_depth                 = isset($corp_card['depth']) && !empty($corp_card['depth']) ? $corp_card['depth'] : "1";
    $team_label                 = getTeamFullName($my_db, $team_depth, $corp_card['team']);
    $corp_card["team_name"]     = $team_label;
    $corp_card["manager_name"]  = $corp_card['s_name'];
    $corp_card_num_list         = isset($corp_card['card_num']) && !empty($corp_card['card_num']) ? explode('-',$corp_card['card_num']) : "";

    $title = "수정하기";
}

$process_get = isset($_GET['process']) ? $_GET['process'] : "";
if($process_get == 'staff_new_card')
{
    $s_no           = isset($_GET['s_no']) ? $_GET['s_no'] : "";
    $s_name_sql     = "SELECT s.team, s.s_name, s.my_c_no, (SELECT t.team_name FROM team t WHERE t.team_code=s.team LIMIT 1) as t_name, (SELECT t.depth FROM team t WHERE t.team_code=`s`.team) as `depth` FROM staff s WHERE s.s_no='{$s_no}'";
    $s_name_query   = mysqli_query($my_db, $s_name_sql);
    $s_name_result  = mysqli_fetch_assoc($s_name_query);

    $team_depth                 = isset($s_name_result['depth']) && !empty($s_name_result['depth']) ? $s_name_result['depth'] : "1";
    $team_label                 = getTeamFullName($my_db, $team_depth, $s_name_result['team']);

    $corp_card['my_c_no']       = isset($s_name_result['my_c_no']) ? $s_name_result['my_c_no'] : "1";
    $corp_card["card_kind"]     = "개인카드";
    $corp_card['card_type']     = '3';
    $corp_card['manager']       = $s_no;
    $corp_card['team']          = isset($s_name_result['team']) ? $s_name_result['team'] : "";
    $corp_card['team_name']     = $team_label;
    $corp_card["share_card"]    = isset($s_name_result['s_name']) ? $s_name_result['s_name'] : "";
    $corp_card['manager_name']  = isset($s_name_result['s_name']) ? $s_name_result['s_name'] : "";

    $corp_card_num_list = array(
        '0' => "0000",
        '1' => "0000",
        '2' => "0000",
        '3' => "0000",
    );
}

$corp_card['card_num_list'] = array(
    '0' => isset($corp_card_num_list[0]) ? $corp_card_num_list[0] : "",
    '1' => isset($corp_card_num_list[1]) ? $corp_card_num_list[1] : "",
    '2' => isset($corp_card_num_list[2]) ? $corp_card_num_list[2] : "",
    '3' => isset($corp_card_num_list[3]) ? $corp_card_num_list[3] : ""
);

# 카드 그룹리스트 처리
$my_company_model   = MyCompany::Factory();
$my_company_option  = $my_company_model->getNameList();

$smarty->assign("title", $title);
$smarty->assign($corp_card);
$smarty->assign('my_company_option', $my_company_option);
$smarty->assign('card_type_option', getCardTypeOption());
$smarty->assign('display_option', getDisplayOption());

$smarty->display('corp_card_regist.html');
?>
