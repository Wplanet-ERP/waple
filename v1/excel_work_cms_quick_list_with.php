<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$nowdate = date("Y-m-d H:i:s");

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "주문번호")
    ->setCellValue('B1', "구성품명(SKU)")
    ->setCellValue('C1', "수량")
    ->setCellValue('D1', "수령자명")
    ->setCellValue('E1', "수령자전화")
    ->setCellValue('F1', "수령자핸드폰")
    ->setCellValue('G1', "수령지")
    ->setCellValue('H1', "요청사항")
    ->setCellValue('I1', "특이사항")
    ->setCellValue('J1', "참고사항2")
    ->setCellValue('K1', "구분")
;

# 검색 조건
$add_where      = "w.quick_state='2' AND w.delivery_method='1' AND w.sender_type='2809'";
$cms_ord_sql    = "SELECT DISTINCT w.order_number FROM work_cms_quick w WHERE {$add_where} AND w.order_number is not null ORDER BY w.w_no DESC";
$cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);
$order_number_list  = [];
while($order_number = mysqli_fetch_assoc($cms_ord_query)){
	$order_number_list[] =  "'".$order_number['order_number']."'";
}
$order_numbers = implode(',', $order_number_list);

$delivery_sql   = "SELECT order_number, delivery_no, delivery_type FROM work_cms_quick_delivery w WHERE order_number IN({$order_numbers}) GROUP BY order_number ORDER BY `no` ASC";
$delivery_query = mysqli_query($my_db, $delivery_sql);
$delivery_list  = [];
while($delivery = mysqli_fetch_assoc($delivery_query))
{
    $delivery_list[$delivery['order_number']] = $delivery;
}

// 리스트 쿼리
$work_cms_quick_sql = "
	SELECT
		w.order_number,
		w.prd_no,
		w.prd_type,
		w.quantity,
		w.unit_price,
		w.run_c_name,
		w.receiver,
		w.receiver_hp,
        w.zip_code,
		w.receiver_addr
	FROM work_cms_quick w
	WHERE {$add_where} AND order_number IN({$order_numbers})
	ORDER BY w.w_no DESC, w.order_number
    LIMIT 10000
";
$work_cms_quick_query	= mysqli_query($my_db, $work_cms_quick_sql);
$idx = 2;
if(!!$work_cms_quick_query)
{
    while($work_cms_quick = mysqli_fetch_array($work_cms_quick_query))
    {
        $delivery_type  = isset($delivery_list[$work_cms_quick['order_number']]) ? $delivery_list[$work_cms_quick['order_number']]['delivery_type'] : "";
        $delivery_no    = isset($delivery_list[$work_cms_quick['order_number']]) ? $delivery_list[$work_cms_quick['order_number']]['delivery_no'] : "";
        $unit_price     = number_format($work_cms_quick['unit_price']);
        $unit_list      = [];

        if($work_cms_quick['prd_type'] == '1'){
            $unit_sql   = "SELECT pcr.option_no, (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='2809') as sku, pcr.quantity as qty FROM product_cms_relation as pcr WHERE pcr.prd_no ='{$work_cms_quick['prd_no']}' AND pcr.display='1'";
            $unit_query = mysqli_query($my_db, $unit_sql);
            while($unit_result = mysqli_fetch_assoc($unit_query)){
                $unit_result['qty'] = $work_cms_quick['quantity']*$unit_result['qty'];
                $unit_list[]        = $unit_result;
            }
        }else{
            $unit_sql       = "SELECT pcum.prd_unit, pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit='{$work_cms_quick['prd_no']}' AND pcum.log_c_no='2809'";
            $unit_query     = mysqli_query($my_db, $unit_sql);
            $unit_result    = mysqli_fetch_assoc($unit_query);
            $unit_list[]    = array("sku" => $unit_result['sku'], "qty" => $work_cms_quick['quantity']);
        }

        foreach($unit_list as $unit_data)
        {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit("A{$idx}", $work_cms_quick['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("B{$idx}", $unit_data['sku'])
                ->setCellValue("C{$idx}", $unit_data['qty'])
                ->setCellValue("D{$idx}", $work_cms_quick['receiver'])
                ->setCellValue("E{$idx}", $work_cms_quick['receiver_hp'])
                ->setCellValue("F{$idx}", $work_cms_quick['receiver_hp'])
                ->setCellValue("G{$idx}", $work_cms_quick['receiver_addr'])
                ->setCellValue("H{$idx}", $work_cms_quick["req_memo"])
                ->setCellValue("I{$idx}", "")
                ->setCellValue("J{$idx}", "")
                ->setCellValue("K{$idx}", $work_cms_quick["run_c_name"])
            ;

            $idx++;
        }
    }
}
$idx--;

$excel_title = "택배 접수완료 EMP양식(SKU)";

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Tahoma');

$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
$objPHPExcel->getActiveSheet()->getStyle('I1:K1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FDE9D9');
$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:K{$idx}")->getFont()->setSize(9);;
$objPHPExcel->getActiveSheet()->getStyle("A2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("A2:K{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(9);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(70);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);

$objPHPExcel->getActiveSheet()->getStyle("C2:C{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("A1:K{$idx}")->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->setTitle($excel_title);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$excel_title}.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
