<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_sales.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');

# Navigation & My Quick
$nav_prd_no  = "49";
$nav_title   = "매출 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 상품 검색조건
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_get	= isset($_GET['sch'])?$_GET['sch']:"";

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);

$kind_model             = Kind::Factory();
$product_model          = ProductCms::Factory();
$company_model          = Company::Factory();
$cms_code               = "product_cms";
$cms_group_list         = $kind_model->getKindGroupList($cms_code);
$prd_total_list         = $product_model->getPrdGroupData();
$dp_company_option      = $company_model->getDpList();
$sch_dp_company_option  = $company_model->getDpDisplayList();
$prd_g1_list = $prd_g2_list = $prd_g3_list = [];

foreach($cms_group_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}

$prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
$sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);


# 검색 쿼리
$add_where = "1=1 AND w.delivery_state='4'";

// 상품(업무) 종류
if (!empty($sch_prd) && $sch_prd != "0") { // 상품
    $add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);

$sch_w_no 			    = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
$sch_reg_s_date         = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : $week_val;
$sch_reg_e_date         = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : $today_val;
$sch_reg_date_type      = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "week";
$sch_stock_date 	    = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : "";
$sch_order_date 	    = isset($_GET['sch_order_date']) ? $_GET['sch_order_date'] : "";
$sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_dp_c_no 		    = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_prd_name 		    = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";

if(!empty($sch_w_no)){
    $add_where .= " AND w.w_no='{$sch_w_no}'";
    $smarty->assign('sch_w_no', $sch_w_no);
}

if(!empty($sch_reg_s_date) || !empty($sch_reg_e_date))
{
    if(!empty($sch_reg_s_date)){
        $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
        $add_where .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
        $smarty->assign('sch_reg_s_date', $sch_reg_s_date);
    }

    if(!empty($sch_reg_e_date)){
        $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
        $add_where .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
        $smarty->assign('sch_reg_e_date', $sch_reg_e_date);
    }
}
$smarty->assign('sch_reg_date_type', $sch_reg_date_type);

if(!empty($sch_stock_date)){
    $add_where .= " AND w.stock_date = '{$sch_stock_date}'";
    $smarty->assign('sch_stock_date', $sch_stock_date);
}

if(!empty($sch_order_date)){
    $sch_order_s_date = $sch_order_date." 00:00:00";
    $sch_order_e_date = $sch_order_date." 23:59:59";
    $add_where .= " AND (w.order_date >= '{$sch_order_s_date}' AND w.order_date <= '{$sch_order_e_date}')";
    $smarty->assign('sch_order_date', $sch_order_date);
}

if(!empty($sch_order_number)){
    $add_where .= " AND w.order_number = '{$sch_order_number}'";
    $smarty->assign('sch_order_number', $sch_order_number);
}

if(!empty($sch_dp_c_no)){
    $add_where .= " AND w.dp_c_no = '{$sch_dp_c_no}'";
    $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
}

if(!empty($sch_prd_name)){
    $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no from product_cms prd_cms where prd_cms.title like '%{$sch_prd_name}%')";
    $smarty->assign('sch_prd_name', $sch_prd_name);
}

# 페이징 처리
$cms_sales_total_sql    = "SELECT count(*) as cnt FROM (SELECT w_no FROM work_cms w WHERE {$add_where}) AS cnt";
$cms_sales_total_query	= mysqli_query($my_db, $cms_sales_total_sql);
$cms_sales_total_result = mysqli_fetch_array($cms_sales_total_query);
$cms_sales_total        = $cms_sales_total_result['cnt'];

//페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($cms_sales_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "work_cms_sales_list.php", $pagenum, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $cms_sales_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$cms_sales_sql = "
    SELECT
        w.w_no,
        w.stock_date,
        w.order_date,
        w.c_name as brand_name,
        DATE_FORMAT(w.order_date, '%Y-%m-%d') as ord_date,
        DATE_FORMAT(w.order_date, '%H:%i') as ord_time,
        w.order_number,
        w.prd_no,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
        (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
        w.quantity,
        w.unit_price,
        w.unit_delivery_price,
        w.dp_c_no,
        (SELECT return_type FROM csm_return cr WHERE cr.order_number=w.order_number AND cr.shop_ord_no=w.shop_ord_no) as return_type,
        (SELECT return_date FROM csm_return cr WHERE cr.order_number=w.order_number AND cr.shop_ord_no=w.shop_ord_no) as return_date
    FROM work_cms w
    WHERE {$add_where}
    ORDER BY w.w_no DESC
    LIMIT {$offset}, {$num}
";
$cms_sales_query = mysqli_query($my_db, $cms_sales_sql);
$cms_sales_list  = [];
while($cms_sales = mysqli_fetch_array($cms_sales_query))
{
    if($cms_sales['return_type'] > 0)
    {
        switch($cms_sales['return_type'])
        {
            case '0':
                $cms_sales['return_type_name'] = "";
                break;
            case '1':
                $cms_sales['return_type_name'] = "반품";
                break;
            case '2':
                $cms_sales['return_type_name'] = "교환";
                break;
            case '3':
                $cms_sales['return_type_name'] = "기타";
                break;
            default:
                $cms_sales['return_type_name'] = "";
                break;
        }
    }

    if($cms_sales['return_date']) {
        $cms_sales['return_day'] = date('Y-m-d', strtotime($cms_sales['return_date']));
        $cms_sales['return_time'] = date('H:i', strtotime($cms_sales['return_date']));
    }

    $cms_sales['dp_c_name']   = $dp_company_option[$cms_sales['dp_c_no']];
    $cms_sales['total_price'] = $cms_sales['unit_price']+$cms_sales['unit_delivery_price'];

    $cms_sales_list[] = $cms_sales;
}

$smarty->assign("page_type_option", getPageTypeOption('4'));
$smarty->assign("sch_dp_company_option", $sch_dp_company_option);
$smarty->assign("cms_sales_list", $cms_sales_list);

$smarty->display('work_cms_sales_list.html');
?>
