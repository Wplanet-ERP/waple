<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/promotion.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Team.php');
require('inc/model/Promotion.php');

# 날짜 설정
$nowdate 	= date("Y-m-d H:i:s");
$today 		= date("Y-m-d");

$smarty->assign("nowdate", $nowdate);
$smarty->assign("today", $today);

# Model 설정
$promotion_model = Promotion::Factory();
$team_model 	 = Team::Factory();
$kind_model 	 = Kind::Factory();

# 프로세스 처리
$proc=(isset($_POST['process']))?$_POST['process']:"";

if($proc == "save_memo")
{
	$p_no 		= (isset($_POST['p_no'])) ? $_POST['p_no'] : "";
	$search_url	= (isset($_POST['search_url']))?$_POST['search_url']:"";
	$mtxt		= (isset($_POST['f_memo']))?$_POST['f_memo']:"";

	if($promotion_model->update(array("p_no" => $p_no, "p_memo" => addslashes($mtxt)))){
		exit("<script>alert('메모를 등록하였습니다');location.href='promotion_list.php?{$search_url}';</script>");
	}else{
		exit("<script>alert('메모 등록에 실패했습니다');location.href='promotion_list.php?{$search_url}';</script>");
	}

}
elseif($proc == "del_promotion")
{
	$p_no 		= (isset($_POST['p_no'])) ? $_POST['p_no'] : "";
	$search_url	= (isset($_POST['search_url']))?$_POST['search_url']:"";

	if (!$promotion_model->delete($p_no))
		exit("<script>alert('p_no = {$p_no} 캠페인 삭제가 실패하였습니다.');location.href='promotion_list.php?{$search_url}';</script>");
	else{
		$work_sql = "DELETE FROM `work` WHERE linked_no = '{$p_no}' AND linked_table = 'promotion'";

		if (!mysqli_query($my_db, $work_sql)){
			echo("<script>alert('업무리스트 삭제에 실패 하였습니다.\\n개발 담당자에게 문의해 주세요.');</script>");
		}
		exit("<script>alert('p_no = {$p_no} 캠페인을 삭제하였습니다.');location.href='promotion_list.php?{$search_url}';</script>");
	}
}

# 각종 옵션들
$date_short_option			= getDayShortOption();
$state_option	 			= getTotalStateOption();
$state_short_option			= getShortStateOption();
$promotion_kind_option		= getPromotionKindOption();
$promotion_channel_option	= getPromotionChannelOption();
$promotion_date_type_option	= getPromotionDateTypeOption();
$location_option			= $kind_model->getKindParentList('location');

$smarty->assign("state_option", $state_option);
$smarty->assign("state_short_option", $state_short_option);
$smarty->assign("promotion_kind_option", $promotion_kind_option);
$smarty->assign("promotion_channel_option", $promotion_channel_option);
$smarty->assign("promotion_date_type_option", $promotion_date_type_option);
$smarty->assign("location_option", $location_option);

# 검색쿼리
$add_where			= "";
$where = $keyword = $field = [];

$search_state		= isset($_GET['sstate']) ? $_GET['sstate'] : "";
$search_s_name		= (isset($_GET['s_name']) && $_GET['s_name'] !='®_s_name=') ? $_GET['s_name'] : "";
$search_team		= isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$search_reg_s_name	= isset($_GET['reg_s_name']) ? $_GET['reg_s_name']:"";
$search_kind		= isset($_GET['skind']) ? $_GET['skind']:"";
$sch_channel_get	= isset($_GET['schannel']) ? $_GET['schannel'] : "";
$f_location1_get	= isset($_GET['f_location1']) ? $_GET['f_location1'] : "";
$f_location2_get	= isset($_GET['f_location2']) ? $_GET['f_location2'] : "";
$search_company		= isset($_GET['scompany']) ? $_GET['scompany'] : "";

if(!empty($search_state)) {
	$field[]	= "sstate";
	$keyword[]	= $search_state;

	if($search_state == '6'){
		$where[] = "p_state1='1'";
	}else{
		$where[] = "p_state='{$search_state}' and p_state1!='1'";
	}
}

if(!empty($search_s_name)) {
    $search_s_name 	= str_replace("®_s_name=","", $search_s_name);
	$field[]		= "s_name";
	$keyword[]		= $search_s_name;
	$where[]		= "(name like '%{$search_s_name}%')";
}

if(!empty($search_team)) {
    $field[]	= "sch_team";
    $keyword[]	= $search_team;

    $sch_team_code_where = getTeamWhere($my_db, $search_team);
    if($sch_team_code_where){
        $where[] = " team IN ({$sch_team_code_where})";
    }
}

if(!empty($search_reg_s_name)) {
    $field[]	= "reg_s_name";
    $keyword[]	= $search_reg_s_name;
    $where[]	= "(reg_s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name like '%{$search_reg_s_name}%'))";
}

if(!empty($search_kind)) {
	$field[]	= "skind";
	$keyword[]	= $search_kind;
	$where[]	= "kind='{$search_kind}'";
}

if(!empty($sch_channel_get)) {
	$field[]	= "schannel";
	$keyword[]	= $sch_channel_get;
	$where[]	= "channel='{$sch_channel_get}'";
}

if(!empty($f_location1_get)) {
	$field[]	= "f_location1";
	$keyword[]	= $f_location1_get;
	$where[]	= "c_no in (select c_no from company where location1='{$f_location1_get}')";
}

if(!empty($f_location2_get)) {
	$field[]	= "f_location2";
	$keyword[]	= $f_location2_get;
	$where[]	= "c_no in (select c_no from company where location2='{$f_location2_get}')";
}

if(!empty($search_company)) {
	$field[]	= "scompany";
	$keyword[]	= $search_company;
	$where[]	= "company like '%{$search_company}%'";
}

#날짜조회
$sch_date_chk 	= isset($_GET['sch_date_chk']) ? $_GET['sch_date_chk'] : "";
$sch_date_type  = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$sch_date_kind	= isset($_GET['sch_date_kind']) ? $_GET['sch_date_kind'] : "1";
$today_s_w		= date('w')-1;
$sch_s_date 	= isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date("Y-m", strtotime("-6 month")); // 월간
$sch_e_date 	= isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date("Y-m"); // 월간
$sch_s_w_date 	= isset($_GET['sch_s_w_date']) ? $_GET['sch_s_w_date'] : date("Y-m-d",strtotime("-{$today_s_w} day")); // 주간
$sch_e_w_date 	= isset($_GET['sch_e_w_date']) ? $_GET['sch_e_w_date'] : date("Y-m-d", strtotime("{$sch_s_w_date} +6 day")); // 주간
$sch_s_date2 	= isset($_GET['sch_s_date2']) ? $_GET['sch_s_date2'] : date("Y-m-d", strtotime("-6 day")); // 일간
$sch_e_date2 	= isset($_GET['sch_e_date2']) ? $_GET['sch_e_date2'] : date("Y-m-d"); // 일간

$smarty->assign("sch_date_chk", $sch_date_chk);
$smarty->assign("sch_date_type", $sch_date_type);
$smarty->assign("sch_date_kind", $sch_date_kind);
$smarty->assign("sch_s_date", $sch_s_date);
$smarty->assign("sch_s_w_date", $sch_s_w_date);
$smarty->assign("sch_s_date2", $sch_s_date2);
$smarty->assign("sch_e_date", $sch_e_date);
$smarty->assign("sch_e_w_date", $sch_e_w_date);
$smarty->assign("sch_e_date2", $sch_e_date2);
if(!empty($sch_date_chk))
{
	$sch_date_where = [];
	$sch_date_column = [];
	switch($sch_date_type)
	{
        case 1:
            $sch_date_column[] = "a.reg_sdate";
        	break;
        case 2:
            $sch_date_column[] = "a.awd_date";
            break;
        case 3:
            $sch_date_column[] = "a.exp_edate";
            break;
        case 4:
            $sch_date_column[] = "a.pres_reward";
            break;
        case 5:
            $sch_date_column[] = "a.reg_sdate";
            $sch_date_column[] = "a.awd_date";
            $sch_date_column[] = "a.exp_edate";
            break;
    }

    if(!empty($sch_date_column))
    {
        foreach($sch_date_column as $date_column)
        {
        	if($sch_date_kind == '1'){
                $sch_s_date .= "-01";
                $sch_e_date .= "-31";
                $sch_date_where[] = "{$date_column} BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
			}elseif($sch_date_kind == '2'){
                $sch_date_where[] = "{$date_column} BETWEEN '{$sch_s_w_date}' AND '{$sch_e_w_date}'";
			}elseif($sch_date_kind == '3'){
                $sch_date_where[] = "{$date_column} BETWEEN '{$sch_s_date2}' AND '{$sch_e_date2}'";
			}

		}
	}

	if(!empty($sch_date_where))
	{
        $sch_date_where_val = implode(" OR ", $sch_date_where);
        $where[] = "({$sch_date_where_val})";
	}
}

if(sizeof($where)>0) {
	$add_where = " WHERE ";
	for($i=0;$i<sizeof($where);$i++) {
		$add_where .= $where[$i].(($i<(sizeof($where)-1))?" and ":"");
		$smarty->assign($field[$i], $keyword[$i]);
	}

}

/* 날짜별검색 20160315 김윤영 Start */
$search_date_type = (isset($_GET['sdate_type'])) ? $_GET['sdate_type'] : "";
$search_date	  = (isset($_GET['sdate'])) ? $_GET['sdate'] : "";
$smarty->assign("sdate_type", $search_date_type);
$smarty->assign("sdate", $search_date);

if($search_date)
{
	switch($search_date_type){
		case 1 :// 모집일
			if(!$add_where)
				$add_where = " where a.reg_sdate='{$search_date}' ";
			else
				$add_where .= " and a.reg_sdate='{$search_date}' ";
			break;
		case 2 :// 발표일
			if(!$add_where)
				$add_where = " where a.awd_date='{$search_date}' ";
			else
				$add_where .= " and a.awd_date='{$search_date}' ";
			break;
		case 3 :// 마감일
			if(!$add_where)
				$add_where = " where a.exp_edate='{$search_date}' ";
			else
				$add_where .= " and a.exp_edate='{$search_date}' ";
			break;
		case 4 :// 보상일
			if(!$add_where)
				$add_where = " where a.pres_reward='{$search_date}' ";
			else
				$add_where .= " and a.pres_reward='{$search_date}' ";
			break;
		case "today" :// 오늘의 모집/발표/마감
			$exp_edate = date("Y-m-d", strtotime("-1 day", strtotime($search_date)));

			if(!$add_where)
				$add_where = " where a.reg_sdate='{$search_date}' or a.awd_date='{$search_date}' or a.exp_edate='{$exp_edate}' ";
			else
				$add_where .= " and a.reg_sdate='{$search_date}' or a.awd_date='{$search_date}' or a.exp_edate='{$exp_edate}' ";
			break;
	}
}
/* 날짜별검색 20160315 김윤영 End */

// 2차 분류에 값이 있는 경우 2차 지역 분류 가져오기(2차)
if($f_location2_get != "" || $f_location1_get != "")
{
	$editlocation = $kind_model->getKindGroupChildList("location", $f_location1_get);
	$smarty->assign("editlocation", $editlocation);
}

// 정렬순서 토글 & 필드 지정
$add_orderby = "";
$order		 = isset($_GET['od']) ? $_GET['od'] : "";
$order_type	 = isset($_GET['by']) ? $_GET['by'] : "1";

if($order_type == '2'){
	$toggle = "DESC";
}else{
	$toggle = "ASC";
}

$order_field=array('','reg_sdate','reg_edate','awd_date','exp_edate','pres_reward');
if($order && $order<6) {
	$add_orderby.="$order_field[$order] $toggle,";
	$smarty->assign("order",$order);
	$smarty->assign("order_type",$order_type);
}

$add_orderby.=" p_no desc";


# 전체 게시물 수 & 페이징 처리
$cnt_sql 	= "SELECT count(p_no) FROM promotion a {$add_where}";
$cnt_query	= mysqli_query($my_db, $cnt_sql);
$cnt_data	= mysqli_fetch_array($cnt_query);
$total_num 	= $cnt_data[0];

$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 15;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($total_num/$num);


if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$search_url = getenv("QUERY_STRING");
$pagelist 	= pagelist($pages, "promotion_list.php", $pagenum, $search_url);

$smarty->assign("total_num",$total_num);
$smarty->assign("search_url", $search_url);
$smarty->assign("pagelist", $pagelist);


# 리스트 쿼리
$promotion_sql = "
	SELECT
		a.p_no,
		(SELECT w_no FROM work w WHERE w.linked_no = a.p_no AND w.linked_table='promotion' ORDER BY w_no DESC LIMIT 1) as w_no,
		(SELECT s.s_name FROM staff s WHERE s.s_no = a.reg_s_no) as reg_s_name,
		(SELECT t.team_name FROM team t WHERE t.team_code = a.team) as t_name,
		a.s_no,
		a.c_no,
		a.kind,
		a.channel,
		a.promotion_code,
		a.company,
		(select k_name from kind k where k.k_name_code=(select location1 from company c where c.c_no=a.c_no)) as location1,
		(select k_name from kind k where k.k_name_code=(select location2 from company c where c.c_no=a.c_no)) as location2,
		a.reg_num,
		a.p_state,
		a.p_state1,
		a.pres_reward,
		a.pres_money,
		(select count(b.a_no) from application b where b.p_no=a.p_no) as app_num,
		(select count(b.a_no) from application b where b.p_no=a.p_no and b.a_state=1) as accept_num,
		(select count(r.r_no) from report r where r.p_no=a.p_no) as report_num,
		(select count(b.a_no) from application b where b.p_no=a.p_no and b.a_state=3) as cancel_num,
		(select count(*) from application b where b.p_no=a.p_no AND (b.a_state=1 OR b.a_state=3)) as select_posting_num,
		a.posting_num,a.name,a.reg_sdate,a.reg_edate,a.awd_date,a.exp_edate,a.p_memo,a.p_regdate,a.keyword_subject
	FROM
		promotion a
		{$add_where}
	ORDER BY {$add_orderby}
	LIMIT {$offset}, {$num}
";
$promotion_query = mysqli_query($my_db, $promotion_sql);
$promotion_list  = [];

while($promotion_array = mysqli_fetch_array($promotion_query))
{
	// 포스팅 건수가 2건 이상인 경우는, 접수인원 x 포스팅 건수로 해주어야 한다.
	// 50명이 접수하고, 한 사람당 2건씩 써야 하면 50 x 2 = 100 으로 화면에 표시
	if($promotion_array['posting_num'] != 0)
		$posting_num = $promotion_array['posting_num'];

	if ($promotion_array['pres_reward'] != NULL)
		$pres_reward = date("m/d(".$date_short_option[date("w",strtotime($promotion_array['pres_reward']))].")",strtotime($promotion_array['pres_reward']));
	else
		$pres_reward = "-";

	if(!empty($promotion_array['pres_money']))
		$pres_money = "\\{$promotion_array['pres_money']}";
	else
		$pres_money = "";

	$cnt_refund_sql		= "SELECT count(*) FROM application WHERE p_no='{$promotion_array['p_no']}'";
	$cnt_refund_query	= mysqli_query($my_db, $cnt_refund_sql);
	$cnt_refund_array 	= mysqli_fetch_array($cnt_refund_query);

	$total_refund=0;

	if ($cnt_refund_array[0] != 0){
		$total_refund_sql	= "SELECT refund_count FROM application WHERE p_no='{$promotion_array['p_no']}'";
		$total_refund_query	= mysqli_query($my_db, $total_refund_sql);

		while($total_refund_array = mysqli_fetch_array($total_refund_query)){
			$total_refund += $total_refund_array[0];
		}
	}
	if (	$promotion_array['p_state1'] != 1)
		$state_code = $state_option[$promotion_array['p_state']];
	else
		$state_code = "진행중단";

	// 날짜 검색시 background-color:#FF0 표기 Start
	$reg_start_bg 	= "";
	$reg_end_bg 	= "";
	$award_bg 		= "";
	$exp_edate_bg 	= "";
	$pres_reward_bg = "";

	switch($search_date){
		case $promotion_array['reg_sdate'] :
			$reg_start_bg = "background-color:#FF0";
			break;
		case $promotion_array['reg_edate'] :
			$reg_end_bg	= "background-color:#FF0";
			break;
		case $promotion_array['awd_date'] :
			$award_bg = "background-color:#FF0";
			break;
		case date("Y-m-d", strtotime("+1 day", strtotime($promotion_array['exp_edate']))) :
			$exp_edate_bg = "background-color:#FF0";
			break;
		case $promotion_array['pres_reward'] :
			if($search_date)
				$pres_reward_bg = "background-color:#FF0";
			break;
	}
	// 날짜 검색시 background-color:#FF0 표기 End

	$promotion_list[] = array(
		"no"			=> $promotion_array['p_no'],
		"w_no"			=> $promotion_array['w_no'],
		"reg_s_name"	=> $promotion_array['reg_s_name'],
		"kind"			=> $promotion_array['kind'],
		"channel"		=> $promotion_channel_option[$promotion_array['channel']],
		"kind_name"		=> $promotion_kind_option[$promotion_array['kind']],
		"date_ymd"		=> date("Y.m.d",strtotime($promotion_array['p_regdate'])),
		"date_hi"		=> date("H:i",strtotime($promotion_array['p_regdate'])),
		"s_no"			=> $promotion_array['s_no'],
		"t_name"		=> $promotion_array['t_name'],
		"name"			=> $promotion_array['name'],
		"location1"		=> $promotion_array['location1'],
		"location2"		=> $promotion_array['location2'],
		"company"		=> $promotion_array['company'],
		"reg_start"		=> date("m/d(".$date_short_option[date("w",strtotime($promotion_array['reg_sdate']))].")",strtotime($promotion_array['reg_sdate'])),
		"reg_start_bg"	=> $reg_start_bg,
		"reg_end"		=> date("m/d(".$date_short_option[date("w",strtotime($promotion_array['reg_edate']))].")",strtotime($promotion_array['reg_edate'])),
		"reg_end_bg"	=> $reg_end_bg,
		"awd_date"		=> $promotion_array['awd_date'],
		"award"			=> date("m/d(".$date_short_option[date("w",strtotime($promotion_array['awd_date']))].")",strtotime($promotion_array['awd_date'])),
		"award_bg"		=> $award_bg,
		"exp_edate"		=> $promotion_array['exp_edate'],
		"expiration"	=> date("m/d(".$date_short_option[date("w",strtotime($promotion_array['exp_edate']))].")",strtotime($promotion_array['exp_edate'])),
		"exp_edate_bg"	=> $exp_edate_bg,
		"pres_reward"	=> $pres_reward,
		"pres_reward_bg"=> $pres_reward_bg,
		"pres_money"	=> $pres_money,
		"awd_complete"	=> (($promotion_array['p_state']>3)?"10/10":"-"),
		"reg_num"		=> $promotion_array['reg_num'],
		"app_num"		=> $promotion_array['app_num'],
		"accept_num"	=> $promotion_array['accept_num'],
		"p_memo"		=> $promotion_array['p_memo'],
		"p_code"		=> $promotion_array['promotion_code'],
		"select_posting_num"=> $posting_num/$promotion_array['reg_num']*$promotion_array['select_posting_num'],
		"total_refund"	=> $total_refund,
		"report_num"	=> $promotion_array['report_num'],
		"cancel_num"	=> $promotion_array['cancel_num'],
		"cancel_posting_num"=> $posting_num/$promotion_array['reg_num']*$promotion_array['cancel_num'],
		"p_state"		=> $promotion_array['p_state'],
		"p_state1"		=> $promotion_array['p_state1'],
		"state_code"	=> $state_code,
		"state_code1"	=> $state_short_option[$promotion_array['p_state1']],
		"keyword_subject"	=> $promotion_array['keyword_subject']
	);
}
$smarty->assign("promotion_list", $promotion_list);

# Navigation & My Quick
$nav_prd_no  = "105";
$nav_title   = "전체 리스트";
$is_my_quick = "";
switch($search_kind)
{
	case '1':
		$nav_prd_no  = "106";
		$nav_title 	 = "체험단 리스트";
		break;
	case '2':
		$nav_prd_no  = "107";
		$nav_title 	 = "기자단 리스트";
		break;
	case '3':
		$nav_prd_no  = "108";
		$nav_title 	 = "배송체험단 리스트";
		break;
	case '4':
		$nav_prd_no  = "109";
		$nav_title 	 = "체험단(+보상) 리스트";
		break;
	case '5':
		$nav_prd_no  = "110";
		$nav_title 	 = "배송체험단(+보상) 리스트";
		break;
}

if(!empty($nav_prd_no))
{
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
}

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_prd_no", $nav_prd_no);
$smarty->assign("nav_title", $nav_title);

$smarty->assign("sch_team_list", $team_model->getTeamFullNameList());
$smarty->display('promotion_list.html');
?>
