<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/advertising.php');
require('inc/model/MyQuick.php');
require('inc/model/Advertising.php');
require('inc/model/Kind.php');

# Navigation & My Quick
$nav_prd_no  = "188";
$nav_title   = "NOSP 광고 결과보고(통계)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);

$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

$sch_result_type    = isset($_GET['sch_result_type']) ? $_GET['sch_result_type'] : "date";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_adv_s_date     = isset($_GET['sch_adv_s_date']) ? $_GET['sch_adv_s_date'] : $month_val;
$sch_adv_e_date     = isset($_GET['sch_adv_e_date']) ? $_GET['sch_adv_e_date'] : $today_val;
$sch_adv_date_type  = isset($_GET['sch_adv_date_type']) ? $_GET['sch_adv_date_type'] : "month";
$sch_date_kind      = isset($_GET['sch_date_kind']) ? $_GET['sch_date_kind'] : "";
$sch_date_s_time    = isset($_GET['sch_date_s_time']) ? $_GET['sch_date_s_time'] : "";
$sch_date_e_time    = isset($_GET['sch_date_e_time']) ? $_GET['sch_date_e_time'] : "";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_media          = 265;
$sch_product        = isset($_GET['sch_product']) ? $_GET['sch_product'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_is_complete    = isset($_GET['sch_is_complete']) ? $_GET['sch_is_complete'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

$smarty->assign("sch_result_type", $sch_result_type);

if(!empty($sch_adv_s_date)){
    $smarty->assign("sch_adv_s_date", $sch_adv_s_date);
}

if(!empty($sch_adv_e_date)){
    $smarty->assign("sch_adv_e_date", $sch_adv_e_date);
}
$smarty->assign("sch_adv_date_type", $sch_adv_date_type);

if($sch_date_kind != ""){
    $smarty->assign("sch_date_kind", $sch_date_kind);
}

if($sch_date_s_time != ""){
    $smarty->assign("sch_date_s_time", $sch_date_s_time);
}

if($sch_date_e_time != ""){
    $smarty->assign("sch_date_e_time", $sch_date_e_time);
}

if(!empty($sch_no)){
    $smarty->assign("sch_no", $sch_no);
}

if(!empty($sch_media)){
    $smarty->assign("sch_media", $sch_media);
}

if(!empty($sch_product)){
    $smarty->assign("sch_product", $sch_product);
}

if(!empty($sch_state)){
    $smarty->assign("sch_state", $sch_state);
}

if(!empty($sch_agency)){
    $smarty->assign("sch_agency", $sch_agency);
}

if(!empty($sch_is_complete)){
    $smarty->assign("sch_is_complete", $sch_is_complete);
}

# 브랜드 옵션
if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("date_kind_option", getDateChartOption());
$smarty->assign("hour_kind_option", getHourOption());
$smarty->assign("adv_media_option", getAdvertisingMediaOption());
$smarty->assign("adv_product_option", getAdvertisingProductOption());
$smarty->assign("adv_state_option", getAdvertisingStateOption());
$smarty->assign("adv_state_color_option", getAdvertisingStateColorOption());
$smarty->assign("adv_agency_option", getAdvertisingAgencyOption());

$smarty->display('advertising_result_stats.html');
?>
