<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

// 접근 권한
if (!$session_s_no){
	$smarty->display('access_company_error.html');
	exit;
}

$reject = false;
if($session_staff_state == '2'){ // 프리랜서의 경우 기본 접근제한으로 설정
	$reject = true;
}

for($i=0 ; $i < sizeof($access_accept_list) ; $i++){ // 접근허용 리스트
	if($session_s_no == $access_accept_list[$i][1]){
		if($_GET['sch_prd'] == $access_accept_list[$i][2] || $access_accept_list[$i][2] == 'all'){
			$reject = false;
		}
	}
}

if($reject){
	for($i=0 ; $i < sizeof($access_reject_list) ; $i++){ // 접근제한 리스트
		if($session_s_no == $access_reject_list[$i][1]){
			if($_GET['sch_prd'] == $access_reject_list[$i][2] || $access_reject_list[$i][2] == 'all'){
				$reject = true;
			}
		}
	}
}
if($reject && !!$_GET['sch_prd']){
	$smarty->display('access_error.html');
	exit;
}

$proc=(isset($_POST['process']))?$_POST['process']:"";


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="1=1";

$set_get=isset($_GET['sch_set'])?$_GET['sch_set']:"";
$sch_set_w_no_get=isset($_GET['sch_set_w_no'])?$_GET['sch_set_w_no']:"";


$q_sch_get=isset($_GET['q_sch'])?$_GET['q_sch']:"";
$sch_get=isset($_GET['sch'])?$_GET['sch']:"";

if(!$sch_set_w_no_get) // 업무 세트 리스트는 예외
	$new_get=isset($_GET['new'])?$_GET['new']:"";
$out_memo_get=isset($_GET['out_memo'])?$_GET['out_memo']:"";

$sch_prd_g1_get=isset($_GET['sch_prd_g1'])?$_GET['sch_prd_g1']:"";
$sch_prd_g2_get=isset($_GET['sch_prd_g2'])?$_GET['sch_prd_g2']:"";
$sch_prd_get=isset($_GET['sch_prd'])?$_GET['sch_prd']:"";

$sch_task_run_regdate_get=isset($_GET['sch_task_run_regdate'])?$_GET['sch_task_run_regdate']:"";
$sch_work_state_get=isset($_GET['sch_work_state'])?$_GET['sch_work_state']:"";
$sch_wd_dp_no_state_get=isset($_GET['sch_wd_dp_no_state'])?$_GET['sch_wd_dp_no_state']:"";
$sch_s_no_get=isset($_GET['sch_s_no'])?$_GET['sch_s_no']:"";
$sch_req_s_name_get=isset($_GET['sch_req_s_name'])?$_GET['sch_req_s_name']:"";
$sch_run_s_name_get=isset($_GET['sch_run_s_name'])?$_GET['sch_run_s_name']:"";
if($session_staff_state == '2'){ // freelancer 계정은 본인 건만 조회 가능
	$sch_run_s_name_get=$session_name;
}
$sch_work_time_s_get=isset($_GET['sch_work_time_s'])?$_GET['sch_work_time_s']:"";
$sch_work_time_e_get=isset($_GET['sch_work_time_e'])?$_GET['sch_work_time_e']:"";
$sch_dp_c_no_get=isset($_GET['sch_dp_c_no'])?$_GET['sch_dp_c_no']:"";
$sch_wd_c_no_get=isset($_GET['sch_wd_c_no'])?$_GET['sch_wd_c_no']:"";
$sch_extension_get=isset($_GET['sch_extension'])?$_GET['sch_extension']:"";
$sch_extension_date_get=isset($_GET['sch_extension_date'])?$_GET['sch_extension_date']:"";

$sch_evaluation_get=isset($_GET['sch_evaluation'])?$_GET['sch_evaluation']:"";
$sch_evaluation_memo_get=isset($_GET['sch_evaluation_memo'])?$_GET['sch_evaluation_memo']:"";

$sch_c_name_get=isset($_GET['sch_c_name'])?$_GET['sch_c_name']:"";
$sch_t_keyword_get=isset($_GET['sch_t_keyword'])?$_GET['sch_t_keyword']:"";
$sch_r_keyword_get=isset($_GET['sch_r_keyword'])?$_GET['sch_r_keyword']:"";
if(!$sch_set_w_no_get) // 업무 세트 리스트는 예외
	$sch_w_no_get=isset($_GET['sch_w_no'])?$_GET['sch_w_no']:"";
$sch_dp_no_get=isset($_GET['sch_dp_no'])?$_GET['sch_dp_no']:"";
$sch_wd_no_get=isset($_GET['sch_wd_no'])?$_GET['sch_wd_no']:"";
$sch_task_req_get=isset($_GET['sch_task_req'])?$_GET['sch_task_req']:"";
$sch_task_run_get=isset($_GET['sch_task_run'])?$_GET['sch_task_run']:"";


//QUICK SEARCH
if(!empty($q_sch_get)) { // 퀵서치
	if($q_sch_get == "1"){ //요청안된건
		$add_where.=" AND (w.work_state='1' OR w.work_state='2')";
	}elseif($q_sch_get == "2"){ //요청중인건
		$add_where.=" AND w.work_state='3'";
	}elseif($q_sch_get == "3"){ //진행중인건
		$add_where.=" AND (w.work_state='4' OR w.work_state='5')";
	}elseif($q_sch_get == "4"){ //진행완료건
		$add_where.=" AND w.work_state='6'";
	}elseif($q_sch_get == "5"){ //연장여부 확인요청건
		// D-10일 ~ +5일
		$add_where.=" AND w.work_state='6' AND w.extension_date <= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL 10 DAY), '%Y-%m-%d') AND w.extension_date >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -5 DAY), '%Y-%m-%d') AND (w.extension='2' OR w.extension='0' OR w.extension IS NULL)";
	}
	$smarty->assign("q_sch",$q_sch_get);
}

if(!empty($sch_get)) { // 상세검색 펼치기
	$smarty->assign("sch",$sch_get);
}
if(!empty($new_get)) { // 업무 추가 요청 펼치기
	$smarty->assign("new",$new_get);
}
if(!empty($out_memo_get)) { // 출금업체 메모 보기 펼치기
	$smarty->assign("out_memo",$out_memo_get);
}


// 상품에 따른 그룹 코드 설정
for ($arr_i = 1 ; $arr_i < count($product_list) ; $arr_i++ ){
	if($product_list[$arr_i]['prd_no'] == $sch_prd_get){
		$sch_prd_g1_get = $product_list[$arr_i]['g1_code'];
		$sch_prd_g2_get = $product_list[$arr_i]['g2_code'];
		break;
	}
}

$smarty->assign("sch_prd_g1",$sch_prd_g1_get);
$smarty->assign("sch_prd_g2",$sch_prd_g2_get);

if(!empty($sch_s_no_get)) { // 업체 담당자
	if($sch_s_no_get != 'all'){
		$add_where.=" AND w.s_no='".$sch_s_no_get."'";
	}
}else{
	if(permissionNameCheck($session_permission, "마케터") && !permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "외주관리자") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "서비스운영")){
		$add_where.=" AND w.s_no='".$session_s_no."'";
	}
}
$smarty->assign("sch_s_no",$sch_s_no_get);

if(!empty($sch_req_s_name_get)) { // 업무 요청자
	$add_where.=" AND w.task_req_s_no IN (SELECT s_no FROM staff WHERE s_name LIKE '%".$sch_req_s_name_get."%')";
	$smarty->assign("sch_req_s_name",$sch_req_s_name_get);
}

if(!empty($sch_run_s_name_get)) { // 업무 처리자
    $add_where .= " AND w.task_run_s_no IN(SELECT s_no FROM staff WHERE s_name like '%{$sch_run_s_name_get}%')";
}

if(!empty($sch_work_time_s_get) || $sch_work_time_s_get == '0') { // work time s
	if($sch_work_time_s_get == "null"){
		$add_where.=" AND w.work_time IS NULL";
	}else{
		$add_where.=" AND w.work_time>='".$sch_work_time_s_get."'";
	}
	$smarty->assign("sch_work_time_s",$sch_work_time_s_get);
}

if(!empty($sch_work_time_e_get) || $sch_work_time_e_get == '0') { // work time e
	if($sch_work_time_e_get == "null"){
		$add_where.=" AND w.work_time IS NULL";
	}else{
		$add_where.=" AND w.work_time<='".$sch_work_time_e_get."'";
	}
	$smarty->assign("sch_work_time_e",$sch_work_time_e_get);
}


if(!empty($sch_prd_get)) { // 상품
	$add_where.=" AND w.prd_no='".$sch_prd_get."'";
}else{
	if($session_staff_state == '1'){ // active 계정
		if($sch_prd_g2_get){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
			$add_where.=" AND (SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)='".$sch_prd_g2_get."'";
		}elseif($sch_prd_g1_get){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
			$add_where.=" AND (SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))='".$sch_prd_g1_get."'";
		}
	}elseif($session_staff_state == '2'){ // freelancer 계정
		$add_or_where="";
		for($i=0 ; $i < sizeof($access_accept_list) ; $i++){ // 접근허용 리스트
			if($session_s_no == $access_accept_list[$i][1]){
				if(!!$add_or_where){
					$add_or_where.=" OR w.prd_no='".$access_accept_list[$i][2]."'";
				}else{
					$add_or_where.=" w.prd_no='".$access_accept_list[$i][2]."'";
				}
			}
		}
		if(!!$add_or_where)
			$add_where.=" AND (".$add_or_where.")";
	}
}
$smarty->assign("sch_prd",$sch_prd_get); // 전체 업무내역에 자주하는 업무가 나타게 하기위해 empty 밖에서 처리함

if(!empty($sch_task_run_regdate_get)) { // 업무완료일
	$add_where.=" AND w.task_run_regdate like '".$sch_task_run_regdate_get."%'";
	$smarty->assign("sch_task_run_regdate",$sch_task_run_regdate_get);
}
if(!empty($sch_work_state_get)&&empty($q_sch_get)) { // 진행상태 [퀵서치를 안한경우]
	$add_where.=" AND w.work_state='".$sch_work_state_get."'";
	$smarty->assign("sch_work_state",$sch_work_state_get);
}
if(!empty($sch_wd_dp_no_state_get)) { // 지급/입금상태
	if($sch_wd_dp_no_state_get == '1'){
		$add_where.=" AND w.wd_no IS NOT NULL AND ((SELECT wd_state FROM withdraw wd where wd.wd_no=w.wd_no) = '1' OR (SELECT wd_state FROM withdraw wd where wd.wd_no=w.wd_no) = '2')";
	}elseif($sch_wd_dp_no_state_get == '2'){
		$add_where.=" AND (SELECT wd_state FROM withdraw wd where wd.wd_no=w.wd_no) = '3'";
	}elseif($sch_wd_dp_no_state_get == '3'){
		$add_where.=" AND w.dp_no IS NOT NULL AND ((SELECT dp_state FROM deposit dp where dp.dp_no=w.dp_no) = '1' OR (SELECT dp_state FROM deposit dp where dp.dp_no=w.dp_no) = '3')";
	}elseif($sch_wd_dp_no_state_get == '4'){
		$add_where.=" AND w.dp_no IS NOT NULL";
	}
	$smarty->assign("sch_wd_dp_no_state",$sch_wd_dp_no_state_get);
}

if($sch_dp_no_get == 'null') { // dp_no is null
	$add_where.=" AND w.dp_no is null";
	$smarty->assign("sch_dp_no_get",$sch_dp_no_get);
}

if($sch_wd_no_get == 'null') { // wd_no is null
	$add_where.=" AND w.wd_no is null";
	$smarty->assign("sch_wd_no_get",$sch_wd_no_get);
}

if(!empty($sch_dp_c_no_get)) { // 입금업체(입금처)
	if($sch_dp_c_no_get != "null"){
		$add_where.=" AND w.dp_c_no='".$sch_dp_c_no_get."'";
	}else{
		$add_where.=" AND (w.dp_c_no IS NULL OR w.dp_c_no ='0')";
	}

	$smarty->assign("sch_dp_c_no",$sch_dp_c_no_get);
}

if(!empty($sch_wd_c_no_get)) { // 출금업체(지급처)
	if($sch_wd_c_no_get != "null"){
		$add_where.=" AND w.wd_c_no='".$sch_wd_c_no_get."'";
	}else{
		$add_where.=" AND (w.wd_c_no IS NULL OR w.wd_c_no ='0')";
	}

	$smarty->assign("sch_wd_c_no",$sch_wd_c_no_get);
}
if(!empty($sch_extension_get)) { // 연장여부
	$add_where.=" AND w.extension='".$sch_extension_get."'";
	$smarty->assign("sch_extension",$sch_extension_get);
}
if(!empty($sch_extension_date_get)) { //연장여부 마감일
	$add_where.=" AND w.extension_date='".$sch_extension_date_get."'";
	$smarty->assign("sch_extension_date",$sch_extension_date_get);
}

if(!empty($sch_evaluation_get)) { //업무평가
	if(permissionNameCheck($session_permission,"마케터") || permissionNameCheck($session_permission,"대표") || permissionNameCheck($session_permission,"마스터관리자")){
		$add_where.=" AND w.evaluation='".$sch_evaluation_get."'";
	}else{
		$add_where.=" AND w.evaluation='".$sch_evaluation_get."' AND ((SELECT staff_state FROM staff WHERE s_no=w.task_run_s_no)='2' || (SELECT staff_state FROM staff WHERE s_no=w.task_run_s_no)='3'	)";
	}
	$smarty->assign("sch_evaluation",$sch_evaluation_get);
}
if(!empty($sch_evaluation_memo_get)) { //업무평가 메모
	if($sch_evaluation_memo_get == '1'){
		$add_where.=" AND w.evaluation_memo IS NOT NULL AND w.evaluation_memo <> ''";
		$smarty->assign("sch_evaluation_memo",$sch_evaluation_memo_get);
	}elseif($sch_evaluation_memo_get == '2'){
		$add_where.=" AND w.evaluation_memo = NULL OR w.evaluation_memo = '' ";
		$smarty->assign("sch_evaluation_memo",$sch_evaluation_memo_get);
	}
}



if(!empty($sch_c_name_get)) { // 업체명
	$add_where.=" AND w.c_name like '%".$sch_c_name_get."%'";
	$smarty->assign("sch_c_name",$sch_c_name_get);
}
if(!empty($sch_t_keyword_get)) { // 타겟키워드 및 타겟채널
	$add_where.=" AND w.t_keyword like '%".$sch_t_keyword_get."%'";
	$smarty->assign("sch_t_keyword",$sch_t_keyword_get);
}
if(!empty($sch_r_keyword_get)) { // 노출키워드
	$add_where.=" AND w.r_keyword like '%".$sch_r_keyword_get."%'";
	$smarty->assign("sch_r_keyword",$sch_r_keyword_get);
}
if(!empty($sch_w_no_get)) { // 업무번호
	$add_where.=" AND w.w_no ='".$sch_w_no_get."'";
	$smarty->assign("sch_w_no",$sch_w_no_get);
}
if(!empty($sch_task_req_get)) { // 업무요청
	$add_where.=" AND w.task_req like '%".$sch_task_req_get."%'";
	$smarty->assign("sch_task_req",$sch_task_req_get);
}
if(!empty($sch_task_run_get)) { // 업무진행
	$add_where.=" AND w.task_run like '%".$sch_task_run_get."%'";
	$smarty->assign("sch_task_run",$sch_task_run_get);
}
if(!!$sch_set_w_no_get){ // 업무 세트의 태그 조건
	$add_where.=" AND w.set_tag IS NOT NULL AND w.set_tag LIKE '%".$sch_set_w_no_get."%'";
	$smarty->assign("sch_set_w_no",$sch_set_w_no_get);

	// 업무 세트 정보를 가져옴
	$work_set_info_sql="
			SELECT
					w_s_i.no,
					w_s_i.w_no,
					w_s_i.title,
					w_s_i.memo,
					w_s_i.regdate
			FROM work_set_info w_s_i
			WHERE
					w_s_i.w_no = {$sch_set_w_no_get}
			";
	//echo $work_set_info_sql; //exit;
	$work_set_info_result=mysqli_query($my_db,$work_set_info_sql);
	$work_set_info=mysqli_fetch_array($work_set_info_result);
	$smarty->assign("ws_info_no",$work_set_info['no']);
	$smarty->assign("ws_info_w_no",$work_set_info['w_no']);
	$smarty->assign("ws_info_title",$work_set_info['title']);
	$smarty->assign("ws_info_memo",$work_set_info['memo']);
	$smarty->assign("ws_info_regdate",$work_set_info['regdate']);
}


// 정렬순서 토글 & 필드 지정
$add_orderby="";
$order=isset($_GET['od'])?$_GET['od']:"";
$order_type=isset($_GET['by'])?$_GET['by']:"";

$toggle=$order_type?"asc":"desc";
$order_field=array('','task_req_dday','task_run_dday','extension_date');
if($order && $order<4) {
	$add_orderby .= " ISNULL($order_field[$order]) ASC, $order_field[$order] $toggle,";
	$add_where .= " AND ((DATE_FORMAT(NOW(), '%Y-%m-%d') <= $order_field[$order]) OR ((w.work_state = '3' OR w.work_state = '4' OR w.work_state = '5') AND $order_field[$order] IS NOT NULL))";
	$smarty->assign("order",$order);
	$smarty->assign("order_type",$order_type);
}

if($q_sch_get == "5"){ // 퀵서치의 연장여부 확인 요청건은 연장여부 마감일순으로 정렬
	if(!!$sch_set_w_no_get){ // 업무 세트의 경우 세트 태그업무는 무조건 최상단 표시
		$add_orderby.=" w.w_no = '{$sch_set_w_no_get}' DESC, extension_date asc ";
	}else{
		$add_orderby.=" extension_date asc ";
	}
}else{
	if(!!$sch_set_w_no_get){ // 업무 세트의 경우 세트 태그업무는 무조건 최상단 표시
		$add_orderby.=" w.w_no = '{$sch_set_w_no_get}' DESC, w.priority ASC, ";
	}
	$add_orderby.=" w_no desc ";
}

if(!!$sch_set_w_no_get){ // 업무 세트의 경우 세트 태그업무는 무조건 포함 시키기
	$add_where .= " OR w_no = '{$sch_set_w_no_get}' ";
}

// 리스트 쿼리
$work_sql="
	SELECT
		*,
		(SELECT k_name FROM kind WHERE k_name_code=w.k_name_code) AS k_name,
		(SELECT regdate FROM withdraw wd where wd.wd_no=w.wd_no) as wd_regdate,
		(SELECT wd_date FROM withdraw wd where wd.wd_no=w.wd_no) as wd_date,
		(SELECT regdate FROM deposit dp where dp.dp_no=w.dp_no) as dp_regdate,
		(SELECT dp_date FROM deposit dp where dp.dp_no=w.dp_no) as dp_date,
		(SELECT s_name FROM staff s where s.s_no=w.task_req_s_no) as task_req_s_name,
		(SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name,
		(SELECT staff_state FROM staff s where s.s_no=w.task_run_s_no) as task_run_staff_state,
		(SELECT s_name from staff s where s.s_no=w.s_no) as s_no_name,
		(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1_name,
		(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2_name,
		(SELECT title from product prd where prd.prd_no=w.prd_no) as prd_no_name,
		(SELECT prd.wd_dp_state FROM product prd	WHERE prd.prd_no = w.prd_no) as wd_dp_state,
		(SELECT prd.work_time_method FROM product prd	WHERE prd.prd_no = w.prd_no) as work_time_method
	FROM
		work w
	WHERE
		$add_where
	ORDER BY
		$add_orderby
	";

//echo $work_sql; //exit;
// 리스트 최대치 500
$work_sql .= "LIMIT 500";


//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "w_no")
	->setCellValue('B3', "작성일")
	->setCellValue('C3', "진행상태")
	->setCellValue('D3', "희망완료일")
	->setCellValue('E3', "예정완료일")
	->setCellValue('F3', "업무완료일")
	->setCellValue('G3', "업체명")
	->setCellValue('H3', "업체담당자")
	->setCellValue('I3', "상품명")
	->setCellValue('J3', "업무요청자")
	->setCellValue('K3', "타겟키워드")
	->setCellValue('L3', "노출키워드")
	->setCellValue('M3', "업무요청내용")
	->setCellValue('N3', "업무처리자")
	->setCellValue('O3', "업무진행내용")
	->setCellValue('P3', "연장여부 마감일")
	->setCellValue('Q3', "연장여부")
	->setCellValue('R3', "입금액(VAT별도)")
	->setCellValue('S3', "입금액(VAT포함)")
	->setCellValue('T3', "입금업체(입금처)")
	->setCellValue('U3', "입금등록일")
	->setCellValue('V3', "입금일")
	->setCellValue('W3', "출금액(VAT별도)")
	->setCellValue('X3', "출금액(VAT포함)")
	->setCellValue('Y3', "출금업체(지급처)")
	->setCellValue('Z3', "출금등록일")
	->setCellValue('AA3', "출금일");


// 리스트 내용
$i=4;

$idx_no=1;
$result= mysqli_query($my_db, $work_sql);
while($work_array = mysqli_fetch_array($result)){
	if($sch_set_w_no_get == $work_array['w_no']){ // 세트의 주체는 생략
		continue;
	}
	// 첫번째 문자가 = 일경우 '을 삽입
	foreach($work_array as $key => $value) {
		if($work_array[$key][0] === '='){
			$work_array[$key] = "'".$work_array[$key];
		}
	}

	if(!empty($work_array['regdate'])) {
		$regdate_day_value = date("Y/m/d",strtotime($work_array['regdate']));
		$regdate_time_value = date("H:i",strtotime($work_array['regdate']));
	}else{
		$regdate_day_value="";
		$regdate_time_value="";
	}

	if(!empty($work_array['dp_price'])){
		$dp_price_value=number_format($work_array['dp_price']);
	}else{
		$dp_price_value="";
	}
	if(!empty($work_array['dp_price_vat'])){
		$dp_price_vat_value=number_format($work_array['dp_price_vat']);
	}else{
		$dp_price_vat_value="";
	}
	if(!empty($work_array['dp_regdate'])) {
		$dp_regdate_day_value = date("Y/m/d",strtotime($work_array['dp_regdate']));
		$dp_regdate_time_value = date("H:i",strtotime($work_array['dp_regdate']));
	}else{
		$dp_regdate_day_value="";
		$dp_regdate_time_value="";
	}

	if(!empty($work_array['wd_price'])){
		$wd_price_value=number_format($work_array['wd_price']);
	}else{
		$wd_price_value="";
	}
	if(!empty($work_array['wd_price_vat'])){
		$wd_price_vat_value=number_format($work_array['wd_price_vat']);
	}else{
		$wd_price_vat_value="";
	}
	if(!empty($work_array['wd_regdate'])) {
		$wd_regdate_day_value = date("Y/m/d",strtotime($work_array['wd_regdate']));
		$wd_regdate_time_value = date("H:i",strtotime($work_array['wd_regdate']));
	}else{
		$wd_regdate_day_value="";
		$wd_regdate_time_value="";
	}

	if($work_array['extension'] == '0')
		$work_array['extension'] = "";

	// 입금업체 이름 찾기
	$dp_c_no_name2 = "";
	for ($arr_i = 1 ; $arr_i < count($dp_company_list) ; $arr_i++ ){
		if($work_array['dp_c_no'] == $dp_company_list[$arr_i][1] && $work_array['prd_no'] == $dp_company_list[$arr_i][2]){
			if(!! $dp_company_list[$arr_i][3]){
				$dp_c_no_name2 = $dp_company_list[$arr_i][3];
			}else{
				$dp_c_no_name2 = $dp_company_list[$arr_i][0];
			}
		}
	}

	// 출금업체 이름 찾기
	$wd_c_no_name2 = "";
	for ($arr_i = 1 ; $arr_i < count($wd_company_list) ; $arr_i++ ){
		if($work_array['wd_c_no'] == $wd_company_list[$arr_i][1] && $work_array['prd_no'] == $wd_company_list[$arr_i][2]){
			$wd_c_no_name2 = $wd_company_list[$arr_i][3];
		}
	}

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$i, $work_array['w_no'])
	->setCellValue('B'.$i, $work_array['regdate'])
	->setCellValue('C'.$i, $work_state[$work_array['work_state']][0])
	->setCellValue('D'.$i, $work_array['task_req_dday'])
	->setCellValue('E'.$i, $work_array['task_run_dday'])
	->setCellValue('F'.$i, $work_array['task_run_regdate'])
	->setCellValue('G'.$i, $work_array['c_name'])
	->setCellValue('H'.$i, $work_array['s_no_name'])
	->setCellValue('I'.$i, $work_array['prd_no_name'])
	->setCellValue('J'.$i, $work_array['task_req_s_name'])
	->setCellValue('K'.$i, $work_array['t_keyword'])
	->setCellValue('L'.$i, $work_array['r_keyword'])
	->setCellValue('M'.$i, $work_array['task_req'])
	->setCellValue('N'.$i, $work_array['task_run_s_name'])
	->setCellValue('O'.$i, $work_array['task_run'])
	->setCellValue('P'.$i, isset($work_array['extension_date'])?date("Y-m-d",strtotime($work_array['extension_date'])):"")
	->setCellValue('Q'.$i, $work_extension[$work_array['extension']][0])
	->setCellValue('R'.$i, $dp_price_value)
	->setCellValue('S'.$i, $dp_price_vat_value)
	->setCellValue('T'.$i, $dp_c_no_name2)
	->setCellValue('U'.$i, $work_array['dp_regdate'])
	->setCellValue('V'.$i, $work_array['dp_date'])
	->setCellValue('W'.$i, $wd_price_value)
	->setCellValue('X'.$i, $wd_price_vat_value)
	->setCellValue('Y'.$i, $wd_c_no_name2)
	->setCellValue('Z'.$i, $work_array['wd_regdate'])
	->setCellValue('AA'.$i, $work_array['wd_date']);

	$i = $i+1;
	$idx_no++;
}

if($i > 1)
	$i = $i-1;

//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:AA1');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:G2');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $work_set_info['title']);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', $work_set_info['memo']);
$objPHPExcel->getActiveSheet()->getStyle('A1:A1')->getFont()->setSize(60);
$objPHPExcel->getActiveSheet()->getStyle('A1:A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('A2:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('A2:A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
$objPHPExcel->getActiveSheet()->getStyle('A2:A2')->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('A2:A2')->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle('A3:AA'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:AA'.$i)->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:AA3')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle('A4:AA'.$i)->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle('A3:AA3')->getFont()->setSize(11);

$objPHPExcel->getActiveSheet()->getStyle('A3:AA3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
															->getStartColor()->setARGB('00c4bd97');
$objPHPExcel->getActiveSheet()->getStyle('A3:AA3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:AA'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('I4:I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('O4:O'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('P4:P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Q4:Q'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('R4:R'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('S4:S'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('T4:T'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('U4:U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('V4:V'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('W4:W'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('X4:X'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('Y4:Y'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Z4:Z'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('AA4:AA'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$i)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$i)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$i)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$i)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('O4:O'.$i)->getAlignment()->setWrapText(true);




$i2_length = $i;
$i2=1;

while($i2_length){
	if($i2 > 2)
		$objPHPExcel->getActiveSheet()->getRowDimension($i2)->setRowHeight(20);

	if($i2 > 3 && $i2 % 2 == 1)
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i2.':AA'.$i2)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ebf1de');

	$i2++;
	$i2_length--;
}

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(12);

$objPHPExcel->getActiveSheet()->setTitle('업무 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);




$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_".$session_name."_업무세트 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
