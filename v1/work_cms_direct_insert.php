<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

require('Classes/PHPExcel.php');
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/model/Custom.php');

# 기본 엑셀 변수 설정
$direct_model  = Custom::Factory();
$direct_model->setMainInit("work_cms_direct", "w_no");

$regdate        = date('Y-m-d H:i:s');
$log_c_no       = isset($_POST['direct_warehouse']) ? $_POST['direct_warehouse'] : 2809;
$direct_file    = $_FILES["direct_file"];

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($direct_file['name']) && !empty($direct_file['name'])){
    $upload_file_path = add_store_file($direct_file, "upload_management");
    $upload_file_name = $direct_file['name'];
}

$upload_data  = array(
    "upload_type"   => "13",
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $session_s_no,
    "regdate"       => $regdate,
);
$upload_model->insert($upload_data);

$file_no         = $upload_model->getInsertId();
$excel_file_path = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($excel_file_path);
$excel->setActiveSheetIndex(0);

$objWorksheet       = $excel->getActiveSheet();
$totalRow           = $objWorksheet->getHighestRow();
$ins_data_list      = [];

# DB 데이터 시작
for ($i = 2; $i <= $totalRow; $i++)
{
    $delivery_date_val  = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //배송일
    $delivery_date      = date("Y-m-d", strtotime($delivery_date_val));
    $direct_type        = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //구분
    $delivery_type      = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   //택배사
    $delivery_no        = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   //택배사
    $prd_info           = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   //상품명
    $quantity           = (int)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));      //수량
    $recipient          = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   //수령자
    $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   //수령자전화
    $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   //수령지

    $ins_data_list[] = array(
        "log_c_no"          => $log_c_no,
        "delivery_date"     => $delivery_date,
        "direct_type"       => $direct_type,
        "delivery_type"     => $delivery_type,
        "delivery_no"       => $delivery_no,
        "prd_info"          => $prd_info,
        "quantity"          => $quantity,
        "recipient"         => $recipient,
        "recipient_hp"      => $recipient_hp,
        "recipient_addr"    => $recipient_addr,
        "file_no"           => $file_no,
        "reg_s_no"          => $session_s_no,
        "regdate"           => $regdate,
    );
}

if(!empty($ins_data_list))
{
    if($direct_model->multiInsert($ins_data_list))
    {
        exit("<script>alert('직택배리스트에 반영 되었습니다.');location.href='work_cms_direct_list.php?sch_file_no={$file_no}';</script>");
    }else{
        exit("<script>alert('직택배리스트 반영에 실패했습니다.');location.href='waple_upload_management.php';</script>");
    }
}

exit("<script>alert('직택배리스트 반영할 데이터가 없습니다.');location.href='waple_upload_management.php';</script>");


?>
