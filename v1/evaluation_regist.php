<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/helper/_navigation.php');
require('inc/helper/evaluation.php');
require('inc/model/Staff.php');
require('inc/model/Evaluation.php');
require('inc/model/Message.php');

# Model Init
$ev_model = Evaluation::Factory();

# 프로세스 처리
$process 		= isset($_POST['process']) ? $_POST['process'] : "";
$is_admin		= false;
$is_editable 	= false;

if ($process == "modify_change_date") # 평가기간 날짜 변경
{
	$ev_no 		= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
	$date_type 	= (isset($_POST['type'])) ? $_POST['type'] : "";
	$date_value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	$upd_data 	= array(
		"ev_no" 	=> $ev_no,
		$date_type	=> (empty($date_value)) ? "NULL" : $date_value
	);

	if (!$ev_model->update($upd_data))
		echo "기간 변경에 실패 하였습니다.";
	else
		echo "기간 변경이 되었습니다.";
	exit;
}
elseif ($process == "f_ev_state") # 진행상태 자동저장
{
    $ev_no		= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
    $value		= (isset($_POST['val'])) ? $_POST['val'] : "";

	$upd_data 	= array(
		"ev_no" 	=> $ev_no,
		"ev_state"	=> (empty($value)) ? "1" : $value
	);

	if(!$ev_model->update($upd_data))
		echo "진행상태 변경에 실패했습니다";
	else
		exit("<script>alert('진행상태를 변경했습니다');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
	exit;
}
elseif ($process == "f_subject") # 평가명 자동저장
{
	$ev_no 		= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	$upd_data 	= array(
		"ev_no" 	=> $ev_no,
		"subject"	=> (empty($value)) ? "NULL" : $value
	);

	if (!$ev_model->update($upd_data))
		echo "평가명 저장에 실패 하였습니다.";
	else
		echo "평가명이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_admin_s_no")	# 평가담당자 자동저장
{
	$ev_no 		= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	$upd_data 	= array(
		"ev_no" 		=> $ev_no,
		"admin_s_no"	=> (empty($value)) ? "NULL" : $value
	);

	if(!$ev_model->update($upd_data))
		echo "평가담당자 저장에 실패했습니다";
	else
		exit("<script>alert('평가담당자가 저장 되었습니다');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
	exit;
}
elseif ($process == "f_ev_u_set_no") # 평가지 자동저장
{
	$ev_no 		= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	$upd_data 	= array(
		"ev_no" 		=> $ev_no,
		"ev_u_set_no"	=> (empty($value)) ? "NULL" : $value
	);

	if (!$ev_model->update($upd_data))
		echo "평가지 저장에 실패 하였습니다.";
	else
		echo "평가지가 저장 되었습니다.";
	exit;
}
elseif ($process == "f_description") # 설명 자동저장
{
	$ev_no 		= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
	$value 		= (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

	$upd_data 	= array(
		"ev_no" 		=> $ev_no,
		"description"	=> (empty($value)) ? "NULL" : $value
	);

	if (!$ev_model->update($upd_data))
		echo "설명 저장에 실패 하였습니다.";
	else
		echo "설명이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_guide") # 가이드 업로드
{
	$ev_no 		= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
	$ev_system  = $ev_model->getItem($ev_no);

	$origin_guide_path = $ev_system['guide_path'];
	$origin_guide_name = $ev_system['guide_name'];
	$upload_guide_path = "";
	$upload_guide_name = "";

	$guide_name = isset($_POST['guide_name']) ? array($_POST['guide_name']) : "";
	$guide_path = isset($_POST['guide_path']) ? array($_POST['guide_path']) : "";

	if(!empty($guide_path))
	{
		$guide_reads  = move_store_files($guide_path, "dropzone_tmp", "evaluation");
		$guide_names  = implode(',', $guide_name);
		$guide_paths  = implode(',', $guide_reads);

		$upload_guide_path = $guide_paths;
		$upload_guide_name = $guide_names;
	}

	$final_guide_path = !empty($origin_guide_path) ? $origin_guide_path.",".$upload_guide_path : $upload_guide_path;
	$final_guide_name = !empty($origin_guide_name) ? $origin_guide_name.",".$upload_guide_name : $upload_guide_name;

	$upd_data = array(
		"ev_no" 		=> $ev_no,
		"guide_path"	=> addslashes(trim($final_guide_path)),
		"guide_name"	=> addslashes(trim($final_guide_name)),
	);

	if (!$ev_model->update($upd_data))
		$result = false;
	else
		$result = true;

	echo json_encode(array("result" => $result));
	exit;
}
elseif ($process == "f_guide_delete")
{
	$ev_no 		= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
	$ev_system  = $ev_model->getItem($ev_no);

	$guide_name = isset($_POST['guide_name']) ? $_POST['guide_name'] : "";
	$guide_path = isset($_POST['guide_path']) ? $_POST['guide_path'] : "";

	$origin_guide_path = explode(",", $ev_system['guide_path']);
	$origin_guide_name = explode(",", $ev_system['guide_name']);

	if(array_search($guide_path, $origin_guide_path) !== false){
		$file_idx = array_search($guide_path, $origin_guide_path);
		unset($origin_guide_path[$file_idx]);
		unset($origin_guide_name[$file_idx]);
		del_files(array($guide_path));
	}

	$final_guide_path = implode(",", $origin_guide_path);
	$final_guide_name = implode(",", $origin_guide_name);

	$upd_data = array(
		"ev_no" 		=> $ev_no,
		"guide_path"	=> addslashes(trim($final_guide_path)),
		"guide_name"	=> addslashes(trim($final_guide_name)),
	);

	if (!$ev_model->update($upd_data))
		$result = false;
	else
		$result = true;

	echo json_encode(array("result" => $result));
	exit;
}
elseif ($process == "new_evaluation")
{
	$ins_data = array(
		"ev_state" 		=> $_POST['f_ev_state'],
		"ev_u_set_no"	=> $_POST['f_ev_u_set_no'],
		"set_s_date" 	=> !empty($_POST['f_set_s_date']) ? $_POST['f_set_s_date'] : "NULL",
		"set_e_date" 	=> !empty($_POST['f_set_e_date']) ? $_POST['f_set_e_date'] : "NULL",
		"mod_s_date" 	=> !empty($_POST['f_mod_s_date']) ? $_POST['f_mod_s_date'] : "NULL",
		"mod_e_date" 	=> !empty($_POST['f_mod_e_date']) ? $_POST['f_mod_e_date'] : "NULL",
		"chk_s_date" 	=> !empty($_POST['f_chk_s_date']) ? $_POST['f_chk_s_date'] : "NULL",
		"chk_e_date" 	=> !empty($_POST['f_chk_e_date']) ? $_POST['f_chk_e_date'] : "NULL",
		"ev_s_date" 	=> $_POST['f_ev_s_date'],
		"ev_e_date"		=> $_POST['f_ev_e_date'],
		"subject"		=> $_POST['f_subject'],
		"admin_s_no"	=> $_POST['f_admin_s_no'],
		"description"	=> addslashes(trim($_POST['f_description'])),
	);

	# 파일첨부
	$guide_name = isset($_POST['guide_name']) ? $_POST['guide_name'] : "";
	$guide_path = isset($_POST['guide_path']) ? $_POST['guide_path'] : "";

	if(!empty($guide_path))
	{
		$guide_reads  = move_store_files($guide_path, "dropzone_tmp", "evaluation");
		$guide_names  = implode(',', $guide_name);
		$guide_paths  = implode(',', $guide_reads);

		$ins_data['guide_path'] = $guide_paths;
		$ins_data['guide_name'] = $guide_names;
	}

	if ($ev_model->insert($ins_data)){
		$last_ev_no = $ev_model->getInsertId();
		exit("<script>alert('등록했습니다');location.href='evaluation_regist.php?ev_no={$last_ev_no}';</script>");
	} else {
		exit("<script>alert('등록에 실패 하였습니다');location.href='evaluation_regist.php';</script>");
	}
}
elseif($process == "send_sms")
{
	$ev_no 			= (isset($_POST['chk_ev_no'])) ? $_POST['chk_ev_no'] : "";
	$send_type		= (isset($_POST['chk_type'])) ? $_POST['chk_type'] : "";
	$ev_system  	= $ev_model->getItem($ev_no);
	$result			= false;
	$c_info   		= "11";
	$sms_title 		= "";
	$content   		= "";
	$receiver_sql 	= "";
	$send_data_list = [];

	# EVAL01(평가자 설정), EVAL02(상호평가), EVAL03(평가자 설정검토), EVAL04(평가하기)
	$receiver_list = [];
	switch($send_type)
	{
	    case '1':
			$c_info_detail  = "EVAL01";
			$sms_title 	    = "평가자 설정 요청";
            $content   	    = "[평가자 설정 요청]\r\n와플 내 평가자 설정 기간이 시작되었습니다. 평가자 설정을 부탁 드립니다.\r\n· 평가명 : {$ev_system['subject']}\r\n· 작성기간 : {$ev_system['set_s_date']} ~ {$ev_system['set_e_date']}\r\n· 바로가기 : https://work.wplanet.co.kr/v1/evaluation_setting_list.php";
            $receiver_sql   = "SELECT manager as s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=er.manager) as s_name, (SELECT s.hp FROM staff s WHERE s.s_no=er.manager) as hp FROM evaluation_relation er WHERE ev_no='{$ev_no}' GROUP BY manager";
            break;
        case '2':
			$c_info_detail  = "EVAL02";
            $sms_title 		= "상호평가 확인";
            $content   	    = "[상호평가 확인/수정 요청]\r\n와플 내 평가 대상에 관한 상호평가 확인 설정을 부탁 드립니다.\r\n평가대상이 아니라고 생각하실 경우 꼭 사유 및 근거도 자세히 남겨주세요.\r\n팀 리더(평가설정 관리자) 분들은 상호평가 확인란에 확인중 또는 불일치가 남지 않도록 최종 검토 부탁드립니다.\r\n· 평가명 : {$ev_system['subject']}\r\n· 작성기간 : {$ev_system['mod_s_date']} ~ {$ev_system['mod_e_date']}\r\n· 바로가기 : https://work.wplanet.co.kr/v1/evaluation_setting_list.php?sch_ev_no={$ev_no}";
            $receiver_sql   = "SELECT evaluator_s_no as s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as s_name, (SELECT s.hp FROM staff s WHERE s.s_no=er.evaluator_s_no) as hp FROM evaluation_relation er WHERE ev_no='{$ev_no}' AND evaluator_s_no != '1' GROUP BY evaluator_s_no";
            break;
        case '3':
			$c_info_detail  = "EVAL03";
            $sms_title 		= "자기평가 기술서 작성 요청";
            $content   	    = "[자기평가 기술서 작성 요청]\r\n평가를 시작하기 전 자기평가 기술서 작성을 부탁 드립니다.\r\n· 평가명 : {$ev_system['subject']}\r\n· 작성기간 : {$ev_system['chk_s_date']} ~ {$ev_system['chk_e_date']}\r\n· 바로가기 : https://work.wplanet.co.kr/v1/evaluation_system.php";
            $receiver_sql   = "SELECT receiver_s_no as s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=er.receiver_s_no) as s_name, (SELECT s.hp FROM staff s WHERE s.s_no=er.receiver_s_no) as hp FROM evaluation_relation er WHERE ev_no='{$ev_no}' GROUP BY receiver_s_no";
            break;
        case '4':
			$c_info_detail  = "EVAL04";
            $sms_title 		= "평가 요청";
            $content   	    = "[평가 요청]\r\n평가가 시작 되었습니다.\r\n공정한 평가 부탁드리며 평가 진행 기간 내 꼭 완료해 주세요.\r\n· 평가명 : {$ev_system['subject']}\r\n· 작성기간 : {$ev_system['ev_s_date']} ~ {$ev_system['ev_e_date']}\r\n· 바로가기 : https://work.wplanet.co.kr/v1/evaluation_progress.php?ev_no={$ev_no}";
            $receiver_sql   = "SELECT evaluator_s_no as s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as s_name, (SELECT s.hp FROM staff s WHERE s.s_no=er.evaluator_s_no) as hp FROM evaluation_relation er WHERE ev_no='{$ev_no}' GROUP BY evaluator_s_no";
            break;

		case '5':
			$c_info_detail  = "EVAL05";
			$sms_title 		= "상호평가 확인/수정 요청";
			$content   	    = "[상호평가 확인/수정 요청]\r\n아직 상호평가 확인이 완료되지 않은 건이 있습니다.\r\n확인해주시고 기간 꼭 지켜주세요.\r\n· 평가명 : {$ev_system['subject']}\r\n· 작성기간 : {$ev_system['mod_s_date']} ~ {$ev_system['mod_e_date']}\r\n· 바로가기 : https://work.wplanet.co.kr/v1/evaluation_setting_list.php?sch_ev_no={$ev_no}";
			$receiver_sql   = "SELECT manager as s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=er.manager) as s_name, (SELECT s.hp FROM staff s WHERE s.s_no=er.manager) as hp FROM evaluation_relation er WHERE ev_no='{$ev_no}' AND rec_is_review=0 GROUP BY manager";
			break;

		case '6':
			$c_info_detail  = "EVAL06";
			$sms_title 		= "상호평가 확인/수정 요청";
			$content   	    = "[상호평가 확인/수정 요청]\r\n상호평가 불일치 건이 있습니다.\r\n확인해주시고 기간 꼭 지켜주세요.\r\n· 평가명 : {$ev_system['subject']}\r\n· 작성기간 : {$ev_system['mod_s_date']} ~ {$ev_system['mod_e_date']}\r\n· 바로가기 : https://work.wplanet.co.kr/v1/evaluation_setting_list.php?sch_ev_no={$ev_no}";
			$receiver_sql   = "SELECT manager as s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=er.manager) as s_name, (SELECT s.hp FROM staff s WHERE s.s_no=er.manager) as hp FROM evaluation_relation er WHERE ev_no='{$ev_no}' AND rec_is_review!=eval_is_review GROUP BY manager";
			break;

		case '7':
			$c_info_detail  = "EVAL07";
			$sms_title 		= "평가 마감기한 준수 요청";
			$content   	    = "[평가 마감기한 준수 요청]\r\n아직 평가완료되지 않는 건이 있습니다.\r\n평가기간 꼭 지켜주세요.\r\n· 평가명 : {$ev_system['subject']}\r\n· 작성기간 : {$ev_system['ev_s_date']} ~ {$ev_system['ev_e_date']}\r\n· 바로가기 : https://work.wplanet.co.kr/v1/evaluation_progress.php?ev_no={$ev_no}";
			$receiver_sql   = "SELECT evaluator_s_no as s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as s_name, (SELECT s.hp FROM staff s WHERE s.s_no=er.evaluator_s_no) as hp FROM evaluation_relation er WHERE ev_no='{$ev_no}' AND is_complete=0 AND rec_is_review=1 AND eval_is_review=1 GROUP BY evaluator_s_no";
			break;

		case '8':
			$c_info_detail  = "EVAL08";
			$sms_title 		= "자기평가 기술서 작성 요청";
			$content   	    = "[자기평가 기술서 작성 요청]\r\n'|name|'님 자기평가 기술서가 아직 작성되지 않았습니다.\r\n· 평가명 : {$ev_system['subject']}\r\n· 작성기간 : {$ev_system['chk_s_date']} ~ {$ev_system['chk_e_date']}\r\n· 바로가기 : https://work.wplanet.co.kr/v1/evaluation_system.php";
			$receiver_sql   = "SELECT receiver_s_no as s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=er.receiver_s_no) as s_name, (SELECT s.hp FROM staff s WHERE s.s_no=er.receiver_s_no) as hp FROM evaluation_relation er WHERE ev_no='{$ev_no}' AND (SELECT COUNT(ers.esr_no) FROM evaluation_receiver_self ers WHERE ers.ev_no=er.ev_no AND ers.rec_s_no=er.receiver_s_no)=0 GROUP BY receiver_s_no";
			break;
	}

	if(empty($c_info_detail)){
	    exit("<script>alert('메세지 전송에 실패했습니다');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
	}

	$receiver_query = mysqli_query($my_db, $receiver_sql);
    $receiver_list  = [];
    while($receiver_result = mysqli_fetch_assoc($receiver_query))
    {
        if(!empty($receiver_result['hp'])){
            $receiver_list[$receiver_result['s_no']] = array(
                "name"     => $receiver_result['s_name'],
                "hp"       => $receiver_result['hp'],
            );
        }
    }

    if($receiver_list)
    {
		$send_name 	    = "와이즈플래닛";
		$send_phone     = "02-830-1912";
		$sms_msg        = addslashes($content);
		$sms_msg_len    = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
		$msg_type       = ($sms_msg_len > 90) ? "L" : "S";
		$sms_msg_tmp 	= $content;
		$cm_id 			= 1;
		$cur_datetime	= date("YmdHis");

        foreach($receiver_list as $receiver_data)
        {
			if($c_info_detail == "EVAL08"){
				$sms_msg = str_replace("|name|",$receiver_data['name'], $sms_msg_tmp);
				$sms_msg = addslashes($sms_msg);
			}

			$msg_id = "W{$cur_datetime}".sprintf('%04d', $cm_id++);

			$send_data_list[$msg_id] = array(
				"msg_id"        => $msg_id,
				"msg_type"      => $msg_type,
				"sender"        => $send_name,
				"sender_hp"     => $send_phone,
				"receiver"      => $receiver_data['s_name'],
				"receiver_hp"   => $receiver_data['hp'],
				"title"         => $sms_title,
				"content"       => $sms_msg,
				"cinfo"         => $c_info,
				"cinfo_detail"  => $c_info_detail,
			);
        }
    }else{
        exit("<script>alert('수신자가 없습니다. 메세지 전송에 실패했습니다');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
    }

    if(!empty($send_data_list))
    {
		$message_model  	= Message::Factory();
		$msg_result_data 	= $message_model->sendMessage($send_data_list);

		if ($msg_result_data['result']){
			exit("<script>alert('메세지를 전송했습니다');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
		} else {
			exit("<script>alert('메세지 전송에 실패했습니다');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
		}
	}else{
		exit("<script>alert('전송할 메세지 내역이 없습니다.');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
	}
}
else
{
	$ev_no 		= isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
	$ev_item 	= [];

	if($ev_no)
	{
		$ev_item	= $ev_model->getItem($ev_no);
		$admin_s_no	= isset($ev_item['admin_s_no']) ? $ev_item['admin_s_no'] : "";

		if(permissionNameCheck($session_permission, "대표") || $session_s_no == '28'){
			$is_admin 		= true;
			$is_editable 	= true;
		}elseif($admin_s_no == $session_s_no){
			$is_editable 	= true;
		}

		$guide_paths = $ev_item['guide_path'];
		$guide_names = $ev_item['guide_name'];
		if(!empty($guide_paths) && !empty($guide_names))
		{
			$ev_item['guide_paths'] = explode(',', $guide_paths);
			$ev_item['guide_names'] = explode(',', $guide_names);
			$ev_item['file_count']	= count($ev_item['guide_paths']);
		}
	}else{
		$is_admin 		= true;
		$is_editable 	= true;
	}

	$staff_model		= Staff::Factory();
	$ev_set_list 		= $ev_model->getEvaluationSetList(1);
	$staff_list 		= $staff_model->getStaffNameList();
	$ev_state_option 	= getEvStateColorOption();

	$smarty->assign($ev_item);
	$smarty->assign("staff_list", $staff_list);
	$smarty->assign("ev_set_list", $ev_set_list);
	$smarty->assign("ev_state_option", $ev_state_option);
}

$smarty->assign("is_admin", $is_admin);
$smarty->assign("is_editable", $is_editable);

$smarty->display('evaluation_regist.html');

?>
