<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Staff.php');
require('inc/model/Approval.php');

$proc = (isset($_POST['process'])) ? $_POST['process'] : "";

if ($proc == "change_my_c_no") {
	$ss_my_c_no_post = (isset($_POST['ss_my_c_no'])) ? $_POST['ss_my_c_no'] : "";

	$_SESSION["ss_my_c_no"] = $ss_my_c_no_post;
	$session_my_c_no = $_SESSION["ss_my_c_no"];

	$smarty->assign("session_my_c_no",$_SESSION["ss_my_c_no"]);

	echo("<script>alert('사업자가 변경되었습니다.');history.back();</script>");

}
elseif($proc)
{
    $staff_sql	= "select my_c_no, s_no,id,s_name,team,`position`,pos_permission,permission,staff_state,my_c_type, (SELECT team_code_parent FROM team WHERE team_code=team) AS team_parent from staff where s_no = '{$proc}'";
    $query		= mysqli_query($my_db, $staff_sql);
    $staff_info	= mysqli_fetch_array($query);

    if($staff_info['id'])
    {
        $_SESSION["ss_my_c_no"] = $staff_info['my_c_no'];
        $_SESSION["ss_s_no"] = $staff_info['s_no'];
        $_SESSION["ss_id"] = $staff_info['id'];
        $_SESSION["ss_name"] = $staff_info['s_name'];
        $_SESSION["ss_team"] = $staff_info['team'];
        $_SESSION["ss_team_parent"] = $staff_info['team_parent'];
        $_SESSION["ss_position"] = $staff_info['position'];
        $_SESSION["ss_pos_permission"] = $staff_info['pos_permission'];
        $_SESSION["ss_permission"] = $staff_info['permission'];
        $_SESSION["ss_staff_state"] = $staff_info['staff_state'];
        $_SESSION["ss_my_c_type"] = $staff_info['my_c_type'];

        $staff_name = $staff_info['s_name'];
        $staff_permission = $staff_info['permission'];

        exit("<script>alert('$staff_name ($staff_permission)으로 로그인 되었습니다.');parent.document.location.reload();</script>");
    }
}

# 계열사 리스트
$my_company_sql = "
	SELECT
		mc.my_c_no,
		mc.ceo_s_no,
		mc.c_name,
		mc.kind_color,
		mc.company_number,
		mc.license_type,
		mc.active_state
	FROM my_company mc
	WHERE mc.active_state='1'
";
$my_company_query = mysqli_query($my_db,$my_company_sql);
while($my_company_data = mysqli_fetch_array($my_company_query)){
    $my_company_list[] = array(
        "my_c_no"		=> $my_company_data['my_c_no'],
        "ceo_s_no"		=> $my_company_data['ceo_s_no'],
        "c_name"		=> $my_company_data['c_name'],
        "kind_color"	=> $my_company_data['kind_color'],
        "company_number"=> $my_company_data['company_number'],
        "license_type"	=> $my_company_data['license_type'],
        "active_state"	=> $my_company_data['active_state']
    );
}
$smarty->assign("my_company_list",$my_company_list);

# 스태프 리스트
$staff_model = Staff::Factory();
$staff_list  = $staff_model->getStaffNameList("s_name", "1");
$smarty->assign("staff_list", $staff_list);

$read_model         = Approval::Factory();
$read_model->setMainInit("approval_report_read", "arr_no");
$approval_read_list = [];
$approval_read_sql  = "
    SELECT
        ar.ar_no,
        ar.ar_state,
        (SELECT count(arr.arr_no) FROM approval_report_read arr WHERE arr.ar_no=ar.ar_no AND arr.ar_state=ar.ar_state AND arr.read_s_no='{$session_s_no}') as read_cnt
    FROM approval_report as ar
    WHERE ar.ar_state = 2 AND ar.ar_no IN(SELECT arp.ar_no FROM approval_report_permission arp WHERE arp.arp_s_no='{$session_s_no}' AND arp.arp_type IN('approval','conference') AND arp.arp_state != '0')
    ORDER BY ar_no DESC
";
$approval_read_query = mysqli_query($my_db, $approval_read_sql);
while($approval_read = mysqli_fetch_assoc($approval_read_query))
{
    $ar_state = $read_model->changeArState($approval_read['ar_no'], $session_s_no, $session_team);
    if($approval_read['read_cnt'] == 0 && ($ar_state == '2_1' || $ar_state == '2_2')) {
        $approval_read_list[$ar_state]++;
    }
}
$smarty->assign("approval_read_list", $approval_read_list);

$is_work_state      = false;
$work_state_list    = array("3" => 0, "4" => 0, "5" => 0);
$work_state_sql  = "
    SELECT
        work_state,
        COUNT(w_no) as work_cnt
    FROM work as `w`
    WHERE task_run_s_no='{$session_s_no}' AND work_state IN(3,4,5)
    GROUP BY work_state
";
$work_state_query = mysqli_query($my_db, $work_state_sql);
while($work_state_result = mysqli_fetch_assoc($work_state_query))
{
    $is_work_state = true;
    $work_state_list[$work_state_result['work_state']] = $work_state_result['work_cnt'];
}
$smarty->assign("is_work_state", $is_work_state);
$smarty->assign("work_state_list", $work_state_list);

$smarty->display('navibar_top.html');
?>
