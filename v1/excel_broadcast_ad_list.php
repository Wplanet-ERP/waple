<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/broadcast.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "고유번호")
	->setCellValue('B1', "매체")
	->setCellValue('C1', "세부매체")
	->setCellValue('D1', "광고주")
	->setCellValue('E1', "광고주명")
	->setCellValue('F1', "진행상태")
	->setCellValue('G1', "광고유형")
	->setCellValue('H1', "프로그램명")
	->setCellValue('I1', "시작시간")
	->setCellValue('J1', "종료시간")
	->setCellValue('K1', "초수")
	->setCellValue('L1', "요일")
	->setCellValue('M1', "시급")
	->setCellValue('N1', "청약시작일")
	->setCellValue('O1', "청약종료일")
	->setCellValue('P1', "청약구분")
	->setCellValue('Q1', "적용율")
	->setCellValue('R1', "단가")
	->setCellValue('S1', "횟수")
	->setCellValue('T1', "청구광고료")
	->setCellValue('U1', "보너스 횟수")
	->setCellValue('V1', "보너스 광고료")
	->setCellValue('W1', "비고")
	->setCellValue('X1', "등록일")
	->setCellValue('Y1', "운행구분")
	->setCellValue('Z1', "신청자")
	->setCellValue('AA1', "신청일")
;

# 검색조건
$sch_media      = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_advertiser = isset($_GET['sch_advertiser']) ? $_GET['sch_advertiser'] : "";
$sch_state      = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_title      = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_sub_s_date = isset($_GET['sch_sub_s_date']) ? $_GET['sch_sub_s_date'] : date('Y-m')."-01";
$sch_sub_e_date = isset($_GET['sch_sub_e_date']) ? $_GET['sch_sub_e_date'] : date('Y-m')."-31";
$sch_type       = isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_notice     = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$sch_detail_media   = isset($_GET['sch_detail_media']) ? $_GET['sch_detail_media'] : "";
$sch_subs_gubun     = isset($_GET['sch_subs_gubun']) ? $_GET['sch_subs_gubun'] : "";
$sch_delivery_gubun = isset($_GET['sch_delivery_gubun']) ? $_GET['sch_delivery_gubun'] : "";

$add_where = "1=1";
if(!empty($sch_media))
{
	$add_where .= " AND `ba`.media = '{$sch_media}'";
}

if(!empty($sch_advertiser))
{
	$add_where .= " AND `ba`.advertiser like '%{$sch_advertiser}%'";
}

if(!empty($sch_state))
{
	$add_where .= " AND `ba`.state = '{$sch_state}'";
}

if(!empty($sch_title))
{
	$add_where .= " AND `ba`.program_title LIKE '%{$sch_title}%'";
}

if(!empty($sch_sub_s_date) || !empty($sch_sub_e_date))
{
	if(empty($sch_sub_s_date)){
		$add_where .= " AND `ba`.sub_s_date <= '{$sch_sub_e_date}'";
	}elseif(empty($sch_sub_e_date)){
		$add_where .= " AND `ba`.sub_e_date >= '{$sch_sub_s_date}'";
	}else{
		$add_where .= " AND (`ba`.sub_s_date BETWEEN '{$sch_sub_s_date}' AND '{$sch_sub_e_date}' OR `ba`.sub_s_date <= '{$sch_sub_s_date}' AND `ba`.sub_e_date >= '{$sch_sub_e_date}')";
	}
}

if(!empty($sch_type))
{
	$add_where .= " AND `ba`.`type` = '{$sch_type}'";
}

if(!empty($sch_notice))
{
	$add_where .= " AND `ba`.notice LIKE '%{$sch_notice}%'";
}

if(!empty($sch_detail_media))
{
	$add_where .= " AND `ba`.detail_media = '{$sch_detail_media}'";
}

if(!empty($sch_subs_gubun))
{
	$add_where .= " AND `ba`.subs_gubun = '{$sch_subs_gubun}'";
}

if(!empty($sch_delivery_gubun))
{
	$add_where .= " AND `ba`.delivery_gubun = '{$sch_delivery_gubun}'";
}

$broadcast_sql  = "
    SELECT *,
      DATE_FORMAT(`ba`.s_time, '%H:%i') as s_time,
      DATE_FORMAT(`ba`.e_time, '%H:%i') as e_time
    FROM broadcast_advertisement `ba`
    WHERE {$add_where}
    ORDER BY `ba`.ba_no DESC
";


$result	= mysqli_query($my_db, $broadcast_sql);
$idx = 2;
if(!!$result)
{
    while($broadcast = mysqli_fetch_array($result))
    {
        $media_name = $ba_media_option[$broadcast['media']];
        $state_name = $ba_state_option[$broadcast['state']];

		$unit_price	 	= number_format($broadcast['unit_price']);
		$billing_price 	= number_format($broadcast['billing_price']);
		$bonus_price	= number_format($broadcast['bonus_price']);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$idx, $broadcast['ba_no'])
            ->setCellValue('B'.$idx, $media_name)
            ->setCellValue('C'.$idx, $broadcast['detail_media'])
            ->setCellValue('D'.$idx, $broadcast['adv_no'])
            ->setCellValue('E'.$idx, $broadcast['advertiser'])
            ->setCellValue('F'.$idx, $state_name)
            ->setCellValue('G'.$idx, $broadcast['type'])
            ->setCellValue('H'.$idx, $broadcast['program_title'])
            ->setCellValue('I'.$idx, $broadcast['s_time'])
            ->setCellValue('J'.$idx, $broadcast['e_time'])
            ->setCellValue('K'.$idx, $broadcast['second_cnt'])
            ->setCellValue('L'.$idx, $broadcast['date_w'])
            ->setCellValue('M'.$idx, $broadcast['hourly_wage'])
            ->setCellValue('N'.$idx, $broadcast['sub_s_date'])
            ->setCellValue('O'.$idx, $broadcast['sub_e_date'])
            ->setCellValue('P'.$idx, $broadcast['subs_gubun'])
            ->setCellValue('Q'.$idx, $broadcast['application_rate'])
            ->setCellValue('R'.$idx, $unit_price)
            ->setCellValue('S'.$idx, $broadcast['base_count'])
            ->setCellValue('T'.$idx, $billing_price)
            ->setCellValue('U'.$idx, $broadcast['bonus_count'])
            ->setCellValue('V'.$idx, $bonus_price)
            ->setCellValue('W'.$idx, $broadcast['notice'])
            ->setCellValue('X'.$idx, $broadcast['regdate'])
            ->setCellValue('Y'.$idx, $broadcast['delivery_gubun'])
            ->setCellValue('Z'.$idx, $broadcast['req_name'])
            ->setCellValue('AA'.$idx, $broadcast['req_date'])
		;

        $idx++;
    }
}
$idx--;

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');

$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');

$objPHPExcel->getActiveSheet()->getStyle("A2:AA{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A1:A'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B1:B'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D1:D'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E1:E'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('F1:F'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('H1:H'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('I1:I'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J1:J'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K1:K'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('L1:L'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('M1:M'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('N1:N'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('O1:O'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('P1:P'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Q1:Q'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('R1:R'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('S1:S'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('T1:T'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('U1:U'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('V1:V'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('W1:W'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('X1:X'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('Y1:Y'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Z1:Z'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('AA1:AA'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle('H2:H'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('P2:P'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('W2:W'.$idx)->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getStyle('A2:AA'.$idx)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(15);

$objPHPExcel->getActiveSheet()->setTitle('배송 자동정리 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_방송광고 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
