<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
include_once('inc/model/Company.php');
include_once('inc/helper/visit.php');

# 기본 엑셀 소스
$file_name   = $_FILES["visit_file"]["tmp_name"];

$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();


# 구매처 목록
$company_model      = Company::Factory();
$dp_company_option  = $company_model->getDpList();
$visit_step_matching= getVisitStepMatchingOption();

$comma   = '';
$ins_sql = "INSERT INTO `work_cms_visit`
            (visit_state, visit_step, req_date, req_num, customer, hp, addr1, addr2, prd_code, visit_code, req_content, run_date, visit_free, visit_price, run_content, center, dp_c_name, order_number, regdate)
          VALUES ";
$upd_sql_list = [];

for ($i = 2; $i <= $totalRow; $i++)
{
    //변수 초기화
    $visit_step = $customer = $hp = $addr1 = $addr2 = $prd_code = $visit_code = "";
    $req_date = $req_date_val = $run_date = $run_date_val = $visit_free ="";
    $req_content = $run_content = $center = $dp_c_name = $order_number  = "";
    $visit_state = $req_num = $visit_price = 0;

    $req_date_val   = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //발주일자 & 발주번호
    $req_date_exp   = explode('-', $req_date_val);
    $req_date_tmp   = isset($req_date_exp[0]) && !empty($req_date_exp[0]) ? $req_date_exp[0] : "";
    $req_num        = isset($req_date_exp[1]) ? $req_date_exp[1] : "";
    $visit_step     = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   //발주단계
    $visit_state    = isset($visit_step_matching[$visit_step]) ? $visit_step_matching[$visit_step] : 0;
    $doc_name       = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));  //닥터피엘 확인
    $customer       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //고객명
    $hp_val         = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //전화번호
    $addr1          = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //주소1
    $addr2          = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //주소2
    $prd_code       = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //제품코드
    $visit_code     = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));  //방문코드
    $req_content    = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //접수내용
    $run_date_val   = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //완료일자
    $visit_free     = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //유/무상여부
    $visit_price    = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //수금액
    $run_content    = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //완료내용
    $center         = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //센터명
    $order_number   = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //주문번호


    if(!empty($req_date_tmp))
    {
        if(strpos($req_date_tmp, "/") !== false){
            $req_date_convert = str_replace('/','-', $req_date_tmp);
            $req_date         = $req_date_convert ? date('Y-m-d', strtotime("{$req_date_convert}")) : "";
        }else{
            $req_date         = $req_date_tmp ? date('Y-m-d', strtotime("{$req_date_tmp}")) : "";
        }
    }

    if(!empty($run_date_val))
    {
        if(strpos($run_date_val, "/") !== false){
            $run_date_convert = str_replace('/','-', $run_date_val);
            $run_date         = $run_date_convert ? date('Y-m-d', strtotime("{$run_date_convert}")) : "";
        }else if(strpos($run_date_val, "-") !== false){
            $run_date         = $run_date_val ? date('Y-m-d', strtotime("{$run_date_val}")) : "";
        }else{
            //엑셀 시간 형식 변경
            $run_time_cal = ($run_date_val-25569)*86400;
            $run_time     = date("Y-m-d", $run_time_cal);
            $run_date     = date("Y-m-d", strtotime("{$run_time}"));
        }
    }

    if(empty($req_date) || empty($req_num) || empty($order_number))
    {
        exit("<script>alert('커머스 방문지원 반영에 실패하였습니다. 엑셀 {$i}번줄의 발주일자와 고유번호가 존재하지 않습니다.');location.href='work_cms_visit_list.php';</script>");
    }

    $visit_chk_sql   = "SELECT v_no FROM work_cms_visit WHERE order_number='{$order_number}' AND req_date ='{$req_date}' AND req_num='{$req_num}'  LIMIT 1";
    $visit_chk_query = mysqli_query($my_db, $visit_chk_sql);
    $visit_chk       = mysqli_fetch_array($visit_chk_query);

    //배송리스트 체크
    if(isset($visit_chk['v_no']))
    {
        $upd_sql_list[] = "UPDATE work_cms_visit SET visit_state='{$visit_state}', visit_step='{$visit_step}', req_content='{$req_content}', run_date='{$run_date}', visit_free='{$visit_free}', visit_price='{$visit_price}', run_content='{$run_content}', center='{$center}' WHERE v_no='{$visit_chk['v_no']}'";
    }
    else
    {
        $empty_visit_chk_sql = "SELECT v_no FROM work_cms_visit WHERE order_number='{$order_number}' AND req_date ='{$req_date}' AND (req_num is null OR req_num='') AND visit_state='4' LIMIT 1";
        $empty_visit_chk_query = mysqli_query($my_db, $empty_visit_chk_sql);
        $empty_visit_chk       = mysqli_fetch_array($empty_visit_chk_query);
        if(isset($empty_visit_chk['v_no']))
        {
            $upd_sql_list[] = "UPDATE work_cms_visit SET req_num='{$req_num}', visit_state='{$visit_state}', visit_step='{$visit_step}', req_content='{$req_content}', run_date='{$run_date}', visit_free='{$visit_free}', visit_price='{$visit_price}', run_content='{$run_content}', center='{$center}' WHERE v_no='{$empty_visit_chk['v_no']}'";
        }
        else
        {
            $order_chk_sql   = "SELECT dp_c_no FROM work_cms WHERE order_number='{$order_number}' LIMIT 1";
            $order_chk_query = mysqli_query($my_db, $order_chk_sql);
            $order_chk       = mysqli_fetch_array($order_chk_query);

            if(!isset($order_chk['dp_c_no'])){
                exit("<script>alert('커머스 방문지원 반영에 실패하였습니다. 엑셀 {$i}번줄의 와플내 주문이 존재하지 않습니다.');location.href='work_cms_visit_list.php';</script>");
            }

            $dp_c_name = $dp_company_option[$order_chk['dp_c_no']];
            $ins_sql  .= $comma."('{$visit_state}', '{$visit_step}', '{$req_date}', '{$req_num}', '{$customer}', '{$hp}', '{$addr1}', '{$addr2}', '{$prd_code}', '{$visit_code}', '{$req_content}', '{$run_date}', '{$visit_free}', '{$visit_price}', '{$run_content}', '{$center}', '{$dp_c_name}', '{$order_number}', now())";
            $comma     = " , ";
        }
    }
}

$result = false;
if(!empty($comma) || !empty($upd_sql_list))
{
    if(!empty($comma)){
        if(mysqli_query($my_db, $ins_sql)){
            $result = true;
        }
    }

    if(!empty($upd_sql_list))
    {
        foreach($upd_sql_list as $upd_sql){
            mysqli_query($my_db, $upd_sql);
        }
        $result = true;
    }
}


if($result){
    exit("<script>alert('방문지원 리스트가 반영 되었습니다.');location.href='work_cms_visit_list.php';</script>");
}else{
    exit("<script>alert('커머스 방문지원 반영에 실패하였습니다. Query 등록 오류 입니다. 담당자에게 문의 부탁드립니다');location.href='work_cms_visit_list.php';</script>");
}


?>
