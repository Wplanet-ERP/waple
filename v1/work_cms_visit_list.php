<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/visit.php');
require('inc/model/MyQuick.php');

# 프리랜서 확인
$is_freelancer = false;
if($session_staff_state == '2'){
    $is_freelancer = true;
}
$smarty->assign("is_freelancer", $is_freelancer);


$process = (isset($_POST['process']))?$_POST['process']:"";

if($process == "visit_state")
{
    $v_no 		= (isset($_POST['v_no'])) ? $_POST['v_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_visit SET visit_state='{$value}' WHERE v_no = '{$v_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "진행상태 저장에 실패 하였습니다.";
    else
        echo "진행상태가 저장 되었습니다.";

    exit;
}
elseif($process == "req_date")
{
    $v_no 		= (isset($_POST['v_no'])) ? $_POST['v_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_visit SET req_date='{$value}' WHERE v_no = '{$v_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "접수일자 저장에 실패 하였습니다.";
    else
        echo "접수일자가 저장 되었습니다.";

    exit;
}
elseif($process == "req_num")
{
    $v_no 		= (isset($_POST['v_no'])) ? $_POST['v_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_visit SET req_num='{$value}' WHERE v_no = '{$v_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "접수번호 저장에 실패 하였습니다.";
    else
        echo "접수번호가 저장 되었습니다.";

    exit;
}
elseif($process == "hp")
{
    $v_no 		= (isset($_POST['v_no'])) ? $_POST['v_no'] : "";
    $value 		= (isset($_POST['val'])) ? trim(addslashes(($_POST['val']))) : "";

    $sql = "UPDATE work_cms_visit SET hp='{$value}' WHERE v_no = '{$v_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "전화번호 저장에 실패 하였습니다.";
    else
        echo "전화번호가 저장 되었습니다.";

    exit;
}
elseif($process == "addr1")
{
    $v_no 		= (isset($_POST['v_no'])) ? $_POST['v_no'] : "";
    $value 		= (isset($_POST['val'])) ? trim(addslashes(($_POST['val']))) : "";

    $sql = "UPDATE work_cms_visit SET addr1='{$value}' WHERE v_no = '{$v_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "주소1 저장에 실패 하였습니다.";
    else
        echo "주소1 저장 되었습니다.";

    exit;
}
elseif($process == "addr2")
{
    $v_no 		= (isset($_POST['v_no'])) ? $_POST['v_no'] : "";
    $value 		= (isset($_POST['val'])) ? trim(addslashes(($_POST['val']))) : "";

    $sql = "UPDATE work_cms_visit SET addr2='{$value}' WHERE v_no = '{$v_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "주소2 저장에 실패 하였습니다.";
    else
        echo "주소2 저장 되었습니다.";

    exit;
}
elseif($process == "visit_code")
{
    $v_no 		= (isset($_POST['v_no'])) ? $_POST['v_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_visit SET visit_code='{$value}' WHERE v_no = '{$v_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "방문코드 저장에 실패 하였습니다.";
    else
        echo "방문코드가 저장 되었습니다.";

    exit;
}
elseif($process == "req_content")
{
    $v_no 		= (isset($_POST['v_no'])) ? $_POST['v_no'] : "";
    $value 		= (isset($_POST['val'])) ? trim(addslashes(($_POST['val']))) : "";

    $sql = "UPDATE work_cms_visit SET req_content='{$value}' WHERE v_no = '{$v_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "접수내용 저장에 실패 하였습니다.";
    else
        echo "접수내용이 저장 되었습니다.";

    exit;
}
elseif($process == "del_visit")
{
    $v_no = (isset($_POST['v_no'])) ? $_POST['v_no'] : "";
    $sql  = "DELETE FROM work_cms_visit WHERE v_no = '{$v_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "삭제에 실패 하였습니다.";
    else
        echo "삭제 되었습니다.";

    exit;
}
elseif($process == "visit_free")
{
    $v_no 		= (isset($_POST['v_no'])) ? $_POST['v_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
    $add_req_content = "";

    if($value == "유상")
    {
        $visit_sql      = "SELECT * FROM work_cms_visit WHERE v_no='{$v_no}'";
        $visit_query    = mysqli_query($my_db, $visit_sql);
        $visit_result   = mysqli_fetch_assoc($visit_query);

        $add_req_content = "";

        if(empty($visit_result['visit_free']) && (isset($visit_result['visit_state']) && $visit_result['visit_state'] == '3') && !empty($visit_result['addr2']))
        {
            $addr_exp_list   = explode(' ', $visit_result['addr2']);
            $idx             = 0;
            $req_content     = "";
            $req_content_val = $visit_result['req_content'];

            foreach($addr_exp_list as $addr_exp)
            {
                if(empty($req_content) && $idx < 2){
                    if(strpos($addr_exp,"읍") !== false || strpos($addr_exp,"면") !== false || strpos($addr_exp,"리") !== false){
                        $req_content = "급지비용 40,000원 추가 수금 필요 / 현장 결제 안내 및 추가 비용 발생 가능 여부 안내 완료. / 설치 전 설치 일자 고객 연락 조율 필요";
                    }
                }
                $idx++;
            }

            if(!empty($req_content))
            {
                $req_content_val .= !empty($req_content_val) ? "/ {$req_content}" : "{$req_content}";
                $add_req_content = ", req_content='".addslashes(trim($req_content_val))."'";
            }
        }
    }

    $sql = "UPDATE work_cms_visit SET visit_free='{$value}' {$add_req_content} WHERE v_no = '{$v_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "유/무상 저장에 실패 하였습니다.";
    else
        echo "유/무상값이 저장 되었습니다.";

    exit;
}
elseif($process == "upd_total_visit")
{
    $search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $sql = "UPDATE work_cms_visit SET visit_state='4' WHERE visit_state='3'";

    if (mysqli_query($my_db, $sql))
        exit("<script>alert('접수완료 처리했습니다.');location.href='work_cms_visit_list.php?{$search_url}';</script>");
    else
        exit("<script>alert('접수완료 처리에 실패했습니다');location.href='work_cms_visit_list.php?{$search_url}';</script>");
}

# Navigation & My Quick
$nav_prd_no  = "";
$is_my_quick = "";
$nav_title   = "커머스 방문지원 리스트";
if(!$is_freelancer)
{
    $nav_prd_no  = "27";
    $quick_model = MyQuick::Factory();
    $is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
}

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$add_where = "1=1";
$sch_v_no           = isset($_GET['sch_v_no']) ? $_GET['sch_v_no'] : "";
$sch_visit_state    = isset($_GET['sch_visit_state']) ? $_GET['sch_visit_state'] : "";
$sch_req_s_date     = isset($_GET['sch_req_s_date']) ? $_GET['sch_req_s_date'] : "";
$sch_req_e_date     = isset($_GET['sch_req_e_date']) ? $_GET['sch_req_e_date'] : "";
$sch_req_num        = isset($_GET['sch_req_num']) ? $_GET['sch_req_num'] : "";
$sch_visit_step     = isset($_GET['sch_visit_step']) ? $_GET['sch_visit_step'] : "";
$sch_customer       = isset($_GET['sch_customer']) ? $_GET['sch_customer'] : "";
$sch_prd_code       = isset($_GET['sch_prd_code']) ? $_GET['sch_prd_code'] : "";
$sch_visit_code     = isset($_GET['sch_visit_code']) ? $_GET['sch_visit_code'] : "";
$sch_req_content    = isset($_GET['sch_req_content']) ? $_GET['sch_req_content'] : "";
$sch_run_s_date     = isset($_GET['sch_run_s_date']) ? $_GET['sch_run_s_date'] : "";
$sch_run_e_date     = isset($_GET['sch_run_e_date']) ? $_GET['sch_run_e_date'] : "";
$sch_visit_free     = isset($_GET['sch_visit_free']) ? $_GET['sch_visit_free'] : "";
$sch_run_content    = isset($_GET['sch_run_content']) ? $_GET['sch_run_content'] : "";
$sch_center         = isset($_GET['sch_center']) ? $_GET['sch_center'] : "";
$sch_order_number   = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";

if(!empty($sch_v_no)) {
    $add_where .= " AND wcv.v_no='{$sch_v_no}'";
    $smarty->assign("sch_v_no", $sch_v_no);
}

if(!empty($sch_visit_state)) {
    $add_where .= " AND wcv.visit_state='{$sch_visit_state}'";
    $smarty->assign("sch_visit_state", $sch_visit_state);
}

if(!empty($sch_req_s_date)) {
    $add_where .= " AND wcv.req_date >= '{$sch_req_s_date}'";
    $smarty->assign("sch_req_s_date", $sch_req_s_date);
}

if(!empty($sch_req_e_date)) {
    $add_where .= " AND wcv.req_date <= '{$sch_req_e_date}'";
    $smarty->assign("sch_req_e_date", $sch_req_e_date);
}

if(!empty($sch_req_num)) {
    $add_where .= " AND wcv.req_num='{$sch_req_num}'";
    $smarty->assign("sch_req_num", $sch_req_num);
}

if(!empty($sch_visit_step)) {
    $add_where .= " AND wcv.visit_step='{$sch_visit_step}'";
    $smarty->assign("sch_visit_step", $sch_visit_step);
}

if(!empty($sch_customer)) {
    $add_where .= " AND wcv.customer like '%{$sch_customer}%'";
    $smarty->assign("sch_customer", $sch_customer);
}

if(!empty($sch_prd_code)) {
    $add_where .= " AND wcv.prd_code='{$sch_prd_code}'";
    $smarty->assign("sch_prd_code", $sch_prd_code);
}

if(!empty($sch_visit_code)) {
    $add_where .= " AND wcv.visit_code='{$sch_visit_code}'";
    $smarty->assign("sch_visit_code", $sch_visit_code);
}

if(!empty($sch_req_content)) {
    $add_where .= " AND wcv.req_content like '%{$sch_req_content}%'";
    $smarty->assign("sch_req_content", $sch_req_content);
}

if(!empty($sch_run_s_date)) {
    $add_where .= " AND wcv.run_date >= '{$sch_run_s_date}'";
    $smarty->assign("sch_run_s_date", $sch_run_s_date);
}

if(!empty($sch_run_e_date)) {
    $add_where .= " AND wcv.run_date <= '{$sch_run_e_date}'";
    $smarty->assign("sch_run_e_date", $sch_run_e_date);
}

if(!empty($sch_visit_free)) {
    $add_where .= " AND wcv.visit_free like '%{$sch_visit_free}%'";
    $smarty->assign("sch_visit_free", $sch_visit_free);
}

if(!empty($sch_run_content)) {
    $add_where .= " AND wcv.run_content like '%{$sch_run_content}%'";
    $smarty->assign("sch_run_content", $sch_run_content);
}

if(!empty($sch_center)) {
    $add_where .= " AND wcv.center like '%{$sch_center}%'";
    $smarty->assign("sch_center", $sch_center);
}

if(!empty($sch_order_number)) {
    $add_where .= " AND wcv.order_number like '%{$sch_order_number}%'";
    $smarty->assign("sch_order_number", $sch_order_number);
}

$cms_visit_total_sql     = "SELECT count(v_no) as cnt FROM work_cms_visit wcv WHERE {$add_where}";
$cms_visit_total_query   = mysqli_query($my_db, $cms_visit_total_sql);
$cms_visit_total_result  = mysqli_fetch_assoc($cms_visit_total_query);
$cms_visit_total         = isset($cms_visit_total_result['cnt']) ? $cms_visit_total_result['cnt'] : 0;

$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 10;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($cms_visit_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "work_cms_visit_list.php", $pagenum, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $cms_visit_total);
$smarty->assign("pagelist", $page);
$smarty->assign("ord_page_type", $page_type);

$cms_visit_sum_sql     = "SELECT SUM(visit_price) as visit_sum FROM work_cms_visit wcv WHERE {$add_where}";
$cms_visit_sum_query   = mysqli_query($my_db, $cms_visit_sum_sql);
$cms_visit_sum_result  = mysqli_fetch_assoc($cms_visit_sum_query);
$cms_visit_sum         = isset($cms_visit_sum_result['visit_sum']) ? $cms_visit_sum_result['visit_sum'] : 0;

$smarty->assign("cms_visit_sum", $cms_visit_sum);

$cms_visit_sql  = "
    SELECT 
        *,
        DATE_FORMAT(wcv.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(wcv.regdate, '%H:%i') as reg_hour
    FROM work_cms_visit `wcv`
    WHERE {$add_where}
    ORDER BY `wcv`.v_no DESC
    LIMIT {$offset}, {$num}
";
$cms_visit_list  = [];
$cms_visit_state_list = array('3' => 0, '5' => 0, '7' => 0, '8' => 0);
$cms_visit_query = mysqli_query($my_db, $cms_visit_sql);
while($cms_visit = mysqli_fetch_assoc($cms_visit_query))
{
    $cms_visit_list[] = $cms_visit;
}

$cms_visit_state_sql    = "SELECT visit_state, count(v_no) as cnt FROM work_cms_visit wcv GROUP BY visit_state";
$cms_visit_state_query  = mysqli_query($my_db, $cms_visit_state_sql);
while($cms_visit_state = mysqli_fetch_assoc($cms_visit_state_query))
{
    if(isset($cms_visit_state_list[$cms_visit_state['visit_state']]))
    {
        $cms_visit_state_list[$cms_visit_state['visit_state']] = $cms_visit_state['cnt'];
    }
}

$sch_reg_s_date = date('Y-m-d', strtotime('-1 Years'));
$sch_reg_e_date = date('Y-m-d');

$costzero_prd_list  = [];
$costzero_prd_sql   = "SELECT prd_no, title FROM product_cms WHERE k_name_code='04034' AND display='1'";
$costzero_prd_query = mysqli_query($my_db, $costzero_prd_sql);
while($costzero = mysqli_fetch_assoc($costzero_prd_query))
{
    $costzero_prd_list[$costzero['prd_no']] = $costzero['title'];
}

$smarty->assign('visit_state_option', getVisitStateOption());
$smarty->assign('visit_state_btn_option', getVisitStateButtonOption());
$smarty->assign('visit_step_option', getVisitStepOption());
$smarty->assign('prd_code_option', getVisitPrdCodeOption());
$smarty->assign('visit_code_option', getVisitCodeOption());
$smarty->assign('visit_free_option', getVisitFreeOption());
$smarty->assign("page_option", getPageTypeOption('4'));
$smarty->assign("cms_visit_list", $cms_visit_list);
$smarty->assign("cms_visit_state_list", $cms_visit_state_list);
$smarty->assign("sch_reg_s_date", $sch_reg_s_date);
$smarty->assign("sch_reg_e_date", $sch_reg_e_date);
$smarty->assign("costzero_prd_list", $costzero_prd_list);

$smarty->display('work_cms_visit_list.html');

?>
