<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/logistics.php');
require('inc/helper/product_cms_stock.php');
require('inc/helper/work.php');
require('inc/model/MyQuick.php');
require('inc/model/Work.php');
require('inc/model/WorkCms.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');
require('inc/model/Logistics.php');
require('inc/model/Message.php');
require('inc/model/Company.php');
require('inc/model/CommerceOrder.php');

$logistics_model    = Logistics::Factory();
$work_model         = Work::Factory();
$cms_model          = WorkCms::Factory();
$unit_model         = ProductCmsUnit::Factory();
$report_model       = ProductCmsStock::Factory();
$report_model->setStockReport();
$move_model         = ProductCmsStock::Factory();
$move_model->setMainInit("product_cms_stock_transfer", "no");

$comm_set_model     = CommerceOrder::Factory();
$comm_set_model->setMainInit("commerce_order_set", "no");
$commerce_model     = CommerceOrder::Factory();
$commerce_model->setMainInit("commerce_order", "no");

# 프로세스 처리
$process     = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "f_work_state")
{
    $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $lm_no      = (isset($_POST['lm_no'])) ? $_POST['lm_no'] : "";
    $work_state = (isset($_POST['val'])) ? $_POST['val'] : "";

    $upd_data = array(
        "lm_no"      => $lm_no,
        "work_state" => $work_state
    );

    $work_data = array(
        "w_no"       => $w_no,
        "work_state" => $work_state
    );

    if($work_state == '6')
    {
        $upd_data['run_s_no']           = $session_s_no;
        $upd_data['run_team']           = $session_team;
        $upd_data['run_date']           = date("Y-m-d");
        
        $work_data['task_run_s_no']     = $session_s_no;
        $work_data['task_run_team']     = $session_team;
        $work_data['task_run_regdate']  = date("Y-m-d H:i:s");
    }

    if ($logistics_model->update($upd_data))
    {
        if(!empty($w_no)) {
            $work_model->update($work_data);
        }
        echo "진행상태가 변경됬습니다.";
    } else{
        echo "진행상태 변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "f_run_date")
{
    $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $lm_no      = (isset($_POST['lm_no'])) ? $_POST['lm_no'] : "";
    $run_date   = (isset($_POST['val'])) ? $_POST['val'] : "";

    $upd_data = array(
        "lm_no"      => $lm_no,
        "run_s_no"   => $session_s_no,
        "run_team"   => $session_team,
        "run_date"   => $run_date
    );

    $work_data = array(
        "w_no"              => $w_no,
        "task_run_s_no"     => $session_s_no,
        "task_run_team"     => $session_team,
        "task_run_regdate"  => $run_date
    );

    if ($logistics_model->update($upd_data))
    {
        if(!empty($w_no)) {
            $work_model->update($work_data);
        }
        echo "업무완료일이 변경됬습니다.";
    } else{
        echo "업무완료일 변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "f_task_run")
{
    $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $lm_no      = (isset($_POST['lm_no'])) ? $_POST['lm_no'] : "";
    $task_run   = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])): "";

    $upd_data = array(
        "lm_no"      => $lm_no,
        "task_run"   => $task_run
    );

    $work_data = array(
        "w_no"       => $w_no,
        "task_run"   => $task_run
    );

    if ($logistics_model->update($upd_data))
    {
        if(!empty($w_no)) {
            $work_model->update($work_data);
        }
        echo "업무진행이 변경됬습니다.";
    } else{
        echo "업무진행 변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "f_task_run_staff")
{

    $lm_no      = (isset($_POST['lm_no'])) ? $_POST['lm_no'] : "";
    $s_no       = (isset($_POST['s_no'])) ? $_POST['s_no'] : "";
    $team       = (isset($_POST['team'])) ? $_POST['team'] : "";
    $logistics  = $logistics_model->getItem($lm_no);
    $w_no       = $logistics['w_no'];

    $upd_data = array(
        "lm_no"      => $lm_no,
        "run_s_no"   => $s_no,
        "run_team"   => $team
    );

    $work_data = array(
        "w_no"          => $w_no,
        "task_run_s_no" => $s_no,
        "task_run_team" => $team
    );

    if ($logistics_model->update($upd_data))
    {
        if(!empty($w_no)) {
            $work_model->update($work_data);
        }
        echo "업무처리자를 변경했습니다.";
    } else{
        echo "업무처리자 변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "f_manager_memo")
{
    $w_no            = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $lm_no           = (isset($_POST['lm_no'])) ? $_POST['lm_no'] : "";
    $manager_memo    = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])): "";

    $upd_data = array(
        "lm_no"         => $lm_no,
        "manager_memo"  => $manager_memo
    );

    $work_data = array(
        "w_no"          => $w_no,
        "manager_memo"  => $manager_memo
    );

    if ($logistics_model->update($upd_data))
    {
        if(!empty($w_no)) {
            $work_model->update($work_data);
        }
        echo "관리자 메모가 변경됬습니다.";
    } else {
        echo "관리자 메모 변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "new_order")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $lm_no              = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";

    $logistics          = $logistics_model->getItem($lm_no);
    $new_ins_data       = [];
    $order_numbers      = "";
    $result             = false;
    $logistics_subject  = $logistics['subject'];
    $notice             = ($logistics_subject == '12' && !empty($logistics['run_ord_no'])) ? "선입매형 매출 주문번호 : {$logistics['run_ord_no']}" : "";

    if($logistics['work_state'] == '7' || $logistics['work_state'] == '8' || $logistics['work_state'] == '9'){
        exit("<script>alert('택배리스트에 주문을 생성할 수 없는 진행상태입니다.');location.href='logistics_management.php?{$search_url}';</script>");
    }
    
    if($logistics['prd_type'] != '1')
    {
        exit("<script>alert('해당 상품타입은 주문을 생성할 수 없습니다.');location.href='logistics_management.php?{$search_url}';</script>");
    }

    $cur_date                   = date("Y-m-d");
    $chk_order_sql              = "SELECT COUNT(*) as cnt FROM work_cms WHERE stock_date='{$cur_date}' AND task_req LIKE '%물류관리시스템 문서번호({$logistics['doc_no']}) 주문추가%'";
    $chk_order_query            = mysqli_query($my_db, $chk_order_sql);
    $chk_order_result           = mysqli_fetch_assoc($chk_order_query);

    if(isset($chk_order_result['cnt']) && $chk_order_result['cnt'] > 0){
        exit("<script>alert('이미 등록된 주문입니다. 택배리스트에 주문을 생성할 수 없습니다.');location.href='logistics_management.php?{$search_url}';</script>");
    }

    $address_cnt_sql            = "SELECT COUNT(*) as cnt FROM logistics_address WHERE lm_no='{$lm_no}'";
    $address_cnt_query          = mysqli_query($my_db, $address_cnt_sql);
    $address_cnt_result         = mysqli_fetch_assoc($address_cnt_query);

    $address_empty_cnt_sql      = "SELECT COUNT(*) as cnt FROM logistics_address WHERE lm_no='{$lm_no}' AND (`name` = '' AND hp = '' AND zipcode = '' AND address='')";
    $address_empty_cnt_query    = mysqli_query($my_db, $address_empty_cnt_sql);
    $address_empty_cnt_result   = mysqli_fetch_assoc($address_empty_cnt_query);

    $address_total_cnt      = (isset($address_cnt_result['cnt']) && $address_cnt_result['cnt'] > 0) ? $address_cnt_result['cnt'] : 1;
    $address_empty_cnt      = (isset($address_empty_cnt_result['cnt']) && $address_empty_cnt_result['cnt'] > 0) ? $address_empty_cnt_result['cnt'] : 0;
    $address_cnt            = $address_total_cnt-$address_empty_cnt;

    $logistics_prd_sql      = "SELECT * FROM logistics_product WHERE lm_no='{$lm_no}' AND prd_type='{$logistics['prd_type']}'";
    $logistics_prd_query    = mysqli_query($my_db, $logistics_prd_sql);
    $logistics_prd_list     = [];
    $logistics_total_price  = 0;
    while($logistics_prd = mysqli_fetch_assoc($logistics_prd_query))
    {
        $prd_sql        = "SELECT prd_cms.manager, (SELECT s.team FROM staff s WHERE s.s_no = prd_cms.manager) as team, prd_cms.c_no, (SELECT c.c_name FROM company c WHERE c.c_no = prd_cms.c_no) as c_name FROM product_cms prd_cms WHERE prd_cms.prd_no = '{$logistics_prd['prd_no']}' ORDER BY prd_cms.prd_no DESC LIMIT 1";
        $prd_query      = mysqli_query($my_db, $prd_sql);
        $prd_result     = mysqli_fetch_assoc($prd_query);
        $prd_quantity   = ($logistics_prd['quantity'] <= 1) ? 1 : (int)floor($logistics_prd['quantity']/$address_cnt);
        $prd_quantity   = ($prd_quantity < 1) ? 1 : $prd_quantity;
        $prd_price      = ($logistics_subject == '12') ? $prd_quantity*$logistics_prd['price'] : 0;

        $logistics_prd_list[] = array(
            "s_no"          => isset($prd_result['manager']) ? $prd_result['manager'] : 0,
            "team"          => isset($prd_result['team']) ? $prd_result['team'] : 0,
            "c_no"          => isset($prd_result['c_no']) ? $prd_result['c_no'] : 0,
            "c_name"        => isset($prd_result['c_name']) ? $prd_result['c_name'] : "",
            "price"         => $prd_price,
            "prd_no"        => $logistics_prd['prd_no'],
            "quantity"      => $prd_quantity,
        );

        $logistics_total_price += $prd_price;
    }

    if(!empty($logistics_prd_list))
    {
        $address_sql        = "SELECT * FROM logistics_address WHERE lm_no='{$lm_no}'";
        $address_query      = mysqli_query($my_db, $address_sql);
        $last_mul_ord_no    = $logistics_model->getLastMulNo();
        $company_option     = getAddrCompanyOption();
        $notice            .= !empty($notice) ? "\r\n".$logistics['quick_req_memo'] : $logistics['quick_req_memo'];
        while($address_result = mysqli_fetch_assoc($address_query))
        {
            if(empty($address_result['name']) && empty($address_result['hp']) && empty($address_result['address']) && empty($address_result['zipcode'])){
                continue;
            }

            $last_mul_ord_no++;
            $order_number       = date("Ymd")."_MUL_".sprintf('%04d', $last_mul_ord_no);
            $order_numbers     .= !empty($order_numbers) ? ",".$order_number : $order_number;
            $task_req           = "주문번호: {$order_number}\r\n물류관리시스템 문서번호({$logistics['doc_no']}) 주문추가";
            $ord_date           = date("Y-m-d H:i:s");
            $stock_date         = $cur_date;
            $dp_c_no            = "2007";
            $dp_c_name          = "CS";

            if($logistics['stock_type'] == '3'){
                $dp_c_no    = $logistics['run_c_no'];
                $dp_c_name  = $company_option[$logistics['run_c_no']];
            }else{
                switch($logistics_subject){
                    case '1':
                        $dp_c_no    = "5475";
                        $dp_c_name  = "WISE_광고선전비";
                        break;
                    case '2':
                        $dp_c_no    = "5477";
                        $dp_c_name  = "WISE_소모품비";
                        break;
                    case '3':
                        $dp_c_no    = "5478";
                        $dp_c_name  = "WISE_접대비";
                        break;
                    case '4':
                        $dp_c_no    = "5479";
                        $dp_c_name  = "WISE_복리후생비";
                        break;
                    case '8':
                        $dp_c_no    = "5480";
                        $dp_c_name  = "WISE_폐기손실";
                        break;
                    case '9':
                        $dp_c_no    = "5476";
                        $dp_c_name  = "WISE_시딩반출";
                        break;
                    case '11':
                        $dp_c_no    = "5482";
                        $dp_c_name  = "WISE_타계정반출(기타)";
                        break;
                    case '14':
                        $dp_c_no    = "5939";
                        $dp_c_name  = "WISE_잡손실";
                        break;
                    case '15':
                        $dp_c_no    = "6002";
                        $dp_c_name  = "WISE_감모손실";
                        break;
                    case '12':
                    case '13':
                        $dp_c_no    = $logistics['run_c_no'];
                        $dp_c_name  = $company_option[$logistics['run_c_no']];
                        break;
                }
            }

            $regdate            = $ord_date;
            $zip_code           = $address_result['zipcode'];
            $recipient          = addslashes(trim($address_result['name']));
            $recipient_hp       = $address_result['hp'];
            $recipient_addr     = addslashes(trim($address_result['address']." ".$address_result['address_detail']));
            $delivery_memo      = addslashes(trim($address_result['delivery_memo']));

            if(!empty($order_number)){
                $ord_sql = "SELECT count(order_number) as cnt FROM work_cms WHERE order_number = '{$order_number}'";
                $ord_query = mysqli_query($my_db, $ord_sql);
                $ord_result = mysqli_fetch_assoc($ord_query);

                if($ord_result['cnt'] > 0){
                    exit("<script>alert('이미 택배리스트에 해당 주문번호로 생성된 주문이 있습니다.');location.href='logistics_management.php?{$search_url}';</script>");
                }
            }

            foreach($logistics_prd_list as $logistics_prd)
            {
                $new_ins_data[] = array(
                    "order_number"      => $order_number,
                    "delivery_state"    => "10",
                    "order_date"        => $ord_date,
                    "stock_date"        => $stock_date,
                    "zip_code"          => $zip_code,
                    "recipient"         => $recipient,
                    "recipient_addr"    => $recipient_addr,
                    "recipient_hp"      => $recipient_hp,
                    "dp_c_no"           => $dp_c_no,
                    "dp_c_name"         => $dp_c_name,
                    "regdate"           => $regdate,
                    "write_date"        => $regdate,
                    "task_req"          => addslashes(trim($task_req)),
                    "task_req_s_no"     => $session_s_no,
                    "task_req_team"     => $session_team,
                    "task_run_s_no"     => $session_s_no,
                    "task_run_team"     => $session_team,
                    "task_run_regdate"  => $regdate,
                    "s_no"              => $logistics_prd['s_no'],
                    "team"              => $logistics_prd['team'],
                    "c_no"              => $logistics_prd['c_no'],
                    "c_name"            => $logistics_prd['c_name'],
                    "prd_no"            => $logistics_prd['prd_no'],
                    "unit_price"        => $logistics_prd['price'],
                    "dp_price_vat"      => $logistics_prd['price'],
                    "quantity"          => $logistics_prd['quantity'],
                    "final_price"       => $logistics_total_price,
                    "notice"            => addslashes($notice)
                );
            }
        }
    }

    if(!empty($new_ins_data))
    {
        if(!$cms_model->multiInsert($new_ins_data)){
            exit("<script>alert('등록 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
        }else{
            $logistics_model->update(array("lm_no" => $lm_no, "order_number" => $order_numbers, "work_state" => 6, "run_date" => date("Y-m-d")));

            if(!empty($logistics['w_no'])){
                $work_model->update(array("w_no" => $logistics['w_no'], "work_state" => '6', "task_run_regdate" => date("Y-m-d H:i:s")));
            }
            $result = true;
        }
    }

    if($result){
        exit("<script>alert('등록 성공했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('등록 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }
}
elseif($process == "new_quick")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $lm_no              = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";

    $logistics          = $logistics_model->getItem($lm_no);
    $new_ins_data       = [];
    $order_numbers      = "";
    $result             = false;
    $logistics_subject  = $logistics['subject'];
    $manager_memo       = ($logistics_subject == '12' && !empty($logistics['run_ord_no'])) ? "선입매형 매출 주문번호 : {$logistics['run_ord_no']}" : "";

    if($logistics['work_state'] == '7' || $logistics['work_state'] == '8' || $logistics['work_state'] == '9'){
        exit("<script>alert('입/출고요청 리스트에 주문을 생성할 수 없는 진행상태입니다.');location.href='logistics_management.php?{$search_url}';</script>");
    }

    $address_cnt_sql        = "SELECT COUNT(*) as cnt FROM logistics_address WHERE lm_no='{$lm_no}'";
    $address_cnt_query      = mysqli_query($my_db, $address_cnt_sql);
    $address_cnt_result     = mysqli_fetch_assoc($address_cnt_query);
    $address_cnt            = (isset($address_cnt_result['cnt']) && $address_cnt_result['cnt'] > 0) ? $address_cnt_result['cnt'] : 1;

    $logistics_prd_sql      = "SELECT * FROM logistics_product WHERE lm_no='{$lm_no}' AND prd_type='{$logistics['prd_type']}'";
    $logistics_prd_query    = mysqli_query($my_db, $logistics_prd_sql);
    $logistics_prd_list     = [];
    $logistics_total_price  = 0;
    $regdate                = date("Y-m-d H:i:s");
    while($logistics_prd = mysqli_fetch_assoc($logistics_prd_query))
    {
        if($logistics['prd_type'] == '2')
        {
            $prd_sql        = "
                SELECT
                   (SELECT c.s_no FROM company c WHERE c.c_no = prd_cms.brand) as manager,
                   (SELECT s.team FROM staff s WHERE s.s_no = (SELECT c.s_no FROM company c WHERE c.c_no = prd_cms.brand)) as team,
                   prd_cms.brand as c_no,
                   (SELECT c.c_name FROM company c WHERE c.c_no = prd_cms.brand) as c_name
                FROM product_cms_unit prd_cms
                WHERE prd_cms.`no` = '{$logistics_prd['prd_no']}'
                ORDER BY prd_cms.`no` DESC LIMIT 1
            ";
        }
        else
        {
            $prd_sql        = "
                SELECT
                   prd_cms.manager,
                   (SELECT s.team FROM staff s WHERE s.s_no = prd_cms.manager) as team,
                   prd_cms.c_no,
                   (SELECT c.c_name FROM company c WHERE c.c_no = prd_cms.c_no) as c_name
                FROM product_cms prd_cms
                WHERE prd_cms.prd_no = '{$logistics_prd['prd_no']}'
                ORDER BY prd_cms.prd_no DESC LIMIT 1
            ";
        }
        $prd_query      = mysqli_query($my_db, $prd_sql);
        $prd_result     = mysqli_fetch_assoc($prd_query);
        $prd_quantity   = ($logistics_prd['quantity'] <= 1) ? 1 : (int)floor($logistics_prd['quantity']/$address_cnt);
        $prd_quantity   = ($prd_quantity < 1) ? 1 : $prd_quantity;
        $prd_price      = ($logistics_prd['prd_type'] == 1 && $logistics_subject == '12') ? $prd_quantity*$logistics_prd['price'] : 0;

        $logistics_prd_list[] = array(
            "s_no"          => isset($prd_result['manager']) ? $prd_result['manager'] : 0,
            "team"          => isset($prd_result['team']) ? $prd_result['team'] : 0,
            "c_no"          => isset($prd_result['c_no']) ? $prd_result['c_no'] : 0,
            "c_name"        => isset($prd_result['c_name']) ? $prd_result['c_name'] : "",
            "price"         => $prd_price,
            "prd_no"        => $logistics_prd['prd_no'],
            "quantity"      => $prd_quantity,
        );

        $logistics_total_price += $prd_price;
    }

    if(!empty($logistics_prd_list))
    {
        $address_sql            = "SELECT * FROM logistics_address WHERE lm_no='{$lm_no}'";
        $address_query          = mysqli_query($my_db, $address_sql);
        $last_quick_ord_no      = $cms_model->getLastQuickNo();
        $company_option         = getAddrCompanyOption();
        while($address_result= mysqli_fetch_assoc($address_query))
        {
            $last_quick_ord_no++;
            $order_number       = date("Ymd")."_GDS_".sprintf('%04d', $last_quick_ord_no);
            $order_numbers     .= !empty($order_numbers) ? ",".$order_number : $order_number;
            $task_req           = "주문번호: {$order_number}\r\n물류관리시스템 문서번호({$logistics['doc_no']}) 주문추가";

            if(!empty($order_number)){
                $ord_sql        = "SELECT count(order_number) as cnt FROM work_cms_quick WHERE order_number = '{$order_number}'";
                $ord_query      = mysqli_query($my_db, $ord_sql);
                $ord_result     = mysqli_fetch_assoc($ord_query);

                if($ord_result['cnt'] > 0){
                    exit("<script>alert('이미 입/출고요청 리스트에 해당 주문번호로 생성된 주문이 있습니다.');location.href='logistics_management.php?{$search_url}';</script>");
                }
            }

            foreach($logistics_prd_list as $logistics_prd)
            {
                $new_ins_data[] = array(
                    "order_number"      => $order_number,
                    "doc_no"            => $logistics['doc_no'],
                    "quick_state"       => "1",
                    "zip_code"          => addslashes(trim($address_result['zipcode'])),
                    "sender"            => addslashes(trim($logistics['sender'])),
                    "sender_type"       => $logistics['sender_type'],
                    "sender_type_name"  => $logistics['sender_type_name'],
                    "receiver_type"     => $logistics['receiver_type'],
                    "receiver_type_name"=> $logistics['receiver_type_name'],
                    "receiver"          => addslashes(trim($address_result['name'])),
                    "receiver_addr"     => addslashes(trim($address_result['address']." ".$address_result['address_detail'])),
                    "receiver_hp"       => $address_result['hp'],
                    "run_c_no"          => $logistics['run_c_no'],
                    "run_c_name"        => $company_option[$logistics['run_c_no']],
                    "req_date"          => $logistics['quick_req_date'],
                    "req_s_no"          => $session_s_no,
                    "is_quick_memo"     => $logistics['is_quick_memo'],
                    "quick_memo"        => addslashes(trim($logistics['quick_memo'])),
                    "req_memo"          => addslashes(trim($logistics['quick_req_memo'])),
                    "regdate"           => $regdate,
                    "s_no"              => $logistics_prd['s_no'],
                    "team"              => $logistics_prd['team'],
                    "c_no"              => $logistics_prd['c_no'],
                    "c_name"            => $logistics_prd['c_name'],
                    "prd_type"          => $logistics['prd_type'],
                    "prd_no"            => $logistics_prd['prd_no'],
                    "unit_price"        => $logistics_prd['price'],
                    "quantity"          => $logistics_prd['quantity'],
                    "final_price"       => $logistics_total_price,
                    "manager_memo"      => $manager_memo
                );
            }
        }
    }

    if(!empty($new_ins_data))
    {
        $cms_model->setMainInit("work_cms_quick", "w_no");
        if(!$cms_model->multiInsert($new_ins_data)){
            exit("<script>alert('등록 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
        }else{
            $logistics_model->update(array("lm_no" => $lm_no, "order_number" => $order_numbers, "work_state" => 6, "run_date" => date("Y-m-d")));

            if(!empty($logistics['w_no'])){
                $work_model->update(array("w_no" => $logistics['w_no'], "work_state" => '6', "task_run_regdate" => date("Y-m-d H:i:s")));
            }
            $result = true;

            if($logistics['run_c_no'] != '5469' && $logistics['run_c_no'] != '5468')
            {
                # SMS 전달(쿠팡, 제트 제외)
                $message_model  = Message::Factory();
                $cm_idx         = 1;
                $content        = "주문번호 : {$order_numbers}\r\n입/출고요청 리스트 등록완료\r\n출고희망일 : {$logistics['quick_req_date']}";
                $sms_cm_date    = date("YmdHis");
                $cur_chk_date   = date("Ymd");
                $req_chk_date   = date("Ymd", strtotime($logistics['quick_req_date']));

                $send_data_list     = [];
                $send_name          = "와이즈미디어커머스";
                $send_phone         = "02-830-1912";
                $dest_phone_list    = array("010-6500-1898","010-8521-0738","010-3783-7648","010-6301-0954","010-8304-1892","010-4848-7532");
                $sms_title          = "입/출고요청 리스트 등록완료";
                $sms_msg            = addslashes($content);
                $sms_msg_len        = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
                $msg_type           = ($sms_msg_len > 90) ? "L" : "S";
                $c_info             = "9"; # 입/출고요청 관리
                $c_info_detail      = "NEW_QUICK";

                foreach($dest_phone_list as $desc_phone)
                {
                    $msg_id = "W{$sms_cm_date}".sprintf('%04d', $cm_idx++);

                    $send_data_list[$msg_id] = array(
                        "msg_id"        => $msg_id,
                        "msg_type"      => $msg_type,
                        "sender"        => $send_name,
                        "sender_hp"     => $send_phone,
                        "receiver_hp"   => $desc_phone,
                        "title"         => $sms_title,
                        "content"       => $sms_msg,
                        "cinfo"         => $c_info,
                        "cinfo_detail"  => $c_info_detail
                    );
                }

                if($cur_chk_date < $req_chk_date)
                {
                    $req_send_date      = date("Y-m-d",strtotime($req_chk_date))." 09:00:00";
                    $req_send_datetime  = date("YmdHis",strtotime($req_send_date));
                    $next_sms_title     = "입/출고요청 리스트 금일 출고확인";
                    $next_content       = "주문번호 : {$order_numbers}\r\n입/출고요청 리스트 금일 출고확인";
                    $next_sms_msg       = addslashes($next_content);
                    $next_sms_msg_len   = mb_strlen(iconv('utf-8', 'euc-kr', $next_sms_msg), '8bit');
                    $next_msg_type      = ($next_sms_msg_len > 90) ? "L" : "S";
                    $next_c_detail      = "CHK_QUICK";

                    foreach($dest_phone_list as $desc_phone)
                    {
                        $msg_id = "W{$sms_cm_date}".sprintf('%04d', $cm_idx++);

                        $send_data_list[$msg_id] = array(
                            "msg_id"        => $msg_id,
                            "msg_type"      => $msg_type,
                            "sender"        => $send_name,
                            "sender_hp"     => $send_phone,
                            "receiver_hp"   => $desc_phone,
                            "reserved_time" => $req_send_datetime,
                            "title"         => $next_sms_title,
                            "content"       => $next_sms_msg,
                            "cinfo"         => $c_info,
                            "cinfo_detail"  => $next_c_detail
                        );
                    }
                }

                if(!empty($send_data_list)) {
                    $message_model->sendMessage($send_data_list);
                }
            }
        }
    }

    if($result){
        exit("<script>alert('등록 성공했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('등록 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }
}
elseif($process == "new_commerce")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $lm_no              = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";
    $sup_c_no           = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";

    $logistics          = $logistics_model->getItem($lm_no);
    $order_numbers      = $logistics["order_number"];
    $result             = false;
    $new_log_c_no       = $logistics["receiver_type"];
    $new_unit_brand     = 0;

    if($new_log_c_no == 99999){
        exit("<script>alert('등록 실패했습니다. 기타 수령지는 등록할 수 없습니다.');location.href='logistics_management.php?{$search_url}';</script>");
    }

    # 구성품 리스트
    $logistics_prd_sql      = "
        SELECT 
            pcu.`no`,
            pcu.brand,
            pcu.sup_c_no,
            lp.quantity
        FROM product_cms_unit pcu 
        LEFT JOIN (SELECT lp.prd_no, lp.quantity FROM logistics_product lp WHERE lp.lm_no='{$lm_no}' AND lp.prd_type='2') AS lp ON lp.prd_no=pcu.`no`
        WHERE pcu.sup_c_no='{$sup_c_no}' AND lp.prd_no IS NOT NULL
    ";
    $logistics_prd_query    = mysqli_query($my_db, $logistics_prd_sql);
    $logistics_prd_list     = [];
    $logistics_unit_list    = [];
    $prd_idx                = 0;
    while($logistics_prd_result = mysqli_fetch_assoc($logistics_prd_query)){
        if($prd_idx == 0){
            $new_unit_brand = $logistics_prd_result["brand"];
        }

        $logistics_unit_list[]  = $logistics_prd_result['no'];
        $logistics_prd_list[]   = $logistics_prd_result;

        $prd_idx++;
    }

    # 마지막 발주 가져오기
    $unit_no_text = implode(",", $logistics_unit_list);

    $company_model 		= Company::Factory();
    $company_item  		= $company_model->getItem($new_unit_brand);
    $sup_company_item  	= $company_model->getItem($sup_c_no);
    $reg_date           = date("Y-m-d");
    $reg_datetime       = date('Y-m-d H:i:s');
    $sup_loc_type       = ($sup_company_item['license_type'] == 4) ? 2 : 1;
    $ord_count          = $comm_set_model->getMaxOrderCount($sup_c_no, $reg_date);

    $set_ins_data  = array(
        "state" 		=> 1,
        "c_no" 			=> $new_unit_brand,
        "c_name" 		=> $company_item['c_name'],
        "my_c_no" 		=> $company_item['my_c_no'],
        "team" 			=> $company_item['team'],
        "s_no" 			=> $company_item['s_no'],
        "log_c_no"		=> $new_log_c_no,
        "sup_c_no"		=> $sup_c_no,
        "sup_c_name"	=> $sup_company_item['c_name'],
        "sup_loc_type"	=> $sup_loc_type,
        "order_count"	=> $ord_count,
        "req_team"		=> $session_team,
        "req_s_no"		=> $session_s_no,
        "req_date"		=> $reg_date,
        "regdate"		=> $reg_datetime,
    );

    if($comm_set_model->insert($set_ins_data))
    {
        $new_set_no     = $comm_set_model->getInsertId();
        $unit_ins_data  = [];
        $unit_priority  = 1;
        foreach($logistics_prd_list as $prd_unit_data)
        {
            $unit_option_no     = $prd_unit_data['no'];
            $unit_quantity      = $prd_unit_data['quantity'];
            $unit_item          = $unit_model->getItem($unit_option_no, $new_log_c_no);

            $unit_price         = !empty($unit_item['sup_price_vat']) ? $unit_item['sup_price_vat'] : 0;
            $total_price        = $unit_price*$unit_quantity;
            $supply_price       = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1,0);
            $vat_price          = ($sup_loc_type == '2') ? 0 : ($total_price-$supply_price);

            $unit_ins_data[] = array(
                "set_no"        => $new_set_no,
                "type"          => 'request',
                "option_no"     => $unit_option_no,
                "unit_price"    => $unit_price,
                "quantity"      => $unit_quantity,
                "supply_price"  => $supply_price,
                "vat"           => $vat_price,
                "regdate"       => date("Y-m-d H:i:s"),
                "priority"      => $unit_priority,
                "memo"          => addslashes(trim($_POST['req_memo_new']))
            );

            $unit_priority++;
        }

        if(!empty($unit_ins_data)){
            if($commerce_model->multiInsert($unit_ins_data)){
                $result = true;
                $order_numbers .= !empty($order_numbers) ? ",{$new_set_no}" : $new_set_no;
                $logistics_model->update(array("lm_no" => $lm_no, "order_number" => $order_numbers));
            }
        }
    }

    if($result){
        exit("<script>alert('등록 성공했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('등록 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }
}
elseif($process == "multi_work_state")
{
    $chk_lm_no_list = isset($_POST['chk_lm_no_list']) ? $_POST['chk_lm_no_list'] : "";
    $chk_value      = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $upd_lm_sql     = "";
    $upd_work_sql   = "";

    if(!empty($chk_lm_no_list))
    {
        $cur_date           = date("Y-m-d");
        $cur_datetime       = date("Y-m-d H:i:s");
        $change_lm_column   = "work_state='{$chk_value}'";
        $change_work_column = "work_state='{$chk_value}'";

        if($chk_value == '6'){
            $change_lm_column   .= ", run_date='{$cur_date}', run_s_no='{$session_s_no}', run_team='{$session_team}'";
            $change_work_column .= ", task_run_regdate='{$cur_datetime}', task_run_s_no='{$session_s_no}', task_run_team='{$session_team}'";
        }
        $upd_lm_sql     = "UPDATE logistics_management SET {$change_lm_column} WHERE lm_no IN({$chk_lm_no_list}) AND work_state != '{$chk_value}'";
        $upd_work_sql   = "UPDATE `work` SET {$change_work_column} WHERE w_no IN(SELECT w_no FROM logistics_management WHERE lm_no IN({$chk_lm_no_list}) AND work_state != '{$chk_value}' AND w_no > 0)";
    }

    if(mysqli_query($my_db, $upd_work_sql) && mysqli_query($my_db, $upd_lm_sql)) {
        exit("<script>alert('진행상태를 변경했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    } else {
        exit("<script>alert('진행상태 변경에 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }
}
elseif($process == "multi_add_order")
{
    $chk_lm_no_list     = isset($_POST['chk_lm_no_list']) ? $_POST['chk_lm_no_list'] : "";
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $cms_ord_model      = WorkCms::Factory();
    $cms_quick_model    = WorkCms::Factory();
    $cms_quick_model->setMainInit("work_cms_quick", "w_no");

    $chk_lm_no_sql      = "SELECT * FROM logistics_management WHERE lm_no IN({$chk_lm_no_list}) AND (order_number IS NULL OR order_number = '') AND work_state NOT IN(7,8,9)";
    $chk_lm_no_query    = mysqli_query($my_db, $chk_lm_no_sql);
    $new_ord_ins_data   = [];
    $new_quick_ins_data = [];
    $logistics_upd_data = [];
    $work_upd_data      = [];
    $regdate            = date("Y-m-d H:i:s");
    $last_mul_ord_no    = $logistics_model->getLastMulNo();
    $last_quick_ord_no  = $cms_quick_model->getLastQuickNo();

    # SMS 전달 변수
    $message_model      = Message::Factory();
    $cm_idx             = 1;
    $send_data_list     = [];
    $dest_phone_list    = array("010-6500-1898","010-8521-0738","010-3783-7648","010-6301-0954","010-8304-1892","010-4848-7532");
    $send_name          = "와이즈미디어커머스";
    $send_phone         = "02-830-1912";
    $sms_cm_date        = date("YmdHis");
    $cur_chk_date       = date("Ymd");

    while($chk_lm = mysqli_fetch_assoc($chk_lm_no_query))
    {
        $lm_no          = $chk_lm['lm_no'];
        $prd_type       = $chk_lm['prd_type'];
        $lm_subject     = $chk_lm['subject'];
        $notice         = ($lm_subject == '12' && !empty($chk_lm['run_ord_no'])) ? "선입매형 매출 주문번호 : {$chk_lm['run_ord_no']}" : "";
        $chk_result     = false;
        $order_numbers  = "";

        if($chk_lm['delivery_type'] == '1' && $prd_type == '1')
        {
            $address_cnt_sql        = "SELECT COUNT(*) as cnt FROM logistics_address WHERE lm_no='{$lm_no}'";
            $address_cnt_query      = mysqli_query($my_db, $address_cnt_sql);
            $address_cnt_result     = mysqli_fetch_assoc($address_cnt_query);
            $address_cnt            = (isset($address_cnt_result['cnt']) && $address_cnt_result['cnt'] > 0) ? $address_cnt_result['cnt'] : 1;

            $address_empty_cnt_sql      = "SELECT COUNT(*) as cnt FROM logistics_address WHERE lm_no='{$lm_no}' AND (`name` = '' AND hp = '' AND zipcode = '' AND address='')";
            $address_empty_cnt_query    = mysqli_query($my_db, $address_empty_cnt_sql);
            $address_empty_cnt_result   = mysqli_fetch_assoc($address_empty_cnt_query);

            $address_total_cnt      = (isset($address_cnt_result['cnt']) && $address_cnt_result['cnt'] > 0) ? $address_cnt_result['cnt'] : 1;
            $address_empty_cnt      = (isset($address_empty_cnt_result['cnt']) && $address_empty_cnt_result['cnt'] > 0) ? $address_empty_cnt_result['cnt'] : 0;
            $address_cnt            = $address_total_cnt-$address_empty_cnt;

            $logistics_prd_sql      = "SELECT * FROM logistics_product WHERE lm_no='{$lm_no}' AND prd_type='{$prd_type}'";
            $logistics_prd_query    = mysqli_query($my_db, $logistics_prd_sql);
            $logistics_prd_list     = [];
            $logistics_total_price  = 0;
            while($logistics_prd = mysqli_fetch_assoc($logistics_prd_query))
            {
                $prd_sql        = "SELECT prd_cms.manager, (SELECT s.team FROM staff s WHERE s.s_no = prd_cms.manager) as team, prd_cms.c_no, (SELECT c.c_name FROM company c WHERE c.c_no = prd_cms.c_no) as c_name FROM product_cms prd_cms WHERE prd_cms.prd_no = '{$logistics_prd['prd_no']}' ORDER BY prd_cms.prd_no DESC LIMIT 1";
                $prd_query      = mysqli_query($my_db, $prd_sql);
                $prd_result     = mysqli_fetch_assoc($prd_query);
                $prd_quantity   = ($logistics_prd['quantity'] <= 1) ? 1 : (int)floor($logistics_prd['quantity']/$address_cnt);
                $prd_quantity   = ($prd_quantity < 1) ? 1: $prd_quantity;
                $prd_price      = ($lm_subject == '12') ? $prd_quantity*$logistics_prd['price'] : 0;

                $logistics_prd_list[] = array(
                    "s_no"          => isset($prd_result['manager']) ? $prd_result['manager'] : 0,
                    "team"          => isset($prd_result['team']) ? $prd_result['team'] : 0,
                    "c_no"          => isset($prd_result['c_no']) ? $prd_result['c_no'] : 0,
                    "c_name"        => isset($prd_result['c_name']) ? $prd_result['c_name'] : "",
                    "price"         => $prd_price,
                    "prd_no"        => $logistics_prd['prd_no'],
                    "quantity"      => $prd_quantity,
                );

                $logistics_total_price += $prd_price;
            }

            if(!empty($logistics_prd_list))
            {
                $address_sql        = "SELECT * FROM logistics_address WHERE lm_no='{$lm_no}'";
                $address_query      = mysqli_query($my_db, $address_sql);
                $company_option     = getAddrCompanyOption();
                while($address_result= mysqli_fetch_assoc($address_query))
                {
                    if(empty($address_result['name']) && empty($address_result['hp']) && empty($address_result['address']) && empty($address_result['zipcode'])){
                        continue;
                    }

                    $last_mul_ord_no++;
                    $order_number       = date("Ymd")."_MUL_".sprintf('%04d', $last_mul_ord_no);
                    $order_numbers     .= !empty($order_numbers) ? ",".$order_number : $order_number;
                    $task_req           = "주문번호: {$order_number}\r\n물류관리시스템 문서번호({$chk_lm['doc_no']}) 주문추가";
                    $ord_date           = date("Y-m-d H:i:s");
                    $stock_date         = date("Y-m-d");
                    $dp_c_no            = "2007";
                    $dp_c_name          = "CS";

                    if($chk_lm['stock_type'] == '3'){
                        $dp_c_no    = $chk_lm['run_c_no'];
                        $dp_c_name  = $company_option[$chk_lm['run_c_no']];
                    }else{
                        switch($lm_subject){
                            case '1':
                                $dp_c_no    = "5475";
                                $dp_c_name  = "WISE_광고선전비";
                                break;
                            case '2':
                                $dp_c_no    = "5477";
                                $dp_c_name  = "WISE_소모품비";
                                break;
                            case '3':
                                $dp_c_no    = "5478";
                                $dp_c_name  = "WISE_접대비";
                                break;
                            case '4':
                                $dp_c_no    = "5479";
                                $dp_c_name  = "WISE_복리후생비";
                                break;
                            case '8':
                                $dp_c_no    = "5480";
                                $dp_c_name  = "WISE_폐기손실";
                                break;
                            case '9':
                                $dp_c_no    = "5476";
                                $dp_c_name  = "WISE_시딩반출";
                                break;
                            case '11':
                                $dp_c_no    = "5482";
                                $dp_c_name  = "WISE_타계정반출(기타)";
                                break;
                            case '14':
                                $dp_c_no    = "5939";
                                $dp_c_name  = "WISE_잡손실";
                                break;
                            case '15':
                                $dp_c_no    = "6002";
                                $dp_c_name  = "WISE_감모손실";
                                break;
                            case '12':
                            case '13':
                                $dp_c_no    = $chk_lm['run_c_no'];
                                $dp_c_name  = $company_option[$chk_lm['run_c_no']];
                                break;
                        }
                    }

                    $regdate            = $ord_date;
                    $zip_code           = $address_result['zipcode'];
                    $recipient          = addslashes(trim($address_result['name']));
                    $recipient_hp       = $address_result['hp'];
                    $recipient_addr     = addslashes(trim($address_result['address']." ".$address_result['address_detail']));
                    $delivery_memo      = addslashes(trim($address_result['delivery_memo']));

                    foreach($logistics_prd_list as $logistics_prd)
                    {
                        $new_ord_ins_data[] = array(
                            "order_number"      => $order_number,
                            "delivery_state"    => "10",
                            "order_date"        => $ord_date,
                            "stock_date"        => $stock_date,
                            "zip_code"          => $zip_code,
                            "recipient"         => $recipient,
                            "recipient_addr"    => $recipient_addr,
                            "recipient_hp"      => $recipient_hp,
                            "dp_c_no"           => $dp_c_no,
                            "dp_c_name"         => $dp_c_name,
                            "regdate"           => $regdate,
                            "write_date"        => $regdate,
                            "task_req"          => addslashes(trim($task_req)),
                            "task_req_s_no"     => $session_s_no,
                            "task_req_team"     => $session_team,
                            "task_run_s_no"     => $session_s_no,
                            "task_run_team"     => $session_team,
                            "task_run_regdate"  => $regdate,
                            "s_no"              => $logistics_prd['s_no'],
                            "team"              => $logistics_prd['team'],
                            "c_no"              => $logistics_prd['c_no'],
                            "c_name"            => $logistics_prd['c_name'],
                            "prd_no"            => $logistics_prd['prd_no'],
                            "unit_price"        => $logistics_prd['price'],
                            "dp_price_vat"      => $logistics_prd['price'],
                            "quantity"          => $logistics_prd['quantity'],
                            "final_price"       => $logistics_total_price,
                            "notice"            => $notice
                        );
                    }
                }
            }
        }
        elseif($chk_lm['delivery_type'] == '2')
        {
            $address_cnt_sql        = "SELECT COUNT(*) as cnt FROM logistics_address WHERE lm_no='{$lm_no}'";
            $address_cnt_query      = mysqli_query($my_db, $address_cnt_sql);
            $address_cnt_result     = mysqli_fetch_assoc($address_cnt_query);
            $address_cnt            = (isset($address_cnt_result['cnt']) && $address_cnt_result['cnt'] > 0) ? $address_cnt_result['cnt'] : 1;

            $logistics_prd_sql      = "SELECT * FROM logistics_product WHERE lm_no='{$lm_no}' AND prd_type='{$prd_type}'";
            $logistics_prd_query    = mysqli_query($my_db, $logistics_prd_sql);
            $logistics_prd_list     = [];
            $logistics_total_price  = 0;

            while($logistics_prd = mysqli_fetch_assoc($logistics_prd_query))
            {
                if($prd_type == '2')
                {
                    $prd_sql        = "
                        SELECT
                           (SELECT c.s_no FROM company c WHERE c.c_no = prd_cms.brand) as manager,
                           (SELECT s.team FROM staff s WHERE s.s_no = (SELECT c.s_no FROM company c WHERE c.c_no = prd_cms.brand)) as team,
                           prd_cms.brand as c_no,
                           (SELECT c.c_name FROM company c WHERE c.c_no = prd_cms.brand) as c_name
                        FROM product_cms_unit prd_cms
                        WHERE prd_cms.`no` = '{$logistics_prd['prd_no']}'
                        ORDER BY prd_cms.`no` DESC LIMIT 1
                    ";
                }
                else
                {
                    $prd_sql        = "
                        SELECT
                           prd_cms.manager,
                           (SELECT s.team FROM staff s WHERE s.s_no = prd_cms.manager) as team,
                           prd_cms.c_no,
                           (SELECT c.c_name FROM company c WHERE c.c_no = prd_cms.c_no) as c_name
                        FROM product_cms prd_cms
                        WHERE prd_cms.prd_no = '{$logistics_prd['prd_no']}'
                        ORDER BY prd_cms.prd_no DESC LIMIT 1
                    ";
                }
                $prd_query      = mysqli_query($my_db, $prd_sql);
                $prd_result     = mysqli_fetch_assoc($prd_query);
                $prd_quantity   = ($logistics_prd['quantity'] <= 1) ? 1 : (int)floor($logistics_prd['quantity']/$address_cnt);
                $prd_quantity   = ($prd_quantity < 1) ? 1: $prd_quantity;
                $prd_price      = ($logistics_prd['prd_type'] == 1 && $lm_subject == '12') ? $prd_quantity*$logistics_prd['price'] : 0;

                $logistics_prd_list[] = array(
                    "s_no"          => isset($prd_result['manager']) ? $prd_result['manager'] : 0,
                    "team"          => isset($prd_result['team']) ? $prd_result['team'] : 0,
                    "c_no"          => isset($prd_result['c_no']) ? $prd_result['c_no'] : 0,
                    "c_name"        => isset($prd_result['c_name']) ? $prd_result['c_name'] : "",
                    "price"         => $prd_price,
                    "prd_no"        => $logistics_prd['prd_no'],
                    "quantity"      => $prd_quantity,
                );

                $logistics_total_price += $prd_price;
            }

            if(!empty($logistics_prd_list))
            {
                $address_sql            = "SELECT * FROM logistics_address WHERE lm_no='{$lm_no}'";
                $address_query          = mysqli_query($my_db, $address_sql);
                $company_option         = getAddrCompanyOption();
                while($address_result= mysqli_fetch_assoc($address_query))
                {
                    $last_quick_ord_no++;
                    $order_number       = date("Ymd")."_GDS_".sprintf('%04d', $last_quick_ord_no);
                    $order_numbers     .= !empty($order_numbers) ? ",".$order_number : $order_number;
                    $task_req           = "주문번호: {$order_number}\r\n물류관리시스템 문서번호({$chk_lm['doc_no']}) 주문추가";

                    foreach($logistics_prd_list as $logistics_prd)
                    {
                        $new_quick_ins_data[] = array(
                            "order_number"      => $order_number,
                            "doc_no"            => $chk_lm['doc_no'],
                            "quick_state"       => "1",
                            "zip_code"          => addslashes(trim($address_result['zipcode'])),
                            "sender"            => addslashes(trim($chk_lm['sender'])),
                            "sender_type"       => $chk_lm['sender_type'],
                            "sender_type_name"  => $chk_lm['sender_type_name'],
                            "receiver_type"     => $chk_lm['receiver_type'],
                            "receiver_type_name"=> $chk_lm['receiver_type_name'],
                            "receiver"          => addslashes(trim($address_result['name'])),
                            "receiver_addr"     => addslashes(trim($address_result['address']." ".$address_result['address_detail'])),
                            "receiver_hp"       => $address_result['hp'],
                            "run_c_no"          => $chk_lm['run_c_no'],
                            "run_c_name"        => $company_option[$chk_lm['run_c_no']],
                            "req_date"          => $chk_lm['quick_req_date'],
                            "req_s_no"          => $session_s_no,
                            "is_quick_memo"     => $chk_lm['is_quick_memo'],
                            "quick_memo"        => addslashes(trim($chk_lm['quick_memo'])),
                            "req_memo"          => addslashes(trim($chk_lm['quick_req_memo'])),
                            "regdate"           => $regdate,
                            "s_no"              => $logistics_prd['s_no'],
                            "team"              => $logistics_prd['team'],
                            "c_no"              => $logistics_prd['c_no'],
                            "c_name"            => $logistics_prd['c_name'],
                            "prd_type"          => $chk_lm['prd_type'],
                            "prd_no"            => $logistics_prd['prd_no'],
                            "unit_price"        => $logistics_prd['price'],
                            "quantity"          => $logistics_prd['quantity'],
                            "final_price"       => $logistics_total_price,
                            "manager_memo"      => $notice
                        );
                    }
                }
            }
        }

        if(!empty($order_numbers))
        {
            $logistics_upd_data[] = array(
                "lm_no"         => $lm_no,
                "order_number"  => $order_numbers,
                "work_state"    => 6,
                "run_date"      => date("Y-m-d"),
            );

            if(!empty($chk_lm['w_no'])){
                $work_upd_data[] = array(
                    "w_no"              => $chk_lm['w_no'],
                    "work_state"        => 6,
                    "task_run_regdate"  => date("Y-m-d H:i:s"),
                );
            }

            if($chk_lm['delivery_type'] == 2 && $chk_lm['run_c_no'] != '5469' && $chk_lm['run_c_no'] != '5468')
            {
                # SMS 전달(쿠팡, 제트 제외)
                $content        = "주문번호 : {$order_numbers}\r\n입/출고요청 리스트 등록완료\r\n출고희망일 : {$chk_lm['quick_req_date']}";
                $req_chk_date   = date("Ymd", strtotime($chk_lm['quick_req_date']));
                $sms_title      = "입/출고요청 리스트 등록완료";
                $sms_msg        = addslashes($content);
                $sms_msg_len    = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
                $msg_type       = ($sms_msg_len > 90) ? "L" : "S";
                $c_info         = "9"; # 입/출고요청 관리
                $c_info_detail  = "NEW_QUICK";

                foreach($dest_phone_list as $desc_phone)
                {
                    $msg_id = "W{$sms_cm_date}".sprintf('%04d', $cm_idx++);

                    $send_data_list[$msg_id] = array(
                        "msg_id"        => $msg_id,
                        "msg_type"      => $msg_type,
                        "sender"        => $send_name,
                        "sender_hp"     => $send_phone,
                        "receiver_hp"   => $desc_phone,
                        "title"         => $sms_title,
                        "content"       => $sms_msg,
                        "cinfo"         => $c_info,
                        "cinfo_detail"  => $c_info_detail
                    );
                }

                if($cur_chk_date < $req_chk_date)
                {
                    $req_send_date      = date("Y-m-d",strtotime($req_chk_date))." 09:00:00";
                    $req_send_datetime  = date("YmdHis",strtotime($req_send_date));
                    $next_sms_title     = "입/출고요청 리스트 금일 출고확인";
                    $next_content       = "주문번호 : {$order_numbers}\r\n입/출고요청 리스트 금일 출고확인";
                    $next_sms_msg       = addslashes($next_content);
                    $next_sms_msg_len   = mb_strlen(iconv('utf-8', 'euc-kr', $next_sms_msg), '8bit');
                    $next_msg_type      = ($next_sms_msg_len > 90) ? "L" : "S";
                    $next_c_detail      = "CHK_QUICK";

                    foreach($dest_phone_list as $desc_phone)
                    {
                        $msg_id = "W{$sms_cm_date}".sprintf('%04d', $cm_idx++);

                        $send_data_list[$msg_id] = array(
                            "msg_id"        => $msg_id,
                            "msg_type"      => $msg_type,
                            "sender"        => $send_name,
                            "sender_hp"     => $send_phone,
                            "receiver_hp"   => $desc_phone,
                            "reserved_time" => $req_send_datetime,
                            "title"         => $next_sms_title,
                            "content"       => $next_sms_msg,
                            "cinfo"         => $c_info,
                            "cinfo_detail"  => $next_c_detail
                        );
                    }
                }
            }
        }
    }

    $ord_cnt = 0;
    if(!empty($new_ord_ins_data))
    {
        $cnt_ord_list = [];
        foreach($new_ord_ins_data as $ord_ins_data)
        {
            if($cms_ord_model->insert($ord_ins_data)){
                if(!isset($cnt_ord_list[$ord_ins_data['order_number']])){
                    $cnt_ord_list[$ord_ins_data['order_number']] = 1;
                    $ord_cnt++;
                }
            }
        }
    }

    $quick_cnt = 0;
    if(!empty($new_quick_ins_data))
    {
        $cnt_quick_list = [];
        foreach($new_quick_ins_data as $quick_ins_data)
        {
            if($cms_quick_model->insert($quick_ins_data)){
                if(!isset($cnt_quick_list[$quick_ins_data['order_number']])){
                    $cnt_quick_list[$quick_ins_data['order_number']] = 1;
                    $quick_cnt++;
                }
            }
        }
    }

    if($ord_cnt > 0 || $quick_cnt > 0) {
        $logistics_model->multiUpdate($logistics_upd_data);
        $work_model->multiUpdate($work_upd_data);
        if(!empty($send_data_list)) {
            $message_model->sendMessage($send_data_list);
        }
        exit("<script>alert('택배리스트 {$ord_cnt}건, 입/출고요청 리스트 {$quick_cnt}건 추가했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('주문 추가에 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }
}
elseif($process == "new_in_stock_report")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $lm_no          = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";
    $confirm_state  = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $match_option   = getMatchingStateByConfirmStateOption();
    $stock_state    = $match_option[$confirm_state];

    $logistics      = $logistics_model->getItem($lm_no);
    $ins_stock_data = [];
    $regdate        = date("Y-m-d");
    $regdatetime    = date("Y-m-d H:i:s");

    $prd_type       = $logistics['prd_type'];
    $ware_log_c_no  = $logistics['receiver_type'];

    $unit_list  = [];
    if($prd_type == "1")
    {
        $prd_sql   = "SELECT lp.log_c_no, lp.prd_no, lp.quantity FROM logistics_product as lp WHERE lm_no='{$lm_no}' AND prd_type='{$prd_type}'";
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $unit_sql  = "
                SELECT 
                    pcu.`no` as option_no, 
                    pcu.option_name,
                    (SELECT c.c_name FROM company `c` WHERE `c`.c_no=pcu.brand) AS brand,
                    pcu.sup_c_no,
                    pcu.type as prd_kind,
                    pcum.sku, 
                    pcr.quantity
                FROM product_cms_relation AS pcr 
                LEFT JOIN product_cms_unit AS pcu ON pcu.`no`=pcr.option_no
                LEFT JOIN product_cms_unit_management as pcum ON pcum.prd_unit=pcr.option_no 
                WHERE pcr.prd_no='{$prd['prd_no']}' AND pcr.display='1' AND pcum.log_c_no='{$prd['log_c_no']}'
            ";
            $unit_query = mysqli_query($my_db, $unit_sql);

            while($unit = mysqli_fetch_assoc($unit_query))
            {
                $unit['qty']    = $prd['quantity']*$unit['quantity'];
                $unit_list[]    = $unit;
            }
        }
    }elseif($prd_type == "2"){
        $prd_sql  = "
            SELECT 
                pcu.`no` as option_no, 
                pcu.type as prd_kind,
                pcu.option_name,
                (SELECT c.c_name FROM company `c` WHERE `c`.c_no=pcu.brand) AS brand,
                pcu.sup_c_no,
                pcu.type as prd_kind,
                pcum.sku, 
                lm.quantity as qty
            FROM logistics_product AS lm 
            LEFT JOIN product_cms_unit AS pcu ON pcu.`no`=lm.prd_no
            LEFT JOIN product_cms_unit_management as pcum ON pcum.prd_unit=lm.prd_no AND pcum.log_c_no=lm.log_c_no
            WHERE lm.lm_no='{$lm_no}' AND lm.prd_type='{$prd_type}'
        ";
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $unit_list[] = $prd;
        }
    }

    if(!empty($unit_list))
    {
        foreach ($unit_list as $unit_data)
        {
            $ins_stock_data[] = array(
                "log_doc_no"        => $logistics['doc_no'],
                "log_subject"       => $logistics['subject'],
                "regdate"           => $regdate,
                "report_type"       => 3,
                "type"              => 1,
                "client"            => "와이즈플래닛컴퍼니",
                "doc_no"            => 1,
                "order"             => 1,
                "state"             => $stock_state,
                "confirm_state"     => $confirm_state,
                "brand"             => $unit_data['brand'],
                "prd_kind"          => ($unit_data['prd_kind'] == "1") ? "상품" : "부속품",
                "prd_unit"          => $unit_data['option_no'],
                "sup_c_no"          => $unit_data['sup_c_no'],
                "log_c_no"          => "1113",
                "option_name"       => $unit_data['option_name'],
                "sku"               => $unit_data['sku'],
                "prd_name"          => $unit_data['option_name'],
                "stock_type"        => "사내창고",
                "stock_qty"         => $unit_data['qty'],
                "memo"              => "물류시스템 {$logistics['doc_no']} 입고 직접추가",
                "reg_s_no"          => $session_s_no,
                "report_date"       => $regdatetime
            );
        }
    }

    if($report_model->multiInsert($ins_stock_data)) {
        exit("<script>alert('입고리스트에 등록했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('입고리스트 등록에 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }
}
elseif($process == "new_out_stock_report")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $lm_no          = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";
    $confirm_state  = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $match_option   = getMatchingStateByConfirmStateOption();
    $stock_state    = $match_option[$confirm_state];

    $logistics      = $logistics_model->getItem($lm_no);
    $ins_stock_data = [];
    $regdate        = date("Y-m-d");
    $regdatetime    = date("Y-m-d H:i:s");
    $add_warehouse_option = $report_model->getWarehouseBaseList();

    $prd_type       = $logistics['prd_type'];
    $ware_log_c_no  = $logistics['sender_type'];
    $add_warehouse  = "";
    foreach($add_warehouse_option[$ware_log_c_no] as $warehouse){
        $add_warehouse = $warehouse;
        break;
    }

    $unit_list      = [];
    if($prd_type == "1")
    {
        $prd_sql   = "SELECT lp.log_c_no, lp.prd_no, lp.quantity FROM logistics_product as lp WHERE lm_no='{$lm_no}' AND prd_type='{$prd_type}'";
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $unit_sql  = "
                SELECT 
                    pcu.`no` as option_no, 
                    pcu.option_name,
                    (SELECT c.c_name FROM company `c` WHERE `c`.c_no=pcu.brand) AS brand,
                    pcu.sup_c_no,
                    pcu.type as prd_kind,
                    pcum.sku,
                    pcr.quantity
                FROM product_cms_relation AS pcr 
                LEFT JOIN product_cms_unit AS pcu ON pcu.`no`=pcr.option_no
                LEFT JOIN product_cms_unit_management as pcum ON pcum.prd_unit=pcr.option_no 
                WHERE pcr.prd_no='{$prd['prd_no']}' AND pcr.display='1' AND pcum.log_c_no='{$prd['log_c_no']}'
            ";
            $unit_query = mysqli_query($my_db, $unit_sql);
            while($unit = mysqli_fetch_assoc($unit_query))
            {
                $unit['qty']        = $prd['quantity']*$unit['quantity'];
                $unit_list[]        = $unit;
            }
        }
    }elseif($prd_type == "2"){
        $prd_sql  = "
            SELECT 
                pcu.`no` as option_no, 
                pcu.option_name,
                (SELECT c.c_name FROM company `c` WHERE `c`.c_no=pcu.brand) AS brand,
                pcu.sup_c_no,
                pcu.type as prd_kind,
                pcum.sku, 
                lm.quantity as qty
            FROM logistics_product AS lm 
            LEFT JOIN product_cms_unit AS pcu ON pcu.`no`=lm.prd_no
            LEFT JOIN product_cms_unit_management as pcum ON pcum.prd_unit=lm.prd_no AND pcum.log_c_no=lm.log_c_no
            WHERE lm.lm_no='{$lm_no}' AND lm.prd_type='{$prd_type}'
        ";
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $unit_list[] = $prd;
        }
    }

    if(!empty($unit_list))
    {
        foreach ($unit_list as $unit_data)
        {
            $ins_stock_data[] = array(
                "log_doc_no"        => $logistics['doc_no'],
                "log_subject"       => $logistics['subject'],
                "regdate"           => $regdate,
                "report_type"       => 3,
                "type"              => 2,
                "client"            => "와이즈플래닛컴퍼니",
                "doc_no"            => 1,
                "order"             => 1,
                "state"             => $stock_state,
                "confirm_state"     => $confirm_state,
                "brand"             => $unit_data['brand'],
                "prd_kind"          => ($unit_data['prd_kind'] == "1") ? "상품" : "부속품",
                "prd_unit"          => $unit_data['option_no'],
                "sup_c_no"          => $unit_data['sup_c_no'],
                "log_c_no"          => $ware_log_c_no,
                "option_name"       => $unit_data['option_name'],
                "sku"               => $unit_data['sku'],
                "prd_name"          => $unit_data['option_name'],
                "stock_type"        => $add_warehouse,
                "stock_qty"         => -$unit_data['qty'],
                "memo"              => "물류시스템 {$logistics['doc_no']} 출고 직접추가",
                "reg_s_no"          => $session_s_no,
                "report_date"       => $regdatetime
            );
        }
    }

    if($report_model->multiInsert($ins_stock_data)) {
        exit("<script>alert('출고리스트에 등록했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('출고리스트 등록에 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }
}
elseif($process == "new_move_stock_report")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $lm_no              = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";
    $chk_type           = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";

    # 물류관리 호출
    $logistics          = $logistics_model->getItem($lm_no);
    $regdate            = date("Y-m-d");
    $regdatetime        = date("Y-m-d H:i:s");
    $warehouse_option   = $move_model->getWarehouseBaseList();
    $add_type           = ($chk_type == "in") ? "1" : "2";
    $add_confirm_state  = ($chk_type == "in") ? "3" : "6";
    $add_log_c_no       = ($chk_type == "in") ? $logistics['receiver_type'] : $logistics['sender_type'];
    $chk_log_c_no       = ($chk_type == "in") ? $logistics['sender_type'] : $logistics['receiver_type'];
    $add_stock_state    = ($chk_type == "in") ? ($chk_log_c_no != "1113" ? "이동입고" : "기타입고") : ($chk_log_c_no != "1113" ? "이동반출" : "기타반출");
    $prd_type           = $logistics['prd_type'];
    $with_log_c_no      = 2809;

    $unit_list          = [];
    if($prd_type == "1")
    {
        $prd_sql   = "SELECT lp.prd_no, lp.log_c_no, lp.quantity FROM logistics_product as lp WHERE lm_no='{$lm_no}' AND prd_type='{$prd_type}'";
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $unit_sql  = "
                SELECT 
                    pcu.`no` as option_no, 
                    pcu.option_name,
                    pcu.type,
                    (SELECT c.c_name FROM company `c` WHERE `c`.c_no=pcu.brand) AS brand,
                    pcu.sup_c_no,
                    pcum.sku,
                    pcum.warehouse,
                    pcr.quantity
                FROM product_cms_relation AS pcr 
                LEFT JOIN product_cms_unit AS pcu ON pcu.`no`=pcr.option_no
                LEFT JOIN product_cms_unit_management as pcum ON pcum.prd_unit=pcr.option_no 
                WHERE pcr.prd_no='{$prd['prd_no']}' AND pcr.display='1' AND pcum.log_c_no='{$prd['log_c_no']}'
            ";
            $unit_query = mysqli_query($my_db, $unit_sql);

            while($unit = mysqli_fetch_assoc($unit_query))
            {
                $unit['qty'] = $prd['quantity']*$unit['quantity'];
                $unit_list[] = $unit;
            }
        }
    }elseif($prd_type == "2"){
        $prd_sql  = "
            SELECT 
                pcu.`no` as option_no, 
                pcu.option_name,
                pcu.type,
                (SELECT c.c_name FROM company `c` WHERE `c`.c_no=pcu.brand) AS brand,
                pcu.sup_c_no,
                pcum.sku, 
                pcum.warehouse, 
                lm.quantity as qty
            FROM logistics_product AS lm 
            LEFT JOIN product_cms_unit AS pcu ON pcu.`no`=lm.prd_no
            LEFT JOIN product_cms_unit_management as pcum ON pcum.prd_unit=lm.prd_no AND pcum.log_c_no=lm.log_c_no
            WHERE lm.lm_no='{$lm_no}' AND lm.prd_type='{$prd_type}'
        ";
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $unit_list[] = $prd;
        }
    }

    if(!empty($unit_list))
    {
        foreach ($unit_list as $unit_data)
        {
            $add_warehouse  = "";

            if($add_log_c_no == "2809") {
                $add_warehouse = $unit_data['warehouse'];
            }else{
                foreach($warehouse_option[$add_log_c_no] as $warehouse){
                    $add_warehouse = $warehouse;
                    break;
                }
            }

            $stock_qty = ($chk_type == "in") ? $unit_data['qty'] : -$unit_data['qty'];
            $ins_stock_data = array(
                "type"              => $add_type,
                "state"             => $add_stock_state,
                "confirm_state"     => $add_confirm_state,
                "log_c_no"          => $add_log_c_no,
                "stock_type"        => $add_warehouse,
                "stock_qty"         => $stock_qty,
                "log_doc_no"        => $logistics['doc_no'],
                "log_subject"       => $logistics['subject'],
                "regdate"           => $regdate,
                "report_type"       => 3,
                "client"            => "와이즈플래닛컴퍼니",
                "doc_no"            => 1,
                "order"             => 1,
                "brand"             => $unit_data['brand'],
                "prd_kind"          => $unit_data['brand'],
                "prd_unit"          => $unit_data['option_no'],
                "sup_c_no"          => $unit_data['sup_c_no'],
                "option_name"       => $unit_data['option_name'],
                "sku"               => $unit_data['sku'],
                "prd_name"          => $unit_data['option_name'],
                "memo"              => "물류시스템 {$logistics['doc_no']} 재고이동 직접추가",
                "reg_s_no"          => $session_s_no,
                "report_date"       => $regdatetime
            );

            $report_model->insert($ins_stock_data);
        }
    }
    exit("<script>alert('재고이동 리스트에에 등록했습니다');location.href='logistics_management.php?{$search_url}';</script>");
}
elseif($process == "del_order")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $lm_no              = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";

    $logistics          = $logistics_model->getItem($lm_no);

    if(empty($logistics['order_number'])){
        exit("<script>alert('주문번호가 없습니다. 택배리스트 주문을 삭제할 수 없습니다.');location.href='logistics_management.php?{$search_url}';</script>");
    }

    $order_number_list  = explode(",", $logistics['order_number']);
    $ord_chk_list       = [];
    foreach($order_number_list as $log_ord){
        $ord_chk_list[] = "'{$log_ord}'";
    }
    $ord_chk_where      = implode(",", $ord_chk_list);
    $ord_del_sql        = "";

    if($logistics['delivery_type'] == "1")
    {
        $ord_del_sql = "DELETE FROM work_cms WHERE order_number IN({$ord_chk_where})";
    }
    elseif($logistics['delivery_type'] == "2")
    {
        $ord_del_sql = "DELETE FROM work_cms_quick WHERE order_number IN({$ord_chk_where})";
    }

    if(!empty($ord_del_sql)){
        if(mysqli_query($my_db, $ord_del_sql)){
            $logistics_model->update(array("lm_no" => $logistics['lm_no'], "order_number" => "NULL"));
            exit("<script>alert('취소/삭제 처리했습니다.');location.href='logistics_management.php?{$search_url}';</script>");
        }
    }

    exit("<script>alert('취소/삭제에 실패했습니다.');location.href='logistics_management.php?{$search_url}';</script>");
}
elseif($process == "new_error")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $lm_no      = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";
    $regdate    = date("Y-m-d H:i:s");

    $logistic_item  = $logistics_model->getItem($lm_no);
    $work_item      = $work_model->getItem($logistic_item['w_no']);
    $new_log_data   = $logistic_item;

    $new_log_data['regdate']        = $regdate;
    $new_log_data['quick_req_date'] = "NULL";
    $new_log_data['doc_no']         = $logistics_model->getNewDocNo($logistic_item['prd_no']);
    $new_log_data['work_state']     = 3;
    $new_log_data['order_number']   = "NULL";
    $new_log_data['reason']         = "오배송 회수\r\n문서번호 : {$logistic_item['doc_no']} 적용";
    unset($new_log_data['lm_no']);
    unset($new_log_data['w_no']);

    $new_work_data  = $work_item;
    $new_work_data['work_state'] = 3;
    $new_work_data['regdate']    = $regdate;
    unset($new_work_data['w_no']);
    unset($new_work_data['task_req_dday']);
    unset($new_work_data['task_run_dday']);
    unset($new_work_data['task_run_regdate']);
    unset($new_work_data['linked_no']);

    if($work_model->insert($new_work_data))
    {
        $new_w_no               = $work_model->getInsertId();
        $new_log_data["w_no"]   = $new_w_no;

        if($logistics_model->insert($new_log_data))
        {
            $new_lm_no = $logistics_model->getInsertId();
            $work_model->update(array("w_no" => $new_w_no, "linked_no" => $new_lm_no));

            $new_addr_sql   = "SELECT * FROM logistics_address WHERE lm_no='{$logistic_item['lm_no']}'";
            $new_addr_query = mysqli_query($my_db, $new_addr_sql);
            $new_addr_data  = [];
            while($new_addr = mysqli_fetch_assoc($new_addr_query)){
                $new_addr_data[] = array(
                    "lm_no"             => $new_lm_no,
                    "zipcode"           => $new_addr["zipcode"],
                    "address"           => $new_addr["address"],
                    "address_detail"    => $new_addr["address_detail"],
                    "name"              => $new_addr["name"],
                    "hp"                => $new_addr["hp"],
                    "delivery_memo"     => $new_addr["delivery_memo"],
                );
            }

            $new_prd_sql    = "SELECT * FROM logistics_product WHERE lm_no='{$logistic_item['lm_no']}'";
            $new_prd_query  = mysqli_query($my_db, $new_prd_sql);
            $new_prd_data   = [];
            while($new_prd = mysqli_fetch_assoc($new_prd_query)){
                $new_prd_data[] = array(
                    'lm_no'     => $new_lm_no,
                    'log_c_no'  => $new_prd['log_c_no'],
                    'prd_type'  => $new_prd['prd_type'],
                    'prd_no'    => $new_prd['prd_no'],
                    'price'     => $new_prd['price'],
                    'quantity'  => -$new_prd['quantity'],
                );
            }

            if(!empty($new_addr_data)) {
                $logistics_addr_model   = Logistics::Factory();
                $logistics_addr_model->setMainInit("logistics_address", "la_no");
                $logistics_addr_model->multiInsert($new_addr_data);
            }

            if(!empty($new_prd_data)) {
                $logistics_prd_model    = Logistics::Factory();
                $logistics_prd_model->setMainInit("logistics_product", "lp_no");
                $logistics_prd_model->multiInsert($new_prd_data);
            }

            exit("<script>alert('오배송 회수 처리의 성공했습니다');location.href='logistics_request.php?lm_no={$new_lm_no}';</script>");
        }else{
            $work_model->delete($new_w_no);
            exit("<script>alert('오배송 회수 처리의 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
        }

    }else{
        exit("<script>alert('오배송 회수 처리의 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }
}
elseif($process == "del_logistics")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $lm_no              = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";
    $logistics          = $logistics_model->getItem($lm_no);

    if(!empty($logistics['order_number'])){
        exit("<script>alert('주문번호가 있습니다. 해당 건은 삭제할 수 없습니다.');location.href='logistics_management.php?{$search_url}';</script>");
    }

    if($logistics['work_state'] != '1' && $logistics['work_state'] != '2' && $logistics['work_state'] != '3'){
        exit("<script>alert('삭제가능한 진행상태가 아닙니다.');location.href='logistics_management.php?{$search_url}';</script>");
    }

    if($logistics_model->delLogisticsTotal($lm_no)){
        $work_model->delete($logistics['w_no']);
        exit("<script>alert('삭제했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제에 실패했습니다');location.href='logistics_management.php?{$search_url}';</script>");
    }
}

# 물류시스템 리스트 페이지
$is_admin_editable  = (permissionNameCheck($session_permission, "물류관리자")) ? true : false;
$is_editable        = (permissionNameCheck($session_permission, "물류관리자") || $session_s_no == '102' || $session_s_no == '18') ? true : false;
$smarty->assign("is_admin_editable", $is_admin_editable);
$smarty->assign("is_editable", $is_editable);

# Navigation & My Quick
$nav_prd_no  = "149";
$nav_title   = "물류관리 시스템";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

#검색 조건
$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));
$years_val  = date('Y-m-d', strtotime('-2 years'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);
$smarty->assign("years_val", $years_val);

$add_where          = "1=1";
$sch_lm_no          = isset($_GET['sch_lm_no']) ? $_GET['sch_lm_no'] : "";
$sch_reg_s_date     = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : $month_val;
$sch_reg_e_date     = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : $today_val;
$sch_reg_date_type  = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "month";
$sch_work_state     = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_run_s_date     = isset($_GET['sch_run_s_date']) ? $_GET['sch_run_s_date'] : "";
$sch_run_e_date     = isset($_GET['sch_run_e_date']) ? $_GET['sch_run_e_date'] : "";
$sch_doc_no         = isset($_GET['sch_doc_no']) ? $_GET['sch_doc_no'] : "";
$sch_req_name       = isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : "";
$sch_req_my         = isset($_GET['sch_req_my']) ? $_GET['sch_req_my'] : "";
$sch_stock_type     = isset($_GET['sch_stock_type']) ? $_GET['sch_stock_type'] : "";
$sch_subject        = isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
$sch_delivery_type  = isset($_GET['sch_delivery_type']) ? $_GET['sch_delivery_type'] : "";
$sch_warehouse      = isset($_GET['sch_warehouse']) ? $_GET['sch_warehouse'] : "";
$sch_sender_type    = isset($_GET['sch_sender_type']) ? $_GET['sch_sender_type'] : "";
$sch_receiver_type  = isset($_GET['sch_receiver_type']) ? $_GET['sch_receiver_type'] : "";
$sch_is_order       = isset($_GET['sch_is_order']) ? $_GET['sch_is_order'] : "";
$sch_run_name       = isset($_GET['sch_run_name']) ? $_GET['sch_run_name'] : "";
$sch_run_my         = isset($_GET['sch_run_my']) ? $_GET['sch_run_my'] : "";
$sch_task_run       = isset($_GET['sch_task_run']) ? $_GET['sch_task_run'] : "";
$sch_quick_type     = isset($_GET['sch_quick_type']) ? $_GET['sch_quick_type'] : "";
$sch_warehouse      = isset($_GET['sch_warehouse']) ? $_GET['sch_warehouse'] : "";
$sch_reason         = isset($_GET['sch_reason']) ? $_GET['sch_reason'] : "";
$sch_order_number   = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_quick_req_date = isset($_GET['sch_quick_req_date']) ? $_GET['sch_quick_req_date'] : "";
$sch_quick_req_memo = isset($_GET['sch_quick_req_memo']) ? $_GET['sch_quick_req_memo'] : "";
$sch_prd_sku        = isset($_GET['sch_prd_sku']) ? $_GET['sch_prd_sku'] : "";
$sch_is_in_out      = isset($_GET['sch_is_in_out']) ? $_GET['sch_is_in_out'] : "";

if(!empty($sch_quick_type))
{
    switch($sch_quick_type){
        case '1':
            $sch_work_state = 3;
            $add_where  .= " AND run_c_no='5468'";
            break;
        case '2':
            $sch_work_state = 3;
            $add_where  .= " AND run_c_no='5469'";
            break;
        case '3':
            $sch_work_state = 3;
            $add_where  .= " AND (run_c_no NOT IN('5468','5469') AND run_c_no > 0)";
            break;
    }
}

if(!empty($sch_lm_no)) {
    $add_where  .= " AND lm.lm_no='{$sch_lm_no}'";
    $smarty->assign("sch_lm_no", $sch_lm_no);
}

if(!empty($sch_reg_s_date) || !empty($sch_reg_e_date))
{
    if(!empty($sch_reg_s_date) && empty($sch_reg_e_date)){
        $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
        $add_where .= " AND lm.regdate >= '{$sch_reg_s_datetime}'";
    }elseif(!empty($sch_reg_e_date) && empty($sch_reg_s_date)){
        $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
        $add_where .= " AND lm.regdate <= '{$sch_reg_e_datetime}'";
    }else{
        $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
        $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";

        $add_where .= " AND (lm.regdate BETWEEN '{$sch_reg_s_datetime}' AND '{$sch_reg_e_datetime}')";
    }

    $smarty->assign('sch_reg_s_date', $sch_reg_s_date);
    $smarty->assign('sch_reg_e_date', $sch_reg_e_date);
}
$smarty->assign('sch_reg_date_type', $sch_reg_date_type);

if(!empty($sch_work_state)) {
    $add_where  .= " AND lm.work_state='{$sch_work_state}'";
    $smarty->assign("sch_work_state", $sch_work_state);
}

if(!empty($sch_run_s_date) || !empty($sch_run_e_date))
{
    if(!empty($sch_run_s_date) && empty($sch_run_e_date)){
        $sch_run_s_datetime = $sch_run_s_date;
        $add_where .= " AND lm.run_date >= '{$sch_run_s_datetime}'";
    }elseif(!empty($sch_run_e_date) && empty($sch_run_s_date)){
        $sch_run_e_datetime = $sch_run_e_date;
        $add_where .= " AND lm.run_date <= '{$sch_run_e_datetime}'";
    }else{
        $sch_run_s_datetime = $sch_run_s_date;
        $sch_run_e_datetime = $sch_run_e_date;

        $add_where .= " AND (lm.run_date BETWEEN '{$sch_run_s_datetime}' AND '{$sch_run_e_datetime}')";
    }

    $smarty->assign('sch_run_s_date', $sch_run_s_date);
    $smarty->assign('sch_run_e_date', $sch_run_e_date);
}

if(!empty($sch_doc_no)) {
    $add_where  .= " AND lm.doc_no LIKE '%{$sch_doc_no}%'";
    $smarty->assign("sch_doc_no", $sch_doc_no);
}

if(!empty($sch_req_my)){
    $add_where  .= " AND lm.req_s_no ='{$session_s_no}'";
    $smarty->assign("sch_req_my", $sch_req_my);
}else{
    if(!empty($sch_req_name)) {
        $add_where  .= " AND lm.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_req_name}%')";
        $smarty->assign("sch_req_name", $sch_req_name);
    }
}

if(!empty($sch_stock_type)) {
    $add_where  .= " AND lm.stock_type='{$sch_stock_type}'";
    $smarty->assign("sch_stock_type", $sch_stock_type);
}

if(!empty($sch_subject)) {
    $add_where  .= " AND lm.subject='{$sch_subject}'";
    $smarty->assign("sch_subject", $sch_subject);
}

if(!empty($sch_delivery_type)) {
    if($sch_delivery_type == "5"){
        $add_where  .= " AND lm.delivery_type != '4'";
    }else{
        $add_where  .= " AND lm.delivery_type='{$sch_delivery_type}'";
    }

    $smarty->assign("sch_delivery_type", $sch_delivery_type);
}

if(!empty($sch_warehouse)) {
    $add_where  .= " AND lm.lm_no IN(SELECT DISTINCT lm_no FROM logistics_product sub LEFT JOIN product_cms_relation pcr ON pcr.prd_no=sub.prd_no LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcr.option_no WHERE prd_type='1' AND pcr.display='1' AND pcu.warehouse='{$sch_warehouse}')";
    $smarty->assign("sch_warehouse", $sch_warehouse);
}

if(!empty($sch_sender_type)) {
    $add_where  .= " AND lm.sender_type='{$sch_sender_type}'";
    $smarty->assign("sch_sender_type", $sch_sender_type);
}

if(!empty($sch_receiver_type)) {
    $add_where  .= " AND lm.receiver_type='{$sch_receiver_type}'";
    $smarty->assign("sch_receiver_type", $sch_receiver_type);
}

if(!empty($sch_is_order)) {
    if($sch_is_order == '1'){
        $add_where  .= " AND lm.order_number != ''";
    }else{
        $add_where  .= " AND (lm.order_number IS NULL OR lm.order_number = '')";
    }
    $smarty->assign("sch_is_order", $sch_is_order);
}

if(!empty($sch_run_my)){
    $add_where  .= " AND lm.run_s_no ='{$session_s_no}'";
    $smarty->assign("sch_run_my", $sch_run_my);
}else{
    if(!empty($sch_run_name)) {
        $add_where  .= " AND lm.run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_run_name}%')";
        $smarty->assign("sch_run_name", $sch_run_name);
    }
}

if(!empty($sch_task_run)) {
    $add_where  .= " AND lm.task_run LIKE '%{$sch_task_run}%'";
    $smarty->assign("sch_task_run", $sch_task_run);
}

if(!empty($sch_reason)) {
    $add_where  .= " AND lm.reason LIKE '%{$sch_reason}%'";
    $smarty->assign("sch_reason", $sch_reason);
}

if(!empty($sch_order_number)){
    $add_where  .= " AND FIND_IN_SET('{$sch_order_number}',lm.order_number) > 0";
    $smarty->assign("sch_order_number", $sch_order_number);
}

if(!empty($sch_quick_req_date)){
    $add_where  .= " AND quick_req_date = '{$sch_quick_req_date}'";
    $smarty->assign("sch_quick_req_date", $sch_quick_req_date);
}

if(!empty($sch_quick_req_memo)){
    $add_where  .= " AND quick_req_memo LIKE '%{$sch_quick_req_memo}%'";
    $smarty->assign("sch_quick_req_memo", $sch_quick_req_memo);
}

if(!empty($sch_prd_sku)){
    $add_where  .= " AND 
    (
        lm_no IN(SELECT lp.lm_no FROM logistics_product lp WHERE lp.prd_type='1' AND lp.prd_no IN(SELECT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.option_no IN(SELECT pcm.prd_unit FROM product_cms_unit_management pcm WHERE pcm.sku LIKE '%{$sch_prd_sku}%') AND pcr.display))
        OR
        lm_no IN(SELECT lp.lm_no FROM logistics_product lp WHERE lp.prd_type='2' AND lp.prd_no IN(SELECT pcm.prd_unit FROM product_cms_unit_management pcm WHERE pcm.sku LIKE '%{$sch_prd_sku}%'))
    )    
    ";
    $smarty->assign("sch_prd_sku", $sch_prd_sku);
}

if(!empty($sch_is_in_out)){
    $add_where  .= " AND 
    (
        lm_no IN(SELECT lp.lm_no FROM logistics_product lp WHERE lp.prd_type='1' AND lp.prd_no IN(SELECT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.option_no IN(SELECT pcu.`no` FROM product_cms_unit pcu WHERE pcu.is_in_out='2') AND pcr.display))
        OR
        lm_no IN(SELECT lp.lm_no FROM logistics_product lp WHERE lp.prd_type='2' AND lp.prd_no IN(SELECT pcu.`no` FROM product_cms_unit pcu WHERE pcu.is_in_out='2'))
    )    
    ";
    $smarty->assign("sch_is_in_out", $sch_is_in_out);
}

$logistics_total_sql     = "SELECT count(lm_no) as cnt FROM logistics_management as lm WHERE {$add_where}";
$logistics_total_query   = mysqli_query($my_db, $logistics_total_sql);
$logistics_total_result  = mysqli_fetch_assoc($logistics_total_query);
$logistics_total         = isset($logistics_total_result['cnt']) ? $logistics_total_result['cnt'] : 0;

$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 10;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($logistics_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list  = pagelist($pages, "logistics_management.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $logistics_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);


# 물류관리시스템 리스트
$logistics_sql = "
    SELECT
        *,
        DATE_FORMAT(lm.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(lm.regdate, '%H:%i') as reg_time,
        (SELECT s.s_name FROM staff s WHERE s.s_no=lm.req_s_no) as req_s_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=lm.req_team) as req_t_name,
        (SELECT c.c_name FROM company c WHERE c.c_no=lm.run_c_no) as run_c_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no=lm.run_s_no) as run_s_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=lm.run_team) as run_t_name,
        (SELECT COUNT(pcsr.`no`) FROM product_cms_stock_report pcsr WHERE pcsr.log_doc_no=lm.doc_no AND pcsr.type='1') as in_stock_count,
        (SELECT COUNT(pcsr.`no`) FROM product_cms_stock_report pcsr WHERE pcsr.log_doc_no=lm.doc_no AND pcsr.type='2') as out_stock_count
    FROM logistics_management as lm
    WHERE {$add_where}
    ORDER BY lm_no DESC
    LIMIT {$offset}, {$num}
";
$logistics_query        = mysqli_query($my_db, $logistics_sql);
$logistics_list         = [];
$subject_option         = getSubjectOption();
$subject_name_option    = getSubjectNameOption();
$stock_option           = getStockTypeOption();
$delivery_option        = getDeliveryTypeOption();
$address_option         = getAddrCompanyOption();
while($logistics_result = mysqli_fetch_assoc($logistics_query))
{
    $logistics_result['subject_name']       = isset($subject_name_option[$logistics_result['subject']]) ? $subject_name_option[$logistics_result['subject']] : "";
    $logistics_result['stock_type_name']    = isset($stock_option[$logistics_result['stock_type']]) ? $stock_option[$logistics_result['stock_type']] : "";
    $logistics_result['delivery_type_name'] = isset($delivery_option[$logistics_result['delivery_type']]) ? $delivery_option[$logistics_result['delivery_type']] : (($logistics_result['delivery_type'] == 3) ? "직접전달" : "");

    $logistics_result['reg_s_date']         = date("Y-m-d", strtotime("{$logistics_result['regdate']} -2 weeks"));
    $logistics_result['reg_e_date']         = date("Y-m-d", strtotime("{$logistics_result['regdate']} +2 weeks"));
    $logistics_result['task_run_name']      = !empty($logistics_result['run_s_name']) ? $logistics_result['run_s_name']." ({$logistics_result['run_t_name']})" : "";

    $logistics_result['view_button'] = true;
    if($logistics_result['work_state'] == 7 || $logistics_result['work_state'] == 8 || $logistics_result['work_state'] == 9){
        $logistics_result['view_button'] = false;
    }

    $logistics_result['is_delete'] = false;
    if(empty($logistics_result['order_number']) && ($logistics_result['work_state'] == '1' || $logistics_result['work_state'] == '2' || $logistics_result['work_state'] == '3')){
        $logistics_result['is_delete'] = true;
    }

    $prd_type = $logistics_result['prd_type'];
    $row_idx  = 0;
    $prd_list = [];
    $prd_sql  = "";
    if($prd_type == "1"){
        $prd_sql   = "SELECT lp.log_c_no, lp.prd_no, (SELECT p.title FROM product_cms p WHERE p.prd_no=lp.prd_no) as prd_name, lp.quantity FROM logistics_product AS lp WHERE lm_no='{$logistics_result['lm_no']}' AND prd_type='{$prd_type}'";
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $unit_sql   = "SELECT pcum.prd_unit as prd_no, pcum.sku, pcr.quantity, pcum.warehouse, pcu.is_in_out, pcu.in_out_msg FROM product_cms_relation pcr LEFT JOIN product_cms_unit_management AS pcum ON pcum.prd_unit=pcr.option_no LEFT JOIN product_cms_unit AS pcu ON pcu.`no`=pcr.option_no WHERE pcr.prd_no='{$prd['prd_no']}' AND pcr.display='1' AND pcum.log_c_no='{$prd['log_c_no']}'";
            $unit_query = mysqli_query($my_db, $unit_sql);
            $unit_list  = [];
            while($unit = mysqli_fetch_assoc($unit_query))
            {
                $unit['qty']    = $prd['quantity']*$unit['quantity'];
                $unit_list[]    = $unit;
            }
            $prd['unit_list'] = $unit_list;
            $prd_list[] = $prd;
        }
    }elseif($prd_type == "2"){
        $prd_sql   = "SELECT lp.prd_no, pcum.sku as prd_name, lp.quantity, pcum.warehouse, pcu.is_in_out, pcu.in_out_msg FROM logistics_product as lp LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=lp.prd_no AND pcum.log_c_no=lp.log_c_no LEFT JOIN product_cms_unit AS pcu ON pcu.`no`=lp.prd_no WHERE lm_no='{$logistics_result['lm_no']}' AND prd_type='{$prd_type}'";
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $prd_list[] = $prd;
        }
    }
    $logistics_result['prd_list'] = $prd_list;

    # 주문 체크
    $order_number_list  = explode(",", $logistics_result['order_number']);
    $first_order_number = "";
    $order_cnt          = 0;
    $quick_ord_cnt      = 0;
    $is_del_ord         = false;

    # 입/출고요청 택배여부 확인
    if(!empty($logistics_result['order_number']))
    {
        $ord_chk_list   = [];
        foreach($order_number_list as $log_ord){
            $ord_chk_list[] = "'{$log_ord}'";
        }
        $ord_chk_where  = implode(",", $ord_chk_list);

        $first_order_number = $order_number_list[0];
        $order_cnt          = count($order_number_list)-1;

        if($logistics_result['delivery_type'] == '1')
        {
            $ord_del_chk_sql    = "SELECT COUNT(DISTINCT order_number) as ord_cnt FROM work_cms WHERE order_number IN({$ord_chk_where}) AND delivery_state IN(1,10)";
            $ord_del_chk_query  = mysqli_query($my_db, $ord_del_chk_sql);
            $ord_del_chk_result = mysqli_fetch_assoc($ord_del_chk_query);
            $ord_del_chk        = isset($ord_del_chk_result['ord_cnt']) ? $ord_del_chk_result['ord_cnt'] : 0;

            if($ord_del_chk == count($order_number_list)){
                $is_del_ord = true;
            }
        }
        elseif($logistics_result['delivery_type'] == '2')
        {
            $ord_chk_sql        = "SELECT COUNT(DISTINCT order_number) as ord_cnt FROM work_cms WHERE order_number IN ({$ord_chk_where})";
            $ord_chk_query      = mysqli_query($my_db, $ord_chk_sql);
            $ord_chk_result     = mysqli_fetch_assoc($ord_chk_query);
            $quick_ord_cnt      = isset($ord_chk_result['ord_cnt']) ? $ord_chk_result['ord_cnt'] : 0;

            $ord_del_chk_sql    = "SELECT COUNT(DISTINCT order_number) as ord_cnt FROM work_cms_quick WHERE order_number IN({$ord_chk_where}) AND quick_state IN(1)";
            $ord_del_chk_query  = mysqli_query($my_db, $ord_del_chk_sql);
            $ord_del_chk_result = mysqli_fetch_assoc($ord_del_chk_query);
            $ord_del_chk        = isset($ord_del_chk_result['ord_cnt']) ? $ord_del_chk_result['ord_cnt'] : 0;

            if($ord_del_chk == count($order_number_list)){
                $is_del_ord = true;
            }
        }
    }

    # 발주 체크
    $unit_sup_wait_list = [];
    $unit_sup_end_list  = [];
    if($logistics_result['delivery_type'] == '4')
    {
        if(!empty($logistics_result['order_number']))
        {
            $commerce_order_sql     = "SELECT `cos`.`no`, `cos`.sup_c_no, (SELECT c.c_name FROM company c WHERE c.c_no=`cos`.sup_c_no) AS sup_c_name FROM commerce_order_set `cos` WHERE `no` IN({$logistics_result['order_number']})";
            $commerce_order_query   = mysqli_query($my_db, $commerce_order_sql);
            while($commerce_order = mysqli_fetch_assoc($commerce_order_query))
            {
                $unit_sup_end_list[$commerce_order['sup_c_no']] = array("c_name" => $commerce_order['sup_c_name'], "set_no" => $commerce_order['no']);
            }
        }

        $unit_sup_sql   = "SELECT pcu.sup_c_no, (SELECT c.c_name FROM company c WHERE c.c_no=pcu.sup_c_no) AS sup_c_name, (SELECT c.license_type FROM company c WHERE c.c_no=pcu.sup_c_no) AS loc_type FROM product_cms_unit pcu WHERE pcu.`no` IN(SELECT lp.prd_no FROM logistics_product lp  WHERE lp.lm_no='{$logistics_result['lm_no']}' AND lp.prd_type='2') GROUP BY sup_c_no";
        $unit_sup_query = mysqli_query($my_db, $unit_sup_sql);
        while($unit_sup = mysqli_fetch_assoc($unit_sup_query))
        {
            if(isset($unit_sup_end_list[$unit_sup['sup_c_no']])){
                continue;
            }
            $unit_sup_wait_list[$unit_sup['sup_c_no']] = array("c_name" => $unit_sup['sup_c_name'], "loc_type" => ($unit_sup['loc_type'] == '4' ? 2 : 1));
        }
    }

    $logistics_result['first_order_number'] = $first_order_number;
    $logistics_result['order_cnt']          = $order_cnt;
    $logistics_result['quick_ord_cnt']      = $quick_ord_cnt;
    $logistics_result['is_del_ord']         = $is_del_ord;
    $logistics_result['unit_sup_wait_list'] = $unit_sup_wait_list;
    $logistics_result['unit_sup_end_list']  = $unit_sup_end_list;

    $logistics_result['group_state'] = 0;
    if($logistics_result['stock_type'] == '2' && ($logistics_result['sender_type'] != 99999 && $logistics_result['sender_type'] != 2809)){
        switch($logistics_result['subject']){
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '9':
            case '11':
            case '14':
                $logistics_result['group_state'] = 2;
                break;
            case '12':
                $logistics_result['group_state'] = 1;
                break;
            case '13':
                $logistics_result['group_state'] = 4;
                break;
        }
    }
    
    $logistics_list[] = $logistics_result;
}

$warehouse_option = $unit_model->getWarehouseOption();
$is_global = false;
if($session_s_no == "9" || $session_s_no == "237" || $session_s_no == "298"){
    $is_global = true;
}

# 입/출고 정지 구성품리스트
$logistics_stop_sql = "
    SELECT
        lm.work_state,
        COUNT(DISTINCT lm.lm_no) AS lm_cnt
    FROM logistics_product AS lp
    LEFT JOIN logistics_management AS lm ON lm.lm_no=lp.lm_no
    WHERE (
        (lp.prd_type='1' AND lp.prd_no IN(SELECT DISTINCT prd_no FROM product_cms_relation p WHERE p.option_no IN(SELECT pcu.`no` FROM product_cms_unit AS pcu WHERE pcu.is_in_out='2') AND p.display='1'))
        OR (lp.prd_type='2' AND lp.prd_no IN(SELECT pcu.`no` FROM product_cms_unit AS pcu WHERE pcu.is_in_out='2'))
    )               
    AND lm.work_state IN(3,4,5)
    GROUP BY lm.work_state
";
$logistics_stop_query   = mysqli_query($my_db, $logistics_stop_sql);
$logistics_stop_list    = array("3" => 0, "4" => 0, "5" => 0);
$is_stop_cnt            = false;
while($logistics_stop = mysqli_fetch_assoc($logistics_stop_query)){
    $is_stop_cnt = true;
    $logistics_stop_list[$logistics_stop['work_state']] = $logistics_stop['lm_cnt'];
    $logistics_stop_list['total'] += $logistics_stop['lm_cnt'];
}

$smarty->assign("is_global", $is_global);
$smarty->assign("page_type_option", getPageTypeOption('4'));
$smarty->assign("work_state_option", getWorkStateOption());
$smarty->assign("work_state_color_option", getWorkStateOptionColor());
$smarty->assign("addr_company_option", $address_option);
$smarty->assign("subject_option", $subject_option);
$smarty->assign("stock_type_option", $stock_option);
$smarty->assign("delivery_type_option", $delivery_option);
$smarty->assign("is_order_option", getIsOrderOption());
$smarty->assign("sch_warehouse_option", $warehouse_option);
$smarty->assign("sch_quick_option", getLogisticsQuickOption());
$smarty->assign("confirm_state_option", getConfirmStateOption());
$smarty->assign("stock_state_group_option", getStockStateGroupOption());
$smarty->assign("is_stop_cnt", $is_stop_cnt);
$smarty->assign("logistics_stop_list", $logistics_stop_list);
$smarty->assign("logistics_list", $logistics_list);

$smarty->display('logistics_management.html');
?>