<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/broadcast.php');
require('inc/model/BroadCast.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "17";
$nav_title   = "방송광고 캘린더";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# BroadCast Model
$broadcastModel = BroadCast::Factory();

# 기본 날짜 설정(오늘, 년도, 월, 일)
$sch_date   = isset($_GET['sch_date']) ? $_GET['sch_date'] : date('Y-m-d');
$day_token  = explode("-", $sch_date);
$base_year  = $day_token[0]; // 지정 년도
$base_month = $day_token[1]; // 지정된 월
$base_day   = $day_token[2]; // 지정된 일

$smarty->assign("sch_date", $sch_date);
$smarty->assign("base_year", $base_year);
$smarty->assign("base_month", $base_month);
$smarty->assign("base_day", $base_day);

# 첫째주, 첫째날, 마지막날, 그달 총 주 구하기
$day_length       = date("t",mktime(0,0,0,$base_month,$base_day,$base_year));
$first_week       = date("N",mktime(0,0,0,$base_month,1,$base_year));
$first_empty_week = $first_week%7;
$week_length      = ($day_length + $first_empty_week)/7;
$week_length      = ceil($week_length);

$smarty->assign("day_length", $day_length);
$smarty->assign("first_week", $first_week);
$smarty->assign("first_empty_week", $first_empty_week);
$smarty->assign("week_length", $week_length);

// 날짜와 요일 정보 담기
$calendar_date  = [];
$date_i         = 1;
for($i = 1 ; $i <= $first_empty_week ; $i++)
{
    $calendar_date[] = array(
        'day'   => '',
        'date'  => '',
        'week'  => ''
    );
}

$date_w_arr = array("일","월","화","수","목","금","토");
while($date_i <= $day_length) {
    if($date_i < 10)
        $cal_day = '0'.$date_i;
    else {
        $cal_day = $date_i;
    }

    $chk_date = $base_year.$base_month.$cal_day;
    $holiday_title = isset($holiday_list[$chk_date]) ? $holiday_list[$chk_date]['title']:'';

    $calendar_date[] = array(
        'day'            => $cal_day,
        'date'           => date('Y-m-d', strtotime($base_year.'-'.$base_month.'-'.$date_i)),
        'week'           => date('w', strtotime($base_year.'-'.$base_month.'-'.$date_i)),
        'week_name'      => $date_w_arr[date('w', strtotime($base_year.'-'.$base_month.'-'.$date_i))],
        'holiday'        => $holiday_title,
        'broadcast_list' => []
    );
    $date_i++;
}
$smarty->assign("calendar_date",$calendar_date);


$today 	    = date("Y-m-d"); // 오늘
$next_day 	= date("Y-m-d",mktime(0,0,0,$base_month,$base_day+1,$base_year)); // 다음날
$prev_dday 	= date("Y-m-d",mktime(0,0,0,$base_month,$base_day-1,$base_year)); // 이전날
$next_month = date("Y-m-d",mktime(0,0,0,$base_month+1,$base_day,$base_year)); // 다음달
$prev_month = date("Y-m-d",mktime(0,0,0,$base_month-1,$base_day,$base_year)); // 지난달
$next_year 	= date("Y-m-d",mktime(0,0,0,$base_month,$base_day,$base_year+1)); // 내년
$prev_year 	= date("Y-m-d",mktime(0,0,0,$base_month,$base_day,$base_year-1)); // 작년

$smarty->assign("today", $today);
$smarty->assign("next_day", $next_day);
$smarty->assign("prev_dday", $prev_dday);
$smarty->assign("next_month", $next_month);
$smarty->assign("prev_month", $prev_month);
$smarty->assign("next_year", $next_year);
$smarty->assign("prev_year", $prev_year);

/* 검색결과 */
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$sch_media      = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_advertiser = isset($_GET['sch_advertiser']) ? $_GET['sch_advertiser'] : "";
$sch_state      = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_title      = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_type       = isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_notice     = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$sch_detail_media   = isset($_GET['sch_detail_media']) ? $_GET['sch_detail_media'] : "";
$sch_subs_gubun     = isset($_GET['sch_subs_gubun']) ? $_GET['sch_subs_gubun'] : "";
$sch_delivery_gubun = isset($_GET['sch_delivery_gubun']) ? $_GET['sch_delivery_gubun'] : "";

$sch_s_date = "{$base_year}-{$base_month}-01";
$sch_e_date = "{$base_year}-{$base_month}-31";

$add_where  = "1=1 AND ((`ba`.sub_s_date is not null AND `ba`.sub_s_date != '0000-00-00') AND (`ba`.sub_e_date is not null AND `ba`.sub_e_date != '0000-00-00') AND (`ba`.s_time is not null AND `ba`.s_time != '00:00:00'))";
$add_where .= " AND ((sub_s_date BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' OR sub_e_date BETWEEN '{$sch_s_date}' AND '{$sch_e_date}') OR (sub_s_date <= '{$sch_s_date}' AND sub_e_date >= '{$sch_e_date}'))";

if(!empty($sch_media))
{
    $add_where .= " AND `ba`.media = '{$sch_media}'";
    $smarty->assign('sch_media', $sch_media);
}

if(!empty($sch_advertiser))
{
    $add_where .= " AND `ba`.advertiser like '%{$sch_advertiser}%'";
    $smarty->assign('sch_advertiser', $sch_advertiser);
}

if(!empty($sch_state))
{
    $add_where .= " AND `ba`.state = '{$sch_state}'";
    $smarty->assign('sch_state', $sch_state);
}

if(!empty($sch_title))
{
    $add_where .= " AND `ba`.program_title LIKE '%{$sch_title}%'";
    $smarty->assign('sch_title', $sch_title);
}

if(!empty($sch_type))
{
    $add_where .= " AND `ba`.`type` = '{$sch_type}'";
    $smarty->assign('sch_type', $sch_type);
}

if(!empty($sch_notice))
{
    $add_where .= " AND `ba`.notice LIKE '%{$sch_notice}%'";
    $smarty->assign('sch_notice', $sch_notice);
}

if(!empty($sch_detail_media))
{
    $add_where .= " AND `ba`.detail_media = '{$sch_detail_media}'";
    $smarty->assign('sch_detail_media', $sch_detail_media);
}

if(!empty($sch_subs_gubun))
{
    $add_where .= " AND `ba`.subs_gubun = '{$sch_subs_gubun}'";
    $smarty->assign('sch_subs_gubun', $sch_subs_gubun);
}

if(!empty($sch_delivery_gubun))
{
    $add_where .= " AND `ba`.delivery_gubun = '{$sch_delivery_gubun}'";
    $smarty->assign('sch_delivery_gubun', $sch_delivery_gubun);
}

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

# Model & Helper 처리
$ba_state_option = getBaStateOption();
$ba_media_option = getBaMediaOption();

$broadcastModel->setBaAllTypeOption();
$ba_type_option             = $broadcastModel->getBaTypeOption();
$ba_detail_media_option     = $broadcastModel->getBaDetailMediaOption();
$ba_subs_gubun_option       = $broadcastModel->getBaSubsOption();
$ba_delivery_gubun_option   = $broadcastModel->getBaDeliveryOption();

$broadcast_sql  = "
    SELECT *,
      IF(`ba`.s_time='00:00:00', '', DATE_FORMAT(`ba`.s_time, '%H:%i')) as s_time,
      IF(`ba`.e_time='00:00:00', '', DATE_FORMAT(`ba`.e_time, '%H:%i')) as e_time
    FROM broadcast_advertisement `ba`
    WHERE {$add_where}
    ORDER BY sub_s_date, s_time ASC
";
$broadcast_query    = mysqli_query($my_db, $broadcast_sql);
$broadcast_list     = [];
while($broadcast = mysqli_fetch_assoc($broadcast_query))
{
    $broadcast['media_name']  = !empty($broadcast['media']) ? $ba_media_list[$broadcast['media']] : "";
    $broadcast['state_name']  = !empty($broadcast['state']) ? $ba_state_list[$broadcast['state']] : "";
    $broadcast_title        = "[{$broadcast['s_time']}~{$broadcast['e_time']}] {$broadcast['program_title']}";
    $broadcast_full_title   = "[{$broadcast['s_time']}~{$broadcast['e_time']}][{$broadcast['media_name']}][{$broadcast['advertiser']}] {$broadcast['program_title']}";

    if(!empty($broadcast['second_cnt'])){
        $broadcast_full_title .= " [{$broadcast['second_cnt']}초]";
    }

    if(!empty($broadcast['hourly_wage'])){
        $broadcast_full_title .= " [{$broadcast['hourly_wage']}]";
    }

    if(!empty($broadcast['type'])){
        $broadcast_full_title .= " [{$broadcast['type']}]";
    }

    $delivery_gubun_val = 0;
    if(!empty($broadcast['delivery_gubun']))
    {
        $broadcast_gubun = $broadcast['delivery_gubun'];

        if(strpos($broadcast_gubun, "방송") !== false){
            $delivery_gubun_val = "1";
        }elseif(strpos($broadcast_gubun, "불방") !== false){
            $delivery_gubun_val = "2";
        }elseif(strpos($broadcast_gubun, "중지") !== false){
            $delivery_gubun_val = "3";
        }
    }

    $broadcast_list[] = array(
        'title'      => $broadcast_title,
        'full_title' => $broadcast_full_title,
        'sub_s_date' => $broadcast['sub_s_date'],
        'sub_e_date' => $broadcast['sub_e_date'],
        's_time'     => $broadcast['s_time'],
        'date_w'     => $broadcast['date_w'],
        'gubun'      => $delivery_gubun_val
    );
}

$calendar_date_list = [];
foreach($calendar_date as $cal_data)
{
    if($broadcast_list)
    {
        foreach($broadcast_list as $bc_data)
        {
            $sub_s_date = $bc_data['sub_s_date'];
            $sub_e_date = $bc_data['sub_e_date'];
            $bc_date_w  = $bc_data['date_w'];

            if(($sub_s_date <= $cal_data['date'] && $cal_data['date'] <= $sub_e_date) && (strpos($bc_date_w, $cal_data['week_name']) !== false))
            {
                $cal_data['broadcast_list'][$bc_data['s_time']][] = $bc_data;
            }
        }

        if(isset($cal_data['broadcast_list']) && !empty($cal_data['broadcast_list'])){
            $broadcast_sort = $cal_data['broadcast_list'];
            ksort($broadcast_sort);
            $cal_data['broadcast_list'] = $broadcast_sort;
        }
    }

    $calendar_date_list[] = $cal_data;
}

$smarty->assign('ba_media_option', $ba_media_option);
$smarty->assign('ba_state_option', $ba_state_option);
$smarty->assign('ba_type_option', $ba_type_option);
$smarty->assign('ba_detail_media_option', $ba_detail_media_option);
$smarty->assign('ba_subs_gubun_option', $ba_subs_gubun_option);
$smarty->assign('ba_delivery_gubun_option', $ba_delivery_gubun_option);
$smarty->assign('calendar_date_list', $calendar_date_list);

$smarty->display('broadcast_ad_calendar.html');
?>
