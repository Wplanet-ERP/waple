<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');

$w_no    = isset($_GET['w_no']) ? $_GET['w_no'] : "";
$w_type  = isset($_GET['w_type']) ? $_GET['w_type'] : "";
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'add_file') # 생성
{
    $w_no_val   = isset($_POST['w_no']) ? $_POST['w_no'] : "";
    $w_type_val = isset($_POST['w_type']) ? ($_POST['w_type'] == "new" ? "task_req" : $_POST['w_type']) : "";
    $new_upload = ($_POST['w_type'] == 'new') ? true : false;

    # 업무번호 및 타입 확인
    if(empty($w_no_val) || empty($w_type_val)){
        $smarty->display('access_company_error.html');
        exit;
    }

    # Dropbox 이미지 확인 및 저장
    $file_origin_read = isset($_POST['file_origin_read']) ? $_POST['file_origin_read'] : "";
    $file_origin_name = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_path        = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_name        = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk         = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    $file_origin = "";
    $file_read   = "";
    if($file_chk)
    {
        if(!empty($file_path) && !empty($file_name))
        {
            $file_reads  = move_store_files($file_path, "dropzone_tmp", "out_".$w_type_val);
            $file_origin = implode(',', $file_name);
            $file_read   = implode(',', $file_reads);

            if(count($file_reads) != count($file_name))
            {
                exit("<script>alert('파일 업로드에 실패했습니다');location.href='work_list.file_upload.php?w_no={$w_no_val}&w_type={$w_type_val}';</script>");
            }

            if(!empty($file_origin_read) && !empty($file_origin_name))
            {
                if(!empty($file_path) && !empty($file_name))
                {
                    $del_images = array_diff(explode(',', $file_origin_read), $file_path);
                }else{
                    $del_images = explode(',', $file_origin_read);
                }

                del_files($del_images);
            }
        }
    }

    if($new_upload)
    {
        exit("<script>alert('파일 업로드에 성공했습니다');opener.document.getElementById('f_task_req_file_origin_new').value='{$file_origin}';opener.document.getElementById('f_task_req_file_read_new').value='{$file_read}';self.close();</script>");
    }
    else
    {
        # 파일 Path 및 이름 저장
        $name_column = $w_type_val."_file_origin";
        $path_column = $w_type_val."_file_read";

        $upd_sql = "UPDATE `work` SET {$name_column}='{$file_origin}', {$path_column}='{$file_read}' WHERE w_no = '{$w_no_val}'";

        if(mysqli_query($my_db, $upd_sql))
        {
            exit("<script>alert('파일 업로드에 성공했습니다');opener.document.location.reload();self.close();</script>");
        }
    }

    exit("<script>alert('파일 업로드에 실패했습니다');location.href='work_list.file_upload.php?w_no={$w_no_val}&w_type={$w_type_val}';</script>");
}

// 업무번호 및 타입 확인
if(empty($w_no) || empty($w_type)){
    $smarty->display('access_company_error.html');
    exit;
}

if($w_type == 'new')
{
    $file_names = [];
    $file_paths = [];
}else{
    $f_name_column = $w_type."_file_origin";
    $f_path_column = $w_type."_file_read";

    $work_sql 	 = "SELECT {$f_name_column} as file_name, {$f_path_column} as file_path FROM `work` WHERE w_no = {$w_no} LIMIT 1";
    $work_query  = mysqli_query($my_db, $work_sql);
    $work 		 = mysqli_fetch_assoc($work_query);

    $file_name = $work['file_name'];
    $file_path = $work['file_path'];

    if(!empty($file_name) && !empty($file_path))
    {
        $file_names = explode(',', $file_name);
        $file_paths = explode(',', $file_path);
    }else{
        $file_names = [];
        $file_paths = [];
    }
}

$total_count = empty($file_paths) ? 0 : count($file_paths);

$smarty->assign("file_count", $total_count);
$smarty->assign("w_no", $w_no);
$smarty->assign("w_type", $w_type);
$smarty->assign("file_names", $file_names);
$smarty->assign("file_paths", $file_paths);

$smarty->display('work_list.file_upload.html');

?>
