<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');

if(!(permissionNameCheck($session_permission, "마스터관리자") || permissionNameCheck($session_permission, "서비스운영"))){
	$smarty->display('access_error.html');
	exit;
}

# Process 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";
if($process == "reload")
{
	$b_no		= (isset($_POST['save_no'])) ? $_POST['save_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$blog_sql	= "UPDATE blog SET reload = NOW() where b_no='{$b_no}'";

	if(!mysqli_query($my_db, $blog_sql)){
		echo ("<script>alert('재평가완료에 실패하였습니다');</script>");
	}else{
		echo ("<script>alert('재평가를 완료하였습니다다');</script>");
	}
	exit ("<script>location.href='blogger_list.php?{$search_url}';</script>");
}
elseif($process == "modify")
{
	$b_no		= (isset($_POST['b_no'])) ? $_POST['b_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$blog_url	= (isset($_POST['blog_url'])) ? addslashes(trim($_POST['blog_url'])) : "";
	$memo		= (isset($_POST['memo'])) ? addslashes(trim($_POST['memo'])) : "";
	$hps[]		= (isset($_POST['hp_0'])) ? trim($_POST['hp_0']) : "";
	$hps[]		= (isset($_POST['hp_1'])) ? trim($_POST['hp_1']) : "";
	$hps[]		= (isset($_POST['hp_2'])) ? trim($_POST['hp_2']) : "";
	$hp			= implode("-", $hps);

	$emails[]	= (isset($_POST['email_0'])) ? trim($_POST['email_0']) : "";
	$emails[]	= (isset($_POST['email_1'])) ? trim($_POST['email_1']) : "";
	$email		= implode("@", $emails);

	$upd_sql	= "
		UPDATE blog SET
			cafe_id  = '{$_POST['cafe_id']}',
			nick 	 = '{$_POST['nick']}',
			username = '{$_POST['username']}',
			blog_url = '{$blog_url}',
			hp 		 = '{$hp}',
			email 	 = '{$email}',
			reload 	 = NOW(),
			b_memo	 = '{$memo}'
		WHERE
			b_no='{$b_no}'
	";

	if(!mysqli_query($my_db, $upd_sql)){
		echo ("<script>alert('수정에 실패하였습니다');</script>");
	}else{
		echo ("<script>alert('수정하였습니다');</script>");
	}
	exit ("<script>location.href='blogger_list.php?{$search_url}';</script>");
}


# Navigation & My Quick
$nav_prd_no  = "112";
$nav_title   = "블로거 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색쿼리
$add_where		= "1=1";
$opt_search		= (isset($_GET['opt_search'])) ? $_GET['opt_search']:"";
$search_text 	= (isset($_GET['search_text'])) ? $_GET['search_text']:"";

if(!empty($opt_search) && !empty($search_text))
{
	switch($opt_search)
	{
		case "1":
			$add_where .= " AND nick LIKE '%{$search_text}'";
			break;
		case "2":
			$add_where .= " AND username LIKE '%{$search_text}'";
			break;
		case "3":
			$add_where .= " AND cafe_id LIKE '%{$search_text}'";
			break;
		case "4":
			$add_where .= " AND hp LIKE '%{$search_text}'";
			break;
		case "5":
			$add_where .= " AND blog_url LIKE '%{$search_text}'";
			break;
	}

	$smarty->assign("opt_search", $opt_search);
	$smarty->assign("search_text", $search_text);
}

# 페이징
$blog_cnt_sql 	= "select count(*) as cnt FROM blog WHERE {$add_where}";
$blog_cnt_query	= mysqli_query($my_db, $blog_cnt_sql);
$blog_cnt_data	= mysqli_fetch_array($blog_cnt_query);
$total_num  	= $blog_cnt_data['cnt'];

$pages 		= isset($_GET['page']) ? intval($_GET['page']) : 1;
$num 		= 10;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($total_num/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url 	= getenv("QUERY_STRING");
$page_list		= pagelist($pages, "blogger_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $total_num);
$smarty->assign("page_list", $page_list);

# 리스트 쿼리
$blog_sql 		= "SELECT * FROM blog b WHERE {$add_where} ORDER BY regdate DESC LIMIT {$offset}, {$num}";
$blog_query		= mysqli_query($my_db, $blog_sql);
$blog_idx   	= 1;
$blogger_list	= [];
while($blog_data = mysqli_fetch_array($blog_query))
{
	$hp 		= explode("-", $blog_data['hp']);
	$email 		= explode("@", $blog_data['email']);
	$blog_url	= $blog_data['blog_url'];

	$select_sql = "
			SELECT count(*) as apply_count,
			(select count(*) from application a where a.blog_url='{$blog_url}' and (a.a_state='1' OR a.a_state='3')) as select_count,
			(select count(*) from application a where a.blog_url='{$blog_url}' and a.a_state='3') as cancel_count
			FROM application
			where blog_url = '{$blog_data['blog_url']}'
	";

	$select_query	= mysqli_query($my_db, $select_sql);
	$select_data	= mysqli_fetch_array($select_query);

	if ($select_data['apply_count'] > 0)
		$percent = ($select_data['select_count']/$select_data['apply_count'])*100;
	else
		$percent = '0';

	$report_late_sql 	= "SELECT COUNT(*) FROM application WHERE limit_yn = '2' AND blog_url='{$blog_url}'";
	$report_late_query	= mysqli_query($my_db, $report_late_sql);
	$report_late		= mysqli_fetch_array($report_late_query);

	if ($report_late[0] > 0 && $select_data['select_count'] > 0)
		$report_late_percent=($report_late[0]/$select_data['select_count'])*100;
	else
		$report_late_percent = '0';

	$blog_data['select_count'] 			= $select_data['select_count'];
	$blog_data['apply_count'] 			= $select_data['apply_count'];
	$blog_data['percent'] 				= round($percent);
	$blog_data['cancel_count'] 			= $select_data['cancel_count'];
	$blog_data['report_late_count'] 	= $report_late[0];
	$blog_data['report_late_percent'] 	= round($report_late_percent);
	$blog_data['hp_0'] 					= $hp['0'];
	$blog_data['hp_1'] 					= $hp['1'];
	$blog_data['hp_2'] 					= $hp['2'];
	$blog_data['email_0'] 				= $email['0'];
	$blog_data['email_1'] 				= $email['1'];

	$blogger_list[]	= $blog_data;
}

$smarty->assign("blogger_list", $blogger_list);

$smarty->display('blogger_list.html');

?>
