<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/evaluation.php');
require('inc/model/Staff.php');
require('inc/model/Team.php');
require('inc/model/Evaluation.php');

$ev_model           = Evaluation::Factory();
$staff_model        = Staff::Factory();
$team_model         = Team::Factory();

$add_where          = "1=1";
$add_result_where   = "1=1";
$ev_no              = isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_receiver_team  = isset($_GET['sch_receiver_team']) ? $_GET['sch_receiver_team'] : "";
$sch_receiver_staff = isset($_GET['sch_receiver_staff']) ? $_GET['sch_receiver_staff'] : "";
$sch_evaluator_cnt  = isset($_GET['sch_evaluator_cnt']) ? $_GET['sch_evaluator_cnt'] : "";
$sch_is_review      = isset($_GET['sch_is_review']) ? $_GET['sch_is_review'] : "";
$sch_is_match       = isset($_GET['sch_is_match']) ? $_GET['sch_is_match'] : "";
$sch_is_self        = isset($_GET['sch_is_self']) ? $_GET['sch_is_self'] : "";
$sch_is_complete    = isset($_GET['sch_is_complete']) ? $_GET['sch_is_complete'] : "";

if(empty($ev_no)){
    echo "평가지 번호가 없습니다.";
    exit;
}

$team_all_list          = $team_model->getTeamAllList();
$team_all_staff_list    = $team_all_list['staff_list'];
$team_child_list        = $team_model->getChildTeamList();
$staff_manager_list     = [];
$sch_staff_list         = [];
$is_super_admin         = false;
$evaluation_system      = $ev_model->getItem($ev_no);

if($session_s_no == '1' || $session_s_no == '28' || $evaluation_system['admin_s_no'] == $session_s_no){
    $is_super_admin = true;
}
$smarty->assign("is_super_admin", $is_super_admin);

if(!empty($ev_no)) {
    $add_where .= " AND er.ev_no='{$ev_no}'";
    $smarty->assign("ev_no", $ev_no);
}

if(!empty($sch_manager)) {
    $add_where .= " AND er.manager='{$sch_manager}'";
    $smarty->assign("sch_manager", $sch_manager);
}

if(!empty($sch_receiver_team))
{
    $sch_staff_list = $team_all_staff_list[$sch_receiver_team];

    $add_where .= " AND er.receiver_team='{$sch_receiver_team}'";
    $smarty->assign("sch_receiver_team", $sch_receiver_team);
}

if(!empty($sch_receiver_staff)) {
    $add_where .= " AND er.receiver_s_no='{$sch_receiver_staff}'";
    $smarty->assign("sch_receiver_staff", $sch_receiver_staff);
}

if(!empty($sch_evaluator_cnt)) {
    $add_result_where .= " AND er.total_cnt <= '{$sch_evaluator_cnt}'";
    $smarty->assign("sch_evaluator_cnt", $sch_evaluator_cnt);
}

if(!empty($sch_is_review)) {
    $add_result_where .= " AND er.not_cnt > 0";
    $smarty->assign("sch_is_review", $sch_is_review);
}

if(!empty($sch_is_match)) {
    $add_result_where .= " AND er.not_match_cnt > 0";
    $smarty->assign("sch_is_match", $sch_is_match);
}

if(!empty($sch_is_self)) {
    $add_result_where .= " AND er.self_cnt > 0";
    $smarty->assign("sch_is_self", $sch_is_self);
}

if(!empty($sch_is_complete)) {
    $add_result_where .= " AND (er.complete_cnt = 0 OR er.complete_cnt IS NULL)";
    $smarty->assign("sch_is_complete", $sch_is_complete);
}

# 자기기술 평가 체크
$ev_self_cnt_sql    = "SELECT COUNT(*) as self_cnt FROM evaluation_unit WHERE ev_u_set_no='{$evaluation_system['ev_u_set_no']}' AND evaluation_state='100'";
$ev_self_cnt_query  = mysqli_query($my_db, $ev_self_cnt_sql);
$ev_self_cnt_result = mysqli_fetch_assoc($ev_self_cnt_query);
$ev_self_cnt        = isset($ev_self_cnt_result['self_cnt']) ? $ev_self_cnt_result['self_cnt'] : "";

$ev_self_chk_sql = "
    SELECT
        rec_s_no,
        COUNT(esr_no) AS rec_self_cnt
    FROM evaluation_receiver_self
    WHERE ev_no='{$ev_no}' AND (answer IS NOT NULL AND answer != '')
    GROUP BY rec_s_no
";
$ev_self_chk_query = mysqli_query($my_db, $ev_self_chk_sql);
$ev_self_chk_list  = [];
while($ev_self_chk_result  = mysqli_fetch_assoc($ev_self_chk_query))
{
    if($ev_self_cnt > 0 && $ev_self_chk_result['rec_self_cnt'] == $ev_self_cnt) {
        $ev_self_chk_list[$ev_self_chk_result['rec_s_no']] = 1;
    }
}

# 평가 관계 리스트
$ev_relation_sql    = "
    SELECT 
        *
    FROM (
        SELECT 
            *, 
            (SELECT s.s_name FROM staff s WHERE s.s_no=er.manager) as manager_name, 
            (SELECT t.team_name FROM team t WHERE t.team_code=er.receiver_team) as receiver_team_name,
            (SELECT t.priority FROM team t WHERE t.team_code=er.receiver_team) as t_priority,
            (SELECT s.s_name FROM staff s WHERE s.s_no=er.receiver_s_no) as receiver_name,
            COUNT(ev_r_no) AS total_cnt,
            SUM(IF(er.rec_is_review=0, 1, 0)) AS not_cnt,
            SUM(IF(er.rec_is_review!=er.eval_is_review, 1, 0)) AS not_match_cnt,
            SUM(er.is_complete) AS complete_cnt,
            SUM(IF(er.rec_is_review=1 AND er.eval_is_review=1, 1, 0)) AS select_cnt,
            (SELECT COUNT(ers.esr_no) FROM evaluation_receiver_self ers WHERE ers.ev_no=er.ev_no AND ers.rec_s_no=er.receiver_s_no) as self_cnt 
        FROM evaluation_relation as er 
        WHERE {$add_where}
        GROUP BY receiver_s_no
    ) AS er
    WHERE {$add_result_where}
    ORDER BY t_priority, receiver_team, receiver_s_no
";
$ev_relation_query          = mysqli_query($my_db, $ev_relation_sql);
$ev_relation_manager_list   = [];
$ev_relation_receiver_list  = [];

while($ev_relation = mysqli_fetch_assoc($ev_relation_query))
{
    $staff_manager_list[$ev_relation['manager']]        = $ev_relation['manager_name'];
    $ev_relation_manager_list[$ev_relation['manager']]  = $ev_relation['manager_name'];

    $ev_relation_receiver_list[$ev_relation['manager']][$ev_relation['receiver_s_no']] = array(
        'name'          => $ev_relation['receiver_name'],
        'rec_team'      => $ev_relation['receiver_team'],
        "t_name"        => $ev_relation['receiver_team_name'],
        'total_cnt'     => $ev_relation['total_cnt'],
        'review_cnt'    => $ev_relation['not_cnt'],
        'match_cnt'     => $ev_relation['not_match_cnt'],
        'complete_cnt'  => $ev_relation['complete_cnt'],
        'self_chk'      => isset($ev_self_chk_list[$ev_relation['receiver_s_no']]) ? $ev_self_chk_list[$ev_relation['receiver_s_no']] : 0,
        'select_cnt'    => $ev_relation['select_cnt'],
    );
}

$smarty->assign("ev_relation_manager_list", $ev_relation_manager_list);
$smarty->assign("ev_relation_receiver_list", $ev_relation_receiver_list);
$smarty->assign("sch_team_list", $team_child_list);
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("staff_manager_list", $staff_manager_list);
$smarty->assign("evaluation_system", $evaluation_system);

$smarty->display('evaluation_regist_iframe.html');

?>
