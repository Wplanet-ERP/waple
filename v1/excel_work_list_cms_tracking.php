<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$fontbold = array(
    'font' => array(
        'bold' => true,
    ),
);

# 검색쿼리 & 검색 변수 초기화
$type           = isset($_GET['type']) ? $_GET['type'] : "1";
$stock_date     = isset($_GET['stock_date']) ? $_GET['stock_date'] : "";
$delivery_type  = isset($_GET['delivery_type']) ? $_GET['delivery_type'] : "";
$dp_company     = isset($_GET['dp_company']) ? $_GET['dp_company'] : "";
$imweb_type     = isset($_GET['imweb_type']) ? $_GET['imweb_type'] : "1372";

$add_where      = "w.stock_date = '{$stock_date}' AND w.delivery_state = '8'";
$add_order_by   = "w.w_no DESC";
$title          = "아임웹 운송장";
$idx            = 3;

# 운송장 다운로드(Type 1: 아임웹, 2: 스토어팜, 3: 사방넷, 8: 아이레놀(스토어팜), 10: 베라베프(스토어팜), 12: 누잠(스토어팜) , 13: CS/교환, 15: 올리브영
if($type == 2 || $type == 8 || $type == 10 || $type == 12 || $type == 26)
{
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "상품주문번호")
        ->setCellValue('B1', "배송방법")
        ->setCellValue('C1', "택배사")
        ->setCellValue('D1', "송장번호")
    ;

    if($type == 10){
        $title      = "베라베프(스토어팜) 운송장";
        $add_where .= " AND w.dp_c_no = '3295'";
    }elseif($type == 12){
        $title      = "누잠(스토어팜) 운송장";
        $add_where .= " AND w.dp_c_no = '4629'";
    }elseif($type == 8){
        $title      = "아이레놀(스토어팜) 운송장";
        $add_where .= " AND w.dp_c_no = '5588'";
    }elseif($type == 26){
        $title      = "채식주의(스토어팜) 운송장";
        $add_where .= " AND w.dp_c_no = '5770'";
    }else{
        $title      = "닥터피엘(스토어팜) 운송장";
        $add_where .= " AND w.dp_c_no = '5427'";
    }

    $idx        = 2;
}
elseif($type == 3)
{
    $title      = "사방넷 운송장";
    $add_where .= " AND w.dp_c_no NOT IN('1372','5800','5958','5965','6012','5261','1816','3294','3295','4629','5427','5588','5770','5615','5762','5712','5720','5762','5792','5847','5866','2005','2007','2008','5127','5128','5134','5135','5899','5900','5901','5662','5908','6031','5859') AND (w.shop_ord_no is not null AND w.shop_ord_no != '')";
    $idx        = 1;
}
elseif($type == 13)
{
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "이전구매처")
        ->setCellValue('B1', "이전주문번호")
        ->setCellValue('C1', "주문번호")
        ->setCellValue('D1', "택배사")
        ->setCellValue('E1', "운송자번호")
        ->setCellValue('F1', "구매처")
    ;

    $title      = "CS교환 운송장";
    $add_where  = "w.stock_date = '{$stock_date}' AND w.delivery_state IN('4','8')";
    $add_where .= " AND w.dp_c_no IN('2007', '2008', '5134', '5135')";
    $add_where .= " AND w.parent_order_number IS NOT NULL AND w.parent_order_number !=''";
    $idx        = 2;
}
elseif($type == 15)
{
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "주문번호")
        ->setCellValue('B1', "택배사")
        ->setCellValue('C1', "운송자번호")
        ->setCellValue('D1', "배송번호")
    ;

    $title      = "올리브영(누잠_에이원비앤에이치) 운송장";
    $add_where  = "w.stock_date = '{$stock_date}' AND w.delivery_state IN('4','8')";
    $add_where .= " AND w.dp_c_no IN('5615')";
    $idx        = 2;
}
elseif($type == 16)
{
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "발주일자")
        ->setCellValue('B1', "순번")
        ->setCellValue('C1', "주문번호")
        ->setCellValue('D1', "상품명")
        ->setCellValue('E1', "옵션명")
        ->setCellValue('F1', "수량")
        ->setCellValue('G1', "수취인명")
        ->setCellValue('H1', "수취인 휴대폰")
        ->setCellValue('I1', "수취인 전화번호")
        ->setCellValue('J1', "우편번호")
        ->setCellValue('K1', "배송지 주소")
        ->setCellValue('L1', "배송 요청사항")
        ->setCellValue('M1', "택배 출고일")
        ->setCellValue('N1', "택배사")
        ->setCellValue('O1', "출고송장")
    ;

    $title      = "블루베리몰 운송장";
    $add_where  = "w.stock_date = '{$stock_date}' AND w.delivery_state IN('4','8')";
    $add_where .= " AND w.dp_c_no IN('5712')";
    $idx        = 2;
}
elseif($type == 17)
{
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "orderId")
        ->setCellValue('B1', "serviceName")
        ->setCellValue('C1', "serviceCode")
        ->setCellValue('D1', "trackingNumber")
    ;

    $title      = "알리익스프레스 운송장";
    $add_where  = "w.stock_date = '{$stock_date}'";
    $add_where .= " AND w.dp_c_no IN('5720')";
    $idx        = 2;
}
elseif($type == 18)
{
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "주문번호")
        ->setCellValue('B1', "주문일")
        ->setCellValue('C1', "입금입")
        ->setCellValue('D1', "주문자명")
        ->setCellValue('E1', "주문자휴대폰")
        ->setCellValue('F1', "이메일")
        ->setCellValue('G1', "상품명")
        ->setCellValue('H1', "사은품")
        ->setCellValue('I1', "이벤트 당첨")
        ->setCellValue('J1', "옵션")
        ->setCellValue('K1', "주문수량")
        ->setCellValue('L1', "단가")
        ->setCellValue('M1', "결제금액")
        ->setCellValue('N1', "배송비")
        ->setCellValue('O1', "합계")
        ->setCellValue('P1', "택배사")
        ->setCellValue('Q1', "송장번호")
        ->setCellValue('R1', "수령인명")
        ->setCellValue('S1', "수령인연락처")
        ->setCellValue('T1', "수령인휴대폰")
        ->setCellValue('U1', "우편번호")
        ->setCellValue('V1', "주소")
        ->setCellValue('W1', "주문상태")
        ->setCellValue('X1', "배송시요청사항")
    ;

    $title      = "아이레놀(공동구매) 운송장";
    $add_where  = "w.stock_date = '{$stock_date}' AND w.delivery_state IN(4,8) AND w.dp_c_no ='5847'";
    $idx        = 2;
}
elseif($type == 20)
{
    $idx   = 2;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "셀렉샵")
        ->setCellValue('B1', "주문번호")
        ->setCellValue('C1', "주문품목번호")
        ->setCellValue('D1', "주문상품명")
        ->setCellValue('E1', "주문상품명(옵션포함)")
        ->setCellValue('F1', "자체상품코드")
        ->setCellValue('G1', "자체품목코드")
        ->setCellValue('H1', "상품가격")
        ->setCellValue('I1', "옵션가격")
        ->setCellValue('J1', "수량")
        ->setCellValue('K1', "총 주문금액")
        ->setCellValue('L1', "수령인")
        ->setCellValue('M1', "수령인 휴대전화")
        ->setCellValue('N1', "수령인 우편번호")
        ->setCellValue('O1', "수령인 상세 주소")
        ->setCellValue('P1', "배송 요청사항")
        ->setCellValue('Q1', "발주일")
        ->setCellValue('R1', "택배사")
        ->setCellValue('S1', "운송장번호")
    ;

    $title      = "온더룩 운송장";
    $add_where  = "w.stock_date = '{$stock_date}' AND w.dp_c_no = '5792'";
}
elseif($type == 22)
{
    $idx   = 2;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "주문일시")
        ->setCellValue('B1', "주문번호")
        ->setCellValue('C1', "결제일시")
        ->setCellValue('D1', "상품명")
        ->setCellValue('E1', "상품코드")
        ->setCellValue('F1', "외주상품코드")
        ->setCellValue('G1', "수량")
        ->setCellValue('H1', "결제금액")
        ->setCellValue('I1', "배송비")
        ->setCellValue('J1', "수령자명")
        ->setCellValue('K1', "수령자전화")
        ->setCellValue('L1', "수령자핸드폰")
        ->setCellValue('M1', "우편번호")
        ->setCellValue('N1', "수령지")
        ->setCellValue('O1', "배송메모")
        ->setCellValue('P1', "특이사항")
        ->setCellValue('Q1', "구매처")
        ->setCellValue('R1', "택배사")
        ->setCellValue('S1', "운송장번호")
    ;

    $title          = "기본양식 운송장";
    $add_where      = "w.stock_date = '{$stock_date}' AND delivery_state IN(4,8) AND w.dp_c_no = '{$dp_company}' AND w.unit_price > 0";
    $add_order_by   = "w.w_no ASC";
}
elseif($type == 21)
{
    $idx   = 3;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "상품주문번호")
        ->setCellValue('B1', "택배사")
        ->setCellValue('C1', "송장번호")
        ->setCellValue('D1', "주문 상태 변경")
        ->setCellValue('A2', "발주처리를 하고자 하는 해당 상품의 주문번호 또는 품목주문번호를 입력해 주세요. *동일한 주문번호와 품목주문번호가 존재할 경우, 해당 건은 실패처리 되며 개별 송장등록이 필요합니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('B2', "발송처리 시 사용하실 택배사를 정확히 입력해 주세요. (예: CJ대한통운, 우체국택배, 한진택배 등) 송장번호가 아직 발행되지 않았거나 배송조회가 불가능한 택배인 경우 기타택배 를 입력해 주세요. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('C2', "발송처리 시 사용된 송장번호를 입력해 주세요. 송장번호가 없는 '기타택배'를 사용하신 경우에는 임의의 숫자(ex: 1111)를 입력해 주시기 바랍니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('D2', "원하시는 주문 상태를 입력해주세요.(배송 준비중, 배송중) *입력하지 않으면 상태 변경이 되지 않습니다. 배송준비중으로 상태 변경시 택배사와 송장번호 입력을 생략할 수 있으며, 생략시 택배사, 송장번호가 변경되지 않습니다.")
    ;

    $title      = "아임웹(닥터피엘) 운송장";
    $add_where .= " AND w.dp_c_no = '5800' AND (w.section_ord_no IS NULL OR w.section_ord_no = '')";
}
elseif($type == 23)
{
    $idx   = 3;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "상품주문번호")
        ->setCellValue('B1', "택배사")
        ->setCellValue('C1', "송장번호")
        ->setCellValue('D1', "주문 상태 변경")
        ->setCellValue('A2', "발주처리를 하고자 하는 해당 상품의 주문번호 또는 품목주문번호를 입력해 주세요. *동일한 주문번호와 품목주문번호가 존재할 경우, 해당 건은 실패처리 되며 개별 송장등록이 필요합니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('B2', "발송처리 시 사용하실 택배사를 정확히 입력해 주세요. (예: CJ대한통운, 우체국택배, 한진택배 등) 송장번호가 아직 발행되지 않았거나 배송조회가 불가능한 택배인 경우 기타택배 를 입력해 주세요. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('C2', "발송처리 시 사용된 송장번호를 입력해 주세요. 송장번호가 없는 '기타택배'를 사용하신 경우에는 임의의 숫자(ex: 1111)를 입력해 주시기 바랍니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('D2', "원하시는 주문 상태를 입력해주세요.(배송 준비중, 배송중) *입력하지 않으면 상태 변경이 되지 않습니다. 배송준비중으로 상태 변경시 택배사와 송장번호 입력을 생략할 수 있으며, 생략시 택배사, 송장번호가 변경되지 않습니다.")
    ;

    $title      = "아임웹(아이레놀) 운송장";
    $add_where .= " AND w.dp_c_no = '5958' AND (w.section_ord_no IS NULL OR w.section_ord_no = '')";
}
elseif($type == 24)
{
    $idx   = 3;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "상품주문번호")
        ->setCellValue('B1', "택배사")
        ->setCellValue('C1', "송장번호")
        ->setCellValue('D1', "주문 상태 변경")
        ->setCellValue('A2', "발주처리를 하고자 하는 해당 상품의 주문번호 또는 품목주문번호를 입력해 주세요. *동일한 주문번호와 품목주문번호가 존재할 경우, 해당 건은 실패처리 되며 개별 송장등록이 필요합니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('B2', "발송처리 시 사용하실 택배사를 정확히 입력해 주세요. (예: CJ대한통운, 우체국택배, 한진택배 등) 송장번호가 아직 발행되지 않았거나 배송조회가 불가능한 택배인 경우 기타택배 를 입력해 주세요. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('C2', "발송처리 시 사용된 송장번호를 입력해 주세요. 송장번호가 없는 '기타택배'를 사용하신 경우에는 임의의 숫자(ex: 1111)를 입력해 주시기 바랍니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('D2', "원하시는 주문 상태를 입력해주세요.(배송 준비중, 배송중) *입력하지 않으면 상태 변경이 되지 않습니다. 배송준비중으로 상태 변경시 택배사와 송장번호 입력을 생략할 수 있으며, 생략시 택배사, 송장번호가 변경되지 않습니다.")
    ;

    $title      = "아임웹(채식주의) 운송장";
    $add_where .= " AND w.dp_c_no = '5965' AND (w.section_ord_no IS NULL OR w.section_ord_no = '')";
}
elseif($type == 25)
{
    $idx   = 3;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "상품주문번호")
        ->setCellValue('B1', "택배사")
        ->setCellValue('C1', "송장번호")
        ->setCellValue('D1', "주문 상태 변경")
        ->setCellValue('A2', "발주처리를 하고자 하는 해당 상품의 주문번호 또는 품목주문번호를 입력해 주세요. *동일한 주문번호와 품목주문번호가 존재할 경우, 해당 건은 실패처리 되며 개별 송장등록이 필요합니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('B2', "발송처리 시 사용하실 택배사를 정확히 입력해 주세요. (예: CJ대한통운, 우체국택배, 한진택배 등) 송장번호가 아직 발행되지 않았거나 배송조회가 불가능한 택배인 경우 기타택배 를 입력해 주세요. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('C2', "발송처리 시 사용된 송장번호를 입력해 주세요. 송장번호가 없는 '기타택배'를 사용하신 경우에는 임의의 숫자(ex: 1111)를 입력해 주시기 바랍니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('D2', "원하시는 주문 상태를 입력해주세요.(배송 준비중, 배송중) *입력하지 않으면 상태 변경이 되지 않습니다. 배송준비중으로 상태 변경시 택배사와 송장번호 입력을 생략할 수 있으며, 생략시 택배사, 송장번호가 변경되지 않습니다.")
    ;

    $title      = "아임웹(누잠) 운송장";
    $add_where .= " AND w.dp_c_no = '6012' AND (w.section_ord_no IS NULL OR w.section_ord_no = '')";
}
elseif($type == 99)
{
    $idx   = 3;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "주문섹션번호")
        ->setCellValue('B1', "주문섹션품목번호")
        ->setCellValue('C1', "수량")
        ->setCellValue('D1', "택배사")
        ->setCellValue('E1', "송장번호")
        ->setCellValue('F1', "주문 상태 변경")
        ->setCellValue('A2', "발주처리를 하고자 하는 상품에 해당하는 섹션번호를 입력해주세요. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('B2', "발주처리를 하고자 하는 상품에 해당하는 섹션품목번호를 입력해주세요. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('C2', "발주처리를 하고자 하는 상품의 수량을 입력해주세요. 주문수량보다 적을경우 부분배송처리가 되지만, 주문수량을 초과하는 값을 입력할 경우, 해당 섹션은 배송실패처리 됩니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('D2', "발송처리 시 사용하실 택배사를 정확히 입력해 주세요. (예: CJ대한통운, 우체국택배, 한진택배 등) 송장 번호가 아직 발행되지 않았거나 배송조회가 불가능한 택배인 경우 기타택배를 입력해주세요. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('E2', "발송처리 시 사용된 송장번호를 입력해 주세요. 송장번호가 없는 '기타택배'를 사용하신 경우에는 임의의 숫자(ex: 1111)를 입력해 주시기 바랍니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('F2', "원하시는 주문 상태를 입력해주세요. 변경 가능 상태: (배송대기, 배송중) *입력하지 않으면 상태 변경이 되지 않습니다.")
    ;

    $title      = "아임웹(베라베프)_v2 운송장";
    $add_where .= " AND w.dp_c_no = '{$imweb_type}' AND (w.section_ord_no IS NOT NULL AND w.section_ord_no != '')";

    switch($imweb_type){
        case "5800":
            $title      = "아임웹(닥터피엘)_v2 운송장";
            break;
        case "5261":
            $title      = "아임웹(와이즈웰)_v2 운송장";
            break;
        case "5958":
            $title      = "아임웹(아이레놀)_v2 운송장";
            break;
        case "6012":
            $title      = "아임웹(누잠)_v2 운송장";
            break;
    }
}
else
{
    $idx   = 3;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', "상품주문번호")
        ->setCellValue('B1', "택배사")
        ->setCellValue('C1', "송장번호")
        ->setCellValue('D1', "주문 상태 변경")
        ->setCellValue('A2', "발주처리를 하고자 하는 해당 상품의 주문번호 또는 품목주문번호를 입력해 주세요. *동일한 주문번호와 품목주문번호가 존재할 경우, 해당 건은 실패처리 되며 개별 송장등록이 필요합니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('B2', "발송처리 시 사용하실 택배사를 정확히 입력해 주세요. (예: CJ대한통운, 우체국택배, 한진택배 등) 송장번호가 아직 발행되지 않았거나 배송조회가 불가능한 택배인 경우 기타택배 를 입력해 주세요. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('C2', "발송처리 시 사용된 송장번호를 입력해 주세요. 송장번호가 없는 '기타택배'를 사용하신 경우에는 임의의 숫자(ex: 1111)를 입력해 주시기 바랍니다. 셀 서식이 반드시 텍스트여야 합니다.")
        ->setCellValue('D2', "원하시는 주문 상태를 입력해주세요.(배송 준비중, 배송중) *입력하지 않으면 상태 변경이 되지 않습니다. 배송준비중으로 상태 변경시 택배사와 송장번호 입력을 생략할 수 있으며, 생략시 택배사, 송장번호가 변경되지 않습니다.")
    ;

    $add_where .= " AND w.dp_c_no = '1372' AND (w.section_ord_no IS NULL OR w.section_ord_no = '')";
}

$add_deli_where = "1=1";
if(!empty($delivery_type)){
    $add_deli_where .= " AND wcd.delivery_type = '{$delivery_type}'";
}

# 리스트 쿼리
$work_cms_sql = "
    SELECT
        *,
        DATE_FORMAT(main.order_date, '%Y-%m-%d %H:%i') as order_date,
        DATE_FORMAT(main.payment_date, '%Y-%m-%d %H:%i') as payment_date,
        (SELECT title from product_cms prd_cms where prd_cms.prd_no=main.prd_no) as prd_name,
        (SELECT prd_code from product_cms prd_cms where prd_cms.prd_no=main.prd_no) as prd_code,
        (SELECT wcd.delivery_no FROM work_cms_delivery wcd WHERE wcd.order_number=main.order_number AND wcd.prd_no=main.prd_no AND {$add_deli_where} ORDER BY wcd.`no` ASC LIMIT 1) AS delivery_no,
        (SELECT wcd.delivery_type FROM work_cms_delivery wcd WHERE wcd.order_number=main.order_number AND wcd.prd_no=main.prd_no AND {$add_deli_where} ORDER BY wcd.`no` ASC LIMIT 1) AS delivery_type
    FROM
    (
        SELECT
            w.stock_date,
            w.order_number,
            w.parent_order_number,
            w.order_date,
            w.payment_date,
            w.prd_no,
            w.quantity,
            w.dp_price_vat,
            w.recipient,
            w.recipient_hp,
            w.recipient_hp2,
            w.zip_code,
            w.recipient_addr,
            w.task_req,
            w.delivery_memo,
            w.notice,
            w.dp_c_name,
            w.section_ord_no,
            w.shop_ord_no,
            w.delivery_price
        FROM work_cms w
        WHERE {$add_where}
        ORDER BY {$add_order_by}
    ) AS main
";
if($type == 13)
{
    $work_cms_sql = "
        SELECT
            *,
           (SELECT sub.dp_c_name FROM work_cms as sub WHERE sub.order_number=main.parent_order_number LIMIT 1) AS parent_dp_c_name,
            DATE_FORMAT(main.order_date, '%Y-%m-%d %H:%i') as order_date,
            DATE_FORMAT(main.payment_date, '%Y-%m-%d %H:%i') as payment_date,
            (SELECT title from product_cms prd_cms where prd_cms.prd_no=main.prd_no) as prd_name,
            (SELECT wcd.delivery_no FROM work_cms_delivery wcd WHERE wcd.order_number=main.order_number AND wcd.prd_no=main.prd_no AND {$add_deli_where} ORDER BY wcd.`no` ASC LIMIT 1) AS delivery_no,
            (SELECT wcd.delivery_type FROM work_cms_delivery wcd WHERE wcd.order_number=main.order_number AND wcd.prd_no=main.prd_no AND {$add_deli_where} ORDER BY wcd.`no` ASC LIMIT 1) AS delivery_type
        FROM (
            SELECT
                w.order_number,
                w.parent_order_number,
                w.order_date,
                w.payment_date,
                w.prd_no,
                w.quantity,
                w.dp_price_vat,
                w.recipient,
                w.recipient_hp,
                w.zip_code,
                w.recipient_addr,
                w.delivery_memo,
                w.notice,
                w.dp_c_name,
                w.shop_ord_no
            FROM work_cms w
            WHERE {$add_where}
            GROUP BY order_number
            ORDER BY w.w_no DESC
        ) AS main
";
}
elseif($type == 18)
{
    $work_cms_sql = "
        SELECT
            *,
            DATE_FORMAT(main.order_date, '%Y-%m-%d %H:%i') as order_date,
            DATE_FORMAT(main.payment_date, '%Y-%m-%d %H:%i') as payment_date,
            (SELECT title from product_cms prd_cms where prd_cms.prd_no=main.prd_no) as prd_name,
            (SELECT wcd.delivery_no FROM work_cms_delivery wcd WHERE wcd.order_number=main.order_number AND wcd.prd_no=main.prd_no AND {$add_deli_where} ORDER BY wcd.`no` ASC LIMIT 1) AS delivery_no,
            (SELECT wcd.delivery_type FROM work_cms_delivery wcd WHERE wcd.order_number=main.order_number AND wcd.prd_no=main.prd_no AND {$add_deli_where} ORDER BY wcd.`no` ASC LIMIT 1) AS delivery_type
        FROM
        (
            SELECT
                w.stock_date,
                w.order_number,
                w.parent_order_number,
                w.order_date,
                w.payment_date,
                w.prd_no,
                w.quantity,
                w.dp_price_vat,
                w.final_price,
                w.recipient,
                w.recipient_hp,
                w.recipient_hp2,
                w.zip_code,
                w.recipient_addr,
                w.task_req,
                w.delivery_memo,
                w.notice,
                w.dp_c_name,
                w.shop_ord_no
            FROM work_cms w
            WHERE {$add_where} AND unit_price > 0
            ORDER BY w.w_no ASC
        ) AS main
    ";
}
$work_cms_query	= mysqli_query($my_db, $work_cms_sql);
if(!!$work_cms_query)
{
    $order_idx = 1;

    while($work_cms = mysqli_fetch_array($work_cms_query))
    {
        if(empty($work_cms['delivery_no'])){
            continue;
        }

        $delivery_type = $work_cms['delivery_type'];
        if(strpos($work_cms['delivery_type'], "대한통운") !== false){
            $delivery_type = "CJ대한통운";
        }

        if($type == 2 || $type == 8 || $type == 10 || $type == 12 || $type == 26)
        {
            if(!empty($work_cms['shop_ord_no'])){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit("A{$idx}", $work_cms['shop_ord_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue("B{$idx}", "택배,소포,등기")
                    ->setCellValue("C{$idx}", $delivery_type)
                    ->setCellValueExplicit("D{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ;
            }else{
                continue;
            }
        }
        elseif($type == 3)
        {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit("K{$idx}", $work_cms['shop_ord_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("L{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
            ;
        }
        elseif($type == 13)
        {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A{$idx}", $work_cms['parent_dp_c_name'])
                ->setCellValueExplicit("B{$idx}", $work_cms['parent_order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("C{$idx}", $work_cms['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("D{$idx}", $delivery_type)
                ->setCellValueExplicit("E{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("F{$idx}", $work_cms['dp_c_name'])
            ;
        }
        elseif($type == 15)
        {
            if(empty($work_cms['shop_ord_no'])){
                continue;
            }
            
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit("A{$idx}", $work_cms['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("B{$idx}", $delivery_type)
                ->setCellValueExplicit("C{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("D{$idx}", $work_cms['shop_ord_no'], PHPExcel_Cell_DataType::TYPE_STRING)
            ;
        }
        elseif($type == 16)
        {
            $task_req_val           = $work_cms['task_req'];
            $task_req_tmp           = preg_split('/\n|\r\n?/', $work_cms['task_req']);
            $task_req_prd_name      = isset($task_req_tmp[0]) ? trim(str_replace("상품명 :: ", "", $task_req_tmp[0])) : "";
            $task_req_option_name   = isset($task_req_tmp[1]) ? trim(str_replace("옵션명 :: ", "", $task_req_tmp[1])) : "";

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A{$idx}", $work_cms['stock_date'])
                ->setCellValue("B{$idx}", $order_idx)
                ->setCellValueExplicit("C{$idx}", $work_cms['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("D{$idx}", $task_req_prd_name)
                ->setCellValue("E{$idx}", $task_req_option_name)
                ->setCellValue("F{$idx}", "1")
                ->setCellValue("G{$idx}", $work_cms['recipient'])
                ->setCellValue("H{$idx}", $work_cms['recipient_hp'])
                ->setCellValue("I{$idx}", $work_cms['recipient_hp2'])
                ->setCellValue("J{$idx}", "({$work_cms['zip_code']})")
                ->setCellValue("K{$idx}", $work_cms['recipient_addr'])
                ->setCellValue("L{$idx}", $work_cms['delivery_memo'])
                ->setCellValue("M{$idx}", $work_cms['stock_date'])
                ->setCellValue("N{$idx}", $work_cms['delivery_type'])
                ->setCellValueExplicit("O{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
            ;
            $order_idx++;
        }
        elseif($type == 17)
        {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit("A{$idx}", $work_cms['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("B{$idx}", "CJ EXPRESS")
                ->setCellValue("C{$idx}", "CJ_KR")
                ->setCellValueExplicit("D{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
            ;
        }
        elseif($type == 18)
        {
            $task_req_val      = $work_cms['task_req'];
            $task_req_tmp      = preg_split('/\n|\r\n?/', $work_cms['task_req']);
            $chk_prd_name      = isset($task_req_tmp[0]) ? trim(str_replace("상품명 :: ", "", $task_req_tmp[0])) : "";
            $chk_option_name   = isset($task_req_tmp[1]) ? trim(str_replace("옵션명 :: ", "", $task_req_tmp[1])) : "";

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit("A{$idx}", $work_cms['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("B{$idx}", $work_cms['order_date'])
                ->setCellValue("C{$idx}", $work_cms['order_date'])
                ->setCellValue("D{$idx}", "")
                ->setCellValue("E{$idx}", "")
                ->setCellValue("F{$idx}", "")
                ->setCellValue("G{$idx}", $chk_prd_name)
                ->setCellValue("H{$idx}", "")
                ->setCellValue("I{$idx}", "")
                ->setCellValue("J{$idx}", $chk_option_name)
                ->setCellValue("K{$idx}", $work_cms['quantity'])
                ->setCellValue("L{$idx}", $work_cms['dp_price_vat'])
                ->setCellValue("M{$idx}", $work_cms['dp_price_vat'])
                ->setCellValue("N{$idx}", 0)
                ->setCellValue("O{$idx}", $work_cms['dp_price_vat'])
                ->setCellValue("P{$idx}", $work_cms['delivery_type'])
                ->setCellValueExplicit("Q{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("R{$idx}", $work_cms['recipient'])
                ->setCellValue("S{$idx}", $work_cms['recipient_hp'])
                ->setCellValue("T{$idx}", $work_cms['recipient_hp2'])
                ->setCellValue("U{$idx}", $work_cms['zip_code'])
                ->setCellValue("V{$idx}", $work_cms['recipient_addr'])
                ->setCellValue("W{$idx}", "배송준비")
                ->setCellValue("X{$idx}", $work_cms['delivery_memo'])
            ;
        }
        elseif($type == 20)
        {
            $task_req_val           = $work_cms['task_req'];
            $task_req_tmp           = preg_split('/\n|\r\n?/', $work_cms['task_req']);
            $task_req_prd_name      = isset($task_req_tmp[0]) ? trim(str_replace("상품명 :: ", "", $task_req_tmp[0])) : "";
            $task_req_option_name   = isset($task_req_tmp[1]) ? trim(str_replace("옵션명 :: ", "", $task_req_tmp[1])) : "";
            $delivery_memo          = addslashes($work_cms['delivery_memo']);

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A{$idx}", "기쟁니 셀렉샵")
                ->setCellValueExplicit("B{$idx}", $work_cms['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("C{$idx}", $work_cms['shop_ord_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("D{$idx}", $task_req_prd_name)
                ->setCellValue("E{$idx}", $task_req_option_name)
                ->setCellValue("F{$idx}", "")
                ->setCellValue("G{$idx}", $work_cms['prd_code'])
                ->setCellValue("H{$idx}", "")
                ->setCellValue("I{$idx}", "")
                ->setCellValue("J{$idx}", $work_cms['quantity'])
                ->setCellValue("K{$idx}", $work_cms['dp_price_vat'])
                ->setCellValue("L{$idx}", $work_cms['recipient'])
                ->setCellValue("M{$idx}", $work_cms['recipient_hp'])
                ->setCellValueExplicit("N{$idx}", $work_cms['zip_code'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("O{$idx}", $work_cms['recipient_addr'])
                ->setCellValue("P{$idx}", $delivery_memo)
                ->setCellValue("Q{$idx}", $work_cms['order_date'])
                ->setCellValueExplicit("R{$idx}", $work_cms['delivery_type'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("S{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
            ;
        }
        elseif($type == 22)
        {
            $task_req_val           = $work_cms['task_req'];
            $task_req_tmp           = preg_split('/\n|\r\n?/', $work_cms['task_req']);
            $task_req_prd_name      = isset($task_req_tmp[0]) ? trim(str_replace("상품명 :: ", "", $task_req_tmp[0])) : "";

            if($dp_company == "4316") {
                $work_cms['order_number'] = $work_cms['shop_ord_no'];
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A{$idx}", date("Y-m-d", strtotime($work_cms['order_date'])))
                ->setCellValueExplicit("B{$idx}", $work_cms['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("C{$idx}", date("Y-m-d", strtotime($work_cms['payment_date'])))
                ->setCellValue("D{$idx}", $task_req_prd_name)
                ->setCellValue("E{$idx}", $work_cms['prd_code'])
                ->setCellValue("F{$idx}", "")
                ->setCellValue("G{$idx}", $work_cms["quantity"])
                ->setCellValue("H{$idx}", $work_cms["dp_price_vat"])
                ->setCellValue("I{$idx}", $work_cms["delivery_price"])
                ->setCellValue("J{$idx}", $work_cms['recipient'])
                ->setCellValue("K{$idx}", $work_cms['recipient_hp'])
                ->setCellValue("L{$idx}", $work_cms['recipient_hp'])
                ->setCellValueExplicit("M{$idx}", $work_cms['zip_code'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("N{$idx}", $work_cms['recipient_addr'])
                ->setCellValue("O{$idx}", $work_cms['delivery_memo'])
                ->setCellValue("P{$idx}", $work_cms['notice'])
                ->setCellValue("Q{$idx}", $work_cms['dp_c_name'])
                ->setCellValueExplicit("R{$idx}", $work_cms['delivery_type'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("S{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
            ;

            if($dp_company == "4316" && strpos($work_cms["notice"], "분리배송") !== false){
                $sub_order_number = str_replace("분리배송:","",$work_cms['notice']);

                if(!empty($sub_order_number))
                {
                    $idx++;
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A{$idx}", date("Y-m-d", strtotime($work_cms['order_date'])))
                        ->setCellValueExplicit("B{$idx}", $sub_order_number, PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValue("C{$idx}", date("Y-m-d", strtotime($work_cms['payment_date'])))
                        ->setCellValue("D{$idx}", "분리배송 상품: {$task_req_prd_name}")
                        ->setCellValue("E{$idx}", $work_cms['prd_code'])
                        ->setCellValue("F{$idx}", "")
                        ->setCellValue("G{$idx}", 1)
                        ->setCellValue("H{$idx}", 0)
                        ->setCellValue("I{$idx}", $work_cms["delivery_price"])
                        ->setCellValue("J{$idx}", $work_cms['recipient'])
                        ->setCellValue("K{$idx}", $work_cms['recipient_hp'])
                        ->setCellValue("L{$idx}", $work_cms['recipient_hp'])
                        ->setCellValueExplicit("M{$idx}", $work_cms['zip_code'], PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValue("N{$idx}", $work_cms['recipient_addr'])
                        ->setCellValue("O{$idx}", $work_cms['delivery_memo'])
                        ->setCellValue("P{$idx}", "")
                        ->setCellValue("Q{$idx}", $work_cms['dp_c_name'])
                        ->setCellValueExplicit("R{$idx}", $work_cms['delivery_type'], PHPExcel_Cell_DataType::TYPE_STRING)
                        ->setCellValueExplicit("S{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ;
                }
            }
        }
        elseif($type == 99)
        {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit("A{$idx}", $work_cms['section_ord_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("B{$idx}", $work_cms['shop_ord_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("C{$idx}", $work_cms['quantity'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("D{$idx}", $work_cms['delivery_type'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("E{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("F{$idx}", "배송중", PHPExcel_Cell_DataType::TYPE_STRING)
            ;
        }
        else
        {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit("A{$idx}", $work_cms['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("B{$idx}", $delivery_type, PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("C{$idx}", $work_cms['delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValueExplicit("D{$idx}", "배송중", PHPExcel_Cell_DataType::TYPE_STRING)
            ;
        }

        $idx++;
    }
}
$idx--;

// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:X{$idx}")->getFont()->setSize(9);;

$objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
if($type == 2 || $type == 8 || $type == 10 || $type == 12 || $type == 26)
{
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00CCCCFF');
    $objPHPExcel->getActiveSheet()->getStyle("A2:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(24);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);

}
elseif($type == 3)
{
    $objPHPExcel->getActiveSheet()->getStyle("K1:L{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
}
elseif($type == 13)
{
    $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00B0DD7F');
    $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
}
elseif($type == 15)
{
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00B0DD7F');
    $objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
}
elseif($type == 16)
{
    $objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FFF2CC');
    $objPHPExcel->getActiveSheet()->getStyle("A1:O1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:O{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("C1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('0000B0F0');
    $objPHPExcel->getActiveSheet()->getStyle("C2:L{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00B4C6E7');
    $objPHPExcel->getActiveSheet()->getStyle("A1:O{$idx}")->applyFromArray($styleArray);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(6);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(24);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(40);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(24);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(6);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
}
elseif($type == 17)
{
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00B0DD7F');
    $objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
}
elseif($type == 18)
{
    $objPHPExcel->getActiveSheet()->getStyle('A1:X1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
    $objPHPExcel->getActiveSheet()->getStyle("A1:X1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A1:X1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:X{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(24);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(52);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(42);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(110);
    $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(35);

    $objPHPExcel->getActiveSheet()->getStyle("A1:X{$idx}")->applyFromArray($styleArray);
}
elseif($type == 20)
{
    $objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
    $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:S{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);

    $objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->applyFromArray($styleArray);
}
elseif($type == 22)
{
    $objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
    $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:S{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(50);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(14);

    $objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->applyFromArray($styleArray);
}
elseif($type == 99)
{
    $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00B0DD7F');
    $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("C2:C{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
}
else
{
    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00B0DD7F');
    $objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setWrapText(true);
}

$objPHPExcel->getActiveSheet()->setTitle($title);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

if($type == 99)
{
    $excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$title}.xlsx");
    header('Content-Type: text/html; charset=UTF-8');
    header("Content-type: application/octetstream");
    header('Content-Disposition: attachment;filename='.$excel_filename);

    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
}
else
{
    $excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$title}.xls");
    header('Content-Type: text/html; charset=UTF-8');
    header("Content-type: application/octetstream");
    header('Content-Disposition: attachment;filename='.$excel_filename);

    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
}

$objWriter->save('php://output');
exit;

?>
