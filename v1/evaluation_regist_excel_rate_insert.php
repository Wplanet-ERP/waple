<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');
error_reporting(E_ALL ^ E_NOTICE);

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
include_once('inc/model/Evaluation.php');


$ev_no      = isset($_POST['ev_no']) ? $_POST['ev_no'] : "";
$file_name  = $_FILES["rate_file"]["tmp_name"];

$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet   = $excel->getActiveSheet();
$totalRow       = $objWorksheet->getHighestRow();
$ev_model       = Evaluation::Factory();
$ev_model->setMainInit("evaluation_relation", "ev_r_no");
$upd_data       = [];
$chk_list = [];

for ($i = 3; $i <= $totalRow; $i++)
{
    //변수 초기화
    $receiver_s_name    = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));
    $evaluator_s_name   = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));
    $rate               = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));

    if(strpos($rate, "=") !== false){
        $rate = $objWorksheet->getCell("E{$i}")->getCalculatedValue();
    }

    $rec_s_no = $eval_s_no = "";

    if(empty($receiver_s_name)){
        break;
    }

    $receiver_sql       = "SELECT s_no FROM staff WHERE s_name = '{$receiver_s_name}' AND staff_state='1'";
    $receiver_query     = mysqli_query($my_db, $receiver_sql);
    $receiver_result    = mysqli_fetch_assoc($receiver_query);
    $rec_s_no           = isset($receiver_result['s_no']) ? $receiver_result['s_no'] : "";

    $evaluator_sql      = "SELECT s_no FROM staff WHERE s_name = '{$evaluator_s_name}' AND staff_state='1'";
    $evaluator_query    = mysqli_query($my_db, $evaluator_sql);
    $evaluator_result   = mysqli_fetch_assoc($evaluator_query);
    $eval_s_no          = isset($evaluator_result['s_no']) ? $evaluator_result['s_no'] : "";

    if(empty($rec_s_no))
    {
        echo "평가받는 사람: {$receiver_s_name}<br/>";
        echo "ROW : {$i}, 해당 직원을 찾을 수 없습니다";
        exit;
    }

    if(empty($eval_s_no))
    {
        echo "평가하는 사람: {$evaluator_s_name}<br/>";
        echo "ROW : {$i}, 해당 직원을 찾을 수 없습니다";
        exit;
    }

    $chk_ev_sql    = "SELECT ev_r_no FROM evaluation_relation WHERE ev_no='{$ev_no}' AND evaluator_s_no='{$eval_s_no}' AND receiver_s_no='{$rec_s_no}'";
    $chk_ev_query  = mysqli_query($my_db, $chk_ev_sql);
    $chk_ev_result = mysqli_fetch_assoc($chk_ev_query);

    if($chk_ev_result['ev_r_no'])
    {
        if(!isset($chk_list[$rec_s_no][$eval_s_no])){
            $upd_data[] = array(
                "ev_r_no"   => $chk_ev_result['ev_r_no'],
                "rate"      => $rate
            );
            $chk_list[$rec_s_no][$eval_s_no] = 1;
        }
    }
}

if (!$ev_model->multiUpdate($upd_data)){
    echo "평가자 비중 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    exit;
}else{
    exit("<script>alert('평가자 비중이 반영 되었습니다.');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
}


?>
