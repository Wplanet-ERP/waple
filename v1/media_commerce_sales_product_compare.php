<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Company.php');

# Navigation & My Quick
$nav_prd_no  = "237";
$nav_title   = "커머스 상품별 판매 비교";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$kind_model                 = Kind::Factory();
$company_model              = Company::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$dp_company_list            = $company_model->getDpList();
$cubing_prd_option          = getCubingPrdOption();
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

# 검색조건
$add_where              = "1=1 AND `w`.delivery_state='4'";
$today_val              = date('Y-m-d');
$month_val              = date('Y-m', strtotime('-3 months'))."-01";
$sch_order_type         = isset($_GET['sch_order_type']) ? $_GET['sch_order_type'] : "unit_price";
$sch_brand_g1           = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2           = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand              = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_order_date_type_val= isset($_GET['sch_order_date_type_val']) ? $_GET['sch_order_date_type_val'] : "3";
$sch_order_date_type    = isset($_GET['sch_order_date_type']) ? $_GET['sch_order_date_type'] : $sch_order_date_type_val;
$sch_order_s_date       = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : $month_val;
$sch_order_e_date       = isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : $today_val;
$sch_comp_s_date        = isset($_GET['sch_comp_s_date']) ? $_GET['sch_comp_s_date'] : "";
$sch_comp_e_date        = isset($_GET['sch_comp_e_date']) ? $_GET['sch_comp_e_date'] : "";
$sch_price_type         = isset($_GET['sch_price_type']) ? $_GET['sch_price_type'] : "";
$sch_dp_company         = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";
$sch_cubing_combined    = isset($_GET['sch_cubing_combined']) ? $_GET['sch_cubing_combined'] : "";
$search_url             = getenv("QUERY_STRING");

if(empty($search_url)){
    $search_url = "sch_order_type=unit_price&sch_order_date_type=3";
}

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_price_type)){

    if($sch_price_type == '1'){
        $add_where .= " AND w.unit_price > 0";
    }elseif($sch_price_type == '2'){
        $add_where .= " AND w.unit_price = 0";
    }
    $smarty->assign("sch_price_type", $sch_price_type);
}

if(!empty($sch_dp_company)){
    $add_where .= " AND w.dp_c_no = '{$sch_dp_company}'";
    $smarty->assign("sch_dp_company", $sch_dp_company);
}

# 브랜드 옵션
if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("search_url", $search_url);
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);
$smarty->assign("sch_cubing_combined", $sch_cubing_combined);

# 날짜 처리
$all_base_date_where    = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_order_s_date}' AND '{$sch_order_e_date}'";
$all_comp_date_where    = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_comp_s_date}' AND '{$sch_comp_e_date}'";
$all_date_key 	        = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
$all_date_title         = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";

$smarty->assign("sch_order_type", $sch_order_type);
$smarty->assign("sch_order_date_type", $sch_order_date_type);
$smarty->assign("sch_order_s_date", $sch_order_s_date);
$smarty->assign("sch_order_e_date", $sch_order_e_date);
$smarty->assign("sch_comp_s_date", $sch_comp_s_date);
$smarty->assign("sch_comp_e_date", $sch_comp_e_date);

$cms_product_name_list          = [];
$cms_product_total_qty_list     = [];
$cms_product_total_price_list   = [];
$cms_product_total_base_qty     = 0;
$cms_product_total_comp_qty     = 0;
$cms_product_total_qty_per      = 0;
$cms_product_total_base_price   = 0;
$cms_product_total_comp_price   = 0;
$cms_product_total_price_per    = 0;
if(!empty($sch_order_s_date) && !empty($sch_order_e_date) && !empty($sch_comp_s_date) && !empty($sch_comp_e_date))
{
    # 날짜 생성
    $total_all_date_list        = [];
    $total_all_base_date_list   = [];
    $total_all_comp_date_list   = [];

    $all_base_date_sql = "
        SELECT
            {$all_date_title} as chart_title,
            {$all_date_key} as chart_key
        FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
        as allday
        WHERE {$all_base_date_where}
        ORDER BY chart_key
    ";
    $all_base_date_query = mysqli_query($my_db, $all_base_date_sql);
    while($all_base_date = mysqli_fetch_assoc($all_base_date_query))
    {
        $total_all_date_list[$all_base_date['chart_key']]       = array("s_date" => date("Y-m-d",strtotime($all_base_date['chart_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($all_base_date['chart_key']))." 23:59:59");
        $total_all_base_date_list[$all_base_date['chart_key']]  = array("s_date" => date("Y-m-d",strtotime($all_base_date['chart_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($all_base_date['chart_key']))." 23:59:59");
    }

    $all_comp_date_sql = "
        SELECT
            {$all_date_title} as chart_title,
            {$all_date_key} as chart_key
        FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
        as allday
        WHERE {$all_comp_date_where}
        ORDER BY chart_key
    ";
    $all_comp_date_query = mysqli_query($my_db, $all_comp_date_sql);
    while($all_comp_date = mysqli_fetch_assoc($all_comp_date_query))
    {
        $total_all_date_list[$all_comp_date['chart_key']]       = array("s_date" => date("Y-m-d",strtotime($all_comp_date['chart_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($all_comp_date['chart_key']))." 23:59:59");
        $total_all_comp_date_list[$all_comp_date['chart_key']]  = array("s_date" => date("Y-m-d",strtotime($all_comp_date['chart_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($all_comp_date['chart_key']))." 23:59:59");
    }

    $cms_product_base_qty_list     = [];
    $cms_product_base_price_list   = [];
    $cms_product_comp_qty_list     = [];
    $cms_product_comp_price_list   = [];

    foreach($total_all_date_list as $init_date)
    {
        $chk_prd_all_sql   = "SELECT prd_no, (SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no) as prd_name FROM work_cms w WHERE {$add_where} AND (order_date BETWEEN '{$init_date['s_date']}' AND '{$init_date['e_date']}') GROUP BY prd_no";
        $chk_prd_all_query = mysqli_query($my_db, $chk_prd_all_sql);
        while($chk_prd = mysqli_fetch_assoc($chk_prd_all_query))
        {
            $chk_prd_no     = $chk_prd['prd_no'];
            $chk_prd_name   = $chk_prd['prd_name'];
            if($sch_cubing_combined == "1"){
                if(in_array($chk_prd['prd_no'], $cubing_prd_option['1'])){
                    $chk_prd_no     = "single";
                    $chk_prd_name   = "닥터피엘 큐빙 단품";
                }elseif(in_array($chk_prd['prd_no'], $cubing_prd_option['2'])){
                    $chk_prd_no     = "double";
                    $chk_prd_name   = "닥터피엘 큐빙 2개 세트";
                }elseif(in_array($chk_prd['prd_no'], $cubing_prd_option['3'])){
                    $chk_prd_no     = "triple";
                    $chk_prd_name   = "닥터피엘 큐빙 3개 세트";
                }elseif(in_array($chk_prd['prd_no'], $cubing_prd_option['4'])){
                    $chk_prd_no     = "quad";
                    $chk_prd_name   = "닥터피엘 큐빙 4개 세트";
                }
            }

            $cms_product_name_list[$chk_prd_no]        = $chk_prd_name;
            $cms_product_base_qty_list[$chk_prd_no]    = 0;
            $cms_product_base_price_list[$chk_prd_no]  = 0;
            $cms_product_comp_qty_list[$chk_prd_no]    = 0;
            $cms_product_comp_price_list[$chk_prd_no]  = 0;
        }
    }

    foreach($total_all_base_date_list as $base_date)
    {
        $chk_prd_base_sql   = "SELECT prd_no, SUM(quantity) as total_qty, SUM(unit_price) as total_price FROM work_cms w WHERE {$add_where} AND (order_date BETWEEN '{$base_date['s_date']}' AND '{$base_date['e_date']}') GROUP BY prd_no";
        $chk_prd_base_query = mysqli_query($my_db, $chk_prd_base_sql);
        while($chk_prd_base = mysqli_fetch_assoc($chk_prd_base_query))
        {
            $chk_prd_no     = $chk_prd_base['prd_no'];
            if($sch_cubing_combined == "1"){
                if(in_array($chk_prd_base['prd_no'], $cubing_prd_option['1'])){
                    $chk_prd_no     = "single";
                }elseif(in_array($chk_prd_base['prd_no'], $cubing_prd_option['2'])){
                    $chk_prd_no     = "double";
                }elseif(in_array($chk_prd_base['prd_no'], $cubing_prd_option['3'])){
                    $chk_prd_no     = "triple";
                }elseif(in_array($chk_prd_base['prd_no'], $cubing_prd_option['4'])){
                    $chk_prd_no     = "quad";
                }
            }

            $cms_product_base_qty_list[$chk_prd_no]   += $chk_prd_base['total_qty'];
            $cms_product_base_price_list[$chk_prd_no] += $chk_prd_base['total_price'];
        }
    }
    arsort($cms_product_base_qty_list);
    arsort($cms_product_base_price_list);

    foreach($total_all_comp_date_list as $comp_date)
    {
        $chk_prd_comp_sql   = "SELECT prd_no, SUM(quantity) as total_qty, SUM(unit_price) as total_price  FROM work_cms w WHERE {$add_where} AND (order_date BETWEEN '{$comp_date['s_date']}' AND '{$comp_date['e_date']}') GROUP BY prd_no";
        $chk_prd_comp_query = mysqli_query($my_db, $chk_prd_comp_sql);
        while($chk_prd_comp = mysqli_fetch_assoc($chk_prd_comp_query))
        {
            $chk_prd_no     = $chk_prd_comp['prd_no'];
            if($sch_cubing_combined == "1"){
                if(in_array($chk_prd_comp['prd_no'], $cubing_prd_option['1'])){
                    $chk_prd_no     = "single";
                }elseif(in_array($chk_prd_comp['prd_no'], $cubing_prd_option['2'])){
                    $chk_prd_no     = "double";
                }elseif(in_array($chk_prd_comp['prd_no'], $cubing_prd_option['3'])){
                    $chk_prd_no     = "triple";
                }elseif(in_array($chk_prd_comp['prd_no'], $cubing_prd_option['4'])){
                    $chk_prd_no     = "quad";
                }
            }

            $cms_product_comp_qty_list[$chk_prd_no]   += $chk_prd_comp['total_qty'];
            $cms_product_comp_price_list[$chk_prd_no] += $chk_prd_comp['total_price'];
        }
    }

    foreach($cms_product_base_qty_list as $qty_prd_no => $total_qty){
        $base_qty   = $total_qty;
        $comp_qty   = isset($cms_product_comp_qty_list[$qty_prd_no]) ? $cms_product_comp_qty_list[$qty_prd_no] : 0;
        $qty_per    = ($comp_qty == 0) ? 0 : (($base_qty == 0) ? -100 : ROUND(((($base_qty-$comp_qty)/$comp_qty) *100), 1));

        $cms_product_total_base_qty             += $base_qty;
        $cms_product_total_comp_qty             += $comp_qty;
        $cms_product_total_qty_list[$qty_prd_no] = array("base_qty" => $base_qty, "comp_qty" => $comp_qty, "qty_per" => $qty_per);
    }

    foreach($cms_product_base_price_list as $price_prd_no => $total_price){
        $base_price   = $total_price;
        $comp_price   = isset($cms_product_comp_price_list[$price_prd_no]) ? $cms_product_comp_price_list[$price_prd_no] : 0;
        $price_per    = ($comp_price == 0) ? 0 : (($base_price == 0) ? -100 : ROUND(((($base_price-$comp_price)/$comp_price) *100), 1));

        $cms_product_total_base_price               += $base_price;
        $cms_product_total_comp_price               += $comp_price;
        $cms_product_total_price_list[$price_prd_no] = array("base_price" => $base_price, "comp_price" => $comp_price, "price_per" => $price_per);
    }

    $cms_product_total_qty_per      = ($cms_product_total_comp_qty == 0) ? 0 : (($cms_product_total_base_qty == 0) ? -100 : ROUND(((($cms_product_total_base_qty-$cms_product_total_comp_qty)/$cms_product_total_comp_qty) *100), 1));
    $cms_product_total_price_per    = ($cms_product_total_comp_price == 0) ? 0 : (($cms_product_total_base_price == 0) ? -100 : ROUND(((($cms_product_total_base_price-$cms_product_total_comp_price)/$cms_product_total_comp_price) *100), 1));
}

$smarty->assign("sch_order_type_option", getDateTypeOption());
$smarty->assign("dp_company_list", $dp_company_list);
$smarty->assign("cms_price_type_option", getCmsPriceType());
$smarty->assign("cms_product_total_base_qty", $cms_product_total_base_qty);
$smarty->assign("cms_product_total_comp_qty", $cms_product_total_comp_qty);
$smarty->assign("cms_product_total_qty_per", $cms_product_total_qty_per);
$smarty->assign("cms_product_total_base_price", $cms_product_total_base_price);
$smarty->assign("cms_product_total_comp_price", $cms_product_total_comp_price);
$smarty->assign("cms_product_total_price_per", $cms_product_total_price_per);
$smarty->assign("cms_product_name_list", $cms_product_name_list);
$smarty->assign("cms_product_total_qty_list", $cms_product_total_qty_list);
$smarty->assign("cms_product_total_price_list", $cms_product_total_price_list);

$smarty->display('media_commerce_sales_product_compare.html');
?>