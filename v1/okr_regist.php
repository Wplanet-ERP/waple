<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');

// GET : oo_no= 로 넘어온 프로모션 고유번호(oo_no)를 체크하고 있으면 idx에 저장
$oo_no=isset($_GET['oo_no']) ? $_GET['oo_no'] : "";
$smarty->assign("oo_no",$oo_no);

// GET : 위의 oo_no 번호가 없으면 등록(write)으로, 없으면 수정(modify)으로 mode에 저장
$mode=(!$oo_no) ? "write":"modify";

// POST : form action 을 통해 넘겨받은 process 가 있는지 체크하고 있으면 proc에 저장
$proc=isset($_POST['process']) ? $_POST['process'] : "";

// 담당자리스트
$staff_sql="select s_no,s_name from staff where staff_state = '1'";
$staff_query=mysqli_query($my_db,$staff_sql);

while($staff_data=mysqli_fetch_array($staff_query)) {
	$staffs[]=array(
		'no'=>$staff_data['s_no'],
		'name'=>$staff_data['s_name']
	);
}

$smarty->assign("staff",$staffs);

if ($proc == "f_state") { // 진행상태 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET state = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET state = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "진행상태 저장에 실패 하였습니다.";
	else
		echo "진행상태가 저장 되었습니다.";

	exit;

}else if ($proc == "f_sdate") { // 진행기간 시작일 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET sdate = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET sdate = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "진행기간 시작일 저장에 실패 하였습니다.";
	else
		echo "진행기간 시작일이 저장 되었습니다.";

	exit;

}else if ($proc == "f_edate") { // 진행기간 종료일 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET edate = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET edate = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "진행기간 종료일 저장에 실패 하였습니다.";
	else
		echo "진행기간 종료일이 저장 되었습니다.";

	exit;

}else if ($proc == "f_kind") { // 구분 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET kind = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET kind = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "구분 저장에 실패 하였습니다.";
	else
		echo "구분이 저장 되었습니다.";

	exit;

}else if ($proc == "f_obj_name") { // OBJECT NAME 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET obj_name = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET obj_name = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "OBJECT NAME 저장에 실패 하였습니다.";
	else
		echo "OBJECT NAME이 저장 되었습니다.";

	exit;

}else if ($proc == "f_s_no") { // 담당자 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET s_no = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET s_no = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "담당자 저장에 실패 하였습니다.";
	else
		echo "담당자가 저장 되었습니다.";

	exit;

}else if ($proc == "f_team") { // 부서 자동저장
    $oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
    $value = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($value)){
        $sql = "UPDATE okr_obj SET team = NULL WHERE oo_no = '" . $oo_no . "'";
    }else{
        $sql = "UPDATE okr_obj SET team = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
    }

    if (!mysqli_query($my_db, $sql))
        echo "부서 저장에 실패 하였습니다.";
    else
        echo "부서가 저장 되었습니다.";

    exit;

}else if ($proc == "f_check_1_date") { // 1차점검 점검일 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET check_1_date = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET check_1_date = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "1차점검 점검일 저장에 실패 하였습니다.";
	else
		echo "1차점검 점검일이 저장 되었습니다.";

	exit;

}else if ($proc == "f_check_1_rate") { // 1차점검 완료율 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET check_1_rate = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET check_1_rate = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "1차점검 완료율 저장에 실패 하였습니다.";
	else
		echo "1차점검 완료율이 저장 되었습니다.";

	exit;

}else if ($proc == "f_check_1_memo") { // 1차점검 진행현황 및 점검내용 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET check_1_memo = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET check_1_memo = '" . addslashes($value) . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "1차점검 진행현황 및 점검내용 저장에 실패 하였습니다.";
	else
		echo "1차점검 진행현황 및 점검내용이 저장 되었습니다.";

	exit;

}else if ($proc == "f_check_2_date") { // 2차점검 점검일 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET check_2_date = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET check_2_date = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "2차점검 점검일 저장에 실패 하였습니다.";
	else
		echo "2차점검 점검일이 저장 되었습니다.";

	exit;

}else if ($proc == "f_check_2_rate") { // 2차점검 완료율 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET check_2_rate = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET check_2_rate = '" . $value . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "2차점검 완료율 저장에 실패 하였습니다.";
	else
		echo "2차점검 완료율이 저장 되었습니다.";

	exit;

}else if ($proc == "f_check_2_memo") { // 2차점검 진행현황 및 점검내용 자동저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_obj SET check_2_memo = NULL WHERE oo_no = '" . $oo_no . "'";
	}else{
		$sql = "UPDATE okr_obj SET check_2_memo = '" . addslashes($value) . "' WHERE oo_no = '" . $oo_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "2차점검 진행현황 및 점검내용 저장에 실패 하였습니다.";
	else
		echo "2차점검 진행현황 및 점검내용이 저장 되었습니다.";

	exit;

}else if ($proc == "f_kr_name") { // KEY RESULT NAME 자동저장
	$ok_no = (isset($_POST['ok_no'])) ? $_POST['ok_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_kr SET kr_name = NULL WHERE ok_no = '" . $ok_no . "'";
	}else{
		$sql = "UPDATE okr_kr SET kr_name = '" . addslashes($value) . "' WHERE ok_no = '" . $ok_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "KEY RESULT NAME 저장에 실패 하였습니다.";
	else
		echo "KEY RESULT NAME이 저장 되었습니다.";

	exit;

}else if ($proc == "f_kr_reason") { // KEY RESULT 설정사유 및 근거 자동저장
	$ok_no = (isset($_POST['ok_no'])) ? $_POST['ok_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_kr SET kr_reason = NULL WHERE ok_no = '" . $ok_no . "'";
	}else{
		$sql = "UPDATE okr_kr SET kr_reason = '" . addslashes($value) . "' WHERE ok_no = '" . $ok_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "KEY RESULT 설정사유 및 근거 저장에 실패 하였습니다.";
	else
		echo "KEY RESULT 설정사유 및 근거가 저장 되었습니다.";

	exit;

}else if ($proc == "f_complete_rate") { // 완료율 자동저장
	$ok_no = (isset($_POST['ok_no'])) ? $_POST['ok_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_kr SET complete_rate = NULL WHERE ok_no = '" . $ok_no . "'";
	}else{
		$sql = "UPDATE okr_kr SET complete_rate = '" . $value . "' WHERE ok_no = '" . $ok_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "완료율 저장에 실패 하였습니다.";
	else
		echo "완료율이 저장 되었습니다.";

	exit;

}else if ($proc == "f_importance") { // 중요도 자동저장
	$ok_no = (isset($_POST['ok_no'])) ? $_POST['ok_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_kr SET importance = NULL WHERE ok_no = '" . $ok_no . "'";
	}else{
		$sql = "UPDATE okr_kr SET importance = '" . $value . "' WHERE ok_no = '" . $ok_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "중요도 저장에 실패 하였습니다.";
	else
		echo "중요도가 저장 되었습니다.";

	exit;

}else if ($proc == "f_difficulty") { // 난이도 자동저장
	$ok_no = (isset($_POST['ok_no'])) ? $_POST['ok_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_kr SET difficulty = NULL WHERE ok_no = '" . $ok_no . "'";
	}else{
		$sql = "UPDATE okr_kr SET difficulty = '" . $value . "' WHERE ok_no = '" . $ok_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "난이도 저장에 실패 하였습니다.";
	else
		echo "난이도가 저장 되었습니다.";

	exit;

}else if ($proc == "f_challenge") { // 도전성 자동저장
	$ok_no = (isset($_POST['ok_no'])) ? $_POST['ok_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_kr SET challenge = NULL WHERE ok_no = '" . $ok_no . "'";
	}else{
		$sql = "UPDATE okr_kr SET challenge = '" . $value . "' WHERE ok_no = '" . $ok_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "도전성 저장에 실패 하였습니다.";
	else
		echo "도전성가 저장 되었습니다.";

	exit;

}else if ($proc == "f_kr_result") { // KR 과정 및 성과 자동저장
	$ok_no = (isset($_POST['ok_no'])) ? $_POST['ok_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	if(empty($value)){
		$sql = "UPDATE okr_kr SET kr_result = NULL WHERE ok_no = '" . $ok_no . "'";
	}else{
		$sql = "UPDATE okr_kr SET kr_result = '" . addslashes($value) . "' WHERE ok_no = '" . $ok_no . "'";
	}

	if (!mysqli_query($my_db, $sql))
		echo "KR 성과 저장에 실패 하였습니다.";
	else
		echo "KR 성과 저장 되었습니다.";

	exit;

/////////////////////////////////////////

}elseif($proc=="del_okr_kr") {  // key result 삭제
	$oo_no = (isset($_POST['oo_no']))?$_POST['oo_no']:"";
	$ok_no = (isset($_POST['ok_no'])) ? $_POST['ok_no'] : "";

	$okr_kr_sel_sql 	= "SELECT priority FROM okr_kr WHERE ok_no='{$ok_no}' LIMIT 1";
	$okr_kr_sel_query 	= mysqli_query($my_db, $okr_kr_sel_sql);
    $okr_kr_sel_result	= mysqli_fetch_assoc($okr_kr_sel_query);
    $f_priority_new		= $okr_kr_sel_result['priority'];

    $chk_priority_sql 	= "SELECT * FROM okr_kr WHERE oo_no='{$oo_no}' AND priority >= {$f_priority_new} AND ok_no != '{$ok_no}' ORDER BY priority ASC, ok_no ASC";
    $chk_priority_query = mysqli_query($my_db, $chk_priority_sql);
    $chk_priority_list  = [];
    while($chk_priority = mysqli_fetch_assoc($chk_priority_query))
    {
        $chk_priority_list[$chk_priority['ok_no']] = $f_priority_new;
        $f_priority_new++;
    }

    $okr_kr_sql = "DELETE FROM okr_kr WHERE ok_no = '{$ok_no}'";
	if(!mysqli_query($my_db, $okr_kr_sql)){
		exit("<script>alert('ok_no = {$ok_no} 삭제에 실패 하였습니다.');location.href='okr_regist.php?oo_no=$oo_no';</script>");
	}else{
        if(!empty($chk_priority_list)){
            foreach($chk_priority_list as $ok_no => $priority){
                $upd_priority_sql = "UPDATE okr_kr SET priority='{$priority}' WHERE ok_no='{$ok_no}'";
                mysqli_query($my_db, $upd_priority_sql);
            }
        }
		exit("<script>location.href='okr_regist.php?oo_no=$oo_no';</script>");
	}

}elseif($proc=="modify_sort_okr") {  // key result 삭제
    $oo_no 		= (isset($_POST['oo_no']))?$_POST['oo_no']:"";
    $ok_no 		= (isset($_POST['ok_no'])) ? $_POST['ok_no'] : "";
    $priority 	= (isset($_POST['f_priority'])) ? $_POST['f_priority'] : "";

    $okr_kr_sql = "UPDATE okr_kr SET priority='{$priority}' WHERE ok_no = '{$ok_no}'";

    $f_priority_new 	= $priority;
    $chk_priority_sql 	= "SELECT * FROM okr_kr WHERE oo_no='{$oo_no}' AND priority >= {$f_priority_new} AND ok_no != '{$ok_no}' ORDER BY priority ASC, ok_no ASC";
    $chk_priority_query = mysqli_query($my_db, $chk_priority_sql);
    $chk_priority_list  = [];
    while($chk_priority = mysqli_fetch_assoc($chk_priority_query))
    {
        $f_priority_new++;
        $chk_priority_list[$chk_priority['ok_no']] = $f_priority_new;
    }

    if(!mysqli_query($my_db, $okr_kr_sql)){
        exit("<script>alert('순서 변경에 실패 하였습니다.');location.href='okr_regist.php?oo_no=$oo_no';</script>");
    }else{
        if(!empty($chk_priority_list)){
            foreach($chk_priority_list as $ok_no => $priority){
                $upd_priority_sql = "UPDATE okr_kr SET priority='{$priority}' WHERE ok_no='{$ok_no}'";
                mysqli_query($my_db, $upd_priority_sql);
            }
        }
        exit("<script>location.href='okr_regist.php?oo_no=$oo_no';</script>");
    }

}elseif($proc=="create_keyresult") {  // key result 추가
	$oo_no=(isset($_POST['oo_no']))?$_POST['oo_no']:"";
    $f_priority_new = $_POST['f_priority_new'];

	$okr_kr_sql = "INSERT into okr_kr set
					oo_no = '".$oo_no."',
					priority = '{$f_priority_new}',
					kr_name = '".addslashes($_POST['f_kr_name_new'])."',
					kr_reason = '".addslashes($_POST['f_kr_reason_new'])."'
				";

    $chk_priority_sql 	= "SELECT * FROM okr_kr WHERE oo_no='{$oo_no}' AND priority >= {$f_priority_new} ORDER BY priority ASC, ok_no ASC";
    $chk_priority_query = mysqli_query($my_db, $chk_priority_sql);
    $chk_priority_list  = [];
    while($chk_priority = mysqli_fetch_assoc($chk_priority_query))
	{
        $f_priority_new++;
        $chk_priority_list[$chk_priority['ok_no']] = $f_priority_new;
	}

	if(!mysqli_query($my_db, $okr_kr_sql)){
		exit("<script>alert('oo_no = {$oo_no} 추가에 실패 하였습니다.');location.href='okr_regist.php?oo_no=$oo_no';</script>");
	}else{
		if(!empty($chk_priority_list)){
			foreach($chk_priority_list as $ok_no => $priority){
				$upd_priority_sql = "UPDATE okr_kr SET priority='{$priority}' WHERE ok_no='{$ok_no}'";
				mysqli_query($my_db, $upd_priority_sql);
			}
		}
		exit("<script>location.href='okr_regist.php?oo_no=$oo_no';</script>");
	}
}

$okr_year = date('Y');
$date_list = array(
    'one_third' => array('start' => $okr_year.'-01-01', 'end' => $okr_year.'-04-30'),
    'two_third' => array('start' => $okr_year.'-05-01', 'end' => $okr_year.'-08-31'),
    'thr_third'	=> array('start' => $okr_year.'-09-01', 'end' => $okr_year.'-12-31'),
    'one_year' 	=> array('start' => $okr_year.'-01-01', 'end' => $okr_year.'-12-31'),
);


$okr_s_no = $session_s_no;
if($mode == "modify")
{
	// OKR OBJECT 리스트 쿼리
	$okr_obj_sql = "
			SELECT
				*,
				(SELECT t.team_name FROM team t WHERE t.team_code = oo.team) AS team_name,
				(SELECT s.rar FROM staff s WHERE s.s_no = oo.s_no) AS rar
			FROM okr_obj oo
			WHERE oo.oo_no='{$oo_no}'
	";

	$okr_obj_result = mysqli_query($my_db, $okr_obj_sql);
	$okr_obj = mysqli_fetch_array($okr_obj_result);
	$okr_s_no = $okr_obj['s_no'];

	$obj_d_day = intval((strtotime(date("Y-m-d",time()))-strtotime($okr_obj['edate'])) / 86400);

	if($okr_obj['display'] == '2'){
		alert('삭제된 OKR 입니다', 'okr_list.php');
	}

	$okr_obj['obj_d_day'] = $obj_d_day;
	$okr_obj['popup_view'] = false;
	if($okr_obj['state'] == '1' && $session_s_no == $okr_obj['s_no']){
		$okr_obj['popup_view'] = true;
	}

	if($okr_obj['sdate'] && $okr_obj['edate'])
	{
        $okr_year = date('Y', strtotime($okr_obj['sdate']));
        $date_list = array(
            'one_third' => array('start' => $okr_year.'-01-01', 'end' => $okr_year.'-04-30'),
            'two_third' => array('start' => $okr_year.'-05-01', 'end' => $okr_year.'-08-31'),
            'thr_third'	=> array('start' => $okr_year.'-09-01', 'end' => $okr_year.'-12-31'),
            'one_year' 	=> array('start' => $okr_year.'-01-01', 'end' => $okr_year.'-12-31'),
		);

        $check_s_date = date('m', strtotime($okr_obj['sdate']));
        $check_e_date = date('m-d', strtotime($okr_obj['edate']));

        $date_term = 0;
        switch($check_s_date){
			case '01': $date_term = ($check_e_date == '12-31') ? 4 : 1; break;
			case '05': $date_term = 2; break;
			case '09': $date_term = 3; break;
		}

		$smarty->assign("date_term", $date_term);
	}

	// OKR KEY RESULT 리스트 쿼리
	$add_where = " 1 = 1";
	$add_where .= " AND (SELECT oo.s_no FROM okr_obj oo WHERE oo.oo_no = ok.oo_no) = '" . $session_s_no . "' "; // 담당자

	// OKR OBJECT 리스트 쿼리
	$okr_kr_sql = "
		SELECT
			ok.ok_no,
			ok.oo_no,
			ok.kr_name,
			ok.complete_rate,
			ok.kr_reason,
			ok.importance,
			ok.difficulty,
			ok.challenge,
			ok.kr_result,
			ok.priority
		FROM okr_kr ok
		WHERE ok.oo_no='".$oo_no."' 
		ORDER BY ok.priority ASC, ok.ok_no ASC 
	";

	// 리스트 쿼리 결과(데이터)
	$result= mysqli_query($my_db, $okr_kr_sql);

    $purpose_quality  = 0;
    $complete_quality = 0;
    $f_priority_new   = 1;
	while($okr_kr_array = mysqli_fetch_array($result))
	{
		$okr_obj['popup_view'] = false;
        $importance_val 	= $okr_kr_array['importance'];
        $difficulty_val 	= $okr_kr_array['difficulty'];
        $challenge_val  	= $okr_kr_array['challenge'];

        $quantity   = ($importance_val*2*3)+($difficulty_val*2*3)+($challenge_val*2*4);
        $grade_val  = floor($quantity/10);
        $grade 		= "F";
        switch($grade_val)
        {
			case 10: case 9: $grade="S"; break;
			case 8: $grade="A"; break;
			case 7: $grade="B"; break;
			case 6: $grade="C"; break;
			case 5: $grade="D"; break;
			case 4: $grade="E"; break;
			case 3: case 2: case 1: $grade="F"; break;
			default: $grade = "F";
        }


        $complete_rate_val  = $okr_kr_array['complete_rate'];
        $purpose_quality += round((($importance_val*0.3) + ($difficulty_val*0.3) + ($challenge_val*0.4)), 1);

        if($complete_rate_val > 0){
            $complete_rate = $complete_rate_val/100;
            $complete_quality += round((($importance_val*0.3*$complete_rate) + ($difficulty_val*0.3*$complete_rate) + ($challenge_val*0.4*$complete_rate)), 1);
        }

        if($f_priority_new <= $okr_kr_array['priority']) {
            $f_priority_new = $okr_kr_array['priority']+1;
        }

		$okr_kr[] = array(
			"ok_no"=>$okr_kr_array['ok_no'],
			"oo_no"=>$okr_kr_array['oo_no'],
			"kr_name"=>$okr_kr_array['kr_name'],
			"complete_rate"=>$okr_kr_array['complete_rate'],
			"kr_reason"=>htmlspecialchars($okr_kr_array['kr_reason']),
			"importance"=>$okr_kr_array['importance'],
			"difficulty"=>$okr_kr_array['difficulty'],
			"challenge"=>$okr_kr_array['challenge'],
			"grade" => $grade,
			"quality"=>$quantity,
			"kr_result"=>htmlspecialchars($okr_kr_array['kr_result']),
            "priority"=>$okr_kr_array['priority']
		);
	}

	$avg_complete_rate = ($complete_quality > 0 && $purpose_quality > 0) ? ($complete_quality/$purpose_quality)*100 : 0;

	$smarty->assign($okr_obj);
	$smarty->assign("purpose_quality", sprintf('%0.1f',$purpose_quality));
	$smarty->assign("complete_quality", sprintf('%0.1f',$complete_quality));
	$smarty->assign("avg_complete_rate", sprintf('%0.2f',$avg_complete_rate));
	$smarty->assign("okr_kr", $okr_kr);
    $smarty->assign("f_priority_new", $f_priority_new);

    $team_where = "s.team = '{$okr_obj['team']}'";
    if($okr_obj['team'] == '00221'){
		$team_where = "s.team IN('{$okr_obj['team']}', '00236')";
	}
    $staff_data_sql  	= "SELECT * FROM staff s WHERE {$team_where} AND staff_state='1' ORDER BY s_no";
    $staff_data_query 	= mysqli_query($my_db, $staff_data_sql);
    $staff_data_list 	= [];
	while($staff_data = mysqli_fetch_assoc($staff_data_query))
	{
		$staff_data_list[$staff_data['s_no']] = $staff_data;
	}
	$smarty->assign("staff_data_list", $staff_data_list);

} else if ($proc=="write") {
	$add_set = "`type`='base',";

	if(!empty($_POST['f_state'])){
		$add_set.="state = '".$_POST['f_state']."',";
	}

	if(!empty($_POST['f_sdate'])){
		$add_set.="sdate = '".$_POST['f_sdate']."',";
	}else if(empty($_POST['f_sdate'])){
		$add_set.="sdate = NULL,";
	}
	if(!empty($_POST['f_edate'])){
		$add_set.="edate = '".$_POST['f_edate']."',";
	}else if(empty($_POST['f_edate'])){
		$add_set.="edate = NULL,";
	}

	if(!empty($_POST['f_kind'])){
		$add_set.="kind = '".$_POST['f_kind']."',";
	}

	if(!empty($_POST['f_obj_name'])){
		$add_set.="obj_name = '".addslashes($_POST['f_obj_name'])."',";
	}

	if(!empty($_POST['f_s_no'])){
		$add_set .= "s_no = '".$_POST['f_s_no']."',";
		$add_set .= "team = '".$_POST['f_team']."'";
	}

	$sql = "INSERT INTO okr_obj SET {$add_set}";

	if (mysqli_query($my_db, $sql)){
		$result= mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
		$okr_obj_array=mysqli_fetch_array($result);
		$last_oo_no = $okr_obj_array[0];

		exit("<script>location.href='okr_regist.php?oo_no=$last_oo_no';</script>");
	} else {
		exit("<script>alert('등록에 실패 하였습니다');location.href='okr_regist.php';</script>");
	}

// form action 이 수정일 경우
} elseif($proc=="modify") {
	$supply_cost_value = str_replace(",","",trim($_POST['f_supply_cost'])); // 컴마 제거하기
	$supply_cost_vat_value = str_replace(",","",trim($_POST['f_supply_cost_vat'])); // 컴마 제거하기
	$cost_value = str_replace(",","",trim($_POST['f_cost'])); // 컴마 제거하기
	$wd_money_value = str_replace(",","",trim($_POST['f_wd_money'])); // 컴마 제거하기

	$add_set = "";

	if(!empty($_POST['f_wd_date'])){
		$add_set.="wd_date = '".$_POST['f_wd_date']."',";
	}else if(empty($_POST['f_wd_date'])){
		$add_set.="wd_date = NULL,";
	}
	if(!empty($_POST['f_incentive_date'])){
		$add_set.="incentive_date = '".$_POST['f_incentive_date']."',";
	}
	if(!empty($_POST['f_cost_info'])){
		$add_set.="cost_info = '".addslashes($_POST['f_cost_info'])."',";
	}
	if(!empty($_POST['f_wd_tax_date'])){
		$add_set.="wd_tax_date = '".$_POST['f_wd_tax_date']."',";
	}else if(empty($_POST['f_wd_tax_date'])){
		$add_set.="wd_tax_date = NULL,";
	}
	if(!empty($_POST['f_wd_tax_item'])){
		$add_set.="wd_tax_item = '".addslashes($_POST['f_wd_tax_item'])."',";
	}

	$files=$_FILES["file"];
	if($files){
		image_check($files);
		$save_name=store_image($files, "evaluation");

		if(!empty($save_name[0])) {
			$add_set.="file_read='".$save_name[0]."', ";
		}
		if(!empty($_FILES["file"]["name"][0])) {
			$add_set.="file_origin='".addslashes($_FILES["file"]["name"][0])."', ";
		}
	}

	$sql="UPDATE evaluation SET
				wd_subject = '".addslashes($_POST['f_wd_subject'])."',
				wd_state = '".$_POST['f_wd_state']."',
				wd_method = '".$_POST['f_wd_method']."',
				c_no = '".$_POST['f_c_no']."',
				c_name = '".addslashes($_POST['f_c_name'])."',
				s_no = '".$_POST['s_no']."',
				supply_cost = '".$supply_cost_value."',
				vat_choice = '".$_POST['f_vat_choice']."',
				supply_cost_vat = '".$supply_cost_vat_value."',
				cost = '".$cost_value."',
				wd_money = '".$wd_money_value."',
				".$add_set."
				wd_tax_state = '".$_POST['f_wd_tax_state']."'
		WHERE
			oo_no = '".$_POST['oo_no']."'
		";
	//echo "<br><br><br>".$sql;exit;
	$my_db->query($sql);

	alert("수정하였습니다.","okr_list.php");

}elseif($proc=='del_okr_all')
{
	$oo_no = isset($_POST['oo_no']) ? $_POST['oo_no'] : "";
	$team  = isset($_POST['team']) ? $_POST['team'] : "";

	if($state == '1' && ($session_team == '00211' || (($session_team == $team && ($session_pos_position == "2" || $session_pos_position == "3" || $session_pos_position == "4")) || permissionNameCheck($session_permission, "대표"))))
	{
		if($oo_no)
		{
			$oo_del_sql  = "UPDATE okr_obj SET display=2 WHERE oo_no = '$oo_no'";
			if(!mysqli_query($my_db, $oo_del_sql)) {
				exit("<script>alert('oo_no = {$oo_no} 삭제에 실패 하였습니다.');location.href='okr_regist.php?oo_no=$oo_no';</script>");
			}else{
				alert("삭제하였습니다","okr_list.php");
			}
		}
	}else{
		exit("<script>alert('권한이 없습니다.');location.href='okr_regist.php?oo_no=$oo_no';</script>");
	}

	exit("<script>alert('해당 OKR이 없습니다');location.href='okr_list.php';</script>");
}


#팀 리스트
$f_team_list 		= [];
$f_team_list_sql 	= "SELECT team_list FROM staff WHERE s_no='{$okr_s_no}'";
$f_team_list_query  = mysqli_query($my_db, $f_team_list_sql);
$f_team_list_result = mysqli_fetch_assoc($f_team_list_query);
$f_team_list_val    = isset($f_team_list_result['team_list']) ? explode(',',$f_team_list_result['team_list']) : array($session_team);

$f_team_codes = implode(",", $f_team_list_val);
$f_team_sql 	= "SELECT * FROM team WHERE team_code IN({$f_team_codes})";
$f_team_query 	= mysqli_query($my_db, $f_team_sql);
while($f_team_result = mysqli_fetch_assoc($f_team_query))
{
    $f_team_list[$f_team_result['team_code']] = $f_team_result['team_name'];
}
$smarty->assign('f_team_list', $f_team_list);
$smarty->assign('f_team_count', count($f_team_list));

$smarty->assign('date_list', $date_list);

// 템플릿에 해당 입력값 던져주기
$smarty->assign(
	array(
		"proc"=>$proc,
		"mode"=>$mode,
		"idx"=>$idx
	)
);
$smarty->display('okr_regist.html');

?>
