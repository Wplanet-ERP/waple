<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/work_cms.php');
require('inc/helper/work_return.php');
require('inc/helper/deposit.php');
require('inc/helper/withdraw.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Deposit.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');
require('inc/model/Staff.php');
require('inc/model/Work.php');
require('inc/model/WorkCms.php');

$is_freelancer  = false;
if($session_staff_state == '2'){ // 프리랜서의 경우 기본 접근제한으로 설정
    $is_freelancer = true;
}
$smarty->assign("is_freelancer", $is_freelancer);

# Model Init
$cms_model  = WorkCms::Factory();

# Process 처리
$process = (isset($_POST['process']))?$_POST['process']:"";

if($process == "mod_cs_issue")
{
    $w_no    = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $value   = (isset($_POST['val'])) ? $_POST['val'] : "";
    $upd_sql = "UPDATE work_cms w SET w.cs_issue='{$value}' WHERE w.w_no = '{$w_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "이슈 저장에 실패 하였습니다.";
    else
        echo "이슈 데이터가 저장 되었습니다.";
    exit;
}
elseif($process == "new_work_cms")
{
    $search_url       = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $ord_no           = isset($_POST['new_ord_number']) ? $_POST['new_ord_number'] : "";
    $ord_date         = isset($_POST['new_ord_date']) ? $_POST['new_ord_number'] : "";
    $recipient        = isset($_POST['new_recipient']) ? $_POST['new_recipient'] : "";
    $recipient_hp     = isset($_POST['new_recipient_hp']) ? $_POST['new_recipient_hp'] : "";
    $zip_code         = isset($_POST['new_zipcode']) ? $_POST['new_zipcode'] : "";
    $recipient_addr1  = isset($_POST['new_recipient_addr1']) ? $_POST['new_recipient_addr1'] : "";
    $recipient_addr2  = isset($_POST['new_recipient_addr2']) ? $_POST['new_recipient_addr2'] : "";
    $recipient_addr   = $recipient_addr1." ".$recipient_addr2;
    $delivery_memo    = isset($_POST['new_delivery_memo']) ? $_POST['new_delivery_memo'] : "";
    $dp_c_no          = isset($_POST['new_dp_c_no']) ? $_POST['new_dp_c_no'] : "";
    $dp_c_name        = "";
    $manager_memo     = isset($_POST['new_manager_memo']) ? $_POST['new_manager_memo'] : "";
    $regdate          = date('Y-m-d H:i:s');
    $prd_no           = isset($_POST['new_prd_no']) ? $_POST['new_prd_no'] : "";
    $quantities       = isset($_POST['new_quantity']) ? $_POST['new_quantity'] : "";
    $prices           = isset($_POST['new_price']) ? $_POST['new_price'] : "";
    $notices          = isset($_POST['new_notice']) ? $_POST['new_notice'] : "";
    $prd_list         = [];
    $chk_shop_list    = [];
    $result           = false;

    $ord_date  = date('Y-m-d');
    $cur_date  = date('Ymd');

    $last_cs_ord_no = $cms_model->getLastCsNo();
    $ord_no         = $cur_date."_CS_".sprintf('%04d', $last_cs_ord_no+1);

    if($dp_c_no){
        $dp_c_name_sql    = "SELECT c_name FROM company where c_no = '{$dp_c_no}'";
        $dp_c_name_query  = mysqli_query($my_db, $dp_c_name_sql);
        $dp_c_name_result = mysqli_fetch_assoc($dp_c_name_query);
        $dp_c_name        = isset($dp_c_name_result['c_name']) ? $dp_c_name_result['c_name'] : "";
    }

    $init_insert_data = array(
        "delivery_state"    => ($dp_c_no == 2005) ? 10 : 4,
        "order_number"      => $ord_no,
        "order_date"        => $regdate,
        "stock_date"        => $regdate,
        "zip_code"          => $zip_code,
        "recipient"         => $recipient,
        "recipient_addr"    => $recipient_addr,
        "recipient_hp"      => $recipient_hp,
        "delivery_memo"     => $delivery_memo,
        "dp_c_no"           => $dp_c_no,
        "dp_c_name"         => $dp_c_name,
        "manager_memo"      => $manager_memo,
        "regdate"           => $regdate,
        "write_date"        => $regdate,
        "task_req_s_no"     => $session_s_no,
        "task_run_s_no"     => $session_s_no,
        "task_run_regdate"  => $regdate,
        "task_run_team"     => $session_team
    );

    $add_deli_add_deposit = isset($_POST['add_deli_add_deposit']) ? $_POST['add_deli_add_deposit'] : "";
    $add_deposit_name     = isset($_POST['add_deposit_name']) ? addslashes(trim($_POST['add_deposit_name'])) : "";
    $add_deposit_price    = isset($_POST['add_deposit_price']) ? str_replace(',','',$_POST['add_deposit_price']) : "";
    $chk_total_price      = 0;

    // 상품 정리
    if(!empty($prd_no))
    {
        for($i=0; $i<count($prd_no); $i++)
        {
            if(isset($prd_no[$i]) && !empty($prd_no[$i]))
            {
                $quantity   = isset($quantities[$i]) ? $quantities[$i] : 1;
                $price      = isset($prices[$i]) ? str_replace(",","",$prices[$i]) : 0;
                $notice     = isset($notices[$i]) ? $notices[$i] : '';
                $prd_list[] = array(
                    'prd_no'    => $prd_no[$i],
                    'quantity'  => $quantity,
                    'price'     => $price,
                    'notice'    => $notice
                );

                $chk_total_price += $price;
            }
        }

        if($prd_list)
        {
            $insert_data = [];

            $chk_prd_price  = $add_deposit_price;
            $chk_prd_cnt    = count($prd_list);
            $prd_idx        = 1;

            foreach($prd_list as $prd_data)
            {
                $p_no            = $prd_data['prd_no'];
                $new_insert_data = $init_insert_data;
                if($p_no)
                {
                    $prd_sql        = "SELECT prd_cms.manager, (SELECT s.team FROM staff s WHERE s.s_no = prd_cms.manager) as team, prd_cms.c_no, (SELECT c.c_name FROM company c WHERE c.c_no = prd_cms.c_no) as c_name FROM product_cms prd_cms WHERE prd_cms.prd_no = '{$p_no}' ORDER BY prd_cms.prd_no DESC LIMIT 1";
                    $prd_query      = mysqli_query($my_db, $prd_sql);
                    $prd_result     = mysqli_fetch_assoc($prd_query);
                    $s_no           = isset($prd_result['manager']) ? $prd_result['manager'] : 0;
                    $team           = isset($prd_result['team']) ? $prd_result['team'] : 0;
                    $c_no           = isset($prd_result['c_no']) ? $prd_result['c_no'] : 0;
                    $c_name         = isset($prd_result['c_name']) ? $prd_result['c_name'] : "";
                    $quantity       = $prd_data['quantity'];
                    $notice         = $prd_data['notice'];
                    $new_shop_no    = "W".$ord_no."_".$p_no;

                    if(isset($chk_shop_list[$new_shop_no])){
                        $chk_shop_list[$new_shop_no]++;
                        $chk_value       = $chk_shop_list[$new_shop_no];
                        $new_shop_no    .= "_".$chk_value;
                    }else{
                        $chk_shop_list[$new_shop_no] = 1;
                    }

                    if($prd_idx == '1'){
                        $first_brand        = $c_no;
                        $first_brand_name   = $c_name;
                        $first_brand_staff  = $s_no;
                        $first_brand_team   = $team;
                    }

                    if($chk_total_price > 0)
                    {
                        $cal_prd_per    = $prd_data['price']/$chk_total_price;
                        $cal_price      = round($add_deposit_price*$cal_prd_per, -1);
                        $chk_prd_price -= $cal_price;

                        if($chk_prd_cnt == $prd_idx){
                            $cal_price += $chk_prd_price;
                        }

                        $dp_price       = (int)$cal_price / 1.1;
                        $dp_price       = round($dp_price, -1);
                        $dp_price_vat   = (int)$cal_price;
                    }
                    else{
                        $dp_price_vat = 0;
                        $dp_price     = 0;
                    }

                    $new_insert_data['unit_price']      = $dp_price_vat;
                    $new_insert_data['dp_price']        = $dp_price;
                    $new_insert_data['dp_price_vat']    = $dp_price_vat;

                    $new_insert_data['shop_ord_no']     = $new_shop_no;
                    $new_insert_data['s_no']            = $s_no;
                    $new_insert_data['team']            = $team;
                    $new_insert_data['c_no']            = $c_no;
                    $new_insert_data['c_name']          = $c_name;
                    $new_insert_data['prd_no']          = $p_no;
                    $new_insert_data['quantity']        = $quantity;
                    $new_insert_data['notice']          = addslashes($notice);

                    $insert_data[] = $new_insert_data;
                }else{
                    $result = false;
                }

                $prd_idx++;
            }

            if(!$cms_model->multiInsert($insert_data)){
                exit("<script>alert('등록 실패했습니다');location.href='work_list_cs.php?{$search_url}';</script>");
            }else{
                if($add_deli_add_deposit == '1')
                {
                    $new_task_req         = "주문번호 : {$ord_no}\r\n입금자명 : {$add_deposit_name}\r\n금액(택배비 포함가) : {$add_deposit_price}";
                    $work_dp_c_no         = "4026";
                    $work_dp_c_name       = "커머스 고객";
                    $work_prd_no          = "260";

                    $work_model  = Work::Factory();
                    $work_data   = array(
                        "work_state"        => 6,
                        "c_no"              => $first_brand,
                        "c_name"            => $first_brand_name,
                        "s_no"              => $first_brand_staff,
                        "team"              => $first_brand_team,
                        "prd_no"            => $work_prd_no,
                        "quantity"          => "1",
                        "regdate"           => date('Y-m-d H:i:s'),
                        "dp_price"          => $add_deposit_price,
                        "dp_price_vat"      => $add_deposit_price,
                        "dp_c_no"           => $work_dp_c_no,
                        "dp_c_name"         => $work_dp_c_name,
                        "task_req"          => addslashes(trim($new_task_req)),
                        "task_req_s_no"     => $session_s_no,
                        "task_req_team"     => $session_team,
                        "task_run_s_no"     => $session_s_no,
                        "task_run_team"     => $session_team,
                        "task_run_regdate"  => $regdate,
                        "linked_no"         => 0,
                        "linked_table"      => "work_cms",
                        "linked_shop_no"    => $ord_no
                    );

                    if ($work_model->insert($work_data))
                    {
                        $add_w_no           = $work_model->getInsertId();
                        $add_deposit_chk    = isset($_POST['add_deposit_chk']) ? addslashes(trim($_POST['add_deposit_chk'])) : "3";
                        $dp_tax_state       = ($add_deposit_chk == "3") ? "4" : "1";

                        $deposit_model      = Deposit::Factory();
                        $deposit_data       = array(
                            "my_c_no"         => '3',
                            "dp_subject"      => '커머스 입금',
                            "dp_state"        => '1',
                            "dp_method"       => '2',
                            "dp_account"      => '2',
                            "bk_name"         => $add_deposit_name,
                            "w_no"            => $add_w_no,
                            "c_no"            => $work_dp_c_no,
                            "c_name"          => $work_dp_c_name,
                            "s_no"            => $session_s_no,
                            "team"            => $session_team,
                            "supply_cost"     => $add_deposit_price,
                            "vat_choice"      => '3',
                            "supply_cost_vat" => '0',
                            "cost"            => $add_deposit_price,
                            "regdate"         => date('Y-m-d H:i:s'),
                            "dp_tax_state"    => $dp_tax_state,
                            "reg_s_no"        => $session_s_no,
                            "reg_team"        => $session_team
                        );

                        if($add_deposit_chk == '1')
                        {
                            $add_deposit_chk_name   = isset($_POST['add_deposit_chk_name']) ? trim($_POST['add_deposit_chk_name']) : "";
                            $add_deposit_chk_num    = isset($_POST['add_deposit_chk_num']) ? trim($_POST['add_deposit_chk_num']) : "";
                            $add_deposit_cost_info  = "[커머스 고객 계산서 발행정보]\r\n성함 : {$add_deposit_chk_name}\r\n휴대전화 또는 사업자번호 : {$add_deposit_chk_num}";

                            $deposit_data['cost_info'] = addslashes(trim($add_deposit_cost_info));
                        }
                        elseif($add_deposit_chk == '2')
                        {
                            $add_deposit_chk_email  = isset($_POST['add_deposit_chk_email']) ? trim($_POST['add_deposit_chk_email']) : "";
                            $add_deposit_cost_info  = "[커머스 고객 계산서 발행정보]\r\n이메일 : {$add_deposit_chk_email}";

                            $add_deposit_chk_file   = $_FILES['add_deposit_chk_file'];
                            if($add_deposit_chk_file)
                            {
                                if(isset($add_deposit_chk_file['name']) && !empty($add_deposit_chk_file['name'])){
                                    $file_read          = add_store_file($add_deposit_chk_file, "deposit");
                                    $file_origin        = $add_deposit_chk_file['name'];

                                    $add_deposit_cost_info      .= "\r\n(파일첨부에 저장)";
                                    $deposit_data['file_read']   = $file_read;
                                    $deposit_data['file_origin'] = addslashes(trim($file_origin));
                                }
                            }
                            $deposit_data['cost_info'] = addslashes(trim($add_deposit_cost_info));
                        }

                        if ($deposit_model->insert($deposit_data))
                        {
                            $add_dp_no = $deposit_model->getInsertId();
                            $work_model->update(array("w_no" => $add_w_no, 'dp_no' => $add_dp_no));

                            $cms_model->updateToArray(array('is_deposit' => 1), array("order_number" => $ord_no));
                        }else{
                            echo "에러입니다!";
                            echo json_encode($deposit_data);
                            die;
                        }
                    }
                }
                $result = true;
            }
        }else{
            $result = false;
        }
    }

    if($result){
        exit("<script>alert('등록 성공했습니다');location.href='work_list_cs.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('등록 실패했습니다');location.href='work_list_cs.php?{$search_url}';</script>");
    }
}
elseif($process == "add_refund_work")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $new_w_no       = isset($_POST['new_w_no']) ? $_POST['new_w_no'] : "";
    $new_c_no       = isset($_POST['new_c_no']) ? $_POST['new_c_no'] : "";
    $new_bankname   = isset($_POST['new_bankname']) ? addslashes(trim($_POST['new_bankname'])) : "";
    $new_accname    = isset($_POST['new_accname']) ? addslashes(trim($_POST['new_accname'])) : "";
    $new_accnum     = isset($_POST['new_accnum']) ? trim($_POST['new_accnum']) : "";
    $new_price      = isset($_POST['new_price']) ? str_replace(",", "", $_POST['new_price']) : "";
    $new_reason     = isset($_POST['new_reason']) ? addslashes(trim($_POST['new_reason'])) : "";

    if(empty($new_w_no))
    {
        exit("<script>alert('등록 실패했습니다. 업무번호가 없습니다');location.href='work_list_cs.php?{$search_url}';</script>");
    }

    $chk_order = $cms_model->getItem($new_w_no);

    if(empty($chk_order['order_number']))
    {
        exit("<script>alert('등록 실패했습니다. 해당 주문이 없습니다');location.href='work_list_cs.php?{$search_url}';</script>");
    }

    $new_ord_no  = $chk_order['order_number'];
    $new_shop_no = $chk_order['shop_ord_no'];
    if(empty($new_shop_no))
    {
        $new_shop_no = "W{$new_ord_no}_{$chk_order['prd_no']}";
        $cms_model->update(array("w_no" => $new_w_no, "shop_ord_no" => $new_shop_no));
    }

    $new_task_req   = "주문번호 : {$new_ord_no}\r\n쇼핑몰(상품)주문번호: {$new_shop_no}\r\n은행 : {$new_bankname}\r\n계좌번호 : {$new_accnum}\r\n예금주 : {$new_accname}\r\n금액 : {$new_price}";

    $comp_no    = "";
    $comp_name  = "";
    $comp_s_no  = "";
    $comp_team  = "";

    if (!empty($new_c_no))
    {
        $new_comp_sql = "SELECT c_no, c_name, (SELECT s.team FROM staff s WHERE s.s_no=c.s_no) as team, s_no FROM company c where c_no = '{$new_c_no}'";
        $new_comp_query = mysqli_query($my_db, $new_comp_sql);
        $new_comp_result = mysqli_fetch_assoc($new_comp_query);

        $comp_no    = isset($new_comp_result['c_no']) ? $new_comp_result['c_no'] : "";
        $comp_name  = isset($new_comp_result['c_name']) ? $new_comp_result['c_name'] : "";
        $comp_s_no  = isset($new_comp_result['s_no']) ? $new_comp_result['s_no'] : "";
        $comp_team  = isset($new_comp_result['team']) ? $new_comp_result['team'] : "";
    }

    if(!empty($new_reason))
    {
        $new_task_req .= "\r\n사유 : {$new_reason}";
    }

    // 업무 처리자 고정
    $wd_c_no         = "4026";
    $wd_c_name       = "커머스 고객";
    $prd_no          = "229";
    $task_run_s_no   = '146'; // 업무처리 담당자 (최지욱)
    $task_run_s_team = '00244'; // 업무처리 담당자 팀 (미디어 커머스 CS팀)
    $regdate         = date('Y-m-d H:i:s');

    $work_model  = Work::Factory();
    $insert_data = array(
        "work_state"        => 3,
        "c_no"              => $comp_no,
        "c_name"            => $comp_name,
        "s_no"              => $comp_s_no,
        "team"              => $comp_team,
        "prd_no"            => $prd_no,
        "quantity"          => '1',
        "regdate"           => $regdate,
        "wd_price"          => $new_price,
        "wd_price_vat"      => $new_price,
        "wd_c_no"           => $wd_c_no,
        "wd_c_name"         => $wd_c_name,
        "task_req"          => addslashes($new_task_req),
        "task_req_s_no"     => $session_s_no,
        "task_req_team"     => $session_team,
        "task_run_s_no"     => $task_run_s_no,
        "task_run_team"     => $task_run_s_team,
        "linked_no"         => $new_w_no,
        "linked_table"      => 'work_cms',
        "linked_shop_no"    => $new_shop_no
    );

    if ($work_model->insert($insert_data))
    {
        $upd_ord_sql = "UPDATE work_cms SET is_refund='1' WHERE w_no='{$new_w_no}'";
        $cms_model->update(array("w_no" => $new_w_no, "is_refund" => '1'));

        exit("<script>alert('등록 성공했습니다');location.href='work_list_cs.php?{$search_url}';</script>");
    } else {
        exit("<script>alert('등록 실패했습니다');location.href='work_list_cs.php?{$search_url}';</script>");
    }
}
elseif($process == "add_deposit_work")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $new_ord_no         = isset($_POST['new_ord_no']) ? $_POST['new_ord_no'] : "";

    $new_deposit_name   = isset($_POST['new_deposit_name']) ? addslashes(trim($_POST['new_deposit_name'])) : "";
    $new_deposit_price  = isset($_POST['new_deposit_price']) ? str_replace(',','',$_POST['new_deposit_price']) : "";
    $new_task_req       = "주문번호 : {$new_ord_no}\r\n입금자명 : {$new_deposit_name}\r\n금액(택배비 포함가) : {$new_deposit_price}";

    if(empty($new_ord_no))
    {
        exit("<script>alert('등록 실패했습니다. 주문번호가 없습니다');location.href='work_list_cs.php?{$search_url}';</script>");
    }

    $cms_order  = $cms_model->getWhereItem("order_number='{$new_ord_no}' LIMIT 1");
    if(!isset($cms_order['order_number']))
    {
        exit("<script>alert('등록 실패했습니다. 해당 주문이 없습니다');location.href='work_list_cs.php?{$search_url}';</script>");
    }

    $comp_no    = $cms_order['c_no'];
    $comp_name  = $cms_order['c_name'];
    $comp_s_no  = $cms_order['s_no'];
    $comp_team  = $cms_order['team'];

    // 업무 처리자 고정
    $dp_c_no         = "4026";
    $dp_c_name       = "커머스 고객";
    $prd_no          = "260";
    $regdate         = date('Y-m-d H:i:s');

    $work_model  = Work::Factory();
    $work_data   = array(
        "work_state"        => 6,
        "c_no"              => "{$comp_no}",
        "c_name"            => "{$comp_name}",
        "s_no"              => "{$comp_s_no}",
        "team"              => "{$comp_team}",
        "prd_no"            => $prd_no,
        "quantity"          => "1",
        "regdate"           => $regdate,
        "dp_price"          => $new_deposit_price,
        "dp_price_vat"      => $new_deposit_price,
        "dp_c_no"           => $dp_c_no,
        "dp_c_name"         => $dp_c_name,
        "task_req"          => addslashes(trim($new_task_req)),
        "task_req_s_no"     => $session_s_no,
        "task_req_team"     => $session_team,
        "task_run_s_no"     => $session_s_no,
        "task_run_team"     => $session_team,
        "task_run_regdate"  => $regdate,
        "linked_no"         => 0,
        "linked_table"      => "work_cms",
        "linked_shop_no"    => $new_ord_no
    );


    if ($work_model->insert($work_data))
    {
        $new_w_no           = $work_model->getInsertId();
        $new_deposit_chk    = isset($_POST['new_deposit_chk']) ? addslashes(trim($_POST['new_deposit_chk'])) : "3";
        $dp_tax_state       = ($new_deposit_chk == "3") ? "4" : "1";

        $deposit_model      = Deposit::Factory();
        $deposit_data       = array(
            "my_c_no"         => '3',
            "dp_subject"      => '커머스 입금',
            "dp_state"        => '1',
            "dp_method"       => '2',
            "dp_account"      => '2',
            "bk_name"         => $new_deposit_name,
            "w_no"            => $new_w_no,
            "c_no"            => $dp_c_no,
            "c_name"          => $dp_c_name,
            "s_no"            => $session_s_no,
            "team"            => $session_team,
            "supply_cost"     => $new_deposit_price,
            "vat_choice"      => '3',
            "supply_cost_vat" => '0',
            "cost"            => $new_deposit_price,
            "regdate"         => date('Y-m-d H:i:s'),
            "dp_tax_state"    => $dp_tax_state,
            "reg_s_no"        => $session_s_no,
            "reg_team"        => $session_team
        );

        if($new_deposit_chk == '1')
        {
            $new_deposit_chk_name   = isset($_POST['new_deposit_chk_name']) ? trim($_POST['new_deposit_chk_name']) : "";
            $new_deposit_chk_num    = isset($_POST['new_deposit_chk_num']) ? trim($_POST['new_deposit_chk_num']) : "";
            $new_deposit_cost_info  = "[커머스 고객 계산서 발행정보]\r\n성함 : {$new_deposit_chk_name}\r\n휴대전화 또는 사업자번호 : {$new_deposit_chk_num}";

            $deposit_data['cost_info'] = addslashes(trim($new_deposit_cost_info));
        }
        elseif($new_deposit_chk == '2')
        {
            $new_deposit_chk_email  = isset($_POST['new_deposit_chk_email']) ? trim($_POST['new_deposit_chk_email']) : "";
            $new_deposit_cost_info  = "[커머스 고객 계산서 발행정보]\r\n이메일 : {$new_deposit_chk_email}";

            $new_deposit_chk_file   = $_FILES['new_deposit_chk_file'];
            if($new_deposit_chk_file)
            {
                if(isset($new_deposit_chk_file['name']) && !empty($new_deposit_chk_file['name'])){
                    $file_read          = add_store_file($new_deposit_chk_file, "deposit");
                    $file_origin        = $new_deposit_chk_file['name'];

                    $new_deposit_cost_info      .= "\r\n(파일첨부에 저장)";
                    $deposit_data['file_read']   = $file_read;
                    $deposit_data['file_origin'] = addslashes(trim($file_origin));
                }
            }
            $deposit_data['cost_info'] = addslashes(trim($new_deposit_cost_info));
        }

        if ($deposit_model->insert($deposit_data))
        {
            $new_dp_no = $deposit_model->getInsertId();
            $work_model->update(array("w_no" => $new_w_no, 'dp_no' => $new_dp_no));

            $cms_model->updateToArray(array('is_deposit' => 1), array("order_number" => $new_ord_no));
        }

        exit("<script>alert('등록 성공했습니다');location.href='work_list_cs.php?{$search_url}';</script>");
    } else {
        exit("<script>alert('등록 실패했습니다');location.href='work_list_cs.php?{$search_url}';</script>");
    }
}
elseif($process == "change_total_manager")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $chk_ord_list   = isset($_POST['chk_ord_list']) ? $_POST['chk_ord_list'] : "";
    $chk_manager    = isset($_POST['chk_manager']) ? $_POST['chk_manager'] : "";

    $change_sql     = "";
    if(!empty($chk_ord_list))
    {
        $chk_ord_val = explode(',', $chk_ord_list);
        foreach($chk_ord_val as $chk_ord)
        {
            $chk_sql    = "SELECT * FROM work_cs_management WHERE order_number='{$chk_ord}'";
            $chk_query  = mysqli_query($my_db, $chk_sql);
            $chk_result = mysqli_fetch_assoc($chk_query);

            if(isset($chk_result['cs_no']) && !empty($chk_result['cs_no'])) {
                $change_sql .= "UPDATE work_cs_management SET cs_s_no='{$chk_manager}' WHERE cs_no='{$chk_result['cs_no']}';";
            }else{
                $change_sql .= "INSERT INTO work_cs_management SET `order_number`='{$chk_ord}', cs_s_no='{$chk_manager}';";
            }
        }

    }else{
        exit("<script>alert('선택된 주문이 없습니다.');location.href='work_list_cs.php?{$search_url}';</script>");
    }

    if (mysqli_multi_query($my_db, $change_sql) == true) {
        exit("<script>alert('CS 담당자를 수정했습니다.');location.href='work_list_cs.php?{$search_url}';</script>");
    } else {
        exit("<script>alert('CS 담당자를 변경에 실패했습니다.');location.href='work_list_cs.php?{$search_url}';</script>");
    }
}
elseif($process == "mod_cs_manager")
{
    $ord_no	 = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 	 = (isset($_POST['val'])) ? $_POST['val'] : "";

    $chk_sql    = "SELECT * FROM work_cs_management WHERE order_number='{$ord_no}'";
    $chk_query  = mysqli_query($my_db, $chk_sql);
    $chk_result = mysqli_fetch_assoc($chk_query);

    if(isset($chk_result['cs_no']) && !empty($chk_result['cs_no'])) {
        $upd_sql = "UPDATE work_cs_management SET cs_s_no='{$value}' WHERE cs_no = '{$chk_result['cs_no']}'";
    }else{
        $upd_sql = "INSERT INTO work_cs_management SET `order_number`='{$ord_no}', cs_s_no='{$value}'";
    }

    if (!mysqli_query($my_db, $upd_sql))
        echo "CS 담당자 저장에 실패했습니다.";
    else
        echo "CS 담당자를 저장했습니다.";
    exit;
}
elseif($process == "mod_cs_status")
{
    $ord_no	 = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 	 = (isset($_POST['val'])) ? $_POST['val'] : "";

    $chk_sql    = "SELECT * FROM work_cs_management WHERE order_number='{$ord_no}'";
    $chk_query  = mysqli_query($my_db, $chk_sql);
    $chk_result = mysqli_fetch_assoc($chk_query);

    if(isset($chk_result['cs_no']) && !empty($chk_result['cs_no'])) {
        $upd_sql = "UPDATE work_cs_management SET cs_status='{$value}' WHERE cs_no = '{$chk_result['cs_no']}'";
    }else{
        $upd_sql = "INSERT INTO work_cs_management SET `order_number`='{$ord_no}', cs_status='{$value}'";
    }

    if (!mysqli_query($my_db, $upd_sql))
        echo "CS 진행중 저장에 실패했습니다.";
    else
        echo "CS 진행중 상태값을 저장했습니다.";
    exit;
}
else
{
    # Navigation & My Quick
    $nav_prd_no  = "33";
    $nav_title   = "커머스 C/S 리스트";
    $quick_model = MyQuick::Factory();
    $is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

    $smarty->assign("is_my_quick", $is_my_quick);
    $smarty->assign("nav_title", $nav_title);
    $smarty->assign("nav_prd_no", $nav_prd_no);


    $add_where = "1=1";

    // [상품(업무) 종류]
    $sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
    $sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
    $sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
    $sch_get	= isset($_GET['sch'])?$_GET['sch']:"";

    $smarty->assign("sch_prd_g1", $sch_prd_g1);
    $smarty->assign("sch_prd_g2", $sch_prd_g2);
    $smarty->assign("sch_prd", $sch_prd);

    if(!empty($sch_get)) {
        $smarty->assign("sch", $sch_get);
    }

    $kind_model     = Kind::Factory();
    $product_model  = ProductCms::Factory();
    $cms_code       = "product_cms";
    $cms_group_list = $kind_model->getKindGroupList($cms_code);
    $prd_total_list = $product_model->getPrdGroupData();
    $prd_g1_list = $prd_g2_list = $prd_g3_list = [];

    foreach($cms_group_list as $key => $prd_data)
    {
        if(!$key){
            $prd_g1_list = $prd_data;
        }else{
            $prd_g2_list[$key] = $prd_data;
        }
    }

    $prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
    $sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

    $smarty->assign("prd_g1_list", $prd_g1_list);
    $smarty->assign("prd_g2_list", $prd_g2_list);
    $smarty->assign("sch_prd_list", $sch_prd_list);

    // 상품(업무) 종류
    if (!empty($sch_prd) && $sch_prd != "0") { // 상품
        $add_where .= " AND w.prd_no='{$sch_prd}'";
    }else{
        if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
            $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
        }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
            $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
        }
    }

    $today_val  = date('Y-m-d');
    $week_val   = date('Y-m-d', strtotime('-1 weeks'));
    $month_val  = date('Y-m-d', strtotime('-1 months'));
    $months_val = date('Y-m-d', strtotime('-3 months'));
    $year_val   = date('Y-m-d', strtotime('-1 years'));
    $years_val  = date('Y-m-d', strtotime('-2 years'));
    $return_val = date('Y-m-d', strtotime('-15 days'));

    $smarty->assign("today_val", $today_val);
    $smarty->assign("week_val", $week_val);
    $smarty->assign("month_val", $month_val);
    $smarty->assign("months_val", $months_val);
    $smarty->assign("year_val", $year_val);
    $smarty->assign("years_val", $years_val);
    $smarty->assign("return_val", $return_val);

    //검색 처리
    $sch_w_no 			    = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
    $sch_stock_date 	    = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : "";
    $sch_ord_s_date         = isset($_GET['sch_ord_s_date']) ? $_GET['sch_ord_s_date'] : $year_val;
    $sch_ord_e_date         = isset($_GET['sch_ord_e_date']) ? $_GET['sch_ord_e_date'] : $today_val;
    $sch_ord_date_type      = isset($_GET['sch_ord_date_type']) ? $_GET['sch_ord_date_type'] : "year";
    $sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
    $sch_recipient 		    = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
    $sch_recipient_hp 	    = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
    $sch_recipient_addr 	= isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
    $sch_delivery_no 	    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
    $sch_dp_c_no 		    = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
    $sch_c_name 		    = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
    $sch_prd_name 		    = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
    $sch_notice             = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
    $sch_is_refund 		    = isset($_GET['sch_is_refund']) ? $_GET['sch_is_refund'] : "";
    $sch_is_deposit 		= isset($_GET['sch_is_deposit']) ? $_GET['sch_is_deposit'] : "";
    $sch_cs_type 		    = isset($_GET['sch_cs_type']) ? $_GET['sch_cs_type'] : "";
    $sch_cs_issue 		    = isset($_GET['sch_cs_issue']) ? $_GET['sch_cs_issue'] : "";
    $sch_cs_manager 		= isset($_GET['sch_cs_manager']) ? $_GET['sch_cs_manager'] : "";
    $sch_cs_manager_null	= isset($_GET['sch_cs_manager_null']) ? $_GET['sch_cs_manager_null'] : "";
    $sch_cs_status 		    = isset($_GET['sch_cs_status']) ? $_GET['sch_cs_status'] : "";
    $sch_cs_comment 		= isset($_GET['sch_cs_comment']) ? $_GET['sch_cs_comment'] : "";
    $sch_req_name 		    = isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : "";
    $sch_coupon_price       = isset($_GET['sch_coupon_price']) ? $_GET['sch_coupon_price'] : "";
    $sch_coupon_type        = isset($_GET['sch_coupon_type']) ? $_GET['sch_coupon_type'] : "";
    $sch_log_c_no           = isset($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "";

    if(!empty($sch_w_no)){
        $add_where .= " AND w.w_no='{$sch_w_no}'";
        $smarty->assign('sch_w_no', $sch_w_no);
    }

    if(!empty($sch_stock_date)){
        $add_where .= " AND w.stock_date = '{$sch_stock_date}'";
        $smarty->assign('sch_stock_date', $sch_stock_date);
    }

    if(!empty($sch_ord_s_date) || !empty($sch_ord_e_date))
    {
        if(!empty($sch_ord_s_date) && empty($sch_ord_e_date)){
            $sch_ord_s_datetime = $sch_ord_s_date." 00:00:00";
            $add_where .= " AND w.regdate >= '{$sch_ord_s_datetime}'";
        }elseif(!empty($sch_ord_e_date) && empty($sch_ord_s_date)){
            $sch_ord_e_datetime = $sch_ord_e_date." 23:59:59";
            $add_where .= " AND w.regdate <= '{$sch_ord_e_datetime}'";
        }else{
            $sch_ord_s_datetime = $sch_ord_s_date." 00:00:00";
            $sch_ord_e_datetime = $sch_ord_e_date." 23:59:59";

            $add_where .= " AND (w.regdate BETWEEN '{$sch_ord_s_datetime}' AND '{$sch_ord_e_datetime}')";
        }

        $smarty->assign('sch_ord_s_date', $sch_ord_s_date);
        $smarty->assign('sch_ord_e_date', $sch_ord_e_date);
    }
    $smarty->assign('sch_ord_date_type', $sch_ord_date_type);


    if(!empty($sch_order_number)){
        $add_where .= " AND w.order_number = '{$sch_order_number}'";
        $smarty->assign('sch_order_number', $sch_order_number);
    }

    if(!empty($sch_recipient)){
        $add_where .= " AND w.recipient = '{$sch_recipient}'";
        $smarty->assign('sch_recipient', $sch_recipient);
    }

    if(!empty($sch_recipient_hp)){
        $add_where .= " AND w.recipient_hp like '%{$sch_recipient_hp}%'";
        $smarty->assign('sch_recipient_hp', $sch_recipient_hp);
    }

    if(!empty($sch_recipient_addr)){
        $add_where .= " AND w.recipient_addr like '%{$sch_recipient_addr}%'";
        $smarty->assign('sch_recipient_addr', $sch_recipient_addr);
    }

    if(!empty($sch_delivery_no)){
        $add_where .= " AND w.order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery wcd WHERE wcd.delivery_no = '{$sch_delivery_no}')";
        $smarty->assign('sch_delivery_no', $sch_delivery_no);
    }

    if(!empty($sch_dp_c_no)){
        $add_where .= " AND w.dp_c_no='{$sch_dp_c_no}'";
        $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
    }

    if(!empty($sch_c_name)){
        $add_where .= " AND w.c_name like '%{$sch_c_name}%'";
        $smarty->assign('sch_c_name', $sch_c_name);
    }

    if(!empty($sch_prd_name)){
        $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no from product_cms prd_cms where prd_cms.title like '%{$sch_prd_name}%')";
        $smarty->assign('sch_prd_name', $sch_prd_name);
    }

    if(!empty($sch_notice)){
        $add_where .= " AND w.notice like '%{$sch_notice}%'";
        $smarty->assign('sch_notice', $sch_notice);
    }

    if(!empty($sch_cs_type)){
        if($sch_cs_type == '1'){
            $add_where .= " AND (w.shop_ord_no != '' AND w.shop_ord_no IS NOT NULL) AND w.shop_ord_no IN(SELECT wcr.parent_shop_ord_no FROM work_cms_return wcr WHERE wcr.return_purpose='1')";
        }elseif($sch_cs_type == '2'){
            $add_where .= " AND (w.shop_ord_no != '' AND w.shop_ord_no IS NOT NULL) AND w.shop_ord_no IN(SELECT wcr.parent_shop_ord_no FROM work_cms_return wcr WHERE wcr.return_purpose='2')";
        }elseif($sch_cs_type == '3'){
            $add_where .= " AND (w.shop_ord_no != '' AND w.shop_ord_no IS NOT NULL) AND w.shop_ord_no IN(SELECT wcr.parent_shop_ord_no FROM work_cms_return wcr WHERE wcr.return_purpose='3')";
        }

        $smarty->assign('sch_cs_type', $sch_cs_type);
    }

    if(!empty($sch_is_refund)) {
        $add_where .= " AND is_refund='{$sch_is_refund}'";
        $smarty->assign('sch_is_refund', $sch_is_refund);
    }

    if(!empty($sch_is_deposit)) {
        $add_where .= " AND is_deposit='{$sch_is_deposit}'";
        $smarty->assign('sch_is_deposit', $sch_is_deposit);
    }

    if(!empty($sch_cs_issue)){
        $add_where .= " AND FIND_IN_SET('{$sch_cs_issue}', w.cs_issue) > 0";
        $smarty->assign('sch_cs_issue', $sch_cs_issue);
    }

    if(!empty($sch_cs_manager_null)){
        $add_where .= " AND w.order_number NOT IN(SELECT wcm.order_number FROM work_cs_management as wcm WHERE wcm.cs_s_no > 0)";
        $smarty->assign('sch_cs_manager_null', $sch_cs_manager_null);
    }else{
        if(!empty($sch_cs_manager)){
            $add_where .= " AND w.order_number IN(SELECT wcm.order_number FROM work_cs_management as wcm WHERE wcm.cs_s_no='{$sch_cs_manager}') ";
            $smarty->assign('sch_cs_manager', $sch_cs_manager);
        }
    }

    if(!empty($sch_cs_status)){
        $add_where .= " AND (SELECT dp.dp_state FROM deposit dp WHERE dp_no IN(SELECT sub.dp_no FROM `work` as sub WHERE sub.prd_no='260' AND sub.work_state='6' AND sub.linked_table='work_cms' AND sub.linked_shop_no=w.order_number))='{$sch_cs_status}' ";
        $smarty->assign('sch_cs_status', $sch_cs_status);
    }

    if(!empty($sch_cs_comment)){
        $add_where .= " AND w.order_number IN(SELECT DISTINCT ord_no FROM work_cms_comment wcc WHERE wcc.comment LIKE '%{$sch_cs_comment}%')";
        $smarty->assign('sch_cs_comment', $sch_cs_comment);
    }

    if(!empty($sch_req_name)){
        $add_where .= " AND w.task_req_s_no IN(SELECT s.s_no FROM staff s WHERE s_name LIKE '%{$sch_req_name}%')";
        $smarty->assign('sch_req_name', $sch_req_name);
    }

    if(!empty($sch_coupon_price))
    {
        if($sch_coupon_price == '1'){
            $add_where .= " AND (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) > 0";
        }else{
            $add_where .= " AND (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) IS NULL";
        }
        $smarty->assign('sch_coupon_price', $sch_coupon_price);
    }

    if(!empty($sch_coupon_type))
    {
        if($sch_coupon_type == '1'){
            $add_where .= " AND w.dp_c_no = '1372'";
        }elseif($sch_coupon_type == '2'){
            $add_where .= " AND w.dp_c_no IN(3295,4629,5427,5588)";
        }elseif($sch_coupon_type == '3'){
            $add_where .= " AND w.dp_c_no = '5800'";
        }elseif($sch_coupon_type == '3'){
            $add_where .= " AND w.dp_c_no = '5958'";
        }elseif($sch_coupon_type == '5'){
            $add_where .= " AND w.dp_c_no = '6012'";
        }
        $smarty->assign('sch_coupon_type', $sch_coupon_type);
    }

    if(!empty($sch_log_c_no))
    {
        $add_where .= " AND w.log_c_no = '{$sch_log_c_no}'";
        $smarty->assign('sch_log_c_no', $sch_log_c_no);
    }

    // 페이지에 따른 limit 설정
    $pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
    $smarty->assign("page",$pages);

    $num = 10;
    $offset = ($pages-1) * $num;

    $search_url             = getenv("QUERY_STRING");
    $wd_state_option        = getWdStateOption();
    $dp_state_option        = getDpStateOption();
    $delivery_state_option  = getDeliveryStateOption();
    $return_state_option    = getReturnStateOption();
    $company_model          = Company::Factory();
    $dp_company_option      = $company_model->getDpList();
    $sch_dp_company_option  = $company_model->getDpDisplayList();
    $log_company_list       = $company_model->getLogisticsList();
    $work_cs_list           = [];

    if(!empty($search_url))
    {
        // 전체 게시물 수
        $work_cs_total_sql   = "SELECT count(order_number) as cnt FROM (SELECT DISTINCT w.order_number FROM work_cms w WHERE {$add_where}) AS cnt";
        $work_cs_total_query = mysqli_query($my_db, $work_cs_total_sql);
        if(!!$work_cs_total_query)
            $work_cs_total_result = mysqli_fetch_array($work_cs_total_query);

        $work_cs_total = $work_cs_total_result['cnt'];

        //페이징
        $page_type  = (isset($_GET['ord_page_type']) && !empty($_GET['ord_page_type'])) ? intval($_GET['ord_page_type']) : "10";
        $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
        $num 		= $page_type;
        $offset 	= ($pages-1) * $num;
        $pagenum 	= ceil($work_cs_total/$num);

        if ($pages >= $pagenum){$pages = $pagenum;}
        if ($pages <= 0){$pages = 1;}

        $pagelist	= pagelist($pages, "work_list_cs.php", $pagenum, $search_url);
        $smarty->assign("search_url", $search_url);
        $smarty->assign("total_num", $work_cs_total);
        $smarty->assign("pagelist", $pagelist);
        $smarty->assign("ord_page_type", $page_type);

        //GET ORDER Number
        $work_cs_ord_sql    = "
            SELECT
                DISTINCT w.order_number
            FROM work_cms w
            WHERE {$add_where} AND w.order_number is not null
            ORDER BY w.w_no DESC LIMIT {$offset},{$num}
        ";
        $work_cs_ord_query  = mysqli_query($my_db, $work_cs_ord_sql);
        $order_number_list  = [];
        while($order_number = mysqli_fetch_assoc($work_cs_ord_query)){
            $order_number_list[] =  "'".$order_number['order_number']."'";
        }

        $order_numbers   = implode(',', $order_number_list);

        $add_order_where = !empty($order_numbers) ? "w.order_number IN({$order_numbers})" : "1!=1";


        // 리스트 쿼리
        $work_cs_sql = "
            SELECT
                w.w_no,
                w.order_date,
                DATE_FORMAT(w.order_date, '%Y-%m-%d') as ord_date,
                DATE_FORMAT(w.order_date, '%H:%i') as ord_time,
                w.order_number,
                w.parent_order_number,
                w.shop_ord_no,
                w.log_c_no,
                w.dp_c_no,
                w.recipient,
                w.recipient_hp,
                w.recipient_hp2,
                w.recipient_addr,
                IF(w.zip_code, CONCAT('[',w.zip_code,']'), '') as postcode,
                w.stock_date,
                w.delivery_memo,
                w.delivery_state,
                w.dp_c_name,
                w.c_no,
                w.c_name,
                w.prd_no,
                w.task_req_s_no,
                w.quantity,
                w.notice,
                w.unit_price,
                w.unit_delivery_price,
                (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon as wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
                w.final_price,
                w.cs_issue,
                (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
                (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
                (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
                (SELECT s_name from staff s where s.s_no=w.task_req_s_no) as task_req_s_name,
                (SELECT wcm.cs_s_no FROM work_cs_management as wcm WHERE wcm.order_number=w.order_number) as cs_manager,
                (SELECT count(*) FROM work_cms_comment wc WHERE wc.ord_no=w.order_number) as comment_count,
                (SELECT count(wc.w_no) FROM work_cms wc WHERE wc.parent_shop_ord_no=w.shop_ord_no AND wc.parent_shop_ord_no > 0) as ord_count,
                (SELECT count(wcr.r_no) FROM work_cms_return wcr WHERE wcr.parent_shop_ord_no=w.shop_ord_no AND wcr.parent_shop_ord_no != '' AND wcr.parent_shop_ord_no IS NOT NULL AND wcr.return_purpose='1') as trade_count,
                (SELECT count(wcr.r_no) FROM work_cms_return wcr WHERE wcr.parent_shop_ord_no=w.shop_ord_no AND wcr.parent_shop_ord_no != '' AND wcr.parent_shop_ord_no IS NOT NULL AND wcr.return_purpose='2') as return_count,
                (SELECT count(wcr.r_no) FROM work_cms_return wcr WHERE wcr.parent_shop_ord_no=w.shop_ord_no AND wcr.parent_shop_ord_no != '' AND wcr.parent_shop_ord_no IS NOT NULL AND wcr.return_purpose='3') as not_count
            FROM work_cms w
            WHERE {$add_order_where}
            ORDER BY w.w_no DESC, w.prd_no ASC
        ";

        if($work_cs_query  = mysqli_query($my_db, $work_cs_sql))
        {
            while($work_array = mysqli_fetch_assoc($work_cs_query))
            {
                $refund_list  = [];
                if(!empty($work_array['shop_ord_no']))
                {
                    $refund_sql    = "SELECT w_no, wd_no, wd_price_vat, (SELECT wd.wd_state FROM withdraw wd WHERE wd.wd_no=w.wd_no) as wd_state FROM work w WHERE prd_no='229' AND linked_table='work_cms' AND linked_shop_no='{$work_array['shop_ord_no']}'";
                    $refund_query  = mysqli_query($my_db, $refund_sql);
                    while($refund = mysqli_fetch_assoc($refund_query))
                    {
                        $price = isset($refund['wd_price_vat']) && !empty($refund['wd_price_vat']) ? $refund['wd_price_vat']  : 0;
                        $state = isset($refund['wd_state']) && !empty($refund['wd_state']) ? "[".$wd_state_option[$refund['wd_state']]."]" : "[대기]";
                        $refund_list[$refund['w_no']] = array('price' => $price, 'state' => $state);
                    }
                }

                $product = array(
                    'w_no'                  => $work_array['w_no'],
                    'c_no'                  => $work_array['c_no'],
                    'c_name'                => $work_array['c_name'],
                    'k_prd1_name'           => $work_array['k_prd1_name'],
                    'k_prd2_name'           => $work_array['k_prd2_name'],
                    'prd_no'                => $work_array['prd_no'],
                    'prd_name'              => $work_array['prd_name'],
                    'quantity'              => $work_array['quantity'],
                    'notice'                => $work_array['notice'],
                    'delivery_state'        => $work_array['delivery_state'],
                    'log_c_name'            => $log_company_list[$work_array['log_c_no']],
                    'dp_c_no'               => $work_array['dp_c_no'],
                    'dp_price'              => number_format($work_array['unit_price']),
                    'shop_ord_no'           => $work_array['shop_ord_no'],
                    'deli_count'            => $work_array['deli_count'],
                    'trade_count'           => $work_array['trade_count'],
                    'return_count'          => $work_array['return_count'],
                    'not_count'             => $work_array['not_count'],
                    'cs_issue'              => explode(',', $work_array['cs_issue']),
                    'unit_delivery_price'   => number_format($work_array['unit_delivery_price']),
                    'coupon_price'          => number_format($work_array['coupon_price']),
                    'refund_list'           => $refund_list
                );

                if(!isset($work_cs_list[$work_array['order_number']]))
                {
                    if(isset($work_array['recipient_hp']) && !empty($work_array['recipient_hp'])){
                        $f_hp  = substr($work_array['recipient_hp'],0,4);
                        $e_hp  = substr($work_array['recipient_hp'],7,15);
                        $work_array['recipient_end_hp'] = substr($work_array['recipient_hp'],-4);
                        $work_array['recipient_sc_hp']  = $f_hp."***".$e_hp;
                    }

                    # 입금확인
                    $deposit_list   = [];
                    $deposit_sql    = "SELECT w_no, dp_no, dp_price_vat, (SELECT dp.dp_state FROM deposit dp WHERE dp.dp_no=w.dp_no) as dp_state FROM work w WHERE prd_no='260' AND work_state='6' AND linked_table='work_cms' AND linked_shop_no='{$work_array['order_number']}'";
                    $deposit_query  = mysqli_query($my_db, $deposit_sql);
                    while($deposit = mysqli_fetch_assoc($deposit_query))
                    {
                        $price = isset($deposit['dp_price_vat']) && !empty($deposit['dp_price_vat']) ? $deposit['dp_price_vat']  : 0;
                        $state = (empty($deposit['dp_no'])) ? "[입금생성오류]" : (isset($deposit['dp_state']) && !empty($deposit['dp_state']) ? "[".$dp_state_option[$deposit['dp_state']]."]" : "[대기]");
                        $deposit_list[$deposit['dp_no']] = array('price' => $price, 'state' => $state);
                    }

                    # 교환 주문번호 체크(CMS)
                    $trade_sql    = "SELECT order_number, delivery_state FROM work_cms WHERE parent_order_number = '{$work_array['order_number']}' ORDER BY w_no";
                    $trade_query  = mysqli_query($my_db, $trade_sql);
                    $trade_list   = [];
                    while($trade = mysqli_fetch_assoc($trade_query))
                    {
                        $trade_list[$trade['order_number']] = array('ord_no' => $trade['order_number'], 'delivery_state' => "[".$delivery_state_option[$trade['delivery_state']]."]");
                    }

                    # 회수 리스트
                    $return_sql    = "SELECT order_number, return_state FROM work_cms_return WHERE parent_order_number = '{$work_array['order_number']}' ORDER BY r_no";
                    $return_query  = mysqli_query($my_db, $return_sql);
                    $return_list   = [];
                    while($return = mysqli_fetch_assoc($return_query))
                    {
                        $return_list[$return['order_number']] = array("ord_no" => $return['order_number'], "return_state" => "[".$return_state_option[$return['return_state']]."]");
                    }

                    # 운송장 리스트
                    $delivery_list  = [];
                    $delivery_sql   = "SELECT `no`, delivery_no, delivery_type FROM work_cms_delivery WHERE order_number='{$work_array['order_number']}' GROUP BY delivery_no";
                    $delivery_query = mysqli_query($my_db, $delivery_sql);
                    while($delivery = mysqli_fetch_assoc($delivery_query))
                    {
                        $delivery_list[$delivery['no']] = array('delivery_no' => $delivery['delivery_no'], 'delivery_type' => $delivery['delivery_type']);
                    }

                    $work_array['deposit_list']     = $deposit_list;
                    $work_array['trade_list']       = $trade_list;
                    $work_array['return_list']      = $return_list;
                    $work_array['delivery_list']    = $delivery_list;

                    $work_array['cs_status']    = explode(',', $work_array['cs_status']);

                    $work_cs_list[$work_array['order_number']] = $work_array;
                }

                $work_cs_list[$work_array['order_number']]['prd_list'][] = $product;
            }
        }
    }

    $new_ord_date           = date('Y-m-d');
    $cur_date               = date('Ymd');
    $new_ord_no             = $cur_date."_CS_".sprintf('%04d', 1);
    $staff_model            = Staff::Factory();
    $cs_manager_list        = $staff_model->getActiveTeamStaff("00244");

    $smarty->assign('cur_date', $new_ord_date);
    $smarty->assign('new_ord_no', $new_ord_no);
    $smarty->assign('new_ord_date', $new_ord_date);
    $smarty->assign('dp_cs_list', getDpCsList());
    $smarty->assign('sch_dp_company_option', $sch_dp_company_option);
    $smarty->assign('dp_company_option', $dp_company_option);
    $smarty->assign("cs_manager_list", $cs_manager_list);
    $smarty->assign("cs_status_option", getCsStatusOption());
    $smarty->assign("is_refund_option", getIsRefundOption());
    $smarty->assign("cs_issue_option", getCsIssueOption());
    $smarty->assign("cs_type_option", getCsTypeOption());
    $smarty->assign('is_exist_option', getIsExistOption());
    $smarty->assign("page_type_option", getPageTypeOption('4'));
    $smarty->assign('coupon_type_option', getCouponTypeOption());
    $smarty->assign('delivery_state_option', getDeliveryStateColorOption());
    $smarty->assign('logistics_company_list', $log_company_list);
    $smarty->assign("work_cs_list", $work_cs_list);

    $smarty->display('work_list_cs.html');
}

?>
