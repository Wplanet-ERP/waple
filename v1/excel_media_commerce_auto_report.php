<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/commerce_sales.php');
require('inc/helper/work_cms.php');
require('inc/helper/wise_bm.php');
require('inc/model/CommerceSales.php');
require('inc/model/Kind.php');
require('Classes/PHPExcel.php');

$allowed_download = false;
if($session_s_no == '1' || $session_s_no == '6' || $session_s_no == '7' || $session_s_no == '10' || $session_s_no == '15' || $session_s_no == '18' || $session_s_no == '42' || $session_s_no == '59'
    || $session_s_no == '67' || $session_s_no == '147' || $session_s_no == '166' || $session_s_no == '167' || $session_s_no == '171' || permissionNameCheck($session_permission, "재무관리자")){
    $allowed_download = true;
}

if(!$allowed_download){
    exit("<script>alert('다운로드 권한이 없습니다.');location.href='media_commerce_auto_report.php';</script>");
}

# Kind(cost,sales), Brand Group 검색
$comm_sales_model           = CommerceSales::Factory();
$comm_chart_total_init      = $comm_sales_model->getChartAutoReportData();
$comm_g1_title_list         = $comm_chart_total_init['comm_g1_title'];
$comm_g2_title_list         = $comm_chart_total_init['comm_g2_title'];

$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$dp_except_list             = getNotApplyDpList();
$dp_except_text             = implode(",", $dp_except_list);
$dp_self_imweb_list         = getSelfDpImwebCompanyList();
$global_dp_all_list         = getGlobalDpCompanyList();
$global_dp_total_list       = $global_dp_all_list['total'];
$global_dp_ilenol_list      = $global_dp_all_list['ilenol'];
$global_brand_list          = getGlobalBrandList();
$global_brand_option        = array_keys($global_brand_list);
$doc_brand_list             = getTotalDocBrandList();
$doc_brand_text             = implode(",", $doc_brand_list);
$ilenol_brand_list          = getIlenolBrandList();
$ilenol_brand_text          = implode(",", $ilenol_brand_list);
$ilenol_global_dp_text      = implode(",", $global_dp_ilenol_list);
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$sch_brand_name             = "전체";

/** 검색조건 START */
$add_where                  = "display='1'";
$add_cms_where              = "w.delivery_state='4' AND dp_c_no NOT IN({$dp_except_text})";
$add_quick_where            = "w.prd_type='1' AND w.unit_price > 0 AND w.quick_state='4'";
$add_kind_where             = "";
$add_result_where           = "`am`.state IN(3,5) AND `am`.product IN(1,2) AND `am`.media='265'";

# 브랜드 검색 설정 및 수정여부
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$kind_column_list   = [];
$brand_column_list  = [];
$brand_company_list = [];

if(!isset($_GET['sch_brand']))
{
    switch($session_s_no){
        case '4':
            $sch_brand_g1   = '70003';
            break;
        case '6':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70008';
            break;
        case '7':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70014';
            $sch_brand      = '4446';
            break;
        case '10':
            $sch_brand_g1   = '70022';
            $sch_brand_g2   = '70023';
            $sch_brand      = '5493';
            break;
        case '15':
            $sch_brand_g1   = '70002';
            break;
        case '17':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '1314';
            break;
        case '42':
        case '177':
        case '239':
        case '265':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            break;
        case '59':
            $sch_brand_g1   = '70005';
            $sch_brand_g2   = '70020';
            $sch_brand      = '2402';
            break;
        case '67':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70011';
            $sch_brand      = '5434';
            break;
        case '76':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70013';
            $sch_brand      = '4445';
            break;
        case '145':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70010';
            break;
        case '147':
            $sch_brand_g1   = '70004';
            $sch_brand_g2   = '70019';
            break;
        case '43':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70026';
            $sch_brand      = '5812';
            break;
    }
}

if(!empty($sch_brand))
{
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];

    $sch_brand_g2_list              = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list                 = $brand_list[$sch_brand_g2];
    $brand_column_list[$sch_brand]  = $brand_list[$sch_brand_g2][$sch_brand];
    $brand_company_list[$sch_brand] = $sch_brand;
    $sch_brand_name                 = $brand_list[$sch_brand_g2][$sch_brand];

    if(in_array($sch_brand, $global_brand_option)) {
        if($sch_brand == '5979' || $sch_brand == '6044'){
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','09056'))";
            $add_kind_where .= " AND k_parent IN('08027','09056')";
        }elseif($sch_brand == '5514'){
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08033','08034','08035','08036','08042','08046','09060','09061','09062','09063','09081','09092'))";
            $add_kind_where .= " AND k_parent IN('08033','08034','08035','08036','08042','08046','09060','09061','09062','09063','09081','09092')";
        }else{
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092'))";
            $add_kind_where .= " AND k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092')";
        }
    } else {
        $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code NOT IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092'))";
        $add_kind_where .= " AND k_parent NOT IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092')";
    }

    if(in_array($sch_brand, $doc_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5956' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif(in_array($sch_brand, $ilenol_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5659' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "5513"){
        $add_cms_where      .= " AND `w`.c_no IN({$doc_brand_text}) AND (`w`.log_c_no = '5956' OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$doc_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5514"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND ((`w`.log_c_no = '5659' AND `w`.dp_c_no NOT IN({$ilenol_global_dp_text})) OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5979"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.c_no != '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "6044"){
        $add_cms_where      .= " AND `w`.c_no = '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    else{
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}'";
    }

    $add_result_where  .= " AND `ar`.brand = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g1       = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $brand_column_list  = $sch_brand_list;
    $sch_brand_name     = $brand_company_g2_list[$sch_brand_g1][$sch_brand_g2];

    foreach($sch_brand_list as $c_no => $c_name) {
        $brand_company_list[$c_no] = $c_no;
    }

    $add_where         .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_cms_where     .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_quick_where   .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_result_where  .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $brand_column_list  = $sch_brand_g2_list;
    $sch_brand_name     = $brand_company_g1_list[$sch_brand_g1];

    foreach($sch_brand_g2_list as $brand_g2 => $brand_g2_name)
    {
        $init_company_list = $brand_list[$brand_g2];
        if(!empty($init_company_list))
        {
            foreach($init_company_list as $c_no => $c_name) {
                $brand_company_list[$c_no] = $c_no;
            }
        }
    }

    $add_where         .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_cms_where     .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_quick_where   .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_result_where  .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
else
{
    $brand_column_list  = $brand_company_g1_list;

    foreach($brand_company_g1_list as $brand_g1 => $brand_g1_name)
    {
        $init_g2_list = $brand_company_g2_list[$brand_g1];
        if(!empty($init_g2_list))
        {
            foreach($init_g2_list as $brand_g2 => $brand_g2_name)
            {
                $init_company_list = $brand_list[$brand_g2];
                if(!empty($init_company_list))
                {
                    foreach($init_company_list as $c_no => $c_name) {
                        $brand_company_list[$c_no] = $c_no;
                    }
                }
            }
        }
    }
}


$today_s_w		 = date('w')-1;
$sch_s_year      = isset($_GET['sch_s_year']) ? $_GET['sch_s_year'] : date('Y', strtotime("-5 years"));;
$sch_e_year      = isset($_GET['sch_e_year']) ? $_GET['sch_e_year'] : date('Y');
$sch_s_month     = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("-5 months"));
$sch_e_month     = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week      = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week      = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date      = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-8 day"));
$sch_e_date      = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');
$sch_not_empty   = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : "";

$all_date_where     = "";
$all_date_key	    = "";
$add_date_where     = "";
$add_date_column    = "";
$add_cms_date_where = "";
$add_cms_column     = "";
$add_quick_column   = "";
$add_result_column  = "";
$sch_net_date       = "";

if($sch_date_type == '4') //연간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y')";

    $sch_s_datetime     = $sch_s_year."-01-01 00:00:00";
    $sch_e_datetime     = $sch_e_year."-12-31 23:59:59";

    $add_cms_date_where = " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column     = "DATE_FORMAT(order_date, '%Y')";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y')";

    $add_result_where  .= " AND am.adv_s_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y')";

    $sch_net_date       = $sch_s_year."-01-01";
}
elseif($sch_date_type == '3') //월간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y%m')";

    $sch_s_datetime     = $sch_s_month."-01 00:00:00";
    $sch_e_datetime     = $sch_e_month."-31 23:59:59";

    $add_cms_date_where = " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m')";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y%m')";

    $add_result_where  .= " AND am.adv_s_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y%m')";

    $sch_net_date       = $sch_s_month."-01";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	    = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title     = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_date_column    = "DATE_FORMAT(DATE_SUB(sales_date, INTERVAL(IF(DAYOFWEEK(sales_date)=1,8,DAYOFWEEK(sales_date))-2) DAY), '%Y%m%d')";

    $sch_s_datetime     = $sch_s_week." 00:00:00";
    $sch_e_datetime     = $sch_e_week." 23:59:59";

    $add_cms_date_where = " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column     = "DATE_FORMAT(DATE_SUB(order_date, INTERVAL(IF(DAYOFWEEK(order_date)=1,8,DAYOFWEEK(order_date))-2) DAY), '%Y%m%d')";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(DATE_SUB(run_date, INTERVAL(IF(DAYOFWEEK(run_date)=1,8,DAYOFWEEK(run_date))-2) DAY), '%Y%m%d')";

    $add_result_where  .= " AND am.adv_s_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'";
    $add_result_column  = "DATE_FORMAT(DATE_SUB(am.adv_s_date, INTERVAL(IF(DAYOFWEEK(am.adv_s_date)=1,8,DAYOFWEEK(am.adv_s_date))-2) DAY), '%Y%m%d')";

    $sch_net_date       = $sch_s_week;
}
else //일간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y%m%d')";

    $sch_s_datetime     = $sch_s_date." 00:00:00";
    $sch_e_datetime     = $sch_e_date." 23:59:59";

    $add_cms_date_where = " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m%d')";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y%m%d')";

    $add_result_where  .= " AND am.adv_s_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y%m%d')";

    $sch_net_date       = $sch_s_date;
}

# 비용/매출 수기 리스트
$kind_column_list  = [];
$kind_column_sql   = "
    SELECT 
        k_parent, 
        (SELECT sub_k.k_name FROM kind sub_k WHERE sub_k.k_name_code=k.k_parent) as k_parent_name, 
        (SELECT sub_k.priority FROM kind sub_k WHERE sub_k.k_name_code=k.k_parent) as k_parent_priority, 
        k_code, 
        k_name_code, 
        k_name 
    FROM kind k 
    WHERE (k_code='cost' OR k_code='sales')
      AND k_parent is not null {$add_kind_where} 
    ORDER BY k_parent_priority ASC, priority ASC
";
$kind_column_query = mysqli_query($my_db, $kind_column_sql);
while($kind_column_data = mysqli_fetch_assoc($kind_column_query))
{
    $kind_column_list[$kind_column_data['k_parent']][$kind_column_data['k_name_code']] = array(
        'k_name'        => $kind_column_data['k_name'],
        'k_name_code'   => $kind_column_data['k_name_code'],
        'type'          => $kind_column_data['k_code'],
        'k_parent'      => $kind_column_data['k_parent'],
        'k_parent_name' => $kind_column_data['k_parent_name'],
        'date'          => "",
        'price'         => 0
    );
}

# 배송리스트 구매처
$cms_title_sql              = "SELECT DISTINCT dp_c_no, dp_c_name, (SELECT c.priority FROM company c WHERE c.c_no=w.dp_c_no) as priority FROM work_cms as w WHERE {$add_cms_where} {$add_cms_date_where} ORDER BY priority ASC, dp_c_name ASC";
$cms_title_query            = mysqli_query($my_db, $cms_title_sql);
$cms_report_title_list      = [];
$cms_report_delivery_list   = [];
$cms_delivery_free_chk_list = [];
while($cms_title = mysqli_fetch_assoc($cms_title_query)) {
    $cms_report_title_list[$cms_title['dp_c_no']]       = $cms_title['dp_c_name'];
    $cms_report_delivery_list[$cms_title['dp_c_no']]    = 0;
    $cms_delivery_free_chk_list[$cms_title['dp_c_no']]  = 0;
}

# 입/출고요청 구매처
$quick_title_sql            = "SELECT DISTINCT run_c_no, run_c_name FROM work_cms_quick as w WHERE {$add_quick_where} ORDER BY run_c_name ASC";
$quick_title_query          = mysqli_query($my_db, $quick_title_sql);
$quick_report_title_list    = [];
while($quick_title = mysqli_fetch_assoc($quick_title_query)) {
    $quick_report_title_list[$quick_title['run_c_no']] = $quick_title['run_c_name'];
}

$total_all_date_list            = [];
$commerce_date_list             = [];
$cms_report_list                = [];
$sales_report_list              = [];
$cost_report_list               = [];
$memo_report_list               = [];
$memo_report_total_list         = [];
$coupon_imweb_report_total_list = [];
$coupon_smart_report_total_list = [];
$cms_delivery_free_list         = [];
$cms_delivery_free_total_list   = [];
$nosp_result_special_list       = [];
$nosp_result_time_list          = [];
$quick_report_list              = [];

$sales_tmp_total_list           = [];
$cost_tmp_total_list            = [];
$net_report_date_list           = [];
$net_parent_percent_list        = [];
$company_chk_list               = [];
$report_empty_check_list        = [];

# 날짜 리스트
$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
if($all_date_where != '')
{
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $total_all_date_list[$date['date_key']] = array("s_date" => date("Y-m-d",strtotime($date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($date['date_key']))." 23:59:59");

        $chart_title = $date['chart_title'];
        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        $report_empty_check_list['delivery'] = 0;
        foreach($kind_column_list as $comm_g1 => $kind_columns)
        {
            $report_empty_check_list[$comm_g1] = 0;
            foreach($kind_columns as $comm_g2 => $kind_column){
                if($kind_column['type'] == 'sales'){
                    $sales_report_list[$comm_g1][$comm_g2][$date['chart_key']] = $kind_column;
                    $sales_report_list[$comm_g1][$comm_g2][$date['chart_key']]['date'] = $date['chart_key'];
                }elseif($kind_column['type'] == 'cost'){
                    $cost_report_list[$comm_g1][$comm_g2][$date['chart_key']] = $kind_column;
                    $cost_report_list[$comm_g1][$comm_g2][$date['chart_key']]['date'] = $date['chart_key'];
                }

                $memo_report_list[$date['date_key']][$comm_g2] = "";
                $report_empty_check_list[$comm_g2] = 0;
            }
            $memo_report_total_list[$date['date_key']][$comm_g1] = "";
        }

        foreach($cms_report_title_list as $cms_key => $cms_title)
        {
            $cms_report_list[$cms_key][$date['chart_key']]['subtotal'] = 0;
            $cms_report_list[$cms_key][$date['chart_key']]['delivery'] = 0;

            $cms_delivery_free_list[$cms_key][$date['chart_key']] = 0;
        }

        foreach($quick_report_title_list as $quick_key => $quick_title)
        {
            $quick_report_list[$quick_key][$date['chart_key']] = 0;
        }

        $coupon_imweb_report_total_list[$date['chart_key']] = array("total" => 0, "belabef" => 0, "doc" => 0, "ilenol" => 0, "nuzam" => 0);
        $coupon_smart_report_total_list[$date['chart_key']] = 0;
        $cms_delivery_free_total_list[$date['chart_key']]   = 0;
        $nosp_result_special_list[$date['chart_key']]       = 0;
        $nosp_result_time_list[$date['chart_key']]          = 0;

        if(!isset($commerce_date_list[$date['chart_key']]))
        {
            $chart_s_date = "";
            $chart_e_date = "";
            $chart_mon    = "";

            if($sch_date_type == '4'){
                $chart_s_date = $chart_title."-01-01";
                $chart_e_date = $chart_title."-12-31";
                $chart_mon    = date("Y-m", strtotime($chart_s_date));
            }elseif($sch_date_type == '3'){
                $chart_s_date_val   = $date['chart_key']."01";
                $chart_s_date       = date("Y-m-d", strtotime("{$chart_s_date_val}"));
                $chart_e_day        = DATE('t', strtotime($chart_s_date));
                $chart_e_date_val   = $date['chart_key'].$chart_e_day;
                $chart_e_date       = date("Y-m-d", strtotime("{$chart_e_date_val}"));
                $chart_mon          = date("Y-m", strtotime($chart_s_date));
            }elseif($sch_date_type == '2'){
                $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_e_date = date("Y-m-d", strtotime("{$chart_s_date} +6 days"));
                $chart_mon    = date("Y-m", strtotime($chart_s_date));
            }elseif($sch_date_type == '1'){
                $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_e_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_mon    = date("Y-m", strtotime($chart_s_date));
            }
            $commerce_date_list[$date['chart_key']] = array('title' => $chart_title, 's_date' => $chart_s_date, 'e_date' => $chart_e_date, 'mon_date' => $chart_mon);
        }

        foreach($brand_column_list as $kind_key => $kind_title)
        {
            foreach($brand_total_list[$kind_key] as $brand){
                $company_chk_list[$brand] = $kind_key;
            }
        }

        foreach($brand_company_list as $c_no)
        {
            $cost_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
            $sales_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
            $net_report_date_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
            $net_parent_percent_list[$c_no] = 0;
        }
    }
}

# 발주관련 작업
$commerce_sql  = "
    SELECT
        cr.`type`,
        cr.price as price,
        cr.brand,
        cr.k_name_code as comm_g2,
        DATE_FORMAT(sales_date, '%Y%m%d') as sales_date,
        {$add_date_column} as key_date,
        (SELECT k.k_code FROM kind k WHERE k.k_name_code=`cr`.k_name_code) as comm_type,
        (SELECT k.k_name_code FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cr`.k_name_code)) as comm_g1,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cr`.k_name_code)) as comm_g1_name,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code=`cr`.k_name_code) as comm_g2_name
    FROM commerce_report `cr`
    WHERE {$add_where}
    {$add_date_where}
    ORDER BY sales_date ASC
";
$commerce_query = mysqli_query($my_db, $commerce_sql);
while($commerce = mysqli_fetch_assoc($commerce_query))
{
    $comm_g1 = str_pad($commerce['comm_g1'], 5, "0", STR_PAD_LEFT);
    if($commerce['type'] == 'sales'){
        $sales_report_list[$comm_g1][$commerce['comm_g2']][$commerce['key_date']]['price'] += $commerce['price'];
        $sales_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
    }elseif($commerce['type'] == 'cost'){
        $cost_report_list[$comm_g1][$commerce['comm_g2']][$commerce['key_date']]['price'] += $commerce['price'];
        $cost_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
    }

    $report_empty_check_list[$comm_g1] += $commerce['price'];
    $report_empty_check_list[$commerce['comm_g2']] += $commerce['price'];
}

# 지출 Total 값 구하기
$cost_report_total_list = [];
$cost_report_total_sum_list = [];
if(!empty($cost_report_list))
{
    foreach($cost_report_list as $comm_g1 => $cost_parent_report)
    {
        if($sch_not_empty && $report_empty_check_list[$comm_g1] == 0)
        {
            unset($cost_report_list[$comm_g1]);
            continue;
        }

        foreach($cost_parent_report as $comm_g2 => $cost_report)
        {
            if($sch_not_empty && $report_empty_check_list[$comm_g2] == 0)
            {
                unset($cost_report_list[$comm_g1][$comm_g2]);
                continue;
            }

            foreach($cost_report as $key_date => $cost_data){
                if(!isset($cost_report_total_list[$comm_g1])){
                    $cost_report_total_list[$comm_g1][$key_date] = 0;
                }
                $cost_report_total_list[$comm_g1][$key_date] += $cost_data['price'];

                if(!isset($cost_report_total_sum_list[$key_date])){
                    $cost_report_total_sum_list[$key_date] = 0;
                }
                $cost_report_total_sum_list[$key_date] += $cost_data['price'];
            }
        }
    }
}

# 매출 Total 값 구하기
$sales_report_total_list        = [];
$sales_report_total_sum_list    = [];
if(!empty($sales_report_list))
{
    foreach($sales_report_list as $comm_g1 => $sales_parent_report)
    {
        if($sch_not_empty && $report_empty_check_list[$comm_g1] == 0)
        {
            unset($sales_report_list[$comm_g1]);
            continue;
        }

        foreach($sales_parent_report as $comm_g2 => $sales_report)
        {
            if($sch_not_empty && $report_empty_check_list[$comm_g2] == 0)
            {
                unset($sales_report_list[$comm_g1][$comm_g2]);
                continue;
            }

            foreach($sales_report as $key_date => $sales_data){
                if(!isset($sales_report_total_list[$comm_g1])){
                    $sales_report_total_list[$comm_g1][$key_date] = 0;
                }
                $sales_report_total_list[$comm_g1][$key_date] += $sales_data['price'];

                if(!isset($sales_report_total_sum_list[$key_date])){
                    $sales_report_total_sum_list[$key_date] = 0;
                }
                $sales_report_total_sum_list[$key_date] += $sales_data['price'];
            }

        }
    }
}

# 배송리스트 매출관련
$commerce_fee_option         = getCommerceFeeOption();
$cms_delivery_chk_fee_list   = [];
$cms_delivery_chk_nuzam_list = [];
$cms_delivery_chk_ord_list   = [];
$cms_delivery_chk_list       = [];
$commerce_fee_list           = [];
foreach($total_all_date_list as $date_data)
{
    $cms_report_sql      = "
        SELECT 
            c_no,
            dp_c_no,
            dp_c_name,
            order_number,
            DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
            {$add_cms_column} as key_date, 
            unit_price,
            (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
            unit_delivery_price,
            (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
        FROM work_cms as w
        WHERE {$add_cms_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}')
    ";
    $cms_report_query = mysqli_query($my_db, $cms_report_sql);
    while($cms_report_result = mysqli_fetch_assoc($cms_report_query))
    {
        $commerce_fee_total_list    = isset($commerce_fee_option[$cms_report_result['c_no']]) ? $commerce_fee_option[$cms_report_result['c_no']] : [];
        $commerce_fee_per_list      = !empty($commerce_fee_total_list) && isset($commerce_fee_total_list[$cms_report_result['dp_c_no']]) ? $commerce_fee_total_list[$cms_report_result['dp_c_no']] : [];
        $commerce_fee_per           = 0;

        if(!empty($commerce_fee_per_list))
        {
            foreach($commerce_fee_per_list as $fee_date => $fee_val){
                if($cms_report_result['sales_date'] >= $fee_date){
                    $commerce_fee_per = $fee_val;
                    $commerce_fee_list[$cms_report_result['dp_c_no']] = $fee_val;
                }
            }
        }

        $cms_report_fee      = $cms_report_result['unit_price']*($commerce_fee_per/100);
        $total_price         = $cms_report_result['unit_price'] - $cms_report_fee;

        $cms_report_list[$cms_report_result['dp_c_no']][$cms_report_result['key_date']]['subtotal'] += $total_price;
        $cms_report_list[$cms_report_result['dp_c_no']][$cms_report_result['key_date']]['delivery'] += $cms_report_result['unit_delivery_price'];
        $cms_report_delivery_list[$cms_report_result['dp_c_no']] += $cms_report_result['unit_delivery_price'];

        if(in_array($sch_brand, $global_brand_option)){
            $cms_report_result['c_no'] = $sch_brand;
        }

        $sales_tmp_total_list[$cms_report_result['c_no']][$cms_report_result['key_date']][$cms_report_result['sales_date']] += $total_price;

        if(in_array($cms_report_result['dp_c_no'], $dp_self_imweb_list))
        {
            if($cms_report_result['dp_c_no'] == '1372'){
                $coupon_imweb_report_total_list[$cms_report_result['key_date']]['belabef'] += $cms_report_result['coupon_price'];
            }
            elseif($cms_report_result['dp_c_no'] == '5800'){
                $coupon_imweb_report_total_list[$cms_report_result['key_date']]['doc'] += $cms_report_result['coupon_price'];
            }
            elseif($cms_report_result['dp_c_no'] == '5958'){
                $coupon_imweb_report_total_list[$cms_report_result['key_date']]['ilenol'] += $cms_report_result['coupon_price'];
            }
            elseif($cms_report_result['dp_c_no'] == '6012'){
                $coupon_imweb_report_total_list[$cms_report_result['key_date']]['nuzam'] += $cms_report_result['coupon_price'];
            }

            $coupon_imweb_report_total_list[$cms_report_result['key_date']]['total'] += $cms_report_result['coupon_price'];
            $cost_tmp_total_list[$cms_report_result['c_no']][$cms_report_result['key_date']][$cms_report_result['sales_date']] += $cms_report_result['coupon_price'];
        }

        if($cms_report_result['dp_c_no'] == '3295' || $cms_report_result['dp_c_no'] == '4629' || $cms_report_result['dp_c_no'] == '5427' || $cms_report_result['dp_c_no'] == '5588'){
            $coupon_smart_report_total_list[$cms_report_result['key_date']] += $cms_report_result['coupon_price'];
        }

        if($cms_report_result['sales_date'] >= 20240101)
        {
            if($cms_report_result['unit_delivery_price'] > 0) {
                $cms_delivery_chk_fee_list[$cms_report_result['order_number']] = 1;
            }
            elseif($cms_report_result['unit_delivery_price'] == 0 && $cms_report_result['unit_price'] > 0)
            {
                $cms_delivery_chk_list[$cms_report_result['order_number']][$cms_report_result['dp_c_no']][$cms_report_result['key_date']][$cms_report_result['sales_date']] = $cms_report_result['deli_cnt'];
                $cms_delivery_chk_ord_list[$cms_report_result['order_number']][$cms_report_result['c_no']] = $cms_report_result['c_no'];

                if($cms_report_result['c_no'] == '2827' || $cms_report_result['c_no'] == '4878'){
                    $cms_delivery_chk_nuzam_list[$cms_report_result['order_number']] = 1;
                }
            }
        }
    }
}

$cms_report_total_sum_list      = [];
$cms_report_subtotal_sum_list   = [];
$cms_report_delivery_sum_list   = [];
if(!empty($cms_report_list))
{
    foreach($cms_report_list as $dp_c_no => $cms_parent_report)
    {
        foreach($cms_parent_report as $key_date => $cms_report)
        {
            if(!isset($cms_report_total_sum_list[$key_date])){
                $cms_report_total_sum_list[$key_date] = 0;
            }

            if(!isset($cms_report_subtotal_sum_list[$key_date])){
                $cms_report_subtotal_sum_list[$key_date] = 0;
            }

            if(!isset($cms_report_delivery_sum_list[$key_date])){
                $cms_report_delivery_sum_list[$key_date] = 0;
            }

            $cms_report_total_sum_list[$key_date]     += ($cms_report['subtotal']+$cms_report['delivery']);
            $cms_report_subtotal_sum_list[$key_date]  += $cms_report['subtotal'];
            $cms_report_delivery_sum_list[$key_date]  += $cms_report['delivery'];
        }

        if($sch_not_empty && $cms_report_delivery_list[$dp_c_no] == 0) {
            unset($cms_report_delivery_list[$dp_c_no]);
        }
    }
}

# 무료 배송비 계산
foreach($cms_delivery_chk_list as $chk_ord => $chk_ord_data)
{
    if(isset($cms_delivery_chk_fee_list[$chk_ord])){
        continue;
    }

    $chk_delivery_price = isset($cms_delivery_chk_nuzam_list[$chk_ord]) ? 5000 : 3000;

    foreach ($chk_ord_data as $chk_dp_c_no => $chk_dp_data)
    {
        if(in_array($chk_dp_c_no, $global_dp_total_list)){
            $chk_delivery_price = 6000;
        }
        
        foreach ($chk_dp_data as $chk_key_date => $chk_key_data)
        {
            foreach ($chk_key_data as $chk_sales_date => $deli_cnt)
            {
                $cal_delivery_price = $chk_delivery_price * $deli_cnt;

                $cms_delivery_free_list[$chk_dp_c_no][$chk_key_date] += $cal_delivery_price;
                $cms_delivery_free_total_list[$chk_key_date] += $cal_delivery_price;
                $cms_delivery_free_chk_list[$chk_dp_c_no] += $deli_cnt;

                $chk_brand_deli_price   = $cal_delivery_price;
                $chk_brand_cnt          = count($cms_delivery_chk_ord_list[$chk_ord]);
                $cal_brand_deli_price   = round($cal_delivery_price/$chk_brand_cnt);
                $cal_brand_idx          = 1;
                foreach($cms_delivery_chk_ord_list[$chk_ord] as $chk_c_no)
                {
                    $cal_one_delivery_price  = $cal_brand_deli_price;
                    $chk_brand_deli_price   -= $cal_one_delivery_price;

                    if($cal_brand_idx == $chk_brand_cnt){
                        $cal_one_delivery_price += $chk_brand_deli_price;
                    }

                    $cost_tmp_total_list[$chk_c_no][$chk_key_date][$chk_sales_date] += $cal_one_delivery_price;

                    $cal_brand_idx++;
                }
            }
        }
    }
}

if(!empty($cms_delivery_free_chk_list))
{
    foreach($cms_delivery_free_chk_list as $free_chk => $free_chk_value){
        if($free_chk_value == 0){
            unset($cms_delivery_free_list[$free_chk]);
        }
    }
}

# 입/출고요청 리스트
$quick_report_sql      = "
    SELECT 
        c_no,
        run_c_no,
        run_c_name,
        order_number,
        DATE_FORMAT(run_date, '%Y%m%d') as sales_date,
        {$add_quick_column} as key_date, 
        unit_price
    FROM work_cms_quick as w
    WHERE {$add_quick_where}
";
$quick_report_query = mysqli_query($my_db, $quick_report_sql);
while($quick_report_result = mysqli_fetch_assoc($quick_report_query))
{
    if(in_array($sch_brand, $global_brand_option)){
        $quick_report_result['c_no'] = $sch_brand;
    }

    $quick_report_list[$quick_report_result['run_c_no']][$quick_report_result['key_date']] += $quick_report_result['unit_price'];
    $sales_tmp_total_list[$quick_report_result['c_no']][$quick_report_result['key_date']][$quick_report_result['sales_date']] += $quick_report_result['unit_price'];
    $cms_report_subtotal_sum_list[$quick_report_result['key_date']] += $quick_report_result['unit_price'];
    $cms_report_total_sum_list[$quick_report_result['key_date']]    += $quick_report_result['unit_price'];
}

# NOSP 계산
$nosp_result_sql = "
        SELECT
        `ar`.am_no,
        `ar`.brand,
        `am`.product,
        `am`.fee_per,
        `am`.price,
        `ar`.impressions,
        `ar`.click_cnt,
        `ar`.adv_price,
        DATE_FORMAT(am.adv_s_date, '%Y%m%d') as adv_s_day,
        {$add_result_column} as key_date
    FROM advertising_result `ar`
    LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
    WHERE {$add_result_where}
    ORDER BY `am`.adv_s_date ASC
";
$nosp_result_query       = mysqli_query($my_db, $nosp_result_sql);
$nosp_result_tmp_list   = [];
while($nosp_result = mysqli_fetch_assoc($nosp_result_query))
{
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["product"]          = $nosp_result['product'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["sales_date"]       = $nosp_result['adv_s_day'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["key_date"]         = $nosp_result['key_date'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["fee_per"]          = $nosp_result['fee_per'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_adv_price"]  += $nosp_result['adv_price'];
}

foreach($nosp_result_tmp_list as $am_no => $nosp_result_tmp)
{
    foreach($nosp_result_tmp as $brand => $nosp_brand_data)
    {
        $fee_price      = $nosp_brand_data['total_adv_price']*($nosp_brand_data['fee_per']/100);
        $cal_price      = $nosp_brand_data["total_adv_price"]-$fee_price;
        $cal_price_vat  = $cal_price*1.1;

        if($nosp_brand_data['product'] == "1"){
            $nosp_result_time_list[$nosp_brand_data['key_date']]    += $cal_price_vat;
        }elseif($nosp_brand_data['product'] == "2"){
            $nosp_result_special_list[$nosp_brand_data['key_date']] += $cal_price_vat;
        }

        $cost_tmp_total_list[$brand][$nosp_brand_data['key_date']][$nosp_brand_data['sales_date']] += $cal_price_vat;
    }
}

# 예산지출비율 및 ROAS 계산
$net_sales_sum_list     = [];
$net_cost_sum_list      = [];
$expected_sales_list    = [];
$roas_percent_list      = [];

if(!empty($cost_report_total_sum_list) || !empty($sales_report_total_sum_list))
{
    foreach($cost_report_total_sum_list as $key_date => $cost_report_total)
    {
        $sales_total        = $sales_report_total_sum_list[$key_date]+$cms_report_subtotal_sum_list[$key_date];
        $cost_total         = $cost_report_total+$coupon_imweb_report_total_list[$key_date]["total"]+$cms_delivery_free_total_list[$key_date]+$nosp_result_special_list[$key_date]+$nosp_result_time_list[$key_date];
        $expected_sales     = ($sales_total > 0) ? ($cost_total/$sales_total)*100 : 0;
        $roas_percent       = ($cost_total > 0) ? ($sales_total/$cost_total)*100 : 0;

        $net_sales_sum_list[$key_date]  = $sales_total;
        $net_cost_sum_list[$key_date]   = $cost_total;
        $expected_sales_list[$key_date] = $expected_sales;
        $roas_percent_list[$key_date]   = $roas_percent;
    }
}

# NET 매출 관련 작업
foreach($brand_company_list as $c_no)
{
    $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$sch_net_date}' ORDER BY sales_date DESC LIMIT 1;";
    $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
    $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
    $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
    $net_parent             = isset($net_pre_percent_result['sales_date']) ? $net_pre_percent_result['sales_date'] : 0;

    $net_parent_percent_list[$c_no] = $net_pre_percent;

    $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date, {$add_date_column} as key_date FROM commerce_report_net WHERE brand='{$c_no}' {$add_date_where} ORDER BY sales_date ASC";
    $net_report_query       = mysqli_query($my_db, $net_report_sql);
    while($net_report = mysqli_fetch_assoc($net_report_query))
    {
        $net_report_date_list[$c_no][$net_report['key_date']][$net_report['sales_date']] = $net_report['percent'];
    }
}

$net_report_list            = [];
$net_report_cal_list        = [];
$net_report_profit_list     = [];
$net_report_profit_per_list = [];

foreach($net_report_date_list as $c_no => $net_company_data)
{
    $net_pre_percent = $net_parent_percent_list[$c_no];
    $parent_key      = $company_chk_list[$c_no];

    foreach($net_company_data as $key_date => $net_report_data)
    {
        if($net_report_data)
        {
            foreach($net_report_data as $sales_date => $net_percent_val)
            {
                if($net_percent_val > 0 ){
                    $net_pre_percent = $net_percent_val;
                }

                $net_percent = $net_pre_percent/100;
                $sales_total = round($sales_tmp_total_list[$c_no][$key_date][$sales_date],0);
                $cost_total  = $cost_tmp_total_list[$c_no][$key_date][$sales_date];
                $net_price   = $sales_total > 0 ? (int)($sales_total*$net_percent) : 0;
                $profit      = $cost_total > 0 ? ($net_price-$cost_total) : $net_price;

                $net_report_cal_list[$key_date]    += (int)$net_price;
                $net_report_profit_list[$key_date] += $profit;

                $net_report_list[$key_date][$sales_date] = array('percent' => $net_pre_percent, 'parent' => ($net_percent_val > 0) ? 0 : $net_parent);
            }
        }else{
            $net_report_cal_list[$key_date]         += 0;
            $net_report_profit_list[$key_date]      += 0;
        }
    }
}

foreach($net_report_profit_list as $key_date => $net_profit)
{
    /**
    $cms_total_sum = round($cms_report_subtotal_sum_list[$key_date]);

    $cms_report_subtotal_sum_list[$key_date] = $cms_total_sum;
    if($cms_total_sum > 0){
        $net_report_profit_per_list[$key_date] = round($net_profit/$cms_total_sum * 100, 2);
    }else{
        $net_report_profit_per_list[$key_date] = 0;
    }
    */

    $sales_total_sum = $net_sales_sum_list[$key_date];

    if($sales_total_sum > 0){
        $net_report_profit_per_list[$key_date] = round($net_profit/$sales_total_sum * 100, 2);
    }else{
        $net_report_profit_per_list[$key_date] = 0;
    }
}


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$workSheet = $objPHPExcel->setActiveSheetIndex(0);
$eng_abc  = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
$eng_abcc = [];
$eng_list = $eng_abc;
foreach($eng_abc as $a){
    foreach($eng_abc as $b){
        $eng_list[] = $a.$b;
        $eng_abcc[] = $a.$b;
    }
}

foreach($eng_abcc as $aa){
    foreach($eng_abc as $b){
        $eng_list[] = $aa.$b;
    }
}

$eng_idx = 2;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$fontBlackColor = new PHPExcel_Style_Color();
$fontBlackColor->setRGB('0000000');
$background_color = "00262626";
$empty_data_idx   = [];
$excel_title_idx  = [];
if(!empty($commerce_date_list))
{
    $row_idx = 1;
    $workSheet->mergeCells("A{$row_idx}:B{$row_idx}");
    $workSheet->setCellValue("A{$row_idx}", "비용(자동)");

    $workSheet->getColumnDimension("A")->setWidth(20);
    $workSheet->getColumnDimension("B")->setWidth(20);
    foreach($commerce_date_list as $date_label)
    {
        $workSheet->setCellValue("{$eng_list[$eng_idx]}{$row_idx}", $date_label['title']);
        $workSheet->getColumnDimension("{$eng_list[$eng_idx]}")->setWidth(20);
        $eng_idx++;
    }
    $eng_idx--;
    $objPHPExcel->getActiveSheet()->getStyle("C1:{$eng_list[$eng_idx]}{$row_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
    $objPHPExcel->getActiveSheet()->getStyle("C1:{$eng_list[$eng_idx]}{$row_idx}")->getFont()->setColor($fontColor);
    $objPHPExcel->getActiveSheet()->getStyle("A1:{$eng_list[$eng_idx]}{$row_idx}")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A1:{$eng_list[$eng_idx]}{$row_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $row_idx++;

    $workSheet->mergeCells("A{$row_idx}:B{$row_idx}");
    $workSheet->setCellValue("A{$row_idx}", "아임웹_베라베프(쿠폰)");
    if(!empty($coupon_imweb_report_total_list)){
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $coupon_imweb_report_total_list[$comm_date]["belabef"]);
            $data_eng_idx++;
        }
    }
    $row_idx++;

    $workSheet->mergeCells("A{$row_idx}:B{$row_idx}");
    $workSheet->setCellValue("A{$row_idx}", "아임웹_닥터피엘(쿠폰)");
    if(!empty($coupon_imweb_report_total_list)){
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $coupon_imweb_report_total_list[$comm_date]["doc"]);
            $data_eng_idx++;
        }
    }
    $row_idx++;

    $workSheet->mergeCells("A{$row_idx}:B{$row_idx}");
    $workSheet->setCellValue("A{$row_idx}", "아임웹_아이레놀(쿠폰)");
    if(!empty($coupon_imweb_report_total_list)){
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $coupon_imweb_report_total_list[$comm_date]["ilenol"]);
            $data_eng_idx++;
        }
    }
    $row_idx++;

    $workSheet->mergeCells("A{$row_idx}:B{$row_idx}");
    $workSheet->setCellValue("A{$row_idx}", "아임웹_누잠(쿠폰)");
    if(!empty($coupon_imweb_report_total_list)){
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $coupon_imweb_report_total_list[$comm_date]["nuzam"]);
            $data_eng_idx++;
        }
    }
    $row_idx++;

    $workSheet->mergeCells("A{$row_idx}:B{$row_idx}");
    $workSheet->setCellValue("A{$row_idx}", "스마트스토어(쿠폰)");
    if(!empty($coupon_smart_report_total_list)){
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $coupon_smart_report_total_list[$comm_date]);
            $data_eng_idx++;
        }
    }
    $row_idx++;

    $workSheet->mergeCells("A{$row_idx}:B{$row_idx}");
    $workSheet->setCellValue("A{$row_idx}", "무료배송비");
    if (!empty($cms_delivery_free_list))
    {
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $cms_delivery_free_total_list[$comm_date]);
            $data_eng_idx++;
        }
    }
    $row_idx++;

    $workSheet->mergeCells("A{$row_idx}:B{$row_idx}");
    $workSheet->setCellValue("A{$row_idx}", "NOSP(스폐셜DA)");
    if (!empty($cms_delivery_free_list))
    {
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $nosp_result_special_list[$comm_date]);
            $data_eng_idx++;
        }
    }
    $row_idx++;

    $workSheet->mergeCells("A{$row_idx}:B{$row_idx}");
    $workSheet->setCellValue("A{$row_idx}", "NOSP(타임보드)");
    if (!empty($cms_delivery_free_list))
    {
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$row_idx}", $nosp_result_time_list[$comm_date]);
            $data_eng_idx++;
        }
    }
    $row_idx++;

    $empty_data_idx[]  = $row_idx;
    $row_idx++;
    $excel_title_idx[] = $row_idx;

    $workSheet->mergeCells("A{$row_idx}:B{$row_idx}");
    $workSheet->setCellValue("A{$row_idx}", "비용(수기입력)");
    $row_idx++;

    $data_idx = $row_idx;
    /** 지출 리포트 */
    if(!empty($cost_report_list) || !empty($cost_report_total_sum_list))
    {
        if(!empty($cost_report_list))
        {
            foreach($cost_report_list as $comm_g1 => $cost_report_data)
            {
                if($cost_report_data)
                {
                    $rowspan     = count($cost_report_data);
                    $comm_g1_idx = 1;
                    foreach ($cost_report_data as $comm_g2 => $cost_report)
                    {
                        if($comm_g1_idx == 1){
                            $start_data_idx = $data_idx;
                            $end_data_idx   = ($data_idx+$rowspan)-1;
                            $workSheet->mergeCells("A{$start_data_idx}:A{$end_data_idx}");
                            $workSheet->setCellValue("A{$start_data_idx}",$comm_g1_title_list[$comm_g1]['title']);
                        }

                        $workSheet->setCellValue("B{$data_idx}", $comm_g2_title_list[$comm_g1][$comm_g2]['title']);

                        $data_eng_idx = 2;
                        foreach($commerce_date_list as $comm_date => $date_label)
                        {
                            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}",$cost_report[$comm_date]['price']);
                            $data_eng_idx++;
                        }
                        $data_idx++;

                        if($comm_g1_idx == $rowspan){
                            $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
                            $workSheet->setCellValue("A{$data_idx}", $comm_g1_title_list[$comm_g1]['title']." 합계");

                            $data_eng_idx = 2;
                            foreach($cost_report_total_list[$comm_g1] as $date_total_price)
                            {
                                $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $date_total_price);
                                $data_eng_idx++;
                            }
                            $data_idx++;
                        }

                        $comm_g1_idx++;
                    }
                }
            }
        }

        if(!empty($cost_report_total_sum_list))
        {
            $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
            $workSheet->setCellValue("A{$data_idx}", "지출 총 합계");
            $workSheet->getRowDimension($data_idx)->setRowHeight(30);

            $data_eng_idx = 2;
            foreach($commerce_date_list as $comm_date => $date_label)
            {
                $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $cost_report_total_sum_list[$comm_date]);
                $data_eng_idx++;
            }
            $data_idx++;
        }
    }

    /** 매출 리포트 */
    if(!empty($cms_report_list) || !empty($quick_report_list))
    {
        if (!empty($coupon_imweb_report_total_list) || !empty($cost_report_list) || !empty($cost_report_total_sum_list)) {
            $empty_data_idx[] = $data_idx;
            $data_idx++;

            $excel_title_idx[] = $data_idx;
            $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
            $workSheet->setCellValue("A{$data_idx}", "매출(자동)");
            $data_idx++;
        }

        if(!empty($cms_report_list))
        {
            foreach ($cms_report_list as $dp_c_no => $cms_report)
            {
                $data_next_idx = $data_idx+1;
                $dp_c_name     = isset($commerce_fee_list[$dp_c_no]) ? $cms_report_title_list[$dp_c_no]."({$commerce_fee_list[$dp_c_no]}%)" : $cms_report_title_list[$dp_c_no];
                $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
                $workSheet->mergeCells("A{$data_next_idx}:B{$data_next_idx}");
                $workSheet->setCellValue("A{$data_idx}", $dp_c_name);
                $workSheet->setCellValue("A{$data_next_idx}", $cms_report_title_list[$dp_c_no]."(배송비)");

                $data_eng_idx = 2;
                foreach($commerce_date_list as $comm_date => $date_label)
                {
                    $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}",$cms_report[$comm_date]["subtotal"]);
                    $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_next_idx}",$cms_report[$comm_date]["delivery"]);
                    $data_eng_idx++;
                }
                $data_idx += 2;
            }
        }

        if(!empty($quick_report_list))
        {
            foreach ($quick_report_list as $run_c_no => $quick_report)
            {
                $run_c_name    = "(입/출고요청) ".$quick_report_title_list[$run_c_no];
                $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
                $workSheet->setCellValue("A{$data_idx}", $run_c_name);

                $data_eng_idx = 2;
                foreach($commerce_date_list as $comm_date => $date_label)
                {
                    $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}",$quick_report[$comm_date]);
                    $data_eng_idx++;
                }
                $data_idx++;
            }
        }

        if(!empty($cms_report_total_sum_list))
        {
            $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
            $workSheet->setCellValue("A{$data_idx}", "상품 매출 총 합계(수수료 제외)");
            $workSheet->getRowDimension($data_idx)->setRowHeight(30);

            $data_eng_idx = 2;
            foreach($commerce_date_list as $comm_date => $date_label)
            {
                $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $cms_report_subtotal_sum_list[$comm_date]);
                $data_eng_idx++;
            }
            $data_idx++;

            $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
            $workSheet->setCellValue("A{$data_idx}", "배송비 매출 총 합계");
            $workSheet->getRowDimension($data_idx)->setRowHeight(30);

            $data_eng_idx = 2;
            foreach($commerce_date_list as $comm_date => $date_label)
            {
                $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $cms_report_delivery_sum_list[$comm_date]);
                $data_eng_idx++;
            }
            $data_idx++;

            $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
            $workSheet->setCellValue("A{$data_idx}", "매출 총 합계");
            $workSheet->getRowDimension($data_idx)->setRowHeight(30);

            $data_eng_idx = 2;
            foreach($commerce_date_list as $comm_date => $date_label)
            {
                $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $cms_report_total_sum_list[$comm_date]);
                $data_eng_idx++;
            }
            $data_idx++;
        }
    }

    /** 매출 리포트 */
    if(!empty($sales_report_list) || !empty($sales_report_total_sum_list))
    {
        if(!empty($coupon_imweb_report_total_list) || !empty($cost_report_list) || !empty($cost_report_total_sum_list) || !empty($cms_report_list) || !empty($cms_report_total_sum_list))
        {
            $empty_data_idx[] = $data_idx;
            $data_idx++;

            $excel_title_idx[] = $data_idx;
            $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
            $workSheet->setCellValue("A{$data_idx}", "매출(수기입력)");
            $data_idx++;
        }

        if(!empty($sales_report_list))
        {
            foreach($sales_report_list as $comm_g1 => $sales_report_data)
            {
                if($sales_report_data)
                {
                    $rowspan     = count($sales_report_data);
                    $comm_g1_idx = 1;
                    foreach ($sales_report_data as $comm_g2 => $sales_report)
                    {
                        if($comm_g1_idx == 1){
                            $start_data_idx = $data_idx;
                            $end_data_idx   = ($data_idx+$rowspan)-1;
                            $workSheet->mergeCells("A{$start_data_idx}:A{$end_data_idx}");
                            $workSheet->setCellValue("A{$start_data_idx}",$comm_g1_title_list[$comm_g1]['title']);
                        }

                        $workSheet->setCellValue("B{$data_idx}", $comm_g2_title_list[$comm_g1][$comm_g2]['title']);

                        $data_eng_idx = 2;
                        foreach($commerce_date_list as $comm_date => $date_label)
                        {
                            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}",$sales_report[$comm_date]['price']);
                            $data_eng_idx++;
                        }
                        $data_idx++;

                        if($comm_g1_idx == $rowspan){
                            $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
                            $workSheet->setCellValue("A{$data_idx}", $comm_g1_title_list[$comm_g1]['title']." 합계");

                            $data_eng_idx = 2;
                            foreach($sales_report_total_list[$comm_g1] as $date_total_price)
                            {
                                $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $date_total_price);
                                $data_eng_idx++;
                            }
                            $data_idx++;
                        }

                        $comm_g1_idx++;
                    }
                }
            }
        }

        if(!empty($sales_report_total_sum_list))
        {
            $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
            $workSheet->setCellValue("A{$data_idx}", "매출 총 합계");
            $workSheet->getRowDimension($data_idx)->setRowHeight(30);

            $data_eng_idx = 2;
            foreach($commerce_date_list as $comm_date => $date_label)
            {
                $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $sales_report_total_sum_list[$comm_date]);
                $data_eng_idx++;
            }
            $data_idx++;
        }
    }

    // NET 매출, 차익
    if(!empty($net_report_list))
    {
        if((!empty($cost_report_list) || !empty($cost_report_total_sum_list)) || (!empty($cms_report_list) || !empty($cms_report_total_sum_list)) || (!empty($sales_report_list) || !empty($sales_report_total_sum_list)))
        {
            $empty_data_idx[] = $data_idx;
            $data_idx++;
        }

        $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
        $workSheet->setCellValue("A{$data_idx}", "지출 총 합계(+쿠폰 +무료배송비) +광고비");
        $workSheet->getRowDimension($data_idx)->setRowHeight(30);
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $net_cost_sum_list[$comm_date]);
            $data_eng_idx++;
        }
        $data_idx++;

        $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
        $workSheet->setCellValue("A{$data_idx}", "매출 총 합계(상품 매출(수수료 제외) + 매출(수기))");
        $workSheet->getRowDimension($data_idx)->setRowHeight(30);
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $net_sales_sum_list[$comm_date]);
            $data_eng_idx++;
        }
        $data_idx++;

        $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
        $net_title = "NET (".$sch_brand_name.")";
        $workSheet->setCellValue("A{$data_idx}", $net_title);
        $workSheet->getRowDimension($data_idx)->setRowHeight(30);
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            if($sch_brand){
                if ($sch_date_type == '1'){
                    foreach ($net_report_list[$comm_date] as $sales_date => $net_report){
                        $percent = empty($net_report['percent']) ? 0 : $net_report['percent'];

                        $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $percent."%");
                        $data_eng_idx++;
                    }
                }else{
                    $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", "-");
                    $data_eng_idx++;
                }
            }else{
                $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", "-");
                $data_eng_idx++;
            }
        }
        $data_idx++;

        $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
        $workSheet->setCellValue("A{$data_idx}", "NET 매출");
        $workSheet->getRowDimension($data_idx)->setRowHeight(30);
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $net_report_cal_list[$comm_date]);
            $data_eng_idx++;
        }
        $data_idx++;

        $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
        $workSheet->setCellValue("A{$data_idx}", "예산지출비용");
        $workSheet->getRowDimension($data_idx)->setRowHeight(30);
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $expected_sales = round($expected_sales_list[$comm_date]);
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $expected_sales."%");
            $data_eng_idx++;
        }
        $data_idx++;

        $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
        $workSheet->setCellValue("A{$data_idx}", "ROAS");
        $workSheet->getRowDimension($data_idx)->setRowHeight(30);
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $roas = round($roas_percent_list[$comm_date]);
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $roas."%");
            $data_eng_idx++;
        }
        $data_idx++;

        $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
        $workSheet->setCellValue("A{$data_idx}", "이익률");
        $workSheet->getRowDimension($data_idx)->setRowHeight(30);
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $profit_per = round($net_report_profit_per_list[$comm_date], 2);
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $profit_per."%");
            $data_eng_idx++;
        }
        $data_idx++;

        $workSheet->mergeCells("A{$data_idx}:B{$data_idx}");
        $workSheet->setCellValue("A{$data_idx}", "차액 (공헌이익)");
        $workSheet->getRowDimension($data_idx)->setRowHeight(30);
        $data_eng_idx = 2;
        foreach($commerce_date_list as $comm_date => $date_label)
        {
            $workSheet->setCellValue("{$eng_list[$data_eng_idx]}{$data_idx}", $net_report_profit_list[$comm_date]);
            $data_eng_idx++;
        }
    }
}

$last_field = $eng_list[$eng_idx].$data_idx;
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_field}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A2:B{$data_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A2:B{$data_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
$objPHPExcel->getActiveSheet()->getStyle("A2:B{$data_idx}")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A2:B{$data_idx}")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A2:B{$data_idx}")->applyFromArray(
    array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('rgb' => 'FFFFFF')
            )
        )
    )
);

$objPHPExcel->getActiveSheet()->getStyle("C2:{$last_field}")->getNumberFormat()->setFormatCode('#,##0');
$objPHPExcel->getActiveSheet()->getStyle("C2:{$last_field}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("A2:{$last_field}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


if(!empty($excel_title_idx)){
    foreach($excel_title_idx as $title_idx){
        $objPHPExcel->getActiveSheet()->getStyle("A{$title_idx}:B{$title_idx}")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle("A{$title_idx}:B{$title_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FFFFFF');
        $objPHPExcel->getActiveSheet()->getStyle("A{$title_idx}")->getFont()->setColor($fontBlackColor);
    }
}

if(!empty($empty_data_idx)){
    foreach($empty_data_idx as $emp_idx){
        $objPHPExcel->getActiveSheet()->getStyle("A{$emp_idx}:B{$emp_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FFFFFF');
        $objPHPExcel->getActiveSheet()->getRowDimension($emp_idx)->setRowHeight(25);
    }
}

$objPHPExcel->getActiveSheet()->setTitle("커머스 매출현황");

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_커머스매출현황.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

?>
