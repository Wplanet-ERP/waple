<?php
require('inc/common.php');
require('ckadmin.php');

$s_no    = isset($_GET['s_no']) ? $_GET['s_no'] : (isset($_POST['s_no']) ? $_POST['s_no'] : "");

if(empty($s_no)){
    exit("<script>alert('s_no 값이 없습니다.'); history.back();</script>");
}else{

    $process = isset($_POST['process']) ? $_POST['process'] : "";
    if($process == "add_mail_signature")
    {
        $sms_no     = isset($_POST['sms_no']) ? $_POST['sms_no'] : "";
        $s_no       = isset($_POST['s_no']) ? $_POST['s_no'] : "";
        $eng_name   = isset($_POST['eng_name']) ? addslashes(trim($_POST['eng_name'])) : "";
        $kor_name   = isset($_POST['kor_name']) ? addslashes(trim($_POST['kor_name'])) : "";
        $position   = isset($_POST['position']) ? trim($_POST['position']) : "";
        $team       = isset($_POST['team']) ? $_POST['team'] : "";
        $team_parent= isset($_POST['team_parent']) ? $_POST['team_parent'] : "";
        $hp         = isset($_POST['hp']) ? addslashes(trim($_POST['hp'])) : "";
        $tel        = isset($_POST['tel']) ? addslashes(trim($_POST['tel'])) : "";
        $com_tel    = isset($_POST['com_tel']) ? addslashes(trim($_POST['com_tel'])) : "";
        $fax        = isset($_POST['fax']) ? addslashes(trim($_POST['fax'])) : "";
        $email      = isset($_POST['email']) ? addslashes(trim($_POST['email'])) : "";
        $c_no       = isset($_POST['c_no']) ? trim($_POST['c_no']) : "";
        $eng_c_name = isset($_POST['eng_c_name']) ? addslashes(trim($_POST['eng_c_name'])) : "";
        $kor_c_name = isset($_POST['kor_c_name']) ? addslashes(trim($_POST['kor_c_name'])) : "";
        $c_addr     = isset($_POST['c_addr']) ? addslashes(trim($_POST['c_addr'])) : "";
        $url        = isset($_POST['url']) ? addslashes(trim($_POST['url'])) : "";


        if(!empty($sms_no)){
            $upd_sql = "UPDATE staff_mail_signature SET eng_name='{$eng_name}', kor_name='{$kor_name}', `position`='{$position}', team='{$team}', team_parent='{$team_parent}', hp='{$hp}', tel='{$tel}', com_tel='{$com_tel}', fax='{$fax}', email='{$email}', c_no='{$c_no}', eng_c_name='{$eng_c_name}', kor_c_name='{$kor_c_name}', c_addr='{$c_addr}', url='{$url}' WHERE sms_no='{$sms_no}'";
        }else{
            $upd_sql = "INSERT INTO staff_mail_signature SET s_no='{$s_no}', eng_name='{$eng_name}', kor_name='{$kor_name}', `position`='{$position}', team='{$team}', team_parent='{$team_parent}', hp='{$hp}', tel='{$tel}', com_tel='{$com_tel}', fax='{$fax}', email='{$email}', c_no='{$c_no}', eng_c_name='{$eng_c_name}', kor_c_name='{$kor_c_name}', c_addr='{$c_addr}', url='{$url}'";
        }

        if(mysqli_query($my_db, $upd_sql)){
            exit("<script>location.href='staff_regist_mail_signature_view.php?s_no={$s_no}';</script>");
        }else {
            exit("<script>alert('저장에 실패했습니다');history.back();</script>");
        }
    }
    else
    {
        $mail_sign_sql    = "SELECT *, (SELECT t.team_name FROM team t WHERE t.team_code=sms.team) as team_name, (SELECT t.team_name FROM team t WHERE t.team_code=sms.team_parent) as team_parent_name FROM staff_mail_signature sms WHERE sms.s_no='{$s_no}' LIMIT 1";
        $mail_sign_query  = mysqli_query($my_db, $mail_sign_sql);
        $mail_sign_result = mysqli_fetch_assoc($mail_sign_query);

        $mail_signature = $mail_sign_result;
        $content = "";

        if($mail_sign_result['c_no'] == '3')
        {
            $content = "<hr size='3px' width='100%' color='#2951ac'>
<p style='margin: 22px 0 10px 0;'><img src='https://work.wplanet.co.kr/sign/wmc_symbol.png' inlineid='undefined'></p>
<span style='margin-bottom: 8px; color: rgb(41, 81, 172);font-family: 나눔고딕, NanumGothic, sans-serif;'>
    <p style='font-weight: bold; font-size: 17pt; letter-spacing: -1px;'>{$mail_signature['eng_name']}<span style='font-size: 12pt; font-weight: normal;'>&nbsp;|&nbsp;{$mail_signature['kor_name']}</span></p>";

            if($mail_sign_result['team_parent_name']){
                $content .= "<p style='font-size: 11pt; margin-bottom: 8px;'>{$mail_signature['position']} | {$mail_signature['team_parent_name']}, {$mail_signature['team_name']}</p>";
            }else{
                $content .= "<p style='font-size: 11pt; margin-bottom: 8px;'>{$mail_signature['position']} | {$mail_signature['team_name']}</p>";
            }

$content .= "</span>
<span style='line-height: 17px; color: rgb(41, 81, 172); font-family: 나눔고딕, NanumGothic, sans-serif;'>
    <p style='line-height: 17px;'>
";

            if(!empty($mail_signature['hp'])){
                $content .= "        <span style='font-weight: bold;'>MOBILE</span>/ {$mail_signature['hp']}&nbsp;&nbsp;
";
            }

            if(!empty($mail_signature['tel'])){
                $content .= "        <span style='font-weight: bold;'>TEL</span>/ {$mail_signature['tel']}";
            }
            $content .= "
    </p>

    <p style='line-height: 17px;'>
        <span style='font-weight: bold;'>E-MAIL</span>/ {$mail_signature['email']}
    </p>

    <p style='margin: 2px 0;'><br></p>

    <p style='line-height: 17px; margin-bottom: 8px; font-size: 16px; letter-spacing: -1px;'>
        <span style='font-weight: bold;'>{$mail_signature['eng_c_name']}</span>&nbsp;|&nbsp;{$mail_signature['kor_c_name']}
    </p>

    <p style='line-height: 17px; font-size: 13px; margin-bottom: 2px;'>
        <span style='font-weight: bold;'>ADDRESS</span>/ {$mail_signature['c_addr']}
    </p>

    <p style='line-height: 17px; font-size: 13px; margin-bottom: 2px;'>
        <span style='font-weight: bold;'>TEL</span>/ {$mail_signature['com_tel']}&nbsp;&nbsp;
        <span style='font-weight: bold;'>FAX</span>/ {$mail_signature['fax']}&nbsp;&nbsp;
        <span style='font-weight: bold;'>OFFICIAL</span>/ <a style='color: rgb(41, 81, 172);' href='http://www.belabef.com' each='_blank' target='_blank'>www.belabef.com</a>
    </p>
</span>
<p style='margin:22px 0 0 0;'></p>
<hr size='1px' width='100%' color='#2951ac'>
";

        }
        else
        {
            $content = "<hr size='3px' width='100%' color='#444444'>
<p style='margin: 22px 0 10px 0;'><img src='https://work.wplanet.co.kr/sign/wp_symbol.png' inlineid='undefined'></p>
<span style='margin-bottom: 8px; color: rgb(68, 68, 68);font-family: 나눔고딕, NanumGothic, sans-serif;'>
    <p style='font-weight: bold; font-size: 17pt; letter-spacing: -1px;'>{$mail_signature['eng_name']}<span style='font-size: 12pt; font-weight: normal;'>&nbsp;|&nbsp;{$mail_signature['kor_name']}</span></p>";

            if($mail_sign_result['team_parent_name']){
                $content .= "<p style='font-size: 11pt; margin-bottom: 8px;'>{$mail_signature['position']} | {$mail_signature['team_parent_name']}, {$mail_signature['team_name']}</p>";
            }else{
                $content .= "<p style='font-size: 11pt; margin-bottom: 8px;'>{$mail_signature['position']} | {$mail_signature['team_name']}</p>";
            }
            
            $content .= "</span>
<span style='line-height: 17px; color: rgb(68, 68, 68); font-family: 나눔고딕, NanumGothic, sans-serif;'>
    <p style='line-height: 17px;'>
";
            if(!empty($mail_signature['hp'])){
                $content .= "        <span style='font-weight: bold;'>MOBILE</span>/ {$mail_signature['hp']}&nbsp;&nbsp;
";
            }

            if(!empty($mail_signature['tel'])){
                $content .= "        <span style='font-weight: bold;'>TEL</span>/ {$mail_signature['tel']}";
            }

            $content .= "
    </p>
    <p style='line-height: 17px;'>
        <span style='font-weight: bold;'>E-MAIL</span>/ {$mail_signature['email']}
    </p>
    <p style='margin: 2px 0;'><br></p>
    <p style='line-height: 17px; margin-bottom: 8px; font-size: 16px; letter-spacing: -1px;'>
        <span style='font-weight: bold;'>{$mail_signature['eng_c_name']}</span>&nbsp;|&nbsp;{$mail_signature['kor_c_name']}
    </p>
    <p style='line-height: 17px; font-size: 13px; margin-bottom: 2px;'>
        <span style='font-weight: bold;'>ADDRESS</span>/ {$mail_signature['c_addr']}
    </p>
    <p style='line-height: 17px; font-size: 13px; margin-bottom: 2px;'>
        <span style='font-weight: bold;'>TEL</span>/ 02-830-1912&nbsp;&nbsp;
        <span style='font-weight: bold;'>FAX</span>/ 02-830-1913&nbsp;&nbsp;
        <span style='font-weight: bold;'>OFFICIAL</span>/ <a style='color: rgb(68, 68, 68);' href='http://www.wplanet.co.kr' each='_blank' target='_blank'>www.wplanet.co.kr</a>
    </p>
</span>
<p style='margin:22px 0 0 0;'></p>
<hr size='1px' width=100%' color='#444444'>
";
        }

        $smarty->assign("mail_content", $content);
    }
    $smarty->display('staff_regist_mail_signature_view.html');
}
?>
