<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/company.php');
require('Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);


$lfcr = chr(10) ;

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "NO")
    ->setCellValue('B1', "DISPLAY")
    ->setCellValue('C1', "계열사")
    ->setCellValue('D1', "법인/개인")
    ->setCellValue('E1', "ID")
    ->setCellValue('F1', "파트너구분")
    ->setCellValue('G1', "업체명")
    ->setCellValue('H1', "사업자번호")
    ->setCellValue('I1', "사업자등록 업체명")
    ->setCellValue('J1', "담당자")
;

# 검색 조건
$add_where	        = "1=1";
$sch_display 		= isset($_GET['sch_display']) ? $_GET['sch_display'] : "";
$sch_my_c_no		= isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_license_type 	= isset($_GET['sch_license_type']) ? $_GET['sch_license_type'] : "";
$sch_partner_state	= isset($_GET['sch_partner_state']) ? $_GET['sch_partner_state'] : "";
$sch_corp_kind 		= isset($_GET['sch_corp_kind']) ? $_GET['sch_corp_kind'] : "";
$sch_c_name 		= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_s_name 		= isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
$sch_keyword 		= isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";
$sch_about 			= isset($_GET['sch_about']) ? $_GET['sch_about'] : "";
$sch_memo 			= isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
$sch_leave 			= isset($_GET['sch_leave']) ? $_GET['sch_leave'] : "";

if(!empty($sch_display))
{
    $add_where .= " AND c.display='{$sch_display}'";
}

if(!empty($sch_my_c_no))
{
    $add_where .= " AND c.my_c_no='{$sch_my_c_no}'";
}

if(!empty($sch_license_type))
{
    $add_where .= " AND c.license_type='{$sch_license_type}'";
}

if(!empty($sch_partner_state))
{
    $add_where .= " AND c.partner_state='{$sch_partner_state}'";
}

if(!empty($sch_corp_kind))
{
    $add_where .= " AND c.corp_kind='{$sch_corp_kind}'";
}

if(!empty($sch_c_name))
{
    $add_where .= " AND c.c_name like '%{$sch_c_name}%'";
}

if(!empty($sch_s_name))
{
    $add_where .= " AND c.s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_s_name}%')";
}

if(!empty($sch_keyword))
{
    $add_where .= " AND c.keyword like '%{$sch_keyword}%'";
}

if(!empty($sch_about))
{
    $add_where .= " AND c.about like '%{$sch_about}%'";
}

if(!empty($sch_memo))
{
    $add_where .= " AND c.c_memo like '%{$sch_memo}%'";
}

if(!empty($sch_leave))
{
    if($sch_leave == 'lector')
    {
        $lec_date   = date('Y-m-d', strtotime("-10 years"))." 23:59:59";
        $add_where .= " AND (SELECT per.pj_participation_date FROM project_external_report per WHERE per.pj_c_no=`c`.c_no AND per.pj_participation='1' AND per.active='1' ORDER BY per.pj_participation_date DESC LIMIT 1) <= '{$lec_date}'";
    }
    elseif($sch_leave == 'leave')
    {
        $lev_date   = date('Y-m-d', strtotime("-1 years"))." 23:59:59";
        $add_where .= " AND c.leave_date <= '{$lev_date}'";
    }
}

// 정렬순서 토글 & 필드 지정
$add_orderby = "`c`.c_no DESC";
$ord_type    = (isset($_GET['ord_type']) && !empty($_GET['ord_type'])) ? $_GET['ord_type'] : "";
$ord_type_by = (isset($_GET['ord_type_by']) && !empty($_GET['ord_type_by'])) ? $_GET['ord_type_by'] : "";

if(!empty($ord_type) && !empty($ord_type_by))
{
    if($ord_type_by == '1'){
        $add_orderby = "{$ord_type} ASC, `c`.c_no DESC";
    }elseif($ord_type_by == '2'){
        $add_orderby = "{$ord_type} DESC, `c`.c_no DESC";
    }else{
        $add_orderby = "`c`.c_no DESC";
    }
}

# 리스트 쿼리
$work_sheet         = $objPHPExcel->setActiveSheetIndex(0);
$idx                = 2;
$display_option     = getDisplayOption();
$license_option     = getLicenseTypeOption();
$corp_kind_option   = getCorpKindOption();

$company_sql = "
    SELECT
        `c`.*,
        (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=c.my_c_no) AS my_c_name,
        (select s_name from staff b where s_no=c.s_no) as s_name
    FROM company `c`
    WHERE {$add_where}
    ORDER BY {$add_orderby}
";
$company_query = mysqli_query($my_db, $company_sql);
while($company = mysqli_fetch_assoc($company_query))
{
    $display_text   = $display_option[$company['display']];
    $license_text   = $license_option[$company['license_type']];
    $corp_kind_text = $corp_kind_option[$company['corp_kind']];

    $work_sheet
        ->setCellValue("A{$idx}", $company['c_no'])
        ->setCellValue("B{$idx}", $display_text)
        ->setCellValue("C{$idx}", $company['my_c_name'])
        ->setCellValue("D{$idx}", $license_text)
        ->setCellValue("E{$idx}", $company['id'])
        ->setCellValue("F{$idx}", $corp_kind_text)
        ->setCellValue("G{$idx}", $company['c_name'])
        ->setCellValue("H{$idx}", $company['tx_company_number'])
        ->setCellValue("I{$idx}", $company['tx_company'])
        ->setCellValue("J{$idx}", $company['s_name'])
    ;

    $idx++;
}
$idx--;


$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF343A40');
$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFont()->setColor($fontColor);

$objPHPExcel->getActiveSheet()->getStyle("A1:J{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:J{$idx}")->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:Z{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A2:Z{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);


$excel_filename = "파트너리스트";
$objPHPExcel->getActiveSheet()->setTitle($excel_filename);
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.date("Ymd")."_{$excel_filename}.xlsx");

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save('php://output');
?>
