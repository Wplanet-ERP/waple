<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/asset.php');
require('inc/model/Kind.php');

# 검색 초기화 및 조건 생성
$add_where      = "1=1";
$sch_asset_g1   = isset($_GET['sch_asset_g1']) ? $_GET['sch_asset_g1'] : "";
$sch_asset_g2   = isset($_GET['sch_asset_g2']) ? $_GET['sch_asset_g2'] : "";
$sch_asset      = isset($_GET['sch_asset']) ? $_GET['sch_asset'] : "";
$sch_get        = isset($_GET['sch']) ? $_GET['sch'] : "";

$smarty->assign("sch_asset_g1", $sch_asset_g1);
$smarty->assign("sch_asset_g2", $sch_asset_g2);

if(!empty($sch_get)) {
    $smarty->assign("sch", $sch_get);
}

if (!empty($sch_asset)) {
    $add_where .= " AND as.name like '%{$sch_asset}%'";
    $smarty->assign("sch_asset", $sch_asset);
}

if($sch_asset_g2){
    $add_where .= " AND as.k_name_code ='{$sch_asset_g2}'";
}elseif($sch_asset_g1){
    $add_where .= " AND as.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent ='{$sch_asset_g1}')";
}

$kind_model       = Kind::Factory();
$asset_group_list = $kind_model->getKindGroupList("asset");
$asset_g1_list = $asset_g2_list = [];

foreach($asset_group_list as $key => $asset_data)
{
    if(!$key){
        $asset_g1_list = $asset_data;
    }else{
        $asset_g2_list[$key] = $asset_data;
    }
}
$sch_asset_g2_list = isset($asset_g2_list[$sch_asset_g1]) ? $asset_g2_list[$sch_asset_g1] : [];

$smarty->assign("asset_g1_list", $asset_g1_list);
$smarty->assign("asset_g2_list", $sch_asset_g2_list);


$sch_asset_state    = isset($_GET['sch_asset_state']) ? $_GET['sch_asset_state'] : "";
$sch_my_c_name      = isset($_GET['sch_my_c_name']) ? $_GET['sch_my_c_name'] : "";
$sch_keyword        = isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";
$sch_as_no          = isset($_GET['sch_as_no']) ? $_GET['sch_as_no'] : "";
$sch_management     = isset($_GET['sch_management']) ? $_GET['sch_management'] : "";
$sch_manager_name   = isset($_GET['sch_manager_name']) ? $_GET['sch_manager_name'] : "";
$sch_description    = isset($_GET['sch_description']) ? $_GET['sch_description'] : "";
$sch_use_type       = isset($_GET['sch_use_type']) ? $_GET['sch_use_type'] : "";
$sch_object_type    = isset($_GET['sch_object_type']) ? $_GET['sch_object_type'] : "";
$sch_share_type     = isset($_GET['sch_share_type']) ? $_GET['sch_share_type'] : "";
$sch_asset_loc      = isset($_GET['sch_asset_loc']) ? $_GET['sch_asset_loc'] : "";

if(!empty($sch_asset_state))
{
    $add_where .= " AND `as`.asset_state = '{$sch_asset_state}'";
    $smarty->assign('sch_asset_state', $sch_asset_state);
}

if(!empty($sch_my_c_name))
{
    $add_where .= " AND `as`.my_c_no IN(SELECT mc.my_c_no FROM my_company mc WHERE mc.c_name LIKE '%{$sch_my_c_name}%')";
    $smarty->assign('sch_my_c_name', $sch_my_c_name);
}

if(!empty($sch_keyword))
{
    $add_where .= " AND `as`.keyword like '%{$sch_keyword}%'";
    $smarty->assign('sch_keyword', $sch_keyword);
}

if(!empty($sch_as_no))
{
    $add_where .= " AND `as`.as_no = '{$sch_as_no}'";
    $smarty->assign('sch_as_no', $sch_as_no);
}

if(!empty($sch_management))
{
    $add_where .= " AND `as`.management LIKE '%{$sch_management}%'";
    $smarty->assign('sch_management', $sch_management);
}

if(!empty($sch_manager_name))
{
    $add_where .= " AND (`as`.manager_name LIKE '%{$sch_manager_name}%' OR `as`.sub_manager_name LIKE '%{$sch_manager_name}%')";
    $smarty->assign('sch_manager_name', $sch_manager_name);
}

if(!empty($sch_description))
{
    $add_where .= " AND `as`.description LIKE '%{$sch_description}%'";
    $smarty->assign('sch_description', $sch_description);
}

if(!empty($sch_use_type))
{
    $add_where .= " AND `as`.use_type = '{$sch_use_type}'";
    $smarty->assign('sch_use_type', $sch_use_type);
}

if(!empty($sch_object_type))
{
    $add_where .= " AND `as`.object_type = '{$sch_object_type}'";
    $smarty->assign('sch_object_type', $sch_object_type);
}

if(!empty($sch_share_type))
{
    $add_where .= " AND `as`.share_type = '{$sch_share_type}'";
    $smarty->assign('sch_share_type', $sch_share_type);
}

if(!empty($sch_asset_loc))
{
    $add_where .= " AND `as`.asset_loc LIKE '%{$sch_asset_loc}%'";
    $smarty->assign('sch_asset_loc', $sch_asset_loc);
}

# 전체 게시물수
$asset_total_sql	= "SELECT count(as_no) as cnt FROM (SELECT `as`.as_no FROM asset `as` WHERE {$add_where}) AS cnt";
$asset_total_query	= mysqli_query($my_db, $asset_total_sql);
$asset_total_result = mysqli_fetch_array($asset_total_query);
$asset_total        = $asset_total_result['cnt'];

# 페이징 처리
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 20;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($asset_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "asset_search.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $asset_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("search_url", $search_url);

# 리스트 쿼리
$asset_search_sql  = "
    SELECT 
        `as`.as_no,
        DATE_FORMAT(`as`.regdate, '%Y-%m-%d') as reg_date,
        DATE_FORMAT(`as`.regdate, '%H:%i') as reg_time,
        `as`.asset_state,
        `as`.my_c_no,
        (SELECT mc.c_name FROM my_company as mc WHERE mc.my_c_no = `as`.my_c_no) as my_c_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=`as`.k_name_code)) AS k_asset1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=`as`.k_name_code) AS k_asset2_name,
        `as`.`name`,
        `as`.management,
        `as`.manager,
        `as`.manager_name,
        `as`.sub_manager,
        `as`.sub_manager_name,
        `as`.description,
        `as`.use_type,
        `as`.share_type,
        `as`.object_type,
        `as`.non_description,
        `as`.object_url,
        `as`.object_site,
        `as`.object_id,
        `as`.object_pw,
        `as`.keyword,
        `as`.qr_code,
        `as`.asset_loc,
        `as`.img_name,
        `as`.img_path
    FROM asset `as` 
    WHERE {$add_where} 
    ORDER BY `as`.as_no DESC
    LIMIT {$offset}, {$num}
";
$asset_search_query         = mysqli_query($my_db, $asset_search_sql);
$asset_search_list          = [];
$asset_state_option         = getAssetStateOption();
$asset_use_type_option      = getAssetUseTypeOption();
$asset_share_type_option    = getAssetShareTypeOption();
$asset_object_type_option   = getAssetObjectTypeOption();
while($asset_search = mysqli_fetch_assoc($asset_search_query))
{
    $asset_state     = isset($asset_search['asset_state']) && !empty($asset_search['asset_state']) ? $asset_search['asset_state'] : "";
    $use_type        = isset($asset_search['use_type']) && !empty($asset_search['use_type']) ? $asset_search['use_type'] : "";
    $share_type      = isset($asset_search['share_type']) && !empty($asset_search['share_type']) ? $asset_search['share_type'] : "";
    $object_type     = isset($asset_search['object_type']) && !empty($asset_search['object_type']) ? $asset_search['object_type'] : "";
    $keyword_list    = isset($asset_search['keyword']) && !empty($asset_search['keyword']) ? explode(',', $asset_search['keyword']) : "";
    $keyword         = "";

    $asset_search['asset_state_name']  = !empty($asset_state) ? $asset_state_option[$asset_state]['label'] : "";
    $asset_search['asset_state_color'] = !empty($asset_state) ? $asset_state_option[$asset_state]['color'] : "black";
    $asset_search['use_type_name']     = !empty($use_type) ? $asset_use_type_option[$use_type] : "";
    $asset_search['share_type_name']   = !empty($share_type) ? $asset_share_type_option[$share_type] : "";
    $asset_search['object_type_name']  = !empty($object_type) ? $asset_object_type_option[$object_type] : "";
    $asset_search['description']       = str_replace("\r\n","<br />", $asset_search['description']);

    #키워드
    if(!empty($keyword_list))
    {
        $keyword = "#" . implode(' #', $keyword_list);
        if ($sch_keyword) {
            $keyword = str_replace($sch_keyword, "<span style='color:red;'>{$sch_keyword}</span>", $keyword);
        }
    }

    $asset_search['keyword'] = $keyword;
    $asset_search['asset_state_result'] = ($asset_state == '1') ? true : (($share_type == '2' && $asset_state == '3') ? true : false);

    #Qr코드 및 이미지
    $img_paths = $asset_search['img_path'];
    $img_names = $asset_search['img_name'];

    if(!empty($img_paths) && !empty($img_names))
    {
        $img_paths_arr = explode(',', $img_paths);
        $img_names_arr = explode(',', $img_names);
        $asset_search['first_img_path'] = $img_paths_arr[0];
        $asset_search['first_img_name'] = $img_names_arr[0];
    }else{
        $asset_search['first_img_path'] = "";
        $asset_search['first_img_name'] = "";
    }

    $asset_search_list[] = $asset_search;
}

$smarty->assign('asset_state_option', $asset_state_option);
$smarty->assign('asset_use_type_option', $asset_use_type_option);
$smarty->assign('asset_share_type_option', $asset_share_type_option);
$smarty->assign('asset_object_type_option', $asset_object_type_option);
$smarty->assign('asset_search_list', $asset_search_list);

$smarty->display('asset_search.html');
?>
