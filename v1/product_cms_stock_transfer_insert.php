<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');
require('inc/model/Custom.php');
require('Classes/PHPExcel.php');

# Model 설정
$unit_model     = ProductCmsUnit::Factory();
$stock_model    = ProductCmsStock::Factory();
$stock_model->setMainInit("product_cms_stock_transfer", 'no');
$stock_data     = [];

# 재고이동 Post
$transfer_file  = $_FILES["transfer_file"];
$log_c_no       = isset($_POST['transfer_warehouse']) ? $_POST['transfer_warehouse'] : 2809;
$with_log_c_no  = "2809";
$regdate        = date("Y-m-d H:i:s");

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($transfer_file['name']) && !empty($transfer_file['name'])){
    $upload_file_path = add_store_file($transfer_file, "upload_management");
    $upload_file_name = $transfer_file['name'];
}

$upload_data  = array(
    "upload_type"   => 12,
    "upload_kind"   => $log_c_no,
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $session_s_no,
    "regdate"       => $regdate,
);
$upload_model->insert($upload_data);

$file_no         = $upload_model->getInsertId();
$excel_file_path = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;

# 엑셀 파일 처리
$excelReader    = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($excel_file_path);
$excel->setActiveSheetIndex(0);
$objWorksheet   = $excel->getActiveSheet();
$totalRow       = $objWorksheet->getHighestRow();

$doc_no_cell    = $order_cell = $t_type_cell = $t_state_cell = $brand_cell = "";
$prd_kind_cell  = $sku_cell = $prd_name_cell = $out_warehouse_cell = $in_warehouse_cell = $qty_cell = "";
$eng_abc        = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

for ($i = 2; $i <= $totalRow; $i++)
{
    if($i == "2")
    {
        $highestColumn  = $objWorksheet->getHighestColumn();
        $titleRowData   = $objWorksheet->rangeToArray("A{$i}:".$highestColumn.$i, NULL, TRUE, FALSE);

        foreach ($titleRowData[0] as $key => $title)
        {
            switch($title){
                case "전표번호":
                    $doc_no_cell = $eng_abc[$key];
                    break;
                case "순번":
                    $order_cell = $eng_abc[$key];
                    break;
                case "이동형태":
                    $t_type_cell = $eng_abc[$key];
                    break;
                case "이동상태":
                    $t_state_cell = $eng_abc[$key];
                    break;
                case "브랜드":
                    $brand_cell = $eng_abc[$key];
                    break;
                case "대표코드":
                    $prd_kind_cell = $eng_abc[$key];
                    break;
                case "상품코드":
                    $sku_cell = $eng_abc[$key];
                    break;
                case "상품명":
                    $prd_name_cell = $eng_abc[$key];
                    break;
                case "반출창고":
                    $out_warehouse_cell = $eng_abc[$key];
                    break;
                case "반입창고":
                    $in_warehouse_cell = $eng_abc[$key];
                    break;
                case "이동수량":
                    $qty_cell = $eng_abc[$key];
                    break;
            }
        }
        continue;
    }

    if(
        empty($doc_no_cell) || empty($order_cell) || empty($t_type_cell) || empty($t_state_cell) || empty($brand_cell) || empty($prd_kind_cell)
        || empty($sku_cell) || empty($prd_name_cell) || empty($out_warehouse_cell) || empty($in_warehouse_cell) || empty($qty_cell)
    )
    {
        echo "변환되지 않는 필드 값이 있습니다. 개발팀 임태형에게 문의해주세요!";
        exit;
    }

    $prd_unit = $sup_c_no = $option_name = "";

    $move_date_val  = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //일자
    $move_date      = (!empty($move_date_val)) ? date('Y-m-d', strtotime($move_date_val)) : date('Y-m-d');
    $doc_no         = (string)trim(addslashes($objWorksheet->getCell("{$doc_no_cell}{$i}")->getValue()));   //전표번호
    $order          = (string)trim(addslashes($objWorksheet->getCell("{$order_cell}{$i}")->getValue()));   //순번
    $t_type         = (string)trim(addslashes($objWorksheet->getCell("{$t_type_cell}{$i}")->getValue()));   //이동형태
    $t_state        = (string)trim(addslashes($objWorksheet->getCell("{$t_state_cell}{$i}")->getValue()));   //이동상태
    $brand          = (string)trim(addslashes($objWorksheet->getCell("{$brand_cell}{$i}")->getValue()));   //브랜드
    $prd_kind       = (string)trim(addslashes($objWorksheet->getCell("{$prd_kind_cell}{$i}")->getValue()));   //대표코드
    $sku            = (string)trim(addslashes($objWorksheet->getCell("{$sku_cell}{$i}")->getValue()));   //상품코드
    $prd_name       = (string)trim(addslashes($objWorksheet->getCell("{$prd_name_cell}{$i}")->getValue()));   //상품명
    $out_warehouse  = (string)trim(addslashes($objWorksheet->getCell("{$out_warehouse_cell}{$i}")->getValue()));   //반출창고
    $in_warehouse   = (string)trim(addslashes($objWorksheet->getCell("{$in_warehouse_cell}{$i}")->getValue()));   //반입창고
    $qty            = (string)trim(addslashes($objWorksheet->getCell("{$qty_cell}{$i}")->getValue()));   //이동수량

    if(empty($sku)){
        continue;
    }

    if($qty < 0){
        $qty *= -1;
    }

    # 구성품값 가져오기기
    $unit_item = $unit_model->getSkuItem($sku, $with_log_c_no);
    if(!isset($unit_item['no']))
    {
        echo "Row : {$i}, 재고이동 데이터 반영에 실패하였습니다.<br>확인되지 않은 상품코드(SKU)가 있습니다.<br>
            일자     : {$move_date_val}<br>
            상품코드 : {$sku}<br>
            상품명   : {$prd_name}<br>
            ";
        exit;
    }else{
        $prd_unit    = $unit_item['prd_unit'];
        $sup_c_no    = $unit_item['sup_c_no'];
        $option_name = $unit_item['option_name'];
    }

    $ins_data = array(
        'move_date'     => $move_date,
        'file_no'       => $file_no,
        'doc_no'        => $doc_no,
        'order'         => $order,
        't_type'        => $t_type,
        't_state'       => $t_state,
        'brand'         => $brand,
        'prd_kind'      => $prd_kind,
        'prd_unit'      => $prd_unit,
        'sup_c_no'      => $sup_c_no,
        'option_name'   => $option_name,
        'prd_name'      => $prd_name,
        'out_log_c_no'  => $with_log_c_no,
        'out_warehouse' => $out_warehouse,
        'out_sku'       => $sku,
        'out_qty'       => $qty,
        'in_log_c_no'   => $with_log_c_no,
        'in_warehouse'  => $in_warehouse,
        'in_sku'        => $sku,
        'in_qty'        => $qty,
        'regdate'       => $regdate
    );

    $stock_data[] = $ins_data;
}

if (!$stock_model->multiInsert($stock_data)){
    echo "재고이동 반영에 실패하였습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영";
    exit;
}else{
    exit("<script>alert('재고이동 현황이 반영 되었습니다.');location.href='product_cms_stock_transfer_list.php';</script>");
}

?>
