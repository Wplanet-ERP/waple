<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/evaluation.php');
require('inc/model/Evaluation.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
	->setLastModifiedBy("Maarten Balliauw")
	->setTitle("Office 2007 XLSX Test Document")
	->setSubject("Office 2007 XLSX Test Document")
	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	->setKeywords("office 2007 openxml php")
	->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

function setBorder($sheet, $row)
{
	$sheet->getStyle($row)->applyFromArray(
		array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)
		)
	);
}

$ev_no 	= isset($_GET['ev_no']) ? $_GET['ev_no'] : "";

$eval_model				= Evaluation::Factory();
$ev_unit_question_list 	= $eval_model->getEvalQuestionList();
$alpha_option 			= getEvaluationAlphaOption();
$score_option			= getEvaluationScoreOption();
$score_title_option		= getEvaluationScoreTitleOption();

$evaluation_total_question_list = [];
$evaluation_total_receiver_list = [];
$evaluation_total_list 			= [];
$evaluation_data_list  			= [];

// 평가 시스템 및 평가 문항 쿼리
$evaluation_system_sql="
	SELECT
		ev_system.subject,
		ev_system.ev_s_date,
		ev_system.ev_e_date,
		ev_system.ev_u_set_no,
		(SELECT subject FROM evaluation_unit_set WHERE ev_u_set_no = ev_system.ev_u_set_no) AS ev_u_set_subject,
		ev_system.admin_s_no,
		(SELECT count(DISTINCT esr.ev_u_no) FROM evaluation_system_result esr WHERE esr.ev_no = ev_system.ev_no AND esr.evaluation_state='1') as question_cnt
	FROM evaluation_system ev_system
	WHERE ev_system.ev_no='{$ev_no}'
";
$evaluation_system_result=mysqli_query($my_db,$evaluation_system_sql);
$system_result 	= mysqli_fetch_array($evaluation_system_result);
$ev_subject  	= $system_result['subject'];
$question_cnt  	= $system_result['question_cnt'];
$excel_title 	= "평가지_{$ev_no}_평가결과";
$admin_s_no     = $system_result['admin_s_no'];

if(!(permissionNameCheck($session_permission, "대표") || $session_s_no == '28' || $admin_s_no == $session_s_no)){
	$smarty->display('access_error.html');
	exit;
}

$evaluation_receiver_sql   = "
	SELECT 
	    er.receiver_s_no,
	    er.receiver_team,
	    (SELECT t.priority FROM team t WHERE t.team_code=er.receiver_team) as t_priority
	FROM evaluation_relation er
	WHERE er.ev_no='{$ev_no}' AND er.rec_is_review='1' AND er.eval_is_review='1'
	GROUP BY er.receiver_s_no
	ORDER BY t_priority, receiver_team ASC
";
$evaluation_receiver_query = mysqli_query($my_db, $evaluation_receiver_sql);
$sheet_idx = 1;
while($evaluation_receiver = mysqli_fetch_assoc($evaluation_receiver_query))
{
	if($sheet_idx > 0){
		$objPHPExcel->createSheet($sheet_idx);
	}
	$sheet = $objPHPExcel->setActiveSheetIndex($sheet_idx);
	$sheet->getDefaultStyle()->getFont()
		->setName('나눔 고딕')
		->setSize(9);

	$objPHPExcel->setActiveSheetIndex($sheet_idx);

	$receiver_s_no 	= $evaluation_receiver['receiver_s_no'];
	$title 	 		= "";
	$subject 		= "";
	$unit_score_idx	= 0;
	$ev_u_list 		= [];
	$evaluation_unit_score_detail_total = 0;

	# 1. 종합만족도 조사 결과
	$evaluation_unit_score_sql = "
		SELECT
			ev_result.ev_u_no,
			ev_result.receiver_s_no,
			(SELECT team_name FROM team t WHERE t.team_code=(SELECT team FROM staff s WHERE s.s_no = ev_result.receiver_s_no)) as team_name,
			(SELECT s_name FROM staff s WHERE s.s_no = ev_result.receiver_s_no) as receiver_s_name,
			SUM(ev_result.evaluation_value) as ev_sum,
			COUNT(ev_result.evaluator_s_no) as ev_cnt
		FROM evaluation_system_result ev_result
		WHERE ev_result.ev_no='{$ev_no}' AND ev_result.evaluation_state='1' AND ev_result.receiver_s_no='{$receiver_s_no}'
		GROUP BY ev_result.ev_u_no
		ORDER BY `order` ASC, ev_u_no ASC
	";
	$evaluation_unit_score_query = mysqli_query($my_db, $evaluation_unit_score_sql);

	$ev_unit_score_total = 0;
	$ev_unit_total_alpha = "G";
	while($evaluation_unit_score = mysqli_fetch_assoc($evaluation_unit_score_query))
	{
		if(!$subject)
		{
			$subject = "1. {$ev_subject} {$evaluation_unit_score['team_name']}_{$evaluation_unit_score['receiver_s_name']}";
			$sheet_title = $evaluation_unit_score['receiver_s_name'];

			$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
			$objPHPExcel->getActiveSheet()->setCellValue("A1", $subject);
			$objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight(26);
			$objPHPExcel->getActiveSheet()->mergeCells("A3:G3");
			$objPHPExcel->getActiveSheet()->setCellValue("A3", "  ■ 종합 만족도 조사 결과");

			$objPHPExcel->getActiveSheet()->setCellValue("A4", "부문");
			$objPHPExcel->getActiveSheet()->setCellValue("A5", "{$evaluation_unit_score['team_name']}_{$evaluation_unit_score['receiver_s_name']}");
			$evaluation_unit_score_detail_total = $evaluation_unit_score['ev_cnt'];
			$evaluation_total_receiver_list[$evaluation_unit_score['receiver_s_no']] ="{$evaluation_unit_score['team_name']}-{$evaluation_unit_score['receiver_s_name']}";
			$unit_score_idx++;
		}

		$row_alpha 		= $alpha_option[$unit_score_idx];
		$unit_avg  		= sprintf('%0.2f', ($evaluation_unit_score['ev_sum']/$evaluation_unit_score['ev_cnt']));
		$ev_unit_score_total += $unit_avg;
		$objPHPExcel->getActiveSheet()->setCellValue("{$row_alpha}4", $ev_unit_question_list[$evaluation_unit_score['ev_u_no']]);
		$objPHPExcel->getActiveSheet()->getStyle("{$row_alpha}5")->getNumberFormat()->setFormatCode('0.00');
		$objPHPExcel->getActiveSheet()->setCellValue("{$row_alpha}5", $unit_avg);

		if($unit_score_idx == $question_cnt){
			$avg_title_idx 	= $question_cnt+1;
			$row_alpha 		= $alpha_option[$avg_title_idx];
			$total_avg		= sprintf('%0.3f',($ev_unit_score_total/$question_cnt));
			$objPHPExcel->getActiveSheet()->setCellValue("{$row_alpha}4", "전체 평균");
			$objPHPExcel->getActiveSheet()->getStyle("{$row_alpha}5")->getNumberFormat()->setFormatCode('0.000');
			$objPHPExcel->getActiveSheet()->setCellValue("{$row_alpha}5", $total_avg);
			$evaluation_total_list[$evaluation_unit_score['receiver_s_no']] = $total_avg;
			$ev_unit_total_alpha = $row_alpha;
		}

		$unit_score_idx++;

		$ev_u_list[] = $evaluation_unit_score['ev_u_no'];
		$evaluation_data_list[$evaluation_unit_score['receiver_s_no']][$evaluation_unit_score['ev_u_no']] = $unit_avg;
		$evaluation_total_question_list[$evaluation_unit_score['ev_u_no']] = $ev_unit_question_list[$evaluation_unit_score['ev_u_no']];
	}


	$objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(26);
	$objPHPExcel->getActiveSheet()->getStyle("{$ev_unit_total_alpha}4:{$ev_unit_total_alpha}5")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFBAEC94');
	setBorder($sheet, "A4:{$ev_unit_total_alpha}5");

	//세부 항목별 조사결과(응답자 수, 비율)
	$unit_score_detail_idx	= 8;
	$objPHPExcel->getActiveSheet()->mergeCells("A{$unit_score_detail_idx}:G{$unit_score_detail_idx}");
	$objPHPExcel->getActiveSheet()->setCellValue("A{$unit_score_detail_idx}", "2. 세부항목별 조사결과(응답자 수, 비율)");
	$objPHPExcel->getActiveSheet()->getRowDimension("{$unit_score_detail_idx}")->setRowHeight(26);
	$unit_score_detail_idx++;
	$unit_score_detail_idx++;

	$objPHPExcel->getActiveSheet()->setCellValue("A{$unit_score_detail_idx}", "구분");
	foreach($score_title_option as $key => $label){
		$objPHPExcel->getActiveSheet()->setCellValue("{$alpha_option[$key]}{$unit_score_detail_idx}", "{$label}");
	}
	$objPHPExcel->getActiveSheet()->setCellValue("G{$unit_score_detail_idx}", "표본");
	$objPHPExcel->getActiveSheet()->getRowDimension("{$unit_score_detail_idx}")->setRowHeight(26);
	$unit_score_detail_idx++;

	$evaluation_unit_score_detail_sql = "
		SELECT
			ev_result.ev_u_no,
			ev_result.evaluation_value,
			count(ev_result.evaluator_s_no) as ev_s_cnt
		FROM evaluation_system_result ev_result
		WHERE ev_result.ev_no='{$ev_no}' AND ev_result.evaluation_state='1' AND ev_result.receiver_s_no='{$receiver_s_no}' AND ev_result.evaluation_value > 0
		GROUP BY ev_result.ev_u_no, ev_result.evaluation_value 
		ORDER BY `order` ASC, ev_u_no ASC, evaluation_value ASC
	";
	$evaluation_unit_score_detail_query = mysqli_query($my_db, $evaluation_unit_score_detail_sql);
	$evaluation_unit_score_detail_list = [];

	# 세부항목 값 초기화
	foreach ($ev_u_list as $ev_u_no)
	{
		foreach($score_option as $score)
		{
			$evaluation_unit_score_detail_list[$ev_u_no][$score] = 0;
		}
	}

	# 세부항목 값 배분
	while($evaluation_unit_score_detail = mysqli_fetch_assoc($evaluation_unit_score_detail_query))
	{
		$evaluation_unit_score_detail_list[$evaluation_unit_score_detail['ev_u_no']][$evaluation_unit_score_detail['evaluation_value']] = $evaluation_unit_score_detail['ev_s_cnt'];
	}

	$detail_color_idx = 0;
	foreach($evaluation_unit_score_detail_list as $ev_u_no => $evaluation_unit_data)
	{
		$unit_score_detail_idx_next = $unit_score_detail_idx+1;
		$objPHPExcel->getActiveSheet()->mergeCells("A{$unit_score_detail_idx}:A{$unit_score_detail_idx_next}");
		$objPHPExcel->getActiveSheet()->setCellValue("A{$unit_score_detail_idx}", $ev_unit_question_list[$ev_u_no]);

		foreach($evaluation_unit_data as $ev_value => $evaluation_unit_score_detail_score)
		{
			$evaluation_unit_score_detail_avg = ($evaluation_unit_score_detail_score > 0) ? ($evaluation_unit_score_detail_score/$evaluation_unit_score_detail_total)*100 : 0;
			$objPHPExcel->getActiveSheet()->setCellValue("{$alpha_option[$ev_value]}{$unit_score_detail_idx}", $evaluation_unit_score_detail_score);
			$objPHPExcel->getActiveSheet()->setCellValue("{$alpha_option[$ev_value]}{$unit_score_detail_idx_next}", round($evaluation_unit_score_detail_avg)."%");
		}

		$objPHPExcel->getActiveSheet()->setCellValue("G{$unit_score_detail_idx}", $evaluation_unit_score_detail_total);
		$objPHPExcel->getActiveSheet()->setCellValue("G{$unit_score_detail_idx_next}", "100%");

		$objPHPExcel->getActiveSheet()->getRowDimension("{$unit_score_detail_idx}")->setRowHeight(13);
		$objPHPExcel->getActiveSheet()->getRowDimension("{$unit_score_detail_idx_next}")->setRowHeight(13);

		if($detail_color_idx % 2){
			$objPHPExcel->getActiveSheet()->getStyle("A{$unit_score_detail_idx}:G{$unit_score_detail_idx_next}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFF2CC');
		}

		$detail_color_idx ++;
		$unit_score_detail_idx += 2;
	}
	$unit_score_detail_idx_border = $unit_score_detail_idx-1;
	setBorder($sheet, "A10:G{$unit_score_detail_idx_border}");
	$unit_score_detail_idx += 2;

	# 3. 기타의견
	$objPHPExcel->getActiveSheet()->mergeCells("A{$unit_score_detail_idx}:G{$unit_score_detail_idx}");
	$objPHPExcel->getActiveSheet()->setCellValue("A{$unit_score_detail_idx}", "3. 기타의견 (하고싶은 이야기)");
	$objPHPExcel->getActiveSheet()->getRowDimension("{$unit_score_detail_idx}")->setRowHeight(26);
	$etc_title_idx = $unit_score_detail_idx;
	$unit_score_detail_idx ++;

	$etc_content_idx = $unit_score_detail_idx;
	$evaluation_unit_text_sql = "
		SELECT 
			ev_result.evaluation_value
		FROM evaluation_system_result ev_result
		WHERE ev_result.ev_no='{$ev_no}' AND ev_result.evaluation_state='5' AND ev_result.receiver_s_no='{$receiver_s_no}'
	";
	$evaluation_unit_text_query = mysqli_query($my_db, $evaluation_unit_text_sql);
	$evaluation_unit_text_list  = [];

	while($evaluation_unit_text = mysqli_fetch_assoc($evaluation_unit_text_query))
	{
		if(!empty($evaluation_unit_text['evaluation_value']))
		{
			$objPHPExcel->getActiveSheet()->mergeCells("A{$unit_score_detail_idx}:G{$unit_score_detail_idx}");
			$objPHPExcel->getActiveSheet()->setCellValue("A{$unit_score_detail_idx}", " - {$evaluation_unit_text['evaluation_value']}");
			$objPHPExcel->getActiveSheet()->getStyle("A{$unit_score_detail_idx}:G{$unit_score_detail_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFBDD7EE');
			$objPHPExcel->getActiveSheet()->getStyle("A{$unit_score_detail_idx}")->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getRowDimension("{$unit_score_detail_idx}")->setRowHeight(-1);
			$unit_score_detail_idx++;
		}
	}
	$unit_score_detail_idx--;


	$objPHPExcel->getActiveSheet()->getStyle("A1:G{$unit_score_detail_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle("A1:G{$unit_score_detail_idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setSize(12);
	$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

	$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFont()->setSize(10);
	$objPHPExcel->getActiveSheet()->getStyle("A3:G3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

	$objPHPExcel->getActiveSheet()->getStyle('A8:G8')->getFont()->setSize(12);
	$objPHPExcel->getActiveSheet()->getStyle('A8:G8')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A8:G8")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

	$objPHPExcel->getActiveSheet()->getStyle("A{$etc_title_idx}:G{$etc_title_idx}")->getFont()->setSize(12);
	$objPHPExcel->getActiveSheet()->getStyle("A{$etc_title_idx}:G{$etc_title_idx}")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A{$etc_title_idx}:G{$unit_score_detail_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
	$objPHPExcel->getActiveSheet()->setTitle($sheet_title);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);

	$sheet_idx++;
}

if(!!$evaluation_data_list)
{
	$sheet_idx = 0;
	$sheet = $objPHPExcel->setActiveSheetIndex($sheet_idx);
	$sheet->getDefaultStyle()->getFont()
		->setName('나눔 고딕')
		->setSize(9);

	$objPHPExcel->setActiveSheetIndex($sheet_idx);

	$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
	$objPHPExcel->getActiveSheet()->setCellValue("A1", "1. {$ev_subject} 설문결과 (종합)");
	$objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight(26);

	$question_idx = 0;
	$objPHPExcel->getActiveSheet()->setCellValue("{$alpha_option[$question_idx]}3", "부문");
	$question_idx++;
	foreach($evaluation_total_question_list as $question){
		$objPHPExcel->getActiveSheet()->setCellValue("{$alpha_option[$question_idx]}3", $question);
		$question_idx++;
	}
	$objPHPExcel->getActiveSheet()->setCellValue("{$alpha_option[$question_idx]}3", "전체 평균");
	$objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(18);

	$total_idx = 4;

	foreach($evaluation_data_list as $receiver => $avg_data)
	{
		$total_data_idx = 0;
		$objPHPExcel->getActiveSheet()->setCellValue("{$alpha_option[$total_data_idx]}{$total_idx}", "{$evaluation_total_receiver_list[$receiver]}");
		$objPHPExcel->getActiveSheet()->getStyle("{$alpha_option[$total_data_idx]}{$total_idx}")->getNumberFormat()->setFormatCode('0.00');
		$total_data_idx++;
		foreach($avg_data as $ev_no => $avg){
			$objPHPExcel->getActiveSheet()->setCellValue("{$alpha_option[$total_data_idx]}{$total_idx}", "{$avg}");
			$objPHPExcel->getActiveSheet()->getStyle("{$alpha_option[$total_data_idx]}{$total_idx}")->getNumberFormat()->setFormatCode('0.00');
			$total_data_idx++;
		}
		$objPHPExcel->getActiveSheet()->setCellValue("{$alpha_option[$total_data_idx]}{$total_idx}", "{$evaluation_total_list[$receiver]}");
		$objPHPExcel->getActiveSheet()->getStyle("{$alpha_option[$total_data_idx]}{$total_idx}")->getNumberFormat()->setFormatCode('0.000');
		$objPHPExcel->getActiveSheet()->getStyle("{$alpha_option[$total_data_idx]}{$total_idx}")->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getRowDimension("{$total_idx}")->setRowHeight(20);
		$total_idx++;
	}
	$total_idx--;

	$objPHPExcel->getActiveSheet()->getStyle("A1:G{$total_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle("A1:G{$total_idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setSize(12);
	$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	$objPHPExcel->getActiveSheet()->getStyle("{$alpha_option[$question_idx]}3:{$alpha_option[$question_idx]}{$total_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFCE4D6');

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);

	if($sheet_idx == 0){
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
	}else{
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
	}

	setBorder($sheet, "A3:{$alpha_option[$question_idx]}{$total_idx}");
	$objPHPExcel->getActiveSheet()->setTitle("종합");
	$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
}

$objPHPExcel->setActiveSheetIndex(0);
$excel_filename=iconv('UTF-8','EUC-KR',"{$excel_title}.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

?>
