<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

# Create new PHPExcel object & SET document
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_HAIR,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$outlineStyle = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$depart_color_list = array(
    "00200" => "00FFFFFF",
    "00211" => "00FDE9D9",
    "00210" => "00DAEEF3",
    "00213" => "00E4DFEC",
    "00218" => "00F2F2F2",
    "00219" => "00D9D9D9",
    "00220" => "00D9D9D9",
    "00207" => "00D9D9D9",
);

$team_color_list = array(
    "00200" => "00FFFFFF",
    "00211" => "00FDE9D9",
    "00210" => "00DAEEF3",
    "00213" => "00E4DFEC",
    "00218" => "00EBF1DE",
    "00221" => "00F2DCDB",
    "00222" => "00DDD9C4",
    "00223" => "00FDE9D9",
    "00231" => "00DAEEF3",
    "00214" => "00E4DFEC",
    "00219" => "00EBF1DE",
    "00202" => "00F2DCDB",
    "00215" => "00DDD9C4",
    "00216" => "00FDE9D9",
    "00201" => "00DAEEF3",
    "00217" => "00E4DFEC",
    "00220" => "00EBF1DE",
    "00226" => "00F2DCDB",
    "00227" => "00DDD9C4",
    "00229" => "00FDE9D9",
    "00232" => "00DAEEF3",
);

# 상단타이틀
$work_sheet = $objPHPExcel->setActiveSheetIndex(0);
$work_sheet->getDefaultStyle()->getFont()->setName('맑은 고딕');
$work_sheet->mergeCells('B1:I1');
$work_sheet->getRowDimension('1')->setRowHeight(25);
$work_sheet->getStyle('B1:I1')->getFont()->setSize(15);
$work_sheet->getStyle('B1:I1')->getFont()->setBold(true);
$work_sheet->setCellValue("B1", "와이즈플래닛 컴퍼니 연락망");

$header_title_list  = array("NO.", "부서명", "팀", "성명", "휴대전화", "내선번호", "070 직통전화", "이메일");
$head_idx           = 0;
$idx                = 2;
foreach(range('B', 'I') as $columnID){
    $work_sheet->setCellValue("{$columnID}{$idx}", $header_title_list[$head_idx]);
    $head_idx++;
}
$work_sheet->getStyle('B2:I2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00D9D9D9');
$idx++;

$team_parent_sql 	= "SELECT team_code, team_name FROM team WHERE (team_code_parent is null OR team_code_parent='') AND display='1' AND my_c_no='1' ORDER BY priority";
$team_parent_query 	= mysqli_query($my_db, $team_parent_sql);
$team_parent_list   = [];

while($team_parent = mysqli_fetch_assoc($team_parent_query))
{
    $team_parent_list[$team_parent['team_code']] = $team_parent['team_name'];
}

$team_list = [];
foreach($team_parent_list as $team_parent_code => $team_parent_name)
{
    $team_child_sql 	= "SELECT team_code, team_name FROM team WHERE team_group='{$team_parent_code}' AND display='1' AND my_c_no='1' ORDER BY priority";
    $team_child_query 	= mysqli_query($my_db, $team_child_sql);
    while($team_child = mysqli_fetch_assoc($team_child_query))
    {
        $team_list[$team_parent_code][$team_child['team_code']] = $team_child['team_name'];
    }
}

$staff_no   = 1;
foreach($team_list as $team_parent => $department_data)
{
    if($department_data)
    {
        $department   	  = $team_parent_list[$team_parent];
        $depart_color 	  = isset($depart_color_list[$team_parent]) ? $depart_color_list[$team_parent] : "00FFFFFF";
        $depart_start_idx = $idx;
        $department_result = false;

        foreach($department_data as $team_code => $team_name)
        {
            $team_color 	= isset($team_color_list[$team_code]) ? $team_color_list[$team_code] : "00FFFFFF";
            $team_start_idx = $idx;

            $staff_sql = "
				SELECT 
					s.s_name, 
					s.hp, 
					IF(s.extension!='',s.extension,'-') AS extension, 
					IF(s.tel!='',s.tel,'-') AS tel, 
					s.email 
				FROM staff s 
				WHERE s.staff_state=1 AND s.team_list like '%{$team_code}%'
				ORDER BY s.employment_date ASC
			";

            $staff_query = mysqli_query($my_db, $staff_sql);
            if($staff_query)
            {
                $team_result = false;
                while ($staff = mysqli_fetch_assoc($staff_query))
                {
                    $work_sheet->setCellValue("B{$idx}", $staff_no);
                    $work_sheet->setCellValue("E{$idx}", $staff['s_name']);
                    $work_sheet->setCellValue("F{$idx}", $staff['hp']);
                    $work_sheet->setCellValue("G{$idx}", $staff['extension']);
                    $work_sheet->setCellValue("H{$idx}", $staff['tel']);
                    $work_sheet->setCellValue("I{$idx}", $staff['email']);
                    $work_sheet->getStyle("D{$idx}:I{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($team_color);

                    $team_result = true;
                    $idx++;
                    $staff_no++;
                }

                if($team_result)
                {
                    $team_end_idx = $idx-1;

                    $work_sheet->mergeCells("D{$team_start_idx}:D{$team_end_idx}");
                    $work_sheet->setCellValue("D{$team_start_idx}", $team_name);

                    $department_result = true;
                }
            }
        }

        if($department_result)
        {
            $depart_end_idx = $idx-1;
            $work_sheet->setCellValue("C{$depart_start_idx}", $department);
            $work_sheet->mergeCells("C{$depart_start_idx}:C{$depart_end_idx}");
            $work_sheet->getStyle("C{$depart_start_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($depart_color);
        }
    }
}
$idx--;

$work_sheet->getStyle("B2:I{$idx}")->getFont()->setSize(11);
$work_sheet->getStyle("B2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$work_sheet->getStyle("B2:I{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$work_sheet->getStyle("B2:I{$idx}")->applyFromArray($styleArray);
$work_sheet->getStyle("B2:I{$idx}")->applyFromArray($outlineStyle);
$work_sheet->getStyle("B2:I2")->applyFromArray($outlineStyle);

$idx++;
$idx++;

# 미디어 커머스 연락망
$work_sheet->mergeCells("B{$idx}:I{$idx}");
$work_sheet->getRowDimension("{$idx}")->setRowHeight(25);
$work_sheet->getStyle("B{$idx}:I{$idx}")->getFont()->setSize(15);
$work_sheet->getStyle("B{$idx}:I{$idx}")->getFont()->setBold(true);
$work_sheet->setCellValue("B{$idx}", "미디어 커머스 연락망");
$idx++;

$media_start_idx = $idx;
$header_title_list  = array("NO.", "부서명", "팀", "성명", "휴대전화", "내선번호", "070 직통전화", "이메일");
$head_idx           = 0;
foreach(range('B', 'I') as $columnID){
    $work_sheet->setCellValue("{$columnID}{$idx}", $header_title_list[$head_idx]);
    $head_idx++;
}
$work_sheet->getStyle("B{$idx}:I{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00D9D9D9');
$idx++;


$team_parent_sql 	= "SELECT team_code, team_name FROM team WHERE (team_code_parent is null OR team_code_parent='') AND display='1' AND my_c_no='3' ORDER BY priority";
$team_parent_query 	= mysqli_query($my_db, $team_parent_sql);
$team_parent_list   = [];

while($team_parent = mysqli_fetch_assoc($team_parent_query))
{
    $team_parent_list[$team_parent['team_code']] = $team_parent['team_name'];
}

$team_list = [];
foreach($team_parent_list as $team_parent_code => $team_parent_name)
{
    $team_child_sql 	= "SELECT team_code, team_name FROM team WHERE team_group='{$team_parent_code}' AND display='1' AND my_c_no='3' ORDER BY priority";
    $team_child_query 	= mysqli_query($my_db, $team_child_sql);
    while($team_child = mysqli_fetch_assoc($team_child_query))
    {
        $team_list[$team_parent_code][$team_child['team_code']] = $team_child['team_name'];
    }
}

foreach($team_list as $team_parent => $department_data)
{
    if($department_data)
    {
        $department   	  = $team_parent_list[$team_parent];
        $depart_color 	  = isset($depart_color_list[$team_parent]) ? $depart_color_list[$team_parent] : "00FFFFFF";
        $depart_start_idx = $idx;
        $department_result = false;

        foreach($department_data as $team_code => $team_name)
        {
            $team_color 	= isset($team_color_list[$team_code]) ? $team_color_list[$team_code] : "00FFFFFF";
            $team_start_idx = $idx;

            $staff_sql = "
				SELECT 
					s.s_name, 
					s.hp, 
					IF(s.extension!='',s.extension,'-') AS extension, 
					IF(s.tel!='',s.tel,'-') AS tel, 
					s.email 
				FROM staff s 
				WHERE s.staff_state=1 AND s.team_list like '%{$team_code}%'
				ORDER BY s.employment_date ASC
			";

            $staff_query = mysqli_query($my_db, $staff_sql);
            if($staff_query)
            {
                $team_result = false;
                while ($staff = mysqli_fetch_assoc($staff_query))
                {
                    $work_sheet->setCellValue("B{$idx}", $staff_no);
                    $work_sheet->setCellValue("E{$idx}", $staff['s_name']);
                    $work_sheet->setCellValue("F{$idx}", $staff['hp']);
                    $work_sheet->setCellValue("G{$idx}", $staff['extension']);
                    $work_sheet->setCellValue("H{$idx}", $staff['tel']);
                    $work_sheet->setCellValue("I{$idx}", $staff['email']);
                    $work_sheet->getStyle("D{$idx}:I{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($team_color);

                    $team_result = true;
                    $idx++;
                    $staff_no++;
                }

                if($team_result)
                {
                    $team_end_idx = $idx-1;

                    $work_sheet->mergeCells("D{$team_start_idx}:D{$team_end_idx}");
                    $work_sheet->setCellValue("D{$team_start_idx}", $team_name);
                    $department_result = true;
                }
            }
        }

        if($department_result)
        {
            $depart_end_idx = $idx-1;
            $work_sheet->setCellValue("C{$depart_start_idx}", $department);
            $work_sheet->mergeCells("C{$depart_start_idx}:C{$depart_end_idx}");
            $work_sheet->getStyle("C{$depart_start_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($depart_color);
        }
    }
}
$idx--;

$work_sheet->getStyle("B{$media_start_idx}:I{$idx}")->getFont()->setSize(11);
$work_sheet->getStyle("B{$media_start_idx}:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$work_sheet->getStyle("B{$media_start_idx}:I{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$work_sheet->getStyle("B{$media_start_idx}:I{$idx}")->applyFromArray($styleArray);
$work_sheet->getStyle("B{$media_start_idx}:I{$idx}")->applyFromArray($outlineStyle);
$work_sheet->getStyle("B{$media_start_idx}:I{$media_start_idx}")->applyFromArray($outlineStyle);

$work_sheet->getColumnDimension('A')->setWidth(1);
$work_sheet->getColumnDimension('B')->setWidth(6);
$work_sheet->getColumnDimension('C')->setWidth(15);
$work_sheet->getColumnDimension('D')->setWidth(15);
$work_sheet->getColumnDimension('E')->setWidth(11);
$work_sheet->getColumnDimension('F')->setWidth(16);
$work_sheet->getColumnDimension('G')->setWidth(8);
$work_sheet->getColumnDimension('H')->setWidth(16);
$work_sheet->getColumnDimension('I')->setWidth(30);

$work_sheet->setTitle('와이즈플래닛 연락망');
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename = iconv('UTF-8','EUC-KR',"와이즈플래닛_연락망.xls");

header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
