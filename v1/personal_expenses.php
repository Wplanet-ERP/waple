<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/personal_expenses.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/MyCompany.php');
require('inc/model/Work.php');
require('inc/model/Team.php');
require('inc/model/Staff.php');
require('inc/model/Withdraw.php');
require('inc/model/Custom.php');
require('inc/model/Expenses.php');
require('Classes/PHPExcel.php');

# Model Init
$work_model     = Work::Factory();
$kind_model     = Kind::Factory();
$staff_model    = Staff::Factory();
$custom_model   = Custom::Factory();
$expenses_model = Expenses::Factory();

$is_admin_search = false;
$is_team_search  = false;

if(permissionNameCheck($session_permission, "재무관리자")){
    $is_admin_search = true;
}

if($session_s_no == 62){
    $is_team_search = true;
}

$smarty->assign("is_admin_search", $is_admin_search);
$smarty->assign("is_team_search", $is_team_search);

# 개인경비 제출기한
$to_date        = date("Y-m-d");
$deadline_list  = $work_model->getDeadLine();
$deadline_date  = ""; // 제출마감일
$deadline_d_day = ""; // 제출마감일 남은 일수
$deadline_w_no  = ""; // 제출마감일 업무번호

foreach($deadline_list as $deadline_token)
{
	$deadline 		= $deadline_token['dday'];
	$deadline_w_no 	= $deadline_token['w_no'];
	$gap = (strtotime($to_date) - strtotime($deadline))/60/60/24; // 문자형 날짜를 초로 계산하여 일자로 변환
		
	if($gap >= -19 && $gap <= 14){ // 18일 이전 14일이 지나지 않은 날짜를 제출마감일로 정함
		$deadline_date  = $deadline;
		$deadline_d_day = $gap;
		break;
	}
}
$pre_month_deadline_date = date("Y-m",strtotime ("-1 month", strtotime($deadline_date))); // 제출마감일의 전달

$smarty->assign("deadline_date", $deadline_date);
$smarty->assign("deadline_d_day", $deadline_d_day);
$smarty->assign("deadline_w_no", $deadline_w_no);
$smarty->assign("pre_month_deadline_date", $pre_month_deadline_date);

# Process 시작
$process = (isset($_POST['process'])) ? $_POST['process'] : "";

if ($process == "f_state")
{
    $upd_data = array(
        "pe_no" => (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "",
        "state" => (isset($_POST['val'])) ? $_POST['val'] : ""
    );

	if (!$expenses_model->update($upd_data))
		echo "진행상태 저장에 실패 하였습니다.";
	else{
		echo "진행상태가 저장 되었습니다.";
		echo "<script>setTimeout(function() {location.reload();}, 1200);</script>";
	}
	exit;
}
elseif($process == "chk_state")
{
	$pe_no_list = (isset($_POST['chk_pe_no_list'])) ? $_POST['chk_pe_no_list'] : "";
	$save_state = (isset($_POST['chk_state'])) ? $_POST['chk_state'] : "";
    $search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$pe_no_tok  = explode(",", $pe_no_list);
    $upd_data   = [];
	foreach($pe_no_tok as $pe_no){
        $upd_data[] = array(
            "pe_no" => $pe_no,
            "state" => $save_state
        );
    }

	if (!$expenses_model->multiUpdate($upd_data)){
		echo ("<script>alert('저장에 실패 하였습니다.\\n담당자에게 문의해 주세요.');</script>");
	}else{
		echo ("<script>alert('모두 저장 하였습니다.');</script>");
	}
	exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");
}
elseif($process == "chk_wd_method")
{
	$pe_no_list     = (isset($_POST['chk_pe_no_list'])) ? $_POST['chk_pe_no_list'] : "";
	$save_wd_method = (isset($_POST['chk_wd_method'])) ? $_POST['chk_wd_method'] : "";
    $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $pe_no_tok  = explode(",", $pe_no_list);
    $upd_data   = [];
    foreach($pe_no_tok as $pe_no){
        $upd_data[] = array(
            "pe_no"     => $pe_no,
            "wd_method" => $save_wd_method
        );
    }

    if (!$expenses_model->multiUpdate($upd_data)){
		echo ("<script>alert('저장에 실패 하였습니다.\\n담당자에게 문의해 주세요.');</script>");
	}else{
		echo ("<script>alert('모두 저장 하였습니다.');</script>");
	}
	exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");
}
elseif($process == "chk_account_code")
{
    $pe_no_list         = (isset($_POST['chk_pe_no_list'])) ? $_POST['chk_pe_no_list'] : "";
    $save_account_code  = (isset($_POST['chk_account_code'])) ? $_POST['chk_account_code'] : "";
    $search_url         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $pe_no_tok  = explode(",", $pe_no_list);
    $upd_data   = [];
    foreach($pe_no_tok as $pe_no){
        $upd_data[] = array(
            "pe_no"         => $pe_no,
            "account_code"  => $save_account_code
        );
    }

    if (!$expenses_model->multiUpdate($upd_data)){
        echo ("<script>alert('저장에 실패 하였습니다.\\n담당자에게 문의해 주세요.');</script>");
    }else{
        echo ("<script>alert('모두 저장 하였습니다.');</script>");
    }
    exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");

}
elseif($process == "chk_pe_delete")
{
    $pe_no_list = (isset($_POST['chk_pe_no_list'])) ? $_POST['chk_pe_no_list'] : "";
    $search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $pe_no_tok  = explode(",", $pe_no_list);
    $del_data   = [];
    foreach($pe_no_tok as $pe_no){
        $del_data[] = array(
            "pe_no"    => $pe_no,
            "display"  => '2'
        );
    }

    if (!$expenses_model->multiUpdate($del_data)){
		echo ("<script>alert('삭제에 실패 하였습니다.\\n담당자에게 문의해 주세요.');</script>");
	}else{
		echo ("<script>alert('모두 삭제 하였습니다.');</script>");
	}
	exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");
}
elseif ($process == "f_priority")
{
    $upd_data = array(
        "pe_no"     => (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "",
        "priority"  => (isset($_POST['val'])) ? $_POST['val'] : ""
    );

    if (!$expenses_model->update($upd_data))
		echo "순번 저장에 실패 하였습니다.";
	else
		echo "순번이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_my_c_no")
{
    $upd_data = array(
        "pe_no"     => (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "",
        "my_c_no"   => (isset($_POST['val'])) ? $_POST['val'] : ""
    );

    if (!$expenses_model->update($upd_data))
		echo "사업자 저장에 실패 하였습니다.";
	else
		echo "사업자가 저장 되었습니다.";
	exit;
}
elseif ($process == "f_payment_date")
{
	$pe_no = (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	// 제출기한 체크 [start]
	if(!permissionNameCheck($session_permission,"재무관리자"))
    {
		$input_value    = new DateTime($value);
		$tomonth_01     = new DateTime(date("Y-m-01"));
		$prev_month_01  = new DateTime(date("Y-m-01", strtotime ("-1 month")));

		if($prev_month_01 > $input_value){ // 전달 이전의 경우 입력 불가
			echo "제출기한 종료로 수정이 불가능합니다.";
			echo "<script>setTimeout(function() {location.reload();}, 1200);</script>";
			exit;
		}else{
			if($deadline_d_day > 0){ // 기간종료 이후 입력 불가
				if($tomonth_01 > $input_value){ // 전달 입력 불가
					echo "제출기한 종료로 수정이 불가능합니다.";
					echo "<script>setTimeout(function() {location.reload();}, 1200);</script>";
				exit;}
			}
		}
	}
	// 제출기한 체크 [end]

    $upd_data = array(
        "pe_no"         => $pe_no,
        "payment_date"  => !empty($value) ? $value : "NULL"
    );

    if (!$expenses_model->update($upd_data))
		echo "결제일 저장에 실패 하였습니다.";
	else
		echo "결제일이 저장 되었습니다.";
	exit;

}
elseif ($process == "f_account_code")
{
    $upd_data = array(
        "pe_no"         => (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "",
        "account_code"  => (isset($_POST['val'])) ? $_POST['val'] : ""
    );

    if (!$expenses_model->update($upd_data))
		echo "계정과목 저장에 실패 하였습니다.";
	else
		echo "계정과목이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_wd_method")
{
    $upd_data = array(
        "pe_no"     => (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "",
        "wd_method" => (isset($_POST['val'])) ? $_POST['val'] : ""
    );

    if (!$expenses_model->update($upd_data))
		echo "결제수단 저장에 실패 하였습니다.";
	else
		echo "결제수단이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_card_num")
{
    $upd_data = array(
        "pe_no"     => (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "",
        "card_num"  => (isset($_POST['val'])) ? $_POST['val'] : ""
    );

    if (!$expenses_model->update($upd_data))
		echo "카드번호 저장에 실패 하였습니다.";
	else
		echo "카드번호가 저장 되었습니다.";
	exit;
}
elseif ($process == "f_payment_contents")
{
    $upd_data = array(
        "pe_no"             => (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "",
        "payment_contents"  => (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : ""
    );

    if (!$expenses_model->update($upd_data))
		echo "상세내역 저장에 실패 하였습니다.";
	else
		echo "상세내역이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_money")
{
	$pe_no          = (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "";
	$value          = (isset($_POST['val'])) ? str_replace(",","", trim($_POST['val'])) : "";
	$f_account_code = (isset($_POST['f_account_code'])) ? $_POST['f_account_code'] : "";
	$value_vat      = 0;

	if(
        $f_account_code != '03004'      // 시내교통비
		&& $f_account_code != '03008'   // 외주용역비
		&& $f_account_code != '03009'   // 지급수수료
		&& $f_account_code != '03012'   // 도서인쇄비
		&& $f_account_code != '03012'   // 접대비
		&& $f_account_code != '03013'   // 광고선전비
		&& $f_account_code != '03015'   // 통신비
    )
    {
		$value_vat = round($value/11);
	}

    $upd_data = array(
        "pe_no"     => $pe_no,
        "money"     => $value,
        "money_vat" => $value_vat,
    );

    if (!$expenses_model->update($upd_data))
		echo "금액 저장에 실패 하였습니다.";
	else
		echo "금액이 저장 되었습니다.";
	exit;
}
elseif($process == "add_req_record") # 개인경비 개별추가
{
	// 제출기한 체크 [start]
	if(!permissionNameCheck($session_permission,"재무관리자"))
    {
		$input_value    = new DateTime($_POST['f_payment_date_new']);
		$tomonth_01     = new DateTime(date("Y-m-01"));
		$prev_month_01  = new DateTime(date("Y-m-01",strtotime ("-1 month")));

		if($prev_month_01 > $input_value){ // 전달 이전의 경우 입력 불가
			echo("<script>alert('제출기한 종료로 추가할 수 없습니다.');history.back();</script>");
			exit;
		}else{
			if($deadline_d_day > 0){ // 기간종료 이후 입력 불가
				if($tomonth_01 > $input_value){ // 전달 입력 불가
					echo("<script>alert('제출기한 종료로 추가할 수 없습니다.');history.back();</script>");
					exit;
				}
			}
		}
	}
	// 제출기한 체크 [end]

    $wd_no_sql      = "SELECT COUNT(pe.wd_no) as cnt FROM `personal_expenses` pe WHERE DATE_FORMAT(pe.payment_date, '%Y-%m') BETWEEN DATE_FORMAT('{$_POST['f_payment_date_new']}', '%Y-%m') AND DATE_FORMAT('{$_POST['f_payment_date_new']}', '%Y-%m') AND s_no = '{$session_s_no}'";
    $wd_no_query    = mysqli_query($my_db, $wd_no_sql);
    $wd_no_result   = mysqli_fetch_array($wd_no_query);
	$wd_no_count    = $wd_no_result['cnt'];

	if($wd_no_count > 1){
		echo("<script>alert('개인경비 지급요청이 불가능 합니다.\\n이미 지급 요청된 내역이 있습니다.\\n담당자에게 문의해주세요.');history.back();</script>");
		exit;
	}
    $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $f_s_no         = $_POST['f_s_no_new'];
    $staff_item     = $staff_model->getItem($f_s_no);
    $f_money_value  = str_replace(",","", trim($_POST['f_money_new']));

    $ins_data = array(
        "priority"          => $_POST['f_priority_new'],
        "my_c_no"           => $_POST['f_my_c_no_new'],
        "team"              => $staff_item['team'],
        "s_no"              => $f_s_no,
        "req_s_no"          => $f_s_no,
        "payment_date"      => !empty($_POST['f_payment_date_new']) ? $_POST['f_payment_date_new'] : "NULL",
        "account_code"      => !empty($_POST['f_account_code_new']) ? $_POST['f_account_code_new'] : "NULL",
        "wd_method"         => !empty($_POST['f_wd_method_new']) ? $_POST['f_wd_method_new'] : "1",
        "card_num"          => !empty($_POST['f_card_num_new']) ? $_POST['f_card_num_new'] : "NULL",
        "payment_contents"  => !empty($_POST['f_payment_contents_new']) ? addslashes(trim($_POST['f_payment_contents_new'])) : "NULL",
        "money"             => !empty($f_money_value) ? $f_money_value : 0,
        "regdate"           => date("Y-m-d H:i:s")
    );

    #법인카드 번호
    $corp_card_sql 	  = "SELECT cc_no FROM corp_card WHERE card_type='3' AND manager='{$f_s_no}' LIMIT 1";
    $corp_card_query  = mysqli_query($my_db, $corp_card_sql);
    $corp_card_result = mysqli_fetch_assoc($corp_card_query);
    $corp_card_no	  = $corp_card_result['cc_no'];

    if(!empty($corp_card_no)){
        $ins_data['corp_card_no'] = $corp_card_no;
    }

    if(!$expenses_model->insert($ins_data)){
        echo ("<script>alert('개인경비 리스트 추가에 실패 하였습니다');</script>");
    }else{
        echo ("<script>alert('개인경비 리스트에 추가 하였습니다');</script>");
    }
	exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");

}
elseif($process == "add_req_multi_record") # 일괄 업무 추가
{
    $my_c_no 	    = (isset($_POST['f_my_c_no_new']))?$_POST['f_my_c_no_new']:"";
    $f_s_no_new     = (isset($_POST['f_s_no_new']))?$_POST['f_s_no_new']:"";
    $f_new_multi    = (isset($_POST['f_new_multi']))?$_POST['f_new_multi']:"";
    $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    #법인카드 번호
    $corp_card_sql 	  = "SELECT cc_no FROM corp_card WHERE card_type='3' AND manager='{$f_s_no_new}' LIMIT 1";
    $corp_card_query  = mysqli_query($my_db, $corp_card_sql);
    $corp_card_result = mysqli_fetch_assoc($corp_card_query);
    $corp_card_no	  = $corp_card_result['cc_no'];

    $f_new_array    = explode("\n", $f_new_multi);
    $ins_multi_data = [];
    $regdate        = date("Y-m-d H:i:s");
    foreach($f_new_array as $index =>$f_new_array_value)
    {
        if(!!trim($f_new_array_value))
        {
            $f_new_array_tt = explode("\t", $f_new_array_value);

            foreach($f_new_array_tt as $index2 =>$f_new_array_tt_value)
            {
                $f_new_array_tt_value = trim($f_new_array_tt_value);

                switch($index2)
                {
                    case '0' : # 순번
                        $priority_value = $f_new_array_tt_value;
                        break;
                    case '1': # 결제수단
                        $wd_method_value = '5';
                        break;
                    case '2' : # 승인일시
                        $payment_date_value = $f_new_array_tt_value;

                        // 제출기한 체크 [start]
                        if(!permissionNameCheck($session_permission,"재무관리자"))
                        {
                            $input_value    = new DateTime($payment_date_value);
                            $tomonth_01     = new DateTime(date("Y-m-01"));
                            $prev_month_01  = new DateTime(date("Y-m-01",strtotime ("-1 month")));

                            if($prev_month_01 > $input_value){ // 전달 이전의 경우 입력 불가
                                echo("<script>alert('제출기한 종료로 추가할 수 없습니다.');history.back();</script>");
                                exit;
                            }else{
                                if($deadline_d_day > 0){ // 기간종료 이후 입력 불가
                                    if($tomonth_01 > $input_value){ // 전달 입력 불가
                                        echo("<script>alert('제출기한 종료로 추가할 수 없습니다.');history.back();</script>");
                                        exit;
                                    }
                                }
                            }
                        }
                        // 제출기한 체크 [end]

                        $wd_no_sql      = "SELECT COUNT(pe.wd_no) as cnt FROM `personal_expenses` pe WHERE DATE_FORMAT(pe.payment_date, '%Y-%m') BETWEEN DATE_FORMAT('{$payment_date_value}', '%Y-%m') AND DATE_FORMAT('{$payment_date_value}', '%Y-%m') AND s_no = '{$session_s_no}'";
                        $wd_no_query    = mysqli_query($my_db, $wd_no_sql);
                        $wd_no_result   = mysqli_fetch_array($wd_no_query);
                        $wd_no_count    = $wd_no_result['cnt'];

                        if($wd_no_count > 1){
                            echo("<script>alert('개인경비 지급요청이 불가능 합니다.\\n이미 지급 요청된 내역이 있습니다.\\n담당자에게 문의해주세요.');history.back();</script>");
                            exit;
                        }
                        break;
                    case '3' :
                        $appr_price         = str_replace(",","",trim($f_new_array_tt_value));
                        break;
                    case '4' :
                        $f_money_value      = str_replace(",","",trim($f_new_array_tt_value));
                        $money_value        = $f_money_value;
                        $money_value_vat    = round((int)$money_value/11);

                        break;
                    case '5' : # 계정과목 체크
                        $account_name           = trim($f_new_array_tt_value);
                        $account_code_sql       = "SELECT k.k_name_code FROM kind k WHERE k.k_code = 'account_code' AND k.k_name = '{$account_name}' AND k.k_parent IS NOT NULL";
                        $account_code_query     = mysqli_query($my_db, $account_code_sql);
                        $account_code_result    = mysqli_fetch_array($account_code_query);

                        if(!isset($account_code_result['k_name_code'])){
                            echo("<script>alert('확인되지 않은 계정과목이 있습니다.\\n계정과목을 확인 후 다시 시도해 주세요.');history.back();</script>");
                            exit;
                        }

                        $account_code = $account_code_result['k_name_code'];
                        break;
                    case '6':
                        $payment_contents_value = addslashes($f_new_array_tt_value);
                        break;
                }
            }

            $staff_item         = $staff_model->getItem($f_s_no_new);
            $ins_multi_data[]   = array(
                "state"             => "1",
                "priority"          => $priority_value,
                "my_c_no"           => $my_c_no,
                "team"              => $staff_item['team'],
                "s_no"              => $f_s_no_new,
                "req_s_no"          => $f_s_no_new,
                "payment_date"      => $payment_date_value,
                "account_code"      => $account_code,
                "wd_method"         => $wd_method_value,
                "payment_contents"  => $payment_contents_value,
                "money"             => $money_value,
                "money_vat"         => $money_value_vat,
                "regdate"           => $regdate,
                "appr_price"        => $appr_price,
                "corp_card_no"      => $corp_card_no,
            );
        }
    }

    if(!$expenses_model->multiInsert($ins_multi_data)){
        echo ("<script>alert('개인경비 추가에 실패 하였습니다');</script>");
    }else{
        echo ("<script>alert('개인경비 리스트에 추가 하였습니다');</script>");
    }

    exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");
}
elseif($process == "add_req_multi_record_file") # 일괄 업무 추가 파일
{
    $payment_date_value = (isset($_POST['f_payment_date_new']))?$_POST['f_payment_date_new']:"";
    $f_insert_type_new  = (isset($_POST['f_insert_type_new']))?$_POST['f_insert_type_new']:"";
    $search_url         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    //제출기한 체크 [start]
    if(!permissionNameCheck($session_permission,"재무관리자"))
    {
        $input_value    = new DateTime($payment_date_value);
        $tomonth_01     = new DateTime(date("Y-m-01"));
        $prev_month_01  = new DateTime(date("Y-m-01",strtotime ("-1 month")));

        if($prev_month_01 > $input_value){ // 전달 이전의 경우 입력 불가
            echo("<script>alert('제출기한 종료로 추가할 수 없습니다.');history.back();</script>");
            exit;
        }else{
            if($deadline_d_day > 0){ // 기간종료 이후 입력 불가
                if($tomonth_01 > $input_value){ // 전달 입력 불가
                    echo("<script>alert('제출기한 종료로 추가할 수 없습니다.');history.back();</script>");
                    exit;
                }
            }
        }
    }
    //제출기한 체크 [end]

    // 업로드 된 엑셀 형식에 맞는 Reader객체를 만든다.
    $file_name  = $_FILES["f_new_file"]["tmp_name"];
    $objReader  = PHPExcel_IOFactory::createReaderForFile($file_name);
    $objReader->setReadDataOnly(true);
    $objExcel   = $objReader->load($file_name);
    $objExcel->setActiveSheetIndex(0);

    $objWorksheet   = $objExcel->getActiveSheet();
    $totalRow       = $objWorksheet->getHighestRow();

    //INIT DATA
    $ins_multi_data         = [];
    $kind_account_2_list    = $kind_model->getKindChildList("account_code");
    $j                      = 4;
    $priority_idx           = 1;
    $regdate                = date('Y-m-d H:i:s');

    for ($i = 2; $i <= $totalRow; $i++)
    {
        $payment_contents = $account_code_val = $card_num_val = $payment_date = "";
        $inout_type	= $appr_no = $sales_type = $req_s_name = "";
        $fran_c_name = $fran_c_addr = $fran_no = $priority = $corp_card_no = "";
        $s_no = $team = $card_num = $my_c_no = "";
        $req_s_no = $session_s_no;

        $money = $money_vat = $appr_price = $pay_price = $tax_price = $pay_out_price = $account_code = 0;

        if($f_insert_type_new == '1')
        {
            $objWorksheet1  	= $objExcel->setActiveSheetIndex(0);
            $card_num_val		= (string)trim(addslashes($objWorksheet1->getCell("B{$i}")->getValue())); # 법인카드 리스트 수정
            $inout_type 		= (string)trim(addslashes($objWorksheet1->getCell("G{$i}")->getValue()));
			$payment_date_val   = (string)trim(addslashes($objWorksheet1->getCell("I{$i}")->getValue()))." ".(string)trim(addslashes($objWorksheet1->getCell("J{$i}")->getValue()));
            $appr_no 			= (string)trim(addslashes($objWorksheet1->getCell("H{$i}")->getValue()));
            $sales_type 		= (string)trim(addslashes($objWorksheet1->getCell("K{$i}")->getValue()));
            $fran_c_name 		= (string)trim(addslashes($objWorksheet1->getCell("W{$i}")->getValue()));
            $fran_c_addr 		= (string)trim(addslashes($objWorksheet1->getCell("Z{$i}")->getValue()));
            $fran_no 			= (string)trim(addslashes($objWorksheet1->getCell("V{$i}")->getValue()));

			if(strpos($payment_date_val, '-') !== false){
				$payment_date = $payment_date_val;
			}else{
				$payment_date = date('Y-m-d', strtotime(jdtogregorian((int)$payment_date_val + 2415019)));
			}

            $objWorksheet2      = $objExcel->setActiveSheetIndex(1);
            $priority 		    = ($i == 2) ? (string)trim(addslashes($objWorksheet2->getCell("A{$j}")->getValue())) : (string)trim(addslashes($objWorksheet2->getCell("A{$j}")->getCalculatedValue()));
            $appr_price_val	    = $objWorksheet2->getCell("I{$j}")->getOldCalculatedValue() ? $objWorksheet2->getCell("I{$j}")->getOldCalculatedValue() : $objWorksheet2->getCell("I{$j}")->getValue();
            $appr_price         = (string)trim(addslashes(str_replace(',','',$appr_price_val)));
            $pay_price_val	    = $objWorksheet2->getCell("J{$j}")->getOldCalculatedValue() ? $objWorksheet2->getCell("J{$j}")->getOldCalculatedValue() : $objWorksheet2->getCell("J{$j}")->getValue();
            $pay_price 		    = (string)trim(addslashes(str_replace(',','',$pay_price_val)));
            $tax_price_val	    = $objWorksheet2->getCell("K{$j}")->getOldCalculatedValue() ? $objWorksheet2->getCell("K{$j}")->getOldCalculatedValue() : $objWorksheet2->getCell("K{$j}")->getValue();
            $tax_price 		    = (string)trim(addslashes(str_replace(',','',$tax_price_val)));
            $pay_out_price_val	= $objWorksheet2->getCell("L{$j}")->getOldCalculatedValue() ? $objWorksheet2->getCell("L{$j}")->getOldCalculatedValue() : $objWorksheet2->getCell("L{$j}")->getValue();
            $pay_out_price      = (string)trim(addslashes(str_replace(',','',$pay_out_price_val)));
            $payment_contents   = (string)trim(addslashes($objWorksheet2->getCell("R{$j}")->getValue()));
            $account_code_val   = (string)trim(addslashes($objWorksheet2->getCell("Q{$j}")->getValue()));

            $money_val		    = $objWorksheet2->getCell("P{$j}")->getOldCalculatedValue() ? $objWorksheet2->getCell("P{$j}")->getOldCalculatedValue() : $objWorksheet2->getCell("P{$j}")->getValue();
            $money			    = (string)trim(addslashes(str_replace(',','',$money_val)));
            $money_vat 		    = round((int)$money/11);
            $j++;

        }
        elseif($f_insert_type_new == '2')
        {
            $objWorksheet1  = $objExcel->setActiveSheetIndex(0);
            $card_num_val	= (string)trim(addslashes($objWorksheet1->getCell("B{$i}")->getValue())); # 법인카드 리스트 수정
            $inout_type 	= (string)trim(addslashes($objWorksheet1->getCell("G{$i}")->getValue()));
            $payment_date   = (string)trim(addslashes($objWorksheet1->getCell("I{$i}")->getValue()))." ".(string)trim(addslashes($objWorksheet1->getCell("J{$i}")->getValue()));
            $appr_no 		= (string)trim(addslashes($objWorksheet1->getCell("H{$i}")->getValue()));
            $sales_type 	= (string)trim(addslashes($objWorksheet1->getCell("K{$i}")->getValue()));
            $fran_c_name 	= (string)trim(addslashes($objWorksheet1->getCell("W{$i}")->getValue()));
            $fran_c_addr 	= (string)trim(addslashes($objWorksheet1->getCell("Z{$i}")->getValue()));
            $fran_no 		= (string)trim(addslashes($objWorksheet1->getCell("V{$i}")->getValue()));
            $appr_price 	= (string)trim(addslashes(str_replace(',','',$objWorksheet1->getCell("L{$i}")->getValue())));
            $pay_price 		= (string)trim(addslashes(str_replace(',','',$objWorksheet1->getCell("O{$i}")->getValue())));
            $tax_price 		= (string)trim(addslashes(str_replace(',','',$objWorksheet1->getCell("P{$i}")->getValue())));
            $pay_out_price 	= (string)trim(addslashes(str_replace(',','',$objWorksheet1->getCell("M{$i}")->getValue())));

            $money			= $appr_price;
            $money_vat 		= round((int)$money/11);
            $priority		= $priority_idx;
            $priority_idx++;
        }

        if(empty($appr_no) || empty($appr_price)){
            continue;
        }

        # 카드 검색
        $new_corp_card_sql      = "SELECT cc_no, my_c_no, team, manager, card_num FROM corp_card WHERE card_num ='{$card_num_val}' LIMIT 1";
        $new_corp_card_query    = mysqli_query($my_db, $new_corp_card_sql);
        $new_corp_card_result    = mysqli_fetch_assoc($new_corp_card_query);

        $s_no 		  = $new_corp_card_result['manager'];
        $my_c_no 	  = isset($new_corp_card_result['my_c_no']) ? $new_corp_card_result['my_c_no'] : "";
        $team 		  = isset($new_corp_card_result['team']) ? $new_corp_card_result['team'] : "";
        $card_num 	  = isset($new_corp_card_result['card_num']) ? $new_corp_card_result['card_num'] : "";
        $corp_card_no = isset($new_corp_card_result['cc_no']) ? $new_corp_card_result['cc_no'] : "";

        if(empty($s_no) || empty($team) || empty($card_num)) {
            exit("<script>alert('법인카드 정보가 없습니다');location.href='personal_expenses.php?{$search_url}'</script>");
        }

        if($account_code_val) {
            if($account_code_val == '소모품비교육'){
                exit("<script>alert('계정과목에 `소모품비교육`은 `외주용역비(입찰 등)`으로 변경되 등록에 실패하였습니다. 새로운 개인경비 양식 참고하시고, 수정 후 다시 시도 부탁드립니다.');location.href='personal_expenses.php?{$search_url}'</script>");
            }

            $account_code = array_search($account_code_val, $kind_account_2_list);
        }

        # 법인카드 등록
        $ins_multi_data[] = array(
            "state"             => "1",
            "my_c_no"           => $my_c_no,
            "priority"          => $priority,
            "team"              => $team,
            "s_no"              => $s_no,
            "req_s_no"          => $req_s_no,
            "payment_date"      => $payment_date,
            "wd_method"         => "1",
            "card_num"          => $card_num,
            "corp_card_no"      => $corp_card_no,
            "money"             => $money,
            "money_vat"         => $money_vat,
            "regdate"           => $regdate,
            "inout_type"        => $inout_type,
            "appr_no"           => $appr_no,
            "sales_type"        => $sales_type,
            "fran_c_name"       => $fran_c_name,
            "fran_c_addr"       => $fran_c_addr,
            "fran_no"           => $fran_no,
            "appr_price"        => $appr_price,
            "pay_price"         => $pay_price,
            "tax_price"         => $tax_price,
            "pay_out_price"     => $pay_out_price,
            "payment_contents"  => $payment_contents,
            "account_code"      => $account_code,
        );
    }

	if($f_insert_type_new == '1')
	{
        $objExcel->setActiveSheetIndex(2);
        $objWorksheet3 = $objExcel->getActiveSheet();
        $totalRow3 = $objWorksheet3->getHighestRow();

        for ($k = 4; $k <= $totalRow3; $k++)
        {
            $priority = $payment_date = $account_code = $payment_contents = '';
            $req_s_no = "";
            $money 	  = $money_vat = $appr_price = 0;

            $req_s_name_tmp     = !empty($objWorksheet3->getCell("C{$k}")->getOldCalculatedValue()) ? $objWorksheet3->getCell("C{$k}")->getOldCalculatedValue() : $objWorksheet3->getCell("C{$k}")->getCalculatedValue();
            $priority 			= (string)trim(addslashes($objWorksheet3->getCell("B{$k}")->getOldCalculatedValue()));
            $req_s_name_val		= (string)trim(addslashes($req_s_name_tmp));
            $payment_date_val   = (string)trim(addslashes($objWorksheet3->getCell("D{$k}")->getValue()));
            $appr_price 		= (string)trim(addslashes(str_replace(',','',$objWorksheet3->getCell("E{$k}")->getValue())));
            $money 				= (string)trim(addslashes(str_replace(',','',$objWorksheet3->getCell("F{$k}")->getValue())));
            $account_code_val 	= (string)trim(addslashes($objWorksheet3->getCell("G{$k}")->getValue()));
            $payment_contents 	= (string)trim(addslashes($objWorksheet3->getCell("H{$k}")->getValue()));
            $money_vat 			= round((int)$money/11);

            if(empty($req_s_name_val) || empty($appr_price)){
                continue;
            }

            if(strpos($payment_date_val, '-') !== false){
                $payment_date = $payment_date_val;
            }else{
                $payment_date = date('Y-m-d', strtotime(jdtogregorian((int)$payment_date_val + 2415019)));
            }

            if($account_code_val){
                $account_code = array_search($account_code_val, $kind_account_2_list);
            }

            if($req_s_name_val){
                $req_staff_val_sql = "SELECT s_no FROM staff WHERE s_name ='{$req_s_name_val}' AND staff_state='1' LIMIT 1";
                $req_staff_val_query = mysqli_query($my_db, $req_staff_val_sql);
                $req_staff_val_result = mysqli_fetch_assoc($req_staff_val_query);

                $req_s_no = isset($req_staff_val_result['s_no']) ? $req_staff_val_result['s_no'] : "";
            }

            if(empty($req_s_no)){
                exit("<script>alert('3번시트의 제출자명을 본인이름으로 수정해주세요');location.href='personal_expenses.php?{$search_url}'</script>");
			}

            $new_corp_card_sql    = "SELECT cc_no, my_c_no, team, manager, card_num FROM corp_card WHERE card_type='3' AND manager='{$req_s_no}' LIMIT 1";
            $new_corp_card_query  = mysqli_query($my_db, $new_corp_card_sql);
            $new_corp_card_result = mysqli_fetch_assoc($new_corp_card_query);

            $s_no 		  = $new_corp_card_result['manager'];
            $my_c_no 	  = isset($new_corp_card_result['my_c_no']) ? $new_corp_card_result['my_c_no'] : "";
            $team 		  = isset($new_corp_card_result['team']) ? $new_corp_card_result['team'] : "";
            $card_num 	  = isset($new_corp_card_result['card_num']) ? $new_corp_card_result['card_num'] : "";
            $corp_card_no = isset($new_corp_card_result['cc_no']) ? $new_corp_card_result['cc_no'] : "";

            if(empty($s_no) || empty($team) || empty($card_num)) {
                exit("<script>alert('개인카드 정보가 없습니다');location.href='personal_expenses.php?{$search_url}'</script>");
            }

            if(!empty($priority) && !empty($payment_date) && !empty($appr_price) && !empty($money) && !empty($account_code))
            {
                $ins_multi_data[] = array(
                    "state"             => "1",
                    "my_c_no"           => $my_c_no,
                    "priority"          => $priority,
                    "team"              => $team,
                    "s_no"              => $s_no,
                    "req_s_no"          => $req_s_no,
                    "payment_date"      => $payment_date,
                    "wd_method"         => "5",
                    "card_num"          => $card_num,
                    "corp_card_no"      => $corp_card_no,
                    "money"             => $money,
                    "money_vat"         => $money_vat,
                    "regdate"           => $regdate,
                    "appr_price"        => $appr_price,
                    "payment_contents"  => $payment_contents,
                    "account_code"      => $account_code,
                );
            }
        }
	}

    if(!$expenses_model->multiInsert($ins_multi_data)){
        echo ("<script>alert('개인경비 추가에 실패 하였습니다');</script>");
    }else{
        echo ("<script>alert('개인경비 리스트에 추가 하였습니다');</script>");
    }
    exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");

}
elseif ($process == "set_withdraw")
{
	$sch_month      = (isset($_POST['sch_month'])) ? $_POST['sch_month'] : "";
    $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $add_where      = "pe.display = '1' AND DATE_FORMAT(pe.payment_date, '%Y-%m') BETWEEN '{$sch_month}' AND '{$sch_month}'";
    $withdraw_model = Withdraw::Factory();

	# 개인경비 지급요청(출금등록) 존재여부 확인 [START]
	$wd_no_count_sql    = "SELECT COUNT(pe.wd_no) as cnt FROM personal_expenses pe WHERE {$add_where}";
	$wd_no_count_query  = mysqli_query($my_db,$wd_no_count_sql);
	$wd_no_count_data   = mysqli_fetch_array($wd_no_count_query);

	if($wd_no_count_data['cnt'] != '0'){
		echo "<script>alert('개인경비 지급요청(출금등록)이 불가능 합니다.\\n출금등록이 이미 처리({$wd_no_count_data['cnt']})된 것으로 확인됩니다.\\n담당자에게 문의해주세요.');history.back();</script>";
		exit;
	}

	# 개인경비 지급요청 대상 직원 리스트 가져오기
	$pe_s_no_sql = "
        SELECT
            DISTINCT pe.s_no,
            pe.team,
            (SELECT s_name FROM staff s WHERE s.s_no=pe.s_no) AS s_name
        FROM personal_expenses pe
        WHERE {$add_where}
        ORDER BY s_name ASC
    ";
	$pe_s_no_query      = mysqli_query($my_db, $pe_s_no_sql);
    $wd_method_option   = getPeWdMethodOption();
    $regdate            = date("Y-m-d H:i:s");
	while($pe_s_no_data = mysqli_fetch_array($pe_s_no_query))
	{
		$pe_s_no_add_where = "";

		if(!$pe_s_no_data['s_no']) {
			echo "<script>alert('이름이 확인 되지 않는 사람이 있습니다.\\n지급요청(출금등록)이 불가능 합니다.\\n담당자에게 문의해주세요.');history.back();</script>";
			exit;
		}

		$pe_s_no_add_where  = " AND pe.s_no = '{$pe_s_no_data['s_no']}'";
		$add_order_by       = " pe.priority is NULL ASC, pe.priority ASC, pe.payment_date ASC";

        # 개인경비 내역 정리
        $cost_info      = ""; // 내역
        $cost_sum       = 0; // 금액 합산
        $cost_vat_sum   = 0; // 부가세 금액 합산

		# 개인경비 지급요청 대상 직원 리스트 별로 개인경배 내역 가져오기
		$personal_expenses_sql = "
            SELECT
                pe.pe_no,
                pe.my_c_no,
                (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=pe.my_c_no) as my_c_name,
                pe.payment_date,
                pe.account_code,
                (SELECT k_name FROM kind WHERE k_name_code=pe.account_code) AS account_code_name,
                pe.wd_method,
                pe.card_num,
                pe.payment_contents,
                pe.money,
                pe.money_vat
            FROM personal_expenses pe
            WHERE {$add_where} {$pe_s_no_add_where}
            ORDER BY {$add_order_by}
        ";
		$personal_expenses_query = mysqli_query($my_db, $personal_expenses_sql);
        $personal_base_c_no     = 0;
        $personal_base_c_name   = "";
		while($personal_expenses_data = mysqli_fetch_array($personal_expenses_query))
		{
            if(empty($personal_base_c_name)){
                $personal_base_c_no     = $personal_expenses_data['my_c_no'];
                $personal_base_c_name   = $personal_expenses_data['my_c_name'];
            }

			$cost_info      .= "{$personal_expenses_data['payment_date']} :: {$personal_expenses_data['account_code_name']} :: {$wd_method_option[$personal_expenses_data['wd_method']]} :: {$personal_expenses_data['card_num']} :: {$personal_expenses_data['payment_contents']} :: {$personal_expenses_data['money']} :: {$personal_expenses_data['money_vat']}\r\n";
			$cost_sum       += $personal_expenses_data['money'];
			$cost_vat_sum   += $personal_expenses_data['money_vat'];
		}

		$supply_cost    = $cost_sum - $cost_vat_sum; // 공급가액
		$withdraw_data  = array(
            "my_c_no"           => $session_my_c_no,
            "wd_subject"        => "[{$sch_month}] {$pe_s_no_data['s_name']}님의 개인경비",
            "wd_state"          => "2",
            "wd_method"         => "1",
            "c_no"              => $personal_base_c_no,
            "c_name"            => $personal_base_c_name,
            "s_no"              => $pe_s_no_data['s_no'],
            "team"              => $pe_s_no_data['team'],
            "vat_choice"        => "1",
            "cost"              => $cost_sum,
            "supply_cost"       => $supply_cost,
            "supply_cost_vat"   => $cost_vat_sum,
            "cost_info"         => $cost_info,
            "wd_tax_state"      => "4",
            "regdate"           => $regdate,
            "reg_s_no"          => $session_s_no,
        );

		if (!$withdraw_model->insert($withdraw_data)){
			echo "<script>alert('개인경비 지급요청(출금등록)에 실패하였습니다.\\n개발담당자에게 문의해주세요.');history.back();</script>";
			exit;
		}else{
			$last_wd_no = $withdraw_model->getInsertId();
			$pe_upd_sql = "UPDATE personal_expenses pe SET pe.state = '5', pe.wd_no = '{$last_wd_no}' WHERE {$add_where} {$pe_s_no_add_where} AND pe.wd_no IS NULL	";

			if (!mysqli_query($my_db, $pe_upd_sql)){
				echo ("<script>alert('개인경비 지급요청에 대한 출금번호 저장에 실패하였습니다.');history.back();</script>");
				exit;
			}
		}
	}

	exit ("<script>alert('전체 개인경비 지급요청(출금등록) 되었습니다.');location.href='personal_expenses.php?{$search_url}';</script>");
}
elseif ($process == "pe_delete")
{
	$pe_no      = (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "";
    $search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $del_data   = array("pe_no" => $pe_no, "display"=> "2");

    if (!$expenses_model->update($del_data)){
		echo ("<script>alert('삭제에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('삭제하였습니다');</script>");
	}
	exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");
}
elseif ($process == "pe_duplicate")
{
    $pe_no          = (isset($_POST['pe_no'])) ? $_POST['pe_no'] : "";
    $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $pe_item        = $expenses_model->getItem($pe_no);
    $staff_item     = $staff_model->getItem($session_s_no);

    $dup_data   = array(
        "state"             => "1",
        "my_c_no"           => $pe_item['my_c_no'],
        "team"              => $staff_item['team'],
        "s_no"              => $session_s_no,
        "req_s_no"          => $session_s_no,
        "payment_date"      => $pe_item['payment_date'],
        "account_code"      => $pe_item['account_code'],
        "wd_method"         => $pe_item['wd_method'],
        "card_num"          => $pe_item['card_num'],
        "payment_contents"  => addslashes($pe_item['payment_contents']."_복제"),
        "money"             => $pe_item['money'],
        "money_vat"         => $pe_item['money_vat'],
        "regdate"           => date("Y-m-d H:i:s")
    );

	if(!$expenses_model->insert($dup_data)){
		echo ("<script>alert('복제에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('복제하였습니다');</script>");
	}

	exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");
}
elseif ($process == "comment_delete")
{
	$cmt_no     = (isset($_POST['cmt_no'])) ? $_POST['cmt_no'] : "";
    $search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $custom_model->setMainInit("comment", "no");

	if(!$custom_model->update(array("no" => $cmt_no, "display" => "2"))){
		echo ("<script>alert('삭제에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('삭제하였습니다');</script>");
	}
	exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");
}
elseif ($process == "comment_add")
{
	$pe_no_new      = (isset($_POST['f_pe_no_new'])) ? $_POST['f_pe_no_new'] : "";
	$comment_new    = (isset($_POST['f_comment_new'])) ? $_POST['f_comment_new'] : "";
    $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $custom_model->setMainInit("comment", "no");

    $ins_data = array(
        "table_name"    => "personal_expenses",
        "table_no"      => $pe_no_new,
        "comment"       => addslashes($comment_new),
        "team"          => $session_team,
        "s_no"          => $session_s_no,
        "regdate"       => date("Y-m-d H:i:s"),
    );

	if(!$custom_model->insert($ins_data)){
		echo ("<script>alert('코멘트 추가에 실패 하였습니다');</script>");
	}
	exit ("<script>location.href='personal_expenses.php?{$search_url}';</script>");
}
elseif ($process == "save_sms_content")
{
	$sms_content    = (isset($_POST['sms_content'])) ? $_POST['sms_content'] : "";
    $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	if(!$kind_model->updateToArray(array("k_discription" => addslashes($sms_content)), array("k_code" => "sms", "k_name" => "개인경비"))){
		exit ("<script>alert('개인경비 문자 설정에 실패 하였습니다');</script>");
	}
	exit ("<script>alert('개인경비 문자 설정에 성공했습니다');location.href='personal_expenses.php?{$search_url}';</script>");
}
elseif ($process == "save_personal_date")
{
    $f_w_no          = (isset($_POST['f_w_no'])) ? $_POST['f_w_no'] : "";
    $f_deadline_date = (isset($_POST['f_deadline_date'])) ? $_POST['f_deadline_date'] : "";
    $search_url      = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $result          = false;

    if(!empty($f_w_no)){
        if($work_model->update(array("w_no" => $f_w_no, "task_run_dday" => $f_deadline_date))){
            $result = true;
        }
	}else{
        $ins_data = array(
            "work_state"    => "1",
            "c_no"          => "1580",
            "c_name"        => "와이즈플래닛 컴퍼니",
            "s_no"          => "1",
            "team"          => "00200",
            "prd_no"        => "179",
            "quantity"      => "1",
            "service_apply" => "1",
            "regdate"       => date("Y-m-d"),
            "task_req_s_no" => "29",
            "task_req_team" => "00211",
            "task_run_s_no" => "29",
            "task_run_team" => "00211",
            "task_run_dday" => $f_deadline_date,
        );

        if($work_model->insert($ins_data)){
            $result = true;
        }
	}

    if (!$result)
        exit ("<script>alert('개인경비 날짜 변경에 실패 하였습니다');</script>");
    else
        exit ("<script>alert('개인경비 날짜 변경에 성공했습니다');location.href='personal_expenses.php?{$search_url}';</script>");
}
else
{
    # Navigation & My Quick
    $nav_prd_no  = "59";
    $nav_title   = "개인경비";
    $quick_model = MyQuick::Factory();
    $is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

    $smarty->assign("is_my_quick", $is_my_quick);
    $smarty->assign("nav_title", $nav_title);
    $smarty->assign("nav_prd_no", $nav_prd_no);

    # 검색용 리스트
    $team_model             = Team::Factory();
    $team_all_list          = $team_model->getTeamAllList();
    $team_full_name_list    = $team_model->getTeamFullNameList();
    $kind_account_2_list    = $kind_model->getKindChildList("account_code");
    $sch_team_name_list     = [];

	# 검색쿼리
	$add_where              = " pe.display = 1";
	$sch_pe_no              = isset($_GET['sch_pe_no']) ? $_GET['sch_pe_no'] : "";
	$sch_req_s_name         = isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : $session_name;
	$sch_state              = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
    $sch_team               = isset($_GET['sch_team']) ? $_GET['sch_team'] : $session_team;
    $sch_share_card         = isset($_GET['sch_share_card']) ? $_GET['sch_share_card'] : "";
	$sch_s_month            = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : $pre_month_deadline_date; // 월간
	$sch_e_month            = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : $pre_month_deadline_date; // 월간
	$sch_month_all          = isset($_GET['sch_month_all']) ? $_GET['sch_month_all'] : ""; // 월간(전체)
	$sch_payment_date       = isset($_GET['sch_payment_date']) ? $_GET['sch_payment_date'] : "";
	$sch_account_code       = isset($_GET['sch_account_code']) ? $_GET['sch_account_code'] : "";
	$sch_wd_method          = isset($_GET['sch_wd_method']) ? $_GET['sch_wd_method'] : "";
	$sch_card_num           = isset($_GET['sch_card_num']) ? $_GET['sch_card_num'] : "";
	$sch_payment_contents   = isset($_GET['sch_payment_contents']) ? $_GET['sch_payment_contents'] : "";
    $sch_corp_my_c_no       = isset($_GET['sch_corp_my_c_no']) ? $_GET['sch_corp_my_c_no'] : "";
    $sch_fran_c_name        = isset($_GET['sch_fran_c_name']) ? $_GET['sch_fran_c_name'] : "";
	$sch_month              = ($sch_s_month == $sch_e_month) ? $sch_e_month : "";

    # 검색용 권한
    $is_sch_admin           = false;
    if(permissionNameCheck($session_permission, "재무관리자") && $session_team == '00211'){
        $is_sch_admin = true;
        $sch_team_name_list = $team_full_name_list;
    }elseif($session_s_no == "62"){
        $is_sch_admin = true;
        $sch_team_name_list = array("all" => ":: 전체 ::", "00251" => "입찰/마케팅 파트", "00252" => " >(구)입찰/마케팅 1팀" , "00254" => " >입찰/마케팅 1팀");
    }else{
        $sch_team_name_list = $team_all_list["team_name_list"];
        $sch_team           = $session_team;
        $sch_req_s_name     = $session_name;
    }
    $smarty->assign("is_sch_admin", $is_sch_admin);

    if(!empty($sch_corp_my_c_no)){
        if($sch_corp_my_c_no == 'default'){
            $add_where .= " AND pe.my_c_no IN(1,2,5,7)";
        }elseif($sch_corp_my_c_no == 'commerce'){
            $add_where .= " AND pe.my_c_no = 3";
        }
        $smarty->assign("sch_corp_my_c_no", $sch_corp_my_c_no);
    }

	if (!empty($sch_pe_no)) {
		$add_where .= " AND pe.pe_no='{$sch_pe_no}'";
		$smarty->assign("sch_pe_no", $sch_pe_no);
	}

    if (!empty($sch_req_s_name)) {
        $add_where .= " AND pe.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_req_s_name}')";
        $smarty->assign("sch_req_s_name", $sch_req_s_name);
    }

	if (!empty($sch_state)) {
		$add_where .= " AND pe.state='{$sch_state}'";
		$smarty->assign("sch_state", $sch_state);
	}

    if(!empty($sch_team)) {
        if ($sch_team != "all")
        {
            $sch_team_code_where = getTeamWhere($my_db, $sch_team);
            $add_where .= " AND pe.team IN ({$sch_team_code_where})";
        }
        elseif($sch_team == "all" && $session_s_no == '62')
        {
            if($sch_account_code != '03011'){
                $add_where .= " AND pe.team IN('00251','00252','00254')";
            }
        }
        $smarty->assign("sch_team", $sch_team);
    }

    if (!empty($sch_share_card)) {
        $add_where .= " AND pe.corp_card_no IN (SELECT `cc`.cc_no FROM corp_card `cc` WHERE `cc`.share_card like '%{$sch_share_card}%')";
		$smarty->assign("sch_share_card", $sch_share_card);
    }

	if($sch_month_all != 'all') {
		if (!empty($sch_s_month)) {
			$add_where .= " AND DATE_FORMAT(pe.payment_date, '%Y-%m') >= '{$sch_s_month}'";
		}

		if (!empty($sch_e_month)) {
			$add_where .= " AND DATE_FORMAT(pe.payment_date, '%Y-%m') <= '{$sch_e_month}' ";
		}
	}
	$smarty->assign("sch_month_all", $sch_month_all);
	$smarty->assign("sch_month", $sch_month);
	$smarty->assign("sch_s_month", $sch_s_month);
	$smarty->assign("sch_e_month", $sch_e_month);

	if (!empty($sch_payment_date)) {
		$add_where .= " AND pe.payment_date='{$sch_payment_date}'";
		$smarty->assign("sch_payment_date", $sch_payment_date);
	}

	if (!empty($sch_account_code)) {
		$add_where .= " AND pe.account_code='{$sch_account_code}'";
		$smarty->assign("sch_account_code", $sch_account_code);
	}

	if (!empty($sch_wd_method)) {
		$add_where .= " AND pe.wd_method='{$sch_wd_method}'";
		$smarty->assign("sch_wd_method", $sch_wd_method);
	}

	if (!empty($sch_card_num)) {
		$add_where .= " AND pe.card_num LIKE '%{$sch_card_num}%'";
		$smarty->assign("sch_card_num", $sch_card_num);
	}

	if (!empty($sch_payment_contents)) {
		$sch_payment_contents_list      = explode(" ", $sch_payment_contents);
		$sch_payment_contents_trim_list = [];

		foreach($sch_payment_contents_list as $sch_payment_contents_word){
			$sch_payment_contents_trim_list[] = trim($sch_payment_contents_word);
		}
		$sch_payment_contents_trim = implode("",$sch_payment_contents_trim_list);

		$add_where .= " AND (pe.payment_contents like '%{$sch_payment_contents}%' OR pe.payment_contents like '%{$sch_payment_contents_trim}%')";
		$smarty->assign("sch_payment_contents", $sch_payment_contents);
	}

    if (!empty($sch_fran_c_name)) {
        $add_where .= " AND pe.fran_c_name LIKE '%{$sch_fran_c_name}%'";
        $smarty->assign("sch_fran_c_name", $sch_fran_c_name);
    }

	# 정렬순서 토글 & 필드 지정
    $ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "priority";
    $ori_ord_type   = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "";
    $ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "1";
    $add_order_by   = "";
    $order_by_val   = "";
    if(!empty($ord_type))
    {
        if(!empty($ord_type_by))
        {
            if($ori_ord_type == $ord_type)
            {
                if($ord_type_by == '1'){
                    $order_by_val = "ASC";
                }elseif($ord_type_by == '2'){
                    $order_by_val = "DESC";
                }
            }else{
                $ord_type_by  = '1';
                $order_by_val = "ASC";
            }

            $add_order_by .= "ISNULL({$ord_type}) ASC, {$ord_type} {$order_by_val}, pe_no ASC";
        }
    }

    $smarty->assign('ord_type', $ord_type);
    $smarty->assign('ord_type_by', $ord_type_by);

	# 전체 수 & 페이징처리
    $pe_total_sql       = "SELECT COUNT(pe.pe_no) as total_cnt, SUM(pe.money) as total_pe_money FROM personal_expenses pe WHERE {$add_where}";
    $pe_total_query     = mysqli_query($my_db, $pe_total_sql);
    $pe_total_result    = mysqli_fetch_array($pe_total_query);
    $total_num          = $pe_total_result['total_cnt'];
    $total_pe_money     = $pe_total_result['total_pe_money'];

	$pages              = isset($_GET['page']) ?intval($_GET['page']) : 1;
    $ord_page_type      = isset($_GET['ord_page_type']) ? intval($_GET['ord_page_type']) : 100;

	$num                = $ord_page_type;
	$offset             = ($pages-1) * $num;
    $pagenum            = ceil($total_num/$num);

    if ($pages>=$pagenum){$pages=$pagenum;}
    if ($pages<=0){$pages=1;}

    $search_url = getenv("QUERY_STRING");
    $pagelist   = pagelist($pages, "personal_expenses.php", $pagenum, $search_url);

    $smarty->assign("total_num", $total_num);
    $smarty->assign("total_pe_money", $total_pe_money);
    $smarty->assign("page", $pages);
    $smarty->assign("ord_page_type", $ord_page_type);
    $smarty->assign("search_url", $search_url);
    $smarty->assign("pagelist", $pagelist);

	# 리스트 쿼리
	$personal_expenses_sql = "
		SELECT
            pe.*,
		    (SELECT t.team_name FROM team t WHERE t.team_code=(SELECT sub_t.team_code_parent FROM team sub_t WHERE sub_t.team_code=`pe`.team LIMIT 1)) AS parent_group_name,
			(SELECT t.team_name FROM team t WHERE t.team_code=pe.team) as group_name,
			(SELECT share_card FROM corp_card `cc` WHERE `cc`.cc_no = pe.corp_card_no LIMIT 1) as card_name,
			(SELECT s.staff_state FROM staff s where s.s_no=pe.s_no AND s.staff_state='1') AS s_display,
			(SELECT s.s_name FROM staff s where s.s_no=pe.s_no) AS s_name,
			(SELECT s.s_name FROM staff s where s.s_no=pe.req_s_no) AS req_s_name,
			(SELECT k_name FROM kind WHERE k_name_code=pe.account_code) AS account_code_name,
			(SELECT wd_state FROM withdraw wd where wd.wd_no=pe.wd_no) as wd_state,
			(SELECT regdate FROM withdraw wd where wd.wd_no=pe.wd_no) as wd_regdate,
			(SELECT wd_date FROM withdraw wd where wd.wd_no=pe.wd_no) as wd_date
		FROM personal_expenses pe
		WHERE {$add_where}
		ORDER BY {$add_order_by}
        LIMIT {$offset}, {$num}
	";
    $personal_expenses_query = mysqli_query($my_db, $personal_expenses_sql);
    $comment_table_no_list   = [];
    $personal_expenses_list  = [];
    $pe_comment_list         = [];
    $pe_idx                  = 0;
    $first_pe_no             = "";
	while($personal_expenses = mysqli_fetch_array($personal_expenses_query))
    {
        $personal_expenses['wd_regdate_day']    = !empty($personal_expenses['wd_regdate']) ? date("Y/m/d", strtotime($personal_expenses['wd_regdate'])) : "";
        $personal_expenses['wd_regdate_time']   = !empty($personal_expenses['wd_regdate']) ? date("H:i", strtotime($personal_expenses['wd_regdate'])) : "";
        $personal_expenses['regdate_day']       = !empty($personal_expenses['regdate']) ? date("Y/m/d", strtotime($personal_expenses['regdate'])) : "";
        $personal_expenses['regdate_time']      = !empty($personal_expenses['regdate']) ? date("H:i", strtotime($personal_expenses['regdate'])) : "";
        $personal_expenses['wd_pay_date']       = !empty($personal_expenses['wd_date']) ? date("Y-m-d", strtotime($personal_expenses['wd_date'])) : "";
        $personal_expenses["payment_contents"]  = htmlentities($personal_expenses['payment_contents']);

		if(!empty($personal_expenses['corp_card_no']) && empty($personal_expenses['card_num']))
		{
			$empty_card_sql     = "SELECT `cc`.team, `cc`.share_card, (SELECT t.team_name FROM team t WHERE t.team_code=(SELECT sub_t.team_code_parent FROM team sub_t WHERE sub_t.team_code=`cc`.team LIMIT 1)) as parent_group_name, (SELECT t.team_name FROM team t WHERE t.team_code=`cc`.team) as group_name FROM corp_card `cc` WHERE `cc`.card_type='3' AND `cc`.manager='{$personal_expenses['s_no']}'";
            $empty_card_query   = mysqli_query($my_db, $empty_card_sql);
            $empty_card_result  = mysqli_fetch_assoc($empty_card_query);

            if(isset($empty_card_result['share_card'])){
                $personal_expenses["card_name"]         = $empty_card_result['share_card'];
                $personal_expenses["parent_group_name"] = $empty_card_result['parent_group_name'];
                $personal_expenses["group_name"]        = $empty_card_result['group_name'];
			}
		}

        $personal_expenses['card_num']      = !empty($personal_expenses['card_num']) ? substr($personal_expenses['card_num'],-4) : "";
        $personal_expenses['is_admin']      = false;
        $personal_expenses['is_editable']   = false;

        if(permissionNameCheck($session_permission,"재무관리자")){
            if($session_team == "00211"){
                $personal_expenses['is_admin']      = true;
            }
            $personal_expenses['is_editable']   = true;
        }elseif(($personal_expenses['s_no'] == $session_s_no || $personal_expenses['req_s_no'] == $session_s_no) && $personal_expenses['wd_no'] == 0 && $personal_expenses['state'] == '1'){
            $personal_expenses['is_editable']   = true;
        }

        if($pe_idx == 0){
            $first_pe_no = $personal_expenses['pe_no'];
        }

        $comment_table_no_list[]    = $personal_expenses['pe_no'];
		$personal_expenses_list[]   = $personal_expenses;

        $pe_idx++;
	}

    # 코멘트 리스트
	if(!empty($comment_table_no_list))
	{
        $comment_table_no   = implode(",", $comment_table_no_list);
		$comment_sql        = "
			SELECT
				cmt.no,
				cmt.table_name,
				cmt.table_no,
				cmt.comment,
				cmt.team,
				(SELECT t.team_name FROM team t WHERE t.team_code=cmt.team) as t_name,
				cmt.s_no,
				(SELECT s_name FROM staff s where s.s_no=cmt.s_no) AS s_name,
				cmt.regdate
			FROM `comment` cmt
			WHERE cmt.table_no IN ({$comment_table_no}) AND cmt.table_name='personal_expenses' AND cmt.display = '1'
			ORDER BY cmt.no ASC
		";
        $comment_query = mysqli_query($my_db, $comment_sql);
		while($pe_comment = mysqli_fetch_array($comment_query))
        {
            $pe_comment['comment']      = str_replace("\r\n","<br />",htmlspecialchars($pe_comment['comment']));
            $pe_comment['regdate_day']  = date("Y/m/d", strtotime($pe_comment['regdate']));
            $pe_comment['regdate_time'] = date("H:i", strtotime($pe_comment['regdate']));

            $pe_comment_list[] = $pe_comment;
		}
	}

	#개인경비 SMS 문자 내용 및 발송일정
	$sms_content_sql    = "SELECT k_discription FROM kind WHERE k_code='sms' LIMIT 1";
	$sms_content_query  = mysqli_query($my_db, $sms_content_sql);
	$sms_content_result = mysqli_fetch_assoc($sms_content_query);

	$sms_content_val    = isset($sms_content_result['k_discription']) ? $sms_content_result['k_discription'] : "";
	$date_w 		    = getDayShortOption();
	$prev_sms_date 	    = "";
	$cur_sms_date  	    = "";

	# 전월 발송일정
	$prev_base_date     = date("Y-m", strtotime('-1 month'))."-23";
    $prev_base_date_w 	= date('w', strtotime($prev_base_date));
    if($prev_base_date_w == '0'){
        $prev_base_date = date('Y-m-d', strtotime("{$prev_base_date} +1 day"));
    }elseif($prev_base_date_w == '6'){
        $prev_base_date = date('Y-m-d', strtotime("{$prev_base_date} +2 day"));
    }
    $prev_date   = date('Y-m-d', strtotime("{$prev_base_date} -1 day"));
    $prev_date_w = date('w', strtotime("{$prev_base_date}"));

    # 이번달 발송일정
	$cur_base_date      = date("Y-m")."-23";
    $cur_base_date_w 	= date('w', strtotime($cur_base_date));
	if($cur_base_date_w == '0'){
        $cur_base_date  = date('Y-m-d', strtotime("{$cur_base_date} +1 day"));
	}elseif($cur_base_date_w == '6'){
        $cur_base_date  = date('Y-m-d', strtotime("{$cur_base_date} +2 day"));
	}
    $cur_date   = date('Y-m-d', strtotime("{$cur_base_date} -1 day"));
    $cur_date_w = date('w', strtotime("{$cur_base_date}"));

	$prev_sms_date  = "{$prev_date}({$date_w[$prev_date_w]})";
	$cur_sms_date   = "{$cur_date}({$date_w[$cur_date_w]})";

	$my_company_model   = MyCompany::Factory();
    $my_company_list    = $my_company_model->getList();

    $smarty->assign("order_field_option", getPeOrderOption());
    $smarty->assign("page_type_option", getPageTypeOption(4));
    $smarty->assign("sch_corp_my_c_list", getPeMyCompanyOption());
    $smarty->assign("wd_method_option", getPeWdMethodOption());
    $smarty->assign("pe_state_option", getPeStateOption());
    $smarty->assign("my_company_list", $my_company_list);
    $smarty->assign("sch_team_list", $sch_team_name_list);
    $smarty->assign("kind_account_2_list", $kind_account_2_list);
	$smarty->assign("personal_sms_content", $sms_content_val);
	$smarty->assign("prev_sms_date", $prev_sms_date);
	$smarty->assign("cur_sms_date", $cur_sms_date);
    $smarty->assign("first_pe_no", $first_pe_no);
	$smarty->assign("pe_comment_list", $pe_comment_list);
    $smarty->assign("personal_expenses_list", $personal_expenses_list);

	$smarty->display('personal_expenses.html');
}
?>