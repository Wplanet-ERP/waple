<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$nowdate = date("Y-m-d H:i:s");
$lfcr    = chr(10) ;

# 검색 조건
$add_where          = "w.quick_state='2' AND w.delivery_method='2' AND w.sender_type='2809' AND w.run_s_no > 0";

$check_user_list    = [];
$check_user_sql     = "SELECT DISTINCT w.run_s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=w.run_s_no) as run_s_name FROM work_cms_quick w WHERE {$add_where} AND w.order_number is not null ORDER BY w.w_no DESC";
$check_user_query   = mysqli_query($my_db, $check_user_sql);
while($check_user = mysqli_fetch_assoc($check_user_query))
{
    $check_user_list[$check_user['run_s_no']] = $check_user['run_s_name'];
}

$sheet_idx = 0;
foreach($check_user_list as $check_user => $run_s_name)
{
    if($sheet_idx > 0){
        $objPHPExcel->createSheet($sheet_idx);
    }

    $work_sheet = $objPHPExcel->setActiveSheetIndex($sheet_idx);
    $idx        = 3;

    $cms_ord_sql        = "SELECT DISTINCT w.order_number FROM work_cms_quick w WHERE {$add_where} AND w.order_number is not null AND w.run_s_no='{$check_user}' ORDER BY w.w_no DESC";
    $cms_ord_query      = mysqli_query($my_db, $cms_ord_sql);
    $order_number_list  = [];
    while($order_number = mysqli_fetch_assoc($cms_ord_query)){
        $order_number_list[] =  "'".$order_number['order_number']."'";
    }
    $order_numbers = implode(',', $order_number_list);

    $work_cms_quick_sql = "
        SELECT
            DATE_FORMAT(w.regdate, '%Y-%m-%d') as reg_day,
            DATE_FORMAT(w.regdate, '%H:%i') as reg_hour,
            DATE_FORMAT(w.req_date, '%Y-%m-%d') as req_day,
            w.w_no,
            w.order_number,
            w.c_name,
            w.prd_no,
            w.prd_type,
            w.quantity,
            w.receiver,
            w.receiver_hp,
            w.zip_code,
            w.receiver_addr,
            w.req_memo,
            w.quick_memo
        FROM work_cms_quick w
        WHERE {$add_where} AND order_number IN({$order_numbers})
        ORDER BY w.w_no DESC, w.order_number
        LIMIT 10000
    ";
    $work_cms_quick_query	= mysqli_query($my_db, $work_cms_quick_sql);
    $quick_list = [];
    $total_list = [];
    if(!!$work_cms_quick_query)
    {
        while($work_cms_quick = mysqli_fetch_array($work_cms_quick_query))
        {
            $unit_list  = [];
            if($work_cms_quick['prd_type'] == '1'){
                $unit_sql   = "SELECT pcr.option_no, (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='2809') as sku, pcr.quantity as qty FROM product_cms_relation as pcr WHERE pcr.prd_no ='{$work_cms_quick['prd_no']}' AND pcr.display='1'";
                $unit_query = mysqli_query($my_db, $unit_sql);
                while($unit_result = mysqli_fetch_assoc($unit_query)){
                    $unit_result['qty'] = $work_cms_quick['quantity']*$unit_result['qty'];
                    $unit_list[]        = $unit_result;
                    $total_list[$work_cms_quick['order_number']] += $unit_result['qty'];
                }
            }else{
                $unit_sql       = "SELECT pcum.prd_unit, pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit='{$work_cms_quick['prd_no']}' AND pcum.log_c_no='2809'";
                $unit_query     = mysqli_query($my_db, $unit_sql);
                $unit_result    = mysqli_fetch_assoc($unit_query);
                $unit_list[]    = array("sku" => $unit_result['sku'], "qty" => $work_cms_quick['quantity']);
                $total_list[$work_cms_quick['order_number']] += $work_cms_quick['quantity'];
            }

            $work_cms_quick["unit_list"] = $unit_list;
            $quick_list[$work_cms_quick['order_number']][] = $work_cms_quick;
        }
    }

    if(!empty($quick_list))
    {
        //상단타이틀
        $work_sheet
            ->setCellValue('A2', "w_no{$lfcr}작성일")
            ->setCellValue('B2', "운송형태")
            ->setCellValue('C2', "주문번호")
            ->setCellValue('D2', "수령자명{$lfcr}수령자전화{$lfcr}[우편번호]{$lfcr}수령지")
            ->setCellValue('E2', "업체명/브랜드명")
            ->setCellValue('F2', "구성품목(SKU) * 수량{$lfcr}(수량 = 구성품수량 X 상품수량)")
            ->setCellValue('G2', "총수량")
            ->setCellValue('H2', "입/출고형태")
            ->setCellValue('I2', "출고희망일")
            ->setCellValue('J2', "요청사항")
        ;

        foreach($quick_list as $quick_data)
        {
            $row_cnt    = count($quick_data)-1;
            $start_idx  = $idx;
            $end_idx    = $idx+$row_cnt;

            foreach($quick_data as $prd_data)
            {
                $unit_text = "";
                foreach($prd_data['unit_list'] as $unit_data){
                    $unit_text .= "{$unit_data['sku']} * {$unit_data['qty']}".$lfcr;
                }

                if($idx == $start_idx)
                {
                    $work_sheet->mergeCells("B{$start_idx}:B{$end_idx}");
                    $work_sheet->mergeCells("C{$start_idx}:C{$end_idx}");
                    $work_sheet->mergeCells("D{$start_idx}:D{$end_idx}");
                    $work_sheet->mergeCells("G{$start_idx}:G{$end_idx}");
                    $work_sheet->mergeCells("H{$start_idx}:H{$end_idx}");
                    $work_sheet->mergeCells("I{$start_idx}:I{$end_idx}");
                    $work_sheet->mergeCells("J{$start_idx}:J{$end_idx}");

                    $work_sheet
                        ->setCellValue("C{$idx}", $prd_data['order_number'])
                        ->setCellValue("D{$idx}", $prd_data['receiver'].$lfcr.$prd_data['receiver_hp'].$lfcr."(".$prd_data['zip_code'].")".$lfcr.$prd_data['receiver_addr'])
                        ->setCellValue("G{$idx}", $total_list[$prd_data['order_number']])
                        ->setCellValue("I{$idx}", $prd_data['req_day'])
                        ->setCellValue("J{$idx}", "[EMP 입고/반출 메모 삽입요청]".$lfcr.$prd_data['quick_memo'].$lfcr.$prd_data['req_memo'])
                    ;
                }

                $work_sheet
                    ->setCellValue("A{$idx}", $prd_data['w_no'].$lfcr.$prd_data['reg_day'].$lfcr.$prd_data['reg_hour'])
                    ->setCellValue("E{$idx}", $prd_data['c_name'])
                    ->setCellValue("F{$idx}", $unit_text)
                ;

                $idx++;
            }
        }
    }
    $idx--;


    $work_sheet->mergeCells('A1:J1');
    $objPHPExcel->getActiveSheet()->setCellValue('A1', "와이즈미디어커머스 퀵/화물 출고건 작업지시서");
    $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setSize(18);
    $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A2:J2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
    $objPHPExcel->getActiveSheet()->getStyle('A2:J2')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A2:J{$idx}")->getFont()->setSize(9);
    $objPHPExcel->getActiveSheet()->getStyle("A2:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:J{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

    $objPHPExcel->getActiveSheet()->getStyle("A2:A{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("D2:D{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setWrapText(true);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(40);
    $objPHPExcel->getActiveSheet()->getStyle("A1:J{$idx}")->applyFromArray($styleArray);

    $objPHPExcel->getActiveSheet()->setTitle("{$run_s_name} 퀵화물 리스트");

    $sheet_idx++;
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_퀵화물 출고건 작업지시서.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
