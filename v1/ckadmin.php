<?php

if($_SESSION["ss_id"]=="")
{
    if($_SESSION["ss_my_c_type"] == '3'){
        alert("시간이 만료되어 로그아웃되었거나 접속권한이 없습니다. 로그인후 사용하여 주세요.", $baseur2 . "work_list_cms_login.php");
    }elseif($_SESSION["ss_my_c_type"] == '2') {
        alert("시간이 만료되어 로그아웃되었거나 접속권한이 없습니다. 로그인후 사용하여 주세요.", $baseur2 . "partners_login.php");
    }else {
        alert("시간이 만료되어 로그아웃되었거나 접속권한이 없습니다. 로그인후 사용하여 주세요.", $baseur2 . "login.php");
    }
}

if(isset($_SESSION["ss_first_login"]) && $_SESSION["ss_first_login"] == '1')
{
    unset($_SESSION["ss_first_login"]);
    
    # 알림 생성용
    $chat_chk_day           = date("Y-m-d");
    $chat_chk_five_day      = date("Y-m-d", strtotime("-5 days"));
    $chat_chk_week_day      = date("Y-m-d", strtotime("-1 weeks"));
    $chat_chk_s_date        = $chat_chk_day." 00:00:00";
    $chat_chk_s_week_date   = $chat_chk_week_day." 00:00:00";
    $chat_chk_e_date        = $chat_chk_day." 23:59:59";
    $chat_chk_five_date     = $chat_chk_five_day." 00:00:00";
    $team_chk_date          = date('Y-m-d', strtotime("+5 days"));
    $return_chk_date        = date('Y-m-d', strtotime("-3 days"));

    # 발주서 입고여부 확인
    $first_chk_sql   = "SELECT * FROM product_cms_stock_report pcsr WHERE is_order='1' AND ord_no <= 0 AND regdate > '2022-04-30 23:59:59' AND prd_unit IN(SELECT DISTINCT `no` FROM product_cms_unit WHERE (ord_s_no='{$_SESSION["ss_s_no"]}' OR ord_sub_s_no='{$_SESSION["ss_s_no"]}' OR ord_third_s_no='{$_SESSION["ss_s_no"]}') AND display='1') ORDER BY regdate DESC";
    $first_chk_query = mysqli_query($my_db, $first_chk_sql);
    $first_chk_list  = [];
    while($first_chk = mysqli_fetch_assoc($first_chk_query)){
        $first_chk_list[] = $first_chk;
    }

    if(!empty($first_chk_list))
    {
        # 등록된 건인지 체크(하루 한번 생성)
        $chat_chk_sql    = "SELECT COUNT(wcc_no) as cnt FROM waple_chat_content WHERE wc_no='2' AND s_no='{$_SESSION["ss_s_no"]}' AND regdate BETWEEN '{$chat_chk_s_week_date}' AND '{$chat_chk_e_date}' AND alert_type='11'";
        $chat_chk_query  = mysqli_query($my_db, $chat_chk_sql);
        $chat_chk_result = mysqli_fetch_assoc($chat_chk_query);

        if($chat_chk_result['cnt'] == 0)
        {
            $first_chk_msg  = date("Y-m-d", strtotime($first_chk_list[0]['regdate']))." 발주서 입고로 보이는 ".$first_chk_list[0]['sku']." (+".number_format($first_chk_list[0]['stock_qty'],0)."개) ";
            $first_chk_msg .= count($first_chk_list) > 1 ? "외 ".(count($first_chk_list)-1)."건이 있습니다." : "이 있습니다.";
            $first_chk_msg .= " 발주서입고 여부를 확인 후 해당 발주서에 입고완료를 반드시 적용해 주세요.";
            $first_chk_msg .= "\r\nhttps://work.wplanet.co.kr/v1/commerce_order_stock.php";
            $first_chk_msg  = addslashes($first_chk_msg);
            $chk_ins_sql    = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$_SESSION["ss_s_no"]}', content='{$first_chk_msg}', alert_type='11', regdate=now()";
            mysqli_query($my_db, $chk_ins_sql);
        }
    }

    # 이벤트 종료 알림(5일전)
    $first_event_chk_sql    = "SELECT (SELECT k.k_name FROM kind k WHERE k.k_name_code=main.k_name_code) AS brand, gift_e_date FROM product_cms as main WHERE manager='{$_SESSION["ss_s_no"]}' AND display='1' AND ((gift_date_type='1' AND gift_e_date > NOW() AND DATEDIFF(gift_e_date, NOW()) <= 5) OR (sub_gift_date_type='1' AND sub_gift_e_date > NOW() AND DATEDIFF(sub_gift_e_date, NOW()) <= 5)) GROUP BY manager LIMIT 1";
    $first_event_chk_query  = mysqli_query($my_db, $first_event_chk_sql);
    $first_event_chk_list   = [];
    if($first_event_chk_query)
    {
        $first_event_chk_result = mysqli_fetch_assoc($first_event_chk_query);
        if (!empty($first_event_chk_result))
        {
            # 등록된 건인지 체크(5일전까지 체크)
            $chat_chk_sql    = "SELECT COUNT(wcc_no) as cnt FROM waple_chat_content WHERE wc_no='2' AND s_no='{$_SESSION["ss_s_no"]}' AND regdate BETWEEN '{$chat_chk_five_date}' AND '{$chat_chk_e_date}' AND alert_type='71'";
            $chat_chk_query  = mysqli_query($my_db, $chat_chk_sql);
            $chat_chk_result = mysqli_fetch_assoc($chat_chk_query);

            if($chat_chk_result['cnt'] == 0)
            {
                $first_event_chk_msg    = "[{$first_event_chk_result['brand']}]의 사은품 설정이 {$first_event_chk_result['gift_e_date']}에 종료됩니다. 연장시 날짜를 수정해주세요.";
                $first_event_chk_msg    = addslashes($first_event_chk_msg);
                $chk_ins_sql            = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$_SESSION["ss_s_no"]}', content='{$first_event_chk_msg}', alert_type='71', regdate=now()";
                mysqli_query($my_db, $chk_ins_sql);
            }
        }
    }

    # 퇴사자 알림
    $retire_staffs = [];
    if($_SESSION["ss_s_no"] == '18')
    {
        $staff_chk_sql   = "SELECT s_no, s_name FROM staff WHERE staff_state = '1' AND retirement_date BETWEEN '{$chat_chk_day}' AND '{$team_chk_date}'";
        $staff_chk_query = mysqli_query($my_db, $staff_chk_sql);
        while($staff_chk = mysqli_fetch_assoc($staff_chk_query)){
            $retire_staffs[$staff_chk['s_no']] = $staff_chk['s_name'];
        }
    }
    else{
        $team_retire_sql    = "SELECT team_code FROM team WHERE display='1' AND team_leader='{$_SESSION["ss_s_no"]}'";
        $team_retire_query  = mysqli_query($my_db, $team_retire_sql);
        while($team_retire  = mysqli_fetch_assoc($team_retire_query))
        {
            $team_where      = getTeamWhere($my_db, $team_retire['team_code']);
            $staff_chk_sql   = "SELECT s_no, s_name FROM staff WHERE team IN({$team_where}) AND staff_state = '1' AND retirement_date BETWEEN '{$chat_chk_day}' AND '{$team_chk_date}'";
            $staff_chk_query = mysqli_query($my_db, $staff_chk_sql);
            while($staff_chk = mysqli_fetch_assoc($staff_chk_query)){
                $retire_staffs[$staff_chk['s_no']] = $staff_chk['s_name'];
            }
        }
    }

    if(!empty($retire_staffs))
    {
        # 등록된 건인지 체크(한번만 생성)
        foreach($retire_staffs as $retire_s_no => $retire_s_name)
        {
            $chat_chk_sql    = "SELECT COUNT(wcc_no) as cnt FROM waple_chat_content WHERE wc_no='2' AND s_no='{$_SESSION["ss_s_no"]}' AND alert_type='31' AND alert_check='RETIRE_{$retire_s_no}'";
            $chat_chk_query  = mysqli_query($my_db, $chat_chk_sql);
            $chat_chk_result = mysqli_fetch_assoc($chat_chk_query);

            if($chat_chk_result['cnt'] == 0)
            {
                $retire_chk_msg = "[와플 보안 주의]\r\n퇴사자 {$retire_s_name}에 대한 와플 외의 권한을 반드시 해지 해 주시기 바랍니다.\r\nex) 외부 권한 = 페이스북, 구글ADS, e카운트, 은행/카드사 등 와플 권한(하드코딩 부분) = 매출및비용관리, UTM";
                $retire_chk_msg = addslashes($retire_chk_msg);
                $chk_ins_sql    = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$_SESSION["ss_s_no"]}', content='{$retire_chk_msg}', alert_type='31', alert_check='RETIRE_{$retire_s_no}', regdate=now()";
                mysqli_query($my_db, $chk_ins_sql);
            }
        }
    }

    # 자산승인 반납요청 알림
    $asset_staff_chk_sql   = "SELECT s_no FROM staff WHERE staff_state = '1' AND retirement_date BETWEEN '{$chat_chk_day}' AND '{$team_chk_date}'";
    $asset_staff_chk_query = mysqli_query($my_db, $asset_staff_chk_sql);
    $retire_asset_staffs   = [];
    while($staff_chk = mysqli_fetch_assoc($asset_staff_chk_query)){
        $retire_asset_staffs[] = $staff_chk['s_no'];
    }

    if(!empty($retire_asset_staffs))
    {
        # 등록된 건인지 체크(한번만 생성)
        foreach($retire_asset_staffs as $as_s_no)
        {
            $retire_asset_sql    = "SELECT count(as_r_no) as cnt, req_name FROM asset_reservation as asr LEFT JOIN asset as `a` ON a.as_no=asr.as_no WHERE asr.work_state='2' AND asr.req_no='{$as_s_no}' AND a.share_type IN(1,2) AND (asr.manager='{$_SESSION["ss_s_no"]}' OR asr.sub_manager='{$_SESSION["ss_s_no"]}')";
            $retire_asset_query  = mysqli_query($my_db, $retire_asset_sql);
            $retire_asset_result = mysqli_fetch_assoc($retire_asset_query);

            if($retire_asset_result['cnt'] > 0)
            {
                $chat_chk_sql    = "SELECT COUNT(wcc_no) as cnt FROM waple_chat_content WHERE wc_no='2' AND s_no='{$_SESSION["ss_s_no"]}' AND alert_type='32' AND alert_check='RETIRE_ASSET_{$as_s_no}'";
                $chat_chk_query  = mysqli_query($my_db, $chat_chk_sql);
                $chat_chk_result = mysqli_fetch_assoc($chat_chk_query);

                if($chat_chk_result['cnt'] == 0)
                {
                    $retire_asset_chk_msg = "[와플 자산 반납 안내]\r\n퇴사자 {$retire_asset_result['req_name']}에 대한 사용중인 와플자산 {$retire_asset_result['cnt']}건을 모두 반납처리 해 주세요.\r\n※중요 보안 대상의 계정 자산의 경우 비밀번호 변경을 진행해주세요.";
                    $retire_asset_chk_msg.= "\r\nhttps://work.wplanet.co.kr/v1/asset_reservation.php?sch_work_state=2&sch_manager={$_SESSION["ss_name"]}&sch_req_name={$retire_asset_result['req_name']}";
                    $retire_asset_chk_msg = addslashes($retire_asset_chk_msg);
                    $chk_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$_SESSION["ss_s_no"]}', content='{$retire_asset_chk_msg}', alert_type='32', alert_check='RETIRE_ASSET_{$as_s_no}', regdate=now()";
                    mysqli_query($my_db, $chk_ins_sql);
                }
            }
        }
    }


    # 자산비밀번호 변경알림
    $asset_password_sql     = "SELECT as_no, `name`, pw_date, pw_term FROM asset WHERE pw_date != '' AND (manager='{$_SESSION["ss_s_no"]}' OR sub_manager='{$_SESSION["ss_s_no"]}')";
    $asset_password_query   = mysqli_query($my_db, $asset_password_sql);
    while($asset_password_result  = mysqli_fetch_assoc($asset_password_query))
    {
        $pw_term     = $asset_password_result['pw_term'];
        $pw_date_val = date("Y-m-d", strtotime("{$asset_password_result['pw_date']} +{$pw_term} months"));
        $pw_idx = 1;
        while($chat_chk_day >= $pw_date_val)
        {
            if($chat_chk_day == $pw_date_val)
            {
                # 등록된 건인지 체크(하루 한번 생성)
                $chat_chk_sql    = "SELECT COUNT(wcc_no) as cnt FROM waple_chat_content WHERE wc_no='2' AND s_no='{$_SESSION["ss_s_no"]}' AND regdate BETWEEN '{$chat_chk_s_date}' AND '{$chat_chk_e_date}' AND alert_type='52'";
                $chat_chk_query  = mysqli_query($my_db, $chat_chk_sql);
                $chat_chk_result = mysqli_fetch_assoc($chat_chk_query);

                if($chat_chk_result['cnt'] == 0)
                {
                    $pass_chk_msg  = "[와플 자산 변경 안내]\r\n보안을 위해 `{$asset_password_result['name']}`의 비밀번호를 변경해 주세요.";
                    $pass_chk_msg .= "\r\nhttps://work.wplanet.co.kr/v1/asset_regist.php?as_no={$asset_password_result['as_no']}";
                    $pass_chk_msg  = addslashes($pass_chk_msg);
                    $chk_ins_sql   = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$_SESSION["ss_s_no"]}', content='{$pass_chk_msg}', alert_type='52', regdate=now()";
                    mysqli_query($my_db, $chk_ins_sql);
                }
            }
            $pw_term     = $asset_password_result['pw_term']*$pw_idx;
            $pw_date_val = date("Y-m-d", strtotime("{$asset_password_result['pw_date']} +{$pw_term} months"));
            $pw_idx++;
        }
    }
}

$session_my_c_no 		= $_SESSION["ss_my_c_no"];
$session_id 			= $_SESSION["ss_id"];
$session_s_no 			= $_SESSION["ss_s_no"];
$session_name 			= $_SESSION["ss_name"];
$session_team 			= $_SESSION["ss_team"];
$session_team_parent	= $_SESSION["ss_team_parent"];
$session_position 		= $_SESSION["ss_position"];
$session_pos_permission = isset($_SESSION["ss_pos_permission"]) ? $_SESSION["ss_pos_permission"] : "1";
$session_permission 	= $_SESSION["ss_permission"];
$session_staff_state 	= $_SESSION["ss_staff_state"];
$session_my_c_type 	    = $_SESSION["ss_my_c_type"];
$session_partner 	    = isset($_SESSION["ss_partner"]) ? $_SESSION["ss_partner"] : "";

# 허용 URL 정리(2: PARTNERS, 3: DELIVERY)
$allow_freelancer_url = array(
    "2" => array(
        "/v1/api/comm.utm.api.php",
        "/v1/media_commerce_utm_management.php",
        "/v1/work_cms_visit_list.php",
        "/v1/excel_work_cms_visit_list.php",
        "/v1/work_cms_visit_insert.php",
        "/v1/board/board_normal_list.php",
        "/v1/board/board_normal_detail.php",
        "/v1/out_return_qc_list.php",
        "/v1/out_return_mi_list.php",
        "/v1/out_return_mi_regist.php",
        "/v1/excel_out_return_qc_list.php",
        "/v1/excel_out_return_mi_list.php",
        "/v1/asset_reservation_calendar.php",
        "/v1/api/asset_reservation.modal.api.php",
        "/v1/api/asset.api.php",
        "/v1/api/asset_reservation.check.api.php",
        "/v1/work_list_cms.php",
        "/v1/work_list_cs.php",
        "/v1/work_list_cs_comment_iframe.php",
        "/v1/work_cms_return_window.php",
        "/v1/work_cms_return_list.php"
    ),
    "3" => array(
        "/v1/work_list_cms.php",
        "/v1/work_cms_return_list.php",
        "/v1/work_cms_return_window.php",
        "/v1/work_cms_return_extra.php",
        "/v1/work_cms_delivery_insert.php",
        "/v1/work_cms_delivery_insert_additional.php",
        "/v1/excel_work_list_cms_emp.php",
        "/v1/work_cms_sms_popup.php",
        "/v1/excel_work_list_cms.php",
        "/v1/with_product_cms_list.php",
        "/v1/with_product_cms_unit.php",
        "/v1/board/board_normal_list.php",
        "/v1/board/board_normal_detail.php",
        "/v1/work_cms_return_delivery_insert.php",
        "/v1/excel_work_cms_return_list.php",
        "/v1/excel_work_cms_return_list_emp.php",
        "/v1/excel_work_cms_return_list_delivery.php",
        "/v1/excel_work_cms_return_list_delivery_cj.php",
        "/v1/ajax_proc_data.php",
        "/v1/popup/quick_prd_set.php",
        "/v1/popup/quick_prd_sel.php",
        "/v1/api/quick.product.info.php",
        "/v1/api/quick.product.json.php",
        "/v1/product_cms_load_list.php",
        "/v1/work_cms_quick_list.php",
        "/v1/work_cms_quick_delivery_insert.php",
        "/v1/excel_work_cms_quick_list.php",
        "/v1/excel_work_cms_quick_list_with.php",
        "/v1/excel_work_cms_quick_list_work.php",
        "/v1/logistics.file_download.php",
        "/v1/calendar/calendar_schedule.php",
        "/v1/calendar/calendar_schedule_regist.php",
        "/v1/inc/calendar_file_upload.php"
    )
);

$allow_partner_url = array(
    "dansaek"   => array("/v1/api/comm.utm.api.php", "/v1/media_commerce_utm_management.php"),
    "nes"       => array("/v1/work_cms_visit_list.php", "/v1/excel_work_cms_visit_list.php", "/v1/work_cms_visit_insert.php"),
    "csis"      => array("/v1/board/board_normal_list.php", "/v1/board/board_normal_detail.php", "/v1/work_list_cms.php", "/v1/work_list_cs.php", "/v1/work_list_cs_comment_iframe.php","/v1/work_cms_return_window.php", "/v1/work_cms_return_list.php"),
    "seongill"  => array("/v1/out_return_qc_list.php", "/v1/out_return_mi_list.php", "/v1/out_return_mi_regist.php", "/v1/excel_out_return_qc_list.php", "/v1/excel_out_return_mi_list.php"),
    "dewbell"   => array("/v1/out_return_qc_list.php", "/v1/out_return_mi_list.php", "/v1/out_return_mi_regist.php", "/v1/excel_out_return_qc_list.php", "/v1/excel_out_return_mi_list.php"),
    "pico"      => array("/v1/out_return_qc_list.php", "/v1/out_return_mi_list.php", "/v1/out_return_mi_regist.php", "/v1/excel_out_return_qc_list.php", "/v1/excel_out_return_mi_list.php"),
    "paino"     => array("/v1/out_return_qc_list.php", "/v1/out_return_mi_list.php", "/v1/out_return_mi_regist.php", "/v1/excel_out_return_qc_list.php", "/v1/excel_out_return_mi_list.php"),
    "wp_guest"  => array("/v1/asset_reservation_calendar.php","/v1/api/asset_reservation.modal.api.php","/v1/api/asset.api.php","/v1/api/asset_reservation.check.api.php"),
);

$allow_freelancer_login = array(
    "2" 	=> "/v1/partners_login.php",
    "3" 	=> "/v1/work_list_cms_login.php"
);

if($session_staff_state != '1')
{
    $request_uri 	    	= isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']) ? explode("?", $_SERVER['REQUEST_URI'])[0] : "";
    $request_exp            = !empty($request_uri) ? explode("/", $request_uri) : "";
    $request_cnt            = !empty($request_exp) ? count($request_exp) : 0;
    $freelancer_url     	= isset($allow_freelancer_url[$session_my_c_type]) && !empty($request_uri) ? $allow_freelancer_url[$session_my_c_type] : "";
    $freelancer_default 	= $freelancer_url[0];
    $throw_url 				= $allow_freelancer_login[$session_my_c_type];
    $freelancer_partner_url = isset($allow_partner_url[$session_partner]) && !empty($request_uri) ? $allow_partner_url[$session_partner] : "";

    if($request_cnt > 3){
        $dir_location = "../";
        $smarty->assign("dir_location",$dir_location);
    }

    if($request_uri != '/v1/navibar_top.php')
    {
        if(empty($freelancer_url) || empty($throw_url)){
            $smarty->display('access_error.html');
            exit;
        }else{
            if(!in_array($request_uri, $freelancer_url)){
                exit("<script>alert('허용된 URL이 아닙니다.');location.href='{$throw_url}';</script>");
            }elseif($session_my_c_type == '2' && !in_array($request_uri, $freelancer_partner_url)){
               exit("<script>alert('허용된 URL이 아닙니다.');location.href='{$throw_url}';</script>");
            }
        }
    }
}

//user page session
$session_user_b_no = $_SESSION["ss_user_b_no"];
$session_user_b_url = $_SESSION["ss_user_b_url"];

#업무 요청자, 처리자, 작성자 처리작업
$session_team_sql    = "SELECT team_name FROM team WHERE team_code='{$_SESSION["ss_team"]}'";
$session_team_query  = mysqli_query($my_db, $session_team_sql);
$session_team_result = mysqli_fetch_assoc($session_team_query);
$session_team_name   = isset($session_team_result['team_name']) ? "({$session_team_result['team_name']})" : "";
$session_s_team_name = $_SESSION["ss_name"].$session_team_name;

$smarty->assign(array(
    "session_my_c_no"        => $_SESSION["ss_my_c_no"],
    "session_id"             => $_SESSION["ss_id"],
    "session_s_no"           => $_SESSION["ss_s_no"],
    "session_name"           => $_SESSION["ss_name"],
    "session_team"           => $_SESSION["ss_team"],
    "session_team_parent"    => $_SESSION["ss_team_parent"],
    "session_position"       => $_SESSION["ss_position"],
    "session_pos_permission" => isset($_SESSION["ss_pos_permission"]) ? $_SESSION["ss_pos_permission"] : "1",
    "session_permission"     => $_SESSION["ss_permission"],
    "session_staff_state"    => $_SESSION["ss_staff_state"],
    "session_user_b_no"      => $_SESSION["ss_user_b_no"],
    "session_user_b_url"     => $_SESSION["ss_user_b_url"],
    "session_my_c_type"      => $_SESSION["ss_my_c_type"],
    "session_partner"        => $session_partner,
    "session_s_team_name"    => $session_s_team_name		#업무 요청자, 처리자, 작성자 처리작업
));

// 업무권한 배열 정의
$permission = array(
    array('전체','00000000'),
    array('마케터','10000000'),
    array('서비스운영','01000000'),
    array('재무관리자','00100000'),
    array('외주관리자','00010000'),
    array('대표','00001000'),
    array('마스터관리자','00000100'),
    array('인큐베이팅','00000010'),
    array('물류관리자','00000001')
);
$smarty->assign(array("permission"=>$permission));

$permission_name = array(
    '전체'			=> '00000000',
    '마케터'		    => '10000000',
    '서비스운영'	    => '01000000',
    '재무관리자'	    => '00100000',
    '외주관리자'	    => '00010000',
    '대표'			=> '00001000',
    '마스터관리자'	=> '00000100',
    '인큐베이팅'	    => '00000010',
    '물류관리자'	    => '00000001'
);
$smarty->assign(array("permission_name"=>$permission_name));
?>
