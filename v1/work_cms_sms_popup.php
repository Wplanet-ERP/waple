<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Message.php');
require('inc/model/Calendar.php');
require('inc/model/Staff.php');
require('inc/model/WorkCms.php');

if ($_POST)
{
	$message_model 	= Message::Factory();
	$receiver_list	= isset($_POST['receiver_list']) ? $_POST['receiver_list'] : "";
	$sms_title 		= isset($_POST['sms_title']) ? $_POST['sms_title'] : "";
	$sms_msg_val 	= isset($_POST['sms_msg']) ? $_POST['sms_msg'] : "";
	$send_phone		= isset($_POST['send_phone']) ? $_POST['send_phone'] : "";
	$c_info 		= isset($_POST['c_info']) ? $_POST['c_info'] : "4";
	$c_info_detail	= isset($_POST['c_info_detail']) ? $_POST['c_info_detail'] : "";
	$cs_no			= isset($_POST['cs_no']) ? $_POST['cs_no'] : "";
	$send_data_list = [];

	if($c_info_detail == "STOCK_CALENDAR"){
		$cal_comment_data = array(
			"cal_no" 	=> "8",
			"cs_no" 	=> $cs_no,
			"comment" 	=> addslashes(trim($sms_msg_val)),
			"s_no" 		=> $session_s_no,
			"team" 		=> $session_team,
			"regdate"	=> date("Y-m-d H:i:s")
		);

		$cal_comment_model = Calendar::Factory();
		$cal_comment_model->setMainInit("calendar_schedule_comment", "csc_no");
		$cal_comment_model->insert($cal_comment_data);
	}

	$receiver_list		= str_replace(" ", "", $receiver_list);
	$receiver_list 		= preg_replace("/\r\n|\r|\n/", "||", $receiver_list);
	$receiver_hp_list 	= explode("||", $receiver_list);

	$sms_msg		= addslashes($sms_msg_val);
	$sms_msg_len 	= mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
	$msg_type 		= ($sms_msg_len > 90) ? "L" : "S";
	$send_name 		= $session_name;

	$cur_datetime	= date("YmdHis");
	$cm_id 			= 1;

	foreach ($receiver_hp_list as $receiver_hp)
	{
		if(empty($receiver_hp)){
			continue;
		}

		$chk_hp 	 = str_replace("-","", $receiver_hp);
		$msg_id 	 = "W{$cur_datetime}".sprintf('%04d', $cm_id++);

		$chk_sql	 = "SELECT s_name FROM staff WHERE hp IS NOT NULL AND staff_state='1' AND REPLACE(hp,'-','')='{$chk_hp}' LIMIT 1";
		$chk_query	 = mysqli_query($my_db, $chk_sql);
		$chk_result  = mysqli_fetch_assoc($chk_query);
		$receiver  	 = isset($chk_result['s_name']) ? $chk_result['s_name'] : "";

		$send_data_list[$msg_id] = array(
			"msg_id"        => $msg_id,
			"msg_type"      => $msg_type,
			"sender"        => $send_name,
			"sender_hp"     => $send_phone,
			"receiver"      => $receiver,
			"receiver_hp"   => $receiver_hp,
			"title"         => $sms_title,
			"content"       => $sms_msg,
			"cinfo"         => $c_info,
			"cinfo_detail"  => $c_info_detail,
		);
	}

	if(!empty($send_data_list)){
		$result_data = $message_model->sendMessage($send_data_list);

		if ($result_data['result']) {
			exit("<script>alert('문자 메시지를 발송하였습니다.');if (self) self.close();</script>");
		}
	}

	exit("<script>alert('문자 메세지 발송에 실패했습니다.');history.back();</script>");
}

$type       		= isset($_GET['type']) ? $_GET['type'] : "";
$staff_type 		= isset($_GET['staff_type']) ? $_GET['staff_type'] : "";
$return_date 		= isset($_GET['return_date']) ? $_GET['return_date'] : "";
$quick_ord_no 		= isset($_GET['quick_ord_no']) ? $_GET['quick_ord_no'] : "";
$cal_cs_no 			= isset($_GET['cal_cs_no']) ? $_GET['cal_cs_no'] : "";
$title 	    		= "";
$sms_text  	 		= "";
$c_info 			= "";
$c_info_detail 		= "";
$init_caller 		= "";
$init_receiver_list = [];
$cur_date			= date('Y-m-d');
$staff_model		= Staff::Factory();

if($staff_type == '1')
{
	$init_caller = "010-7730-9905";
	switch($type){
		case 'apply':
			$title 				= "운송장 요청 알림";
			$sms_text 			= "[운송장 요청 알림]\r\n{$cur_date} 택배 리스트 정리 완료. [STEP 01 ~ 03] 처리부탁드립니다.";
			$c_info				= "4";
			$c_info_detail		= "CMS_REQUEST";
			$init_receiver_list = array("010-3348-7264","010-6301-0954","010-9184-1260");
			break;
		case 'cancel':
			$title 				= "취소건 알림";
			$sms_text 			= "[운송장 취소건 알림]\r\n 고객취소건이 있습니다. [STEP 04] 처리부탁드립니다.";
			$c_info				= "4";
			$c_info_detail		= "DELIVERY_CANCEL";
			$init_receiver_list = array("010-3348-7264","010-6301-0954","010-9184-1260");
			break;
		case 'visit':
			$title 				= "기사설치 접수요청 문자(오후 1회 발송 요망)";
			$sms_text 			= "[닥터피엘 기사방문 접수요청 알림] 새로운 기사방문 요청이 있습니다. 접수 부탁드립니다.";
			$c_info				= "4";
			$c_info_detail		= "VISIT_REQUEST";
			$init_receiver_list = array("010-5744-5975");
			break;
		case 'return':
			$title 				= "회수 운송장 요청 알림";
			$sms_text 			= "[회수 운송장 요청 알림]\r\n{$cur_date} 회수 요청 리스트 정리 완료. 커머스 회수 리스트 처리 부탁드립니다." ;
			$c_info				= "4";
			$c_info_detail		= "RETURN_REQUEST";
			$init_receiver_list = array("010-3348-7264","010-6301-0954","010-9184-1260", "010-3712-6638");
			break;
		case 'stock_calendar':
			$cal_model			= Calendar::Factory();
			$cal_model->setMainInit("calendar_schedule", "cs_no");
			$calendar_item		= $cal_model->getItem($cal_cs_no);
			$init_caller		= "070-7873-1373";
			$title 				= "해외 제조사 입고 배차정보 알림";
			$sms_text 			= "[해외 제조사 입고 배차정보 알림]\r\n\r\n● 예정 입고일시 : \r\n● 차량 : \r\n● 기사님 : \r\n● 특이사항 : \r\n\r\n{$calendar_item['cs_content']}";
			$c_info				= "92";
			$c_info_detail		= "STOCK_CALENDAR";
			$init_receiver_list = array("010-4189-7089","010-6500-1898");
			break;
	}
}
elseif($staff_type == '2')
{
	$init_caller   		= "010-6500-1898";
	$init_receiver_list = array("010-7730-9905", "010-4052-6742");

	switch($type){
		case 'apply':
			$title 				= "운송장 적용 알림";
			$sms_text 			= "[운송장 적용 알림]\r\n운송장번호 배정 완료. [STEP 02] 취소건 확인 부탁드립니다.";
			$c_info				= "4";
			$c_info_detail		= "WITH_CMS_APPLY";
			$init_receiver_list = array("010-7730-9905", "010-4052-6742", "010-8649-7496", "010-8024-0704", "010-9265-8009");
			break;
		case 'cancel':
			$title 				= "운송장 변경 알림";
			$sms_text 			= "[운송장 변경 알림]\r\n운송장 변경건이 있습니다. [STEP 03 ~ 04] 확인 부탁드립니다.";
			$c_info				= "4";
			$c_info_detail		= "WITH_DELIVERY_CHANGE";
			$init_receiver_list = array("010-7730-9905", "010-4052-6742", "010-8649-7496", "010-8024-0704", "010-9265-8009");
			break;
		case 'return':
			$title 				= "회수운송장 적용 알림";
			$sms_text 			= "[회수운송장 적용 알림]\r\n회수운송장번호 적용 완료 되었습니다.";
			$c_info				= "4";
			$c_info_detail		= "WITH_RETURN_APPLY";
			break;
		case 'with_date':
			$title 				= "수거 후 제조사/본사 전달 알림";
			$sms_text 			= "[수거 후 제조사/본사 전달 알림]\r\n제조사/본사 발송일:\r\n{$return_date}";
			$c_info				= "4";
			$c_info_detail		= "WITH_RETURN_SEND";
			$init_receiver_list = array("010-7730-9905");
			break;
		case 'quick':
			$quick_model		= WorkCms::Factory();
			$quick_model->setMainInit("work_cms_quick", "w_no");
			$quick_ord_item		= $quick_model->getOrderItem($quick_ord_no);
			$quick_req_sql		= "SELECT s.hp FROM staff s WHERE s.s_no = (SELECT lm.req_s_no FROM logistics_management lm WHERE lm.doc_no = '{$quick_ord_item['doc_no']}' LIMIT 1) LIMIT 1";
			$quick_req_query 	= mysqli_query($my_db, $quick_req_sql);
			$quick_req_staff	= mysqli_fetch_assoc($quick_req_query);
			$title 				= "퀵/화물 출고 알림";
			$sms_text 			= "[퀵/화물 출고 알림]\r\n# 발송지 -> 수령지 = {$quick_ord_item['sender_type_name']} -> {$quick_ord_item['receiver']}\r\n\r\n# 배차정보 =\r\n{$quick_ord_item['quick_info']}";
			$c_info				= "9";
			$c_info_detail		= "WITH_QUICK_SEND";
			$init_receiver_list = array($quick_req_staff['hp'],"010-6301-0954","010-3783-7648");

			if($quick_req_staff['hp'] != "010-9357-4423"){
				$init_receiver_list[] = "010-9357-4423";
			}
			break;
		case 'with_stock_calendar':
			$cal_model			= Calendar::Factory();
			$cal_model->setMainInit("calendar_schedule", "cs_no");
			$calendar_item		= $cal_model->getItem($cal_cs_no);
			$staff_item			= $staff_model->getStaff($calendar_item['cs_s_no']);
			$cal_comment_sql	= "SELECT * FROM calendar_schedule_comment WHERE cs_no='{$cal_cs_no}' AND s_no='179' ORDER BY regdate ASC LIMIT 1";
			$cal_comment_query 	= mysqli_query($my_db, $cal_comment_sql);
			$cal_comment_result	= mysqli_fetch_assoc($cal_comment_query);
			$cal_comment		= isset($cal_comment_result['comment']) ? $cal_comment_result['comment'] : "";
			$title 				= "입고일정확정 알림";
			$sms_text 			= "[입고일정확정 알림]\r\n- 제목 : {$calendar_item['cs_title']}\r\n\r\n- 상세내용 : {$cal_comment}\r\n\r\n- 입고일자 및 시간 : {$calendar_item['cs_s_date']}";
			$c_info				= "91";
			$c_info_detail		= "WITH_STOCK_CALENDAR";
			$init_receiver_list = array($staff_item['hp']);
			break;
	}
}

$smarty->assign("title", $title);
$smarty->assign("sms_text", $sms_text);
$smarty->assign("c_info", $c_info);
$smarty->assign("c_info_detail", $c_info_detail);
$smarty->assign("cs_no", $cal_cs_no);
$smarty->assign("init_caller", $init_caller);
$smarty->assign("init_receiver_list", $init_receiver_list);

$smarty->display('work_cms_sms_popup.html');
?>
