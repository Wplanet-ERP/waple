<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');

# 검색 초기화 및 조건 생성
$add_where      = "na.nav_kind='waple' ANd na.display='1'";
$sch_keyword    = isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";
$smarty->assign('sch_keyword', $sch_keyword);

if(!empty($sch_keyword)) {
    $add_where .= " AND na.nav_name like '%{$sch_keyword}%'";
}

# 전체 게시물 수
$nav_keyworkd_total_sql		= "SELECT COUNT(*) FROM navigation `na` WHERE {$add_where}";
$nav_keyworkd_total_query	= mysqli_query($my_db, $nav_keyworkd_total_sql);
if(!!$nav_keyworkd_total_query)
    $nav_keyworkd_total_result = mysqli_fetch_array($nav_keyworkd_total_query);

$nav_keyworkd_total = $nav_keyworkd_total_result[0];

# 페이징
$page_type  = (isset($_GET['sch_org_page']) && intval($_GET['sch_org_page']) > 0) ? intval($_GET['sch_org_page']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($nav_keyworkd_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "total_search_work_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $nav_keyworkd_total);
$smarty->assign("pagelist", $page);
$smarty->assign("ord_page_type", $page_type);

$nav_keyword_sql  = "
    SELECT
       *
    FROM navigation `na`
    WHERE {$add_where}
    ORDER BY `no` DESC
    LIMIT {$offset}, {$num}
";
$navigation_list 	= [];
$nav_keyword_query 	= mysqli_query($my_db, $nav_keyword_sql);
while ($nav_keyword = mysqli_fetch_array($nav_keyword_query))
{
    $navigation_list[] = $nav_keyword;
}

$smarty->assign('navigation_list', $navigation_list);
$smarty->assign('page_type_list', getPageTypeOption('4'));

$smarty->display('total_search_nav_list.html');

?>
