<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/product_cms_stock.php');
require('inc/helper/logistics.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/CommerceOrder.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');
require('inc/model/Logistics.php');

$process        = (isset($_POST['process'])) ? $_POST['process'] : "";
$unit_model     = ProductCmsUnit::Factory();
$stock_model    = ProductCmsStock::Factory();
$report_model   = ProductCmsStock::Factory();
$report_model->setStockReport();
$company_model  = Company::Factory();
$commerce_model = CommerceOrder::Factory();
$commerce_model->setMainInit("commerce_order", "no");

if ($process == "f_is_order")
{
    $report_data = array(
        'no'        => (isset($_POST['no'])) ? $_POST['no'] : "",
        'is_order'  => (isset($_POST['val'])) ? $_POST['val'] : "",
    );

    if ($report_model->update($report_data)){
        echo "발주서 입고가 저장 되었습니다.";
    }else{
        echo "발주서 입고 저장에 실패 하였습니다.";
    }
    exit;
}
elseif ($process == "f_confirm_state")
{
    $report_data = array(
        'no'                => (isset($_POST['no'])) ? $_POST['no'] : "",
        'confirm_state'     => (isset($_POST['val'])) ? $_POST['val'] : "",
    );

    if ($report_model->update($report_data)){
        echo "확정 입고/반출형태가 저장 되었습니다.";
    }else{
        echo "확정 입고/반출형태 저장에 실패 하였습니다.";
    }
    exit;
}
elseif ($process == "modify_confirm_state")
{
    $no_list 		= (isset($_POST['select_no_list'])) ? $_POST['select_no_list'] : "";
    $confirm_state	= (isset($_POST['select_value'])) ? $_POST['select_value'] : "";
    $search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(!empty($no_list) && !empty($confirm_state))
    {
        $chk_report_sql 	= "SELECT * FROM product_cms_stock_report WHERE `no` IN({$no_list})";
        $chk_report_query	= mysqli_query($my_db, $chk_report_sql);
        $upd_report_data 	= [];
        while($chk_report = mysqli_fetch_array($chk_report_query))
        {
            $upd_report_data[] = array("no" => $chk_report['no'], "confirm_state" => $confirm_state);
        }

        if(!empty($upd_report_data)){
            if($report_model->multiUpdate($upd_report_data)){
                exit ("<script>alert('확정 입고/반출형태 변경했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
            }else{
                exit ("<script>alert('확정 입고/반출형태 변경에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
            }
        }
    }
    exit ("<script>alert('확정 입고/반출형태 변경에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
}
elseif ($process == "del_multi_report")
{
    $no_list 		= (isset($_POST['select_no_list'])) ? $_POST['select_no_list'] : "";
    $search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $result 	    = false;
    $result_cnt     = 0;

    if(!empty($no_list))
    {
        $chk_report_sql 	= "SELECT `no` FROM product_cms_stock_report WHERE `no` IN({$no_list}) AND ord_no=0 AND `type`!='3'";
        $chk_report_query	= mysqli_query($my_db, $chk_report_sql);
        while($chk_report = mysqli_fetch_array($chk_report_query))
        {
            $report_item = $report_model->getItem($chk_report['no']);
            if($report_model->delete($chk_report['no'])) {
                if($report_item['connect_no'] > 0){
                    $report_model->update(array("no" => $report_item['connect_no'], 'connect_no' => 0));
                }

                if(!empty($report_item['sub_connect_no'])){
                    if(strpos($report_item['sub_connect_no'], ",") !== false){
                        $chk_tmp_sub_connect_list = explode(",", $report_item['sub_connect_no']);
                        foreach($chk_tmp_sub_connect_list as $tmp_sub_connect_no){
                            $report_model->update(array("no" => $tmp_sub_connect_no, "sub_connect_no" => ""));
                        }
                    }else{
                        $chk_report_item            = $report_model->getItem($report_item['sub_connect_no']);
                        $chk_sub_connect_tmp_list   = explode(",", $chk_report_item['sub_connect_no']);
                        $upd_sub_connect_no_list    = [];

                        foreach($chk_sub_connect_tmp_list as $tmp_sub_connect_no){
                            if($tmp_sub_connect_no != $report_item['no']){
                                $upd_sub_connect_no_list[] = $tmp_sub_connect_no;
                            }
                        }
                        $upd_sub_connect_no = implode(",", $upd_sub_connect_no_list);

                        $report_model->update(array("no" => $report_item['sub_connect_no'], "sub_connect_no" => $upd_sub_connect_no));
                    }
                }

                $result = true;
                $result_cnt++;
            }
        }
    }

    if($result){
        exit ("<script>alert('총 {$result_cnt}건 삭제했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }else{
        exit ("<script>alert('삭제에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }
}
elseif ($process == "f_log_doc_no")
{
    $r_no       = (isset($_POST['no'])) ? $_POST['no'] : "";
    $val        = (isset($_POST['val'])) ? $_POST['val'] : "";

    $logistics_model    = Logistics::Factory();
    $logistics_item     = $logistics_model->getLogisticsByDoc($val);

    $report_data = array(
        "no"            => $r_no,
        "log_doc_no"    => $val,
        "log_subject"   => $logistics_item['subject'],
    );

    if ($report_model->update($report_data)){
        echo "문서번호가 저장 되었습니다.";
    }else{
        echo "문서번호 저장에 실패 하였습니다.";
    }
    exit;
}
elseif ($process == "del_report")
{
    $report_no 		= (isset($_POST['select_value'])) ? $_POST['select_value'] : "";
    $search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(!empty($report_no))
    {
        $report_item = $report_model->getItem($report_no);
        if($report_model->delete($report_no)) {
            if($report_item['connect_no'] > 0){
                $report_model->update(array("no" => $report_item['connect_no'], 'connect_no' => 0));
            }

            if(!empty($report_item['sub_connect_no'])){
                if(strpos($report_item['sub_connect_no'], ",") !== false){
                    $chk_tmp_sub_connect_list = explode(",", $report_item['sub_connect_no']);
                    foreach($chk_tmp_sub_connect_list as $tmp_sub_connect_no){
                        $report_model->update(array("no" => $tmp_sub_connect_no, "sub_connect_no" => ""));
                    }
                }else{
                    $chk_report_item            = $report_model->getItem($report_item['sub_connect_no']);
                    $chk_sub_connect_tmp_list   = explode(",", $chk_report_item['sub_connect_no']);
                    $upd_sub_connect_no_list    = [];

                    foreach($chk_sub_connect_tmp_list as $tmp_sub_connect_no){
                        if($tmp_sub_connect_no != $report_item['no']){
                            $upd_sub_connect_no_list[] = $tmp_sub_connect_no;
                        }
                    }
                    $upd_sub_connect_no = implode(",", $upd_sub_connect_no_list);

                    $report_model->update(array("no" => $report_item['sub_connect_no'], "sub_connect_no" => $upd_sub_connect_no));
                }
            }

            exit ("<script>alert('삭제했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
        }else{
            exit ("<script>alert('입고/반출 삭제에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
        }
    }

    die;
    exit ("<script>alert('입고/반출 삭제에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
}
elseif ($process == "add_report")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $new_option_no  = isset($_POST['new_option_no']) ? $_POST['new_option_no'] : "";
    $new_log_c_no   = isset($_POST['new_log_c_no']) ? $_POST['new_log_c_no'] : "2809";
    $with_log_c_no  = "2809";

    if(empty($new_option_no)){
        exit ("<script>alert('입고/반출 등록에 실패했습니다. 구성품목 번호 값이 없습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }

    $new_state = isset($_POST['new_state']) ? $_POST['new_state'] : "";
    if(!empty($_POST['new_confirm_state']) && empty($new_state)){
        $match_state_option = getMatchingStateByConfirmStateOption();
        $new_state = $match_state_option[$_POST['new_confirm_state']];
    }

    $unit_item = $unit_model->getItem($new_option_no, $new_log_c_no);
    if(empty($unit_item)){
        $unit_item = $unit_model->getItem($new_option_no, $with_log_c_no);
    }

    $ins_data  = array(
        "report_type"       => isset($_POST['new_report_type']) ? $_POST['new_report_type'] : "3",
        "regdate"           => isset($_POST['new_regdate']) ? $_POST['new_regdate'] : date("Y-m-d"),
        "type"              => isset($_POST['new_type']) ? $_POST['new_type'] : "1",
        "is_order"          => "2",
        "state"             => $new_state,
        "confirm_state"     => isset($_POST['new_confirm_state']) ? $_POST['new_confirm_state'] : "1",
        "brand"             => $unit_item['brand_name'],
        "prd_kind"          => $unit_item['type'] == "1" ? "상품" : "부속품",
        "prd_unit"          => isset($_POST['new_option_no']) ? $_POST['new_option_no'] : "",
        "sup_c_no"          => isset($_POST['new_sup_c_no']) ? $_POST['new_sup_c_no'] : "",
        "log_c_no"          => $new_log_c_no,
        "option_name"       => isset($_POST['new_option_name']) ? $_POST['new_option_name'] : "",
        "sku"               => $unit_item["sku"],
        "prd_name"          => isset($_POST['new_option_name']) ? $_POST['new_option_name'] : "",
        "stock_type"        => isset($_POST['new_warehouse']) ? $_POST['new_warehouse'] : "",
        "stock_qty"         => isset($_POST['new_qty']) ? $_POST['new_qty'] : "0",
        "memo"              => isset($_POST['new_memo']) ? addslashes(trim($_POST['new_memo'])) : "",
        "reg_s_no"          => $session_s_no,
        "report_date"       => date("Y-m-d H:i:s"),
    );

    if($report_model->insert($ins_data)) {
        exit ("<script>alert('추가했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }else{
        exit ("<script>alert('입고/반출 추가에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }
}
elseif ($process == "f_regdate")
{
    $report_data = array(
        'no'        => (isset($_POST['no'])) ? $_POST['no'] : "",
        'regdate'   => (isset($_POST['val'])) ? $_POST['val'] : "",
    );

    if ($report_model->update($report_data)){
        echo "일자가 저장 되었습니다.";
    }else{
        echo "일자 저장에 실패 하였습니다.";
    }
    exit;
}
elseif ($process == "add_etc_in")
{
    $no                 = (isset($_POST['no'])) ? $_POST['no'] : "";
    $in_log_c_no        = (isset($_POST['f_in_warehouse'])) ? $_POST['f_in_warehouse'] : "";
    $search_url         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $with_log_c_no      = '2809';

    $report_item        = $report_model->getItem($no);
    $warehouse_option   = $stock_model->getWarehouseBaseList();
    $in_warehouse_list  = $warehouse_option[$in_log_c_no];
    $in_warehouse       = "";

    foreach($in_warehouse_list as $in_warehouse_tmp){
        $in_warehouse = $in_warehouse_tmp;
        break;
    }

    $unit_item = $unit_model->getItem($report_item['prd_unit'], $in_log_c_no);
    if(empty($unit_item)){
        $unit_item = $unit_model->getItem($report_item['prd_unit'], $with_log_c_no);
    }

    $unit_type      = $unit_item['type'] == "1" ? "상품" : "부속품";
    $stock_qty      = $report_item['stock_qty']*-1;
    $memo           = addslashes($report_item['memo']);
    $regdatetime    = date("Y-m-d H:i:s");

    $ins_data = array(
        "report_type"   => "3",
        "connect_no"    => $no,
        "regdate"       => $report_item['regdate'],
        "type"          => "1",
        "is_order"      => "2",
        "state"         => "기타입고",
        "confirm_state" => "3",
        "brand"         => $unit_item['brand_name'],
        "prd_kind"      => $unit_type,
        "prd_unit"      => $report_item['prd_unit'],
        "sup_c_no"      => $unit_item['sup_c_no'],
        "log_c_no"      => $in_log_c_no,
        "prd_name"      => $report_item['prd_name'],
        "option_name"   => $unit_item['option_name'],
        "sku"           => $unit_item['sku'],
        "stock_type"    => $in_warehouse,
        "stock_qty"     => $stock_qty,
        "log_doc_no"    => $report_item['log_doc_no'],
        "log_subject"   => $report_item['log_subject'],
        "reg_s_no"      => $session_s_no,
        "report_date"   => $regdatetime,
    );

    if($report_model->insert($ins_data)){
        $new_report_no = $report_model->getInsertId();
        $report_model->update(array("no" => $no, "connect_no" => $new_report_no));
        exit ("<script>alert('타물류업체 데이터로 추가했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }else{
        exit ("<script>alert('타물류업체 데이터 추가에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }
}
elseif ($process == "add_etc_out")
{
    $no                 = (isset($_POST['no'])) ? $_POST['no'] : "";
    $out_log_c_no       = (isset($_POST['f_out_warehouse'])) ? $_POST['f_out_warehouse'] : "";
    $search_url         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $with_log_c_no      = '2809';

    $report_item        = $report_model->getItem($no);
    $warehouse_option   = $stock_model->getWarehouseBaseList();
    $out_warehouse_list = $warehouse_option[$out_log_c_no];
    $out_warehouse      = "";

    foreach($out_warehouse_list as $out_warehouse_tmp){
        $out_warehouse = $out_warehouse_tmp;
        break;
    }

    $unit_item = $unit_model->getItem($report_item['prd_unit'], $out_log_c_no);
    if(empty($unit_item)){
        $unit_item = $unit_model->getItem($report_item['prd_unit'], $with_log_c_no);
    }

    $unit_type      = $unit_item['type'] == "1" ? "상품" : "부속품";
    $stock_qty      = $report_item['stock_qty']*-1;
    $memo           = addslashes($report_item['memo']);
    $regdatetime    = date("Y-m-d H:i:s");

    $ins_data = array(
        "report_type"   => "3",
        "connect_no"    => $no,
        "regdate"       => $report_item['regdate'],
        "type"          => "2",
        "is_order"      => "2",
        "state"         => "기타반출",
        "confirm_state" => "6",
        "brand"         => $unit_item['brand_name'],
        "prd_kind"      => $unit_type,
        "prd_unit"      => $report_item['prd_unit'],
        "sup_c_no"      => $unit_item['sup_c_no'],
        "log_c_no"      => $out_log_c_no,
        "prd_name"      => $report_item['prd_name'],
        "option_name"   => $unit_item['option_name'],
        "sku"           => $unit_item['sku'],
        "stock_type"    => $out_warehouse,
        "stock_qty"     => $stock_qty,
        "log_doc_no"    => $report_item['log_doc_no'],
        "log_subject"   => $report_item['log_subject'],
        "reg_s_no"      => $session_s_no,
        "report_date"   => $regdatetime,
    );

    if($report_model->insert($ins_data)){
        $new_report_no = $report_model->getInsertId();
        $report_model->update(array("no" => $no, "connect_no" => $new_report_no));
        exit ("<script>alert('타물류업체 데이터로 추가했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }else{
        exit ("<script>alert('타물류업체 데이터 추가에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }
}
elseif ($process == "add_etc_multi")
{
    $no_list 		    = (isset($_POST['select_no_list'])) ? explode(",", $_POST['select_no_list']) : "";
    $other_log_c_no     = (isset($_POST['select_value'])) ? $_POST['select_value'] : "";
    $search_url 	    = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $with_log_c_no      = "2809";
    $warehouse_option   = $stock_model->getWarehouseBaseList();
    $multi_ins_data     = [];
    $regdate            = date("Y-m-d");
    $regdatetime        = date("Y-m-d H:i:s");

    foreach($no_list as $no)
    {
        $report_item = $report_model->getItem($no);

        if($report_item['connect_no'] == 0)
        {
            $unit_item = $unit_model->getItem($report_item['prd_unit'], $other_log_c_no);
            if(empty($unit_item)){
                $unit_item = $unit_model->getItem($report_item['prd_unit'], $with_log_c_no);
            }

            $unit_type      = $unit_item['type'] == "1" ? "상품" : "부속품";
            $stock_qty      = $report_item['stock_qty']*-1;

            if(($report_item['state'] == "기타반출" || $report_item['state'] == "업체반출") && $report_item["confirm_state"] == "6")
            {
                $other_warehouse_list = $warehouse_option[$other_log_c_no];
                $other_warehouse      = "";
                foreach($other_warehouse_list as $other_warehouse_tmp){
                    $other_warehouse = $other_warehouse_tmp;
                    break;
                }

                $multi_ins_data[$report_item['no']] = array(
                    "report_type"   => "3",
                    "connect_no"    => $no,
                    "regdate"       => $report_item['regdate'],
                    "type"          => "1",
                    "is_order"      => "2",
                    "state"         => "기타입고",
                    "confirm_state" => "3",
                    "brand"         => $unit_item['brand_name'],
                    "prd_kind"      => $unit_type,
                    "prd_unit"      => $report_item['prd_unit'],
                    "sup_c_no"      => $unit_item['sup_c_no'],
                    "log_c_no"      => $other_log_c_no,
                    "prd_name"      => $report_item['prd_name'],
                    "option_name"   => $unit_item['option_name'],
                    "sku"           => $unit_item['sku'],
                    "stock_type"    => $other_warehouse,
                    "stock_qty"     => $stock_qty,
                    "log_doc_no"    => $report_item['log_doc_no'],
                    "log_subject"   => $report_item['log_subject'],
                    "reg_s_no"      => $session_s_no,
                    "report_date"   => $regdatetime,
                );
            }
            elseif(($report_item['state'] == "기타반출" || $report_item['state'] == "업체반출") && $report_item["confirm_state"] == "3")
            {
                $other_warehouse_list = $warehouse_option[$other_log_c_no];
                $other_warehouse      = "";
                foreach($other_warehouse_list as $other_warehouse_tmp){
                    $other_warehouse = $other_warehouse_tmp;
                    break;
                }

                $multi_ins_data[$report_item['no']] = array(
                    "report_type"   => "3",
                    "connect_no"    => $no,
                    "regdate"       => $report_item['regdate'],
                    "type"          => "2",
                    "is_order"      => "2",
                    "state"         => "기타반출",
                    "confirm_state" => "6",
                    "brand"         => $unit_item['brand_name'],
                    "prd_kind"      => $unit_type,
                    "prd_unit"      => $report_item['prd_unit'],
                    "sup_c_no"      => $unit_item['sup_c_no'],
                    "log_c_no"      => $other_log_c_no,
                    "prd_name"      => $report_item['prd_name'],
                    "option_name"   => $unit_item['option_name'],
                    "sku"           => $unit_item['sku'],
                    "stock_type"    => $other_warehouse,
                    "stock_qty"     => $stock_qty,
                    "log_doc_no"    => $report_item['log_doc_no'],
                    "log_subject"   => $report_item['log_subject'],
                    "reg_s_no"      => $session_s_no,
                    "report_date"   => $regdatetime,
                );
            }
        }
    }

    if(!empty($multi_ins_data))
    {
        $report_cnt = 0;
        foreach($multi_ins_data as $report_no => $ins_data){
            if($report_model->insert($ins_data)) {
                $new_report_no = $report_model->getInsertId();
                $report_model->update(array("no" => $report_no, "connect_no" => $new_report_no));
                $report_cnt++;
            }
        }

        if($report_cnt > 0){
            exit ("<script>alert('{$report_cnt}건 입고/반출리스트로 추가했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
        }else{
            exit ("<script>alert('입고/반출리스트 추가에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
        }
    }else{
        exit ("<script>alert('입고/반출리스트 추가할 내역이 없습니다. 입고/반출리스트 추가에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }
}
elseif ($process == "add_sub_etc_report")
{
    $no                 = (isset($_POST['no'])) ? $_POST['no'] : "";
    $search_url         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $with_log_c_no      = "2809";
    $warehouse_option   = $stock_model->getWarehouseBaseList();
    $multi_ins_data     = [];
    $regdatetime        = date("Y-m-d H:i:s");

    $report_item        = $report_model->getItem($no);
    if(empty($report_item['sub_connect_no']) && $report_item['is_order'] == '1')
    {
        $other_log_c_no       = $report_item['sup_c_no'];
        $other_warehouse_list = $warehouse_option[$other_log_c_no];
        $other_warehouse      = "";
        foreach($other_warehouse_list as $other_warehouse_tmp){
            $other_warehouse = $other_warehouse_tmp;
            break;
        }

        $sub_unit_list  = [];
        $chk_sub_sql    = "SELECT * FROM product_cms_unit_subcontract WHERE parent_unit='{$report_item['prd_unit']}'";
        $chk_sub_query  = mysqli_query($my_db, $chk_sub_sql);
        while($chk_sub = mysqli_fetch_assoc($chk_sub_query)){
            $sub_unit_list[] = $chk_sub;
        }

        foreach($sub_unit_list as $sub_unit)
        {
            $sub_unit_item  = $unit_model->getItem($sub_unit['sub_unit'], $other_log_c_no);
            if(empty($sub_unit_item)){
                $sub_unit_item = $unit_model->getItem($sub_unit['sub_unit'], $with_log_c_no);
            }
            $sub_stock_qty  = $report_item['stock_qty']*$sub_unit['qty']*-1;

            $multi_ins_data[$no][] = array(
                "report_type"       => "3",
                "sub_connect_no"    => $no,
                "regdate"           => $report_item['regdate'],
                "type"              => "2",
                "is_order"          => "2",
                "state"             => "기타반출",
                "confirm_state"     => "8",
                "brand"             => $sub_unit_item['brand_name'],
                "prd_kind"          => "부속품",
                "prd_unit"          => $sub_unit_item['no'],
                "sup_c_no"          => $sub_unit_item['sup_c_no'],
                "log_c_no"          => $other_log_c_no,
                "prd_name"          => $sub_unit_item['option_name'],
                "option_name"       => $sub_unit_item['option_name'],
                "sku"               => $sub_unit_item['sku'],
                "stock_type"        => $other_warehouse,
                "stock_qty"         => $sub_stock_qty,
                "log_doc_no"        => $report_item['log_doc_no'],
                "log_subject"       => $report_item['log_subject'],
                "reg_s_no"          => $session_s_no,
                "report_date"       => $regdatetime,
            );
        }
    }

    if(!empty($multi_ins_data))
    {
        $report_cnt = 0;
        foreach($multi_ins_data as $report_no => $ins_data_list)
        {
            $new_sub_connect_list = [];
            foreach($ins_data_list as $ins_data)
            {
                if($report_model->insert($ins_data)) {
                    $new_report_no          = $report_model->getInsertId();
                    $new_sub_connect_list[] = $new_report_no;
                    $report_cnt++;
                }
            }

            if(!empty($new_sub_connect_list)){
                $report_model->update(array("no" => $report_no, "sub_connect_no" => implode(",", $new_sub_connect_list)));
            }
        }

        if($report_cnt > 0){
            exit ("<script>alert('제조사업체 데이터로 추가했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
        }else{
            exit ("<script>alert('제조사업체 데이터 추가에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
        }
    }else{
        exit ("<script>alert('제조사업체 데이터 추가에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }
}
elseif ($process == "add_sub_etc_multi")
{
    $no_list 		    = (isset($_POST['select_no_list'])) ? explode(",", $_POST['select_no_list']) : "";
    $search_url 	    = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $with_log_c_no      = "2809";
    $warehouse_option   = $stock_model->getWarehouseBaseList();
    $multi_ins_data     = [];
    $regdatetime        = date("Y-m-d H:i:s");

    foreach($no_list as $no)
    {
        $report_item = $report_model->getItem($no);

        if(empty($report_item['sub_connect_no']) && $report_item['is_order'] == '1')
        {
            $other_log_c_no       = $report_item['sup_c_no'];
            $other_warehouse_list = $warehouse_option[$other_log_c_no];
            $other_warehouse      = "";
            foreach($other_warehouse_list as $other_warehouse_tmp){
                $other_warehouse = $other_warehouse_tmp;
                break;
            }

            $sub_unit_list  = [];
            $chk_sub_sql    = "SELECT * FROM product_cms_unit_subcontract WHERE parent_unit='{$report_item['prd_unit']}'";
            $chk_sub_query  = mysqli_query($my_db, $chk_sub_sql);
            while($chk_sub = mysqli_fetch_assoc($chk_sub_query)){
                $sub_unit_list[] = $chk_sub;
            }

            if(empty($sub_unit_list)){
                continue;
            }

            foreach($sub_unit_list as $sub_unit)
            {
                $sup_log_c_no   = $sub_unit['sup_c_no'];
                $sub_unit_item  = $unit_model->getItem($sub_unit['sub_unit'], $sup_log_c_no);
                if(empty($sub_unit_item)){
                    $sub_unit_item = $unit_model->getItem($sub_unit['sub_unit'], $with_log_c_no);
                }
                $sub_stock_qty  = $report_item['stock_qty']*$sub_unit['qty']*-1;

                $multi_ins_data[$no][] = array(
                    "report_type"       => "3",
                    "sub_connect_no"    => $no,
                    "regdate"           => $report_item['regdate'],
                    "type"              => "2",
                    "is_order"          => "2",
                    "state"             => "기타반출",
                    "confirm_state"     => "8",
                    "brand"             => $sub_unit_item['brand_name'],
                    "prd_kind"          => "부속품",
                    "prd_unit"          => $sub_unit_item['no'],
                    "sup_c_no"          => $sub_unit_item['sup_c_no'],
                    "log_c_no"          => $other_log_c_no,
                    "prd_name"          => $sub_unit_item['option_name'],
                    "option_name"       => $sub_unit_item['option_name'],
                    "sku"               => $sub_unit_item['sku'],
                    "stock_type"        => $other_warehouse,
                    "stock_qty"         => $sub_stock_qty,
                    "log_doc_no"        => $report_item['log_doc_no'],
                    "log_subject"       => $report_item['log_subject'],
                    "reg_s_no"          => $session_s_no,
                    "report_date"       => $regdatetime,
                );
            }
        }
    }

    if(!empty($multi_ins_data))
    {
        $report_cnt = 0;
        foreach($multi_ins_data as $report_no => $ins_data_list)
        {
            $new_sub_connect_list = [];
            foreach($ins_data_list as $ins_data)
            {
                if($report_model->insert($ins_data)) {
                    $new_report_no          = $report_model->getInsertId();
                    $new_sub_connect_list[] = $new_report_no;
                    $report_cnt++;
                }
            }

            if(!empty($new_sub_connect_list)){
                $report_model->update(array("no" => $report_no, "sub_connect_no" => implode(",", $new_sub_connect_list)));
            }
        }

        if($report_cnt > 0){
            exit ("<script>alert('{$report_cnt}건 입고/반출리스트로 추가했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
        }else{
            exit ("<script>alert('입고/반출리스트 추가에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
        }
    }else{
        exit ("<script>alert('입고/반출리스트 추가할 내역이 없습니다. 입고/반출리스트 추가에 실패했습니다.');location.href='product_cms_stock_report_list.php?{$search_url}';</script>");
    }
}
elseif ($process == "f_memo")
{
    $report_data = array(
        'no'        => (isset($_POST['no'])) ? $_POST['no'] : "",
        'memo'      => (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "",
    );

    if ($report_model->update($report_data)){
        echo "메모가 저장 되었습니다.";
    }else{
        echo "메모 저장에 실패 하였습니다.";
    }
    exit;
}
elseif ($process == "f_stock_qty")
{
    $report_no      = (isset($_POST['no'])) ? $_POST['no'] : "";
    $stock_qty      = (isset($_POST['val'])) ? str_replace(",", "", $_POST['val']) : 0;
    $report_item    = $report_model->getItem($report_no);

    $report_data = array(
        'no'            => $report_no,
        'stock_qty'     => $stock_qty,
    );

    if ($report_model->update($report_data))
    {
        if($report_item['ord_no'] > 0)
        {
            $commerce_item  = $commerce_model->getOrderItem($report_item['ord_no']);
            $sup_loc_type   = $commerce_item['sup_loc_type'];
            $unit_price     = $commerce_item['unit_price'];
            $total_price    = $unit_price*$stock_qty;
            $supply_price   = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1, 0);
            $vat_price      = ($sup_loc_type == '2') ? 0 : ($total_price-$supply_price);

            $comm_upd_data       = array(
                "no"            => $commerce_item['no'],
                "option_no"     => $commerce_item['option_no'],
                "quantity"      => $stock_qty,
                "supply_price"  => $supply_price,
                "vat"           => $vat_price,
                "total_price"   => $supply_price
            );

            $commerce_model->update($comm_upd_data);
        }

        echo "수량이 저장 되었습니다.";
    }else{
        echo "수량 저장에 실패 하였습니다.";
    }
    exit;
}
elseif ($process == "f_unit_price")
{
    $report_no      = (isset($_POST['no'])) ? $_POST['no'] : "";
    $unit_price     = (isset($_POST['val'])) ? str_replace(",", "", $_POST['val']) : 0;

    $report_item    = $report_model->getItem($report_no);
    $commerce_item  = $commerce_model->getOrderItem($report_item['ord_no']);
    $sup_loc_type   = $commerce_item['sup_loc_type'];
    $stock_qty      = $report_item['stock_qty'];
    $total_price    = $unit_price*$stock_qty;
    $supply_price   = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1, 0);
    $vat_price      = ($sup_loc_type == '2') ? 0 : ($total_price-$supply_price);

    $upd_data       = array(
        "no"            => $commerce_item['no'],
        "option_no"     => $commerce_item['option_no'],
        "unit_price"    => $unit_price,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price,
        "total_price"   => $supply_price
    );

    if ($commerce_model->update($upd_data)){
        echo "입고단가가 저장 되었습니다.";
    }else{
        echo "입고단가 저장에 실패 하였습니다.";
    }
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "44";
$nav_title   = "입고/반출 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

$is_editable = false;
if(permissionNameCheck($session_permission,"마스터관리자") || permissionNameCheck($session_permission, "물류관리자") || ($session_s_no == '17' || $session_s_no == '102')){
    $is_editable = true;
}
$smarty->assign("is_editable", $is_editable);

# 검색조건
$add_where = "1=1";
$add_order = "pcsr.no DESC";

$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));
$years_val  = date('Y-m-d', strtotime('-2 years'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);
$smarty->assign("years_val", $years_val);

$sch_report_type    = isset($_GET['sch_report_type']) ? $_GET['sch_report_type'] : "";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_s_regdate      = isset($_GET['sch_s_regdate']) ? $_GET['sch_s_regdate'] : $months_val;
$sch_e_regdate      = isset($_GET['sch_e_regdate']) ? $_GET['sch_e_regdate'] : $today_val;
$sch_type           = isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_except_term    = isset($_GET['sch_except_term']) ? $_GET['sch_except_term'] : "";
$sch_confirm_state  = isset($_GET['sch_confirm_state']) ? $_GET['sch_confirm_state'] : "";
$sch_is_connect     = isset($_GET['sch_is_connect']) ? $_GET['sch_is_connect'] : "";
$sch_is_sub_connect = isset($_GET['sch_is_sub_connect']) ? $_GET['sch_is_sub_connect'] : "";
$sch_log_company    = isset($_GET['sch_log_company']) ? $_GET['sch_log_company'] : "";
$sch_option_name    = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_prd_sku        = isset($_GET['sch_prd_sku']) ? $_GET['sch_prd_sku'] : "";
$sch_sup_c_no       = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_stock_type     = isset($_GET['sch_stock_type']) ? $_GET['sch_stock_type'] : "";
$sch_memo           = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
$sch_is_order       = isset($_GET['sch_is_order']) ? $_GET['sch_is_order'] : "";
$sch_expiration     = isset($_GET['sch_expiration']) ? $_GET['sch_expiration'] : "";
$sch_reg_date_type  = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "months";
$add_stock_type     = isset($_GET['add_stock_type']) ? $_GET['add_stock_type'] : "";
$sch_log_doc_no     = isset($_GET['sch_log_doc_no']) ? $_GET['sch_log_doc_no'] : "";
$sch_is_log_doc     = isset($_GET['sch_is_log_doc']) ? $_GET['sch_is_log_doc'] : "";
$sch_log_subject    = isset($_GET['sch_log_subject']) ? $_GET['sch_log_subject'] : "";
$sch_log_run_c_no   = isset($_GET['sch_log_run_c_no']) ? $_GET['sch_log_run_c_no'] : "";
$sch_file_no        = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";

$new_stock_data     = [];
if(!empty($add_stock_type))
{
    $add_unit_item = $unit_model->getSkuItem($sch_prd_sku, $sch_log_company);

    if(empty($add_unit_item)){
        $add_unit_item = $unit_model->getSkuItem($sch_prd_sku, "2809");
    }

    $state = "";
    $type = "";
    switch($add_stock_type){
        case "1":
            $state = "발주입고";
            $type = "1";
            break;
        case "3":
            $state = "이동입고";
            $type = "1";
            break;
        case "5":
            $state = "반품입고";
            $type = "1";
            break;
        case "9":
            $state = "판매반출";
            $type = "2";
            break;
    }

    $new_stock_data = array(
        "regdate"       => $sch_e_regdate,
        "type"          => $type,
        "state"         => $state,
        "confirm_state" => $add_stock_type,
        "log_c_no"      => $sch_log_company,
        "warehouse"     => $sch_stock_type,
        "prd_sku"       => $sch_prd_sku,
        "option_no"     => $add_unit_item['prd_unit'],
        "option_name"   => $add_unit_item['option_name'],
        "sup_c_no"      => $add_unit_item['sup_c_no'],
        "sup_c_name"    => $add_unit_item['sup_c_name'],
    );
}
$smarty->assign("new_stock_data", $new_stock_data);

if (!empty($sch_report_type)) {
    $add_where .= " AND pcsr.report_type='{$sch_report_type}'";
    $smarty->assign("sch_report_type", $sch_report_type);
}

if (!empty($sch_no)) {
    $add_where .= " AND pcsr.no='{$sch_no}'";
    $smarty->assign("sch_no", $sch_no);
}

if (!empty($sch_s_regdate))
{
    $add_where .= " AND pcsr.regdate >= '{$sch_s_regdate}'";
    $smarty->assign("sch_s_regdate", $sch_s_regdate);
}

if (!empty($sch_e_regdate)) {
    $add_where .= " AND pcsr.regdate <= '{$sch_e_regdate}'";
    $smarty->assign("sch_e_regdate", $sch_e_regdate);
}
$smarty->assign('sch_reg_date_type', $sch_reg_date_type);

if (!empty($sch_type)) {
    $add_where .= " AND pcsr.type='{$sch_type}'";
    $smarty->assign("sch_type", $sch_type);
}

if (!empty($sch_state)) {
    $add_where .= " AND pcsr.state='{$sch_state}'";
    $smarty->assign("sch_state", $sch_state);
}

if (!empty($sch_except_term)) {
    $add_where .= " AND pcsr.state NOT LIKE '%기간%'";
    $smarty->assign("sch_except_term", $sch_except_term);
}

if (!empty($sch_confirm_state)) {
    $add_where .= " AND pcsr.confirm_state='{$sch_confirm_state}'";
    $smarty->assign("sch_confirm_state", $sch_confirm_state);
}

if (!empty($sch_is_connect)) {
    if($sch_is_connect == '1'){
        $add_where .= " AND pcsr.connect_no > 0";
    }elseif($sch_is_connect == '2'){
        $add_where .= " AND pcsr.connect_no = 0";
    }
    $smarty->assign("sch_is_connect", $sch_is_connect);
}

if (!empty($sch_is_sub_connect)) {
    if($sch_is_sub_connect == '1'){
        $add_where .= " AND (pcsr.sub_connect_no != '' AND pcsr.sub_connect_no IS NOT NULL)";
    }elseif($sch_is_sub_connect == '2'){
        $add_where .= " AND (pcsr.sub_connect_no IS NULL OR pcsr.sub_connect_no = '')";
    }
    $smarty->assign("sch_is_sub_connect", $sch_is_sub_connect);
}

if (!empty($sch_option_name)) {
    $add_where .= " AND pcsr.option_name like '%{$sch_option_name}%'";
    $smarty->assign("sch_option_name", $sch_option_name);
}

if (!empty($sch_log_company)) {
    $add_where .= " AND pcsr.log_c_no='{$sch_log_company}'";
    $smarty->assign("sch_log_company", $sch_log_company);
}

if (!empty($sch_prd_sku)) {
    $add_where .= " AND pcsr.sku = '{$sch_prd_sku}'";
    $smarty->assign("sch_prd_sku", $sch_prd_sku);
}

if (!empty($sch_sup_c_no)) {
    $add_where .= " AND pcsr.sup_c_no = '{$sch_sup_c_no}'";
    $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
}

if (!empty($sch_stock_type)) {
    $add_where .= " AND pcsr.stock_type like '%{$sch_stock_type}%'";
    $smarty->assign("sch_stock_type", $sch_stock_type);
}

if (!empty($sch_memo)) {
    $add_where .= " AND pcsr.memo like '%{$sch_memo}%'";
    $smarty->assign("sch_memo", $sch_memo);
}

if (!empty($sch_is_order)) {
    $add_where .= " AND pcsr.is_order ='{$sch_is_order}'";
    $smarty->assign("sch_is_order", $sch_is_order);
}

if (!empty($sch_expiration)) {
    $add_where .= " AND pcsr.expiration = '{$sch_expiration}'";
    $smarty->assign("sch_expiration", $sch_expiration);
}

if(!empty($sch_is_log_doc)){
    $add_where .= " AND (pcsr.log_doc_no IS NOT NULL AND pcsr.log_doc_no != '')";
    $smarty->assign("sch_is_log_doc", $sch_is_log_doc);
}else{
    if (!empty($sch_log_doc_no)) {
        $add_where .= " AND pcsr.log_doc_no = '{$sch_log_doc_no}'";
        $smarty->assign("sch_log_doc_no", $sch_log_doc_no);
    }
}

if (!empty($sch_log_subject)) {
    $add_where .= " AND pcsr.log_subject = '{$sch_log_subject}'";
    $smarty->assign("sch_log_subject", $sch_log_subject);
}

if (!empty($sch_log_run_c_no)) {
    $add_where .= " AND lm.run_c_no = '{$sch_log_run_c_no}'";
    $smarty->assign("sch_log_run_c_no", $sch_log_run_c_no);
}

if (!empty($sch_file_no)) {
    $add_where .= " AND pcsr.file_no = '{$sch_file_no}'";
    $smarty->assign("sch_file_no", $sch_file_no);
}

// 페이지에 따른 limit 설정
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
$smarty->assign("page",$pages);

// 전체 게시물 수
$stock_report_total_sql   = "SELECT count(*) as total, SUM(stock_qty) as sum_qty, SUM(stock_qty*unit_price) as total_sum_price FROM (SELECT pcsr.no, pcsr.stock_qty, IFNULL(co.unit_price,0) as unit_price FROM product_cms_stock_report pcsr LEFT JOIN commerce_order co ON co.`no`=pcsr.ord_no LEFT JOIN logistics_management lm ON lm.doc_no=pcsr.log_doc_no WHERE {$add_where}) AS cnt";
$stock_report_total_query = mysqli_query($my_db, $stock_report_total_sql);
if(!!$stock_report_total_query)
    $stock_report_total_result = mysqli_fetch_array($stock_report_total_query);

$stock_report_total     = $stock_report_total_result['total'];
$stock_sum_total        = $stock_report_total_result['sum_qty'];
$stock_sum_total_price  = $stock_report_total_result['total_sum_price'];

//페이징
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 20;
$num        = $page_type;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($stock_report_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "product_cms_stock_report_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $stock_report_total);
$smarty->assign("stock_sum_total", $stock_sum_total);
$smarty->assign("stock_sum_total_price", $stock_sum_total_price);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

$report_sql = "
    SELECT
        pcsr.*,
        DATE_FORMAT(pcsr.report_date, '%Y/%m/%d') as report_day,
        DATE_FORMAT(pcsr.report_date, '%H:%i') as report_time,
        (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.sup_c_no) as sup_c_name,
        (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.log_c_no) as log_c_name,
        (SELECT s.s_name FROm staff s WHERE s.s_no=pcsr.reg_s_no) as reg_s_name,
        lm.lm_no as log_no,
        lm.reason as log_reason,
        lm.run_c_no as log_run_c_no,
        (SELECT s.s_name FROm staff s WHERE s.s_no=lm.req_s_no) as log_staff,
        (SELECT t.team_name FROm team t WHERE t.team_code=lm.req_team) as log_team,
        (SELECT um.file_path FROM upload_management um WHERE um.file_no=pcsr.file_no) as file_path,
        (SELECT um.file_name FROM upload_management um WHERE um.file_no=pcsr.file_no) as file_name,
        (SELECT COUNT(pcus.no) FROM product_cms_unit_subcontract as pcus WHERE pcus.parent_unit=pcsr.prd_unit) as sub_count
    FROM product_cms_stock_report as pcsr
    LEFT JOIN logistics_management lm ON lm.doc_no=pcsr.log_doc_no
    WHERE {$add_where}
    ORDER BY {$add_order}
    LIMIT {$offset},{$num}
";
$report_query           = mysqli_query($my_db, $report_sql);
$report_list            = [];
$state_option           = $stock_model->getStockStateOption();
$sup_c_list             = $unit_model->getDistinctUnitCompanyData("sup_c_no");
$log_company_list       = $company_model->getLogisticsList();
$log_run_company_list   = getAddrCompanyOption();
$report_type_option     = getReportTypeOption();
$type_option            = getTypeOption();
$log_subject_option     = getSubjectNameOption();
while($report = mysqli_fetch_assoc($report_query))
{
    $report['report_type_name'] = isset($report_type_option[$report['report_type']]) ? $report_type_option[$report['report_type']] : "";
    $report['stock_type_name']  = isset($type_option[$report['type']]) ? $type_option[$report['type']] : "";
    $report['log_subject_name'] = isset($log_subject_option[$report['log_subject']]) ? $log_subject_option[$report['log_subject']] : "";
    $report['log_run_c_name']   = isset($log_run_company_list[$report['log_run_c_no']]) ? $log_run_company_list[$report['log_run_c_no']] : "";
    $report['is_editable']      = false;

    if($report['reg_s_no'] == $session_s_no || $session_s_no == '3' || permissionNameCheck($session_permission,"마스터관리자")){
        $report['is_editable'] = true;
    }

    $report['order_title']  = "";
    $report['order_price']  = "";
    $report['order_limit']  = "";
    $report['order_qty']    = "";
    $report['order_total']  = "";
    $report['sup_loc_type'] = "";

    $report['unit_sum_price'] = 0;
    if($report['ord_no'] > 0){
        $commerce_order_sql     = "SELECT co.no, co.set_no, cos.sup_loc_type, cos.sup_c_name, cos.order_count, cos.title, co.quantity, co.unit_price, co.limit_date FROM commerce_order co LEFT JOIN commerce_order_set cos ON cos.no=co.set_no WHERE co.`no`='{$report['ord_no']}'";
        $commerce_order_query   = mysqli_query($my_db, $commerce_order_sql);
        $commerce_order_result  = mysqli_fetch_assoc($commerce_order_query);
        if($commerce_order_result['no'])
        {
            $report['set_no']           = $commerce_order_result['set_no'];
            $report['ord_name']         = $commerce_order_result['sup_c_name']." [{$commerce_order_result['order_count']}차]";
            $report['main_title']       = $commerce_order_result['title'];
            $report['sup_loc_type']     = $commerce_order_result['sup_loc_type'];
            $report['unit_price']       = ($commerce_order_result['sup_loc_type'] == '1') ? $commerce_order_result['unit_price'] : "-";
            $report['quantity']         = $commerce_order_result['quantity'];
            $report['limit_date']       = $commerce_order_result['limit_date'];
            $report['unit_sum_price']   = ($commerce_order_result['sup_loc_type'] == '1') ? $report['stock_qty']*$report['unit_price'] : 0;

            if($commerce_order_result['limit_date'])
            {
                $s_date_val = strtotime(date('Y-m-d'));
                $e_date_val = strtotime($commerce_order_result['limit_date']);
                $sub_day    = (int)(($e_date_val-$s_date_val)/86400);
                $report['sub_day'] = $sub_day;
            }
        }
    }

    if(!empty($report['sub_connect_no'])){
        $report['sub_connect_list'] = explode(",", $report['sub_connect_no']);
    }

    $report_list[] = $report;
}

$smarty->assign("page_type_option", getPageTypeOption('4'));
$smarty->assign("report_type_option", $report_type_option);
$smarty->assign('sup_c_list', $sup_c_list);
$smarty->assign('state_option', $state_option);
$smarty->assign('is_exist_option', getIsExistOption());
$smarty->assign('confirm_state_option', getConfirmStateOption());
$smarty->assign('type_option', $type_option);
$smarty->assign('stock_expiration_option', getStockExpirationOption());
$smarty->assign('is_order_option', getStockIsOrderOption());
$smarty->assign('log_company_list', $log_company_list);
$smarty->assign('log_subject_option', getReportSubjectNameOption());
$smarty->assign('log_run_company_list', $log_run_company_list);
$smarty->assign('stock_report_list', $report_list);

$smarty->display('product_cms_stock_report_list.html');
?>
