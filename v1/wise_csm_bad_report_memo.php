<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/model/Staff.php');

# 프로세스 처리
$process     = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'add_csm_memo')
{
    $s_no        = isset($_POST['s_no']) ? $_POST['s_no'] : "";
    $k_name_code = isset($_POST['k_name_code']) ? $_POST['k_name_code'] : "";
    $bad_date    = isset($_POST['bad_date']) ? $_POST['bad_date'] : "";
    $memo        = isset($_POST['memo']) ? addslashes($_POST['memo']) : "";
    $type        = isset($_POST['type']) ? $_POST['type'] : "read";

    if($s_no != $session_s_no){
        exit("<script>alert('메모 등록 권한이 없습니다.');history.back();</script>");
    }

    # 파일첨부
    $file_names = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_paths = isset($_POST['file_path']) ? $_POST['file_path'] : "";

    $move_file_name = "";
    $move_file_path = "";
    if(!empty($file_names) && !empty($file_paths))
    {
        $move_file_paths = move_store_files($file_paths, "dropzone_tmp", "csm");
        $move_file_name  = implode(',', $file_names);
        $move_file_path  = implode(',', $move_file_paths);
    }

    $add_file_column = "";
    if(!empty($move_file_path) && !empty($move_file_name)){
        $add_file_column = ",file_path='{$move_file_path}', file_name='{$move_file_name}'";
    }

    $ins_sql = "
      INSERT INTO `csm_bad_memo` SET
        `s_no`='{$s_no}',
        `k_name_code`='{$k_name_code}',
        `bad_date`='{$bad_date}',
        `memo`='{$memo}'
        {$add_file_column}
    ";

    if(mysqli_query($my_db, $ins_sql)) {
        $new_cbm_no   = mysqli_insert_id($my_db);
        exit("<script>alert('메모 등록에 성공했습니다');location.href='wise_csm_bad_report_memo.php?cbm_no={$new_cbm_no}&s_no={$s_no}&k_name_code={$k_name_code}&bad_date={$bad_date}&type={$type}';</script>");
    }else{
        exit("<script>alert('메모 등록에 실패했습니다');history.back();</script>");
    }

}
elseif($process == 'upd_csm_memo') # 수정
{
    $cbm_no      = isset($_POST['cbm_no']) ? $_POST['cbm_no'] : "";
    $s_no        = isset($_POST['s_no']) ? $_POST['s_no'] : "";
    $k_name_code = isset($_POST['k_name_code']) ? $_POST['k_name_code'] : "";
    $bad_date    = isset($_POST['bad_date']) ? $_POST['bad_date'] : "";
    $memo        = isset($_POST['memo']) ? addslashes($_POST['memo']) : "";
    $type        = isset($_POST['type']) ? $_POST['type'] : "read";

    if($s_no != $session_s_no){
        exit("<script>alert('메모 등록 권한이 없습니다.');history.back();</script>");
    }

    # 파일첨부
    $file_origin_path   = isset($_POST['file_origin_path']) ? $_POST['file_origin_path'] : "";
    $file_origin_name   = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_names         = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_paths         = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_chk           = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    $move_file_path  = "";
    $move_file_name  = "";
    $add_file_column = "";
    if($file_chk)
    {
        if(!empty($file_paths) && !empty($file_names))
        {
            $move_file_paths = move_store_files($file_paths, "dropzone_tmp", "csm");
            $move_file_name  = implode(',', $file_names);
            $move_file_path  = implode(',', $move_file_paths);
        }

        if(!empty($file_origin_path) && !empty($file_origin_name))
        {
            if(!empty($file_paths) && !empty($file_names)){
                $del_files = array_diff(explode(',', $file_origin_path), $file_paths);
            }else{
                $del_files = explode(',', $file_origin_path);
            }

            if(!empty($del_files))
            {
                del_files($del_files);
            }
        }

        if(!empty($move_file_path) && !empty($move_file_name)){
            $add_file_column = ",file_path='{$move_file_path}', file_name='{$move_file_name}'";
        }else{
            $add_file_column = ",file_path='', file_name='',";
        }
    }

    $upd_sql = "
        UPDATE csm_bad_memo SET 
            `memo`='{$memo}'
            {$add_file_column}
        WHERE cbm_no='{$cbm_no}'
    ";

    if(mysqli_query($my_db, $upd_sql)) {
        exit("<script>alert('메모 수정에 성공했습니다');location.href='wise_csm_bad_report_memo.php?cbm_no={$cbm_no}&s_no={$s_no}&k_name_code={$k_name_code}&bad_date={$bad_date}&type={$type}';</script>");
    }else{
        exit("<script>alert('메모 수정에 실패했습니다');history.back();</script>");
    }

}
else
{
    $cbm_no         = isset($_GET['cbm_no']) ? $_GET['cbm_no'] : "";
    $s_no           = isset($_GET['s_no']) ? $_GET['s_no'] : "";
    $k_name_code    = isset($_GET['k_name_code']) ? $_GET['k_name_code'] : "";
    $bad_date       = isset($_GET['bad_date']) ? $_GET['bad_date'] : "";
    $type           = isset($_GET['type']) ? $_GET['type'] : "read";
    $staff_model    = Staff::Factory();
    $staff_name     = $staff_model->getStaffName($s_no);

    $csm_memo   = array(
        "cbm_no"        => $cbm_no,
        "s_no"          => $s_no,
        "s_name"        => $staff_name,
        "k_name_code"   => $k_name_code,
        "bad_date"      => $bad_date,
        "memo"          => "",
        "type"          => $type,
        "file_paths"    => "",
        "file_names"    => ""
    );

    $csm_memo_sql    = "SELECT * FROM csm_bad_memo WHERE s_no='{$s_no}' AND k_name_code='{$k_name_code}' AND bad_date='{$bad_date}'";
    $csm_memo_query  = mysqli_query($my_db, $csm_memo_sql);
    $csm_memo_result = mysqli_fetch_assoc($csm_memo_query);

    if(isset($csm_memo_result['cbm_no']) && !empty($csm_memo_result['cbm_no']))
    {
        $csm_memo['cbm_no'] = $csm_memo_result['cbm_no'];
        $csm_memo['memo']   = $csm_memo_result['memo'];

        $file_paths = $csm_memo_result['file_path'];
        $file_names = $csm_memo_result['file_name'];
        if(!empty($file_paths) && !empty($file_names))
        {
            $csm_memo['file_paths'] = explode(',', $file_paths);
            $csm_memo['file_names'] = explode(',', $file_names);
            $csm_memo['file_origin_path'] = $file_paths;
            $csm_memo['file_origin_name'] = $file_names;
        }
    }

    $file_count = !empty($csm_memo['file_paths']) ? count($csm_memo['file_paths']) : 0;
    $smarty->assign("file_count", $file_count);

    $smarty->assign($csm_memo);
}

$smarty->display('wise_csm_bad_report_memo.html');
?>