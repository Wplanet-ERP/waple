<?php


/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.7, 2012-05-19
 */

/** Error reporting */
ini_set('error_reporting',E_ALL & ~E_NOTICE | E_STRICT);
date_Default_TimeZone_set("Asia/Seoul");	// 시간설정
define('ROOTPATH', dirname(__FILE__));


/** Include PHPExcel */
require_once './Classes/PHPExcel.php';
require_once 'inc/common.php';
require_once 'ckadmin.php';
require_once 'inc/data_state.php';
require_once 'inc/helper/promotion.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
),
),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
),
);


// 단순히 상태값을 텍스트 표현하기 위해 배열에 담은 정보
$state_name=array(1=>'접수대기', 2=>'접수완료',3=>'모집중',4=>'진행중',5=>'마감');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";

$sch_my_c_no_get=isset($_GET['sch_my_c_no'])?$_GET['sch_my_c_no']:"";
$sch_kind_get=isset($_GET['sch_kind'])?$_GET['sch_kind']:"";
$sch_state_get=isset($_GET['sch_state'])?$_GET['sch_state']:"";
$sch_code_get=isset($_GET['sch_code'])?$_GET['sch_code']:"";
$sch_company_get=isset($_GET['sch_company'])?$_GET['sch_company']:"";
$sch_reward_date_get=isset($_GET['sch_reward_date'])?$_GET['sch_reward_date']:"";
$sch_wd_due_date_get=isset($_GET['sch_wd_due_date'])?$_GET['sch_wd_due_date']:"";
$sch_tax_set_get=isset($_GET['sch_tax_set'])?$_GET['sch_tax_set']:"";
$sch_wd_due_date_null_get=isset($_GET['sch_wd_due_date_null'])?$_GET['sch_wd_due_date_null']:"";
$sch_refund_get=isset($_GET['sch_refund'])?$_GET['sch_refund']:"n";
$sch_nick_get=isset($_GET['sch_nick'])?$_GET['sch_nick']:"";
$sch_username_get=isset($_GET['sch_username'])?$_GET['sch_username']:"";
$sch_blog_url_get=isset($_GET['sch_blog_url'])?$_GET['sch_blog_url']:"";

if(!empty($sch_my_c_no_get)) {
    $add_where.=" AND p.c_no IN(SELECT c.c_no FROM company c WHERE c.my_c_no='{$sch_my_c_no_get}')";
}

if(!empty($sch_kind_get)) {
    $add_where.=" AND p.kind = '".$sch_kind_get."'";
}

if(!empty($sch_state_get)) {
	$add_where.=" AND p.p_state = '".$sch_state_get."'";
}

if(!empty($sch_code_get)) {
	$add_where.=" AND p.promotion_code like '%".$sch_code_get."%'";
}

if(!empty($sch_company_get)) {
	$add_where.=" AND p.company like '%".$sch_company_get."%'";
}

if(!empty($sch_reward_date_get)) {
	$add_where.=" AND p.pres_reward = '".$sch_reward_date_get."'";
}

if(!empty($sch_wd_due_date_get)) {
	$add_where.=" AND a.wd_due_date = '".$sch_wd_due_date_get."'";
}

if($sch_wd_due_date_null_get == 'null') { // wd_due_date is null
	$add_where.=" AND (a.wd_due_date is null OR a.wd_due_date = '')";
}

if(!empty($sch_tax_set_get)) {
	if($sch_tax_set_get == "y") { // 소득세 저장완료
		$add_where.=" AND ((a.reward_cost IS NOT NULL AND a.reward_cost<>'0') AND a.biz_tax IS NOT NULL AND a.local_tax IS NOT NULL AND (a.reward_cost_result IS NOT NULL AND a.reward_cost_result<>'0'))";
	}elseif($sch_tax_set_get == "n"){ // 소득세 저장 미완료
		$add_where.=" AND ((a.reward_cost IS NULL OR a.reward_cost='0') OR a.biz_tax IS NULL OR a.local_tax IS NULL OR (a.reward_cost_result IS NULL OR a.reward_cost_result='0'))";
	}
}

if($sch_refund_get == "y") {
	$add_where.=" AND a.refund_count >= 1";
}else if($sch_refund_get == "n") {
	$add_where.=" AND (a.refund_count is NULL or a.refund_count = 0)";
}
if(!empty($sch_nick_get)) {
	$add_where.=" AND a.nick like '%".$sch_nick_get."%'";
}
if(!empty($sch_username_get)) {
	$add_where.=" AND a.username like '%".$sch_username_get."%'";
}
if(!empty($sch_blog_url_get)) {
	$add_where.=" AND a.blog_url like '%".$sch_blog_url_get."%'";
}


//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "계열사")
->setCellValue('B1', "마케팅채널")
->setCellValue('C1', "캠페인종류")
->setCellValue('D1', "p_no")
->setCellValue('E1', "업체명")
->setCellValue('F1', "담당자")
->setCellValue('G1', "건수")
->setCellValue('H1', "보상지급일")
->setCellValue('I1', "보상지급액(소득세별도)")
->setCellValue('J1', "사업소득세")
->setCellValue('K1', "지방소득세")
->setCellValue('L1', "실지급액");

// 리스트 내용
$i=2;
$ttamount=0;

// 정렬순서 토글 & 필드 지정
$add_orderby="";
$order=isset($_GET['od'])?$_GET['od']:"";
$order_type=isset($_GET['by'])?$_GET['by']:"";
$toggle=$order_type?"asc":"desc";
$order_field=array('','bk_jumin');
if($order && $order<6) {
	$add_orderby.="$order_field[$order] $toggle,";
	$smarty->assign("order",$order);
	$smarty->assign("order_type",$order_type);
}

$add_orderby.=" a.p_no DESC, a.a_no ASC";

$sql="
	SELECT p.channel AS channel,
		p.kind AS kind,
		p.p_no AS p_no,
		p.c_no AS c_no,
		(SELECT c.my_c_no FROM company c WHERE c.c_no=p.c_no) AS my_c_no,
		p.company AS company,
		p.name AS staff_name,
		a.wd_due_date,
		COUNT(a.a_no) AS reward_cnt,
		SUM(a.reward_cost) AS sum_reward_cost,
		SUM(a.biz_tax) AS sum_biz_tax,
		SUM(a.local_tax) AS sum_local_tax,
		SUM(a.reward_cost_result) AS sum_reward_cost_result
	FROM promotion p, application a
	WHERE
		p.p_no=a.p_no AND (p.kind='2' OR p.kind='4' OR p.kind='5') AND a.a_state='1' ".$add_where."
	GROUP BY
		p.p_no
	ORDER BY
		$add_orderby
	";

//echo "<br><br><br>".$sql;
$idx_no=1;

$promotion_channel_option = getPromotionChannelOption();
$query=mysqli_query($my_db,$sql);
while($data=mysqli_fetch_array($query)) {

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$i, $my_company_name_list[$data['my_c_no']]['c_name'])
	->setCellValue('B'.$i, $promotion_channel_option[$data['channel']])
	->setCellValue('C'.$i, $promotion_kind[$data['kind']][0])
	->setCellValue('D'.$i, $data['p_no'])
	->setCellValue('E'.$i, $data['company'])
	->setCellValue('F'.$i, $data['staff_name'])
	->setCellValue('G'.$i, $data['reward_cnt'])
    ->setCellValue('H'.$i, $data['wd_due_date'])
	->setCellValue('I'.$i, $data['sum_reward_cost'])
	->setCellValue('J'.$i, $data['sum_biz_tax'])
	->setCellValue('K'.$i, $data['sum_local_tax'])
	->setCellValue('L'.$i, $data['sum_reward_cost_result']);

	$i = $i+1;
		$idx_no++;
}


if($i > 1)
	$i = $i-1;

$objPHPExcel->getActiveSheet()->getStyle('A1:L'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A2:L'.$i)->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
															->getStartColor()->setARGB('0031A5A7');
$objPHPExcel->getActiveSheet()->getStyle('I2:I'.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
															->getStartColor()->setARGB('00EEF2FF');
$objPHPExcel->getActiveSheet()->getStyle('L2:L'.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
															->getStartColor()->setARGB('00EEF2FF');


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->setTitle('업체별 보상지급 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="'.$today.'_업체별 보상지급 내역.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
