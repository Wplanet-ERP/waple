<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

require('Classes/PHPExcel.php');
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_price.php');
require('inc/helper/_upload.php');
require('inc/helper/work_cms.php');
require('inc/model/ProductCms.php');
require('inc/model/WorkCms.php');
require('inc/model/Custom.php');


# 기본 엑셀 변수 설정
$start_file         = $_FILES["start_file"];
$product_model      = ProductCms::Factory();
$order_model        = WorkCms::Factory();
$delivery_model     = WorkCms::Factory();
$delivery_model->setMainInit("work_cms_delivery", "no");
$jpy_currency       = getCurrency("jpy");
$jpy_currency_rate  = $jpy_currency['krw'];
$log_c_no           = isset($_POST['start_log_company']) ? $_POST['start_log_company'] : "5659";
$task_run_s_no      = $session_s_no;
$task_run_team      = $session_team;
$regdatetime        = date('Y-m-d H:i:s');
$regdate            = date('Y-m-d');

$total_dp_company_list          = getStartDpCompanyList();
$chk_dp_company_list            = $total_dp_company_list[$log_c_no];
$total_dp_company_name_list     = getStartConvertDpCompanyList();

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($start_file['name']) && !empty($start_file['name'])){
    $upload_file_path = add_store_file($start_file, "upload_management");
    $upload_file_name = $start_file['name'];
}

$upload_data  = array(
    "upload_type"   => "7",
    "upload_kind"   => $log_c_no,
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $task_run_s_no,
    "regdate"       => $regdatetime,
);
$upload_model->insert($upload_data);

$file_no         = $upload_model->getInsertId();
$excel_file_path = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($excel_file_path);
$excel->setActiveSheetIndex(0);

$objWorksheet       = $excel->getActiveSheet();
$totalRow           = $objWorksheet->getHighestRow();
$excel_data_list    = [];
$delivery_data      = [];
$order_dp_list      = [];
$order_prd_list     = [];

# DB 데이터 시작
for ($i = 2; $i <= $totalRow; $i++)
{
    #변수 초기화
    $stock_date = $order_date = $payment_date = "";
    $delivery_price = $final_price = $dp_price = $dp_price_vat = $unit_price = $unit_delivery_price = 0;

    $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));  //상품주문번호
    $order_number       = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));  //주문번호
    $sub_order_number   = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));  //주문번호2
    $zip_code           = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));  //우편번호
    $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //수령지
    $recipient          = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //수령자
    $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //수령자 전화번호
    $manager_sku        = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  //매니저코드
    $sku                = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //상품코드
    $prd_name           = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //상품명
    $prd_option         = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //옵션정보
    $quantity           = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  //수량
    $order_date         = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //주문일자
    $stock_date         = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue()));  //발송처리일
    $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //결제금액
    $price_type         = $objWorksheet->getCell("U{$i}")->getValue() ? (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue())) : "JPY";  //화폐단위
    $dp_c_name_val      = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //판매 site
    $delivery_type      = "";  //택배사
    $delivery_no        = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //송장번호

    if(empty($order_number)){
        continue;
    }

    # 엑셀 시간 형식 변경
    if(empty($order_date) && empty($stock_date)) {
        $order_date = $regdatetime;
        $stock_date = $regdate;
    }
    elseif(empty($order_date) && !empty($stock_date)){
        $order_date = $stock_date;
    }
    elseif(!empty($order_date) && empty($stock_date)){
        $stock_date = date("Y-m-d", strtotime($order_date));
    }
    $payment_date  = $order_date;

    # 금액 작업
    $org_price      = $dp_price_val;
    $convert_price  = ($org_price > 0) ? round($dp_price_val*$jpy_currency_rate, -1) : 0;
    $dp_price       = (int)$convert_price / 1.1;
    $dp_price       = round($dp_price, -1); // 1의자리 반올림
    $dp_price_vat   = (int)$convert_price;
    $unit_price     = (int)$convert_price;

    if($dp_c_name_val == "Qoo10"){
        $order_number = $sub_order_number;
    }
    $origin_ord_no      = $order_number;

    if(isset($order_prd_list[$order_number][$manager_sku])){
        continue;
    }

    # 상품코드가 없는 경우
    if(!$sku){
        echo "ROW : {$i}<br/>";
        echo "택배리스트 반영에 실패하였습니다.<br>상품코드가 없습니다.<br>
            주문날짜    : {$order_date}<br>
            주문번호    : {$order_number}<br>
            수령자명    : {$recipient}<br>
            Excel 상품명: {$prd_name}<br>";
        exit;
    }

    if(!empty($manager_sku)){
        $prd_cms_data   = $product_model->getWorkCmsItem($manager_sku, "prd_code");
    }else{
        $prd_cms_data   = $product_model->getWorkCmsItem($sku, "prd_code");
    }

    if(empty($prd_cms_data))
    {
        echo "ROW : {$i}<br/>";
        echo "택배리스트 반영에 실패하였습니다.<br>해당 상품이 없습니다.<br>
            주문날짜    : {$order_date}<br>
            주문번호    : {$order_number}<br>
            수령자명    : {$recipient}<br>
            Excel 상품명: {$prd_name}<br>";
        exit;
    }

    # 구매처 처리
    if(!isset($order_dp_list[$order_number]))
    {
        if(array_search($dp_c_name_val, $chk_dp_company_list) !== false) {
            $ord_dp_c_no = array_search($dp_c_name_val, $chk_dp_company_list);
            $order_dp_list[$order_number]['dp_c_no']   = $ord_dp_c_no;
            $order_dp_list[$order_number]['dp_c_name'] = $total_dp_company_name_list[$ord_dp_c_no];
        }
        else
        {
            echo "ROW : {$i}<br/>";
            echo "택배리스트 반영에 실패하였습니다.<br>판매사이트 값이 없습니다.<br>
                주문날짜    : {$order_date}<br>
                주문번호    : {$order_number}<br>
                수령자명    : {$recipient}<br>
            ";
            exit;
        }
    }

    $dp_c_no    = $order_dp_list[$order_number]['dp_c_no'];
    $dp_c_name  = $order_dp_list[$order_number]['dp_c_name'];

    if(strpos($order_number, "CS") !== false && empty($dp_c_name_val))
    {
        $delivery_data[$delivery_no] = array(
            "order_number"  => $order_number,
            "dp_c_no"       => $dp_c_no,
            "delivery_type" => $delivery_type,
            "delivery_no"   => $delivery_no
        );

        continue;
    }

    if(!empty($manager_sku)){
        $order_prd_list[$order_number][$manager_sku] = 1;
    }

    $prd_no         = $prd_cms_data['prd_no'];
    $c_no           = $prd_cms_data['c_no'];
    $c_name         = $prd_cms_data['c_name'];
    $s_no           = $prd_cms_data['manager'];
    $team           = $prd_cms_data['team'];
    $task_req       = "상품명 :: {$prd_name}\r\n주문번호 :: {$order_number}\r\n구매처 :: {$dp_c_name}";
    $task_req_s_no  = $s_no;
    $task_req_team  = $team;

    if(!$order_model->chkExistOrder("order_number='{$order_number}' AND prd_no='{$prd_no}'"))
    {
        $excel_data_list[] = array(
            "delivery_state"        => 4,
            "stock_date"            => $stock_date,
            "c_no"                  => $c_no,
            "c_name"                => $c_name,
            "log_c_no"              => $log_c_no,
            "s_no"                  => $s_no,
            "team"                  => $team,
            "prd_no"                => $prd_no,
            "quantity"              => $quantity,
            "order_number"          => $order_number,
            "order_date"            => $order_date,
            "payment_date"          => $payment_date,
            "recipient"             => $recipient,
            "recipient_hp"          => $recipient_hp,
            "recipient_addr"        => $recipient_addr,
            "zip_code"              => $zip_code,
            "dp_price"              => $dp_price,
            "dp_price_vat"          => $dp_price_vat,
            "dp_c_no"               => $dp_c_no,
            "dp_c_name"             => $dp_c_name,
            "regdate"               => $regdatetime,
            "write_date"            => $regdatetime,
            "task_req"              => $task_req,
            "task_req_s_no"         => $task_req_s_no,
            "task_req_team"         => $task_req_team,
            "task_run_regdate"      => !empty($order_date) ? $order_date : $regdatetime,
            "task_run_s_no"         => $task_run_s_no,
            "task_run_team"         => $task_run_team,
            "price_type"            => $price_type,
            "org_price"             => $org_price,
            "shop_ord_no"           => $shop_ord_no,
            "delivery_price"        => $delivery_price,
            "unit_price"            => $unit_price,
            "unit_delivery_price"   => $unit_delivery_price,
            "origin_ord_no"         => $origin_ord_no,
            "file_no"               => $file_no,
        );

        if(!empty($delivery_no))
        {
            $delivery_data[$delivery_no] = array(
                "order_number"  => $order_number,
                "dp_c_no"       => $dp_c_no,
                "delivery_type" => $delivery_type,
                "delivery_no"   => $delivery_no
            );
        }
    }
}

if(!empty($excel_data_list))
{
    if($order_model->multiInsert($excel_data_list))
    {
        if(!empty($delivery_data)) {
            $delivery_model->multiInsert($delivery_data);
        }

        exit("<script>alert('택배리스트에 반영 되었습니다.');location.href='work_list_cms.php?sch_ord_bundle=1&sch_file_no={$file_no}';</script>");
    }else{
        exit("<script>alert('택배리스트 반영에 실패했습니다.');location.href='waple_upload_management.php';</script>");
    }
}
elseif(!empty($delivery_data)) {
    $delivery_model->multiInsert($delivery_data);
    exit("<script>alert('택배리스트 운송장 반영 되었습니다.');location.href='work_list_cms.php?sch_ord_bundle=1&sch_file_no={$file_no}';</script>");
}

exit("<script>alert('택배리스트 반영할 데이터가 없습니다.');location.href='waple_upload_management.php';</script>");


?>
