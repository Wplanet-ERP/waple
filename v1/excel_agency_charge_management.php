<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/agency.php');
require('inc/model/Team.php');
require('inc/model/Agency.php');
require('Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);


$lfcr = chr(10) ;

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "기준월")
    ->setCellValue('B1', "광고매체")
    ->setCellValue('C1', "매체대행사")
    ->setCellValue('D1', "광고주 ID")
    ->setCellValue('E1', "계열사")
    ->setCellValue('F1', "업체명")
    ->setCellValue('G1', "부서")
    ->setCellValue('H1', "담당자")
    ->setCellValue('I1', "전액잔액")
    ->setCellValue('J1', "충전금")
    ->setCellValue('K1', "소진금")
    ->setCellValue('L1', "광고주 충전금")
    ->setCellValue('M1', "잔액")
    ->setCellValue('N1', "메모")
;

# 검색조건
$add_where          = "1=1";
$prev_base_mon      = date('Y-m', strtotime("-1 months"));
$sch_base_mon       = isset($_GET['sch_base_mon']) ? $_GET['sch_base_mon'] : $prev_base_mon;
$sch_base_s_date    = $sch_base_mon."-01";
$sch_base_e_day     = date("t", strtotime($sch_base_s_date));
$sch_base_e_date    = $sch_base_mon."-{$sch_base_e_day}";
$sch_prev_mon       = date('Y-m', strtotime("{$sch_base_mon} -1 months"));
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "1";
$sch_media          = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_partner_name   = isset($_GET['sch_partner_name']) ? $_GET['sch_partner_name'] : "";
$sch_manager_team   = isset($_GET['sch_manager_team']) ? $_GET['sch_manager_team'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_is_memo        = isset($_GET['sch_is_memo']) ? $_GET['sch_is_memo'] : "";
$sch_quick_type     = isset($_GET['sch_quick_type']) ? $_GET['sch_quick_type'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "charge_price";

if(!empty($sch_base_mon)) {
    $add_where .= " AND `ac`.base_mon = '{$sch_base_mon}'";
}

if(!empty($sch_quick_type))
{
    switch($sch_quick_type)
    {
        case '1':
            $add_where  .= " AND `ac`.total_price <= 0";
            break;
        case '2':
            $add_where  .= " AND `ac`.prev_price <= 0";
            break;
    }
}

if(!empty($sch_my_c_no)){
    $add_where   .= " AND (SELECT c.my_c_no FROM company c WHERE c.c_no=`ac`.partner) = '{$sch_my_c_no}'";
}

if(!empty($sch_media)){
    $add_where   .= " AND `ac`.media = '{$sch_media}'";
}

if(!empty($sch_agency)){
    $add_where   .= " AND `ac`.agency = '{$sch_agency}'";
}

if(!empty($sch_partner_name)){
    $add_where   .= " AND `ac`.partner_name LIKE '%{$sch_partner_name}%'";
}

if(!empty($sch_is_memo)){
    if($sch_is_memo == '1') {
        $add_where .= " AND (`ac`.memo IS NOT NULL AND `ac`.memo != '')";
    }elseif($sch_is_memo == '2') {
        $add_where .= " AND (`ac`.memo IS NULL OR `ac`.memo = '')";
    }
}

if (!empty($sch_manager_team))
{
    if ($sch_manager_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_manager_team);
        $add_where 		 .= " AND `ac`.manager_team IN({$sch_team_code_where})";
    }
}

if (!empty($sch_manager))
{
    if ($sch_manager != "all") {
        $add_where 		 .= " AND `ac`.manager='{$sch_manager}'";
    }
}

$add_order_by = "";
$ord_type_by  = "";
$order_by_val = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

    if(!empty($ord_type_by))
    {
        if($ord_type_by == '1'){
            $order_by_val = "ASC";
        }elseif($ord_type_by == '2'){
            $order_by_val = "DESC";
        }
        $add_order_by .= "{$ord_type} {$order_by_val}";
    }
}

# 충전금
$agency_charge_sql = "
    SELECT
        *,
        (SELECT c.my_c_no FROM company c WHERE c.c_no=`ac`.partner) AS my_c_no,         
        (SELECT s.s_name FROM staff s WHERE s.s_no=`ac`.manager) AS s_name,         
        (SELECT t.team_name FROM team t WHERE t.team_code=`ac`.manager_team) AS t_name
    FROM agency_charge AS `ac` 
    WHERE {$add_where}
    ORDER BY {$add_order_by}
";
$agency_charge_query = mysqli_query($my_db, $agency_charge_sql);
$agency_option       = getSettleAgencyOption();
$media_option        = getAdvertiseMediaOption();
$sch_my_company_list = getAdMyCompanyOption();
$work_sheet = $objPHPExcel->setActiveSheetIndex(0);
$idx        = 2;
while($agency_charge = mysqli_fetch_assoc($agency_charge_query))
{
    $agency_charge['agency_name'] = $agency_option[$agency_charge['agency']];
    $agency_charge['media_name']  = $media_option[$agency_charge['media']];
    $agency_charge['my_company']  = $sch_my_company_list[$agency_charge['my_c_no']];
    
    $work_sheet
        ->setCellValue("A{$idx}", $agency_charge['base_mon'])
        ->setCellValue("B{$idx}", $agency_charge['media_name'])
        ->setCellValue("C{$idx}", $agency_charge['agency_name'])
        ->setCellValue("D{$idx}", $agency_charge['advertiser_id'])
        ->setCellValue("E{$idx}", $agency_charge['my_company'])
        ->setCellValue("F{$idx}", $agency_charge['partner_name'])
        ->setCellValue("G{$idx}", $agency_charge['t_name'])
        ->setCellValue("H{$idx}", $agency_charge['s_name'])
        ->setCellValue("I{$idx}", $agency_charge['prev_price'])
        ->setCellValue("J{$idx}", $agency_charge['charge_price'])
        ->setCellValue("K{$idx}", $agency_charge['ad_total_price'])
        ->setCellValue("L{$idx}", $agency_charge['ad_charge_price'])
        ->setCellValue("M{$idx}", $agency_charge['total_price'])
        ->setCellValue("N{$idx}", $agency_charge['memo'])
    ;

    $idx++;
}
$idx--;


$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF343A40');
$objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:N{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:N{$idx}")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("A1:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:N{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("D2:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("I2:M{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);

$excel_filename = "충전금 잔액관리 리스트";
$objPHPExcel->getActiveSheet()->setTitle($excel_filename);
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.date("Ymd")."_{$excel_filename}.xlsx");

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save('php://output');
?>
