<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/project.php');

# 검색 초기화 및 조건 생성
$pj_er_no = isset($_GET['pj_er_no']) ? $_GET['pj_er_no'] : (isset($_POST['pj_er_no']) ? isset($_POST['pj_er_no']) : "");
$pj_c_no  = isset($_GET['pj_c_no']) ? $_GET['pj_c_no'] : (isset($_POST['pj_c_no']) ? isset($_POST['pj_c_no']) : "");

$partner_evaluation_list = [];
if(!empty($pj_er_no) || !empty($pj_c_no))
{
    $add_where = "1=1 AND per.active='1' AND per.pj_participation='2'";
    if(!empty($pj_c_no))
    {
        $add_where .= " AND `pere`.pj_c_no = '{$pj_c_no}'";
        $smarty->assign('$pj_c_no', $pj_c_no);
    }

    if(!empty($pj_er_no))
    {
        $add_where .= " AND `pere`.pj_er_no = '{$pj_er_no}'";
        $smarty->assign('pj_er_no', $pj_er_no);
    }

    // 리스트 쿼리
    $partner_evaluation_sql = "
        SELECT
            pere.`pj_ere_no`,
            pere.`pj_no`,
            per.pj_c_type,
            per.pj_c_name,
            pere.score,
            pere.regdate,        
            (SELECT s.s_name FROM staff s WHERE s.s_no=pere.req_s_no) as req_s_name
        FROM
            `project_external_report_evaluation` `pere`
        LEFT JOIN project_external_report `per` ON `per`.pj_er_no = `pere`.pj_er_no
        WHERE {$add_where}
        ORDER BY pere.pj_ere_no DESC
  ";

    // 전체 게시물 수
    $partner_evaluation_total_sql	= "SELECT count(pj_ere_no) FROM (SELECT `pere`.pj_ere_no FROM `project_external_report_evaluation` `pere` LEFT JOIN project_external_report `per` ON `per`.pj_er_no = `pere`.pj_er_no WHERE {$add_where}) AS cnt";
    $partner_evaluation_total_query	= mysqli_query($my_db, $partner_evaluation_total_sql);
    if(!!$partner_evaluation_total_query)
        $partner_evaluation_total_result = mysqli_fetch_array($partner_evaluation_total_query);

    $partner_evaluation_total = $partner_evaluation_total_result[0];

    $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= 20;
    $offset 	= ($pages-1) * $num;
    $pagenum 	= ceil($partner_evaluation_total/$num);

    if ($pages >= $pagenum){$pages = $pagenum;}
    if ($pages <= 0){$pages = 1;}

    $search_url = getenv("QUERY_STRING");
    $page		= pagelist($pages, "project_regist_external_partner_evaluation_list.php", $pagenum, $search_url);
    $smarty->assign("search_url", $search_url);
    $smarty->assign("total_num", $partner_evaluation_total);
    $smarty->assign("pagelist", $page);

    $partner_evaluation_sql    .= " LIMIT {$offset}, {$num} ";
    $partner_evaluation_query	= mysqli_query($my_db, $partner_evaluation_sql);
    $resource_type_option       = getResourceTypeOption();
    if(!!$partner_evaluation_query) {
        while ($partner_evaluation = mysqli_fetch_assoc($partner_evaluation_query))
        {
            $partner_evaluation['pj_c_type_name'] = $resource_type_option[$partner_evaluation['pj_c_type']];
            $partner_evaluation_list[] = $partner_evaluation;
        }
    }
}

$smarty->assign("partner_evaluation_list", $partner_evaluation_list);
$smarty->display('project_regist_external_partner_evaluation_list.html');

?>
