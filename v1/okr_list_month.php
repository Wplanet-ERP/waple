<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');
require('inc/model/MyQuick.php');

$proc = (isset($_POST['process'])) ? $_POST['process'] : "";

if ($proc == "f_state") { // 진행상태 저장
	$oo_no = (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	$sql = "UPDATE okr_obj oo SET oo.state = '" . $value . "' WHERE oo.oo_no = '" . $oo_no . "'";
	if (!mysqli_query($my_db, $sql))
		echo "진행상태 저장에 실패 하였습니다.";
	else
		echo "진행상태가 저장 되었습니다.";

	exit;

}
elseif ($proc == "chk_state")
{
    $oo_no_list = (isset($_POST['chk_oo_no_list'])) ? $_POST['chk_oo_no_list'] : "";
    $save_state = (isset($_POST['chk_state'])) ? $_POST['chk_state'] : "";
    $search_url_val = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $oo_no_tok = explode("||", $oo_no_list);
    $oo_no_ins = implode(",", $oo_no_tok);

    if(!!$oo_no_tok && !!$save_state){
        $sql = "UPDATE okr_obj SET state='{$save_state}' WHERE oo_no IN({$oo_no_ins})" ;

        if (!mysqli_query($my_db, $sql)){
            echo ("<script>alert('저장에 실패 하였습니다.\\n담당자에게 문의해 주세요.');</script>");
        }else{
            echo ("<script>alert('모두 저장 하였습니다.');</script>");
        }
	}else{
        echo ("<script>alert('선택된 상태값이 없습니다.');</script>");
	}

    exit ("<script>location.href='okr_list_month.php?$search_url_val';</script>");
}
elseif($proc=="del_okr")
{
	$oo_no			= (isset($_POST['oo_no'])) ? $_POST['oo_no'] : "";
    $search_url_val	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $okr_del_sql = "UPDATE okr_obj SET display='2' WHERE oo_no = '{$oo_no}'";
    if(!mysqli_query($my_db, $okr_del_sql)){
        exit("<script>alert('삭제에 실패 하였습니다.');location.href='okr_list_month.php?{$search_url_val}';</script>");
    }else{
        exit("<script>alert('삭제에 성공 하였습니다.');location.href='okr_list_month.php?{$search_url_val}';</script>");
    }

}
else
{
	# Navigation & My Quick
	$nav_prd_no  = "86";
	$nav_title   = "월간목표 리스트";
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_title", $nav_title);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where = " 1=1 AND oo.display=1 AND oo.type='month'";
	$add_orderby = "oo.oo_no DESC";

	$sch_oo_no_get 	 = isset($_GET['sch_oo_no']) ? $_GET['sch_oo_no'] : "";
	$sch_state_get 	 = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
	$sch_kind_get 	 = isset($_GET['sch_kind']) ? $_GET['sch_kind'] : "0";
  	$sch_active 	 = isset($_GET['sch_active']) ? $_GET['sch_active'] : "1";
	$sch_team_get 	 = isset($_GET['sch_team']) ? $_GET['sch_team'] : $session_team;
	$sch_s_no_get 	 = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
	$sch_s_name_get  = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
	$sch_obj_name 	 = isset($_GET['sch_obj_name']) ? $_GET['sch_obj_name'] : "";
	$sch_s_date_term = isset($_GET['sch_s_date_term']) ? $_GET['sch_s_date_term'] : "";
	$sch_e_date_term = isset($_GET['sch_e_date_term']) ? $_GET['sch_e_date_term'] : "";
	$sch_month = isset($_GET['sch_month']) ? $_GET['sch_month'] : "";

	#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
	$url_check_str 			= $_SERVER['QUERY_STRING'];
	$url_chk_team_result 	= false;
	$url_chk_staff_result 	= false;
	if(empty($url_check_str)){
		$url_check_team_where   = getTeamWhere($my_db, $session_team);
		$url_check_sql    		= "SELECT count(*) as cnt FROM okr_obj oo WHERE oo.display=1 AND oo.type='month' AND team IN({$url_check_team_where})";
		$url_check_query  		= mysqli_query($my_db, $url_check_sql);
		$url_check_result 		= mysqli_fetch_assoc($url_check_query);

		if($url_check_result['cnt'] == 0){
			$sch_team_get 		 = "all";
			$url_chk_team_result = true;
		}else{
			$url_check_sub_sql    = "SELECT count(*) as cnt FROM okr_obj oo WHERE oo.display=1 AND oo.type='month' AND team IN({$url_check_team_where}) AND oo.s_no = '{$session_s_no}'";
			$url_check_sub_query  = mysqli_query($my_db, $url_check_sub_sql);
			$url_check_sub_result = mysqli_fetch_assoc($url_check_sub_query);

			if($url_check_sub_result['cnt'] == 0){
				$sch_s_no_get 		  = "all";
				$url_chk_staff_result = true;
			}
		}
	}

	if (!empty($sch_s_date_term)) {
		$add_where .= " AND oo.sdate='".$sch_s_date_term."'";
		$smarty->assign("sch_s_date_term", $sch_s_date_term);
	}

	if (!empty($sch_e_date_term)) {
		$add_where .= " AND oo.edate='".$sch_e_date_term."'";
		$smarty->assign("sch_e_date_term", $sch_e_date_term);
	}

	if (!empty($sch_month)) {
		$smarty->assign("sch_month", $sch_month);
	}

	if (!empty($sch_oo_no_get)) {
		$add_where .= " AND oo.oo_no='" . $sch_oo_no_get . "'";
		$smarty->assign("sch_oo_no", $sch_oo_no_get);
	}

	if (!empty($sch_state_get)) {
		$add_where .= " AND oo.state='" . $sch_state_get . "'";
		$smarty->assign("sch_state", $sch_state_get);
	}

	if (!empty($sch_kind_get)) {
		$add_where .= " AND oo.kind='" . $sch_kind_get . "'";
		$smarty->assign("sch_kind", $sch_kind_get);
	}

    if (!empty($sch_active))
    {
     	if($sch_active == '1'){
            $add_where .= " AND oo.s_no IN(SELECT s.s_no FROM staff s WHERE s.staff_state='1')";
		}elseif($sch_active == '2'){
            $add_where .= " AND oo.s_no IN(SELECT s.s_no FROM staff s WHERE s.staff_state='3')";
		}
        $smarty->assign("sch_active", $sch_active);
    }

    $sch_team_code_where = "";
	if (!empty($sch_team_get)) {
		if ($sch_team_get != "all") {
            $sch_team_code_where = getTeamWhere($my_db, $sch_team_get);
			$add_where .= " AND team IN ({$sch_team_code_where})";
		}
		$smarty->assign("sch_team", $sch_team_get);
	}else{
        $sch_team_code_where = getTeamWhere($my_db, $session_team);
        $add_where .= " AND team IN ({$sch_team_code_where})";
		$smarty->assign("sch_team", $session_team);
	}

	if (!empty($sch_s_no_get)) {
		if ($sch_s_no_get != "all") {
			$add_where .= " AND (oo.s_no = '" . $sch_s_no_get . "' or oo.s_no = 0) ";
		}
		$smarty->assign("sch_s_no",$sch_s_no_get);
	}else{
		if($sch_team_get == $session_team){
			$add_where .= " AND oo.s_no = '{$session_s_no}' ";
			$smarty->assign("sch_s_no", $session_s_no);
		}
	}

	if (!empty($sch_s_name_get)) {
        $add_where .= " AND oo.s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_s_name_get}%')";
        $smarty->assign("sch_s_name", $sch_s_name_get);
	}

	// 부서별 직원 목록 가져오기
    $staff_list = [];
    if($sch_team_code_where)
    {
        $staff_team_where 		= "";
        $staff_team_where_list 	= [];

        $sch_team_code_list_val = explode(",", $sch_team_code_where);

        foreach($sch_team_code_list_val as $sch_team_code){
            $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
        }

        if(!empty($staff_team_where_list))
        {
            $staff_team_where = implode(" OR ", $staff_team_where_list);
            $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
            $staff_result=mysqli_query($my_db,$staff_sql);
            while($staff=mysqli_fetch_array($staff_result)) {
                $staff_list[]=array(
                    "s_no"=>trim($staff['s_no']),
                    "s_name"=>trim($staff['s_name'])
                );
            }
        }
    }

	$smarty->assign("staff_list", $staff_list);
    $smarty->assign("sch_team_list", $sch_team_name_list);


	if (!empty($sch_obj_name)) {
		$add_where .= " AND oo.obj_name like '%" . $sch_obj_name . "%'";
		$smarty->assign("sch_obj_name", $sch_obj_name);
	}

	# 부서별 우선순위
    $sch_team_sort 	 = isset($_GET['sch_team_sort']) ? $_GET['sch_team_sort'] : "";
	if(!empty($sch_team_sort)){
		$add_orderby = "t_priority ASC, oo.oo_no DESC";
        $smarty->assign("sch_team_sort", $sch_team_sort);
	}

	// 페이지에 따른 limit 설정
	$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num = 20;
	$offset = ($pages-1) * $num;

	// 리스트 쿼리

	// OKR OBJECT 리스트 쿼리
	$okr_obj_sql = "
		SELECT
			oo.oo_no,
			oo.state,
			oo.kind,
			oo.obj_name,
			oo.sdate,
			oo.edate,
			oo.team,
			oo.s_no,
			(SELECT s_name FROM staff s where s.s_no=oo.s_no) AS s_name,
			oo.check_1_date,
			oo.check_1_rate,
			oo.check_2_date,
			oo.check_2_rate,
			round((SELECT AVG(ok.complete_rate) FROM okr_kr ok WHERE ok.oo_no=oo.oo_no),1) AS avg_complete_rate,
			(SELECT t.priority FROM team as t WHERE t.team_code=oo.team) as t_priority
		FROM okr_obj oo
		WHERE {$add_where}
		ORDER BY $add_orderby
	";

	// 전체 게시물 수
	$cnt_sql = "SELECT count(*) FROM (".$okr_obj_sql.") AS cnt";
	$result= mysqli_query($my_db, $cnt_sql);
	$cnt_data=mysqli_fetch_array($result);
	$total_num = $cnt_data[0];

	$smarty->assign(array(
		"total_num"=>number_format($total_num)
	));

	$smarty->assign("total_num",$total_num);
	$pagenum = ceil($total_num/$num);

	// 페이징
	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	if(empty($url_check_str)){
		if($url_chk_team_result){
			$search_url = "sch_team=all";
		}elseif($url_chk_staff_result) {
			$search_url = "sch_team={$session_team}&sch_staff=all";
		}
	}else{
		$search_url = getenv("QUERY_STRING");
	}

	$smarty->assign("search_url",$search_url);
	$page=pagelist($pages, "okr_list_month.php", $pagenum, $search_url);
	$smarty->assign("pagelist",$page);

	// 리스트 쿼리 결과(데이터)
	//echo "$okr_obj_sql<br>"; //exit;
	$okr_obj_sql .= "LIMIT $offset,$num";
	$result= mysqli_query($my_db, $okr_obj_sql);

	while($okr_obj_array = mysqli_fetch_array($result))
	{

		$obj_d_day = intval((strtotime(date("Y-m-d",time()))-strtotime($okr_obj_array['edate'])) / 86400);

		$obj_term = 0;
		$obj_sdate = date('m', strtotime($okr_obj_array['sdate']));
		$obj_edate = date('m-d', strtotime($okr_obj_array['edate']));

		switch($obj_sdate){
			case '01': $obj_term = ($obj_edate == '12-31') ? 4 : 1; break;
			case '05': $obj_term = 2; break;
			case '09': $obj_term = 3; break;
		}

		$obj_term_name = $obj_sdate."월";

		#OKR 목표점수 및 완료점수 계산
        $okr_kr_sql = "SELECT importance, difficulty, challenge, complete_rate FROM okr_kr ok where ok.oo_no='{$okr_obj_array['oo_no']}'";
        $okr_kr_query = mysqli_query($my_db, $okr_kr_sql);
        $purpose_quality  = 0;
        $complete_quality = 0;
        while($okr_kr_result = mysqli_fetch_assoc($okr_kr_query))
		{
        	$importance_val 	= $okr_kr_result['importance'];
        	$difficulty_val 	= $okr_kr_result['difficulty'];
        	$challenge_val  	= $okr_kr_result['challenge'];
        	$complete_rate_val  = $okr_kr_result['complete_rate'];

        	$purpose_quality += round((($importance_val*0.3) + ($difficulty_val*0.3) + ($challenge_val*0.4)), 1);

        	if($complete_rate_val > 0){
                $complete_rate = $complete_rate_val/100;
                $complete_quality += round((($importance_val*0.3*$complete_rate) + ($difficulty_val*0.3*$complete_rate) + ($challenge_val*0.4*$complete_rate)), 1);
			}
		}


		$okr_obj[] = array(
			"oo_no"=>$okr_obj_array['oo_no'],
			"state"=>$okr_obj_array['state'],
			"kind"=>$okr_obj_array['kind'],
			"obj_name"=>$okr_obj_array['obj_name'],
			"sdate"=>$okr_obj_array['sdate'],
			"edate"=>$okr_obj_array['edate'],
			"obj_term" => $obj_term_name,
			"team"=>$okr_obj_array['team'],
			"s_no"=>$okr_obj_array['s_no'],
			"s_name"=>$okr_obj_array['s_name'],
			"purpose_quality" => sprintf('%0.1f',$purpose_quality),
			"complete_quality" =>sprintf('%0.1f',$complete_quality),
			"check_1_date"=>$okr_obj_array['check_1_date'],
			"check_2_date"=>$okr_obj_array['check_2_date'],
			"check_1_rate"=>$okr_obj_array['check_1_rate'],
			"check_2_rate"=>$okr_obj_array['check_2_rate'],
			"avg_complete_rate"=>$okr_obj_array['avg_complete_rate'],
			"obj_d_day"=>$obj_d_day
		);
	}
	$smarty->assign("okr_obj", $okr_obj);

    $staff_state_list = array('1' => 'active', '2' => 'deactivate');
	$smarty->assign("staff_state_list", $staff_state_list);

	$current_year  = date('Y');
	$month_list = [];
	for($i=1; $i<13; $i++){
		$sdate = $current_year.'-'.str_pad($i, 2, '0', STR_PAD_LEFT).'-01';
		$eday = DATE('t', strtotime($sdate));
		$edate = $current_year.'-'.str_pad($i, 2, '0', STR_PAD_LEFT).'-'.$eday;

		$month_list[$i] = array(
			'label' => str_pad($i, 2, '0', STR_PAD_LEFT)."월",
			'date'	=> $sdate.",".$edate
		);
	}
	$smarty->assign("month_list", $month_list);

	$smarty->display('okr_list_month.html');
}
?>
