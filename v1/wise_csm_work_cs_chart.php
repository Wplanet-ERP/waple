<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_date.php');
require('inc/helper/wise_csm.php');
require('inc/model/Staff.php');

# 사용 변수들
$staff_model        = Staff::Factory();
$cs_staff_list      = $staff_model->getActiveTeamStaff("00244");
$cs_staff_list['309'] = "이채하";
$picker_list        = getCsDpList();
$cs_dp_list         = array_keys($picker_list);
$cs_dp_list_text    = implode(",", $cs_dp_list);
$date_name_option   = getDateChartOption();

# 날짜별 검색
$add_where      = "w.delivery_state!='5' AND w.dp_c_no IN({$cs_dp_list_text})";
$today_s_w		= date('w')-1;
$base_s_date    = date('Y-m-d', strtotime("-{$today_s_w} day"));
$sch_date_type  = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "3";
$sch_date       = isset($_GET['sch_date']) ? $_GET['sch_date'] : date('Y-m');
$sch_s_month    = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("-3 months"));
$sch_e_month    = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week     = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d', strtotime("{$base_s_date} -2 weeks"));
$sch_e_week     = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d", strtotime("{$base_s_date} +6 day"));
$sch_s_date     = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d', strtotime("-10 day"));
$sch_e_date     = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');
$sch_s_no       = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_date', $sch_date);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);

if(!empty($sch_s_no)) {
    $add_where  .= " AND w.task_run_s_no='{$sch_s_no}'";
    $smarty->assign('sch_s_no', $sch_s_no);
}

# 전체 기간 조회 및 누적데이터 조회
$all_date_where   = "";
$all_date_key	  = "";
$add_date_where   = "";
$add_date_column  = "";
$stats_list       = [];

$add_return_date_where  = "";
$add_return_date_column = "";

if($sch_date_type == '3') //월간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $sch_s_datetime  = "{$sch_s_month}-01";
    $sch_e_day       = date("t", strtotime($sch_e_month));
    $sch_e_datetime  = "{$sch_e_month}-{$sch_e_day}";

    $add_where      .= " AND (w.stock_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}')";
    $add_key_column  = "DATE_FORMAT(w.stock_date, '%Y%m')";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}')";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $sch_s_datetime  = $sch_s_week;
    $sch_e_datetime  = $sch_e_week;

    $add_where      .= " AND (w.stock_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}')";
    $add_key_column  = "DATE_FORMAT(DATE_SUB(w.stock_date, INTERVAL(IF(DAYOFWEEK(w.stock_date)=1,8,DAYOFWEEK(w.stock_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '$sch_e_date')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $sch_s_datetime  = $sch_s_date;
    $sch_e_datetime  = $sch_e_date;

    $add_where      .= " AND (w.stock_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}')";
    $add_key_column  = "DATE_FORMAT(w.stock_date, '%Y%m%d')";
}

# 기본 STAT 리스트 Init 및 x key 및 타이틀 설정
$x_label_list       = [];
$x_table_th_list    = [];
$work_cs_list       = [];

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	GROUP BY chart_key
	ORDER BY chart_key
";
if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_name_option[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        $x_label_list[$date['chart_key']]    = "'{$chart_title}'";
        $x_table_th_list[$date['chart_key']] = $chart_title;

        foreach($picker_list as $c_no => $label){
            $work_cs_list[$date['chart_key']][$c_no] = 0;
        }
    }
}

# Y 컬럼 Label
$y_label_list   = [];
$y_label_title  = [];
foreach($picker_list as $c_no => $c_name)
{
    $y_label_list[$c_no]  = $c_name;
    $y_label_title[$c_no] = $c_name;
    $y_label_title['all'] = "합산";
}

# 파손/분실/누락 데이터 가져오기
$work_cs_stats_sql = "
    SELECT
        {$add_key_column} as key_date,
        w.dp_c_no,
        COUNT(DISTINCT w.order_number) as ord_cnt 
    FROM `work_cms` as w
    WHERE {$add_where} 
    GROUP BY key_date, dp_c_no
    ORDER BY key_date ASC
";
$work_cs_stats_query = mysqli_query($my_db, $work_cs_stats_sql);
while($work_cs_stats = mysqli_fetch_assoc($work_cs_stats_query))
{
    $work_cs_list[$work_cs_stats['key_date']][$work_cs_stats['dp_c_no']] += $work_cs_stats['ord_cnt'];
}

$full_data = [];
foreach($work_cs_list as $sales_date => $stats_work_data)
{
    $date_qty_sum = 0;
    if($stats_work_data)
    {
        $qty_sum = 0;
        foreach ($stats_work_data as $key => $qty)
        {
            $full_data["each"]["line"][$key]['title']   = $picker_list[$key];
            $full_data["each"]["line"][$key]['data'][]  = $qty;

            $full_data["each"]["bar"][$key]['title']  = $picker_list[$key];
            $full_data["each"]["bar"][$key]['data'][] = $qty;
            $qty_sum += $qty;
            $date_qty_sum += $qty;
        }
    }

    $full_data["sum"]["line"]["all"]['title'] = "합산";
    $full_data["sum"]["line"]["all"]['data'][] = $date_qty_sum;
    $full_data["sum"]["bar"]["all"]['title']  = "합산";
    $full_data["sum"]["bar"]["all"]['data'][] = $date_qty_sum;
}

$smarty->assign('cs_staff_list', $cs_staff_list);
$smarty->assign('x_label_list', implode(',', $x_label_list));
$smarty->assign('x_table_th_list', json_encode($x_table_th_list));
$smarty->assign('legend_list', json_encode($y_label_list));
$smarty->assign('y_label_title', json_encode($y_label_title));
$smarty->assign('picker_list', $picker_list);
$smarty->assign('full_data', json_encode($full_data));

$smarty->display('wise_csm_work_cs_chart.html');
?>
