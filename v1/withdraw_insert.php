<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
include_once('ckadmin.php');
include_once('inc/model/Withdraw.php');
include_once('inc/model/Work.php');
include_once('inc/model/Company.php');

# 파일 변수
$file_name = $_FILES["withdraw_file"]["tmp_name"];

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$withdraw_model = Withdraw::Factory();
$work_model     = Work::Factory();
$company_model  = Company::Factory();

$withdraw_data_list     = [];
$regdate                = date("Y-m-d H:i:s");

for ($i = 2; $i <= $totalRow; $i++)
{
    $wd_date         = "";
    $wd_date_val     = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 출금완료일
    $my_company_text = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 출금 하는 계열사
    $my_company      = "";
    $my_account      = "";
    $wd_company_text = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 지급처
    $bank_name       = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 은행명
    $acc_name        = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 예금주
    $acc_no          = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 계좌번호
    $company_text    = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 업체명
    $wd_price_tmp    = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 출금완료액 VAT 포함
    $wd_fee_per      = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 세율
    $wd_detail       = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   // 지출내역
    $is_upload       = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   // 업로드 여부

    if($is_upload != "여"){
        continue;
    }

    if(!empty($wd_date_val))
    {
        if(strpos($wd_date_val, "T") !== false){
            $wd_date = str_replace("T"," ", $wd_date_val);
            $wd_date = date("Y-m-d", strtotime($wd_date_val));
        }elseif(strpos($wd_date_val, "-") !== false){
            $wd_date = $wd_date_val;
        }elseif(strpos($wd_date_val, ".") !== false){
            $wd_date = str_replace(".","-", $wd_date_val);
        }elseif(strpos($wd_date_val, "/") !== false){
            $wd_date = str_replace("/","-", $wd_date_val);
        }else{
            $wd_date_time   = ($wd_date_val-25569)*86400;
            $wd_date        = date("Y-m-d", $wd_date_time);
        }
    }

    if($my_company_text == "와이즈플래닛컴퍼니"){
        $my_company = "1";
        $my_account = "5";
    }elseif($my_company_text == "와이즈미디어커머스"){
        $my_company = "3";
        $my_account = "8";
    }else{
        echo "{$i} 행의 `계열사`를 찾을 수 없습니다.<br/>계열사 : {$my_company_text}";
        exit;
    }

    $wd_company = $company_model->getNameWhereItem($wd_company_text);
    if(empty($wd_company)){
        echo "{$i} 행의 `지급처`를 찾을 수 없습니다.<br/>지급처 : {$wd_company_text}";
        exit;
    }

    if($company_text == "닥터피엘"){
        $company_text = "닥터피엘(샤워기/세면대/주방용/세탁기)";
    }

    $company = $company_model->getNameWhereItem($company_text);
    if(empty($company)){
        $company = array("my_c_no" => 0,"c_no" => 0, "c_name" => $company_text, "s_no" => 0, "team" => "NULL");
    }

    if(empty($bank_name)){
        echo "{$i} 행의 `은행명`이 비어있습니다.<br/>";
        exit;
    }

    if($wd_fee_per == "") {
        exit("<script>alert('업로드 실패!! {$i}행 세율이 누락되어있습니다.');location.href='withdraw_list.php';</script>");
    }

    $vat_choice         = 0;
    $cost               = 0;
    $supply_cost        = 0;
    $supply_cost_vat    = 0;
    $biz_tax            = 0;
    $local_tax          = 0;
    $wd_price_vat       = 0;

    if($wd_company_text == "인건비"){
        $supply_cost        = $wd_price_tmp;
        $vat_choice         = 2;
        $biz_tax_ratio_chk  = (int)$wd_fee_per;
        $biz_tax_ratio      = 0;
        switch ($biz_tax_ratio_chk){
            case "3":
                $biz_tax_ratio = 0.3;
                break;
            case "4":
                $biz_tax_ratio = 0.4;
                break;
            case "8":
                $biz_tax_ratio = 0.8;
                break;
            case "22":
                $biz_tax_ratio = 2;
                break;
        }
        $local_tax_ratio    = $wd_fee_per-$biz_tax_ratio;
        
        $biz_tax            = floor(($supply_cost * ($biz_tax_ratio / 100))/10)*10;
        $local_tax          = floor(($supply_cost * ($local_tax_ratio / 100))/10)*10;
        $cost               = $supply_cost-($biz_tax+$local_tax);
        $wd_price_vat       = $cost;
    }else{
        $cost           = $wd_price_tmp;
        $wd_price_vat   = $wd_price_tmp;

        if($wd_fee_per > 0){
            $vat_choice         = 1;
            $tax_ratio          = 10;
            $supply_cost_vat    = round($wd_price_vat / ((100 + $tax_ratio) / 10));
            $supply_cost        = (int)$wd_price_vat - (int)$supply_cost_vat;
        }else{
            $vat_choice         = 3;
            $supply_cost        = $wd_price_vat;
        }
    }

    $withdraw_data_list[] = array(
        "wd_subject"        => "주간 외주비 지출 업로드",
        "wd_method"         => 1,
        "wd_state"          => 3,
        "my_c_no"           => $my_company,
        "wd_account"        => $my_account,
        "c_no"              => $wd_company['c_no'],
        "c_name"            => $wd_company['c_name'],
        "s_no"              => $wd_company['s_no'],
        "team"              => $wd_company['team'],
        "wd_date"           => $wd_date,
        "reg_s_no"          => $session_s_no,
        "reg_team"          => $session_team,
        "wd_tax_state"      => 2,
        "vat_choice"        => $vat_choice,
        "supply_cost"       => $supply_cost,
        "supply_cost_vat"   => $supply_cost_vat,
        "biz_tax"           => $biz_tax,
        "local_tax"         => $local_tax,
        "cost"              => $cost,
        "wd_money"          => $wd_price_vat,
        "bk_title"          => $bank_name,
        "bk_name"           => $acc_name,
        "bk_num"            => $acc_no,
        "wd_detail"         => addslashes($wd_detail),
        "regdate"           => $regdate,
        "work_data"         => array(
            "work_state"        => 6,
            "c_no"              => $company['c_no'],
            "c_name"            => $company['c_name'],
            "s_no"              => $company['s_no'],
            "team"              => $company['team'],
            "prd_no"            => 278,
            "regdate"           => $regdate,
            "quantity"          => 1,
            "wd_c_no"           => $wd_company['c_no'],
            "wd_c_name"         => $wd_company['c_name'],
            "wd_price"          => $supply_cost,
            "wd_price_vat"      => $wd_price_vat,
            "task_req"          => "외주비 엑셀 업로드\r\n지급처: {$wd_company['c_name']}\r\n업체명 : {$company['c_name']}\r\n금액: {$wd_price_vat}",
            "task_req_s_no"     => $session_s_no,
            "task_req_team"     => $session_team,
            "task_run_s_no"     => $session_s_no,
            "task_run_team"     => $session_team,
            "task_run_regdate"  => $regdate,
        )
    );
}

if(!empty($withdraw_data_list))
{
    foreach($withdraw_data_list as $withdraw_data)
    {
        $work_data      = $withdraw_data['work_data'];
        unset($withdraw_data['work_data']);

        if($withdraw_model->insert($withdraw_data))
        {
            $new_wd_no = $withdraw_model->getInsertId();
            $work_data['wd_no'] = $new_wd_no;
            if($work_model->insert($work_data)){
                $new_w_no = $work_model->getInsertId();
                $withdraw_model->update(array("wd_no" => $new_wd_no, "w_no" => $new_w_no));
            }
        }
    }
}

exit("<script>alert('주간 외주비 내역이 업로드 되었습니다.');location.href='withdraw_list.php';</script>");

?>