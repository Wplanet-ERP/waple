<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/company.php');
require('inc/helper/project.php');
require('inc/model/MyQuick.php');
require('inc/model/MyCompany.php');
require('inc/model/Company.php');

# Company Model
$company_model = Company::Factory();

# Process 처리
$proc = (isset($_POST['process']))?$_POST['process']:"";

if($proc == "memo")
{
    $c_no		= (isset($_POST['idx']))?$_POST['idx']:"";
	$c_memo		= (isset($_POST['c_memo']))?addslashes($_POST['c_memo']):"";
    $search_url	= isset($_POST['search_url'])?$_POST['search_url']:"";

	$upd_sql    = "UPDATE company SET c_memo='{$c_memo}', c_memo_date=now() WHERE c_no='{$c_no}'";

	if(!mysqli_query($my_db, $upd_sql)){
		echo ("<script>alert('메모 등록에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('메모를 등록하였습니다');</script>");
	}

	exit("<script>location.href='company_list.php?{$search_url}';</script>");
}
elseif($proc == "del_info")
{
    $c_no       = (isset($_POST['idx'])) ? $_POST['idx']:"";
    $lp_no      = (isset($_POST['lp_no'])) ? $_POST['lp_no']:"";
    $type       = (isset($_POST['type'])) ? $_POST['type']:"";
    $search_url = isset($_POST['search_url'])?$_POST['search_url']:"";

    $upd_msg = "삭제에 실패했습니다.";
    if($type == 'default')
    {
    	$chk_file_sql    = "SELECT r_registration, r_accdoc, r_etc FROM company WHERE c_no={$c_no}";
    	$chk_file_query  = mysqli_query($my_db, $chk_file_sql);
    	$chk_file_result = mysqli_fetch_assoc($chk_file_query);

    	if(isset($chk_file_result['r_registration']) && !empty($chk_file_result['r_registration'])){
    	    del_file($chk_file_result['r_registration']);
        }

        if(isset($chk_file_result['r_accdoc']) && !empty($chk_file_result['r_accdoc'])){
            del_file($chk_file_result['r_accdoc']);
        }

        if(isset($chk_file_result['r_etc']) && !empty($chk_file_result['r_etc'])){
            del_file($chk_file_result['r_etc']);
        }
        
        $upd_sql = "UPDATE company SET default_del_status='1', birthday=null, about=null, zipcode=null, address1=null, address2=null, tel=null, fax=null, site=null, tx_company=null, tx_company_ceo=null, tx_company_number=null, tx_manager=null, tx_call1=null, tx_call2=null, tx_email=null, bk_title=null, bk_num=null, bk_name=null, bk_manager=null, o_registration=null, r_registration=null, o_accdoc=null, r_accdoc=null, o_etc=null, r_etc=null WHERE c_no='{$c_no}'";
        if(!mysqli_query($my_db, $upd_sql)){
            $upd_msg = "기본 개인정보 삭제에 실패했습니다";
        }else{
            $upd_msg = "기본 개인정보를 삭제했습니다";
        }
        
	}elseif($type == 'tax'){
        $upd_sql = "UPDATE company set tax_del_status='1' where c_no='{$c_no}'";
        if(mysqli_query($my_db, $upd_sql)){
            $upd_msg = "세금관련 개인정보 삭제에 실패했습니다";
        }else{
            $upd_msg = "세금관련 개인정보를 삭제했습니다";
		}

    }
    elseif($type == 'lector' && $lp_no)
    {
        $upd_sql = "UPDATE company set lector_del_status='1' where c_no='{$c_no}'";

        if(!mysqli_query($my_db, $upd_sql)){
            $upd_msg = "강사 프로필 삭제에 실패했습니다";
        }else{
            $del_sql = "DELETE FROM lector_profile WHERE lp_no='{$lp_no}';";
            $del_sql .= "DELETE FROM lector_profile_career WHERE lp_no='{$lp_no}';";
            $del_sql .= "DELETE FROM lector_profile_etc WHERE lp_no='{$lp_no}';";
            $del_sql .= "DELETE FROM lector_profile_license WHERE lp_no='{$lp_no}';";
            
            if(!mysqli_multi_query($my_db, $del_sql)){
                $upd_msg = "강사 프로필 삭제에 실패했습니다";
            }else{
                $upd_msg = "강사 프로필을 삭제했습니다";
            }
        }
	}

    exit("<script>alert('{$upd_msg}');location.href='company_list.php?{$search_url}';</script>");
}
elseif ($proc == "modify_display")
{
    $company_list 	= (isset($_POST['select_c_no_list'])) ? $_POST['select_c_no_list'] : "";
    $display_state 	= (isset($_POST['display_state'])) ? $_POST['display_state'] : "";
    $search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if($company_list)
    {
        $chk_comp_sql 	= "SELECT * FROM company WHERE c_no IN({$company_list})";
        $chk_comp_query	= mysqli_query($my_db, $chk_comp_sql);
        $upd_comp_data 	= [];
        while($chk_comp = mysqli_fetch_array($chk_comp_query))
        {
            if($display_state != $chk_comp['display']){
                $upd_comp_data[] = array("c_no" => $chk_comp['c_no'], "display" => $display_state);
            }
        }
        
        if(!empty($upd_comp_data)){
            if($company_model->multiUpdate($upd_comp_data)){
                exit ("<script>alert('Display 변경을 완료했습니다.');location.href='company_list.php?{$search_url}';</script>");
            }else{
                exit ("<script>alert('Display 변경에 실패했습니다.');location.href='company_list.php?{$search_url}';</script>");
            }
        }
    }
    exit ("<script>alert('Display 변경에 실패했습니다.');location.href='company_list.php?{$search_url}';</script>");
}
else
{
    # Navigation & My Quick
    $nav_prd_no  = "66";
    $nav_title   = "파트너 리스트";
    $quick_model = MyQuick::Factory();
    $is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

    $smarty->assign("is_my_quick", $is_my_quick);
    $smarty->assign("nav_title", $nav_title);
    $smarty->assign("nav_prd_no", $nav_prd_no);

	#검색쿼리
	$add_where	        = "1=1";
	$sch_display 		= isset($_GET['sch_display']) ? $_GET['sch_display'] : "";
	$sch_my_c_no		= isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
	$sch_license_type 	= isset($_GET['sch_license_type']) ? $_GET['sch_license_type'] : "";
    $sch_partner_state	= isset($_GET['sch_partner_state']) ? $_GET['sch_partner_state'] : "";
	$sch_corp_kind 		= isset($_GET['sch_corp_kind']) ? $_GET['sch_corp_kind'] : "";
    $sch_c_name 		= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
    $sch_s_name 		= isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
    $sch_keyword 		= isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";
    $sch_about 			= isset($_GET['sch_about']) ? $_GET['sch_about'] : "";
    $sch_memo 			= isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
    $sch_leave 			= isset($_GET['sch_leave']) ? $_GET['sch_leave'] : "";

	if(!empty($sch_display))
	{
		$add_where .= " AND c.display='{$sch_display}'";
		$smarty->assign("sch_display", $sch_display);
	}

    if(!empty($sch_my_c_no))
    {
        $add_where .= " AND c.my_c_no='{$sch_my_c_no}'";
        $smarty->assign("sch_my_c_no", $sch_my_c_no);
    }

    if(!empty($sch_license_type))
    {
        $add_where .= " AND c.license_type='{$sch_license_type}'";
        $smarty->assign("sch_license_type", $sch_license_type);
    }

    if(!empty($sch_partner_state))
    {
        $add_where .= " AND c.partner_state='{$sch_partner_state}'";
        $smarty->assign("sch_partner_state", $sch_partner_state);
    }

    if(!empty($sch_corp_kind))
    {
        $add_where .= " AND c.corp_kind='{$sch_corp_kind}'";
        $smarty->assign("sch_corp_kind", $sch_corp_kind);
    }

    if(!empty($sch_c_name))
    {
        $add_where .= " AND c.c_name like '%{$sch_c_name}%'";
        $smarty->assign("sch_c_name", $sch_c_name);
    }

    if(!empty($sch_s_name))
    {
        $add_where .= " AND c.s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_s_name}%')";
        $smarty->assign("sch_s_name", $sch_s_name);
    }

    if(!empty($sch_keyword))
    {
        $add_where .= " AND c.keyword like '%{$sch_keyword}%'";
        $smarty->assign("sch_keyword", $sch_keyword);
    }

    if(!empty($sch_about))
    {
        $add_where .= " AND c.about like '%{$sch_about}%'";
        $smarty->assign("sch_about", $sch_about);
    }

    if(!empty($sch_memo))
    {
        $add_where .= " AND c.c_memo like '%{$sch_memo}%'";
        $smarty->assign("sch_memo", $sch_memo);
    }

	if(!empty($sch_leave))
	{
		if($sch_leave == 'lector')
		{
			$lec_date   = date('Y-m-d', strtotime("-10 years"))." 23:59:59";
            $add_where .= " AND (SELECT per.pj_participation_date FROM project_external_report per WHERE per.pj_c_no=`c`.c_no AND per.pj_participation='1' AND per.active='1' ORDER BY per.pj_participation_date DESC LIMIT 1) <= '{$lec_date}'";
		}
		elseif($sch_leave == 'leave')
		{
            $lev_date   = date('Y-m-d', strtotime("-1 years"))." 23:59:59";
            $add_where .= " AND c.leave_date <= '{$lev_date}'";
		}
	}

    // 정렬순서 토글 & 필드 지정
    $add_orderby = "`c`.c_no DESC";
    $ord_type    = (isset($_GET['ord_type']) && !empty($_GET['ord_type'])) ? $_GET['ord_type'] : "";
    $ord_type_by = (isset($_GET['ord_type_by']) && !empty($_GET['ord_type_by'])) ? $_GET['ord_type_by'] : "";

    if(!empty($ord_type) && !empty($ord_type_by))
    {
        if($ord_type_by == '1'){
            $add_orderby = "{$ord_type} ASC, `c`.c_no DESC";
        }elseif($ord_type_by == '2'){
            $add_orderby = "{$ord_type} DESC, `c`.c_no DESC";
        }else{
            $add_orderby = "`c`.c_no DESC";
        }
    }
    $smarty->assign('ord_type', $ord_type);
    $smarty->assign('ord_type_by', $ord_type_by);

    # 페이징 처리
	$company_total_sql 	    = "SELECT count(c.c_no) as cnt FROM company c WHERE {$add_where}";
    $company_total_query	= mysqli_query($my_db, $company_total_sql);
    $company_total_result	= mysqli_fetch_array($company_total_query);
	$company_total_num 	    = $company_total_result['cnt'];

    $pages 	 = isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 	 = 20;
    $offset  = ($pages-1) * $num;
	$pagenum = ceil($company_total_num/$num);

	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	$search_url = getenv("QUERY_STRING");
    $pagelist   = pagelist($pages, "company_list.php", $pagenum, $search_url);

    $smarty->assign("total_num", $company_total_num);
	$smarty->assign("search_url", $search_url);
    $smarty->assign("pagelist", $pagelist);

    $company_sql="
		SELECT
			c.c_no,
			(SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=c.my_c_no) AS my_c_name,
			(SELECT mc.kind_color FROM my_company mc WHERE mc.my_c_no=c.my_c_no) AS my_c_kind_color,
			c.s_no,
			(select s_name from staff b where s_no=c.s_no) as s_name,
			c.id,
			c.c_name,
			c.corp_kind,
			c.display,
			c.license_type,
			c.c_memo,
			c.c_memo_date,
			c.partner_state,
			c.leave_date,
			(SELECT per.pj_participation_date FROM project_external_report per WHERE per.pj_c_no=`c`.c_no AND per.pj_participation='2' AND per.active='1' ORDER BY per.pj_participation_date DESC LIMIT 1) as partner_date,
			(SELECT AVG(pere.score) FROM project_external_report_evaluation as pere WHERE pere.pj_c_no=`c`.c_no) as total_avg,
			(SELECT COUNT(per.pj_er_no) FROM project_external_report as per WHERE per.pj_c_no=`c`.c_no AND per.pj_participation='2' AND per.active='1') as total_cnt,
			(SELECT lp.lp_no FROM lector_profile lp WHERE lp.c_no=`c`.c_no AND lp.display='1' LIMIT 1) as lp_no,
			c.default_del_status,
			c.tax_del_status,
			c.lector_del_status,
			(SELECT lp.mod_date FROM lector_profile lp WHERE lp.c_no=`c`.c_no LIMIT 1) as mod_date
		FROM company c
		WHERE {$add_where}
		ORDER BY {$add_orderby}
        LIMIT {$offset}, {$num}
	";
	$company_query          = mysqli_query($my_db, $company_sql);
    $company_list           = [];
    $partner_state_option   = getPartnerStateOption();
    $my_company_model       = MyCompany::Factory();
    $my_company_option      = $my_company_model->getNameList();
	while($company_result = mysqli_fetch_array($company_query))
	{
		$company_list[] = $company_result;
	}

	$smarty->assign("display_option", getDisplayOption());
	$smarty->assign("license_type_option", getLicenseTypeOption());
    $smarty->assign("corp_kind_option", getCorpKindOption());
    $smarty->assign("partner_state_option", getPartnerStateOption());
	$smarty->assign("my_company_list", $my_company_option);
    $smarty->assign("company_list", $company_list);

	$smarty->display('company_list.html');
}
?>
