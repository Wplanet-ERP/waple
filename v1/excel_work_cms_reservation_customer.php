<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_cms.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$excel_title    = "예약판매 고객리스트";
$nowdate        = date("Y-m-d H:i:s");

# 상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "고객명")
	->setCellValue('B1', "연락처")
;

$add_where 	= "delivery_state = '1'";
$sch_option = isset($_GET['sch_option']) ? $_GET['sch_option'] : "";

if(!empty($sch_option))
{
	$chk_option_where = "";
	$chk_option_list  = [];

	if($sch_option == "240"){
		$chk_option_where = "option_no IN(240,241)";
	}elseif($sch_option == "238"){
		$chk_option_where = "option_no IN(238,239)";
	}elseif($sch_option == "475"){
		$chk_option_where = "option_no IN(473,475)";
	}elseif($sch_option == "100000"){
		$chk_option_list = array(1732,1733,1735,1736,1737,1738,1740,1742,1745,1734,1739,1741,1743,1744);
	}elseif($sch_option == "100001"){
		$chk_option_list = array(1733,1735,1738,1742,1745);
	}elseif($sch_option == "100002"){
		$chk_option_list = array(1732,1736,1737,1734,1739,1740,1741,1743,1744);
	}else{
		$chk_option_where = "option_no = '{$sch_option}'";
	}

	if(!empty($chk_option_where))
	{
		$chk_option_sql   = "SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE {$chk_option_where} AND pcr.display='1'";
		$chk_option_query = mysqli_query($my_db, $chk_option_sql);
		while($chk_option_result = mysqli_fetch_assoc($chk_option_query)){
			$chk_option_list[] = "'{$chk_option_result['prd_no']}'";
		}
	}

	if(!empty($chk_option_list)){
		$chk_option_text = implode(",", $chk_option_list);
		$add_where .= " AND w.prd_no IN({$chk_option_text})";
	}
}

$cms_reservation_sql = "
    SELECT        
        w.order_number,
        w.recipient,
        w.recipient_hp
    FROM work_cms_reservation w
    WHERE {$add_where}
    GROUP BY order_number
";
$cms_reservation_query  = mysqli_query($my_db, $cms_reservation_sql);
$idx = 2;
$lfcr = chr(10);
$send_order_list = [];
if(!!$cms_reservation_query)
{
    while($cms_reservation = mysqli_fetch_array($cms_reservation_query))
    {
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$idx, $cms_reservation['recipient'])
            ->setCellValueExplicit('B'.$idx, $cms_reservation['recipient_hp'],PHPExcel_Cell_DataType::TYPE_STRING)
        ;

        $send_order_list[$cms_reservation['order_number']] = "'{$cms_reservation['order_number']}'";
        $idx++;
    }
}
$idx--;

if(!empty($send_order_list)){
    $send_order_text = implode(",", $send_order_list);
    $upd_reservation_sql = "UPDATE work_cms_reservation SET delivery_state='2' WHERE order_number IN({$send_order_text})";
    mysqli_query($my_db, $upd_reservation_sql);
}

$objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
$objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("A2:B{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:B{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getStyle("A1:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->setTitle($excel_title);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$excel_title}.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
