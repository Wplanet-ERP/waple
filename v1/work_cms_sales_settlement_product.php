<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Custom.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');

# Navigation & My Quick
$nav_prd_no  = "215";
$nav_title   = "커머스 단품 상품별 정산률";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수(브랜드, 상품 리스트)
$company_model              = Company::Factory();
$product_model              = ProductCms::Factory();
$kind_model                 = Kind::Factory();
$dp_company_list            = $company_model->getSettleDpCompanyList();
$dp_company_text            = implode(",", array_keys($dp_company_list));
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_info_list            = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$prd_kind_code		        = "product_cms";
$prd_kind_total_data_list 	= $kind_model->getKindChartData($prd_kind_code);
$prd_kind_group_list 		= $prd_kind_total_data_list['kind_group_list'];
$prd_kind_group_code_list	= $prd_kind_total_data_list['kind_group_code'];
$prd_kind_group_code 		= implode(",", $prd_kind_group_code_list);
$prd_total_data_list		= $product_model->getPrdGroupChartData($prd_kind_group_code);
$prd_total_list			    = $prd_total_data_list['prd_only_total'];
$prd_total_info_list		= $prd_total_data_list['prd_info_list'];

$sch_brand_g1_list          = $brand_company_g1_list;
$sch_prd_g1_list            = [];
$prd_g2_total_list          = [];

foreach($prd_kind_group_list as $key => $prd_data)
{
    if(!$key){
        $sch_prd_g1_list = $prd_data;
    }else{
        $prd_g2_total_list[$key] = $prd_data;
    }
}

# 사용 리스트
$chk_brand_list         = [];
$chk_dp_list            = [];
$chk_brand_sum_list     = [];
$chk_brand_dp_sum_list  = [];
$work_cms_settle_list   = [];

# 검색조건
$add_cms_where      = "w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN({$dp_company_text}) AND p.prd_type != '2'";
$cms_group_where    = "w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN({$dp_company_text})";
$settle_group_where = "1=1";
$sch_ord_s_date     = isset($_GET['sch_ord_s_date']) ? $_GET['sch_ord_s_date'] : "";
$sch_ord_e_date     = isset($_GET['sch_ord_e_date']) ? $_GET['sch_ord_e_date'] : "";
$sch_ord_date_type  = isset($_GET['sch_ord_date_type']) ? $_GET['sch_ord_date_type'] : "month";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_prd_g1	        = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	        = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	        = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_prd_name       = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_dp_company     = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "1812";
$sch_is_settle      = isset($_GET['sch_is_settle']) ? $_GET['sch_is_settle'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(empty($sch_ord_s_date)){
    $sch_ord_s_date = date("Y-m-d", strtotime("-2 months"));
}

if(empty($sch_ord_e_date)){
    $sch_ord_e_date = date("Y-m-d", strtotime("{$sch_ord_s_date} +1 months"));
}

# 브랜드 검색
$sch_brand_g2_list  = isset($brand_company_g2_list[$sch_brand_g1]) ? $brand_company_g2_list[$sch_brand_g1] : [];
$sch_brand_list     = isset($brand_list[$sch_brand_g2]) ? $brand_list[$sch_brand_g2] : [];

if(!empty($sch_brand)) {
    $add_cms_where .= " AND `w`.c_no = '{$sch_brand}'";
    $chk_brand_list[$sch_brand] = $brand_info_list[$sch_brand];
}
elseif(!empty($sch_brand_g2)) {
    $add_cms_where .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";

    foreach($sch_brand_list as $chk_brand => $chk_brand_data)
    {
        if($chk_brand != '5513' && $chk_brand != '5514' && $chk_brand != '5979' && $chk_brand != '6044'){
            $chk_brand_list[$chk_brand] = $brand_info_list[$sch_brand];
        }
    }
}
elseif(!empty($sch_brand_g1)) {
    $add_cms_where .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";

    foreach($sch_brand_g2_list as $chk_brand_g2 => $chk_brand_g2_data)
    {
        foreach($brand_list[$chk_brand_g2] as $chk_brand => $chk_brand_data)
        {
            if($chk_brand != '5513' && $chk_brand != '5514' && $chk_brand != '5979' && $chk_brand != '6044'){
                $chk_brand_list[$chk_brand] = $brand_info_list[$sch_brand];
            }
        }
    }
}else{
    $chk_brand_list = $brand_info_list;
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $sch_brand_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 상품 검색
$sch_prd_g2_list    = isset($prd_g2_total_list[$sch_prd_g1]) ? $prd_g2_total_list[$sch_prd_g1] : [];
$sch_prd_list       = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

if (!empty($sch_prd) && $sch_prd != "0") {
    $add_cms_where .= " AND w.prd_no='{$sch_prd}'";
}elseif($sch_prd_g2){
    $add_cms_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code='{$sch_prd_g2}')";
}elseif($sch_prd_g1){
    $add_cms_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
}
$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);
$smarty->assign("sch_prd_g1_list", $sch_prd_g1_list);
$smarty->assign("sch_prd_g2_list", $sch_prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);

if(!empty($sch_ord_s_date) && !empty($sch_ord_e_date))
{
    $sch_ord_s_datetime = $sch_ord_s_date." 00:00:00";
    $sch_ord_e_datetime = $sch_ord_e_date." 23:59:59";

    $add_cms_where     .= " AND w.order_date BETWEEN '{$sch_ord_s_datetime}' AND '{$sch_ord_e_datetime}'";
    $cms_group_where   .= " AND w.order_date BETWEEN '{$sch_ord_s_datetime}' AND '{$sch_ord_e_datetime}'";
}
$smarty->assign("sch_ord_s_date", $sch_ord_s_date);
$smarty->assign("sch_ord_e_date", $sch_ord_e_date);
$smarty->assign("sch_ord_date_type", $sch_ord_date_type);

if(!empty($sch_prd_name)){
    $add_cms_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.title LIKE '%{$sch_prd_name}%')";
    $smarty->assign("sch_prd_name", $sch_prd_name);
}

if(!empty($sch_dp_company)) {
    $chk_dp_list[$sch_dp_company] = $dp_company_list[$sch_dp_company];
    $add_cms_where      .= " AND `w`.dp_c_no='{$sch_dp_company}'";
    $cms_group_where    .= " AND `w`.dp_c_no='{$sch_dp_company}'";
    $smarty->assign("sch_dp_company", $sch_dp_company);
}else{
    $chk_dp_list = $dp_company_list;
}

if(!empty($sch_is_settle)) {

    if($sch_is_settle == "1"){
        $settle_group_where = "rs.settle_cnt > 0";
    }elseif($sch_is_settle == "2"){
        $settle_group_where = "rs.settle_cnt = 0";
    }
    $smarty->assign("sch_is_settle", $sch_is_settle);
}

foreach($chk_brand_list as $chk_brand => $chk_brand_data)
{
    $chk_brand_sum_list[$chk_brand] = 0;
    foreach($chk_dp_list as $chk_dp => $chk_dp_label)
    {
        $chk_brand_dp_sum_list[$chk_brand][$chk_dp] = 0;
    }
}

# 검색쿼리
$work_cms_settle_sql = "
    SELECT
        rs.c_no,
        rs.dp_c_no,
        (SELECT c.priority FROM company c WHERE c.c_no=rs.dp_c_no) AS dp_priority,
        rs.prd_no,
        rs.prd_name,
        (SELECT k.priority FROM kind k WHERE k.k_name_code=rs.k_name_code) as priority,
        SUM(rs.quantity) AS total_qty,
        SUM(rs.unit_price) AS total_sales,
        SUM(rs.settle_price) AS total_settle,
        SUM(rs.unit_delivery) AS total_delivery,
        ROUND(AVG(rs.unit_sales_price/rs.quantity)) AS avg_sales,
        ROUND(AVG(rs.settle_price/rs.quantity)) AS avg_settle,
        (SUM(rs.settle_price)/SUM(rs.unit_sales_price)) AS avg_per,
        MAX(rs.unit_sales_price/rs.quantity) AS max_sales,
        MIN(rs.unit_sales_price/rs.quantity) AS min_sales,
        MAX(rs.settle_price/rs.quantity) AS max_settle,
        MIN(rs.settle_price/rs.quantity) AS min_settle
    FROM
    (    
        SELECT
            w.order_number,
            w.c_no,
            w.dp_c_no,
            w.prd_no,
            p.title AS prd_name,
            p.k_name_code,
            quantity,
            unit_price,
            unit_delivery_price AS unit_delivery,
            (unit_price+unit_delivery_price) AS unit_sales_price,
            (SELECT SUM(wcs.settle_price_vat) FROM work_cms_settlement wcs WHERE wcs.order_number=w.order_number) AS settle_price,
            (SELECT COUNT(*) FROM work_cms_settlement wcs WHERE wcs.order_number=w.order_number) AS settle_cnt
        FROM work_cms w
        LEFT JOIN product_cms p ON p.prd_no=w.prd_no
        WHERE {$add_cms_where} AND w.order_number IN(
            SELECT 
                DISTINCT order_number 
            FROM work_cms w
            WHERE {$cms_group_where}
            GROUP BY order_number HAVING COUNT(*) = 1
        ) AND order_number NOT IN(SELECT DISTINCT wcs.order_number FROM work_cms_settlement wcs WHERE wcs.order_number=w.order_number AND wcs.settle_price_vat < 0) 
        GROUP BY order_number
    ) AS rs
    WHERE {$settle_group_where}
    GROUP BY c_no, dp_c_no, prd_no
    ORDER BY priority ASC, prd_name ASC
";
$work_cms_settle_query = mysqli_query($my_db, $work_cms_settle_sql);
while($work_cms_settle = mysqli_fetch_assoc($work_cms_settle_query))
{
    $chk_brand_sum_list[$work_cms_settle['c_no']] += $work_cms_settle['total_sales'];
    $chk_brand_dp_sum_list[$work_cms_settle['c_no']][$work_cms_settle['dp_c_no']] += $work_cms_settle['total_sales'];

    $work_cms_settle['prd_g_name']      = $prd_total_info_list[$work_cms_settle['prd_no']]['group_name'];
    $work_cms_settle['prd_name']        = $prd_total_info_list[$work_cms_settle['prd_no']]['title'];
    $work_cms_settle['total_avg_per']   = $work_cms_settle['avg_per']*100;

    $work_cms_settle_list[$work_cms_settle['c_no']][$work_cms_settle['dp_c_no']][$work_cms_settle['prd_no']] = $work_cms_settle;
}

$table_brand_list   = [];
$table_dp_list      = [];
foreach($chk_brand_sum_list as $chk_brand => $chk_brand_price)
{
    if($chk_brand_price == 0){
        continue;
    }

    $table_brand_list[$chk_brand] = array(
        "brand_name"    => $brand_info_list[$chk_brand]['c_name'],
        "brand_total"   => 0,
    );

    foreach($chk_brand_dp_sum_list[$chk_brand] as $chk_dp_no => $chk_dp_price)
    {
        if($chk_dp_price == 0){
            continue;
        }

        $prd_count = count($work_cms_settle_list[$chk_brand][$chk_dp_no]);
        $table_brand_list[$chk_brand]["brand_total"] += $prd_count;
        $table_dp_list[$chk_brand][$chk_dp_no] = array(
            "dp_name"   => $dp_company_list[$chk_dp_no],
            "dp_total"  => $prd_count,
        );
    }
}

$smarty->assign("is_settle_type_option", getIsSettleTypeOption());
$smarty->assign("sch_dp_company_list", $dp_company_list);
$smarty->assign("table_brand_list", $table_brand_list);
$smarty->assign("table_dp_list", $table_dp_list);
$smarty->assign("work_cms_settle_list", $work_cms_settle_list);

$smarty->display('work_cms_sales_settlement_product.html');
?>
