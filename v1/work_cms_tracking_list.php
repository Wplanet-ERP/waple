<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');

# Navigation & My Quick
$nav_prd_no  = "45";
$nav_title   = "배송비 내역 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 브랜드 정리
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

#검색 처리
$add_where       = "1=1";
$add_where_group = "1=1";

$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where      .= " AND `wct`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where      .= " AND `wct`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

    $add_where      .= " AND `wct`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

$billing_cur_date       = date('Y-m', strtotime('-1 month'));
$sch_billing_date       = isset($_GET['sch_billing_date']) ? $_GET['sch_billing_date'] : date('Y-m', strtotime('-1 month'));
$sch_delivery_no 	    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
$sch_track_type 	    = isset($_GET['sch_track_type']) ? $_GET['sch_track_type'] : "";
$sch_ord_bundle 	    = isset($_GET['sch_ord_bundle']) ? $_GET['sch_ord_bundle'] : "1";
$sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_order_number_null  = isset($_GET['sch_order_number_null']) ? $_GET['sch_order_number_null'] : "";
$sch_sender 		    = isset($_GET['sch_sender']) ? $_GET['sch_sender'] : "";
$sch_recipient 		    = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_deducted_fee 	    = isset($_GET['sch_deducted_fee']) ? $_GET['sch_deducted_fee'] : "";
$sch_deducted_reason    = isset($_GET['sch_deducted_reason']) ? $_GET['sch_deducted_reason'] : "";
$sch_pickup_date       	= isset($_GET['sch_pickup_date']) ? $_GET['sch_pickup_date'] : "";
$sch_delivery_date    	= isset($_GET['sch_delivery_date']) ? $_GET['sch_delivery_date'] : "";
$sch_is_prd_fee 	    = isset($_GET['sch_is_prd_fee']) ? $_GET['sch_is_prd_fee'] : "";
$sch_is_duplicate 	    = isset($_GET['sch_is_duplicate']) ? $_GET['sch_is_duplicate'] : "";

if(!empty($sch_ord_bundle))
{
    $smarty->assign("sch_ord_bundle", $sch_ord_bundle);
}

if(!empty($sch_billing_date))
{
    $add_where       .= " AND wct.billing_date='{$sch_billing_date}'";
    $add_where_group .= " AND wct.billing_date='{$sch_billing_date}'";
    $smarty->assign('sch_billing_date', $sch_billing_date);
}

if(!empty($sch_delivery_no)){
    $add_where .= " AND wct.delivery_no = '{$sch_delivery_no}'";
    $smarty->assign('sch_delivery_no', $sch_delivery_no);
}

if(!empty($sch_track_type)){
    $add_where .= " AND wct.track_type = '{$sch_track_type}'";
    $smarty->assign('sch_track_type', $sch_track_type);
}

if(!empty($sch_order_number_null))
{
    $add_where .= " AND (wct.order_number IS NULL OR wct.order_number = '')";
    $smarty->assign('sch_order_number_null', $sch_order_number_null);
}else{
    if(!empty($sch_order_number)){
        $add_where .= " AND wct.order_number = '{$sch_order_number}'";
        $smarty->assign('sch_order_number', $sch_order_number);
    }
}

if(!empty($sch_sender)){
    $add_where .= " AND wct.sender like '%{$sch_sender}%'";
    $smarty->assign('sch_sender', $sch_sender);
}

if(!empty($sch_recipient)){
    $add_where .= " AND wct.recipient like '%{$sch_recipient}%'";
    $smarty->assign('sch_recipient', $sch_recipient);
}

if(!empty($sch_deducted_fee)){
    if($sch_deducted_fee == '1'){
        $add_where .= " AND wct.deducted_fee > 0";
    }else{
        $add_where .= " AND wct.deducted_fee <= 0";
    }
    $smarty->assign('sch_deducted_fee', $sch_deducted_fee);
}

if(!empty($sch_deducted_reason)){
    $add_where .= " AND wct.deducted_reason like '%{$sch_deducted_reason}%'";
    $smarty->assign('sch_deducted_reason', $sch_deducted_reason);
}

if(!empty($sch_pickup_date)){
    $add_where .= " AND wct.pickup_date = '{$sch_pickup_date}'";
    $smarty->assign('sch_pickup_date', $sch_pickup_date);
}

if(!empty($sch_delivery_date)){
    $add_where .= " AND wct.delivery_date = '{$sch_delivery_date}'";
    $smarty->assign('sch_delivery_date', $sch_delivery_date);
}

$add_having = "";
if(!empty($sch_is_prd_fee))
{
    if($sch_ord_bundle == '1'){
        $add_having = "HAVING SUM(prd_fee) = 0";
    }else{
        $add_where .= " AND wct.prd_fee = 0";
    }
    $smarty->assign('sch_is_prd_fee', $sch_is_prd_fee);
}

if(!empty($sch_is_duplicate))
{
    $chk_dup_list   = [];
    $chk_dup_sql    = "SELECT DISTINCT delivery_no FROM work_cms_tracking WHERE billing_date ='{$sch_billing_date}' GROUP BY delivery_no HAVING COUNT(DISTINCT track_type) > 1";
    $chk_dup_query  = mysqli_query($my_db, $chk_dup_sql);
    while($chk_dup_result = mysqli_fetch_assoc($chk_dup_query))
    {
        $chk_dup_list[] = "'{$chk_dup_result['delivery_no']}'";
    }

    if(!empty($chk_dup_list)) {
        $chk_dup_text = implode(",", $chk_dup_list);
        $add_where = "billing_date='{$sch_billing_date}' AND delivery_no IN({$chk_dup_text})";
    }else{
        $add_where = "1!=1 AND billing_date='{$sch_billing_date}'";
    }

    $smarty->assign('sch_is_duplicate', $sch_is_duplicate);
}

if($sch_ord_bundle == '1') {
    $tracking_total_sql = "SELECT count(*) as cnt FROM (SELECT DISTINCT wct.delivery_no FROM work_cms_tracking wct WHERE {$add_where} GROUP BY wct.delivery_no {$add_having}) AS cnt";
}else{
    $tracking_total_sql = "SELECT count(*) as cnt FROM (SELECT wct.t_no FROM work_cms_tracking wct WHERE {$add_where}) AS cnt";
}

$tracking_total_query	= mysqli_query($my_db, $tracking_total_sql);
$tracking_total_result  = mysqli_fetch_array($tracking_total_query);
$tracking_total         = isset($tracking_total_result['cnt']) ? $tracking_total_result['cnt'] : 0;

//페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($tracking_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist	= pagelist($pages, "work_cms_tracking_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $tracking_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);
$smarty->assign("page_type_option", getPageTypeOption(3));

if($sch_ord_bundle == '1')
{
    $group_delivery_sql     = "SELECT DISTINCT delivery_no FROM work_cms_tracking as wct WHERE {$add_where} GROUP BY wct.delivery_no {$add_having} ORDER BY wct.t_no DESC LIMIT {$offset},{$num}";
    $group_delivery_query   = mysqli_query($my_db, $group_delivery_sql);
    $group_delivery_list    = [];
    while($group_delivery = mysqli_fetch_assoc($group_delivery_query))
    {
        $group_delivery_list[] = "'{$group_delivery['delivery_no']}'";
    }

    $delivery_nos     = implode(',', $group_delivery_list);
    $add_where_group .= " AND wct.delivery_no IN({$delivery_nos})";

    # 리스트 쿼리
    $tracking_sql = "
        SELECT
            *,
            `wct`.billing_date,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no))) AS k_prd1_name,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no)) AS k_prd2_name,
            (SELECT title from product_cms prd_cms where prd_cms.prd_no=wct.prd_no) as prd_name
        FROM work_cms_tracking wct
        WHERE {$add_where_group}
        ORDER BY wct.t_no DESC
    ";
}
else
{
    # 리스트 쿼리
    $tracking_sql = "
        SELECT
            *,
            `wct`.billing_date,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no))) AS k_prd1_name,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no)) AS k_prd2_name,
            (SELECT title from product_cms prd_cms where prd_cms.prd_no=wct.prd_no) as prd_name
        FROM work_cms_tracking wct
        WHERE {$add_where}
        ORDER BY wct.t_no DESC
        LIMIT {$offset},{$num}
    ";
}

$tracking_query     = mysqli_query($my_db, $tracking_sql);
$tracking_list      = [];
$tracking_tmp_list  = [];
$track_type_option  = getTrackTypeOption();
$track_fee_option   = getTrackFeeOption();
if(!!$tracking_query)
{
    while($tracking_data = mysqli_fetch_assoc($tracking_query))
    {
        $tracking_data['track_type_name'] = isset($track_type_option[$tracking_data['track_type']]) ? $track_type_option[$tracking_data['track_type']] : "";
        $tracking_data['delivery_date']   = ($tracking_data['delivery_date'] == '0000-00-00') ? "" : $tracking_data['delivery_date'];
        $tracking_data['pickup_date']     = ($tracking_data['pickup_date'] == '0000-00-00') ? "" : $tracking_data['pickup_date'];
        $tracking_list[$tracking_data['delivery_no']][] = $tracking_data;
    }
}

$smarty->assign("billing_cur_date", $billing_cur_date);
$smarty->assign("track_type_option", $track_type_option);
$smarty->assign("track_fee_option", $track_fee_option);
$smarty->assign("tracking_list", $tracking_list);

# 배송비 통계요약 확인건들
$tracking_stat_sql = "
    SELECT
        wct.delivery_no,
        wct.track_type,
        wct.c_no,
        wct.c_name,
        wct.basic_fee,
        wct.ferry_fee,
        wct.jeju_fee,
        wct.etc_fee,
        wct.total_fee,
        wct.deducted_fee,
        SUM(wct.prd_fee) as sum_prd_fee,
        IF(wct.track_type='1',(SELECT COUNT(DISTINCT sub.c_no) FROM work_cms_tracking AS sub WHERE sub.delivery_no=wct.delivery_no AND sub.prd_price > 0 AND sub.c_no > 0),(SELECT COUNT(DISTINCT sub.c_no) FROM work_cms_tracking AS sub WHERE sub.delivery_no=wct.delivery_no AND sub.c_no > 0)) AS brand_cnt,
        `wct`.billing_date
    FROM work_cms_tracking wct
    WHERE {$add_where}
    GROUP BY wct.delivery_no
    ORDER BY wct.prd_price DESC
";
$tracking_stat_query    = mysqli_query($my_db, $tracking_stat_sql);
$tracking_stat_tmp_list = [];
while($tracking_stat_result = mysqli_fetch_assoc($tracking_stat_query))
{
    $brand      = $tracking_stat_result['c_no'];
    $brand_name = $tracking_stat_result['c_name'];

    if(empty($brand)) {
        $brand = "0";
        $brand_name = "-";
    }elseif($tracking_stat_result['brand_cnt'] > 1){
        $brand      = "9999";
        $brand_name = "(혼합)";
    }

    $tracking_stat_tmp_list[$brand]['c_name']         = $brand_name;
    $tracking_stat_tmp_list[$brand]['billing_date']   = $tracking_stat_result['billing_date'];
    $tracking_stat_tmp_list[$brand]['basic_fee']     += $tracking_stat_result['basic_fee'];
    $tracking_stat_tmp_list[$brand]['ferry_fee']     += $tracking_stat_result['ferry_fee'];
    $tracking_stat_tmp_list[$brand]['jeju_fee']      += $tracking_stat_result['jeju_fee'];
    $tracking_stat_tmp_list[$brand]['etc_fee']       += $tracking_stat_result['etc_fee'];
    $tracking_stat_tmp_list[$brand]['total_fee']     += $tracking_stat_result['total_fee'];
    $tracking_stat_tmp_list[$brand]['deducted_fee']  += $tracking_stat_result['deducted_fee'];
    $tracking_stat_tmp_list[$brand]['prd_fee']       += $tracking_stat_result['sum_prd_fee'];

    if($tracking_stat_result['track_type'] == '1'){
        $tracking_stat_tmp_list[$brand]['normal_cnt']++;
    }elseif($tracking_stat_result['track_type'] == '2'){
        $tracking_stat_tmp_list[$brand]['return_cnt']++;
    }

    $tracking_stat_tmp_list[$brand]['total_cnt']++;
}

$tracking_prd_fee_sql = "
    SELECT
        wct.c_no,
        SUM(wct.prd_fee) as total_prd_fee
    FROM work_cms_tracking wct
    WHERE {$add_where}
    GROUP BY wct.c_no
";
$tracking_prd_fee_query = mysqli_query($my_db, $tracking_prd_fee_sql);
$tracking_prd_fee_list  = [];
$tracking_prd_fee_total = 0;
while($tracking_prd_fee_result = mysqli_fetch_assoc($tracking_prd_fee_query))
{
    $tracking_prd_fee_list[$tracking_prd_fee_result['c_no']] += $tracking_prd_fee_result['total_prd_fee'];

    $tracking_prd_fee_total += $tracking_prd_fee_result['total_prd_fee'];
}
$smarty->assign("tracking_prd_fee_total", $tracking_prd_fee_total);
$smarty->assign("tracking_prd_fee_list", $tracking_prd_fee_list);

$stat_row           = 0;
$stat_confirm_row   = 0;
$stat_confirm_total = 0;
$track_stat_sort_list  = [];
$track_stat_total_list = [];
$tracking_stat_list    = [];
if(!empty($tracking_stat_tmp_list))
{
    foreach($tracking_stat_tmp_list as $brand => $stat_tmp)
    {
        $final_price = $stat_tmp['total_fee'] - $stat_tmp['deducted_fee'];
        if($brand != '0'){
            $track_stat_sort_list[$brand] = $final_price;
            $stat_confirm_total += $stat_tmp['total_cnt'];
            $stat_confirm_row++;
        }

        $track_stat_total_list['total_cnt']     += $stat_tmp['total_cnt'];
        $track_stat_total_list['normal_cnt']    += $stat_tmp['normal_cnt'];
        $track_stat_total_list['return_cnt']    += $stat_tmp['return_cnt'];
        $track_stat_total_list['basic_fee']     += $stat_tmp['basic_fee'];
        $track_stat_total_list['ferry_fee']     += $stat_tmp['ferry_fee'];
        $track_stat_total_list['jeju_fee']      += $stat_tmp['jeju_fee'];
        $track_stat_total_list['etc_fee']       += $stat_tmp['etc_fee'];
        $track_stat_total_list['total_fee']     += $stat_tmp['total_fee'];
        $track_stat_total_list['deducted_fee']  += $stat_tmp['deducted_fee'];
        $track_stat_total_list['prd_fee']       += $stat_tmp['prd_fee'];
        $track_stat_total_list['final_price']   += $final_price;
        $track_stat_total_list['media_price']   += $final_price-$tracking_prd_fee_list[$brand];

        $tracking_stat_tmp_list[$brand]['final_price'] = $final_price;
        $stat_row++;
    }

    $stat_row++;

    if(!empty($track_stat_sort_list))
    {
        arsort($track_stat_sort_list);
        foreach($track_stat_sort_list as $brand => $total)
        {
            $tracking_stat_list[$brand] = $tracking_stat_tmp_list[$brand];
        }
    }

    if(isset($tracking_stat_tmp_list['0']))
    {
        $tracking_stat_list['0'] = $tracking_stat_tmp_list['0'];
    }
}

$smarty->assign("brand_option", $brand_option);
$smarty->assign("stat_row", $stat_row);
$smarty->assign("stat_confirm_row", $stat_confirm_row);
$smarty->assign("stat_confirm_total", $stat_confirm_total);
$smarty->assign("tracking_stat_list", $tracking_stat_list);
$smarty->assign("track_stat_total_list", $track_stat_total_list);

$smarty->display('work_cms_tracking_list.html');

?>
