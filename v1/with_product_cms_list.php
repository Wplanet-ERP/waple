<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/product_cms.php');
require('inc/model/Kind.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where          = "1=1";
$sch_display        = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$sch_prd_g1         = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2         = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd_name       = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_c_name         = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_prd_code       = isset($_GET['sch_prd_code']) ? $_GET['sch_prd_code'] : "";

if(!empty($sch_display)) {
    $add_where  .= " AND display = '{$sch_display}'";
    $smarty->assign("sch_display", $sch_display);
}

if(!empty($sch_prd_g1)) {
    $add_where  .= " AND k_prd1 = '{$sch_prd_g1}'";
    $smarty->assign("sch_prd_g1", $sch_prd_g1);
}

if(!empty($sch_prd_g2)) {
    $add_where  .= " AND k_prd2 = '{$sch_prd_g2}'";
    $smarty->assign("sch_prd_g2", $sch_prd_g2);
}

if(!empty($sch_prd_name)) {
    $add_where  .= " AND title like '%{$sch_prd_name}%'";
    $smarty->assign("sch_prd_name", $sch_prd_name);
}

if(!empty($sch_manager)) {
    $add_where  .= " AND manager in (SELECT s.s_no FROM staff s where s.s_name like '%{$sch_manager}%')";
    $smarty->assign("sch_manager", $sch_manager);
}

if(!empty($sch_c_name)) {
    $add_where  .= " AND c_no in (SELECT c.c_no FROM company c where c.c_name like '%{$sch_c_name}%')";
    $smarty->assign("sch_c_name", $sch_c_name);
}

if(!empty($sch_prd_code)) {
    $add_where  .= " AND prd_code = '{$sch_prd_code}'";
    $smarty->assign("sch_prd_code", $sch_prd_code);
}


$kind_model     = Kind::Factory();
$cms_code       = "product_cms";
$cms_group_list = $kind_model->getKindGroupList($cms_code);
$prd_g1_list = $prd_g2_list = $sch_prd_g2_list = [];

foreach($cms_group_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}

if(!empty($sch_prd_g2) || !empty($sch_prd_g1))
{
    $sch_prd_g2_list = $prd_g2_list[$sch_prd_g1];
}

$smarty->assign("sch_prd_g1_list", $prd_g1_list);
$smarty->assign("sch_prd_g2_list", $sch_prd_g2_list);

# 정렬순서 토글 & 필드 지정
$add_orderby = "prd_no DESC";
$ord_type_by = (isset($_GET['ord_type_by']) && !empty($_GET['ord_type_by'])) ? $_GET['ord_type_by'] : "";
if(!empty($ord_type_by))
{
    if($ord_type_by == '1'){
        $add_orderby = "order_date ASC, prd_no DESC";
    }elseif($ord_type_by == '2'){
        $add_orderby = "order_date DESC, prd_no DESC";
    }else{
        $add_orderby = "prd_no DESC";
    }
    $smarty->assign('ord_type_by', $ord_type_by);
}

$product_cms_sql = "
    SELECT * FROM (
        SELECT
            (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd_cms.k_name_code)) AS k_prd1,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd_cms.k_name_code)) AS k_prd1_name,
            (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=prd_cms.k_name_code) AS k_prd2,
            (SELECT k_name FROM kind k WHERE k.k_name_code=prd_cms.k_name_code) AS k_prd2_name,
            (SELECT s_name FROM staff s WHERE s.s_no=prd_cms.manager) AS manager_name,
            (SELECT team_name FROM team t WHERE t.team_code=prd_cms.manager_team) AS t_name,
            (SELECT c_name FROM company c WHERE c.c_no=prd_cms.c_no) AS c_name,
            (SELECT s_name FROM staff s WHERE s.s_no=prd_cms.writer) AS writer_name,
            prd_cms.prd_no,
            prd_cms.display,
            prd_cms.c_no,
            prd_cms.title,
            prd_cms.manager,
            prd_cms.description,
            prd_cms.priority,
            prd_cms.prd_code,
            prd_cms.order_date,
            prd_cms.writer_date,
            prd_cms.combined_pack,
            prd_cms.combined_except,
            prd_cms.combined_product,
            (SELECT sub.q_no FROM quick_search as sub WHERE sub.`type`='cms' AND sub.s_no='{$session_s_no}' AND sub.prd_no=prd_cms.prd_no) AS quick_prd
        FROM product_cms prd_cms) AS sql_result
    WHERE {$add_where}
    ORDER BY {$add_orderby}
";

# 전체 게시물 수 및 페이징처리
$product_cms_total_sql      = "SELECT count(prd_no) as cnt FROM ({$product_cms_sql}) as cnt";
$product_cms_total_query    = mysqli_query($my_db, $product_cms_total_sql);
$product_cms_total_result   = mysqli_fetch_array($product_cms_total_query);
$product_cms_total          = $product_cms_total_result['cnt'];

$pages      = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num        = 20;
$offset     = ($pages-1) * $num;
$pagenum    = ceil($product_cms_total/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "with_product_cms_list.php", $pagenum, $search_url);

$smarty->assign("search_url",$search_url);
$smarty->assign("total_num", $product_cms_total);
$smarty->assign("pagelist", $pagelist);

# 리스트 쿼리
$product_cms_sql   .= "LIMIT {$offset}, {$num}";
$product_cms_query  = mysqli_query($my_db, $product_cms_sql);
$product_cms_list   = [];
$display_option     = getDisplayOption();
while($product_cms = mysqli_fetch_array($product_cms_query))
{
    if(!empty($product_cms['writer_date'])) {
        $writer_day_value = date("Y/m/d",strtotime($product_cms['writer_date']));
        $writer_time_value = date("H:i",strtotime($product_cms['writer_date']));
    }else{
        $writer_day_value = '';
        $writer_time_value = '';
    }

    $prd_unit_list  = [];
    $prd_unit_sql   = "SELECT CONCAT(pcu.option_name,'(',pcr.quantity,')') as prd_unit_name, pcr.prd_no FROM product_cms_relation pcr LEFT JOIN product_cms_unit pcu ON pcu.no = pcr.option_no  WHERE pcr.prd_no ='{$product_cms['prd_no']}' AND pcr.display = 1 ORDER BY pcr.priority ASC";
    $prd_unit_query = mysqli_query($my_db, $prd_unit_sql);
    while($prd_unit_result = mysqli_fetch_assoc($prd_unit_query))
    {
        $prd_unit_list[] = $prd_unit_result['prd_unit_name'];
    }

    $product_cms_list[] = array(
        "prd_no"        => $product_cms['prd_no'],
        "display"       => $display_option[$product_cms['display']],
        "k_prd1_name"   => $product_cms['k_prd1_name'],
        "k_prd2_name"   => $product_cms['k_prd2_name'],
        "priority"      => $product_cms['priority'],
        "title"         => $product_cms['title'],
        "manager"       => $product_cms['manager_name'],
        "t_name"        => $product_cms['t_name'],
        "c_name"        => $product_cms['c_name'],
        "description"   => $product_cms['description'],
        "prd_code"      => $product_cms['prd_code'],
        "order_date"    => $product_cms['order_date'],
        "writer_name"   => $product_cms['writer_name'],
        "writer_day"    => $writer_day_value,
        "writer_time"   => $writer_time_value,
        "quick_prd"     => $product_cms['quick_prd'],
        "prd_unit_list"  => $prd_unit_list
    );

    $prd_unit[] = $product_cms['prd_no'];
}

$smarty->assign("display_option", $display_option);
$smarty->assign("product_cms_list", $product_cms_list);

$smarty->display('with_product_cms_list.html');
?>
