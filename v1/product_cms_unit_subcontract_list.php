<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/product_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/ProductCmsUnit.php');

# 구성품 모델 Init
$unit_model 		= ProductCmsUnit::Factory();
$unit_subs_model 	= ProductCmsUnit::Factory();
$unit_subs_model->setMainInit("product_cms_unit_subcontract", "no");

# 프로세스 처리
$process = (isset($_POST['process'])) ? $_POST['process'] : "";

if($process == "add_unit_subcontract")
{
	$no				= (isset($_POST['no'])) ? $_POST['no'] : "";
	$f_sup_company 	= (isset($_POST['f_sup_company_'.$no])) ? $_POST['f_sup_company_'.$no] : "";
	$f_sub_option 	= (isset($_POST['f_sub_option_'.$no])) ? $_POST['f_sub_option_'.$no] : "";
	$f_sub_type 	= (isset($_POST['f_sub_type_'.$no])) ? $_POST['f_sub_type_'.$no] : "";
	$f_qty 			= (isset($_POST['f_qty_'.$no])) ? $_POST['f_qty_'.$no] : "";
	$search_url		= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$multi_ins_data	= [];

	foreach($f_sup_company as $key => $sup_company)
	{
		$sub_unit_no	= $f_sub_option[$key];
		$sub_unit_item 	= $unit_model->getBaseItem($sub_unit_no);

		$multi_ins_data[] = array(
			"parent_unit"	=> $no,
			"sup_c_no"		=> $sup_company,
			"type"			=> $f_sub_type[$key],
			"sub_unit"		=> $sub_unit_no,
			"sub_unit_name" => $sub_unit_item['option_name'],
			"qty"			=> $f_qty[$key]
		);
	}


	if($unit_subs_model->multiInsert($multi_ins_data)){
        exit("<script>alert('사급 품목을 추가 하였습니다');location.href='product_cms_unit_subcontract_list.php?{$search_url}';</script>");
	} else {
        exit("<script>alert('사급 품목 추가에 실패 하였습니다');history.back();</script>");
	}
}
else
{
	# Navigation & My Quick
	$nav_prd_no  = "178";
	$nav_title   = "커머스 구성품목(사급)";
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_title", $nav_title);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	# 검색쿼리
	$add_where 			= " 1=1 AND `type`='1'";
	$sch_no 		 	= isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
    $sch_brand_name  	= isset($_GET['sch_brand_name']) ? $_GET['sch_brand_name'] : "";
	$sch_sup_c_no 	 	= isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
	$sch_option_name 	= isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
	$sch_is_subcontract	= isset($_GET['sch_is_subcontract']) ? $_GET['sch_is_subcontract'] : "";
	$sch_sub_sup_c_no	= isset($_GET['sch_sub_sup_c_no']) ? $_GET['sch_sub_sup_c_no'] : "";
	$sch_sub_option_name= isset($_GET['sch_sub_option_name']) ? $_GET['sch_sub_option_name'] : "";

    if (!empty($sch_no)) {
        $add_where .= " AND pcu.no = '{$sch_no}'";
        $smarty->assign("sch_no", $sch_no);
    }

	if (!empty($sch_brand_name)) {
		$add_where .= " AND pcu.brand IN (SELECT c.c_no FROM company c WHERE c.c_name LIKE '%{$sch_brand_name}%')";
		$smarty->assign("sch_brand_name", $sch_brand_name);
	}

	if (!empty($sch_sup_c_no)) {
		$add_where .= " AND pcu.sup_c_no = '{$sch_sup_c_no}'";
		$smarty->assign("sch_sup_c_no", $sch_sup_c_no);
	}

	if (!empty($sch_option_name)) {
		$add_where .= " AND pcu.option_name LIKE '%{$sch_option_name}%'";
		$smarty->assign("sch_option_name", $sch_option_name);
	}

	if (!empty($sch_is_subcontract)) {
		if($sch_is_subcontract == '1'){
			$add_where .= " AND (SELECT COUNT(pcus.`no`) FROM product_cms_unit_subcontract pcus WHERE pcus.parent_unit=pcu.`no`) > 0";
		}elseif($sch_is_subcontract == '2'){
			$add_where .= " AND (SELECT COUNT(pcus.`no`) FROM product_cms_unit_subcontract pcus WHERE pcus.parent_unit=pcu.`no`) = 0";
		}

		$smarty->assign("sch_is_subcontract", $sch_is_subcontract);
	}

	if (!empty($sch_sub_sup_c_no)) {
		$add_where .= " AND pcu.`no` IN (SELECT pcus.parent_unit FROM product_cms_unit_subcontract pcus WHERE pcus.sup_c_no='{$sch_sub_sup_c_no}')";
		$smarty->assign("sch_sub_sup_c_no", $sch_sub_sup_c_no);
	}

	if (!empty($sch_sub_option_name)) {
		$add_where .= " AND pcu.`no` IN (SELECT pcus.parent_unit FROM product_cms_unit_subcontract pcus WHERE pcus.`sub_unit_name` LIKE '%{$sch_sub_option_name}%')";
		$smarty->assign("sch_sub_option_name", $sch_sub_option_name);
	}

	# 페이징 처리
	$cnt_sql 	 	= "SELECT count(*) AS total_count FROM product_cms_unit pcu WHERE {$add_where}";
	$cnt_query	 	= mysqli_query($my_db, $cnt_sql);
	$cnt_data	 	= mysqli_fetch_array($cnt_query);
	$total_num 	 	= $cnt_data['total_count'];

	$page 		 	= isset($_GET['page']) ?intval($_GET['page']) : 1;
	$ord_page_type 	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 20;

	$num 		 	= $ord_page_type;
	$offset 	 	= ($page-1) * $num;
	$pagenum 	 	= ceil($total_num/$num);

	if ($page>=$pagenum){$page=$pagenum;}
	if ($page<=0){$page=1;}

	$search_url = getenv("QUERY_STRING");
	$pagelist 	= pagelist($page, "product_cms_unit_subcontract_list.php", $pagenum, $search_url);

	$smarty->assign("page", $page);
	$smarty->assign("ord_page_type", $ord_page_type);
	$smarty->assign("total_num", $total_num);
	$smarty->assign("search_url", $search_url);
	$smarty->assign("pagelist", $pagelist);

	# 리스트 쿼리
	$product_cms_unit_sql = "
		SELECT
			pcu.`no`,
		    pcu.brand,
		    pcu.sup_c_no,
		    pcu.`type`,
		   	pcu.option_name,
		   	pcu.display,
		   	(SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcu.brand) as brand_name,
		    (SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcu.sup_c_no) as sup_c_name
		FROM product_cms_unit pcu
		WHERE {$add_where}
		ORDER BY pcu.no DESC
		LIMIT {$offset}, {$num}
	";
    $product_cms_unit_query = mysqli_query($my_db, $product_cms_unit_sql);
	$unit_subcontract_list	= [];
	$unit_type_option		= getUnitTypeOption();
	while($product_cms_unit = mysqli_fetch_array($product_cms_unit_query))
	{
		$product_cms_unit['type_name'] = $unit_type_option[$product_cms_unit['type']];

		$subcontract_list 	= [];
		$subcontract_sql	= "
			SELECT 
			    *,
				(SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcus.sup_c_no) as sub_sup_c_name
			FROM product_cms_unit_subcontract pcus 
			WHERE parent_unit = '{$product_cms_unit['no']}'
		";
		$subcontract_query = mysqli_query($my_db, $subcontract_sql);
		while($subcontract = mysqli_fetch_assoc($subcontract_query)) {
			$subcontract['sub_type_name'] = $unit_type_option[$subcontract['type']];;
			$subcontract_list[] = $subcontract;
		}

		$product_cms_unit['subcontract_cnt'] 	= count($subcontract_list);
		$product_cms_unit['subcontract_list'] 	= $subcontract_list;

		$unit_subcontract_list[] = $product_cms_unit;
	}

	$sup_company_list 	 = $unit_model->getDistinctUnitCompanyData('sup_c_no');
	$sub_sup_company_list = $unit_model->getSubcontractSupCompanyData();

	$smarty->assign("page_type_option", getPageTypeOption('4'));
	$smarty->assign("sup_company_list", $sup_company_list);
	$smarty->assign("sub_sup_company_list", $sub_sup_company_list);
	$smarty->assign("is_exist_option", getIsExistOption());
	$smarty->assign("unit_type_option", $unit_type_option);
	$smarty->assign("unit_subcontract_list", $unit_subcontract_list);

	$smarty->display('product_cms_unit_subcontract_list.html');
}
?>
