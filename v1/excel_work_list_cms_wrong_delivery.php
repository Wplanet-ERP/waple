<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "문의내용")
	->setCellValue('B1', "이전 구매처")
	->setCellValue('C1', "이전 주문번호")
	->setCellValue('D1', "출고 송장번호")
	->setCellValue('E1', "재출고 송장번호")
	->setCellValue('F1', "수령자명")
	->setCellValue('G1', "수령자 전화")
    ->setCellValue('H1', "오배송 상품명")
    ->setCellValue('I1', "내품 수량")
    ->setCellValue('J1', "재출고 상품명")
    ->setCellValue('K1', "내품 수량")
    ->setCellValue('L1', "비고")
;

#검색 처리
$sch_file_down_type 	= isset($_GET['sch_file_down_type']) ? $_GET['sch_file_down_type'] : "cms";
$sch_stock_month    	= isset($_GET['sch_stock_month']) ? $_GET['sch_stock_month'] : date("Y-m");
$sch_stock_s_month    	= isset($_GET['sch_stock_s_month']) ? $_GET['sch_stock_s_month'] : "";
$sch_stock_e_month    	= isset($_GET['sch_stock_e_month']) ? $_GET['sch_stock_e_month'] : "";
$sch_wrong_type			= isset($_GET['sch_wrong_type']) ? $_GET['sch_wrong_type'] : "1";
$excel_title 	    	= "오배송건";
$excel_file_name		= "";
$add_where 				= "w.delivery_state NOT IN(5)";

switch ($sch_wrong_type){
	case "1":
		$excel_title = "오배송&누락건";
		$add_where	.= " AND w.dp_c_no IN('5127','5128','5134','5135')";
		break;
	case "2":
		$excel_title = "파손건";
		$add_where	.= " AND w.dp_c_no IN('5900','5901')";
		break;
	case "3":
		$excel_title = "분실건";
		$add_where	.= " AND w.dp_c_no IN('5899')";
		break;
	default:
		$excel_title = "오배송&누락건";
		$add_where	.= " AND w.dp_c_no IN('5127','5128','5134','5135')";
		break;
}

if($sch_file_down_type == "csm"){
	$sch_stock_e_day	= date("t", strtotime($sch_stock_e_month));
	$sch_stock_s_date	= "{$sch_stock_s_month}-01 00:00:00";
	$sch_stock_e_date	= "{$sch_stock_e_month}-{$sch_stock_e_day} 00:00:00";
	$excel_file_name	= date("Ym",strtotime($sch_stock_s_month))."~".date("Ym",strtotime($sch_stock_e_month))."_{$excel_title}";
}
else{
	$sch_stock_e_day	= date("t", strtotime($sch_stock_month));
	$sch_stock_s_date 	= "{$sch_stock_month}-01 00:00:00";
	$sch_stock_e_date 	= "{$sch_stock_month}-{$sch_stock_e_day} 00:00:00";
	$excel_file_name	= date("Ym",strtotime($sch_stock_month))."_{$excel_title}";
}
$add_where .= " AND (w.stock_date BETWEEN '{$sch_stock_s_date}' AND '{$sch_stock_e_date}')";

$cms_ord_sql    = "SELECT DISTINCT w.order_number FROM work_cms w WHERE {$add_where} AND w.order_number is not null GROUP BY w.order_number ORDER BY w.w_no DESC LIMIT 10000";
$cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);

$order_number_list  = [];
while($order_number = mysqli_fetch_assoc($cms_ord_query)){
	$order_number_list[] =  "'".$order_number['order_number']."'";
}

$order_numbers = implode(',', $order_number_list);

// 리스트 쿼리
$with_sql = "
	SELECT
	    w.manager_memo,
		(SELECT sub.dp_c_name FROM work_cms sub WHERE sub.order_number = w.parent_order_number LIMIT 1) as parent_dp_name,
		w.parent_order_number,
        w.order_number,
	    GROUP_CONCAT((SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no),'*',w.quantity) as prd_name,
	    w.recipient,
	    w.recipient_hp,
	    w.dp_c_name,
	    (SELECT r.order_number FROM work_cms_return r WHERE r.parent_order_number=w.parent_order_number ORDER BY r.r_no DESC LIMIT 1) as r_order_number
	FROM work_cms w
	WHERE {$add_where} AND w.order_number IN({$order_numbers})
	GROUP BY order_number
	ORDER BY w.w_no DESC
";

$result	= mysqli_query($my_db, $with_sql);
$idx = 2;
if(!!$result)
{
    $lfcr = chr(10) ;
    while($work_cms = mysqli_fetch_array($result))
    {
        $r_order_number = $work_cms['r_order_number'];
		$r_prd_name		= "";

        if(!empty($r_order_number)){
			$return_sql 	= "SELECT GROUP_CONCAT((SELECT p.title FROM product_cms p WHERE p.prd_no=r.prd_no),'*',r.quantity) as r_prd_name FROM work_cms_return r WHERE r.order_number='{$r_order_number}' GROUP BY order_number";
			$return_query 	= mysqli_query($my_db, $return_sql);
			$return_result 	= mysqli_fetch_assoc($return_query);
			$r_prd_name 	= str_replace(',', $lfcr, $return_result['r_prd_name']);
		}

        $prd_name 	= str_replace(',', $lfcr, $work_cms['prd_name']);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $work_cms['manager_memo'])
            ->setCellValue("B{$idx}", $work_cms['parent_dp_name'])
			->setCellValueExplicit("C{$idx}", $work_cms['parent_order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("D{$idx}", '')
            ->setCellValue("E{$idx}", '')
            ->setCellValue("F{$idx}", $work_cms['recipient'])
            ->setCellValue("G{$idx}", $work_cms['recipient_hp'])
            ->setCellValue("H{$idx}", $r_prd_name)
            ->setCellValue("I{$idx}", "")
            ->setCellValue("J{$idx}", $prd_name)
            ->setCellValue("K{$idx}", "")
            ->setCellValue("L{$idx}", str_replace("CS_", "", $work_cms['dp_c_name']))
        ;

        $idx++;
    }
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');

$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00A6A6A6');
$objPHPExcel->getActiveSheet()->getStyle('H1:K1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FDE9D9');

$objPHPExcel->getActiveSheet()->getStyle("A1:L{$idx}")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("A1:L1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:L{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("A2:A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("B2:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("A2:A{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setWrapText(true);


// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(52);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(28);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(28);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);

$objPHPExcel->getActiveSheet()->setTitle($excel_title);
$objPHPExcel->getActiveSheet()->getStyle("A1:L{$idx}")->applyFromArray($styleArray);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename = iconv('UTF-8','EUC-KR',"{$excel_file_name}.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>
