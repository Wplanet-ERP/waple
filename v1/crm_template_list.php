<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_message.php');
require('inc/model/MyQuick.php');
require('inc/model/Message.php');

# Process
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$template_model = Message::Factory();
$template_model->setCrmTemplateInit();
$template_model->setCharset("utf8mb4");
$my_db->set_charset("utf8mb4");

if($process == 'new_crm_template')
{
    $search_url_val         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $new_title              = (isset($_POST['new_title'])) ? $_POST['new_title'] : "";
    $new_temp_type          = (isset($_POST['new_temp_type'])) ? $_POST['new_temp_type'] : "";
    $new_temp_key           = (isset($_POST['new_temp_key'])) ? $_POST['new_temp_key'] : "";
    $new_send_key           = (isset($_POST['new_send_key'])) ? $_POST['new_send_key'] : "";
    $new_send_name_key      = (isset($_POST['new_send_name'])) ? $_POST['new_send_name'] : "";
    $new_send_phone_name    = (isset($_POST['new_send_phone_name'])) ? $_POST['new_send_phone_name'] : "";
    $new_send_phone         = (isset($_POST['new_send_phone'])) ? $_POST['new_send_phone'] : "";
    $new_subject            = (isset($_POST['new_subject'])) ? addslashes($_POST['new_subject']) : "";
    $new_content            = (isset($_POST['new_content'])) ? addslashes($_POST['new_content']) : "";
    $new_btn_type           = (isset($_POST['new_btn_type'])) ? $_POST['new_btn_type'] : "";
    $new_btn_caption        = (isset($_POST['new_caption'])) ? $_POST['new_caption'] : "";
    $new_btn_url            = (isset($_POST['new_url'])) ? $_POST['new_url'] : "";
    $new_reg_date           = date("Y-m-d H:i:s");
    $is_add_btn             = false;
    $temp_ins_data          = [];

    if ($new_temp_type == "AT")
    {
        if (empty($new_title) || empty($new_temp_key) || empty($new_send_key) || empty($new_send_name_key) || empty($new_content)) {
            exit ("<script>alert('데이터를 다시 입력해 주세요.'); location.href='crm_template_list.php?{$search_url_val}';</script>");
        }

        $idx        = 0;
        $btn_idx    = 1;
        $btn_list   = [];
        if(!empty($new_btn_caption))
        {
            foreach ($new_btn_caption as $caption)
            {
                $btn_type = $new_btn_type[$idx];
                $btn_url  = $new_btn_url[$idx];
                if($btn_type == 'AC')
                {
                    $btn_list["button{$btn_idx}"] = array(
                        "name"  => "채널추가",
                        "type"  => $btn_type
                    );
                }else{
                    $btn_list["button{$btn_idx}"] = array(
                        "name"          => $caption,
                        "type"          => $btn_type,
                        "url_mobile"    => $btn_url,
                        "url_pc"        => $btn_url
                    );
                }

                $idx++;
                $btn_idx++;
            }
        }

        $crm_send_name_option   = getCrmSendNameOption();
        $new_btn_content        = !empty($btn_list) ? json_encode($btn_list, JSON_UNESCAPED_UNICODE) : "";
        $new_send_name          = $crm_send_name_option[$new_send_name_key];

        $temp_ins_data          = array(
            "title"       => $new_title,
            "temp_type"   => $new_temp_type,
            "temp_key"    => $new_temp_key,
            "send_key"    => $new_send_key,
            "send_name"   => $new_send_name,
            "content"     => $new_content,
            "regdate"     => $new_reg_date
        );

        if(!empty($btn_list)) {
            $is_add_btn = true;
            $temp_ins_data["btn_content"] = $new_btn_content;
        }
    }
    elseif ($new_temp_type == "SMS")
    {
        $new_subject         = "(광고) ".$new_subject;
        $new_send_phone_name = "(광고) ".$new_send_phone_name;

        $temp_ins_data      = array(
            "title"         => $new_title,
            "temp_type"     => $new_temp_type,
            "send_name"     => $new_send_phone_name,
            "send_phone"    => $new_send_phone,
            "subject"       => $new_subject,
            "content"       => $new_content,
            "regdate"       => $new_reg_date
        );
    }

    if ($template_model->insert($temp_ins_data))
    {
        if($is_add_btn)
        {
            $crm_temp_no = $template_model->getInsertId();
            $new_btn_key = "EASY_MSGKEY_".$crm_temp_no;
            $template_model->update(array("t_no" => $crm_temp_no, "btn_key" => $new_btn_key));
        }

        echo("<script>alert('알림톡 설정 저장되었습니다');location.href='crm_template_list.php?{$search_url_val}';</script>");
    } else {
        echo("<script>alert('알림톡 설정 저장에 실패하였습니다');location.href='crm_template_list.php?{$search_url_val}';</script>");
    }
}
elseif($process == 'modify_btn_info')
{
    $search_url         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $t_no               = (isset($_POST['crm_t_no'])) ? $_POST['crm_t_no'] : "";
    $crm_btn_key        = (isset($_POST['crm_btn_key'])) ? $_POST['crm_btn_key'] : "";
    $crm_btn_type       = (isset($_POST['crm_btn_type'])) ? explode(",",$_POST['crm_btn_type']) : "";
    $crm_btn_url        = (isset($_POST['crm_btn_url'])) ? explode(",",$_POST['crm_btn_url']) : "";
    $crm_btn_caption    = (isset($_POST['crm_btn_caption'])) ?  explode(",",$_POST['crm_btn_caption']) : "";

    if(empty($t_no))
    {
        exit ("<script>alert('데이터를 다시 입력해 주세요.'); location.href='crm_template_list.php?{$search_url}';</script>");
    }

    $idx        = 0;
    $btn_idx    = 1;
    $btn_list   = [];
    foreach($crm_btn_caption as $caption)
    {
        $btn_type = $crm_btn_type[$idx];
        $btn_url  = $crm_btn_url[$idx];

        if($btn_type == 'AC'){
            $btn_list["button{$btn_idx}"] = array(
                "name"  => "채널 추가",
                "type"  => $btn_type
            );
        }else{
            $btn_list["button{$btn_idx}"] = array(
                "name"          => $caption,
                "type"          => $btn_type,
                "url_mobile"    => $btn_url,
                "url_pc"        => $btn_url
            );
        }

        $idx++;
        $btn_idx++;
    }
    $btn_content = !empty($btn_list) ? json_encode($btn_list, JSON_UNESCAPED_UNICODE) : "";

    if (!$template_model->update(array("t_no" => $t_no, "btn_content" => $btn_content))){
        echo ("<script>alert('버튼설정 저장에 실패하였습니다');location.href='crm_template_list.php?{$search_url}';</script>");
    }else{
        echo ("<script>alert('버튼설정 저장되었습니다');location.href='crm_template_list.php?{$search_url}';</script>");
    }
}
elseif($process == 'modify_title')
{
    $t_no  = (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $value = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($t_no)){
        echo "t_no 값이 없습니다.";
        exit;
    }

    if (!$template_model->update(array("t_no" => $t_no, "title" => $value)))
        echo "템플릿명 변경에 실패하였습니다.";
    else
        echo "템플릿명이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_temp_key')
{
    $t_no  = (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $value = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($t_no)){
        echo "t_no 값이 없습니다.";
        exit;
    }

    if (!$template_model->update(array("t_no" => $t_no, "temp_key" => $value)))
        echo "템플릿 Key 변경에 실패하였습니다.";
    else
        echo "템플릿 Key값이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_send_key')
{
    $t_no  = (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $value = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($t_no)){
        echo "t_no 값이 없습니다.";
        exit;
    }

    if (!$template_model->update(array("t_no" => $t_no, "send_key" => $value)))
        echo "발신자 Key 변경에 실패하였습니다.";
    else
        echo "발신자 Key값이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_send_name')
{
    $t_no  = (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $value = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if(empty($t_no)){
        echo "t_no 값이 없습니다.";
        exit;
    }

    if (!$template_model->update(array("t_no" => $t_no, "send_name" => $value)))
        echo "발신프로필명 변경에 실패하였습니다.";
    else
        echo "발신프로필명이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_send_phone')
{
    $t_no  = (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $value = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($t_no)){
        echo "t_no 값이 없습니다.";
        exit;
    }

    if (!$template_model->update(array("t_no" => $t_no, "send_phone" => $value)))
        echo "발신자 전화번호 변경에 실패하였습니다.";
    else
        echo "발신자 전화번호가 변경되었습니다.";
    exit;
}
elseif($process == 'modify_subject')
{
    $t_no  = (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $value = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if(empty($t_no)){
        echo "t_no 값이 없습니다.";
        exit;
    }

    if (!$template_model->update(array("t_no" => $t_no, "subject" => $value)))
        echo "제목 변경에 실패하였습니다.";
    else
        echo "제목이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_content')
{
    $t_no  = (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $value = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if(empty($t_no)){
        echo "t_no 값이 없습니다.";
        exit;
    }

    if (!$template_model->update(array("t_no" => $t_no, "content" => $value)))
        echo "템플릿 내용 변경에 실패하였습니다.";
    else
        echo "템플릿 내용이 변경되었습니다.";
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "53";
$nav_title   = "SMS/알림톡 템플릿 설정";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$sch_t_no           = isset($_GET['sch_t_no']) ? $_GET['sch_t_no'] : "";
$sch_active         = isset($_GET['sch_active']) ? $_GET['sch_active'] : "1";
$sch_temp_type      = isset($_GET['sch_temp_type']) ? $_GET['sch_temp_type'] : "";
$sch_title          = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_content        = isset($_GET['sch_content']) ? $_GET['sch_content'] : "";
$sch_btn_content    = isset($_GET['sch_btn_content']) ? $_GET['sch_btn_content'] : "";

$add_where = "1=1";
if(!empty($sch_t_no))
{
    $add_where .= " AND `ct`.t_no = '{$sch_t_no}'";
    $smarty->assign('sch_t_no', $sch_t_no);
}

if(!empty($sch_active))
{
    $add_where .= " AND `ct`.active = '{$sch_active}'";
    $smarty->assign('sch_active', $sch_active);
}

if(!empty($sch_temp_type))
{
    $add_where .= " AND `ct`.temp_type = '{$sch_temp_type}'";
    $smarty->assign('sch_temp_type', $sch_temp_type);
}

if(!empty($sch_title))
{
    $add_where .= " AND `ct`.title like '%{$sch_title}%'";
    $smarty->assign('sch_title', $sch_title);
}

if(!empty($sch_content))
{
    $add_where .= " AND `ct`.content like '%{$sch_content}%'";
    $smarty->assign('sch_content', $sch_content);
}

if(!empty($sch_btn_content))
{
    $add_where .= " AND `ct`.btn_content like '%{$sch_btn_content}%'";
    $smarty->assign('sch_btn_content', $sch_btn_content);
}

// 전체 게시물 수
$crm_template_total_sql      = "SELECT count(t_no) FROM (SELECT `ct`.t_no FROM crm_template `ct` WHERE {$add_where}) AS cnt";
$crm_template_total_query	= mysqli_query($my_db, $crm_template_total_sql);
if(!!$crm_template_total_query)
    $crm_template_total_result = mysqli_fetch_array($crm_template_total_query);

$crm_template_total = $crm_template_total_result[0];

//페이징
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 10;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($crm_template_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list  = pagelist($pages, "crm_template_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $crm_template_total);
$smarty->assign("page_list", $page_list);

# CRM 템플릿 설정
$crm_template_list  = [];
$crm_template_sql   = "SELECT * FROM crm_template `ct` WHERE {$add_where} ORDER BY `ct`.t_no DESC LIMIT {$offset}, {$num}";
$crm_template_query = mysqli_query($my_db, $crm_template_sql);
while($crm_template = mysqli_fetch_assoc($crm_template_query))
{
    if($crm_template['btn_content'])
    {
        $btn_content = json_decode($crm_template['btn_content'], true);

        if(strpos($crm_template['btn_key'], "EASY") !== false){
            foreach($btn_content as $btn){
                $crm_template["btn_type_list"][]    = $btn['type'];
                $crm_template["btn_caption_list"][] = $btn['name'];
                $crm_template["btn_url_list"][]     = $btn['url_mobile'];
            }
        }else{
            foreach($btn_content as $btns){
                foreach($btns as $btn){
                    $crm_template["btn_type_list"][]    = $btn['type'];
                    $crm_template["btn_caption_list"][] = $btn['name'];
                    $crm_template["btn_url_list"][]     = $btn['url_mobile'];
                }
            }
        }
    }
    $crm_template_list[] = $crm_template;
}

$smarty->assign('display_option', getDisplayOption());
$smarty->assign('crm_send_name_option', getCrmSendNameOption());
$smarty->assign('crm_btn_type_option', getCrmBtnTypeOption());
$smarty->assign('crm_temp_type_option', getCrmTempTypeOption());
$smarty->assign('crm_template_list', $crm_template_list);

$smarty->display('crm_template_list.html');
?>
