<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/commerce_sales.php');
require('inc/helper/work_cms.php');
require('inc/helper/wise_bm.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');

# Navigation & My Quick
$nav_prd_no  = "24";
$nav_title   = "커머스 매출 및 비용관리(브랜드통계)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

/** 검색조건 START */
$dp_except_list             = getNotApplyDpList();
$dp_except_text             = implode(",", $dp_except_list);
$date_name_option           = getDateChartOption();
$daily_hour_option          = getHourOption();
$dp_self_imweb_list         = getSelfDpImwebCompanyList();
$dp_self_imweb_text         = implode(",", $dp_self_imweb_list);
$global_dp_all_list         = getGlobalDpCompanyList();
$global_dp_total_list       = $global_dp_all_list['total'];
$global_dp_ilenol_list      = $global_dp_all_list['ilenol'];
$global_brand_list          = getGlobalBrandList();
$global_brand_option        = array_keys($global_brand_list);
$doc_brand_list             = getTotalDocBrandList();
$doc_brand_text             = implode(",", $doc_brand_list);
$ilenol_brand_list          = getIlenolBrandList();
$ilenol_brand_text          = implode(",", $ilenol_brand_list);
$ilenol_global_dp_text      = implode(",", $global_dp_ilenol_list);

$add_where              = "display='1'";
$add_cms_where          = "delivery_state='4' AND dp_c_no NOT IN({$dp_except_text})";
$add_quick_where        = "w.prd_type='1' AND w.unit_price > 0 AND w.quick_state='4'";
$add_result_where       = "`am`.state IN(3,5) AND `am`.product IN(1,2) AND `am`.media='265'";
$self_cms_base_where    = "w.page_idx > 0 AND w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN({$dp_self_imweb_text})";
$add_convert_where      = "`cc`.dp_c_no > 0 AND `cc`.brand > 0 AND `cc`.dp_c_no IN({$dp_self_imweb_text})";

# Brand Group 검색
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$sch_brand_name             = "전체";
$search_term_url            = "";

# 유입/전환 min, max
$chart_base_type    = getBaseChartType();
$chart_brand_type   = getTotalBrandChartType();
$chart_doc_type     = getDocBrandChartType();
$chart_empty_type   = getEmptyChartType();

# 브랜드 검색 설정 및 수정여부
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$brand_column_list  = [];
$brand_company_list = [];
$total_column_list  = [];
$add_group          = "";
$sch_brand_url      = "";

if(!isset($_GET['sch_brand_g1']))
{
    switch($session_s_no){
        case '4':
            $sch_brand_g1   = '70003';
            break;
        case '6':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70008';
            break;
        case '7':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70014';
            $sch_brand      = '4446';
            break;
        case '10':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5368';
            break;
        case '15':
            $sch_brand_g1   = '70002';
            break;
        case '17':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '1314';
            break;
        case '42':
        case '177':
        case '265':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            break;
        case '59':
            $sch_brand_g1   = '70005';
            $sch_brand_g2   = '70020';
            $sch_brand      = '2402';
            break;
        case '67':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5434';
            break;
        case '145':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70010';
            break;
        case '147':
            $sch_brand_g1   = '70004';
            $sch_brand_g2   = '70019';
            break;
        case '43':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5812';
            break;
    }
}

$convert_total_chart_style_list = $chart_base_type;

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1    = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2    = $brand_parent_list[$sch_brand]["brand_g2"];
    $search_term_url = "sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}&sch_brand={$sch_brand}";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1    = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
    $search_term_url = "sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}";
}
elseif(!empty($sch_brand_g1)){
    $search_term_url = "sch_brand_g1={$sch_brand_g1}";
}

if(!empty($sch_brand_g1)){
    $sch_brand_url = "sch_brand_g1={$sch_brand_g1}";

    switch ($sch_brand_g1){
        case '70001':   # 닥터피엘
            $convert_total_chart_style_list = $chart_brand_type["doc"];
            break;
        case '70002':   # 아이레놀
            $convert_total_chart_style_list = $chart_brand_type["ilenol"];
            break;
        case '70003':   # 누잠
            $convert_total_chart_style_list = $chart_brand_type["nuzam"];
            break;
        case '70004':   # 시티파이
            $convert_total_chart_style_list = $chart_brand_type["city"];
            break;
        default:
            $convert_total_chart_style_list = $chart_empty_type;
            break;
    }
}

if(!empty($sch_brand_g2)){
    $sch_brand_url .= "&sch_brand_g2={$sch_brand_g2}";

    switch ($sch_brand_g2) {
        case '70007':   # 닥터피엘 필터라인
            $convert_total_chart_style_list = $chart_doc_type["1314"];
            break;
        case '70008':   # 닥터피엘 큐빙
            $convert_total_chart_style_list = $chart_doc_type["3386"];
            break;
        case '70009':   # 닥터피엘 퓨어팟
            $convert_total_chart_style_list = $chart_doc_type["2863"];
            break;
        case '70010':   # 닥터피엘 에코호스
            $convert_total_chart_style_list = $chart_doc_type["3303"];
            break;
        case '70011':   # 닥터피엘 여행용
            $convert_total_chart_style_list = $chart_doc_type["5434"];
            break;
        case '70026':   # 닥터피엘 아토샤워헤드
            $convert_total_chart_style_list = $chart_doc_type["5812"];
            break;
        case '70014':   # 닥터피엘 피쳐정수기
            $convert_total_chart_style_list = $chart_doc_type["5434"];
            break;
        default:
            $convert_total_chart_style_list = $chart_empty_type;
            break;
    }
}

if(!empty($sch_brand))
{
    $sch_brand_g2_list              = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list                 = $brand_list[$sch_brand_g2];
    $brand_column_list[$sch_brand]  = $brand_list[$sch_brand_g2][$sch_brand];
    $brand_company_list[$sch_brand] = $sch_brand;
    $sch_brand_name                 = $brand_company_g1_list[$sch_brand_g1]." > ".$brand_company_g2_list[$sch_brand_g1][$sch_brand_g2]." > ".$brand_list[$sch_brand_g2][$sch_brand];

    if(in_array($sch_brand, $doc_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5956' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif(in_array($sch_brand, $ilenol_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5659' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "5513"){
        $add_cms_where      .= " AND `w`.c_no IN({$doc_brand_text}) AND (`w`.log_c_no = '5956' OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$doc_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5514"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND ((`w`.log_c_no = '5659' AND `w`.dp_c_no NOT IN({$ilenol_global_dp_text})) OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5979"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.c_no != '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "6044"){
        $add_cms_where      .= " AND `w`.c_no = '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    else{
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}'";
    }

    $add_result_where       .= " AND `ar`.brand = '{$sch_brand}'";
    $add_where              .= " AND cr.brand='{$sch_brand}'";
    $self_cms_base_where    .= " AND `w`.c_no = '{$sch_brand}'";
    $add_convert_where      .= " AND `cc`.brand = '{$sch_brand}'";
    $sch_brand_url          .= "&sch_brand={$sch_brand}";
}
elseif(!empty($sch_brand_g2)){
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $brand_column_list  = $sch_brand_list;
    $sch_brand_name     = $brand_company_g1_list[$sch_brand_g1]." > ".$brand_company_g2_list[$sch_brand_g1][$sch_brand_g2];

    foreach($sch_brand_list as $c_no => $c_name) {
        $brand_company_list[$c_no] = $c_no;
    }

    $add_where              .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_cms_where          .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_quick_where        .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_result_where       .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $self_cms_base_where    .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_convert_where      .= " AND `cc`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $brand_column_list  = $sch_brand_g2_list;
    $sch_brand_name     = $brand_company_g1_list[$sch_brand_g1];

    foreach($sch_brand_g2_list as $brand_g2 => $brand_g2_name)
    {
        $init_company_list = $brand_list[$brand_g2];
        if(!empty($init_company_list))
        {
            foreach($init_company_list as $c_no => $c_name) {
                $brand_company_list[$c_no] = $c_no;
            }
        }
    }

    $add_where              .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_cms_where          .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_quick_where        .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_result_where       .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $self_cms_base_where    .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_convert_where      .= " AND `cc`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
else
{
    $brand_column_list  = $brand_company_g1_list;

    foreach($brand_company_g1_list as $brand_g1 => $brand_g1_name)
    {
        $init_g2_list = $brand_company_g2_list[$brand_g1];
        if(!empty($init_g2_list))
        {
            foreach($init_g2_list as $brand_g2 => $brand_g2_name)
            {
                $init_company_list = $brand_list[$brand_g2];
                if(!empty($init_company_list))
                {
                    foreach($init_company_list as $c_no => $c_name) {
                        $brand_company_list[$c_no] = $c_no;
                    }
                }
            }
        }
    }
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_name", $sch_brand_name);
$smarty->assign("sch_brand_url", $sch_brand_url);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

$today_s_w		 = date('w')-1;
$sch_is_rate     = isset($_GET['sch_is_rate']) ? $_GET['sch_is_rate'] : "";
$sch_date_type   = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$sch_s_year      = isset($_GET['sch_s_year']) ? $_GET['sch_s_year'] : date('Y', strtotime("-5 years"));;
$sch_e_year      = isset($_GET['sch_e_year']) ? $_GET['sch_e_year'] : date('Y');
$sch_s_month     = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("-5 months"));
$sch_e_month     = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week      = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} days"));
$sch_e_week      = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 days"));
$sch_s_date      = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-8 days"));
$sch_e_date      = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d',strtotime("-1 days"));

$smarty->assign('sch_is_rate', $sch_is_rate);
$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_s_year', $sch_s_year);
$smarty->assign('sch_e_year', $sch_e_year);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);

# 전체 기간 조회 및 누적데이터 조회
$all_date_where     = "";
$all_date_key	    = "";
$add_date_where     = "";
$add_date_column    = "";
$add_cms_column     = "";
$add_result_column  = "";
$add_convert_column = "";
$sch_net_date       = "";
$sch_rate_date      = "";
$chk_s_date         = "";
$chk_e_date         = "";

if($sch_date_type == '4') //연간
{
    $sch_brand_name    .= " ({$sch_s_year} ~ {$sch_e_year}) 합계";

    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y')";

    $chk_s_date         = "{$sch_s_year}-01-01";
    $chk_e_date         = "{$sch_e_year}-12-31";

    $add_cms_column     = "DATE_FORMAT(order_date, '%Y')";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y')";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y')";
    $add_convert_column = "DATE_FORMAT(`cc`.convert_date, '%Y')";
}
elseif($sch_date_type == '3') //월간
{
    $sch_brand_name    .= " ({$sch_s_month} ~ {$sch_e_month}) 합계";
    $sch_s_month        = ($sch_is_rate == "1") ? date("Y-m", strtotime("{$sch_s_month} -1 months")) : $sch_s_month;
    $sch_rate_date      = date("Ym", strtotime($sch_s_month));

    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y%m')";

    $chk_s_date         = "{$sch_s_month}-01";
    $chk_e_day          = date("t", strtotime($sch_e_month));
    $chk_e_date         = "{$sch_e_month}-{$chk_e_day}";

    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m')";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y%m')";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y%m')";
    $add_convert_column = "DATE_FORMAT(`cc`.convert_date, '%Y%m')";
}
elseif($sch_date_type == '2') //주간
{
    $sch_brand_name    .= " ({$sch_s_week} ~ {$sch_e_week}) 합계";
    $sch_s_week         = ($sch_is_rate == "1") ? date("Y-m-d", strtotime("{$sch_s_week} -1 weeks")) : $sch_s_week;
    $sch_rate_date      = date("Ymd", strtotime($sch_s_week));

    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	    = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title     = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_date_column    = "DATE_FORMAT(DATE_SUB(sales_date, INTERVAL(IF(DAYOFWEEK(sales_date)=1,8,DAYOFWEEK(sales_date))-2) DAY), '%Y%m%d')";

    $chk_s_date         = $sch_s_week;
    $chk_e_date         = $sch_e_week;

    $add_cms_column     = "DATE_FORMAT(DATE_SUB(order_date, INTERVAL(IF(DAYOFWEEK(order_date)=1,8,DAYOFWEEK(order_date))-2) DAY), '%Y%m%d')";
    $add_quick_column   = "DATE_FORMAT(DATE_SUB(run_date, INTERVAL(IF(DAYOFWEEK(run_date)=1,8,DAYOFWEEK(run_date))-2) DAY), '%Y%m%d')";
    $add_result_column  = "DATE_FORMAT(DATE_SUB(am.adv_s_date, INTERVAL(IF(DAYOFWEEK(am.adv_s_date)=1,8,DAYOFWEEK(am.adv_s_date))-2) DAY), '%Y%m%d')";
    $add_convert_column = "DATE_FORMAT(DATE_SUB(`cc`.convert_date, INTERVAL(IF(DAYOFWEEK(`cc`.convert_date)=1,8,DAYOFWEEK(`cc`.convert_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $sch_brand_name    .= " ({$sch_s_date} ~ {$sch_e_date}) 합계";

    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y%m%d')";

    $chk_s_date         = $sch_s_date;
    $chk_e_date         = $sch_e_date;

    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m%d')";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y%m%d')";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y%m%d')";
    $add_convert_column = "DATE_FORMAT(`cc`.convert_date, '%Y%m%d')";
}

$search_term_url   .= "&sch_date_type={$sch_date_type}&sch_base_s_date={$chk_s_date}&sch_base_e_date={$chk_e_date}";
$sch_net_date       = $chk_s_date;
$chk_s_datetime     = "{$chk_s_date} 00:00:00";
$chk_e_datetime     = "{$chk_e_date} 23:59:59";
$add_quick_where   .= " AND run_date BETWEEN '{$chk_s_datetime}' AND '{$chk_e_datetime}' ";
$add_result_where  .= " AND am.adv_s_date BETWEEN '{$chk_s_datetime}' AND '{$chk_e_datetime}'";
$add_convert_where .= " AND `cc`.convert_date BETWEEN '{$chk_s_datetime}' AND '{$chk_e_datetime}'";

$smarty->assign("chk_s_date", $chk_s_date);
$smarty->assign("chk_e_date", $chk_e_date);
$smarty->assign("sch_brand_date_name", $sch_brand_name);
$smarty->assign("search_term_url", $search_term_url);

# 이벤트 일정(해당일만 나옴 수정 필요)
$cal_event_sql = "
    SELECT
        DATE_FORMAT(cs_s_date, '%Y-%m-%d') as cs_s_date,
        DATE_FORMAT(cs_e_date, '%Y-%m-%d') as cs_e_date,
        cs_title
    FROM calendar_schedule
    WHERE cal_id='dp_event' AND cs_important='1' AND (  
        ('{$chk_s_datetime}' <= cs_s_date AND cs_s_date <= '{$chk_e_datetime}') 
        OR 
        ('{$chk_s_datetime}' <= cs_e_date AND cs_e_date <= '{$chk_e_datetime}') 
        OR
        (cs_s_date <= '{$chk_s_datetime}' AND '{$chk_s_datetime}' <= cs_e_date) 
        OR
        (cs_s_date <= '{$chk_e_datetime}' AND '{$chk_e_datetime}' <= cs_e_date) 
    )
";
$cal_event_query    = mysqli_query($my_db, $cal_event_sql);
$cal_event_all_list = [];
while($cal_event = mysqli_fetch_assoc($cal_event_query))
{
    $cal_event_all_list[] = $cal_event;
}

$commerce_date_list         = [];
$commerce_link_date_list    = [];
$sales_tmp_total_list       = [];
$cost_tmp_total_list        = [];
$net_report_date_list       = [];
$net_parent_percent_list    = [];
$company_chk_list           = [];
$nosp_result_list           = [];
$nosp_result_total          = 0;
$cal_event_list             = [];
$cms_prd_tmp_total_list     = [];
$cms_prd_tmp_list           = [];
$cms_prd_best_list          = [];
$cms_prd_name_list          = [];

$total_all_date_list        = [];
$total_report_table_list    = [];
$total_report_brand_list    = [];
$total_report_date_list     = [];
$convert_total_list         = [];

$date_name_option       = getDateChartOption();;
$nosp_result_init       = array("1" => array("count" => 0, "df_total" => 0, "n_total" => 0, "e_total" => 0, "total" => 0), "2" => array("count" => 0, "df_total" => 0, "n_total" => 0, "e_total" => 0, "total" => 0)); # 1: 타임보드, 2: 스페셜DA
$nosp_df_option         = array(1314);
$nosp_nuzam_option      = array(2827,4878);
$conversion_option      = array("traffic" => 0, "conversion" => 0, "conversion_rate" => 0,"total_price" => 0, "avg_price" => 0);

# 날짜 정리 및 리스트 Init
$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
if($all_date_where != '')
{
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    while($date = mysqli_fetch_array($all_date_query))
    {
        $total_all_date_list[$date['date_key']] = array("s_date" => date("Y-m-d",strtotime($date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($date['date_key']))." 23:59:59");
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_name_option[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        foreach($brand_column_list as $kind_key => $kind_title)
        {
            $total_report_table_list[$kind_key]['title'] = $kind_title;
            $total_report_table_list[$kind_key][$date['chart_key']] = array(
                "sales"     => 0,
                "cost"      => 0,
                "profit"    => 0
            );

            $total_report_brand_list[$kind_key] = array(
                "sales"     => 0,
                "cost"      => 0,
                "profit"    => 0
            );

            foreach($brand_total_list[$kind_key] as $brand){
                $company_chk_list[$brand] = $kind_key;
            }
        }

        foreach($brand_company_list as $c_no)
        {
            $cost_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
            $sales_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
            $net_report_date_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
            $net_parent_percent_list[$c_no] = 0;
        }

        if(!isset($commerce_date_list[$date['chart_key']]))
        {
            $commerce_date_list[$date['chart_key']]     = $chart_title;
            $nosp_result_list[$date['chart_key']]       = $nosp_result_init;
            $convert_total_list[$date['chart_key']]     = $conversion_option;

            $total_report_date_list[$date['chart_key']] = array(
                "sales"     => 0,
                "cost"      => 0,
                "profit"    => 0
            );

            if($sch_date_type == "4"){
                $link_year      = $date['chart_key'];
                $link_s_date    = "{$link_year}-01-01";
                $link_e_date    = "{$link_year}-12-31";
                $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
            }
            elseif($sch_date_type == "3"){
                $link_month     = date("Y-m", strtotime($date['date_key']));
                $link_e_day     = date("t", strtotime($link_month));
                $link_s_date    = "{$link_month}-01";
                $link_e_date    = "{$link_month}-{$link_e_day}";
                $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
            }
            elseif($sch_date_type == "2"){
                $link_s_date    = date("Y-m-d", strtotime($date['date_key']));
                $link_s_w       = date('w', strtotime($link_s_date));
                $link_e_w       = 7-$link_s_w;
                $link_e_date    = date("Y-m-d", strtotime("{$link_s_date} +{$link_e_w} days"));
                $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
            }
            else{
                $link_s_date    = date("Y-m-d", strtotime($date['date_key']));
                $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_s_date);
            }

            if($sch_brand_g2 == '70007' && !isset($cal_event_list[$date['chart_key']]))
            {
                $cal_event_text_list = [];

                foreach($cal_event_all_list as $cal_event)
                {
                    if($link_s_date <= $cal_event['cs_s_date'] && $cal_event['cs_s_date'] <= $link_e_date){
                        $cal_event_text_list[] = $cal_event['cs_title'];
                    }elseif($link_s_date <= $cal_event['cs_e_date'] && $cal_event['cs_e_date'] <= $link_e_date){
                        $cal_event_text_list[] = $cal_event['cs_title'];
                    }elseif($cal_event['cs_s_date'] <= $link_s_date && $link_s_date <= $cal_event['cs_e_date']){
                        $cal_event_text_list[] = $cal_event['cs_title'];
                    }elseif($cal_event['cs_s_date'] <= $link_e_date && $link_e_date <= $cal_event['cs_e_date']){
                        $cal_event_text_list[] = $cal_event['cs_title'];
                    }
                }

                if(!empty($cal_event_text_list)){
                    $cal_event_list[$date['chart_key']] = implode("\r\n", $cal_event_text_list);
                }
            }
        }
    }
}

# 수기 데이터
$commerce_sql  = "
    SELECT
        cr.`type`,
        cr.price as price,
        cr.brand,
        DATE_FORMAT(sales_date, '%Y%m%d') as sales_date,
        {$add_date_column} as key_date
    FROM commerce_report `cr`
    WHERE {$add_where}
    {$add_date_where}
    ORDER BY key_date ASC
";
$commerce_query = mysqli_query($my_db, $commerce_sql);
while($commerce = mysqli_fetch_assoc($commerce_query))
{
    if($commerce['type'] == 'cost'){
        $cost_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
    }elseif($commerce['type'] == 'sales'){
        $sales_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
    }
}

# 배송리스트 데이터
$commerce_fee_option         = getCommerceFeeOption();
$cms_delivery_chk_fee_list   = [];
$cms_delivery_chk_nuzam_list = [];
$cms_delivery_chk_ord_list   = [];
$cms_delivery_chk_list       = [];
foreach($total_all_date_list as $date_data)
{
    $cms_report_sql      = "
        SELECT
            c_no,
            dp_c_no,
            prd_no,
            (SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no) as prd_name,
            order_number,
            DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
            {$add_cms_column} as key_date, 
            unit_price,
            (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
            unit_delivery_price,
            (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
        FROM work_cms as w
        WHERE {$add_cms_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}')
    ";
    $cms_report_query = mysqli_query($my_db, $cms_report_sql);
    while($cms_report = mysqli_fetch_assoc($cms_report_query))
    {
        $commerce_fee_total_list    = isset($commerce_fee_option[$cms_report['c_no']]) ? $commerce_fee_option[$cms_report['c_no']] : [];
        $commerce_fee_per_list      = !empty($commerce_fee_total_list) && isset($commerce_fee_total_list[$cms_report['dp_c_no']]) ? $commerce_fee_total_list[$cms_report['dp_c_no']] : [];
        $commerce_fee_per           = 0;

        if(!empty($commerce_fee_per_list))
        {
            foreach($commerce_fee_per_list as $fee_date => $fee_val){
                if($cms_report['sales_date'] >= $fee_date){
                    $commerce_fee_per = $fee_val;
                }
            }
        }

        $cms_report_fee = $cms_report['unit_price']*($commerce_fee_per/100);
        $total_price    = $cms_report['unit_price'] - $cms_report_fee;

        if(in_array($sch_brand, $global_brand_option)){
            $cms_report['c_no'] = $sch_brand;
        }

        $sales_tmp_total_list[$cms_report['c_no']][$cms_report['key_date']][$cms_report['sales_date']] += $total_price;

        if(in_array($cms_report['dp_c_no'], $dp_self_imweb_list)){
            $cost_tmp_total_list[$cms_report['c_no']][$cms_report['key_date']][$cms_report['sales_date']] += $cms_report['coupon_price'];
        }

        if($cms_report['sales_date'] >= 20240101)
        {
            if ($cms_report['unit_delivery_price'] > 0) {
                $cms_delivery_chk_fee_list[$cms_report['order_number']] = 1;
            }
            elseif($cms_report['unit_delivery_price'] == 0 && $cms_report['unit_price'] > 0)
            {
                $cms_delivery_chk_list[$cms_report['order_number']][$cms_report['dp_c_no']][$cms_report['key_date']][$cms_report['sales_date']] = $cms_report['deli_cnt'];
                $cms_delivery_chk_ord_list[$cms_report['order_number']][$cms_report['c_no']] = $cms_report['c_no'];

                if ($cms_report['c_no'] == '2827' || $cms_report['c_no'] == '4878') {
                    $cms_delivery_chk_nuzam_list[$cms_report['order_number']] = 1;
                }
            }
        }

        $cms_prd_name_list[$cms_report['prd_no']]       = $cms_report['prd_name'];
        $cms_prd_tmp_total_list[$cms_report['prd_no']] += $cms_report['unit_price'];

        if(!isset($cms_prd_tmp_list[$cms_report['prd_no']])){
            foreach($commerce_date_list as $key_date => $key_title){
                $cms_prd_tmp_list[$cms_report['prd_no']][$key_date] = 0;
            }
        }

        $cms_prd_tmp_list[$cms_report['prd_no']][$cms_report['key_date']] += $cms_report['unit_price'];
    }
}
arsort($cms_prd_tmp_total_list);

$cms_best_idx = 0;
foreach($cms_prd_tmp_total_list as $tmp_prd_no => $total_price){
    if($cms_best_idx > 39){
        break;
    }

    foreach($commerce_date_list as $key_date => $key_label){
        $cms_prd_best_list[$tmp_prd_no][$key_date] = $cms_prd_tmp_list[$tmp_prd_no][$key_date];
    }

    $cms_best_idx++;
}
$cms_prd_best_cnt = count($commerce_date_list)+2;

# 무료 배송비 계산
foreach($cms_delivery_chk_list as $chk_ord => $chk_ord_data)
{
    if(isset($cms_delivery_chk_fee_list[$chk_ord])){
        continue;
    }

    $chk_delivery_price = isset($cms_delivery_chk_nuzam_list[$chk_ord]) ? 5000 : 3000;

    foreach ($chk_ord_data as $chk_dp_c_no => $chk_dp_data)
    {
        if(in_array($chk_dp_c_no, $global_dp_total_list)){
            $chk_delivery_price = 6000;
        }

        foreach ($chk_dp_data as $chk_key_date => $chk_key_data)
        {
            foreach ($chk_key_data as $chk_sales_date => $deli_cnt)
            {
                $cal_delivery_price     = $chk_delivery_price * $deli_cnt;

                $chk_brand_deli_price   = $cal_delivery_price;
                $chk_brand_cnt          = count($cms_delivery_chk_ord_list[$chk_ord]);
                $cal_brand_deli_price   = round($cal_delivery_price/$chk_brand_cnt);
                $cal_brand_idx          = 1;
                foreach($cms_delivery_chk_ord_list[$chk_ord] as $chk_c_no)
                {
                    $cal_one_delivery_price  = $cal_brand_deli_price;
                    $chk_brand_deli_price   -= $cal_one_delivery_price;

                    if($cal_brand_idx == $chk_brand_cnt){
                        $cal_one_delivery_price += $chk_brand_deli_price;
                    }

                    $cost_tmp_total_list[$chk_c_no][$chk_key_date][$chk_sales_date] += $cal_one_delivery_price;

                    $cal_brand_idx++;
                }
            }
        }
    }
}

# 입/출고요청 리스트
$quick_report_sql      = "
    SELECT 
        c_no,
        run_c_no,
        run_c_name,
        order_number,
        DATE_FORMAT(run_date, '%Y%m%d') as sales_date,
        {$add_quick_column} as key_date, 
        unit_price
    FROM work_cms_quick as w
    WHERE {$add_quick_where}
";
$quick_report_query = mysqli_query($my_db, $quick_report_sql);
while($quick_report_result = mysqli_fetch_assoc($quick_report_query))
{
    if(in_array($sch_brand, $global_brand_option)){
        $quick_report_result['c_no'] = $sch_brand;
    }

    $sales_tmp_total_list[$quick_report_result['c_no']][$quick_report_result['key_date']][$quick_report_result['sales_date']] += $quick_report_result['unit_price'];
}

# NOSP 계산
$nosp_result_sql = "
        SELECT
        `ar`.am_no,
        `ar`.brand,
        `am`.product,
        `am`.fee_per,
        `am`.price,
        `ar`.impressions,
        `ar`.click_cnt,
        `ar`.adv_price,
        DATE_FORMAT(am.adv_s_date, '%Y%m%d') as adv_s_day,
        {$add_result_column} as key_date
    FROM advertising_result `ar`
    LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
    WHERE {$add_result_where}
    ORDER BY `am`.adv_s_date ASC
";
$nosp_result_query      = mysqli_query($my_db, $nosp_result_sql);
$nosp_result_tmp_list   = [];
$nosp_result_chk_list   = [];
while($nosp_result = mysqli_fetch_assoc($nosp_result_query))
{
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["product"]          = $nosp_result['product'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["sales_date"]       = $nosp_result['adv_s_day'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["key_date"]         = $nosp_result['key_date'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["fee_per"]          = $nosp_result['fee_per'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_adv_price"] += $nosp_result['adv_price'];
}

foreach($nosp_result_tmp_list as $am_no => $nosp_result_tmp)
{
    foreach($nosp_result_tmp as $brand => $nosp_brand_data)
    {
        $fee_price      = $nosp_brand_data['total_adv_price']*($nosp_brand_data['fee_per']/100);
        $cal_price      = $nosp_brand_data["total_adv_price"]-$fee_price;
        $cal_price_vat  = $cal_price*1.1;

        $cost_tmp_total_list[$brand][$nosp_brand_data['key_date']][$nosp_brand_data['sales_date']]  += $cal_price_vat;

        $chk_brand  = "e_total";
        if(array_search($brand, $nosp_df_option) !== false){
            $chk_brand = "df_total";
        }elseif(array_search($brand, $nosp_nuzam_option) !== false){
            $chk_brand = "n_total";
        }

        $nosp_result_list[$nosp_brand_data['key_date']][$nosp_brand_data['product']]['total']       += $cal_price_vat;
        $nosp_result_list[$nosp_brand_data['key_date']][$nosp_brand_data['product']][$chk_brand]    += $cal_price_vat;

        if(!isset($nosp_result_chk_list[$am_no])){
            $nosp_result_chk_list[$am_no] = 1;
            $nosp_result_list[$nosp_brand_data['key_date']][$nosp_brand_data['product']]['count']++;
            $nosp_result_total++;
        }
    }
}

foreach($brand_company_list as $c_no)
{
    # NET 매출 관련 작업
    $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$sch_net_date}' ORDER BY sales_date DESC LIMIT 1;";
    $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
    $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
    $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
    $net_parent             = isset($net_pre_percent_result['sales_date']) ? $net_pre_percent_result['sales_date'] : 0;

    $net_parent_percent_list[$c_no] = $net_pre_percent;

    $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date, {$add_date_column} as key_date FROM commerce_report_net WHERE brand='{$c_no}' {$add_date_where} ORDER BY sales_date ASC";
    $net_report_query       = mysqli_query($my_db, $net_report_sql);
    while($net_report = mysqli_fetch_assoc($net_report_query))
    {
        $net_report_date_list[$c_no][$net_report['key_date']][$net_report['sales_date']] = $net_report['percent'];
    }
}

foreach($net_report_date_list as $c_no => $net_company_data)
{
    $net_pre_percent = $net_parent_percent_list[$c_no];
    $parent_key      = $company_chk_list[$c_no];

    foreach($net_company_data as $key_date => $net_report_data)
    {
        foreach($net_report_data as $sales_date => $net_percent_val)
        {
            if($net_percent_val > 0 ){
                $net_pre_percent = $net_percent_val;
            }

            $net_percent = $net_pre_percent/100;
            $sales_total = round($sales_tmp_total_list[$c_no][$key_date][$sales_date],0);
            $cost_total  = $cost_tmp_total_list[$c_no][$key_date][$sales_date];
            $net_price   = $sales_total > 0 ? (int)($sales_total*$net_percent) : 0;
            $profit      = $cost_total > 0 ? ($net_price-$cost_total) : $net_price;

            $total_report_table_list[$parent_key][$key_date]['sales']     += $sales_total;
            $total_report_table_list[$parent_key][$key_date]['cost']      += $cost_total;
            $total_report_table_list[$parent_key][$key_date]['profit']    += $profit;

            $total_report_date_list[$key_date]['sales']     += $sales_total;
            $total_report_date_list[$key_date]['cost']      += $cost_total;
            $total_report_date_list[$key_date]['profit']    += $profit;

            if($sch_is_rate != "1" || $key_date != $sch_rate_date)
            {
                $total_report_brand_list[$parent_key]['sales']  += $sales_total;
                $total_report_brand_list[$parent_key]['cost']   += $cost_total;
                $total_report_brand_list[$parent_key]['profit'] += $profit;
            }
        }
    }
}


if($sch_is_rate == "1" && !empty($sch_rate_date))
{
    $prev_data_list = [];
    foreach($total_report_table_list as $group_key => $group_data)
    {
        foreach($group_data as $key_date => $date_data)
        {
            if($key_date == "title"){
                continue;
            } elseif($key_date == $sch_rate_date){
                $prev_data_list[$group_key] = $date_data;
                continue;
            }

            $prev_data          = $prev_data_list[$group_key];
            $prev_sales_per     = ($prev_data['sales'] == 0 || $date_data['sales'] == 0) ? 0 : $prev_data['sales']/100;
            $prev_cost_per      = ($prev_data['cost'] == 0 || $date_data['cost'] == 0) ? 0 : $prev_data['cost']/100;
            $prev_profit_per    = ($prev_data['profit'] == 0 || $date_data['profit'] == 0) ? 0 : $prev_data['profit']/100;

            $sales_tmp  = $date_data['sales']-$prev_data['sales'];
            $sales_per  = ($prev_sales_per == 0) ? 0 : round($sales_tmp/$prev_sales_per, 1);
            $cost_tmp   = ($date_data['cost']*-1)-($prev_data['cost']*-1);
            $cost_per   = ($prev_cost_per == 0) ? 0 : round($cost_tmp/$prev_cost_per, 1);
            $profit_tmp = $date_data['profit']-$prev_data['profit'];

            if($prev_data['profit'] < 0 && $date_data['profit'] > 0){
                $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1)*-1;
            }elseif($prev_data['profit'] < 0 && $date_data['profit'] < 0){
                $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1)*-1;
            }else{
                $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1);
            }

            $total_report_table_list[$group_key][$key_date]["sales_per"]    = $sales_per;
            $total_report_table_list[$group_key][$key_date]["cost_per"]     = $cost_per;
            $total_report_table_list[$group_key][$key_date]["profit_per"]   = $profit_per;

            $prev_data_list[$group_key] = $date_data;
        }
    }

    $prev_date_data = [];
    foreach($total_report_date_list as $key_date => $date_data)
    {
        if($key_date == $sch_rate_date){
            $prev_date_data = $date_data;
            continue;
        }

        $prev_sales_per     = ($prev_date_data['sales'] == 0 || $date_data['sales'] == 0) ? 0 : $prev_date_data['sales']/100;
        $prev_cost_per      = ($prev_date_data['cost'] == 0 || $date_data['cost'] == 0) ? 0 : $prev_date_data['cost']/100;
        $prev_profit_per    = ($prev_date_data['profit'] == 0 || $date_data['profit'] == 0) ? 0 : $prev_date_data['profit']/100;

        $sales_tmp  = $date_data['sales']-$prev_date_data['sales'];
        $sales_per  = ($prev_sales_per == 0) ? 0 : round($sales_tmp/$prev_sales_per, 1);
        $cost_tmp   = ($date_data['cost']*-1)-($prev_date_data['cost']*-1);
        $cost_per   = ($prev_cost_per == 0) ? 0 : round($cost_tmp/$prev_cost_per, 1);
        $profit_tmp = $date_data['profit']-$prev_date_data['profit'];

        if($prev_date_data['profit'] < 0 && $date_data['profit'] > 0){
            $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1)*-1;
        }elseif($prev_date_data['profit'] < 0 && $date_data['profit'] < 0){
            $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1)*-1;
        }else{
            $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1);
        }

        $total_report_date_list[$key_date]["sales_per"]  = $sales_per;
        $total_report_date_list[$key_date]["cost_per"]   = $cost_per;
        $total_report_date_list[$key_date]["profit_per"] = $profit_per;

        $prev_date_data = $date_data;
    }

    unset($commerce_date_list[$sch_rate_date]);
    unset($total_report_date_list[$sch_rate_date]);
}

$brand_chart_name_list      = array_reverse($brand_column_list);
$total_report_brand_list    = array_reverse($total_report_brand_list);
$brand_chart_list           = [];
foreach($total_report_brand_list as $brand_key => $brand_data) {
    $brand_chart_list['sales_data']['title']    = "매출";
    $brand_chart_list['sales_data']['color']    = "rgba(0,0,255,0.8)";
    $brand_chart_list['sales_data']['data'][]   = $brand_data['sales'];
    $brand_chart_list['cost_data']['title']     = "비용";
    $brand_chart_list['cost_data']['color']     = "rgba(255,0,0,0.8)";
    $brand_chart_list['cost_data']['data'][]    = $brand_data['cost'];
    $brand_chart_list['profit_data']['title']   = "공헌이익";
    $brand_chart_list['profit_data']['color']   = "rgba(0,0,0,0.8)";
    $brand_chart_list['profit_data']['data'][]  = $brand_data['profit'];
}

$date_chart_name_list  = $commerce_date_list;
$date_chart_list       = [];
foreach($total_report_date_list as $date_key => $date_data)
{
    $date_chart_list['sales_data']['title']    = "매출";
    $date_chart_list['sales_data']['color']    = "rgba(0,0,255,0.8)";
    $date_chart_list['sales_data']['data'][]   = $date_data['sales'];
    $date_chart_list['cost_data']['title']     = "비용";
    $date_chart_list['cost_data']['color']     = "rgba(255,0,0,0.8)";
    $date_chart_list['cost_data']['data'][]    = $date_data['cost'];
    $date_chart_list['profit_data']['title']   = "공헌이익";
    $date_chart_list['profit_data']['color']   = "rgba(0,0,0,0.8)";
    $date_chart_list['profit_data']['data'][]  = $date_data['profit'];
}

if($sch_is_rate == "2" && $sch_date_type == "3")
{
    $prev_s_month           = date("Y-m", strtotime("{$sch_s_month} -1 years"));
    $prev_e_month           = date("Y-m", strtotime("{$sch_e_month} -1 years"));
    $prev_e_day             = date("t", strtotime($prev_e_month));;
    $prev_s_datetime        = $prev_s_month."-01 00:00:00";
    $prev_e_datetime        = $prev_e_month."-{$prev_e_day} 23:59:59";
    $prev_net_date          = $prev_s_month."-01";

    $prev_all_date_where    = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$prev_s_month}' AND '{$prev_e_month}'";
    $prev_comm_where        = "display='1' AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$prev_s_month}' AND '{$prev_e_month}'";
    $prev_net_where         = "DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$prev_s_month}' AND '{$prev_e_month}'";
    $prev_result_where      = "`am`.state IN(3,5) AND `am`.product IN(1,2) AND `am`.media='265' AND am.adv_s_date BETWEEN '{$prev_s_datetime}' AND '{$prev_e_datetime}'";

    if(!empty($sch_brand))
    {
        $prev_comm_where    .= " AND brand='{$sch_brand}'";
        $prev_result_where  .= " AND `ar`.brand='{$sch_brand}'";
    }
    elseif(!empty($sch_brand_g2)){
        $prev_comm_where    .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
        $prev_result_where  .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    }
    elseif(!empty($sch_brand_g1))
    {
        $prev_comm_where    .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
        $prev_result_where  .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    }

    # 전년도 사용할 변수
    $prev_sales_tmp_total_list      = [];
    $prev_cost_tmp_total_list       = [];
    $prev_net_report_date_list      = [];
    $prev_net_parent_percent_list   = [];
    $prev_total_all_date_list       = [];
    $prev_total_report_table_list   = [];
    $prev_total_report_date_list    = [];

    $prev_all_date_sql = "
        SELECT
            {$all_date_title} as chart_title,
            {$all_date_key} as chart_key,
            DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
        FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
        as allday
        WHERE {$prev_all_date_where}
        ORDER BY chart_key, date_key
    ";
    $prev_all_date_query = mysqli_query($my_db, $prev_all_date_sql);
    while($prev_date = mysqli_fetch_array($prev_all_date_query))
    {
        $prev_total_all_date_list[$prev_date['date_key']] = array("s_date" => date("Y-m-d",strtotime($prev_date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($prev_date['date_key']))." 23:59:59");

        foreach($brand_company_list as $c_no)
        {
            $prev_cost_tmp_total_list[$c_no][$prev_date['chart_key']][$prev_date['date_key']]  = 0;
            $prev_sales_tmp_total_list[$c_no][$prev_date['chart_key']][$prev_date['date_key']] = 0;
            $prev_net_report_date_list[$c_no][$prev_date['chart_key']][$prev_date['date_key']] = 0;
            $prev_et_parent_percent_list[$c_no] = 0;
        }

        foreach($brand_column_list as $kind_key => $kind_title)
        {
            $prev_total_report_table_list[$kind_key][$prev_date['chart_key']] = array(
                "sales"     => 0,
                "cost"      => 0,
                "profit"    => 0
            );
        }

        $prev_total_report_date_list[$date['chart_key']] = array(
            "sales"     => 0,
            "cost"      => 0,
            "profit"    => 0
        );
    }

    # 수기 데이터
    $prev_commerce_sql  = "
        SELECT
            cr.`type`,
            cr.price as price,
            cr.brand,
            DATE_FORMAT(sales_date, '%Y%m%d') as sales_date,
            {$add_date_column} as key_date
        FROM commerce_report `cr`
        WHERE {$prev_comm_where}
        ORDER BY key_date ASC
    ";
    $prev_commerce_query = mysqli_query($my_db, $prev_commerce_sql);
    while($prev_commerce = mysqli_fetch_assoc($prev_commerce_query))
    {
        if($prev_commerce['type'] == 'cost'){
            $prev_cost_tmp_total_list[$prev_commerce['brand']][$prev_commerce['key_date']][$prev_commerce['sales_date']] += $prev_commerce['price'];
        }elseif($prev_commerce['type'] == 'sales'){
            $prev_sales_tmp_total_list[$prev_commerce['brand']][$prev_commerce['key_date']][$prev_commerce['sales_date']] += $prev_commerce['price'];
        }
    }

    $prev_cms_delivery_chk_fee_list   = [];
    $prev_cms_delivery_chk_nuzam_list = [];
    $prev_cms_delivery_chk_ord_list   = [];
    $prev_cms_delivery_chk_list       = [];

    foreach($prev_total_all_date_list as $prev_date_data)
    {
        $prev_cms_report_sql      = "
            SELECT
                c_no,
                dp_c_no,
                order_number,
                DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
                {$add_cms_column} as key_date, 
                unit_price,
                (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
                unit_delivery_price,
                (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
            FROM work_cms as w
            WHERE {$add_cms_where} AND (order_date BETWEEN '{$prev_date_data['s_date']}' AND '{$prev_date_data['e_date']}') 
        ";
        $prev_cms_report_query = mysqli_query($my_db, $prev_cms_report_sql);
        while($prev_cms_report = mysqli_fetch_assoc($prev_cms_report_query))
        {
            $prev_commerce_fee_total_list    = isset($commerce_fee_option[$prev_cms_report['c_no']]) ? $commerce_fee_option[$prev_cms_report['c_no']] : [];
            $prev_commerce_fee_per_list      = !empty($prev_commerce_fee_total_list) && isset($prev_commerce_fee_total_list[$prev_cms_report['dp_c_no']]) ? $prev_commerce_fee_total_list[$prev_cms_report['dp_c_no']] : 0;
            $prev_commerce_fee_per           = 0;

            if(!empty($prev_commerce_fee_per_list))
            {
                foreach($prev_commerce_fee_per_list as $prev_fee_date => $prev_fee_val){
                    if($prev_cms_report['sales_date'] >= $prev_fee_date){
                        $prev_commerce_fee_per = $prev_fee_val;
                    }
                }
            }

            $prev_cms_report_fee = $prev_cms_report['unit_price']*($prev_commerce_fee_per/100);
            $prev_total_price    = $prev_cms_report['unit_price'] - $prev_cms_report_fee;

            $prev_sales_tmp_total_list[$prev_cms_report['c_no']][$prev_cms_report['key_date']][$prev_cms_report['sales_date']] += $prev_total_price;

            if(in_array($prev_cms_report['dp_c_no'], $dp_self_imweb_list)){
                $prev_cost_tmp_total_list[$prev_cms_report['c_no']][$prev_cms_report['key_date']][$prev_cms_report['sales_date']] += $prev_cms_report['coupon_price'];
            }

            if($prev_cms_report['sales_date'] >= 20240101)
            {
                if ($prev_cms_report['unit_delivery_price'] > 0) {
                    $prev_cms_delivery_chk_fee_list[$prev_cms_report['order_number']] = 1;
                }
                elseif($prev_cms_report['unit_delivery_price'] == 0 && $prev_cms_report['unit_price'] > 0)
                {
                    $prev_cms_delivery_chk_list[$prev_cms_report['order_number']][$prev_cms_report['dp_c_no']][$prev_cms_report['key_date']][$prev_cms_report['sales_date']] = $prev_cms_report['deli_cnt'];
                    $prev_cms_delivery_chk_ord_list[$prev_cms_report['order_number']][$prev_cms_report['c_no']] = $prev_cms_report['c_no'];

                    if ($prev_cms_report['c_no'] == '2827' || $prev_cms_report['c_no'] == '4878') {
                        $prev_cms_delivery_chk_nuzam_list[$prev_cms_report['order_number']] = 1;
                    }
                }
            }
        }
    }

    # 무료 배송비 계산
    foreach($prev_cms_delivery_chk_list as $prev_chk_ord => $prev_chk_ord_data)
    {
        if(isset($prev_cms_delivery_chk_fee_list[$prev_chk_ord])){
            continue;
        }

        $prev_chk_delivery_price = isset($prev_cms_delivery_chk_nuzam_list[$prev_chk_ord]) ? 5000 : 3000;

        foreach ($prev_chk_ord_data as $prev_chk_dp_c_no => $prev_chk_dp_data)
        {
            if(in_array($prev_chk_dp_c_no, $global_dp_company_list)){
                $chk_delivery_price = 6000;
            }

            foreach ($prev_chk_dp_data as $prev_chk_key_date => $prev_chk_key_data)
            {
                foreach ($prev_chk_key_data as $prev_chk_sales_date => $prev_deli_cnt)
                {
                    $prev_cal_delivery_price     = $prev_chk_delivery_price * $prev_deli_cnt;

                    $prev_chk_brand_deli_price   = $prev_cal_delivery_price;
                    $prev_chk_brand_cnt          = count($prev_cms_delivery_chk_ord_list[$prev_chk_ord]);
                    $prev_cal_brand_deli_price   = round($prev_cal_delivery_price/$prev_chk_brand_cnt);
                    $prev_cal_brand_idx          = 1;
                    foreach($prev_cms_delivery_chk_ord_list[$prev_chk_ord] as $chk_c_no)
                    {
                        $prev_cal_one_delivery_price  = $prev_cal_brand_deli_price;
                        $prev_chk_brand_deli_price   -= $prev_cal_one_delivery_price;

                        if($prev_cal_brand_idx == $prev_chk_brand_cnt){
                            $prev_cal_one_delivery_price += $prev_chk_brand_deli_price;
                        }

                        $prev_cost_tmp_total_list[$chk_c_no][$prev_chk_key_date][$prev_chk_sales_date] += $prev_cal_one_delivery_price;

                        $prev_cal_brand_idx++;
                    }
                }
            }
        }
    }

    # NOSP 계산
    $prev_nosp_result_sql = "
        SELECT
            `ar`.am_no,
            `ar`.brand,
            `am`.product,
            `am`.fee_per,
            `am`.price,
            `ar`.impressions,
            `ar`.click_cnt,
            `ar`.adv_price,
            DATE_FORMAT(am.adv_s_date, '%Y%m%d') as adv_s_day,
            {$add_result_column} as key_date
        FROM advertising_result `ar`
        LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
        WHERE {$prev_result_where}
        ORDER BY `am`.adv_s_date ASC
    ";
    $prev_nosp_result_query       = mysqli_query($my_db, $prev_nosp_result_sql);
    $prev_nosp_result_tmp_list   = [];
    while($prev_nosp_result = mysqli_fetch_assoc($prev_nosp_result_query))
    {
        $prev_nosp_result_tmp_list[$prev_nosp_result['am_no']][$prev_nosp_result['brand']]["product"]          = $prev_nosp_result['product'];
        $prev_nosp_result_tmp_list[$prev_nosp_result['am_no']][$prev_nosp_result['brand']]["sales_date"]       = $prev_nosp_result['adv_s_day'];
        $prev_nosp_result_tmp_list[$prev_nosp_result['am_no']][$prev_nosp_result['brand']]["key_date"]         = $prev_nosp_result['key_date'];
        $prev_nosp_result_tmp_list[$prev_nosp_result['am_no']][$prev_nosp_result['brand']]["fee_per"]          = $prev_nosp_result['fee_per'];
        $prev_nosp_result_tmp_list[$prev_nosp_result['am_no']][$prev_nosp_result['brand']]["total_adv_price"] += $prev_nosp_result['adv_price'];
    }

    foreach($prev_nosp_result_tmp_list as $prev_am_no => $prev_nosp_result_tmp)
    {
        foreach($prev_nosp_result_tmp as $prev_brand => $prev_nosp_brand_data)
        {
            $prev_fee_price      = $prev_nosp_brand_data['total_adv_price']*($prev_nosp_brand_data['fee_per']/100);
            $prev_cal_price      = $prev_nosp_brand_data["total_adv_price"]-$prev_fee_price;
            $prev_cal_price_vat  = $prev_cal_price*1.1;

            $prev_cost_tmp_total_list[$prev_brand][$prev_nosp_brand_data['key_date']][$prev_nosp_brand_data['sales_date']] += $prev_cal_price_vat;
        }
    }

    foreach($brand_company_list as $c_no)
    {
        # NET 매출 관련 작업
        $prev_net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$prev_net_date}' ORDER BY sales_date DESC LIMIT 1;";
        $prev_net_pre_percent_query  = mysqli_query($my_db, $prev_net_pre_percent_sql);
        $prev_net_pre_percent_result = mysqli_fetch_assoc($prev_net_pre_percent_query);
        $prev_net_pre_percent        = isset($prev_net_pre_percent_result['percent']) ? $prev_net_pre_percent_result['percent'] : 0;
        $prev_net_parent             = isset($prev_net_pre_percent_result['sales_date']) ? $prev_net_pre_percent_result['sales_date'] : 0;

        $prev_net_parent_percent_list[$c_no] = $prev_net_pre_percent;

        $prev_net_report_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date, {$add_date_column} as key_date FROM commerce_report_net WHERE {$prev_net_where} AND brand='{$c_no}' ORDER BY sales_date ASC";
        $prev_net_report_query  = mysqli_query($my_db, $prev_net_report_sql);
        while($prev_net_report  = mysqli_fetch_assoc($prev_net_report_query))
        {
            $prev_net_report_date_list[$c_no][$prev_net_report['key_date']][$prev_net_report['sales_date']] = $prev_net_report['percent'];
        }
    }

    foreach($prev_net_report_date_list as $prev_c_no => $prev_net_company_data)
    {
        $prev_cal_net_percent = $prev_net_parent_percent_list[$prev_c_no];
        $prev_parent_key      = $company_chk_list[$prev_c_no];

        foreach($prev_net_company_data as $prev_key_date => $prev_net_report_data)
        {
            foreach($prev_net_report_data as $prev_sales_date => $prev_net_percent_val)
            {
                if($prev_net_percent_val > 0 ){
                    $prev_cal_net_percent = $prev_net_percent_val;
                }

                $prev_net_percent = $prev_cal_net_percent/100;
                $prev_sales_total = round($prev_sales_tmp_total_list[$prev_c_no][$prev_key_date][$prev_sales_date],0);
                $prev_cost_total  = $prev_cost_tmp_total_list[$prev_c_no][$prev_key_date][$prev_sales_date];
                $prev_net_price   = $prev_sales_total > 0 ? (int)($prev_sales_total*$prev_net_percent) : 0;
                $prev_profit      = $prev_cost_total > 0 ? ($prev_net_price-$prev_cost_total) : $prev_net_price;

                $prev_total_report_table_list[$prev_parent_key][$prev_key_date]['sales']  += $prev_sales_total;
                $prev_total_report_table_list[$prev_parent_key][$prev_key_date]['cost']   += $prev_cost_total;
                $prev_total_report_table_list[$prev_parent_key][$prev_key_date]['profit'] += $prev_profit;

                $prev_total_report_date_list[$prev_key_date]['sales']     += $prev_sales_total;
                $prev_total_report_date_list[$prev_key_date]['cost']      += $prev_cost_total;
                $prev_total_report_date_list[$prev_key_date]['profit']    += $prev_profit;
            }
        }
    }

    foreach($total_report_table_list as $group_key => $group_data)
    {
        foreach($group_data as $key_date => $date_data)
        {
            if($key_date == "title"){
                continue;
            }

            $cur_date_label     = $commerce_date_list[$key_date];
            $cur_date_title     = str_replace(".","-", $cur_date_label);
            $prev_date          = date("Ym", strtotime("{$cur_date_title} -1 years"));

            $prev_data          = $prev_total_report_table_list[$group_key][$prev_date];
            $prev_sales_per     = ($prev_data['sales'] == 0 || $date_data['sales'] == 0) ? 0 : $prev_data['sales']/100;
            $prev_cost_per      = ($prev_data['cost'] == 0 || $date_data['cost'] == 0) ? 0 : $prev_data['cost']/100;
            $prev_profit_per    = ($prev_data['profit'] == 0 || $date_data['profit'] == 0) ? 0 : $prev_data['profit']/100;

            $sales_tmp          = $date_data['sales']-$prev_data['sales'];
            $sales_per          = ($prev_sales_per == 0) ? 0 : round($sales_tmp/$prev_sales_per, 1);
            $cost_tmp           = ($date_data['cost']*-1)-($prev_data['cost']*-1);
            $cost_per           = ($prev_cost_per == 0) ? 0 : round($cost_tmp/$prev_cost_per, 1);
            $profit_tmp         = $date_data['profit']-$prev_data['profit'];

            if($prev_data['profit'] < 0 && $date_data['profit'] > 0){
                $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1)*-1;
            }elseif($prev_data['profit'] < 0 && $date_data['profit'] < 0){
                $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1)*-1;
            }else{
                $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1);
            }

            $total_report_table_list[$group_key][$key_date]["sales_per"]    = $sales_per;
            $total_report_table_list[$group_key][$key_date]["cost_per"]     = $cost_per;
            $total_report_table_list[$group_key][$key_date]["profit_per"]   = $profit_per;
        }
    }

    foreach($total_report_date_list as $key_date => $date_data)
    {
        $cur_date_label     = $commerce_date_list[$key_date];
        $cur_date_title     = str_replace(".","-", $cur_date_label);
        $prev_date          = date("Ym", strtotime("{$cur_date_title} -1 years"));

        $prev_data          = $prev_total_report_date_list[$prev_date];
        $prev_sales_per     = ($prev_data['sales'] == 0 || $date_data['sales'] == 0) ? 0 : $prev_data['sales']/100;
        $prev_cost_per      = ($prev_data['cost'] == 0 || $date_data['cost'] == 0) ? 0 : $prev_data['cost']/100;
        $prev_profit_per    = ($prev_data['profit'] == 0 || $date_data['profit'] == 0) ? 0 : $prev_data['profit']/100;

        $sales_tmp          = $date_data['sales']-$prev_data['sales'];
        $sales_per          = ($prev_sales_per == 0) ? 0 : round($sales_tmp/$prev_sales_per, 1);
        $cost_tmp           = ($date_data['cost']*-1)-($prev_data['cost']*-1);
        $cost_per           = ($prev_cost_per == 0) ? 0 : round($cost_tmp/$prev_cost_per, 1);
        $profit_tmp         = $date_data['profit']-$prev_data['profit'];

        if($prev_data['profit'] < 0 && $date_data['profit'] > 0){
            $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1)*-1;
        }elseif($prev_data['profit'] < 0 && $date_data['profit'] < 0){
            $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1)*-1;
        }else{
            $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1);
        }

        $total_report_date_list[$key_date]["sales_per"]    = $sales_per;
        $total_report_date_list[$key_date]["cost_per"]     = $cost_per;
        $total_report_date_list[$key_date]["profit_per"]   = $profit_per;
    }
}

# 요일/시간별 판매실적 현황/비교
$prev_date                  = date('Y-m-d', strtotime("-1 days"));
$daily_s_date               = date("Y-m-d", strtotime("{$prev_date} -6 days"));
$daily_e_date               = $prev_date;
$daily_s_datetime           = "{$daily_s_date} 00:00:00";
$daily_e_datetime           = "{$daily_e_date} 23:59:59";
$daily_s_date_tmp           = date("m/d_w", strtotime($daily_s_date));
$daily_e_date_tmp           = date("m/d_w", strtotime($daily_e_date));

$daily_s_date_convert       = explode('_', $daily_s_date_tmp);
$daily_s_date_name          = $date_name_option[$daily_s_date_convert[1]];
$daily_s_date_text          = $daily_s_date_convert[0]."({$daily_s_date_name})";
$daily_e_date_convert       = explode('_', $daily_e_date_tmp);
$daily_e_date_name          = $date_name_option[$daily_e_date_convert[1]];
$daily_e_date_text          = $daily_e_date_convert[0]."({$daily_e_date_name})";

$prev_daily_s_date          = date("Y-m-d", strtotime("{$daily_s_date} -1 weeks"));
$prev_daily_e_date          = date("Y-m-d", strtotime("{$daily_e_date} -1 weeks"));
$prev_daily_s_datetime      = "{$prev_daily_s_date} 00:00:00";
$prev_daily_e_datetime      = "{$prev_daily_e_date} 23:59:59";
$prev_daily_s_date_tmp      = date("m/d_w", strtotime($prev_daily_s_date));
$prev_daily_e_date_tmp      = date("m/d_w", strtotime($prev_daily_e_date));

$prev_daily_s_date_convert  = explode('_', $prev_daily_s_date_tmp);
$prev_daily_s_date_name     = $date_name_option[$prev_daily_s_date_convert[1]];
$prev_daily_s_date_text     = $prev_daily_s_date_convert[0]."({$prev_daily_s_date_name})";
$prev_daily_e_date_convert  = explode('_', $prev_daily_e_date_tmp);
$prev_daily_e_date_name     = $date_name_option[$prev_daily_e_date_convert[1]];
$prev_daily_e_date_text     = $prev_daily_e_date_convert[0]."({$prev_daily_e_date_name})";

$daily_date_list            = [];
$daily_hour_list            = [];
$daily_sales_list           = [];
$daily_date_total_list      = [];
$daily_hour_total_list      = array("max" => 0);
$daily_sales_total_price    = 0;

$sales_term_hour_list       = [];
$sales_term_date_list       = [];
$sales_term_total_list      = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0, "hour_base_max" => 0, "hour_comp_max" => 0, "date_base_max" => 0, "date_comp_max" => 0, "date_base_min" => 0, "date_comp_min" => 0);
$sales_term_hour_total_list = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0);

foreach($daily_hour_option as $hour){
    $hour_key = (int)$hour;
    $daily_hour_list[$hour_key]     = "{$hour}시";
    $sales_term_hour_list[$hour_key]= array("base_price" => 0,"comp_price" => 0, "comp_per" => 0);
}

foreach($date_name_option as $date_key => $date_title)
{
    $daily_date_list[$date_key]         = $date_title;
    $daily_sales_list[$date_key]['max'] = 0;
    $daily_date_total_list[$date_key]   = 0;
    $sales_term_date_list[$date_key]    = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0);

    foreach($daily_hour_list as $hour_key){
        $daily_sales_list[$date_key][$hour_key] = 0;
        $daily_hour_total_list[$hour_key]       = 0;
    }
}

$add_sales_daily_where      = $self_cms_base_where." AND (order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}')";
$add_sales_term_where       = $self_cms_base_where." AND (order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}' OR order_date BETWEEN '{$prev_daily_s_datetime}' AND '{$prev_daily_e_datetime}')";

# 요일/시간별 판매실적 현황 쿼리
$daily_sales_sql  = "
        SELECT
            DATE_FORMAT(w.order_date, '%w') as date_key,
            DATE_FORMAT(w.order_date, '%H') as hour_key,
            SUM(unit_price) as total_price
        FROM work_cms w 
        WHERE {$add_sales_daily_where}
        GROUP BY date_key, hour_key
    ";
$daily_sales_query    = mysqli_query($my_db, $daily_sales_sql);
while($daily_sales_result = mysqli_fetch_assoc($daily_sales_query))
{
    $daily_date_key     = $daily_sales_result['date_key'];
    $daily_hour_key     = (int)$daily_sales_result['hour_key'];
    $daily_total_price  = (int)$daily_sales_result['total_price'];
    $daily_max          = $daily_sales_list[$daily_date_key]["max"];

    $daily_date_total_list[$daily_date_key] += $daily_total_price;
    $daily_hour_total_list[$daily_hour_key] += $daily_total_price;
    $daily_sales_total_price                += $daily_total_price;

    $daily_sales_list[$daily_date_key][$daily_hour_key] += $daily_total_price;
    if($daily_max < $daily_total_price){
        $daily_sales_list[$daily_date_key]["max"] = $daily_total_price;
    }
}

$daily_date_total_list["min"] = min($daily_date_total_list);
$daily_date_total_list["max"] = max($daily_date_total_list);
$daily_hour_total_list["max"] = max($daily_hour_total_list);

# 요일/시간별 판매실적 비교 쿼리
$term_sales_sql    = "

        SELECT    
            DATE_FORMAT(w.order_date, '%w') as date_key,
            DATE_FORMAT(w.order_date, '%H') as hour_key,
            SUM(
                IF(order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}',unit_price, 0)
            ) AS base_price,
            SUM(
                IF(order_date BETWEEN '{$prev_daily_s_datetime}' AND '{$prev_daily_e_datetime}',unit_price, 0)
            ) AS comp_price
        FROM work_cms w 
        WHERE {$add_sales_term_where}
        GROUP BY date_key, hour_key
        ORDER BY order_date
    ";
$term_sales_query = mysqli_query($my_db, $term_sales_sql);
while($term_sales = mysqli_fetch_assoc($term_sales_query))
{
    $hour_key   = (int)$term_sales['hour_key'];
    $date_key   = (int)$term_sales['date_key'];
    $base_price = $term_sales['base_price'];
    $comp_price = $term_sales['comp_price'];

    $sales_term_hour_list[$hour_key]["base_price"] += $base_price;
    $sales_term_hour_list[$hour_key]["comp_price"] += $comp_price;
    $sales_term_date_list[$date_key]["base_price"] += $base_price;
    $sales_term_date_list[$date_key]["comp_price"] += $comp_price;

    $sales_term_total_list["base_price"] += $base_price;
    $sales_term_total_list["comp_price"] += $comp_price;
}

if(!empty($sales_term_total_list)){
    $total_comp_per = ($sales_term_total_list["comp_price"] == 0) ? 0 : (($sales_term_total_list["base_price"] == 0) ? -100 : ROUND( ((($sales_term_total_list["base_price"]-$sales_term_total_list["comp_price"])/$sales_term_total_list["comp_price"]) *100), 1));
    $sales_term_total_list["comp_per"] = $total_comp_per;
}

if(!empty($sales_term_hour_list))
{
    foreach($sales_term_hour_list as $hour_key => $hour_data)
    {
        $hour_base_price    = $hour_data["base_price"];
        $hour_comp_price    = $hour_data["comp_price"];
        $hour_base_max      = $sales_term_total_list["hour_base_max"];
        $hour_comp_max      = $sales_term_total_list["hour_comp_max"];

        $hour_comp_per = ($hour_comp_price == 0) ? 0 : (($hour_base_price == 0) ? -100 : ROUND( ((($hour_base_price-$hour_comp_price)/$hour_comp_price) *100), 1));
        $sales_term_hour_list[$hour_key]["comp_per"] = $hour_comp_per;

        if($hour_base_max < $hour_base_price){
            $sales_term_total_list["hour_base_max"] = $hour_base_price;
        }

        if($hour_comp_max < $hour_comp_price){
            $sales_term_total_list["hour_comp_max"] = $hour_comp_price;
        }
    }
}

if(!empty($sales_term_date_list))
{
    $date_idx = 0;
    foreach($sales_term_date_list as $date_key => $date_data)
    {
        $date_base_price    = $date_data["base_price"];
        $date_comp_price    = $date_data["comp_price"];
        $date_base_max      = $sales_term_total_list["date_base_max"];
        $date_base_min      = $sales_term_total_list["date_base_min"];
        $date_comp_max      = $sales_term_total_list["date_comp_max"];
        $date_comp_min      = $sales_term_total_list["date_comp_min"];

        if($date_idx == 0){
            $sales_term_total_list["date_base_min"] = $date_base_price;
            $sales_term_total_list["date_comp_min"] = $date_comp_price;
        }

        $date_comp_per = ($date_comp_price == 0) ? 0 : (($date_base_price == 0) ? -100 : ROUND( ((($date_base_price-$date_comp_price)/$date_comp_price) *100), 1));
        $sales_term_date_list[$date_key]["comp_per"] = $date_comp_per;

        if($date_base_max < $date_base_price){
            $sales_term_total_list["date_base_max"] = $date_base_price;
        }

        if($date_base_min > $date_base_price){
            $sales_term_total_list["date_base_min"] = $date_base_price;
        }

        if($date_comp_max < $date_comp_price){
            $sales_term_total_list["date_comp_max"] = $date_comp_price;
        }

        if($date_comp_min > $date_comp_price){
            $sales_term_total_list["date_comp_min"] = $date_comp_price;
        }

        $date_idx++;
    }
}

# 유입/전환
$convert_sql = "
    SELECT
        `cc`.brand,
        {$add_convert_column} as key_date,
        SUM(`cc`.traffic_count) as traffic_cnt,
        SUM(`cc`.conversion_count) as convert_cnt
    FROM commerce_conversion as `cc`
    WHERE {$add_convert_where}
    GROUP BY key_date
    ORDER BY key_date
";
$convert_query = mysqli_query($my_db, $convert_sql);
while($convert_result = mysqli_fetch_assoc($convert_query))
{
    $traffic_count  = $convert_result['traffic_cnt'];
    $convert_count  = $convert_result['convert_cnt'];
    $convert_rate   = ($traffic_count > 0) ? round((($convert_count/$traffic_count)*100),2) : 0;

    $convert_total_list[$convert_result['key_date']]["traffic"]         = $traffic_count;
    $convert_total_list[$convert_result['key_date']]["conversion"]      = $convert_count;
    $convert_total_list[$convert_result['key_date']]["conversion_rate"] = $convert_rate;
}

# 매출
$add_self_cms_where = $self_cms_base_where." AND `w`.order_date BETWEEN '{$chk_s_datetime}' AND '{$chk_e_datetime}'";
$convert_cms_sql    = "
    SELECT
        key_date,
        SUM(ord_price) as total_price,
        AVG(ord_price) as avg_price
    FROM
    (
        SELECT
            {$add_cms_column} as key_date,
            SUM(`w`.unit_price) as ord_price
        FROM work_cms as `w`
        WHERE {$add_self_cms_where}
        GROUP BY order_number
    ) as w
    GROUP BY key_date
    ORDER BY key_date
";
$convert_cms_query = mysqli_query($my_db, $convert_cms_sql);
while($convert_cms = mysqli_fetch_assoc($convert_cms_query))
{
    $convert_total_list[$convert_cms['key_date']]["total_price"]  = $convert_cms['total_price'];
    $convert_total_list[$convert_cms['key_date']]["avg_price"]    = round($convert_cms['avg_price']);
}

#  유입/전환 그래프
$convert_total_chart_list   = array(
    array("title" => "유입", "type" => "line", "color" => "rgba(0,176,80)", "data" => array()),
    array("title" => "전환", "type" => "line", "color" => "rgba(255,192,0)", "data" => array()),
    array("title" => "전환율", "type" => "line", "color" => "rgba(0,176,240)", "data" => array()),
    array("title" => "아임웹 매출", "type" => "line", "color" => "rgba(128,100,162)", "data" => array()),
    array("title" => "주문건당 단가", "type" => "line", "color" => "rgba(255,51,204)", "data" => array())
);

foreach($convert_total_list as $key_date => $date_data)
{
    $convert_total_chart_list[0]['data'][]  = $date_data['traffic'];
    $convert_total_chart_list[1]['data'][]  = $date_data['conversion'];
    $convert_total_chart_list[2]['data'][]  = $date_data['conversion_rate'];
    $convert_total_chart_list[3]['data'][]  = $date_data['total_price'];
    $convert_total_chart_list[4]['data'][]  = $date_data['avg_price'];
}

$smarty->assign("search_url", getenv("QUERY_STRING"));
$smarty->assign("sch_rate_date", $sch_rate_date);
$smarty->assign("sch_rate_type_option", getRateTypeOption());
$smarty->assign("commerce_date_list", $commerce_date_list);
$smarty->assign("commerce_link_date_list", $commerce_link_date_list);
$smarty->assign("total_report_table_list", $total_report_table_list);
$smarty->assign("total_report_date_list", $total_report_date_list);
$smarty->assign("brand_chart_name_list", json_encode($brand_chart_name_list));
$smarty->assign("brand_chart_list", json_encode($brand_chart_list));
$smarty->assign("date_chart_name_list", json_encode($date_chart_name_list));
$smarty->assign("date_chart_list", json_encode($date_chart_list));
$smarty->assign("nosp_result_total", $nosp_result_total);
$smarty->assign("nosp_result_list", $nosp_result_list);
$smarty->assign('cal_event_list', $cal_event_list);
$smarty->assign("daily_s_date", $daily_s_date);
$smarty->assign("daily_e_date", $daily_e_date);
$smarty->assign("daily_s_date_text", $daily_s_date_text);
$smarty->assign("daily_e_date_text", $daily_e_date_text);
$smarty->assign("prev_daily_s_date", $prev_daily_s_date);
$smarty->assign("prev_daily_e_date", $prev_daily_e_date);
$smarty->assign("prev_daily_s_date_text", $prev_daily_s_date_text);
$smarty->assign("prev_daily_e_date_text", $prev_daily_e_date_text);
$smarty->assign("daily_date_list", $daily_date_list);
$smarty->assign("daily_hour_list", $daily_hour_list);
$smarty->assign("daily_sales_total_price", $daily_sales_total_price);
$smarty->assign("daily_date_total_list", $daily_date_total_list);
$smarty->assign("daily_hour_total_list", $daily_hour_total_list);
$smarty->assign("daily_sales_list", $daily_sales_list);
$smarty->assign("sales_term_hour_list", $sales_term_hour_list);
$smarty->assign("sales_term_date_list", $sales_term_date_list);
$smarty->assign("sales_term_total_list", $sales_term_total_list);
$smarty->assign("convert_total_list", $convert_total_list);
$smarty->assign("convert_total_chart_list", json_encode($convert_total_chart_list));
$smarty->assign("convert_chart_name_list", json_encode($commerce_date_list));
$smarty->assign("convert_total_chart_style_list", json_encode($convert_total_chart_style_list));
$smarty->assign("cms_prd_best_cnt", $cms_prd_best_cnt);
$smarty->assign("cms_prd_name_list", $cms_prd_name_list);
$smarty->assign("cms_prd_best_list", $cms_prd_best_list);

$smarty->display('media_commerce_compare_chart.html');
?>
