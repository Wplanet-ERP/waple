<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_message.php');
require('inc/model/MyQuick.php');
require('inc/model/Message.php');

# Navigation & My Quick
$nav_prd_no  = "234";
$nav_title   = "필터 교체 알림 발송 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수
$crm_model                      = Message::Factory();
$crm_model->setCrmCertReservationInit();
$crm_template_list              = $crm_model->getCrmTemplateList();
$crm_send_state_option          = getCrmReservationStateOption();
$crm_cert_prd_option            = getCertPrdOption();
$crm_temp_type_option           = getCrmTempTypeOption();
$message_result_status_option   = getMessageCallStatusOption();

# 검색조건
$add_where              = "1=1";
$sch_cr_no              = isset($_GET['sch_cr_no']) ? $_GET['sch_cr_no'] : "";
$sch_cr_state           = isset($_GET['sch_cr_state']) ? $_GET['sch_cr_state'] : "";
$sch_out_name           = isset($_GET['sch_out_name']) ? $_GET['sch_out_name'] : "";
$sch_out_hp             = isset($_GET['sch_out_hp']) ? $_GET['sch_out_hp'] : "";
$sch_prd_no             = isset($_GET['sch_prd_no']) ? $_GET['sch_prd_no'] : "";
$sch_temp_profile       = isset($_GET['sch_temp_profile']) ? $_GET['sch_temp_profile'] : "";
$sch_temp_no            = isset($_GET['sch_temp_no']) ? $_GET['sch_temp_no'] : "";
$sch_cert_no            = isset($_GET['sch_cert_no']) ? $_GET['sch_cert_no'] : "";
$sch_cert_prd_no        = isset($_GET['sch_cert_prd_no']) ? $_GET['sch_cert_prd_no'] : "";

if(!empty($sch_cr_no)) {
    $add_where .= " AND ccr.cr_no='{$sch_cr_no}'";
    $smarty->assign("sch_cr_no", $sch_cr_no);
}

if(!empty($sch_cr_state)) {
    $add_where .= " AND ccr.cr_state='{$sch_cr_state}'";
    $smarty->assign("sch_cr_state", $sch_cr_state);
}

if(!empty($sch_out_name)) {
    $add_where .= " AND ccr.out_name LIKE '%{$sch_out_name}%'";
    $smarty->assign("sch_out_name", $sch_out_name);
}

if(!empty($sch_out_hp)) {
    $add_where .= " AND ccr.out_hp LIKE '%{$sch_out_hp}%'";
    $smarty->assign("sch_out_hp", $sch_out_hp);
}

if(!empty($sch_prd_no)) {
    $add_where .= " AND ccr.cert_prd_no IN(SELECT ccp.prd_no FROM crm_cert_product ccp WHERE ccp.prd_no = '{$sch_prd_no}')";
    $smarty->assign("sch_prd_no", $sch_prd_no);
}

if(!empty($sch_temp_no)) {
    $add_where .= " AND ccr.temp_no='{$sch_temp_no}'";
    $smarty->assign("sch_temp_no", $sch_temp_no);
}

if(!empty($sch_temp_profile)) {
    $add_where .= " AND ccr.temp_no IN(SELECT ct.t_no FROM crm_template ct WHERE ct.send_name LIKE '%{$sch_temp_profile}%')";
    $smarty->assign("sch_temp_profile", $sch_temp_profile);
}

if(!empty($sch_cert_no)) {
    $add_where .= " AND ccr.cert_no='{$sch_cert_no}'";
    $smarty->assign("sch_cert_no", $sch_cert_no);
}

if(!empty($sch_cert_prd_no)) {
    $add_where .= " AND ccr.cert_prd_no='{$sch_cert_prd_no}'";
    $smarty->assign("sch_cert_prd_no", $sch_cert_prd_no);
}

# 전체 게시물 수
$crm_send_cnt_sql       = "SELECT COUNT(`ccr`.cr_no) as cnt FROM crm_cert_reservation AS `ccr` WHERE {$add_where}";
$crm_send_cnt_query     = mysqli_query($my_db, $crm_send_cnt_sql);
$crm_send_cnt_result    = mysqli_fetch_array($crm_send_cnt_query);
$crm_send_total 	    = $crm_send_cnt_result['cnt'];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($crm_send_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = (!empty($new_am_no)) ? "" : getenv("QUERY_STRING");
$page_list	= pagelist($pages, "crm_cert_send_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $crm_send_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$crm_send_sql = "
    SELECT
        ccr.cr_no,
        ccr.cr_state,
        ccr.out_name,
        ccr.out_hp,
        ccr.cert_no,
        ccr.cert_prd_no,
        ccr.temp_no,
        ccr.sms_type,
        ccr.exp_date,
        ccr.send_date,
        (SELECT `cc`.cert_key FROM crm_cert AS `cc` WHERE `cc`.`no`=ccr.cert_no) as cert_key,
        (SELECT `ccp`.prd_no FROM crm_cert_product AS `ccp` WHERE `ccp`.`no`=ccr.cert_prd_no) as prd_no,
        (SELECT `ct`.send_name FROM crm_template AS `ct` WHERE `ct`.t_no=ccr.temp_no) as ct_profile,
        (SELECT `es`.CALL_STATUS FROM EASY_SEND_FILTER_LOG AS `es` WHERE `es`.MSG_ID=ccr.send_id) as easy_status,
        (SELECT `es`.REPORT_TIME FROM EASY_SEND_FILTER_LOG AS `es` WHERE `es`.MSG_ID=ccr.send_id) as report_date
    FROM crm_cert_reservation AS ccr
    WHERE {$add_where}
    ORDER BY cr_no DESC
    LIMIT {$offset}, {$num}
";
$crm_send_query     = mysqli_query($my_db, $crm_send_sql);
$crm_cert_send_list = [];
while($crm_send = mysqli_fetch_assoc($crm_send_query))
{
    $crm_send['cr_state_name']  = $crm_send_state_option[$crm_send['cr_state']];
    $crm_send['prd_name']       = "닥터피엘 ".$crm_cert_prd_option[$crm_send['prd_no']];
    $crm_send['sms_type_name']  = $crm_temp_type_option[$crm_send['sms_type']];
    $crm_send['send_result']    = $message_result_status_option[$crm_send['easy_status']];
    $crm_send['temp_name']      = $crm_template_list[$crm_send['temp_no']];

    if(!empty($crm_send['exp_date']) && $crm_send['exp_date'] != "0000-00-00"){
        $crm_send['exp_date'] = date("Y-m-d H:i", strtotime($crm_send['exp_date']));
    }

    if(!empty($crm_send['send_date']) && $crm_send['send_date'] != "0000-00-00"){
        $crm_send['send_date'] = date("Y-m-d H:i", strtotime($crm_send['send_date']));
    }

    if(!empty($crm_send['report_date']) && $crm_send['report_date'] != "0000-00-00"){
        $crm_send['report_date'] = date("Y-m-d H:i", strtotime($crm_send['report_date']));
    }

    if(isset($crm_send['out_hp']) && !empty($crm_send['out_hp'])){
        $f_hp2  = substr($crm_send['out_hp'],0,4);
        $e_hp2  = substr($crm_send['out_hp'],7,15);
        $crm_send['out_sch_hp'] = $f_hp2."***".$e_hp2;
    }

    $crm_cert_send_list[] = $crm_send;
}

$smarty->assign('crm_send_state_option', $crm_send_state_option);
$smarty->assign('crm_cert_prd_option', $crm_cert_prd_option);
$smarty->assign('crm_template_list', $crm_template_list);
$smarty->assign('crm_cert_send_list', $crm_cert_send_list);

$smarty->display('crm_cert_send_list.html');
?>
