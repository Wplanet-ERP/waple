<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_cms.php');
require('inc/model/Company.php');
require('inc/model/WorkCms.php');


$process        = (isset($_POST['process']))?$_POST['process']:"";
$cms_model      = WorkCms::Factory();
$company_model  = Company::Factory();
$cms_model->setMainInit("work_cms_temporary", "t_no");

if($process == "memo")
{
    $t_no 		= (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
    $upd_data   = array("t_no" => $t_no, "manager_memo" => addslashes($value));

    if (!$cms_model->update($upd_data))
        echo "관리자 메모 저장에 실패 하였습니다.";
    else
        echo "관리자 메모가 저장 되었습니다.";
    exit;
}
elseif($process == "notice")
{
    $t_no 		= (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
    $upd_data   = array("t_no" => $t_no, "notice" => addslashes($value));

    if (!$cms_model->update($upd_data))
        echo "특이사항 저장에 실패 하였습니다.";
    else
        echo "특이사항이 저장 되었습니다.";
    exit;
}
elseif($process == "del_temporary")
{
    $t_no 		= (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $search_url	= isset($_POST['search_url'])?$_POST['search_url']:"";

    if (!$cms_model->delete($t_no))
        exit("<script>alert('삭제에 실패했습니다.');location.href='work_cms_temporary.php?{$search_url}';</script>");
    else
        exit("<script>alert('삭제했습니다');location.href='work_cms_temporary.php?{$search_url}';</script>");
    exit;
}
elseif($process == "all_delete")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $del_sql    = "DELETE FROM work_cms_temporary";

    if(!mysqli_query($my_db, $del_sql)){
        exit("<script>alert('삭제에 실패했습니다');location.href='work_cms_temporary.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('모두 삭제했습니다');location.href='work_cms_temporary.php?{$search_url}';</script>");
    }
}
elseif($process == "cms_all_delete")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $del_sql    = "DELETE FROM work_cms_temporary";

    if(!mysqli_query($my_db, $del_sql)){
        exit("<script>alert('삭제에 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('모두 삭제했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}
else
{
    # 검색처리
    $add_where          = "1=1";
	$sch_order_number 	= isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
	$sch_recipient 		= isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
	$sch_recipient_hp 	= isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
    $sch_c_name 		= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
    $sch_s_name		    = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
	$sch_dp_c_no 		= isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
	$sch_prd_name 		= isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";

    if(!empty($sch_order_number)){
        $add_where .= " AND wct.order_number = '{$sch_order_number}'";
        $smarty->assign('sch_order_number', $sch_order_number);
    }

    if(!empty($sch_recipient)){
        $add_where .= " AND wct.recipient like '%{$sch_recipient}%'";
        $smarty->assign('sch_recipient', $sch_recipient);
    }

    if(!empty($sch_recipient_hp)){
        $add_where .= " AND wct.recipient_hp like '%{$sch_recipient_hp}%'";
        $smarty->assign('sch_recipient_hp', $sch_recipient_hp);
    }

    if(!empty($sch_c_name)){
        $add_where .= " AND wct.c_name like '%{$sch_c_name}%'";
        $smarty->assign('sch_c_name', $sch_c_name);
    }

    if(!empty($sch_s_name)){
        $add_where .= " AND (SELECT s_name from staff s where s.s_no=wct.s_no) like '%{$sch_s_name}%'";
        $smarty->assign('sch_s_name', $sch_s_name);
    }

    if(!empty($sch_prd_name)){
        $add_where .= " AND wct.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.title like '%{$sch_prd_name}%')";
        $smarty->assign('sch_prd_name', $sch_prd_name);
    }

    if(!empty($sch_dp_c_no)){
        $add_where .= " AND wct.dp_c_no='{$sch_dp_c_no}'";
        $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
    }

    # 리스트 Total
    $temporary_total_sql	= "SELECT count(wct.t_no) as cnt FROM work_cms_temporary wct WHERE {$add_where}";
    $temporary_total_query	= mysqli_query($my_db, $temporary_total_sql);
    $temporary_total_result = mysqli_fetch_array($temporary_total_query);
    $temporary_total        = isset($temporary_total_result['cnt']) ? $temporary_total_result['cnt'] : 0;

    # 페이징
    $page 	    = isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= 10;
    $offset 	= ($page-1) * $num;
    $page_num 	= ceil($temporary_total/$num);

    if ($page >= $page_num){$page = $page_num;}
    if ($page <= 0){$page = 1;}

    $search_url = getenv("QUERY_STRING");
    $page_list	= pagelist($page, "work_cms_temporary.php", $page_num, $search_url);

    $smarty->assign("search_url", $search_url);
    $smarty->assign("total_num", $temporary_total);
    $smarty->assign("page_list", $page_list);
    
	# 리스트 쿼리
	$temporary_sql = "
		SELECT
			wct.t_no,
			DATE_FORMAT(wct.regdate, '%Y-%m-%d') as reg_date,
			DATE_FORMAT(wct.regdate, '%H:%i') as reg_time,
			wct.stock_date,
			wct.order_date,
		 	DATE_FORMAT(wct.order_date, '%Y-%m-%d') as ord_date,
			DATE_FORMAT(wct.order_date, '%H:%i') as ord_time,
			wct.order_number,
			wct.recipient,
			wct.recipient_hp,
			wct.recipient_addr,
			IF(wct.zip_code, CONCAT('[',wct.zip_code,']'), '') as postcode,
			wct.delivery_memo,
			wct.c_name,
            (SELECT c.c_name FROM company as c WHERE c.c_no=(SELECT p.log_c_no FROM product_cms p WHERE p.prd_no=wct.prd_no)) as log_c_name,
			(SELECT s.s_name FROM staff s WHERE s.s_no=wct.s_no) as s_name,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no))) AS k_prd1_name,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no)) AS k_prd2_name,
			(SELECT title from product_cms prd_cms where prd_cms.prd_no=wct.prd_no) as prd_name,
			(SELECT s.s_name FROM staff s WHERE s.s_no=wct.task_req_s_no) as task_req_s_name,
			wct.task_req,
			(SELECT s_name FROM staff s where s.s_no=wct.task_run_s_no) as task_run_s_name,
			wct.quantity,
			wct.dp_price_vat,
			DATE_FORMAT(wct.payment_date, '%Y-%m-%d') as pay_date,
			DATE_FORMAT(wct.payment_date, '%H:%i') as pay_time,
			wct.dp_c_name,
			wct.manager_memo,
			wct.notice
		FROM work_cms_temporary wct
		WHERE {$add_where}
		ORDER BY wct.t_no DESC
        LIMIT {$offset}, {$num}
	";
    $temporary_query = mysqli_query($my_db, $temporary_sql);
    $temporary_list  = [];
    while($temporary_data = mysqli_fetch_array($temporary_query))
    {
        $temporary_data['dp_price_vat'] = number_format($temporary_data['dp_price_vat']);
        if(isset($temporary_data['recipient_hp']) && !empty($temporary_data['recipient_hp'])){
            $f_hp  = substr($temporary_data['recipient_hp'],0,4);
            $e_hp  = substr($temporary_data['recipient_hp'],7,15);
            $temporary_data['recipient_sc_hp'] = $f_hp."***".$e_hp;
        }

        $temporary_list[] = $temporary_data;
    }

    # 입/출고 금지 구성품 리스트
    $stop_unit_sql      = "SELECT * FROM product_cms_unit WHERE is_in_out='2' AND display='1'";
    $stop_unit_query    = mysqli_query($my_db, $stop_unit_sql);
    $stop_unit_list     = [];
    $is_stop_unit       = false;
    while($stop_unit_result = mysqli_fetch_assoc($stop_unit_query)){
        $stop_unit_list[] = $stop_unit_result;
    }

    $temp_stop_ord_list = [];
    if(!empty($stop_unit_list))
    {
        foreach ($stop_unit_list as $stop_unit)
        {
            $chk_stop_ord_list  = [];
            $chk_stop_cnt_sql   = "SELECT * FROM work_cms_temporary WHERE prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation AS pcr WHERE pcr.option_no='{$stop_unit['no']}' AND pcr.display='1') GROUP BY order_number";
            $chk_stop_cnt_query = mysqli_query($my_db, $chk_stop_cnt_sql);
            $chk_stop_ord_cnt   = mysqli_num_rows($chk_stop_cnt_query);

            if($chk_stop_ord_cnt > 0)
            {
                $chk_stop_ord_sql   = "SELECT order_number FROM work_cms_temporary WHERE prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation AS pcr WHERE pcr.option_no='{$stop_unit['no']}' AND pcr.display='1') GROUP BY order_number LIMIT 3";
                $chk_stop_ord_query = mysqli_query($my_db, $chk_stop_ord_sql);
                while($chk_stop_ord_result = mysqli_fetch_assoc($chk_stop_ord_query)){
                    $chk_stop_ord_list[] = $chk_stop_ord_result['order_number'];
                }
            }

            if(!empty($chk_stop_ord_list))
            {
                $is_stop_unit = true;
                $temp_stop_ord_list[$stop_unit['no']] = array(
                    "option_name"   => $stop_unit['option_name'],
                    "stop_msg"      => $stop_unit['in_out_msg'],
                    "cnt"           => $chk_stop_ord_cnt,
                    "order_number"  => implode(",", $chk_stop_ord_list)
                );
            }
        }
    }

    $smarty->assign('is_stop_unit', $is_stop_unit);
    $smarty->assign('temp_stop_ord_list', $temp_stop_ord_list);
    $smarty->assign('dp_company_option', $company_model->getDpList());
    $smarty->assign('choice_company_option', getChoiceCompanyOption());
    $smarty->assign('dp_cs_option', getAutoDpCsOption());
    $smarty->assign("temporary_list", $temporary_list);

    $smarty->display('work_cms_temporary.html');
}
?>
