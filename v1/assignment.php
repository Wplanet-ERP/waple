<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');
require('inc/model/MyCompany.php');

# Model Init
$team_model = Team::Factory();
$team_model->setMainInit("team", "id");

# 프로세스 처리
$process = (isset($_POST['process'])) ? $_POST['process'] : "";

if($process == "f_major_assignment") # 핵심업무 저장
{
	$id 	= (isset($_POST['id'])) ? $_POST['id'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	$team_upd_data = array("id" => $id, "major_assignment" => $value);

	if (!$team_model->update($team_upd_data))
		echo "핵심업무 저장에 실패 하였습니다.";
	else
		echo "핵심업무가 저장 되었습니다.";
	exit;
}
elseif($process == "f_purpose")# 업무목표 저장
{
	$id 	= (isset($_POST['id'])) ? $_POST['id'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	$team_upd_data = array("id" => $id, "purpose" => $value);

	if (!$team_model->update($team_upd_data))
		echo "업무목표 저장에 실패 하였습니다.";
	else
		echo "업무목표가 저장 되었습니다.";
	exit;
}
elseif($process == "f_priority") # 순서 저장
{
	$id 	= (isset($_POST['id'])) ? $_POST['id'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	$team_upd_data = array("id" => $id, "priority" => $value);

	if (!$team_model->update($team_upd_data))
		echo "순서 저장에 실패 하였습니다.";
	else{
		echo "순서가 저장 되었습니다.";
		echo "<script>setTimeout(function() {location.reload();}, 300);</script>";
	}
	exit;
}
elseif($process == "f_assignment")	// 핵심업무 저장
{
	$id 	= (isset($_POST['id'])) ? $_POST['id'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	$sql 	= "UPDATE job_assignment j_a SET j_a.assignment = '{$value}' WHERE j_a.id = '{$id}'";

	if (!mysqli_query($my_db, $sql))
		echo "업무분장 저장에 실패 하였습니다.";
	else
		echo "업무분장이 저장 되었습니다.";

	exit;
}
elseif($process == "f_job_priority")	// 업무분장 순서 저장
{
	$id 	= (isset($_POST['id'])) ? $_POST['id'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	$sql 	= "UPDATE job_assignment j_a SET j_a.priority = '{$value}' WHERE j_a.id = '{$id}'";

	if (!mysqli_query($my_db, $sql))
		echo "업무분장 순서 저장에 실패 하였습니다.";
	else{
		echo "업무분장 순서가 저장 되었습니다.";
		echo "<script>setTimeout(function() {location.reload();}, 300);</script>";
	}
	exit;
}
elseif($process == "add_job_assignment") 	// 업무분장 추가하기
{
	$id = (isset($_POST['id'])) ? $_POST['id'] : "";
	$team_code = (isset($_POST['team_code'])) ? $_POST['team_code'] : "";

	$sql = "INSERT INTO job_assignment SET
				team_code = '{$team_code}',
				priority = (SELECT IF(ISNULL(MAX(j_a.priority)), '0', MAX(j_a.priority))+1 FROM job_assignment j_a WHERE j_a.team_code='{$team_code}' AND j_a.display='1')
	";

	if(mysqli_query($my_db, $sql)) {
		echo("<script>history.back();</script>");
	} else {
		echo("<script>alert('업무분장 추가에 실패 하였습니다'); history.back();</script>");
	}
	exit;
}
elseif($process == "del_job_assignment")	// 업무분장 삭제하기
{
	$id 	= (isset($_POST['job_id'])) ? $_POST['job_id'] : "";

	$sql 	= "UPDATE job_assignment j_a SET j_a.display = '2' WHERE j_a.id = '{$id}'";

	if(mysqli_query($my_db, $sql)) {
		exit("<script>alert('업무분장을 삭제 하였습니다'); history.back();</script>");
	} else
		exit("<script>alert('업무분장 삭제에 실패 하였습니다'); history.back();</script>");
}
else
{
	# Navigation & My Quick
	$nav_prd_no  = "79";
	$nav_title   = "조직도 및 업무분장";
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_title", $nav_title);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	$my_company_model	= MyCompany::Factory();
	$team_total_list	= $team_model->getTeamAllList();
	$team_all_list		= $team_total_list['team_list'];
	$staff_all_list		= $team_total_list['staff_list'];
	$department_list 	= [];
	$child_team_list 	= [];
	foreach($team_all_list as $team)
	{
		if(!!$team['team_code_parent']){
			$child_team_list[$team['team_code_parent']][$team['team_code']] = $team['team_name'];
		}else{
			$department_list[$team['team_code']] = $team['team_name'];
		}
	}

	# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where 			= " 1=1 AND display='1' ";
	$add_my_c_no 		= "";
	$sch_my_c_no_get 	= isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
	$sch_department_get	= isset($_GET['sch_department'])?$_GET['sch_department']:"";
	$sch_team_get		= isset($_GET['sch_team'])?$_GET['sch_team']:"";
	$sch_assignment_get = isset($_GET['sch_assignment']) ? $_GET['sch_assignment'] : "";

	if (!empty($sch_my_c_no_get)) {
		$add_my_c_no = " AND my_c_no = '" . $sch_my_c_no_get . "'";
		$smarty->assign("sch_my_c_no", $sch_my_c_no_get);
	}

	if(!empty($sch_department_get)){
		$add_where .= " AND (t.team_code='{$sch_department_get}' OR t.team_code_parent='{$sch_department_get}')";
		$smarty->assign('sch_department', $sch_department_get);
	}

	if(!empty($sch_team_get)) {
		$add_where.=" AND team_code='".$sch_team_get."'";
		$smarty->assign("sch_team", $sch_team_get);
	}

	if (!empty($sch_assignment_get)) {
		$add_where .= " AND (major_assignment like '%" . $sch_assignment_get . "%' OR (SELECT count(j_a.assignment) FROM job_assignment j_a WHERE j_a.assignment like '%" . $sch_assignment_get . "%') > 1)";
		$smarty->assign("sch_assignment", $sch_assignment_get);
	}

	$sch_team_list = (!empty($sch_department_get) && isset($child_team_list[$sch_department_get])) ? $child_team_list[$sch_department_get] : "";

	$smarty->assign('department_list', $department_list);
	$smarty->assign('team_list', $sch_team_list);

	# 전체 게시물 수
	$team_total_sql		= "SELECT count(*) as cnt FROM team t WHERE {$add_where} {$add_my_c_no}";
	$team_total_query	= mysqli_query($my_db, $team_total_sql);
	$team_total_result	= mysqli_fetch_array($team_total_query);
	$team_total_num 	= $team_total_result['cnt'];

	# 페이징 처리
	$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num 		= 10;
	$offset 	= ($pages-1) * $num;
	$page_num 	= ceil($team_total_num/$num);

	if ($pages>=$page_num){$pages=$page_num;}
	if ($pages<=0){$pages=1;}

	$search_url = getenv("QUERY_STRING");
	$page_list 	= pagelist($pages, "assignment.php", $page_num, $search_url);

	$smarty->assign("total_num", $team_total_num);
	$smarty->assign("search_url", $search_url);
	$smarty->assign("page_list", $page_list);

	# 리스트 쿼리
	$team_assignment_sql = "
		SELECT
			t.id,
			t.team_code,
			t.team_code_parent,
			(SELECT team_name FROM team WHERE team_code=t.team_code_parent) AS team_parent_name,
			t.team_name,
			t.purpose,
			t.major_assignment,
			(SELECT count(*) FROM job_assignment j_a WHERE j_a.team_code=t.team_code AND j_a.display='1') AS job_assignment_cnt,
			t.my_c_no,
			(SELECT m_c.c_name FROM my_company m_c WHERE m_c.my_c_no=t.my_c_no) AS my_c_name,
			t.priority
		FROM team t
		WHERE {$add_where} {$add_my_c_no}
		ORDER BY priority ASC
		LIMIT {$offset}, {$num}
	";
	$team_assignment_query	= mysqli_query($my_db, $team_assignment_sql);
	$team_assignment_list 	= [];
	while($team_assignment = mysqli_fetch_array($team_assignment_query))
	{
		$team_assignment['job_assignment_cnt'] += 2;
		$team_assignment_list[] = $team_assignment;
	}

	# 업무분장 where 조건 재설정
	$add_assignment_where = " 1=1 AND display='1' ";
	if (!empty($sch_assignment_get)) {
		$add_assignment_where .= " AND j_a.assignment like '%{$sch_assignment_get}%' ";
	}

	# 업무분장 리스트 쿼리
	$job_assignment_sql = "
		SELECT
			j_a.id,
			j_a.team_code,
			j_a.priority,
			j_a.assignment,
			j_a.main_s_no,
			j_a.sub_s_no
		FROM job_assignment j_a
		WHERE {$add_assignment_where}
		ORDER BY priority ASC
	";
	$job_assignment_query = mysqli_query($my_db, $job_assignment_sql);
	$job_assignment_list  = [];
	while($job_assignment_array = mysqli_fetch_array($job_assignment_query))
	{
		$main_s_no_token 	= array();
		$sub_s_no_token 	= array();
		$main_s_no_token 	= stringTokenized($job_assignment_array['main_s_no'],'#');
		$sub_s_no_token 	= stringTokenized($job_assignment_array['sub_s_no'],'#');

		$job_assignment_array['main_s_no'] = $main_s_no_token;
		$job_assignment_array['sub_s_no']  = $sub_s_no_token;

		$job_assignment_list[$job_assignment_array['team_code']][] = $job_assignment_array;
	}

	$smarty->assign("team_assignment_list", $team_assignment_list);
	$smarty->assign("job_assignment_list", $job_assignment_list);
	$smarty->assign("sch_my_company_list", $my_company_model->getNameList());
	$smarty->assign("staff_all_list", $staff_all_list);

	$smarty->display('assignment.html');
}
?>
