<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_cms.php');
require('inc/model/Company.php');
require('inc/model/ProductCmsUnit.php');

# 부가세 정산 판매처 리스트
$company_model          = Company::Factory();
$unit_model             = ProductCmsUnit::Factory();
$dp_company_list        = $company_model->getDpTotalList();
$dp_except_list         = getNotApplyDpList();
$dp_except_text         = implode(",", $dp_except_list);
$sch_dp_company_list    = $company_model->getDpDisplayList();
$brand_option           = $unit_model->getBrandData();

foreach($sch_dp_company_list as $dp_c_no => $dp_c_name){
    if(in_array($dp_c_no, $dp_except_list)){
        unset($sch_dp_company_list[$dp_c_no]);
    }
}

# 검색조건
$add_cms_where      = "1=1 AND dp_c_no NOT IN({$dp_except_text})";
$base_s_month       = date('Y-m', strtotime("-3 months"));
$base_e_month       = date('Y-m', strtotime("-1 months"));
$sch_review_s_month = isset($_GET['sch_review_s_month']) ? $_GET['sch_review_s_month'] : $base_s_month;
$sch_review_e_month = isset($_GET['sch_review_e_month']) ? $_GET['sch_review_e_month'] : $base_e_month;
$sch_c_no           = isset($_GET['sch_c_no']) ? $_GET['sch_c_no'] : "";
$sch_dp_c_no        = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "1812";

if(!isset($_GET['sch_review_s_month']) && isset($_GET['sch_review_e_month'])){
    $sch_review_s_month = date('Y-m', strtotime("-2 months {$sch_review_e_month}"));
}

if(!empty($sch_review_s_month) && !empty($sch_review_e_month))
{
    $sch_review_s_date      = $sch_review_s_month."-01";
    $sch_review_e_date_cal  = $sch_review_e_month."-01";
    $sch_review_e_day       = date("t", strtotime($sch_review_e_date_cal));
    $sch_review_e_date      = $sch_review_e_month."-".$sch_review_e_day;
    $sch_review_s_datetime  = $sch_review_s_date." 00:00:00";
    $sch_review_e_datetime  = $sch_review_e_date." 23:59:59";
    $add_cms_where         .= " AND (order_date BETWEEN '{$sch_review_s_datetime}' AND '{$sch_review_e_datetime}')";

    $smarty->assign('sch_review_s_month', $sch_review_s_month);
    $smarty->assign('sch_review_e_month', $sch_review_e_month);
}

if(!empty($sch_c_no))
{
    $add_cms_where .= " AND c_no='{$sch_c_no}'";
    $smarty->assign('sch_c_no', $sch_c_no);
}

if(!empty($sch_dp_c_no))
{
    $add_cms_where .= " AND dp_c_no='{$sch_dp_c_no}'";
    $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
}



# 부가세 계산
$sales_review_vat_sql = "
    SELECT
        dp_c_no,
        ord_mon,
        COUNT(DISTINCT ord_no) AS ord_cnt,
        SUM(ord_price) AS ord_price,
        SUM(vat_cnt) AS total_vat_cnt,
        SUM(vat_price) AS total_vat_price,
        SUM(settle_cnt) AS total_settle_cnt,
        SUM(settle_price) AS total_settle_price
    FROM
    (
        SELECT
            w.dp_c_no,
            w.ord_no,
            DATE_FORMAT(order_date, '%Y-%m') AS ord_mon,
            w.ord_price,
            (SELECT COUNT(DISTINCT wcv.order_number) FROM work_cms_vat wcv WHERE wcv.order_number=w.ord_no) AS vat_cnt,
            (SELECT SUM(wcv.card_price+wcv.cash_price+wcv.etc_price+wcv.tax_price) FROM work_cms_vat wcv WHERE wcv.order_number=w.ord_no) AS vat_price,
            (SELECT COUNT(DISTINCT wcs.order_number) FROM work_cms_settlement wcs WHERE wcs.order_number=w.ord_no) AS settle_cnt,
            (SELECT SUM(wcs.settle_price) FROM work_cms_settlement wcs WHERE wcs.order_number=w.ord_no) AS settle_price
        FROM
        (
            SELECT 
                IF(origin_ord_no='',order_number, origin_ord_no) AS ord_no,
                dp_c_no,
                order_date,
                SUM(unit_price+unit_delivery_price+coupon_price) AS ord_price
            FROM work_cms w 
            WHERE {$add_cms_where}
            GROUP BY ord_no
        ) AS w
    ) AS main
    GROUP BY dp_c_no, ord_mon
    ORDER BY dp_c_no, ord_mon
";
$sales_review_vat_query     = mysqli_query($my_db, $sales_review_vat_sql);
$sales_review_list          = [];
$sales_review_count_list    = [];
$sales_review_mon_list      = [];
while($sales_review = mysqli_fetch_assoc($sales_review_vat_query))
{
    $not_vat_cnt    = $sales_review['ord_cnt']-$sales_review['total_vat_cnt'];
    $not_settle_cnt = $sales_review['ord_cnt']-$sales_review['total_settle_cnt'];

    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['c_name']         = $dp_company_list[$sales_review['dp_c_no']];
    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['ord_cnt']        = $sales_review['ord_cnt'];
    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['vat_cnt']        = $sales_review['total_vat_cnt'];
    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['not_vat_cnt']    = $not_vat_cnt;
    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['settle_cnt']     = $sales_review['total_settle_cnt'];
    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['not_settle_cnt'] = $not_settle_cnt;

    if($not_settle_cnt > 0) {
        $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['not_settle_per'] = round($not_settle_cnt/$sales_review['ord_cnt']*100, 1);
    }

    $not_vat_price    = $sales_review['ord_price'] - $sales_review['total_vat_price'];
    $not_settle_price = $sales_review['ord_price'] - $sales_review['total_settle_price'];

    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['ord_price']       = $sales_review['ord_price'];
    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['vat_price']       = $sales_review['total_vat_price'];
    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['not_vat_price']   = $not_vat_price;
    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['settle_price']    = $sales_review['total_settle_price'];
    $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['not_settle_price']= $not_settle_price;

    if($not_vat_price > 0) {
        $sales_review_list[$sales_review['dp_c_no']][$sales_review['ord_mon']]['not_vat_per'] = round($not_vat_price/$sales_review['ord_price']*100, 1);
    }

    if(!isset($sales_review_mon_list[$sales_review['ord_mon']])){
        $mon_s_date = $sales_review['ord_mon']."-01";
        $mon_e_day  = date("t", strtotime($mon_s_date));
        $mon_e_date = $sales_review['ord_mon']."-".$mon_e_day;
        $sales_review_mon_list[$sales_review['ord_mon']] = array("s_date" => $mon_s_date, "e_date" => $mon_e_date);
    }
}

foreach($sales_review_list as $dp_c_no => $sales_review_data)
{
    $sales_review_count_list[$dp_c_no] = count($sales_review_data);
}

$smarty->assign("sch_dp_company_list", $sch_dp_company_list);
$smarty->assign("brand_option", $brand_option);
$smarty->assign("sales_review_list", $sales_review_list);
$smarty->assign("sales_review_count_list", $sales_review_count_list);
$smarty->assign("sales_review_mon_list", $sales_review_mon_list);

$smarty->display('work_cms_sales_vat_settle_review_iframe.html');
?>
