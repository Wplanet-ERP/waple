<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');
error_reporting(E_ALL ^ E_NOTICE);

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
include_once('inc/model/Evaluation.php');


$ev_no      = isset($_POST['ev_no']) ? $_POST['ev_no'] : "";
$file_name = $_FILES["regist_file"]["tmp_name"];

$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();
$ev_model     = Evaluation::Factory();
$ev_item      = $ev_model->getItem($ev_no);
$ins_list = [];
$chk_list = [];

for ($i = 2; $i <= $totalRow; $i++)
{
    //변수 초기화
    $manager_s_name  = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));
    $receiver_s_name = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));

    $manager_s_no = $receiver_s_no = "";

    if(empty($receiver_s_name)){
        break;
    }
    
    if(!!$manager_s_name)
    {
        $manager_sql    = "SELECT s_no FROM staff WHERE s_name = '{$manager_s_name}' AND staff_state='1'";
        $manager_query  = mysqli_query($my_db, $manager_sql);
        $manager_result = mysqli_fetch_assoc($manager_query);
        $manager_s_no   = isset($manager_result['s_no']) ? $manager_result['s_no'] : "";
    }

    $receiver_sql    = "SELECT s_no, team FROM staff WHERE s_name = '{$receiver_s_name}' AND staff_state='1'";
    $receiver_query  = mysqli_query($my_db, $receiver_sql);
    $receiver_result = mysqli_fetch_assoc($receiver_query);
    $receiver_s_no   = isset($receiver_result['s_no']) ? $receiver_result['s_no'] : "";
    $receiver_team   = isset($receiver_result['team']) ? $receiver_result['team'] : "";

    if(empty($receiver_s_no))
    {
        echo "관리자 : {$manager_s_name}<br/>";
        echo "평가받는 사람: {$receiver_s_name}<br/>";
        echo "ROW : {$i}, 해당 직원을 찾을 수 없습니다";
        exit;
    }

    $chk_ev_sql    = "SELECT COUNT(ev_r_no) as cnt FROM evaluation_relation WHERE ev_no='{$ev_no}' AND manager='{$receiver_s_no}' AND receiver_s_no='{$receiver_s_no}'";
    $chk_ev_query  = mysqli_query($my_db, $chk_ev_sql);
    $chk_ev_result = mysqli_fetch_assoc($chk_ev_query);

    if($chk_ev_result['cnt'] == 0)
    {
        if(!isset($chk_list[$manager_s_no][$receiver_s_no])){
            if($ev_item['ev_u_set_no'] == '9'){
                $ins_list[] = "('{$ev_no}', '{$ev_item['ev_u_set_no']}', '{$manager_s_no}', '{$receiver_s_no}', '{$receiver_team}', '{$receiver_s_no}', '{$receiver_team}', 0, 0)";
            }elseif($ev_item['ev_u_set_no'] == '13'){
                $ins_list[] = "('{$ev_no}', '{$ev_item['ev_u_set_no']}', '{$manager_s_no}', '{$receiver_s_no}', '{$receiver_team}', '{$receiver_s_no}', '{$receiver_team}', 1, 1)";
            }else{
                $ins_list[] = "('{$ev_no}', '{$ev_item['ev_u_set_no']}', '{$manager_s_no}', '{$receiver_s_no}', '{$receiver_team}', '', '', 0, 0)";
            }

            $chk_list[$manager_s_no][$receiver_s_no] = 1;
        }
    }
}

# 평가관계 빈값 체크
if(empty($ins_list)){
    exit("<script>alert('데이터가 없습니다');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
}

# 평가관계 업데이트
$ins_result = false;
if(!empty($ins_list)){
    $ins_sql = " INSERT INTO `evaluation_relation`
            (ev_no, ev_u_set_no, manager, receiver_s_no, receiver_team, evaluator_s_no, evaluator_team, rec_is_review, eval_is_review)
          VALUES ";

    $ins_sql .= implode(",", $ins_list);
    if (mysqli_query($my_db, $ins_sql)){
        $ins_result = true;
    }
}else{
    $ins_result = true;
}

if (!$ins_result){
    echo "평가관계 엑셀반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    exit;
}else{
    exit("<script>alert('평가관계 엑셀반영 되었습니다.');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
}


?>
