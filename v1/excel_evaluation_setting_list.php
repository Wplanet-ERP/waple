<?php
ini_set('display_errors', -1);
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/evaluation.php');
require('Classes/PHPExcel.php');
require('inc/model/Staff.php');
require('inc/model/Team.php');
require('inc/model/Evaluation.php');

$ev_model 			= Evaluation::Factory();
$ev_system_model 	= Evaluation::Factory();
$staff_model		= Staff::Factory();
$team_model			= Team::Factory();
$ev_model->setMainInit("evaluation_relation", "ev_r_no");

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

$sheet = $objPHPExcel->setActiveSheetIndex(0);
$sheet->getDefaultStyle()->getFont()
->setName('맑은 고딕')
->setSize(9);

//상단타이틀
$sheet->setCellValue('A1', "평가 받는사람");
$sheet->setCellValue('B1', "대표님");
$sheet->setCellValue('D1', "내부 평가자");
$sheet->setCellValue('M1', "외부 평가자");

$eng_abc  = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
$eng_abcc = [];
$eng_list = $eng_abc;
foreach($eng_abc as $a){
	foreach($eng_abc as $b){
		$eng_list[] = $a.$b;
		$eng_abcc[] = $a.$b;
	}
}

foreach($eng_abcc as $aa){
	foreach($eng_abc as $b){
		$eng_list[] = $aa.$b;
	}
}

$ev_no = $_GET['ev_no'];

$ev_relation_sql    = "
		SELECT 
			*, 
			(SELECT s.s_name FROM staff s WHERE s.s_no=er.receiver_s_no) as rec_name,
			(SELECT t.team_name FROM team t WHERE t.team_code=er.receiver_team) as rec_team_name,
			(SELECT t.priority FROM team t WHERE t.team_code=er.receiver_team) as rec_t_priority,
			(SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as eval_name,
			(SELECT t.team_name FROM team t WHERE t.team_code=er.evaluator_team) as eval_team_name,   
			(SELECT t.priority FROM team t WHERE t.team_code=er.evaluator_team) as eval_t_priority,
			(SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as eval_name,
			IF(er.receiver_s_no!=0,'1', '2') as rec_chk,
			IF(er.evaluator_s_no!=0, '1', '2') as eval_chk,
			IF(er.evaluator_s_no='1', '1', '2') as leader_chk,
			IF(er.receiver_team=er.evaluator_team, '1', '2') as team_chk
		FROM evaluation_relation as er
		LEFT JOIN evaluation_system es ON es.ev_no=er.ev_no
		WHERE er.ev_no='{$ev_no}'  AND er.receiver_s_no != er.evaluator_s_no
		ORDER BY manager, rec_chk, rec_t_priority, receiver_s_no, leader_chk, team_chk, eval_chk, evaluator_team, evaluator_s_no 
";
//AND er.rec_is_review='1' AND er.eval_is_review='1'
$ev_relation_query = mysqli_query($my_db, $ev_relation_sql);
$ev_relation_list  		= [];
$ev_relation_name_list  = [];
while($ev_relation = mysqli_fetch_assoc($ev_relation_query))
{
	$ev_relation_name_list[$ev_relation['receiver_s_no']] = $ev_relation['rec_name'];
	$ev_relation_list[$ev_relation['receiver_s_no']][$ev_relation['evaluator_s_no']] = array(
		"name" 			=> $ev_relation['eval_name'],
		"leader_chk"	=> $ev_relation['leader_chk'],
		"team_chk"		=> $ev_relation['team_chk'],
	);
}

$idx = 2;
$work_sheet = $objPHPExcel->getActiveSheet();
foreach($ev_relation_list as $rec_s_no => $eval_data)
{
	$alpha_idx = 3;
	$same_chk  = 0;
	$work_sheet->setCellValue("A{$idx}", $ev_relation_name_list[$rec_s_no]);
	foreach($eval_data as $eval_s_no => $data)
	{
		if($data['leader_chk'] == '1'){
			$work_sheet->getColumnDimension("B")->setWidth(10);
			$work_sheet->setCellValue("B{$idx}", $data['name']);
		}else{
			if($same_chk != 1 && $data['team_chk'] == '2'){
				$alpha_idx = 12;
				$same_chk  = 1;
			}

			$alpha_text = $eng_list[$alpha_idx];
			$work_sheet->getColumnDimension("{$alpha_text}")->setWidth(10);
			$work_sheet->setCellValue("{$alpha_text}{$idx}", $data['name']);
			$alpha_idx++;
		}
	}
	$idx++;
}

$objPHPExcel->getActiveSheet()->setTitle('평가리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
//exit;
// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="평가자설정리스트.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
