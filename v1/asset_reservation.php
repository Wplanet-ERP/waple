<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/asset.php');
require('inc/model/MyQuick.php');
require('inc/model/Asset.php');
require('inc/model/Kind.php');
require('inc/model/Staff.php');
require('inc/model/Message.php');

# Modal 설정
$asset_model = Asset::Factory();
$kind_model  = Kind::Factory();

# Process 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'task_run'){
    $as_r_no    = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $value      = isset($_POST['val']) ? $_POST['val'] : "";

    if(!!$as_r_no)
    {
        $upd_sql = "UPDATE asset_reservation SET task_run='{$value}' WHERE as_r_no='{$as_r_no}'";
    }else{
        echo "처리내용 저장에 실패 하였습니다.";
        exit;
    }

    if (!mysqli_query($my_db, $upd_sql))
        echo "처리내용 저장에 실패 하였습니다.";
    else
        echo "처리내용 저장 되었습니다.";
    exit;
}
elseif($process == 'task_req'){
    $as_r_no    = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $value      = isset($_POST['val']) ? addslashes(trim($_POST['val'])) : "";

    if(!!$as_r_no)
    {
        $upd_sql = "UPDATE asset_reservation SET task_req='{$value}' WHERE as_r_no='{$as_r_no}'";
    }else{
        echo "요청내용 저장에 실패 하였습니다.";
        exit;
    }

    if (!mysqli_query($my_db, $upd_sql))
        echo "요청내용 저장에 실패 하였습니다.";
    else
        echo "요청내용 저장 되었습니다.";
    exit;
}
elseif($process == 'new_asset_reservation')
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $new_work_state     = isset($_POST['new_work_state']) ? $_POST['new_work_state'] : "";
    $new_asset_no       = isset($_POST['new_asset_no']) ? $_POST['new_asset_no'] : "";
    $new_asset_name     = isset($_POST['new_asset_name']) ? $_POST['new_asset_name'] : "";
    $new_management     = isset($_POST['new_management']) ? $_POST['new_management'] : "";
    $new_manager        = isset($_POST['new_manager']) ? $_POST['new_manager'] : "";
    $new_manager_name   = isset($_POST['new_manager_name']) ? $_POST['new_manager_name'] : "";
    $new_sub_manager        = isset($_POST['new_sub_manager']) ? $_POST['new_sub_manager'] : "";
    $new_sub_manager_name   = isset($_POST['new_sub_manager_name']) ? $_POST['new_sub_manager_name'] : "";
    $new_task_req       = isset($_POST['new_task_req']) ? addslashes(trim($_POST['new_task_req'])) : "";
    $new_req_no         = isset($_POST['new_req_no']) ? $_POST['new_req_no'] : "";
    $new_req_name       = isset($_POST['new_req_name']) ? $_POST['new_req_name'] : "";
    $new_share_type     = isset($_POST['new_share_type']) ? $_POST['new_share_type'] : "";
    $new_regdate        = date('Y-m-d H:i:s');
    $asset_msg          = "자산 사용요청 되었습니다.";

    //자산 사용중인지 확인
    if($new_share_type == '1' || $new_share_type == '2')
    {
        $chk_asset_reserve_sql = "SELECT count(as_r_no) as cnt FROM asset_reservation WHERE as_no='{$new_asset_no}' AND work_state='2'";
        $chk_msg = "사용자가 있는 자산입니다";

        if($new_share_type == '2'){
            $chk_asset_reserve_sql .= " AND req_no='{$new_req_no}'";
            $chk_msg = "이미 사용중입니다";
        }

        $chk_asset_query = mysqli_query($my_db, $chk_asset_reserve_sql);
        $chk_asset_result = mysqli_fetch_assoc($chk_asset_query);

        if($chk_asset_result['cnt'] > 0){
            exit("<script>alert('{$chk_msg}');location.href='asset_reservation.php?{$search_url}';</script>");
        }
    }

    $run_s_no   = $new_manager;
    $run_s_name = $new_manager_name;
    if($new_work_state == '1' && ($new_manager == $new_req_no || $new_sub_manager == $new_req_no))
    {
        $new_work_state = '2';

        if($new_sub_manager == $new_req_no){
            $run_s_no   = $new_sub_manager;
            $run_s_name = $new_sub_manager_name;
        }
    }

    $ins_sql = "INSERT INTO `asset_reservation` SET
        `work_state`  = '{$new_work_state}',
        `as_no`       = '{$new_asset_no}',
        `management`  = '{$new_management}',
        `manager`     = '{$new_manager}',
        `sub_manager` = '{$new_sub_manager}',
        `task_req`    = '{$new_task_req}',
        `regdate`     = '{$new_regdate}',
        `req_no`      = '{$new_req_no}',
        `req_name`    = '{$new_req_name}',
        `req_date`    = '{$new_regdate}',
        `run_no`      = '{$run_s_no}',
        `run_name`    = '{$run_s_name}'
     ";

    $new_r_s_day  = isset($_POST['new_r_s_day']) ? $_POST['new_r_s_day'] : "";
    $new_r_s_hour = isset($_POST['new_r_s_hour']) ? $_POST['new_r_s_hour'] : "00";
    $new_r_s_min  = isset($_POST['new_r_s_min']) ? $_POST['new_r_s_min'] : "00";
    $new_r_e_day  = isset($_POST['new_r_e_day']) ? $_POST['new_r_e_day'] : "";
    $new_r_e_hour = isset($_POST['new_r_e_hour']) ? $_POST['new_r_e_hour'] : "00";
    $new_r_e_min  = isset($_POST['new_r_e_min']) ? $_POST['new_r_e_min'] : "00";

    if(!empty($new_r_s_day)){
        $r_s_date = $new_r_s_day." ".$new_r_s_hour.":".$new_r_s_min.":00";
        $ins_sql .= ",r_s_date='{$r_s_date}'";
    }

    if(!empty($new_r_e_day)){
        $r_e_date = $new_r_e_day." ".$new_r_e_hour.":".$new_r_e_min.":00";
        $ins_sql .= ",r_e_date='{$r_e_date}'";
    }

    if($new_work_state == '2' || $new_share_type == '3')
    {
        $ins_sql .= ", run_date='{$new_regdate}'";
        $asset_msg = "자산 사용승인이 완료되었습니다.";
    }

    if(mysqli_query($my_db, $ins_sql))
    {
        $new_as_r_no = $my_db->insert_id;

        if($new_share_type == '1') {
            $upd_asset_state_sql = "UPDATE asset SET asset_state='5' WHERE as_no='{$new_asset_no}'";
            mysqli_query($my_db, $upd_asset_state_sql);
        }

        if($new_as_r_no && ($new_share_type == '1' || $new_share_type == '2'))
        {
            $req_staff_sql    = "SELECT * FROM staff WHERE s_no='{$session_s_no}' AND staff_state='1'";
            $req_staff_query  = mysqli_query($my_db, $req_staff_sql);
            $req_staff_result = mysqli_fetch_assoc($req_staff_query);

            $req_s_no  = isset($req_staff_result['s_no']) ? $req_staff_result['s_no'] : "";
            $req_team  = isset($req_staff_result['team']) ? $req_staff_result['team'] : "";
            $req_c_no  = isset($req_staff_result['my_c_no']) ? $req_staff_result['my_c_no'] : "";

            $work_c_no = $work_c_name = $work_s_no = $work_team = "";
            if($req_c_no == '3'){
                $work_c_name ="와이즈플래닛_미디어커머스";
                $work_c_no   = "1113";
                $work_s_no   = "1";
                $work_team   = "00200";
            }else{
                $work_c_name = "와이즈플래닛 컴퍼니";
                $work_c_no   = "1580";
                $work_s_no   = "1";
                $work_team   = "00200";
            }

            if($new_work_state == '2'){
                $work_sql = "INSERT INTO `work` SET work_state='6', c_no='{$work_c_no}', c_name='{$work_c_name}', s_no='{$work_s_no}', team='{$work_team}', prd_no='227', k_name_code='02300', task_req='{$new_task_req}', task_req_s_no='{$req_s_no}', task_req_team='{$req_team}', task_run_s_no='{$run_s_no}', task_run_team=(SELECT s.team FROM staff s WHERE s.s_no='{$run_s_no}'), task_run_regdate='{$new_regdate}', linked_no = '{$new_as_r_no}', linked_table = 'asset', regdate='{$new_regdate}'";
            }else{
                $work_sql = "INSERT INTO `work` SET work_state='3', c_no='{$work_c_no}', c_name='{$work_c_name}', s_no='{$work_s_no}', team='{$work_team}', prd_no='227', k_name_code='02300', task_req='{$new_task_req}', task_req_s_no='{$req_s_no}', task_req_team='{$req_team}', task_run_s_no='{$run_s_no}', task_run_team=(SELECT s.team FROM staff s WHERE s.s_no='{$run_s_no}'), linked_no = '{$new_as_r_no}', linked_table = 'asset', regdate='{$new_regdate}'";
            }
            mysqli_query($my_db, $work_sql);
        }

        if ($new_work_state == '1' && !empty($run_s_no))
        {
            if(!!$new_as_r_no)
            {
                $return 	    = "https://work.wplanet.co.kr/v1/asset_reservation.php?sch=Y&sch_work_state=&sch_req_name=&sch_as_r_no={$new_as_r_no}";
                $message 	    = "자산 사용 요청 되었습니다.\r\n[자산 사용 내역] {$new_asset_name} (요청자 : {$new_req_name})\r\n{$return}";
                $asset_chk_msg  = addslashes($message);
                $chat_ins_sql   = "INSERT INTO waple_chat_content SET wc_no='3', view_type='2', s_no='{$new_manager}', content='{$asset_chk_msg}', alert_type='51', alert_check='ASSET_REQUEST_{$new_asset_no}_{$new_as_r_no}', regdate=now()";
                mysqli_query($my_db, $chat_ins_sql);

                if($new_sub_manager > 0){
                    $chat_ins_sub_sql = "INSERT INTO waple_chat_content SET wc_no='3', view_type='2', s_no='{$new_sub_manager}', content='{$asset_chk_msg}', alert_type='51', alert_check='ASSET_REQUEST_{$new_asset_no}_{$new_as_r_no}', regdate=now()";
                    mysqli_query($my_db, $chat_ins_sub_sql);
                }
            }
        }

    }else{
        $asset_msg = "오류가 발생했습니다. 다시 시도해 주세요.";
    }

    exit("<script>alert('{$asset_msg}');location.href='asset_reservation.php?{$search_url}';</script>");
}
elseif($process == 'asset_approve')
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $as_r_no            = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $as_no              = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $asset              = $asset_model->getItem($as_no);
    $asset_reservation  = $asset_model->getReservationItem($as_r_no);
    $as_name            = $asset['name'];
    $share_type         = $asset['share_type'];
    $use_type           = $asset['use_type'];
    $manager            = $asset_reservation['manager'];
    $sub_manager        = ($asset_reservation['sub_manager'] != 0) ? $asset_reservation['sub_manager'] : "";
    $req_no             = $asset_reservation['req_no'];
    $work_state         = 2;
    $link_state         = 6;

    if(!empty($as_r_no) && ($manager == $session_s_no || $sub_manager == $session_s_no))
    {
        $upd_sql = "UPDATE asset_reservation SET work_state='{$work_state}', run_date=NOW() WHERE as_r_no='{$as_r_no}'";

        if (mysqli_query($my_db, $upd_sql))
        {
            if($use_type == '1' && $as_r_no)
            {
                $work_sql = "UPDATE `work` SET work_state='{$link_state}', task_run_regdate=now() WHERE linked_no='{$as_r_no}' AND linked_table='asset'";
                mysqli_query($my_db, $work_sql);

                $return 	 = "https://work.wplanet.co.kr/v1/asset_reservation.php?sch=Y&sch_work_state=&sch_req_name=&sch_as_r_no={$as_r_no}";
                $message 	 = "자산 사용 승인 되었습니다.\r\n[자산 사용 내역] {$as_name} (처리자 : {$session_name})\r\n{$return}";
                $asset_chk_msg  = addslashes($message);
                $chat_ins_sql   = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$req_no}', content='{$asset_chk_msg}', alert_type='54', alert_check='ASSET_CONFIRM_{$as_no}_{$as_r_no}', regdate=now()";
            }

            exit("<script>alert('자산 승인에 성공했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }else {
            exit("<script>alert('자산 승인에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }
    } else {
        exit("<script>alert('자산 승인에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }
}
elseif($process == 'asset_refuse')
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $as_r_no            = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $as_no              = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $asset              = $asset_model->getItem($as_no);
    $asset_reservation  = $asset_model->getReservationItem($as_r_no);
    $as_name            = $asset['name'];
    $share_type         = $asset['share_type'];
    $use_type           = $asset['use_type'];
    $manager            = $asset_reservation['manager'];
    $sub_manager        = ($asset_reservation['sub_manager'] != 0) ? $asset_reservation['sub_manager'] : "";
    $req_no             = $asset_reservation['req_no'];
    $work_state         = 3;
    $link_state         = 9;

    if(!empty($as_r_no) && ($manager == $session_s_no || $sub_manager == $session_s_no))
    {
        $upd_sql = "UPDATE asset_reservation SET work_state='{$work_state}', run_date=NOW() WHERE as_r_no='{$as_r_no}'";

        if (mysqli_query($my_db, $upd_sql))
        {
            if ($share_type == '1') {
                $upd_asset_state_sql = "UPDATE asset SET asset_state='1' WHERE as_no='{$as_no}'";
                mysqli_query($my_db, $upd_asset_state_sql);
            }

            if($use_type == '1' && $as_r_no)
            {
                $work_sql = "UPDATE `work` SET work_state='{$link_state}' WHERE linked_no='{$as_r_no}' AND linked_table='asset'";
                mysqli_query($my_db, $work_sql);

                $return 	    = "https://work.wplanet.co.kr/v1/asset_reservation.php?sch=Y&sch_work_state=&sch_req_name=&sch_as_r_no={$as_r_no}";
                $message 	    = "자산 사용 불가 처리 되었습니다.\r\n[자산 사용 내역] {$as_name} (처리자 : {$session_name})\r\n{$return}";
                $asset_chk_msg  = addslashes($message);
                $chat_ins_sql   = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$req_no}', content='{$asset_chk_msg}', alert_type='53', alert_check='ASSET_CANCEL_{$as_no}_{$as_r_no}', regdate=now()";
                mysqli_query($my_db, $chat_ins_sql);
            }
            exit("<script>alert('자산 승인거부에 성공했습니다');location.href='asset_reservation.php?{$search_url}';</script>");

        }else {
            exit("<script>alert('자산 승인거부에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }
    } else {
        exit("<script>alert('자산 승인거부에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }
}
elseif($process == 'asset_cancel')
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $as_r_no            = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $as_no              = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $asset              = $asset_model->getItem($as_no);
    $asset_reservation  = $asset_model->getReservationItem($as_r_no);
    $share_type         = $asset['share_type'];
    $req_no             = $asset_reservation['req_no'];
    $work_state         = 4;
    $link_state         = 8;

    if(!empty($as_r_no) && $req_no == $session_s_no)
    {
        $upd_sql = "UPDATE asset_reservation SET work_state='{$work_state}', run_date=NOW() WHERE as_r_no='{$as_r_no}'";

        if (mysqli_query($my_db, $upd_sql))
        {
            if ($share_type == '1') {
                $upd_asset_state_sql = "UPDATE asset SET asset_state='1' WHERE as_no='{$as_no}'";
                mysqli_query($my_db, $upd_asset_state_sql);
            }

            $work_sql   = "UPDATE `work` SET work_state='{$link_state}' WHERE linked_no='{$as_r_no}' AND linked_table='asset'";
            mysqli_query($my_db, $work_sql);

            exit("<script>alert('사용요청 취소했습니다');location.href='asset_reservation.php?{$search_url}';</script>");

        }else {
            exit("<script>alert('사용요청취소 처리에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }
    }else {
        exit("<script>alert('사용요청취소 처리에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }
}
elseif($process == 'asset_repair')
{
    $search_url  = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $as_r_no     = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $as_no       = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $asset       = $asset_model->getItem($as_no);
    $as_name     = $asset['name'];
    $task_req    = isset($_POST['task_req']) ? addslashes($_POST['task_req']) : "";
    $cur_date    = date('Y-m-d H:i:s');
    $work_state  = '5';

    if($asset['object_type'] != '1'){
        exit("<script>alert('유형자산만 수리요청 하실 수 있습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }

    if(!empty($as_r_no)){
        $dup_sql = "INSERT INTO asset_reservation(`work_state`, `as_no`, `management`, `manager`, `sub_manager`, `regdate`, `req_no`, `req_name`, `task_req`, `req_date`, `run_no`, `run_name`) (SELECT '{$work_state}', '{$as_no}', management, `manager`, `sub_manager`, '{$cur_date}', '{$session_s_no}', '{$session_name}', '{$task_req}', '{$cur_date}', '260', '이지윤' FROM asset_reservation WHERE as_r_no = '{$as_r_no}')";
    }else{
        exit("<script>alert('수리 요청에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }

    if (mysqli_query($my_db, $dup_sql))
    {
        $last_as_r_no   = mysqli_insert_id($my_db);
        $staff_model    = Staff::Factory();
        $req_staff      = $staff_model->getStaff($session_s_no);

        $req_s_no  = isset($req_staff['s_no']) ? $req_staff['s_no'] : "";
        $req_team  = isset($req_staff['team']) ? $req_staff['team'] : "";
        $req_c_no  = isset($req_staff['my_c_no']) ? $req_staff['my_c_no'] : "";

        $work_c_no = $work_c_name = $work_s_no = $work_team = "";
        if($req_c_no == '3'){
            $work_c_name ="와이즈플래닛_미디어커머스";
            $work_c_no   = "1113";
            $work_s_no   = "1";
            $work_team   = "00200";
        }else{
            $work_c_name = "와이즈플래닛 컴퍼니";
            $work_c_no   = "1580";
            $work_s_no   = "1";
            $work_team   = "00200";
        }

        $asset_state_sql = "UPDATE asset_reservation SET work_state='3' WHERE as_r_no='{$as_r_no}'";
        mysqli_query($my_db, $asset_state_sql);

        $work_sql = "INSERT INTO `work` SET work_state='3', c_no='{$work_c_no}', c_name='{$work_c_name}', s_no='{$work_s_no}', team='{$work_team}', prd_no='227', k_name_code='02301', task_req='{$task_req}', task_req_s_no='{$req_s_no}', task_req_team='{$req_team}', task_run_s_no='260', task_run_team='00211', linked_no = '{$last_as_r_no}', linked_table = 'asset', regdate='{$cur_date}'";
        mysqli_query($my_db, $work_sql);

        $upd_asset_state_sql = "UPDATE asset SET asset_state='3' WHERE as_no='{$as_no}'";
        mysqli_query($my_db, $upd_asset_state_sql);


        if(isset($as_name))
        {
            $receiver_staff = $staff_model->getStaff($asset['manager']);
            $message_model  = Message::Factory();

            $send_name      = "와이즈플래닛";
            $send_phone     = "02-830-1912";
            $sms_title      = "와이즈플래닛 자산관리";
            $sms_msg        = "{$session_name}님이 {$as_name} 자산수리를 요청하였습니다.";
            $sms_msg_len    = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
            $msg_type 		= ($sms_msg_len > 90) ? "L" : "S";
            $msg_id 	    = "W".date("YmdHis").sprintf('%04d', "1");
            $c_info         = "13"; #자산관리
            $c_info_detail  = "ASSET_REPAIR";

            $send_data_list[$msg_id] = array(
                "msg_id"        => $msg_id,
                "msg_type"      => $msg_type,
                "sender"        => $send_name,
                "sender_hp"     => $send_phone,
                "receiver"      => $receiver_staff['s_name'],
                "receiver_hp"   => $receiver_staff['hp'],
                "title"         => $sms_title,
                "content"       => $sms_msg,
                "cinfo"         => $c_info,
                "cinfo_detail"  => $c_info_detail,
            );

            $message_model->sendMessage($send_data_list);
        }

        exit("<script>alert('수리 요청에 성공했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }else {
        exit("<script>alert('수리 요청에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }
}
elseif($process == 'repair_cancel')
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $as_r_no            = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $as_no              = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $asset              = $asset_model->getItem($as_no);
    $asset_reservation  = $asset_model->getReservationItem($as_r_no);
    $as_name            = $asset['name'];
    $share_type         = $asset['share_type'];
    $use_type           = $asset['use_type'];
    $req_no             = $asset_reservation['req_no'];
    $work_state         = 6;
    $link_state         = 8;

    if(!empty($as_r_no))
    {
        $upd_sql = "UPDATE asset_reservation SET work_state='{$work_state}', run_date=now() WHERE as_r_no='{$as_r_no}'";

        if (mysqli_query($my_db, $upd_sql))
        {
            if($share_type == '1'){
                $upd_asset_state_sql = "UPDATE asset SET asset_state='2' WHERE as_no='{$as_no}'";
            }else{
                $upd_asset_state_sql = "UPDATE asset SET asset_state='1' WHERE as_no='{$as_no}'";
            }
            mysqli_query($my_db, $upd_asset_state_sql);

            $work_sql = "UPDATE work SET work_state='{$link_state}' WHERE linked_no='{$as_r_no}' AND linked_table='asset'";
            mysqli_query($my_db, $work_sql);

            exit("<script>alert('수리취소 처리했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('수리취소 처리에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }
    }else {
        exit("<script>alert('수리취소 처리에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }
}
elseif($process == 'repair_complete')
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $as_r_no            = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $as_no              = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $asset              = $asset_model->getItem($as_no);
    $asset_reservation  = $asset_model->getReservationItem($as_r_no);
    $as_name            = $asset['name'];
    $share_type         = $asset['share_type'];
    $use_type           = $asset['use_type'];
    $req_no             = $asset_reservation['req_no'];
    $work_state         = 7;
    $link_state         = 6;

    if(!empty($as_r_no))
    {
        $upd_sql = "UPDATE asset_reservation SET work_state='{$work_state}', run_date=NOW() WHERE as_r_no='{$as_r_no}'";

        if (mysqli_query($my_db, $upd_sql))
        {
            if($share_type == '1'){
                $upd_asset_state_sql = "UPDATE asset SET asset_state='5' WHERE as_no='{$as_no}'";
                $auto_use_asset_sql  = "INSERT INTO asset_reservation(work_state, as_no, management, manager, sub_manager, regdate, req_no, req_name, task_req, req_date, run_no, run_name, run_date) (SELECT '2', main.as_no, main.management, `a`.manager, `a`.sub_manager, now(), main.req_no, main.req_name, '수리완료 후 자동 사용승인', now(), '{$session_s_no}', '{$session_name}', now() FROM asset_reservation as main LEFT JOIN asset as `a` ON `a`.as_no=main.as_no WHERE main.work_state='3' AND main.as_no='{$as_no}' ORDER BY main.as_r_no DESC LIMIT 1)";
                mysqli_query($my_db, $auto_use_asset_sql);
            }else{
                $upd_asset_state_sql = "UPDATE asset SET asset_state='1' WHERE as_no='{$as_no}'";
            }
            mysqli_query($my_db, $upd_asset_state_sql);

            $staff_model    = Staff::Factory();
            $req_staff      = $staff_model->getStaff($req_no);

            if(isset($req_staff['hp']))
            {
                $message_model  = Message::Factory();

                $send_name      = "와이즈플래닛";
                $send_phone     = "02-830-1912";
                $sms_title      = "와이즈플래닛 자산관리";
                $sms_msg        = "{$req_staff['s_name']}님의 자산수리가 완료되었습니다.\r\n와플 자산 사용 내역에서 확인해주세요.";
                $sms_msg_len    = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
                $msg_type 		= ($sms_msg_len > 90) ? "L" : "S";
                $msg_id 	    = "W".date("YmdHis").sprintf('%04d', "1");
                $c_info         = "13"; #자산관리
                $c_info_detail  = "ASSET_REPAIR";

                $send_data_list[$msg_id] = array(
                    "msg_id"        => $msg_id,
                    "msg_type"      => $msg_type,
                    "sender"        => $send_name,
                    "sender_hp"     => $send_phone,
                    "receiver"      => $req_staff['s_name'],
                    "receiver_hp"   => $req_staff['hp'],
                    "title"         => $sms_title,
                    "content"       => $sms_msg,
                    "cinfo"         => $c_info,
                    "cinfo_detail"  => $c_info_detail,
                );

                $message_model->sendMessage($send_data_list);
            }

            $work_sql = "UPDATE `work` SET work_state='{$link_state}' WHERE linked_no='{$as_r_no}' AND linked_table='asset'";
            mysqli_query($my_db, $work_sql);

            exit("<script>alert('수리완료 처리했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('수리완료 처리에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('수리완료 처리에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }
}
elseif($process == 'asset_return')
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $as_r_no            = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $as_no              = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $asset              = $asset_model->getItem($as_no);
    $asset_reservation  = $asset_model->getReservationItem($as_r_no);
    $as_name            = $asset['name'];
    $share_type         = $asset['share_type'];
    $object_type        = $asset['object_type'];
    $manager            = $asset['manager'];
    $manager_name       = $asset['manager_name'];
    $sub_manager        = $asset['sub_manager'];
    $sub_manager_name   = $asset['sub_manager_name'];
    $cur_date           = date('Y-m-d H:i:s');
    $work_state         = '8';
    $link_state         = '3';
    $task_req           = "자산 {$as_name} 반납요청입니다.";

    if(!empty($as_r_no))
    {
        if(($session_team == '00211' || $session_s_no == $manager || $session_s_no == $sub_manager) || $object_type != '1'){
            $work_state = '9';
            $link_state = '6';
            $task_req   = "자산 {$as_name} 반납완료입니다.";

            $dup_sql    = "INSERT INTO asset_reservation(`work_state`, `as_no`, `management`, `manager`, `sub_manager`, `regdate`, `req_no`, `req_name`, `req_date`, `run_no`, `run_name`, `run_date`) (SELECT '{$work_state}', '{$as_no}', management, `manager`, `sub_manager`, '{$cur_date}', '{$session_s_no}', '{$session_name}', '{$cur_date}', '{$session_s_no}', '{$session_name}', '{$cur_date}' FROM asset_reservation WHERE as_r_no = '{$as_r_no}')";
        }else{
            $dup_sql    = "INSERT INTO asset_reservation(`work_state`, `as_no`, `management`, `manager`, `sub_manager`, `regdate`, `req_no`, `req_name`, `req_date`, `run_no`, `run_name`) (SELECT '{$work_state}', '{$as_no}', management, `manager`, `sub_manager`, '{$cur_date}', req_no, req_name, '{$cur_date}', '{$manager}', '{$manager_name}' FROM asset_reservation WHERE as_r_no = '{$as_r_no}')";
        }

        if (mysqli_query($my_db, $dup_sql))
        {
            $last_as_r_no       = mysqli_insert_id($my_db);
            $req_staff_sql      = "SELECT * FROM staff WHERE s_no='{$session_s_no}' AND staff_state='1'";
            $req_staff_query    = mysqli_query($my_db, $req_staff_sql);
            $req_staff_result   = mysqli_fetch_assoc($req_staff_query);

            $req_s_no  = isset($req_staff_result['s_no']) ? $req_staff_result['s_no'] : "";
            $req_team  = isset($req_staff_result['team']) ? $req_staff_result['team'] : "";
            $req_c_no  = isset($req_staff_result['my_c_no']) ? $req_staff_result['my_c_no'] : "";

            $work_c_no = $work_c_name = $work_s_no = $work_team = "";
            if($req_c_no == '3'){
                $work_c_name ="와이즈플래닛_미디어커머스";
                $work_c_no   = "1113";
                $work_s_no   = "1";
                $work_team   = "00200";
            }else{
                $work_c_name = "와이즈플래닛 컴퍼니";
                $work_c_no   = "1580";
                $work_s_no   = "1";
                $work_team   = "00200";
            }

            if($link_state ==  '6'){
                $work_sql = "INSERT INTO `work` SET work_state='{$link_state}', c_no='{$work_c_no}', c_name='{$work_c_name}', s_no='{$work_s_no}', team='{$work_team}', prd_no='227', k_name_code='02302', task_req='{$task_req}', task_req_s_no='{$req_s_no}', task_req_team='{$req_team}', task_run_s_no='{$session_s_no}', task_run_team=(SELECT s.team FROM staff s WHERE s.s_no='{$session_s_no}'), task_run_regdate=now(), linked_no = '{$last_as_r_no}', linked_table = 'asset', regdate='{$cur_date}'";
            }else{
                $work_sql = "INSERT INTO `work` SET work_state='{$link_state}', c_no='{$work_c_no}', c_name='{$work_c_name}', s_no='{$work_s_no}', team='{$work_team}', prd_no='227', k_name_code='02302', task_req='{$task_req}', task_req_s_no='{$req_s_no}', task_req_team='{$req_team}', task_run_s_no='{$manager}', task_run_team=(SELECT s.team FROM staff s WHERE s.s_no='{$manager}'), linked_no = '{$last_as_r_no}', linked_table = 'asset', regdate='{$cur_date}'";
            }
            mysqli_query($my_db, $work_sql);

            if($share_type == '1')
            {
                if($work_state == '9'){
                    $upd_asset_state_sql = "UPDATE asset SET asset_state='1' WHERE as_no='{$as_no}'";
                }else{
                    $upd_asset_state_sql = "UPDATE asset SET asset_state='2' WHERE as_no='{$as_no}'";
                }

                mysqli_query($my_db, $upd_asset_state_sql);
            }

            $dup_upd_sql = "UPDATE asset_reservation SET work_state='10' WHERE as_r_no = '{$as_r_no}'";
            mysqli_query($my_db, $dup_upd_sql);

            exit("<script>alert('반납에 성공했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }else {
            exit("<script>alert('반납에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }

    }else{
        exit("<script>alert('반납에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }
}
elseif($process == 'return_complete')
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $as_r_no            = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $as_no              = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $asset              = $asset_model->getItem($as_no);
    $share_type         = $asset['share_type'];
    $work_state  = 9;
    $link_state  = 6;

    if(!empty($as_r_no))
    {
        $upd_sql = "UPDATE asset_reservation SET work_state='{$work_state}', run_date=NOW() WHERE as_r_no='{$as_r_no}'";

        if (mysqli_query($my_db, $upd_sql))
        {
            if($share_type == '1') {
                $upd_asset_state_sql = "UPDATE asset SET asset_state='1' WHERE as_no='{$as_no}'";
                mysqli_query($my_db, $upd_asset_state_sql);
            }

            $work_sql = "UPDATE `work` SET work_state='{$link_state}', task_run_regdate=now() WHERE linked_no='{$as_r_no}' AND linked_table='asset'";
            mysqli_query($my_db, $work_sql);

            exit("<script>alert('반납완료 처리했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('반납완료 처리에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('반납완료 처리에 실패했습니다');location.href='asset_reservation.php?{$search_url}';</script>");
    }
}


# Navigation & My Quick
$nav_prd_no  = "72";
$nav_title   = "자산 사용 내역";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색 초기화 및 조건 생성
$add_where = "1=1";

# 자산요청 처리
$sch_reservation    = isset($_GET['sch_reservation']) ? $_GET['sch_reservation'] : "";
$smarty->assign("sch_reservation", $sch_reservation);

# 자산관리 상품 처리
$sch_asset_g1   = isset($_GET['sch_asset_g1']) ? $_GET['sch_asset_g1'] : "";
$sch_asset_g2   = isset($_GET['sch_asset_g2']) ? $_GET['sch_asset_g2'] : "";
$sch_asset      = isset($_GET['sch_asset']) ? $_GET['sch_asset'] : "";
$sch_get        = isset($_GET['sch']) ? $_GET['sch'] : "";

$smarty->assign("sch_asset_g1", $sch_asset_g1);
$smarty->assign("sch_asset_g2", $sch_asset_g2);

if(!empty($sch_get)) {
    $smarty->assign("sch", $sch_get);
}

// 상품(자산) 종류
if (!empty($sch_asset)) {
    $add_where .= " AND `as`.`name` like '%{$sch_asset}%'";
    $smarty->assign("sch_asset", $sch_asset);
}

if($sch_asset_g2){
    $add_where .= " AND `as`.k_name_code ='".$sch_asset_g2."'";
}elseif($sch_asset_g1){
    $add_where .= " AND (SELECT k_parent FROM kind k WHERE k.k_name_code=`as`.k_name_code) ='".$sch_asset_g1."'";
}

$kind_model       = Kind::Factory();
$asset_group_list = $kind_model->getKindGroupList("asset");
$asset_g1_list = $asset_g2_list = $asset_prd_list = [];

foreach($asset_group_list as $key => $asset_data)
{
    if(!$key){
        $asset_g1_list = $asset_data;
    }else{
        $asset_g2_list[$key] = $asset_data;
    }
}

$sch_asset_g2_list = isset($asset_g2_list[$sch_asset_g1]) ? $asset_g2_list[$sch_asset_g1] : [];

$smarty->assign("asset_g1_list", $asset_g1_list);
$smarty->assign("asset_g2_list", $sch_asset_g2_list);

$sch_quick_state    = isset($_GET['sch_quick_state']) ? $_GET['sch_quick_state'] : "";
$sch_work_state     = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_keyword        = isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_req_name       = isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : $session_name;
$sch_run_name       = isset($_GET['sch_run_name']) ? $_GET['sch_run_name'] : "";
$sch_as_r_no        = isset($_GET['sch_as_r_no']) ? $_GET['sch_as_r_no'] : "";
$sch_management     = isset($_GET['sch_management']) ? $_GET['sch_management'] : "";
$sch_task_req       = isset($_GET['sch_task_req']) ? $_GET['sch_task_req'] : "";
$sch_task_run       = isset($_GET['sch_task_run']) ? $_GET['sch_task_run'] : "";
$sch_qrcode         = isset($_GET['sch_qrcode']) ? $_GET['sch_qrcode'] : "N";

if(!empty($sch_quick_state))
{
    switch($sch_quick_state){
        case '1':
            $add_where .= " AND asr.req_no='{$session_s_no}' AND asr.work_state='1'";
            break;
        case '2':
            $add_where .= " AND asr.req_no='{$session_s_no}' AND asr.work_state='2'";
            break;
        case '3':
            $add_where .= " AND (asr.manager='{$session_s_no}' OR asr.sub_manager='{$session_s_no}') AND asr.work_state='1'";
            break;
        case '4':
            $add_where .= " AND asr.run_no='{$session_s_no}' AND asr.work_state='5'";
            break;
        case '5':
            $add_where .= " AND asr.run_no='{$session_s_no}' AND asr.work_state='8'";
            break;

    }
    $smarty->assign("sch_quick_state", $sch_quick_state);
}
else
{
    if(!empty($sch_work_state))
    {
        $add_where .= " AND asr.work_state = '{$sch_work_state}'";
        $smarty->assign('sch_work_state', $sch_work_state);
    }

    if(!empty($sch_manager))
    {
        $add_where .= " AND (asr.manager IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_manager}%') OR asr.sub_manager IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_manager}%'))";
        $smarty->assign('sch_manager', $sch_manager);
    }

    if(!empty($sch_run_name))
    {
        $add_where .= " AND asr.run_name LIKE '%{$sch_run_name}%'";
        $smarty->assign('sch_run_name', $sch_run_name);
    }

    if(!empty($sch_as_r_no))
    {
        $add_where .= " AND asr.as_r_no = '{$sch_as_r_no}'";
        $smarty->assign('sch_as_r_no', $sch_as_r_no);
    }

    if(!empty($sch_req_name))
    {
        $add_where .= " AND asr.req_name LIKE '%{$sch_req_name}%'";
        $smarty->assign('sch_req_name', $sch_req_name);
    }
}

if(!empty($sch_keyword))
{
    $add_where .= " AND `as`.keyword like '%{$sch_keyword}%'";
    $smarty->assign('sch_keyword', $sch_keyword);
}

if(!empty($sch_management))
{
    $add_where .= " AND asr.management LIKE '%{$sch_management}%'";
    $smarty->assign('sch_management', $sch_management);

    if($sch_qrcode == 'Y') {
        # QRCODE 접속시 처리
        $qr_chk_sql = "SELECT as_no, asset_state FROM asset WHERE management='{$sch_management}' AND share_type IN('1','2')";
        $qr_chk_query = mysqli_query($my_db, $qr_chk_sql);
        $qr_chk_result = mysqli_fetch_assoc($qr_chk_query);

        if (isset($qr_chk_result['asset_state']) && $qr_chk_result['asset_state'] == '1') {
            $smarty->assign('qrcode_as_no', $qr_chk_result['as_no']);
        }
    }
}
$smarty->assign('is_connect_qrcode', $sch_qrcode);

if(!empty($sch_task_req))
{
    $add_where .= " AND asr.task_req LIKE '%{$sch_task_req}%'";
    $smarty->assign('sch_task_req', $sch_task_req);
}

if(!empty($sch_task_run))
{
    $add_where .= " AND asr.task_run LIKE '%{$sch_task_run}%'";
    $smarty->assign('sch_task_run', $sch_task_run);
}

# 전체 게시물 수
$asset_reservation_total_sql	= "SELECT count(as_r_no) as cnt FROM (SELECT `asr`.as_r_no FROM asset_reservation `asr`  LEFT JOIN asset `as` ON `as`.as_no = `asr`.as_no WHERE {$add_where}) AS cnt";
$asset_reservation_total_query	= mysqli_query($my_db, $asset_reservation_total_sql);
$asset_reservation_total_result = mysqli_fetch_array($asset_reservation_total_query);
$asset_reservation_total        = $asset_reservation_total_result['cnt'];

# 페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($asset_reservation_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
if($search_url){
    $search_url_list = explode("&", $search_url);
    $sch_idx = 0;
    foreach($search_url_list as $sch_url_path){
        if(strpos($sch_url_path, "sch_qrcode") !== false){
            unset($search_url_list[$sch_idx]);
        }
        $sch_idx++;
    }

    $search_url = implode("&", $search_url_list);
}
$page_list	= pagelist($pages, "asset_reservation.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $asset_reservation_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$asset_reservation_sql  = "
    SELECT
        `asr`.as_r_no,
        DATE_FORMAT(`asr`.regdate, '%Y-%m-%d') as reg_date,
        DATE_FORMAT(`asr`.regdate, '%H:%i') as reg_time,
        `asr`.work_state,
        `asr`.as_no,
        `as`.`share_type` as share_type,
        `as`.`use_type` as use_type,
        `as`.`object_type` as object_type,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=`as`.k_name_code)) AS k_asset1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=`as`.k_name_code) AS k_asset2_name,
        `as`.`name` as asset_name,
        `as`.`non_description` as non_description,
        `as`.asset_loc,
        `as`.object_site,
        `as`.object_url,
        `as`.object_id,
        `as`.object_pw,
        `asr`.management,
        `asr`.manager,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`asr`.manager) as manager_name,
        `asr`.sub_manager,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`asr`.sub_manager) as sub_manager_name,
        DATE_FORMAT(`asr`.r_s_date, '%Y/%m/%d') as r_s_day,
        DATE_FORMAT(`asr`.r_s_date, '%H') as r_s_hour,
        DATE_FORMAT(`asr`.r_s_date, '%i') as r_s_min,
        DATE_FORMAT(`asr`.r_e_date, '%Y/%m/%d') as r_e_day,
        DATE_FORMAT(`asr`.r_e_date, '%H') as r_e_hour,
        DATE_FORMAT(`asr`.r_e_date, '%i') as r_e_min,
        `asr`.req_no,
        `asr`.task_req,
        `asr`.req_name,
        `asr`.run_no,
        `asr`.task_run,
        `asr`.run_name,
        `as`.qr_code,
        `as`.img_name,
        `as`.img_path
    FROM asset_reservation `asr`
    LEFT JOIN asset `as` ON `as`.as_no = `asr`.as_no
    WHERE {$add_where}
    ORDER BY asr.as_r_no DESC
    LIMIT {$offset}, {$num}
";
$asset_reservation_query    = mysqli_query($my_db, $asset_reservation_sql);
$asset_reservation_list     = [];
$asset_work_state_option    = getAssetWorkStateOption();
$asset_object_type_option   = getAssetObjectTypeOption();
while($asset_reservation = mysqli_fetch_assoc($asset_reservation_query))
{
    $work_state     = isset($asset_reservation['work_state']) && !empty($asset_reservation['work_state']) ? $asset_reservation['work_state'] : "";
    $asset_reservation['work_state_name']  = !empty($work_state) ? $asset_work_state_option[$work_state]['label'] : "";
    $asset_reservation['work_state_color'] = !empty($work_state) ? $asset_work_state_option[$work_state]['color'] : "";
    $asset_reservation['object_type_name'] = isset($asset_object_type_option[$asset_reservation['object_type']]) ? $asset_object_type_option[$asset_reservation['object_type']] : "";

    $img_paths = $asset_reservation['img_path'];
    $img_names = $asset_reservation['img_name'];

    if(!empty($img_paths) && !empty($img_names))
    {
        $img_paths_arr = explode(',', $img_paths);
        $img_names_arr = explode(',', $img_names);
        $asset_reservation['first_img_path'] = $img_paths_arr[0];
        $asset_reservation['first_img_name'] = $img_names_arr[0];
    }else{
        $asset_reservation['first_img_path'] = "";
        $asset_reservation['first_img_name'] = "";
    }

    $asset_reservation['show_permission'] = '1';
    if(($asset_reservation['use_type'] == '1' && $asset_reservation['object_type'] == '3') && ($asset_reservation['req_no'] != $session_s_no && $asset_reservation['manager'] != $session_s_no && $asset_reservation['sub_manager'] != $session_s_no)) {
        $asset_reservation['show_permission'] = '2';
    }

    $asset_reservation_list[] = $asset_reservation;
}

# 자산내역 Quick 슬롯
$reservation_cnt_list   = [];
$reservation_req_sql    = "SELECT COUNT(IF(work_state=1,'1',NULL)) as req_cnt, COUNT(IF(work_state=2,'1',NULL)) as use_cnt FROM asset_reservation WHERE req_no='{$session_s_no}' AND work_state IN(1,2)";
$reservation_req_query  = mysqli_query($my_db, $reservation_req_sql);
$reservation_req_result = mysqli_fetch_assoc($reservation_req_query);

$reservation_run_sql    = "SELECT COUNT(*) as manager_cnt FROM asset_reservation WHERE (manager='{$session_s_no}' OR sub_manager='{$session_s_no}') AND work_state='1'";
$reservation_run_query  = mysqli_query($my_db, $reservation_run_sql);
$reservation_run_result = mysqli_fetch_assoc($reservation_run_query);

$reservation_repair_sql    = "SELECT COUNT(*) as repair_cnt FROM asset_reservation WHERE run_no='{$session_s_no}' AND work_state='5'";
$reservation_repair_query  = mysqli_query($my_db, $reservation_repair_sql);
$reservation_repair_result = mysqli_fetch_assoc($reservation_repair_query);

$reservation_return_sql    = "SELECT COUNT(*) as return_cnt FROM asset_reservation WHERE run_no='{$session_s_no}' AND work_state='8'";
$reservation_return_query  = mysqli_query($my_db, $reservation_return_sql);
$reservation_return_result = mysqli_fetch_assoc($reservation_return_query);

$reservation_cnt_list['1'] = isset($reservation_req_result['req_cnt']) ? $reservation_req_result['req_cnt'] : 0;
$reservation_cnt_list['2'] = isset($reservation_req_result['use_cnt']) ? $reservation_req_result['use_cnt'] : 0;
$reservation_cnt_list['3'] = isset($reservation_run_result['manager_cnt']) ? $reservation_run_result['manager_cnt'] : 0;
$reservation_cnt_list['4'] = isset($reservation_repair_result['repair_cnt']) ? $reservation_repair_result['repair_cnt'] : 0;
$reservation_cnt_list['5'] = isset($reservation_return_result['return_cnt']) ? $reservation_return_result['return_cnt'] : 0;

$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign('asset_hour_option', getHourOption());
$smarty->assign('asset_min_option', getMinOption());
$smarty->assign('asset_work_state_option', $asset_work_state_option);
$smarty->assign("asset_quick_option", getAssetQuickOption());
$smarty->assign('reservation_cnt_list', $reservation_cnt_list);
$smarty->assign('asset_reservation_list', $asset_reservation_list);

$smarty->display('asset_reservation.html');
?>
