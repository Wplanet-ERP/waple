<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_sales.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');

# Navigation & My Quick
$nav_prd_no  = "48";
$nav_title   = "매출 현황";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);

$kind_model         = Kind::Factory();
$cms_code           = "product_cms";
$cms_group_list     = $kind_model->getKindGroupList($cms_code);
$company_model      = Company::Factory();
$dp_company_option  = $company_model->getDpDisplayList();
$prd_g1_list = $prd_g2_list = [];

foreach($cms_group_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}
$prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];

$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);


# 검색 쿼리
$add_where = "1=1 AND w.delivery_state='4'";

// 상품(업무) 종류
if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
    $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code='{$sch_prd_g2}')";
}elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
    $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
}

$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);

$sch_w_no 			    = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
$sch_reg_s_date         = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : $week_val;
$sch_reg_e_date         = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : $today_val;
$sch_reg_date_type      = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "week";
$sch_dp_c_no 		    = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";

if(!empty($sch_w_no)){
    $add_where .= " AND w.w_no='{$sch_w_no}'";
    $smarty->assign('sch_w_no', $sch_w_no);
}

if(!empty($sch_reg_s_date) || !empty($sch_reg_e_date))
{
    if(!empty($sch_reg_s_date)){
        $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
        $add_where .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
        $smarty->assign('sch_reg_s_date', $sch_reg_s_date);
    }

    if(!empty($sch_reg_e_date)){
        $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
        $add_where .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
        $smarty->assign('sch_reg_e_date', $sch_reg_e_date);
    }
}
$smarty->assign('sch_reg_date_type', $sch_reg_date_type);

if(!empty($sch_dp_c_no)){
    $add_where .= " AND w.dp_c_no='{$sch_dp_c_no}'";
    $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
}

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

$total_dp_sales_list        = [];
$total_pro_sales_list       = [];
$total_dp_sales_sum_list    = getTotalDpSumInit();
$total_pro_sales_sum_list   = getTotalProSumInit();

# 리스트 쿼리
$cms_sales_sql = "
    SELECT
        w.w_no,
        w.c_no,
        w.c_name,
        w.prd_no,
        w.quantity,
        w.unit_price,
        w.dp_c_no,
        w.dp_c_name,
        (SELECT return_type FROM csm_return cr WHERE cr.order_number=w.order_number AND cr.shop_ord_no=w.shop_ord_no) as return_type
    FROM work_cms w
    WHERE {$add_where}
    ORDER BY w.w_no DESC
";
$cms_sales_query = mysqli_query($my_db, $cms_sales_sql);
while($cms_sales = mysqli_fetch_array($cms_sales_query))
{
    if(!isset($total_dp_sales_list[$cms_sales['dp_c_no']]))
    {
        $total_dp_sales_list[$cms_sales['dp_c_no']] = array(
            'c_name'            => $cms_sales['dp_c_name'],
            'ord_qty'           => 0,
            'ord_total'         => 0,
            'return_qty'        => 0,
            'return_total'      => 0,
            'trade_qty'         => 0,
            'trade_total'       => 0,
            'total_qty'         => 0,
            'total_price'       => 0,
            'total_price_vat'   => 0,
        );
    }

    $total_dp_sales_list[$cms_sales['dp_c_no']]['ord_qty']   += $cms_sales['quantity'];
    $total_dp_sales_list[$cms_sales['dp_c_no']]['ord_total'] += $cms_sales['unit_price'];
    $total_dp_sales_sum_list['ord_qty']     += $cms_sales['quantity'];
    $total_dp_sales_sum_list['ord_total']   += $cms_sales['unit_price'];

    if($cms_sales['return_type'] == '1'){
        $total_dp_sales_list[$cms_sales['dp_c_no']]['return_qty']   += $cms_sales['quantity'];
        $total_dp_sales_list[$cms_sales['dp_c_no']]['return_total'] += $cms_sales['unit_price'];
        $total_dp_sales_sum_list['return_qty']      += $cms_sales['quantity'];
        $total_dp_sales_sum_list['return_total']    += $cms_sales['unit_price'];
    }elseif($cms_sales['return_type'] == '2'){
        $total_dp_sales_list[$cms_sales['dp_c_no']]['trade_qty']   += $cms_sales['quantity'];
        $total_dp_sales_list[$cms_sales['dp_c_no']]['trade_total'] += $cms_sales['unit_price'];
        $total_dp_sales_sum_list['trade_qty']       += $cms_sales['quantity'];
        $total_dp_sales_sum_list['trade_total']     += $cms_sales['unit_price'];
    }

    if(!isset($total_pro_sales_list[$cms_sales["c_no"]]))
    {
        $total_pro_sales_list[$cms_sales["c_no"]] = array(
            'prd_name'          => $cms_sales['c_name'],
            'ord_qty'           => 0,
            'ord_total'         => 0,
            'return_qty'        => 0,
            'return_total'      => 0,
            'trade_qty'         => 0,
            'trade_total'       => 0,
            'total_qty'         => 0,
            'total_price'       => 0,
            'total_price_vat'   => 0,
        );
    }

    $total_pro_sales_list[$cms_sales["c_no"]]['ord_qty']   += $cms_sales['quantity'];
    $total_pro_sales_list[$cms_sales["c_no"]]['ord_total'] += $cms_sales['unit_price'];
    $total_pro_sales_sum_list['ord_qty']     += $cms_sales['quantity'];
    $total_pro_sales_sum_list['ord_total']   += $cms_sales['unit_price'];

    if($cms_sales['return_type'] == '1'){
        $total_pro_sales_list[$cms_sales["c_no"]]['return_qty']   += $cms_sales['quantity'];
        $total_pro_sales_list[$cms_sales["c_no"]]['return_total'] += $cms_sales['unit_price'];
        $total_pro_sales_sum_list['return_qty']      += $cms_sales['quantity'];
        $total_pro_sales_sum_list['return_total']    += $cms_sales['unit_price'];
    }elseif($cms_sales['return_type'] == '2'){
        $total_pro_sales_list[$cms_sales["c_no"]]['trade_qty']   += $cms_sales['quantity'];
        $total_pro_sales_list[$cms_sales["c_no"]]['trade_total'] += $cms_sales['unit_price'];
        $total_pro_sales_sum_list['trade_qty']       += $cms_sales['quantity'];
        $total_pro_sales_sum_list['trade_total']     += $cms_sales['unit_price'];
    }
}

$total_dp_sales_final_list      = [];
$total_dp_sales_tmp_list        = [];
$total_dp_sales_tmp_sum_list    = [];

if($total_dp_sales_list)
{
    foreach($total_dp_sales_list as $key => $total_dp_sales)
    {
        $dp_total_qty       = $total_dp_sales['ord_qty']-$total_dp_sales['return_qty']-$total_dp_sales['trade_qty'];
        $dp_total_price_vat = $total_dp_sales['ord_total']-$total_dp_sales['return_total']-$total_dp_sales['trade_total'];
        $dp_total_price     = round($dp_total_price_vat - ($dp_total_price_vat/11), 0);

        $total_dp_sales['total_qty']        = $dp_total_qty;
        $total_dp_sales['total_price']      = $dp_total_price;
        $total_dp_sales['total_price_vat']  = $dp_total_price_vat;

        $total_dp_sales_sum_list['total_qty']       += $dp_total_qty;
        $total_dp_sales_sum_list['total_price']     += $dp_total_price;
        $total_dp_sales_sum_list['total_price_vat'] += $dp_total_price_vat;

        $total_dp_sales_tmp_list[$key]      = $total_dp_sales;
        $total_dp_sales_tmp_sum_list[$key]  = $total_dp_sales['total_price_vat'];
    }

    if(!empty($total_dp_sales_tmp_list))
    {
        arsort($total_dp_sales_tmp_sum_list);
        foreach($total_dp_sales_tmp_sum_list as $sum_key => $sum_data){
            $total_dp_sales_final_list[$sum_key] = $total_dp_sales_tmp_list[$sum_key];
        }
    }

    $total_dp_sales_final_list['sum'] = $total_dp_sales_sum_list;
}

$total_pro_sales_final_list     = [];
$total_pro_sales_tmp_list       = [];
$total_pro_sales_tmp_sum_list   = [];

if($total_pro_sales_list)
{
    foreach($total_pro_sales_list as $key => $cms_sales)
    {
        $dp_total_qty       = $cms_sales['ord_qty']-$cms_sales['return_qty']-$cms_sales['trade_qty'];
        $dp_total_price_vat = $cms_sales['ord_total']-$cms_sales['return_total']-$cms_sales['trade_total'];
        $dp_total_price     = round($dp_total_price_vat - ($dp_total_price_vat/11), 0);

        $cms_sales['total_qty']        = $dp_total_qty;
        $cms_sales['total_price']      = $dp_total_price;
        $cms_sales['total_price_vat']  = $dp_total_price_vat;

        $total_pro_sales_sum_list['total_qty']       += $dp_total_qty;
        $total_pro_sales_sum_list['total_price']     += $dp_total_price;
        $total_pro_sales_sum_list['total_price_vat'] += $dp_total_price_vat;

        $total_pro_sales_tmp_list[$key]      = $cms_sales;
        $total_pro_sales_tmp_sum_list[$key]  = $cms_sales['total_price_vat'];
    }

    if(!empty($total_pro_sales_tmp_list))
    {
        arsort($total_pro_sales_tmp_sum_list);
        foreach($total_pro_sales_tmp_sum_list as $sum_key => $sum_data){
            $total_pro_sales_final_list[$sum_key] = $total_pro_sales_tmp_list[$sum_key];
        }
    }

    $total_pro_sales_final_list['sum'] = $total_pro_sales_sum_list;
}

$smarty->assign("dp_company_option", $dp_company_option);
$smarty->assign("total_dp_sales_list", $total_dp_sales_final_list);
$smarty->assign("total_pro_sales_list", $total_pro_sales_final_list);

$smarty->display('work_cms_sales_stats.html');
?>
