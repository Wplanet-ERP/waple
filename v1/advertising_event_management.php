<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/advertising.php');
require('inc/model/MyQuick.php');
require('inc/model/Advertising.php');
require('inc/model/Calendar.php');
require('inc/model/Company.php');
require('inc/model/Staff.php');

# Model Init
$event_model        = Advertising::Factory();
$event_model->setEventTable();
$advertise_model    = Advertising::Factory();
$event_sms_model    = Advertising::Factory();
$event_sms_model->setMainInit("advertising_event_sms", "ae_no");
$calendar_model     = Calendar::Factory();
$calendar_model->setScheduleTable();

# 프로세스 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "f_event_no")
{
    $ae_no      = isset($_POST['ae_no']) ? $_POST['ae_no'] : "";
    $event_no   = isset($_POST['val']) ? $_POST['val'] : "";

    if(!$event_model->update(array("ae_no" => $ae_no, "event_no" => $event_no))){
        echo "이벤트명 변경에 실패했습니다.";
    }else{
        echo "이벤트명을 변경했습니다.";
    }
    exit;
}
elseif($process == "f_dp_list")
{
    $ae_no      = isset($_POST['ae_no']) ? $_POST['ae_no'] : "";
    $dp_list    = isset($_POST['val']) ? implode(",", $_POST['val']) : "";

    if(!$event_model->update(array("ae_no" => $ae_no, "dp_list" => $dp_list))){
        echo "구매처 변경에 실패했습니다.";
    }else{
        echo "구매처를 변경했습니다.";
    }
    exit;
}
elseif($process == "f_is_active")
{
    $ae_no      = isset($_POST['ae_no']) ? $_POST['ae_no'] : "";
    $is_active  = isset($_POST['val']) ? $_POST['val'] : "";

    if(!$event_model->update(array("ae_no" => $ae_no, "is_active" => $is_active))){
        echo "중단 처리에 실패했습니다.";
    }else{
        echo "중단 처리했습니다.";
    }
    exit;
}
elseif($process == "f_event_s_date")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $ae_no      = isset($_POST['chk_ae_no']) ? $_POST['chk_ae_no'] : "";
    $chk_s_day  = isset($_POST['chk_day']) ? $_POST['chk_day'] : "";
    $chk_s_hour = isset($_POST['chk_hour']) ? $_POST['chk_hour'] : "";
    $chk_s_min  = isset($_POST['chk_min']) ? $_POST['chk_min'] : "";
    $upd_data   = array("ae_no" => $ae_no);

    if(!empty($chk_s_day)){
        $upd_data['event_s_date'] = $chk_s_day." {$chk_s_hour}:{$chk_s_min}";
    }else{
        $upd_data['event_s_date'] = "NULL";
    }

    if($event_model->update($upd_data))
    {
        if(!empty($upd_data['event_s_date'])){
            $chk_cal_sql = "UPDATE calendar_schedule SET cs_s_date='{$upd_data['event_s_date']}' WHERE cal_no='6' AND cal_id='mc_event' AND linked_table='advertising_event' AND linked_no='{$ae_no}'";
            mysqli_query($my_db, $chk_cal_sql);
        }
        exit("<script>alert('시작일시를 변경했습니다');location.href='advertising_event_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('시작일시 변경에 실패했습니다');location.href='advertising_event_management.php?{$search_url}';</script>");
    }
}
elseif($process == "f_event_e_date")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $ae_no      = isset($_POST['chk_ae_no']) ? $_POST['chk_ae_no'] : "";
    $chk_e_day  = isset($_POST['chk_day']) ? $_POST['chk_day'] : "";
    $chk_e_hour = isset($_POST['chk_hour']) ? $_POST['chk_hour'] : "";
    $chk_e_min  = isset($_POST['chk_min']) ? $_POST['chk_min'] : "";
    $upd_data   = array("ae_no" => $ae_no);

    if(!empty($chk_e_day)){
        $upd_data['event_e_date'] = $chk_e_day." {$chk_e_hour}:{$chk_e_min}";
    }else{
        $upd_data['event_e_date'] = "NULL";
    }

    if($event_model->update($upd_data))
    {
        if(!empty($upd_data['event_e_date'])){
            $chk_cal_sql = "UPDATE calendar_schedule SET cs_e_date='{$upd_data['event_e_date']}' WHERE cal_no='6' AND cal_id='mc_event' AND linked_table='advertising_event' AND linked_no='{$ae_no}'";
            mysqli_query($my_db, $chk_cal_sql);
        }
        exit("<script>alert('종료일시를 변경했습니다');location.href='advertising_event_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('종료일시 변경에 실패했습니다');location.href='advertising_event_management.php?{$search_url}';</script>");
    }
}
elseif($process == "add_event")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $new_am_no          = isset($_POST['new_am_no']) ? $_POST['new_am_no'] : "";
    $new_event_no       = isset($_POST['new_event_no']) ? $_POST['new_event_no'] : "";
    $new_explain        = isset($_POST['new_explain']) ? addslashes(trim($_POST['new_explain'])) : "";
    $new_dp_list        = isset($_POST['new_dp_list']) ? implode(",",$_POST['new_dp_list']) : "";
    $new_event_s_day    = isset($_POST['new_event_s_day']) ? $_POST['new_event_s_day'] : "";
    $new_event_s_hour   = isset($_POST['new_event_s_hour']) ? $_POST['new_event_s_hour'] : "";
    $new_event_s_min    = isset($_POST['new_event_s_min']) ? $_POST['new_event_s_min'] : "";
    $new_event_e_day    = isset($_POST['new_event_e_day']) ? $_POST['new_event_e_day'] : "";
    $new_event_e_hour   = isset($_POST['new_event_e_hour']) ? $_POST['new_event_e_hour'] : "";
    $new_event_e_min    = isset($_POST['new_event_e_min']) ? $_POST['new_event_e_min'] : "";
    $new_event_s_date   = "{$new_event_s_day} {$new_event_s_hour}:{$new_event_s_min}:00";
    $new_event_e_date   = "{$new_event_e_day} {$new_event_e_hour}:{$new_event_e_min}:00";
    $regdate            = date("Y-m-d H:i:s");

    $insert_data = array(
        "event_no"      => $new_event_no,
        "am_no"         => $new_am_no,
        "explain"       => $new_explain,
        "dp_list"       => $new_dp_list,
        "event_s_date"  => $new_event_s_date,
        "event_e_date"  => $new_event_e_date,
        "reg_s_no"      => $session_s_no,
        "regdate"       => $regdate,
    );

    if($event_model->insert($insert_data))
    {
        $event_option = getEventOption();
        $cs_content   = $event_option[$new_event_no];
        if($new_am_no) {
            $cs_content .= "\r\n광고 관리번호 : {$new_am_no}";
        }

        $cal_ins_data = array(
            "cs_type"       => "normal",
            "cs_category"   => "event",
            "cal_no"        => "6",
            "cal_id"        => "mc_event",
            "cs_permission" => '1',
            "cs_title"      => $event_option[$new_event_no],
            "cs_s_no"       => $session_s_no,
            "cs_all"        => 0,
            "cs_s_date"     => $new_event_s_date,
            "cs_e_date"     => $new_event_e_date,
            "cs_content"    => $cs_content,
            "linked_table"  => "advertising_event",
            "linked_no"     => $event_model->getInsertId(),
            "regdate"       => $regdate
        );

        $calendar_model->insert($cal_ins_data);

        exit("<script>alert('이벤트 등록에 성공했습니다');location.href='advertising_event_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('이벤트 등록에 실패했습니다');location.href='advertising_event_management.php?{$search_url}';</script>");
    }
}
elseif($process == "f_event_sms")
{
    $staff_model    = Staff::Factory();
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $ae_no          = isset($_POST['chk_ae_no']) ? $_POST['chk_ae_no'] : "";
    $chk_value      = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $event_item     = $event_model->getItem($ae_no);
    $staff_item     = $staff_model->getItem($event_item['reg_s_no']);

    $confirm_exist_sms  = $event_sms_model->confirmEventSms($ae_no);
    $chk_exist_sms      = $event_sms_model->checkEventSms($ae_no);
    $message            = "종료 전 SMS 알림 변경에 실패했습니다.";

    if($confirm_exist_sms){
        exit("<script>alert('메세지 전송이 완료된 건 입니다.');location.href='advertising_event_management.php?{$search_url}';</script>");
    }

    if($chk_exist_sms && $chk_value == "1"){
        exit("<script>alert('등록된 SMS 알림이 있습니다.');location.href='advertising_event_management.php?{$search_url}';</script>");
    }

    if(!$chk_exist_sms && $chk_value == "2"){
        exit("<script>alert('이미 SMS 알림 삭제된 건입니다.');location.href='advertising_event_management.php?{$search_url}';</script>");
    }

    $upd_data       = array("ae_no" => $ae_no, "is_sms" => $chk_value);
    $insert_data    = array("ae_no" => $ae_no, "sms_state" => 1, "s_no" => $staff_item['s_no'], "name" => $staff_item['s_name'], "hp" => $staff_item["hp"], "sms_date" => date("Y-m-d", strtotime("{$event_item['event_e_date']} -2 days"))." 11:00:00", "regdate" => date("Y-m-d H:i:s"));

    if($event_model->update($upd_data))
    {
        if($chk_value == "2"){
            $event_sms_model->delete($ae_no);
            $message = "종료 전 SMS 알림을 삭제했습니다";
        }elseif($chk_value == "1"){
            $event_sms_model->insert($insert_data);
            $message = "종료 전 SMS 알림을 등록했습니다";
        }
    }
    exit("<script>alert('{$message}');location.href='advertising_event_management.php?{$search_url}';</script>");
}

# Navigation & My Quick
$nav_prd_no  = "181";
$nav_title   = "커머스 이벤트 관리";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

$company_model      = Company::Factory();
$dp_company_list    = $company_model->getDpList();

# 검색조건
$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);

$add_where              = "1=1";
$new_am_no              = isset($_GET['new_am_no']) ? $_GET['new_am_no'] : "";
$sch_am_no              = isset($_GET['sch_am_no']) ? $_GET['sch_am_no'] : "";
$sch_ae_no              = isset($_GET['sch_ae_no']) ? $_GET['sch_ae_no'] : "";
$sch_event_s_date       = isset($_GET['sch_event_s_date']) ? $_GET['sch_event_s_date'] : $month_val;
$sch_event_e_date       = isset($_GET['sch_event_e_date']) ? $_GET['sch_event_e_date'] : "";
$sch_event_date_type    = isset($_GET['sch_event_date_type']) ? $_GET['sch_event_date_type'] : "month";
$sch_is_active          = isset($_GET['sch_is_active']) ? $_GET['sch_is_active'] : "";
$sch_event              = isset($_GET['sch_event']) ? $_GET['sch_event'] : "";
$sch_reg_name           = isset($_GET['sch_reg_name']) ? $_GET['sch_reg_name'] : $session_name;

if(!empty($sch_am_no)){
    $add_where          .= " AND ae.am_no = '{$sch_am_no}'";
    $smarty->assign("sch_am_no", $sch_am_no);
}

if(!empty($sch_ae_no)){
    $add_where          .= " AND ae.ae_no = '{$sch_ae_no}'";
    $smarty->assign("sch_ae_no", $sch_ae_no);
}

if(!empty($sch_event_s_date)){
    $event_s_datetime    = "{$sch_event_s_date} 00:00:00";
    $add_where          .= " AND ae.event_s_date >= '{$event_s_datetime}'";
    $smarty->assign("sch_event_s_date", $sch_event_s_date);
}

if(!empty($sch_event_e_date)){
    $event_e_datetime    = "{$sch_event_e_date} 23:59:59";
    $add_where          .= " AND ae.event_s_date <= '{$event_e_datetime}'";
    $smarty->assign("sch_event_e_date", $sch_event_e_date);
}
$smarty->assign("sch_event_date_type", $sch_event_date_type);

if(!empty($sch_is_active))
{
    $cur_chk_date = date("Y-m-d H:i:s");
    if($sch_is_active == '1'){
        $add_where  .= " AND (ae.is_active = '1' AND (ae.event_e_date >= '{$cur_chk_date}' OR ae.event_e_date IS NULL))";
    }elseif($sch_is_active == '2'){
        $add_where  .= " AND ae.is_active = '{$sch_is_active}'";
    }elseif($sch_is_active == '3'){
        $add_where  .= " AND (ae.is_active = '1' AND ae.event_e_date < '{$cur_chk_date}')";
    }

    $smarty->assign("sch_is_active", $sch_is_active);
}

if(!empty($sch_event)){
    $add_where .= " AND ae.event_no = '{$sch_event}'";
    $smarty->assign("sch_event", $sch_event);
}

if(!empty($sch_reg_name)){
    $add_where .= " AND ae.reg_s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_reg_name}%')";
    $smarty->assign("sch_reg_name", $sch_reg_name);
}

if(!empty($new_am_no))
{
    $advertise_item     = $advertise_model->getAdvertisingItem($new_am_no);
    $new_event_s_day    = date("Y-m-d", strtotime($advertise_item['adv_s_date']));
    $new_event_s_hour   = date("H", strtotime($advertise_item['adv_s_date']));
    $new_event_s_min    = date("i", strtotime($advertise_item['adv_s_date']));
    $new_event_e_day    = date("Y-m-d", strtotime($advertise_item['adv_e_date']));
    $new_event_e_hour   = date("H", strtotime($advertise_item['adv_e_date']));
    $new_event_e_min    = date("i", strtotime($advertise_item['adv_e_date']));

    $smarty->assign("new_event_s_day", $new_event_s_day);
    $smarty->assign("new_event_s_hour", $new_event_s_hour);
    $smarty->assign("new_event_s_min", $new_event_s_min);
    $smarty->assign("new_event_e_day", $new_event_e_day);
    $smarty->assign("new_event_e_hour", $new_event_e_hour);
    $smarty->assign("new_event_e_min", $new_event_e_min);
    $smarty->assign("new_am_no", $new_am_no);
}

# 전체 게시물 수
$event_total_sql        = "SELECT count(ae_no) as cnt FROM advertising_event `ae` WHERE {$add_where}";
$event_total_query      = mysqli_query($my_db, $event_total_sql);
$event_total_result     = mysqli_fetch_array($event_total_query);
$event_total 	        = $event_total_result['cnt'];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($event_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = (!empty($new_am_no)) ? "" : getenv("QUERY_STRING");
$page_list	= pagelist($pages, "advertising_event_management.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $event_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

$event_management_sql = "
    SELECT
        *,
        (SELECT s_name FROM staff AS s WHERE s.s_no=`ae`.reg_s_no) AS reg_s_name,
        (SELECT sms_state FROM advertising_event_sms AS aes WHERE aes.ae_no=`ae`.ae_no) AS sms_state
    FROM advertising_event `ae`
    WHERE {$add_where}
    ORDER BY ae_no DESC
    LIMIT {$offset}, {$num}
";
$event_management_query = mysqli_query($my_db, $event_management_sql);
$event_management_list  = [];
$cur_date               = date("Ymd");
$prev_date              = date("Ymd",strtotime("-1 days"));
$cur_day                = date("w");
$cur_datetime           = date("YmdHis");
$cur_chk_am_datetime    = $cur_date."090000";
$cur_chk_pm_datetime    = $cur_date."140000";
$prev_chk_pm_datetime   = $prev_date."140000";
$chk_event_sms_datetime = $cur_date."100000";
$cur_chk_datetime       = "";

if($cur_day > 4){
    $del_day = 5-$cur_day;
    if($del_day == 0)
    {
        if($cur_datetime < $cur_chk_am_datetime && $cur_datetime > $prev_chk_pm_datetime){
            $cur_chk_datetime = $prev_chk_pm_datetime;
        }elseif($cur_datetime > $cur_chk_am_datetime && $cur_datetime < $cur_chk_pm_datetime){
            $cur_chk_datetime = $cur_chk_am_datetime;
        }else{
            $cur_chk_datetime = $cur_chk_pm_datetime;
        }
    }else{
        $cur_chk_datetime = date("Ymd", strtotime("{$del_day} days"))."140000";
    }

}else{
    if($cur_day == '1' && $cur_datetime < $cur_chk_am_datetime){
        $cur_chk_datetime = date("Ymd", strtotime("-3 days"))."140000";
    }else{
        if($cur_datetime > $cur_chk_pm_datetime){
            $cur_chk_datetime = $cur_chk_pm_datetime;
        }elseif($cur_datetime < $cur_chk_am_datetime && $cur_datetime > $prev_chk_pm_datetime){
            $cur_chk_datetime = $prev_chk_pm_datetime;
        }else{
            $cur_chk_datetime = $cur_chk_am_datetime;
        }
    }
}

while($event_management = mysqli_fetch_assoc($event_management_query))
{
    $event_management['reg_day']            = !empty($event_management['regdate']) ? date("Y/m/d", strtotime($event_management['regdate'])) : "";
    $event_management['reg_time']           = !empty($event_management['regdate']) ? date("H:i", strtotime($event_management['regdate'])) : "";
    $event_management['chk_reg_day']        = !empty($event_management['regdate']) ? date("Ymd", strtotime($event_management['regdate'])) : "";
    $event_management['event_s_day']        = !empty($event_management['event_s_date']) ? date("Y-m-d", strtotime($event_management['event_s_date'])) : "";
    $event_management['event_s_hour']       = !empty($event_management['event_s_date']) ? date("H", strtotime($event_management['event_s_date'])) : "";
    $event_management['event_s_min']        = !empty($event_management['event_s_date']) ? date("i", strtotime($event_management['event_s_date'])) : "";
    $event_management['event_e_day']        = !empty($event_management['event_e_date']) ? date("Y-m-d", strtotime($event_management['event_e_date'])) : "";
    $event_management['event_e_hour']       = !empty($event_management['event_e_date']) ? date("H", strtotime($event_management['event_e_date'])) : "";
    $event_management['event_e_min']        = !empty($event_management['event_e_date']) ? date("i", strtotime($event_management['event_e_date'])) : "";
    $event_management['dp_company_list']    = !empty($event_management['dp_list']) ? explode(",", $event_management['dp_list']) : [];

    $chk_event_s_datetime   = date("YmdHis", strtotime($event_management['event_s_date']));
    $chk_event_e_datetime   = date("YmdHis", strtotime($event_management['event_e_date']));

    $event_management["s_date_editable"] = false;
    if(!empty($event_management['event_s_date']))
    {
        if($event_management['chk_reg_day'] == $cur_date) {
            $event_management["s_date_editable"] = true;
        }elseif($cur_chk_datetime < $chk_event_s_datetime){
            $event_management["s_date_editable"] = true;
        }
    }else{
        $event_management["s_date_editable"] = true;
    }

    $event_management["e_date_editable"] = false;
    if(!empty($event_management['event_e_date']))
    {
        if($event_management['chk_reg_day'] == $cur_date) {
            $event_management["e_date_editable"] = true;
        }elseif($cur_chk_datetime < $chk_event_e_datetime){
            $event_management["e_date_editable"] = true;
        }
    }
    else {
        $event_management["e_date_editable"] = false;
    }

    $event_management['is_duplicate'] = false;
    if($event_management['is_active'] == '1')
    {
        $chk_dup_sql = "
            SELECT 
               COUNT(ae_no) as cnt 
            FROM advertising_event ae 
            WHERE event_no = '{$event_management['event_no']}' AND ae_no != '{$event_management['ae_no']}' AND is_active='1' AND 
            (
                (
                    (event_s_date >= '{$event_management['event_s_date']}' AND event_s_date < '{$event_management['event_e_date']}') OR
                    (event_e_date >= '{$event_management['event_s_date']}' AND event_e_date < '{$event_management['event_e_date']}')
                ) OR (event_s_date <= '{$event_management['event_s_date']}' AND event_e_date > '{$event_management['event_e_date']}')
            ) AND dp_list LIKE '%{$event_management['dp_list']}%'
        ";
        $chk_dup_query  = mysqli_query($my_db, $chk_dup_sql);
        $chk_dup_result = mysqli_fetch_assoc($chk_dup_query);

        if($chk_dup_result['cnt'] > 0){
            $event_management['is_duplicate'] = true;
        }
    }

    $event_management["is_sms_editable"] = false;
    $event_sms_end_datetime = date("YmdHis", strtotime("{$event_management['event_e_date']} -2 days"));
    if($chk_event_sms_datetime <= $event_sms_end_datetime){
        $event_management["is_sms_editable"] = true;
    }

    $event_management_list[] = $event_management;
}


$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("hour_option", getHourOption());
$smarty->assign("min_option", getMinOption());
$smarty->assign("event_option", getEventOption());
$smarty->assign("event_state_option", getEventStateOption());
$smarty->assign("dp_company_list", $dp_company_list);
$smarty->assign("event_management_list", $event_management_list);

$smarty->display('advertising_event_management.html');
?>
