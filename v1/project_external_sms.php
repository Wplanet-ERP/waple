<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Message.php');

if ($_POST)
{
	$message_model 	= Message::Factory();
	$sms_title		= isset($_POST['title']) ? $_POST['title'] : "";
	$receiver_tmp	= isset($_POST['receiver_list']) ? $_POST['receiver_list'] : "";
	$sms_msg		= isset($_POST['sms_msg']) ? $_POST['sms_msg'] : "";
	$send_phone		= isset($_POST['send_phone']) ? $_POST['send_phone'] : "";
	$send_data_list = [];

	$receiver_list  = str_replace(" ", "", $receiver_tmp);
	$receiver_list  = preg_replace("/\r\n|\r|\n/", "||", $receiver_list);
	$receiver_list 	= explode("||", $receiver_list);

	if (!$send_phone) {
		$send_hp = "02-830-1912";
	}

	$sms_msg  		= addslashes($sms_msg);
	$sms_msg_len 	= mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
	$msg_type 		= ($sms_msg_len > 90) ? "L" : "S";
	$send_name 		= $session_name;

	$cur_datetime	= date("YmdHis");
	$cm_id 			= 1;
	$c_info 		= "6"; # 강의/멘토링 안내문자

	foreach ($receiver_list as $receiver_hp)
	{
		$msg_id 	 = "W{$cur_datetime}".sprintf('%04d', $cm_id++);

		$send_data_list[$msg_id] = array(
			"msg_id"        => $msg_id,
			"msg_type"      => $msg_type,
			"sender"        => $send_name,
			"sender_hp"     => $send_phone,
			"receiver"      => $receiver,
			"receiver_hp"   => $receiver_hp,
			"title"         => $sms_title,
			"content"       => $sms_msg,
			"cinfo"         => $c_info,
		);
	}

	if(!empty($send_data_list))
	{
		$result_data = $message_model->sendMessage($send_data_list);

		if ($result_data['result']) {
			exit("<script>alert('문자 메시지를 발송하였습니다.'); if (self) self.close();</script>");
		}
	}

	exit("<script>alert('문자 메세지 발송에 실패했습니다.');history.back();</script>");
}

$title 				= "강의/멘토링 확인서 안내 문자";
$sms_text 			= "안녕하세요 강사님. 와이즈플래닛입니다.\r\nhttps://wise-expert.co.kr 로그인 후 강의참여 수락 및 강의확인서 작성 부탁드립니다.\r\n감사합니다.";
$init_caller   		= "010-2705-2943";
$init_receiver_list = [];
$cur_date			= date('Y-m-d');

$pj_no         = isset($_GET['pj_no']) ? $_GET['pj_no'] : "";
$pj_comp_sql   = "SELECT c.c_name, c.tel FROM company as c WHERE c.c_no IN(SELECT DISTINCT per.pj_c_no FROM project_external_report per WHERE per.pj_no='{$pj_no}' AND per.pj_participation IN(1,2) AND per.active='1' AND per.pj_c_no NOT IN(SELECT lec.c_no FROM lector_profile lec WHERE lec.display='1'))";
$pj_comp_query = mysqli_query($my_db, $pj_comp_sql);
while($pj_comp_result = mysqli_fetch_assoc($pj_comp_query))
{
	$init_receiver_list[] = $pj_comp_result['tel']." ({$pj_comp_result['c_name']})";
}

$smarty->assign("title", $title);
$smarty->assign("sms_text", $sms_text);
$smarty->assign("init_caller", $init_caller);
$smarty->assign("init_receiver_list", $init_receiver_list);

$smarty->display('project_external_sms.html');
?>
