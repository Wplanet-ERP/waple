<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/advertising.php');
require('inc/model/MyQuick.php');
require('inc/model/Advertising.php');

# Model Init
$advertise_model    = Advertising::Factory();
$application_model  = Advertising::Factory();
$application_model->setMainInit("advertising_application", "aa_no");
$event_model        = Advertising::Factory();
$event_model->setEventTable();

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "f_product")
{
    $am_no      = isset($_POST['am_no']) ? $_POST['am_no'] : "";
    $product    = isset($_POST['val']) ? $_POST['val'] : "";

    if(!$advertise_model->update(array("am_no" => $am_no, "product" => $product))){
        echo "상품명 변경에 실패했습니다.";
    }else{
        echo "상품명을 변경했습니다.";
    }
    exit;
}
elseif($process == "f_fee_per")
{
    $am_no      = isset($_POST['am_no']) ? $_POST['am_no'] : "";
    $fee_per    = isset($_POST['val']) ? $_POST['val'] : "";

    if(!$advertise_model->update(array("am_no" => $am_no, "fee_per" => $fee_per))){
        echo "수수료 변경에 실패했습니다.";
    }else{
        echo "수수료를 변경했습니다.";
    }
    exit;
}
elseif($process == "modify_state")
{
    $chk_am_no      = isset($_POST['chk_am_no']) ? $_POST['chk_am_no'] : "";
    $chk_value      = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $upd_data       = array("am_no" => $chk_am_no, "state" => $chk_value);

    if($advertise_model->update($upd_data))
    {
        $adv_item = $advertise_model->getAdvertisingItem($chk_am_no);

        if($chk_value == '3')
        {
            $adv_date_option    = getDayShortOption();
            $adv_media_option   = getAdvertisingMediaOption();
            $adv_product_option = getAdvertisingProductOption();
            $adv_s_date_text    = $adv_item['adv_s_day']." (".$adv_date_option[date("w", strtotime($adv_item['adv_s_day']))].") ".$adv_item['adv_s_time'];
            $adv_media_name     = $adv_media_option[$adv_item['media']];
            $adv_product_name   = $adv_product_option[$adv_item['product']];

            $adv_application_sql    = "SELECT aa_no, s_no, s_name, (SELECT COUNT(wcc_no) FROM waple_chat_content wcc WHERE wcc.wc_no='2' AND alert_type='61' AND alert_check='ADVERTISE_CONFIRM_{$chk_am_no}' AND wcc.s_no=ap.s_no) as chat_cnt FROM advertising_application ap WHERE ap.am_no='{$chk_am_no}'";
            $adv_application_query  = mysqli_query($my_db, $adv_application_sql);
            while($adv_application = mysqli_fetch_assoc($adv_application_query))
            {
                if($adv_application['chat_cnt'] == 0)
                {
                    $adv_msg = "[NOSP 입찰확정 알림] {$adv_media_name}, {$adv_product_name} {$adv_s_date_text}";
                    $adv_msg .= "\r\nhttps://work.wplanet.co.kr/v1/advertising_management.php?sch_no={$chk_am_no}";
                    $adv_msg = addslashes($adv_msg);
                    $chk_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$adv_application['s_no']}', content='{$adv_msg}', alert_type='61', alert_check='ADVERTISE_CONFIRM_{$chk_am_no}', regdate=now()";
                    mysqli_query($my_db, $chk_ins_sql);
                }
            }
        }

        exit("<script>alert('진행상태를 변경했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('진행상태 변경에 실패했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }
}
elseif($process == 'modify_multi_state')
{
    $chk_am_no_list = isset($_POST['chk_am_no_list']) ? explode(",", $_POST['chk_am_no_list']) : "";
    $chk_value      = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $result         = true;
    $success_cnt    = 0;

    foreach($chk_am_no_list as $chk_am_no)
    {
        $upd_data  = array("am_no" => $chk_am_no, "state" => $chk_value);

        if($advertise_model->update($upd_data))
        {
            $success_cnt++;

            $adv_item   = $advertise_model->getAdvertisingItem($chk_am_no);

            if($chk_value == '3')
            {
                $adv_date_option    = getDayShortOption();
                $adv_media_option   = getAdvertisingMediaOption();
                $adv_product_option = getAdvertisingProductOption();
                $adv_s_date_text    = $adv_item['adv_s_day']." (".$adv_date_option[date("w", strtotime($adv_item['adv_s_day']))].") ".$adv_item['adv_s_time'];
                $adv_media_name     = $adv_media_option[$adv_item['media']];
                $adv_product_name   = $adv_product_option[$adv_item['product']];

                $adv_application_sql    = "SELECT aa_no, s_no, s_name, (SELECT COUNT(wcc_no) FROM waple_chat_content wcc WHERE wcc.wc_no='2' AND alert_type='61' AND alert_check='ADVERTISE_CONFIRM_{$chk_am_no}' AND wcc.s_no=ap.s_no) as chat_cnt FROM advertising_application ap WHERE ap.am_no='{$chk_am_no}'";
                $adv_application_query  = mysqli_query($my_db, $adv_application_sql);
                while($adv_application = mysqli_fetch_assoc($adv_application_query))
                {
                    if($adv_application['chat_cnt'] == 0)
                    {
                        $adv_msg     = "[NOSP 입찰확정 알림] {$adv_media_name}, {$adv_product_name} {$adv_s_date_text}";
                        $adv_msg    .= "\r\nhttps://work.wplanet.co.kr/v1/advertising_management.php?sch_no={$chk_am_no}";
                        $adv_msg     = addslashes($adv_msg);
                        $chk_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$adv_application['s_no']}', content='{$adv_msg}', alert_type='61', alert_check='ADVERTISE_CONFIRM_{$chk_am_no}', regdate=now()";
                        mysqli_query($my_db, $chk_ins_sql);
                    }
                }
            }
        }
    }

    if($success_cnt > 0) {
        exit("<script>alert('{$success_cnt}건 진행상태 변경에 성공했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('진행상태 변경에 실패했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_agency")
{
    $chk_am_no      = isset($_POST['chk_am_no']) ? $_POST['chk_am_no'] : "";
    $chk_value      = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $adv_item       = $advertise_model->getAdvertisingItem($chk_am_no);
    $upd_data       = array("am_no" => $chk_am_no, "state" => 3, "agency" => $chk_value);

    if($adv_item['fee_per'] == 0 && !empty($adv_item['product'])){
        $auto_fee_option = getAutoAdvertisingFee();
        $fee_per         = isset($auto_fee_option[$chk_value][$adv_item['product']]) ? $auto_fee_option[$chk_value][$adv_item['product']] : 0;
        if($fee_per > 0){
            $upd_data['fee_per'] = $fee_per;
        }
    }

    if($advertise_model->update($upd_data)) {
        exit("<script>alert('매체대행사를 변경했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('매체대행사 변경에 실패했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_multi_agency")
{
    $chk_am_no_list = isset($_POST['chk_am_no_list']) ? explode(",", $_POST['chk_am_no_list']) : "";
    $chk_value      = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $result         = true;
    $success_cnt    = 0;

    foreach($chk_am_no_list as $chk_am_no)
    {
        $upd_data  = array("am_no" => $chk_am_no, "state" => 3, "agency" => $chk_value);
        $adv_item  = $advertise_model->getAdvertisingItem($chk_am_no);

        if($adv_item['fee_per'] == 0 && !empty($adv_item['product'])){
            $auto_fee_option = getAutoAdvertisingFee();
            $fee_per         = isset($auto_fee_option[$chk_value][$adv_item['product']]) ? $auto_fee_option[$chk_value][$adv_item['product']] : 0;
            if($fee_per > 0){
                $upd_data['fee_per'] = $fee_per;
            }
        }

        if($advertise_model->update($upd_data)) {
            $success_cnt++;
        }
    }

    if($success_cnt > 0) {
        exit("<script>alert('{$success_cnt}건 매체대행사 변경에 성공했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('매체대행사 변경에 실패했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }
}
elseif($process == 'add_application')
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $chk_am_no      = isset($_POST['chk_am_no']) ? $_POST['chk_am_no'] : "";

    $insert_data    = array(
        "am_no"     => $chk_am_no,
        "s_no"      => $session_s_no,
        "s_name"    => $session_name,
        "regdate"   => date("Y-m-d H:i:s")
    );

    if($application_model->checkApplication($chk_am_no, $session_s_no)){
        exit("<script>alert('이미 신청한 신청자입니다. 추가에 실패했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }
    
    if($application_model->insert($insert_data)) {
        exit("<script>alert('신청자 추가에 성공했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('신청자 추가에 실패했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }
}
elseif($process == 'del_application')
{
    $chk_am_no      = isset($_POST['chk_am_no']) ? $_POST['chk_am_no'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if($application_model->delete($chk_am_no)) {
        exit("<script>alert('신청자 삭제에 성공했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('신청자 삭제에 실패했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }
}
elseif($process == 'add_advertising')
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $new_media      = isset($_POST['new_media']) ? $_POST['new_media'] : "";
    $new_product    = isset($_POST['new_product']) ? $_POST['new_product'] : "";
    $new_state      = isset($_POST['new_state']) ? $_POST['new_state'] : "1";
    $new_agency     = isset($_POST['new_agency']) ? $_POST['new_agency'] : "";
    $new_fee_fer    = isset($_POST['new_fee_per']) ? $_POST['new_fee_per'] : "";
    $new_adv_s_day  = isset($_POST['new_adv_s_day']) ? $_POST['new_adv_s_day'] : "";
    $new_adv_s_hour = isset($_POST['new_adv_s_hour']) ? $_POST['new_adv_s_hour'] : "";
    $new_adv_s_min  = isset($_POST['new_adv_s_min']) ? $_POST['new_adv_s_min'] : "";
    $new_adv_e_day  = isset($_POST['new_adv_e_day']) ? $_POST['new_adv_e_day'] : "";
    $new_adv_e_hour = isset($_POST['new_adv_e_hour']) ? $_POST['new_adv_e_hour'] : "";
    $new_adv_e_min  = isset($_POST['new_adv_e_min']) ? $_POST['new_adv_e_min'] : "";
    $new_price      = isset($_POST['new_price']) ? str_replace(",", "", $_POST['new_price']) : "";
    $new_adv_s_date = "{$new_adv_s_day} {$new_adv_s_hour}:{$new_adv_s_min}:00";
    $new_adv_e_date = "{$new_adv_e_day} {$new_adv_e_hour}:{$new_adv_e_min}:00";
    $regdate        = date("Y-m-d H:i:s");

    $insert_data = array(
        "media"         => $new_media,
        "product"       => $new_product,
        "agency"        => $new_agency,
        "fee_per"       => $new_fee_fer,
        "price"         => $new_price,
        "adv_s_date"    => $new_adv_s_date,
        "adv_e_date"    => $new_adv_e_date,
        "reg_s_no"      => $session_s_no,
        "regdate"       => $regdate,
    );

    if($advertise_model->insert($insert_data)) {
        exit("<script>alert('광고등록에 성공했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('광고 등록에 실패했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }
}
elseif($process == "cancel_application")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $chk_sql    = "SELECT `am`.am_no FROM advertising_management AS `am` WHERE `am`.state='1' AND (SELECT COUNT(*) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no) = 0";
    $chk_query  = mysqli_query($my_db, $chk_sql);
    $upd_data   = [];
    while($chk_result = mysqli_fetch_assoc($chk_query))
    {
        $upd_data[] = array(
            "am_no" => $chk_result['am_no'],
            "state" => 6
        );
    }

    if($advertise_model->multiUpdate($upd_data)) {
        exit("<script>alert('미신청(종료) 처리에 성공했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('미신청(종료) 처리에 실패했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }
}
elseif($process == "progress_application")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $chk_sql    = "SELECT `am`.am_no FROM advertising_management AS `am` WHERE `am`.state='1' AND (SELECT COUNT(*) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no) > 1";
    $chk_query  = mysqli_query($my_db, $chk_sql);
    $upd_data   = [];
    while($chk_result = mysqli_fetch_assoc($chk_query))
    {
        $upd_data[] = array(
            "am_no" => $chk_result['am_no'],
            "state" => 2
        );
    }

    if($advertise_model->multiUpdate($upd_data)) {
        exit("<script>alert('부킹중 처리에 성공했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('부킹중 처리에 실패했습니다');location.href='advertising_management.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "180";
$nav_title   = "NOSP/GFA 광고관리";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('+1 weeks'));
$month_val  = date('Y-m-d', strtotime('+1 months'));
$months_val = date('Y-m-d', strtotime('+3 months'));
$year_val   = date('Y-m-d', strtotime('+1 years'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);

$add_where          = "1=1";
$sch_adv_s_date     = isset($_GET['sch_adv_s_date']) ? $_GET['sch_adv_s_date'] : $today_val;
$sch_adv_e_date     = isset($_GET['sch_adv_e_date']) ? $_GET['sch_adv_e_date'] : $month_val;
$sch_adv_date_type  = isset($_GET['sch_adv_date_type']) ? $_GET['sch_adv_date_type'] : "month";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_media          = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_product        = isset($_GET['sch_product']) ? $_GET['sch_product'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : 3;
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_application    = isset($_GET['sch_application']) ? $_GET['sch_application'] : $session_name;
$sch_application_my = isset($_GET['sch_application_my']) ? $_GET['sch_application_my'] : "";
$sch_is_application = isset($_GET['sch_is_application']) ? $_GET['sch_is_application'] : "";
$sch_notice         = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$quick_btn          = isset($_GET['quick_btn']) ? $_GET['quick_btn'] : 5;
$smarty->assign("quick_btn", $quick_btn);

if(!empty($sch_adv_s_date)){
    $adv_s_datetime  = $sch_adv_s_date." 00:00:00";
    $add_where      .= " AND am.adv_s_date >= '{$adv_s_datetime}'";
    $smarty->assign("sch_adv_s_date", $sch_adv_s_date);
}

if(!empty($sch_adv_e_date)){
    $adv_e_datetime  = $sch_adv_e_date." 23:59:59";
    $add_where      .= " AND am.adv_s_date <= '{$adv_e_datetime}'";
    $smarty->assign("sch_adv_e_date", $sch_adv_e_date);
}
$smarty->assign("sch_adv_date_type", $sch_adv_date_type);

if(!empty($sch_no)){
    $add_where .= " AND am.am_no = '{$sch_no}'";
    $smarty->assign("sch_no", $sch_no);
}

if(!empty($sch_media)){
    $add_where .= " AND am.media = '{$sch_media}'";
    $smarty->assign("sch_media", $sch_media);
}

if(!empty($sch_product)){
    $add_where .= " AND am.product = '{$sch_product}'";
    $smarty->assign("sch_product", $sch_product);
}

if(!empty($sch_state)){
    $add_where .= " AND am.state = '{$sch_state}'";
    $smarty->assign("sch_state", $sch_state);
}

if(!empty($sch_agency)){
    $add_where .= " AND am.agency = '{$sch_agency}'";
    $smarty->assign("sch_agency", $sch_agency);
}

if($sch_application_my == 1){
    $sch_application = $session_name;
    $smarty->assign("sch_application_my", $sch_application_my);
}

if(!empty($sch_application)){
    $add_where .= " AND am.am_no IN (SELECT `aa`.am_no FROM advertising_application `aa` WHERE `aa`.s_name LIKE '%{$sch_application}%')";
    $smarty->assign("sch_application", $sch_application);
}

if(!empty($sch_is_application)){
    if($sch_is_application == '1'){
        $add_where .= " AND (SELECT COUNT(`aa`.am_no) FROM advertising_application `aa` WHERE `aa`.am_no=`am`.am_no) > 0";
    }elseif($sch_is_application == '2'){
        $add_where .= " AND (SELECT COUNT(`aa`.am_no) FROM advertising_application `aa` WHERE `aa`.am_no=`am`.am_no) = 0";
    }
    $smarty->assign("sch_is_application", $sch_is_application);
}

if(!empty($sch_notice)){
    $add_where .= " AND am.notice LIKE '%{$sch_notice}%'";
    $smarty->assign("sch_notice", $sch_notice);
}

# 전체 게시물 수
$adv_total_sql	    = "SELECT count(am_no) as cnt FROM advertising_management `am` WHERE {$add_where}";
$adv_total_query    = mysqli_query($my_db, $adv_total_sql);
$adv_total_result   = mysqli_fetch_array($adv_total_query);
$adv_total 	        = $adv_total_result['cnt'];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "100";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($adv_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "advertising_management.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $adv_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

$adv_management_sql = "
    SELECT
        *,
        DATE_FORMAT(`am`.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(`am`.regdate, '%H:%i') as reg_hour,
        DATE_FORMAT(`am`.adv_s_date, '%Y-%m-%d') as adv_s_day,
        DATE_FORMAT(`am`.adv_s_date, '%H:%i') as adv_s_time,
        DATE_FORMAT(`am`.adv_e_date, '%Y-%m-%d') as adv_e_day,
        DATE_FORMAT(`am`.adv_e_date, '%H:%i') as adv_e_time,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`am`.reg_s_no) AS reg_s_name,
        (SELECT COUNT(ae.ae_no) FROM advertising_event ae WHERE ae.am_no=`am`.am_no) AS event_cnt,
        CASE `am`.state
            WHEN '4' THEN '3'
            WHEN '5' THEN '2'
            WHEN '6' THEN '3'
            ELSE '1'
        END AS `order_state`
    FROM advertising_management `am`
    WHERE {$add_where}
    ORDER BY order_state ASC, adv_s_date ASC
";
$adv_management_query = mysqli_query($my_db, $adv_management_sql);
$adv_management_list    = [];
$adv_media_option       = getAdvertisingMediaOption();
$adv_product_option     = getAdvertisingProductOption();
$adv_date_option        = getDayShortOption();
$adv_state_color_option = getAdvertisingStateColorOption();
while($adv_management = mysqli_fetch_assoc($adv_management_query))
{
    $adv_management['state_color']      = isset($adv_state_color_option[$adv_management['state']]) ? $adv_state_color_option[$adv_management['state']] : "";
    $adv_management['media_name']       = isset($adv_media_option[$adv_management['media']]) ? $adv_media_option[$adv_management['media']] : "";
    $adv_management['adv_s_date_text']  = $adv_management['adv_s_day']." (".$adv_date_option[date("w", strtotime($adv_management['adv_s_day']))].") ".$adv_management['adv_s_time'];
    $adv_management['adv_e_date_text']  = $adv_management['adv_e_day']." (".$adv_date_option[date("w", strtotime($adv_management['adv_e_day']))].") ".$adv_management['adv_e_time'];
    $adv_management['in_application']   = false;
    $adv_management['is_editable']      = ($session_s_no == $adv_management['reg_s_no'] && $adv_management['state'] == '1') ? true : false;

    $application_list       = [];
    $adv_application_sql    = "SELECT aa_no, s_no, s_name FROM advertising_application WHERE am_no='{$adv_management['am_no']}'";
    $adv_application_query  = mysqli_query($my_db, $adv_application_sql);
    while($adv_application = mysqli_fetch_assoc($adv_application_query)){
        if($adv_application['s_no'] == $session_s_no){
            $adv_management['in_application'] = true;
        }
        $application_list[] = $adv_application;
    }

    if($adv_management['state'] == '3'){
        $adv_management['cal_price'] = $adv_management['price'] - ($adv_management['price']*($adv_management['fee_per']/100));
    }

    $adv_management['application_list'] = $application_list;
    $adv_management_list[] = $adv_management;
}

$adv_check_total_sql        = "
    SELECT 
        `am`.*,
        DATE_FORMAT(`am`.adv_s_date, '%Y-%m-%d') as adv_s_day,
        DATE_FORMAT(`am`.adv_s_date, '%H:%i') as adv_s_time,
        DATE_FORMAT(`am`.adv_e_date, '%Y-%m-%d') as adv_e_day,
        DATE_FORMAT(`am`.adv_e_date, '%H:%i') as adv_e_time, 
        (SELECT COUNT(*) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no) AS app_total_cnt,
        (SELECT COUNT(*) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no AND `aa`.s_no='{$session_s_no}') AS app_cnt
    FROM advertising_management AS `am` WHERE `am`.state IN(1,3) ORDER BY adv_s_date DESC";
$adv_check_total_query      = mysqli_query($my_db, $adv_check_total_sql);
$adv_check_total_list       = array("1" => 0, "2" => 0, "3" => 0, "4" => 0, '5' => 0, '6' => 0);
$adv_check_confirm_total    = 0;
$application_text_list      = [];
while($adv_check_total = mysqli_fetch_assoc($adv_check_total_query))
{
    if($adv_check_total['state'] == '1'){
        $adv_check_total_list["1"]++;
        if($adv_check_total['app_total_cnt'] > 0){
            $adv_check_total_list["2"]++;
            $application_text_list[$adv_check_total["product"]][] = $adv_check_total['adv_s_day']." (".$adv_date_option[date("w", strtotime($adv_check_total['adv_s_day']))].") ".$adv_check_total['adv_s_time']." ~ ".$adv_check_total['adv_e_time'];
        }else{
            $adv_check_total_list["6"]++;
        }

        if($adv_check_total['app_cnt'] > 0){
            $adv_check_total_list["4"]++;
        }
    }

    if($adv_check_total['state'] == '3'){
        $adv_check_total_list["3"]++;

        if($adv_check_total['app_cnt'] > 0){
            $adv_check_total_list["5"]++;
        }

        $cal_price = $adv_check_total['price'] - ($adv_management['price']*($adv_check_total['fee_per']/100));
        $adv_check_confirm_total += $cal_price;
    }
}

$application_text = "";
if(!empty($application_text_list))
{
    foreach($application_text_list as $pro_key => $pro_text_data)
    {
        $application_text .= !empty($application_text) ? "\r\n[{$adv_product_option[$pro_key]}]\r\n" : "[{$adv_product_option[$pro_key]}]\r\n";
        foreach ($pro_text_data as $pro_text) {
            $application_text .= "{$pro_text}\r\n";
        }
    }
}

$smarty->assign("page_type_option", getPageTypeOption('big'));
$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("hour_option", getHourOption());
$smarty->assign("min_option", getMinOption());
$smarty->assign("adv_media_option", $adv_media_option);
$smarty->assign("adv_product_option", $adv_product_option);
$smarty->assign("adv_state_option", getAdvertisingStateOption());
$smarty->assign("adv_type_option", getAdvertisingTypeOption());
$smarty->assign("adv_state_color_option", $adv_state_color_option);
$smarty->assign("adv_agency_option", getAdvertisingAgencyOption());
$smarty->assign("adv_quick_search_option", getQuickSearchOption());
$smarty->assign("application_text", $application_text);
$smarty->assign("adv_check_confirm_total", $adv_check_confirm_total);
$smarty->assign("adv_check_total_list", $adv_check_total_list);
$smarty->assign("adv_management_list", $adv_management_list);

$smarty->display('advertising_management.html');
?>
