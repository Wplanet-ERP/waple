<?php
require('inc/common.php');

if (trim($_SESSION["ss_s_no"])) {
	goto_url("work_list_cms.php");
}

if ($user_id && $pw)
{
	sleep(1);

	$getpwd         = substr(md5($pw), 8, 16);
	$login_sql      = "SELECT s_no, id, s_name, team, `position`, pos_permission, permission, staff_state, my_c_no, my_c_type, partner_c_no FROM staff WHERE id = '{$user_id}' AND `pass`='{$getpwd}' AND partner_c_no IN('2809') AND staff_state IN ('1','2') AND my_c_type='3'";
	$login_query    = mysqli_query($my_db, $login_sql);
	$staff_info     = mysqli_fetch_array($login_query);

	if($staff_info['id'])
	{
		$_SESSION["ss_my_c_no"]         = $staff_info['my_c_no'];
		$_SESSION["ss_s_no"]            = $staff_info['s_no'];
		$_SESSION["ss_id"]              = $staff_info['id'];
		$_SESSION["ss_name"]            = $staff_info['s_name'];
		$_SESSION["ss_team"]            = $staff_info['team'];
		$_SESSION["ss_position"]        = $staff_info['position'];
		$_SESSION["ss_pos_permission"]  = $staff_info['pos_permission'];
		$_SESSION["ss_permission"]      = $staff_info['permission'];
		$_SESSION["ss_staff_state"]     = $staff_info['staff_state'];
        $_SESSION["ss_my_c_type"] 	    = $staff_info['my_c_type'];

		goto_url("work_list_cms.php");
	}else{
		alert("정확한 정보를 입력하여주세요!","work_list_cms_login.php");
	}
}

$smarty->assign("nobg","style='background:none'");
$smarty->display('work_list_cms_login.html');
?>