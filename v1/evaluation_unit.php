<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/evaluation.php');
require('inc/model/Evaluation.php');

# 접근 권한
if (!(permissionNameCheck($session_permission,"재무관리자") || permissionNameCheck($session_permission,"대표") || permissionNameCheck($session_permission,"마스터관리자"))){
	$smarty->display('access_error.html');
	exit;
}

# 프로세스 처리
$process 		= (isset($_POST['process']))?$_POST['process']:"";
$ev_set_model 	= Evaluation::Factory();
$ev_set_model->setMainInit("evaluation_unit_set","ev_u_set_no");
$ev_unit_model 	= Evaluation::Factory();
$ev_unit_model->setMainInit("evaluation_unit","ev_u_no");

if ($process == "f_set_subject")
{
	$ev_u_set_no = (isset($_POST['ev_u_set_no'])) ? $_POST['ev_u_set_no'] : "";
	$value 		 = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

	if (!$ev_set_model->update(array("ev_u_set_no" => $ev_u_set_no, "subject" => $value)))
		echo "평가지명 저장에 실패 하였습니다.";
	else
		echo "평가지명이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_set_description")
{
	$ev_u_set_no = (isset($_POST['ev_u_set_no'])) ? $_POST['ev_u_set_no'] : "";
	$value 		 = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

	if (!$ev_set_model->update(array("ev_u_set_no" => $ev_u_set_no, "description" => $value)))
		echo "평가지 설명 저장에 실패 하였습니다.";
	else
		echo "평가지 설명이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_order")
{
	$ev_u_no 	= (isset($_POST['ev_u_no'])) ? $_POST['ev_u_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$ev_unit_model->update(array("ev_u_no" => $ev_u_no, "order" => $value)))
		echo "순서 저장에 실패 하였습니다.";
	else
		echo "순서가 저장 되었습니다.";
	exit;
}
elseif ($process == "f_page")
{
	$ev_u_no 	= (isset($_POST['ev_u_no'])) ? $_POST['ev_u_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$ev_unit_model->update(array("ev_u_no" => $ev_u_no, "page" => $value)))
		echo "페이지 저장에 실패 하였습니다.";
	else
		echo "페이지가 저장 되었습니다.";
	exit;
}
elseif ($process == "f_kind")
{
	$ev_u_no 	= (isset($_POST['ev_u_no'])) ? $_POST['ev_u_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$ev_unit_model->update(array("ev_u_no" => $ev_u_no, "kind" => $value)))
		echo "구분명 저장에 실패 하였습니다.";
	else{
		echo "구분명이 저장 되었습니다.";
	}
	exit;
}
elseif ($process == "f_question")
{
	$ev_u_no 	= (isset($_POST['ev_u_no'])) ? $_POST['ev_u_no'] : "";
	$value 		= (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

	if (!$ev_unit_model->update(array("ev_u_no" => $ev_u_no, "question" => $value)))
		echo "질문 저장에 실패 하였습니다.";
	else
		echo "질문이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_description")
{
	$ev_u_no 	= (isset($_POST['ev_u_no'])) ? $_POST['ev_u_no'] : "";
	$value 		= (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

	if (!$ev_unit_model->update(array("ev_u_no" => $ev_u_no, "description" => $value)))
		echo "질문 설명 저장에 실패 하였습니다.";
	else
		echo "질문 설명이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_evaluation_state")
{
	$ev_u_no 	= (isset($_POST['ev_u_no'])) ? $_POST['ev_u_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$ev_unit_model->update(array("ev_u_no" => $ev_u_no, "evaluation_state" => $value)))
		echo "평가종류 저장에 실패 하였습니다.";
	else
		echo "평가종류가 저장 되었습니다.";
	exit;
}
elseif ($process == "f_choice_items")
{
	$ev_u_no 	= (isset($_POST['ev_u_no'])) ? $_POST['ev_u_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$ev_unit_model->update(array("ev_u_no" => $ev_u_no, "choice_items" => $value)))
		echo "객관형 항목 저장에 실패 하였습니다.";
	else
		echo "객관형 항목이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_essential")
{
	$ev_u_no 	= (isset($_POST['ev_u_no'])) ? $_POST['ev_u_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$ev_unit_model->update(array("ev_u_no" => $ev_u_no, "essential" => $value)))
		echo "필수여부 저장에 실패 하였습니다.";
	else
		echo "필수여부가 저장 되었습니다.";
	exit;
}
elseif($process == "del_evaluation_unit_set")
{
	die;
	$ev_u_set_no	= (isset($_POST['ev_u_set_no']))?$_POST['ev_u_set_no']:"";
	$search_url		= isset($_POST['search_url']) ? $_POST['search_url'] : "";
	$unit_sql 		= "DELETE FROM evaluation_unit WHERE ev_u_set_no = '{$ev_u_set_no}'";

	if(mysqli_query($my_db, $unit_sql))
	{
		$unit_set_sql = "DELETE FROM evaluation_unit_set WHERE ev_u_set_no = '{$ev_u_set_no}'";

		if(mysqli_query($my_db, $unit_set_sql)) {
			exit("<script>alert('ev_u_set_no = {$ev_u_set_no} 평가지를 삭제하였습니다.');location.href='evaluation_unit.php?{$search_url}';</script>");
		}else{
			exit("<script>alert('ev_u_set_no = {$ev_u_set_no} 평가지 삭제에 실패 하였습니다.');location.href='evaluation_unit.php?{$search_url}';</script>");
		}
	}else{
		exit("<script>alert('ev_u_set_no = {$ev_u_set_no} 평가지 항목들 삭제에 실패 하였습니다.');location.href='evaluation_unit.php?{$search_url}';</script>");
	}
}
elseif($process == "del_evaluation_unit")
{
    die;
	$ev_u_no		= (isset($_POST['ev_u_no'])) ? $_POST['ev_u_no'] : "";
	$ev_u_set_no	= (isset($_POST['ev_u_set_no'])) ? $_POST['ev_u_set_no'] : "";
	$search_url		= isset($_POST['search_url']) ? $_POST['search_url'] : "";

	if($ev_unit_model->delete($ev_u_no)) {
		exit("<script>location.href='evaluation_unit.php?{$search_url}&open_ev_u_set_no={$ev_u_set_no}';</script>");
	}else{
		exit("<script>alert('ev_u_no = {$ev_u_no} 평가지 항목 삭제에 실패 하였습니다.');location.href='evaluation_unit.php?{$search_url}&open_ev_u_set_no={$ev_u_set_no}';</script>");
	}

}
elseif($process == "add_evaluation_unit_set")
{
	$search_url	= isset($_POST['search_url']) ? $_POST['search_url'] : "";
	$ins_data 	= array(
		"subject" 		=> addslashes(trim($_POST['f_set_subject_new'])),
		"description" 	=> addslashes(trim($_POST['f_set_description_new'])),
		"regdate"		=> date("Y-m-d H:i:s")
	);

	if($ev_set_model->insert($ins_data)) {
		exit("<script>alert('평가지를 제작하었습니다');location.href='evaluation_unit.php?{$search_url}';</script>");
	} else {
		exit("<script>alert('평가지 제작에 실패 하였습니다');location.href='evaluation_unit.php?{$search_url}';</script>");
	}
}
elseif($process == "add_evaluation_unit")
{
	$search_url		= isset($_POST['search_url']) ? $_POST['search_url'] : "";
	$ev_u_set_no	= isset($_POST['ev_u_set_no']) ? $_POST['ev_u_set_no'] : "";
	$ins_data 		= array(
		"ev_u_set_no"		=> $_POST['ev_u_set_no'],
		"order"				=> $_POST['f_order_new'],
		"page"				=> $_POST['f_page_new'],
		"kind"				=> $_POST['f_kind_new'],
		"question" 			=> addslashes(trim($_POST['f_question_new'])),
		"description" 		=> addslashes(trim($_POST['f_description_new'])),
		"evaluation_state"	=> $_POST['f_evaluation_state_new'],
	);

	if($ev_unit_model->insert($ins_data)) {
		exit("<script>location.href='evaluation_unit.php?{$search_url}&open_ev_u_set_no={$ev_u_set_no}';</script>");
	} else {
		exit("<script>alert('평가지 항목 추가에 실패 하였습니다');location.href='evaluation_unit.php?{$search_url}&open_ev_u_set_no={$ev_u_set_no}';</script>");
	}
}
elseif($process == "set_display_on")
{
	$ev_u_set_no = isset($_POST['ev_u_set_no']) ? $_POST['ev_u_set_no'] : "";
	$search_url	 = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if($ev_set_model->update(array("ev_u_set_no" => $ev_u_set_no, "active" => 1)))
    {
        $unit_set_sql = "UPDATE evaluation_unit SET active='1' WHERE ev_u_set_no = '{$ev_u_set_no}'";
        if(mysqli_query($my_db, $unit_set_sql)) {
            exit("<script>alert('ev_u_set_no = {$ev_u_set_no} 평가지를 공개 처리했습니다.');location.href='evaluation_unit.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('ev_u_set_no = {$ev_u_set_no} 평가지 공개 처리에 실패 하였습니다.');location.href='evaluation_unit.php?{$search_url}';</script>");
        }
    } else {
        exit("<script>alert('ev_u_set_no = {$ev_u_set_no} 평가지 공개 처리에 실패 하였습니다.');location.href='evaluation_unit.php?{$search_url}';</script>");
    }
}
elseif($process == "set_display_off")
{
	$ev_u_set_no = isset($_POST['ev_u_set_no']) ? $_POST['ev_u_set_no'] : "";
	$search_url	 = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if($ev_set_model->update(array("ev_u_set_no" => $ev_u_set_no, "active" => 2)))
    {
        $unit_set_sql = "UPDATE evaluation_unit SET active='2' WHERE ev_u_set_no = '{$ev_u_set_no}'";
        if(mysqli_query($my_db, $unit_set_sql)) {
            exit("<script>alert('ev_u_set_no = {$ev_u_set_no} 평가지를 비공개 처리했습니다.');location.href='evaluation_unit.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('ev_u_set_no = {$ev_u_set_no} 평가지 비공개 처리에 실패 하였습니다.');location.href='evaluation_unit.php?{$search_url}';</script>");
        }
    } else {
        exit("<script>alert('ev_u_set_no = {$ev_u_set_no} 평가지 비공개 처리에 실패 하였습니다.');location.href='evaluation_unit.php?{$search_url}';</script>");
    }

}
elseif($process == "unit_display_on")
{
    $ev_u_no	 = isset($_POST['ev_u_no']) ? $_POST['ev_u_no'] : "";
    $ev_u_set_no = isset($_POST['ev_u_set_no']) ? $_POST['ev_u_set_no'] : "";
    $search_url	 = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if($ev_unit_model->update(array("ev_u_no" => $ev_u_no, "active" => 1)))
        exit("<script>alert('공개 처리했습니다');location.href='evaluation_unit.php?{$search_url}';</script>");
    else
        exit("<script>alert('공개 처리에 실패했습니다');location.href='evaluation_unit.php?{$search_url}';</script>");
}
elseif($process == "unit_display_off")
{
	$ev_u_no	 = isset($_POST['ev_u_no']) ? $_POST['ev_u_no'] : "";
	$ev_u_set_no = isset($_POST['ev_u_set_no']) ? $_POST['ev_u_set_no'] : "";
	$search_url	 = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if($ev_unit_model->update(array("ev_u_no" => $ev_u_no, "active" => 2)))
        exit("<script>alert('비공개 처리했습니다');location.href='evaluation_unit.php?{$search_url}';</script>");
    else
        exit("<script>alert('비공개 처리에 실패했습니다');location.href='evaluation_unit.php?{$search_url}';</script>");
}
else
{

	# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where			="1=1";
	$sch_subject		= isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
	$sch_description	= isset($_GET['sch_description']) ? $_GET['sch_description'] : "";
	$open_ev_u_set_no	= isset($_GET['open_ev_u_set_no']) ? $_GET['open_ev_u_set_no'] : "";

	if(!empty($sch_subject)) {
		$add_where .= " AND es_u_set.subject like '%{$sch_subject}%'";
		$smarty->assign("sch_subject", $sch_subject);
	}

	if(!empty($sch_description)) {
		$add_where .= " AND es_u_set.description like '%{$sch_description}%'";
		$smarty->assign("sch_description", $sch_description);
	}

	if(!empty($open_ev_u_set_no)) {
		$smarty->assign("open_ev_u_set_no", $open_ev_u_set_no);
	}

	# 전체 게시물 수 & 페이징 처리
	$evaluation_unit_set_sql 	= "SELECT count(ev_u_set_no) as cnt FROM evaluation_unit_set es_u_set WHERE {$add_where}";
	$evaluation_unit_set_query	= mysqli_query($my_db, $evaluation_unit_set_sql);
	$evaluation_unit_set_result	= mysqli_fetch_assoc($evaluation_unit_set_query);
	$evaluation_unit_set_total 	= $evaluation_unit_set_result['cnt'];

	$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num 		= 10;
	$offset 	= ($pages-1) * $num;
	$pagenum 	= ceil($evaluation_unit_set_total/$num);

	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	$search_url = "sch_subject={$sch_subject}&sch_description={$sch_description}";
	$pagelist 	= pagelist($pages, "evaluation_unit.php", $pagenum, $search_url);

	$smarty->assign("search_url", $search_url);
	$smarty->assign("total_num", $evaluation_unit_set_total);
	$smarty->assign("pagelist", $pagelist);

	# 리스트 쿼리
	$evaluation_unit_set_sql = "
		SELECT
			es_u_set.ev_u_set_no,
			es_u_set.regdate,
			es_u_set.subject,
			(SELECT COUNT(*) FROM evaluation_unit es_u WHERE es_u.ev_u_set_no=es_u_set.ev_u_set_no) AS unit_count,
			es_u_set.description,
			es_u_set.active
		FROM evaluation_unit_set es_u_set
		WHERE {$add_where}
		LIMIT {$offset}, {$num}
	";
	$evaluation_unit_set_query 	= mysqli_query($my_db, $evaluation_unit_set_sql);
	$evaluation_unit_set_list 	= [];
	$evaluation_unit_list 		= [];
	while($evaluation_unit_set = mysqli_fetch_array($evaluation_unit_set_query)){

		if(!empty($evaluation_unit_set['regdate'])) {
			$regdate_day_value 	= date("Y/m/d", strtotime($evaluation_unit_set['regdate']));
			$regdate_time_value = date("H:i", strtotime($evaluation_unit_set['regdate']));
		}else{
			$regdate_day_value="";
			$regdate_time_value="";
		}

		# 평가지 항목 리스트 쿼리
		$evaluation_unit_sql 	= "SELECT * FROM evaluation_unit es_u WHERE es_u.ev_u_set_no='{$evaluation_unit_set['ev_u_set_no']}' ORDER BY `order` ASC";
		$evaluation_unit_query 	= mysqli_query($my_db, $evaluation_unit_sql);
		while($evaluation_unit = mysqli_fetch_array($evaluation_unit_query)){
			$evaluation_unit_list[$evaluation_unit['ev_u_set_no']][] = array(
				"ev_u_no"			=> $evaluation_unit['ev_u_no'],
				"ev_u_set_no"		=> $evaluation_unit['ev_u_set_no'],
				"order"				=> $evaluation_unit['order'],
				"page"				=> $evaluation_unit['page'],
				"kind"				=> $evaluation_unit['kind'],
				"question"			=> $evaluation_unit['question'],
       			"description"		=> $evaluation_unit['description'],
        		"evaluation_state" 	=> $evaluation_unit['evaluation_state'],
				"choice_items"		=> $evaluation_unit['choice_items'],
		        "essential"			=> $evaluation_unit['essential'],
				"display"			=> $evaluation_unit['active']
			);
		}

		$evaluation_unit_set_list[$evaluation_unit_set['ev_u_set_no']] = array(
			"ev_u_set_no"	=> $evaluation_unit_set['ev_u_set_no'],
			"regdate"		=> $evaluation_unit_set['regdate'],
			"regdate_day"	=> $regdate_day_value,
			"regdate_time"	=> $regdate_time_value,
			"subject"		=> $evaluation_unit_set['subject'],
			"unit_count"	=> $evaluation_unit_set['unit_count'],
			"description"	=> $evaluation_unit_set['description'],
			"display"		=> $evaluation_unit_set['active']
		);
	}

	$smarty->assign("evaluation_unit_type_option", getEvUnitTypeOption());
	$smarty->assign("evaluation_unit_set_list", $evaluation_unit_set_list);
	$smarty->assign("evaluation_unit_list", $evaluation_unit_list);

	$smarty->display('evaluation_unit.html');
}

?>
