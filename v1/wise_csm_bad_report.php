<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Staff.php');

# 프로세스 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'modify_qty')
{
    $type       = (isset($_POST['type'])) ? $_POST['type'] : "";
    $s_no       = (isset($_POST['s_no'])) ? $_POST['s_no'] : "";
    $csm_g2     = (isset($_POST['csm_g2'])) ? $_POST['csm_g2'] : "";
    $bad_date   = (isset($_POST['date'])) ? $_POST['date'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $chk_sql = "SELECT count(cbr_no) as cnt FROM csm_bad_report WHERE bad_date='{$bad_date}' AND s_no='{$s_no}' AND k_name_code='{$csm_g2}'";
    $chk_query = mysqli_query($my_db, $chk_sql);
    $chk_result = mysqli_fetch_assoc($chk_query);

    if($chk_result['cnt'] > 0){
        $sql = "UPDATE csm_bad_report SET qty='{$value}' WHERE bad_date='{$bad_date}' AND s_no='{$s_no}' AND k_name_code='{$csm_g2}'";
    }else{
        $sql = "INSERT INTO csm_bad_report SET bad_date='{$bad_date}', s_no='{$s_no}', k_name_code='{$csm_g2}', qty='{$value}'";
    }

    if (!mysqli_query($my_db, $sql))
        echo "실패했습니다";
    else
        echo "저장했습니다";
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "30";
$nav_title   = "불량사유 관리";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

/** 검색조건 START */
$add_where          = "1=1";
$add_return_where   = "wcr.bad_reason != '' AND wcr.return_state NOT IN('7','8')";
$add_column_where   = "";

# 그룹별 검색
$sch_csm_g1     = isset($_GET['sch_csm_g1']) ? $_GET['sch_csm_g1'] : "";
$sch_csm_g2     = isset($_GET['sch_csm_g2']) ? $_GET['sch_csm_g2'] : "";

$smarty->assign("sch_csm_g1", $sch_csm_g1);
$smarty->assign("sch_csm_g2", $sch_csm_g2);

if($sch_csm_g2){
    $add_where          .= " AND cbr.k_name_code ='{$sch_csm_g2}'";
    $add_return_where   .= " AND wcr.bad_reason ='{$sch_csm_g2}'";
    $add_column_where    = " AND k_name_code ='{$sch_csm_g2}'";
}elseif($sch_csm_g1){
    $add_where          .= " AND (SELECT k_parent FROM kind k WHERE k.k_name_code=`cbr`.k_name_code) ='{$sch_csm_g1}'";
    $add_return_where   .= " AND (SELECT k_parent FROM kind k WHERE k.k_name_code=`wcr`.bad_reason) ='{$sch_csm_g1}'";
    $add_column_where    = " AND k_parent ='{$sch_csm_g1}'";
}

$kind_model             = Kind::Factory();
$csm_group_total_list   = $kind_model->getKindGroupList("bad_reason", 0);
$csm_g1_list = $csm_g2_list = [];
foreach($csm_group_total_list as $key => $asset_data)
{
    if(!$key){
        $csm_g1_list = $asset_data;
    }else{
        $csm_g2_list[$key] = $asset_data;
    }
}
$sch_csm_g2_list = isset($csm_g2_list[$sch_csm_g1]) ? $csm_g2_list[$sch_csm_g1] : [];

$smarty->assign("csm_g1_list", $csm_g1_list);
$smarty->assign("csm_g2_list", $sch_csm_g2_list);

# 브랜드 검색
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_return_where  .= " AND `wcr`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_return_where  .= " AND `wcr`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

    $add_return_where  .= " AND `wcr`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 날짜별 검색
$today_s_w		 = date('w')-1;
$sch_date_type   = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$sch_date        = isset($_GET['sch_date']) ? $_GET['sch_date'] : date('Y-m');
$sch_s_month     = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m');
$sch_e_month     = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week      = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week      = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date      = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-8 day"));
$sch_e_date      = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_date', $sch_date);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);

# 브랜드 검색 설정 및 수정여부
$sch_s_no   = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$sch_toggle = isset($_GET['sch_toggle']) ? $_GET['sch_toggle'] : "";

if(!empty($sch_s_no))
{
    $add_where          .= " AND `cbr`.s_no = '{$sch_s_no}'";
    $add_return_where   .= " AND `wcr`.req_s_no = '{$sch_s_no}'";
    $smarty->assign('sch_s_no', $sch_s_no);
}
$smarty->assign("sch_toggle", $sch_toggle);

//전체 기간 조회 및 누적데이터 조회
$all_date_where   = "";
$all_date_key	  = "";
$add_date_where   = "";
$add_date_column  = "";

if($sch_date_type == '3') //월간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where   = " AND DATE_FORMAT(return_req_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_date_column  = "DATE_FORMAT(return_req_date, '%Y%m')";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	    = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title     = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where     = " AND DATE_FORMAT(return_req_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_date_column    = "DATE_FORMAT(DATE_SUB(return_req_date, INTERVAL(IF(DAYOFWEEK(return_req_date)=1,8,DAYOFWEEK(return_req_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_date_where   = " AND DATE_FORMAT(return_req_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_date_column  = "DATE_FORMAT(return_req_date, '%Y%m%d')";
}

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";

//기본 Report 리스트 Init 및 x key 및 타이틀 설정
$y_column_list = [];
$y_column_sql = "SELECT k_parent, (SELECT sub_k.k_name FROM kind sub_k WHERE sub_k.k_name_code=k.k_parent) as k_parent_name, k_code, k_name_code, k_name FROM kind k WHERE k_code IN('bad_reason') AND k_parent is not null {$add_column_where} ORDER BY k_parent ASC, priority ASC";
$y_column_query = mysqli_query($my_db, $y_column_sql);

while($y_column_data = mysqli_fetch_assoc($y_column_query))
{
    $y_column_list[$y_column_data['k_parent']][$y_column_data['k_name_code']] = array(
        'k_name'        => $y_column_data['k_name'],
        'k_name_code'   => $y_column_data['k_name_code'],
        'type'          => $y_column_data['k_code'],
        'k_parent'      => $y_column_data['k_parent'],
        'k_parent_name' => $y_column_data['k_parent_name'],
        'date'          => "",
        'qty'           => 0
    );
}

$csm_bad_date_list    = [];
$csm_bad_report_list  = [];
$memo_report_list     = [];
$csm_bad_report_tmp_total_list = [];

if($all_date_where != '')
{
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        foreach($y_column_list as $csm_g1 => $y_columns)
        {
            foreach($y_columns as $csm_g2 => $y_column)
            {
                $csm_bad_report_list[$csm_g1][$csm_g2][$date['chart_key']] = $y_column;
                $csm_bad_report_list[$csm_g1][$csm_g2][$date['chart_key']]['date'] = $date['chart_key'];
                $memo_report_list[$date['date_key']][$csm_g2] = "";
            }
        }

        if($sch_date_type == '3'){
            $chart_s_date_val   = $date['chart_key']."01";
            $chart_s_date       = date("Y-m-d", strtotime("{$chart_s_date_val}"));
            $chart_e_day        = DATE('t', strtotime($chart_s_date));
            $chart_e_date_val   = $date['chart_key'].$chart_e_day;
            $chart_e_date       = date("Y-m-d", strtotime("{$chart_e_date_val}"));
        }elseif($sch_date_type == '2'){
            $chart_e_date = date("Y-m-d", strtotime("{$date['date_key']}"));
            $chart_s_date = date("Y-m-d", strtotime("{$chart_e_date} -6 days"));
        }else{
            $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
            $chart_e_date = date("Y-m-d", strtotime("{$date['date_key']}"));
        }

        $csm_bad_date_list[$date['chart_key']] = array("title" => $chart_title, "s_date" => $chart_s_date, "e_date" => $chart_e_date);
        $csm_bad_report_tmp_total_list[$date['date_key']] = 0;
    }
}
$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

# 파일 체크
$bad_file_chk_list = [];
if(!empty($csm_bad_date_list))
{
    foreach($csm_bad_date_list as $chart_key => $date_data)
    {
        $chk_file_sql = "
            SELECT 
                DISTINCT r.bad_reason
            FROM work_cms_comment w
            LEFT JOIN (SELECT parent_order_number, bad_reason FROM `work_cms_return` as r WHERE r.return_req_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}' AND r.bad_reason IS NOT NULL AND r.bad_reason != '') AS r ON r.parent_order_number=w.ord_no
            WHERE ((file1_origin IS NOT NULL AND file1_read != '') OR (file2_origin IS NOT NULL AND file2_read != '') OR (file3_origin IS NOT NULL AND file3_read != ''))
            AND r.bad_reason IS NOT NULL AND r.bad_reason != ''
        ";
        $chk_file_query = mysqli_query($my_db, $chk_file_sql);
        while($chk_file = mysqli_fetch_assoc($chk_file_query))
        {
            $bad_file_chk_list[$chart_key][$chk_file['bad_reason']] = 1;
        }
    }
}

# 회수리스트 값 구하기
$return_sql = "
    SELECT
        1 as qty,
        wcr.bad_reason as csm_g2,
        DATE_FORMAT(return_req_date, '%Y%m%d') as bad_date,        
        {$add_date_column} as key_date,
        (SELECT k.k_code FROM kind k WHERE k.k_name_code=`wcr`.bad_reason) as csm_type,
        (SELECT k.k_name_code FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`wcr`.bad_reason)) as csm_g1,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`wcr`.bad_reason)) as csm_g1_name,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code=`wcr`.bad_reason) as csm_g2_name
    FROM work_cms_return wcr
    WHERE {$add_return_where}
    {$add_date_where}
    GROUP BY order_number
    ORDER BY bad_date ASC
";
$return_query = mysqli_query($my_db, $return_sql);
while($return = mysqli_fetch_assoc($return_query))
{
    $csm_g1 = str_pad($return['csm_g1'], 5, "0", STR_PAD_LEFT);
    $csm_bad_report_list[$csm_g1][$return['csm_g2']][$return['key_date']]['qty'] += $return['qty'];
    $csm_bad_report_tmp_total_list[$return['bad_date']] += $return['qty'];
}

# 지출 Total 값 구하기
$csm_bad_report_total_list = [];
$csm_bad_report_total_sum_list = [];
if(!empty($csm_bad_report_list))
{
    foreach($csm_bad_report_list as $csm_g1 => $csm_parent_report)
    {
        foreach($csm_parent_report as $csm_g2 => $csm_report)
        {
            foreach($csm_report as $key_date => $csm_data){
                if(!isset($csm_bad_report_total_list[$csm_g1])){
                    $csm_bad_report_total_list[$csm_g1][$key_date] = 0;
                }
                $csm_bad_report_total_list[$csm_g1][$key_date] += $csm_data['qty'];

                if(!isset($csm_bad_report_total_sum_list[$key_date])){
                    $csm_bad_report_total_sum_list[$key_date] = 0;
                }
                $csm_bad_report_total_sum_list[$key_date] += $csm_data['qty'];
            }

        }
    }
}

$staff_model            = Staff::Factory();
$cs_staff_list          = $staff_model->getActiveTeamStaff("00244");
$kind_total_name_list   = $kind_model->getKindDisplayNameList("bad_reason");
$csm_g1_title_list      = $kind_total_name_list['k_parent_list'];
$csm_g2_title_list      = $kind_total_name_list['k_child_list'];

$smarty->assign('csm_bad_date_list', $csm_bad_date_list);
$smarty->assign('csm_bad_date_count', count($csm_bad_date_list));
$smarty->assign('csm_bad_report_list', $csm_bad_report_list);
$smarty->assign('csm_bad_report_total_list', $csm_bad_report_total_list);
$smarty->assign('csm_bad_report_total_sum_list', $csm_bad_report_total_sum_list);
$smarty->assign('memo_report_list', $memo_report_list);
$smarty->assign('csm_g1_title_list', $csm_g1_title_list);
$smarty->assign('csm_g2_title_list', $csm_g2_title_list);
$smarty->assign("bad_file_chk_list", $bad_file_chk_list);
$smarty->assign("cs_staff_list", $cs_staff_list);

$smarty->display('wise_csm_bad_report.html');
?>
