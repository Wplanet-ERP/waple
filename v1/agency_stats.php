<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/agency.php');
require('inc/helper/work.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');
require('inc/model/MyCompany.php');
require('inc/model/Agency.php');

# 기본 변수 값 설정
$prev_settle_month  = date('Y-m', strtotime("-1 months"));

# 검색조건
$add_where          = "1=1";
$sch_settle_month   = isset($_GET['sch_settle_month']) ? $_GET['sch_settle_month'] : $prev_settle_month;
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_status         = isset($_GET['sch_status']) ? $_GET['sch_status'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_media          = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_is_partner     = isset($_GET['sch_is_partner']) ? $_GET['sch_is_partner'] : "";
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_partner_name   = isset($_GET['sch_partner_name']) ? $_GET['sch_partner_name'] : "";
$sch_manager_team   = isset($_GET['sch_manager_team']) ? $_GET['sch_manager_team'] : $session_team;
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_run_name       = isset($_GET['sch_run_name']) ? $_GET['sch_run_name'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ori_ord_type       = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "";

#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
$url_check_str  = $_SERVER['QUERY_STRING'];
$url_chk_result = false;
if(empty($url_check_str)){
    $url_check_team_where   = getTeamWhere($my_db, $session_team);
    $url_check_sql    		=  "SELECT count(`as_no`) as cnt FROM agency_settlement `as` WHERE `as`.settle_month = '{$sch_settle_month}' AND manager_team IN({$url_check_team_where})";
    $url_check_query  		= mysqli_query($my_db, $url_check_sql);
    $url_check_result 		= mysqli_fetch_assoc($url_check_query);

    if($url_check_result['cnt'] == 0){
        $sch_manager_team   = "all";
        $url_chk_result     = true;
    }
}

if(!empty($sch_settle_month))
{
    $add_where .= " AND `as`.settle_month = '{$sch_settle_month}'";
    $smarty->assign('sch_settle_month', $sch_settle_month);
}

if(!empty($sch_no))
{
    $add_where .= " AND `as`.as_no = '{$sch_no}'";
    $smarty->assign('sch_no', $sch_no);
}

if(!empty($sch_status)){
    $add_where   .= " AND `as`.status = '{$sch_status}'";
    $smarty->assign('sch_status', $sch_status);
}

if(!empty($sch_agency)){
    $add_where   .= " AND `as`.agency = '{$sch_agency}'";
    $smarty->assign('sch_agency', $sch_agency);
}

if(!empty($sch_media)){
    $add_where   .= " AND `as`.media = '{$sch_media}'";
    $smarty->assign('sch_media', $sch_media);
}

if(!empty($sch_is_partner))
{
    if($sch_is_partner == '1'){
        $add_where   .= " AND `as`.partner > 0";
    }elseif($sch_is_partner == '2'){
        $add_where   .= " AND (`as`.partner is null OR `as`.partner < 1)";
    }
    $smarty->assign('sch_is_partner', $sch_is_partner);
}

if(!empty($sch_my_c_no)){
    $add_where   .= " AND `as`.my_c_no = '{$sch_my_c_no}'";
    $smarty->assign('sch_my_c_no', $sch_my_c_no);
}

if(!empty($sch_partner_name)){
    $add_where   .= " AND `as`.partner_name LIKE '%{$sch_partner_name}%'";
    $smarty->assign('sch_partner_name', $sch_partner_name);
}

$team_model         = Team::Factory();
$team_all_list      = $team_model->getTeamAllList();
$staff_team_list    = $team_all_list['staff_list'];
$team_full_name_list= $team_model->getTeamFullNameList();

$sch_staff_list      = [];
if (!empty($sch_manager_team))
{
    if ($sch_manager_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_manager_team);
        $add_where 		 .= " AND `as`.manager_team IN({$sch_team_code_where})";
        $sch_staff_list   = $staff_team_list[$sch_manager_team];
    }
    $smarty->assign("sch_manager_team", $sch_manager_team);
}

if (!empty($sch_manager))
{
    if ($sch_manager != "all") {
        $add_where 		 .= " AND `as`.manager='{$sch_manager}'";
    }
    $smarty->assign("sch_manager", $sch_manager);
}

if(!empty($sch_run_name)){
    $add_where   .= " AND `as`.run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_run_name}%')";
    $smarty->assign('sch_run_name', $sch_run_name);
}

if(empty($url_check_str) && $url_chk_result){
    $search_url = "sch_manager_team=all";
}else{
    $search_url = getenv("QUERY_STRING");
}
$smarty->assign("search_url", $search_url);

$agency_stats_sql = "
    SELECT
        `as`.agency,
        `as`.media,
        `c`.c_name as agency_name,
        `c`.adv_fee as agency_fee,
        `as`.partner,
        `as`.partner_name,
        `as`.dp_price,
        `as`.wd_price
    FROM agency_settlement `as`
    LEFT JOIN company c ON c.c_no=`as`.agency
    WHERE {$add_where} AND c.adv_fee > 0
    ORDER BY adv_fee ASC, c_no ASC
";
$agency_stats_query = mysqli_query($my_db, $agency_stats_sql);
$agency_stats_list  = [];
$agency_cnt_list    = [];
$partner_name_list  = [];
while($agency_stats = mysqli_fetch_assoc($agency_stats_query))
{
    if(!isset($partner_name_list[$agency_stats['partner']])){
        $partner_name_list[$agency_stats['partner']] = $agency_stats['partner_name'];
    }

    if(!isset($agency_cnt_list[$agency_stats['agency']][$agency_stats['media']])){
        $agency_cnt_list[$agency_stats['agency']]['media_cnt']++;
    }

    if(!isset($agency_stats_list[$agency_stats['agency']][$agency_stats['media']][$agency_stats['partner']]))
    {
        $agency_stats_list[$agency_stats['agency']][$agency_stats['media']][$agency_stats['partner']] = array(
            'wd_price'          => 0,
            'dp_price'          => 0,
            'total_price'       => 0,
            'total_price_vat'   => 0,
        );

        $agency_cnt_list[$agency_stats['agency']][$agency_stats['media']]++;
        $agency_cnt_list[$agency_stats['agency']]['total']++;
    }

    $total_price     = 0;
    $total_price_vat = 0;

    if($agency_stats['agency_fee'] == '1'){
        $total_price = $agency_stats['wd_price'];
    }else{
        $total_price = $agency_stats['wd_price']-$agency_stats['dp_price'];
    }

    if($total_price != 0){
        $total_price_vat = $total_price + ($total_price * 0.1);
    }

    $agency_stats_list[$agency_stats['agency']][$agency_stats['media']][$agency_stats['partner']]['wd_price']        += $agency_stats['wd_price'];
    $agency_stats_list[$agency_stats['agency']][$agency_stats['media']][$agency_stats['partner']]['dp_price']        += $agency_stats['dp_price'];
    $agency_stats_list[$agency_stats['agency']][$agency_stats['media']][$agency_stats['partner']]['total_price']     += $total_price;
    $agency_stats_list[$agency_stats['agency']][$agency_stats['media']][$agency_stats['partner']]['total_price_vat'] += $total_price_vat;
}

$my_company_model       = MyCompany::Factory();
$my_company_list        = $my_company_model->getList();
$my_company_name_list   = $my_company_model->getNameList();

$smarty->assign("my_company_name_list", $my_company_name_list);
$smarty->assign("status_option", getWorkStateOption());
$smarty->assign("status_color_option", getWorkStateOptionColor());
$smarty->assign("is_partner_option", getSettleIsParteOption());
$smarty->assign("agency_option", getSettleAgencyOption());
$smarty->assign("media_option", getAdvertiseMediaOption());
$smarty->assign("advertise_type_option", getAdvertiseTypeOption());
$smarty->assign('page_type_list', getPageTypeOption(4));
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("my_company_list", $my_company_list);
$smarty->assign("prev_settle_month", $prev_settle_month);
$smarty->assign("partner_name_list", $partner_name_list);
$smarty->assign("agency_cnt_list", $agency_cnt_list);
$smarty->assign("agency_stats_list", $agency_stats_list);

$smarty->display('agency_stats.html');
?>
