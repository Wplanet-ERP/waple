<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');

$com_no  = isset($_GET['com_no']) ? $_GET['com_no'] : "";
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'add_file') # 생성
{
    $com_no_val   = isset($_POST['com_no']) ? $_POST['com_no'] : "";

    # 발주번호 확인
    if(empty($com_no_val)){
        $smarty->display('access_company_error.html');
        exit;
    }

    # Dropbox 이미지 확인 및 저장
    $file_path        = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_name        = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk         = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    $file_origin = "";
    $file_read   = "";
    if(!empty($file_path) && !empty($file_name))
    {
        $file_reads  = move_store_files($file_path, "dropzone_tmp", "commerce_order");
        $file_origin = implode(',', $file_name);
        $file_read   = implode(',', $file_reads);

        if(count($file_reads) != count($file_name))
        {
            exit("<script>alert('파일 업로드에 실패했습니다');location.href='commerce_order.file_upload.php?com_no={$com_no_val}';</script>");
        }
    }

    # 파일 Path 및 이름 저장
    $new_sql = "UPDATE `commerce_order_set` SET file_name='{$file_origin}', file_path='{$file_read}' WHERE `no` = '{$com_no_val}'";

    if(mysqli_query($my_db, $new_sql)) {
        exit("<script>alert('파일 업로드에 성공했습니다');opener.document.location.reload();self.close();</script>");
    }else{
        exit("<script>alert('파일 업로드에 실패했습니다');location.href='commerce_order.file_upload.php?com_no={$com_no_val}';</script>");
    }
}
elseif($process == 'mod_file')
{
    $com_no_val   = isset($_POST['com_no']) ? $_POST['com_no'] : "";

    # 발주번호 확인
    if(empty($com_no_val)){
        $smarty->display('access_company_error.html');
        exit;
    }

    # Dropbox 이미지 확인 및 저장
    $file_origin_read = isset($_POST['file_origin_read']) ? $_POST['file_origin_read'] : "";
    $file_origin_name = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_path        = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_name        = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk         = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";


    $file_origin = "";
    $file_read   = "";
    if($file_chk)
    {
        if(!empty($file_path) && !empty($file_name))
        {
            $file_reads  = move_store_files($file_path, "dropzone_tmp", "commerce_order");
            $file_origin = implode(',', $file_name);
            $file_read   = implode(',', $file_reads);

            if(count($file_reads) != count($file_name))
            {
                exit("<script>alert('파일 업로드에 실패했습니다');location.href='commerce_order.file_upload.php?com_no={$com_no_val}';</script>");
            }
        }

        if(!empty($file_origin_read) && !empty($file_origin_name))
        {
            if(!empty($file_path) && !empty($file_name))
            {
                $del_images = array_diff(explode(',', $file_origin_read), $file_path);
            }else{
                $del_images = explode(',', $file_origin_read);
            }

            if(!empty($del_images))
            {
                del_files($del_images);
            }
        }
    }

    # 파일 Path 및 이름 저장
    $upd_sql = "UPDATE `commerce_order_set` SET file_name='{$file_origin}', file_path='{$file_read}' WHERE `no`='{$com_no_val}'";

    if(mysqli_query($my_db, $upd_sql)) {
        exit("<script>alert('파일 업로드에 성공했습니다');opener.document.location.reload();self.close();</script>");
    }else{
        exit("<script>alert('파일 업로드에 실패했습니다');location.href='commerce_order.file_upload.php?com_no={$com_no_val}';</script>");
    }

}

// 업무번호 및 타입 확인
if(empty($com_no)){
    $smarty->display('access_company_error.html');
    exit;
}

$comm_sql 	 = "SELECT file_name, file_path FROM `commerce_order_set` WHERE `no` = {$com_no} LIMIT 1";
$comm_query  = mysqli_query($my_db, $comm_sql);
$comm 		 = mysqli_fetch_assoc($comm_query);

$file_name   = $comm['file_name'];
$file_path   = $comm['file_path'];

$file_origin_read = "";
$file_origin_name = "";
if(!empty($file_name) && !empty($file_path))
{
    $file_names = explode(',', $file_name);
    $file_paths = explode(',', $file_path);

    $file_origin_read = $file_path;
    $file_origin_name = $file_name;
}else{
    $file_names = [];
    $file_paths = [];
}

$total_count  = empty($file_paths) ? 0 : count($file_paths);
$process_type = $total_count > 0 ? "mod_file" : "add_file";

$smarty->assign("file_count", $total_count);
$smarty->assign("com_no", $com_no);
$smarty->assign("file_names", $file_names);
$smarty->assign("file_paths", $file_paths);
$smarty->assign("file_origin_read", $file_origin_read);
$smarty->assign("file_origin_name", $file_origin_name);
$smarty->assign("process_type", $process_type);

$smarty->display('commerce_order.file_upload.html');
?>
