<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/model/MyQuick.php');


$sch_my_c_list = array(
    "1" => "와이즈플래닛컴퍼니+와이즈미디어커머스",
    "2" => "와이즈플래닛",
    "3" => "와이즈미디어커머스"
);
$smarty->assign("sch_my_c_list", $sch_my_c_list);

# Navigation & My Quick
$nav_prd_no  = "80";
$nav_title   = "조직도";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색 초기화 및 조건 생성
$add_where = "1=1 AND t.display='1'";

# 자산관리 상품 처리
$sch_my_c_no    = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "1";

$sch_my_c_name = "와이즈플래닛+와이즈미디어커머스";

if(!empty($sch_my_c_no))
{
    if($sch_my_c_no == '2'){
        $add_where .= " AND `t`.my_c_no = '1'";
        $sch_my_c_name = "와이즈플래닛";
    }elseif($sch_my_c_no == '3'){
        $add_where .= " AND `t`.my_c_no = '3'";
        $sch_my_c_name = "와이즈미디어커머스";
    }else{
        $add_where .= " AND `t`.my_c_no IN('1','3')";
    }

    $smarty->assign('sch_my_c_no', $sch_my_c_no);
}

$org_team_list_sql   = "SELECT team_name, team_code, team_code_parent, priority, my_c_no, team_leader FROM team t WHERE {$add_where} ORDER BY my_c_no, team_code_parent, priority";
$org_team_list_query = mysqli_query($my_db, $org_team_list_sql);
$org_team_list[] = array(
    "label"     => $sch_my_c_name,
    "itemId"    => "root",
    "parentId"  => "999",
    "order"     => "1"
);

while($org_team = mysqli_fetch_array($org_team_list_query))
{
    $staff_sql   = "SELECT * FROM staff WHERE team_list like '%{$org_team['team_code']}%' AND staff_state='1' ORDER BY s_no ASC";
    $staff_query = mysqli_query($my_db, $staff_sql);
    $staff_leader = [];
    $staff_list   = [];

    while($staff_result = mysqli_fetch_assoc($staff_query))
    {
        if($staff_result['s_no'] == $org_team['team_leader'])
        {
            $staff_leader[] = $staff_result['s_name'];
        }else{
            $staff_list[]   = $staff_result['s_name'];
        }
    }

    if(!empty($org_team['team_code_parent'])){
        $org_team_list[] = array(
            "label"     => $org_team['team_name'],
            "itemId"    => $org_team['team_code'],
            "parentId"  => $org_team['team_code_parent'],
            "order"     => $org_team['priority'],
            "leader"    => implode(" ", $staff_leader),
            "staff"     => implode(" ", $staff_list),
        );
    }else{
        $org_team_list[] = array(
            "label"     => $org_team['team_name'],
            "itemId"    => $org_team['team_code'],
            "parentId"  => "root",
            "order"     => $org_team['priority'],
            "leader"    => implode(" ", $staff_leader),
            "staff"     => implode(" ", $staff_list),
        );
    }
}

$smarty->assign('org_team_list', json_encode($org_team_list));

$smarty->display('org_chart.html');
?>
