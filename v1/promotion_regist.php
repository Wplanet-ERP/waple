<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');

$kind_color="#f77b18"; // 체험단 : #f77b18 / 기자단 : #5175fe / 배송체험단 : #63F

// 요일배열
$datey=array('일','월','화','수','목','금','토');


// 관리자일 경우 실행
if ($us_id =='admin' OR $us_id == 'wplanet' OR $us_id == 'tblog' OR permissionCheck($session_permission, "1101111"))
{

	// GET : main.php?no= 로 넘어온 프로모션 고유번호(p_no)를 체크하고 있으면 idx에 저장
	$idx = (isset($_GET['no'])) ? $_GET['no'] : "";

	// GET : 위의 idx 번호가 없으면 등록(write)으로, 없으면 수정(modify)으로 mode에 저장
	$mode = (!$idx) ? "write" : "modify";

	// POST : form action 을 통해 넘겨받은 process 가 있는지 체크하고 있으면 proc에 저장
	$proc = isset($_POST['process']) ? $_POST['process'] : "";

	// GET : code 로 넘어온 값을 kind에 저장
	$kind 	 = (isset($_GET['kind'])) ? $_GET['kind'] : "";
	$channel = (isset($_GET['channel'])) ? $_GET['channel'] : "";

	if($kind==null)	// POST : code 로 넘어온 값을 kind에 저장<strong></strong>
		$kind=(isset($_POST['kind'])) ? $_POST['kind'] : "";
	if($channel==null)	// POST : code 로 넘어온 값을 channel에 저장<strong></strong>
		$channel=(isset($_POST['promotion_channel'])) ? $_POST['promotion_channel'] : "";

	if($channel==null)	// POST : code 로 넘어온 값을 channel에 저장<strong></strong>
		$channel=(isset($_POST['channel'])) ? $_POST['channel'] : "";
		$smarty->assign("channel",$channel);

	if($mode=="modify"){
		// promotion kind 가져오기
		$kind_sql="select kind from promotion where p_no='".$idx."'";
		$kind_result=mysqli_query($my_db,$kind_sql);

		$kind_result_array = mysqli_fetch_array($kind_result);
		$kind=$kind_result_array[0];
	}
	$smarty->assign("kind",$kind);

	$p_regdate=date("Y-m-d H:i:s");
	// 버튼명 구별하여 출력
	$submit_btn=($mode=="write") ? "등록하기":"수정하기";
	$smarty->assign("submit_btn",$submit_btn);


	// 직원가져오기
	$add_where_permission = str_replace("0", "_", $permission_name['마케터']);
	$staff_sql="select s_no,id,s_name from staff where staff_state < '3' AND permission like '".$add_where_permission."'";
	$staff_query=mysqli_query($my_db,$staff_sql);

	while($staff_data=mysqli_fetch_array($staff_query)) {
		$staffs[]=array(
				'no'=>$staff_data['s_no'],
				'name'=>$staff_data['s_name']
		);
		$smarty->assign("staff",$staffs);
	}

	// 프로모션 상태코드(p_state)에 텍스트 이름
	$state=array(
				1=>'접수대기',
				2=>'접수완료',
				3=>'모집중',
				4=>'진행중',
				5=>'진행중단',
				6=>'마감'
			);

	$kind_query="";

	if($kind == 1) {
		$cons_tels[]=$_POST['cons_tel_0'];
		$cons_tels[]=$_POST['cons_tel_1'];
		$cons_tels[]=$_POST['cons_tel_2'];
		$cons_tel=implode("-",$cons_tels);
		$kind_query.="pres_reward='".$_POST['pres_reward']."', ";
		$kind_query.="cons_exp_time='".$_POST['cons_exp_time']."', ";
		$kind_query.="cons_contact='".$_POST['cons_contact']."', ";
		$kind_query.="cons_tel='".$cons_tel."', ";
		$kind_query.="cons_tel_perm='".$_POST['cons_tel_perm']."', ";
		$kind_color="#f77b18";
	} elseif($kind == 2) {
		$pres_visit_view = isset($_POST['pres_visit_view']) ? 1 : 0;
		$kind_query.="pres_money='".addslashes($_POST['pres_money'])."', ";
		$kind_query.="pres_visit='".$_POST['pres_visit']."', ";
		$kind_query.="pres_visit_view='".$pres_visit_view."', ";
		$kind_query.="pres_purpose='".addslashes($_POST['pres_purpose'])."', ";
		$kind_query.="pres_file='".addslashes($_POST['pres_file'])."', ";
		$kind_color="#5175fe";
	} elseif($kind == 3) {
		$kind_query.="deli_seller='".$_POST['deli_seller']."', ";
		$kind_query.="deli_shipping='".$_POST['deli_shipping']."', ";
		$kind_query.="deli_shipping_memo='".addslashes($_POST['deli_shipping_memo'])."', ";
		$kind_query.="deli_zone='".$_POST['deli_zone']."', ";
		$kind_query.="deli_zone_memo='".addslashes($_POST['deli_zone_memo'])."', ";
		$kind_query.="deli_type='".$_POST['deli_type']."', ";
		$kind_query.="deli_type_memo='".addslashes($_POST['deli_type_memo'])."', ";
		$kind_color="#63F";
	} elseif($kind == 4) {
		$cons_tels[]=$_POST['cons_tel_0'];
		$cons_tels[]=$_POST['cons_tel_1'];
		$cons_tels[]=$_POST['cons_tel_2'];
		$cons_tel=implode("-",$cons_tels);
		$kind_query.="pres_reward='".$_POST['pres_reward']."', ";
		$kind_query.="pres_reason='".addslashes($_POST['pres_reason'])."', ";
		$kind_query.="pres_money='".addslashes($_POST['pres_money'])."', ";
		$kind_query.="cons_exp_time='".$_POST['cons_exp_time']."', ";
		$kind_query.="cons_contact='".$_POST['cons_contact']."', ";
		$kind_query.="cons_tel='".$cons_tel."', ";
		$kind_query.="cons_tel_perm='".$_POST['cons_tel_perm']."', ";
		$kind_color="#f77b18";
	} elseif($kind == 5) {
		$kind_query.="pres_reward='".$_POST['pres_reward']."', ";
		$kind_query.="pres_reason='".addslashes($_POST['pres_reason'])."', ";
		$kind_query.="pres_money='".addslashes($_POST['pres_money'])."', ";
		$kind_query.="deli_seller='".$_POST['deli_seller']."', ";
		$kind_query.="deli_shipping='".$_POST['deli_shipping']."', ";
		$kind_query.="deli_shipping_memo='".addslashes($_POST['deli_shipping_memo'])."', ";
		$kind_query.="deli_zone='".$_POST['deli_zone']."', ";
		$kind_query.="deli_zone_memo='".addslashes($_POST['deli_zone_memo'])."', ";
		$kind_query.="deli_type='".$_POST['deli_type']."', ";
		$kind_query.="deli_type_memo='".addslashes($_POST['deli_type_memo'])."', ";
		$kind_color="#63F";
	}

	// 지역 분류 가져오기(1차)
	$sql="select * from kind where k_code='location' and k_parent is null";
	$query=mysqli_query($my_db,$sql);
	while($result=mysqli_fetch_array($query)) {
		$location[]=array(
			"location_name"=>trim($result['k_name']),
			"location_code"=>trim($result['k_name_code'])
		);
		$smarty->assign("location",$location);
	}

	// 지역 분류 가져오기(2차)
	$sql="select * from kind where k_code='location'";
	$query=mysqli_query($my_db,$sql);
	while($result=mysqli_fetch_array($query)) {
		$location1[]=array(
			"location_name"=>trim($result['k_name']),
			"location_code"=>trim($result['k_name_code'])
		);
		$smarty->assign("location1",$location1);
	}

	// 업종 분류 가져오기(1차)
	$sql="select * from kind where k_code='job' and k_parent is null";
	$query=mysqli_query($my_db,$sql);
	while($result=mysqli_fetch_array($query)) {
		$job[]=array(
			"job_name"=>trim($result['k_name']),
			"job_code"=>trim($result['k_name_code'])
		);
		$smarty->assign("job",$job);
	}

	// 업종 분류 가져오기(2차)
	$sql="select k_name_code,k_name from kind where k_code='job'";
	$query=mysqli_query($my_db,$sql);
	while($result=mysqli_fetch_array($query)) {
		$job1[]=array(
			"job_name"=>trim($result['k_name']),
			"job_code"=>trim($result['k_name_code'])
		);
		$smarty->assign("job1",$job1);
	}


	if($mode=="modify")
	{
		$query	= "SELECT *, (SELECT depth FROM team t WHERE t.team_code=p.team) AS t_depth, (SELECT s_name FROM staff s WHERE s.s_no=p.reg_s_no) AS reg_s_name, (SELECT depth FROM team t WHERE t.team_code=p.reg_team) AS reg_t_depth FROM promotion p WHERE p_no='{$idx}'";
		$result = mysqli_query($my_db, $query);
		$promotion = mysqli_fetch_array($result);

		$sql 	= "SELECT * FROM company where c_no='{$promotion['c_no']}'";
		$result = mysqli_query($my_db,$sql);
		$info 	= mysqli_fetch_array($result);
		$zip	= explode("-",$info['zipcode']);
		$zip1	= $zip[0];
		$zip2	= $zip[1];
		$tel	= explode("-",$info['tel']);
		$tel1	= $tel[0];
		$tel2	= $tel[1];
		$tel3	= $tel[2];

		// 신청자 수
		$sql	= "SELECT COUNT(*) FROM application WHERE p_no='".$idx."'";
		$result = mysqli_query($my_db, $sql);
		$application = mysqli_fetch_array($result);

		$app_num = $application[0];

		// 취소 수
		$sql	= "SELECT COUNT(*) FROM application WHERE p_no='".$idx."' AND a_state=3";
		$result = mysqli_query($my_db, $sql);
		$application = mysqli_fetch_array($result);

		$cancel_num = $application[0];

		// 알림 요청 수
		$sql = "SELECT COUNT(*) FROM reservation WHERE p_no in (SELECT p_no FROM promotion WHERE c_no = '" . $promotion['c_no'] . "') ";
		$result = mysqli_query($my_db, $sql);
		$row = mysqli_fetch_array($result);

		$reservation_num = $row[0];

		// 선정된 포스팅 수
		$sql= "SELECT COUNT(*) FROM application WHERE p_no='".$idx."' AND (a_state=1 OR a_state=3)";
		$result = mysqli_query($my_db, $sql);
		$application = mysqli_fetch_array($result);

		$select_posting_num=$application[0];

		// 완료수
		$cnt_refund_sql="select count(*) from application where p_no='".$idx."'";
		$cnt_refund_query= mysqli_query($my_db, $cnt_refund_sql);
		$cnt_refund_array = mysqli_fetch_array($cnt_refund_query);

		if ($cnt_refund_array[0] != 0){
			$total_refund_sql="select refund_count from application where p_no='".$idx."'";
			$total_refund_query= mysqli_query($my_db, $total_refund_sql);
			$total_refund=0;
			while($total_refund_array = mysqli_fetch_array($total_refund_query)){
				$total_refund += $total_refund_array[0];
			}
		}

		$sql="select count(*) from report where p_no='".$idx."'";
		$query=mysqli_query($my_db,$sql);
		$data=mysqli_fetch_array($query);
		$total_num = $data[0];
		$smarty->assign("total_num",$data[0]);

		// 포스팅 수 구하는 공식
		if($promotion['posting_num']!=0)
			$posting_num=$promotion['reg_num'];

		$smarty->assign("total_posting_num", $posting_num);


		// 데이터 변환
		$tels = explode("-",$promotion['cons_tel']);
		$smarty->assign(
			array(
				"cons_tel_0"=>$tels[0],
				"cons_tel_1"=>$tels[1],
				"cons_tel_2"=>$tels[2]
			)
		);

		// 프로모션 상태코드(p_state)에 따른 하단 버튼 출력문자와 명령옵션 지정 (단, 진행중단이 아닐경우 노출)
		switch($promotion['p_state']) {
			// 진행대기일 경우
			case 1 :
				$state_text="접수완료";
				$state_proc="accept";
				break;
			// 접수완료일 경우
			case 2 :
				$state_text="모집중";
				$state_proc="unlock";
				break;
			// 모집중일 경우
			case 3 :
				$state_text="진행중";
				$state_proc="on";
				break;
		}
		$smarty->assign(
			array(
				"state_now"  => $state[$promotion['p_state']], // 현재 프로모션 상태
				"state_text" => $state_text,					// 하단 버튼에 출력할 문자
				"state_proc" => $state_proc					// 하단 버튼의 실행명령
			)
		);

        $t_label 	 = getTeamFullName($my_db, $promotion['t_depth'], $promotion['team']);
        $reg_t_label = getTeamFullName($my_db, $promotion['reg_t_depth'], $promotion['reg_team']);

        $s_label 	 = ($t_label) ? "{$promotion['name']} ({$t_label})" : "";
        $reg_s_label = ($reg_t_label) ? "{$promotion['reg_s_name']} ({$reg_t_label})" : "";

		$smarty->assign(
			array(
				"c_no"=>$promotion['c_no'],
				"s_no"=>$promotion['s_no'],
                "s_name"=>$s_label,
                "name"=>$promotion['name'],
                "team"=>$promotion['team'],
				"reg_s_no"=>$promotion['reg_s_no'],
				"reg_s_name"=>$reg_s_label,
				"reg_team"=>$promotion['reg_team'],
				"kind"=>$promotion['kind'],
				"channel"=>$promotion['channel'],
				"company"=>$promotion['company'],
				"cate1"=>$info['cate1'],
				"cate2"=>$info['cate2'],
				"company_detail"=>$info['about'],
				"zip1"=>$zip1,
				"zip2"=>$zip2,
				"tel1"=>$tel1,
				"tel2"=>$tel2,
				"tel3"=>$tel3,
				"address1"=>$info['address1'],
				"address2"=>$info['address2'],
				"parking"=>htmlspecialchars($info['parking']),
				"company_site"=>$info['site'],
				"location_1"=>$info['location1'],
				"location_2"=>$info['location2'],
				"reg_sdate"=>$promotion['reg_sdate'],
				"reg_edate"=>$promotion['reg_edate'],
				"awd_date"=>$promotion['awd_date'],
				"exp_sdate"=>$promotion['exp_sdate'],
				"exp_edate"=>$promotion['exp_edate'],
				"reg_num"=>$promotion['reg_num'],
				"posting_num"=>$promotion['posting_num'],
				"re_apply"=>$promotion['re_apply'],
				"re_apply_memo"=>htmlspecialchars($promotion['re_apply_memo']),
				"cons_exp_time"=>$promotion['cons_exp_time'],
				"cons_contact"=>$promotion['cons_contact'],
				"cons_tel_perm"=>$promotion['cons_tel_perm'],
				"pres_reward"=>$promotion['pres_reward'],
				"pres_reason"=>htmlspecialchars($promotion['pres_reason']),
				"pres_money"=>htmlspecialchars($promotion['pres_money']),
				"pres_visit"=>$promotion['pres_visit'],
				"pres_visit_view"=>$promotion['pres_visit_view'],
				"pres_purpose"=>htmlspecialchars($promotion['pres_purpose']),
				"pres_guide"=>htmlspecialchars($promotion['pres_guide']),
				"pres_guide_email"=>htmlspecialchars($promotion['pres_guide_email']),
				"pres_file"=>htmlspecialchars($promotion['pres_file']),
				"deli_seller"=>$promotion['deli_seller'],
				"deli_shipping"=>$promotion['deli_shipping'],
				"deli_shipping_memo"=>htmlspecialchars($promotion['deli_shipping_memo']),
				"deli_zone"=>$promotion['deli_zone'],
				"deli_zone_memo"=>htmlspecialchars($promotion['deli_zone_memo']),
				"deli_type"=>$promotion['deli_type'],
				"deli_type_memo"=>htmlspecialchars($promotion['deli_type_memo']),
				"subject_option"=>$promotion['subject_option'],
				"subject_option_memo"=>addslashes($promotion['subject_option_memo']),
				"product"=>$promotion['product'],
				"keyword_subject"=>htmlspecialchars($promotion['keyword_subject']),
				"keyword_print"=>htmlspecialchars($promotion['keyword_print']),
				"remark_memo"=>htmlspecialchars($promotion['remark_memo']),
				"refer_url1"=>htmlspecialchars($promotion['refer_url1']),
				"refer_url2"=>htmlspecialchars($promotion['refer_url2']),
				"raw_display"=>$promotion['raw_display'],
				"raw_display_memo"=>$promotion['raw_display_memo'],
				"opt_a_title"=>htmlspecialchars($promotion['opt_a_title']),
				"opt_a_desc"=>htmlspecialchars($promotion['opt_a_desc']),
				"opt_a_use"=>$promotion['opt_a_use'],
				"opt_b_title"=>htmlspecialchars($promotion['opt_b_title']),
				"opt_b_desc"=>htmlspecialchars($promotion['opt_b_desc']),
				"opt_b_use"=>$promotion['opt_b_use'],
				"opt_c_title"=>htmlspecialchars($promotion['opt_c_title']),
				"opt_c_desc"=>htmlspecialchars($promotion['opt_c_desc']),
				"opt_c_use"=>$promotion['opt_c_use'],
				"opt_d_title"=>htmlspecialchars($promotion['opt_d_title']),
				"opt_d_desc"=>htmlspecialchars($promotion['opt_d_desc']),
				"opt_d_use"=>$promotion['opt_d_use'],
				"opt_e_title"=>htmlspecialchars($promotion['opt_e_title']),
				"opt_e_desc"=>htmlspecialchars($promotion['opt_e_desc']),
				"opt_e_use"=>$promotion['opt_e_use'],
				"p_state"=>$promotion['p_state'],
				"p_state1"=>$promotion['p_state1'],
				"p_memo"=>htmlspecialchars($promotion['p_memo']),
				"app_num"=>$app_num,
				"cancel_num"=>$cancel_num,
				"reservation_num" => $reservation_num,
				"select_posting_num"=>$promotion['posting_num']/$promotion['reg_num']*$select_posting_num,
				"cancel_posting_num"=>$promotion['posting_num']/$promotion['reg_num']*$cancel_num,
				"total_refund"=>$total_refund,
				"cafe_url"=>htmlspecialchars($promotion['cafe_url']),
				"promotion_code"=>$promotion['promotion_code'],
				"is_file" => $promotion['is_file'],
				"title" => $promotion['title']
			)
		);


		// 수정 페이지 쿼리 저장
		$save_query=http_build_query($_GET);
		$smarty->assign("save_query",$save_query);
	}elseif($mode == 'write'){
        $reg_t_label = getTeamFullName($my_db, 0, $session_team);
        $reg_s_label = "{$session_name} ({$reg_t_label})";
        $smarty->assign("reg_s_no", $session_s_no);
        $smarty->assign("reg_team", $session_team);
        $smarty->assign("reg_s_name", $reg_s_label);
	}

	// form action 이 등록일 경우
	if($proc=="write")
	{
		$cons_tels[]=trim($_POST['cons_tel_0']);
		$cons_tels[]=trim($_POST['cons_tel_1']);
		$cons_tels[]=trim($_POST['cons_tel_2']);
		$cons_tel=implode("-",$cons_tels);

		$product = isset($_POST['product']) ? $_POST['product'] : "";

		$query="insert into promotion set
					c_no = '".$_POST['c_no']."',
					s_no = '{$_POST['s_no']}',
					team = '{$_POST['team']}',
					reg_s_no = '{$_POST['reg_s_no']}',
					reg_team = '{$_POST['reg_team']}',
					`name` = '{$_POST['name']}',
					company = '".$_POST['company']."',
					kind = '".$_POST['kind']."',
					channel = '".$_POST['promotion_channel']."',
					is_file = '".$_POST['is_file']."',
					reg_sdate = '".$_POST['reg_sdate']."',
					reg_edate = '".$_POST['reg_edate']."',
					exp_sdate = '".$_POST['exp_sdate']."',
					exp_edate = '".$_POST['exp_edate']."',
					awd_date = '".$_POST['awd_date']."',
					product = '". addslashes($product)."',
					pres_guide = '". addslashes($_POST['pres_guide'])."',
					pres_guide_email = '". addslashes($_POST['pres_guide_email'])."',";
		if ($kind == 2){
			$pres_visit_view = isset($_POST['pres_visit_view']) ? 1 : 0;
			$query.="	pres_reward = '".$_POST['pres_reward']."',
					pres_money = '".addslashes($_POST['pres_money'])."',
					pres_visit = '".$_POST['pres_visit']."',
					pres_visit_view = '".$pres_visit_view."',
					pres_purpose = '".addslashes($_POST['pres_purpose'])."',
					pres_file = '".addslashes($_POST['pres_file'])."',
					";
		}elseif ($kind == 4 || $kind == 5){
			$query.="	pres_reward = '".$_POST['pres_reward']."',
					pres_money = '".addslashes($_POST['pres_money'])."',
					pres_reason = '".addslashes($_POST['pres_reason'])."',
					";
		}elseif($kind == 1 || $kind == 3){
			$query.="pres_money = '".addslashes($_POST['pres_money'])."',
					pres_reason = '".addslashes($_POST['pres_reason'])."',
					";
		}

		if($_POST['posting_num'] < $_POST['reg_num']){
			$_POST['posting_num'] = $_POST['reg_num'];
		}

		$query.="	p_regdate = '".$p_regdate."',
					reg_num = '".$_POST['reg_num']."',
					posting_num = '".$_POST['posting_num']."',
					re_apply = '".$_POST['re_apply']."',
					re_apply_memo = '".addslashes($_POST['re_apply_memo'])."',
					subject_option = '".$_POST['subject_option']."',
					subject_option_memo = '".addslashes($_POST['subject_option_memo'])."',
					keyword_subject = '".addslashes($_POST['keyword_subject'])."',
					keyword_print = '".addslashes($_POST['keyword_print'])."',
					remark_memo = '".addslashes($_POST['remark_memo'])."',
					refer_url1 = '".addslashes($_POST['refer_url1'])."',
					refer_url2 = '".addslashes($_POST['refer_url2'])."',
					cons_exp_time = '".$_POST['cons_exp_time']."',
					cons_contact = '".$_POST['cons_contact']."',
					cons_tel = '".$cons_tel."',
					cons_tel_perm = '".$_POST['cons_tel_perm']."',
					raw_display = '".$_POST['raw_display']."',
					raw_display_memo = '".addslashes($_POST['raw_display_memo'])."',
					opt_a_title = '".addslashes($_POST['opt_a_title'])."',
					opt_a_desc = '".addslashes($_POST['opt_a_desc'])."',
					opt_a_use = '".$_POST['opt_a_use']."',
					opt_b_title = '".addslashes($_POST['opt_b_title'])."',
					opt_b_desc = '".addslashes($_POST['opt_b_desc'])."',
					opt_b_use = '".$_POST['opt_b_use']."',
					opt_c_title = '".addslashes($_POST['opt_c_title'])."',
					opt_c_desc = '".addslashes($_POST['opt_c_desc'])."',
					opt_c_use = '".$_POST['opt_c_use']."',
					opt_d_title = '".addslashes($_POST['opt_d_title'])."',
					opt_d_desc = '".addslashes($_POST['opt_d_desc'])."',
					opt_d_use = '".$_POST['opt_d_use']."',
					opt_e_title = '".addslashes($_POST['opt_e_title'])."',
					opt_e_desc = '".addslashes($_POST['opt_e_desc'])."',
					opt_e_use = '".$_POST['opt_e_use']."'
				";


		if (!mysqli_query($my_db, $query)){
			echo("<script>alert('프로모션 생성에 실패 하였습니다.\\n개발 담당자에게 문의해 주세요.');history.back();</script>");
			exit;
		}

		$sql_query= mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
		$sql_result=mysqli_fetch_array($sql_query);
		$last_p_no = $sql_result[0];

		$p_code = "tb-";

		if ($kind == '1')
			$p_code .= "exp-";
		elseif ($kind == '2')
			$p_code .= "rep-";
		else
			$p_code .= "dlv-";

		$p_code .= $last_p_no;

		$insert_code_sql = "update promotion set promotion_code ='".$p_code."' where p_no = '".$last_p_no."'";
		//echo "<br><br><br> Insert_code_query = ".$insert_code_sql;exit;

		if (!mysqli_query($my_db, $insert_code_sql)){
			echo("<script>alert('프로모션 코드 저장에 실패 하였습니다.\\n개발 담당자에게 문의해 주세요.');</script>");
		}

		$my_db->query($insert_code_sql);


		// work table 저장 [START]

		// prd_no 구하기
		$kind = $_POST['kind'];
		$prd_no = "";
		if($kind=='1') // 체험단
			$prd_no = "1";
		elseif($kind=='2') // 기자단
			$prd_no = "2";
		elseif($kind=='3') // 배송체험단
			$prd_no = "3";
		elseif($kind=='4') // 체험단+보상
			$prd_no = "170";
		elseif($kind=='5') // 배송체험단+보상
			$prd_no = "171";

		// k_name 구하기
		$channel = $_POST['promotion_channel'];
		$k_name = "";
		if($channel=='1') // 블로그
			$k_name = "블로그";
		elseif($channel=='2') // 인스타그램
			$k_name = "인스타그램";
		elseif($channel=='3') // 페이스북
			$k_name = "페이스북";
		elseif($channel=='4') // 카페
			$k_name = "카페";
		elseif($channel=='5') // 촬영
			$k_name = "촬영";
		elseif($channel=='6') // 컨텐츠
			$k_name = "컨텐츠";
		elseif($channel=='7') // 인플루언서
			$k_name = "인플루언서";
		elseif($channel=='8')  // 유튜브
			$k_name = "유튜브";

		$work_sql="INSERT into work set
						work_state = '2',
						c_no = '{$_POST['c_no']}',
						c_name = '{$_POST['company']}',
						s_no = '{$_POST['s_no']}',
						team = '{$_POST['team']}',
						prd_no = '".$prd_no."',
						k_name_code = (SELECT k.k_name_code FROM kind k WHERE k.k_code='work_task_run' AND k.k_parent='{$prd_no}' AND k.k_name='{$k_name}'),
						task_req = CONCAT('모집인원 : ', '{$_POST['reg_num']}', '\r\n포스팅 수 : ', '{$_POST['posting_num']}', '\r\n포스팅보상 : ', '{$_POST['pres_money']}'),
						task_req_s_no = '{$_POST['reg_s_no']}',
						task_req_team = '{$_POST['reg_team']}',
						task_run_s_no = '61',
						task_run_team = '00221',
						linked_no = '".$last_p_no."',
						linked_table = 'promotion',
						regdate = '".date("Y-m-d H:i:s")."'
					";
		//echo $work_sql; exit;
		if (!mysqli_query($my_db, $work_sql)){
			echo("<script>alert('업무리스트 저장에 실패 하였습니다.\\n개발 담당자에게 문의해 주세요.');</script>");
		}

		// work table 저장 [END]

		exit("<script>alert('등록하였습니다');location.href='promotion_list.php';</script>");


	// form action 이 수정일 경우
	} elseif($proc=="modify") {
		//print_r($_POST);echo "<br><br><br><br>";exit;
		//exit("<hr>");

		$sql = "select s_name from staff where s_no='".$_POST['s_no']."'";
		$result = mysqli_query($my_db,$sql);
		$info = mysqli_fetch_array($result);
		$s_name=$info['s_name'];
		$product = isset($_POST['product']) ? $_POST['product'] : "";

		$query="update promotion set
					c_no = '".$_POST['c_no']."',
					s_no = '{$_POST['s_no']}',
					team = '{$_POST['team']}',
					reg_s_no = '{$_POST['reg_s_no']}',
					reg_team = '{$_POST['reg_team']}',
					`name` = '{$_POST['name']}',
					company = '".$_POST['company']."',
					kind = '".$_POST['kind']."',
					channel = '".$_POST['promotion_channel']."',
					is_file = '".$_POST['is_file']."',
					reg_sdate = '".$_POST['reg_sdate']."',
					reg_edate = '".$_POST['reg_edate']."',
					exp_sdate = '".$_POST['exp_sdate']."',
					exp_edate = '".$_POST['exp_edate']."',
					awd_date = '".$_POST['awd_date']."',
					product = '".addslashes($product)."',
					pres_guide = '".addslashes($_POST['pres_guide'])."',
					pres_guide_email = '".addslashes($_POST['pres_guide_email'])."',";

		if ($kind == 1){
			$cons_tels[]=$_POST['cons_tel_0'];
			$cons_tels[]=$_POST['cons_tel_1'];
			$cons_tels[]=$_POST['cons_tel_2'];
			$cons_tel=implode("-",$cons_tels);

			$query.="cons_exp_time='".$_POST['cons_exp_time']."',
						cons_contact='".$_POST['cons_contact']."',
						cons_tel='".$cons_tel."',
						cons_tel_perm='".$_POST['cons_tel_perm']."', 
					";
		}

		if($kind == 1 || $kind == 3){
			$query.="pres_reason = '".addslashes($_POST['pres_reason'])."',
					pres_money = '".addslashes($_POST['pres_money'])."',
					";
		}

		if ($kind == 2){
			$pres_visit_view = isset($_POST['pres_visit_view']) ? 1 : 0;
			$query.="pres_reward = '".$_POST['pres_reward']."',
					pres_money = '".addslashes($_POST['pres_money'])."',
					pres_visit = '".$_POST['pres_visit']."',
					pres_visit_view = '".$pres_visit_view."',
					pres_purpose = '".addslashes($_POST['pres_purpose'])."',
					pres_file = '".addslashes($_POST['pres_file'])."',
					";
			if($channel == 5) { // 촬영
				$cons_tels[]=$_POST['cons_tel_0'];
				$cons_tels[]=$_POST['cons_tel_1'];
				$cons_tels[]=$_POST['cons_tel_2'];
				$cons_tel=implode("-",$cons_tels);

				$query.="cons_exp_time='".$_POST['cons_exp_time']."', ";
				$query.="cons_contact='".$_POST['cons_contact']."', ";
				$query.="cons_tel='".$cons_tel."', ";
				$query.="cons_tel_perm='".$_POST['cons_tel_perm']."', ";
			}
		}
		if ($kind == 4 || $kind == 5){
			$query.="pres_reward = '".$_POST['pres_reward']."',
					pres_reason = '".addslashes($_POST['pres_reason'])."',
					pres_money = '".addslashes($_POST['pres_money'])."',
					";
		}

		if($_POST['posting_num'] < $_POST['reg_num']){
			$_POST['posting_num'] = $_POST['reg_num'];
		}

		$query.="
					p_state = '".$p_state."',
					p_state1 = '".$p_state1."',
					reg_num = '".$_POST['reg_num']."',
					posting_num = '".$_POST['posting_num']."',
					re_apply = '".$_POST['re_apply']."',
					re_apply_memo = '".addslashes($_POST['re_apply_memo'])."',
					subject_option = '".$_POST['subject_option']."',
					subject_option_memo = '".addslashes($_POST['subject_option_memo'])."',
					$kind_query
					keyword_subject = '".addslashes($_POST['keyword_subject'])."',
					keyword_print = '".addslashes($_POST['keyword_print'])."',
					remark_memo = '".addslashes($_POST['remark_memo'])."',
					refer_url1 = '".addslashes($_POST['refer_url1'])."',
					refer_url2 = '".addslashes($_POST['refer_url2'])."',
					raw_display = '".$_POST['raw_display']."',
					raw_display_memo = '".addslashes($_POST['raw_display_memo'])."',
					opt_a_title = '".addslashes($_POST['opt_a_title'])."',
					opt_a_desc = '".addslashes($_POST['opt_a_desc'])."',
					opt_a_use = '".$_POST['opt_a_use']."',
					opt_b_title = '".addslashes($_POST['opt_b_title'])."',
					opt_b_desc = '".addslashes($_POST['opt_b_desc'])."',
					opt_b_use = '".$_POST['opt_b_use']."',
					opt_c_title = '".addslashes($_POST['opt_c_title'])."',
					opt_c_desc = '".addslashes($_POST['opt_c_desc'])."',
					opt_c_use = '".$_POST['opt_c_use']."',
					opt_d_title = '".addslashes($_POST['opt_d_title'])."',
					opt_d_desc = '".addslashes($_POST['opt_d_desc'])."',
					opt_d_use = '".$_POST['opt_d_use']."',
					opt_e_title = '".addslashes($_POST['opt_e_title'])."',
					opt_e_desc = '".addslashes($_POST['opt_e_desc'])."',
					opt_e_use = '".$_POST['opt_e_use']."'
				where
					p_no='".$_POST['idx']."'
				";
		//echo "<br><br><br><br>".$query;exit;
		$my_db->query($query);


		// work table 수정 [START]
		// prd_no 구하기
		$kind = $_POST['kind'];
		$prd_no = "";
		if($kind=='1') // 체험단
			$prd_no = "1";
		elseif($kind=='2') // 기자단
			$prd_no = "2";
		elseif($kind=='3') // 배송체험단
			$prd_no = "3";
		elseif($kind=='4') // 체험단+보상
			$prd_no = "170";
		elseif($kind=='5') // 배송체험단+보상
			$prd_no = "171";

		// k_name 구하기
		$channel = $_POST['promotion_channel'];
		$k_name = "";
		if($channel=='1') // 블로그
			$k_name = "블로그";
		elseif($channel=='2') // 인스타그램
			$k_name = "인스타그램";
		elseif($channel=='3') // 페이스북
			$k_name = "페이스북";
		elseif($channel=='4') // 카페
			$k_name = "카페";
		elseif($channel=='5') // 촬영
			$k_name = "촬영";
		elseif($channel=='6') // 컨텐츠
			$k_name = "컨텐츠";
		elseif($channel=='7') // 인플루언서
			$k_name = "인플루언서";
		elseif($channel=='8') // 유튜브
			$k_name = "유튜브";

		// 진행상태 설정
		$work_state_val = "";
		$add_set = "";
		$task_run_regdate_val = $_POST['reg_sdate'];
		if($p_state1 == '1'){ // 진행중단 -> 보류
			$work_state_val = '7';
		}elseif($p_state == '2'){ // 접수완료 -> 접수완료
			$work_state_val = '4';
		}elseif($p_state == '3' || $p_state == '4'){ // 모집중 or 진행중 -> 진행중
			$work_state_val = '5';
		}elseif($p_state == '5'){ // 마감 -> 진행완료
			$work_state_val = '6';
			$add_set .= " task_run_regdate = '{$task_run_regdate_val}', ";
		}

		if(!!$work_state_val){
			$add_set .= " work_state = '{$work_state_val}', ";
		}

		if(!permissionNameCheck($session_permission, "서비스운영")){
			$add_set .= " task_req_s_no = '{$_POST['reg_s_no']}', ";
			$add_set .= " task_req_team = '{$_POST['reg_team']}', ";
		}

		$work_sql = "UPDATE work SET
									$add_set
									c_no = '{$_POST['c_no']}',
									c_name = '{$_POST['company']}',
									s_no = '{$_POST['s_no']}',
									team = '{$_POST['team']}',
									prd_no = '{$prd_no}',
									k_name_code = (SELECT k.k_name_code FROM kind k WHERE k.k_code='work_task_run' AND k.k_parent='{$prd_no}' AND k.k_name='{$k_name}'),
									task_req = CONCAT('모집인원 : ', '{$_POST['reg_num']}', '\r\n포스팅 수 : ', '{$_POST['posting_num']}', '\r\n포스팅보상 : ', '{$_POST['pres_money']}')
								WHERE
									linked_no = '{$_POST['idx']}' AND linked_table = 'promotion'
								";
		//echo $work_sql; exit;
		if (!mysqli_query($my_db, $work_sql)){
			echo("<script>alert('업무리스트 수정에 실패 하였습니다.\\n개발 담당자에게 문의해 주세요.');</script>");
		}
		// work table 수정 [END]

		alert("수정하였습니다.","promotion_regist.php?no=".$_POST['idx']);

	// form 하단 접수완료 클릭시
	} elseif($proc=="accept") {
		$query="update promotion set p_state=2 where p_no='".$_POST['idx']."'";
		$my_db->query($query);
		alert("승인하였습니다.","main.php");

	// form 하단 모집중 클릭시
	} elseif($proc=="unlock") {
		$query="update promotion set p_state=3 where p_no='".$_POST['idx']."'";
		$my_db->query($query);
		alert("게시를 완료하였습니다.","main.php");

	// form 하단 진행중 클릭시
	} elseif($proc=="on") {
		$query="update promotion set p_state=4 where p_no='".$_POST['idx']."'";
		$my_db->query($query);
		alert("진행중으로 변경하였습니다.","main.php");

	// form 하단 진행중단 클릭시
	} elseif($proc=="off") {
		$query="update promotion set p_state=5 where p_no='".$_POST['idx']."'";
		$my_db->query($query);
		alert("중단하였습니다.","main.php");

	// 포스팅 가이드(마케터용)의 저장 버튼 클릭시
	} elseif($proc=="pres_guide") {
		$mno=(isset($_POST['idx']))?$_POST['idx']:"";
		$mtxt=(isset($_POST['pres_guide']))?addslashes($_POST['pres_guide']):"";

		$sql="update promotion set pres_guide='".$mtxt."' where p_no='".$mno."'";
		mysqli_query($my_db,$sql);

		exit("<script>alert('포스팅 가이드(마케터용)를 수정하였습니다');location.href='promotion_regist.php?no={$mno}';</script>");

		// 포스팅 가이드(가이드 메일용)의 저장 버튼 클릭시
	} elseif($proc=="pres_guide_email") {
		$mno=(isset($_POST['idx']))?$_POST['idx']:"";
		$mtxt=(isset($_POST['pres_guide_email']))?addslashes($_POST['pres_guide_email']):"";

		$sql="update promotion set pres_guide_email='".$mtxt."' where p_no='".$mno."'";
		mysqli_query($my_db,$sql);

		exit("<script>alert('포스팅 가이드(가이드 메일용)를 수정하였습니다');location.href='promotion_regist.php?no={$mno}';</script>");

	// 방문전 연락처의 저장 버튼 클릭시
	} elseif($proc=="cons_tel") {
		$mno=(isset($_POST['idx']))?$_POST['idx']:"";

		$cons_tels[]=$_POST['cons_tel_0'];
		$cons_tels[]=$_POST['cons_tel_1'];
		$cons_tels[]=$_POST['cons_tel_2'];
		$cons_tel=implode("-",$cons_tels);

		$sql="update promotion set cons_tel='".$cons_tel."' where p_no='".$mno."'";
		mysqli_query($my_db,$sql);

		exit("<script>alert('방문전 연락처를 수정하였습니다');location.href='promotion_regist.php?no={$mno}';</script>");

	// 포스팅 사진자료의 저장 버튼 클릭시
	} elseif($proc=="pres_file") {
		$mno=(isset($_POST['idx']))?$_POST['idx']:"";
		$mtxt=(isset($_POST['pres_file']))?addslashes($_POST['pres_file']):"";

		$sql="update promotion set pres_file='".$mtxt."' where p_no='".$mno."'";
		mysqli_query($my_db,$sql);

		exit("<script>alert('포스팅 사진자료를 수정하였습니다');location.href='promotion_regist.php?no={$mno}';</script>");

	// 제목 키워드의 저장 버튼 클릭시
	} elseif($proc=="keyword_subject") {
		$mno=(isset($_POST['idx']))?$_POST['idx']:"";
		$mtxt=(isset($_POST['keyword_subject']))?addslashes($_POST['keyword_subject']):"";

		$sql="update promotion set keyword_subject='".$mtxt."' where p_no='".$mno."'";
		mysqli_query($my_db,$sql);

		exit("<script>alert('제목 키워드를 수정하였습니다');location.href='promotion_regist.php?no={$mno}';</script>");

	// 노출체크 키워드의 저장 버튼 클릭시
	} elseif($proc=="keyword_print") {
		$mno=(isset($_POST['idx']))?$_POST['idx']:"";
		$mtxt=(isset($_POST['keyword_print']))?addslashes($_POST['keyword_print']):"";

		$sql="update promotion set keyword_print='".$mtxt."' where p_no='".$mno."'";
		mysqli_query($my_db,$sql);

		exit("<script>alert('노출체크 키워드를 수정하였습니다');location.href='promotion_regist.php?no={$mno}';</script>");

	// 특이사항 저장 버튼 클릭시
	} elseif($proc=="remark_memo") {
		$mno=(isset($_POST['idx']))?$_POST['idx']:"";
		$mtxt=(isset($_POST['remark_memo']))?addslashes($_POST['remark_memo']):"";

		$sql="update promotion set remark_memo='".$mtxt."' where p_no='".$mno."'";
		mysqli_query($my_db,$sql);

		exit("<script>alert('특이사항을 수정하였습니다');location.href='promotion_regist.php?no={$mno}';</script>");

	} elseif($proc=="memo") {

		$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
		$mno=(isset($_POST['idx']))?$_POST['idx']:"";
		$mtxt=(isset($_POST['p_memo']))?addslashes($_POST['p_memo']):"";
		$sql="update promotion set p_memo='".$mtxt."' where p_no='".$mno."'";

		mysqli_query($my_db,$sql);
		exit("<script>alert('메모를 등록하였습니다');location.href='promotion_regist.php".((!empty($qstr))?"?".$qstr:"")."';</script>");

	} elseif($proc=="cafe_url") {

		$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
		$mno=(isset($_POST['idx']))?$_POST['idx']:"";
		$mtxt=(isset($_POST['cafe_url']))?addslashes($_POST['cafe_url']):"";
		$sql="update promotion set cafe_url='".$mtxt."' where p_no='".$mno."'";

		mysqli_query($my_db,$sql);
		exit("<script>alert('카페 모집 공고 URL을 저장하였습니다');location.href='promotion_regist.php".((!empty($qstr))?"?".$qstr:"")."';</script>");

	} elseif($proc=="promo_title") { //캠페인명 추가

		$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
		$mno=(isset($_POST['idx']))?$_POST['idx']:"";
		$mtxt=(isset($_POST['promo_title']))?addslashes($_POST['promo_title']):"";
		$sql="update promotion set title='".$mtxt."' where p_no='".$mno."'";

		mysqli_query($my_db,$sql);
		exit("<script>alert('캠페인명을 저장하였습니다');location.href='promotion_regist.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
	} elseif($proc=="subject_option_memo") {
		$mno	= (isset($_POST['idx']))?$_POST['idx']:"";
		$mtxt	= (isset($_POST['subject_option_memo']))?addslashes($_POST['subject_option_memo']):"";

		$sql="update promotion set subject_option_memo='{$mtxt}' where p_no='{$mno}'";
		mysqli_query($my_db,$sql);

		exit("<script>alert('표기할 업체명을 저장했습니다.');location.href='promotion_regist.php?no={$mno}';</script>");

	}

	// 템플릿에 해당 입력값 던져주기
	$smarty->assign(
		array(
			"proc"=>$proc,
			"mode"=>$mode,
			"idx"=>$idx,
			"kind"=>$kind,
			"kind_color"=>$kind_color,
			"kind_name"=>$promotion_kind[$kind][0]
		)
	);
	$smarty->display('promotion_regist.html');

}else{ // 접근 권한이 없는경우
	$smarty->display('access_error.html');
}
?>
