<?php
require('inc/common.php');
require('ckadmin.php');

# 날짜별 검색
$sch_s_no  = isset($_GET['s_no']) ? $_GET['s_no'] : "";

if(empty($sch_s_no)){
    exit("<script>alert('s_no 값이 없습니다.');history.back();</script>");
}

$is_view = false;
if($session_s_no == '1'){
    $is_view = true;
}elseif($session_s_no == '28' && $sch_s_no != $session_s_no){
    $is_view = true;
}
$smarty->assign("is_view", $is_view);

# 퍼미션 체크
$team_leader_total_list = getTeamLeader($my_db);
$team_chk_sql 	        = "SELECT s.team FROM staff s WHERE s.s_no='{$sch_s_no}' LIMIT 1";
$team_chk_query         = mysqli_query($my_db, $team_chk_sql);
$team_chk_result        = mysqli_fetch_assoc($team_chk_query);
$team_code              = isset($team_chk_result['team']) ? $team_chk_result['team'] : "";
$team_leader_list       = isset($team_leader_total_list[$session_s_no]) ? $team_leader_total_list[$session_s_no] : [];

$is_leader = false;

if(!empty($team_leader_list) && in_array($team_code, $team_leader_list))
{
    $is_leader = true;
}

if (!($session_s_no == '28' || permissionNameCheck($session_permission,"대표")) && !($is_leader))
{
    $smarty->display('access_error.html');
    exit;
}

# 질문유형 정리
$ev_set_question_list   = [];
$ev_set_question_sql    = "SELECT ev_u_no, evaluation_state, question FROM evaluation_unit WHERE ev_u_set_no='3' AND active='1' ORDER BY ev_u_no ASC";
$ev_set_question_query  = mysqli_query($my_db, $ev_set_question_sql);
while($ev_set_question_result = mysqli_fetch_assoc($ev_set_question_query))
{
    $ev_set_question_list[$ev_set_question_result['ev_u_no']] = array(
        "q_type"    => ($ev_set_question_result['evaluation_state'] == '5') ? "서술형(장문)" : "점수형(1~5점)",
        "ev_state"  => $ev_set_question_result['evaluation_state'],
        "question"  => $ev_set_question_result['question']
    );
}

# 내부만족도 최근 ev_no 5개 출력
$ev_sat_val_sql    = "SELECT DISTINCT esr.ev_no FROM evaluation_system_result esr WHERE esr.receiver_s_no = '{$sch_s_no}' AND esr.ev_u_set_no=3 AND (SELECT es.ev_state FROM evaluation_system es WHERE es.ev_no=esr.ev_no)='4' ORDER BY ev_no DESC LIMIT 5";
$ev_sat_val_query  = mysqli_query($my_db, $ev_sat_val_sql);
$ev_sat_val_list   = [];
while($ev_sat_val = mysqli_fetch_assoc($ev_sat_val_query)){
    $ev_sat_val_list[] = $ev_sat_val['ev_no'];
}

$ev_no_val = '';
if(!empty($ev_sat_val_list)){
    sort($ev_sat_val_list);
    $ev_no_val = implode(",", $ev_sat_val_list);
}


# 내부만족도 X label
$ev_add_where   = "1=1";
$ev_x_add_where = "1=1";
if(!empty($ev_no_val)){
    $ev_add_where .= " AND ev_no IN({$ev_no_val})";
}else{
    $ev_add_where .= " AND ev_no IN('')";
}
$ev_sat_x_label_sql      = "SELECT es.ev_no, DATE_FORMAT(es.ev_e_date,'%Y-%m') as e_date, (SELECT count(er.ev_r_no) FROM evaluation_relation er WHERE er.ev_no=es.ev_no AND er.receiver_s_no='{$sch_s_no}') as eval_cnt FROM evaluation_system es WHERE {$ev_add_where} ORDER BY ev_no ASC";
$ev_sat_x_label_query    = mysqli_query($my_db, $ev_sat_x_label_sql);
$ev_sat_x_label_list     = [];
$ev_sat_table_label_tmp  = [];

while($ev_x_label = mysqli_fetch_assoc($ev_sat_x_label_query)){
    $ev_sat_x_label_list[] = "'{$ev_x_label['e_date']}'";
    $ev_sat_table_label_tmp[] = array("date" => $ev_x_label['e_date'], "eval_cnt" => $ev_x_label['eval_cnt']);
}

$ev_sat_y_title = array("98" => "전반적인 만족도", "99" => "산출물", "100" => "정확성", "101" => "신속성", "102" => "의사소통");
$ev_sat_y_label = array("전반적인 만족도", "산출물", "정확성", "신속성", "의사소통");

$ev_sat_sql = "
    SELECT 
        esr.ev_u_no,
        esr.ev_no,
        ROUND(AVG(esr.evaluation_value),2) as ev_score
    FROM evaluation_system_result esr 
    WHERE esr.receiver_s_no = '{$sch_s_no}' AND esr.ev_u_no IN(SELECT eu.ev_u_no FROM evaluation_unit eu WHERE eu.ev_u_set_no='3' AND eu.evaluation_state='1') AND esr.ev_u_no != '99'
      AND {$ev_add_where}
    GROUP BY ev_u_no, ev_no
    ORDER BY ev_no ASC, ev_u_no ASC
";
$ev_sat_query    = mysqli_query($my_db, $ev_sat_sql);
$ev_sat_list     = [];
while($ev_sat = mysqli_fetch_assoc($ev_sat_query))
{
    $ev_sat_list[$ev_sat['ev_no']][$ev_sat['ev_u_no']] = $ev_sat['ev_score'];
}


$ev_sat_full_data = [];
$ev_sat_tmp_data  = [];
$ev_sat_avg_list  = [];
$ev_sat_full_table_data       = [];
$ev_sat_full_table_total_data = [];
if($ev_sat_list){
    foreach($ev_sat_list as $ev_no => $stats_ev_data)
    {
        $ev_avg     = 0;
        $ev_total   = 0;
        $ev_cnt     = 0;
        if($stats_ev_data)
        {
            foreach ($stats_ev_data as $ev_u_no => $ev_score)
            {
                $ev_sat_full_data["each"]["line"][$ev_u_no]['title']  = $ev_sat_y_title[$ev_u_no];
                $ev_sat_full_data["each"]["line"][$ev_u_no]['data'][] = $ev_score;

                $ev_sat_full_data["each"]["bar"][$ev_u_no]['title']  = $ev_sat_y_title[$ev_u_no];
                $ev_sat_full_data["each"]["bar"][$ev_u_no]['data'][] = $ev_score;

                $ev_sat_tmp_data[$ev_u_no][] = array('ev_no' => $ev_no, "score" => $ev_score);

                $ev_total += $ev_score;
                $ev_cnt++;
            }
        }

        if($ev_cnt > 0){
            $ev_avg = ROUND(($ev_total/$ev_cnt), 2);
        }

        $ev_sat_avg_list[$ev_no] = $ev_avg;
        $ev_sat_full_data["sum"]["line"]["all"]['title']  = "합산";
        $ev_sat_full_data["sum"]["line"]["all"]['data'][] = $ev_avg;
        $ev_sat_full_data["sum"]["bar"]["all"]['title']   = "합산";
        $ev_sat_full_data["sum"]["bar"]["all"]['data'][]  = $ev_avg;
    }
}

$ev_total_cnt   = count($ev_set_question_list);
$ev_cur_cnt     = count($ev_sat_list);
$empty_cnt      = $ev_total_cnt-$ev_cur_cnt;
foreach($ev_set_question_list as $ev_u_no => $ev_set_data)
{
    $ev_sat_full_table_data[$ev_u_no] = array(
        "q_type"        => $ev_set_data['q_type'],
        "question"      => $ev_set_data['question'],
        "ev_state"      => $ev_set_data['ev_state'],
        "ev_state_name" => ($ev_set_data['ev_state'] == '5') ? "상세내역" : "평균점수",
    );

    $ev_idx = 1;
    if($ev_set_data['ev_state'] == '5')
    {
        for($ev_idx=1; $ev_idx<=$empty_cnt; $ev_idx++)
        {
            $idx_name    = "score_{$ev_idx}";
            $ev_idx_name = "ev_no_{$ev_idx}";

            if($ev_idx <= $empty_cnt)
            {
                $ev_sat_full_table_data[$ev_u_no][$idx_name]    = 0;
                $ev_sat_full_table_data[$ev_u_no][$ev_idx_name] = "";
                $ev_sat_full_table_data["total"][$idx_name]     = "-";
            }
        }

        if(!empty($ev_sat_avg_list))
        {
            foreach($ev_sat_avg_list as $ev_no => $ev_sat_avg)
            {
                $idx_name    = "score_{$ev_idx}";
                $ev_idx_name = "ev_no_{$ev_idx}";

                $ev_sat_full_table_data[$ev_u_no][$idx_name]    = 1;
                $ev_sat_full_table_data[$ev_u_no][$ev_idx_name] = $ev_no;
                $ev_sat_full_table_data["total"][$idx_name]     = $ev_sat_avg;
                $ev_idx++;
            }
        }
    }else{
        if($empty_cnt > 0){
            for($ev_idx=1; $ev_idx<=$empty_cnt; $ev_idx++){
                $idx_name = "score_{$ev_idx}";
                $ev_sat_full_table_data[$ev_u_no][$idx_name] = '-';
            }
        }

        if(!empty($ev_sat_tmp_data) && isset($ev_sat_tmp_data[$ev_u_no]))
        {
            foreach($ev_sat_tmp_data[$ev_u_no] as $tmp_data){
                $idx_name = "score_{$ev_idx}";
                $ev_sat_full_table_data[$ev_u_no][$idx_name] = $tmp_data['score'];
                $ev_idx++;
            }
        }
    }
}

$ev_sat_table_label_list = [];
if(!empty($ev_sat_table_label_tmp)){
    if($empty_cnt > 0)
    {
        for ($ev_idx = 1; $ev_idx <= $empty_cnt; $ev_idx++) {
            $ev_sat_table_label_list[] = '-';
        }
    }

    foreach($ev_sat_table_label_tmp as $ev_sat_table_tmp){
        $ev_sat_table_label_list[] = "{$ev_sat_table_tmp['date']}<br/>{$ev_sat_table_tmp['eval_cnt']}명";
    }

}else{
    $ev_sat_table_label_list = array("-","-","-","-","-");
}

$smarty->assign("s_no", $sch_s_no);
$smarty->assign("ev_set_question_list", $ev_set_question_list);
$smarty->assign("ev_cur_cnt", $ev_cur_cnt);
$smarty->assign('ev_sat_x_label_list', implode(',', $ev_sat_x_label_list));
$smarty->assign('ev_sat_legend_list', json_encode($ev_sat_y_label));
$smarty->assign('ev_sat_full_data', json_encode($ev_sat_full_data));
$smarty->assign('ev_sat_full_table_data', $ev_sat_full_table_data);
$smarty->assign('ev_sat_table_label_list', $ev_sat_table_label_list);

# 그래프 타이틀
$title_sql = "SELECT s.s_name, (SELECT t.team_name FROM team t WHERE t.team_code=s.team) as t_name FROM staff s WHERE s.s_no='{$sch_s_no}' LIMIT 1";
$title_query = mysqli_query($my_db, $title_sql);
$title_result = mysqli_fetch_assoc($title_query);

$ev_sat_title = "{$title_result['t_name']} \"{$title_result['s_name']}\"님 내부만족도 평가";
$smarty->assign("ev_sat_title", $ev_sat_title);

$smarty->display('staff_regist_evaluation_list.html');
?>
