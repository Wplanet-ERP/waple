<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/Team.php');

# 검색용
$active_option  = getDisplayOption();
$team_model    	= Team::Factory();
$sch_team_list	= $team_model->getTeamFullNameList();

# 검색조건
$add_where          = "apply_s_date IS NOT NULL AND apply_s_date IS NOT NULL";
$add_cal_where      = "lc.lsm_no=lsm.lsm_no";
$sch_leave_year     = isset($_GET['sch_leave_year']) ? $_GET['sch_leave_year'] : date("Y");
$sch_leave_team     = isset($_GET['sch_leave_team']) ? $_GET['sch_leave_team'] : "";
$sch_leave_staff    = isset($_GET['sch_leave_staff']) ? $_GET['sch_leave_staff'] : "";
$sch_active         = isset($_GET['sch_active']) ? $_GET['sch_active'] : "1";
$is_search_year     = false;

if(!empty($sch_leave_year))
{
    $is_search_year         = true;
    $sch_apply_s_date       = "{$sch_leave_year}-01-01";
    $sch_apply_e_date       = "{$sch_leave_year}-12-31";
    $sch_leave_s_datetime   = "{$sch_apply_s_date} 00:00:00";
    $sch_leave_e_datetime   = "{$sch_apply_e_date} 23:59:59";

    $add_where       .= " AND ((apply_s_date BETWEEN '{$sch_apply_s_date}' AND '{$sch_apply_e_date}') OR (apply_e_date BETWEEN '{$sch_apply_s_date}' AND '{$sch_apply_e_date}'))";
    $add_cal_where   .= " AND ((leave_s_date BETWEEN '{$sch_leave_s_datetime}' AND '{$sch_leave_e_datetime}') OR (leave_e_date BETWEEN '{$sch_leave_s_datetime}' AND '{$sch_leave_e_datetime}'))";
    $smarty->assign("sch_leave_year", $sch_leave_year);
}

if(!empty($sch_leave_team)){
    if ($sch_leave_team != "all") {
        $sch_team_code_where 	= getTeamWhere($my_db, $sch_leave_team);
        $add_where 			   .= " AND `team` IN ({$sch_team_code_where})";
    }
    $smarty->assign("sch_leave_team", $sch_leave_team);
}

if(!empty($sch_leave_staff)){
    $add_where  .= " AND `s_no` IN (SELECT s.s_no FROM staff AS s WHERE s.s_name LIKE '%{$sch_leave_staff}%')";
    $smarty->assign("sch_leave_staff", $sch_leave_staff);
}

if(!empty($sch_active)){
    $add_where  .= " AND active='{$sch_active}'";
    $smarty->assign("sch_active", $sch_active);
}

$staff_chk_sql          = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=lsm.s_no) as s_name, (SELECT t.team_name FROM `team` AS t WHERE t.team_code=lsm.`team`) as t_name FROM leave_staff_management AS lsm WHERE {$add_where} ORDER BY s_no, apply_s_date ASC";
$staff_chk_query        = mysqli_query($my_db, $staff_chk_sql);
$staff_leave_total_list = [];
$staff_leave_list       = [];
while($staff_chk = mysqli_fetch_assoc($staff_chk_query))
{
    if(!isset($staff_leave_total_list[$staff_chk['s_no']])){
        $staff_leave_total_list[$staff_chk['s_no']] = array(
            "leave_title"   => "{$staff_chk['s_name']} 휴가내역",
            "create_day"    => 0,
            "use_day"       => 0,
            "available_day" => 0,
        );
    }

    $staff_chk['available_day'] = ($staff_chk['create_day']-$staff_chk['use_day']);
    $staff_leave_total_list[$staff_chk['s_no']]['create_day']    += $staff_chk['create_day'];
    $staff_leave_total_list[$staff_chk['s_no']]['use_day']       += $staff_chk['use_day'];
    $staff_leave_total_list[$staff_chk['s_no']]['available_day'] += $staff_chk['available_day'];
    $staff_leave_list[$staff_chk['s_no']][$staff_chk['lm_no']]    = $staff_chk;

    $leave_etc_sql      = "SELECT *, (SELECT SUM(leave_value) as total FROM leave_calendar AS lc WHERE {$add_cal_where}) AS cal_use_day FROM leave_staff_management AS lsm WHERE lm_no != '1' AND active='1' AND s_no='{$staff_chk['s_no']}' ORDER BY lm_no";
    $leave_etc_query    = mysqli_query($my_db, $leave_etc_sql);
    while($leave_etc = mysqli_fetch_assoc($leave_etc_query))
    {
        $leave_etc['use_day'] = ($is_search_year) ? (float)$leave_etc['cal_use_day'] : $leave_etc['use_day'];

        $staff_leave_total_list[$leave_etc['s_no']]['create_day'] += $leave_etc['create_day'];
        $staff_leave_total_list[$leave_etc['s_no']]['use_day']    += $leave_etc['use_day'];
        $staff_leave_list[$leave_etc['s_no']][$leave_etc['lm_no']] = $leave_etc;
    }
}

$smarty->assign("active_option", $active_option);
$smarty->assign("sch_team_list", $sch_team_list);
$smarty->assign("staff_leave_total_list", $staff_leave_total_list);
$smarty->assign("staff_leave_list", $staff_leave_list);

$smarty->display('leave_management.html');
?>
