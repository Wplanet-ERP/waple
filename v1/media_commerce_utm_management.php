<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/commerce_utm.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Staff.php');
require('inc/model/Custom.php');

# 접근제한
$comm_utm_allow_list = getUtmAllowList();
if (!in_array($session_s_no, $comm_utm_allow_list) && !permissionNameCheck($session_permission, "마스터관리자") && $session_my_c_type != "2"){
    $smarty->display('access_company_error.html');
    exit;
}

# Process
$process    = isset($_POST['process']) ? $_POST['process'] : "";
$kind_model = Kind::Factory();
$utm_model  = Custom::Factory();
$utm_model->setMainInit("commerce_utm", "utm_no");

if($process == 'new_commerce_utm')
{
    $new_brand          = isset($_POST['new_brand']) ? $_POST['new_brand'] : "";
    $new_s_name         = isset($_POST['new_s_name']) ? $_POST['new_s_name'] : "";
    $new_s_date         = isset($_POST['new_s_date']) ? $_POST['new_s_date'] : "";
    $new_e_date         = isset($_POST['new_e_date']) ? $_POST['new_e_date'] : "";
    $new_memo           = isset($_POST['new_memo']) ? trim(addslashes($_POST['new_memo'])) : "";
    $new_land_url       = isset($_POST['new_land_url']) ? trim($_POST['new_land_url']) : "";
    $new_utm_medium     = isset($_POST['new_utm_medium']) ? trim($_POST['new_utm_medium']) : "";
    $new_utm_content    = isset($_POST['new_utm_content']) ? trim($_POST['new_utm_content']) : "";
    $new_utm_term       = isset($_POST['new_utm_term']) ? trim(addslashes($_POST['new_utm_term'])) : "";
    $new_regdate        = date('Y-m-d H:i:s');
    $kind_item          = $kind_model->getKindParent($new_utm_medium);
    $new_utm_source     = !empty($kind_item) ? $kind_item['k_parent'] : "";
    $new_utm_campaign   = $new_brand;
    $search_url         = $new_brand;

    if(empty($new_brand) || empty($new_s_name) || empty($new_s_date) || empty($new_land_url) || empty($new_utm_source) || empty($new_utm_medium) || empty($new_utm_campaign))
    {
        exit("<script>alert('다시 입력해주세요');history.back();</script>");
    }

    $staff_model        = Staff::Factory();
    $staff_item         = $staff_model->getStaffWhereName($new_s_name);
    $new_s_no           = isset($staff_item['s_no']) && !empty($staff_item['s_no']) ? $staff_item['s_no'] : "";

    if(empty($new_s_no)){
        exit("<script>alert('작성자를 다시 입력해주세요');history.back();</script>");
    }

    $utm_ins_data       = array(
        "brand"         => $new_brand,
        "s_no"          => $new_s_no,
        "s_date"        => $new_s_date,
        "e_date"        => $new_e_date,
        "memo"          => $new_memo,
        "land_url"      => $new_land_url,
        "utm_source"    => $new_utm_source,
        "utm_medium"    => $new_utm_medium,
        "utm_campaign"  => $new_utm_campaign,
        "utm_content"   => $new_utm_content,
        "utm_term"      => $new_utm_term,
        "regdate"       => $new_regdate
    );

    if($utm_model->insert($utm_ins_data)){
        exit("<script>alert('등록 성공했습니다');location.href='media_commerce_utm_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('등록 실패했습니다');location.href='media_commerce_utm_management.php?{$search_url}';</script>");
    }
}
elseif($process == 'modify_display')
{
    $utm_no     = (isset($_POST['utm_no'])) ? $_POST['utm_no'] : "";
    $display    = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($utm_no)){
        echo "utm_no 값이 없습니다.";
        exit;
    }

    if (!$utm_model->update(array("utm_no" => $utm_no, "display" => $display)))
        echo "Display 변경에 실패 하였습니다.";
    else
        echo "Display 값을 변경했습니다";
    exit;
}
elseif($process == 'modify_memo')
{
    $utm_no     = (isset($_POST['utm_no'])) ? $_POST['utm_no'] : "";
    $memo       = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if(empty($utm_no)){
        echo "utm_no 값이 없습니다.";
        exit;
    }

    if (!$utm_model->update(array("utm_no" => $utm_no, "memo" => $memo)))
        echo "관리메모 저장에 실패 하였습니다.";
    else
        echo "관리메모가 저장 되었습니다.";
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "";
$is_my_quick = "";
$nav_title   = "커머스 UTM 리스트";

if($session_my_c_type == "1"){
    $nav_prd_no  = "35";
    $quick_model = MyQuick::Factory();
    $is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
}

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$add_where       = "1=1";
$sch_display     = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$sch_brand       = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_s_name      = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
$sch_s_date      = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m')."-01";
$sch_e_date      = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m')."-31";
$sch_memo        = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
$sch_utm_source  = isset($_GET['sch_utm_source']) ? $_GET['sch_utm_source'] : "";
$sch_utm_medium  = isset($_GET['sch_utm_medium']) ? $_GET['sch_utm_medium'] : "";
$sch_utm_camp    = isset($_GET['sch_utm_camp']) ? $_GET['sch_utm_camp'] : "";
$sch_utm_content = isset($_GET['sch_utm_content']) ? $_GET['sch_utm_content'] : "";
$sch_utm_term    = isset($_GET['sch_utm_term']) ? $_GET['sch_utm_term'] : "";

$comm_utm_brand_option  = getUtmBrandOption();
if(!isset($_GET['sch_brand']))
{
    switch($session_s_no){
        case '42': $sch_brand = '1314'; break;
        case '17': $sch_brand = '1314'; break;
        case '177': $sch_brand = '1314'; break;
        case '303': $sch_brand = '1314'; break;
        case '3': $sch_brand = '2863'; break;
        case '7': $sch_brand = '2863'; break;
        case '105': $sch_brand = '2863'; break;
        case '171': $sch_brand = '2863'; break;
        case '15': $sch_brand = '2388'; break;
        case '209': $sch_brand = '2388'; break;
        case '4': $sch_brand = '2827'; break;
        case '166': $sch_brand = '2827'; break;
        case '6': $sch_brand = '3386'; break;
        case '59': $sch_brand = '2402'; break;
        case '184': $sch_brand = '3109'; break;
        case '63': $sch_brand = '3667'; break;
        case '10': $sch_brand = '1314'; break;
        case '205': $sch_brand = '3668'; break;
        case '67': $sch_brand = '5434'; break;
        case '243': $sch_brand = '5490'; break;
        case '193': $sch_brand = '4604'; break;
        case '300': $sch_brand = '4243'; break;
        case '52': $sch_brand = '3438'; break;
    }
}

if($session_my_c_type == "2")
{
    $add_where .= " AND `utm`.s_no IN('218','219','220')";
    $sch_brand = "3438";

    $comm_utm_brand_option = array("3438" => "단색");
}

if(!empty($sch_display))
{
    $add_where .= " AND `utm`.display = '{$sch_display}'";
    $smarty->assign('sch_display', $sch_display);
}

if(!empty($sch_brand))
{
    $add_where .= " AND `utm`.brand = '{$sch_brand}'";
    $smarty->assign('sch_brand', $sch_brand);
}

if(!empty($sch_s_name))
{
    $add_where .= " AND `utm`.s_no IN (SELECT s_no FROM staff WHERE s_name like '%{$sch_s_name}%')";
    $smarty->assign('sch_s_name', $sch_s_name);
}

if(!empty($sch_s_date) || !empty($sch_e_date))
{
    if(empty($sch_s_date)){
        $add_where .= " AND `utm`.s_date <= '{$sch_e_date}'";
    }elseif(empty($sch_e_date)){
        $add_where .= " AND `utm`.e_date >= '{$sch_s_date}'";
    }else{
        $add_where .= " AND (`utm`.s_date BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' OR `utm`.s_date <= '{$sch_s_date}' AND `utm`.e_date >= '{$sch_e_date}')";
    }

    $smarty->assign('sch_s_date', $sch_s_date);
    $smarty->assign('sch_e_date', $sch_e_date);
}

if(!empty($sch_memo))
{
    $add_where .= " AND `utm`.memo like '{$sch_memo}'";
    $smarty->assign('sch_memo', $sch_memo);
}

if(!empty($sch_utm_source))
{
    $add_where .= " AND `utm`.utm_source = '{$sch_utm_source}'";
    $smarty->assign('sch_utm_source', $sch_utm_source);
}

if(!empty($sch_utm_medium))
{
    $add_where .= " AND `utm`.utm_medium = '{$sch_utm_medium}'";
    $smarty->assign('sch_utm_medium', $sch_utm_medium);
}

if(!empty($sch_utm_camp))
{
    $add_where .= " AND `utm`.utm_campaign = '{$sch_utm_camp}'";
    $smarty->assign('sch_utm_camp', $sch_utm_camp);
}

if(!empty($sch_utm_content))
{
    $add_where .= " AND `utm`.utm_content like '{$sch_utm_content}'";
    $smarty->assign('sch_utm_content', $sch_utm_content);
}

if(!empty($sch_utm_term))
{
    $add_where .= " AND `utm`.utm_term like '{$sch_utm_term}'";
    $smarty->assign('sch_utm_term', $sch_utm_term);
}

# 전체 게시물 수
$comm_utm_total_sql     = "SELECT count(`utm`.utm_no) as cnt FROM commerce_utm `utm` WHERE {$add_where}";
$comm_utm_total_query	= mysqli_query($my_db, $comm_utm_total_sql);
$comm_utm_total_result  = mysqli_fetch_array($comm_utm_total_query);
$comm_utm_total = $comm_utm_total_result['cnt'];

# 페이징
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 10;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($comm_utm_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "media_commerce_utm_management.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $comm_utm_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$comm_utm_sql  = "
    SELECT 
        *,
        (SELECT s.s_name FROM staff s WHERE s.s_no = `utm`.s_no) as s_name,
        DATE_FORMAT(`utm`.s_date, '%Y%m%d') as s_date,
        DATE_FORMAT(`utm`.regdate, '%Y-%m-%d %H:%i') as regdate,
        DATE_FORMAT(`utm`.e_date, '%Y%m%d') as e_date
    FROM commerce_utm `utm`
    WHERE {$add_where}
    ORDER BY `utm`.utm_no DESC
    LIMIT {$offset}, {$num}
";
$comm_utm_list          = [];
$comm_utm_query         = mysqli_query($my_db, $comm_utm_sql);
$comm_utm_camp_option   = getUtmCampaignOption();
$comm_utm_total_list    = $kind_model->getKindDisplayNameList("utm");
$comm_utm_source_list   = $comm_utm_total_list["k_parent_list"];
$comm_utm_medium_list   = $comm_utm_total_list["k_child_list"];
while($comm_utm = mysqli_fetch_assoc($comm_utm_query))
{
    $comm_utm['brand_name']         = isset($comm_utm_brand_option[$comm_utm['brand']]) ? $comm_utm_brand_option[$comm_utm['brand']] : "";
    $comm_utm['utm_source_name']    = isset($comm_utm_source_list[$comm_utm['utm_source']]) ? $comm_utm_source_list[$comm_utm['utm_source']] : "";
    $comm_utm['utm_medium_name']    = isset($comm_utm_medium_list[$comm_utm['utm_medium']]) ? $comm_utm_medium_list[$comm_utm['utm_medium']] : "";
    $comm_utm['utm_campaign_name']  = isset($comm_utm_camp_option[$comm_utm['utm_campaign']]) ? $comm_utm_camp_option[$comm_utm['utm_campaign']] : "";
    $comm_utm['total_url']          = "";
    $comm_utm['e_date']             = ($comm_utm['e_date'] == '000000') ? "" : $comm_utm['e_date'];
    $add_total_url                  = [];

    if(!empty($comm_utm['utm_source_name'])){
        $add_total_url[] = "utm_source=".$comm_utm['utm_source_name'];
    }

    if(!empty($comm_utm['utm_medium_name'])){
        $add_total_url[] = "utm_medium=".$comm_utm['utm_medium_name'];
    }

    if(!empty($comm_utm['utm_campaign_name'])){
        $add_total_url[] = "utm_campaign=".$comm_utm['utm_campaign_name'];
    }

    if(!empty($comm_utm['utm_content'])){
        $add_total_url[] = "utm_content=".$comm_utm['utm_content'];
    }

    if(!empty($comm_utm['utm_term'])){
        $add_total_url[] = "utm_term=".urlencode($comm_utm['utm_term']);
    }

    if(!empty($comm_utm['land_url']) && !empty($add_total_url)){
        if (strpos($comm_utm['land_url'], "?") === false) {
            $comm_utm['total_url'] = $comm_utm['land_url']."?".implode("&", $add_total_url);
        }else{
            $comm_utm['total_url'] = $comm_utm['land_url']."&".implode("&", $add_total_url);
        }
    }

    $comm_utm_list[] = $comm_utm;
}

$smarty->assign('cur_date', date('Y-m-d'));
$smarty->assign('display_option', getDisplayOption());
$smarty->assign('page_type_option', getPageTypeOption('4'));
$smarty->assign('comm_utm_brand_option', $comm_utm_brand_option);
$smarty->assign('comm_utm_source_list', $comm_utm_source_list);
$smarty->assign('comm_utm_medium_list', $comm_utm_medium_list);
$smarty->assign('comm_utm_camp_option', $comm_utm_camp_option);
$smarty->assign('comm_utm_list', $comm_utm_list);

$smarty->display('media_commerce_utm_management.html');
?>
