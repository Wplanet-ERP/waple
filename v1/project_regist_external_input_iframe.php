<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Project.php');
require('inc/helper/project.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

# 변수 설정
$pj_no      = isset($_GET['pj_no']) ? $_GET['pj_no'] : "";
$list_url   = "project_regist_external_input_iframe.php";
$cur_date   = date("Y-m-d");
$project_partner_list   = [];
$resource_type_option   = getResourceTypeOption();

# 프로세스 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'del_external_partner')
{
    $pj_no    = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_er_no = isset($_POST['pj_er_no']) ? $_POST['pj_er_no'] : "";
    $upd_sql  = "UPDATE `project_external_report` SET active='2' WHERE pj_er_no='{$pj_er_no}'";

    if(!mysqli_query($my_db, $upd_sql)){
        alert("외부투입인력 삭제처리에 실패했습니다", "{$list_url}?pj_no={$pj_no}");
    }else{
        alert("외부투입인력 삭제처리 했습니다", "{$list_url}?pj_no={$pj_no}&del=1");
    }
}
elseif($process == 'modify_pj_c_type')
{
    $pj_no       = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_er_no    = isset($_POST['pj_er_no']) ? $_POST['pj_er_no'] : "";
    $pj_c_type   = isset($_POST['pj_c_type']) ? $_POST['pj_c_type'] : "";

    $upd_sql  = "UPDATE `project_external_report` SET pj_c_type='{$pj_c_type}' WHERE pj_er_no='{$pj_er_no}'";

    if(!mysqli_query($my_db, $upd_sql)){
        alert("투입분야 변경에 실패했습니다", "{$list_url}?pj_no={$pj_no}");
    }else{
        alert("투입분야 변경했습니다", "{$list_url}?pj_no={$pj_no}");
    }
}
elseif($process == 'modify_pj_participation')
{
    $pj_no              = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_er_no           = isset($_POST['pj_er_no']) ? $_POST['pj_er_no'] : "";
    $pj_participation   = isset($_POST['pj_participation']) ? $_POST['pj_participation'] : "";

    $upd_sql  = "UPDATE `project_external_report` SET pj_participation='{$pj_participation}' WHERE pj_er_no='{$pj_er_no}'";

    if(!mysqli_query($my_db, $upd_sql)){
        alert("참여상태 변경에 실패했습니다", "{$list_url}?pj_no={$pj_no}");
    }else{
        alert("참여상태 변경했습니다", "{$list_url}?pj_no={$pj_no}");
    }
}
elseif($process == 'modify_evaluation')
{
    $pj_er_no   = isset($_POST['pj_er_no']) ? $_POST['pj_er_no'] : "";
    $star_value = isset($_POST['val']) ? $_POST['val'] : "";

    $pj_staff_model  = Project::Factory();
    $pj_staff_model->setMainInit("project_external_report", "pj_er_no");
    $pj_staff_item   = $pj_staff_model->getItem($pj_er_no);

    $chk_eval_sql    = "SELECT * FROM project_external_report_evaluation WHERE pj_er_no='{$pj_er_no}' AND pj_no='{$pj_staff_item['pj_no']}' LIMIT 1";
    $chk_eval_query  = mysqli_query($my_db, $chk_eval_sql);
    $chk_eval_result = mysqli_fetch_assoc($chk_eval_query);
    $eval_no         = isset($chk_eval_result['pj_ere_no']) ? $chk_eval_result['pj_ere_no'] : "";

    if(!empty($eval_no)){
        $query_sql = "UPDATE project_external_report_evaluation SET score='{$star_value}' WHERE pj_ere_no='{$eval_no}'";
    }
    else{
        $query_sql = "INSERT INTO `project_external_report_evaluation` SET pj_no='{$pj_staff_item['pj_no']}', pj_er_no='{$pj_er_no}', pj_c_no='{$pj_staff_item['pj_c_no']}', score='{$star_value}', req_s_no='{$session_s_no}', regdate=now()";
    }

    if(!mysqli_query($my_db, $query_sql)){
        echo "평가점수를 저장에 실패했습니다.";
    }else{
        echo "평가점수를 저장했습니다.";
    }
    exit;
}
else
{
    $project_model  = Project::Factory();
    $project_item   = $project_model->getItem($pj_no);
    $is_editable    = false;
    if(($session_s_no == $project_item['manager']) || $session_s_no == "62"){
        $is_editable = true;
    }
    $smarty->assign("is_editable", $is_editable);

    $project_partner_sql = "
        SELECT 
            per.pj_er_no,
            per.pj_no,
            per.pj_c_no,
            per.pj_c_name,
            per.pj_c_type,
            per.pj_participation,
            pere.score as eval_score,
            DATE_FORMAT(per.pj_participation_date, '%Y-%m-%d') as pj_participation_date,
            DATE_FORMAT(per.pj_participation_date, '%H:%i:%s') as pj_participation_time,
            (SELECT COUNT(sub.pj_no) FROM project sub WHERE sub.pj_no='{$pj_no}' AND (sub.pj_s_date <= '{$cur_date}' AND sub.pj_e_date >= '{$cur_date}')) as active_pj_cnt,
            (SELECT c.resource_type FROM company as `c` WHERE c.c_no=per.pj_c_no LIMIT 1) as resource_type,
            (SELECT c.c_memo FROM company as `c` WHERE c.c_no=per.pj_c_no LIMIT 1) as c_memo
        FROM project_external_report as `per`
        LEFT JOIN project_external_report_evaluation as `pere` ON `pere`.pj_er_no=`per`.pj_er_no
        WHERE per.pj_no='{$pj_no}' AND per.active='1'
    ";

    $project_partner_query   = mysqli_query($my_db, $project_partner_sql);
    while($project_partner = mysqli_fetch_assoc($project_partner_query))
    {
        if(!empty($project_partner['pj_c_type'])){
            $project_partner['part_name'] = $resource_type_option[$project_partner['pj_c_type']];
        }

        $pj_c_type          = $project_partner['pj_c_type'];
        $chk_c_type_sql     = "SELECT per.pj_er_no, per.pj_c_type, pere.score FROM project_external_report per LEFT JOIN project_external_report_evaluation as `pere` ON `pere`.pj_er_no=`per`.pj_er_no WHERE per.active='1' AND per.pj_c_no='{$project_partner['pj_c_no']}'";
        $chk_c_type_query   = mysqli_query($my_db, $chk_c_type_sql);
        $chk_c_type_list    = array("total_cnt" => 0, "part_cnt" => 0, "total_score" => 0, "part_score" => 0);
        while($chk_c_type_result  = mysqli_fetch_assoc($chk_c_type_query))
        {
            $chk_c_type_list["total_cnt"]++;
            $chk_c_type_list["total_score"] += $chk_c_type_result['score'];

            if($chk_c_type_result['pj_c_type'] == $pj_c_type){
                $chk_c_type_list["part_cnt"]++;
                $chk_c_type_list["part_score"] += $chk_c_type_result['score'];
            }
        }

        $project_partner['part_cnt']    = empty($chk_c_type_list['part_cnt']) ? '0' : $chk_c_type_list['part_cnt'];
        $project_partner['total_cnt']   = empty($chk_c_type_list['total_cnt']) ? '0' : $chk_c_type_list['total_cnt'];
        $project_partner['part_avg']    = ($chk_c_type_list['part_score'] > 0 && $chk_c_type_list['part_cnt'] > 0) ?  $chk_c_type_list['part_score']/$chk_c_type_list['part_cnt'] : "0.00";
        $project_partner['total_avg']   = ($chk_c_type_list['total_score'] > 0 && $chk_c_type_list['total_cnt'] > 0) ? $chk_c_type_list['total_score']/$chk_c_type_list['total_cnt'] : "0.00";

        $project_partner['resource_type_list'] = !empty($project_partner['resource_type']) ? explode(",", $project_partner['resource_type']) : [];

        $project_partner_list[] = $project_partner;
    }
}

$smarty->assign("pj_no", $pj_no);
$smarty->assign("project_partner_list", $project_partner_list);
$smarty->assign("pj_participation_state_option", getProjectParticipationStateOption());
$smarty->assign("resource_type_option", $resource_type_option);

$smarty->display('project_regist_external_input_iframe.html');
?>
