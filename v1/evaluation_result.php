<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');

$ev_no=isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$smarty->assign("ev_no",$ev_no);
$ev_r_no=isset($_GET['ev_r_no']) ? $_GET['ev_r_no'] : "";
$smarty->assign("ev_r_no",$ev_r_no);

// POST : form action 을 통해 넘겨받은 process 가 있는지 체크하고 있으면 proc에 저장
$proc=isset($_POST['process']) ? $_POST['process'] : "";


// 평가 시스템 쿼리
$evaluation_system_sql="SELECT
 														ev_system.subject,
														ev_system.ev_s_date,
														ev_system.ev_e_date,
                            ev_system.ev_u_set_no,
                            (SELECT subject FROM evaluation_unit_set WHERE ev_u_set_no = ev_system.ev_u_set_no) AS ev_u_set_subject,
														ev_system.admin_s_no
													FROM evaluation_system ev_system
													WHERE ev_system.ev_no='{$ev_no}'
													";
$evaluation_system_result=mysqli_query($my_db,$evaluation_system_sql);
$system_result = mysqli_fetch_array($evaluation_system_result);

$smarty->assign(
	array(
		"f_subject"         => trim($system_result['subject']),
		"f_ev_s_date"       => trim($system_result['ev_s_date']),
		"f_ev_e_date"       => trim($system_result['ev_e_date']),
        "f_ev_u_set_no"     => trim($system_result['ev_u_set_no']),
        "ev_u_set_subject"  => trim($system_result['ev_u_set_subject']),
		"f_admin_s_no"      => trim($system_result['admin_s_no'])
	)
);

// 접근 권한
if (!(permissionNameCheck($session_permission, "대표") || $session_s_no == '28' || $session_s_no == $system_result['admin_s_no'])){
	$smarty->display('access_company_error.html');
	exit;
}else{
  if($_SESSION["security_pw"] == "PW_ACCESS"){
    unset($_SESSION["security_pw"]);
  }else{
    exit("<script>location.href='security_pw.php?ev_no={$ev_no}';</script>");
  }
}

// 평가지 항목 쿼리
$evaluation_unit_sql="SELECT
                        ev_u.kind,
												ev_u.question,
												ev_u.description,
												ev_u.evaluation_state
											FROM evaluation_unit ev_u
											WHERE ev_u.ev_u_set_no=(SELECT ev_system.ev_u_set_no FROM evaluation_system ev_system WHERE ev_system.ev_no='{$ev_no}')
                           AND ev_u.evaluation_state >= '1' AND ev_u.evaluation_state < '99' AND ev_u.active='1'
                      ORDER BY
                				`order` ASC
											";

$evaluation_unit_result=mysqli_query($my_db,$evaluation_unit_sql);
while($unit_array=mysqli_fetch_array($evaluation_unit_result)) {

	$evaluation_unit_list[]=array(
		"kind"=>$unit_array['kind'],
    "kind_substr"=>mb_substr($unit_array['kind'], 0, 3, 'UTF-8'),
    "question"=>$unit_array['question'],
		"question_substr"=>mb_substr($unit_array['question'], 0, 4, 'UTF-8'),
		"description"=>$unit_array['description'],
    "evaluation_state"=>$unit_array['evaluation_state']
	);
}

$smarty->assign("ev_unit_count",count($evaluation_unit_list));
$smarty->assign("ev_unit_list",$evaluation_unit_list);


#평가자 리스트
$evaluator_sql="SELECT DISTINCT evaluator_s_no,
                  (SELECT s_name FROM staff s WHERE s.s_no=evaluator_s_no) AS evaluator_s_name
                  FROM evaluation_system_result
                  WHERE ev_no='{$ev_no}'
                  ORDER BY evaluator_s_name ASC
                ";
$evaluator_result=mysqli_query($my_db,$evaluator_sql);
while($evaluator_array=mysqli_fetch_array($evaluator_result)){

  #평가자의 평가지 항목별 리스트 evaluator_s_no
  $unit_sql="SELECT DISTINCT ev_u_no
                FROM evaluation_system_result
                WHERE ev_no='{$ev_no}' AND evaluator_s_no='{$evaluator_array['evaluator_s_no']}' AND (evaluation_state='1' OR evaluation_state='2' OR evaluation_state='3')
                ORDER BY ev_u_no ASC
              ";
  $unit_result=mysqli_query($my_db,$unit_sql);
  while($unit_array=mysqli_fetch_array($unit_result)){

    #평가자의 평가지 항목별 점수(자기평가 제외)
    $unit_value_sql="SELECT
                        (SELECT s_name FROM staff s WHERE s.s_no=receiver_s_no) AS receiver_s_name,
                        ev_r_no,
                        rate,
                        evaluation_value
                      FROM evaluation_system_result
                      WHERE ev_no='{$ev_no}' AND evaluator_s_no='{$evaluator_array['evaluator_s_no']}' AND ev_u_no='{$unit_array['ev_u_no']}' AND evaluator_s_no <> receiver_s_no
                      ORDER BY receiver_s_name ASC
                      ";
    $unit_value_result=mysqli_query($my_db,$unit_value_sql);
    while($unit_value_array=mysqli_fetch_array($unit_value_result)){

      $result_list[]=array(
        "ev_u_no"=>$unit_array['ev_u_no'],
        "evaluator_s_no"=>$evaluator_array['evaluator_s_no'],
        "ev_r_no"=>$unit_value_array['ev_r_no'],
        "rate"=>$unit_value_array['rate'],
        "evaluation_value"=>$unit_value_array['evaluation_value']
      );
    }
  }
}

//print_r($result_list);
$smarty->assign("result_list",$result_list);


#평가관계 단위의 피평가자 리스트
$receiver_sql="SELECT DISTINCT ev_r_no,
                  receiver_s_no,
                  (SELECT s_name FROM staff s WHERE s.s_no=receiver_s_no) AS receiver_s_name,
                  evaluator_s_no,
                  (SELECT s_name FROM staff s WHERE s.s_no=evaluator_s_no) AS evaluator_s_name,
                  rate
                FROM evaluation_system_result
                WHERE ev_no='{$ev_no}'
                ORDER BY receiver_s_name ASC, evaluator_s_name ASC
                ";
$receiver_result=mysqli_query($my_db,$receiver_sql);
while($receiver_array=mysqli_fetch_array($receiver_result)){

  $result_total_list[]=array(
    "receiver_s_no"=>$receiver_array['receiver_s_no'],
    "receiver_s_name"=>$receiver_array['receiver_s_name'],
    "evaluator_s_no"=>$receiver_array['evaluator_s_no'],
    "evaluator_s_name"=>$receiver_array['evaluator_s_name'],
    "ev_r_no"=>$receiver_array['ev_r_no'],
    "rate"=>$receiver_array['rate']
  );
}
/*
// 다차원 배열 정렬
foreach ($result_total_list as $key => $row) {
  $receiver_s_no[$key] = $row['receiver_s_no'];
  $receiver_s_name[$key] = $row['receiver_s_name'];
  $evaluator_s_no[$key] = $row['evaluator_s_no'];
  $evaluator_s_name[$key] = $row['evaluator_s_name'];
  $ev_r_no[$key] = $row['ev_r_no'];
  $rate[$key] = $row['rate'];
}
// 피평가자 오름차순 정렬
array_multisort($receiver_s_name, SORT_ASC, $result_total_list);*/

$smarty->assign("result_total_list",$result_total_list);


// 평가 결과의 평가질문 답변 쿼리
$evaluation_unit_result_sql="SELECT
												ev_result.ev_r_no,
                        ev_result.ev_u_no,
												ev_result.question,
												ev_result.evaluation_state,
												ev_result.evaluation_value,
                        ev_result.choice_items
											FROM evaluation_system_result ev_result
											WHERE ev_result.ev_no='{$ev_no}'
                                            ORDER BY `order` ASC
											";

$evaluation_unit_result=mysqli_query($my_db,$evaluation_unit_result_sql);
while($unit_result_array=mysqli_fetch_array($evaluation_unit_result)) {
  $e_value = "";

  if($unit_result_array['evaluation_state'] == '9'){ // 객관형(복수)
    $e_value = str_replace("||",",",$unit_result_array['evaluation_value']);
  }else{
    $e_value = $unit_result_array['evaluation_value'];
  }

	$evaluation_unit_result_list[]=array(
		"ev_r_no"=>$unit_result_array['ev_r_no'],
    "ev_u_no"=>$unit_result_array['ev_u_no'],
		"question"=>$unit_result_array['question'],
		"evaluation_state"=>$unit_result_array['evaluation_state'],
    "evaluation_value"=>$e_value,
    "choice_items"=>$unit_result_array['choice_items'],
    "choice_items_explode"=>explode('||', $unit_result_array['choice_items'])
	);
}
$smarty->assign("ev_u_result_list",$evaluation_unit_result_list);

$smarty->display('evaluation_result.html');

?>
