<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '4G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_cms.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

# 검색 조건
$sch_log_c_no   = isset($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "5659";
$excel_title 	= ($sch_log_c_no == "5659") ? "스타트투데이_아이레놀 양식" : "스타트투데이_닥터피엘 양식";

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "도착국가(DESTINATION)")
	->setCellValue('B1', "배송종류(DELIVERY TYPE)")
	->setCellValue('C1', "송장번호(INVOICE NO)")
	->setCellValue('D1', "주문번호(ORDER NO1)")
	->setCellValue('E1', "주문번호(ORDER NO2)")
	->setCellValue('F1', "보내는사람 이름(SENDER NAME)")
	->setCellValue('G1', "보내는사람 전화(SENDER TEL NO)")
	->setCellValue('H1', "보내는사람 주소(SENDER ADDRESS)")
	->setCellValue('I1', "받는사람 이름(RECIPIENT NAME)")
	->setCellValue('J1', "받는사람 이름(YOMIGANA)")
	->setCellValue('K1', "받는사람 전화1(RECIPIENT TEL NO 1)")
	->setCellValue('L1', "받는사람 전화2(RECIPIENT TEL NO 2)")
	->setCellValue('M1', "받는사람 우편번호(ZIPCODE)")
	->setCellValue('N1', "받는사람 주소-성(STATE)")
	->setCellValue('O1', "받는사람 주소-시(CITY)")
	->setCellValue('P1', "받는사람 주소-구(DISTRICT)")
	->setCellValue('Q1', "받는사람 나머지 주소(ADDRESS 2)")
	->setCellValue('R1', "신분증번호(ID NUMBER)")
	->setCellValue('S1', "무게(WEIGHT) kg")
	->setCellValue('T1', "박스갯수(BOX COUNT)")
	->setCellValue('U1', "통화단위(CURRENCY UNIT)")
	->setCellValue('V1', "배송메세지(DELIVERY MESSAGE)")
	->setCellValue('W1', "사용자1(USER DATA1)")
	->setCellValue('X1', "사용자2(USER DATA2)")
	->setCellValue('Y1', "사용자3(USER DATA3)")
	->setCellValue('Z1', "대만 편의점배송 여부")
	->setCellValue('AA1', "판매Site")
	->setCellValue('AB1', "거래유형(B2C, B2B)")
	->setCellValue('AC1', "상품명(ProductName) 1")
	->setCellValue('AD1', "갯수(Count) 1")
	->setCellValue('AE1', "단가(Unit Price) 1")
	->setCellValue('AF1', "브랜드명(Brand) 1")
	->setCellValue('AG1', "제품용량(Capacity) 1")
	->setCellValue('AH1', "단위(Unit) 1")
	->setCellValue('AI1', "브랜드명(SKU) 1")
	->setCellValue('AJ1', "HSCODE 1")
	->setCellValue('AK1', "구매제품 URL 1")
	->setCellValue('AL1', "상품타입 1")
;



# 리스트 쿼리
$global_hp		= ($sch_log_c_no == "5659") ? "070-5099-5169" : "070-5099-5176";
$global_brand	= ($sch_log_c_no == "5659") ? "ilenol" : "Dr.piel";
$global_ord_sql = "
	SELECT
	   	w.delivery_state,
	   	w.order_number,
	   	w.parent_order_number,
	    w.prd_no,
	    (SELECT p.prd_code FROM product_cms p WHERE p.prd_no=w.prd_no) as sku,
	    w.quantity,
	    w.log_c_no,
	    (SELECT sub.dp_c_no FROM work_cms sub WHERE sub.order_number=w.parent_order_number LIMIT 1) as org_dp_c_no,
		w.recipient,
		w.recipient_hp,
		w.recipient_addr,
		w.stock_date,
		w.zip_code
	FROM work_cms w
	WHERE delivery_state='1' AND log_c_no='{$sch_log_c_no}'
	ORDER BY w.order_number, w.prd_no ASC
";
$global_ord_query	= mysqli_query($my_db, $global_ord_sql);
$dp_company_list	= getStartDpCompanyList();
$idx = 2;
if(!!$global_ord_query)
{
    while($work_cms = mysqli_fetch_array($global_ord_query))
    {
        $recipient_addr_val = trim($work_cms['recipient_addr']);
        $recipient_addr 	= preg_replace('/\r\n|\r|\n/','',$recipient_addr_val);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", "JP")
            ->setCellValue("B{$idx}", "A")
			->setCellValueExplicit("D{$idx}", $work_cms['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValue("F{$idx}", "Wise Mediacommerce")
			->setCellValue("G{$idx}", $global_hp)
			->setCellValue("H{$idx}", "Seoul Geumcheon-gu 902, 244, Beotkkot-ro")
			->setCellValue("I{$idx}", $work_cms['recipient'])
			->setCellValueExplicit("K{$idx}", $work_cms['recipient_hp'], PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValueExplicit("M{$idx}", $work_cms['zip_code'], PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValue("Q{$idx}", $recipient_addr)
			->setCellValue("S{$idx}", 0.3)
			->setCellValue("T{$idx}", 1)
			->setCellValue("U{$idx}", "JPY")
			->setCellValue("AD{$idx}", $work_cms["quantity"])
			->setCellValue("AE{$idx}", $work_cms["dp_price_vat"])
			->setCellValue("AF{$idx}", $global_brand)
			->setCellValue("AI{$idx}", $work_cms["sku"])
			->setCellValue("AL{$idx}", "N")
        ;
        $idx++;
    }
}
$idx--;

# 엑셀 크기, 정렬
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getActiveSheet()->getStyle('A1:AL1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00D9E1F2');
$objPHPExcel->getActiveSheet()->getStyle("A1:AL1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:AL{$idx}")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle('A1:AL1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A2:AL{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("A2:AL{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(12);

$objPHPExcel->getActiveSheet()->getStyle("S2:T{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("A1:AL{$idx}")->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->setTitle($excel_title);
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$excel_title}.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
