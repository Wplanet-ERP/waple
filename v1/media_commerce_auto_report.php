<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/commerce_sales.php');
require('inc/helper/work_cms.php');
require('inc/helper/wise_bm.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/CommerceSales.php');

# 프로세스 처리 & Model
$process          = isset($_POST['process']) ? $_POST['process'] : "";
$comm_sales_model = CommerceSales::Factory();

if($process == 'modify_price')
{
    $type       = (isset($_POST['type'])) ? $_POST['type'] : "";
    $brand      = (isset($_POST['brand'])) ? $_POST['brand'] : "";
    $comm_g2    = (isset($_POST['comm_g2'])) ? $_POST['comm_g2'] : "";
    $sales_date = (isset($_POST['date'])) ? $_POST['date'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
    $result     = false;

    $where_data = array(
        "type"          => $type,
        "sales_date"    => $sales_date,
        "brand"         => $brand,
        "k_name_code"   => $comm_g2
    );

    if($comm_sales_model->existReportData($where_data)){
        $comm_data = array(
            "price"         => $value
        );
        $result = $comm_sales_model->updateToArray($comm_data, $where_data);
    }else{
        $comm_data = array(
            "type"          => $type,
            "sales_date"    => $sales_date,
            "brand"         => $brand,
            "k_name_code"   => $comm_g2,
            "price"         => $value
        );
        $result = $comm_sales_model->insert($comm_data);
    }

    if (!$result)
        echo "실패했습니다";
    else
        echo "저장했습니다";
    exit;
}
elseif($process == 'modify_net_percent')
{
    $type       = (isset($_POST['type'])) ? $_POST['type'] : "";
    $brand      = (isset($_POST['brand'])) ? $_POST['brand'] : "";
    $comm_g2    = (isset($_POST['comm_g2'])) ? $_POST['comm_g2'] : "";
    $sales_date = (isset($_POST['date'])) ? $_POST['date'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
    $result     = false;

    $comm_sales_model->setMainInit("commerce_report_net", "cnp_no");

    $where_data = array(
        "sales_date"    => $sales_date,
        "brand"         => $brand
    );

    if($comm_sales_model->existReportData($where_data)){
        $comm_data = array(
            "percent"   => $value
        );
        $result = $comm_sales_model->updateToArray($comm_data, $where_data);
    }else{
        $comm_data = array(
            "sales_date"    => $sales_date,
            "brand"         => $brand,
            "percent"       => $value
        );
        $result = $comm_sales_model->insert($comm_data);
    }

    if (!$result)
        echo "실패했습니다";
    else
        echo "저장했습니다";
    exit;
}
elseif($process == 'add_commerce_memo')
{
    $cm_no       = (isset($_POST['cm_no'])) ? $_POST['cm_no'] : "";
    $brand       = (isset($_POST['brand'])) ? $_POST['brand'] : "";
    $k_name_code = (isset($_POST['k_name_code'])) ? $_POST['k_name_code'] : "";
    $sales_date  = (isset($_POST['sales_date'])) ? $_POST['sales_date'] : "";
    $memo 		 = (isset($_POST['memo'])) ? addslashes(trim($_POST['memo'])) : "";
    $result      = false;

    $comm_sales_model->setMainInit("commerce_report_memo", "cm_no");

    if(!empty($cm_no)){
        $comm_data = array(
            "cm_no" => $cm_no,
            "memo"  => $memo
        );
        $result = $comm_sales_model->update($comm_data);
    }else{
        $comm_data = array(
            "sales_date"    => $sales_date,
            "brand"         => $brand,
            "k_name_code"   => $k_name_code,
            "memo"          => $memo
        );
        $result = $comm_sales_model->insert($comm_data);
    }

    if (!$result)
        echo "2";
    else
        echo "1";
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "156";
$nav_title   = "커머스 매출 및 비용관리";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# Kind(cost,sales), Brand Group 검색
$comm_chart_total_init      = $comm_sales_model->getChartAutoReportData();
$comm_g1_title_list         = $comm_chart_total_init['comm_g1_title'];
$comm_g2_title_list         = $comm_chart_total_init['comm_g2_title'];

$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$dp_except_list             = getNotApplyDpList();
$dp_except_text             = implode(",", $dp_except_list);
$dp_self_imweb_list         = getSelfDpImwebCompanyList();
$global_dp_all_list         = getGlobalDpCompanyList();
$global_dp_total_list       = $global_dp_all_list['total'];
$global_dp_ilenol_list      = $global_dp_all_list['ilenol'];
$global_brand_list          = getGlobalBrandList();
$global_brand_option        = array_keys($global_brand_list);
$doc_brand_list             = getTotalDocBrandList();
$doc_brand_text             = implode(",", $doc_brand_list);
$ilenol_brand_list          = getIlenolBrandList();
$ilenol_brand_text          = implode(",", $ilenol_brand_list);
$ilenol_global_dp_text      = implode(",", $global_dp_ilenol_list);
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$sch_brand_name             = "전체";
$search_term_url            = "";

/** 검색조건 START */
$add_where                  = "display='1'";
$add_cms_where              = "w.delivery_state='4' AND dp_c_no NOT IN({$dp_except_text})";
$add_quick_where            = "w.prd_type='1' AND w.unit_price > 0 AND w.quick_state='4'";
$add_kind_where             = "";
$add_result_where           = "`am`.state IN(3,5) AND `am`.product IN(1,2) AND `am`.media='265'";
$add_board_where            = "";

# 브랜드 검색 설정 및 수정여부
$sch_toggle         = isset($_GET['sch_toggle']) ? $_GET['sch_toggle'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$kind_column_list   = [];
$brand_column_list  = [];
$brand_company_list = [];
$editable           = false;
$memo_editable      = false;
$allowed_download   = false;

$smarty->assign("sch_toggle", $sch_toggle);

if(!isset($_GET['sch_brand_g1']))
{
    switch($session_s_no){
        case '4':
            $sch_brand_g1   = '70003';
            break;
        case '6':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70008';
            break;
        case '7':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70009';
            $sch_brand      = '2863';
            break;
        case '10':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5368';
            break;
        case '15':
            $sch_brand_g1   = '70002';
            break;
        case '17':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '1314';
            break;
        case '42':
        case '177':
        case '265':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            break;
        case '59':
            $sch_brand_g1   = '70005';
            $sch_brand_g2   = '70020';
            $sch_brand      = '2402';
            break;
        case '67':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5434';
            break;
        case '145':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70010';
            break;
        case '147':
            $sch_brand_g1   = '70004';
            $sch_brand_g2   = '70019';
            break;
        case '43':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5812';
            break;
        case '326':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70014';
            $sch_brand      = '4446';
            break;
    }
}

if($session_s_no == '1' || $session_s_no == '6' || $session_s_no == '7' || $session_s_no == '10' || $session_s_no == '15' || $session_s_no == '18' || $session_s_no == '42' || $session_s_no == '59'
    || $session_s_no == '67' || $session_s_no == '76' || $session_s_no == '147' || $session_s_no == '166' || $session_s_no == '167' || $session_s_no == '171'){
    $allowed_download = true;
}

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1    = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2    = $brand_parent_list[$sch_brand]["brand_g2"];
    $search_term_url = "sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}&sch_brand={$sch_brand}";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1    = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
    $search_term_url = "sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}";
}elseif(!empty($sch_brand_g1)){
    $search_term_url = "sch_brand_g1={$sch_brand_g1}";
}

if(!empty($sch_brand_g1))
{
    if($sch_brand_g1 == "70003"){
        $add_board_where = "category='누잠'";
    }elseif($sch_brand_g1 == "70002"){
        $add_board_where = "category='아이레놀'";
    }
}

if(!empty($sch_brand_g2))
{
    if($sch_brand_g2 == "70007"){
        $add_board_where = "category='닥터피엘(필터라인)'";
    }elseif($sch_brand_g2 == "70008"){
        $add_board_where = "category='큐빙'";
    }elseif($sch_brand_g2 == "70012"){
        $add_board_where = "category='칫솔'";
    }elseif($sch_brand_g2 == "70014"){
        $add_board_where = "category='피처형정수기'";
    }elseif($sch_brand_g2 == "70013"){
        $add_board_where = "category='직수정수기'";
    }
}

$sch_cms_brand_url = "sch=Y";
if(!empty($sch_brand))
{
    if($sch_date_type == '1')
    {
        switch ($sch_brand) {
            case '1314':
            case '5513':
                if ($session_s_no == '1' || $session_s_no == '17' || $session_s_no == '42' || $session_s_no == '177' || $session_s_no == '239' || $session_s_no == '265') {
                    $editable = true;
                }
                break;
            case '5368':
                if ($session_s_no == '1' || $session_s_no == '10' || $session_s_no == '42') {
                    $editable = true;
                }
                break;

            case '2388':
            case '4440':
            case '4809':
            case '5759':
            case '5932':
                if ($session_s_no == '15' || $session_s_no == '239' || $session_s_no == '265' || $session_s_no == '327') {
                    $editable = true;
                }
                break;
            case '5514':
            case '5979':
            case '6044':
                if ($session_s_no == '15' || $session_s_no == '239' || $session_s_no == '265' || $session_s_no == "310" || $session_s_no == '327') {
                    $editable = true;
                }
                break;

            case '2402':
            case '4378':
                if ($session_s_no == '59') {
                    $editable = true;
                }
                break;

            case '2673':
                if ($session_s_no == '214') {
                    $editable = true;
                }
                break;

            case '2827':
            case '4878':
            case '5201':
            case '5642':
            case '5415':
            case '5810':
            case '6026':
            case '6060':
                if ($session_s_no == '4' || $session_s_no == '166') {
                    $editable = true;
                }
                break;

            case '2863':
                if ($session_s_no == '1' || $session_s_no == '7' || $session_s_no == '42' || $session_s_no == '171' || $session_s_no == '177' || $session_s_no == "326") {
                    $editable = true;
                }
                break;

            case '3303':
                if ($session_s_no == '1' || $session_s_no == '3' || $session_s_no == '42' || $session_s_no == '177' || $session_s_no == '145') {
                    $editable = true;
                }
                break;

            case '3386':
            case '5283':
                if ($session_s_no == '6' || $session_s_no == '7' || $session_s_no == '265') {
                    $editable = true;
                }
                break;
            case '4140':
                if ($session_s_no == '6' || $session_s_no == '7') {
                    $editable = true;
                }
                break;

            case '3832':
            case '4446':
                if ($session_s_no == '7' || $session_s_no == "326") {
                    $editable = true;
                }
                break;

            case '4333':
            case '5447':
                if ($session_s_no == '147') {
                    $editable = true;
                }
                break;

            case '4445':
                if ($session_s_no == '1' || $session_s_no == '42' || $session_s_no == '76') {
                    $editable = true;
                }
                break;

            case '4612':
            case '5812':
            case '5910':
                if ($session_s_no == '43' || $session_s_no == '323') {
                    $editable = true;
                }
                break;
            case '5434':
                if ($session_s_no == '67' || $session_s_no == '265') {
                    $editable = true;
                }
                break;
            case '5493':
                if ($session_s_no == '10') {
                    $editable = true;
                }
                break;
            case '5769':
                if ($session_s_no == '67') {
                    $editable = true;
                }
                break;
        }

        if (permissionNameCheck($session_permission, "마스터관리자")) {
            $editable = true;
        }
    }

    $sch_brand_g2_list              = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list                 = $brand_list[$sch_brand_g2];
    $brand_column_list[$sch_brand]  = $brand_list[$sch_brand_g2][$sch_brand];
    $brand_company_list[$sch_brand] = $sch_brand;
    $sch_brand_name                 = $brand_list[$sch_brand_g2][$sch_brand];

    if(in_array($sch_brand, $global_brand_option)) {
        if($sch_brand == '5979' || $sch_brand == '6044'){
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','09056'))";
            $add_kind_where .= " AND k_parent IN('08027','09056')";
        }elseif($sch_brand == '5514'){
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08033','08034','08035','08036','08042','08046','09060','09061','09062','09063','09081','09092'))";
            $add_kind_where .= " AND k_parent IN('08033','08034','08035','08036','08042','08046','09060','09061','09062','09063','09081','09092')";
        }else{
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092'))";
            $add_kind_where .= " AND k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092')";
        }
    } else {
        $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code NOT IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092'))";
        $add_kind_where .= " AND k_parent NOT IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092')";
    }

    if(in_array($sch_brand, $doc_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5956' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif(in_array($sch_brand, $ilenol_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5659' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "5513"){
        $add_cms_where      .= " AND `w`.c_no IN({$doc_brand_text}) AND (`w`.log_c_no = '5956' OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$doc_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5514"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND ((`w`.log_c_no = '5659' AND `w`.dp_c_no NOT IN({$ilenol_global_dp_text})) OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5979"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.c_no != '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "6044"){
        $add_cms_where      .= " AND `w`.c_no = '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    else{
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}'";
    }

    $add_result_where   .= " AND `ar`.brand = '{$sch_brand}'";
    $sch_cms_brand_url  .= "&sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}&sch_brand={$sch_brand}";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $brand_column_list  = $sch_brand_list;
    $sch_brand_name     = $brand_company_g2_list[$sch_brand_g1][$sch_brand_g2];

    foreach($sch_brand_list as $c_no => $c_name) {
        $brand_company_list[$c_no] = $c_no;
    }

    $add_where          .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_cms_where      .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_quick_where    .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_result_where   .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $sch_cms_brand_url  .= "&sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $brand_column_list  = $sch_brand_g2_list;
    $sch_brand_name     = $brand_company_g1_list[$sch_brand_g1];

    foreach($sch_brand_g2_list as $brand_g2 => $brand_g2_name)
    {
        $init_company_list = $brand_list[$brand_g2];
        if(!empty($init_company_list))
        {
            foreach($init_company_list as $c_no => $c_name) {
                $brand_company_list[$c_no] = $c_no;
            }
        }
    }

    $add_where          .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_cms_where      .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_quick_where    .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_result_where   .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $sch_cms_brand_url  .= "&sch_brand_g1={$sch_brand_g1}";
}
else
{
    $brand_column_list  = $brand_company_g1_list;

    foreach($brand_company_g1_list as $brand_g1 => $brand_g1_name)
    {
        $init_g2_list = $brand_company_g2_list[$brand_g1];
        if(!empty($init_g2_list))
        {
            foreach($init_g2_list as $brand_g2 => $brand_g2_name)
            {
                $init_company_list = $brand_list[$brand_g2];
                if(!empty($init_company_list))
                {
                    foreach($init_company_list as $c_no => $c_name) {
                        $brand_company_list[$c_no] = $c_no;
                    }
                }
            }
        }
    }
}

$smarty->assign("allowed_download", $allowed_download);
$smarty->assign("editable", $editable);
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_name", $sch_brand_name);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 날짜별 검색
$today_s_w		 = date('w')-1;
$cur_month       = date('Y-m');
$sch_s_year      = isset($_GET['sch_s_year']) ? $_GET['sch_s_year'] : date('Y', strtotime("-5 years"));;
$sch_e_year      = isset($_GET['sch_e_year']) ? $_GET['sch_e_year'] : date('Y');
$sch_s_month     = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("-5 months"));
$sch_e_month     = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week      = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week      = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date      = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-8 day"));
$sch_e_date      = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');
$sch_not_empty   = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : "";

$search_url      = getenv("QUERY_STRING");

if(empty($search_url) && $session_s_no == '1'){
    $sch_not_empty  = 1;
    $search_url     = "sch_not_empty=1";
}

$smarty->assign("search_url", $search_url);
$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_s_year', $sch_s_year);
$smarty->assign('sch_e_year', $sch_e_year);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);
$smarty->assign('sch_not_empty', $sch_not_empty);
$smarty->assign('cur_month', $cur_month);

# 전체 기간 조회 및 누적데이터 조회
$all_date_where     = "";
$all_date_key	    = "";
$add_date_where     = "";
$add_date_column    = "";
$add_cms_date_where = "";
$add_cms_column     = "";
$add_quick_column   = "";
$add_result_column  = "";
$sch_net_date       = "";
$chk_s_date         = "";
$chk_e_date         = "";

if($sch_date_type == '4') //연간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y')";

    $chk_s_date         = "{$sch_s_year}-01-01";
    $chk_e_date         = "{$sch_e_year}-12-31";

    $add_cms_column     = "DATE_FORMAT(order_date, '%Y')";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y')";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y')";
}
elseif($sch_date_type == '3') //월간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y%m')";

    $chk_s_date         = "{$sch_s_month}-01";
    $chk_e_day          = date("t", strtotime($sch_e_month));
    $chk_e_date         = "{$sch_e_month}-{$chk_e_day}";

    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m')";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y%m')";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y%m')";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	    = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title     = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_date_column    = "DATE_FORMAT(DATE_SUB(sales_date, INTERVAL(IF(DAYOFWEEK(sales_date)=1,8,DAYOFWEEK(sales_date))-2) DAY), '%Y%m%d')";

    $chk_s_date         = $sch_s_week;
    $chk_e_date         = $sch_e_week;

    $add_cms_column     = "DATE_FORMAT(DATE_SUB(order_date, INTERVAL(IF(DAYOFWEEK(order_date)=1,8,DAYOFWEEK(order_date))-2) DAY), '%Y%m%d')";
    $add_quick_column   = "DATE_FORMAT(DATE_SUB(run_date, INTERVAL(IF(DAYOFWEEK(run_date)=1,8,DAYOFWEEK(run_date))-2) DAY), '%Y%m%d')";
    $add_result_column  = "DATE_FORMAT(DATE_SUB(am.adv_s_date, INTERVAL(IF(DAYOFWEEK(am.adv_s_date)=1,8,DAYOFWEEK(am.adv_s_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y%m%d')";

    $chk_s_date         = $sch_s_date;
    $chk_e_date         = $sch_e_date;

    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m%d')";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y%m%d')";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y%m%d')";
}

$search_term_url   .= "&sch_date_type={$sch_date_type}&sch_base_s_date={$chk_s_date}&sch_base_e_date={$chk_e_date}";
$sch_net_date       = $chk_s_date;
$chk_s_datetime     = "{$chk_s_date} 00:00:00";
$chk_e_datetime     = "{$chk_e_date} 23:59:59";
$add_cms_date_where = " AND order_date BETWEEN '{$chk_s_datetime}' AND '{$chk_e_datetime}' ";
$add_quick_where   .= " AND run_date BETWEEN '{$chk_s_datetime}' AND '{$chk_e_datetime}' ";
$add_result_where  .= " AND am.adv_s_date BETWEEN '{$chk_s_datetime}' AND '{$chk_e_datetime}'";

$smarty->assign("search_term_url", $search_term_url);

# 이벤트 일정(해당일만 나옴 수정 필요)
$cal_event_sql = "
    SELECT
        DATE_FORMAT(cs_s_date, '%Y-%m-%d') as cs_s_date,
        DATE_FORMAT(cs_e_date, '%Y-%m-%d') as cs_e_date,
        cs_title
    FROM calendar_schedule
    WHERE cal_id='dp_event' AND cs_important='1' AND (  
        ('{$chk_s_datetime}' <= cs_s_date AND cs_s_date <= '{$chk_e_datetime}') 
        OR 
        ('{$chk_s_datetime}' <= cs_e_date AND cs_e_date <= '{$chk_e_datetime}') 
        OR
        (cs_s_date <= '{$chk_s_datetime}' AND '{$chk_s_datetime}' <= cs_e_date) 
        OR
        (cs_s_date <= '{$chk_e_datetime}' AND '{$chk_e_datetime}' <= cs_e_date) 
    )
";
$cal_event_query    = mysqli_query($my_db, $cal_event_sql);
$cal_event_all_list = [];
while($cal_event = mysqli_fetch_assoc($cal_event_query))
{
    $cal_event_all_list[] = $cal_event;
}

# 비용/매출 수기 리스트
$kind_display_list          = [];
$kind_column_list           = [];
$report_empty_check_list    = array("imweb_base" => 0, "imweb_doc" => 0, "imweb_nuzam" => 0, "imweb_ilenol" => 0, "smart" => 0, "delivery" => 0, "nosp_special" => 0, "nosp_time" => 0);
$kind_column_sql    = "
    SELECT 
        k_parent, 
        (SELECT sub_k.k_name FROM kind sub_k WHERE sub_k.k_name_code=k.k_parent) as k_parent_name, 
        (SELECT sub_k.priority FROM kind sub_k WHERE sub_k.k_name_code=k.k_parent) as k_parent_priority, 
        k_code, 
        k_name_code, 
        k_name,
        display
    FROM kind k 
    WHERE (k_code='cost' OR k_code='sales')
      AND k_parent is not null {$add_kind_where} 
    ORDER BY k_parent_priority ASC, priority ASC
";
$kind_column_query = mysqli_query($my_db, $kind_column_sql);
while($kind_column_data = mysqli_fetch_assoc($kind_column_query))
{
    $kind_display_list[$kind_column_data['k_name_code']] = $kind_column_data['display'];
    $kind_column_list[$kind_column_data['k_parent']][$kind_column_data['k_name_code']] = array(
        'k_name'        => $kind_column_data['k_name'],
        'k_name_code'   => $kind_column_data['k_name_code'],
        'type'          => $kind_column_data['k_code'],
        'k_parent'      => $kind_column_data['k_parent'],
        'k_parent_name' => $kind_column_data['k_parent_name'],
        'date'          => "",
        'price'         => 0
    );

    $report_empty_check_list[$kind_column_data['k_parent']] = 0;
    $report_empty_check_list[$kind_column_data['k_name_code']] = 0;
}

# 배송리스트 구매처
$cms_title_sql              = "SELECT DISTINCT dp_c_no, dp_c_name, (SELECT c.priority FROM company c WHERE c.c_no=w.dp_c_no) as priority FROM work_cms as w WHERE {$add_cms_where} {$add_cms_date_where} ORDER BY priority ASC, dp_c_name ASC";
$cms_title_query            = mysqli_query($my_db, $cms_title_sql);
$cms_report_title_list      = [];
$cms_report_delivery_list   = [];
$cms_delivery_free_chk_list = [];
while($cms_title = mysqli_fetch_assoc($cms_title_query)) {
    $cms_report_title_list[$cms_title['dp_c_no']]       = $cms_title['dp_c_name'];
    $cms_report_delivery_list[$cms_title['dp_c_no']]    = 0;
    $cms_delivery_free_chk_list[$cms_title['dp_c_no']]  = 0;
}

# 입/출고요청 구매처
$quick_title_sql            = "SELECT DISTINCT run_c_no, run_c_name FROM work_cms_quick as w WHERE {$add_quick_where} ORDER BY run_c_name ASC";
$quick_title_query          = mysqli_query($my_db, $quick_title_sql);
$quick_report_title_list    = [];
while($quick_title = mysqli_fetch_assoc($quick_title_query)) {
    $quick_report_title_list[$quick_title['run_c_no']] = $quick_title['run_c_name'];
}

$total_all_date_list            = [];
$commerce_date_list             = [];
$cms_report_list                = [];
$sales_report_list              = [];
$cost_report_list               = [];
$memo_report_list               = [];
$memo_report_total_list         = [];
$coupon_imweb_report_total_list = [];
$coupon_smart_report_total_list = [];
$cms_delivery_free_list         = [];
$cms_delivery_free_total_list   = [];
$nosp_result_special_list       = [];
$nosp_result_time_list          = [];
$quick_report_list              = [];
$cal_event_list                 = [];

$sales_tmp_total_list           = [];
$cost_tmp_total_list            = [];
$net_report_date_list           = [];
$net_parent_percent_list        = [];
$company_chk_list               = [];

# 날짜 리스트
$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
if($all_date_where != '')
{
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $total_all_date_list[$date['date_key']] = array("s_date" => date("Y-m-d",strtotime($date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($date['date_key']))." 23:59:59");

        $chart_title = $date['chart_title'];
        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        foreach($kind_column_list as $comm_g1 => $kind_columns)
        {
            foreach($kind_columns as $comm_g2 => $kind_column){
                if($kind_column['type'] == 'sales'){
                    $sales_report_list[$comm_g1][$comm_g2][$date['chart_key']] = $kind_column;
                    $sales_report_list[$comm_g1][$comm_g2][$date['chart_key']]['date'] = $date['chart_key'];
                }elseif($kind_column['type'] == 'cost'){
                    $cost_report_list[$comm_g1][$comm_g2][$date['chart_key']] = $kind_column;
                    $cost_report_list[$comm_g1][$comm_g2][$date['chart_key']]['date'] = $date['chart_key'];
                }

                $memo_report_list[$date['date_key']][$comm_g2] = "";
            }
            $memo_report_total_list[$date['date_key']][$comm_g1] = "";
        }

        foreach($cms_report_title_list as $cms_key => $cms_title)
        {
            $cms_report_list[$cms_key][$date['chart_key']]['subtotal'] = 0;
            $cms_report_list[$cms_key][$date['chart_key']]['delivery'] = 0;

            $cms_delivery_free_list[$cms_key][$date['chart_key']] = 0;
        }

        foreach($quick_report_title_list as $quick_key => $quick_title)
        {
            $quick_report_list[$quick_key][$date['chart_key']] = 0;
        }

        $coupon_imweb_report_total_list[$date['chart_key']] = array("total" => 0, "belabef" => 0, "doc" => 0, "ilenol" => 0, "nuzam" => 0);
        $coupon_smart_report_total_list[$date['chart_key']] = 0;
        $cms_delivery_free_total_list[$date['chart_key']]   = 0;
        $nosp_result_special_list[$date['chart_key']]       = 0;
        $nosp_result_time_list[$date['chart_key']]          = 0;

        if(!isset($commerce_date_list[$date['chart_key']]))
        {
            $chart_s_date = "";
            $chart_e_date = "";
            $chart_mon    = "";

            if($sch_date_type == '4'){
                $chart_s_date = $chart_title."-01-01";
                $chart_e_date = $chart_title."-12-31";
                $chart_mon    = date("Y-m", strtotime($chart_s_date));
            }elseif($sch_date_type == '3'){
                $chart_s_date_val   = $date['chart_key']."01";
                $chart_s_date       = date("Y-m-d", strtotime("{$chart_s_date_val}"));
                $chart_e_day        = DATE('t', strtotime($chart_s_date));
                $chart_e_date_val   = $date['chart_key'].$chart_e_day;
                $chart_e_date       = date("Y-m-d", strtotime("{$chart_e_date_val}"));
                $chart_mon          = date("Y-m", strtotime($chart_s_date));
            }elseif($sch_date_type == '2'){
                $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_e_date = date("Y-m-d", strtotime("{$chart_s_date} +6 days"));
                $chart_mon    = date("Y-m", strtotime($chart_s_date));
            }elseif($sch_date_type == '1'){
                $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_e_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_mon    = date("Y-m", strtotime($chart_s_date));
            }
            $commerce_date_list[$date['chart_key']] = array('title' => $chart_title, 's_date' => $chart_s_date, 'e_date' => $chart_e_date, 'mon_date' => $chart_mon);

            if($sch_brand_g2 == '70007' && !isset($cal_event_list[$date['chart_key']]))
            {
                $cal_event_text_list = [];

                foreach($cal_event_all_list as $cal_event)
                {
                    if($chart_s_date <= $cal_event['cs_s_date'] && $cal_event['cs_s_date'] <= $chart_e_date){
                        $cal_event_text_list[] = $cal_event['cs_title'];
                    }elseif($chart_s_date <= $cal_event['cs_e_date'] && $cal_event['cs_e_date'] <= $chart_e_date){
                        $cal_event_text_list[] = $cal_event['cs_title'];
                    }elseif($cal_event['cs_s_date'] <= $chart_s_date && $chart_s_date <= $cal_event['cs_e_date']){
                        $cal_event_text_list[] = $cal_event['cs_title'];
                    }elseif($cal_event['cs_s_date'] <= $chart_e_date && $chart_e_date <= $cal_event['cs_e_date']){
                        $cal_event_text_list[] = $cal_event['cs_title'];
                    }
                }

                if(!empty($cal_event_text_list)){
                    $cal_event_list[$date['chart_key']] = implode("\r\n", $cal_event_text_list);
                }
            }
        }

        foreach($brand_column_list as $kind_key => $kind_title)
        {
            foreach($brand_total_list[$kind_key] as $brand){
                $company_chk_list[$brand] = $kind_key;
            }
        }

        foreach($brand_company_list as $c_no)
        {
            $cost_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']]  = 0;
            $sales_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
            $net_report_date_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
            $net_parent_percent_list[$c_no] = 0;
        }
    }
}

# 발주관련 작업
$commerce_sql  = "
    SELECT
        cr.`type`,
        cr.price as price,
        cr.brand,
        cr.k_name_code as comm_g2,
        DATE_FORMAT(sales_date, '%Y%m%d') as sales_date,
        {$add_date_column} as key_date,
        (SELECT k.k_code FROM kind k WHERE k.k_name_code=`cr`.k_name_code) as comm_type,
        (SELECT k.k_name_code FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cr`.k_name_code)) as comm_g1,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cr`.k_name_code)) as comm_g1_name,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code=`cr`.k_name_code) as comm_g2_name
    FROM commerce_report `cr`
    WHERE {$add_where}
    {$add_date_where}
    ORDER BY sales_date ASC
";
$commerce_query = mysqli_query($my_db, $commerce_sql);
while($commerce = mysqli_fetch_assoc($commerce_query))
{
    $comm_g1 = str_pad($commerce['comm_g1'], 5, "0", STR_PAD_LEFT);
    if($commerce['type'] == 'sales'){
        $sales_report_list[$comm_g1][$commerce['comm_g2']][$commerce['key_date']]['price'] += $commerce['price'];
        $sales_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
    }elseif($commerce['type'] == 'cost'){
        $cost_report_list[$comm_g1][$commerce['comm_g2']][$commerce['key_date']]['price'] += $commerce['price'];
        $cost_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
    }

    $report_empty_check_list[$comm_g1] += $commerce['price'];
    $report_empty_check_list[$commerce['comm_g2']] += $commerce['price'];
}

# 지출 Total 값 구하기
$cost_report_total_list = [];
$cost_report_total_sum_list = [];
if(!empty($cost_report_list))
{
    foreach($cost_report_list as $comm_g1 => $cost_parent_report)
    {
        $comm_g1_display = $kind_display_list[$comm_g1];
        if(($sch_not_empty || $comm_g1_display == "2") && $report_empty_check_list[$comm_g1] == 0)
        {
            unset($cost_report_list[$comm_g1]);
            continue;
        }

        foreach($cost_parent_report as $comm_g2 => $cost_report)
        {
            $comm_g2_display = $kind_display_list[$comm_g2];
            if(($sch_not_empty || $comm_g2_display == "2") && $report_empty_check_list[$comm_g2] == 0)
            {
                unset($cost_report_list[$comm_g1][$comm_g2]);
                continue;
            }

            foreach($cost_report as $key_date => $cost_data){
                if(!isset($cost_report_total_list[$comm_g1])){
                    $cost_report_total_list[$comm_g1][$key_date] = 0;
                }
                $cost_report_total_list[$comm_g1][$key_date] += $cost_data['price'];

                if(!isset($cost_report_total_sum_list[$key_date])){
                    $cost_report_total_sum_list[$key_date] = 0;
                }
                $cost_report_total_sum_list[$key_date] += $cost_data['price'];
            }
        }
    }
}

# 매출 Total 값 구하기
$sales_report_total_list        = [];
$sales_report_total_sum_list    = [];
if(!empty($sales_report_list))
{
    foreach($sales_report_list as $comm_g1 => $sales_parent_report)
    {
        $comm_g1_display = $kind_display_list[$comm_g1];
        if(($sch_not_empty || $comm_g1_display == "2") && $report_empty_check_list[$comm_g1] == 0)
        {
            unset($sales_report_list[$comm_g1]);
            continue;
        }

        foreach($sales_parent_report as $comm_g2 => $sales_report)
        {
            $comm_g2_display = $kind_display_list[$comm_g2];
            if(($sch_not_empty || $comm_g2_display == "2") && $report_empty_check_list[$comm_g2] == 0)
            {
                unset($sales_report_list[$comm_g1][$comm_g2]);
                continue;
            }

            foreach($sales_report as $key_date => $sales_data){
                if(!isset($sales_report_total_list[$comm_g1])){
                    $sales_report_total_list[$comm_g1][$key_date] = 0;
                }
                $sales_report_total_list[$comm_g1][$key_date] += $sales_data['price'];

                if(!isset($sales_report_total_sum_list[$key_date])){
                    $sales_report_total_sum_list[$key_date] = 0;
                }
                $sales_report_total_sum_list[$key_date] += $sales_data['price'];
            }

        }
    }
}

# 배송리스트 매출관련
$commerce_fee_option         = getCommerceFeeOption();
$cms_delivery_chk_fee_list   = [];
$cms_delivery_chk_nuzam_list = [];
$cms_delivery_chk_ord_list   = [];
$cms_delivery_chk_list       = [];
$commerce_fee_list           = [];
foreach($total_all_date_list as $date_data)
{
    $cms_report_sql      = "
        SELECT 
            c_no,
            dp_c_no,
            dp_c_name,
            order_number,
            DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
            {$add_cms_column} as key_date, 
            unit_price,
            (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
            unit_delivery_price,
            (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
        FROM work_cms as w
        WHERE {$add_cms_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}')
    ";
    $cms_report_query = mysqli_query($my_db, $cms_report_sql);
    while($cms_report_result = mysqli_fetch_assoc($cms_report_query))
    {
        $commerce_fee_total_list    = isset($commerce_fee_option[$cms_report_result['c_no']]) ? $commerce_fee_option[$cms_report_result['c_no']] : [];
        $commerce_fee_per_list      = !empty($commerce_fee_total_list) && isset($commerce_fee_total_list[$cms_report_result['dp_c_no']]) ? $commerce_fee_total_list[$cms_report_result['dp_c_no']] : [];
        $commerce_fee_per           = 0;

        if(!empty($commerce_fee_per_list))
        {
            foreach($commerce_fee_per_list as $fee_date => $fee_val){
                if($cms_report_result['sales_date'] >= $fee_date){
                    $commerce_fee_per = $fee_val;
                    $commerce_fee_list[$cms_report_result['dp_c_no']] = $fee_val;
                }
            }
        }

        $cms_report_fee      = $cms_report_result['unit_price']*($commerce_fee_per/100);
        $total_price         = $cms_report_result['unit_price'] - $cms_report_fee;

        $cms_report_list[$cms_report_result['dp_c_no']][$cms_report_result['key_date']]['subtotal'] += $total_price;
        $cms_report_list[$cms_report_result['dp_c_no']][$cms_report_result['key_date']]['delivery'] += $cms_report_result['unit_delivery_price'];
        $cms_report_delivery_list[$cms_report_result['dp_c_no']] += $cms_report_result['unit_delivery_price'];

        if(in_array($sch_brand, $global_brand_option)){
            $cms_report_result['c_no'] = $sch_brand;
        }

        $sales_tmp_total_list[$cms_report_result['c_no']][$cms_report_result['key_date']][$cms_report_result['sales_date']] += $total_price;

        if(in_array($cms_report_result['dp_c_no'], $dp_self_imweb_list))
        {
            if($cms_report_result['dp_c_no'] == '1372'){
                $report_empty_check_list['imweb_base'] += $cms_report_result['coupon_price'];
                $coupon_imweb_report_total_list[$cms_report_result['key_date']]['belabef'] += $cms_report_result['coupon_price'];
            }elseif($cms_report_result['dp_c_no'] == '5800'){
                $report_empty_check_list['imweb_doc'] += $cms_report_result['coupon_price'];
                $coupon_imweb_report_total_list[$cms_report_result['key_date']]['doc'] += $cms_report_result['coupon_price'];
            }elseif($cms_report_result['dp_c_no'] == '5958'){
                $report_empty_check_list['imweb_nuzam'] += $cms_report_result['coupon_price'];
                $coupon_imweb_report_total_list[$cms_report_result['key_date']]['ilenol'] += $cms_report_result['coupon_price'];
            }elseif($cms_report_result['dp_c_no'] == '6012'){
                $report_empty_check_list['imweb_ilenol'] += $cms_report_result['coupon_price'];
                $coupon_imweb_report_total_list[$cms_report_result['key_date']]['nuzam'] += $cms_report_result['coupon_price'];
            }

            $coupon_imweb_report_total_list[$cms_report_result['key_date']]['total'] += $cms_report_result['coupon_price'];
            $cost_tmp_total_list[$cms_report_result['c_no']][$cms_report_result['key_date']][$cms_report_result['sales_date']] += $cms_report_result['coupon_price'];
        }

        if($cms_report_result['dp_c_no'] == '3295' || $cms_report_result['dp_c_no'] == '4629' || $cms_report_result['dp_c_no'] == '5427' || $cms_report_result['dp_c_no'] == '5588'){
            $report_empty_check_list['smart'] += $cms_report_result['coupon_price'];
            $coupon_smart_report_total_list[$cms_report_result['key_date']] += $cms_report_result['coupon_price'];
        }

        if($cms_report_result['sales_date'] >= 20240101)
        {
            if($cms_report_result['unit_delivery_price'] > 0) {
                $cms_delivery_chk_fee_list[$cms_report_result['order_number']] = 1;
            }
            elseif($cms_report_result['unit_delivery_price'] == 0 && $cms_report_result['unit_price'] > 0)
            {
                $cms_delivery_chk_list[$cms_report_result['order_number']][$cms_report_result['dp_c_no']][$cms_report_result['key_date']][$cms_report_result['sales_date']] = $cms_report_result['deli_cnt'];
                $cms_delivery_chk_ord_list[$cms_report_result['order_number']][$cms_report_result['c_no']] = $cms_report_result['c_no'];

                if($cms_report_result['c_no'] == '2827' || $cms_report_result['c_no'] == '4878'){
                    $cms_delivery_chk_nuzam_list[$cms_report_result['order_number']] = 1;
                }
            }
        }
    }
}

$cms_report_total_sum_list      = [];
$cms_report_subtotal_sum_list   = [];
$cms_report_delivery_sum_list   = [];
if(!empty($cms_report_list))
{
    foreach($cms_report_list as $dp_c_no => $cms_parent_report)
    {
        foreach($cms_parent_report as $key_date => $cms_report)
        {
            if(!isset($cms_report_total_sum_list[$key_date])){
                $cms_report_total_sum_list[$key_date] = 0;
            }

            if(!isset($cms_report_subtotal_sum_list[$key_date])){
                $cms_report_subtotal_sum_list[$key_date] = 0;
            }

            if(!isset($cms_report_delivery_sum_list[$key_date])){
                $cms_report_delivery_sum_list[$key_date] = 0;
            }

            $cms_report_total_sum_list[$key_date]     += ($cms_report['subtotal']+$cms_report['delivery']);
            $cms_report_subtotal_sum_list[$key_date]  += $cms_report['subtotal'];
            $cms_report_delivery_sum_list[$key_date]  += $cms_report['delivery'];
        }

        if($sch_not_empty && $cms_report_delivery_list[$dp_c_no] == 0) {
            unset($cms_report_delivery_list[$dp_c_no]);
        }
    }
}

# 무료 배송비 계산
foreach($cms_delivery_chk_list as $chk_ord => $chk_ord_data)
{
    if(isset($cms_delivery_chk_fee_list[$chk_ord])){
        continue;
    }

    $chk_delivery_price = isset($cms_delivery_chk_nuzam_list[$chk_ord]) ? 5000 : 3000;

    foreach ($chk_ord_data as $chk_dp_c_no => $chk_dp_data)
    {
        if(in_array($chk_dp_c_no, $global_dp_total_list)){
            $chk_delivery_price = 6000;
        }

        foreach ($chk_dp_data as $chk_key_date => $chk_key_data)
        {
            foreach ($chk_key_data as $chk_sales_date => $deli_cnt)
            {
                $cal_delivery_price = $chk_delivery_price * $deli_cnt;

                $cms_delivery_free_list[$chk_dp_c_no][$chk_key_date] += $cal_delivery_price;
                $cms_delivery_free_total_list[$chk_key_date] += $cal_delivery_price;
                $cms_delivery_free_chk_list[$chk_dp_c_no] += $deli_cnt;

                $chk_brand_deli_price   = $cal_delivery_price;
                $chk_brand_cnt          = count($cms_delivery_chk_ord_list[$chk_ord]);
                $cal_brand_deli_price   = round($cal_delivery_price/$chk_brand_cnt);
                $cal_brand_idx          = 1;
                foreach($cms_delivery_chk_ord_list[$chk_ord] as $chk_c_no)
                {
                    $cal_one_delivery_price  = $cal_brand_deli_price;
                    $chk_brand_deli_price   -= $cal_one_delivery_price;

                    if($cal_brand_idx == $chk_brand_cnt){
                        $cal_one_delivery_price += $chk_brand_deli_price;
                    }

                    $cost_tmp_total_list[$chk_c_no][$chk_key_date][$chk_sales_date] += $cal_one_delivery_price;

                    $cal_brand_idx++;
                }
            }
        }
    }
}

if(!empty($cms_delivery_free_chk_list))
{
    foreach($cms_delivery_free_chk_list as $free_chk => $free_chk_value){
        if($free_chk_value == 0){
            unset($cms_delivery_free_list[$free_chk]);
        }
    }
}

# 입/출고요청 리스트
$quick_report_sql      = "
    SELECT 
        c_no,
        run_c_no,
        run_c_name,
        order_number,
        DATE_FORMAT(run_date, '%Y%m%d') as sales_date,
        {$add_quick_column} as key_date, 
        unit_price
    FROM work_cms_quick as w
    WHERE {$add_quick_where}
";
$quick_report_query = mysqli_query($my_db, $quick_report_sql);
while($quick_report_result = mysqli_fetch_assoc($quick_report_query))
{
    if(in_array($sch_brand, $global_brand_option)){
        $quick_report_result['c_no'] = $sch_brand;
    }

    $quick_report_list[$quick_report_result['run_c_no']][$quick_report_result['key_date']] += $quick_report_result['unit_price'];
    $sales_tmp_total_list[$quick_report_result['c_no']][$quick_report_result['key_date']][$quick_report_result['sales_date']] += $quick_report_result['unit_price'];
    $cms_report_subtotal_sum_list[$quick_report_result['key_date']] += $quick_report_result['unit_price'];
    $cms_report_total_sum_list[$quick_report_result['key_date']]    += $quick_report_result['unit_price'];
}

# NOSP 계산
$nosp_result_sql = "
        SELECT
        `ar`.am_no,
        `ar`.brand,
        `am`.product,
        `am`.fee_per,
        `am`.price,
        `ar`.impressions,
        `ar`.click_cnt,
        `ar`.adv_price,
        DATE_FORMAT(am.adv_s_date, '%Y%m%d') as adv_s_day,
        {$add_result_column} as key_date
    FROM advertising_result `ar`
    LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
    WHERE {$add_result_where}
    ORDER BY `am`.adv_s_date ASC
";
$nosp_result_query       = mysqli_query($my_db, $nosp_result_sql);
$nosp_result_tmp_list   = [];
while($nosp_result = mysqli_fetch_assoc($nosp_result_query))
{
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["product"]          = $nosp_result['product'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["sales_date"]       = $nosp_result['adv_s_day'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["key_date"]         = $nosp_result['key_date'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["fee_per"]          = $nosp_result['fee_per'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_adv_price"] += $nosp_result['adv_price'];
}

foreach($nosp_result_tmp_list as $am_no => $nosp_result_tmp)
{
    foreach($nosp_result_tmp as $brand => $nosp_brand_data)
    {
        $fee_price      = $nosp_brand_data['total_adv_price']*($nosp_brand_data['fee_per']/100);
        $cal_price      = $nosp_brand_data["total_adv_price"]-$fee_price;
        $cal_price_vat  = $cal_price*1.1;

        if($nosp_brand_data['product'] == "1"){
            $report_empty_check_list['nosp_time'] += $cal_price_vat;
            $nosp_result_time_list[$nosp_brand_data['key_date']]    += $cal_price_vat;
        }elseif($nosp_brand_data['product'] == "2"){
            $report_empty_check_list['nosp_special'] += $cal_price_vat;
            $nosp_result_special_list[$nosp_brand_data['key_date']] += $cal_price_vat;
        }

        $cost_tmp_total_list[$brand][$nosp_brand_data['key_date']][$nosp_brand_data['sales_date']] += $cal_price_vat;
    }
}

# 예산지출비율 및 ROAS 계산
$net_sales_sum_list     = [];
$net_cost_sum_list      = [];
$expected_sales_list    = [];
$roas_percent_list      = [];

if(!empty($cost_report_total_sum_list) || !empty($sales_report_total_sum_list))
{
    foreach($cost_report_total_sum_list as $key_date => $cost_report_total)
    {
        $sales_total        = $sales_report_total_sum_list[$key_date]+$cms_report_subtotal_sum_list[$key_date];
        $cost_total         = $cost_report_total+$coupon_imweb_report_total_list[$key_date]["total"]+$cms_delivery_free_total_list[$key_date]+$nosp_result_special_list[$key_date]+$nosp_result_time_list[$key_date];
        $expected_sales     = ($sales_total > 0) ? ($cost_total/$sales_total)*100 : 0;
        $roas_percent       = ($cost_total > 0) ? ($sales_total/$cost_total)*100 : 0;

        $net_sales_sum_list[$key_date]  = $sales_total;
        $net_cost_sum_list[$key_date]   = $cost_total;
        $expected_sales_list[$key_date] = $expected_sales;
        $roas_percent_list[$key_date]   = $roas_percent;
    }
}

# NET 매출 관련 작업
foreach($brand_company_list as $c_no)
{
    $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$sch_net_date}' ORDER BY sales_date DESC LIMIT 1;";
    $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
    $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
    $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
    $net_parent             = isset($net_pre_percent_result['sales_date']) ? $net_pre_percent_result['sales_date'] : 0;

    $net_parent_percent_list[$c_no] = $net_pre_percent;

    $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date, {$add_date_column} as key_date FROM commerce_report_net WHERE brand='{$c_no}' {$add_date_where} ORDER BY sales_date ASC";
    $net_report_query       = mysqli_query($my_db, $net_report_sql);
    while($net_report = mysqli_fetch_assoc($net_report_query))
    {
        $net_report_date_list[$c_no][$net_report['key_date']][$net_report['sales_date']] = $net_report['percent'];
    }
}

$net_report_list            = [];
$net_report_cal_list        = [];
$net_report_profit_list     = [];
$net_report_profit_per_list = [];

foreach($net_report_date_list as $c_no => $net_company_data)
{
    $net_pre_percent = $net_parent_percent_list[$c_no];
    $parent_key      = $company_chk_list[$c_no];

    foreach($net_company_data as $key_date => $net_report_data)
    {
        if($net_report_data)
        {
            foreach($net_report_data as $sales_date => $net_percent_val)
            {
                if($net_percent_val > 0 ){
                    $net_pre_percent = $net_percent_val;
                }

                $net_percent = $net_pre_percent/100;
                $sales_total = round($sales_tmp_total_list[$c_no][$key_date][$sales_date],0);
                $cost_total  = $cost_tmp_total_list[$c_no][$key_date][$sales_date];
                $net_price   = $sales_total > 0 ? (int)($sales_total*$net_percent) : 0;
                $profit      = $cost_total > 0 ? ($net_price-$cost_total) : $net_price;

                $net_report_cal_list[$key_date]    += $net_price;
                $net_report_profit_list[$key_date] += $profit;

                $net_report_list[$key_date][$sales_date] = array('percent' => $net_pre_percent, 'parent' => ($net_percent_val > 0) ? 0 : $net_parent);
            }
        }else{
            $net_report_cal_list[$key_date]         += 0;
            $net_report_profit_list[$key_date]      += 0;
        }
    }
}

foreach($net_report_profit_list as $key_date => $net_profit)
{
    /**
        $cms_total_sum = round($cms_report_subtotal_sum_list[$key_date]);
        $cms_report_subtotal_sum_list[$key_date] = $cms_total_sum;
        if($cms_total_sum > 0){
            $net_report_profit_per_list[$key_date] = round($net_profit/$cms_total_sum * 100, 2);
        }else{
            $net_report_profit_per_list[$key_date] = 0;
        }
     */

    $sales_total_sum = $net_sales_sum_list[$key_date];

    if($sales_total_sum > 0){
        $net_report_profit_per_list[$key_date] = round($net_profit/$sales_total_sum * 100, 2);
    }else{
        $net_report_profit_per_list[$key_date] = 0;
    }
}

#메모 관련
$memo_report_sql    = "SELECT DATE_FORMAT(sales_date, '%Y%m%d') as sales_date, (SELECT k.k_parent FROM kind k WHERE k.k_name_code=main.k_name_code) AS k_parent, k_name_code, cm_no, memo FROM commerce_report_memo as main WHERE brand='{$sch_brand}' {$add_date_where} ORDER BY sales_date ASC";
$memo_report_query    = mysqli_query($my_db, $memo_report_sql);
while($memo_report = mysqli_fetch_assoc($memo_report_query))
{
    $memo_report_list[$memo_report['sales_date']][$memo_report['k_name_code']] = array('cm_no' => $memo_report['cm_no'], 'memo' => $memo_report['memo']);
    $k_parent = "0".$memo_report['k_parent'];
    $memo_report_total_list[$memo_report['sales_date']][$k_parent] = $k_parent;
}

# 이슈게시판
$board_issue_list   = [];
if(!empty($add_board_where)){
    $board_date         = date('Y-m-d', strtotime("-7 days"))." 00:00:00";
    $board_issue_sql    = "
        SELECT 
            *, 
           (SELECT count(br.r_no) FROM board_read br WHERE br.read_s_no='{$session_s_no}' AND br.b_no = bn.`no` AND br.b_id='cms_report') as read_cnt 
        FROM board_normal as bn 
        WHERE {$add_board_where} AND b_id='cms_report' AND regdate >= '{$board_date}' 
        ORDER BY regdate DESC
    ";
    $board_issue_query  = mysqli_query($my_db, $board_issue_sql);
    while($board_issue = mysqli_fetch_assoc($board_issue_query))
    {
        $board_issue['issue_date']  = date("Y/m/d", strtotime($board_issue['regdate']));
        $board_issue_list[]         = $board_issue;
    }
}

$smarty->assign("sch_cms_brand_url", $sch_cms_brand_url);
$smarty->assign('commerce_date_list', $commerce_date_list);
$smarty->assign('commerce_date_count', count($commerce_date_list));
$smarty->assign('cost_report_list', $cost_report_list);
$smarty->assign('cost_report_total_list', $cost_report_total_list);
$smarty->assign('cost_report_total_sum_list', $cost_report_total_sum_list);
$smarty->assign('cms_report_list', $cms_report_list);
$smarty->assign('cms_report_delivery_list', $cms_report_delivery_list);
$smarty->assign('cms_delivery_free_cnt', count($cms_delivery_free_list)+1);
$smarty->assign('cms_delivery_free_list', $cms_delivery_free_list);
$smarty->assign('cms_delivery_free_total_list', $cms_delivery_free_total_list);
$smarty->assign('cms_report_title_list', $cms_report_title_list);
$smarty->assign('cms_report_total_sum_list', $cms_report_total_sum_list);
$smarty->assign('cms_report_subtotal_sum_list', $cms_report_subtotal_sum_list);
$smarty->assign('cms_report_delivery_sum_list', $cms_report_delivery_sum_list);
$smarty->assign('coupon_imweb_report_total_list', $coupon_imweb_report_total_list);
$smarty->assign('coupon_smart_report_total_list', $coupon_smart_report_total_list);
$smarty->assign('sales_report_list', $sales_report_list);
$smarty->assign('sales_report_total_list', $sales_report_total_list);
$smarty->assign('sales_report_total_sum_list', $sales_report_total_sum_list);
$smarty->assign('net_sales_sum_list', $net_sales_sum_list);
$smarty->assign('net_cost_sum_list', $net_cost_sum_list);
$smarty->assign('expected_sales_list', $expected_sales_list);
$smarty->assign('roas_percent_list', $roas_percent_list);
$smarty->assign('net_report_list', $net_report_list);
$smarty->assign('net_report_cal_list', $net_report_cal_list);
$smarty->assign('net_report_profit_list', $net_report_profit_list);
$smarty->assign('net_report_profit_per_list', $net_report_profit_per_list);
$smarty->assign('memo_report_list', $memo_report_list);
$smarty->assign('memo_report_total_list', $memo_report_total_list);
$smarty->assign('comm_g1_title_list', $comm_g1_title_list);
$smarty->assign('comm_g2_title_list', $comm_g2_title_list);
$smarty->assign('commerce_fee_list', $commerce_fee_list);
$smarty->assign('mc_today', date('Y-m-d'));
$smarty->assign('report_empty_check_list', $report_empty_check_list);
$smarty->assign('nosp_result_time_list', $nosp_result_time_list);
$smarty->assign('nosp_result_special_list', $nosp_result_special_list);
$smarty->assign('quick_report_title_list', $quick_report_title_list);
$smarty->assign('quick_report_list', $quick_report_list);
$smarty->assign('board_issue_list', $board_issue_list);
$smarty->assign('cal_event_list', $cal_event_list);

$smarty->display('media_commerce_auto_report.html');
?>