<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_sales.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/ProductCms.php');
require('inc/model/Kind.php');
require('inc/model/WorkCmsSettle.php');

# 프로세스 처리
$process = (isset($_POST['process']))?$_POST['process']:"";

if($process == "new_vat")
{
    $vat_model      = WorkCmsSettle::Factory();
    $vat_model->setMainInit("work_cms_vat", "no");

    $company_model      = Company::Factory();
    $dp_company_option  = $company_model->getDpList();

    $prev_month         = date('Y-m', strtotime("-1 months"));
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $new_vat_month      = isset($_POST['new_vat_month']) ? $_POST['new_vat_month'] : $prev_month;
    $new_vat_dp_c_no    = isset($_POST['new_vat_dp_c_no']) ? $_POST['new_vat_dp_c_no'] : "";
    $new_vat_date       = isset($_POST['new_vat_date']) ? $_POST['new_vat_date'] : "";
    $new_cancel_date    = isset($_POST['new_cancel_date']) ? $_POST['new_cancel_date'] : "";
    $new_order_number   = isset($_POST['new_order_number']) ? $_POST['new_order_number'] : "";
    $new_c_no           = isset($_POST['new_c_no']) ? $_POST['new_c_no'] : "";
    $new_c_name         = isset($_POST['new_c_name']) ? $_POST['new_c_name'] : "";
    $new_card_price     = isset($_POST['new_card_price']) ? $_POST['new_card_price'] : "0";
    $new_cash_price     = isset($_POST['new_cash_price']) ? $_POST['new_cash_price'] : "0";
    $new_etc_price      = isset($_POST['new_etc_price']) ? $_POST['new_etc_price'] : "0";
    $new_tax_price      = isset($_POST['new_tax_price']) ? $_POST['new_tax_price'] : "0";
    $new_notice         = isset($_POST['new_notice']) ? addslashes(trim($_POST['new_tax_price'])) : "";
    $cur_date           = date("Y-m-d H:i:s");

    $vat_data   = array(
        "vat_type"      => "99",
        "vat_month"     => $new_vat_month,
        "vat_dp_c_no"   => $new_vat_dp_c_no,
        "vat_dp_c_name" => !empty($new_vat_dp_c_no) ? $dp_company_option[$new_vat_dp_c_no] : "",
        "vat_date"      => !empty($new_vat_date) ? $new_vat_date : "NULL",
        "cancel_date"   => !empty($new_cancel_date) ? $new_cancel_date : "NULL",
        "total_ord_no"  => $new_order_number,
        "order_number"  => $new_order_number,
        "c_no"          => $new_c_no,
        "c_name"        => $new_c_name,
        "card_price"    => $new_card_price,
        "cash_price"    => $new_cash_price,
        "etc_price"     => $new_etc_price,
        "tax_price"     => $new_tax_price,
        "is_return"     => 2,
        "reg_s_no"      => $session_s_no,
        "reg_team"      => $session_team,
        "notice"        => $new_notice,
        "regdate"       => $cur_date
    );

    if($vat_model->insert($vat_data)){
        exit("<script>alert('등록 성공했습니다');location.href='work_cms_sales_vat_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('등록 실패했습니다');location.href='work_cms_sales_vat_list.php?{$search_url}';</script>");
    }
}
elseif($process == "del_vat")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $del_vat_mon        = isset($_POST['del_vat_month']) ? $_POST['del_vat_month'] : "";
    $del_vat_type       = isset($_POST['del_vat_type']) ? $_POST['del_vat_type'] : "";
    $del_vat_dp_c_no    = isset($_POST['del_vat_dp_c_no']) ? $_POST['del_vat_dp_c_no'] : "";

    if(empty($del_vat_mon)){
        exit("<script>alert('선택된 부가세월이 없습니다. 다시 시도해주세요.');location.href='work_cms_sales_vat_list.php?{$search_url}';</script>");
    }

    if(empty($del_vat_type) && empty($del_vat_dp_c_no)){
        exit("<script>alert('선택된 엑셀양식 및 판매처가 없습니다. 다시 시도해주세요.');location.href='work_cms_sales_vat_list.php?{$search_url}';</script>");
    }

    $del_vat_where = "vat_month = '{$del_vat_mon}'";

    if(!empty($del_vat_type)){
        $del_vat_where .= " AND vat_type='{$del_vat_type}'";
    }

    if(!empty($del_vat_dp_c_no)){
        $del_vat_where .= " AND vat_dp_c_no='{$del_vat_dp_c_no}'";
    }

    $del_sql = "DELETE FROM work_cms_vat WHERE {$del_vat_where}";

    if(!mysqli_query($my_db, $del_sql)){
        exit("<script>alert('삭제에 실패했습니다');location.ref='work_cms_sales_vat_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제했습니다');location.href='work_cms_sales_vat_list.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "158";
$nav_title   = "부가세 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수
$company_model              = Company::Factory();
$sch_dp_company_option      = $company_model->getDpDisplayList();
$vat_company_option         = $company_model->getDpList();
$product_model              = ProductCms::Factory();
$brand_option               = $product_model->getPrdBrandOption();
$brand_option[9999]         = "혼합";
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$sch_brand_total_list       = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

# 검색조건
$add_where          = "1=1";
$current_month      = date('Y-m');
$prev_month         = date('Y-m', strtotime("-1 months"));
$sch_vat_month      = isset($_GET['sch_vat_month']) ? $_GET['sch_vat_month'] : $prev_month;
$sch_vat_type       = isset($_GET['sch_vat_type']) ? $_GET['sch_vat_type'] : "";
$sch_vat_dp_c_no    = isset($_GET['sch_vat_dp_c_no']) ? $_GET['sch_vat_dp_c_no'] : "";
$sch_is_cancel      = isset($_GET['sch_is_cancel']) ? $_GET['sch_is_cancel'] : "";
$sch_ord_no         = isset($_GET['sch_ord_no']) ? $_GET['sch_ord_no'] : "";
$sch_is_return      = isset($_GET['sch_is_return']) ? $_GET['sch_is_return'] : "";
$sch_is_order       = isset($_GET['sch_is_order']) ? $_GET['sch_is_order'] : "";
$sch_notice         = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `wcv`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `wcv`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where         .= " AND `wcv`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

if(!empty($sch_vat_month)) {
    $add_where .= " AND `wcv`.vat_month = '{$sch_vat_month}'";
    $smarty->assign('sch_vat_month', $sch_vat_month);
}

if(!empty($sch_vat_type)) {
    $add_where .= " AND `wcv`.vat_type = '{$sch_vat_type}'";
    $smarty->assign('sch_vat_type', $sch_vat_type);
}

if(!empty($sch_vat_dp_c_no)) {
    $add_where .= " AND `wcv`.vat_dp_c_no = '{$sch_vat_dp_c_no}'";
    $smarty->assign('sch_vat_dp_c_no', $sch_vat_dp_c_no);
}

if(!empty($sch_is_cancel)) {
    if($sch_is_cancel == '1'){
        $add_where   .= " AND `wcv`.cancel_date > '1970-01-01'";
    }elseif($sch_is_cancel == '2'){
        $add_where   .= " AND (`wcv`.cancel_date IS NULL OR `wcv`.cancel_date = '0000-00-00')";
    }
    $smarty->assign('sch_is_cancel', $sch_is_cancel);
}

if(!empty($sch_ord_no)) {
    $add_where   .= " AND (`wcv`.order_number = '{$sch_ord_no}' OR `wcv`.total_ord_no ='{$sch_ord_no}')";
    $smarty->assign('sch_ord_no', $sch_ord_no);
}

if(!empty($sch_is_return)) {
    $add_where   .= " AND is_return = '{$sch_is_return}'";
    $smarty->assign('sch_is_return', $sch_is_return);
}

if(!empty($sch_is_order)){
    if($sch_is_order == '1'){
        $add_where   .= " AND wcv.c_no > 0";
    }elseif($sch_is_order == '2'){
        $add_where   .= " AND (wcv.c_no = 0 OR wcv.c_no IS null)";
    }
    $smarty->assign('sch_is_order', $sch_is_order);
}

if(!empty($sch_notice)) {
    $add_where .= " AND `wcv`.notice LIKE '%{$sch_notice}%'";
    $smarty->assign('sch_notice', $sch_notice);
}

# 페이징 처리
$sales_vat_total_sql	    = "SELECT count(`no`) as cnt, SUM(card_price) as total_card_price, SUM(cash_price) as total_cash_price, SUM(etc_price) as total_etc_price, SUM(tax_price) as total_tax_price FROM work_cms_vat `wcv` WHERE {$add_where}";
$sales_vat_total_query      = mysqli_query($my_db, $sales_vat_total_sql);
$sales_vat_total_result     = mysqli_fetch_array($sales_vat_total_query);
$sales_vat_total_list       = array(
    "total_price"           => ($sales_vat_total_result['total_card_price'] + $sales_vat_total_result['total_cash_price'] + $sales_vat_total_result['total_etc_price'] + $sales_vat_total_result['total_tax_price']),
    "total_card_price"      => $sales_vat_total_result['total_card_price'],
    "total_cash_price"      => $sales_vat_total_result['total_cash_price'],
    "total_etc_price"       => $sales_vat_total_result['total_etc_price'],
    "total_tax_price"       => $sales_vat_total_result['total_tax_price']
);
$sales_vat_total 	        = $sales_vat_total_result['cnt'];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($sales_vat_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "work_cms_sales_vat_list.php", $page_num, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $sales_vat_total);
$smarty->assign("pagelist", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$sales_vat_sql = "
    SELECT 
        *,
        DATE_FORMAT(`wcv`.regdate, '%Y-%m-%d') as reg_day,
        (SELECT s.s_name FROM staff s WHERE s.s_no=wcv.reg_s_no) as reg_s_name,
        (SELECT COUNT(w_no) FROM work_cms w WHERE w.order_number=`wcv`.order_number) as ord_cnt,
        (SELECT COUNT(w_no) FROM work_cms w WHERE w.origin_ord_no=`wcv`.order_number) as org_cnt
    FROM work_cms_vat `wcv`
    WHERE {$add_where}
    ORDER BY `no` DESC 
    LIMIT {$offset}, {$num}
";
$sales_vat_query    = mysqli_query($my_db, $sales_vat_sql);
$sales_vat_list     = [];
while($sales_vat = mysqli_fetch_assoc($sales_vat_query))
{
    $sales_vat['total_price'] = $sales_vat['card_price']+$sales_vat['cash_price']+$sales_vat['etc_price']+$sales_vat['tax_price'];

    $sales_vat_list[] = $sales_vat;
}

$smarty->assign('current_month', $prev_month);
$smarty->assign("vat_type_option", getVatTypeOption());
$smarty->assign("sch_dp_company_option", $sch_dp_company_option);
$smarty->assign("vat_company_option", $vat_company_option);
$smarty->assign("vat_brand_option", $brand_option);
$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("is_order_option", getIsOrderOption());
$smarty->assign('page_type_option', getPageTypeOption(4));
$smarty->assign("sales_vat_total_list", $sales_vat_total_list);
$smarty->assign("sales_vat_list", $sales_vat_list);

$smarty->display('work_cms_sales_vat_list.html');
?>
