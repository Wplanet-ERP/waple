<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');

if(!permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자")){
    $smarty->display('access_error.html');
    exit;
}

$keyword_no = isset($_GET['keyword_no']) ? $_GET['keyword_no'] : "";
$strength	= [];
$title		= "등록";

$process    = isset($_POST['process']) ? $_POST['process'] : "";
$regist_url = "staff_strength_regist.php";
$list_url   = "staff_strength_list.php";

if($process == 'new_strength')
{
	$keyword  = isset($_POST['keyword']) ? $_POST['keyword'] : "";
	$strength = isset($_POST['strength']) ? addslashes($_POST['strength']) : "";

	if(!$keyword || !$strength)
	{
		exit("<script>alert('데이터가 없습니다');location.href='{$regist_url}';</script>");
	}
	
	$ins_sql = "INSERT INTO staff_strength_keyword SET `keyword` = '{$keyword}', `strength` = '{$strength}'";
	
	if($my_db->query($ins_sql))
	{
		exit("<script>alert('강점 키워드 등록에 성공했습니다');location.href='{$regist_url}?keyword_no={$my_db->insert_id}';</script>");
	}else{
		exit("<script>alert('강점 키워드 등록에 실패했습니다');location.href='{$regist_url}';</script>");
	}
}
elseif($process == 'modify_strength')
{
	$k_no     = isset($_POST['keyword_no']) ? $_POST['keyword_no'] : "";
	$keyword  = isset($_POST['keyword']) ? $_POST['keyword'] : "";
	$strength = isset($_POST['strength']) ? addslashes($_POST['strength']) : "";

	if(!$keyword || !$strength)
	{
		exit("<script>alert('데이터가 없습니다');location.href='{$regist_url}';</script>");
	}

	$upd_sql = "UPDATE staff_strength_keyword SET `keyword` = '{$keyword}', `strength` = '{$strength}' WHERE k_no='{$k_no}'";

	if($my_db->query($upd_sql))
	{
		$upd_keyword_sql = "UPDATE staff_strength SET `keyword`='{$keyword}' WHERE keyword_no='{$k_no}'";
		
		if($my_db->query($upd_keyword_sql)){
			exit("<script>alert('강점 키워드 수정에 성공했습니다');location.href='{$list_url}';</script>");
		}else{
			exit("<script>alert('강점 키워드 회원정보 수정에 실패했습니다');location.href='{$regist_url}?keyword_no={$k_no}';</script>");
		}
		
	}else{
		exit("<script>alert('강점 키워드 수정에 실패했습니다');location.href='{$regist_url}?keyword_no={$k_no}';</script>");
	}
}
elseif($keyword_no)
{
	$strength_sql 	= "SELECT * FROM staff_strength_keyword WHERE k_no = {$keyword_no} LIMIT 1";
	$strength_query = mysqli_query($my_db, $strength_sql);
	$strength 		= mysqli_fetch_assoc($strength_query);

	if(!!$strength)
	{
		$title = "수정";
	}
}


$smarty->assign("title", $title);
$smarty->assign($strength);
$smarty->display('staff_strength_regist.html');

?>
