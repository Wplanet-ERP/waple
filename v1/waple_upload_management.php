<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/work_sales.php');
require('inc/helper/wise_csm.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Custom.php');

# Init Model
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management", "file_no");

$process = isset($_POST['process']) ? $_POST['process'] : "";
if($process == "del_settle_data")
{
    $chk_file_no    = isset($_POST['chk_file_no']) ? $_POST['chk_file_no'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(empty($chk_file_no)){
        exit("<script>alert('삭제에 실패했습니다. 파일 번호가 없습니다.');location.href='waple_upload_management.php?{$search_url}';</script>");
    }

    $upload_item    = $upload_model->getItem($chk_file_no);
    $del_settle_sql = "DELETE FROM work_cms_settlement WHERE file_no='{$chk_file_no}'";
    $del_upload_sql = "DELETE FROM upload_management WHERE file_no='{$chk_file_no}'";

    if(mysqli_query($my_db, $del_upload_sql) && mysqli_query($my_db, $del_settle_sql)){
        del_file($upload_item['file_path']);
        exit("<script>alert('삭제했습니다');location.href='waple_upload_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제에 실패했습니다');location.href='waple_upload_management.php?{$search_url}';</script>");
    }
}
elseif($process == "del_empty_data")
{
    $chk_file_no    = isset($_POST['chk_file_no']) ? $_POST['chk_file_no'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(empty($chk_file_no)){
        exit("<script>alert('삭제에 실패했습니다. 파일 번호가 없습니다.');location.href='waple_upload_management.php?{$search_url}';</script>");
    }

    $upload_item = $upload_model->getItem($chk_file_no);

    if($upload_model->delete($chk_file_no)){
        del_file($upload_item['file_path']);
        exit("<script>alert('삭제했습니다');location.href='waple_upload_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제에 실패했습니다');location.href='waple_upload_management.php?{$search_url}';</script>");
    }
}
elseif($process == "f_notice")
{
    $file_no    = isset($_POST['file_no']) ? $_POST['file_no'] : "";
    $notice     = isset($_POST['val']) ? addslashes(trim($_POST['val'])) : "";

    if(!$upload_model->update(array("file_no" => $file_no, "notice" => $notice))){
        echo "비고 저장에 실패했습니다.";
    }else{
        echo "비고를 저장했습니다.";
    }
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "185";
$nav_title   = "와플 업로드 파일 관리";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색 처리
$add_where          = "1=1";
$sch_file_no        = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";
$sch_upload_type    = isset($_GET['sch_upload_type']) ? $_GET['sch_upload_type'] : "";
$sch_reg_name       = isset($_GET['sch_reg_name']) ? $_GET['sch_reg_name'] : "";
$sch_notice         = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";

if(!empty($sch_file_no)){
    $add_where .= " AND um.file_no='{$sch_file_no}'";
    $smarty->assign("sch_file_no", $sch_file_no);
}

if(!empty($sch_upload_type)){
    $add_where .= " AND um.upload_type='{$sch_upload_type}'";
    $smarty->assign("sch_upload_type", $sch_upload_type);
}

if(!empty($sch_reg_name)){
    $add_where .= " AND um.reg_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_reg_name}%')";
    $smarty->assign("sch_reg_name", $sch_reg_name);
}

if(!empty($sch_notice)){
    $add_where .= " AND um.notice LIKE '%{$sch_notice}%'";
    $smarty->assign("sch_notice", $sch_notice);
}

# 페이징 처리
$waple_upload_total_sql     = "SELECT count(`file_no`) as cnt FROM upload_management `um` WHERE {$add_where}";
$waple_upload_total_query   = mysqli_query($my_db, $waple_upload_total_sql);
$waple_upload_total_result  = mysqli_fetch_array($waple_upload_total_query);
$waple_upload_total         = $waple_upload_total_result['cnt'];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($waple_upload_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "waple_upload_management.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $waple_upload_total);
$smarty->assign("pagelist", $page_list);
$smarty->assign("ord_page_type", $page_type);

$company_model              = Company::Factory();
$settle_company_option      = $company_model->getDpList();
$log_company_option         = $company_model->getLogisticsList();
$excel_upload_type_option   = getExcelUploadTypeOption();
$settle_type_option         = getSettleTypeOption();
$return_file_type_option    = getReturnExcelOption();

# 리스트 쿼리
$waple_upload_sql = "
    SELECT 
        *,
        DATE_FORMAT(regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(regdate, '%H:%i') as reg_time,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`um`.reg_s_no) AS reg_s_name
    FROM upload_management `um`
    WHERE {$add_where}
    ORDER BY `file_no` DESC 
    LIMIT {$offset}, {$num}
";
$waple_upload_query         = mysqli_query($my_db, $waple_upload_sql);
$waple_upload_list          = [];
while($waple_upload = mysqli_fetch_assoc($waple_upload_query))
{
    $upload_type_data = $excel_upload_type_option[$waple_upload['upload_type']];

    $waple_upload['upload_type_name']   = $upload_type_data['title'];
    $waple_upload['upload_explain']     = $upload_type_data['explain'];
    $waple_upload['upload_page_url']    = $upload_type_data['page'];
    $waple_upload['upload_page_name']   = $upload_type_data['page_title'];
    $waple_upload['upload_search_url']  = $upload_type_data['search_url'];

    $upload_set     = "";
    $chk_upload_sql = "";
    $file_no        = $waple_upload['file_no'];

    switch ($waple_upload['upload_type']){
        case '1':
            $upload_set     = "기준일 : {$waple_upload['upload_date']}<br/>물류창고 업체명 : {$log_company_option[$waple_upload['upload_kind']]}";
            $chk_upload_sql = "SELECT COUNT(`no`) AS chk_cnt FROM product_cms_stock WHERE file_no='{$file_no}'";
            break;
        case '2':
            $upload_set     = "물류창고 업체명 : {$log_company_option[$waple_upload['upload_kind']]}";
            $chk_upload_sql = "SELECT COUNT(`no`) AS chk_cnt FROM product_cms_stock_report WHERE file_no='{$file_no}'";
            break;
        case '3':
        case '4':
            $waple_upload['upload_search_url'] .= ($waple_upload['upload_kind'] == 24) ? "&sch_settle_type=23" : "&sch_settle_type={$waple_upload['upload_kind']}";
            $upload_set     = "정산월 : {$waple_upload['upload_date']}<br/>엑셀양식 : {$settle_type_option[$waple_upload['upload_kind']]}";
            $chk_upload_sql = "SELECT COUNT(`no`) AS chk_cnt FROM work_cms_settlement WHERE FIND_IN_SET('{$file_no}',file_no) > 0";
            break;
        case '5':
            $upload_set     = "업무완료일 : {$waple_upload['upload_date']}";
            $chk_upload_sql = "SELECT COUNT(`w_no`) AS chk_cnt FROM `work` WHERE file_no='{$file_no}'";
            break;
        case '6':
            $upload_set     = "업로드 타입 : ";
            if($waple_upload['upload_kind'] == "1"){
                $upload_set .= "주문";
            }elseif($waple_upload['upload_kind'] == "2"){
                $upload_set .= "교환";
            }elseif($waple_upload['upload_kind'] == "3"){
                $upload_set .= "반품";
            }
            $chk_upload_sql = "SELECT COUNT(`w_no`) AS chk_cnt FROM `work_cms` WHERE file_no='{$file_no}'";
            break;
        case '7':
            $upload_company = ($waple_upload['upload_kind'] == '5659') ? "스타트투데이_아이레놀" : "스타트투데이_닥터피엘";
            $upload_set     = "업로드 타입 : {$upload_company}";
            $chk_upload_sql = "SELECT COUNT(`w_no`) AS chk_cnt FROM `work_cms` WHERE file_no='{$file_no}'";
            break;
        case '8':
            $chk_upload_sql = "SELECT COUNT(*) AS chk_cnt FROM `work_cms_coupon` WHERE file_no='{$file_no}'";
            break;
        case '9':
            $upload_set     = "정산월 : {$waple_upload['upload_date']}";
            $chk_upload_sql = "SELECT COUNT(`no`) AS chk_cnt FROM work_cms_settlement WHERE FIND_IN_SET('{$file_no}',file_no) > 0";
            break;
        case '10':
            $upload_set = "엑셀양식 : {$return_file_type_option[$waple_upload['upload_kind']]}";
            $chk_upload_sql = "SELECT COUNT(*) AS chk_cnt FROM `csm_return` WHERE file_no='{$file_no}'";
            break;
        case '11':
            $upload_set     = "정산월 : {$waple_upload['upload_date']}<br/>물류창고 업체명 : {$log_company_option[$waple_upload['upload_kind']]}";
            $chk_upload_sql = "SELECT COUNT(`no`) AS chk_cnt FROM `product_cms_stock_report` WHERE file_no='{$file_no}'";
            break;
        case '12':
            $chk_upload_sql = "SELECT COUNT(`no`) AS chk_cnt FROM `product_cms_stock_transfer` WHERE file_no='{$file_no}'";
            break;
        case '13':
            $chk_upload_sql = "SELECT COUNT(`w_no`) AS chk_cnt FROM `work_cms_direct` WHERE file_no='{$file_no}'";
            break;
    }

    $is_empty = false;
    if(!empty($chk_upload_sql)){
        $chk_upload_query   = mysqli_query($my_db, $chk_upload_sql);
        $chk_upload_result  = mysqli_fetch_assoc($chk_upload_query);
        $chk_upload_cnt     = isset($chk_upload_result['chk_cnt']) ? $chk_upload_result['chk_cnt'] : 0;
        if($chk_upload_cnt == 0){
            $is_empty = true;
        }
    }

    $waple_upload['is_empty']   = $is_empty;
    $waple_upload['upload_set'] = $upload_set;

    $file_origin = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$waple_upload['file_path']}";
    if(file_exists($file_origin)) {
        $file_size = filesize($file_origin) / 1024;
        $file_size = floor($file_size);
        $waple_upload['file_size'] = $file_size." KB";
    }
    $waple_upload_list[] = $waple_upload;
}

$smarty->assign('cur_date', date("Y-m-d"));
$smarty->assign('current_month', date("Y-m"));
$smarty->assign('page_type_option', getPageTypeOption(4));
$smarty->assign('excel_upload_type_option', $excel_upload_type_option);
$smarty->assign('json_excel_upload_type_option', json_encode($excel_upload_type_option));
$smarty->assign('settle_type_option', $settle_type_option);
$smarty->assign('return_file_type_option', $return_file_type_option);
$smarty->assign('settle_company_option', $settle_company_option);
$smarty->assign('waple_upload_list', $waple_upload_list);

$smarty->display('waple_upload_management.html');
?>
