<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/manufacturer.php');
require('inc/model/MyQuick.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/Manufacturer.php');

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$manu_model     = Manufacturer::Factory();

if($process == "f_work_state")
{
    $mi_no      = isset($_POST['mi_no']) ? $_POST['mi_no'] : "";
    $val        = isset($_POST['val']) ? $_POST['val'] : "";
    $upd_data   = array("mi_no" => $mi_no, "work_state" => $val);

    if(!$manu_model->update($upd_data)){
        echo "진행상태 변경에 실패했습니다.";
    }else{
        echo "진행상태를 변경했습니다.";
    }
    exit;
}
elseif($process == "f_run_state")
{
    $mi_no      = isset($_POST['mi_no']) ? $_POST['mi_no'] : "";
    $val        = isset($_POST['val']) ? $_POST['val'] : "";
    $upd_data   = array("mi_no" => $mi_no, "run_state" => $val, "run_s_no" => $session_s_no, "run_team" => $session_team);

    if($val == '4'){
        $upd_data['run_date'] = date("Y-m-d");
    }elseif($val == '0'){
        $upd_data = array("mi_no" => $mi_no, "run_state" => "0", "run_s_no" => "NULL", "run_team" => "NULL", "run_date" => "NULL");
    }

    if(!$manu_model->update($upd_data)){
        echo "확인상태 변경에 실패했습니다.";
    }else{
        echo "확인상태를 변경했습니다.";
    }
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "163";
$nav_title   = "제조사 검수 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색처리
$add_where          = "1=1";
$sch_mi_no          = isset($_GET['sch_mi_no']) ? $_GET['sch_mi_no'] : "";
$sch_work_state     = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_parent_ord_no  = isset($_GET['sch_parent_ord_no']) ? $_GET['sch_parent_ord_no'] : "";
$sch_stock_date     = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : "";
$sch_recipient      = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp   = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_sup_c_no       = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_option         = isset($_GET['sch_option']) ? $_GET['sch_option'] : "";
$sch_title          = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_is_improve     = isset($_GET['sch_is_improve']) ? $_GET['sch_is_improve'] : "";
$sch_run_state      = isset($_GET['sch_run_state']) ? $_GET['sch_run_state'] : "";
$sch_is_as_in       = isset($_GET['sch_is_as_in']) ? $_GET['sch_is_as_in'] : "";
$sch_is_as_out      = isset($_GET['sch_is_as_out']) ? $_GET['sch_is_as_out'] : "";

if(!empty($sch_mi_no)) {
    $add_where .= " AND mi.mi_no='{$sch_mi_no}'";
    $smarty->assign("sch_mi_no", $sch_mi_no);
}

if(!empty($sch_work_state)) {
    $add_where .= " AND mi.work_state='{$sch_work_state}'";
    $smarty->assign("sch_work_state", $sch_work_state);
}

if(!empty($sch_parent_ord_no)) {
    $add_where .= " AND mi.parent_order_number LIKE '%{$sch_parent_ord_no}%'";
    $smarty->assign("sch_parent_ord_no", $sch_parent_ord_no);
}

if(!empty($sch_stock_date)) {
    $add_where .= " AND mi.stock_date = '{$sch_stock_date}'";
    $smarty->assign("sch_stock_date", $sch_stock_date);
}

if(!empty($sch_recipient)) {
    $add_where .= " AND mi.recipient LIKE '%{$sch_recipient}%'";
    $smarty->assign("sch_recipient", $sch_recipient);
}

if(!empty($sch_recipient_hp)) {
    $add_where .= " AND mi.recipient_hp LIKE '%{$sch_recipient_hp}%'";
    $smarty->assign("sch_recipient_hp", $sch_recipient_hp);
}

if(!empty($sch_sup_c_no)) {
    $add_where .= " AND mi.mi_no IN(SELECT DISTINCT sub.mi_no FROM manufacturer_inspection_unit sub WHERE sub.sup_c_no ='{$sch_sup_c_no}')";
    $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
}

if(!empty($sch_option)) {
    $add_where .= " AND mi.mi_no IN(SELECT DISTINCT sub.mi_no FROM manufacturer_inspection_unit sub WHERE sub.sku LIKE '%{$sch_option}%')";
    $smarty->assign("sch_option", $sch_option);
}

if(!empty($sch_title)) {
    $add_where .= " AND mi.ins_title LIKE '%{$sch_title}%'";
    $smarty->assign("sch_title", $sch_title);
}

if(!empty($sch_is_improve)) {
    $add_where .= " AND mi.is_improve='{$sch_is_improve}'";
    $smarty->assign("sch_is_improve", $sch_is_improve);
}

if(!empty($sch_run_state)) {
    $add_where .= " AND mi.run_state='{$sch_run_state}'";
    $smarty->assign("sch_run_state", $sch_run_state);
}

if(!empty($sch_is_as_in)) {
    $add_where .= " AND mi.is_as_in='{$sch_is_as_in}'";
    $smarty->assign("sch_is_as_in", $sch_is_as_in);
}

if(!empty($sch_is_as_out)) {
    $add_where .= " AND mi.is_as_out='{$sch_is_as_out}'";
    $smarty->assign("sch_is_as_out", $sch_is_as_out);
}

# 페이징처리
$return_mi_total_sql    = "SELECT COUNT(mi_no) as cnt FROM manufacturer_inspection mi WHERE {$add_where}";
$return_mi_total_query  = mysqli_query($my_db, $return_mi_total_sql);
$return_mi_total_result = mysqli_fetch_assoc($return_mi_total_query);
$return_mi_total        = isset($return_mi_total_result['cnt']) ? $return_mi_total_result['cnt'] : 0;

$page       = isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 20;
$num 		= $page_type;
$offset 	= ($page-1) * $num;
$pagenum 	= ceil($return_mi_total/$num);

if ($page >= $pagenum){$page = $pagenum;}
if ($page <= 0){$page = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($page, "work_cms_return_mi_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $return_mi_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$return_mi_sql = "
    SELECT
        *,
        DATE_FORMAT(mi.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(mi.regdate, '%H:%i') as reg_hour,
        (SELECT sub.c_name FROM company sub WHERE sub.c_no=mi.add_c_no) as add_c_name,
        (SELECT sub.s_name FROM staff sub WHERE sub.s_no=mi.reg_s_no) as reg_s_name,
        (SELECT sub.s_name FROM staff sub WHERE sub.s_no=mi.run_s_no) as run_s_name
    FROM manufacturer_inspection mi
    WHERE {$add_where}
    ORDER BY mi_no DESC 
    LIMIT {$offset}, {$num}
";
$return_mi_query    = mysqli_query($my_db, $return_mi_sql);
$return_mi_list     = [];
$run_state_option   = getMiRunStateOption();
$is_improve_option  = getIsImproveOption();
while($return_mi = mysqli_fetch_assoc($return_mi_query))
{
    if (isset($return_mi['recipient_hp']) && !empty($return_mi['recipient_hp'])) {
        $f_hp = substr($return_mi['recipient_hp'], 0, 4);
        $e_hp = substr($return_mi['recipient_hp'], 7, 15);
        $return_mi['recipient_sc_hp'] = $f_hp . "***" . $e_hp;
    }

    $return_mi['is_improve_name']   = isset($is_improve_option[$return_mi['is_improve']]) ? $is_improve_option[$return_mi['is_improve']] : "";
    $return_mi['run_state_name']    = $run_state_option[$return_mi['run_state']];
    $return_mi['stock_date']        = ($return_mi['stock_date'] == '0000-00-00') ? "" : $return_mi['stock_date'];
    $return_mi['return_type']       = ($return_mi['add_c_no'] == '1113') ? "4" : "2";

    $return_mi_unit_sql    = "
        SELECT
            miu.sku,
            (SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=miu.sup_c_no) as sup_c_name,
            miu.quantity
        FROM manufacturer_inspection_unit miu 
        WHERE miu.mi_no = '{$return_mi['mi_no']}'
    ";
    $return_mi_unit_query  = mysqli_query($my_db, $return_mi_unit_sql);
    $return_mi_unit_list   = [];
    while($mi_unit  = mysqli_fetch_assoc($return_mi_unit_query))
    {
        $return_mi_unit_list[] = array("option_info" => "{$mi_unit['sku']}::{$mi_unit['quantity']}", "sup_c_name" => $mi_unit['sup_c_name']);
    }
    $return_mi['return_mi_unit_list'] = $return_mi_unit_list;

    $return_mi_list[] = $return_mi;
}

# 공급업체
$unit_model			= ProductCmsUnit::Factory();
$sch_sup_c_list 	= $unit_model->getDistinctUnitCompanyData('sup_c_no');

$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("is_improve_option", $is_improve_option);
$smarty->assign("work_state_option", getWorkStateOption());
$smarty->assign("run_state_option", $run_state_option);
$smarty->assign("sup_c_list", $sch_sup_c_list);
$smarty->assign("return_mi_list", $return_mi_list);

$smarty->display('work_cms_return_mi_list.html');
?>