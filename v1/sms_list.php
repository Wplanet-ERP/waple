<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_date.php');
require('inc/helper/_message.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "83";
$nav_title   = "문자 발송 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

// 초기값 세팅
$arr_year  = range(date("Y"), date("Y") - 2);
$arr_month = range(1, 12);
foreach($arr_month as $key => $val) {
	$arr_month[$key] = str_pad($val, 2, '0', STR_PAD_LEFT);
}

$smarty->assign("arr_year", $arr_year);
$smarty->assign("arr_month", $arr_month);

// 리스트 페이지 쿼리 저장
$save_query = http_build_query($_GET);
$smarty->assign("save_query", $save_query);

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";

$s_dest_phone 	= isset($_GET['s_dest_phone']) ? $_GET['s_dest_phone'] : "";
$s_msg 			= isset($_GET['s_msg']) ? $_GET['s_msg'] : "";
$sch_cinfo 		= isset($_GET['sch_cinfo']) ? $_GET['sch_cinfo'] : "";
$sch_year 		= isset($_GET['s_year']) ? $_GET['s_year'] : date("Y");
$sch_month 		= isset($_GET['s_month']) ? $_GET['s_month'] : date("m");

$smarty->assign("sch_year", $sch_year);
$smarty->assign("sch_month", $sch_month);

$s_yyyymm = $sch_year . $sch_month;


$add_where = "1=1";

if(!empty($s_dest_phone)) {
	$add_where .= " AND DEST_PHONE like '%{$s_dest_phone}%'";
	$smarty->assign("s_dest_phone", $s_dest_phone);
}

if(!empty($s_msg)) {
	$add_where .= " AND MSG_BODY LIKE '%{$s_msg}%'";
	$smarty->assign("s_msg", $s_msg);
}

if(!empty($sch_cinfo)) {

	if($sch_cinfo == 10){
        $add_where .= " AND SEND_PHONE = '02-2675-6260' AND CINFO NOT IN('1','2')";
	}else{
        $add_where .= " AND CINFO = '{$sch_cinfo}'";
	}
	$smarty->assign("sch_cinfo", $sch_cinfo);
}

if($sch_cinfo == '10' && ($session_s_no == '61' || $session_s_no == '20' || $session_s_no == '21')){

}elseif (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자")) {
	$add_where .= " AND cinfo in (null, '', '1', '2','100')";
}

$base_sms_sql = "";
if($sch_year == '2024' && $sch_month < '07')
{
	$add_where .= " AND CMID LIKE '{$s_yyyymm}%'";
	$base_sms_sql = "
		SELECT
			CMID AS MSG_ID,
			CINFO,
			IF(MSG_TYPE==5,'LMS','SMS') AS MSG_TYPE,
			SEND_TIME,
			REPORT_TIME,
			SEND_PHONE,
			DEST_PHONE,
			STATUS,
			MSG_BODY
		FROM Z_UDS_LOG
		WHERE {$add_where}
	";
}
else
{
	$add_where .= " AND MSG_TYPE != 'AT'";
	$base_sms_sql = "
		SELECT
			MSG_ID,
			CINFO,
			CALL_MSG,
			MSG_TYPE,
			SEND_TIME,
			REPORT_TIME,
			SEND_PHONE,
			DEST_PHONE,
			STATUS,
			MSG_BODY
		FROM EASY_SEND_LOG
		WHERE {$add_where}
	";
}

# 전체 게시물 수
$total_sms_sql 		= "SELECT COUNT(*) as cnt FROM ({$base_sms_sql}) as rs";
$total_sms_query	= mysqli_query($my_db, $total_sms_sql);
$total_sms_result	= mysqli_fetch_array($total_sms_query);
$total_num 			= $total_sms_result['cnt'];

$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 20;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($total_num/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "sms_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $total_num);
$smarty->assign("pagelist", $page_list);

$message_state_option 	= getMessageStateOption();
$c_info_option 			= getCInfoOption();
$base_sms_list  		= [];

$base_sms_sql .= "ORDER BY MSG_ID DESC LIMIT {$offset}, {$num}";
$base_sms_query = mysqli_query($my_db, $base_sms_sql);
while ($base_sms_result = mysqli_fetch_array($base_sms_query))
{
	$base_sms_result['SEND_TIME'] 	= date("Y-m-d H:i", strtotime($base_sms_result['SEND_TIME']));
	$base_sms_result['REPORT_TIME']	= !empty($base_sms_result['REPORT_TIME']) ? date("Y-m-d H:i", strtotime($base_sms_result['REPORT_TIME'])) : "";
	$base_sms_result['CINFO_NAME']	= $c_info_option[$base_sms_result['CINFO']]["option_title"];
	$base_sms_result['STATUS_NAME']	= $message_state_option[$base_sms_result['STATUS']];

	$base_sms_list[] = $base_sms_result;
}

$smarty->assign("year_option", getCurYearOption());
$smarty->assign("month_option", getMonthOption());
$smarty->assign("c_info_option", $c_info_option);
$smarty->assign("base_sms_list", $base_sms_list);

$smarty->display('sms_list.html');
?>
