<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "접수No")
	->setCellValue('B1', "발주일자")
	->setCellValue('C1', "발주단계")
	->setCellValue('D1', "발주처 및 판매경로")
	->setCellValue('E1', "고객명")
	->setCellValue('F1', "연락처1")
	->setCellValue('G1', "주소1")
	->setCellValue('H1', "주소2")
	->setCellValue('I1', "제품코드")
	->setCellValue('J1', "방문코드")
	->setCellValue('K1', "접수내용")
	->setCellValue('L1', "완료일자")
	->setCellValue('M1', "유/무상")
	->setCellValue('N1', "수금액")
	->setCellValue('O1', "시리얼/완료내용")
	->setCellValue('P1', "센터명")
	->setCellValue('Q1', "기사명")
	->setCellValue('R1', "네스급지")
	->setCellValue('S1', "고유번호&회원번호");


// 리스트 쿼리
$visit_sql = "
	SELECT
		*
	FROM
		work_cms_visit
	WHERE
		visit_state='3'
	ORDER BY v_no DESC
";

$result	= mysqli_query($my_db, $visit_sql);
$idx = 2;
if(!!$result)
{
    while($visit = mysqli_fetch_array($result))
    {
        $req_date    = date('y/m/d',strtotime($visit['req_date']))."-".$visit['req_num'];
        $run_date    = !empty($visit['run_date']) ? date('y/m/d',strtotime($visit['run_date'])) : "";
		$visit_price = number_format($visit['visit_price']);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$idx, "")
            ->setCellValue('B'.$idx, $req_date)
            ->setCellValue('C'.$idx, $visit['visit_step'])
            ->setCellValue('D'.$idx, "닥터피엘")
            ->setCellValue('E'.$idx, $visit['customer'])
            ->setCellValue('F'.$idx, $visit['hp'])
            ->setCellValue('G'.$idx, $visit['addr1'])
            ->setCellValue('H'.$idx, $visit['addr2'])
            ->setCellValue('I'.$idx, $visit['prd_code'])
            ->setCellValue('J'.$idx, $visit['visit_code'])
            ->setCellValue('K'.$idx, $visit['req_content'])
            ->setCellValue('L'.$idx, $run_date)
            ->setCellValue('M'.$idx, $visit['visit_free'])
            ->setCellValue('N'.$idx, $visit_price)
            ->setCellValue('O'.$idx, $visit['run_content'])
            ->setCellValue('P'.$idx, $visit['center'])
            ->setCellValue('Q'.$idx, "-")
            ->setCellValue('R'.$idx, "-")
			->setCellValueExplicit('S'.$idx, $visit['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
		;

        $idx++;
    }
}
$idx--;

$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');


$objPHPExcel->getActiveSheet()->getStyle("A2:S{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A2:A'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B2:B'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C2:C'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D2:D'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E2:E'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('F2:F'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G2:G'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('H2:H'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('I2:I'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J2:J'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K2:K'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('L2:L'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('M2:M'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('N2:N'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('O2:O'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('P2:P'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Q2:Q'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('R2:R'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('S2:S'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('H2:H'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('K2:K'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('O2:O'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:S'.$idx)->applyFromArray($styleArray);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);


$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('커머스 방문지원 리스트');

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_진행요청건.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

exit;

?>
