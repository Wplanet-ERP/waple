<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:B1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A2:A3");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("B2:B3");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("C2:D2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("E2:F2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("G2:H2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("I2:J2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("K2:L2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("M2:N2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("O2:P2");

$lfcr = chr(10) ;

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', "구성품명")
    ->setCellValue('B2', "단가")
    ->setCellValue("C2", "광고선전비")
    ->setCellValue("C3", "수량")
    ->setCellValue("D3", "금액")
    ->setCellValue("E2", "시딩출고")
    ->setCellValue("E3", "수량")
    ->setCellValue("F3", "금액")
    ->setCellValue("G2", "소모품비")
    ->setCellValue("G3", "수량")
    ->setCellValue("H3", "금액")
    ->setCellValue("I2", "접대비")
    ->setCellValue("I3", "수량")
    ->setCellValue("J3", "금액")
    ->setCellValue("K2", "복리후생비")
    ->setCellValue("K3", "수량")
    ->setCellValue("L3", "금액")
    ->setCellValue("M2", "폐기손실")
    ->setCellValue("M3", "수량")
    ->setCellValue("N3", "금액")
    ->setCellValue("O2", "합계")
    ->setCellValue("O3", "수량")
    ->setCellValue("P3", "금액")
;

$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFDDEBF7');
$objPHPExcel->getActiveSheet()->getStyle('A2:P3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFF2F2F2');
$add_where          = "1=1 AND log_subject IN(1,2,3,4,5,9) AND state='타계정반출'";
$sch_move_mon       = isset($_GET['sch_move_mon']) ? $_GET['sch_move_mon'] : date('Y-m', strtotime('-1 months'));
$sch_move_s_date    = $sch_move_mon."-01";
$sch_move_end_day   = date("t", strtotime($sch_move_mon));
$sch_move_e_date    = $sch_move_mon."-{$sch_move_end_day}";

if(!empty($sch_move_mon))
{
    $add_where  .=  " AND pcsr.regdate BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}'";
}

# 입고/반출 리스트
$stock_report_list        = [];
$stock_report_total_list  = array("1" => 0, "2" => 0, "3" => 0, "4" => 0, "5" => 0, "9" => 0, "1_price" => 0, "2_price" => 0, "3_price" => 0, "4_price" => 0, "5_price" => 0, "9_price" => 0,"total" => 0);
$stock_report_sql   = "
    SELECT 
        option_name,
        prd_unit,
        (SELECT price FROM product_cms_stock_confirm pcsc WHERE base_type='start' AND pcsc.prd_unit=pcsr.prd_unit AND base_mon='{$sch_move_mon}' ORDER BY price DESC LIMIT 1) as unit_price,
        log_subject,
        SUM(stock_qty) as total_qty
    FROM product_cms_stock_report pcsr 
    WHERE {$add_where}
    GROUP BY prd_unit, log_subject
    ORDER BY sku
";
$stock_report_query = mysqli_query($my_db, $stock_report_sql);
while($stock_report = mysqli_fetch_assoc($stock_report_query))
{
    if(!isset($stock_report_list[$stock_report['prd_unit']])){
        $stock_report_list[$stock_report['prd_unit']] = array(
            "name"          => $stock_report['option_name'],
            "prd_unit"      => $stock_report['prd_unit'],
            "price"         => $stock_report['unit_price'],
            "1"             => 0,
            "1_price"       => 0,
            "2"             => 0,
            "2_price"       => 0,
            "3"             => 0,
            "3_price"       => 0,
            "4"             => 0,
            "4_price"       => 0,
            "5"             => 0,
            "5_price"       => 0,
            "9"             => 0,
            "9_price"       => 0,
            "total"         => 0,
            "total_price"   => 0
        );
    }

    $price_name     = $stock_report['log_subject']."_price";
    $total_qty      = $stock_report['total_qty']*-1;
    $unit_price     = $stock_report_list[$stock_report['prd_unit']]['price'];
    $total_price    = $total_qty*$unit_price;


    $stock_report_list[$stock_report['prd_unit']][$stock_report['log_subject']] += $total_qty;
    $stock_report_list[$stock_report['prd_unit']][$price_name] += $total_price;
    $stock_report_list[$stock_report['prd_unit']]["total"] += $total_qty;
    $stock_report_list[$stock_report['prd_unit']]["total_price"] += $total_price;

    $stock_report_total_list[$stock_report['log_subject']] += $total_qty;
    $stock_report_total_list[$price_name]   += $total_price;
    $stock_report_total_list["total"]       += $total_qty;
    $stock_report_total_list["total_price"] += $total_price;
}

# 합계
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A1", "합계")
    ->setCellValue("C1", number_format($stock_report_total_list[1]))
    ->setCellValue("D1", number_format($stock_report_total_list["1_price"]))
    ->setCellValue("E1", number_format($stock_report_total_list[9]))
    ->setCellValue("F1", number_format($stock_report_total_list["9_price"]))
    ->setCellValue("G1", number_format($stock_report_total_list[2]))
    ->setCellValue("H1", number_format($stock_report_total_list["2_price"]))
    ->setCellValue("I1", number_format($stock_report_total_list[3]))
    ->setCellValue("J1", number_format($stock_report_total_list["3_price"]))
    ->setCellValue("K1", number_format($stock_report_total_list[4]))
    ->setCellValue("L1", number_format($stock_report_total_list["4_price"]))
    ->setCellValue("M1", number_format($stock_report_total_list[5]))
    ->setCellValue("N1", number_format($stock_report_total_list["5_price"]))
    ->setCellValue("O1", number_format($stock_report_total_list['total']))
    ->setCellValue("P1", number_format($stock_report_total_list["total_price"]))
;

$idx = 4;
if($stock_report_list){
    foreach($stock_report_list as $stock_report)
    {
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $stock_report["name"])
            ->setCellValue("B{$idx}", $stock_report["price"])
            ->setCellValue("C{$idx}", number_format($stock_report["1"]))
            ->setCellValue("D{$idx}", number_format($stock_report["1_price"]))
            ->setCellValue("E{$idx}", number_format($stock_report["9"]))
            ->setCellValue("F{$idx}", number_format($stock_report["9_price"]))
            ->setCellValue("G{$idx}", number_format($stock_report["2"]))
            ->setCellValue("H{$idx}", number_format($stock_report["2_price"]))
            ->setCellValue("I{$idx}", number_format($stock_report["3"]))
            ->setCellValue("J{$idx}", number_format($stock_report["3_price"]))
            ->setCellValue("K{$idx}", number_format($stock_report["4"]))
            ->setCellValue("L{$idx}", number_format($stock_report["4_price"]))
            ->setCellValue("M{$idx}", number_format($stock_report["5"]))
            ->setCellValue("N{$idx}", number_format($stock_report["6_price"]))
            ->setCellValue("O{$idx}", number_format($stock_report["total"]))
            ->setCellValue("P{$idx}", number_format($stock_report["total_price"]))
        ;

        $idx++;
    }
    $idx--;

    $objPHPExcel->getActiveSheet()->getStyle("A4:B{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFF2CC');
    $objPHPExcel->getActiveSheet()->getStyle("O4:P{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFE2EFDA');
}


$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:P{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:P3")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:P{$idx}")->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A1:P{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("A2:P3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A4:A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("J")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("K")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("L")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("M")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("N")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("O")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("P")->setWidth(12);

$excel_filename = $sch_move_mon." 타계정반출 상세리스트";
$objPHPExcel->getActiveSheet()->setTitle($excel_filename);


$objPHPExcel->setActiveSheetIndex(0);
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename.".xlsx");

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save('php://output');
?>
