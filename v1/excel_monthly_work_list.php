<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('Classes/PHPExcel.php');

// 접근 제한
if(!(permissionNameCheck($session_permission, "마스터관리자") || $session_s_no == '19' || $session_s_no == '101')){ // 대표 및 김영경 외 접근불가
	$smarty->display('access_error.html');
	exit;
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
	->setLastModifiedBy("Maarten Balliauw")
	->setTitle("Office 2007 XLSX Test Document")
	->setSubject("Office 2007 XLSX Test Document")
	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	->setKeywords("office 2007 openxml php")
	->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

//Excel DATA
$month 	= isset($_GET['month']) ? $_GET['month'] : "";
$work_data_list  	  = [];
$work_data_total_list = [];

$excel_title	= str_replace("-","", $month);
$month_s_date 	= "{$month}-01";
$day_count 		= date('t', strtotime($month_s_date));
$month_e_date 	= "{$month}-{$day_count}";
$sheet_date 	= date('ymd', strtotime("{$month_s_date}"))."~".date('ymd', strtotime("{$month_e_date}"));

$month_work_sql = "
	SELECT
		w.w_no,
		w.task_run_dday,
		w.task_run_regdate,
		w.c_name,
		(SELECT s.s_name FROM staff s WHERE s.s_no = w.s_no) AS s_name,
		(SELECT p.title FROM product p WHERE p.prd_no = w.prd_no) AS prd_name,
		w.t_keyword,
		w.wd_c_no,
		w.wd_price,
		w.wd_c_name
	FROM work w
	WHERE w.work_state IN('4','6') AND w.wd_c_no IS NOT NULL AND w.prd_no IN('35','36','39') AND DATE_FORMAT(w.task_run_regdate, '%Y-%m') = '{$month}'
	ORDER BY w.wd_c_name ASC, w.prd_no ASC, w.w_no DESC
";
$month_work_query = mysqli_query($my_db, $month_work_sql);
while($month_work = mysqli_fetch_array($month_work_query))
{
	$work_data_list[$month_work['wd_c_no']][] = array(
		'w_no' 				=> $month_work['w_no'],
		'task_run_dday' 	=> $month_work['task_run_dday'],
		'task_run_regdate'	=> $month_work['task_run_regdate'],
		'c_name' 			=> $month_work['c_name'],
		's_name' 			=> $month_work['s_name'],
		'prd_name' 			=> $month_work['prd_name'],
		't_keyword' 		=> $month_work['t_keyword'],
		'wd_price' 			=> $month_work['wd_price'],
		'wd_c_name' 		=> $month_work['wd_c_name']
	);
}

if(!empty($work_data_list))
{
	$sheet_idx = 1;
	foreach($work_data_list as $sheet_c_no => $work_data)
	{
		if($sheet_idx > 0){
			$objPHPExcel->createSheet($sheet_idx);
		}
		$sheet = $objPHPExcel->setActiveSheetIndex($sheet_idx);
		$sheet->getDefaultStyle()->getFont()
			->setName('맑은 고딕')
			->setSize(9);

		//상단타이틀
		$sheet->setCellValue('A1', "No.")
			->setCellValue('B1', "w_no")
			->setCellValue('C1', "예정완료일")
			->setCellValue('D1', "업무완료일")
			->setCellValue('E1', "업체명")
			->setCellValue('F1', "업체담당자")
			->setCellValue('G1', "상품명")
			->setCellValue('H1', "타겟키워드")
			->setCellValue('I1', "출금액(VAT별도)")
			->setCellValue('J1', "출금업체(지급처)")
		;

		$idx = 2;
		$sheet_total = $sheet_cnt = 0;
		$sheet_title = "";
		foreach($work_data as $work)
		{
			$sheet_title = $work['wd_c_name'];

			$sheet->setCellValue("A{$idx}", $idx-1)
				->setCellValue("B{$idx}", $work['w_no'])
				->setCellValue("C{$idx}", $work['task_run_dday'])
				->setCellValue("D{$idx}", $work['task_run_regdate'])
				->setCellValue("E{$idx}", $work['c_name'])
				->setCellValue("F{$idx}", $work['s_name'])
				->setCellValue("G{$idx}", $work['prd_name'])
				->setCellValue("H{$idx}", $work['t_keyword'])
				->setCellValue("I{$idx}", $work['wd_price'])
				->setCellValue("J{$idx}", $work['wd_c_name'])
			;

			$sheet->getRowDimension("{$idx}")->setRowHeight(20);
			$sheet->getStyle("E{$idx}")->getAlignment()->setWrapText(true);
			$sheet->getStyle("H{$idx}")->getAlignment()->setWrapText(true);
			$sheet->getStyle("I{$idx}")->getNumberFormat()->setFormatCode('#,##0');

			$sheet_total += $work['wd_price'];
			$sheet_cnt++;
			$idx++;
		}

		$sheet->setCellValue("I{$idx}", $sheet_total);
		$sheet->getRowDimension("{$idx}")->setRowHeight(20);
		$sheet->getStyle("I{$idx}")->getFont()->setBold(true);
		$sheet->getStyle("I{$idx}")->getNumberFormat()->setFormatCode('#,##0');
		$sheet->getStyle("I{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FFFF00');

		$idx--;
		$sheet->getStyle("A1:J{$idx}")->applyFromArray($styleArray);
		$sheet->getStyle("A1:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("A1:J{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$sheet->getStyle('A1:J1')->getFont()->setSize(11);
		$sheet->getStyle('A1:J1')->getFont()->setBold(true);
		$sheet->getStyle("A1:J1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00F2DCDB');

		$sheet->getStyle("A2:A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$sheet->getStyle("E2:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$sheet->getStyle("H2:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$sheet->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		$sheet->getColumnDimension('A')->setWidth(6);
		$sheet->getColumnDimension('B')->setWidth(9);
		$sheet->getColumnDimension('C')->setWidth(13);
		$sheet->getColumnDimension('D')->setWidth(19);
		$sheet->getColumnDimension('E')->setWidth(22);
		$sheet->getColumnDimension('F')->setWidth(14);
		$sheet->getColumnDimension('G')->setWidth(25);
		$sheet->getColumnDimension('H')->setWidth(18);
		$sheet->getColumnDimension('I')->setWidth(21);
		$sheet->getColumnDimension('J')->setWidth(21);

		$sheet->setTitle($sheet_title);
		$sheet->getPageSetup()->setFitToPage(true);

		$work_data_total_list[] = array(
			'name' => $sheet_title,
			'date' => $sheet_date,
			'count' => $sheet_cnt,
			'total' => $sheet_total,
			'c_no'  => $sheet_c_no
		);
		$sheet_idx++;
	}

    if(!empty($work_data_total_list))
	{
		$sheet = $objPHPExcel->setActiveSheetIndex(0);

		$sheet->getDefaultStyle()->getFont()
			->setName('굴림')
			->setSize(10);

		//상단타이틀
		$sheet->setCellValue('A1', "순번")
			->setCellValue('B1', "이름")
			->setCellValue('C1', "산정기간")
			->setCellValue('D1', "건수")
			->setCellValue('E1', "금액")
			->setCellValue('F1', "생년월일")
			->setCellValue('G1', "은행명")
			->setCellValue('H1', "예금주")
			->setCellValue('I1', "계좌번호")
		;

		$sheet_all_total = $sheet_all_cnt = 0;
		$total_idx = 2;
		foreach($work_data_total_list as $work_data_total)
		{
			$wd_c_no 	= $work_data_total['c_no'];
			$wd_c_sql 	= "SELECT birthday, bk_title, bk_name, bk_num FROM company WHERE c_no = '{$wd_c_no}' LIMIT 1";
			$wd_c_query = mysqli_query($my_db, $wd_c_sql);
			$wd_result  = mysqli_fetch_assoc($wd_c_query);

			$birthday   = date('y.m.d', strtotime($wd_result['birthday']));
			$sheet->setCellValue("A{$total_idx}", $total_idx-1)
				->setCellValue("B{$total_idx}", $work_data_total['name'])
				->setCellValue("C{$total_idx}", $work_data_total['date'])
				->setCellValue("D{$total_idx}", $work_data_total['count'])
				->setCellValue("E{$total_idx}", $work_data_total['total'])
				->setCellValue("F{$total_idx}", $birthday)
				->setCellValue("G{$total_idx}", $wd_result['bk_title'])
				->setCellValue("H{$total_idx}", $wd_result['bk_name'])
				->setCellValue("I{$total_idx}", $wd_result['bk_num'])
			;

			$sheet->getRowDimension("{$total_idx}")->setRowHeight(18);
			$sheet->getStyle("E{$total_idx}")->getNumberFormat()->setFormatCode('#,##0');

			$sheet_all_total += $work_data_total['total'];
			$sheet_all_cnt += $work_data_total['count'];
			$total_idx++;
		}

		$sheet->mergeCells("A{$total_idx}:C{$total_idx}");
		$sheet->setCellValue("A{$total_idx}", "합계");
		$sheet->setCellValue("D{$total_idx}", $sheet_all_cnt);
		$sheet->setCellValue("E{$total_idx}", $sheet_all_total);
		$sheet->getRowDimension("{$total_idx}")->setRowHeight(18);

		$sheet->getStyle("E{$total_idx}")->getFont()->setBold(true);
		$sheet->getStyle("E{$total_idx}")->getNumberFormat()->setFormatCode('#,##0');
		$sheet->getStyle("E{$total_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FFFF00');

		$sheet->getStyle("A1:I{$total_idx}")->applyFromArray($styleArray);
		$sheet->getStyle("A1:I{$total_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("A1:I{$total_idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$sheet->getStyle("A1:I1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00D8E4BC');
		$sheet->getStyle("I2:I{$total_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

		$sheet->getColumnDimension('A')->setWidth(10);
		$sheet->getColumnDimension('B')->setWidth(10);
		$sheet->getColumnDimension('C')->setWidth(15);
		$sheet->getColumnDimension('D')->setWidth(10);
		$sheet->getColumnDimension('E')->setWidth(13);
		$sheet->getColumnDimension('F')->setWidth(13);
		$sheet->getColumnDimension('G')->setWidth(13);
		$sheet->getColumnDimension('H')->setWidth(13);
		$sheet->getColumnDimension('I')->setWidth(20);

		$sheet->setTitle("총결산");
	}
}

$objPHPExcel->setActiveSheetIndex(0);
$excel_filename=iconv('UTF-8','EUC-KR',"{$excel_title}_재택근무자 급여 정산.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

?>
