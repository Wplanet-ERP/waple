<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/deposit.php');
require('inc/helper/withdraw.php');
require('inc/helper/incentive.php');
require('inc/model/MyQuick.php');
require('inc/model/Deposit.php');
require('inc/model/Withdraw.php');
require('inc/model/Team.php');
require('inc/model/Staff.php');

// 접근 권한
if (!(permissionNameCheck($session_permission,"마스터관리자") || permissionNameCheck($session_permission,"재무관리자"))){
	$smarty->display('access_error.html');
	exit;
}

# 프로세스 처리
$process 		= (isset($_POST['process'])) ? $_POST['process'] : "";
$withdraw_model = Withdraw::Factory();
$deposit_model  = Deposit::Factory();
$team_model     = Team::Factory();

if ($process == "f_manager_memo") // 메모 변경
{
    $main_no    = (isset($_POST['main_no'])) ? $_POST['main_no'] : "";
    $main_type  = (isset($_POST['main_type'])) ? $_POST['main_type'] : "";
    $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if($main_no && $main_type)
    {
        $main_key   = ($main_type == 'deposit') ? "dp_no" : "wd_no";
        $main_model = ($main_type == 'deposit') ? $deposit_model : $withdraw_model;

        $upd_data   = array(
            $main_key               => $main_no,
            "manager_memo"          => $value,
            "manager_memo_regdate"  => date("Y-m-d H:i:s"),
            "manager_memo_s_no"     => $session_s_no
        );

        if (!$main_model->update($upd_data))
            echo "관리자 메모 저장에 실패 하였습니다.";
        else
            echo "관리자 메모가 저장 되었습니다.";
	}
	exit;
}
elseif ($process == "f_incentive_state")
{
    $main_no    = (isset($_POST['main_no'])) ? $_POST['main_no'] : "";
    $main_type  = (isset($_POST['main_type'])) ? $_POST['main_type'] : "";
    $value      = (isset($_POST['val'])) ? $_POST['val'] : "";

    if($main_no && $main_type)
    {
        $main_key   = ($main_type == 'deposit') ? "dp_no" : "wd_no";
        $main_model = ($main_type == 'deposit') ? $deposit_model : $withdraw_model;

        if($value == 2){
            $upd_data   = array(
                $main_key           => $main_no,
                "incentive_state"   => $value,
                "incentive_month"   => date("Y-m"),
                "incentive_date"    => date("Y-m-d H:i:s")
            );
        }else{
            $upd_data   = array(
                $main_key           => $main_no,
                "incentive_state"   => $value,
                "incentive_month"   => "NULL",
                "incentive_date"    => "NULL"
            );
        }

        if (!$main_model->update($upd_data))
            echo '인센티브 상태값 변경에 실패 하였습니다';
        else
            echo '인센티브 상태값 변경에 성공 하였습니다';
    }

    exit;
}
elseif ($process == "f_incentive_month")
{
    $main_no    = (isset($_POST['main_no'])) ? $_POST['main_no'] : "";
    $main_type  = (isset($_POST['main_type'])) ? $_POST['main_type'] : "";
    $value      = (isset($_POST['val'])) ? $_POST['val'] : "";

    if($main_no && $main_type)
    {
        $main_key   = ($main_type == 'deposit') ? "dp_no" : "wd_no";
        $main_model = ($main_type == 'deposit') ? $deposit_model : $withdraw_model;

        if(empty($value)){
            $upd_data   = array(
                $main_key           => $main_no,
                "incentive_state"   => 1,
                "incentive_month"   => $value,
                "incentive_date"    => "NULL"
            );
        }else{
            $upd_data   = array(
                $main_key           => $main_no,
                "incentive_state"   => 2,
                "incentive_month"   => $value,
                "incentive_date"    => date("Y-m-d H:i:s")
            );
        }

        if (!$main_model->update($upd_data))
            echo '인센티브 정산 년/월 변경에 실패 하였습니다';
        else
            echo '인센티브 정산 년/월 변경에 성공 하였습니다';
    }

    exit;
}
elseif ($process == "sel_incentive_state") // 선택된 인센확인 상태값 변경
{
    $chk_inc_ids        = (isset($_POST['chk_inc_no_list'])) ? $_POST['chk_inc_no_list'] : "";
    $save_inc_state     = (isset($_POST['chk_status'])) ? $_POST['chk_status'] : "";
    $search_url         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if($chk_inc_ids)
    {
        $incentive_list      = explode(",", $chk_inc_ids);
        $incentive_multi_sql = "";
        $incentive_month     = date("Y-m");
        $incentive_date      = date("Y-m-d H:i:s");

        foreach($incentive_list as $incentive_data)
        {
            $incentive_val = explode("_", $incentive_data);
            $main_table = $incentive_val[0];
            $main_key   = ($main_table == 'deposit') ? "dp_no" : "wd_no";
            $main_val   = $incentive_val[1];

            if(!empty($main_table) && !empty($main_key) && !empty($main_val)) {
                if ($save_inc_state == 2) {
                    $incentive_multi_sql .= "UPDATE {$main_table} SET incentive_state='{$save_inc_state}', incentive_month='{$incentive_month}', incentive_date='{$incentive_date}' WHERE {$main_key} = '{$main_val}';";
                } else {
                    $incentive_multi_sql .= "UPDATE {$main_table} SET incentive_state='{$save_inc_state}', incentive_month=NULL, incentive_date=NULL WHERE {$main_key} = '{$main_val}';";
                }
            }
        }

        if (!mysqli_multi_query($my_db, $incentive_multi_sql)) {
            echo("<script>alert('인센티브 상태값들 변경에 실패 하였습니다');</script>");
        }else{
            echo("<script>alert('인센티브 상태값을 변경하였습니다');</script>");
        }
    }
    exit ("<script>location.href='incentive_settlement.php?{$search_url}';</script>");
}
elseif ($process == "sel_incentive_month") // 선택된 인센확인 정산 년/월 변경
{
    $chk_inc_ids = (isset($_POST['chk_inc_no_list'])) ? $_POST['chk_inc_no_list'] : "";
    $chk_mon     = (isset($_POST['chk_mon'])) ? $_POST['chk_mon'] : "";
    $search_url  = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if($chk_inc_ids)
    {
        $incentive_list      = explode(",", $chk_inc_ids);
        $incentive_multi_sql = "";
        $incentive_date      = date("Y-m-d H:i:s");

        foreach($incentive_list as $incentive_data)
        {
            $incentive_val = explode("_", $incentive_data);
            $main_table = $incentive_val[0];
            $main_key   = ($main_table == 'deposit') ? "dp_no" : "wd_no";
            $main_val   = $incentive_val[1];

            if(!empty($main_table) && !empty($main_key) && !empty($main_val)){
                $incentive_multi_sql .= "UPDATE {$main_table} SET incentive_state='2', incentive_month='{$chk_mon}', incentive_date='{$incentive_date}' WHERE {$main_key} = '{$main_val}';";
            }
        }

        if (!mysqli_multi_query($my_db, $incentive_multi_sql)) {
            echo("<script>alert('인센티브 정산 년/월 변경에 실패 하였습니다');</script>");
        }else{
            echo("<script>alert('인센티브 정산 년/월을 변경하였습니다');</script>");
        }
    }
    exit ("<script>location.href='incentive_settlement.php?{$search_url}';</script>");
}


# Navigation & My Quick
$nav_prd_no  = "64";
$nav_title   = "인센티브 정산 관리";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);


# 기본 검색조건
$incentive_data_list = $team_model->getIncentiveTeamAndStaffList();
$team_full_name_list = $incentive_data_list["team_list"];
$team_where_list     = $incentive_data_list["team_where"];
$team_title          = "전체";
$default_team_where  = implode(',', $team_where_list);

$add_dp_where       = "1=1 AND main.display='1' AND main.dp_state='2' AND main.team IN({$default_team_where})";
$add_wd_where       = "1=1 AND main.display='1' AND main.wd_state='3' AND main.team IN({$default_team_where})";
$add_where          = "1=1";

# 기본 function
$inc_state_option       = getIncStateOption();
$inc_type_option        = getIncTypeOption();
$inc_sort_option        = getIncSortOption();
$inc_method_option      = [];
$deposit_state_option   = getDpStateOption();
$withdraw_state_option  = getWdStateOption();
$deposit_method_option  = getDpMethodOption();
$withdraw_method_option = getWdMethodOption();

# 검색조건
$sch_dp_wd_no		    = isset($_GET['sch_dp_wd_no']) ? $_GET['sch_dp_wd_no'] : "";
$sch_s_dp_wd_date 		= isset($_GET['sch_s_dp_wd_date']) ? $_GET['sch_s_dp_wd_date'] : "";
$sch_e_dp_wd_date 		= isset($_GET['sch_e_dp_wd_date']) ? $_GET['sch_e_dp_wd_date'] : "";
$sch_incentive_month    = isset($_GET['sch_incentive_month']) ? $_GET['sch_incentive_month'] : "";
$sch_incentive_state    = isset($_GET['sch_incentive_state']) ? $_GET['sch_incentive_state'] : "1";
$sch_incentive_type     = isset($_GET['sch_incentive_type']) ? $_GET['sch_incentive_type'] : "withdraw";
$sch_incentive_method   = isset($_GET['sch_incentive_method']) ? $_GET['sch_incentive_method'] : "";
$sch_manager_team       = isset($_GET['sch_manager_team']) ? $_GET['sch_manager_team'] : "";
$sch_manager	        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_work_company       = isset($_GET['sch_work_company']) ? $_GET['sch_work_company'] : "";
$sch_work_name          = isset($_GET['sch_work_name']) ? $_GET['sch_work_name'] : "";
$sch_work_no            = isset($_GET['sch_work_no']) ? $_GET['sch_work_no'] : "";
$sch_subject            = isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
$sch_company            = isset($_GET['sch_company']) ? $_GET['sch_company'] : "";
$sch_memo               = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";

if(!empty($sch_dp_wd_no)){
    $add_dp_where .= " AND main.dp_no = '{$sch_dp_wd_no}'";
    $add_wd_where .= " AND main.wd_no = '{$sch_dp_wd_no}'";
    $smarty->assign("sch_dp_wd_no", $sch_dp_wd_no);
}

if (!empty($sch_s_dp_wd_date)) {
	if($sch_incentive_type == 'deposit'){
		$add_dp_where .= " AND main.dp_date >= '{$sch_s_dp_wd_date}'";
	}elseif($sch_incentive_type == 'withdraw'){
		$add_wd_where .= " AND main.wd_date >= '{$sch_s_dp_wd_date}'";
	}else{
        $add_dp_where .= " AND main.dp_date >= '{$sch_s_dp_wd_date}'";
        $add_wd_where .= " AND main.wd_date >= '{$sch_s_dp_wd_date}'";
    }
	$smarty->assign("sch_s_dp_wd_date", $sch_s_dp_wd_date);
}

if (!empty($sch_e_dp_wd_date)) {
	if($sch_incentive_type == 'deposit'){
		$add_dp_where .= " AND main.dp_date <= '{$sch_e_dp_wd_date}'";
	}elseif($sch_incentive_type == 'withdraw'){
		$add_wd_where .= " AND main.wd_date <= '{$sch_e_dp_wd_date}'";
	}else{
        $add_dp_where .= " AND main.dp_date <= '{$sch_e_dp_wd_date}'";
        $add_wd_where .= " AND main.wd_date <= '{$sch_e_dp_wd_date}'";
    }
	$smarty->assign("sch_e_dp_wd_date", $sch_e_dp_wd_date);
}

if(!empty($sch_incentive_month))
{
    $add_dp_where .= " AND main.incentive_month = '{$sch_incentive_month}'";
    $add_wd_where .= " AND main.incentive_month = '{$sch_incentive_month}'";
    $smarty->assign("sch_incentive_month", $sch_incentive_month);
}

if(!empty($sch_incentive_type))
{
    if($sch_incentive_type == 'deposit'){
        $inc_method_option = $deposit_method_option;
        $add_wd_where .= " AND 1!=1";
    }elseif($sch_incentive_type == 'withdraw'){
        $inc_method_option = $withdraw_method_option;
        $add_dp_where .= " AND 1!=1";
    }
    $smarty->assign("sch_incentive_type", $sch_incentive_type);

    if(!empty($sch_incentive_method))
    {
        $add_where .= " AND method = '{$sch_incentive_method}'";
        $smarty->assign("sch_incentive_method", $sch_incentive_method);
    }
}

if (!empty($sch_manager_team))
{
    if ($sch_manager_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_manager_team);
        $add_dp_where   .= " AND main.`team` IN({$sch_team_code_where})";
        $add_wd_where 	.= " AND main.`team` IN({$sch_team_code_where})";

        $team_item  = $team_model->getItem($sch_manager_team);
        $team_title = $team_item['team_name'];
    }
    $smarty->assign("sch_manager_team", $sch_manager_team);
}

if (!empty($sch_manager))
{
    $add_dp_where  .= " AND main.`s_no` IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_manager}%')";
    $add_wd_where  .= " AND main.`s_no` IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_manager}%')";

    $team_title .= " > {$sch_manager}";
    $smarty->assign("sch_manager", $sch_manager);
}

if(!empty($sch_work_company))
{
    $add_dp_where .= " AND w.c_name LIKE '%{$sch_work_company}%'";
    $add_wd_where .= " AND w.c_name LIKE '%{$sch_work_company}%'";
    $smarty->assign("sch_work_company", $sch_work_company);
}

if(!empty($sch_work_name))
{
    $add_dp_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product p WHERE p.title LIKE '%{$sch_work_name}%' AND p.display='1')";
    $add_wd_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product p WHERE p.title LIKE '%{$sch_work_name}%' AND p.display='1')";
    $smarty->assign("sch_work_name", $sch_work_name);
}

if(!empty($sch_work_no))
{
    $add_dp_where .= " AND w.w_no = '{$sch_work_no}'";
    $add_wd_where .= " AND w.w_no = '{$sch_work_no}'";
    $smarty->assign("sch_work_no", $sch_work_no);
}

if(!empty($sch_subject))
{
    $add_dp_where .= " AND main.dp_subject LIKE '%{$sch_subject}%'";
    $add_wd_where .= " AND main.wd_subject LIKE '%{$sch_subject}%'";
    $smarty->assign("sch_subject", $sch_subject);
}

if(!empty($sch_company))
{
    $add_dp_where .= " AND main.c_name LIKE '%{$sch_company}%'";
    $add_wd_where .= " AND main.c_name LIKE '%{$sch_company}%'";
    $smarty->assign("sch_company", $sch_company);
}

if(!empty($sch_memo))
{
    $add_dp_where .= " AND main.manager_memo LIKE '%{$sch_memo}%'";
    $add_wd_where .= " AND main.manager_memo LIKE '%{$sch_memo}%'";
    $smarty->assign("sch_memo", $sch_memo);
}


$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";
$add_order_by   = "confirm_date DESC";

if(!empty($ord_type))
{
    if($ord_type_by == '1'){
        $orderby_val = "ASC";
    }else{
        $orderby_val = "DESC";
    }

    $add_order_by = "{$ord_type} {$orderby_val}, regdate DESC";

    $smarty->assign('ord_type_by', $ord_type_by);
}
$smarty->assign('ord_type', $ord_type);


# Total 쿼리문
$incentive_total_sql = "
    SELECT
       	COUNT(*) as cnt,
        SUM(IF(incentive_state = 2, 1,0)) AS confirm_cnt,
        SUM(IF(incentive_state = 1, 1,0)) AS unconfirm_cnt,
        SUM(IF(incentive_state = 3, 1,0)) AS not_cnt,
        SUM(IF(`type`='deposit', IF(method='3',supply_cost, confirm_price), 0)) AS dp_sum,
        SUM(IF(`type`='withdraw', supply_cost, 0)) AS wd_sum
    FROM
    (
        (
            SELECT
                main.w_no,
                main.dp_no AS main_no,
                main.dp_state AS state,
                main.dp_method AS method,
                main.vat_choice AS vat_type,
                w.c_name AS work_company,
                (SELECT s_name FROM staff s WHERE s.s_no=main.s_no) AS s_name,
                (SELECT team_name FROM team t WHERE t.team_code=main.team) AS t_name,
                (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) AS prd_title,
                main.dp_subject as subject,
                w.task_run,
                'deposit' AS type,
                main.supply_cost,
                main.cost,
                main.dp_money AS confirm_price,
                main.dp_money_vat AS confirm_price_vat,
                main.dp_date AS confirm_date,
                main.c_name AS confirm_company,
                main.incentive_state,
                main.incentive_month,
                main.incentive_date,
                main.manager_memo,
                main.regdate
            FROM deposit main
            LEFT JOIN work w ON w.w_no=main.w_no
            WHERE {$add_dp_where}
        )
        UNION
        (
            SELECT
                main.w_no,
                main.wd_no AS main_no,
                main.wd_state as state,
                main.wd_method AS method,
                main.vat_choice AS vat_type,
                w.c_name AS work_company,
                (SELECT s_name FROM staff s WHERE s.s_no=main.s_no) AS s_name,
                (SELECT team_name FROM team t WHERE t.team_code=main.team) AS t_name,
                (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) AS prd_title,
                main.wd_subject as subject,
                w.task_run,
                'withdraw' AS type,
                main.supply_cost,
                main.cost,
                '0' AS confirm_price,
                main.wd_money AS confirm_price_vat,
                main.wd_date AS confirm_date,
                main.c_name AS confirm_company,
                main.incentive_state,
                main.incentive_month,
                main.incentive_date,
                main.manager_memo,
                main.regdate
            FROM withdraw main
            LEFT JOIN work w ON w.w_no=main.w_no
            WHERE {$add_wd_where}
        )
    ) AS main_result
    WHERE {$add_where}
";
$incentive_total_query  = mysqli_query($my_db, $incentive_total_sql);
$incentive_total_result = mysqli_fetch_assoc($incentive_total_query);
$incentive_total_list   = $incentive_total_result;
$incentive_total_list['title']   = $team_title;
$incentive_total_list['cal_sum'] = $incentive_total_list['dp_sum']-$incentive_total_list['wd_sum'];

# 페이징 쿼리
if(!empty($sch_incentive_state))
{
    $add_dp_where .= " AND main.incentive_state = '{$sch_incentive_state}'";
    $add_wd_where .= " AND main.incentive_state = '{$sch_incentive_state}'";
    $smarty->assign("sch_incentive_state", $sch_incentive_state);
}

$incentive_total_sql = "
    SELECT
       	COUNT(*) as cnt
    FROM
    (
        (
            SELECT
                main.w_no,
                main.dp_no AS main_no,
                main.dp_state AS state,
                main.dp_method AS method,
                main.vat_choice AS vat_type,
                w.c_name AS work_company,
                (SELECT s_name FROM staff s WHERE s.s_no=main.s_no) AS s_name,
                (SELECT team_name FROM team t WHERE t.team_code=main.team) AS t_name,
                (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) AS prd_title,
                main.dp_subject as subject,
                w.task_run,
                'deposit' AS type,
                main.supply_cost,
                main.cost,
                main.dp_money AS confirm_price,
                main.dp_money_vat AS confirm_price_vat,
                main.dp_date AS confirm_date,
                main.c_name AS confirm_company,
                main.incentive_state,
                main.incentive_month,
                main.incentive_date,
                main.manager_memo,
                main.regdate
            FROM deposit main
            LEFT JOIN work w ON w.w_no=main.w_no
            WHERE {$add_dp_where}
        )
        UNION
        (
            SELECT
                main.w_no,
                main.wd_no AS main_no,
                main.wd_state as state,
                main.wd_method AS method,
                main.vat_choice AS vat_type,
                w.c_name AS work_company,
                (SELECT s_name FROM staff s WHERE s.s_no=main.s_no) AS s_name,
                (SELECT team_name FROM team t WHERE t.team_code=main.team) AS t_name,
                (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) AS prd_title,
                main.wd_subject as subject,
                w.task_run,
                'withdraw' AS type,
                main.supply_cost,
                main.cost,
                '0' AS confirm_price,
                main.wd_money AS confirm_price_vat,
                main.wd_date AS confirm_date,
                main.c_name AS confirm_company,
                main.incentive_state,
                main.incentive_month,
                main.incentive_date,
                main.manager_memo,
                main.regdate
            FROM withdraw main
            LEFT JOIN work w ON w.w_no=main.w_no
            WHERE {$add_wd_where}
        )
    ) AS main_result
    WHERE {$add_where}
";
$incentive_total_query       = mysqli_query($my_db, $incentive_total_sql);
$incentive_total_result      = mysqli_fetch_assoc($incentive_total_query);
$incentive_total             = $incentive_total_result['cnt'];

# 페이징 처리
$page_type  = isset($_GET['ord_page_type']) ? intval($_GET['ord_page_type']) : "100";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($incentive_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list  = pagelist($pages, "incentive_settlement.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("incentive_total", $incentive_total);
$smarty->assign("incentive_total_list", $incentive_total_list);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$incentive_sql = "
    SELECT
       *
    FROM
    (
        (
            SELECT
                main.w_no,
                main.dp_no AS main_no,
                main.dp_state AS state,
                main.dp_method AS method,
                main.vat_choice AS vat_type,
                w.c_name AS work_company,
                (SELECT s_name FROM staff s WHERE s.s_no=main.s_no) AS s_name,
                (SELECT team_name FROM team t WHERE t.team_code=main.team) AS t_name,
                (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) AS prd_title,
                main.dp_subject as subject,
                w.task_run,
                'deposit' AS type,
                main.supply_cost,
                main.cost,
                main.dp_money AS confirm_price,
                main.dp_money_vat AS confirm_price_vat,
                main.dp_date AS confirm_date,
                main.c_name AS confirm_company,
                main.incentive_state,
                main.incentive_month,
                main.incentive_date,
                main.manager_memo,
                main.regdate
            FROM deposit main
            LEFT JOIN work w ON w.w_no=main.w_no
            WHERE {$add_dp_where}
        )
        UNION
        (
            SELECT
                main.w_no,
                main.wd_no AS main_no,
                main.wd_state as state,
                main.wd_method AS method,
                main.vat_choice AS vat_type,
                w.c_name AS work_company,
                (SELECT s_name FROM staff s WHERE s.s_no=main.s_no) AS s_name,
                (SELECT team_name FROM team t WHERE t.team_code=main.team) AS t_name,
                (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) AS prd_title,
                main.wd_subject as subject,
                w.task_run,
                'withdraw' AS type,
                main.supply_cost,
                main.cost,
                main.supply_cost AS confirm_price,
                main.wd_money AS confirm_price_vat,
                main.wd_date AS confirm_date,
                main.c_name AS confirm_company,
                main.incentive_state,
                main.incentive_month,
                main.incentive_date,
                main.manager_memo,
                main.regdate
            FROM withdraw main
            LEFT JOIN work w ON w.w_no=main.w_no
            WHERE {$add_wd_where}
        )
    ) AS main_result
    WHERE {$add_where}
    ORDER BY {$add_order_by}
    LIMIT {$offset}, {$num}
";
$incentive_query = mysqli_query($my_db, $incentive_sql);
$incentive_list  = [];
while($incentive = mysqli_fetch_assoc($incentive_query))
{
    $incentive['type_name']  = $inc_type_option[$incentive['type']]['title'];
    $incentive['type_class'] = $inc_type_option[$incentive['type']]['class'];

    if($incentive['type'] == 'deposit'){
        $incentive['state_name']    = $deposit_state_option[$incentive['state']];
        $incentive['method_name']   = $deposit_method_option[$incentive['method']];
    }else{
        $incentive['state_name']    = $withdraw_state_option[$incentive['state']];
        $incentive['method_name']   = $withdraw_method_option[$incentive['method']];
    }

    $incentive['incentive_date']    = date("Y-m-d", strtotime("{$incentive['incentive_date']}"));

    if($incentive['type'] == 'deposit')
    {
        if($incentive['method'] == '3'){
            $incentive['view_price']     = $incentive['supply_cost'];
            $incentive['view_price_vat'] = $incentive['cost'];
        }else{
            $incentive['view_price']     = $incentive['confirm_price'];
            $incentive['view_price_vat'] = $incentive['confirm_price_vat'];
        }
    }
    elseif($incentive['type'] == 'withdraw')
    {
        if($incentive['method'] == '3' || $incentive['method'] == '4'){
            $incentive['view_price']     = $incentive['supply_cost'];
            $incentive['view_price_vat'] = $incentive['cost'];
        }elseif($incentive['vat_type'] == '2'){
            $incentive['view_price']     = $incentive['supply_cost'];
            $incentive['view_price_vat'] = $incentive['confirm_price_vat'];
        }else{
            $incentive['view_price']     = $incentive['confirm_price'];
            $incentive['view_price_vat'] = $incentive['confirm_price_vat'];
        }

    }else{
        $incentive['view_price']        = $incentive['confirm_price'];
        $incentive['view_price_vat']    = $incentive['confirm_price_vat'];
    }

    $incentive_list[] = $incentive;
}

$is_editable = ($session_s_no == '28' || $session_s_no == '233') ? true : false;

$smarty->assign("inc_state_option", $inc_state_option);
$smarty->assign("inc_type_option", $inc_type_option);
$smarty->assign("inc_method_option", $inc_method_option);
$smarty->assign("inc_sort_option", $inc_sort_option);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("page_type_option", getPageTypeOption('big'));
$smarty->assign("is_editable", $is_editable);
$smarty->assign("incentive_list", $incentive_list);

$smarty->display('incentive_settlement.html');

?>
