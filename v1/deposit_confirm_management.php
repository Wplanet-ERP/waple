<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/deposit.php');
require('inc/helper/work.php');
require('inc/model/MyQuick.php');
require('inc/model/Corporation.php');
require('inc/model/Company.php');
require('inc/model/Work.php');
require('inc/model/Deposit.php');

# 프로세스 처리
$process       = isset($_POST['process']) ? $_POST['process'] : "";
$deposit_model = Deposit::Factory();

if($process == "confirm_deposit")
{
    $dp_no 			    = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
    $dpc_no 		    = (isset($_POST['dpc_no'])) ? $_POST['dpc_no'] : "";
    $search_url	        = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $cur_date	 	    = date("Y-m-d");
    $upd_confirm_data   = array("dpc_no" => $dpc_no, "work_state" => "6", "dp_no" => $dp_no, "run_date" => $cur_date, "run_s_no" => $session_s_no, "run_team" => $session_team);
    $back_confirm_data  = array("dpc_no" => $dpc_no, "work_state" => "3", "dp_no" => "NULL", "run_date" => "NULL", "run_s_no" => "NULL", "run_team" => "NULL");

    $deposit_result = $deposit_model->getItem($dp_no);

    if(!empty($deposit_result['w_no']))
    {
        $work_model  = Work::Factory();
        $work_item   = $work_model->getItem($deposit_result['w_no']);
        if(isset($work_item['w_no']) && !empty($work_item['w_no']))
        {
            $upd_confirm_data['c_no']   = $work_item['c_no'];
            $upd_confirm_data['c_name'] = $work_item['c_name'];
        }
    }

    if (!$deposit_model->updateConfirm($upd_confirm_data)){
        exit("<script>alert('입금확인 완료처리에 실패했습니다.');location.href='deposit_confirm_management.php?{$search_url}';</script>");
    }
    else
    {
        $upd_data       = array("dp_no" => $dp_no, "dp_state" => "2", "dp_date" => $cur_date, "confirm_no" => $dpc_no, "run_s_no" => $session_s_no, "run_team" => $session_team, "confirm_date" => $cur_date);

        if($deposit_result['dp_money_vat'] == 0) {
            $upd_data['dp_money'] 	  = $deposit_result['supply_cost'];
            $upd_data['dp_money_vat'] = $deposit_result['cost'];
        }

        if($deposit_model->update($upd_data)) {
            exit("<script>alert('입금확인 완료처리에 성공했습니다.');location.href='deposit_confirm_management.php?{$search_url}';</script>");
        }else{
            $deposit_model->updateConfirm($back_confirm_data);
            exit("<script>alert('입금확인 완료처리에 실패했습니다.');location.href='deposit_confirm_management.php?{$search_url}';</script>");
        }
    }
}
elseif($process == "multi_confirm_deposit")
{
    $dpc_no_val = (isset($_POST['dpc_no_list'])) ? $_POST['dpc_no_list'] : "";
    $search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $false_cnt  = 0;

    if(empty($dpc_no_val)){
        exit("<script>alert('입금확인 완료처리에 실패했습니다.');location.href='deposit_confirm_management.php?{$search_url}';</script>");
    }

    $dpc_no_list  = explode("||", $dpc_no_val);
    $cur_date	  = date("Y-m-d");
    $work_model   = Work::Factory();

    foreach($dpc_no_list as $dpc_no)
    {
        if($dpc_no > 0)
        {
            $confirm_data       = $deposit_model->getConfirmItem($dpc_no);
            $deposit_sql        = "SELECT w_no, dp_no, supply_cost, cost, dp_money_vat FROM deposit dp WHERE (confirm_no IS NULL OR confirm_no = '' OR confirm_no = '0') AND display='1' AND dp_state='1' AND bk_name='{$confirm_data['bk_name']}' AND cost='{$confirm_data['price']}' LIMIT 1";
            $deposit_query      = mysqli_query($my_db, $deposit_sql);
            $deposit_data       = mysqli_fetch_array($deposit_query);

            if(isset($deposit_data['dp_no']) && !empty($deposit_data['dp_no']))
            {
                $dp_no              = $deposit_data['dp_no'];
                $w_no               = $deposit_data['w_no'];
                $upd_confirm_data   = array("dpc_no" => $dpc_no, "work_state" => "6", "dp_no" => $dp_no, "run_date" => $cur_date, "run_s_no" => $session_s_no, "run_team" => $session_team);
                $back_confirm_data  = array("dpc_no" => $dpc_no, "work_state" => "3", "dp_no" => "NULL", "run_date" => "NULL", "run_s_no" => "NULL", "run_team" => "NULL");

                if(!empty($w_no))
                {
                    $work_item   = $work_model->getItem($w_no);
                    if(isset($work_item['w_no']) && !empty($work_item['w_no']))
                    {
                        $upd_confirm_data['c_no']   = $work_item['c_no'];
                        $upd_confirm_data['c_name'] = $work_item['c_name'];
                    }
                }

                if($deposit_model->updateConfirm($upd_confirm_data))
                {
                    $upd_data = array("dp_no" => $dp_no, "dp_state" => "2", "dp_date" => $cur_date, "confirm_no" => $dpc_no, "run_s_no" => $session_s_no, "run_team" => $session_team, "confirm_date" => $cur_date);

                    if($deposit_data['dp_money_vat'] == 0) {
                        $upd_data['dp_money'] 	  = $deposit_data['supply_cost'];
                        $upd_data['dp_money_vat'] = $deposit_data['cost'];
                    }

                    if(!$deposit_model->update($upd_data)){
                        $deposit_model->updateConfirm($back_confirm_data);
                        $false_cnt++;
                    }
                }else{
                    $false_cnt++;
                }
            }else{
                $false_cnt++;
            }
        }else{
            $false_cnt++;
        }
    }

    if($false_cnt > 0){
        exit("<script>alert('{$false_cnt}건 입금확인 완료처리에 실패했습니다.');location.href='deposit_confirm_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('입금확인 완료처리에 성공했습니다.');location.href='deposit_confirm_management.php?{$search_url}';</script>");
    }
}
elseif($process == "confirm_deposit_tax")
{
    $dp_no 			    = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
    $search_url	        = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(!$deposit_model->update(array("dp_no" => $dp_no, "dp_tax_state" => 2))){
        exit("<script>alert('계산서 발행완료 처리에 실패했습니다.');location.href='deposit_confirm_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('계산서 발행완료 처리되었습니다.');location.href='deposit_confirm_management.php?{$search_url}';</script>");
    }
}
elseif($process == "f_work_state")
{
    $dpc_no     = isset($_POST['dpc_no']) ? $_POST['dpc_no'] : "";
    $val        = isset($_POST['val']) ? $_POST['val'] : "";
    $upd_data   = array("dpc_no" => $dpc_no, "work_state" => $val);

    if(!$deposit_model->updateConfirm($upd_data)){
        echo "진행상태 변경에 실패했습니다.";
    }else{
        echo "진행상태를 변경했습니다.";
    }
    exit;
}
elseif($process == "f_brand")
{
    $dpc_no     = isset($_POST['dpc_no']) ? $_POST['dpc_no'] : "";
    $c_no       = isset($_POST['val']) ? $_POST['val'] : "";

    $confirm_item   = $deposit_model->getConfirmItem($dpc_no);
    $company_model  = Company::Factory();
    $company_item   = $company_model->getItem($c_no);

    $upd_confirm_data = array("dpc_no" => $dpc_no, "c_no" => $c_no, "c_name" => $company_item['c_name']);

    if($deposit_model->updateConfirm($upd_confirm_data))
    {
        if(!empty($confirm_item['dp_no'])) {
            $work_where  = "dp_no='{$confirm_item['dp_no']}'";
            $work_model  = Work::Factory();
            $work_item   = $work_model->getWhereItem($work_where);
            if(isset($work_item['w_no']) && !empty($work_item['w_no']))
            {
                $upd_data = array("w_no" => $work_item['w_no'], "c_no" => $company_item['c_no'], "c_name" => $company_item['c_name'], "s_no" => $company_item['s_no'], "team" => $company_item["team"]);
                $work_model->update($upd_data);
            }
        }
        echo "업체/브랜드명 변경에 성공했습니다.";
    }
    else
    {
        echo "업체/브랜드명 변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "f_notice")
{
    $dpc_no     = isset($_POST['dpc_no']) ? $_POST['dpc_no'] : "";
    $val        = isset($_POST['val']) ? $_POST['val'] : "";
    $upd_data   = array("dpc_no" => $dpc_no, "notice" => addslashes(trim($val)));

    if(!$deposit_model->updateConfirm($upd_data)){
        echo "비고 변경에 실패했습니다.";
    }else{
        echo "비고 내용을 변경했습니다.";
    }
    exit;
}
elseif($process == "f_dp_content")
{
    $dpc_no     = isset($_POST['dpc_no']) ? $_POST['dpc_no'] : "";
    $val        = isset($_POST['val']) ? $_POST['val'] : "";
    $upd_data   = array("dp_no" => $dpc_no, "cost_info" => addslashes(trim($val)));

    if(!$deposit_model->update($upd_data)){
        echo "입금 상세내용 변경에 실패했습니다.";
    }else{
        echo "입금 상세내용을 변경했습니다.";
    }
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "152";
$nav_title   = "입금 확인 데이터";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 기본 변수 값 설정
$today_val      = date('Y-m-d');
$days_val       = date('Y-m-d', strtotime('-3 days'));
$week_val       = date('Y-m-d', strtotime('-1 weeks'));
$month_val      = date('Y-m-d', strtotime('-1 months'));
$base_day_val   = ($session_team == "00211") ? $days_val : $today_val;
$base_type_val  = ($session_team == "00211") ? "days" : "today";

$smarty->assign("today_val", $today_val);
$smarty->assign("days_val", $days_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);

# 검색조건
$add_where          = "1=1";
$sch_dp_s_date      = isset($_GET['sch_dp_s_date']) ? $_GET['sch_dp_s_date'] : $base_day_val;
$sch_dp_e_date      = isset($_GET['sch_dp_e_date']) ? $_GET['sch_dp_e_date'] : $today_val;
$sch_dp_date_type   = isset($_GET['sch_dp_date_type']) ? $_GET['sch_dp_date_type'] : $base_type_val;
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_work_state     = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_dp_account     = isset($_GET['sch_dp_account']) ? $_GET['sch_dp_account'] : "";
$sch_bk_name        = isset($_GET['sch_bk_name']) ? $_GET['sch_bk_name'] : "";
$sch_price          = isset($_GET['sch_price']) ? $_GET['sch_price'] : "";
$sch_dp_no          = isset($_GET['sch_dp_no']) ? $_GET['sch_dp_no'] : "";
$sch_dp_tax_state   = isset($_GET['sch_dp_tax_state']) ? $_GET['sch_dp_tax_state'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ori_ord_type       = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "";

if(!empty($sch_dp_s_date) || !empty($sch_dp_e_date))
{
    if(!empty($sch_dp_s_date)){
        $add_where .= " AND `dpc`.dp_date >= '{$sch_dp_s_date}'";
        $smarty->assign('sch_dp_s_date', $sch_dp_s_date);
    }

    if(!empty($sch_dp_e_date)){
        $add_where .= " AND `dpc`.dp_date <= '{$sch_dp_e_date}'";
        $smarty->assign('sch_dp_e_date', $sch_dp_e_date);
    }
}
$smarty->assign('sch_dp_date_type', $sch_dp_date_type);

if(!empty($sch_no)){
    $add_where   .= " AND `dpc`.dpc_no = '{$sch_no}'";
    $smarty->assign('sch_no', $sch_no);
}

if(!empty($sch_work_state)){
    $add_where   .= " AND `dpc`.work_state = '{$sch_work_state}'";
    $smarty->assign('sch_work_state', $sch_work_state);
}

if(!empty($sch_dp_account)){
    $add_where   .= " AND `dpc`.dp_account = '{$sch_dp_account}'";
    $smarty->assign('sch_dp_account', $sch_dp_account);
}

if(!empty($sch_bk_name)){
    $add_where   .= " AND `dpc`.bk_name LIKE '%{$sch_bk_name}%'";
    $smarty->assign('sch_bk_name', $sch_bk_name);
}

if(!empty($sch_price)){
    $add_where   .= " AND `dpc`.price = '{$sch_price}'";
    $smarty->assign('sch_price', $sch_price);
}

if(!empty($sch_dp_no)) {
    if ($sch_dp_no == '1') {
        $add_where .= " AND `dpc`.dp_no > 0";
    } elseif ($sch_dp_no == '2') {
        $add_where .= " AND `dpc`.dp_no=0";
    }
    $smarty->assign('sch_dp_no', $sch_dp_no);
}

if(!empty($sch_dp_tax_state)) {
    $add_where .= " AND `dpc`.dp_no IN(SELECT dp.dp_no FROM deposit dp WHERE dp.dp_tax_state='{$sch_dp_tax_state}')";
    $smarty->assign('sch_dp_tax_state', $sch_dp_tax_state);
}

$add_order_by = "";
$ord_type_by  = "";
$order_by_val = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

    if(!empty($ord_type_by))
    {
        $ord_type_list = [];
        if($ord_type == 'dp_date'){
            $ord_type_list[] = "dp_date";
        }

        if($ori_ord_type == $ord_type)
        {
            if($ord_type_by == '1'){
                $order_by_val = "ASC";
            }elseif($ord_type_by == '2'){
                $order_by_val = "DESC";
            }
        }else{
            $ord_type_by  = '2';
            $order_by_val = "DESC";
        }

        foreach($ord_type_list as $ord_type_val){
            $add_order_by .= "{$ord_type_val} {$order_by_val}, ";
        }
    }
}
$add_order_by .= "dpc_no DESC";

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);


// 전체 게시물 수
$deposit_total_sql	 = "SELECT count(`dpc_no`) as cnt FROM deposit_confirm `dpc` WHERE {$add_where}";
$deposit_total_query  = mysqli_query($my_db, $deposit_total_sql);
$deposit_total_result = mysqli_fetch_array($deposit_total_query);
$deposit_total 	     = $deposit_total_result['cnt'];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($deposit_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "deposit_confirm_management.php", $page_num, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $deposit_total);
$smarty->assign("pagelist", $page_list);
$smarty->assign("ord_page_type", $page_type);

$deposit_cnt_sql	= "SELECT count(`dpc_no`) as cnt FROM deposit_confirm `dpc` WHERE work_state='3'";
$deposit_cnt_query  = mysqli_query($my_db, $deposit_cnt_sql);
$deposit_cnt_result = mysqli_fetch_assoc($deposit_cnt_query);
$smarty->assign("deposit_cnt", $deposit_cnt_result['cnt']);

$deposit_confirm_sql = "
    SELECT 
        *,
        DATE_FORMAT(`dpc`.regdate, '%Y-%m-%d') as reg_day,
        (SELECT `name` FROM corp_account s WHERE s.co_no=`dpc`.dp_account) AS dp_account_name,
        (SELECT s_name FROM staff s WHERE s.s_no=`dpc`.reg_s_no) AS reg_name,
        (SELECT s_name FROM staff s WHERE s.s_no=`dpc`.run_s_no) AS run_name,
        (SELECT dp.cost_info FROM deposit dp WHERE dp.dp_no=dpc.dp_no) AS dp_content,
        (SELECT dp.dp_tax_state FROM deposit dp WHERE dp.dp_no=dpc.dp_no) AS dp_tax_state
    FROM deposit_confirm `dpc`
    WHERE {$add_where}
    ORDER BY {$add_order_by}
    LIMIT {$offset}, {$num}
";
$deposit_confirm_query  = mysqli_query($my_db, $deposit_confirm_sql);
$deposit_confirm_list   = [];
$dp_tax_option          = getDpTaxOption();
while($deposit_confirm = mysqli_fetch_assoc($deposit_confirm_query))
{
    $deposit_list = [];
    if($deposit_confirm['work_state'] == '3')
    {
        $deposit_sql    = "SELECT dp_no, reg_s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=dp.reg_s_no) as reg_name, bk_name, cost FROM deposit dp WHERE (confirm_no IS NULL OR confirm_no = '' OR confirm_no = '0') AND display='1' AND dp_state='1' AND bk_name='{$deposit_confirm['bk_name']}' AND cost='{$deposit_confirm['price']}'";
        $deposit_query  = mysqli_query($my_db, $deposit_sql);
        while($deposit_result = mysqli_fetch_array($deposit_query)){
            $deposit_list[] = $deposit_result;
        }
    }
    $deposit_confirm['deposit_cnt']         = count($deposit_list);
    $deposit_confirm['deposit_list']        = $deposit_list;
    $deposit_confirm['dp_tax_state_name']   = isset($dp_tax_option[$deposit_confirm['dp_tax_state']]) ? $dp_tax_option[$deposit_confirm['dp_tax_state']] : "";
    $deposit_confirm_list[] = $deposit_confirm;
}

# Model 설정
$corp_model = Corporation::Factory();

$smarty->assign('work_state_option', getWorkStateOption());
$smarty->assign('work_state_color_option', getWorkStateOptionColor());
$smarty->assign('dp_account_option', $corp_model->getDpAccountList());
$smarty->assign('dpc_company_option', getDpcCompanyOption());
$smarty->assign('is_deposit_option', getIsDepositOption());
$smarty->assign('sch_dp_tax_option', $dp_tax_option);
$smarty->assign('page_type_option', getPageTypeOption(4));
$smarty->assign("deposit_confirm_list", $deposit_confirm_list);

$smarty->display('deposit_confirm_management.html');
?>
