<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');

$proc=(isset($_POST['process']))?$_POST['process']:"";


/////////////////////////* 자동 저장 처리 부분 *//////////////////////

if ($proc == "f_p_memo") { // 인센티브 재무담당자 메모 (재무담당자 전용)
	$est_wp_no = (isset($_POST['est_wp_no'])) ? $_POST['est_wp_no'] : "";
	$value = (isset($_POST['val'])) ? $_POST['val'] : "";

	$sql = "UPDATE incentive_list i SET i.memo = '" . $value . "' WHERE i.est_wp_no = '" . $est_wp_no . "'";

	if (!mysqli_query($my_db, $sql))
		echo "마케터 메모 저장에 실패 하였습니다.";
	else
		echo "마케터 메모가 저장 되었습니다.";

	exit;


} else {

	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where="1=1";
	$sch_est_wp_no_get=isset($_GET['sch_est_wp_no'])?$_GET['sch_est_wp_no']:"";
	$sch_state_get=isset($_GET['sch_state'])?$_GET['sch_state']:"";
	$sch_incentive_date_get = isset($_GET['sch_incentive_date']) ? $_GET['sch_incentive_date'] : "";
	$sch_incentive_state_get = isset($_GET['sch_incentive_state']) ? $_GET['sch_incentive_state'] : "";
	$sch_s_no_get=isset($_GET['sch_s_no'])?$_GET['sch_s_no']:"";
	$sch_c_name_get=isset($_GET['sch_c_name'])?$_GET['sch_c_name']:"";
	$sch_memo_get=isset($_GET['sch_memo'])?$_GET['sch_memo']:"";
	$open_est_wp_no_get=isset($_GET['open_est_wp_no'])?$_GET['open_est_wp_no']:"";


	if(!empty($sch_est_wp_no_get)) { // est_wp_no
		$add_where.=" AND i.est_wp_no='".$sch_est_wp_no_get."'";
		$smarty->assign("sch_est_wp_no",$sch_est_wp_no_get);
	}
	if(!empty($sch_state_get)) { // 진행상태
		$add_where.=" AND (SELECT ewp.state FROM estimate_work_package ewp WHERE ewp.dp_no=i.dp_no)='" . $sch_state_get . "'";
		$smarty->assign("sch_state",$sch_state_get);
	}
	if (!empty($sch_incentive_date_get)) { // 인센티브 정산 년/월
		$add_where .= " AND (SELECT dp.incentive_date FROM deposit dp WHERE dp.dp_no=i.dp_no)='" . $sch_incentive_date_get . "'";
		$smarty->assign("sch_incentive_date", $sch_incentive_date_get);
	}
	if(!empty($sch_incentive_state_get)) { // 인센티브 진행상태
		$add_where .= " AND i.state='" . $sch_incentive_state_get . "'";
		$smarty->assign("sch_incentive_state",$sch_incentive_state_get);
	}

	if(!empty($sch_s_no_get)) { // 마케팅 담당자
		if($sch_s_no_get != "all"){
			$add_where.=" AND i.s_no='".$sch_s_no_get."'";
		}
		$smarty->assign("sch_s_no",$sch_s_no_get);
	}elseif(empty($sch_s_no_get) && permissionNameCheck($session_permission, "마케터") && !permissionNameCheck($session_permission, "대표")){
		$add_where.=" AND i.s_no='".$session_s_no."'";
	}
	if(!empty($sch_c_name_get)) { // 업체명
		$add_where.=" AND i.c_name like '%".$sch_c_name_get."%'";
		$smarty->assign("sch_c_name",$sch_c_name_get);
	}
	if(!empty($sch_memo_get)) { // 마케터 메모
		$add_where.=" AND i.memo like '%".$sch_memo_get."%'";
		$smarty->assign("sch_memo",$sch_memo_get);
	}
	if(!empty($open_est_wp_no_get)) { // 마케터 업무 패키지의 업무 목록 펼치기 유무
		$smarty->assign("open_est_wp_no",$open_est_wp_no_get);
	}else if(!empty($sch_est_wp_no_get)) { // 갭색시 기본 펼침
		$smarty->assign("open_est_wp_no",$sch_est_wp_no_get);
	}

	// 상품 구분 가져오기(1차)
	$prd_g1_sql="
				SELECT
						k_name,k_name_code
				FROM kind
				WHERE
						k_code='product' AND (k_parent='0' OR k_parent is null) AND display='1'
				ORDER BY
						priority ASC
				";
	$prd_g1_result=mysqli_query($my_db,$prd_g1_sql);

	while($prd_g1=mysqli_fetch_array($prd_g1_result)) {
		$prd_g1_list[]=array(
			"k_name"=>trim($prd_g1['k_name']),
			"k_name_code"=>trim($prd_g1['k_name_code'])
		);
	}
	$smarty->assign("prd_g1_list",$prd_g1_list);

	// 직원가져오기 배열
	$add_where_permission = str_replace("0", "_", $permission_name['마케터']);
	if(permissionNameCheck($session_permission,"재무관리자") || permissionNameCheck($session_permission,"대표") || permissionNameCheck($session_permission,"마스터관리자")){
		$staff_sql="select s_no,s_name from staff where staff_state < '3' AND permission like '".$add_where_permission."'";
		$staff_query=mysqli_query($my_db,$staff_sql);

		while($staff_data=mysqli_fetch_array($staff_query)) {
			$staffs[]=array(
				'no'=>$staff_data['s_no'],
				'name'=>$staff_data['s_name']
			);
		}
	}elseif($session_s_no == '22'){// 소연님의 경유 인큐베이팅 본부건 조회 가능
		$add_where_permission_2 = str_replace("0", "_", $permission_name['인큐베이팅']);
		$staff_sql="select s_no,s_name from staff where staff_state < '3' AND permission like '".$add_where_permission."' AND permission like '".$add_where_permission_2."'";

		$staff_query=mysqli_query($my_db,$staff_sql);

		while($staff_data=mysqli_fetch_array($staff_query)) {
			$staffs[]=array(
				'no'=>$staff_data['s_no'],
				'name'=>$staff_data['s_name']
			);
		}
	}elseif(permissionNameCheck($session_permission,"마케터")){
		$staffs[]=array(
			'no'=>$session_s_no,
			'name'=>$session_name
		);
	}

	$smarty->assign("staff",$staffs);


	// 페이지에 따른 limit 설정
	$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num = 10;
	$offset = ($pages-1) * $num;

	// 리스트 쿼리
	$incentive_list_sql="SELECT
			i_no,
			(SELECT ewp.state FROM estimate_work_package ewp WHERE ewp.est_wp_no=i.est_wp_no) AS ewp_state,
			dp_date,
			incentive_date,
			state,
			est_wp_no,
			dp_no,
			c_no,
			c_name,
			subject,
			s_no,
			(SELECT s_name FROM staff s WHERE s.s_no=i.s_no) AS s_no_name,
			price,
			out_price,
			dp_money,
			total_work_price,
			nat_sales_price,
			memo,
			(SELECT COUNT(*) FROM incentive_work iw WHERE iw.est_wp_no=i.est_wp_no) AS mw_cnt
		FROM
			incentive i
		WHERE
			$add_where
		";

	//echo $incentive_list_sql; exit;

	// 전체 게시물 수
	$cnt_sql = "SELECT count(*) FROM (".$incentive_list_sql.") AS cnt";
	$cnt_query= mysqli_query($my_db, $cnt_sql);
	$cnt_data=mysqli_fetch_array($cnt_query);
	$total_num = $cnt_data[0];

	$smarty->assign(array(
		"total_num"=>number_format($total_num)
	));

	$smarty->assign("total_num",$total_num);
	$pagenum = ceil($total_num/$num);


	// 페이징
	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	// 검색 조건
	$search_url = "sch_est_wp_no=".$sch_est_wp_no_get.
								"&sch_state=".$sch_state_get.
								"&sch_s_no=".$sch_s_no_get.
								"&sch_c_name=".$sch_c_name_get.
								"&sch_memo=".$sch_memo_get;

	$smarty->assign("search_url",$search_url);
	$page=pagelist($pages, "incentive_list.php", $pagenum, $search_url);
	$smarty->assign("pagelist",$page);

	// 리스트 쿼리 결과(데이터)
	$incentive_list_sql .= "LIMIT $offset,$num";
	$result= mysqli_query($my_db, $incentive_list_sql);
	while($incentive_list_array = mysqli_fetch_array($result)){

		$total_work_price_value=0;

		if(!empty($incentive_list_array['price'])){
			$package_price_value=number_format($incentive_list_array['price']);
		}else{
			$package_price_value="";
		}
		if(!empty($incentive_list_array['out_price'])){
			$package_out_price_value=number_format($incentive_list_array['out_price']);
		}else{
			$package_out_price_value="";
		}


		// 인센티브 업무 리스트 쿼리
		$incentive_work_sql="
			SELECT
				est_w_no,
				est_wp_no,
				w_no,
				(SELECT work_state FROM work w WHERE w.w_no=iw.w_no) AS work_state,
				(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=iw.prd_no))) AS k_prd1,
				(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=iw.prd_no))) AS k_prd1_name,
				(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=iw.prd_no)) AS k_prd2,
				(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=iw.prd_no)) AS k_prd2_name,
				prd_no,
				(SELECT title from product prd where prd.prd_no=iw.prd_no) as prd_no_name,
				t_keyword_or_cnt,
				(SELECT t_keyword FROM work w WHERE w.w_no=iw.w_no) AS wrok_t_keyword,
				price,
				out_price,
				(SELECT price FROM work w WHERE w.w_no=iw.w_no) AS work_price,
				memo
			FROM
				incentive_work iw
			WHERE
				iw.est_wp_no='{$incentive_list_array['est_wp_no']}'
			ORDER BY
				k_prd1 ASC, k_prd2 ASC, prd_no ASC
			";
		//echo $incentive_work_sql; exit;

		$incentive_work_result= mysqli_query($my_db, $incentive_work_sql);
		while($incentive_work_array = mysqli_fetch_array($incentive_work_result)){
			if(!empty($incentive_work_array['price'])){
				$price_value=number_format($incentive_work_array['price']);
			}else{
				$price_value="";
			}
			if(!empty($incentive_work_array['out_price'])){
				$out_price_value=number_format($incentive_work_array['out_price']);
			}else{
				$out_price_value="";
			}

			// 상품 구분 가져오기(2차)
			$prd_g2_sql="
		        SELECT
		            k_name,k_name_code,k_parent
		        FROM kind
		        WHERE
		            k_code='product' AND display='1' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='".$incentive_work_array['prd_no']."'))
		        ORDER BY
		            priority ASC
		        ";

			$prd_g2_result=mysqli_query($my_db,$prd_g2_sql);
			while($prd_g2=mysqli_fetch_array($prd_g2_result)) {
				$prd_g2_list[]=array(
		      "est_w_no"=>trim($incentive_work_array['est_w_no']),
					"k_name"=>trim($prd_g2['k_name']),
					"k_name_code"=>trim($prd_g2['k_name_code']),
					"k_parent"=>trim($prd_g2['k_parent'])
				);
			}
			$smarty->assign("prd_g2_list",$prd_g2_list);

		  // 상품 목록 가져오기
			$prd_sql="
		        SELECT
		            title,prd_no,k_name_code
		        FROM product
		        WHERE
		            k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='".$incentive_work_array['prd_no']."')
		        ORDER BY
		            priority ASC
		        ";
			$prd_result=mysqli_query($my_db,$prd_sql);
			while($prd=mysqli_fetch_array($prd_result)) {
				$prd_list[]=array(
					"est_w_no"=>trim($incentive_work_array['est_w_no']),
					"title"=>trim($prd['title']),
					"prd_no"=>trim($prd['prd_no']),
					"k_name_code"=>trim($prd['k_name_code'])
				);
			}
			$smarty->assign("prd_list",$prd_list);

			// 총 실제 외주비 합rP
			$total_work_price_value += $incentive_work_array['work_price'];
			if(!empty($incentive_work_array['w_no'])){
				$work_price_value = number_format($incentive_work_array['work_price']);
			}else{
				$work_price_value = '';
			}

			$incentive_work_lists[] = array(
				"est_w_no"=>$incentive_work_array['est_w_no'],
				"est_wp_no"=>$incentive_work_array['est_wp_no'],
				"w_no"=>$incentive_work_array['w_no'],
				"work_state"=>$incentive_work_array['work_state'],
				"k_prd1"=>$incentive_work_array['k_prd1'],
        "k_prd1_name"=>$incentive_work_array['k_prd1_name'],
        "k_prd2"=>$incentive_work_array['k_prd2'],
        "k_prd2_name"=>$incentive_work_array['k_prd2_name'],
				"prd_no"=>$incentive_work_array['prd_no'],
				"prd_no_name"=>$incentive_work_array['prd_no_name'],
				"t_keyword_or_cnt"=>$incentive_work_array['t_keyword_or_cnt'],
				"wrok_t_keyword"=>$incentive_work_array['wrok_t_keyword'],
				"price"=>$price_value,
				"out_price"=>$out_price_value,
				"work_price"=>$work_price_value,
				"memo"=>$incentive_work_array['memo']
			);
		}

		$incentive_lists[] = array(
			"i_no"=>$incentive_list_array['i_no'],
			"ewp_state"=>$incentive_list_array['ewp_state'],
			"dp_date"=>$incentive_list_array['dp_date'],
			"incentive_date"=>$incentive_list_array['incentive_date'],
			"state"=>$incentive_list_array['state'],
			"est_wp_no"=>$incentive_list_array['est_wp_no'],
			"dp_no"=>$incentive_list_array['dp_no'],
			"c_no"=>$incentive_list_array['c_no'],
			"c_name"=>$incentive_list_array['c_name'],
			"subject"=>$incentive_list_array['subject'],
			"s_no"=>$incentive_list_array['s_no'],
			"s_no_name"=>$incentive_list_array['s_no_name'],
			"price"=>$package_price_value,
			"out_price"=>$package_out_price_value,
			"dp_money"=>number_format($incentive_list_array['dp_money']),
			"total_work_price"=>number_format($incentive_list_array['total_work_price']),
			"nat_sales_price"=>number_format($incentive_list_array['nat_sales_price']),
			"memo"=>$incentive_list_array['memo'],
			"mw_cnt"=>$incentive_list_array['mw_cnt']
		);
	}

	$smarty->assign(array(
		"incentive_list"=>$incentive_lists
	));
	$smarty->assign(array(
		"incentive_work_list"=>$incentive_work_lists
	));

	$smarty->display('incentive_list.html');
}

?>
