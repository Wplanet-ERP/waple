<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/work_return.php');
require('inc/helper/deposit.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');
require('inc/model/Staff.php');

#그룹조회 및 프리랜서
$is_freelancer      = false;
$is_free_editable   = false;
if($session_staff_state == "2"){
    $is_freelancer = true;
    if($session_my_c_type == "3"){
        $is_free_editable = true;
    }
}
$smarty->assign("is_freelancer", $is_freelancer);
$smarty->assign("is_free_editable", $is_free_editable);

$is_super_editable = false;
if($session_team == "00244"){
    $is_super_editable = true;
}
$smarty->assign("is_super_editable", $is_super_editable);


# 프로세스 처리
$proc = (isset($_POST['process']))?$_POST['process']:"";

if($proc == "return_state")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET return_state='{$value}' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql)) {
        echo "회수 진행상태 저장에 실패하였습니다.";
    }else {
        echo "회수 진행상태가 변경 되었습니다.";
    }
    exit;
}
elseif($proc == "return_purpose")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET return_purpose='{$value}' WHERE r_no='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "회수목적 저장에 실패 하였습니다.";
    else
        echo "회수목적이 저장되었습니다.";
    exit;
}
elseif($proc == "return_reason")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $add_value = "";
    if($value != '11'){
        $add_value = ", bad_reason=''";
    }

    $sql = "UPDATE work_cms_return SET return_reason='{$value}' {$add_value} WHERE r_no='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "사유구분 저장에 실패 하였습니다.";
    else
        echo "사유구분이 저장되었습니다.";
    exit;
}
elseif($proc == "cus_memo")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET cus_memo='".addslashes($value)."' WHERE r_no='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "고객메시지 저장에 실패 하였습니다.";
    else
        echo "고객메시지가 저장되었습니다.";
    exit;
}
elseif($proc == "return_type")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET return_type='{$value}' WHERE r_no='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "구분 저장에 실패 하였습니다.";
    else
        echo "구분값이 저장되었습니다.";
    exit;
}
elseif($proc == "req_memo")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET req_memo='".addslashes($value)."' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "요청메시지 저장에 실패 하였습니다.";
    else
        echo "요청메시지가 저장되었습니다.";
    exit;
}
elseif($proc == "return_req_date")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET return_req_date='{$value}' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "회수요청일 저장에 실패 하였습니다.";
    else
        echo "회수요철일이 저장되었습니다.";
    exit;
}
elseif($proc == "return_date")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET return_date='{$value}' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "회수처리일 저장에 실패 하였습니다.";
    else
        echo "회수처리일이 저장되었습니다.";
    exit;
}
elseif($proc == "delivery_type")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET return_delivery_type='{$value}' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "택배사 저장에 실패 하였습니다.";
    else
        echo "택배사가 저장되었습니다.";
    exit;
}
elseif($proc == "delivery_no")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $chk_sql    = "SELECT COUNT(r_no) as cnt FROM work_cms_return WHERE order_number!='{$key}' AND (return_delivery_no='{$value}' OR return_delivery_no2='{$value}')";
    $chk_query  = mysqli_query($my_db, $chk_sql);
    $chk_result = mysqli_fetch_assoc($chk_query);

    if(!empty($_POST['val']) && $chk_result['cnt'] > 0){
        echo "이미 다른 회수에서 사용중인 운송장입니다.";
        exit;
    }

    $chk_ord_sql    = "SELECT COUNT(*) as cnt FROM work_cms_delivery WHERE order_number = (SELECT DISTINCT parent_order_number FROM work_cms_return WHERE order_number='{$key}') AND delivery_no ='{$value}'";
    $chk_ord_query  = mysqli_query($my_db, $chk_ord_sql);
    $chk_ord_result = mysqli_fetch_assoc($chk_ord_query);

    if(!empty($_POST['val']) && $chk_ord_result['cnt'] > 0){
        echo "원 운송장(택배 운송장)에 동일한 운송장 번호가 있습니다.";
        exit;
    }

    $sql = "UPDATE work_cms_return SET return_delivery_no='{$value}' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "운송장번호 저장에 실패 하였습니다.";
    else
        echo "운송장번호가 저장되었습니다.";
    exit;
}
elseif($proc == "delivery_no_2")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET return_delivery_no2='{$value}' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "추가 운송장번호 저장에 실패 하였습니다.";
    else
        echo "추가 운송장번호가 저장되었습니다.";
    exit;
}
elseif($proc == "return_memo")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET return_memo='".addslashes($value)."' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "반품메모 저장에 실패 하였습니다.";
    else
        echo "반품메모가 저장되었습니다.";
    exit;
}
elseif($proc == "delivery_fee")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? str_replace(",","",$_POST['val']) : "";

    $sql = "UPDATE work_cms_return SET return_delivery_fee='{$value}' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "운송비 저장에 실패 하였습니다.";
    else
        echo "운송비가 저장되었습니다.";
    exit;
}
elseif($proc == "return_run_date")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET return_run_date='{$value}' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "처리확인날짜 저장에 실패 하였습니다.";
    else
        echo "처리확인날짜가 저장되었습니다.";
    exit;
}
elseif($proc == "run_state")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET run_state='{$value}' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "처리여부 저장에 실패 하였습니다.";
    else
        echo "처리여부가 저장되었습니다.";
    exit;
}
elseif($proc == "run_memo")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET run_memo='".addslashes($value)."' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "처리내용 저장에 실패 하였습니다.";
    else
        echo "처리내용이 저장되었습니다.";
    exit;
}
elseif($proc == "result_reason")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET result_reason='{$value}' WHERE r_no='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "사유구분(결과) 저장에 실패 하였습니다.";
    else
        echo "사유구분(결과)가 저장되었습니다.";
    exit;
}
elseif($proc == "return_img_path_1")
{
    $order_number      = (isset($_POST['order_number'])) ? $_POST['order_number'] : "";
    $return_state      = (isset($_POST['return_state'])) ? $_POST['return_state'] : "";
    $search_url        = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $return_img_file   = $_FILES["return_img_path_1"];
    $return_img_path_1 = add_store_image($return_img_file, "cms_return");
    $return_img_name_1 = $return_img_file['name'];

    $upd_sql = "UPDATE work_cms_return SET return_date=now(), return_img_path_1='{$return_img_path_1}', return_img_name_1='{$return_img_name_1}', return_state='4', run_with_no='{$session_s_no}' WHERE order_number='{$order_number}'";

    if (mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('이미지가 등록되었습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    } else{
        exit("<script>alert('이미지 등록에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
elseif($proc == "return_img_path_2")
{
    $order_number      = (isset($_POST['order_number'])) ? $_POST['order_number'] : "";
    $search_url        = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $return_img_file   = $_FILES["return_img_path_2"];
    $return_img_path_2 = add_store_image($return_img_file, "cms_return");
    $return_img_name_2 = $return_img_file['name'];

    $upd_sql = "UPDATE work_cms_return SET return_img_path_2='{$return_img_path_2}', return_img_name_2='{$return_img_name_2}' WHERE order_number='{$order_number}'";

    if (mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('이미지가 등록되었습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    } else{
        exit("<script>alert('이미지 등록에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
elseif($proc == "return_img_path_3")
{
    $order_number      = (isset($_POST['order_number'])) ? $_POST['order_number'] : "";
    $search_url        = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $return_img_file   = $_FILES["return_img_path_3"];
    $return_img_path_3 = add_store_image($return_img_file, "cms_return");
    $return_img_name_3 = $return_img_file['name'];

    $upd_sql = "UPDATE work_cms_return SET return_img_path_3='{$return_img_path_3}', return_img_name_3='{$return_img_name_3}' WHERE order_number='{$order_number}'";

    if (mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('이미지가 등록되었습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    } else{
        exit("<script>alert('이미지 등록에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
elseif($proc == "return_img_path_4")
{
    $order_number      = (isset($_POST['order_number'])) ? $_POST['order_number'] : "";
    $search_url        = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $return_img_file   = $_FILES["return_img_path_4"];
    $return_img_path_4 = add_store_image($return_img_file, "cms_return");
    $return_img_name_4 = $return_img_file['name'];

    $upd_sql = "UPDATE work_cms_return SET return_img_path_4='{$return_img_path_4}', return_img_name_4='{$return_img_name_4}' WHERE order_number='{$order_number}'";

    if (mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('이미지가 등록되었습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    } else{
        exit("<script>alert('이미지 등록에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
elseif($proc == "del_return_img")
{
    $order_number      = (isset($_POST['chk_order_number'])) ? $_POST['chk_order_number'] : "";
    $search_url        = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(empty($order_number)){
        exit("<script>alert('주문번호 정보가 없습니다. 이미지 삭제에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }

    $upd_sql = "
        UPDATE work_cms_return SET 
            return_img_path_1=NULL, return_img_name_1=NULL,
            return_img_path_2=NULL, return_img_name_2=NULL,
            return_img_path_3=NULL, return_img_name_3=NULL,
            return_img_path_4=NULL, return_img_name_4=NULL
       WHERE order_number='{$order_number}'
   ";

    if (mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('이미지가 삭제되었습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    } else{
        exit("<script>alert('이미지 삭제에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
elseif($proc == "run_s_no")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $return_ord_sql     = "SELECT DISTINCT order_number FROM work_cms_return WHERE r_no='{$key}'";
    $return_ord_query   = mysqli_query($my_db, $return_ord_sql);
    $return_ord_result  = mysqli_fetch_assoc($return_ord_query);
    $return_ord_no      = $return_ord_result['order_number'];

    $sql = "UPDATE work_cms_return wcr SET run_s_no='{$value}', run_team=(SELECT sub.team FROM staff sub WHERE sub.s_no='{$value}') WHERE order_number='{$return_ord_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "처리담당자 변경에 실패했습니다.";
    else
        echo "처리담당자가 저장되었습니다.";
    exit;
}
elseif($proc == "modify_total_run_s_no")
{
    $chk_r_no_list = isset($_POST['chk_r_no_list']) ? $_POST['chk_r_no_list'] : "";
    $chk_value     = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $search_url    = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $return_ord_sql     = "SELECT DISTINCT order_number FROM work_cms_return WHERE r_no IN({$chk_r_no_list})";
    $return_ord_query   = mysqli_query($my_db, $return_ord_sql);
    $return_ord_list    = [];
    while($return_ord = mysqli_fetch_assoc($return_ord_query))
    {
        $return_ord_list[] = "'".$return_ord['order_number']."'";
    }

    if(!empty($return_ord_list))
    {
        $chk_order_number = implode(',', $return_ord_list);
        $upd_sql = "UPDATE work_cms_return wcr SET run_s_no='{$chk_value}', run_team=(SELECT sub.team FROM staff sub WHERE sub.s_no='{$chk_value}') WHERE order_number IN({$chk_order_number})";

        if(!mysqli_query($my_db, $upd_sql)){
            exit("<script>alert('처리담당자 변경에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('처리담당자 변경했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('처리담당자 변경에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
elseif($proc == "modify_total_return_state")
{
    $chk_r_no_list = isset($_POST['chk_r_no_list']) ? $_POST['chk_r_no_list'] : "";
    $chk_value     = isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $search_url    = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $return_ord_sql     = "SELECT DISTINCT order_number FROM work_cms_return WHERE r_no IN({$chk_r_no_list})";
    $return_ord_query   = mysqli_query($my_db, $return_ord_sql);
    $return_ord_list    = [];
    while($return_ord = mysqli_fetch_assoc($return_ord_query))
    {
        $return_ord_list[] = "'".$return_ord['order_number']."'";
    }

    if(!empty($chk_r_no_list))
    {
        $upd_sql = "UPDATE work_cms_return wcr SET return_state='{$chk_value}' WHERE r_no IN({$chk_r_no_list})";

        if(!mysqli_query($my_db, $upd_sql)){
            exit("<script>alert('회수상태 변경에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('회수상태를 변경했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('회수상태 변경에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
# 일괄적용 작업
elseif($proc == "modify_return_state")
{
    $step_run_date  = (isset($_POST['step_run_date'])) ? $_POST['step_run_date'] : "";
    $search_url 	= isset($_POST['search_url'])?$_POST['search_url']:"";

    $upd_sql = "UPDATE work_cms_return SET return_state='5' WHERE return_state='3' AND return_run_date = '{$step_run_date}'";

    if (mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('진행완료(아임웹미적용) 처리 되었습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    } else{
        exit("<script>alert('진행완료(아임웹미적용) 처리에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
elseif($proc == "f_option_no")
{
    $ord_no  = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $u_no    = (isset($_POST['u_no'])) ? $_POST['u_no'] : "";
    $val     = (isset($_POST['val'])) ? $_POST['val'] : "";
    $sku     = (isset($_POST['sku'])) ? $_POST['sku'] : "";

    $upd_sql = "";
    if(empty($u_no)){
        $upd_sql = "INSERT INTO work_cms_return_unit SET `order_number`='{$ord_no}', `option`='{$val}', sku='{$sku}', quantity='1', regdate=now()";
    }else{
        $upd_sql = "UPDATE work_cms_return_unit SET `option`='{$val}', sku='{$sku}' WHERE u_no='{$u_no}'";
    }

    if (mysqli_query($my_db, $upd_sql)){
        $upd_u_no = (empty($u_no)) ? mysqli_insert_id($my_db) : $u_no;
        echo $upd_u_no;
    }
    exit;
}
elseif($proc == "f_option_quantity")
{
    $ord_no  = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $u_no    = (isset($_POST['u_no'])) ? $_POST['u_no'] : "";
    $val     = (isset($_POST['val'])) ? $_POST['val'] : "";

    $upd_sql = "";
    if(empty($u_no)){
        $upd_sql = "INSERT INTO work_cms_return_unit SET `order_number`='{$ord_no}', `quantity`='{$val}', regdate=now()";
    }else{
        $upd_sql = "UPDATE work_cms_return_unit SET `quantity`='{$val}' WHERE u_no='{$u_no}'";
    }

    if (mysqli_query($my_db, $upd_sql)){
        $upd_u_no = (empty($u_no)) ? mysqli_insert_id($my_db) : $u_no;
        echo $upd_u_no;
    }
    exit;
}
elseif($proc == "f_option_type")
{
    $u_no    = (isset($_POST['u_no'])) ? $_POST['u_no'] : "";
    $ord_no  = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $val     = (isset($_POST['val'])) ? $_POST['val'] : "";

    $upd_sql = "";
    if(empty($u_no)){
        $upd_sql = "INSERT INTO work_cms_return_unit SET `order_number`='{$ord_no}', `type`='{$val}', quantity='1', regdate=now()";
    }else{
        $upd_sql = "UPDATE work_cms_return_unit SET `type`='{$val}' WHERE u_no='{$u_no}'";
    }

    if (mysqli_query($my_db, $upd_sql)){
        $upd_u_no = (empty($u_no)) ? mysqli_insert_id($my_db) : $u_no;
        echo $upd_u_no;
    }
    exit;
}
elseif($proc == "bad_reason")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET bad_reason='{$value}' WHERE r_no='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "불량사유 저장에 실패 하였습니다.";
    else
        echo "불량사유가 저장되었습니다.";
    exit;
}
elseif($proc == "return_with_date")
{
    $key    = (isset($_POST['key'])) ? $_POST['key'] : "";
    $value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return SET return_with_date='{$value}' WHERE order_number='{$key}'";

    if (!mysqli_query($my_db, $sql))
        echo "제조사/본사 발송일 저장에 실패 하였습니다.";
    else
        echo "제조사/본사 발송일이 저장되었습니다.";
    exit;
}
elseif($proc == "modify_state_inspection")
{
    $chk_order_number   = isset($_POST['chk_order_number']) ? $_POST['chk_order_number'] : "";
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_sql = "UPDATE work_cms_return wcr SET return_state='9' WHERE order_number = '{$chk_order_number}'";

    if(!empty($chk_order_number))
    {
        if(!mysqli_query($my_db, $upd_sql)){
            exit("<script>alert('검수대기 처리에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('검수대기 처리했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('검수대기 처리에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
elseif($proc == "modify_state_inspection")
{
    $chk_order_number   = isset($_POST['chk_order_number']) ? $_POST['chk_order_number'] : "";
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_sql = "UPDATE work_cms_return wcr SET return_state='9' WHERE order_number = '{$chk_order_number}'";

    if(!empty($chk_order_number))
    {
        if(!mysqli_query($my_db, $upd_sql)){
            exit("<script>alert('검수대기 처리에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('검수대기 처리했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('검수대기 처리에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
else{

    # Navigation & My Quick
    $nav_prd_no  = "161";
    $quick_model = MyQuick::Factory();
    $is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

    $smarty->assign("is_my_quick", $is_my_quick);
    $smarty->assign("nav_prd_no", $nav_prd_no);

    # 검색 처리
    $add_where          = "1=1";
    $sch_step           = isset($_GET['sch_step']) ? $_GET['sch_step'] : "";
    $sch_req_s_date 	= isset($_GET['sch_req_s_date']) ? $_GET['sch_req_s_date'] : "";
    $sch_req_e_date 	= isset($_GET['sch_req_e_date']) ? $_GET['sch_req_e_date'] : "";
    $sch_return_state   = isset($_GET['sch_return_state']) ? $_GET['sch_return_state'] : "";
    $sch_parent_ord_no  = isset($_GET['sch_parent_ord_no']) ? $_GET['sch_parent_ord_no'] : "";
    $sch_parent_dp_c_no = isset($_GET['sch_parent_dp_c_no']) ? $_GET['sch_parent_dp_c_no'] : "";
    $sch_wise_dp        = isset($_GET['sch_wise_dp']) ? $_GET['sch_wise_dp'] : "";
    $sch_recipient 		= isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
    $sch_recipient_hp 	= isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
    $sch_return_purpose = isset($_GET['sch_return_purpose']) ? $_GET['sch_return_purpose'] : "";
    $sch_return_type    = isset($_GET['sch_return_type']) ? $_GET['sch_return_type'] : "";
    $sch_req_name 		= isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : "";
    $sch_run_s_date     = isset($_GET['sch_run_s_date']) ? $_GET['sch_run_s_date'] : "";
    $sch_run_e_date 	= isset($_GET['sch_run_e_date']) ? $_GET['sch_run_e_date'] : "";
    $sch_run_date_null  = isset($_GET['sch_run_date_null']) ? $_GET['sch_run_date_null'] : "";
    $sch_with_date 	    = isset($_GET['sch_with_date']) ? $_GET['sch_with_date'] : "";
    $sch_with_date_null = isset($_GET['sch_with_date_null']) ? $_GET['sch_with_date_null'] : "";
    $sch_return_img	    = isset($_GET['sch_return_img']) ? $_GET['sch_return_img'] : "";
    $sch_delivery_type  = isset($_GET['sch_delivery_type']) ? $_GET['sch_delivery_type'] : "";
    $sch_delivery_no    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
    $sch_delivery_no_null   = isset($_GET['sch_delivery_no_null']) ? $_GET['sch_delivery_no_null'] : "";
    $sch_run_state      = isset($_GET['sch_run_state']) ? $_GET['sch_run_state'] : "";
    $sch_run_memo       = isset($_GET['sch_run_memo']) ? $_GET['sch_run_memo'] : "";
    $sch_run_name       = isset($_GET['sch_run_name']) ? $_GET['sch_run_name'] : "";
    $sch_with_name      = isset($_GET['sch_with_name']) ? $_GET['sch_with_name'] : "";
    $sch_quick_prd      = isset($_GET['sch_quick_prd']) ? $_GET['sch_quick_prd'] : "";
    $sch_quick_mon      = isset($_GET['sch_quick_mon']) ? $_GET['sch_quick_mon'] : "";

    if(!empty($sch_quick_prd) && !empty($sch_quick_prd))
    {
        $add_where .= " AND r.return_state='6' AND DATE_FORMAT(r.return_date, '%Y-%m')='{$sch_quick_mon}' AND r.order_number IN(SELECT DISTINCT sub.order_number FROM work_cms_return_unit sub WHERE sub.`type` IN(1,2,3,4) AND sub.`option`='{$sch_quick_prd}')";
    }

    if(!empty($sch_req_s_date)){
        $add_where .= " AND r.return_req_date >= '{$sch_req_s_date}'";
        $smarty->assign('sch_req_s_date', $sch_req_s_date);
    }

    if(!empty($sch_req_e_date)){
        $add_where .= " AND r.return_req_date <= '{$sch_req_e_date}'";
        $smarty->assign('sch_req_e_date', $sch_req_e_date);
    }

    if(!empty($sch_return_state)){
        $add_where .= " AND r.return_state = '{$sch_return_state}'";
        $smarty->assign('sch_return_state', $sch_return_state);
    }

    if(!empty($sch_parent_ord_no)){
        $add_where .= " AND r.parent_order_number = '{$sch_parent_ord_no}'";
        $smarty->assign('sch_parent_ord_no', $sch_parent_ord_no);
    }

    if(!empty($sch_wise_dp)){
        $add_where .= " AND r.dp_c_no IN(2280, 5468, 5469, 5475, 5476, 5477, 5478 ,5479, 5480, 5481, 5482, 5939, 6002)";
        $smarty->assign('sch_wise_dp', $sch_wise_dp);
    }else{
        if(!empty($sch_parent_dp_c_no)){
            $add_where .= " AND r.dp_c_no='{$sch_parent_dp_c_no}'";
            $smarty->assign('sch_parent_dp_c_no', $sch_parent_dp_c_no);
        }
    }

    if(!empty($sch_recipient)){
        $add_where .= " AND r.recipient like '%{$sch_recipient}%'";
        $smarty->assign('sch_recipient', $sch_recipient);
    }

    if(!empty($sch_recipient_hp)){
        $add_where .= " AND r.recipient_hp LIKE '%{$sch_recipient_hp}%'";
        $smarty->assign('sch_recipient_hp', $sch_recipient_hp);
    }

    if(!empty($sch_return_purpose)){
        $add_where .= " AND r.return_purpose = '{$sch_return_purpose}'";
        $smarty->assign('sch_return_purpose', $sch_return_purpose);
    }

    if(isset($_GET['sch_return_type']) && $sch_return_type != ""){
        $add_where .= " AND r.return_type = '{$sch_return_type}'";
        $smarty->assign('sch_return_type', $sch_return_type);
    }

    if(!empty($sch_req_name)){
        $add_where .= " AND r.s_no IN(SELECT s.s_no FROM staff s WHERE s.name like '%{$sch_req_name}%')";
        $smarty->assign('sch_req_name', $sch_req_name);
    }

    if(!empty($sch_run_date_null)){
        $add_where .= " AND (r.return_date IS NULL OR r.return_date = '')";
        $smarty->assign('sch_run_date_null', $sch_run_date_null);
    }else{
        if(!empty($sch_run_s_date)){
            $add_where .= " AND r.return_date >='{$sch_run_s_date}'";
            $smarty->assign('sch_run_s_date', $sch_run_s_date);
        }

        if(!empty($sch_run_e_date)){
            $add_where .= " AND r.return_date <='{$sch_run_e_date}'";
            $smarty->assign('sch_run_e_date', $sch_run_e_date);
        }
    }

    if(!empty($sch_with_date_null)){
        $add_where .= " AND (r.return_with_date IS NULL OR r.return_with_date = '')";
        $smarty->assign('sch_with_date_null', $sch_with_date_null);
    }else{
        if(!empty($sch_with_date)){
            $add_where .= " AND r.return_with_date ='{$sch_with_date}'";
            $smarty->assign('sch_with_date', $sch_with_date);
        }
    }

    if(!empty($sch_return_img)){
        if($sch_return_img == '1'){
            $add_where .= " AND (r.return_img_path_1 IS NOT NULL AND r.return_img_path_2 IS NOT NULL)";
        }elseif($sch_return_img == '2'){
            $add_where .= " AND (r.return_img_path_1 IS NOT NULL OR r.return_img_path_2 IS NOT NULL)";
        }elseif($sch_return_img == '3'){
            $add_where .= " AND ((r.return_img_path_1 IS NULL OR r.return_img_path_1 = '') AND r.return_img_path_2 != '') OR (r.return_img_path_1 != '' AND (r.return_img_path_2 IS NULL OR r.return_img_path_2 = ''))";
        }elseif($sch_return_img == '4'){
            $add_where .= " AND ((r.return_img_path_1 IS NULL OR r.return_img_path_1 = '') AND (r.return_img_path_2 IS NULL OR r.return_img_path_2 = ''))";
        }
        $smarty->assign('sch_return_img', $sch_return_img);
    }

    if(!empty($sch_delivery_type)){
        $add_where .= " AND r.return_delivery_type='{$sch_delivery_type}'";
        $smarty->assign('sch_delivery_type', $sch_delivery_type);
    }

    if(!empty($sch_delivery_no_null))
    {
        $add_where .= " AND (r.return_delivery_no IS NULL OR r.return_delivery_no = '')";
        $smarty->assign('sch_delivery_no_null', $sch_delivery_no_null);
    }else{
        if(!empty($sch_delivery_no)){
            $add_where .= " AND (r.return_delivery_no like '%{$sch_delivery_no}%' OR r.return_delivery_no2 like '%{$sch_delivery_no}%')";
            $smarty->assign('sch_delivery_no', $sch_delivery_no);
        }
    }

    if(!empty($sch_delivery_fee)){
        if($sch_delivery_fee == '1'){
            $add_where .= " AND r.return_delivery_fee > 0";
        }elseif($sch_delivery_fee == '2'){
            $add_where .= " AND r.return_delivery_fee = 0";
        }
        $smarty->assign('sch_delivery_fee', $sch_delivery_fee);
    }

    if(!empty($sch_run_state)){
        $add_where .= " AND r.run_state = '{$sch_run_state}'";
        $smarty->assign('sch_run_state', $sch_run_state);
    }

    if(!empty($sch_run_memo)){
        $add_where .= " AND r.run_memo like '%{$sch_run_memo}%'";
        $smarty->assign('sch_run_memo', $sch_run_memo);
    }

    if(!empty($sch_run_name)){
        $add_where .= " AND r.run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_run_name}%')";
        $smarty->assign('sch_run_name', $sch_run_name);
    }

    if(!empty($sch_with_name)){
        $add_where .= " AND r.run_with_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_with_name}%')";
        $smarty->assign('sch_with_name', $sch_with_name);
    }

    # 상세검색[상품등]
    $sch_prd_g1	  = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
    $sch_prd_g2	  = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
    $sch_prd	  = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
    $sch_prd_name = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
    $sch_default  = isset($_GET['sch_default']) ? $_GET['sch_default'] : "Y";
    $sch_detail   = isset($_GET['sch_detail']) ? $_GET['sch_detail'] : "N";

    $smarty->assign("sch_prd_g1", $sch_prd_g1);
    $smarty->assign("sch_prd_g2", $sch_prd_g2);
    $smarty->assign("sch_prd", $sch_prd);
    $smarty->assign("sch_default", $sch_default);
    $smarty->assign("sch_detail", $sch_detail);

    $kind_model     = Kind::Factory();
    $product_model  = ProductCms::Factory();
    $cms_code       = "product_cms";
    $cms_group_list = $kind_model->getKindGroupList($cms_code);
    $prd_total_list = $product_model->getPrdGroupData();
    $prd_g1_list = $prd_g2_list = $prd_g3_list = [];

    foreach($cms_group_list as $key => $prd_data)
    {
        if(!$key){
            $prd_g1_list = $prd_data;
        }else{
            $prd_g2_list[$key] = $prd_data;
        }
    }

    $prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
    $sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

    $smarty->assign("prd_g1_list", $prd_g1_list);
    $smarty->assign("prd_g2_list", $prd_g2_list);
    $smarty->assign("sch_prd_list", $sch_prd_list);

    if (!empty($sch_prd) && $sch_prd != "0") { // 상품
        $add_where .= " AND r.prd_no='{$sch_prd}'";
    }else{
        if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
            $add_where .= " AND r.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
        }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
            $add_where .= " AND r.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
        }
    }

    if(!empty($sch_prd_name)){
        $add_where .= " AND r.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.title like '%{$sch_prd_name}%')";
        $smarty->assign('sch_prd_name', $sch_prd_name);
    }

    # 불량사유 검색
    $sch_bad_reason_g1      = isset($_GET['sch_bad_reason_g1']) ? $_GET['sch_bad_reason_g1'] : "";
    $sch_bad_reason         = isset($_GET['sch_bad_reason']) ? $_GET['sch_bad_reason'] : "";
    $bad_reason_total_list  = $kind_model->getKindGroupList("bad_reason");
    $bad_reason_g1_list     = $bad_reason_total_list[''];
    $bad_reason_g2_list     = [];

    if(!empty($sch_bad_reason_g1))
    {
        $add_where .= " AND r.bad_reason IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_bad_reason_g1}' AND k.display='1')";
        $smarty->assign("sch_bad_reason_g1", $sch_bad_reason_g1);

        $bad_reason_g2_list = $bad_reason_total_list[$sch_bad_reason_g1];
    }

    if(!empty($sch_bad_reason))
    {
        $add_where .= " AND r.bad_reason = '{$sch_bad_reason}'";
        $smarty->assign("sch_bad_reason", $sch_bad_reason);
    }
    $smarty->assign("bad_reason_g1_list", $bad_reason_g1_list);
    $smarty->assign("bad_reason_g2_list", $bad_reason_g2_list);


    $sch_r_no 	        = isset($_GET['sch_r_no']) ? $_GET['sch_r_no'] : "";
    $sch_order_number   = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
    $sch_brand_name     = isset($_GET['sch_brand_name']) ? $_GET['sch_brand_name'] : "";
    $sch_return_reason  = isset($_GET['sch_return_reason']) ? $_GET['sch_return_reason'] : "";
    $sch_result_reason  = isset($_GET['sch_result_reason']) ? $_GET['sch_result_reason'] : "";

    if(!empty($sch_r_no)){
        $add_where .= " AND r.r_no='{$sch_r_no}'";
        $smarty->assign('sch_r_no', $sch_r_no);
    }

    if(!empty($sch_order_number)){
        $add_where .= " AND r.order_number='{$sch_order_number}'";
        $smarty->assign('sch_order_number', $sch_order_number);
    }

    if(!empty($sch_brand_name)){
        $add_where .= " AND r.c_name like '%{$sch_brand_name}%'";
        $smarty->assign('sch_brand_name', $sch_brand_name);
    }

    if(!empty($sch_return_reason)){
        $add_where .= " AND r.return_reason='{$sch_return_reason}'";
        $smarty->assign('sch_return_reason', $sch_return_reason);
    }

    if(!empty($sch_result_reason)){
        $add_where .= " AND r.result_reason='{$sch_result_reason}'";
        $smarty->assign('sch_result_reason', $sch_result_reason);
    }

    #Step 03 처리
    $sch_run_s_no           = isset($_GET['sch_run_s_no']) ? $_GET['sch_run_s_no'] : "";

    if(!empty($sch_run_s_no)){
        $add_where .= " AND r.run_s_no='{$sch_run_s_no}'";
    }

    // Step 날짜 체크
    $step_02_01_date = $step_02_02_date = $step_05_date = $step_07_date = $step_09_date = $step_10_s_date = $step_10_e_date = $cur_date = date('Y-m-d');
    $step_03_run_s_no = "";
    $step_04_delivery_no = $step_04_order_number = $step_04_return_deli_no = $step_04_recipient = $step_04_recipient_hp = $step_04_zipcode = $step_04_recipient_addr = "";

    if(!empty($sch_step))
    {
        switch($sch_step){
            case "step02_01": $step_02_01_date = $sch_req_s_date; break;
            case "step02_02": $step_02_02_date = $sch_run_s_date; break;
            case "step03":
                $step_03_run_s_no = $sch_run_s_no;
                break;
            case "step04":
                $step_04_delivery_no    = isset($_GET['step_04_delivery_no']) ? $_GET['step_04_delivery_no'] : "";
                $step_04_return_deli_no = isset($_GET['step_04_return_deli_no']) ? $_GET['step_04_return_deli_no'] : "";
                $step_04_order_number   = isset($_GET['step_04_order_number']) ? $_GET['step_04_order_number'] : "";
                $step_04_recipient      = isset($_GET['step_04_recipient']) ? $_GET['step_04_recipient'] : "";
                $step_04_recipient_hp   = isset($_GET['step_04_recipient_hp']) ? $_GET['step_04_recipient_hp'] : "";
                $step_04_zipcode        = isset($_GET['step_04_zipcode']) ? $_GET['step_04_zipcode'] : "";
                $step_04_recipient_addr = isset($_GET['step_04_recipient_addr']) ? $_GET['step_04_recipient_addr'] : "";

                if(!empty($step_04_delivery_no)){
                    $add_where .= " AND r.parent_order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery WHERE delivery_no='{$step_04_delivery_no}')";
                }

                if(!empty($step_04_return_deli_no)){
                    $add_where .= " AND (r.return_delivery_no='{$step_04_return_deli_no}' OR r.return_delivery_no2='{$step_04_return_deli_no}')";
                }

                if(!empty($step_04_order_number)){
                    $add_where .= " AND r.parent_order_number='{$step_04_order_number}'";
                }

                if(!empty($step_04_recipient)){
                    $add_where .= " AND r.recipient LIKE '%{$step_04_recipient}%'";
                }

                if(!empty($step_04_recipient_hp)){
                    $add_where .= " AND r.recipient_hp LIKE '%{$step_04_recipient_hp}%'";
                }

                if(!empty($step_04_zipcode)){
                    $add_where .= " AND r.zip_code='{$step_04_zipcode}'";
                }

                if(!empty($step_04_recipient_addr)){
                    $add_where .= " AND r.recipient_addr LIKE '%{$step_04_recipient_addr}%'";
                }

                break;
            case "step05":
                $step_date    = isset($_GET['step_date']) ? $_GET['step_date'] : "";
                $step_05_date = $step_date;
                $add_where   .= " AND r.return_req_date ='{$step_05_date}' AND return_state='1'";
                break;
            case "step07":
                $step_date    = isset($_GET['step_date']) ? $_GET['step_date'] : "";
                $step_07_date = $step_date;

                $add_where .= " AND r.return_req_date ='{$step_07_date}' AND (r.return_delivery_no IS NULL OR r.return_delivery_no ='')";
                break;
            case "step09_01":
                $add_where .= " AND r.return_type='2' AND (r.return_with_date IS NULL OR r.return_with_date = '')";
                break;
            case "step09_02":
                $step_date    = isset($_GET['step_date']) ? $_GET['step_date'] : "";
                $step_09_date = $step_date;

                $add_where .= " AND r.return_with_date ='{$step_09_date}'";
                break;
            case "step10":
                $step_10_s_date = $sch_req_s_date;
                $step_10_e_date = $sch_req_e_date;

                $add_where .= " AND return_type IN('2','4')";
                break;
            case "step11":
                $add_where .= " AND (run_state IS NULL OR run_state = '' OR run_state='0')";
                break;
        }
    }

    $smarty->assign('cur_date', $cur_date);
    $smarty->assign('step_02_01_date', $step_02_01_date);
    $smarty->assign('step_02_02_date', $step_02_02_date);
    $smarty->assign('step_03_run_s_no', $step_03_run_s_no);
    $smarty->assign('step_04_delivery_no', $step_04_delivery_no);
    $smarty->assign('step_04_order_number', $step_04_order_number);
    $smarty->assign('step_04_return_deli_no', $step_04_return_deli_no);
    $smarty->assign('step_04_recipient', $step_04_recipient);
    $smarty->assign('step_04_recipient_hp', $step_04_recipient_hp);
    $smarty->assign('step_04_zipcode', $step_04_zipcode);
    $smarty->assign('step_04_recipient_addr', $step_04_recipient_addr);
    $smarty->assign('step_05_date', $step_05_date);
    $smarty->assign('step_07_date', $step_07_date);
    $smarty->assign('step_09_date', $step_09_date);
    $smarty->assign('step_10_s_date', $step_10_s_date);
    $smarty->assign('step_10_e_date', $step_10_e_date);
    $smarty->assign('step_12_date', date("Y-m"));

    if($is_freelancer){
        $ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "return_date";
        $ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";
    }else{
        $ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
        $ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";
    }

    $add_orderby    = "r.r_no DESC, r.prd_no ASC";
    if(!empty($ord_type))
    {
        if($ord_type_by == '1'){
            $orderby_val = "ASC";
        }else{
            $orderby_val = "DESC";
        }

        if($orderby_val == "DESC"){
            $add_orderby = "r.{$ord_type} IS NULL DESC, r.{$ord_type} {$orderby_val}, r.r_no DESC, r.prd_no ASC";
        }else{
            $add_orderby = "r.{$ord_type} {$orderby_val}, r.r_no DESC, r.prd_no ASC";
        }
    }
    $smarty->assign('ord_type', $ord_type);
    $smarty->assign('ord_type_by', $ord_type_by);

    #회수리스트 Count 정리
    $return_total_sql    = "SELECT count(r_no) as cnt FROM work_cms_return_temporary";
    $return_total_query  = mysqli_query($my_db, $return_total_sql);
    $return_total_result = mysqli_fetch_assoc($return_total_query);
    $return_total        = isset($return_total_result['cnt']) ? $return_total_result['cnt'] : 0;

    $smarty->assign('return_total', number_format($return_total));

    #회수운송장완료시 회수건수
    $step02_apply_cnt_sql    = "SELECT count(r_no) as cnt FROM work_cms_return WHERE (return_run_date BETWEEN '{$sch_run_s_date}' AND '{$sch_run_e_date}') AND return_state='3'";
    $step02_apply_cnt_query  = mysqli_query($my_db, $step02_apply_cnt_sql);
    $step02_apply_cnt_result = mysqli_fetch_assoc($step02_apply_cnt_query);
    $step02_apply_cnt        = isset($step02_apply_cnt_result['cnt']) ? $step02_apply_cnt_result['cnt'] : 0;
    $smarty->assign('step02_apply_cnt', number_format($step02_apply_cnt));

    #회수요청 회수건수
    $step05_apply_cnt_sql    = "SELECT count(r_no) as cnt FROM work_cms_return WHERE (return_req_date BETWEEN '{$sch_req_s_date}' AND '{$sch_req_e_date}') AND return_state='1'";
    $step05_apply_cnt_query  = mysqli_query($my_db, $step05_apply_cnt_sql);
    $step05_apply_cnt_result = mysqli_fetch_assoc($step05_apply_cnt_query);
    $step05_apply_cnt        = isset($step05_apply_cnt_result['cnt']) ? $step05_apply_cnt_result['cnt'] : 0;
    $smarty->assign('step05_apply_cnt', number_format($step05_apply_cnt));

    // 전체 게시물 수
    $cms_return_total_sql   = "SELECT count(DISTINCT order_number) FROM (SELECT r.order_number FROM work_cms_return r WHERE {$add_where} ORDER BY {$add_orderby}) AS cnt";
    $cms_return_total_query = mysqli_query($my_db, $cms_return_total_sql);
    if(!!$cms_return_total_query)
        $cms_return_total_result = mysqli_fetch_array($cms_return_total_query);

    $cms_return_total = $cms_return_total_result[0];

    //페이징
    $page_type  = isset($_GET['ord_page_type']) ? intval($_GET['ord_page_type']) : "10";
    $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= $page_type;
    $offset 	= ($pages-1) * $num;
    $pagenum 	= ceil($cms_return_total/$num);

    if ($pages >= $pagenum){$pages = $pagenum;}
    if ($pages <= 0){$pages = 1;}

    $search_url = getenv("QUERY_STRING");
    $page		= pagelist($pages, "work_cms_return_list.php", $pagenum, $search_url);

    $smarty->assign("search_url", $search_url);
    $smarty->assign("total_num", $cms_return_total);
    $smarty->assign("pagelist", $page);
    $smarty->assign("ord_page_type", $page_type);

    # GET ORDER Number
    $cms_return_ord_sql    = "SELECT DISTINCT r.order_number FROM work_cms_return r WHERE {$add_where} AND r.order_number is not null ORDER BY {$add_orderby} LIMIT {$offset},{$num}";
    $cms_return_ord_query  = mysqli_query($my_db, $cms_return_ord_sql);
    $order_number_list  = [];
    while($order_number = mysqli_fetch_assoc($cms_return_ord_query)){
        $order_number_list[] =  "'".$order_number['order_number']."'";
    }

    $order_numbers = implode(',', $order_number_list);

    # 리스트 쿼리
    $cms_return_sql = "
		SELECT
		    *,
		    DATE_FORMAT(r.regdate, '%Y-%m-%d') as reg_date,
		    DATE_FORMAT(r.regdate, '%H:%i') as reg_hour,
		 	DATE_FORMAT(r.return_cus_date, '%Y-%m-%d %H:%i') as cus_date,
		 	IF(r.zip_code, CONCAT('[',r.zip_code,']'), '') as postcode,
		 	(SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=r.dp_c_no) as dp_c_name,
			(SELECT s.s_name FROM staff s WHERE s.s_no=r.req_s_no) as req_s_name,
		 	(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=r.prd_no))) AS k_prd1_name,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=r.prd_no)) AS k_prd2_name,
		    (SELECT k_parent FROM kind k WHERE k.k_name_code=r.bad_reason) AS bad_reason_g1,
            (SELECT title from product_cms prd_cms where prd_cms.prd_no=r.prd_no) as prd_name,
            (SELECT s.s_name FROM staff s WHERE s.s_no=r.run_s_no) as run_s_name,
            (SELECT s.s_name FROM staff s WHERE s.s_no=r.run_with_no) as with_name
		FROM
			work_cms_return r
		WHERE {$add_where} AND r.order_number IN({$order_numbers})
		ORDER BY {$add_orderby}
	";

    $cms_return_list        = [];
    $dp_state_option        = getDpStateOption();

    $cms_result = mysqli_query($my_db, $cms_return_sql);
    if(!!$cms_result)
    {
        while($cms_return = mysqli_fetch_assoc($cms_result))
        {
            $product = array(
                'r_no'              => $cms_return['r_no'],
                'c_name'            => $cms_return['c_name'],
                'k_prd1_name'       => $cms_return['k_prd1_name'],
                'k_prd2_name'       => $cms_return['k_prd2_name'],
                'prd_no'            => $cms_return['prd_no'],
                'prd_name'          => $cms_return['prd_name'],
                'quantity'          => $cms_return['quantity'],
                'return_purpose'    => $cms_return['return_purpose'],
                'return_reason'     => $cms_return['return_reason'],
                'cus_memo'          => $cms_return['cus_memo'],
                'return_type'       => $cms_return['return_type'],
                'result_reason'     => $cms_return['result_reason'],
                'bad_reason_g1'     => $cms_return['bad_reason_g1'],
                'bad_reason_g2_list'=> !empty($cms_return['bad_reason_g1']) ? $bad_reason_total_list[$cms_return['bad_reason_g1']] : [],
                'bad_reason'        => $cms_return['bad_reason'],
            );

            $cms_return['return_delivery_fee'] = number_format($cms_return['return_delivery_fee'], 0);
            if(!isset($cms_return_list[$cms_return['order_number']]))
            {
                if(isset($cms_return['recipient_hp']) && !empty($cms_return['recipient_hp'])){
                    $f_hp  = substr($cms_return['recipient_hp'],0,4);
                    $e_hp  = substr($cms_return['recipient_hp'],7,15);
                    $cms_return['recipient_sc_hp'] = $f_hp."***".$e_hp;
                }

                $r_link_url = $cms_return['return_delivery_type'] == 'CJ대한통운' ? "https://trace.cjlogistics.com/next/tracking.html?wblNo=" : "https://www.hanjin.co.kr/kor/CMS/DeliveryMgr/WaybillResult.do?mCode=MN038&schLang=KR&wblnumText2=";
                $cms_return['r_link']   = (isset($cms_return['return_delivery_no']) && !empty($cms_return['return_delivery_no'])) ? $r_link_url.$cms_return['return_delivery_no'] : "";
                $cms_return['r_link2']  = (isset($cms_return['return_delivery_no2']) && !empty($cms_return['return_delivery_no2'])) ? $r_link_url.$cms_return['return_delivery_no2'] : "";

                $delivery_sql   = "SELECT delivery_type, delivery_no FROM work_cms_delivery WHERE order_number = '{$cms_return['parent_order_number']}' GROUP BY delivery_no";
                $delivery_query = mysqli_query($my_db, $delivery_sql);
                $delivery_list  = [];
                while($delivery = mysqli_fetch_assoc($delivery_query))
                {
                    $delivery_list[] = $delivery;
                }
                $cms_return['parent_delivery_list'] = $delivery_list;

                $cms_return_list[$cms_return['order_number']] = $cms_return;
            }

            $cms_return_list[$cms_return['order_number']]['prd_list'][] = $product;
        }
    }

    foreach($cms_return_list as $cms_returns)
    {
        $order_number       = $cms_returns['order_number'];
        $prev_order_number  = $cms_returns['parent_order_number'];
        $return_unit_sql    = "SELECT wcr.u_no, wcr.type, wcr.option, wcr.sku, wcr.quantity FROM work_cms_return_unit wcr WHERE wcr.order_number = '{$order_number}' ";
        $return_unit_query  = mysqli_query($my_db, $return_unit_sql);
        $return_unit_list   = [];
        $return_idx         = 1;
        while($return_unit  = mysqli_fetch_assoc($return_unit_query))
        {
            $return_unit_list[$return_unit['u_no']]['unit_idx']     = "{$order_number}_{$return_idx}";
            $return_unit_list[$return_unit['u_no']]['option_no']    = $return_unit['option'];
            $return_unit_list[$return_unit['u_no']]['option_name']  = $return_unit['sku'];
            $return_unit_list[$return_unit['u_no']]['quantity']     = $return_unit['quantity'];
            $return_unit_list[$return_unit['u_no']]['type']         = $return_unit['type'];

            $return_idx++;
        }

        $cms_return_list[$order_number]['return_unit_list'] = $return_unit_list;
        $cms_return_list[$order_number]['return_unit_cnt']  = count($return_unit_list)+1;

        # 입금리스트
        $cms_return_deposit_sql     = "SELECT w.dp_no, w.dp_price_vat, (SELECT dp.dp_state FROM deposit dp WHERE dp.dp_no=w.dp_no) as dp_state FROM `work` as w WHERE prd_no='260' AND work_state='6' AND work_state='6' AND dp_no > 0  AND linked_table='work_cms' AND linked_shop_no='{$prev_order_number}'";
        $cms_return_deposit_query   = mysqli_query($my_db, $cms_return_deposit_sql);
        $cms_return_deposit_list    = [];
        while($cms_return_deposit = mysqli_fetch_assoc($cms_return_deposit_query)){
            $price = isset($cms_return_deposit['dp_price_vat']) && !empty($cms_return_deposit['dp_price_vat']) ? $cms_return_deposit['dp_price_vat']  : 0;
            $state = isset($cms_return_deposit['dp_state']) && !empty($cms_return_deposit['dp_state']) ? "[".$dp_state_option[$cms_return_deposit['dp_state']]."]" : "[대기]";
            $cms_return_deposit_list[$cms_return_deposit['dp_no']] = array('price' => $price, 'state' => $state);
        }

        $cms_return_list[$order_number]['deposit_list'] = $cms_return_deposit_list;
    }

    $staff_model        = Staff::Factory();
    $cs_manager_list    = $staff_model->getActiveTeamStaff("00244");
    $company_model      = Company::Factory();
    $dp_company_option  = $company_model->getDpDisplayList();

    $smarty->assign("return_state_option", getReturnStateOption());
    $smarty->assign("return_state_color_option", getReturnStateColorOption());
    $smarty->assign("return_purpose_option", getReturnPurposeOption());
    $smarty->assign("return_unit_type_option", getReturnUnitTypeOption());
    $smarty->assign("return_image_option", getReturnImageOption());
    $smarty->assign("return_delivery_type_option", getReturnDeliveryTypeOption());
    $smarty->assign("return_delivery_fee_option", getReturnDeliveryFeeOption());
    $smarty->assign("run_state_option", getRunStateOption());
    $smarty->assign("return_type_option", getReturnTypeOption());
    $smarty->assign("return_reason_option", getReturnReasonOption());
    $smarty->assign("result_reason_option", getResultReasonOption());
    $smarty->assign("dp_company_option", $dp_company_option);
    $smarty->assign("page_type_option", getPageTypeOption('4'));
    $smarty->assign("cs_manager_list", $cs_manager_list);
    $smarty->assign("cms_return_list", $cms_return_list);

    $smarty->display('work_cms_return_list.html');
}
?>
