<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Evaluation.php');

$process 		= isset($_POST['process']) ? $_POST['process'] : "";
$ev_model 		= Evaluation::Factory();
$ev_sys_model 	= Evaluation::Factory();
$ev_model->setMainInit("evaluation_receiver_self", "esr_no");

if($process == 'save_self_ev')
{
	$ev_no			= (isset($_POST['ev_no'])) ? $_POST['ev_no'] : "";
	$rec_s_no		= (isset($_POST['rec_s_no'])) ? $_POST['rec_s_no'] : "";
	$esr_nos		= (isset($_POST['esr_no'])) ? $_POST['esr_no'] : "";
	$ev_u_nos		= (isset($_POST['ev_u_no'])) ? $_POST['ev_u_no'] : "";
	$questions		= (isset($_POST['question'])) ? $_POST['question'] : "";
	$descriptions	= (isset($_POST['description'])) ? $_POST['description'] : "";
	$answers		= (isset($_POST['answer'])) ? $_POST['answer'] : "";
	$save_data		= [];
	$save_type		= "ins";

	if(!empty($ev_u_nos))
	{
		$idx = 0;
		foreach($esr_nos as $esr_no){
			$ev_u_no_val 		= $ev_u_nos[$idx];
			$question_val 		= addslashes(trim($questions[$idx]));
			$description_val 	= addslashes(trim($descriptions[$idx]));
			$answer_val 		= addslashes(trim($answers[$idx]));

			if(!empty($esr_no)){
				$save_data[] = array(
					"esr_no" 		=> $esr_no,
					"ev_no"			=> $ev_no,
					"ev_u_no"		=> $ev_u_no_val,
					"rec_s_no"		=> $rec_s_no,
					"question"		=> $question_val,
					"description"	=> $description_val,
					"answer"		=> $answer_val,
				);
				$save_type = "upd";
			}else{
				$save_data[] = array(
					"ev_no"			=> $ev_no,
					"ev_u_no"		=> $ev_u_no_val,
					"rec_s_no"		=> $rec_s_no,
					"question"		=> $question_val,
					"description"	=> $description_val,
					"answer"		=> $answer_val,
				);
			}
			$idx++;
		}
	}

	$result = false;
	if($save_type == 'upd'){
		if($ev_model->multiUpdate($save_data)){
			$result = true;
		}
	}elseif($save_type == 'ins'){
		if($ev_model->multiInsert($save_data)){
			$result = true;
		}
	}

	if(!$result){
		exit("<script>alert('ev_no = {$ev_no} 자기평가기술서 저장에 실패했습니다.');location.href='evaluation_self_regist.php?ev_no={$ev_no}&rec_s_no={$rec_s_no}';</script>");
	}else{
		exit("<script>alert('저장했습니다.');window.close();</script>");
	}
}

$ev_no	 	= isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$rec_s_no 	= isset($_GET['rec_s_no']) ? $_GET['rec_s_no'] : "";
$self_mode 	= isset($_GET['self_mode']) ? $_GET['self_mode'] : "";
$cur_date 	= date("Y-m-d");

if($ev_no == "" && $rec_s_no == ""){
	exit("<script>alert('작성할 수 없습니다.');window.close();</script>");
}

$ev_system = $ev_sys_model->getItem($ev_no);
if($ev_system['ev_state'] != 7){
	$self_mode = "read";
}else{
	if($ev_system['chk_s_date'] > $cur_date || $cur_date > $ev_system['chk_e_date']){
		$self_mode = "read";
	}
}

$evaluation_unit_sql 		= "SELECT * FROM evaluation_unit WHERE `evaluation_state`='100' AND ev_u_set_no IN(SELECT ev_u_set_no FROM evaluation_system WHERE ev_no='{$ev_no}') AND active='1' ORDER BY `order`";
$evaluation_unit_query 		= mysqli_query($my_db, $evaluation_unit_sql);
$evaluation_question_list 	= [];
while($evaluation_unit = mysqli_fetch_assoc($evaluation_unit_query))
{
	$evaluation_question_list[$evaluation_unit['ev_u_no']] = array("question" => $evaluation_unit["question"], "description" => $evaluation_unit["description"]);
}

$self_ev_sql 	= "SELECT * FROM evaluation_receiver_self WHERE ev_no = '{$ev_no}' AND rec_s_no = '{$rec_s_no}'";
$self_ev_query 	= mysqli_query($my_db, $self_ev_sql);
$self_ev_count  = mysqli_num_rows($self_ev_query);
$self_ev_list	= [];
if($self_ev_count > 0)
{
	while ($self_ev_result = mysqli_fetch_array($self_ev_query))
	{
		$self_ev_list[$self_ev_result['ev_u_no']] = array(
			"esr_no"		=> $self_ev_result['esr_no'],
			"ev_no" 		=> $self_ev_result['ev_no'],
			"ev_u_no" 		=> $self_ev_result['ev_u_no'],
			"question" 		=> $self_ev_result['question'],
			"description" 	=> $self_ev_result['description'],
			"answer" 		=> $self_ev_result['answer'],
		);
	}
}

$smarty->assign("ev_no", $ev_no);
$smarty->assign("rec_s_no", $rec_s_no);
$smarty->assign("self_mode", $self_mode);
$smarty->assign("self_ev_count", $self_ev_count);
$smarty->assign("evaluation_question_list", $evaluation_question_list);
$smarty->assign("self_ev_list", $self_ev_list);

$smarty->display('evaluation_self_regist.html');

?>
