<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Company.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "no")
	->setCellValue('B1', "일자")
	->setCellValue('C1', "이동형태")
	->setCellValue('D1', "이동상태")
	->setCellValue('E1', "공급업체")
	->setCellValue('F1', "와이즈 구성품명")
	->setCellValue('G1', "반출::물류업체")
    ->setCellValue('H1', "반출::재고관리코드(SKU)")
	->setCellValue('I1', "반출창고")
	->setCellValue('J1', "반출수량")
	->setCellValue('K1', "반입::물류업체")
	->setCellValue('L1', "반입::재고관리코드(SKU)")
    ->setCellValue('M1', "반입창고")
	->setCellValue('N1', "반입수량")
;


# 검색조건
$add_where = "1=1";
$add_order = "pcst.no ASC";

$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));
$years_val  = date('Y-m-d', strtotime('-2 years'));

$sch_no                 = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_move_s_date        = isset($_GET['sch_move_s_date']) ? $_GET['sch_move_s_date'] : $months_val;
$sch_move_e_date        = isset($_GET['sch_move_e_date']) ? $_GET['sch_move_e_date'] : $today_val;
$sch_t_type             = isset($_GET['sch_t_type']) ? $_GET['sch_t_type'] : "";
$sch_t_state            = isset($_GET['sch_t_state']) ? $_GET['sch_t_state'] : "";
$sch_sup_c_no           = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_option_name        = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_option_complete    = isset($_GET['sch_option_complete']) ? $_GET['sch_option_complete'] : "";
$sch_out_log_company    = isset($_GET['sch_out_log_company']) ? $_GET['sch_out_log_company'] : "";
$sch_out_warehouse      = isset($_GET['sch_out_warehouse']) ? $_GET['sch_out_warehouse'] : "";
$sch_out_sku            = isset($_GET['sch_out_sku']) ? $_GET['sch_out_sku'] : "";
$sch_in_log_company     = isset($_GET['sch_in_log_company']) ? $_GET['sch_in_log_company'] : "";
$sch_in_warehouse       = isset($_GET['sch_in_warehouse']) ? $_GET['sch_in_warehouse'] : "";
$sch_in_sku             = isset($_GET['sch_in_sku']) ? $_GET['sch_in_sku'] : "";
$sch_file_no            = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";
$sch_move_date_type     = isset($_GET['sch_move_date_type']) ? $_GET['sch_move_date_type'] : "months";

if (!empty($sch_no)) {
	$add_where .= " AND pcst.no='{$sch_no}'";
}

if (!empty($sch_move_s_date)) {
	$add_where .= " AND pcst.move_date >= '{$sch_move_s_date}'";
}

if (!empty($sch_move_e_date)) {
	$add_where .= " AND pcst.move_date <= '{$sch_move_e_date}'";
}

if (!empty($sch_t_type)) {
	$add_where .= " AND pcst.t_type='{$sch_t_type}'";
}

if (!empty($sch_t_state)) {
	$add_where .= " AND pcst.t_state='{$sch_t_state}'";
}

if (!empty($sch_sup_c_no)) {
	$add_where .= " AND pcst.sup_c_no = '{$sch_sup_c_no}'";
}

if (!empty($sch_option_name)) {
	if($sch_option_complete == '1'){
		$add_where .= " AND pcst.option_name = '{$sch_option_name}'";
	}else{
		$add_where .= " AND pcst.option_name like '%{$sch_option_name}%'";
	}
}

if (!empty($sch_out_log_company)) {
	$add_where .= " AND pcst.out_log_c_no = '{$sch_out_log_company}'";
}

if (!empty($sch_out_warehouse)) {
	$add_where .= " AND pcst.out_warehouse like '%{$sch_out_warehouse}%'";
}

if (!empty($sch_out_sku)) {
	$add_where .= " AND pcst.out_sku like '%{$sch_out_sku}%'";
}

if (!empty($sch_in_log_company)) {
	$add_where .= " AND pcst.in_log_c_no = '{$sch_in_log_company}'";
}

if (!empty($sch_in_warehouse)) {
	$add_where .= " AND pcst.in_warehouse like '%{$sch_in_warehouse}%'";
}

if (!empty($sch_in_sku)) {
	$add_where .= " AND pcst.in_sku like '%{$sch_in_sku}%'";
}

if (!empty($sch_file_no)) {
	$add_where .= " AND pcst.file_no ='{$sch_file_no}'";
}

# 쿼리
$stock_transfer_sql = "
    SELECT
        *,
        (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcst.sup_c_no) as sup_c_name
    FROM product_cms_stock_transfer as pcst
    WHERE {$add_where}
    ORDER BY {$add_order}
";
$stock_transfer_query 	= mysqli_query($my_db, $stock_transfer_sql);
$company_model			= Company::Factory();
$log_company_list     	= $company_model->getLogisticsList();
$idx = 2;
if(!!$stock_transfer_query)
{
    while($stock_transfer = mysqli_fetch_array($stock_transfer_query))
    {
		$stock_transfer['out_log_c_name']   = $log_company_list[$stock_transfer['out_log_c_no']];
		$stock_transfer['in_log_c_name']    = $log_company_list[$stock_transfer['in_log_c_no']];

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $stock_transfer['no'])
            ->setCellValue("B{$idx}", $stock_transfer['move_date'])
            ->setCellValue("C{$idx}", $stock_transfer['t_type'])
            ->setCellValue("D{$idx}", $stock_transfer['t_state'])
            ->setCellValue("E{$idx}", $stock_transfer['sup_c_name'])
            ->setCellValue("F{$idx}", $stock_transfer['option_name'])
            ->setCellValue("G{$idx}", $stock_transfer['out_log_c_name'])
            ->setCellValue("H{$idx}", $stock_transfer['out_sku'])
            ->setCellValue("I{$idx}", $stock_transfer['out_warehouse'])
            ->setCellValue("J{$idx}", $stock_transfer['out_qty'])
			->setCellValue("K{$idx}", $stock_transfer['in_log_c_name'])
			->setCellValue("L{$idx}", $stock_transfer['in_sku'])
			->setCellValue("M{$idx}", $stock_transfer['in_warehouse'])
			->setCellValue("N{$idx}", $stock_transfer['in_qty'])
		;
		$idx++;
    }
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00343a40');
$objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A2:N{$idx}")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("A1:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:N{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setWrapText(true);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);

$objPHPExcel->getActiveSheet()->getStyle("A2:N{$idx}")->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->setTitle('재고이동 리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_재고이동 리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
