<?php

require('inc/common.php');

ini_set('max_execution_time', 500);
$init_mass_cms = "DROP TABLE IF EXISTS `mass_cms`";

mysqli_query($my_db, $init_mass_cms);


$mass_cms = "CREATE TABLE `mass_cms`(
    `m_no` INT(20) NOT NULL AUTO_INCREMENT COMMENT '고유번호',
    `recipient` VARCHAR(50) NULL COMMENT '수령자',
    `recipient_hp` VARCHAR(50) NULL COMMENT '수령자 전화번호',
    `zip_code` VARCHAR(50) NULL COMMENT '우편번호',
    `recipient_addr` TEXT NULL COMMENT '수령지',
    `c_name` VARCHAR(50) NULL COMMENT '업체명',
    `prd_no` INT(11) NOT NULL COMMENT '상품번호',
    `order_number` VARCHAR(50) NULL COMMENT '주문번호',
    `group` VARCHAR(100) NULL COMMENT '그룹조건',
    `sum_quantity` INT(11) NULL DEFAULT '0' COMMENT '수량합계',
    `sum_price` INT(11) NULL DEFAULT '0' COMMENT '금액합계',
    PRIMARY KEY (`m_no`) USING BTREE,
    INDEX `prd_no` (`prd_no`) USING BTREE,
    INDEX `group` (`group`) USING BTREE,
    INDEX `sum_quantity` (`sum_quantity`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=INNODB";

mysqli_query($my_db, $mass_cms);

$add_group_by_list = array(
    array(
        'group' => "recipient, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, recipient_hp, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, recipient_hp, zip_code, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, recipient_hp, recipient_addr, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, recipient_hp, zip_code, recipient_addr, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, zip_code, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, zip_code, recipient_addr, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, recipient_addr, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, order_number, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, recipient_hp, order_number, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, recipient_hp, zip_code, order_number, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, recipient_hp, recipient_addr, order_number, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, recipient_hp, zip_code, recipient_addr, order_number, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, zip_code, order_number, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, zip_code, recipient_addr, order_number, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient, recipient_addr, order_number, prd_no",
        'where' => " AND (w.recipient IS NOT NULL AND w.recipient != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient_hp, prd_no",
        'where' => " AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient_hp, zip_code, prd_no",
        'where' => " AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient_hp, zip_code, recipient_addr, prd_no",
        'where' => " AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '')  AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '')AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient_hp, recipient_addr, prd_no",
        'where' => " AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient_hp, order_number, prd_no",
        'where' => " AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient_hp, zip_code, order_number, prd_no",
        'where' => " AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient_hp, zip_code, recipient_addr, order_number, prd_no",
        'where' => " AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient_hp, recipient_addr, order_number, prd_no",
        'where' => " AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "zip_code, prd_no",
        'where' => " AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "zip_code, recipient_addr, prd_no",
        'where' => " AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "zip_code, order_number, prd_no",
        'where' => " AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "zip_code, recipient_addr, order_number, prd_no",
        'where' => " AND (w.zip_code IS NOT NULL AND w.zip_code != '') AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient_addr, prd_no",
        'where' => " AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
    array(
        'group' => "recipient_addr, order_number, prd_no",
        'where' => " AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '') AND (w.order_number is not null AND w.order_number != '') AND (w.prd_no IS NOT NULL AND w.prd_no != '')"
    ),
);

foreach($add_group_by_list as $add_group_by)
{
    $add_group = $add_group_by['group'];
    $add_where = $add_group_by['where'];
    $mass_ins     = "INSERT INTO `mass_cms`(recipient, recipient_hp, zip_code, recipient_addr, c_name, prd_no, order_number, `group`, sum_quantity, sum_price) (SELECT w.recipient, w.recipient_hp, w.zip_code, w.recipient_addr, w.c_name, w.prd_no, w.order_number, '{$add_group}', SUM(w.quantity) AS sum_quantity, SUM(w.dp_price_vat) AS sum_price FROM work_cms w WHERE 1=1 {$add_where} GROUP BY {$add_group} HAVING sum_quantity >= 10) ";
    mysqli_query($my_db, $mass_ins);
}

exit("<script>alert('데이터가 반영 되었습니다.');location.href='work_cms_mass_search_list.php';</script>");