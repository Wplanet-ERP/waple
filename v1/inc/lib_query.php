<?php
// -------------------------------------------------- 사용 확인 -------------------------------------------------- //

// query 를 보내주면 관련된 항목 갯수만  플력해주는 함수
function getcnt($query)
{
	global $baseurl;
	require($baseurl.'libs/dbconfig.php');
	//echo "<br><br>".$query."<br>";
	$rs = mysqli_query($my_db, "select count(*) as cnt from $query ");
	while($data = mysqli_fetch_array($rs)){
		return $data['cnt'];
	}
}

// 부가세 자동계산
function getPriceVat($my_db, $f_out_c_no, $f_price, $f_prd_no)
{
	$auto_price_vat = 0;
	//법인회사 개인회사 구분하여 부가세 자동계산 Start
	$company_sql="SELECT c.license_type,
											c.c_no,
											(SELECT prd.equally_vat FROM product prd WHERE prd.prd_no='".$f_prd_no."') AS equally_vat
							FROM company c
							WHERE c.c_no=".$f_out_c_no."
							";
	$company_result = mysqli_query($my_db,$company_sql);
	$result = mysqli_fetch_array($company_result);
	$license_type = $result['license_type'];
	$out_c_no = $result['c_no'];
	$equally_vat = $result['equally_vat'];

	//내부 작업건의 경우 출금요청액(VAT 포함)을 0으로 처리
	if($out_c_no == '5'){ // c_no = 5 와이즈플래닛
		$auto_price_vat = 0;
	}elseif($equally_vat == '1'){ //상품속성에 VAT동일 시
		$auto_price_vat = $f_price;
	}else{
		if($license_type == '1' || $license_type == '2'){ // 법인회사 or 개인회사 (부가세처리)
			$auto_price_vat = ($f_price*1.1);
		}elseif($license_type == '3'){ // 개인
			$auto_price_vat = ($f_price*0.967);
		}
	}
	//법인회사 개인회사 구분하여 부가세 자동계산 End
	return $auto_price_vat;
}

// 부가세 포함
function getReversePriceVat($my_db, $f_out_c_no, $f_price_vat, $f_prd_no)
{
	$auto_price = $f_price_vat;
	//법인회사 개인회사 구분하여 부가세 자동계산 Start
	$company_sql="SELECT c.license_type,
											c.c_no,
											(SELECT prd.equally_vat FROM product prd WHERE prd.prd_no='".$f_prd_no."') AS equally_vat
							FROM company c
							WHERE c.c_no=".$f_out_c_no."
							";
	$company_result = mysqli_query($my_db,$company_sql);
	$result = mysqli_fetch_array($company_result);
	$license_type = $result['license_type'];
	$out_c_no = $result['c_no'];
	$equally_vat = $result['equally_vat'];

	//내부 작업건의 경우 출금요청액(VAT 포함)을 0으로 처리
	if($out_c_no == '5'){ // c_no = 5 와이즈플래닛
		$auto_price = $f_price_vat;
	}elseif($equally_vat == '1'){ //상품속성에 VAT동일 시
		$auto_price = $f_price_vat;
	}else{
		if($license_type == '1' || $license_type == '2'){ // 법인회사 or 개인회사 (부가세처리)
			$tax_ratio 	= 10;
			$amount_tax = round($f_price_vat / ((100 + $tax_ratio) / 10));
			$auto_price = (int)$f_price_vat - (int)$amount_tax;

		}elseif($license_type == '3'){ // 개인
			$tax_ratio 	 = -3.3;
			$amount_total = floor(($f_price_vat/((100 + $tax_ratio)/100))/10)*10;
			$auto_price   = (int)$amount_total;
		}
	}
	//법인회사 개인회사 구분하여 부가세 자동계산 End
	return $auto_price;
}

function getTeamFullName($my_db, $t_depth, $t_code)
{
    if($t_depth == 0){
        $t_depth_sql = "SELECT depth FROM team WHERE team_code='{$t_code}'";
        $t_depth_query = mysqli_query($my_db, $t_depth_sql);
        $t_depth_result = mysqli_fetch_assoc($t_depth_query);
        $t_depth = isset($t_depth_result['depth']) ? $t_depth_result['depth'] : 1;
    }

    $t_label = "";
    $t_label_list = [];
    for($i=0; $i<$t_depth; $i++)
    {
        if(empty($t_code)){
            break;
        }
        $t_label_sql    = "SELECT t.team_name, t.team_code_parent FROM team t WHERE t.team_code='{$t_code}'";
        $t_label_query  = mysqli_query($my_db, $t_label_sql);
        $t_label_result = mysqli_fetch_assoc($t_label_query);

        $t_code = $t_label_result['team_code_parent'];
        $t_label_list[$i] = $t_label_result['team_name'];
    }

    if($t_label_list){
        krsort($t_label_list);
        $t_label = implode(" > ", $t_label_list);
    }

    return $t_label;
}

function getTeamWhere($my_db, $team_code)
{
    $team_chk_list_val = "";

    $team_chk_sql    = "SELECT team_group, depth, (SELECT max(sub_t.depth) FROM team sub_t WHERE sub_t.team_group=t.team_group LIMIT 1) as max_depth FROM team t WHERE team_code='{$team_code}' LIMIT 1";
    $team_chk_query  = mysqli_query($my_db, $team_chk_sql);
    $team_chk_result = mysqli_fetch_assoc($team_chk_query);

    $team_chk_list 		= array($team_code);
    $team_chk_max_depth = $team_chk_result['max_depth'];
    $team_chk_depth     = $team_chk_result['depth'];
    if($team_chk_depth <  $team_chk_max_depth)
    {
        $team_parent_list = array($team_code);
        for($i=$team_chk_depth;$i<$team_chk_max_depth;$i++)
        {
            $team_parent_list_val = implode(",", $team_parent_list);
            $team_parent_list = [];
            $chk_sql = "SELECT team_code FROM team WHERE team_code_parent IN({$team_parent_list_val})";
            $chk_query = mysqli_query($my_db, $chk_sql);
            while($chk_result = mysqli_fetch_assoc($chk_query)){
                $team_parent_list[] = $chk_result['team_code'];
                $team_chk_list[] = $chk_result['team_code'];
            }
        }
    }

    if($team_chk_list){
        $team_chk_list_val = implode(",", $team_chk_list);
    }

    return $team_chk_list_val;
}

function getTeamLeader($my_db)
{
    $team_leader_sql   = "SELECT team_code, team_group, depth, (SELECT max(sub_t.depth) FROM team sub_t WHERE sub_t.team_group=t.team_group LIMIT 1) as max_depth, team_leader FROM team t WHERE team_leader != '' AND team_code != '00218'";
    $team_leader_query = mysqli_query($my_db, $team_leader_sql);

    $team_leader_list = [];
    while($team_leader_result = mysqli_fetch_assoc($team_leader_query))
    {
        $t_depth     = $team_leader_result['depth'];
        $t_max_depth = $team_leader_result['max_depth'];
        $team_code   = $team_leader_result['team_code'];
        $team_leader = $team_leader_result['team_leader'];
        $team_parent_list = array($team_code);
        $team_leader_list[$team_leader][] = $team_code;
        for($i=$t_depth;$i<$t_max_depth;$i++)
        {
            $team_parent_list_val = implode(",", $team_parent_list);
            $team_parent_list = [];
            $chk_sql = "SELECT team_code, team_leader FROM team WHERE team_code_parent IN({$team_parent_list_val}) AND display = '1'";
            $chk_query = mysqli_query($my_db, $chk_sql);
            while($chk_result = mysqli_fetch_assoc($chk_query))
            {
                $team_parent_list[] = $chk_result['team_code'];
                $team_leader_list[$team_leader][] = $chk_result['team_code'];

                if(!empty($chk_result['team_leader']))
                {
                    $team_leader_list[$chk_result['team_leader']][] = $chk_result['team_code'];
                    $team_leader_list[$chk_result['team_leader']] = array_unique($team_leader_list[$chk_result['team_leader']]);
                }
            }
        }

        $team_leader_list[$team_leader] = array_unique($team_leader_list[$team_leader]);
    }

    return $team_leader_list;
}

function get_vl($filed,$tbname,$where,$getvl)
{
    global $baseurl;
    require($baseurl.'libs/dbconfig.php');
    if ($stmt = $my_db->prepare("select $filed from $tbname WHERE $where = ? ")) {
        $stmt->bind_param("s", $getvl);
        $stmt->execute();
        $stmt->bind_result($filed);
        while ($stmt->fetch()) {
            return $filed;
        }
        $stmt->close();
        $my_db->close();
    }
}

function getTeamName($my_db, $team_code)
{
    $team_code_sql    = "SELECT team_name FROM team t WHERE team_code = '{$team_code}' LIMIT 1";
    $team_code_query  = mysqli_query($my_db, $team_code_sql);
    $team_code_result = mysqli_fetch_assoc($team_code_query);
    $team_code        = $team_code_result['team_name'];

    return $team_code;
}

function twoDimensionTotalSortDesc($array_a, $array_b)
{
    if($array_a['total'] == $array_b['total']){
        return 0;
    }

    return ($array_a['total'] > $array_b['total']) ? -1 : 1;
}

function twoDimensionTotalSortAsc($array_a, $array_b)
{
    if($array_a['total'] == $array_b['total']){
        return 0;
    }

    return ($array_a['total'] > $array_b['total']) ? 1 : -1;
}
?>
