<?php

    /* 업무상품 리스트 1차, 2차 START */
    $work_prd_list_sql   = "
            SELECT
                k_name, k_name_code, k_parent
            FROM kind
            WHERE k_code='product' AND display = 1
            ORDER BY priority ASC
        ";

    $work_list_query = mysqli_query($my_db, $work_prd_list_sql);
    $work_list       = [];
    $work_k_names	 = [];
    $work_name_list  = [];
    while($work = mysqli_fetch_array($work_list_query))
    {
        $work_list[$work['k_parent']][] = array(
            'k_name'        => $work['k_name'],
            'k_name_code'   => $work['k_name_code'],
            'k_parent' 		=> $work['k_parent']
        );
        $work_k_names[] = $work['k_name_code'];
        $work_name_list[$work['k_name_code']] = $work['k_name'];
    }
    /* 업무상품 리스트 1차, 2차 END */

    /* 업무상품 리스트 3차 START */
    $work_k_names = implode(',',$work_k_names);
    $word_prd_list_sql   = "
                SELECT
                    title, prd_no, k_name_code
                FROM product
                WHERE display='1' AND k_name_code IN({$work_k_names})";

    $work_prd_list_query = mysqli_query($my_db, $word_prd_list_sql);
    $work_prd_list       = [];
    $work_prd_name_list  = [];
    while($work_prd = mysqli_fetch_array($work_prd_list_query))
    {
        $work_prd_list[$work_prd['k_name_code']][] = array(
            'title'       => $work_prd['title'],
            'prd_no'      => $work_prd['prd_no'],
            'k_name_code' => $work_prd['k_name_code']
        );

        $work_prd_name_list[$work_prd['prd_no']] = $work_prd['title'];
    }
    /* 업무상품 리스트 3차 END */

    /* 상품 DATA(1차, 2차(kind Table: k_code(product_cms)&k_parent=null), 3차 START*/
    $prd_group_sql = "
            SELECT 
                k_name,
                k_name_code,
                k_parent
            FROM kind 
            WHERE k_code='product_cms' ORDER BY priority ASC
        ";

    $prd_group_code      = [];
    $prd_group_list      = [];
    $prd_total_list      = [];
    $prd_group_name_list = [];
    $prd_name_list       = [];

    $prd_group_query = mysqli_query($my_db, $prd_group_sql);
    while($prd_group = mysqli_fetch_array($prd_group_query))
    {
        $prd_group_code[] = $prd_group['k_name_code'];
        $prd_group_list[$prd_group['k_parent']][] = array(
            "k_name"	  => trim($prd_group['k_name']),
            "k_name_code" => trim($prd_group['k_name_code'])
        );
        $prd_group_name_list[$prd_group['k_name_code']] = $prd_group['k_name'];
    }
    $smarty->assign("prd_group_list", $prd_group_list);

    $prd_group_code = implode(",", $prd_group_code);
    $prd_total_sql = "
            SELECT
                prd_no,
                title,
                k_name_code
            FROM product_cms
            WHERE k_name_code in($prd_group_code)
        ";

    $prd_total_query = mysqli_query($my_db, $prd_total_sql);
    while($prd_data = mysqli_fetch_array($prd_total_query))
    {
        $prd_total_list[$prd_data['k_name_code']][] = array(
            'prd_no'        => trim($prd_data['prd_no']),
            'title'         => trim($prd_data['title']),
            'k_name_code'   => trim($prd_data['k_name_code'])
        );

        $prd_name_list[$prd_data['prd_no']] = $prd_data['title'];
    }
    $smarty->assign("prd_total_list", $prd_total_list);
    $smarty->assign("prd_group_name_list", $prd_group_name_list);
    $smarty->assign("prd_name_list", $prd_name_list);
    /* 상품 DATA END */

    /* 회사 팀 리스트 START */
    $team_list_sql   = "SELECT team_name, team_code, team_code_parent, depth FROM team WHERE display = 1 ORDER BY priority";
    $team_list_query = mysqli_query($my_db, $team_list_sql);
    $team_list       = [];
    $sch_team_name_list = array("all" => ":: 전체 ::");
    while($team = mysqli_fetch_array($team_list_query))
    {
        $team_prev = "";
        if($team['depth'] > 1){
            for($i=1;$i<$team['depth'];$i++){
                $team_prev .= ">";
            }
        }
        $sch_team_name_list[$team['team_code']] = empty($team_prev) ? $team['team_name'] : " {$team_prev} {$team['team_name']}";
    }
    /* 회사 팀 리스트 END */

    /* CHART COLOR */
    $color_list = array("#000000", "#006400", "#00FA9A", "#000080", "#00BFFF", "#4169E1", "#483D8B", "#556B2F", "#800000", "#D8BFD8", "#BDB76B", "#FFD700", "#191970", "#008000", "#00FF00", "#00008B", "#00CED1", "#4682B4", "#4B0082", "#696969", "#8B0000", "#708090", "#DDA0DD", "#CD853F", "#228B22", "#00FF7F", "#0000CD", "#00FFFF", "#5F9EA0", "#663399", "#808000", "#8B4513", "#778899", "#FF00FF", "#D2691E", "#2E8B57", "#7CFC00", "#0000FF", "#00FFFF", "#6495ED", "#6A5ACD", "#808080", "#A0522D", "#8FBC8F", "#FF00FF", "#D2B48C", "#2F4F4F", "#7FFF00", "#008080", "#1E90FF", "#66CDAA", "#7B68EE", "#A9A9A9", "#A52A2A", "#FF1493", "#DAA520", "#32CD32", "#90EE90", "#008B8B", "#20B2AA", "#800080", "#BC8F8F", "#B22222", "#CD5C5C", "#DEB887", "#3CB371", "#98FB98", "#40E0D0", "#8A2BE2", "#C0C0C0", "#B8860B", "#DA70D6", "#E9967A", "#6B8E23", "#9ACD32", "#48D1CC", "#8B008B", "#D3D3D3", "#C71585", "#EE82EE", "#F4A460", "#ADFF2F", "#7FFFD4", "#9370DB", "#DCDCDC", "#DB7093", "#F08080", "#F5DEB3", "#87CEEB", "#9400D3", "#DC143C", "#FA8072", "#FFB6C1", "#87CEFA", "#9932CC", "#FF0000", "#FF4500", "#FFC0CB", "#ADD8E6", "#BA55D3", "#FF6347", "#FFDAB9", "#AFEEEE", "#FF69B4", "#FFDEAD", "#B0C4DE", "#FF7F50", "#FFE4B5", "#B0E0E6", "#FF8C00", "#FFE4C4", "#FFA07A", "#FFA500");
    $smarty->assign('color_list', $color_list);


    /* Custom Cart X축, Y축, Z축 */
    $type_list = array(
        'day'      => '날짜',
        'product'  => '상품',
        'staff'    => '업체담당자',
        'dp'       => '구매처',
        'date'     => '요일',
        'ord_time' => '주문시간',
        'state'    => '지역'
    );

    $z_type_list = array(
        '1' => '금액(매출)',
        '2' => '건(주문번호)',
        '3' => '건(수량)'
    );

    $ord_type_list = array(
        '0' => '합계(내림차순)',
        '1' => '합계(올림차순)',
        '2' => 'X축(내림차순)',
        '3' => 'X축(올림차순)',
        '4' => 'Y축(내림차순)',
        '5' => 'Y축(올림차순)'
    );

    $type_day = array(
        '1' => '날짜(월별)',
        '2' => '날짜(주별)',
        '3' => '날짜(일별)'
    );

    $type_date = array(
        '1' => '월요일',
        '2' => '화요일',
        '3' => '수요일',
        '4' => '목요일',
        '5' => '금요일',
        '6' => '토요일',
        '0' => '일요일'
    );

    $type_ord_time = array(
        '1' => '1시간격(00~01시)',
        '2' => '2시간격(00~02시)'
    );

    $type_state = array(
        'gangwon-do'        => '강원도',
        'gyeonggi-do'       => '경기도',
        'gyeongsangnam-do'  => '경상남도',
        'gyeongsangbuk-do'  => '경상북도',
        'gwangju'           => '광주광역시',
        'daegu'             => '대구광역시',
        'daejeon'           => '대전광역시',
        'busan'             => '부산광역시',
        'seoul'             => '서울특별시',
        'sejong-si'         => '세종특별자치시',
        'ulsan'             => '울산광역시',
        'incheon'           => '인천광역시',
        'Jeollanam-do'      => '전라남도',
        'jeollabuk-do'      => '전라북도',
        'jeju-do'           => '제주특별자치도',
        'chungcheongnam-do' => '충청남도',
        'chungcheongbuk-do' => '충청북도'
    );

    $dp_sql    = "SELECT c_no, c_name FROM company WHERE dp_e_type='1' AND display='1' ORDER BY c_name ASC";
    $dp_query  = mysqli_query($my_db, $dp_sql);
    $type_dp   = [];

    while($dp_result = mysqli_fetch_assoc($dp_query))
    {
        $dp_c_name = $dp_result['c_name'];
        if($dp_result['c_name'] == '시티파이'){
            $dp_c_name = "시티파이(아임웹)";
        }

        $type_dp[$dp_result['c_no']] = $dp_c_name;
    }

    /* Work Custom Cart X축, Y축, Z축 */
    $work_type_list = array(
        'day'           => '날짜',
        'work'          => '업무',
        'staff'         => '업체담당자',
        'task_req'      => '업무요청자',
        'task_run'      => '업무처리자',
        'dp_comp'       => '입금업체',
        'wd_comp'       => '출금업체',
        'task_req_eval' => '엽무요청자 평가',
        'task_run_eval' => '엽무처리자 평가',
    );

    $work_type_day = array(
        '1' => '날짜(월별)',
        '2' => '날짜(주별)',
        '3' => '날짜(일별)'
    );

    $type_dp_comp = [];
    $dp_comp_sql = "SELECT DISTINCT dp_c_no, (SELECT sub.c_name FROM company sub WHERE sub.c_no = dp_c_no) as dp_c_name FROM work WHERE work_state='6' AND dp_c_no IS NOT NULL ORDER BY dp_c_name ASC";
    $dp_comp_query = mysqli_query($my_db, $dp_comp_sql);
    while($dp_comp = mysqli_fetch_assoc($dp_comp_query))
    {
        $type_dp_comp[$dp_comp['dp_c_no']] = $dp_comp['dp_c_name'];
    }

    $type_wd_comp = [];
    $wd_comp_sql = "SELECT DISTINCT wd_c_no, (SELECT sub.c_name FROM company sub WHERE sub.c_no = wd_c_no) as wd_c_name FROM work WHERE work_state='6' AND wd_c_no IS NOT NULL ORDER BY wd_c_name ASC";
    $wd_comp_query = mysqli_query($my_db, $wd_comp_sql);
    while($wd_comp = mysqli_fetch_assoc($wd_comp_query))
    {
        $type_wd_comp[$wd_comp['wd_c_no']] = $wd_comp['wd_c_name'];
    }

    $type_task_eval = array(
        "5" => "★★★★★",
        "4" => "★★★★☆",
        "3" => "★★★☆☆",
        "2" => "★★☆☆☆",
        "1" => "★☆☆☆☆"
    );

    $work_z_type_list = array(
        '1' => '건(업무번호 w_no)',
        '2' => '건(수량)',
        '3' => '시간(work time)',
        '4' => '금액(기여매출액 Service Yes)',
        '5' => '금액(판매가 Service No)'
    );


?>
