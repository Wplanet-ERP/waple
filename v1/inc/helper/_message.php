<?php

function getCInfoOption()
{
    return array(
        "1"     => array("sms_title" => "탑블로그[마감]", "option_title" => "마감예정 알림"),
        "2"     => array("sms_title" => "탑블로그[선정]", "option_title" => "마감예정 알림"),
        "10"    => array("sms_title" => "탑블로그[일반]", "option_title" => "탑블로그[일반]"),
        "3"     => array("sms_title" => "직원간 SMS", "option_title" => "직원간 SMS"),
        "4"     => array("sms_title" => "CMS 관리", "option_title" => "CMS 관리"),
        "9"     => array("sms_title" => "퀵/화물 관리", "option_title" => "퀵/화물 관리"),
        "5"     => array("sms_title" => "개인경비 잔액확인 문자", "option_title" => "개인경비 잔액확인 문자"),
        "6"     => array("sms_title" => "강의/멘토링 관리", "option_title" => "강의/멘토링 관리"),
        "7"     => array("sms_title" => "업무관리", "option_title" => "업무관리"),
        "13"    => array("sms_title" => "자산관리", "option_title" => "자산관리"),
        "8"     => array("sms_title" => "재고관리", "option_title" => "재고관리"),
        "11"    => array("sms_title" => "평가관리", "option_title" => "평가관리"),
        "12"    => array("sms_title" => "일정관리", "option_title" => "일정관리"),
        "77"    => array("sms_title" => "이벤트 종료알림", "option_title" => "이벤트 종료알림"),
        "88"    => array("sms_title" => "CMS 예약문자", "option_title" => "CMS 예약문자"),
        "100"   => array("sms_title" => "CS 문자", "option_title" => "CS 문자"),
        "91"    => array("sms_title" => "입고일정확정 알림", "option_title" => "입고일정확정 알림"),
        "92"    => array("sms_title" => "해외 제조사 입고 배차정보 알림", "option_title" => "해외 제조사 입고 알림"),
        "99"    => array("sms_title" => "기타", "option_title" => "기타"),
    );
}

function getMessageStateOption()
{
    return array(
        "0"     => "대기",
        "1"     => "발송중",
        "2"     => "발송완료",
        "3"     => "에러",
        "4"     => "확인불가",
    );
}

function getMessageCallStatusOption()
{
    return array(
        "K000"  => "전송완료", "K001"  => "수신불가 사용자", "K102" => "전화번호 오류", "K103" => "메세지 길이제한 오류",
        "K104"  => "템플릿 확인불가", "K105"  => "템플릿 내용매칭 오류", "K106" => "링크 오류", "K107" => "유효하지 않은 ID",
        "K108"  => "템플릿 버튼 오류", "K109"  => "템플릿 강조매칭 오류", "K110" => "템플릿 강조길이 오류", "K111" => "템플릿 강조타입 오류",
        "K200"  => "발신 프로필 오류", "K201"  => "삭제된 발신프로필", "K202" => "차단된 발신프로필", "K203" => "차단된 카카오톡 채널",
        "K204"  => "닫힌 카카오톡 채널", "K205"  => "삭제된 카카오톡 채널", "K999" => "알림톡 시스템 오류",
        "M000"  => "전성완료", "M001"  => "발송 처리중", "M203" => "착신번호 오류", "M990" => "발송 지연으로 전송실패",
        "R000"  => "예약대기", "R109"  => "예약 중복",
    );
}

function getCrmSendNameOption()
{
    $crm_send_name_option = array(
        "1" => '베라베프(belabef)',
        "2" => '닥터피엘',
    );

    return $crm_send_name_option;
}

function getCrmStateOption()
{
    $crm_state_option = array(
        "1" => "작성중",
        "2" => "진행중",
        "3" => "종료",
        "4" => "종료(예약중단)"
    );

    return $crm_state_option;
}

function getCrmTempTypeOption()
{
    $crm_temp_type_option = array(
        "AT"  => "알림톡",
        "SMS" => "SMS",
    );

    return $crm_temp_type_option;
}

function getCrmIncomeTypeOption()
{
    $crm_income_type_option = array(
        "1" => "구매(상품)",
        "2" => "구매(구성품목)"
    );
    return $crm_income_type_option;
}

function getCrmSendTypeOption()
{
    $crm_send_type_option = array(
        "1" => "1회",
        "2" => "반복"
    );

    return $crm_send_type_option;
}

function getCrmSendTimeOption()
{
    $crm_send_time_option = array(
        "1" => "발송처리일"
    );

    return $crm_send_time_option;
}

function getCrmReservationStateOption()
{
    $crm_r_state_option = array(
        "1" => "발송에약",
        "2" => "발송중",
        "3" => "발송완료",
        "4" => "발송실패",
        "5" => "취소"
    );

    return $crm_r_state_option;
}

function getCrmBtnTypeOption()
{
    $crm_btn_type_option = array(
        "WL" => "웹링크",
        "AC" => "채널추가",
    );

    return $crm_btn_type_option;
}

function getCrmSendMethodOption()
{
    $crm_send_method_option = array(
        "1" => "자동발송",
    );

    return $crm_send_method_option;
}

function getDefaultRefusePhone()
{
    $default_refuse_phone = "080-880-1378";

    return $default_refuse_phone;
}

function getBizStatusOption()
{
    return array("0" => "대기", "1" => "발송 중", "2" => "발송 완료");
}

function getBizCallStatusOption()
{
    return array(
        "7000" => "전달", "7101" => "카카오 형식 오류", "7103" => "Sender key (발신프로필키) 유효하지 않음", "7106" => "삭제된 Sender key (발신프로필키)", "7107" => "차단 상태 Sender key (발신프로필키)", "7108" => "차단 상태 옐로우 아이디", "7109" => "닫힌 상태 옐로우 아이디", "7110" => "삭제된 옐로우 아이디", "7111" => "삭제대기 상태의 플러스친구 (플러스친구 운영툴에서 확인)", "7112" => "유효하지 않은 사업자번호", "7125" => "메시지차단 상태의 플러스친구 (플러스친구 운영툴에서 확인)", "7203" => "친구톡 전송 시 친구대상 아님",
        "7204" => "템플릿 불일치", "7206" => "시리얼넘버 형식 불일치", "7300" => "기타에러", "7305" => "성공불확실(30 일 이내 수신 가능)",
        "7306" => "카카오 시스템 오류", "7308" => "전화번호 오류", "7311" => "메시지가 존재하지 않음", "7314" => "메시지 길이 초과",
        "7315" => "템플릿 없음", "7318" => "메시지를 전송할 수 없음", "7322" => "메시지 발송 불가 시간", "7324" => "재전송 메시지 존재하지 않음",
        "7325" => "변수 글자수 제한 초과", "7326" => "상담/봇 전환 버튼 extra, event 글자수 제한 초과", "7327" => "버튼 내용이 템플릿과 일치하지 않음",
        "7328" => "메시지 강조 표기 타이틀이 템플릿과 일치하지 않음", "7329" => "메시지 강조 표기 타이틀 길이 제한 초과 (50 자)","7421" => "타임아웃", "7521" => "중복발신제", "9070" => "잔액부족"
    );
}

function getCertStatusOption()
{
    return array(
        "1" => "진행중",
        "2" => "해지완료"
    );
}

function getCertPrdOption()
{
    return array(
        "1" => "퓨어팟",
        "2" => "에코 피처형 정수기",
        "3" => "직수정수기",
    );
}

function getCertPrdDetailOption()
{
    return array(
        "1" => array("1" => array("title" => "퓨어팟 :: 본품 (교체주기 12개월)", "key" => "1-1")),
        "2" => array("1" => array("title" => "에코 피처형 정수기 :: 리필 필터 (교체주기 4개월)", "key" => "2-1")),
        "3" => array("1" => array("title" => "직수정수기 :: 1차 필터 (교체주기 6개월)", "key" => "3-1") , "2" => array("title" => "직수정수기 :: 2차 필터 & 3차필터 (교체주기 12개월)", "key" => "3-2")),
    );
}

function getCertPrdNameTermInfo()
{
    return array(
        "1" => array("1" => array("title" => "퓨어팟 본품", "term" => "12")),
        "2" => array("1" => array("title" => "에코 피처형 정수기 리필필터", "term" => "4")),
        "3" => array("1" => array("title" => "직수정수기 1차필터", "term" => "6"), "2" => array("title" => "직수정수기 2차&3차 필터", "term" => "12")),
    );
}

function getCertTempNoList()
{
    return array(
        "1" => array("1" => 26),
        "2" => array("1" => 26),
        "3" => array("1" => 26, "2" => 26),
    );
}

function getCsSendTempList()
{
    return array(
        "1" => "주방용 교환 요청시",
        "2" => "아이레놀 교환 요청시 ",
        "3" => "교환건 닥터피엘 채널톡 이관 안내",
        "4" => "교환건 누잠 채널톡 이관 안내",
        "5" => "교환건 베라베프 채널톡 이관 안내",
        "6" => "일반",
    );
}

function getWapleSendPhoneList(){
    return array(
        "02-2675-6260"  => "탑블로그",
        "02-830-1912"   => "회사",
        "070-7873-1373" => "김윤영",
        "1668-3620"     => "커머스 고객센터",
    );
}