<?php

function getVisitStateOption()
{
    $visit_state_option = array(
        '1' => '작성중',
        '2' => '대기',
        '3' => '진행요청',
        '4' => '접수완료',
        '5' => '진행중',
        '6' => '진행완료',
        '7' => '보류',
        '8' => '취소완료',
        '9' => '진행불가'
    );

    return $visit_state_option;
}

function getVisitStateButtonOption()
{
    $visit_state_btn_option = array(
        "3" => "진행요청",
        "5" => "진행중",
        '8' => '취소완료',
        '7' => '보류'
    );

    return $visit_state_btn_option;
}

function getVisitStepOption()
{
    $visit_step_option   = array('센터완료', '본사검토중', '본사확인', '발주', '방문약속', '방문후 취소', '방문전 취소', '발주취소', '센터보류');

    return $visit_step_option;
}

function getVisitStepMatchingOption()
{
    $visit_step_matching = array(
        '센터완료' => '6',
        '본사검토중' => '6',
        '본사확인' => '6',
        '발주' => '5',
        '방문약속' => '5'   ,
        '방문후 취소' => '8',
        '방문전 취소' => '8',
        '방문전취소' => '8',
        '방문후취소' => '8',
        '발주취소' => '8',
        '센터보류' => '7'
    );

    return $visit_step_matching;
}

function getVisitPrdCodeOption()
{
    $prd_code_option   = array('코스트제로', '코스트제로 (자가설치)');

    return $prd_code_option;
}

function getVisitCodeOption()
{
    $visit_code_option = array('신규설치', '신규유상', '이전설치', '재설치', '유상A/S', '무상A/S', '수리회수', '수리설치', '제품교체');

    return $visit_code_option;
}

function getVisitFreeOption()
{
    $visit_free_option = array('유상', '무상');

    return $visit_free_option;
}