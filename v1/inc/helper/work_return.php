<?php

function getReturnStateOption()
{
    $return_state_option = array(
        '1' => '회수요청',
        '2' => '접수완료',
        '3' => '회수운송장완료',
        '4' => '회수완료',
        '5' => '진행완료(아임웹미적용)',
        '6' => '진행완료',
        '7' => '고객취소',
        '8' => '보류',
        '9' => '입고완료(검수대기)'
    );

    return $return_state_option;
}

function getReturnStateColorOption()
{
    $return_state_color_option = array(
        '1' => 'red',
        '2' => 'blue',
        '3' => 'green',
        '4' => 'green',
        '5' => 'green',
        '6' => 'black',
        '7' => 'red',
        '8' => 'red',
        '9' => 'blue',
    );

    return $return_state_color_option;
}

function getRunStateOption()
{
    $run_state_option = array(
        '1' => '환불계좌작성완료',
        '2' => '구매확정취소완료',
        '3' => '오배송회수',
        '4' => '진행중',
        '5' => '진행완료',
    );

    return $run_state_option;
}


//회수목적
function getReturnPurposeOption()
{
    $return_purpose_option = array(
        '1' => '교환',
        '2' => '반품',
        '3' => '미확인'
    );

    return $return_purpose_option;
}

function getReturnReasonOption()
{
    $return_reason_option = array(
        '1'     => '구매 의사 취소',
        '2'     => '다른 상품 잘못 주문',
        '3'     => '색상 및 사이즈 변경',
        '4'     => '배송누락',
        '5'     => '오배송',
        '6'     => '상품 파손',
        '7'     => '서비스 및 상품 불만족',
        '8'     => '상품 정보 상이',
        '9'     => '기타',
        '11'    => '제품불량',
        '10'    => '미확인',
        '12'    => '물류측 오배송',
        '13'    => '배송사 분실',
        '14'    => '배송 파손'
    );

    return $return_reason_option;
}


function getResultReasonOption()
{
    $result_reason_option = array(
        '1'     => '구매 의사 취소',
        '2'     => '다른 상품 잘못 주문',
        '3'     => '색상 및 사이즈 변경',
        '4'     => '배송누락',
        '5'     => '오배송',
        '6'     => '상품파손',
        '7'     => '서비스 및 상품 불만족',
        '8'     => '상품 정보 상이',
        '9'     => '기타',
        '11'    => '제품불량',
        '10'    => '미확인',
        '12'    => '물류측 오배송',
        '13'    => '배송사 분실',
        '14'    => '배송 파손'
    );

    return $result_reason_option;
}


//회수요청 구분
function getReturnTypeOption()
{
    $return_type_option = array(
        '0' => '없음',
        '1' => '조회불가',
        '2' => '수거 후 제조사 전달',
        '3' => '특이사항',
        '4' => '수거 후 본사 전달',
    );

    return $return_type_option;
}

function getReturnDeliveryTypeOption()
{
    $return_delivery_type_option = array('한진택배','CJ대한통운','로젠택배','편의점택배','우체국택배','롯데택배','기타');

    return $return_delivery_type_option;
}

function getReturnImageOption()
{
    $return_image_option = array(
        '1' => "있음",
        '2' => "없음(1~2개)",
        '3' => "없음(1개)",
        '4' => "없음(2개)",
    );

    return $return_image_option;
}

function getReturnDeliveryFeeOption()
{
    $return_delivery_fee_option = array(
        '1' => "있음",
        '2' => "없음"
    );

    return $return_delivery_fee_option;
}

function getReturnUnitTypeOption()
{
    $return_unit_type_option = array(
        '1' => '정상',
        '2' => '불량',
        '3' => '재포장',
        '4' => '업체전달',
        '5' => '누락',
        '6' => '회수제외',
        '7' => '타사제품'
    );

    return $return_unit_type_option;
}