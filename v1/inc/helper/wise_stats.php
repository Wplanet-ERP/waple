<?php

function getKindTypeOption()
{
    $kind_type_option = array(
        "1" => "year",
        "2" => "month",
        "3" => "week",
        "4" => "day"
    );

    return $kind_type_option;
}

function getKindTypeNameOption()
{
    $kind_type_option = array(
        "year"  => "연간",
        "month" => "월간",
        "week"  => "주간",
        "day"   => "일간"
    );

    return $kind_type_option;
}

function getStaffTypeOption()
{
    $staff_type_option = array(
        "s_no"           => "업체 담당자",
        "task_req_s_no"  => "업무 요청자",
        "task_run_s_no"  => "업무 처리자"
    );

    return $staff_type_option;
}

function getTeamTypeOption()
{
    $staff_type_option = array(
        "s_no"           => "team",
        "task_req_s_no"  => "task_req_team",
        "task_run_s_no"  => "task_run_team"
    );

    return $staff_type_option;
}

function getCsKindOption()
{
    $cs_kind_option = array(
        "1" => "구매",
        "2" => "CS 및 교환"
    );

    return $cs_kind_option;
}

?>
