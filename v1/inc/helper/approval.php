<?php

function getPermissionOption()
{
    return array(
        '1' => '생성/수정 가능',
        '2' => '생성/수정 불가',
    );
}

function getApprovalTypeOption()
{
    return array(
        'approval'   => '결재',
        'conference' => '순차합의',
    );
}

function getApprovalStateOption()
{
    return array(
        '1' => '작성중',
        '2' => '진행중',
        '3' => '결재완료',
        '4' => '반려',
        '5' => '보류'
    );
}

function getApprovalStateReadOption()
{
    return array('1' => 0, '2' => 0, '3'=> 0, '4' => 0, '5' => 0);
}

function getInboxStateOption()
{
    return array(
        '2_1'   => '결재대기',
        '2_2'   => '합의대기',
        '2_3'   => '진행중',
        '3'     => '결재완료',
        '4'     => '반려',
        '5'     => '보류'
    );
}

function getLinkedTableOption()
{
    return array(
        'work'              => '업무관리',
        'commerce_order'    => '발주관리',
        'project_expenses'  => '사업비 지출관리'
    );
}

function getInboxStateReadOption()
{
    return array('2_1' => 0, '2_2' => 0, '2_3'=> 0, '3'=> 0, '4' => 0, '5' => 0);
}

function getLeaveTypeList()
{
    return array(
        "1" => array("name" => "연차", "lm_no" => "1"),
        "2" => array("name" => "오전반차", "lm_no" => "1"),
        "3" => array("name" => "오후반차", "lm_no" => "1"),
        "4" => array("name" => "입사기념 오전반차", "lm_no" => "2"),
        "5" => array("name" => "입사기념 오후반차", "lm_no" => "2"),
        "6" => array("name" => "휴일대체", "lm_no" => "3"),
        "7" => array("name" => "경조휴가", "lm_no" => "4"),
        "8" => array("name" => "안식년휴가", "lm_no" => "5"),
        "9" => array("name" => "공가(예비군)", "lm_no" => "6"),
    );
}