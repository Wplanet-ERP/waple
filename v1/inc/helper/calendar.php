<?php

function getCalendarPermissionOption()
{
    $permission_option = array(
        "1" => "전체공개",
        "2" => "지정공개",
    );

    return $permission_option;
}

function getViewSchedulePermissionSetOption()
{
    $view_schedule_permission_set_option = array(
        "0" => "비공개",
        "1" => "공개",
        "2" => "지정공개",
    );

    return $view_schedule_permission_set_option;
}

function getCalendarDateOption()
{
    $date_option = array(
        "1" => "시간",
        "2" => "종일",
    );

    return $date_option;
}

function getRepeatValOption()
{
    $repeat_val_list = array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30");

    return $repeat_val_list;
}

function getRepeatTypeOption()
{
    $repeat_type_list = array("1" => "매일","2" => "매주", "3" => "매월");

    return $repeat_type_list;
}

function getSearchTypeOption()
{
    $search_type_option = array(
        "1" => "제목/내용",
        "2" => "제목",
        "3" => "내용"
    );

    return $search_type_option;
}

function getBrandTypeOption()
{
    return array(
        "1" => "설정방식",
//        "2" => "검색방식",
    );
}