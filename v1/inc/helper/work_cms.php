<?php

function getDeliveryStateOption()
{
    $delivery_state_option = array(
        '1'  => '진행요청',
        '2'  => '접수완료',
        '3'  => '진행중',
        '7'  => '운송장번호변경',
        '8'  => '운송장완료',
        '4'  => '진행완료',
        '5'  => '고객취소',
        '6'  => '보류',
        '9'  => '작성중',
        '10' => '작성완료(검수요청)',
    );

    return $delivery_state_option;
}

function getDeliveryStateColorOption()
{
    $delivery_state_color_list = array(
        '1' => array('label' => '진행요청', 'bg' => 'yellow', 'color' => 'red'),
        '2' => array('label' => '접수완료', 'bg' => 'white', 'color' => 'blue'),
        '3' => array('label' => '진행중', 'bg' => 'white', 'color' => 'green'),
        '7' => array('label' => '운송장번호변경', 'bg' => 'yellow', 'color' => 'red'),
        '8' => array('label' => '운송장완료', 'bg' => 'white', 'color' => 'black'),
        '4' => array('label' => '진행완료', 'bg' => 'white', 'color' => 'black'),
        '5' => array('label' => '고객취소', 'bg' => 'lightgray', 'color' => 'red'),
        '6' => array('label' => '보류', 'bg' => 'lightgray', 'color' => 'red'),
        '9' => array('label' => '작성중', 'bg' => 'white', 'color' => 'red'),
        '10' => array('label' => '작성완료(검수요청)', 'bg' => 'white', 'color' => 'blue'),
    );

    return $delivery_state_color_list;
}

function getAutoDpCsOption()
{
    $dp_cs_option = array('2007' => 'CS', '2008' => '교환');

    return $dp_cs_option;
}

function getChoiceCompanyOption()
{
    $choice_c_option = array(
        "1"     => "아임웹_베라베프",
        "17"    => "아임웹_닥터피엘",
        "8"     => "아임웹_와이즈웰",
        "20"    => "아임웹_아이레놀",
        "21"    => "아임웹_채식주의",
        "22"    => "아임웹_누잠",
        "91"    => "아임웹_베라베프_v2",
        "92"    => "아임웹_닥터피엘_v2",
        "93"    => "아임웹_와이즈웰_v2",
        "2"     => "스마트스토어_닥터피엘",
        "5"     => "스마트스토어_베라베프",
        "6"     => "스마트스토어_누잠",
        "10"    => "스마트스토어_아이레놀",
        "24"    => "스마트스토어_채식주의",
        "3"     => "사방넷",
        "11"    => "올리브영(누잠_에이원비앤에이치)",
        "4"     => "위드플레이스",
        "13"    => "알리익스프레스",
        "14"    => "아이레놀(공동구매)",
        "16"    => "온더룩 판매자센터",
        "18"    => "택배리스트 업로드 기본양식",
        "19"    => "롯데홈쇼핑",
        "23"    => "후추",
    );

    return $choice_c_option;
}

function getTrackTypeOption()
{
    $track_type_option = array(
        "1" => "일반",
        "2" => "반품"
    );

    return $track_type_option;
}

function getTrackFeeOption()
{
    $track_fee_option = array(
        "1" => "있음",
        "2" => "없음"
    );

    return $track_fee_option;
}

function getQuestionTypeOption()
{
    $question_type_option = array(
        "1" => "구매경로",
        "2" => "마케팅수신동의",
        "3" => "기타",
    );

    return $question_type_option;
}

function getAdditionDpList()
{
    $additional_dp_list = array(
        "1372" => "아임웹",
        "3294" => "브릴리스킨(아임웹)"
    );

    return $additional_dp_list;
}

function getRandIdx($min, $max)
{
    return rand($min, $max);
}

function combinedExceptCheck($order_products, $excepts)
{
    $result = false;
    foreach($order_products as $order_product)
    {
        if(in_array($order_product['prd_no'], $excepts)){
            $result = true;
            break;
        }
    }

    return $result;
}

function getDpCsList()
{
    $dp_cs_list = array('2007' => 'CS', '2008' => '교환', '5127' => 'CS_물류측 오배송', '5128' => 'CS_물류측 누락', '5134' => 'CS_물류측 오배송(교환)', '5135' => 'CS_물류측 누락(교환)', '2005' => '직접구매', '5899' => 'CS_배송사 분실', '5900' => 'CS_배송 파손', '5901' => 'CS_배송 파손(교환)', '5939' => 'WISE_잡손실');

    return $dp_cs_list;
}

function getCsTypeOption()
{
    $cs_type_option = array(
        '1' => '교환',
        '2' => '반품',
        '3' => '미확인',
    );

    return $cs_type_option;
}

function getCsIssueOption()
{
    $cs_issue_option = array(
        '1' => '유선응대',
        '2' => '케어링시스템',
        '3' => '보상요구'
    );

    return $cs_issue_option;
}

function getIsRefundOption()
{
    $is_refund_option = array(
        "1" => "있음",
        "2" => "없음"
    );

    return $is_refund_option;
}

function getCsStatusOption()
{
    $cs_status_option = array(
        '1' => "입금대기",
        '2' => "입금완료",
    );

    return $cs_status_option;
}

function getQuickStateOption()
{
    $quick_state_option = array(
        '1'  => '진행요청',
        '2'  => '접수완료',
        '3'  => '진행중',
        '4'  => '진행완료',
        '5'  => '취소',
        '6'  => '보류'
    );

    return $quick_state_option;
}

function getQuickStateColorOption()
{
    $quick_state_color_list = array(
        '1' => array('label' => '진행요청', 'bg' => 'yellow', 'color' => 'red'),
        '2' => array('label' => '접수완료', 'bg' => 'white', 'color' => 'blue'),
        '3' => array('label' => '진행중', 'bg' => 'white', 'color' => 'green'),
        '4' => array('label' => '진행완료', 'bg' => 'white', 'color' => 'black'),
        '5' => array('label' => '취소', 'bg' => 'lightgray', 'color' => 'red'),
        '6' => array('label' => '보류', 'bg' => 'lightgray', 'color' => 'red'),
    );

    return $quick_state_color_list;
}

function getOrderTypeOption()
{
    return array("택배","방문수령","배송없음");
}

function getVisitTotalOption()
{
    return array('1019','1020','1116','1117','1262','1263','1264','1265','1439','1440','1580','1581','1594','1595','1596','1597');
}

function getVisitSelfOption()
{
    return array('1019','1116','1262','1263','1439','1580','1594','1596');
}

function getCouponTypeOption(){
    return array("1" => "아임웹_베라베프", "3" => "아임웹_닥터피엘", "4" => "아임웹_아이레놀", "5" => "아임웹_누잠", "2" => "스마트스토어");
}

function getCmsPriceType(){
    return array("1" => "유료결제", "2" => "무상제공(0원)");
}

function getTrackType(){
    return array(
        "281"   => "배송비",
        "270"   => "보관료",
        "271"   => "척출료",
        "272"   => "합포장",
        "273"   => "화물 입/출고",
        "274"   => "폐기",
        "275"   => "기타",
    );
}

function getChangeSingleUnitList()
{
    $change_sum_unit_list = array(
        "203" => array("option" => "209", "sku" => "누잠_매트리스_SS_그레이 (단품)"),
        "204" => array("option" => "210", "sku" => "누잠_매트리스_SS_베이지 (단품)"),
        "205" => array("option" => "211", "sku" => "누잠_매트리스_SS_네이비 (단품)"),
        "206" => array("option" => "212", "sku" => "누잠_매트리스_Q_그레이 (단품)"),
        "207" => array("option" => "213", "sku" => "누잠_매트리스_Q_베이지 (단품)"),
        "208" => array("option" => "214", "sku" => "누잠_매트리스_Q_네이비 (단품)"),
        "239" => array("option" => "238", "sku" => "누잠_더블업토퍼_매트리스_SS_그레이 (단품)"),
        "241" => array("option" => "240", "sku" => "누잠_더블업토퍼_매트리스_Q_그레이 (단품)"),
        "340" => array("option" => "334", "sku" => "누잠 대형 리유저블백"),
        "319" => array("option" => "284", "sku" => "누잠 여름이불 (SS 스카이블루)"),
        "320" => array("option" => "285", "sku" => "누잠 여름이불 (SS 쿨그레이)"),
        "321" => array("option" => "286", "sku" => "누잠 여름이불 (Q 스카이블루)"),
        "322" => array("option" => "287", "sku" => "누잠 여름이불 (Q 쿨그레이)"),
        "349" => array("option" => "348", "sku" => "누잠_더블업토퍼_매트리스_하드폼_SS_네이비 (단품)"),
        "351" => array("option" => "350", "sku" => "누잠_더블업토퍼_매트리스_하드폼_Q_네이비 (단품)"),
        "353" => array("option" => "352", "sku" => "누잠_더블업토퍼_하드폼_SS_네이비_BOTTOM (단품)"),
        "355" => array("option" => "354", "sku" => "누잠_더블업토퍼_하드폼_Q_네이비_BOTTOM (단품)"),
        "472" => array("option" => "474", "sku" => "누잠_더블업토퍼_매트리스_하드폼_SS_그레이 (단품)"),
        "473" => array("option" => "475", "sku" => "누잠_더블업토퍼_매트리스_하드폼_Q_그레이 (단품)"),
        "490" => array("option" => "491", "sku" => "누잠_매트리스(세탁망포함)_Q_그레이_단품"),
        "492" => array("option" => "493", "sku" => "누잠_매트리스(세탁망포함)_Q_네이비_단품"),
        "494" => array("option" => "495", "sku" => "누잠_매트리스(세탁망포함)_Q_베이지_단품"),
        "496" => array("option" => "497", "sku" => "누잠_매트리스(세탁망포함)_SS_그레이_단품"),
        "498" => array("option" => "499", "sku" => "누잠_매트리스(세탁망포함)_SS_네이비_단품"),
        "500" => array("option" => "501", "sku" => "누잠_매트리스(세탁망포함)_SS_베이지_단품"),
        "548" => array("option" => "551", "sku" => "누잠_스프링화이버_매트리스_SS_그레이(단품)"),
        "549" => array("option" => "552", "sku" => "누잠_스프링화이버_매트리스_Q_그레이(단품)"),
        "558" => array("option" => "559", "sku" => "누잠_스프링화이버_매트리스_SS_베이지(단품)"),
        "560" => array("option" => "561", "sku" => "누잠_스프링화이버_매트리스_Q_베이지(단품)"),
        "562" => array("option" => "563", "sku" => "누잠_스프링화이버_매트리스_SS_네이비(단품)"),
        "564" => array("option" => "565", "sku" => "누잠_스프링화이버_매트리스_Q_네이비(단품)"),
        "553" => array("option" => "555", "sku" => "더블업_스프링화이버_매트리스_SS_그레이(단품)"),
        "554" => array("option" => "556", "sku" => "더블업_스프링화이버_매트리스_Q_그레이(단품)"),
    );

    return $change_sum_unit_list;
}

function getChangeSubUnitList()
{
    $change_sub_unit_list = array(
        "209" => array("option" => "203", "sku" => "누잠_매트리스_SS_그레이"),
        "210" => array("option" => "204", "sku" => "누잠_매트리스_SS_베이지"),
        "211" => array("option" => "205", "sku" => "누잠_매트리스_SS_네이비"),
        "212" => array("option" => "206", "sku" => "누잠_매트리스_Q_그레이"),
        "213" => array("option" => "207", "sku" => "누잠_매트리스_Q_베이지"),
        "214" => array("option" => "208", "sku" => "누잠_매트리스_Q_네이비"),
        "238" => array("option" => "239", "sku" => "누잠_더블업토퍼_매트리스_SS_그레이"),
        "240" => array("option" => "241", "sku" => "누잠_더블업토퍼_매트리스_Q_그레이"),
        "334" => array("option" => "340", "sku" => "누잠 대형 리유저블백"),
        "348" => array("option" => "349", "sku" => "누잠_더블업토퍼_매트리스_하드폼_SS_네이비"),
        "350" => array("option" => "351", "sku" => "누잠_더블업토퍼_매트리스_하드폼_Q_네이비"),
        "352" => array("option" => "353", "sku" => "누잠_더블업토퍼_하드폼_SS_네이비_BOTTOM"),
        "354" => array("option" => "355", "sku" => "누잠_더블업토퍼_하드폼_Q_네이비_BOTTOM"),
        "474" => array("option" => "472", "sku" => "누잠_더블업토퍼_매트리스_하드폼_SS_그레이"),
        "475" => array("option" => "473", "sku" => "누잠_더블업토퍼_매트리스_하드폼_Q_그레이"),
        "491" => array("option" => "490", "sku" => "누잠_매트리스(세탁망포함)_Q_그레이"),
        "493" => array("option" => "492", "sku" => "누잠_매트리스(세탁망포함)_Q_네이비"),
        "495" => array("option" => "494", "sku" => "누잠_매트리스(세탁망포함)_Q_베이지"),
        "497" => array("option" => "496", "sku" => "누잠_매트리스(세탁망포함)_SS_그레이"),
        "499" => array("option" => "498", "sku" => "누잠_매트리스(세탁망포함)_SS_네이비"),
        "501" => array("option" => "500", "sku" => "누잠_매트리스(세탁망포함)_SS_베이지"),
        "551" => array("option" => "548", "sku" => "누잠_스프링화이버_매트리스_SS_그레이"),
        "552" => array("option" => "549", "sku" => "누잠_스프링화이버_매트리스_Q_그레이"),
        "559" => array("option" => "558", "sku" => "누잠_스프링화이버_매트리스_SS_베이지"),
        "561" => array("option" => "560", "sku" => "누잠_스프링화이버_매트리스_Q_베이지"),
        "563" => array("option" => "562", "sku" => "누잠_스프링화이버_매트리스_SS_네이비"),
        "565" => array("option" => "564", "sku" => "누잠_스프링화이버_매트리스_Q_네이비"),
        "555" => array("option" => "553", "sku" => "더블업_스프링화이버_매트리스_SS_그레이"),
        "556" => array("option" => "554", "sku" => "더블업_스프링화이버_매트리스_Q_그레이"),
    );

    return $change_sub_unit_list;
}

function getReservationStateOption()
{
    return array(
        "1" => "대기",
        "2" => "알림완료",
        "3" => "취소",
        "4" => "주문이동"
    );
}

function getReservationQuickOption()
{
    return array(
        "1" => "대기건 보기",
        "2" => "알림완료건 보기",
    );
}

function getErrorExcelTypeOption()
{
    return array(
        "1" => "아임웹 취소",
        "2" => "아임웹 반품",
        "3" => "아임웹 발주실패",
        "4" => "스마트스토어 취소",
        "5" => "스마트스토어 반품",
        "6" => "스마트스토어 수거",
    );
}

function getNotApplyDpList(){
    return array(2005,2007,2008,3134,4850,4848,4849,5012,5114,5127,5128,5134,5135,5475,5476,5477,5478,5479,5482,5484,5659,5469,5480,5665,5777,5899,5900,5901,5939,6002);
}

function getRateTypeOption(){
    return array(
        "1"   => "전주/전월대비 증감률 (주간/월간에 한함)",
        "2"   => "전년대비 증감률 (월간에 한함)",
    );
}

function getSelfDpCompanyList()
{
    return array(1372,5800,5958,6012,3295,4629,5427,5588);
}

function getSelfDpCompanyNameList()
{
    return array(
        "1372" => "아임웹_베라베프",
        "5800" => "아임웹_닥터피엘",
        "5958" => "아임웹_아이레놀",
        "6012" => "아임웹_누잠",
        "3295" => "스마트스토어_베라베프",
        "4629" => "스마트스토어_누잠",
        "5427" => "스마트스토어_닥터피엘",
        "5588" => "스마트스토어_아이레놀",
    );
}

function getSelfDpImwebCompanyList()
{
    return array(1372,5800,5958,6012);
}

function getSelfDpIlenolImwebCompanyList()
{
    return array(1372,5958);
}

function getTrafficTypeOption()
{
    return array("hour" => "시간별", "day" => "일별", "week" => "주별", "page" => "페이지별");
}

function getTrafficDpCompanyOption()
{
    return array(
        "1372" => array("page_title" => "아임웹_베라베프", "page_url" => "https://belabef.com/"),
        "5800" => array("page_title" => "아임웹_닥터피엘", "page_url" => "https://dr-piel.com/"),
        "5958" => array("page_title" => "아임웹_아이레놀", "page_url" => "https://ilenol.com/"),
        "6012" => array("page_title" => "아임웹_누잠", "page_url" => "https://www.nuzam.com/"),
    );
}

function getIsSettleTypeOption()
{
    return array(
        "1" => "지급완료",
        "2" => "지급미완료"
    );
}

function getStartDpCompanyList()
{
    return array(
        "5659" => array(
            "5661"  => "Rakuten",
            "5795"  => "amazon.co.jp",
            "1816"  => "Qoo10",
            "5662"  => "CS_해외"
        ),
        "5956" => array(
            "5964"  => "Qoo10",
            "5972"  => "아마존_닥터피엘(일본)",
        )
    );
}

function getStartConvertDpCompanyList()
{
    return array(
        "5661"  => "Rakuten_아이레놀(일본)",
        "5795"  => "아마존_아이레놀(일본)",
        "5972"  => "아마존_닥터피엘(일본)",
        "1816"  => "Qoo10_아이레놀(일본)",
        "5964"  => "Qoo10_닥터피엘(일본)",
        "5662"  => "CS_해외"
    );
}

function getGlobalDpCompanyList()
{
    return array(
        "total"     => array("5661", "5795", "1816", "5972", "5964", "5662"),
        "doc"       => array("5972", "5964"),
        "ilenol"    => array("5661", "5795", "1816"),
    );
}

function getCubingPrdOption()
{
    return array(
        "1" => array(362,363,364,1511,1512,1514),
        "2" => array(365,366,367,368,369,370,1515,1516,1517,1518,1519,1520),
        "3" => array(1521,1522,1523,1524,1525,1526,1527,1528,1529,1530),
        "4" => array(371,372,373,374,375,376,377,378,379,380,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,1531,1532,1533,1534,1535,1536,1537,1538,1539,1540,1541,1542,1543,1544,1545),
    );
}