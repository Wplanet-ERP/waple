<?php

# MY Quick Navigation 생성
$my_quick_sql       = "
    SELECT
       nav_name, nav.nav_code, nav.parent_nav_code, nav.nav_level, nav.nav_url
    FROM quick_search AS qs
    LEFT JOIN navigation AS nav ON nav.`no`=qs.prd_no
    WHERE qs.s_no='{$session_s_no}' AND qs.`type`='navigation' AND nav.display = '1' ORDER BY qs.priority ASC";
$my_quick_query     = mysqli_query($my_db, $my_quick_sql);
$my_quick_nav_list  = [];
$my_quick_sub_list  = [];
while($quick_nav = mysqli_fetch_assoc($my_quick_query))
{
    if($quick_nav['nav_level'] == '3'){
        $parent_nav_sql     = "SELECT * FROM navigation WHERE nav_code='{$quick_nav['parent_nav_code']}' AND nav_kind = 'waple' AND display='1' LIMIT 1";
        $parent_nav_query   = mysqli_query($my_db, $parent_nav_sql);
        $parent_nav_result  = mysqli_fetch_assoc($parent_nav_query);

        $my_quick_nav_list[$parent_nav_result['nav_code']] = array(
            "url"   => "/v1/".$parent_nav_result['nav_url'],
            "title" => $parent_nav_result['nav_name'],
        );

        $my_quick_sub_list[$parent_nav_result['nav_code']][$quick_nav['nav_code']] = array(
            "url"   => "/v1/".$quick_nav['nav_url'],
            "title" => $quick_nav['nav_name'],
        );
    }elseif($quick_nav['nav_level'] == '2'){
        $my_quick_nav_list[$quick_nav['nav_code']] = array(
            "url"   => "/v1/".$quick_nav['nav_url'],
            "title" => $quick_nav['nav_name'],
        );

        $child_nav_sql     = "SELECT * FROM navigation WHERE parent_nav_code='{$quick_nav['nav_code']}' AND nav_kind = 'waple' AND display='1' ORDER BY priority ASC";
        $child_nav_query   = mysqli_query($my_db, $child_nav_sql);
        if($child_nav_query)
        {
            while($child_nav_result  = mysqli_fetch_assoc($child_nav_query))
            {
                $my_quick_sub_list[$quick_nav['nav_code']][$child_nav_result['nav_code']] = array(
                    "url"   => "/v1/".$child_nav_result['nav_url'],
                    "title" => $child_nav_result['nav_name'],
                );
            }
        }

    }
}

$smarty->assign("my_quick_nav_list", $my_quick_nav_list);
$smarty->assign("my_quick_sub_list", $my_quick_sub_list);

# Level=1 navigation
$navigation_main_sql = "
    SELECT
        *
    FROM navigation
    WHERE nav_kind = 'waple' AND display='1' AND nav_level='1'
    ORDER BY priority ASC, `no` ASC
";
$navigation_main_query = mysqli_query($my_db, $navigation_main_sql);
$navigation_main_list   = [];
$navigation_sub_list    = [];
$navigation_detail_list = [];
while($navigation_main = mysqli_fetch_array($navigation_main_query))
{
    if($navigation_main['nav_code'] == "approval")
    {
        if($session_s_no != '167' && $session_s_no != '18' && $session_team != '00211'){
            continue;
        }
    }

    $navigation_main_list[$navigation_main['nav_code']] = array(
        "url"   => "/v1/".$navigation_main['nav_url'],
        "title" => $navigation_main['nav_name'],
    );

    # Level=2 navigation
    $navigation_sub_sql = "
        SELECT
            *
        FROM navigation
        WHERE nav_kind = 'waple' AND display='1' AND nav_level='2' AND parent_nav_code='{$navigation_main['nav_code']}'
        ORDER BY priority ASC, `no` ASC
    ";
    $navigation_sub_query = mysqli_query($my_db, $navigation_sub_sql);
    while($navigation_sub = mysqli_fetch_array($navigation_sub_query))
    {
        if($navigation_sub['nav_code'] == "sub_sales")
        {
            if(!permissionNameCheck($session_permission,"재무관리자") && $session_s_no != '171' && $session_s_no != '61'){
                continue;
            }
        }
        elseif($navigation_sub['nav_code'] == "sub_bank_account")
        {
            if(!permissionNameCheck($session_permission,"재무관리자")){
                continue;
            }
        }
        elseif($navigation_sub['nav_code'] == "sub_expenses_result" || $navigation_sub['nav_code'] == "sub_card")
        {
            if(!permissionNameCheck($session_permission,"마스터관리자") && !permissionNameCheck($session_permission,"재무관리자") && !permissionNameCheck($session_permission,"대표")){
                continue;
            }
        }
        elseif($navigation_sub['nav_code'] == "sub_out_withdraw")
        {
            if(!permissionNameCheck($session_permission,"마스터관리자") && !permissionNameCheck($session_permission,"재무관리자")) {
                continue;
            }
        }
        elseif($navigation_sub['nav_code'] == "sub_agency")
        {
            if(!permissionNameCheck($session_permission,"마스터관리자") && !permissionNameCheck($session_permission,"재무관리자") && $session_s_no != '171') {
                continue;
            }
        }
        elseif($navigation_sub['nav_code'] == "sub_staff_management")
        {
            if(!permissionNameCheck($session_permission,"재무관리자") || $session_team != '00211') {
                continue;
            }
        }
        elseif($navigation_sub['nav_code'] == "sub_assignment")
        {
            if(!permissionNameCheck($session_permission,"마스터관리자")){
                continue;
            }
        }
        elseif($navigation_sub['nav_code'] == "sub_board_team")
        {
            if($session_team != '00244' && $session_s_no != '28'){
                continue;
            }
        }
        elseif($navigation_sub['nav_code'] == "sub_board_comment")
        {
            if(!permissionNameCheck($session_permission, "재무관리자") && !permissionNameCheck($session_permission, "마스터관리자")){
                continue;
            }
        }

        $navigation_sub_list[$navigation_main['nav_code']][$navigation_sub['nav_code']] = array(
            "url"   => "/v1/".$navigation_sub['nav_url'],
            "title" => $navigation_sub['nav_name'],
        );

        # Level=3 navigation
        $navigation_sql = "
            SELECT
                *
            FROM navigation
            WHERE nav_kind = 'waple' AND display='1' AND nav_level='3' AND parent_nav_code='{$navigation_sub['nav_code']}'
            ORDER BY priority ASC, `no` ASC
        ";
        $navigation_query = mysqli_query($my_db, $navigation_sql);
        while($navigation = mysqli_fetch_array($navigation_query))
        {
            if($navigation['nav_code'] == "sub_board_home_calendar")
            {
                if($session_team != '00236' && $session_team != '00221' && $session_team != '00222'){
                    continue;
                }
            }

            if($navigation['nav_code'] == "sub_stock_receipt_copy"){
                if(!permissionNameCheck($session_permission, "마스터관리자")){
                    continue;
                }
            }

            $navigation_detail_list[$navigation_sub['nav_code']][] = array(
                "url"   => "/v1/".$navigation['nav_url'],
                "title" => $navigation['nav_name'],
            );
        }
    }
}

$smarty->assign("navigation_main_list", $navigation_main_list);
$smarty->assign("navigation_sub_list", $navigation_sub_list);
$smarty->assign("navigation_detail_list", $navigation_detail_list);

# 가이드 생성
$g_host             = $_SERVER['HTTP_HOST'];
$g_req_main_uri     = $_SERVER['SCRIPT_NAME'];
$g_req_query_uri    = $_SERVER['QUERY_STRING'];
$g_host_url         = $g_host.$g_req_main_uri;

$modal_guide_url_sql 	= "
    SELECT
        bgu.b_no,
        bgu.parameter,
        bgu.new_popup,
        bg.question,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code =(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code = `bg`.k_name_code LIMIT 1)) as guide_g1_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`bg`.manager) AS manager_name,
        (SELECT count(bgr.r_no) FROM board_guide_read bgr WHERE bgr.read_s_no='{$session_s_no}' AND bgr.b_no = bgu.b_no) as read_cnt
    FROM board_guide_url bgu
    LEFT JOIN board_guide bg ON bg.b_no=bgu.b_no
    WHERE bgu.url='{$g_host_url}' AND bg.display='1'
";
$modal_guide_url_query  = mysqli_query($my_db, $modal_guide_url_sql);
$modal_guide_url_list   = [];
$modal_guide_popup      = "";
$modal_guide_cnt        = 0;
$modal_new_guide_cnt    = 0;
while($modal_guide_url_result = mysqli_fetch_assoc($modal_guide_url_query))
{
    $is_chk_url = false;
    if(!empty($modal_guide_url_result['parameter']) && strpos($g_req_query_uri, $modal_guide_url_result['parameter']) !== false){
        $is_chk_url = true;
    }elseif(empty($modal_guide_url_result['parameter'])){
        $is_chk_url = true;
    }

    if($is_chk_url)
    {
        if(!isset($modal_guide_url_list[$modal_guide_url_result['b_no']]))
        {
            $modal_guide_url_list[$modal_guide_url_result['b_no']] = array(
                "guide_g1_name" => $modal_guide_url_result['guide_g1_name'],
                "question"      => $modal_guide_url_result['question'],
                "read_cnt"      => $modal_guide_url_result['read_cnt'],
                "manager_name"  => $modal_guide_url_result['manager_name'],
            );
        }

        if($modal_guide_url_result['read_cnt'] == 0){
            $modal_new_guide_cnt++;
        }
        $modal_guide_cnt++;

        if(empty($modal_guide_popup) &&
            $modal_guide_url_result['new_popup'] == '1' && $modal_guide_url_result['read_cnt'] == 0){
            $modal_guide_popup = $modal_guide_url_result['b_no'];
        }
    }
}

$smarty->assign("modal_guide_popup", $modal_guide_popup);
$smarty->assign("modal_guide_cnt", $modal_guide_cnt);
$smarty->assign("modal_new_guide_cnt", $modal_new_guide_cnt);
$smarty->assign("modal_guide_url_list", $modal_guide_url_list);
?>
