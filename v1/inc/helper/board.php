<?php
function getSecretOption()
{
    $secret_option = array(
        '2' => '공개',
        '1' => '비밀',
    );

    return $secret_option;
}

function getUnknownOption()
{
    $unknown_option = array(
        '2' => '공개',
        '1' => '익명',
    );

    return $unknown_option;
}

function getBrandCategoryOption()
{
    $brand_category_option = array(
        "70001" => "닥터피엘",
        "70002" => "아이레놀",
        "70003" => "누잠",
        "70004" => "시티파이",
        "70008" => "큐빙",
        "70009" => "퓨어팟",
        "70013" => "코스트제로",
        "70014" => "피처형정수기",
    );

    return $brand_category_option;
}

function getBrandCategoryStaffOption()
{
    $brand_category_staff_option = array(
        "17"    => "70001",
        "22"    => "70001",
        "42"    => "70001",
        "145"   => "70001",
        "177"   => "70001",
        "15"    => "70002",
        "4"     => "70003",
        "166"   => "70003",
        "234"   => "70003",
        "147"   => "70004",
        "6"     => "70008",
        "7"     => "70009",
        "76"    => "70013",
    );

    return $brand_category_staff_option;
}