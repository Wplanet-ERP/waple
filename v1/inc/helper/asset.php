<?php

function getAssetUseTypeOption()
{
    $asset_use_type_option = array(
        '1' => '승인제',
        '2' => '비승인제'
    );

    return $asset_use_type_option;
}

function getAssetShareTypeOption()
{
    $asset_share_type_option = array(
        '1' => '개별',
        '2' => '공용',
        '3' => '공용(예약시스템)'
    );

    return $asset_share_type_option;
}

function getAssetObjectTypeOption()
{
    $asset_object_type_option = array(
        '1' => '유형',
        '2' => '무형',
        '3' => '무형(계정)',
    );

    return $asset_object_type_option;
}

function getAssetStateOption()
{
    $asset_state_option = array(
        ''  => array('color' => 'black', 'label' => "전체"),
        '1' => array('color' => 'blue', 'label' => "대기(사용가능)"),
        '2' => array('color' => 'red', 'label' => "대기(사용불가)"),
        '3' => array('color' => 'red', 'label' => "수리중"),
        '4' => array('color' => 'black', 'label' => "폐기"),
        '5' => array('color' => 'red', 'label' => "사용중")
    );

    return $asset_state_option;
}

function getAssetStateNameOption()
{
    $asset_state_name_option = array(
        '1' =>  "사용가능",
        '2' =>  "사용불가",
        '3' =>  "수리중",
        '4' =>  "폐기",
        '5' =>  "사용중",
    );

    return $asset_state_name_option;
}

function getAssetWorkStateOption()
{
    $asset_work_state_option = array(
        ''  => array('color' => 'black', 'label' => "전체"),
        '1' => array('color' => 'red', 'label' => "사용요청중"),
        '2' => array('color' => 'blue', 'label' => "사용승인"),
        '3' => array('color' => 'black', 'label' => "사용불가"),
        '4' => array('color' => 'grey', 'label' => "사용요청취소"),
        '5' => array('color' => 'red', 'label' => "수리요청중"),
        '6' => array('color' => 'grey', 'label' => "수리요청취소"),
        '7' => array('color' => 'blue', 'label' => "수리완료"),
        '8' => array('color' => 'red', 'label' => "반납요청"),
        '9' => array('color' => 'black', 'label' => "반납완료"),
        '10' => array('color' => 'black', 'label' => "사용종료")
    );

    return $asset_work_state_option;
}

function getDefineOption()
{
    $define_option = array("1" => "있음", "2" => "없음");

    return $define_option;
}

function getTermOption()
{
    $term_option = array("1" => "01", "2" => "02", "3" => "03", "4" => "04", "6" => "06", "12" => "12");

    return $term_option;
}

function getAssetQuickOption()
{
    $asset_quick_option = array("1" => "사용요청중", "2" => "사용승인(사용중)", "3" => "사용요청중(승인관리)", "4" => "수리요청건", "5" => "반납요청건");

    return $asset_quick_option;
}