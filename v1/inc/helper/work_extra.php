<?php

function getWorkFormatExtra()
{
    $format_extra_option = array(
        'work_certificate'      => '증명서 발급',
        'logistics_management'  => '물류관리 시스템'
    );

    return $format_extra_option;
}

function getExtraUrlOption()
{
    $extra_url_option = array(
        "promotion"             => "promotion_regist.php?no=",
        "asset"                 => "asset_reservation.php?sch_req_name=&sch_as_r_no=",
        "work_certificate"      => "work_certificate_view.php?wc_no=",
        "logistics_management"  => "logistics_request.php?lm_no=",
    );

    return $extra_url_option;
}

function getCertDocTypeOption()
{
    $doc_type_option = array('1' => '재직증명서', '2' => '경력증명서');

    return $doc_type_option;
}

function getCertCompanySealOption()
{
    $company_seal_option = array(
        "wplanet_A" => array("seal_name" => "플래닛 A", "seal_file" => "wplanet_A.jpg"),
        "wplanet_B" => array("seal_name" => "플래닛 B", "seal_file" => "wplanet_B.png"),
        "media"     => array("seal_name" => "미디어커머스", "seal_file" => "media.png"),
        "partners"  => array("seal_name" => "파트너스", "seal_file" => "partners.png"),
        "empty"     => array("seal_name" => "도장없음", "seal_file" => "")
    );

    return $company_seal_option;
}