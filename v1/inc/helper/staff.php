<?php

function getStaffStateOption()
{
    $staff_state_option = array(
        '1' => 'active',
        '2' => 'freelancer',
        '3' => 'deactivate'
    );

    return $staff_state_option;
}

function getCompanyAddrOption()
{
    $company_addr_option = array(
        "1" => "서울 가산 본사",
        "2" => "세종 지사",
    );

    return $company_addr_option;
}

function getCoreOption()
{
    $core_option = array(
        "5" => "(~2020) 핵심가치 역량평가",
        "9" => "(2021~) 핵심가치 역량평가"
    );

    return $core_option;
}

function getPosPermissionOption()
{
    $pos_permission_option = array(
        '1' => '(기본)',
        '2' => '팀장',
        '3' => '본부장',
        '4' => '이사'
    );

    return $pos_permission_option;
}

function getContractStateOption()
{
    $contract_state_option = array(
        "1" => "진행중",
        "2" => "종료",
    );

    return $contract_state_option;
}

function getStaffResourceTypeOption()
{
    $resource_type_option = array(
        "1" => "신규",
        "2" => "전환",
//        "3" => "전보",
//        "4" => "전배",
//        "5" => "전근",
//        "6" => "전출",
//        "7" => "전적",
        "8" => "강등",
        "9" => "승진",
        "10" => "겸임해제",
    );

    return $resource_type_option;
}

function getStaffMyCompanyOption()
{
    $my_company_option = array(
        "1" => "㈜와이즈플래닛컴퍼니",
        "3" => "㈜와이즈미디어커머스",
        "7" => "㈜와이즈플래닛파트너스"
    );

    return $my_company_option;
}

function getStaffWorkingStateOption()
{
    $staff_work_state_option = array(
        "1" => "일반업무중",
        "2" => "재택근무중",
        "3" => "외근중",
        "4" => "대체휴일중",
        "5" => "휴가중"
    );

    return $staff_work_state_option;
}
?>
