<?php

function getOkrStateOption()
{
    $okr_state_option = array(
        '1' => '작성중',
        '2' => '진행중',
        '3' => '종료',
        '4' => '보류',
        '5' => '취소'
    );

    return $okr_state_option;
}

function getOkrKindOption()
{
    $okr_kind_option = array(
        '0' => '::전체::',
        '1' => '팀',
        '2' => '개인',
    );

    return $okr_kind_option;
}

function getOkrTermOption()
{
    $okr_term_option = array(
        '1' => '1/3분기',
        '2' => '2/3분기',
        '3' => '3/3분기',
        '4' => '연간',
    );

    return $okr_term_option;
}