<?php

function getWorkStateOption()
{
    $work_state_option = array(
        '1' => '작성중',
        '2' => '대기',
        '3' => '진행요청',
        '4' => '접수완료',
        '5' => '진행중',
        '6' => '진행완료',
        '7' => '보류',
        '8' => '취소완료',
        '9' => '진행불가'
    );

    return $work_state_option;
}

function getWorkStateOptionColor()
{
    $work_state_color_option = array(
        '1' => 'black',
        '2' => 'black',
        '3' => 'red',
        '4' => 'blue',
        '5' => 'green',
        '6' => 'black',
        '7' => 'red',
        '8' => 'red',
        '9' => 'black'
    );

    return $work_state_color_option;
}

function getWorkStateTotalColorOption()
{
    $work_state_total_color_option = array(
        '1' => array('label' => '작성중', 'bg' => 'white', 'color' => 'black'),
        '2' => array('label' => '대기', 'bg' => 'white', 'color' => 'black'),
        '3' => array('label' => '진행요청', 'bg' => 'yellow', 'color' => 'red'),
        '4' => array('label' => '접수완료', 'bg' => 'white', 'color' => 'blue'),
        '5' => array('label' => '진행중', 'bg' => 'white', 'color' => 'green'),
        '6' => array('label' => '진행완료', 'bg' => 'white', 'color' => 'black'),
        '7' => array('label' => '보류', 'bg' => 'lightgray', 'color' => 'red'),
        '8' => array('label' => '취소완료', 'bg' => 'lightgray', 'color' => 'red'),
        '9' => array('label' => '진행불가', 'bg' => 'lightgray', 'color' => 'black'),
    );

    return $work_state_total_color_option;
}

function getContentsWorkOption()
{
    $contents_work_option = array(35,36,38,39,40,54,150,158);

    return $contents_work_option;
}