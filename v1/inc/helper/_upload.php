<?php
ini_set('display_errors', -1);

function move_dropzone_file($files, $folder)
{
    if(!tmpFolderCheck($folder)){
        return "NOT FIND DIRECTORY";
    }

    if(isset($files["error"]) && $files["error"] == '1'){
        return "fail";
    }

    $o_saveName = $files["name"][0];
    $r_saveName = "";

    if ($o_saveName)
    {
        $tmp_filename = explode(".", $o_saveName);

        if ($tmp_filename[0] != '')
            $md5filename = md5(time() . $tmp_filename[0]);
        else
            $md5filename = "";

        $ext = end($tmp_filename);


        $r_saveName = $md5filename . "." . $ext;
        move_uploaded_file($files["tmp_name"][0], BASEPATH."/v1/uploads/{$folder}/{$r_saveName}");
    }

    return $r_saveName;
}

function move_store_files($file_paths, $tmp_folder, $folder)
{
    $count		= count($file_paths);
    $sub_folder = folderCheck($folder);
    $r_saveName = [];

    for ($i = 0; $i < $count; $i++) {

        $file_path = $file_paths[$i];

        if($file_path)
        {
            $file_chk = "{$_SERVER['DOCUMENT_ROOT']}/v1/uploads/{$file_path}";
            if(file_exists($file_chk))
            {
                $r_saveName[] = $file_path;
            }else{
                $origin_file  = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$tmp_folder}/{$file_path}";

                if(file_exists($origin_file)){
                    $r_saveName[] = $folder. "/" . $sub_folder . "/" . $file_path;
                    $copy_file    = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$r_saveName[$i]}";

                    copy($origin_file, $copy_file);
                    unlink($origin_file);
                }
            }
        }
    }

    return $r_saveName;
}

function add_store_file($file, $folder)
{
    $sub_folder_name = folderCheck($folder);

    $o_saveName   = $file["name"];
    $tmp_filename = explode(".",$o_saveName);
    $r_saveName   = "";

    if($o_saveName)
    {
        if ($tmp_filename[0] != '')
            $md5filename = md5(time().$tmp_filename[0]);
        else
            $md5filename = "";

        $ext        = end($tmp_filename);
        $r_saveName = $folder. "/" . $sub_folder_name . "/" . $md5filename.".".$ext;

        move_uploaded_file($file["tmp_name"], $_SERVER['DOCUMENT_ROOT']."/v1/uploads/". $r_saveName);
    }

    return $r_saveName;
}

function add_store_image($file, $folder)
{
    $sub_folder_name = folderCheck($folder);

    $o_saveName   = $file["name"];
    $o_tmpName    = $file["tmp_name"];
    $tmp_filename = explode(".",$o_saveName);
    $r_saveName   = "";
    $image        = "";

    $tmp_image = getimagesize($file['tmp_name']);
    if ($tmp_image['mime'] == 'image/jpeg') {
        $image = imagecreatefromjpeg($o_tmpName);
    }elseif ($tmp_image['mime'] == 'image/gif'){
        $image = imagecreatefromgif($o_tmpName);
    }elseif ($tmp_image['mime'] == 'image/png'){
        $image = imagecreatefrompng($o_tmpName);
    }

    if($o_saveName)
    {
        if ($tmp_filename[0] != '')
            $md5filename = md5(time().$tmp_filename[0]);
        else
            $md5filename = "";

        $ext        = end($tmp_filename);
        $r_saveName = $folder. "/" . $sub_folder_name . "/" . $md5filename.".".$ext;

        imagejpeg($image, "uploads/". $r_saveName, 50);
    }

    return $r_saveName;
}

function upd_profile_file($file_type)
{
    $copy_folder_path = "/home/master/wise_expert/public/uploads";
    $sub_folder_name  = profileFolderCheck();

    $o_saveName = $_FILES[$file_type]["name"];
    $r_saveName = "";

    if($o_saveName){
        $tmp_filename = explode(".", $o_saveName);
        if ($tmp_filename[0] != '')
            $md5filename = md5(time().$tmp_filename[0]);
        else
            $md5filename = "";

        $ext = end($tmp_filename);
        $r_saveName = "partner/" . $sub_folder_name . "/" .$md5filename.".".$ext;

        move_uploaded_file($_FILES[$file_type]["tmp_name"], $copy_folder_path."/". $r_saveName);
    }

    return $r_saveName;
}

function tmpFolderCheck($folder)
{
    $tmpFolderPath = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$folder;

    if(!is_dir($tmpFolderPath)){
        mkdir(($tmpFolderPath), 0777);
    }

    return true;
}

function folderCheck($folder)
{
    $rootImgFolderPath	 = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$folder;
    $folderNameY = date("Y"); //폴더 년도 생성
    $folderNameM = date("n"); //폴더 월 생성
    $folderName  = $folderNameY."/".$folderNameM;

    if(!is_dir($rootImgFolderPath."/".$folderNameY)){
        mkdir($rootImgFolderPath."/".$folderNameY, 0777);
    }

    if(!is_dir($rootImgFolderPath."/".$folderNameY."/".$folderNameM)){
        mkdir(($rootImgFolderPath."/".$folderNameY."/".$folderNameM), 0777);
    }

    return $folderName;
}

function profileFolderCheck()
{
    $rootImgFolderPath	 = "/home/master/wise_expert/public/uploads/partner";
    $folderNameY = date("Y"); //폴더 년도 생성
    $folderNameM = date("n"); //폴더 월 생성
    $folderName  = $folderNameY."/".$folderNameM;

    if(!is_dir($rootImgFolderPath."/".$folderNameY)){
        mkdir($rootImgFolderPath."/".$folderNameY, 0777);
    }

    if(!is_dir($rootImgFolderPath."/".$folderNameY."/".$folderNameM)){
        mkdir(($rootImgFolderPath."/".$folderNameY."/".$folderNameM), 0777);
    }

    return $folderName;
}

function del_files($file_paths)
{
    $r_saveName = "";

    foreach ($file_paths as $file_path)
    {
        if($file_path)
        {
            $del_file = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$file_path}";

            if(file_exists($del_file))
            {
                unlink($del_file);
            }
        }
    }

    return $r_saveName;
}

function del_file($file_path)
{
    $result = false;

    if($file_path)
    {
        $del_file = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$file_path}";

        if(file_exists($del_file) && unlink($del_file)){
            $result = true;
        }
    }

    return $result;
}

function getExcelUploadTypeOption()
{
    return array(
        "1" => array(
            "title"         => "재고현황 리스트",
            "explain"       => "위드플레이스 EMP, 아마존_닥터피엘(미국), 아마존_아이레놀(미국), 아마존_아이레놀(일본) 재고현황 업로드",
            "page"          => "product_cms_stock_list.php",
            "page_title"    => "재고 현황 리스트",
            "action"        => "product_cms_stock_insert.php",
            "search_url"    => "sch_stock_date="
        ),
        "2" => array(
            "title"         => "입고/반출 리스트",
            "explain"       => "위드플레이스 EMP, 와이즈미디어커머스, 아마존_닥터피엘(미국), 아마존_아이레놀(미국), 아마존_아이레놀(일본) 입고/반출 업로드",
            "page"          => "product_cms_stock_report_list.php",
            "page_title"    => "입고/반출 리스트",
            "action"        => "product_cms_stock_report_insert.php",
            "search_url"    => "sch_s_regdate=&sch_e_regdate="
        ),
        "12" => array(
            "title"         => "재고이동 리스트",
            "explain"       => "위드플레이스 EMP 재고이동 업로드",
            "page"          => "product_cms_stock_transfer_list.php",
            "page_title"    => "재고이동 리스트",
            "action"        => "product_cms_stock_transfer_insert.php",
            "search_url"    => "sch_move_s_date=&sch_move_e_date="
        ),
        "11" => array(
            "title"         => "기간별 수불현황",
            "explain"       => "매월 재고자산수불부를 위한 데이터 업로드로 입고/반출 리스트에 '기간별 수불현황'으로 입고/반출이 적용 됩니다.\r\n\r\n> EMP = 기간별 수불현황\r\n> 파스토 = 재고수불부\r\n> 스타트투데이 = 입출고현황\r\n\r\n일부 배상처리 및 파손의 경우 확정 입고/반출형태를 '타계정[반출]'로 수기 변경해야 합니다.",
            "page"          => "product_cms_stock_report_list.php",
            "page_title"    => "입고/반출 리스트",
            "action"        => "product_cms_stock_report_insert_base.php",
            "search_url"    => "sch_s_regdate=&sch_e_regdate="
        ),
        "3" => array(
            "title"         => "정산 리스트(기본형)",
            "explain"       => "자사몰, 소셜/오픈마켓 등 기본형식의 국내 판매처의 정산내역을 업로드 합니다.",
            "page"          => "work_cms_sales_settlement.php",
            "page_title"    => "정산 리스트",
            "action"        => "work_cms_sales_settlement_insert.php",
            "search_url"    => "sch_settle_month="
        ),
        "4" => array(
            "title"         => "정산 리스트(아마존)",
            "explain"       => "아마존_닥터피엘(미국), 아마존_아이레놀(미국), 아마존_아이레놀(일본) 판매에 대한 정산내역을 업로드 합니다.",
            "page"          => "work_cms_sales_settlement.php",
            "page_title"    => "정산 리스트",
            "action"        => "work_cms_sales_settlement_insert.php",
            "search_url"    => "sch_settle_month="
        ),
        "5" => array(
            "title"         => "업무 리스트(물류비 정산_통합)",
            "explain"       => "위드플레이스의 물류비 내역을 업무리스트에 업로드 합니다.",
            "page"          => "work_list.php",
            "page_title"    => "업무 리스트",
            "action"        => "work_insert.php",
            "search_url"    => ""
        ),
        "9" => array(
            "title"         => "정산 내역(위드플레이스)",
            "explain"       => "위드플레이스의 월별 물류비 정산 내역을 업로드 합니다.",
            "page"          => "work_cms_sales_settlement.php",
            "page_title"    => "정산 리스트",
            "action"        => "work_cms_sales_settlement_insert.php",
            "search_url"    => "sch_settle_month="
        ),
        "6" => array(
            "title"         => "택배 리스트(파스토)",
            "explain"       => "파스토 주문 내역을 업로드 합니다.",
            "page"          => "work_list_cms.php",
            "page_title"    => "택배 리스트",
            "action"        => "work_cms_insert_fassto.php",
            "search_url"    => "sch_ord_bundle=1&sch_reg_s_date=&sch_reg_e_date="
        ),
        "7" => array(
            "title"         => "택배 리스트(스타트투데이)",
            "explain"       => "스타트투데이 주문 내역을 업로드 합니다.",
            "page"          => "work_list_cms.php",
            "page_title"    => "택배 리스트",
            "action"        => "work_cms_insert_start_today.php",
            "search_url"    => "sch_ord_bundle=1&sch_reg_s_date=&sch_reg_e_date="
        ),
        "8" => array(
            "title"         => "택배 리스트(스마트스토어 쿠폰내역)",
            "explain"       => "스마트스토어 쿠폰 내역을 업로드 합니다.",
            "page"          => "work_list_cms.php",
            "page_title"    => "택배 리스트",
            "action"        => "work_cms_insert_coupon.php",
            "search_url"    => "sch_ord_bundle=1&sch_reg_s_date=&sch_reg_e_date="
        ),
        "10" => array(
            "title"         => "반품/교환 리스트",
            "explain"       => "아임웹, 스마트스토어, 사방넷의 교환/반품 내역을 업로드 합니다.\r\n아임웹, 스마트스토어는 반품/교환 완료 기준으로 업로드 하며, 사방넷은 반품/교환 요청 기준으로 업로드 합니다.",
            "page"          => "wise_csm_return_list.php",
            "page_title"    => "반품/교환 리스트",
            "action"        => "wise_csm_return_list_insert.php",
            "search_url"    => "sch_s_date=&sch_e_date="
        ),
        "13" => array(
            "title"         => "직택배 리스트",
            "explain"       => "CS 응대를 위한 직택배 리스트를 업로드 합니다.",
            "page"          => "work_cms_direct_list.php",
            "page_title"    => "직택배 리스트",
            "action"        => "work_cms_direct_insert.php",
            "search_url"    => "sch_delivery_date="
        ),
    );
}
?>
