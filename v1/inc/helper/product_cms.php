<?php

function getGiftDateTypeOption()
{
    $gift_date_type_option = array(
        "1" => "ON",
        "2" => "OFF"
    );

    return $gift_date_type_option;
}

function getPrdTypeOption()
{
    $unit_type_option = array(
        "2" => "부속품",
        "3" => "이벤트"
    );

    return $unit_type_option;
}

function getUnitTypeOption()
{
    $unit_type_option = array(
        "1" => "상품",
        "2" => "부속품"
    );

    return $unit_type_option;
}

function getSetOption(){
    $set_option = array("1" => "설정", "2" => "미설정");

    return $set_option;
}

function getIsProductLoadOption()
{
    $is_product_load_option = array(
        "1" => "있음",
        "2" => "없음"
    );

    return $is_product_load_option;
}

function getGiftTypeOption()
{
    $gift_type_option = array(
        "gift_date_type"        => array("title" => "사은품 설정(A)", "s_date" => "gift_s_date", "e_date" => "gift_e_date"),
        "sub_gift_date_type"    => array("title" => "사은품 설정(B)", "s_date" => "sub_gift_s_date", "e_date" => "sub_gift_e_date"),
    );

    return $gift_type_option;
}