<?php

function getSettleIsParteOption()
{
    $is_partner_option = array(
        "1" => "확인",
        "2" => "확인불가"
    );

    return $is_partner_option;
}

function getSettleAgencyOption()
{
    $agency_option = array(
        "4004" => "하나애드",
        "1858" => "DMC미디어",
        "3735" => "모비데이즈",
        "4355" => "메조미디어",
        "528"  => "FACEBOOK",
        "4472" => "나스미디어",
    );

    return $agency_option;
}

function getAdvertiseMediaOption()
{
    $advertise_media_option = array(
        "263"   => "네이버(검색광고)",
        "265"   => "네이버(NOSP)",
        "266"   => "네이버(GFA)",
        "267"   => "카카오",
        "268"   => "구글",
        "269"   => "Facebook",
        "282"   => "토스",
        "283"   => "기타",
    );

    return $advertise_media_option;
}

function getAdvertiseTypeOption()
{
    $advertise_type_option = array(
      "naver"       => "네이버(검색광고)",
      "naver_gfa"   => "네이버(GFA)",
      "naver_nosp"  => "네이버(NOSP)",
      "kakao"       => "카카오",
      "google"      => "구글",
      "facebook"    => "Facebook",
      "toss"        => "토스",
      "etc"         => "기타",
    );

    return $advertise_type_option;
}

function getAdMyCompanyOption()
{
    $ad_my_company = array(
        "1" => "㈜와이즈플래닛컴퍼니",
        "3" => "㈜와이즈미디어커머스",
    );

    return $ad_my_company;
}

function getAdIsExistOption()
{
    $is_exist_option = array(
        "1" => "有",
        "2" => "無",
    );

    return $is_exist_option;
}

function getAgencyQuickSearch()
{
    $quick_search_option = array(
        "1" => "잔액 0원 이하 보기",
        "2" => "전월잔액 0원 이하 보기",
    );

    return $quick_search_option;
}

function getAdIsInspectionOption()
{
    $is_inspection_option = array(
        "1" => "검수대상",
        "2" => "검수제외(거래종료)",
        "3" => "검수제외(대행해지)",
        "4" => "검수완료",
    );

    return $is_inspection_option;
}



