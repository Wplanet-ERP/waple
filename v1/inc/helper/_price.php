<?php

function getCurrency($price_type)
{
    $fp         = fopen("https://spot.wooribank.com/pot/Dream?withyou=FXCNT0002&rc=0&divType=1&lang=KOR","r");
    $price_rate = "";

    if($fp) {
        while(!feof($fp))
        {
            $line = fgetss($fp);
            if (trim($line)!="")
            {
                $line = str_replace("&nbsp;","",$line);

                if($o_line == "USD" || $o_line == "JPY"){
                    $ex_rate .= trim($line)."^";
                }
            }

            $o_line = trim($line);
        }

        $fxkeb_rate = explode("^", $ex_rate);

        if($price_type == "jpy"){
            $price_rate = $fxkeb_rate[1]/100;
        }elseif($price_type == "usd"){
            $price_rate = $fxkeb_rate[0];
        }
    }

    return array("krw" => $price_rate, "date" => date("Y-m-d"));
}

function calculateCurrency($currency, $dp_price)
{
    $currency_rate = isset($currency['krw']) ? $currency['krw'] : 0;

    $convert_price = 0;
    if($dp_price > 0 && $currency_rate > 0){
        $convert_price = round($dp_price/$currency_rate, 2);
    }

    $convert_currency = array(
        'date'  => date('m/d', strtotime($currency['date'])),
        'price' => $convert_price
    );

    return $convert_currency;
}


function getNumberFormatPrice($number)
{
    $numbers = explode('.', $number);

    if(isset($numbers[1])){
        $number_text = number_format($number, 1);
    }else{
        $number_text = number_format($number);
    }

    return $number_text;
}

function getKrwFormatPrice($number)
{
    return number_format($number, 2);
}

function getUsdFormatPrice($number)
{
    return number_format($number, 2);
}