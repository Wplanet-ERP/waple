<?php
function getEvaluationTypeOption()
{
    $evaluation_type_option = array(
        '1' => "온라인+ARS",
        '2' => "온라인",
        "3" => "ARS"
    );

    return $evaluation_type_option;
}

function getQuestionTypeOption()
{
    $question_type_option = array(
        '1' => "종합 평점",
        '2' => "서비스만족 평점",
        '3' => "정확한안내 평점",
        '4' => "추천의향 평점",
        '5' => "상담연결 시간 평점"
    );

    return $question_type_option;
}

function getScoreTypeOption()
{
    $score_type_option = array(
        '1' => "1점",
        '2' => "2점",
        '3' => "3점",
        '4' => "4점",
        '5' => "5점"
    );

    return $score_type_option;
}

# 엑셀 업로드 옵션
function getReturnExcelOption()
{
    $excel_option = array(
        '1' => '아임웹(반품)',
        '2' => '아임웹(교환)',
        '3' => '스마트스토어(반품)',
        '4' => '스마트스토어(교환)',
        '5' => '사방넷(클레임확인)'
    );

    return $excel_option;
}

# 구분 옵션
function getReturnTypeOption()
{
    $return_type_option = array(
        '1'  => '반품',
        '2'  => '교환',
    );

    return $return_type_option;
}

function getDeliveryTypeOption()
{
    $delivery_type_option = array(
        'CJ대한통운' => 'CJ대한통운',
        '한진택배' => '한진택배'
    );

    return $delivery_type_option;
}

function getCsmDateTypeOption()
{
    $date_type_option = array(
        'return_date' => '반품/교환 요청일',
        'order_date'  => '주문일',
    );

    return $date_type_option;
}

function getCsDpList()
{
    return array(
        "5128" => "CS_물류측 누락",
        "5135" => "CS_물류측 누락(교환)",
        "5127" => "CS_물류측 오배송",
        "5134" => "CS_물류측 오배송(교환)",
        "5900" => "CS_배송 파손",
        "5901" => "CS_배송 파손(교환)",
        "5899" => "CS_배송사 분실",
    );
}
