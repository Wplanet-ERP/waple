<?php

function getLicenseTypeOption()
{
    $license_type_option = array(
        '1' => "법인회사",
        '2' => "개인회사",
        '3' => "개인",
        '4' => "해외"
    );

    return $license_type_option;
}

function getCorpKindOption()
{
    $corp_kind_option = array(
        '1' => "광고주",
        '2' => "외주업체",
        '3' => "강사/멘토"
    );

    return $corp_kind_option;
}

function getBkTitleOption()
{
    $bk_title_list = array("국민","기업","신한","우리","하나","농협","신협","카카오뱅크","전북","대구","BOA","스탠다드차타드","우체국","씨티","새마을금고","제이피모건체이스","경남","BNK부산은행","KDB산업은행","토스뱅크","토스증권","케이뱅크");

    return $bk_title_list;
}

function getContactTypeOption()
{
    $contact_type_option = array(
        "1" => "ROAS",
        "2" => "STARTUP",
        "3" => "FRANCHISE"
    );

    return $contact_type_option;
}

function getReContactOption()
{
    $re_contact_option = array(
        "1" => "가능",
        "2" => "불가",
    );

    return $re_contact_option;
}

function getAdvTypeOption()
{
    $adv_type_option = array(
        "1" => "선불",
        "2" => "선불(충전식)",
        "3" => "후불"
    );

    return $adv_type_option;
}

function getAdvFeeOption()
{
    $adv_fee_option = array(
        "1" => "광고비",
        "2" => "광고비-대행수수료",
        "3" => "광고비-대행수수료 (대행수수료 계산서 미발행)"
    );

    return $adv_fee_option;
}

function getInfCategoryOption()
{
    $inf_category_option = array(
        "1"     => "일상",
        "2"     => "여행",
        "3"     => "패션",
        "4"     => "뷰티",
        "5"     => "푸드",
        "6"     => "IT테크",
        "7"     => "자동차",
        "8"     => "리빙",
        "9"     => "육아",
        "10"    => "생활건강",
        "22"    => "게임",
        "11"    => "동물/펫",
        "12"    => "운동/레저",
        "13"    => "스포츠",
        "14"    => "방송/연예",
        "15"    => "대중음악",
        "16"    => "영화",
        "17"    => "공연/전시/예술",
        "18"    => "도서",
        "19"    => "경제/비지니스",
        "20"    => "어학/교육",
        "21"    => "VLOG",
    );

    return $inf_category_option;
}

function getInfChannelOption()
{
    $inf_channel_option = array(
        "1"     => "유튜브",
        "2"     => "인스타그램",
        "3"     => "네이버 인플루언서",
    );

    return $inf_channel_option;
}

function getLectorVatTypeOption()
{
    $lec_vat_type_option = array(
        "1"     => array("title" => "사업소득세", "exp" => "(세율:3.3%) 신고"),
        "2"     => array("title" => "기타소득세", "exp" => "(세율:8.8%) 신고"),
    );

    return $lec_vat_type_option;
}