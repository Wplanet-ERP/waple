<?php

function getDpQuickOption()
{
    $dp_quick_option = array(
        "1" => "카드 입금대기",
        "2" => "현금 입금대기",
        "3" => "계산서 발행대기",
        "4" => "입금완료(최신순)",
        "5" => "인센티브 정산 년/월 미설정",
        "6" => "자료 없음",
        "7" => "담당자 알수없음",
        "8" => "커머스 기업입금"
    );

    return $dp_quick_option;
}

function getDpModTypeOption()
{
    $dp_mod_type_option = array(
        "modify" => "수정요청",
        "refund" => "환불요청"
    );

    return $dp_mod_type_option;
}

function getDpEtcOption()
{
    $dp_etc_option = array(
//        "1" => "인센티브 정산 년/월 미설정",
        "2" => "자료 없음"
    );

    return $dp_etc_option;
}

function getDpMethodOption()
{
    $dp_method_option = array(
        "1" => "카드결제",
        "2" => "현금입금",
        "3" => "월정산입금",
    );

    return $dp_method_option;
}

function getDpTaxOption()
{
    $dp_tax_state_option = array(
        '1' => '발행대기',
        '2' => '발행완료',
        '3' => '카드결제',
        '4' => '해당사항없음'
    );

    return $dp_tax_state_option;
}

function getDpStateOption()
{
    $dp_state_option = array(
        '1' => '입금대기',
        '2' => '입금완료',
        '3' => '부분입금',
        '4' => '환불완료',
        '5' => '취소완료'
    );

    return $dp_state_option;
}

function getDpcCompanyOption()
{
    $dpc_company_option = array("ecount");

    return $dpc_company_option;
}

function getIsDepositOption()
{
    $is_deposit_option = array("1" => "있음", "2" => "없음");

    return $is_deposit_option;
}

?>