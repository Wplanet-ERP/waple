<?php

function getBMBaseTypeOption() {
    return array("all" => "커머스 전체", "doc" => "닥터피엘 전체", "nuzam" => "누잠 전체", "mil" => "밀리옹 전체");
}

function getBMDateTypeOption() {
    return array("date" => "일별", "week" => "주별", "month" => "월별");
}

function getTotalMainBrandOption()
{
    return array("doc", "nuzam", "ilenol", "city");
}

function getTotalBrandNameOption()
{
    return array(
        "doc"       => "닥터피엘 전체",
        "ilenol"    => "아이레놀",
        "nuzam"     => "누잠",
        "city"      => "시티파이",
    );
}

function getTotalBrandChartType()
{
    return array(
        "doc"       => array(
            array("max" => 35000, "min" => -10000),
            array("max" => 1200, "min" => -150),
            array("max" => 20, "min" => 0),
            array("max" => 200000000, "min" => 0),
            array("max" => 130000, "min" => 20000)
        ),
        "ilenol"    => array(
            array("max" => 7000, "min" => -3000),
            array("max" => 500, "min" => -100),
            array("max" => 20, "min" => 0),
            array("max" => 10000000, "min" => 0),
            array("max" => 70000, "min" => 10000)
        ),
        "nuzam"     => array(
            array("max" => 90000, "min" => -20000),
            array("max" => 900, "min" => -100),
            array("max" => 8, "min" => 0),
            array("max" => 120000000, "min" => 0),
            array("max" => 250000, "min" => 50000)
        ),
        "city"  => array(
            array("max" => 20000, "min" => 2500),
            array("max" => 300, "min" => 20),
            array("max" => 2, "min" => 0.5),
            array("max" => 20000000, "min" => 2000000),
            array("max" => 125000, "min" => 100000)
        ),
    );
}

function getDocBrandNameOption()
{
    return array(
        "1314"      => "필터라인",
        "3386"      => "큐빙",
        "2863"      => "퓨어팟",
        "4140"      => "칫솔",
        "4445"      => "직수정수기",
        "4446"      => "피처형정수기"
    );
}

function getNuazmBrandNameOption()
{
    return array(
        "2827"      => "누잠매트리스",
        "4878"      => "누잠 더블업토퍼",
        "5201"      => "누잠 여름침구",
        "5642"      => "누잠 겨울침구",
        "5810"      => "누잠 바디필로우",
        "5415"      => "누잠 슬립미스트",
        "6026"      => "누잠 베개",
        "6060"      => "누잠 여름패드",
    );
}

function getMilBrandNameOption()
{
    return array(
        "1"     => "밀리옹",
        "47"    => "뉴 밀링백",
        "23"    => "밀리옹(일본)"
    );
}

function getBrandDetailOption()
{
    return array(
        "doc"       => array(
            "title"         => "닥터피엘(샤워기/세면대/주방용/세탁기)",
            "nav_no"        => "196",
            "manager"       => "42",
            "manager_name"  => "이성현",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70007&sch_brand=1314",
            "brand_list"    => array("1314"),
            "global_base"   => array("5513"),
            "global_log"    => array("5956"),
            "global_cms"    => getTotalDocBrandList(),
            "global_url"    => "sch_brand_g1=70001&sch_brand_g2=70007&sch_brand=5513",
            "chart_type"    => array(
                array("max" => 18000, "min" => 8000),
                array("max" => 900, "min" => 450),
                array("max" => 8, "min" => 4),
                array("max" => 80000000, "min" => 30000000),
                array("max" => 60000, "min" => 52000),
            )
        ),
        "pure"      => array(
            "title"         => "닥터피엘 퓨어팟",
            "nav_no"        => "197",
            "manager"       => "7",
            "manager_name"  => "김준성",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70009",
            "brand_list"    => array("2863"),
            "chart_type"    => array(
                array("max" => 7500, "min" => 700),
                array("max" => 60, "min" => 10),
                array("max" => 3, "min" => 0.5),
                array("max" => 10000000, "min" => 1500000),
                array("max" => 95000, "min" => 78000),
            )
        ),
        "cubing"    => array(
            "title"         => "닥터피엘 큐빙",
            "nav_no"        => "208",
            "manager"       => "6",
            "manager_name"  => "남승욱",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70008",
            "brand_list"    => array("3386","5283"),
            "chart_type"    => array(
                array("max" => 1800, "min" => 1100),
                array("max" => 80, "min" => 20),
                array("max" => 5, "min" => 1.5),
                array("max" => 9000000, "min" => 2000000),
                array("max" => 70000, "min" => 55000),
            )
        ),
        "toothbrush"    => array(
            "title"         => "닥터피엘 칫솔",
            "nav_no"        => "209",
            "manager"       => "6",
            "manager_name"  => "남승욱",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70009",
            "brand_list"    => array("4140"),
            "chart_type"    => array(
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
            )
        ),
        "hose"      => array(
            "title"         => "닥터피엘 에코호스",
            "nav_no"        => "198",
            "manager"       => "145",
            "manager_name"  => "권혁신",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70007&sch_brand=3303",
            "brand_list"    => array("3303"),
            "chart_type"    => array(
                array("max" => 1100, "min" => 500),
                array("max" => 35, "min" => 5),
                array("max" => 5, "min" => 1),
                array("max" => 9000000, "min" => 3000000),
                array("max" => 42000, "min" => 34000),
            )
        ),
        "zero"      => array(
            "title"         => "닥터피엘 코스트제로 정수기",
            "nav_no"        => "199",
            "manager"       => "76",
            "manager_name"  => "조일암",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70013",
            "brand_list"    => array("4445"),
            "chart_type"    => array(
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
            )
        ),
        "pitcher"   => array(
            "title"         => "닥터피엘 에코피처형 정수기",
            "nav_no"        => "200",
            "manager"       => "7",
            "manager_name"  => "김준성",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70014",
            "brand_list"    => array("4446"),
            "chart_type"    => array(
                array("max" => 1200, "min" => 500),
                array("max" => 25, "min" => 0),
                array("max" => 2, "min" => 0),
                array("max" => 3000000, "min" => 500000),
                array("max" => 65000, "min" => 40000),
            )
        ),
        "travel"    => array(
            "title"         => "닥터피엘 여행용",
            "nav_no"        => "201",
            "manager"       => "67",
            "manager_name"  => "윤준영",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70007&sch_brand=5434",
            "brand_list"    => array("5434"),
            "chart_type"    => array(
                array("max" => 1300, "min" => 600),
                array("max" => 80, "min" => 20),
                array("max" => 7, "min" => 3),
                array("max" => 6000000, "min" => 2000000),
                array("max" => 34000, "min" => 23000),
            )
        ),
        "ato"       => array(
            "title"         => "닥터피엘 아토샤워헤드",
            "nav_no"        => "205",
            "manager"       => "43",
            "manager_name"  => "조영민",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70007&sch_brand=5812",
            "brand_list"    => array("5812"),
            "chart_type"    => array(
                array("max" => 2900, "min" => 1100),
                array("max" => 120, "min" => 40),
                array("max" => 6, "min" => 3),
                array("max" => 15000000, "min" => 3000000),
                array("max" => 73000, "min" => 62000),
            )
        ),
        "bubble"    => array(
            "title"         => "버블세면대",
            "nav_no"        => "205",
            "manager"       => "43",
            "manager_name"  => "조영민",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70007&sch_brand=5910",
            "brand_list"    => array("5910"),
            "chart_type"    => array(
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
            )
        ),
        "kitchen"   => array(
            "title"         => "닥터피엘 주방용 프리미엄",
            "nav_no"        => "238",
            "manager"       => "10",
            "manager_name"  => "이민하",
            "brand_url"     => "sch_brand_g1=70001&sch_brand_g2=70007&sch_brand=5368",
            "brand_list"    => array("5368"),
            "chart_type"    => array(
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
            )
        ),
        "nuzam"     => array(
            "title"         => "누잠",
            "nav_no"        => "202",
            "manager"       => "4",
            "manager_name"  => "박재훈",
            "brand_url"     => "sch_brand_g1=70003",
            "brand_list"    => getNuzamBrandList(),
            "chart_type"    => array(
                array("max" => 90000, "min" => -20000),
                array("max" => 900, "min" => -100),
                array("max" => 8, "min" => 0),
                array("max" => 120000000, "min" => 0),
                array("max" => 250000, "min" => 50000),
            )
        ),
        "ilenol"    => array(
            "title"             => "아이레놀",
            "nav_no"            => "203",
            "manager"           => "15",
            "manager_name"      => "고광우",
            "brand_url"          => "sch_brand_g1=70002",
            "brand_list"        => getIlenolBrandList(),
            "global_base"       => array("5514","5979","6044"),
            "global_log"        => array("5659"),
            "global_cms"        => getIlenolBrandList(),
            "global_url"        => "sch_brand_g1=70002&sch_brand_g2=70015&sch_brand=5979",
            "chart_type_base"   => array(
                array("max" => 28000, "min" => 7000),
                array("max" => 450, "min" => 180),
                array("max" => 3.5, "min" => 1),
                array("max" => 16000000, "min" => 7000000),
                array("max" => 34000, "min" => 26000),
            ),
            "chart_type_other"  => array(
                array("max" => 3400, "min" => 1700),
                array("max" => 240, "min" => 140),
                array("max" => 10, "min" => 6),
                array("max" => 5500000, "min" => 2000000),
                array("max" => 33000, "min" => 25000),
            )
        ),
        "city"      => array(
            "title"         => "시티파이",
            "nav_no"        => "204",
            "manager"       => "147",
            "manager_name"  => "이남규",
            "brand_url"     => "sch_brand_g1=70004",
            "brand_list"    => getCityBrandList(),
            "chart_type"    => array(
                array("max" => 20000, "min" => 2500),
                array("max" => 300, "min" => 20),
                array("max" => 2, "min" => 0.5),
                array("max" => 20000000, "min" => 2000000),
                array("max" => 125000, "min" => 100000),
            )
        ),
        "jp_ilenol"      => array(
            "title"         => "(일본)아이레놀",
            "nav_no"        => "223",
            "manager"       => "147",
            "manager_name"  => "한아름",
            "brand_url"     => "sch_brand_g1=70002&sch_brand_g2=70015&sch_brand=5979",
            "main_brand"    => array("5979"),
            "log_company"   => "5659",
            "brand_list"    => getIlenolBrandList(),
            "chart_type"    => array(
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
                array("max" => 0, "min" => 0),
            )
        ),
    );
}

function getTotalBrandList()
{
    return array("1314","2863","3303","3386","4140","4445","4446","5283","5434","5812","5910","5368","2827","4878","5201","5642","5810","5415","6026","6060","2388","4440","4809","5759","5932","4333","5447");
}

function getGlobalBrandList()
{
    return array("5513" => "doc", "5514" => "ilenol", "5979" => "ilenol", "6044" => "ilenol");
}

function getTotalDocBrandList()
{
    return array("1314","2863","3303","3386","4140","4445","4446","5283","5434","5812","5910","5368");
}

function getNuzamBrandList()
{
    return array("2827","4878","5201","5642","5810","5415","6026","6060");
}

function getIlenolBrandList()
{
    return array("2388","4440","4809","5759","5932");
}

function getCityBrandList()
{
    return array("4333","5447");
}

function getMilBrandList()
{
    return array("1", "23", "47");
}

function getTrackOption()
{
    return array(
        "1" => "위드플레이스",
    );
}

function getOtherTypeOption()
{
    return array(
        "1" => "수량",
        "2" => "금액",
    );
}

function getBaseChartType()
{
    return array(
        array("max" => 150000, "min" => -100000),
        array("max" => 2000, "min" => -200),
        array("max" => 8, "min" => 0),
        array("max" => 250000000, "min" => 0),
        array("max" => 170000, "min" => 0)
    );
}

function getEmptyChartType(){
    return array(
        array("max" => 0, "min" => 0),
        array("max" => 0, "min" => 0),
        array("max" => 0, "min" => 0),
        array("max" => 0, "min" => 0),
        array("max" => 0, "min" => 0),
    );
}

function getDocBrandChartType()
{
    return array(
        "1314" => array(
            array("max" => 18000, "min" => 8000),
            array("max" => 900, "min" => 450),
            array("max" => 8, "min" => 4),
            array("max" => 80000000, "min" => 30000000),
            array("max" => 60000, "min" => 52000),
        ),
        "2863" => array(
            array("max" => 7500, "min" => 700),
            array("max" => 60, "min" => 10),
            array("max" => 3, "min" => 0.5),
            array("max" => 10000000, "min" => 1500000),
            array("max" => 95000, "min" => 78000),
        ),
        "3386" => array(
            array("max" => 1800, "min" => 1100),
            array("max" => 80, "min" => 20),
            array("max" => 5, "min" => 1.5),
            array("max" => 9000000, "min" => 2000000),
            array("max" => 70000, "min" => 55000),
        ),
        "4140" => array(
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
        ),
        "3303" => array(
            array("max" => 1100, "min" => 500),
            array("max" => 35, "min" => 5),
            array("max" => 5, "min" => 1),
            array("max" => 9000000, "min" => 3000000),
            array("max" => 42000, "min" => 34000),
        ),
        "4445" => array(
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
        ),
        "4446" => array(
            array("max" => 1200, "min" => 500),
            array("max" => 25, "min" => 0),
            array("max" => 2, "min" => 0),
            array("max" => 3000000, "min" => 500000),
            array("max" => 65000, "min" => 40000),
        ),
        "5434" => array(
            array("max" => 1300, "min" => 600),
            array("max" => 80, "min" => 20),
            array("max" => 7, "min" => 3),
            array("max" => 6000000, "min" => 2000000),
            array("max" => 34000, "min" => 23000),
        ),
        "5812" => array(
            array("max" => 2900, "min" => 1100),
            array("max" => 120, "min" => 40),
            array("max" => 6, "min" => 3),
            array("max" => 15000000, "min" => 3000000),
            array("max" => 73000, "min" => 62000),
        ),
        "5910" => array(
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
        ),
    );
}

function getNuzamBrandChartType()
{
    return array(
        "2827" => array(
            array("max" => 90000, "min" => -20000),
            array("max" => 900, "min" => -100),
            array("max" => 8, "min" => 0),
            array("max" => 120000000, "min" => 0),
            array("max" => 250000, "min" => 50000)
        ),
        "4878" => array(
            array("max" => 90000, "min" => -20000),
            array("max" => 900, "min" => -100),
            array("max" => 8, "min" => 0),
            array("max" => 120000000, "min" => 0),
            array("max" => 250000, "min" => 50000)
        ),
        "5201" => array(
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
        ),
        "5642" => array(
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
        ),
        "5810" => array(
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
        ),
        "5415" => array(
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
        ),
        "6026" => array(
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
            array("max" => 0, "min" => 0),
        ),
    );
}

function getDocFilterOption(){
    return array("1314","3303","5434","5812","5368","5910");
}

function getDocCubingOption(){
    return array("3386", "5283");
}