<?php

$aeskey = "w_planet!@#";

$holiday_sql 	        = "SELECT * FROM holiday_info ORDER BY h_date ASC";
$holiday_query 	        = mysqli_query($my_db, $holiday_sql);
$holiday_list	        = [];
$holiday_name_list      = [];
$calendar_holiday_list  = [];

while($holiday = mysqli_fetch_array($holiday_query))
{
    $holiday_list[$holiday['h_date']]['title']  = $holiday['h_name'];
    $holiday_name_list[$holiday['h_date']]      = $holiday['h_name'];
    $calendar_holiday_list[] = array(
        'title' => $holiday['h_name'],
        's_date' => date('Y-m-d', strtotime($holiday['h_date'])). " 00:00:00",
        'e_date' => date('Y-m-d', strtotime($holiday['h_date'])). " 23:59:59"
    );
}

$smarty->assign('holiday_list', json_encode($holiday_list));
$smarty->assign('holiday_name_list', $holiday_name_list);
$smarty->assign('calendar_holiday_list', json_encode($calendar_holiday_list));

function getColors()
{
    $colors = array(
        "#000000", "#006400", "#00FA9A", "#000080", "#00BFFF", "#4169E1", "#483D8B", "#556B2F", "#800000",
        "#D8BFD8", "#BDB76B", "#FFD700", "#191970", "#008000", "#00FF00", "#00008B", "#00CED1", "#4682B4",
        "#4B0082", "#696969", "#8B0000", "#708090", "#DDA0DD", "#CD853F", "#228B22", "#00FF7F", "#0000CD",
        "#00FFFF", "#5F9EA0", "#663399", "#808000", "#8B4513", "#778899", "#FF00FF", "#D2691E", "#2E8B57",
        "#7CFC00", "#0000FF", "#00FFFF", "#6495ED", "#6A5ACD", "#808080", "#A0522D", "#8FBC8F", "#FF00FF",
        "#D2B48C", "#2F4F4F", "#7FFF00", "#008080", "#1E90FF", "#66CDAA", "#7B68EE", "#A9A9A9", "#A52A2A",
        "#FF1493", "#DAA520", "#32CD32", "#90EE90", "#008B8B", "#20B2AA", "#800080", "#BC8F8F", "#B22222",
        "#CD5C5C", "#DEB887", "#3CB371", "#98FB98", "#40E0D0", "#8A2BE2", "#C0C0C0", "#B8860B", "#DA70D6",
        "#E9967A", "#6B8E23", "#9ACD32", "#48D1CC", "#8B008B", "#D3D3D3", "#C71585", "#EE82EE", "#F4A460",
        "#ADFF2F", "#7FFFD4", "#9370DB", "#DCDCDC", "#DB7093", "#F08080", "#F5DEB3", "#87CEEB", "#9400D3",
        "#DC143C", "#FA8072", "#FFB6C1", "#87CEFA", "#9932CC", "#FF0000", "#FF4500", "#FFC0CB", "#ADD8E6",
        "#BA55D3", "#FF6347", "#FFDAB9", "#AFEEEE", "#FF69B4", "#FFDEAD", "#B0C4DE", "#FF7F50", "#FFE4B5",
        "#B0E0E6", "#FF8C00", "#FFE4C4", "#FFA07A", "#FFA500", "#000000", "#006400", "#00FA9A", "#000080",
        "#00BFFF", "#4169E1", "#483D8B", "#556B2F", "#800000", "#D8BFD8", "#BDB76B", "#FFD700", "#191970",
        "#008000", "#00FF00", "#00008B", "#00CED1", "#4682B4", "#4B0082", "#696969", "#8B0000", "#708090",
        "#DDA0DD", "#CD853F", "#228B22", "#00FF7F", "#0000CD", "#00FFFF", "#5F9EA0", "#663399", "#808000",
        "#8B4513", "#778899", "#FF00FF", "#D2691E", "#2E8B57", "#7CFC00", "#0000FF", "#00FFFF", "#6495ED",
        "#6A5ACD", "#808080", "#A0522D", "#8FBC8F", "#FF00FF", "#D2B48C", "#2F4F4F", "#7FFF00", "#008080",
        "#1E90FF", "#66CDAA", "#7B68EE", "#A9A9A9", "#A52A2A", "#FF1493", "#DAA520", "#32CD32", "#90EE90",
        "#008B8B", "#20B2AA", "#800080", "#BC8F8F", "#B22222", "#CD5C5C", "#DEB887", "#3CB371", "#98FB98",
        "#40E0D0", "#8A2BE2", "#C0C0C0", "#B8860B", "#DA70D6", "#E9967A", "#6B8E23", "#9ACD32", "#48D1CC",
        "#8B008B", "#D3D3D3", "#C71585", "#EE82EE", "#F4A460", "#ADFF2F", "#7FFFD4", "#9370DB", "#DCDCDC",
        "#DB7093", "#F08080", "#F5DEB3", "#87CEEB", "#9400D3", "#DC143C", "#FA8072", "#FFB6C1", "#87CEFA",
        "#9932CC", "#FF0000", "#FF4500", "#FFC0CB", "#ADD8E6", "#BA55D3", "#FF6347", "#FFDAB9", "#AFEEEE",
        "#FF69B4", "#FFDEAD", "#B0C4DE", "#FF7F50", "#FFE4B5", "#B0E0E6", "#FF8C00", "#FFE4C4", "#FFA07A",
        "#FFA500"
    );

    return $colors;
}

function getIsExistOption()
{
    $is_exist_option = array(
        "1" => "유",
        "2" => "무",
    );

    return $is_exist_option;
}

function getIsConfirmOption()
{
    $is_confirm_option = array(
        "1" => "확인",
        "2" => "미확인",
    );

    return $is_confirm_option;
}

function getDisplayOption()
{
    $display_option = array(
        "1" => "ON",
        "2" => "OFF"
    );

    return $display_option;
}

function getActiveOption()
{
    $active_option = array(
        '1' => 'active',
        '2' => 'deactivate'
    );

    return $active_option;
}

function getDateTypeOption(){
    return array("1" => "일간", "2" => "주간", "3" => "월간", "4" => "연간");
}

function getPageTypeOption($type)
{
    $page_type_option = array('20' => '20', '50' => '50', '100' => '100');

    if($type == '4')
    {
        $page_type_option = array('10' => '10', '20' => '20', '50' => '50', '100' => '100');
    }
    elseif($type == '5')
    {
        $page_type_option = array('10' => '10', '15' => '15', '20' => '20', '50' => '50', '100' => '100');
    }
    elseif($type == 'big')
    {
        $page_type_option = array('50' => '50', '100' => '100', '500' => '500');
    }

    return $page_type_option;
}

function getHpOption()
{
    $hp_option = array("010", "011", "016", "017", "018", "019");

    return $hp_option;
}

function getTelOption()
{
    $tel_option = array("02","051","053","032","062","042","052","044","031","033","043","041","063","061","050","054","055","064","070");

    return $tel_option;
}

function getEmailOption()
{
    $email_option = array("naver.com", "gmail.com", "hanmail.net", "hotmail.com", "korea.com", "nate.com", "yahoo.com", "empal.com", "paran.com", "yahoo.co.kr");

    return $email_option;
}
?>
