<?php

function getEvaluationAlphaOption()
{
    $alpha_option = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

    return $alpha_option;
}

function getEvaluationScoreOption()
{
    $score_option = array('1', '2', '3', '4', '5');

    return $score_option;
}

function getEvaluationScoreTitleOption()
{
    $score_title_option = array(
        '1' => '매우불만족',
        '2' => '불만족',
        '3' => '보통',
        '4' => '만족',
        '5' => '매우만족'
    );

    return $score_title_option;
}

function getEvStateOption()
{
    $ev_state_option = array(
        '1' => '작성중',
        '5' => '평가자 설정',
        '6' => '상호평가 확인/수정',
        '7' => '평가자 설정 검토',
        '2' => '평가 진행중',
        '3' => '확인중',
        '4' => '진행종료'
    );

    return $ev_state_option;
}

function getEvStateColorOption()
{
    $ev_state_option = array(
        '1' => array('title' => '작성중', 'color' => 'blue'),
        '5' => array('title' => '평가자 설정', 'color' => 'red'),
        '6' => array('title' => '상호평가 확인/수정', 'color' => 'red'),
        '7' => array('title' => '평가자 설정 검토', 'color' => 'red'),
        '2' => array('title' => '평가 진행중', 'color' => 'red'),
        '3' => array('title' => '확인중', 'color' => 'black'),
        '4' => array('title' => '진행종료', 'color' => 'black'),
    );

    return $ev_state_option;
}

function getEvReviewOption()
{
    $ev_review_option = array(
        '0' => '확인중',
        '1' => '평가',
        '2' => '대상아님'
    );

    return $ev_review_option;
}

function getEvRelationStateOption()
{
    $ev_relation_state_option = array(
        "1" => "평가",
        "2" => "대상아님",
        "3" => "불일치",
        "4" => "확인중",
        "5" => "자기평가",
    );

    return $ev_relation_state_option;
}

function getEvUnitTypeOption()
{
    $ev_unit_type_option = array(
        '1'     => '점수형(1~5점)',
        '2'     => '점수형(1~10점)',
        '3'     => '점수형(1~100점)',
        '4'     => '서술형(단문)',
        '5'     => '서술형(장문)',
        '6'     => '공감형(1~5)',
        '7'     => '공감형(1~10)',
        '8'     => '객관형',
        '9'     => '객관형(복수선택)',
        '100'   => '자기평가기술서',
        '99'    => '평가아님',
    );

    return $ev_unit_type_option;
}

function getEvUnitTypeNumberOption()
{
    $ev_unit_type_option = array(
        '1' => array(
            "title"  => "점수형(1~5점)",
            "option" => array(
                '1'     => '1점',
                '2'     => '2점',
                '3'     => '3점',
                '4'     => '4점',
                '5'     => '5점',
            )
        ),
        '2' => array(
            "title"  => "점수형(1~10점)",
            "option" => array(
                '1'     => '1점',
                '2'     => '2점',
                '3'     => '3점',
                '4'     => '4점',
                '5'     => '5점',
                '6'     => '6점',
                '7'     => '7점',
                '8'     => '8점',
                '9'     => '9점',
                '10'    => '10점',
            )
        ),
        '3' => array(
            "title"     => "점수형(1~100점)",
            "option"    => [],
        ),
        '4' => array(
            "title"     => "서술형(단문)",
            "option"    => [],
        ),
        '5' => array(
            "title"     => "서술형(장문)",
            "option"    => [],
        ),
        '6' => array(
            "title"  => "공감형(1~5)",
            "option" => array(
                '1'     => '1점(매우 아니다)',
                '2'     => '2점(아니다)',
                '3'     => '3점(보통)',
                '4'     => '4점(그렇다)',
                '5'     => '5점(매우 그렇다)',
            )
        ),
        '7' => array(
            "title"  => "공감형(1~10)",
            "option" => array(
                '1'     => '1점(매우 아니다)',
                '2'     => '2점',
                '3'     => '3점(아니다)',
                '4'     => '4점',
                '5'     => '5점(조금 아니다)',
                '6'     => '6점(조금 그렇다)',
                '7'     => '7점',
                '8'     => '8점(그렇다)',
                '9'     => '9점',
                '10'    => '10점(매우 그렇다)',
            )
        ),
        '8' => array(
            "title"     => "객관형",
            "option"    => [],
        ),
        '9' => array(
            "title"     => "객관형(복수선택)",
            "option"    => [],
        )
    );

    return $ev_unit_type_option;
}

function getEvResultTypeOption()
{
    $ev_result_type_option = array(
        'evaluation_value'  => "원점수",
        'sqrt'              => "환산점수",
    );

    return $ev_result_type_option;
}