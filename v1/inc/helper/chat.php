<?php

function getChatTypeOption()
{
    $chat_type_option = array(
        "1" => array("title" => "사내공지", "new_cnt" => 0),
        "2" => array("title" => "와플알림", "new_cnt" => 0),
        "4" => array("title" => "알림톡(팝업)", "new_cnt" => 0),
        "3" => array("title" => "1:1 메세지", "new_cnt" => 0),
    );

    return $chat_type_option;
}


