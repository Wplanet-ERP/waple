<?php

function getProjectStateOption()
{
    $state_option = array(
        '1' => '미확정',
        '2' => '확정',
        '3' => '미확정(종료)'
    );

    return $state_option;
}

function getCalStateOption()
{
    $cal_state_option = array(
        '1' => '비정산',
        '2' => '정산'
    );

    return $cal_state_option;
}

function getProjectWorkStateOption()
{
    $work_state_option = array(
        '1' => array('color' => 'black', 'label' => "작성중"),
        '2' => array('color' => 'black', 'label' => "대기"),
        '3' => array('color' => 'red', 'label' => "진행요청"),
        '4' => array('color' => 'blue', 'label' => "접수완료"),
        '5' => array('color' => 'green', 'label' => "진행중"),
        '6' => array('color' => 'black', 'label' => "진행완료"),
        '7' => array('color' => 'red', 'label' => "보류"),
        '8' => array('color' => 'red', 'label' => "취소완료"),
        '9' => array('color' => 'black', 'label' => "진행불가")
    );

    return $work_state_option;
}


# 파트너 구분 배열
function getProjectParticipationStateOption()
{
    $pj_participation_state_option = array(
        '0' => "작성중",
        '1' => "참여 요청",
        '2' => "참여 수락",
        '3' => "참여 거절",
        '4' => "참여 취소"
    );

    return $pj_participation_state_option;
}

function getContractTypeOption()
{
    $contract_type_option = array(
        '1' => "입찰",
        '2' => "수의",
        '3' => '기타'
    );

    return $contract_type_option;
}

function getExpertiseOption()
{
    $expertise_option = array(
        "1" 	=> "선배창업가",
        "2" 	=> "경영 & 전략",
        "3" 	=> "마케팅 & 세일즈",
        "4" 	=> "기술개발",
        "5" 	=> "지식재산",
        "6" 	=> "세무/회계",
        "7" 	=> "투자",
        "8" 	=> "글로벌",
        "9" 	=> "진로 & 창업",
        "10" 	=> "기타",
        "11" 	=> "인사노무",
    );

    return $expertise_option;
}

function getCalMethodOption()
{
    $cal_method_option = array(
        "1" => "내부",
        "2" => "사업비",
    );

    return $cal_method_option;
}

function getSpendMethodOption()
{
    $spend_method_option = array(
        "1" => "지출품의서",
        "2" => "개인경비",
        "3" => "출금리스트",
    );

    return $spend_method_option;
}

function getBudgetConfirmOption()
{
    $budget_confirm_option = array(
        '1' => '미확정',
        '2' => '확정'
    );

    return $budget_confirm_option;
}

function getExpensesStatusOption()
{
    $expenses_status_option = array(
        "1" => "작성중",
        "2" => "작성완료",
        "3" => "승인완료",
        "4" => "지급완료",
    );

    return $expenses_status_option;
}

function getResourceTypeOption()
{
    $resource_type_option = array(
        'lector'    => '강사',
        'mentor'    => '멘토',
        'audit'     => '심사',
        'part_time' => '알바'
    );

    return $resource_type_option;
}

function getPartnerStateOption()
{
    $partner_state_option = array(
        '1' => '가입대기',
        '2' => '가입완료',
        '3' => '탈퇴신청',
        '4' => '탈퇴완료'
    );

    return $partner_state_option;
}

function getBankBookOption()
{
    $bankbook_option = array(
        "1" => "와이즈플래닛",
        "2" => "미디어커머스",
        "3" => "일사공이랩",
        "4" => "청창사",
        "5" => "탄소진흥원",
        "6" => "2023탄소진흥원"
    );

    return $bankbook_option;
}

function getBankOption()
{
    $bank_option = array('경남','광주','국민','기업','농협','대구','새마을금고','수협','스탠다드차타드','시티', '신한', '신협', '씨티', '외환', '우리', '우체국', '전북', '제일', '카카오뱅크', '하나', 'BNK부산은행');

    return $bank_option;
}