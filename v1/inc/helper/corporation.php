<?php

function getCardTypeOption()
{
    $card_type_option = array(
        '1' => '지정자',
        '2' => '공용',
        '3' => '개인'
    );

    return $card_type_option;
}

function getIsExpiredOption()
{
    $is_expired_option = array(
        '1' => '만료임박',
        '2' => '만료',
    );

    return $is_expired_option;
}

function getAccountTypeOption()
{
    $account_type_option = array(
        "1" => "입금",
        "2" => "출금",
    );

    return $account_type_option;
}
