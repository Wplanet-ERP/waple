<?php

function getCurYearOption()
{
    $cur_year   = date("Y");
    $pre_year   = date("Y", strtotime("{$cur_year} -1 years"));
    $last_year  = date("Y", strtotime("{$pre_year} -1 years"));

    return array($cur_year, $pre_year, $last_year);
}

function getMonthOption()
{
    return array("01","02","03","04","05","06","07","08","09","10","11","12");
}

function getDayChartOption()
{
    $type_day_option = array(
        '1' => '날짜(월별)',
        '2' => '날짜(주별)',
        '3' => '날짜(일별)'
    );

    return $type_day_option;
}

function getDateChartOption()
{
    $type_date_option = array(
        '1' => '월',
        '2' => '화',
        '3' => '수',
        '4' => '목',
        '5' => '금',
        '6' => '토',
        '0' => '일'
    );

    return $type_date_option;
}

function getDateDescChartOption()
{
    $type_date_desc_option = array(
        '0' => '일',
        '6' => '토',
        '5' => '금',
        '4' => '목',
        '3' => '수',
        '2' => '화',
        '1' => '월',
    );

    return $type_date_desc_option;
}

function getTimeChartOption()
{
    $type_time_option = array(
        '0'  => '00시~01시',
        '1'  => '01시~02시',
        '2'  => '02시~03시',
        '3'  => '03시~04시',
        '4'  => '04시~05시',
        '5'  => '05시~06시',
        '6'  => '06시~07시',
        '7'  => '07시~08시',
        '8'  => '08시~09시',
        '9'  => '09시~10시',
        '10' => '10시~11시',
        '11' => '11시~12시',
        '12' => '12시~13시',
        '13' => '13시~14시',
        '14' => '14시~15시',
        '15' => '15시~16시',
        '16' => '16시~17시',
        '17' => '17시~18시',
        '18' => '18시~19시',
        '19' => '19시~20시',
        '20' => '20시~21시',
        '21' => '21시~22시',
        '22' => '22시~23시',
        '23' => '23시~24시'
    );

    return $type_time_option;
}

function getHourOption()
{
    $hour_option = array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');

    return $hour_option;
}

function getMinOption()
{
    $min_option = array('00','10','20','30','40','50');

    return $min_option;
}

function getDayOption()
{
    $day_option = array("일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일");

    return $day_option;
}

function getDayShortOption()
{
    $day_option = array('일','월','화','수','목','금','토');

    return $day_option;
}