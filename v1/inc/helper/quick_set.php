<?php

function getQuickTypeOption()
{
    $quick_type_option = array(
        "work"          => "와이즈 업무",
        "cms"           => "커머스 상품",
        "cms_unit"      => "커머스 구성품(SKU)",
        "cms_with"      => "커머스 상품",
        "cms_unit_with" => "커머스 구성품(SKU)",
        "navigation"    => "메뉴(NAVI)",
        "staff"         => "계정 리스트"
    );

    return $quick_type_option;
}
