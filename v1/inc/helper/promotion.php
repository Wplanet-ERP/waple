<?php

function getPromotionChannelOption()
{
    $promotion_channel = array(
        '1'     => '블로그',
        '7'     => '인플루언서',
        '2'     => '인스타그램',
        '5'     => '촬영',
        '8'     => '유튜브',
        '4'     => '카페',
        '6'     => '컨텐츠'
    );

    return $promotion_channel;
}

function getPromotionKindOption()
{
    $promotion_kind = array(
        '1' => '체험단',
        '4' => '체험단(+보상)',
        '2' => '기자단',
        '3' => '배송체험단',
        '5' => '배송체험단(+보상)'
    );

    return $promotion_kind;
}

function getTotalStateOption()
{
    $total_state_option  = array(1 => '접수대기', 2 => '접수완료', 3 => '모집중', 4 => '진행중', 5 => '마감', 6 => "진행중단");

    return $total_state_option;
}

function getShortStateOption()
{
    $short_state_option = array(0 => '진행중', 1 => '진행중단');

    return $short_state_option;
}

function getPromotionDateTypeOption()
{
    $date_type_option = array('1' => "모집일", "2" => "발표일", "3" => "마감일", "4" => "보상일", "today" => "모집/발표/마감일");

    return $date_type_option;
}

function getApplicationStateOption()
{
    $application_state_option = array(
        "1" => "선정",
        "2" => "탈락",
        "3" => "선정취소",
    );

    return $application_state_option;
}

function getRewardStateOption()
{
    $reward_state_option = array(
        "4" => "진행중",
        "6" => "마감"
    );

    return $reward_state_option;
}

function getRewardAccountOption()
{
    $reward_account_option = array(
        "y" => "계좌번호(유)",
        "n" => "계좌번호(무)"
    );

    return $reward_account_option;
}

function getRewardTaxOption()
{
    $reward_tax_option = array(
        "y" => "저장 완료",
        "n" => "저장 미완료"
    );

    return $reward_tax_option;
}

function getRewardRefundOption()
{
    $reward_refund_option = array(
        "y" => "지급 완료",
        "n" => "지급 미완료"
    );

    return $reward_refund_option;
}

function getRewardRefundCntOption()
{
    $promotion_back_option = array('0','1','2','3','4','5');

    return $promotion_back_option;
}

function getPromotionBankOption()
{
    $promotion_back_option = array(
        "국민" => "국민은행",
        "농협" => "농협",
        "우리" => "우리은행",
        "SC"  => "SC은행",
        "기업" => "기업은행",
        "외환" => "외환은행",
        "수협" => "수협중앙회",
        "신한" => "신한(조흥)은행",
        "씨티" => "한국씨티은행",
        "대구" => "대구은행",
        "부산" => "부산은행",
        "산업" => "한국산업은행",
        "광주" => "광주은행",
        "제주" => "제주은행",
        "전북" => "전북은행",
        "경남" => "경남은행",
        "하나" => "하나은행",
        "우체국" => "우체국",
        "새마을금고" => "새마을금고",
        "산림조합" => "산림조합",
        "신협" => "신협",
        "카카오뱅크" => "카카오뱅크",
        "K뱅크" => "K뱅크",
        "저축은행" => "저축은행",
        "토스뱅크" => "토스뱅크",
    );

    return $promotion_back_option;
}


function getPromoChannelWorkOption()
{
    $promotion_channel = array(
        '1'     => '02419',
        '7'     => '02420',
        '2'     => '02421',
        '5'     => '02422',
        '8'     => '02423',
        '4'     => '02424',
        '6'     => '02425'
    );

    return $promotion_channel;
}
