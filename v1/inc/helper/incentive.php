<?php

function getIncStateOption()
{
    $inc_state_option = array(
        '1' => array('title' => '미확인', 'class' => 'color-red'),
        '2' => array('title' => '확인', 'class' => 'color-blue'),
        '3' => array('title' => '대상아님', 'class' => 'color-black'),
    );

    return $inc_state_option;
}

function getIncTypeOption()
{
    $inc_type_option = array(
        'deposit' => array('title' => '매출(입금)', 'class' => 'color-blue'),
        'withdraw' => array('title' => '비용(출금)', 'class' => 'color-red'),
    );

    return $inc_type_option;
}

function getIncSortOption()
{
    $inc_sort_option = array(
        'confirm_date'   => "입/출금완료일순",
        'incentive_date' => "인센확인순"
    );

    return $inc_sort_option;
}

function getEstimateIncStateOption()
{
    $incentive_state = array(
        '1' => '정산안됨',
        '2' => '정산요청(담당자)',
        '3' => '반려(재무팀)',
        '4' => '승인(재무팀)',
        '5' => '반려(대표)',
        '6' => '승인(대표)',
        '7' => '최종확인완료(담당자)',
        '8' => '지급완료(재무팀)'
    );

    return $incentive_state;
}