<?php

function getBaMediaOption()
{
    $ba_media_option = array(
        "CBS-RADIO" => "CBS-RADIO",
        "CJ E&M-TV" => "CJ E&M-TV",
        "EBS-RADIO" => "EBS-RADIO",
        "EBS-TV"    => "EBS-TV",
        "KBS-RADIO" => "KBS-RADIO",
        "KBS-TV"    => "KBS-TV",
        "KT IPTV"   => "KT IPTV",
        "MBC-RADIO" => "MBC-RADIO",
        "MBC-TV"    => "MBC-TV",
        "SBS-RADIO" => "SBS-RADIO",
        "SBS-TV"    => "SBS-TV",
    );

    return $ba_media_option;
}

function getBaStateOption()
{
    $ba_state_option = array(
        '1' => '신청',
        '2' => '확인중',
        '3' => '확정',
        '4' => '제외'
    );

    return $ba_state_option;
}

function getBaExcelOption()
{
    $ba_excel_option = array(
        '1' => '와이즈 기본양식'
    );

    return $ba_excel_option;
}

function getBaXyTypeOption()
{
    $type_option = array(
        'day'        => '날짜',
        'media'      => '매체',
        'date'       => '요일',
        'time'       => '시간대',
        'advertiser' => '광고주'
    );

    return $type_option;
}

function getBaZtypeOption()
{
    $z_type_option = array(
        '1' => '광고료(금액)',
        '2' => '광고료(청구광고료)',
        '3' => '횟수',
    );

    return $z_type_option;
}

function getBaOrdTypeOption()
{
    $ord_type_option = array(
        '0' => '합계(내림차순)',
        '1' => '합계(올림차순)',
        '2' => 'X축(내림차순)',
        '3' => 'X축(올림차순)',
        '4' => 'Y축(내림차순)',
        '5' => 'Y축(올림차순)'
    );

    return $ord_type_option;
}

function calBcData($bc_data, $comp_value, $sch_type, $sch_main_type, $type_date)
{
    $result = false;

    switch($sch_type)
    {

        case 'day':
            $sub_s_date = ($sch_main_type == '1') ? $bc_data['sub_s_mon'] : (($sch_main_type == '2') ? $bc_data['sub_s_week'] : $bc_data['sub_s_date']);
            $sub_e_date = ($sch_main_type == '1') ? $bc_data['sub_e_mon'] : (($sch_main_type == '2') ? $bc_data['sub_e_week'] : $bc_data['sub_e_date']);

            if(empty($sub_e_date) || $sub_e_date == '00000000' || $sub_e_date == '000000'){
                if($sub_s_date <= $comp_value){
                    $result = true;
                }
            }elseif($sub_s_date <= $comp_value && $comp_value <= $sub_e_date){
                $result = true;
            }
            break;
        case 'date':
            if($bc_data['date_w'] == $type_date[$comp_value]){
                $result = true;
            }
            break;
        case 'time':
            if($bc_data['s_time'] == $comp_value){
                $result = true;
            }
            break;
        case 'advertiser':
            if($bc_data['adv_no'] == $comp_value){
                $result = true;
            }
            break;
        case 'media':
            if($bc_data['media'] == $comp_value){
                $result = true;
            }
            break;
    }

    return $result;
}