<?php

function getWorkDetailOption()
{
    return array(
        "wm"   => array(
            "title"     => "MY WM",
            "nav_no"    => "240",
        ),
        "product"   => array(
            "title"     => "상품개발관리실",
            "nav_no"    => "217",
        ),
        "operate"   => array(
            "title"     => "마케팅 운영팀",
            "nav_no"    => "220",
        ),
        "doc"   => array(
            "title"     => "닥터피엘팀",
            "nav_no"    => "232",
        ),
    );
}

function getInOutCalendarCategoryOption()
{
    return array(
        "입고일정확정"           => "입고일정확정",
        "입고확인서 완료(종료)"   => "입고확인서 완료",
        "입고완료"              => "입고완료",
        "입고요청"              => "입고요청",
    );
}

function getWmWithdrawProductOption()
{
    return array(
        "12"    => "검색&디스플레이 광고",
        "10"    => "언론기사",
        "139"   => "오픈마켓/소셜커머스",
        "9"     => "지식인 리뷰",
        "6"     => "모바일 리뷰",
        "58"    => "선정 대상 앰블램",
        "288"   => "카페바이럴",
        "5"     => "카페 리뷰",
    );
}

function getWorkStateQuickOption()
{
    return array(
        "2" => array("title" => "MY 업무처리", "cnt" => 0),
        "1" => array("title" => "MY 업무요청", "cnt" => 0),
        "3" => array("title" => "처리자 미설정", "cnt" => 0)
    );
}

function getTeamWorkStateQuickOption()
{
    return array(
        "2" => "TEAM 업무처리",
        "1" => "TEAM 업무요청"
    );
}