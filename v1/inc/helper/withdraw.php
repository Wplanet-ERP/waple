<?php

function getWdStateOption()
{
    $wd_state_option = array(
        '1' => '미출금',
        '2' => '출금요청',
        '3' => '출금완료',
        '4' => '취소완료'
    );

    return $wd_state_option;
}

function getWdMethodOption()
{
    $wd_method_option = array(
        '1' => '계좌이체',
        '2' => '법인카드',
        '3' => '선결제차감',
        '4' => '월정산지급',
        '5' => '기타'
    );

    return $wd_method_option;
}

function getWdTaxStateOption()
{
    $wd_tax_state_option = array(
        '1' => '수령대기',
        '2' => '수령완료',
        '3' => '취소완료',
        '4' => '해당사항없음'
    );

    return $wd_tax_state_option;
}

function getWdIsInspectionOption()
{
    $wd_is_inspection_option = array(
        '1' => "YES(check)",
        '2' => "NO",
    );

    return $wd_is_inspection_option;
}

function getWdVatChoiceOption()
{
    $wd_vat_choice_option = array(
        "1" => "부가세 [+10%]",
        "2" => "소득세 원천징수 [-3.3%]",
        "3" => "해당사항없음",
    );

    return $wd_vat_choice_option;
}