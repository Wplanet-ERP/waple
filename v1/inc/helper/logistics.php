<?php

function getSubjectOption()
{
    $subject_option = array(
        "1"     => array(
            "title"     => "타계정반출::광고선전비",
            "explain"   => "체험단, 현장 광고 등 공개적으로 모집하여 자사 상품 지급",
            "display"   => "1",
            "dp_c_no"   => "5475",
            "dp_c_name" => "WISE_광고선전비"
        ),
        "9"     => array(
            "title"     => "타계정반출::시딩반출",
            "explain"   => "유투버, 인플루언서, 블로거 등 먼저 제안하여 상품 무료 증정건",
            "display"   => "1",
            "dp_c_no"   => "5476",
            "dp_c_name" => "WISE_시딩반출"
        ),
        "2"     => array(
            "title"     => "타계정반출::소모품비",
            "explain"   => "미팅 샘플, 촬영 샘플, 대량구매 전 샘플 제공, 라이브방송 DP샘플 등",
            "display"   => "1",
            "dp_c_no"   => "5477",
            "dp_c_name" => "WISE_소모품비"
        ),
        "3"     => array(
            "title"     => "타계정반출::접대비",
            "explain"   => "미팅 손님 선물, 외부 선물, 명절 선물 등",
            "display"   => "1",
            "dp_c_no"   => "5478",
            "dp_c_name" => "WISE_접대비"
        ),
        "4"     => array(
            "title"     => "타계정반출::복리후생비",
            "explain"   => "신규 입사 직원 지원, 퇴사자 제공, 임직원 사은품 제공 등",
            "display"   => "1",
            "dp_c_no"   => "5479",
            "dp_c_name" => "WISE_복리후생비"
        ),
        "5"     => array(
            "title"     => "타계정반출::폐기손실",
            "explain"   => "불량 제품 폐기 업체로 전송 후 폐기 진행 시, 혹은 이미 폐기 완료된 건",
            "display"   => "1",
            "dp_c_no"   => "5480",
            "dp_c_name" => "WISE_폐기손실"
        ),
        "11"    => array(
            "title"     => "타계정반출::기타",
            "explain"   => "",
            "display"   => "1",
            "dp_c_no"   => "5482",
            "dp_c_name" => "WISE_타계정반출(기타)"
        ),
        "14"    => array(
            "title"     => "타계정반출::잡손실",
            "explain"   => "고객 교환 시 기존 수령제품 회수없이 추가 제품 제공된 건",
            "display"   => "1",
            "dp_c_no"   => "5939",
            "dp_c_name" => "WISE_잡손실"
        ),
        "15"    => array(
            "title"     => "타계정반출::감모손실",
            "explain"   => "재고실사의 결과로 과부족재고의 재고고정 진행",
            "display"   => "1",
            "dp_c_no"   => "6002",
            "dp_c_name" => "WISE_감모손실"
        ),
        "12"    => array(
            "title"     => "판매반출",
            "explain"   => "대량구매, 쿠팡(로켓배송) 등 선매입형 매출 발생 반출 건",
            "display"   => "1",
            "dp_c_no"   => "",
            "dp_c_name" => ""
        ),
        "6"     => array(
            "title"     => "매장반출",
            "explain"   => "매출 발생 출고 건",
            "display"   => "2",
            "dp_c_no"   => "",
            "dp_c_name" => ""
        ),
        "8"     => array(
            "title"     => "대량구매",
            "explain"   => "대량구매 건",
            "display"   => "2",
            "dp_c_no"   => "",
            "dp_c_name" => ""
        ),
        "10"    => array(
            "title"     => "대량구매 (쿠팡_로켓배송)",
            "explain"   => "※주의 : 제트배송아님",
            "display"   => "2",
            "dp_c_no"   => "",
            "dp_c_name" => ""
        ),
        "7"     => array(
            "title"     => "재고이동",
            "explain"   => "재고이동 건",
            "display"   => "2",
            "dp_c_no"   => "",
            "dp_c_name" => ""
        ),
        "13"    => array(
            "title"     => "기타반출",
            "explain"   => "",
            "display"   => "1",
            "dp_c_no"   => "",
            "dp_c_name" => ""
        )
    );

    return $subject_option;
}

function getSubjectNameOption()
{
    $subject_option = array(
        "1"     => "타계정반출::광고선전비",
        "2"     => "타계정반출::소모품비",
        "3"     => "타계정반출::접대비",
        "4"     => "타계정반출::복리후생비",
        "5"     => "타계정반출::폐기손실",
        "6"     => "매장반출",
        "7"     => "재고이동",
        "8"     => "대량구매",
        "9"     => "타계정반출::시딩출고",
        "10"    => "대량구매 (쿠팡_로켓배송)",
        "11"    => "타계정반출::기타",
        "12"    => "판매반출",
        "13"    => "기타반출",
        "14"    => "타계정반출::잡손실",
        "15"    => "타계정반출::감모손실",
    );

    return $subject_option;
}

function getReportSubjectNameOption()
{
    $report_subject_option = array(
        "1"     => "타계정반출::광고선전비",
        "2"     => "타계정반출::소모품비",
        "3"     => "타계정반출::접대비",
        "4"     => "타계정반출::복리후생비",
        "5"     => "타계정반출::폐기손실",
        "9"     => "타계정반출::시딩출고",
        "11"    => "타계정반출::기타",
        "14"    => "타계정반출::잡손실",
        "15"    => "타계정반출::감모손실",
    );

    return $report_subject_option;
}

function getUseBmSubjectNameOption()
{
    $bm_subject_option = array(
        "1"     => "광고선전비",
        "9"     => "시딩출고",
        "2"     => "소모품비",
        "3"     => "접대비",
        "4"     => "복리후생비",
        "5"     => "폐기손실",
        "14"    => "잡손실",
        "15"    => "감모손실",
    );

    return $bm_subject_option;
}

function getStockTypeOption()
{
    $stock_type_option = array(
        '1' => '입고',
        '2' => '반출',
        '3' => '재고이동'
    );

    return $stock_type_option;
}

function getStockTypeDetailOption()
{
    $stock_type_option = array(
        '1' => array('title' => '입고', 'explain' => "사용"),
        '2' => array('title' => '반출', 'explain' => "광고/마케팅, 샘플, 촬영, 선물, 대량구매, 선매입 공급, 고객 판매 건"),
        '3' => array('title' => '재고이동', 'explain' => "창고간 재고이동 또는 홈쇼핑, 쿠팡(제트배송), 파스토 등 후불 정산 위탁물류 건"),
    );

    return $stock_type_option;
}

function getDeliveryTypeOption()
{
    $delivery_type_option = array(
        "1" => "택배",
        "2" => "입/출고요청",
        "4" => "발주",
        "5" => "발주 외 전체",
//        "3" => "직접전달",
    );

    return $delivery_type_option;
}

function getAddrFilterOption()
{
    $addr_filter_option = array(
        'sender'    => '발송지',
        'receiver'  => '수령지',
    );

    return $addr_filter_option;
}

function getStockAddrOption()
{
    $stock_addr_option = array(
        "2809"  => "위드플레이스",
        "1113"  => "와이즈미디어커머스",
        "99999" => "기타"
    );

    return $stock_addr_option;
}

function getIsOrderOption()
{
    $is_order_option = array(
        "1" => "유",
        "2" => "무",
    );

    return $is_order_option;
}

function getAddressOption()
{
    $address_option = array(
        "1" => array(
            "zipcode"   => "10864",
            "address1"  => "경기도 파주시 교하로 1173 (오도동114-16)",
            "address2"  => "위드플레이스",
            "name"      => "윤종혁",
            "hp"        => "010-6500-1898",
        ),
        "2" => array(
            "zipcode"   => "08513",
            "address1"  => "서울 금천구 벚꽃로 244 (가산동, 벽산디지털밸리5차)",
            "address2"  => "906호",
            "name"      => "김보균",
            "hp"        => "010-7107-3280",
        )
    );

    return $address_option;
}

function getStatsMainOption()
{
    $stats_main_option = array(
        'day'      => '날짜',
        'subject'  => '계정과목',
        'unit'     => '구성품',
        'company'  => '납품업체',
    );

    return $stats_main_option;
}

function getProductTypeOption()
{
    $product_type_option = array(
        '1' => "상품",
        '2' => "구성품목(SKU)"
    );

    return $product_type_option;
}

function getSchType($sch_type, $sch_type_main, $type_day, $type_subject, $type_unit, $type_company)
{
    $type_main_list         = [];
    $add_type_where         = "";
    $add_type_column        = "";
    $add_type_column_name   = "";

    switch($sch_type)
    {
        case 'day':
            $type_main_list = $type_day;
            if ($sch_type_main == '1') //월간
            {
                $add_type_column      = "DATE_FORMAT(lm.run_date, '%Y%m')";
                $add_type_column_name = "DATE_FORMAT(lm.run_date, '%Y-%m')";
            }
            elseif ($sch_type_main == '2') //주간
            {
                $add_type_column      = "DATE_FORMAT(DATE_SUB(lm.run_date, INTERVAL (IF(DAYOFWEEK(lm.run_date)=1,8,DAYOFWEEK(lm.run_date))-2) DAY), '%Y%m%d')";
                $add_type_column_name = "CONCAT(DATE_FORMAT(DATE_SUB(lm.run_date, INTERVAL (IF(DAYOFWEEK(lm.run_date)=1,8,DAYOFWEEK(lm.run_date))-2) DAY), '%Y-%m-%d'),' ~ ', DATE_FORMAT(DATE_SUB(lm.run_date, INTERVAL(IF(DAYOFWEEK(lm.run_date)=1,8,DAYOFWEEK(lm.run_date))-8) DAY), '%Y-%m-%d'))";
            }
            else //일간
            {
                $add_type_column      = "DATE_FORMAT(lm.run_date, '%Y%m%d')";
                $add_type_column_name = "DATE_FORMAT(lm.run_date, '%Y-%m-%d')";
            }
            break;

        case 'subject':
            $type_main_list  = $type_subject;
            $add_type_column = "lm.subject";
            if (!empty($sch_type_main)) {
                $add_type_where = " AND lm.subject='{$sch_type_main}'";
            }

            break;

        case 'unit':
            $type_main_list     = $type_unit;
            $add_type_column    = "lp.prd_no";
            if (!empty($sch_type_main)) {
                $add_type_where = " AND lp.prd_no='{$sch_type_main}'";
            }
            break;

        case 'company':
            $type_main_list         = $type_company;
            $add_type_column        = "lm.run_c_no";
            $add_type_column_name   = "(SELECT c.c_name FROM company c WHERE c.c_no=lm.run_c_no)";
            if (!empty($sch_type_main)) {
                $add_type_where = " AND lm.run_c_no='{$sch_type_main}'";
            }
            break;
    }

    $sch_type_data = array(
        "type_main_list"        => $type_main_list,
        "add_type_where"        => $add_type_where,
        "add_type_column"       => $add_type_column,
        "add_type_column_name"  => $add_type_column_name
    );

    return $sch_type_data;
}

function getAddrCompanyOption()
{
    $addr_company_option = array(
        "2809"  => "위드플레이스",
        "1113"  => "와이즈미디어커머스",
        "5468"  => "WISE_쿠팡(로켓배송)",
        "5469"  => "WISE_쿠팡(제트배송)",
        "5494"  => "지그재그(직진배송)",
        "5545"  => "이마트(트레이더스)",
        "4752"  => "이마트(트레이더스)_(주)프라우즈",
        "5484"  => "파스토",
        "2280"  => "WISE_대량구매",
        "1372"  => "아임웹_베라베프",
        "5800"  => "아임웹_닥터피엘",
        "5427"  => "스마트스토어_닥터피엘",
        "4629"  => "스마트스토어_누잠",
        "5588"  => "스마트스토어_아이레놀",
        "3295"  => "스마트스토어_베라베프",
        "2304"  => "(주)성일화학",
        "2305"  => "듀벨",
        "5100"  => "베스앤스킨웍스",
        "2772"  => "주식회사 피코그램",
        "5659"  => "스타트투데이_아이레놀",
        "5956"  => "스타트투데이_닥터피엘",
        "5710"  => "부메랑리턴",
        "5315"  => "(주)코진테크",
        "3934"  => "(주)포세이온",
        "5711"  => "아마존_닥터피엘(미국)",
        "5795"  => "아마존_아이레놀(일본)",
        "5885"  => "아마존_아이레놀(미국)",
        "5768"  => "편한가",
        "5777"  => "씨엔제이커머스",
        "5820"  => "마켓컬리",
        "5825"  => "Qxpress",
        "5834"  => "스토리월드",
        "5840"  => "네모네",
        "5736"  => "(주)한국콜마",
        "2889"  => "투에이치",
        "5897"  => "롯데하이마트_팝업",
        "5902"  => "롯데하이마트",
        "5935"  => "리씽크",
        "4798"  => "(주)에이원비앤에이치 (아이레놀)",
        "5970"  => "에스앤제이인터내셔널",
        "5264"  => "NS홈쇼핑",
        "4316"  => "롯데홈쇼핑",
        "6003"  => "WISE_대량구매(해외)",
        "2477"  => "(주)비앤비코리아",
        "5794"  => "(주)더보탬",
        "5803"  => "물류(기타)",
        "99999" => "기타",
    );

    return $addr_company_option;
}

function getMoveCompanyOption()
{
    $move_company_option = array(
        "5469"  => "쿠팡(제트배송)",
        "5494"  => "지그재그(직진배송)",
        "5545"  => "이마트(트레이더스)",
        "4752"  => "이마트(트레이더스)_(주)프라우즈",
        "5484"  => "파스토",
        "2809"  => "위드플레이스",
        "1113"  => "와이즈미디어커머스",
        "5659"  => "스타트투데이_아이레놀",
        "5956"  => "스타트투데이_닥터피엘",
        "5710"  => "부메랑리턴",
        "2304"  => "(주)성일화학",
        "5315"  => "(주)코진테크",
        "5711"  => "아마존_닥터피엘(미국)",
        "5795"  => "아마존_아이레놀(일본)",
        "5885"  => "아마존_아이레놀(미국)",
        "5768"  => "편한가",
        "5777"  => "씨엔제이커머스",
        "5825"  => "Qxpress",
        "5834"  => "스토리월드",
        "5840"  => "네모네",
        "5736"  => "(주)한국콜마",
        "2889"  => "투에이치",
        "5897"  => "롯데하이마트_팝업",
        "5935"  => "리씽크",
        "5970"  => "에스앤제이인터내셔널",
        "5100"  => "베스앤스킨웍스",
//        "5665"  => "재고이동(기타)",
    );

    return $move_company_option;
}

function getSalesCompanyOption()
{
    $sales_company_option = array(
        "5468"  => "쿠팡(로켓배송)",
        "2280"  => "WISE_대량구매",
        "1372"  => "아임웹_베라베프",
        "5800"  => "아임웹_닥터피엘",
        "5427"  => "스마트스토어_닥터피엘",
        "4629"  => "스마트스토어_누잠",
        "5588"  => "스마트스토어_아이레놀",
        "3295"  => "스마트스토어_베라베프",
        "5545"  => "이마트(트레이더스)",
        "5264"  => "NS홈쇼핑",
        "4316"  => "롯데홈쇼핑",
        "2304"  => "(주)성일화학",
        "2305"  => "듀벨",
        "5100"  => "베스앤스킨웍스",
        "2772"  => "주식회사 피코그램",
        "5710"  => "부메랑리턴",
        "3934"  => "(주)포세이온",
        "5820"  => "마켓컬리",
        "5315"  => "(주)코진테크",
        "5736"  => "(주)한국콜마",
        "5825"  => "Qxpress",
        "5902"  => "롯데하이마트",
        "5897"  => "롯데하이마트_팝업",
        "4798"  => "(주)에이원비앤에이치 (아이레놀)",
        "5970"  => "에스앤제이인터내셔널",
        "6003"  => "WISE_대량구매(해외)",
    );

    return $sales_company_option;
}

function getEtcSalesCompanyOption()
{
    $etc_sales_company_option = array(
        "2280"  => "WISE_대량구매",
        "5484"  => "파스토",
        "5469"  => "WISE_쿠팡(제트배송)",
        "5494"  => "지그재그(직진배송)",
        "5545"  => "이마트(트레이더스)",
        "4752"  => "이마트(트레이더스)_(주)프라우즈",
        "5659"  => "스타트투데이_아이레놀",
        "5956"  => "스타트투데이_닥터피엘",
        "2304"  => "(주)성일화학",
        "2305"  => "듀벨",
        "5100"  => "베스앤스킨웍스",
        "2772"  => "주식회사 피코그램",
        "5315"  => "(주)코진테크",
        "5710"  => "부메랑리턴",
        "5711"  => "아마존_닥터피엘(미국)",
        "5795"  => "아마존_아이레놀(일본)",
        "5885"  => "아마존_아이레놀(미국)",
        "5768"  => "편한가",
        "5777"  => "씨엔제이커머스",
        "5902"  => "롯데하이마트",
        "5970"  => "에스앤제이인터내셔널",
        "2477"  => "(주)비앤비코리아",
        "5794"  => "(주)더보탬",
        "5803"  => "물류(기타)",
    );

    return $etc_sales_company_option;
}

function getLogisticsQuickOption()
{
    $logistics_quick_option = array("1" => "요청중 | 쿠팡(로켓배송)", "2" => "요청중 | 쿠팡(제트배송)", "3" => "요청중 | 기타(로켓,제트외)");

    return $logistics_quick_option;
}

function getLogisticsDocType()
{
    return array(
        "1" => "사내창고[반출] : 방문수령",
        "2" => "사내창고[반출] : 판매 퀵 발송",
        "3" => "사내창고[반출] : A/S 퀵 발송",
        "4" => "사내창고[입고] : 교환/반품 입고",
    );
}

function getQuickDeliveryMethodOption()
{
    return array(
        "1" => "택배",
        "2" => "퀵/화물",
        "3" => "직접전달/픽업",
    );
}

function getQuickRunStaffList()
{
    return array(
        "179" => "위드플레이스",
        "311" => "위드물류1팀",
        "312" => "위드물류2팀",
        "247" => "위드1",
        "248" => "위드2",
        "249" => "위드3",
        "250" => "위드4",
    );
}