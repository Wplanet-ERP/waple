<?php

function getReportTypeOption()
{
    $report_type_option = array(
        '1' => 'EMP',
        '2' => '택배',
        '3' => '직접추가',
    );

    return $report_type_option;
}

function getTypeOption()
{
    $type_option = array(
        '1' => '입고',
        '2' => '반출',
        '3' => '택배',
    );

    return $type_option;
}

function getStockExpirationOption()
{
    $stock_expiration_option = array('30','60','90','120','180','210');

    return $stock_expiration_option;
}

function getReportQuickSearch()
{
    $quick_search_option = array(
        "1" => "구매입고(위드)",
        "2" => "기타입고(위드)",
        "3" => "판매출고(위드)",
        "4" => "기타출고(위드)",
        "5" => "구매입고(와이즈)",
        "6" => "기타입고(와이즈)",
        "7" => "판매출고(와이즈)",
        "8" => "기타출고(와이즈)",
    );

    return $quick_search_option;
}

function getConfirmStateOption()
{
    $confirm_state_option = array(
        "1"     => "발주[입고]",
        "2"     => "보상[입고]",
        "3"     => "이동[입고]",
        "4"     => "기타[입고]",
        "5"     => "반품[입고]",
        "9"     => "판매[반출]",
        "6"     => "이동[반출]",
        "10"    => "타계정[반출]",
        "8"     => "기타[반출]",
        "11"    => "기초재고",
        "12"    => "기말재고"
    );

    return $confirm_state_option;
}

function getConfirmStateMatchOption()
{
    $confirm_state_option = array(
        "발주입고"      => "1",
        "보상입고"      => "2",
        "내부이동"      => "3",
        "기타입고"      => "4",
        "AS입고"       => "4",
        "오배송입고"    => "4",
        "AS반출"       => "8",
        "기타반출"      => "8",
        "오배송반출"    => "8",
        "폐기반출"      => "10",
        "본사출고"      => "8",
        "업체반출"      => "8"
    );

    return $confirm_state_option;
}

function getMatchingStateByConfirmStateOption()
{
    $confirm_state_option = array(
        "1"     => "발주입고",
        "2"     => "보상입고",
        "3"     => "이동입고",
        "4"     => "기타입고",
        "5"     => "반품입고",
        "9"     => "판매반출",
        "6"     => "이동반출",
        "10"    => "타계정반출",
        "8"     => "기타반출",
        "11"    => "기초재고",
        "12"    => "기말재고"
    );

    return $confirm_state_option;
}

function getStockIsOrderOption()
{
    $is_order_option = array(
        "1" => "재고 반영(발주서)",
        "2" => "재고 미반영",
        "3" => "재고 반영(기타)"
    );

    return $is_order_option;
}

function getStockStateGroupOption()
{
    $stock_state_group_option   = array(
        '1' => array('state' => '판매반출', 'confirm' => '9'),
        '2' => array('state' => '타계정반출', 'confirm' => '10'),
        '3' => array('state' => '이동반출', 'confirm' => '6'),
        '4' => array('state' => '기타반출', 'confirm' => '8'),
        '5' => array('state' => '이동입고', 'confirm' => '3'),
    );

    return $stock_state_group_option;
}

function getStockInsertTypeOption()
{
    $stock_insert_type_option   = array(
        "1113" => "3",  # 와이즈미디어커머스
        "5484" => "3",  # 파스토
        "5469" => "3",  # WISE_쿠팡(제트배송)
        "5494" => "3",  # 지그재그(직진배송)
        "4752" => "3",  # 이마트(트레이더스)_(주)프라우즈
        "5659" => "3",  # 스타트투데이
//        "2304" => "3",  # 성일화학
//        "2305" => "3",  # 듀벨
//        "5100" => "3",  # 베스앤스킨웍스
//        "2772" => "3",  # 주식회사 피코그램
//        "5315" => "3",  # (주)코진테크
        "5710" => "3",  # 부메랑리턴
        "5777" => "3",  # 씨엔제이커머스
        "5711" => "3",  # 아마존(미국)_닥터피엘
        "5795" => "3",  # 아마존(일본)_아이레놀
        "5885" => "3",  # 아마존(미국)_아이레놀
        "5768" => "3",  # 편한가
        "5834" => "3",  # 스토리월드
        "5840" => "3",  # 네모네
        "5970" => "3",  # 에스앤제이인터내셔널
        "5475" => "2",  # WISE_광고선전비
        "5476" => "2",  # WISE_시딩반출
        "5477" => "2",  # WISE_소모품비
        "5478" => "2",  # WISE_접대비
        "5479" => "2",  # WISE_복리후생비
        "5480" => "2",  # WISE_폐기손실
        "5482" => "2",  # WISE_타계정반출(기타)
        "5939" => "2",  # WISE_잡손실
        "6002" => "2",  # WISE_감모손실
    );

    return $stock_insert_type_option;
}

function getStockInsertSubjectOption()
{
    $stock_insert_subject_option   = array("5475" => "1", "5476" => "9", "5477" => "2", "5478" => "3", "5479" => "4", "5480" => "5", "5482" => "11", "5939" => "14", "6002" => "15");

    return $stock_insert_subject_option;
}

function getStockMatchCompanyOption()
{
    $stock_insert_subject_option   = array("1" => "5475", "9" => "5476", "2" => "5477", "3" => "5478", "4" => "5479", "5" => "5480", "11" => "5482", "14" => "5939", "15" => "6002");

    return $stock_insert_subject_option;
}

function getAmazonStateOption()
{
    $amazon_state_option = array(
        "1"   => array(
            "WhseTransfers"     => array("state" => "이동입고", "confirm_state" => "3"),
            "CustomerReturns"   => array("state" => "반품입고", "confirm_state" => "5"),
            "Adjustments"       => array("state" => "정정입고", "confirm_state" => "4"),
            "Receipts"          => array("state" => "이동입고", "confirm_state" => "3"),
        ),
        "2"    => array(
            "WhseTransfers"     => array("state" => "이동반출", "confirm_state" => "6"),
            "Shipments"         => array("state" => "판매반출", "confirm_state" => "9"),
            "VendorReturns"     => array("state" => "폐기반출", "confirm_state" => "10"),
            "Adjustments"       => array("state" => "정정반출", "confirm_state" => "8"),
            "Receipts"          => array("state" => "정정반출", "confirm_state" => "8"),
        ),
    );

    return $amazon_state_option;
}
?>
