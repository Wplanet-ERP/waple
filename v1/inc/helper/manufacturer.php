<?php

function getWorkStateOption()
{
    $work_state_option = array(
        '1' => '작성중',
        '2' => '대기',
        '3' => '진행요청',
        '4' => '접수완료',
        '5' => '진행중',
        '6' => '진행완료',
        '7' => '보류',
        '8' => '취소완료',
        '9' => '진행불가'
    );

    return $work_state_option;
}

function getMiRunStateOption()
{
    $run_state_option = array(
        '3' => '진행중',
        '4' => '진행완료'
    );

    return $run_state_option;
}

function getIsImproveOption()
{
    $is_improve_option = array(
        "2" => "無",
        "1" => "有",
    );

    return $is_improve_option;
}