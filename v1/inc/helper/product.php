<?php

function getProductKindOption()
{
    $product_kind_option = array(
        '1' => '개별업무형(기본)',
        '2' => '세트업무형'
    );

    return $product_kind_option;
}

function getSchTypeOption()
{
    $sch_type_option = array(
        "1" => "설정방식(아래업체 선택)",
        "2" => "검색방식(자동완성 선택)"
    );

    return $sch_type_option;
}

function getWdDpStateOption()
{
    $sch_wd_dp_state_option = array(
        "1" => "미설정",
        "2" => "입금",
        "3" => "출금",
        "4" => "입금+출금"
    );

    return $sch_wd_dp_state_option;
}

function getScheduleOption()
{
    $schedule_option = array("1" => "O", "2" => "X");

    return $schedule_option;
}


?>
