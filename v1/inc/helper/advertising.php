<?php

function getAdvertisingStateOption()
{
    $adv_state_option = array(
        "1" => "신청중",
        "2" => "부킹중",
        "3" => "입찰확정",
        "4" => "취소완료",
        "5" => "진행완료",
        "6" => "미신청(종료)"
    );

    return $adv_state_option;
}

function getAdvertisingStateColorOption()
{
    $adv_state_option = array(
        "1" => "red",
        "2" => "gray",
        "3" => "blue",
        "4" => "gray",
        "5" => "black",
        "6" => "gray"
    );

    return $adv_state_option;
}

function getAdvertisingAgencyOption()
{
    $adv_agency_option = array(
        "1858" => "DMC미디어",
        "4472" => "나스미디어",
        "4355" => "메조미디어",
        "3735" => "모비데이즈"
    );

    return $adv_agency_option;
}

function getAdvertisingMediaOption()
{
    $adv_media_option = array(
        "265"   => "네이버(NOSP)",
        "266"   => "네이버(GFA)",
    );

    return $adv_media_option;
}

function getAdvertisingProductOption()
{
    $adv_product_option = array(
        "1"   => "타임보드",
        "2"   => "스폐셜DA",
        "3"   => "GFA",
    );

    return $adv_product_option;
}

function getAdvertisingTypeOption()
{
    $adv_product_option = array(
        "1"   => "미판",
        "2"   => "일반",
    );

    return $adv_product_option;
}

function getAutoAdvertisingFee()
{
    return array(
        "1858" => array("1" => "26", "2" => "22"),
        "3735" => array("1" => "26", "2" => "23", "3" => "14.5"),
        "4472" => array("1" => "26", "2" => "23"),
        "4355" => array("1" => "25", "2" => "22"),
    );
}

function getEventOption()
{
    $event_option = array(
        "1"     => "닥터피엘 상시 장바구니 이벤트(6만원)",
        "43"    => "닥터피엘 상시 장바구니 이벤트(7만원)",
        "2"     => "닥터피엘 상시 칫솔 이벤트",
        "37"    => "닥터피엘 상시 핫팩 이벤트(5만원)",
        "39"    => "닥터피엘 상시 핫팩 이벤트(7만원)",
        "3"     => "닥터피엘 스폐셜 DA 장바구니 이벤트",
        "4"     => "닥터피엘 스폐셜 DA 칫솔 이벤트",
        "10"    => "닥터피엘 라이브커머스_202403",
        "19"    => "닥터피엘 라이브커머스_202405",
        "27"    => "닥터피엘 네쇼페 라이브커머스(칫솔4P)",
        "30"    => "닥터피엘 카카오 쇼핑 라이브",
        "31"    => "11월 닥터피엘 세일 페스타",
        "14"    => "닥터피엘 브랜드 장바구니 이벤트",
        "23"    => "닥터피엘 브랜드 스티커 이벤트",
        "21"    => "닥터피엘 에코 피처형 정수기 이벤트",
        "32"    => "누잠 스폐셜 DA 이벤트(NEW)",
        "36"    => "누잠 스폐셜 DA 이벤트(보관백)",
        "22"    => "누잠 상시 주문금액 이벤트(신)",
        "40"    => "누잠 상시 주문금액 이벤트(2025)",
        "41"    => "아이레놀 설명절 특별혜택",
        "42"    => "아이레놀 2월한정 특별혜택",
        "44"    => "아이레놀 3월 봄맞이 브랜드위크",
        "16"    => "누잠 라이브커머스 주문금액 이벤트",
        "20"    => "누잠 라이브커머스 주문금액 이벤트_240624",
        "7"     => "아이레놀 상시 이벤트 사은품 1종류(주문금액)",
        "18"    => "아이레놀 상시 이벤트 사은품 1종류(구매시)",
        "11"    => "아이레놀 상시 이벤트 사은품 2종류",
        "8"     => "닥터피엘 여행용 상시 이벤트",
        "24"    => "닥터피엘 여행용 구매 이벤트",
        "5"     => "누잠 상시 주문금액 이벤트(구)",
        "6"     => "누잠 스폐셜 DA 이벤트(구)",
        "9"     => "누잠 스폐셜 DA 이벤트(신)",
        "13"    => "누잠 스폐셜 29CM 이벤트",
        "12"    => "누잠 상시 주문금액 10만원 이벤트(29CM)",
        "15"    => "누잠 상시 주문금액 15만원 이벤트(29CM)",
        "17"    => "누잠 상시 주문금액 이벤트(CJ온스타일)",
        "25"    => "아이레놀 파우치 증정 이벤트(9월)",
        "26"    => "아이레놀 파우치 증정 이벤트(10월)",
        "29"    => "아이레놀 파우치 증정 이벤트(11월)",
        "33"    => "아이레놀 파우치 증정 이벤트 (11월 블랙프라이데이 메인)",
        "34"    => "아이레놀 파우치 증정 이벤트 (11월 블랙프라이데이 서브)",
        "35"    => "아이레놀 파우치 증정 이벤트 (12월 겨울맞이 혜택)",
        "38"    => "아이레놀 파우치 증정 이벤트 (새해맞이 특별혜택)",
    );

    return $event_option;
}

function getEventProductOption()
{
    $event_product_option = array(
        "1"     => "1092",
        "2"     => "1271",
        "3"     => "1092",
        "4"     => "1271",
        "8"     => "1092",
        "10"    => "1092",
        "14"    => "1092",
        "19"    => "1092",
        "21"    => "1092",
        "23"    => "434",
        "24"    => "1092",
        "27"    => "903",
        "30"    => "1092",
        "31"    => "1271",
        "37"    => "2202",
        "39"    => "2202",
        "43"    => "1092",
    );

    return $event_product_option;
}

function getQuickSearchOption()
{
    $quick_search_option = array(
        "1" => "신청가능 구좌",
        "2" => "신청중",
        "3" => "입찰확정",
        "4" => "MY 신청중",
        "5" => "MY 입찰확정"
    );

    return $quick_search_option;
}

function getEventStateOption()
{
    $event_state_option = array(
        "1" => "진행중",
        "3" => "종료",
        "2" => "중단",
    );

    return $event_state_option;
}
