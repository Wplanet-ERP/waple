<?php

function getCommerceWorkStateOption()
{
    $work_state_option = array(
        "1" => "작성중",
        "2" => "진행중",
        "3" => "종료",
        "4" => "보류",
        "5" => "취소",
    );

    return $work_state_option;
}

function getLocationTypeOption()
{
    $location_type_option = array(
        '1' => '국내',
        '2' => '해외'
    );

    return $location_type_option;
}
?>
