<?php

function getPeOrderOption()
{
    $pe_order_option = array(
        'priority'      => '순번순',
        's_name'        => '이름순',
        'payment_date'  => '승인일순'
    );

    return $pe_order_option;
}

function getPeMyCompanyOption()
{
    $pe_my_company_option = array("default" => "와이즈플래닛", "commerce" => "미디어커머스");

    return $pe_my_company_option;
}

function getPeStateOption()
{
    $pe_state_option = array(
        '1' => '작성중',
        '2' => '작성완료',
        '3' => '승인중',
        '4' => '승인완료',
        '5' => '지급완료'
    );

    return $pe_state_option;
}

function getPeWdMethodOption()
{
    $wd_method_option = array(
        "1" => "법인카드",
        "5" => "기타",
        "6" => "법인(불공)",
        "7" => "현영",
    );

    return $wd_method_option;
}