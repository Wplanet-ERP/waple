<?php
// -------------------------------------------------- 사용 확인 -------------------------------------------------- //
// 메세지 출력 및 포워딩
function goto_url($url)
{
	echo "<script type='text/javascript'> location.href='$url'; </script>";
	exit;
}

function alert($msg='', $url='')
{
	if (!$msg) $msg = '올바른 방법으로 이용해 주십시오.';
	echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
	echo "<script type='text/javascript'>alert('$msg');";
	if (!$url)
	echo "history.go(-1);";
	echo "</script>";
	if ($url)
	// 4.06.00 : 불여우의 경우 아래의 코드를 제대로 인식하지 못함
	goto_url($url);
	exit;
}

# 2차원 배열 정렬
function array_sort($arr, $dimension)
{
	if(!empty($arr)){
		if($dimension){
			for($i = 0; $i < sizeof($arr); $i++) {
				array_unshift($arr[$i], $arr[$i][$dimension]);
			}
			@sort($arr);
			for($i = 0; $i < sizeof($arr); $i++) {
				array_shift($arr[$i]);
			}
		} else {
			@sort($arr);
		}
	}

	return $arr;
}

# 2차원 배열 역순 정렬
function array_rsort($arr, $dimension)
{
	if(!empty($arr)){
		if($dimension){
			for($i = 0; $i < sizeof($arr); $i++) {
				array_unshift($arr[$i], $arr[$i][$dimension]);
			}
			@rsort($arr);
			for($i = 0; $i < sizeof($arr); $i++) {
				array_shift($arr[$i]);
			}
		} else {
			@rsort($arr);
		}
	}

	return $arr;
}

function super_unique($array,$key) { // 다차원 배열의 중복 제거
	if(!empty($array)){
		$temp_array = [];
		foreach ($array as &$v) {
		   if (!isset($temp_array[$v[$key]]))
		   $temp_array[$v[$key]] =& $v;
		}
		$array = array_values($temp_array);
	}
		return $array;

}


// 탑블로그 양식에 맞게 페이징 함수 새로 추가 : 2015-09-03 오후 1:20
function pagelist($page, $web, $pagenum, $url) { // 관리자단

	$strpage="";
	$maxto = 10; //
	$nextpage = $page + 1;
	if ($nextpage > $pagenum) $nextpage = $pagenum;
	if ($pagenum >= ($page + $maxto))
	{
		if ($page<5)
		{
			$for_end=10;
		}else
		{
			if  (($page + 5)>$pagenum )
			{
				$for_end=$pagenum;
			}else
			{
				$for_end=$page+5;
			}
		}
	}
	else
	{
		if  (($page + 5)>$pagenum )
		{
			$for_end=$pagenum;
		}else
		{
			$for_end=$page+5;
		}

	}
	if (($page - 4)>1)
	{
		$for_begin=$page - 4;
	}else
	{
		$for_begin=1;
	}


	if ($page>1)
	{
		if ($url!="")
		{
			$strpage=$strpage."<a class=\"first\" href=\"".$web."?".$url."&page=1\">&lt;&lt;</a><a class=\"prev\" href=\"".$web."?".$url."&page=".($page-1)."\">&lt;</a>";
		}else
		{
			$strpage=$strpage."<a class=\"first\" href=\"".$web."&page=1\">&lt;&lt;</a><a class=\"prev\" href=\"".$web."?page=".($page-1)."\">&lt;</a>";
		}
	}  else
	{
		$strpage=$strpage."<a class=\"first\" href=\"#\" onclick=\"alert('맨 처음입니다');return false;\">&lt;&lt;</a><a class=\"prev\" href=\"#\" onclick=\"alert('이전 페이지가 없습니다');return false;\">&lt;</a>";
	}
	$strpage=$strpage."";
	for ($i = $for_begin; $i <= $for_end; $i++) {

		if ($i != $page){
			if ($url!="") {
				$strpage=$strpage. "<a href=\"".$web."?".$url."&page=".$i."\">$i</a>";
			} else {
				$strpage=$strpage. "<a href=\"".$web."?page=".$i."\">$i</a>";
			}
		} else {
			$strpage=$strpage. "<span class=\"on\">$i</span>";
		}
	}

	if ($page>=$pagenum) {
		$strpage=$strpage."<a class=\"next\" href=\"#\" onclick=\"alert('다음 페이지가 없습니다');return false;\">&gt;</a><a class=\"last\" href=\"#\" onclick=\"alert('맨 마지막입니다');return false;\">&gt;&gt;</a>";
	} else {
		if ($url!="") {
			$strpage=$strpage."<a class=\"next\" href=\"".$web."?".$url."&page=".$nextpage."\">&gt;</a><a class=\"last\" href=\"".$web."?".$url."&page=".$pagenum."\">&gt;&gt;</a>";
		} else {
			$strpage=$strpage."<a class=\"next\" href=\"".$web."?page=".$nextpage."\">&gt;</a><a class=\"last\" href=\"".$web."?page=".$pagenum."\">&gt;&gt;</a>";
		}
	}
	$strpage=$strpage."";
	return $strpage;
}

function stringCut($str,$len) {

	$result = "";
	if (strlen($str) > 40) { // strlen함수는 문자열길이 구하는 함수입니다.
		$result = substr($str, 0, $len); // substr함수는 문자열 자르기 함수입니다.
		$result = $result . "..."; // 자른 문자열 끝에 "..." 붙입니다.
	} else {
		$result = $str; // 문자열길이가  20 이하 일경우 " ..." 붙이지 않고 그대로
	}
	return $result;
}


// <!-- start staff permission 2016-10-06-->

// 권한 체크 함수(2진법)
function permissionCheck($session_permission_f, $permission_f) {
	for( $i = 0 ; $i < strlen($permission_f) ; $i++) {
		if($session_permission_f[$i] == '1' && $permission_f[$i] == '1'){
			return true;
		}
	}
	return false;
}

// 권한 체크 함수(권한 명칭 비교)
function permissionNameCheck($session_permission_f, $permission_name_f) {
	$permission_name = array('마케터','서비스운영','재무관리자','외주관리자','대표','마스터관리자','인큐베이팅','물류관리자');

	for( $i = 0 ; $i < strlen($session_permission_f) ; $i++) {
		if($permission_name[$i] == $permission_name_f){
			if($session_permission_f[$i] == '1'){
				return true;
			}
		}
	}
	return false;
}

// String을 tokenized하여 arrau로 리턴
function stringTokenized($str_f, $token){
	$result = array();
	if(!!$str_f){
		$return_tmp = strtok($str_f, $token);

		while($return_tmp !== false){
			$result[] = $return_tmp;
			$return_tmp = strtok("#");
		}
		return $result;
	}
	return $result;
}


/* -------------------------------------------------- 사용 미확인 -------------------------------------------------- //


// n2br  textarea 에서 입력한 값을 자동 줄바꿈 하여주는 함수
function n2br($vl)
{
	$nans = str_replace("\n","<br>",$vl);
	return $nans;
}

function grt($vl)
{
	$vl = ($vl=="n" || $vl=="") ? "" : $vl;
	return $vl;
}

function gsp($vl)
{
	$vl = ($vl) ? $vl : "n";
	return $vl;
}

function getod_prname($vl) {
	global $baseurl;
	require($baseurl.'dev/libs/dbconfig.php');
	$rs = mysqli_query($my_db, "select a.count,a.dis,a.price,a.oprice,b.pname,c.subject from (su_oditem as a left join su_product as b on a.pid=b.pid) left join su_prodoct_price as c on a.options = c.no  where a.carnum  = '$vl' order by a.no desc");
	$grst = "";
	$k = 0;
	while($dt = mysqli_fetch_array($rs)){

		$xprice = number_format($dt[price])." 원";
		if ($dt[oprice]=="" || $dt[oprice]=="n") {
			$oprice2 = 0;
		} else {
			$oprice2 = number_format($dt[oprice]);
		}
		$oprice2 = $oprice2." 원";


		if ($i == "")
		{
			if ($dt[oprice] =="n" || $dt[oprice] =="") {
				$grst = $dt[pname]."(".$xprice.") ";
			} else {
				$grst = $dt[pname]."(".$xprice.") "." <span style='color:#529336'>".$dt[subject]." [+ $oprice2]</span>";
			}

		}
		else
		{
			if ($dt[oprice] =="n" || $dt[oprice] =="") {
				$grst = $grst."<br>".$dt[pname]."(".$xprice.") ";
			} else {
				$grst = $grst."<br>".$dt[pname]."(".$xprice.") "." <span style='color:#529336'>".$dt[subject]." [+ $oprice2]</span>";
			}

		}

		$grst = $grst." <span style='color:red'>X $dt[count] </span>";

		$i=$i+1;
	}
	return $grst;
}

function getstatus($vl) {
	if ($vl=="1") {
		$rs = "입금대기중";
	} elseif ($vl=="2"){
		$rs = "<span style='color:#529336'>입금완료</span>";
	} elseif ($vl=="3"){
		$rs = "<span style='color:#cf5a16'>배송중</span>";
	} elseif ($vl=="4"){
		$rs = "<span style='color:blue'>거래완료</span>";
	} elseif ($vl=="5"){
		$rs = "<span style='color:red'>취소신청</span>";
	} elseif ($vl=="6"){
		$rs = "<span style='color:#fb9818'>거래취소</span>";
	} elseif ($vl=="7"){
		$rs = "<span style='color:#fb9818'>입금확인중</span>";
	} elseif ($vl=="x"){
		$rs = "<span style='color:#b22f4b'>삭제</span>";
	}
	return $rs;
}

function getbs($vl) {
	if ($vl=="1") {
		$rs = "카드일시불";
	} elseif ($vl=="2"){
		$rs = "카드할부금";
	} elseif ($vl=="3"){
		$rs = "현금결제";
	}
	return $rs;
}




// 한국어로 출력
function alertcl($msg='')
{
	if (!$msg) $msg = '올바른 방법으로 이용해 주십시오.';
	echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
	echo "<script type='text/javascript'>alert('$msg');";
	echo "window.close();";
	echo "</script>";
	exit;
}

// 한국어로 출력
function alertkr($msg='', $url='')
{
	if (!$msg) $msg = '올바른 방법으로 이용해 주십시오.';
	echo "<meta http-equiv='Content-Type' content='text/html; charset=euc-kr'>";
	echo "<script type='text/javascript'>alert('$msg');";
	if (!$url)
	echo "history.go(-1);";
	echo "</script>";
	if ($url)
	// 4.06.00 : 불여우의 경우 아래의 코드를 제대로 인식하지 못함
	goto_url($url);
	exit;
}

//function showpage($page, $num, $pagenum, $totalnum) { // 사용자단 페이징
function paging($page, $web, $pagenum, $url,$skinUrl) {
	$skinUrl = $skinUrl."img";
	$strpage="";
	$maxto = 10; //
	$nextpage = $page + 1;
	if ($nextpage > $pagenum) $nextpage = $pagenum;
	if ($pagenum >=($page + $maxto))
	{
		if ($page<5)
		{
			$for_end=10;
		}else
		{
			if  (($page + 5)>$pagenum )
			{
				$for_end=$pagenum;
			}else
			{
				$for_end=$page+5;
			}
		}
	}
	else
	{
		if  (($page + 5)>$pagenum )
		{
			$for_end=$pagenum;
		}else
		{
			$for_end=$page+5;
		}

	}
	if (($page - 4)>1)
	{
		$for_begin=$page - 4;
	}else
	{
		$for_begin=1;
	}


	if ($page>1)
	{
		if ($url!="")
		{
			$strpage=$strpage."<a href=\"".$web."?".$url."\" ><img src='".$skinUrl."/btn_first.gif' alt='처음'></a> <a href=\"".$web."?page=".($page-1)."&".$url."\" ><img src='".$skinUrl."/btn_prev.gif' alt='이전'></a> ";
		}else
		{
			$strpage=$strpage."<a href=\"".$web."\" ><img src='".$skinUrl."/btn_first.gif' alt='처음'></a> <a href=\"".$web."?page=".($page-1)."\" ><img src='".$skinUrl."/btn_prev.gif' alt='이전'></a> ";
		}
	}  else
	{
		$strpage=$strpage."<a href='#'><img src='".$skinUrl."/btn_first.gif' alt='처음'></a> <a href='#'><img src='".$skinUrl."/btn_prev.gif' alt='이전'></a> ";
	}
	$strpage=$strpage."";
	for ($i = $for_begin; $i <= $for_end; $i++) {

		if ($i != $page){
			if ($url!="")
			{
				$strpage=$strpage. "<a href=\"".$web."?".$url."&page=$i\"> $i </a> ";
			}else

			{
				$strpage=$strpage. "<a href=\"".$web."?page=$i\"> $i </a> ";
			}

		} else {
			$strpage=$strpage. "<strong > $i </strong> ";
		}
	}
	$strpage=$strpage."";
	if ($page>=$pagenum)
	{
		$strpage=$strpage." <a href='#'><img src='".$skinUrl."/btn_next.gif' alt='다음'></a> <a href='#'><img src='".$skinUrl."/btn_last.gif' alt='마지막'></a>";
	}else
	{
		if ($url!="")
		{
			$strpage=$strpage." <a href=\"".$web."?".$url."&page=$nextpage\" ><img src='".$skinUrl."/btn_next.gif' alt='다음'></a> <a href=\"".$web."?page=$pagenum&".$url."\" class=\"last\"><img src='".$skinUrl."/btn_last.gif' alt='마지막'></a>";
		}else
		{
			$strpage=$strpage." <a href=\"".$web."?page=$nextpage\" class=\"next\"><img src='".$skinUrl."/btn_next.gif' alt='다음'></a> <a href=\"".$web."?page=$pagenum\" class=\"last\"><img src='".$skinUrl."/btn_last.gif' alt='마지막'></a>";
		}
	}
	return $strpage;

}


//사용자단 페이징
function showpage2($page, $web, $pagenum, $url) { // 관리자단

	$strpage="";
	$maxto = 10; //
	$nextpage = $page + 1;
	if ($nextpage > $pagenum) $nextpage = $pagenum;
	if ($pagenum >=($page + $maxto))
	{
		if ($page<5)
		{
			$for_end=10;
		}else
		{
			if  (($page + 5)>$pagenum )
			{
				$for_end=$pagenum;
			}else
			{
				$for_end=$page+5;
			}
		}
	}
	else
	{
		if  (($page + 5)>$pagenum )
		{
			$for_end=$pagenum;
		}else
		{
			$for_end=$page+5;
		}

	}
	if (($page - 4)>1)
	{
		$for_begin=$page - 4;
	}else
	{
		$for_begin=1;
	}


	if ($page>1)
	{
		if ($url!="")
		{
			$strpage=$strpage."<button type=\"button\" class=\"list_bt_first\" onclick=\"gourl('".$web."?".$url."')\"><i class=\"fa fa-angle-double-left\"></i></button><button type=\"button\" class=\"list_bt\" onclick=\"gourl('".$web."?page=".($page-1)."&".$url."')\"><i class=\"fa fa-angle-left\"></i></button>";
		}else
		{
			$strpage=$strpage."<button type=\"button\" class=\"list_bt_first\" onclick=\"gourl('".$web."')\"><i class=\"fa fa-angle-double-left\"></i></button><button type=\"button\" class=\"list_bt\" onclick=\"gourl('".$web."?page=".($page-1)."')\"><i class=\"fa fa-angle-left\"></i></button>";
		}
	}  else
	{
		$strpage=$strpage."<button type=\"button\" class=\"list_bt_first\"><i class=\"fa fa-angle-double-left\"></i></button><button type=\"button\" class=\"list_bt\"><i class=\"fa fa-angle-left\"></i></button>";
	}
	$strpage=$strpage."";
	for ($i = $for_begin; $i <= $for_end; $i++) {

		if ($i != $page){
			if ($url!="") {
				$strpage=$strpage. "<button class=\"list_bt\" onclick=\"gourl('".$web."?".$url."&page=".$i."')\">$i</button>";
			} else {
				$strpage=$strpage. "<button class=\"list_bt\" onclick=\"gourl('".$web."?page=".$i."')\">$i</button>";
			}
		} else {
			$strpage=$strpage. "<button  class=\"list_bt\"><strong>$i</strong></button>";
		}
	}

	if ($page>=$pagenum) {
		$strpage=$strpage."<button type=\"button\" class=\"list_bt\"><i class=\"fa fa-angle-right\"></i></button><button type=\"button\" class=\"list_bt_last\"><i class=\"fa fa-angle-double-right\"></i></button>";
	} else {
		if ($url!="") {
			$strpage=$strpage."<button type=\"button\" class=\"list_bt\" onclick=\"gourl('".$web."?".$url."&page=".$nextpage."')\"><i class=\"fa fa-angle-right\"></i></button><button type=\"button\" class=\"list_bt_last\" onclick=\"gourl('".$web."?page=".$pagenum."&".$url."');\"><i class=\"fa fa-angle-double-right\"></i></button>";
		} else {
			$strpage=$strpage."<button type=\"button\" class=\"list_bt\" onclick=\"gourl('".$web."?page=".$nextpage."')\"><i class=\"fa fa-angle-right\"></i></button><button type=\"button\" class=\"list_bt_last\" onclick=\"gourl('".$web."?page=".$pagenum."')\"><i class=\"fa fa-angle-double-right\"></i></button>";
		}
	}
	$strpage=$strpage."";
	return $strpage;
}


//파일 업로드
function uploadfile($type,$name,$ext,$size,$error,$tmp_name,$targetname,$upload_dir)
{
	if ($_FILES["ufile"]["error"] > 0 && $_FILES["ufile2"]["error"] > 0)

	{
		echo "Error: " . $_FILES["ufile"]["error"] . "<br />";
	}

	else
	{
		$MAX_SIZE = 50000000000;
		if($size>$MAX_SIZE)
		alert("업로드 하려는 파일크기가 최대치를 초과하여 업로드시 실패하였습니다.");

		if (sizeof(explode('.php', $name))>1 ||
		sizeof(explode('.phtm', $name))>1 ||
		sizeof(explode('.htm', $name))>1 ||
		sizeof(explode('.cgi', $name))>1 ||
		sizeof(explode('.pl', $name))>1 ||
		sizeof(explode('.exe', $name))>1 ||
		sizeof(explode('.jsp', $name))>1 ||
		sizeof(explode('.asp', $name))>1 ||
		sizeof(explode('.js', $name))>1 ||
		sizeof(explode('.inc', $name))>1
		)
		alert("조건에 부합되는 파일을 업로드 하시길바랍니다.");

		copy($tmp_name, $upload_dir.$targetname);

	}
}

// 메인 공지사항 길이 짜르기
function substr_kr($s, $l)
{
	if( strlen($s) <= ($l+3))
	return $s;

	if( ord($s[$l-1]) > 127 ) {      //ord ->  ascii 체크. 127 초과면 한글
		$nc = 2;
		while( ord($s[$l-$nc]) > 127 ) $nc++;
		$l -= !($nc & 1);
	}
	return substr($s, 0, $l) . "..";
}

function cutlt($vl,$i) {
	return substr($vl, 0, $i)."*****";
}


function strcut_utf8($str, $len, $checkmb=false, $tail='...') {
	preg_match_all('/[\xEA-\xED][\x80-\xFF]{2}|./', $str, $match);

	$m    = $match[0];
	$slen = strlen($str);  // length of source string
	$tlen = strlen($tail); // length of tail string
	$mlen = count($m); // length of matched characters

	if ($slen <= $len) return $str;
	if (!$checkmb && $mlen <= $len) return $str;

	$ret   = array();
	$count = 0;

	for ($i=0; $i < $len; $i++) {
		$count += ($checkmb && strlen($m[$i]) > 1)?2:1;

		if ($count + $tlen > $len) break;
		$ret[] = $m[$i];
	}

	return join('', $ret).$tail;
}

//윤년체크
function leap_chk($year){
	if($year%4 == 0 && $year%100!=0 || $year%400==0)
	return 1; //참
	else
	return 0; //거짓
}

// 요일 계산
function week_chk($_year, $_month, $_day){

	$sum=0;
	for($i=1;$i<$_year;$i++){
		if(leap_chk($i)) //전년도까지 체크
		$sum+=366;
		else
		$sum+=365;
	}
	for($m=1;$m<$_month;$m++){
		if($m==4 || $m==6 || $m==9 || $m==11){
			$sum+=30;
		}
		else if($m==2){
			if(leap_chk($year)) {
				$sum+=29;
			}
			else {
				$sum+=28;
			}
		} else {
			$sum+=31;
		}
	}

	$sum=$sum+$_day;
	$week=$sum%7;

	switch($week){
		case 0 : $WD = "일"; break;
		case 1 : $WD = "월"; break;
		case 2 : $WD = "화"; break;
		case 3 : $WD = "수"; break;
		case 4 : $WD = "목"; break;
		case 5 : $WD = "금"; break;
		case 6 : $WD = "토"; break;
	}
	return $WD;
}

// 카테고리 추가
function getctgrs($tbl,$no)
{
	global $baseurl;
	require($baseurl.'libs/dbconfig.php');
	$rs = mysqli_query($my_db, "select no,deps,useful,stock,subject,date from $tbl order by no desc");
	$gop = "";
	while($dt = mysqli_fetch_array($rs)){
		$nno = $dt['no'];
		$nsubject = $dt['subject'];
		$wck = ($nno==$no) ? "selected='selected'" : "";
		if ($gop == "")
		{
			$gop = "<option value='$nno' $wck >".$nsubject."</option>";
		}
		else
		{
			$gop = $gop."<option value='$nno' $wck >".$nsubject."</option>";
		}

	}
	return $gop;
}

// sub 카테고리 가져오기
function getctgrs2($tbl,$pno,$no)
{
	global $baseurl;
	require($baseurl.'libs/dbconfig.php');
	$rs = mysqli_query($my_db, "select no,deps,useful,stock,subject,date from $tbl where pno='$pno' order by subject");
	$gop = "";
	while($dt = mysqli_fetch_array($rs)){
		$nno = $dt['no'];
		$nsubject = $dt['subject'];
		$wck = ($nno==$no) ? "selected='selected'" : "";
		if ($gop == "")
		{
			$gop = "<option value='$nno' $wck >".$nsubject."</option>";
		}
		else
		{
			$gop = $gop."<option value='$nno' $wck >".$nsubject."</option>";
		}
	}
	return $gop;
}


function getmain($tb_id,$num) {
	global $baseurl;
	require($baseurl.'libs/dbconfig.php');
	$rs = mysqli_query($my_db, "select no,subject,pub_date,date(pub_date) as sortdate from su_board where table_id='$tb_id' and status='o' order by no desc limit 0,$num ");
	while($data = mysqli_fetch_array($rs)){
		$daymns=abs((strtotime(date("Y-m-d"))-strtotime($data['pub_date']))/86400);
	 $getnew = ($daymns<=1) ? "<img src='images/common/ico/ico_new.gif' alt='NEW' style='margin-left:5px'>" : ''; //1일이내 new이미지 보여주기
	 $sbjt = imSubstr($data['subject'],15);
	 $array[]= array("no"=>$data['no'],"subject"=>$sbjt,"pub_date"=>str_replace("-",".",$data['sortdate']),"gnimg"=>$getnew,"tb_id"=>$tb_id);
	}
	$my_db->close();
	return $array;
}

function getprdtmain($num) {
	global $baseurl;
	require($baseurl.'libs/dbconfig.php');
	$rs = mysqli_query($my_db, "select pid,ctgr1,ctgr2,rf1,pname  from su_product where status='o' and hot='o' order by pid desc limit 0,$num ");
	while($dt = mysqli_fetch_array($rs)){
		$url2 = "fupload/poiuytrewqlkjhgfdsa/".$dt[rf1];
		if(file_exists($url2) && $dt[rf1]!="") {
			$brf1 = $url2;
		} else {
			$brf1 = "images/common/no-image.jpg";
		}
		$array[]= array("pid"=>$dt[pid],"ctgr1"=>$dt[ctgr1],"ctgr2"=>$dt[ctgr2],"rf1"=>$brf1,"pname"=>$dt[pname]);
	}
	$my_db->close();
	return $array;
}

// 통계부분

// 월별 매출금액 계산
function yearpoll($year,$mon) {
	global $baseurl;
	require($baseurl.'libs/dbconfig.php');
	$gdate = $year."-".$mon;
	//echo "select sum(amount-zec) as famount from su_order where odtime like '%$gdate%' and status='4' and destatus='o' ";
	$rs = mysqli_query($my_db, "select sum(amount-zec) as famount from su_order where odtime like '%$gdate%' and status='4' and destatus='o' ");
	while($dt = mysqli_fetch_array($rs)){
		$vl = $dt[famount];
	}
	$vl = (trim($vl)=="") ? 0 : $vl;
	$my_db->close();
	return $vl;
}

// 월별 주문신청건수
function mongun($year,$mon,$tp) {
	global $baseurl;
	require($baseurl.'libs/dbconfig.php');
	$gdate = $year."-".$mon;

	if ($tp=="2") {
		$gsql = "and status='4'";
	}
	//echo "select sum(amount-zec) as famount from su_order where odtime like '%$gdate%' and status='4' and destatus='o' ";
	$rs = mysqli_query($my_db, "select count(*) as cnt from su_order where odtime like '%$gdate%' $gsql and destatus='o' ");
	while($dt = mysqli_fetch_array($rs)){
		$vl = $dt[cnt];
	}
	$vl = (trim($vl)=="") ? 0 : $vl;
	$my_db->close();
	return $vl;
}

//  bmp 파일은 자동 php 상 제공 하고 있지 않는사항으로 함수 추가  참고로 fopen 함수 지원하지 않을시 사용하려는 페이지만  상단에 ini_set("allow_url_fopen","1");   를 추가한다
function imagecreatefrombmp($p_sFile)
{
	$file    =    fopen($p_sFile,"rb");
	$read    =    fread($file,10);
	while(!feof($file)&&($read<>""))
	$read    .=    fread($file,1024);
	$temp    =    unpack("H*",$read);
	$hex    =    $temp[1];
	$header    =    substr($hex,0,108);

	//    Structure: http://www.fastgraph.com/help/bmp_header_format.html
	if (substr($header,0,4)=="424d")
	{
		$header_parts    =    str_split($header,2); //    Cut it in parts of 2 bytes
		$width            =    hexdec($header_parts[19].$header_parts[18]); //    Get the width        4 bytes
		$height            =    hexdec($header_parts[23].$header_parts[22]);//    Get the height        4 bytes
		unset($header_parts); //    Unset the header params
	}

	//    Define starting X and Y
	$x                =    0;
	$y                =    1;

	//    Create newimage
	$image            =    imagecreatetruecolor($width,$height);

	//    Grab the body from the image
	$body            =    substr($hex,108);

	//    Calculate if padding at the end-line is needed
	//    Divided by two to keep overview.
	//    1 byte = 2 HEX-chars
	$body_size        =    (strlen($body)/2);
	$header_size    =    ($width*$height);

	//    Use end-line padding? Only when needed
	$usePadding        =    ($body_size>($header_size*3)+4);

	//    Using a for-loop with index-calculation instaid of str_split to avoid large memory consumption
	//    Calculate the next DWORD-position in the body
	for ($i=0;$i<$body_size;$i+=3)
	{
		//    Calculate line-ending and padding
		if ($x>=$width)
		{
			//    If padding needed, ignore image-padding
			//    Shift i to the ending of the current 32-bit-block
			if ($usePadding)
			$i    +=    $width%4;

			//    Reset horizontal position
			$x    =    0;

			//    Raise the height-position (bottom-up)
			$y++;

			//    Reached the image-height? Break the for-loop
			if ($y>$height)
			break;
		}

		//    Calculation of the RGB-pixel (defined as BGR in image-data)
		//    Define $i_pos as absolute position in the body
		$i_pos    =    $i*2;
		$r        =    hexdec($body[$i_pos+4].$body[$i_pos+5]);
		$g        =    hexdec($body[$i_pos+2].$body[$i_pos+3]);
		$b        =    hexdec($body[$i_pos].$body[$i_pos+1]);

		//    Calculate and draw the pixel
		$color    =    imagecolorallocate($image,$r,$g,$b);
		imagesetpixel($image,$x,$height-$y,$color);

		//    Raise the horizontal position
		$x++;
	}

	//    Unset the body / free the memory
	unset($body);

	//    Return image-object
	return $image;
}

// 이미지 resize 함수
function img_resize( $tmpname, $size, $save_dir, $save_name )
{
	$save_dir .= ( substr($save_dir,-1) != "/") ? "/" : "";
	$gis       = GetImageSize($tmpname);
	$type       = $gis[2];

	switch($type)
	{
		case "1": $imorig = imagecreatefromgif($tmpname); break;
		case "2": $imorig = imagecreatefromjpeg($tmpname);break;
		case "3": $imorig = imagecreatefrompng($tmpname); break;
		case "6": $imorig = imagecreatefrombmp($tmpname); break;
		default:  $imorig = imagecreatefromjpeg($tmpname);
	}

	$x = imageSX($imorig);
	$y = imageSY($imorig);
	if($gis[0] <= $size)
	{
		$av = $x;
		$ah = $y;
	}
	else
	{
		$yc = $y*1.3333333;
		$d = $x>$yc?$x:$yc;
		$c = $d>$size ? $size/$d : $size;
		$av = $x*$c;        //высота исходной картинки
		$ah = $y*$c;        //длина исходной картинки
	}
	$im = imagecreate($av, $ah);
	$im = imagecreatetruecolor($av,$ah);
	if (imagecopyresampled($im,$imorig , 0,0,0,0,$av,$ah,$x,$y))
	if (imagejpeg($im, $save_dir.$save_name))
	return true;
	else
	return false;
}

// 포인트 거래내역을 기록으로 넣는다
function addreport($tp,$money,$point,$ip,$extime,$uid,$bfpnt,$afpnt,$pid,$odno,$AuthTy)
{
	require('../libs/dbconfig.php');
	$pid = ($pid) ? $pid : "n";
	$stmt = $my_db->prepare("INSERT INTO su_charge_list (tp,money,bfpoint,lstpoint,point,ip,extime,uid,pid,odno) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	$stmt->bind_param("ssssssssss", $tp, $money,$bfpnt,$afpnt,$point,$ip,$extime,$uid,$pid,$odno);
	$stmt->execute();
	$stmt->close();
	$my_db->close();
}

// 포인트 거래내역을 기록으로 넣는다
function addreport2($tp,$money,$point,$ip,$extime,$uid,$bfpnt,$afpnt,$pid,$odno,$pctbrd,$AuthTy,$irStoreId,$irApprNo,$irDealNo,$irApprTm)
{
	require('../libs/dbconfig.php');
	$pid = ($pid) ? $pid : "n";
	$pctbrd = ($pctbrd) ? $pctbrd : "n";
	$stmt = $my_db->prepare("INSERT INTO su_charge_list (tp,money,bfpoint,lstpoint,point,ip,extime,uid,pid,odno,pctbrd,AuthTy,rStoreId,rApprNo,rDealNo,rApprTm) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	$stmt->bind_param("ssssssssssssssss", $tp, $money,$bfpnt,$afpnt,$point,$ip,$extime,$uid,$pid,$odno,$pctbrd,$AuthTy,$irStoreId,$irApprNo,$irDealNo,$irApprTm);
	$stmt->execute();
	$stmt->close();
	$my_db->close();
}

function getbanner($tp,$num)
{
	require('../libs/dbconfig.php');
	$i = 1;
	$rs = mysqli_query($my_db, "SELECT no,subject,ofile,rfile,link,target,memo from su_banner where type='$tp' order by no limit 0,$num ");
	while($data = mysqli_fetch_array($rs)){
		$target = ($data['target']=="2") ? "target='_blank'" : "";
		$array[]= array("no"=>$data['no'],"noi"=>$i,"alt"=>$data['subject'],"ofile"=>$data['ofile'],"memo"=>$data['memo'],"rfile"=>$data['rfile'],"link"=>$data['link'],"target"=>$target);
		$i=$i+1;
	}
	$my_db->close();
	return $array;
}

// 날자 체크 팝업 시작일과 종료일
function chkDate($sdate='', $edate='', $userDate=''){
	if($sdate == '' && $edate == '') return true;
	$ret = false;
	$userDate = NEVL($userDate, (string)date('Ymd',time()));
	if($sdate != '' && $edate != ''){
		if($userDate >= $sdate && $userDate <= $edate){ $ret = true; }else{ $ret = false; }
	}
	elseif($sdate != ''){ if($userDate >= $sdate){ $ret = true; }else{ $ret = false; }}
	elseif($edate != ''){ if($userDate <= $edate){ $ret = true; }else{ $ret = false; }}
	return $ret;
}


function NEVL($val, $default=''){
	if(is_null($val) || $val == ''){
		return $default;
	}else{
		return $val;
	}
}

function alterText($val1, $val2, $comparison=''){
	if(NEVL($val1) == $comparison){
		if($comparison != '') return $val1;
		else return $val2;
	}else{
		if($comparison != '') return $val2;
		else return $val1;
	}
}

//SELECT BOX 출력하는것
function exselect($filed1,$filed2,$tbname,$where,$getvl,$nowvl)
{
	require('../libs/dbconfig.php');
	$rs = mysqli_query($my_db, "select $filed1,$filed2 from $tbname WHERE $where = '$getvl' and destatus = 'o' order by no");

	$i=0;
	while($dt = mysqli_fetch_array($rs)){
		$wck = ($nowvl==$dt[0]) ? "selected='selected'" : "";
		if ($i==0)
		{
			$gop = "<option value='$dt[0]' $wck >".$dt[1]."</option>";
		}
		else
		{
			$gop = $gop."<option value='$dt[0]' $wck >".$dt[1]."</option>";
		}
		$i=+1;
	}
	return $gop;
}

function oexselect($filed1,$filed2,$tbname,$where,$getvl,$nowvl) // 옵션추가항목 가져오기
{
	require('../libs/dbconfig.php');
	$rs = mysqli_query($my_db, "select $filed1,$filed2,price from $tbname WHERE $where = '$getvl' and destatus = 'o' order by no");

	$i=0;
	while($dt = mysqli_fetch_array($rs)){
		$wck = ($nowvl==$dt[0]) ? "selected='selected'" : "";
		if ($i==0)
		{
			$gop = "<option value='$dt[0]' $wck > ".$dt[1]." [+".number_format($dt[price])."원] </option>";
		}
		else
		{
			$gop = $gop."<option value='$dt[0]' $wck > ".$dt[1]." [+".number_format($dt[price])."원] </option>";
		}
		$i=+1;
	}
	return $gop;
}

// 메일 전달 함수

function encode_2047($subject) {
	return '=?euc-kr?b?'.base64_encode($subject).'?=';
}
function sdmail($email_str, $subject, $message)
{
	$getemailsetting = get2_vl("sitename","manageremail","su_email","no","1");
	$sitename = $getemailsetting[0];
	$manageremail = $getemailsetting[1];
	mb_internal_encoding('UTF-8');
	$from_name = $sitename;
	$from_name = encode_2047(iconv("UTF-8","EUC-KR",$from_name));
	$headers = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";
	$headers .= 'Content-Transfer-Encoding: 8bit' . "\r\n";
	$headers .= 'Content-Disposition: inline' . "\r\n";
	$headers .= 'X-Mailer: PHP' . "\r\n";
	$headers .= 'From: '.$from_name.' < '.$manageremail.' >'. "\r\n";

	// 메일 보내기
	$result = mb_send_mail($email_str, $subject, $message, $headers) ;

	return $result;
}

function imSubstr($sourcestr='',$cutlength=100,$i=0,$endstr='…'){
	$str_length=strlen($sourcestr);//字符串的字节数
	while (($n<$cutlength) and ($i<=$str_length))
	{
		$temp_str=substr($sourcestr,$i,1);
		$ascnum=Ord($temp_str);//ascii码
		if ($ascnum>=224)
		{
			$returnstr=$returnstr.substr($sourcestr,$i,3);
			$i=$i+3;
			$n++;
		}elseif ($ascnum>=192)
		{
			$returnstr=$returnstr.substr($sourcestr,$i,2);
			$i=$i+2;
			$n++;
		}else
		{
			$returnstr=$returnstr.substr($sourcestr,$i,1);
			$i=$i+1;
			$n=$n+0.5;
		}
	}
	if($i<$str_length)$returnstr.=$endstr;
	return $returnstr;
}

// 관리자 등급설정 select box 가져오기
function get_level_select($nvl) {
	global $level_arr;
	$select = "";
	for($i = 0; $i < count($level_arr);$i++) {
		$ckecked = $level_arr[$i]==$nvl ? "selected='selected'" : "";
		$select = $select."<option value='".$level_arr[$i]."' ".$ckecked.">".$level_arr[$i]."</option>";
	}
	return $select;
}

// 배열값들로부터 select box 만들기
function getArrList($vls,$nvl) {
	$key = array_flip($vls);
	foreach ($key as $vl) {
		$ckecked = $vl==$nvl ? "selected='selected'" : "";
		$select = $select."<option value='".$vl."' ".$ckecked.">".$vls[$vl]."</option>";
	}
	return $select;
}

// 배열값들로부터 select box 만들기
function getSelectop($vls,$search,$sltname) {
	$key = array_flip($vls);
	$rtvl = "";
	foreach ($key as $vl) {
		$rtvl = $rtvl.'<li><a href="#" onclick="gselect(\'search\',\''.$vl.'\',\'sltname\',\''.$vls[$vl].'\');return false;">'.$vls[$vl].'</a></li>';
	}
	return $rtvl;
}

// 배열값 ,만들기
function imld($tp,$vl) {
	$rt = (sizeof($vl)==0) ? "" : implode($tp, $vl);
	return $rt;
}

// 리스트 선택항목 가져오기
//function get_arr_selectbox($arr_data,$arrs,$ckbx_name,$tp) {
//	$checkbox_listnum = "";
//	$listNum_arr = explode(",",$arr_data);
//	$key = array_flip($arrs);
//	foreach ($key as $listNums) {
//		$values = ($tp=="1") ? $arrs[$listNums] : $listNums;
//		$checked = (in_array($values, $listNum_arr)) ? "checked='checked'" : "";
//
//		$checkbox_listnum = $checkbox_listnum."<div class='row-fluid'><div class='checkbox check-default'><input type='checkbox' name='".$ckbx_name."[]' id='".$ckbx_name.$listNums."' value='".$values."' ".$checked."><label for='".$ckbx_name.$listNums."'> ".$arrs[$listNums]."</label></div></div> ";
//		unset($checked);
//		unset($values);
//	}
//	return $checkbox_listnum;
//}

// 리스트 선택항목 가져오기
function get_arr_selectbox($arr_data,$arrs,$ckbx_name,$tp) {
	$checkbox_listnum = "";
	$listNum_arr = explode(",",$arr_data);
	$key = array_flip($arrs);
	foreach ($key as $listNums) {
		$values = ($tp=="1") ? $arrs[$listNums] : $listNums;
		$checked = (in_array($values, $listNum_arr)) ? "checked='checked'" : "";
		$checkbox_listnum = $checkbox_listnum."<input type='checkbox' name='".$ckbx_name."[]' id='".$ckbx_name.$listNums."' value='".$values."' ".$checked."><label for='".$ckbx_name.$listNums."'> ".$arrs[$listNums]."</label> ";
		unset($checked);
		unset($values);
	}
	return $checkbox_listnum;
}

// 같은테이블 존재여부체크
function check_same_tbl($tbid) {
	global $baseurl;
	require($baseurl.'libs/dbconfig.php');
	$gvl = get_vl2("bd_tbId","su_board_list","bd_tbId",trim($tbid),"bd_status","o");
	if (trim($gvl)=="") {
		$vl = "o";
	} else {
		$vl = "m";
	}
	return $vl;
}

// 특정폴더내에 있는 파일리스트 불러오는 함수
function myallfile($dir, $ext = '')
{
	$file_arr = array();
	if (is_dir($dir))
	{
		if ($dh = opendir($dir))
		{
			while (($file = readdir($dh)) !== false)
			{
				$type = filetype($dir . $file);

				if($type == 'file')
				{
					if($ext != '')
					{
						$ext = strtolower($ext);
						$temp = explode('.',$file);
						if(strtolower($temp[count($temp)-1]) == $ext) $file_arr[] = $dir.$file;
					}
					else    $file_arr[] = $dir.$file;
				}
				else if($type == 'dir' && ($file != '.' && $file != '..'))
				{

					$temp = myallfile($dir.$file.'/', $ext);
					if(is_array($temp))
					{
						$file_arr = array_merge($file_arr, $temp);
					}
				}
			}
			closedir($dh);
		}
		return $file_arr;
	}
	return 0;
}

function get_folders($dir) {
	$filess = scandir($dir, 1);
	$fdarr = "";
	for ($i=0;$i<sizeof($filess);$i++) {
		$ckfile = explode(".", $filess[$i]);
		if ($fdarr=="") {
			$fdarr = (count($ckfile)=="1") ? $filess[$i] : $fdarr;
		} else {
			$fdarr = (count($ckfile)=="1") ? $fdarr.",".$filess[$i] : $fdarr;
		}
		unset($ckfile);
	}
	return $fdarr;
}

function get_edt($id,$tp) {
	global $baseurl;
	$ctts = ($tp=="n") ? "  { name: 'document', groups: [ 'document' ] },
							     { name: 'insert' },
								 { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
								 { name: 'paragraph', groups: [ 'list', 'align' ] },
								 { name: 'links' },
								 { name: 'styles' },
								 { name: 'colors' },
								 { name: 'tools' },
								 { name: 'others' }

								 " : "

								 { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
							     { name: 'insert' },
								 { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
								 { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
								 { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
								 { name: 'links' },
								 { name: 'styles' },
								 { name: 'colors' },
								 { name: 'tools' },
								 { name: 'others' }";

	$gscript = " 		<script>
			                CKEDITOR.replace('$id',{
							filebrowserUploadUrl: '".$baseurl."main/edtUpload.php',
			                toolbarGroups: [".$ctts."]
			               	});
			            </script>
			            ";
	return $gscript;
}

// 파일번호를 가져오기
function get_fileNum($vl) {
	$arr = explode("_",$vl);
	$arr2 = explode(".", $arr[2]);
	return $arr2[0];
}

// 시간값을 가져오기
function get_hour($time) {
	$tarr= explode(" ", $time);
	$tarr2 = explode(":", $tarr[1]);
	return $tarr2[0];
}

// 일 값을 가져오기
function get_day($time) {
	$tarr= explode(" ", $time);
	$tarr2= explode("-", $tarr[0]);
	return $tarr2[2];
}

// 월값 가져오기
function get_month($time) {
	$tarr = explode("-", $time);
	return $tarr[1];
}

function get_year($time) {
	$tarr = explode("-", $time);
	return $tarr[0];
}

// 2글자숫자 만들기
function get_2num($vl) {
	$rtvl = (strlen($vl)==1) ? "0".$vl : $vl;
	return $rtvl;
}

// 검색엔진 값 추출하는 함수
function get_engine($vl) {
	global $engine_arr;
	$key = array_flip($engine_arr);
	foreach ($key as $vls) {
		if (sizeof(explode($vls, $vl))>1) {
			$rtvl = $rtvl.$engine_arr[$vls];
		}
	}
	$rtvl = (trim($rtvl)=="") ? "기타" : $rtvl;
	$rtvl = (trim($rtvl)=="DaumNate") ? "Nate" : $rtvl;
	return $rtvl;
}

function get_searchEngine($url) {
	$url = str_replace("http://", "", $url);
	$url = str_replace("https://", "", $url);
	$url_arr = explode("/", $url);
	switch ($url_arr[0]) {
		case "search.daum.net":
			$cknate = explode("search.daum.net/nate", $url);
			$rtvl = sizeof($cknate) > 0 ? "nate" : "daum";
			break;
		case "search.naver.com":
			$rtvl = "naver";
			break;
		default :
			$ckgoogle = explode("www.google", $url);
			$ckyahoo = explode("search.yahoo", $url);
			if (sizeof($ckgoogle)>1) {
				$rtvl = "google";
			} elseif (sizeof($ckyahoo)>1) {
				$rtvl = "yahoo";
			} else {
				$rtvl = $url_arr[0];
			}
	}
	return $rtvl;
}

// 현재까지는 네이버,네이트,다음의 검색키워드만 검색가능
function get_searchKey($url) {
	$engine = get_searchEngine($url);
	if ($engine=="naver") {
		$garrs = explode("query=", $url);
		$garrs2 = explode("&", $garrs[1]);
		$rtvl = $garrs2[0];
	} elseif ($engine=="nate" || $engine=="daum") {
		$garrs = explode("&q=", $url);
		$rtvl = $garrs[1];
	}
	return $rtvl;
}

// 브라우저 정보 가져오는 함수
function getBrowser()
{
	$u_agent = $_SERVER['HTTP_USER_AGENT'];
	$bname = 'Unknown';
	$platform = 'Unknown';
	$version= "";

	//First get the platform?
	if (preg_match('/linux/i', $u_agent)) { $platform = 'linux'; }
	elseif (preg_match('/macintosh|mac os x/i', $u_agent)) { $platform = 'mac'; }
	elseif (preg_match('/windows|win32/i', $u_agent)) { $platform = 'windows'; }

	// Next get the name of the useragent yes seperately and for good reason
	if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
		$bname = 'Internet Explorer'; $ub = "MSIE";
		$vs2 = explode(";",$u_agent);
		$vs2= str_replace("MSIE","",$vs2[1]);
	}
	elseif(preg_match('/Firefox/i',$u_agent)) { $bname = 'Mozilla Firefox'; $ub = "Firefox"; }
	elseif(preg_match('/Chrome/i',$u_agent)) { $bname = 'Google Chrome'; $ub = "Chrome"; }
	elseif(preg_match('/Safari/i',$u_agent)) { $bname = 'Apple Safari'; $ub = "Safari"; }
	elseif(preg_match('/Opera/i',$u_agent)) { $bname = 'Opera'; $ub = "Opera"; }
	elseif(preg_match('/Netscape/i',$u_agent)) { $bname = 'Netscape'; $ub = "Netscape"; }

	// finally get the correct version number
	$known = array('Version', $ub, 'other');
	$pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	if (!preg_match_all($pattern, $u_agent, $matches)) {
		// we have no matching number just continue
	}


	// see how many we have
	$i = count($matches['browser']);
	if ($i != 1) {
		//we will have two since we are not using 'other' argument yet
		//see if version is before or after the name
		if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){ $version= $matches['version'][0]; }
		else { $version= $matches['version'][1]; }
	}
	else { $version= $matches['version'][0]; }
	$version = $vs2;
	// check if we have a number
	if ($version==null || $version=="") {$version="";}
	if ($bname=="Unknown") {
		$ckie11 = explode("Trident/",$u_agent);
		$bname = (sizeof($ckie11)>0) ? "Internet Explorer 11" : $bname;
	}
	return array('userAgent'=>$u_agent, 'name'=>$bname, 'version'=>$version, 'platform'=>$platform, 'pattern'=>$pattern);
}

// 퍼센트값 가져오기
function getpersent($tt,$vl) {
	if ($vl==0) {
		return 0;
	} else {
		$rvl = ($vl/$tt)*100;
		return round($rvl,2);
	}
}
*/
?>
