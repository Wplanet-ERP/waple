<?php

class Model
{
    private $_host  = 'db-ff3ed.pub-cdb.ntruss.com';
    private $_user  = 'master_wplanet';
    private $_pwd   = 'wplanet123!';
    private $_db    = 'wplanet_v1';

    public $my_db   = "";

    protected $_main_table  = "";
    protected $_main_key    = "";
    protected $_wplanet_key = "w_planet!@#";

    public function __construct()
    {
        try{
            $this->my_db = new mysqli($this->_host, $this->_user, $this->_pwd, $this->_db);
            mysqli_query($this->my_db,'set names utf8');
        } catch (mysqli_sql_exception $e) {
            var_dump("DB접속중 에러가 발생 하였습니다. : ". $e->getMessage());
        }
    }

    public function setCharset($charset)
    {
        $this->my_db->set_charset($charset);
    }

    public function setMainInit($table, $key)
    {
        $this->_main_table = $table;
        $this->_main_key   = $key;
    }

    public function getInsertId()
    {
        return mysqli_insert_id($this->my_db);
    }

    public function getItem($main_val)
    {
        $item_sql       = "SELECT * FROM {$this->_main_table} WHERE `{$this->_main_key}`='{$main_val}'";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function getWhereItem($add_where)
    {
        $item_sql       = "SELECT * FROM {$this->_main_table} WHERE {$add_where}";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function insert($insert_data)
    {
        $result = false;

        if(!empty($insert_data))
        {
            $ins_sql = "INSERT INTO {$this->_main_table} SET ";
            $comma   = "";
            foreach($insert_data as $key => $data){
                if((string)$data == 'NULL'){
                    $ins_sql .= "{$comma} `{$key}`=NULL";
                }else{
                    $ins_sql .= "{$comma} `{$key}`='{$data}'";
                }

                $comma    = " , ";
            }

            if(mysqli_query($this->my_db, $ins_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function multiInsert($insert_data_list)
    {
        $result = false;
        if(!empty($insert_data_list))
        {
            $multi_ins_sql = "";
            foreach($insert_data_list as $insert_data)
            {
                $ins_sql = "INSERT INTO {$this->_main_table} SET ";
                if (!empty($insert_data)) {

                    $comma = "";
                    foreach ($insert_data as $key => $data) {
                        if ((string)$data == 'NULL') {
                            $ins_sql .= "{$comma} `{$key}`=NULL";
                        } else {
                            $ins_sql .= "{$comma} `{$key}`='{$data}'";
                        }

                        $comma = " , ";
                    }
                }
                $ins_sql .= ";";

                $multi_ins_sql .= $ins_sql;
            }
            
            if (mysqli_multi_query($this->my_db, $multi_ins_sql)) {
                $result = true;
            }
        }

        return $result;
    }

    public function update($update_data)
    {
        $result = false;
        if(!empty($update_data))
        {
            $upd_sql = "UPDATE {$this->_main_table} SET ";
            $comma   = "";
            foreach($update_data as $key => $data)
            {
                if($key == "bk_jumin")
                {
                    $upd_sql .= "{$comma} `{$key}`=HEX(AES_ENCRYPT('{$data}', '{$this->_wplanet_key}'))";
                    $comma    = " , ";
                }
                elseif($key != $this->_main_key)
                {
                    if((string)$data == 'NULL'){
                        $upd_sql .= "{$comma} `{$key}`=NULL";
                    }else{
                        $upd_sql .= "{$comma} `{$key}`='{$data}'";
                    }
                    $comma    = " , ";
                }
            }
            $upd_sql .= " WHERE `{$this->_main_key}`='{$update_data[$this->_main_key]}'";

            if(mysqli_query($this->my_db, $upd_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function updateToArray($update_data, $key_data)
    {
        $result = false;
        if(!empty($update_data))
        {
            $upd_sql = "UPDATE {$this->_main_table} SET ";
            $comma   = "";
            foreach($update_data as $key => $data){
                if(!in_array($key, $key_data)){
                    if((string)$data == 'NULL'){
                        $upd_sql .= "{$comma} `{$key}`=NULL";
                    }else{
                        $upd_sql .= "{$comma} `{$key}`='{$data}'";
                    }
                    $comma    = " , ";
                }
            }

            $upd_where = "";
            foreach($key_data as $upd_key => $upd_val){
                $upd_where .= !empty($upd_where) ? " AND `{$upd_key}`='{$upd_val}'" : "`{$upd_key}`='{$upd_val}'";
            }
            $upd_sql .= " WHERE {$upd_where}";

            if(mysqli_query($this->my_db, $upd_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function multiUpdate($update_data_list)
    {
        $result = false;
        if(!empty($update_data_list))
        {
            $multi_upd_sql = "";
            foreach($update_data_list as $update_data)
            {
                if(!empty($update_data))
                {
                    $upd_sql = "UPDATE {$this->_main_table} SET ";
                    $comma = "";
                    foreach ($update_data as $key => $data)
                    {
                        if ($key != $this->_main_key) {
                            if ((string)$data == 'NULL') {
                                $upd_sql .= "{$comma} `{$key}`=NULL";
                            } else {
                                $upd_sql .= "{$comma} `{$key}`='{$data}'";
                            }
                            $comma = " , ";
                        }
                    }
                    $upd_sql .= " WHERE `{$this->_main_key}`='{$update_data[$this->_main_key]}';";

                    $multi_upd_sql .= $upd_sql;
                }
            }

            if (mysqli_multi_query($this->my_db, $multi_upd_sql)) {
                $result = true;
            }
        }

        return $result;
    }

    public function delete($del_val)
    {
        $result = false;

        if(!empty($del_val))
        {
            $del_sql = "DELETE FROM {$this->_main_table} WHERE `{$this->_main_key}`='{$del_val}'";

            if(mysqli_query($this->my_db, $del_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function deleteToArray($del_data)
    {
        $result = false;

        if(!empty($del_data))
        {
            $del_where = "";

            foreach($del_data as $del_key => $del_val){
                $del_where .= !empty($del_where) ? " AND `{$del_key}` = '{$del_val}'" : "`{$del_key}` = '{$del_val}'";
            }

            if(!empty($del_where))
            {
                $del_sql = "DELETE FROM {$this->_main_table} WHERE {$del_where}";

                if(mysqli_query($this->my_db, $del_sql)){
                    $result = true;
                }
            }
        }

        return $result;
    }
}
