<?php

	$aeskey = "w_planet!@#";

	/* HTML color values and names */
	$colors = array(
		"#000000",
		"#006400",
		"#00FA9A",
		"#000080",
		"#00BFFF",
		"#4169E1",
		"#483D8B",
		"#556B2F",
		"#800000",
		"#D8BFD8",
		"#BDB76B",
		"#FFD700",
		"#191970",
		"#008000",
		"#00FF00",
		"#00008B",
		"#00CED1",
		"#4682B4",
		"#4B0082",
		"#696969",
		"#8B0000",
		"#708090",
		"#DDA0DD",
		"#CD853F",
		"#228B22",
		"#00FF7F",
		"#0000CD",
		"#00FFFF",
		"#5F9EA0",
		"#663399",
		"#808000",
		"#8B4513",
		"#778899",
		"#FF00FF",
		"#D2691E",
		"#2E8B57",
		"#7CFC00",
		"#0000FF",
		"#00FFFF",
		"#6495ED",
		"#6A5ACD",
		"#808080",
		"#A0522D",
		"#8FBC8F",
		"#FF00FF",
		"#D2B48C",
		"#2F4F4F",
		"#7FFF00",
		"#008080",
		"#1E90FF",
		"#66CDAA",
		"#7B68EE",
		"#A9A9A9",
		"#A52A2A",
		"#FF1493",
		"#DAA520",
		"#32CD32",
		"#90EE90",
		"#008B8B",
		"#20B2AA",
		"#800080",
		"#BC8F8F",
		"#B22222",
		"#CD5C5C",
		"#DEB887",
		"#3CB371",
		"#98FB98",
		"#40E0D0",
		"#8A2BE2",
		"#C0C0C0",
		"#B8860B",
		"#DA70D6",
		"#E9967A",
		"#6B8E23",
		"#9ACD32",
		"#48D1CC",
		"#8B008B",
		"#D3D3D3",
		"#C71585",
		"#EE82EE",
		"#F4A460",
		"#ADFF2F",
		"#7FFFD4",
		"#9370DB",
		"#DCDCDC",
		"#DB7093",
		"#F08080",
		"#F5DEB3",
		"#87CEEB",
		"#9400D3",
		"#DC143C",
		"#FA8072",
		"#FFB6C1",
		"#87CEFA",
		"#9932CC",
		"#FF0000",
		"#FF4500",
		"#FFC0CB",
		"#ADD8E6",
		"#BA55D3",
		"#FF6347",
		"#FFDAB9",
		"#AFEEEE",
		"#FF69B4",
		"#FFDEAD",
		"#B0C4DE",
		"#FF7F50",
		"#FFE4B5",
		"#B0E0E6",
		"#FF8C00",
		"#FFE4C4",
		"#FFA07A",
		"#FFA500"
		/*
		"#B0171F",
		"#3D9140",
		"#33A1C9",
		"#800080",
		"#698B22",
		"#00C78C",
		"#551A8B",
		"#EE9A00",
		"#698B22",
		"#0000CD",
		"#A0522D",
		"#008080",
		"#DB7093",
		"#00C957",
		"#FF00FF",
		"#CD2990",
		"#8B8B83",
		"#9B30FF",
		"#8B7B8B",
		"#CDAD00",
		"#7A67EE",
		"#4B0082",
		"#FF9912",
		"#5F9EA0",
		"#000080",
		"#8B0000",
		"#8E388E",
		"#388E8E",
		"#71C671",
		"#8E8E38",
		"#C67171",
		"#555555"*/
	);
	$smarty->assign("color_list",$colors);

	$colors_rgb[] = "";
	for($i = 0 ; $i < count($colors) ; $i++) {
		$colors_rgb[$i] = sscanf($colors[$i], "#%02x%02x%02x")[0] . ", " . sscanf($colors[$i], "#%02x%02x%02x")[1] . ", " . sscanf($colors[$i], "#%02x%02x%02x")[2];
	}

	$smarty->assign("color_rgb_list",$colors_rgb);

	// [table : promotion] kind 배열
	$promotion_kind=array(
				array('::전체::',''),
				array('체험단','1'),
				array('기자단','2'),
				array('배송체험단','3'),
				array('체험단(+보상)','4'),
				array('배송체험단(+보상)','5')
			);
	$smarty->assign("promotion_kind",$promotion_kind);

	// [table : promotion] channel 배열
	$promotion_channel=array(
		array('::전체::',''),
		array('블로그','1'),
		array('인플루언서','7'),
		array('인스타그램','2'),
//		array('페이스북','3'),
		array('카페','4'),
		array('촬영','5'),
		array('유튜브','8'),
		array('컨텐츠','6')
	);
	$smarty->assign("promotion_channel",$promotion_channel);

	// [table : promotion] p_state 배열
	$promotion_p_state=array(
				array('::전체::',''),
				array('접수대기','1'),
				array('접수완료','2'),
				array('모집중','3'),
				array('진행중','4'),
				array('마감종료','5')
			);
	$smarty->assign("promotion_p_state",$promotion_p_state);

	// [table : promotion] p_state1 배열
	$promotion_p_state1=array(
				array('진행중',''),
				array('진행중단','1')
			);
	$smarty->assign("promotion_p_state1",$promotion_p_state1);

	// [table : application] bk_title 배열
	$application_bk_title=array(
			array('국민', '국민은행'),
			array('농협', '농협'),
			array('우리', '우리은행'),
			array('SC', 'SC은행'),
			array('기업', '기업은행'),
			array('외환', '외환은행'),
			array('수협', '수협중앙회'),
			array('신한', '신한(조흥)은행'),
			array('씨티', '한국씨티은행'),
			array('대구', '대구은행'),
			array('부산', '부산은행'),
			array('산업', '한국산업은행'),
			array('광주', '광주은행'),
			array('제주', '제주은행'),
			array('전북', '전북은행'),
			array('경남', '경남은행'),
			array('하나', '하나은행'),
			array('우체국', '우체국'),
			array('새마을금고', '새마을금고'),
			array('산림조합', '산림조합'),
			array('신협', '신협'),
			array('카카오뱅크', '카카오뱅크'),
			array('K뱅크', 'K뱅크'),
			array('저축은행', '저축은행'),
			array('토스뱅크', '토스뱅크'),
		);
	$smarty->assign("application_bk_title",$application_bk_title);

	// [table : application] a_state 배열
	$application_a_state=array(
				array('-',''),
				array('선정','1'),
				array('미선정','2'),
				array('선정취소','3')
			);
	$smarty->assign("application_a_state",$application_a_state);

	// [table : personal_expenses] state 배열
	$personal_expenses_state=array(
				array('::전체::',''),
				array('작성중','1'),
				array('작성완료','2'),
				array('승인중','3'),
				array('승인완료','4'),
				array('지급완료','5')
			);
	$smarty->assign("personal_expenses_state",$personal_expenses_state);

	// [table : public] Display 배열
	$display=array(
				'0'=>array('::전체::','0'),
				'1'=>array('ON','1'),
				'2'=>array('OFF','2')
			);
	$smarty->assign("display",$display);

	// [table : staff] 활동상태 배열
	$staff_state=array(
				'0'=>array('::전체::','0'),
				'1'=>array('active','1'),
				'2'=>array('freelancer','2'),
				'3'=>array('deactivate','3')
			);
	$smarty->assign("staff_state",$staff_state);

	// [table : company] 법인/개인 배열
	$license_type = array(
				'0'=>array('::전체::','0'),
				'1'=>array('법인회사','1'),
				'2'=>array('개인회사','2'),
				'3'=>array('개인','3')
			);
	$smarty->assign("license_type",$license_type);

	// [table : work_package] package_state 배열
	$package_state=array(
				'0'=>array('::전체::',''),
				'1'=>array('대기','1'),
				'2'=>array('진행중','2'),
				'3'=>array('진행완료','3'),
				'4'=>array('보류','4'),
				'5'=>array('취소완료','5')
			);
	$smarty->assign("package_state",$package_state);

	// [table : work_package] package_kind 배열
	$package_kind=array(
				'0'=>array('::전체::',''),
				'1'=>array('마케팅','1'),
				'2'=>array('교육','2')
			);
	$smarty->assign("package_kind",$package_kind);

	// [table : work] work_state 배열
	$work_state=array(
				'0'=>array('::전체::',''),
				'1'=>array('작성중','1'),
				'2'=>array('대기','2'),
				'3'=>array('진행요청','3'),
				'4'=>array('접수완료','4'),
				'5'=>array('진행중','5'),
				'6'=>array('진행완료','6'),
				'7'=>array('보류','7'),
				'8'=>array('취소완료','8'),
				'9'=>array('진행불가','9')
			);
	$smarty->assign("work_state",$work_state);

	$work_state_name_list = array(
		'0'	=>	"[전체]",
		'1'	=>	"[작성]",
		'2'	=>	"[대기]",
		'3'	=>	"[요청]",
		'4'	=>	"[접수]",
		'5'	=>	"[진행]",
		'6'	=>	"[완료]",
		'7'	=>	"[보류]",
		'8'	=>	"[취소]",
		'9'	=>	"[불가]",
	);

	// [table : work] work_extension 배열
	$work_extension=array(
				'0'=>array('::전체::',''),
				'1'=>array('연장','1'),
				'2'=>array('협의중','2'),
				'3'=>array('미연장','3')
			);
	$smarty->assign("work_extension",$work_extension);

	// [table : work] wd_dp_state 배열
	$wd_dp_state=array(
				array('::전체::',''),
				array('미설정','1'),
				array('입금','2'),
				array('출금','3'),
				array('출금+입금','4')
			);
	$smarty->assign("wd_dp_state",$wd_dp_state);

	// [table : deposit] dp_state 배열
	$dp_state=array(
				'0'=>array('::전체::',''),
				'1'=>array('입금대기','1'),
				'2'=>array('입금완료','2'),
				'3'=>array('부분입금','3'),
				'4'=>array('환불완료','4'),
				'5'=>array('취소완료','5')
			);
	$smarty->assign("dp_state",$dp_state);

	$dp_method=array(
				'0'=>array('::전체::',''),
				'1'=>array('카드결제','1'),
				'2'=>array('현금입금','2')
			);
	$smarty->assign("dp_method",$dp_method);

	// [table : deposit] tax_state 배열
	$dp_tax_state=array(
				'0'=>array('::전체::',''),
				'1'=>array('발행대기','1'),
				'2'=>array('발행완료','2'),
				'3'=>array('카드결제','3'),
        '4'=>array('해당사항없음','4')
			);
	$smarty->assign("dp_tax_state",$dp_tax_state);

	// [table : withdraw] wd_state 배열
	$wd_state=array(
				array('::전체::',''),
				array('미출금','1'),
				array('출금요청','2'),
				array('출금완료','3'),
        		array('출금마감','9'),
				array('취소완료','4'),
				array('선결제(지출)','5'),
				array('선결제(차감)','6'), // 선결제 지출에서 차감함.
				array('해당사항없음','7'),
				array('월정산지급','8') //월정산지급 추가
			);
	$smarty->assign("wd_state",$wd_state);

	// [table : withdraw] wd_method 배열
	$wd_method=array(
				array('::전체::',''),
				array('계좌이체','1'),
				array('법인카드','2'),
				array('선결제차감','3'),
				array('월정산지급','4'),
				array('기타','5'),
			);
	$smarty->assign("wd_method",$wd_method);

	// [table : withdraw] wd_tax_state 배열
	$wd_tax_state=array(
				'0'=>array('::전체::',''),
				'1'=>array('수령대기','1'),
				'2'=>array('수령완료','2'),
				'3'=>array('취소완료','3'),
				'4'=>array('해당사항없음','4')
			);
	$smarty->assign("wd_tax_state",$wd_tax_state);

	// [table : kind] k_name_code 계정과목 1차
	$kind_sql="SELECT
								k.k_name, k.k_name_code
							FROM kind k
							WHERE k.display = '1' AND k.k_code = 'account_code' AND k.k_parent IS NULL
							ORDER BY k.priority ASC
						";
	$kind_query = mysqli_query($my_db,$kind_sql);

	if(!!$kind_query){
		while($kind_data=mysqli_fetch_array($kind_query)) {
			$kind_account_1_list[]=array(
					'k_name_code'=>$kind_data['k_name_code'],
					'k_name'=>$kind_data['k_name']
			);
		}
		$smarty->assign("kind_account_1_list",$kind_account_1_list);
	}

	// [table : kind] k_name_code 계정과목 2차
	$kind_sql="SELECT
								k.k_name, k.k_name_code
							FROM kind k
							WHERE k.display = '1' AND k.k_code = 'account_code' AND k.k_parent IS NOT NULL
							ORDER BY k.priority ASC
						";
	$kind_query = mysqli_query($my_db,$kind_sql);

	if(!!$kind_query){
		while($kind_data=mysqli_fetch_array($kind_query)) {
			$kind_account_2_list[]=array(
					'k_name_code'=>$kind_data['k_name_code'],
					'k_name'=>$kind_data['k_name']
			);
		}
		$smarty->assign("kind_account_2_list",$kind_account_2_list);
	}

	// [table : incentive] incentive_state 배열
	$incentive_state=array(
				'0'=>array('::전체::',''),
				'1'=>array('정산안됨','1'),
				'2'=>array('정산요청(담당자)','2'),
				'3'=>array('반려(재무팀)','3'),
				'4'=>array('승인(재무팀)','4'),
				'5'=>array('반려(대표)','5'),
				'6'=>array('승인(대표)','6'),
				'7'=>array('최종확인완료(담당자)','7'),
				'8'=>array('지급완료(재무팀)','8')
			);
	$smarty->assign("incentive_state",$incentive_state);

	// [table : out_manager] incentive_apply 배열
	$incentive_apply=array(
				'0'=>array('::전체::',''),
				'1'=>array('미적용','1'),
				'2'=>array('적용','2')
			);
	$smarty->assign("incentive_apply",$incentive_apply);

	// [table : application] visit_num 배열
	$visit_num=array(
				array('::전체::',''),
				array('300명 이하','1'),
				array('400~500명','2'),
				array('500~600명','3'),
				array('600~700명','4'),
				array('700~800명','5'),
				array('800~900명','6'),
				array('900~1000명','7'),
				array('1000~2000명','8'),
				array('2000~3000명','9'),
				array('3000이상','10')
			);
	$insta_visit_num = array(
		array('::전체::',''),
		array('500 이하','1'),
		array('500~1k','2'),
		array('1k~3k','3'),
		array('3k~5k','4'),
		array('5k~10k','5'),
		array('10k 이상','6'),
		array('100k 이상','7')
	);
	$smarty->assign("visit_num",$visit_num);

	// [table : evaluation_unit] evaluation_state 배열
	$evaluation_state=array(
				'0'=>array('::전체::',''),
				'1'=>array('점수형(1~5점)','1'),
				'2'=>array('점수형(1~10점)','2'),
				'3'=>array('점수형(1~100점)','3'),
				'4'=>array('서술형(단문)','4'),
				'5'=>array('서술형(장문)','5'),
				'6'=>array('공감형(1~5)','6'),
				'7'=>array('공감형(1~10)','7'),
				'8'=>array('객관형','8'),
				'9'=>array('객관형(복수선택)','9'),
				'10'=>array('평가아님','99')
			);
	$smarty->assign("evaluation_state",$evaluation_state);

	// [table : to_do_list] state 배열
	$to_do_state=array(
				array('',''),
				array('진행중','1'),
				array('완료','2'),
				array('보류','3'),
				array('중단','4')
			);
	$smarty->assign("to_do_state",$to_do_state);

	// [table : product] kind 배열
	$product_kind=array(
				array('',''),
				array('개별업무형(기본)','1'),
				array('세트업무형','2')
			);
	$smarty->assign("product_kind",$product_kind);

	// 타겟키워드&노출키워드 적용상품 및 해당 여부 확인
	function is_t_r_keyword_prd($prd_f) {
		// 타겟키워드 적용 상품
		$t_r_keyword_prd=array(
				"", // 전체
				"7", // 연관검색어
				"30", // 자동완성
		);

		for($i = 0 ; $i < count($t_r_keyword_prd) ; $i++) {
			if($t_r_keyword_prd[$i] == $prd_f){
				return true;
			}
		}
		return false;
	}

	// 타겟키워드 적용상품 및 해당 여부 확인
	function is_t_keyword_prd($prd_f) {

		// 타겟키워드 적용상품
		$t_keyword_prd=array(
				"4", // 블로그 리뷰
				"5", // 카페 리뷰
				"6", // 모바일 리뷰
				"8", // 포스트 리뷰
				"9", // 지식인 리뷰
				"159", // 마케팅상품 > 컨텐츠광고 > 동영상 리뷰
				"10", // 언론기사
				"188", // 마케팅상품 > 컨텐츠광고 > 원고대행
				"35", // 블로그/카페 컨텐츠제작
				"36", // 네이버 포스트 컨텐츠제작
				"38", // 네이버 지식인 컨텐츠제작
				"39", // 언론기사 컨텐츠제작
				"40", // 페이스북 컨텐츠제작
				"54", // 인스타그램 컨텐츠제작
				"242", // 사업일정
				"243", // 사업일정 업무처리
		);

		// Smarty front(heml)쪽 스크립트에서 사용되는 변수 선언
		echo "<script>var t_keyword_prd = new Array('4','5','6','8','9','10','35','36','38','39','40','54','242','243');</script>";

		for($i = 0 ; $i < count($t_keyword_prd) ; $i++) {
			if($t_keyword_prd[$i] == $prd_f){
				return true;
			}
		}
		return false;
	}


	// 연장여부 적용상품 배열
	$extension_prd=array(
			array('::전체::',''),
			//array('모바일 리뷰','6'),
			//array('컨텐츠검색어','7'),
			//array('포스트 리뷰','8'),
			array('지식인 리뷰','9')
			//array('자동완성','30')
		);
	$smarty->assign("extension_prd",$extension_prd);

	// 연장여부 적용상품 및 해당 여부 확인
	function is_extension_prd($prd_f) {
		// 연장여부 적용상품
		$extension_prd=array(
				"", // 전체
				//"6", // 모바일 리뷰
				//"7", // 컨텐츠검색어
				//"8", // 포스트 리뷰
				"9" // 지식인 리뷰
				//"30" // 자동완성
		);

		for($i = 0 ; $i < count($extension_prd) ; $i++) {
			if($extension_prd[$i] == $prd_f){
				return true;
			}
		}
		return false;
	}

	#업무상태
	$working_state_max_sql   = "SELECT max(regdate) as reg FROM working_state GROUP BY s_no";
	$working_state_max_query = mysqli_query($my_db, $working_state_max_sql);
	$working_state_max_list  = [];
	while($working_state_max_result = mysqli_fetch_assoc($working_state_max_query))
	{
		$working_state_max_list[] = "'{$working_state_max_result['reg']}'";
	}
	$working_state_max_list = ($working_state_max_list != "") ? implode(',', $working_state_max_list) : "";

	$working_state_sql = "
			SELECT
				ws.s_no,
				(SELECT s_name FROM staff s WHERE s.s_no=ws.s_no) AS s_name,
				ws.team,
				ws.state,
				ws.regdate,
				ws.memo
			FROM working_state ws
			WHERE ws.regdate IN ({$working_state_max_list}) ORDER BY s_name ASC, regdate ASC
		";
	$working_state_query = mysqli_query($my_db,$working_state_sql);

	$working_state_list = [];
	while($working_state_array=mysqli_fetch_array($working_state_query)) {
		$working_state_list[$working_state_array['s_no']] = array(
			's_no'=>$working_state_array['s_no'],
			's_name'=>$working_state_array['s_name'],
			'team'=>$working_state_array['team'],
			'state'=>$working_state_array['state'],
			'regdate'=>$working_state_array['regdate'],
			'memo'=>$working_state_array['memo']
		);
	}

	$vacation_staff_list = [];
	foreach($working_state_list as $working_check){
		if($working_check['state'] == '4' || $working_check['state'] == '5'){
			$vacation_staff_list[] = $working_check['s_no'];
		}
	}

	// 업무처리 담당자 적용상품 및 해당 여부 확인
	$task_run_prd_sql   = "SELECT prd_no, task_run_staff_type FROM product WHERE display=1 AND ((task_run_staff is not null AND task_run_staff != '') OR task_run_staff_type ='2') ORDER BY prd_no ASC";
	$task_run_prd_query = mysqli_query($my_db, $task_run_prd_sql);
	$task_run_prd_list  = [];

	while($task_run_prd = mysqli_fetch_assoc($task_run_prd_query))
	{
		$task_run_prd_list[] = $task_run_prd['prd_no'];
	}
	$smarty->assign("task_run_prd_list", $task_run_prd_list);

	function is_task_run_prd($task_run_prd_list, $prd_f)
	{
		if(in_array($prd_f, $task_run_prd_list)){
			return true;
		}
		return false;
	}

	// 희망완료일, 업무완료예정일 등 업무처리 스케줄 적용상품 및 해당 여부 확인
	$task_dday_prd_sql    = "SELECT prd_no FROM product WHERE is_task_dday='1' ORDER BY prd_no ASC";
	$task_dday_prd_query  = mysqli_query($my_db, $task_dday_prd_sql);
	$task_dday_prd_list   = [];
	while($task_dday_prd = mysqli_fetch_assoc($task_dday_prd_query))
	{
		$task_dday_prd_list[] = $task_dday_prd['prd_no'];
	}
	$smarty->assign("task_dday_prd_list", $task_dday_prd_list);

	function is_task_dday_prd($task_dday_prd_list, $prd_f)
	{
		if(in_array($prd_f, $task_dday_prd_list)){
			return true;
		}
		return false;
	}

	// 상품별 동일한 희망완료일 수
	$task_req_dday_count_list=array(
				// 운영 업무 > 마케팅 컨텐츠 제작 > 블로그/카페 컨텐츠제작
				array('35','10'),
				// 운영 업무 > 마케팅 컨텐츠 제작 > 언론기사 컨텐츠제작
				array('39','8')
			);
	// 상품별 동일한 희망완료일 수
	function task_req_dday_count($f_prd_no, $f_task_req_dday){

		$task_req_dday_cnt_sql="SELECT count(*) FROM `work` w WHERE w.prd_no='{$f_prd_no}' AND w.task_req_dday='{$f_task_req_dday}'";
		//echo $task_req_dday_cnt_sql; //exit;
		$task_req_dday_cnt_data=mysqli_fetch_array(mysqli_query($GLOBALS['my_db'], $task_req_dday_cnt_sql));
		return $task_req_dday_cnt_data[0];
	}

	// 계열사 정보
	$my_company_name_list = [];
	$my_company_sql="SELECT
										mc.my_c_no,
										mc.ceo_s_no,
										mc.c_name,
										mc.kind_color,
										mc.company_number,
										mc.license_type,
										mc.active_state
								FROM my_company mc
								WHERE mc.active_state='1'
							";
	$my_company_query = mysqli_query($my_db,$my_company_sql);
	while($my_company_data = mysqli_fetch_array($my_company_query)){
		$my_company_list[]=array(
			"my_c_no"=>$my_company_data['my_c_no'],
			"ceo_s_no"=>$my_company_data['ceo_s_no'],
			"c_name"=>$my_company_data['c_name'],
			"kind_color"=>$my_company_data['kind_color'],
			"company_number"=>$my_company_data['company_number'],
			"license_type"=>$my_company_data['license_type'],
			"active_state"=>$my_company_data['active_state']
		);

		$my_company_name_list[$my_company_data['my_c_no']] = array('c_name' => $my_company_data['c_name'], 'color' => $my_company_data['kind_color']);
	}

	$smarty->assign("my_company_name_list",$my_company_name_list);
	$smarty->assign("my_company_list",$my_company_list);


  // 업무진행구분 리스트[k_name, k_name_code, prd_no]
	$kind_sql="SELECT
										k_name,
										k_name_code,
										k_parent AS prd_no
								FROM kind k
								WHERE k.k_code = 'work_task_run'
											AND k.display = '1'
								ORDER BY
						        k.priority ASC
							";
	$kind_query = mysqli_query($my_db,$kind_sql);

	while($product_data = mysqli_fetch_array($kind_query)){
		$work_task_run_kind_list[]=array(
			"k_name"=>$product_data['k_name'],
			"k_name_code"=>$product_data['k_name_code'],
			"prd_no"=>$product_data['prd_no']
		);
	}

	$smarty->assign("work_task_run_kind_list",$work_task_run_kind_list);

	// 업무진행구분 적용 상품 확인
	function is_k_name_code_prd($prd_f, $f_work_task_run_kind_list) {

		for($i = 0 ; $i < count($f_work_task_run_kind_list) ; $i++) {
			if($f_work_task_run_kind_list[$i]['prd_no'] == $prd_f){
				return true;
			}
		}
		return false;
	}


	//2차 배열에 관한 index search
	function searchArray($key, $st, $array) {
	   foreach ($array as $k => $v) {
	       if ($v[$key] === $st) {
	           return $k;
	       }
	   }
	   return null;
	}

	// [table : work] 업무 일정(달력)에서 조회일정
	$cal_date_type=array(
				'0'=>array('::전체::',''),
				'1'=>array('예정 완료일','1'),
				'2'=>array('업무 완료일','2'),
				'3'=>array('희망 완료일','3')
			);
	$smarty->assign("cal_date_type",$cal_date_type);

	// [table : work] 업무 일정(달력)에서 표시방법
	$cal_view_type=array(
				'0'=>array('::전체::',''),
				'1'=>array('상품명 보기','1'),
				'2'=>array('타겟키워드 보기','2'),
				'3'=>array('업무처리 담당자 보기','3'),
				'4'=>array('업체명 보기','4'),
				'5'=>array('마케팅 담당자 보기','5')
			);
	$smarty->assign("cal_view_type",$cal_view_type);

	// 상품 리스트 [그룹1 이름, 그룹1 code, 그룹2 이름, 그룹2 code, 상품명, 상품번호]
	$product_sql="SELECT
										(SELECT k2.k_name FROM kind k2 WHERE k2.k_name_code=(SELECT k.k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code)) AS g1_name,
										(SELECT k.k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code) AS g1_code,
										(SELECT k.k_name FROM kind k WHERE k.k_name_code=prd.k_name_code) AS g2_name,
										prd.k_name_code AS g2_code,
										prd.title, prd.prd_no
								FROM product prd
								WHERE prd.display='1'
							";
	$product_query = mysqli_query($my_db,$product_sql);

	$product_list[]=array('::전체::','0','');
	while($product_data = mysqli_fetch_array($product_query)){
		$product_list[]=array(
			"g1_name"=>$product_data['g1_name'],
			"g1_code"=>$product_data['g1_code'],
			"g2_name"=>$product_data['g2_name'],
			"g2_code"=>$product_data['g2_code'],
			"title"=>$product_data['title'],
			"prd_no"=>$product_data['prd_no']
		);
	}

	$smarty->assign("product_list",$product_list);

	// [table : okr_obj] state 배열
	$okr_obj_state=array(
				'0'=>array('::전체::',''),
				'1'=>array('작성중','1'),
				'2'=>array('진행중','2'),
				'3'=>array('종료','3'),
				'4'=>array('보류','4'),
				'5'=>array('취소','5')
			);
	$smarty->assign("okr_obj_state",$okr_obj_state);

	// [table : okr_obj] kind 배열
	$okr_obj_kind = array(
		'0' => array('::전체::',''),
		'1' => array('팀','1'),
		'2' => array('개인','2')
	);
	$smarty->assign("okr_obj_kind",$okr_obj_kind);

	$obj_term_list = array(
		'1' => '1/3분기',
		'2' => '2/3분기',
		'3' => '3/3분기',
		'4' => '연간',
	);

	// 출금 대상 상품 리스트 [상품명, 상품번호, 작업대상]
	$product_sql="SELECT prd.title, prd.prd_no
											, (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT k.k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code))  AS k_name_1
											, (SELECT k.k_name FROM kind k WHERE k.k_name_code=prd.k_name_code)  AS k_name_2
								FROM product prd
								WHERE (prd.wd_dp_state='3' OR prd.wd_dp_state='4') AND prd.display='1'
								ORDER BY prd.k_name_code ASC
							"; // 일부상품만 OPEN함
	$product_query = mysqli_query($my_db,$product_sql);

	$out_product[]=array('::전체::','0','');
	while($product_data = mysqli_fetch_array($product_query)){
		$out_product[]=array($product_data['title'], $product_data['prd_no'], $product_data['k_name_1'], $product_data['k_name_2'] );
	}
	$smarty->assign("out_product",$out_product);


	// 입금 대상 상품 리스트 [상품명, 상품번호, 작업대상]
	$product_sql="SELECT prd.title, prd.prd_no
											, (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT k.k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code))  AS k_name_1
											, (SELECT k.k_name FROM kind k WHERE k.k_name_code=prd.k_name_code)  AS k_name_2
								FROM product prd
								WHERE (prd.wd_dp_state='2' OR prd.wd_dp_state='4') AND prd.display='1'
								ORDER BY prd.k_name_code ASC
							";
	$product_query = mysqli_query($my_db,$product_sql);

	$in_product[]=array('::전체::','0','');
	while($product_data = mysqli_fetch_array($product_query)){
		$in_product[]=array($product_data['title'], $product_data['prd_no'], $product_data['k_name_1'], $product_data['k_name_2'] );
	}
	$smarty->assign("in_product",$in_product);


	// 선지급 업체 및 상품 리스트 [상품번호, 업체번호]
	$paid_list_sql 	 = "SELECT prd_no, wd_c_no_list, wd_method_list, dp_c_no_list, dp_method_list FROM product ORDER BY prd_no ASC";
	$paid_list_query = mysqli_query($my_db, $paid_list_sql);
	$total_dp_list   = [];
	$total_wd_list   = [];
	$paid_list 		 = [];
	$month_paid_list = [];

	while($paid_result = mysqli_fetch_assoc($paid_list_query))
	{
		if(!empty($paid_result['wd_c_no_list'])){
			$wd_c_no_list 	 = explode(",", $paid_result['wd_c_no_list']);
			$wd_method_list  = explode(",", $paid_result['wd_method_list']);

			for($wd_idx=0; $wd_idx < count($wd_c_no_list); $wd_idx++){
				$wd_c_no 	= $wd_c_no_list[$wd_idx];
				$wd_method  = $wd_method_list[$wd_idx];

				if($wd_method == '3'){
					$paid_list[] = array($paid_result['prd_no'], $wd_c_no);
				}elseif($wd_method == '4'){
					$month_paid_list[] = array($paid_result['prd_no'], $wd_c_no);
				}

                $total_wd_list[$paid_result['prd_no']][$wd_c_no] = $wd_method;
			}
		}

		if(!empty($paid_result['dp_c_no_list'])){
			$dp_c_no_list 	 = explode(",", $paid_result['dp_c_no_list']);
			$dp_method_list  = explode(",", $paid_result['dp_method_list']);

			for($dp_idx=0; $dp_idx < count($dp_c_no_list); $dp_idx++)
			{
				$dp_c_no 	= $dp_c_no_list[$dp_idx];
				$dp_method  = !empty($dp_method_list[$dp_idx]) ? $dp_method_list[$dp_idx] : 2;

				$total_dp_list[$paid_result['prd_no']][$dp_c_no] = $dp_method;
			}
		}
	}

	$smarty->assign("paid_list", $paid_list);
	$smarty->assign("month_paid_list", $month_paid_list);

	$team_list_sql   	= "SELECT team_name, team_code, team_code_parent, depth FROM team WHERE display = 1 ORDER BY priority ASC";
	$team_list_query 	= mysqli_query($my_db, $team_list_sql);
	$team_all_list   	= [];
	$team_list   		= [];
	$team_name_list   	= [];
	$sch_team_name_list = array("all" => ":: 전체 ::");
	while($team = mysqli_fetch_array($team_list_query))
	{
		$team_all_list[] = array(
			'team_name'        => $team['team_name'],
			'team_code'        => $team['team_code'],
			'team_code_parent' => $team['team_code_parent']
		);

		$team_list[] = array($team['team_name'], $team['team_code'], $team['team_code_parent']);
		$team_name_list[$team['team_code']] = $team['team_name'];

        $team_prev = "";
        if($team['depth'] > 1){
            for($i=1;$i<$team['depth'];$i++){
                $team_prev .= ">";
            }
        }
        $sch_team_name_list[$team['team_code']] = empty($team_prev) ? $team['team_name'] : " {$team_prev} {$team['team_name']}";
	}
	$smarty->assign("team_list",$team_list);

	# 전체 업체리스트
	$company_name_sql 	= "SELECT c_no, c_name FROM company WHERE display='1'";
	$company_name_query = mysqli_query($my_db, $company_name_sql);
	$company_name_list 	= [];
	while($company_name = mysqli_fetch_assoc($company_name_query))
	{
		$company_name_list[$company_name['c_no']] = $company_name['c_name'];
	}

	# 출급업체 리스트
	$wd_company_list[] = array('::전체::','','','','');
	$wd_company_sql = "SELECT prd_no, wd_c_no_list, wd_c_label_list FROM product WHERE wd_c_no_list != ''";
	$wd_company_query = mysqli_query($my_db, $wd_company_sql);

	while($wd_company_result = mysqli_fetch_assoc($wd_company_query))
	{
		$wd_prd_no			 = $wd_company_result['prd_no'];
		$wd_comp_c_no_list 	 = explode(',', $wd_company_result['wd_c_no_list']);
		$wd_comp_c_nick_list = explode(',', $wd_company_result['wd_c_label_list']);

		for($wd_idx = 0; $wd_idx < count($wd_comp_c_no_list); $wd_idx++)
		{
			$wd_comp_c_no   = isset($wd_comp_c_no_list[$wd_idx]) ? $wd_comp_c_no_list[$wd_idx] : "";
			$wd_comp_c_nick = isset($wd_comp_c_nick_list[$wd_idx]) ? $wd_comp_c_nick_list[$wd_idx] : "";
			$wd_comp_c_name = isset($company_name_list[$wd_comp_c_no]) ? $company_name_list[$wd_comp_c_no] : "";

			if($wd_comp_c_no){
				$wd_company_list[] = array($wd_comp_c_name, $wd_comp_c_no, $wd_prd_no, $wd_comp_c_nick);
			}
		}
	}
	$smarty->assign("wd_company_list",$wd_company_list);

	// 출금업체 리스트 확인
	function is_wd_company_list($prd_f, $wd_company_list_f) {

		for($i = 0 ; $i < count($wd_company_list_f) ; $i++) {
			if($wd_company_list_f[$i][2] == $prd_f){
				return true;
			}
		}
		return false;
	}

	# 입급업체 리스트
	$dp_company_list 	= [];
	$dp_company_sql 	= "SELECT prd_no, dp_c_no_list, dp_c_label_list FROM product WHERE dp_c_no_list != ''";
	$dp_company_query 	= mysqli_query($my_db, $dp_company_sql);
	while($dp_company_result = mysqli_fetch_assoc($dp_company_query))
	{
		$dp_prd_no			 = $dp_company_result['prd_no'];
		$dp_comp_c_no_list 	 = explode(',', $dp_company_result['dp_c_no_list']);
		$dp_comp_c_nick_list = explode(',', $dp_company_result['dp_c_label_list']);

		for($dp_idx = 0; $dp_idx < count($dp_comp_c_no_list); $dp_idx++)
		{
			$dp_comp_c_no   = isset($dp_comp_c_no_list[$dp_idx]) ? $dp_comp_c_no_list[$dp_idx] : "";
			$dp_comp_c_nick = isset($dp_comp_c_nick_list[$dp_idx]) ? $dp_comp_c_nick_list[$dp_idx] : "";
			$dp_comp_c_name = isset($company_name_list[$dp_comp_c_no]) ? $company_name_list[$dp_comp_c_no] : "";

			if($dp_comp_c_no){
				$dp_company_list[] = array($dp_comp_c_name, $dp_comp_c_no, $dp_prd_no, $dp_comp_c_nick);
			}
		}
	}
	$smarty->assign("dp_company_list",$dp_company_list);

	// 입금업체 리스트 확인
	function is_dp_company_list($prd_f, $dp_company_list_f) {

		for($i = 0 ; $i < count($dp_company_list_f) ; $i++) {
			if($dp_company_list_f[$i][2] == $prd_f){
				return true;
			}
		}
		return false;
	}

	//전체 업무상품
	$work_product_list_sql = "SELECT prd_no, title FROM product WHERE display='1'";
	$work_product_list_query = mysqli_query($my_db, $work_product_list_sql);
	$work_product_all_list   = [];

	while($work_product = mysqli_fetch_array($work_product_list_query))
	{
		$work_product_all_list[$work_product['prd_no']] = $work_product['title'];
	}

	//전체 staff list
	$staff_list_sql   = "SELECT s_no, s_name, team, slack_id FROM staff WHERE staff_state < '4' ORDER BY s_no";
	$staff_list_query = mysqli_query($my_db, $staff_list_sql);
	$staff_all_list   = [];
	$slack_all_list	  = [];
	while($staff_info = mysqli_fetch_array($staff_list_query))
	{
		$staff_all_list[$staff_info['s_no']] = array(
			's_name' => $staff_info['s_name'],
			'team' 	 => $staff_info['team'],
		);

		$slack_all_list[$staff_info['s_no']] = $staff_info['slack_id'];
	}

	// 업무요청 관리자(접수 및 처리 담당자 설청) 리스트
	$task_req_staff_sql 	 	= "SELECT prd_no, task_req_staff, task_req_staff_type FROM product where display = 1 AND ((task_req_staff != '' AND task_req_staff IS NOT NULL) OR task_req_staff_type='2')";
	$task_req_staff_query 		= mysqli_query($my_db, $task_req_staff_sql);
	$task_req_accepter_list		= [];
	$task_req_accepter_slack_list = [];
	while($task_req_accepter 	= mysqli_fetch_array($task_req_staff_query))
	{
		$task_req_accepters = explode(',', $task_req_accepter['task_req_staff']);
		foreach($task_req_accepters as $req_accepter){
			$req_data = explode("_", $req_accepter);
			$req_s_no = $req_data[0];
			$task_req_accepter_list[] = array($staff_all_list[$req_s_no]['s_name'], $req_s_no, $task_req_accepter['prd_no'], $task_req_accepter['task_req_staff_type']);
			$task_req_accepter_slack_list[$task_req_accepter['prd_no']][] = $req_s_no;
		}
	}
	$smarty->assign("task_req_accepter_list",$task_req_accepter_list);

	// 업무요청 관리자(접수 및 처리 담당자 설청) 리스트 확인
	function is_task_req_accepter_list($s_no_f, $prd_f, $task_req_accepter_list_f) {

		for($i = 0 ; $i < count($task_req_accepter_list_f) ; $i++) {

            if($task_req_accepter_list_f[$i][2] == $prd_f && $task_req_accepter_list_f[$i][3] == '2'){
                return true;
            }

			if($task_req_accepter_list_f[$i][1] == $s_no_f && $task_req_accepter_list_f[$i][2] == $prd_f){
				return true;
			}
		}
		return false;
	}

	// 퇴사자 업무에서 제외
	$non_staff_list_sql			= "SELECT s_no FROM staff WHERE staff_state = '3' ORDER BY s_no";
	$non_staff_list_query		= mysqli_query($my_db, $non_staff_list_sql);
	$non_staff_list				= [];
	while($non_staff = mysqli_fetch_assoc($non_staff_list_query))
	{
		$non_staff_list[] = $non_staff['s_no'];
	}

	// 업무처리자 리스트
	#업무 요청자, 처리자, 작성자 처리작업
	$task_run_staff_list_sql 	= "SELECT prd_no, task_run_staff, task_run_staff_default, task_run_staff_type FROM product WHERE display = 1 AND ((task_run_staff != '' AND task_run_staff IS NOT NULL) OR task_run_staff_type='2')";
	$task_run_staff_list_query 	= mysqli_query($my_db, $task_run_staff_list_sql);
	$task_run_staff_list[]		= array('::전체::','','');
	$task_run_accepter_list     = [];
	while($task_run_staff = mysqli_fetch_array($task_run_staff_list_query))
	{
		$task_run_staffs =explode(',',  $task_run_staff['task_run_staff']);
		foreach($task_run_staffs as $run_staff)
		{
			if(in_array($run_staff, $non_staff_list)){
				continue;
			}
			$run_staff_data  = explode("_", $run_staff);
			$run_staff_s_no  = $run_staff_data[0];
			$run_staff_team  = $run_staff_data[1];
			$run_staff_label = $staff_all_list[$run_staff_s_no]['s_name']."(".$team_name_list[$run_staff_team].")";
			$task_run_staff_list[] = ($task_run_staff['task_run_staff_default'] == $run_staff) ? array($run_staff_label, $run_staff_s_no, $task_run_staff['prd_no'], '기본', $run_staff_team) : array($run_staff_label, $run_staff_s_no, $task_run_staff['prd_no'], '', $run_staff_team);
			$task_run_accepter_list[] = array($run_staff_label, $run_staff_s_no, $task_run_staff['prd_no'], $task_run_staff['task_run_staff_type']);
		}
	}
	$smarty->assign("task_run_accepter_list",$task_run_accepter_list);

	function is_task_run_accepter_list($s_no_f, $prd_f, $task_run_accepter_list_f)
	{
		for($i = 0 ; $i < count($task_run_accepter_list_f) ; $i++) {

			if($task_run_accepter_list_f[$i][2] == $prd_f && $task_run_accepter_list_f[$i][3] == '2'){
                return true;
            }

			if($task_run_accepter_list_f[$i][1] == $s_no_f && $task_run_accepter_list_f[$i][2] == $prd_f){
				return true;
			}
		}
		return false;
	}

	// 업무처리자 리스트
	$smarty->assign("task_run_staff_list", $task_run_staff_list);


	// (프리랜서 대상) 상품별 접근허용 리스트
	$access_accept_list=array(
		// 운영 업무 > 마케팅 컨텐츠 제작 > 블로그/카페 컨텐츠제작
		array('이해인',	'82',	'35'),
		array('김지현',	'108',	'35'),
		array('엄다정',	'83',	'35'),
		array('김혜미',	'100',	'35'),
		array('최예나',	'115',	'35'),
//		array('이정은',	'116',	'35'),
		array('전하빈',	'118',	'35'),
		array('최민경',	'123',	'35'),
//		array('이규영',	'126',	'35'),

		// 운영 업무 > 마케팅 컨텐츠 제작 > 네이버 포스트 컨텐츠제작
		array('김지현',	'108',	'36'),
		array('김혜미',	'100',	'36'),

		// 운영 업무 > 마케팅 컨텐츠 제작 > 언론기사 컨텐츠제작
		array('엄다정',	'83',	'39'),
//		array('박아름',	'94',	'39'),
//		array('김신희',	'120',	'39'),
//		array('박세연',	'133',	'39')
	);
	$smarty->assign("access_reject_list",$access_reject_list);

	// 상품별 접근제한 리스트
	$access_reject_list=array(
		array('이해인',	'82',	'all'), // all은 전체
		array('김지현',	'108',	'all'),
		array('김혜미',	'100',	'all'),
		array('최예나',	'115',	'all'),
//		array('이정은',	'115',	'all'),
		array('전하빈',	'118',	'all'),
		array('최민경',	'123',	'all'),
//		array('이규영',	'126',	'all'),

		array('엄다정',	'83',	'all'),
//		array('박아름',	'94',	'all'),
//		array('김신희',	'120',	'all'),
//		array('박세연',	'133',	'all'),

		array('문상수',	'117',	'all')
	);
	$smarty->assign("access_reject_list",$access_reject_list);


	/* 커머스 발주 공급업체 리스트
	$supply_commerce_list=array(
		array('(주)성일화학',		'2304'),
		array('듀벨',		'2305'),
		array('(주)비앤비코리아',		'2477'),
		array('자성스포츠',		'2282'),
		array('호호바스',		'2871')
	);
	$smarty->assign("supply_commerce_list",$supply_commerce_list);
	// 커머스 발주 공급업체 리스트
	*/

	// 노출 실패 키워드 사례 상품번호, 키워드, 메모 (4:블로그상위, 6:모바일상위)
	$bad_tkeyword_list=array(
		/*
		array('6','화곡동 맛집'),
		array('6','서현역 맛집'),
		array('6','영등포구청역 맛집'),
		array('6','논현역 맛집'),
		array('6','울산 달동 맛집'),
		array('6','김포 구래동 맛집'),
		array('6','합정역 맛집'),
		array('6','판교 브런치','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('6','홍대 파스타 맛집'),
		array('6','제부도 맛집', '유사공격 사례 있음'),
		array('6','창원 봉곡동 맛집', '유사공격 사례 있음'),
		array('6','홍대 고기 맛집', '유사공격 사례 있음'),
		array('6','광화문역 맛집', '작업불가'),
		array('6','파주 헤이리 맛집', '유사공격 사례 있음'),
		array('6','송파구 맛집', '유사공격 사례 있음'),
		array('6','판교역 맛집','AF에서 실패함'),
		array('6','명동 맛집 베스트','작업불가'),
		array('6','일산 맛집 베스트10','작업불가'),
		array('6','미사리 맛집','작업불가'),
		array('6','제주 모슬포 맛집','작업불가'),
		array('6','혜화 대학로 맛집','작업불가'),
		array('6','잠실 데이트','작업불가'),
		array('6','신사역 맛집','작업불가'),
		array('6','김해 삼계동 맛집','작업불가'),
		array('6','도담동 맛집','작업불가'),
		array('6','화곡 맛집','작업불가'),
		array('6','명동역 맛집','작업불가'),
		array('6','신논현역 맛집','작업불가'),
		array('6','학동역 맛집','작업불가'),
		array('6','삼성역 맛집','작업불가'),
		array('6','인천 구월동 맛집','작업불가'),
		array('6','구월동 맛집','작업불가'),
		array('6','을지로 맛집','작업불가'),
		array('6','서촌 맛집','작업불가'),
		array('6','경복궁 맛집','작업불가'),
		array('6','역삼동 맛집','작업불가'),
		array('6','수유역 맛집','작업불가'),
		*/
		array('4','강남역 모임장소','작업불가'),
		array('4','강남역 회식장소','작업불가'),
		array('4','카오디오 장착','작업불가'),
		array('4','탯줄도장','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','제주도 기념품','작업불가'),
		array('4','선릉역 회식장소','작업불가'),
		array('4','3D 모델링','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','삼성동 회식장소','작업불가'),
		array('4','하남 미사 맛집','작업불가'),
		array('4','소자본 창업','작업불가'),
		array('4','소액창업아이템','작업불가'),
		array('4','권선동 맛집','작업불가'),
		array('4','홍대 무한리필','작업불가'),
		array('4','송도 레스토랑','작업불가'),
		array('4','분당 회식장소','작업불가'),
		array('4','회식장소','작업불가'),
		array('4','단체 펜션','작업불가'),
		array('4','부부창업','작업불가'),
		array('4','양수리 맛집','작업불가'),
		array('4','양평 두물머리 맛집','작업불가'),
		array('4','경기도 양평 맛집','작업불가'),
		array('4','충남대 맛집','작업불가'),
		array('4','신논현역 맛집','작업불가'),
		array('4','강남 논현동 맛집','작업불가'),
		array('4','동탄맛집추천','작업불가'),
		array('4','성공창업아이템','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','뜨는프렌차이즈','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','마장동 축산물시장','작업불가'),
		array('4','커피메이커','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','강남역 점심','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','서울 상견례 장소','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','대화역 맛집','작업불가'),
		array('4','강남 모임장소','작업불가'),
		array('4','신천역 맛집','작업불가'),
		array('4','악건성 수분크림','작업불가'),
		array('4','잠실새내역 맛집','작업불가'),
		array('4','아이스크림 할인점','작업불가'),
		array('4','요즘뜨는창업','작업불가'),
		array('4','여성창업아이템','작업불가'),
		array('4','킨텍스 맛집','작업불가'),
		array('4','남자소자본창업','작업불가'),
		array('4','아이스크림 할인점','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','서울 명동 맛집','작업불가'),
		array('4','서울시청역맛집','작업불가'),
		array('4','서울 한식 맛집','작업불가'),
		array('4','장한평 맛집','작업불가'),
		array('4','중곡동맛집','작업불가'),
		array('4','이태원 마사지','작업불가'),
		array('4','강남 메이크업','작업불가'),
		array('4','흙침대','작업불가'),
		array('4','대외활동','작업불가'),
		array('4','소개팅어플','작업불가'),
		array('4','강아지 산책','작업불가'),
		array('4','건대 미용실','작업불가'),
		array('4','건대미용실추천','작업불가'),
		array('4','노원 미용실','작업불가'),
		array('4','건강식단','작업불가'),
		array('4','유아크림','작업불가'),
		array('4','소자본창업','작업불가'),
		array('4','떡볶이 만들기','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','정부지원 사업','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','업종변경창업','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','중소기업 지원','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','성공창업','작업불가'),
		array('4','음식점창업','작업불가'),
		array('4','블루투스마이크','작업불가'),
		array('4','얼굴비대칭교정','작업불가'),
		array('4','식당창업','작업불가'),
		array('4','고기집창업','작업불가'),
		array('4','뜨는창업','작업불가'),
		array('4','고깃집창업','작업불가'),
		array('4','청주이사','작업불가'),
		array('4','대외활동','작업불가'),
		array('4','청주포장이사','작업불가'),
		array('4','천안포장이사','작업불가'),
		array('4','소개팅어플','작업불가'),
		array('4','한달 다이어트 식단','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','용산마사지','작업불가'),
		array('4','홍대 미용실 추천','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','비발디파크 맛집','작업불가'),
		array('4','오션월드 맛집','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','가로수길 점심','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','강남 메이크업샵','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','용산피부관리','작업불가'),
		array('4','창업박람회','작업불가'),
		array('4','신규창업아이템','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','편의점 도시락','작업불가'),
		array('4','서울행사','작업불가'),
		array('4','서울축제','작업불가'),
		array('4','서울 나들이','작업불가'),
		array('4','서울 가볼만한곳','작업불가'),
		array('4','레몬밤 효능','작업불가'),
		array('4','레몬밤','작업불가'),
		array('4','집들이선물','작업불가'),
		array('4','창업성공사례','작업불가'),
		array('4','요식업창업','작업불가'),
		array('4','서울 이색데이트','작업불가'),
		array('4','소액창업','작업불가'),
		array('4','소규모창업','작업불가'),
		array('4','뜨는창업아이템','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','주부창업아이템','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','11월 제주도 가볼만한곳','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('4','카페창업','작업불가'),
		array('4','유망프렌차이즈','작업불가'),
		array('4','업종전환창업','작업불가'),
		array('4','프랜차이즈창업','작업불가'),
		array('4','소규모창업아이템','작업불가'),
		array('4','가로수길 데이트','작업불가'),

		array('5','신논현역 맛집','실패 및 접수거절 내역이 있음'),
		array('5','소규모창업아이템','실패 및 접수거절 내역이 있음'),
		array('5','여자소자본창업','실패 및 접수거절 내역이 있음'),
		array('5','선릉역 회식장소','실패 및 접수거절 내역이 있음'),
		array('5','판교 맛집','실패 및 접수거절 내역이 있음'),
		array('5','대전 유성 맛집','실패 및 접수거절 내역이 있음'),
		array('5','대전 유성구 맛집','실패 및 접수거절 내역이 있음'),
		array('5','인천 구월동 맛집','실패 및 접수거절 내역이 있음'),
		array('5','대부도 맛집','실패 및 접수거절 내역이 있음'),
		array('5','화성 맛집','실패 및 접수거절 내역이 있음'),
		array('5','강남역 회식장소','실패 및 접수거절 내역이 있음'),
		array('5','강남역 술집','실패 및 접수거절 내역이 있음'),
		array('5','논현동 맛집','실패 및 접수거절 내역이 있음'),
		array('5','강남 논현동 맛집','실패 및 접수거절 내역이 있음'),
		array('5','구의동 맛집','실패 및 접수거절 내역이 있음'),
		array('5','상암 맛집','실패 및 접수거절 내역이 있음'),
		array('5','대구 동성로 맛집','실패 및 접수거절 내역이 있음'),
		array('5','목동 맛집','실패 및 접수거절 내역이 있음'),
		array('5','일산 모임장소','실패 및 접수거절 내역이 있음'),
		array('5','일산회식장소','실패 및 접수거절 내역이 있음'),
		array('5','발산역 맛집','실패 및 접수거절 내역이 있음'),
		array('5','발산 맛집','실패 및 접수거절 내역이 있음'),
		array('5','강서구 맛집','실패 및 접수거절 내역이 있음'),
		array('5','덕수궁 맛집','실패 및 접수거절 내역이 있음'),
		array('5','향남 맛집','실패 및 접수거절 내역이 있음'),
		array('5','주부창업아이템','실패 및 접수거절 내역이 있음'),
		array('5','제부도 가볼만한곳','실패 및 접수거절 내역이 있음'),
		array('5','창업정보','실패 사례가 있음'),
		array('5','유성온천 맛집','실패 사례가 있음'),
		array('5','금천구 맛집','실패 사례가 있음'),

		// 마케팅 상품 > 컨텐츠광고 > 모바일 리뷰
		array('6','대전 둔산동 맛집','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('6','선릉역 맛집','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('6','대구 동성로 맛집','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('6','발산역 맛집','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('6','선릉 맛집','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('6','마포역 맛집','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('6','삼성동 맛집','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('6','대전 봉명동 맛집','실패 사례가 있어 작업가능성이 매우 낮음'),
		array('6','천안고기맛집','실패 사례가 있음'),
		array('6','방이동 먹자골목 맛집','실패 사례가 있음'),
		array('6','울산 달동 맛집','실패 사례가 있음'),
		array('6','상암동 맛집','실패 사례가 있음'),
		array('6','동탄 회식장소','실패 사례가 있음'),
		array('6','종각역 맛집','실패 사례가 있음'),

		// 마케팅 상품 > 컨텐츠광고 > 카페 리뷰
		array('9','샤워기헤드','SS:노출 불안정'),
		array('9','가로수길 데이트','SS:노출 불안정'),
		array('9','신생아선물','SS:장기 미노출 접수불가'),
		array('9','다이어트간식','SS:장기 미노출 접수불가')
	);
	$smarty->assign("bad_tkeyword_list",$bad_tkeyword_list);


	// [table : working_state] state 배열
	$working_state=array(
				array('::전체::',''),
				array('일반업무중','1'),
				array('재택근무중','2'),
				array('외근중','3'),
				array('대체휴일중','4'),
				array('휴가중','5')
	);
	$smarty->assign("working_state",$working_state);


	// 게시판 리스트[table_name, b_id, board_name]
	$board_manager_sql="SELECT
										brd_mng.table_name,
										brd_mng.b_id,
										brd_mng.board_name,
										brd_mng.contents_format,
										brd_mng.comment_format
								FROM board_manager brd_mng
								WHERE brd_mng.display='1'
								ORDER BY brd_mng.priority ASC
							";
	$board_manager_query = mysqli_query($my_db,$board_manager_sql);

	while($board_manager_data = mysqli_fetch_array($board_manager_query)){
		$board_manager_list[]=array(
			"table_name"=>$board_manager_data['table_name'],
			"b_id"=>$board_manager_data['b_id'],
			"board_name"=>$board_manager_data['board_name'],
			"contents_format"=>$board_manager_data['contents_format'],
			"comment_format"=>$board_manager_data['comment_format']
		);
	}

	$smarty->assign("board_manager_list",$board_manager_list);

	// 판매가 적용 상품 리스트 Start
	$product_sql="SELECT
										prd.prd_no,
										prd.title
								FROM product prd
								WHERE prd.price_apply = '2'
											AND prd.display = '1'
							";
	$product_query = mysqli_query($my_db,$product_sql);

	while($product_data = mysqli_fetch_array($product_query)){
		$price_apply_prd_list[]=array(
			"prd_no"=>$product_data['prd_no'],
			"title"=>$product_data['title']
		);
	}
	$smarty->assign("price_apply_prd_list",$price_apply_prd_list);
	// 판매가 적용 상품 리스트 End

	// 판매가 적용 리스트 확인
	function is_price_apply_prd_list($prd_f, $price_apply_prd_list_f) {
		for($i = 0 ; $i < count($price_apply_prd_list_f) ; $i++) {
			if($price_apply_prd_list_f[$i]['prd_no'] == $prd_f){
				return true;
			}
		}
		return false;
	}


	$today = date("Y-m-d");
	$nextday = date("Y-m-d", strtotime("+1 day"));
	$smarty->assign("today",$today);
	$smarty->assign("nextday",$nextday);

	$totime = date("h:i:s");
	$smarty->assign("totime",$totime);

	$toyear = date("Y", strtotime($today));
	$smarty->assign("toyear",$toyear);

	$smarty->assign("dir_location","");

//	$nextmonth = date("Y-m-d", strtotime("+1 month"));
//	$smarty->assign("endday",$endday);

	$tomonth = date("Ym");
	$smarty->assign("tomonth",$tomonth);

	$tomonth_typeB = date("Y-m");
	$smarty->assign("tomonth_typeB",$tomonth_typeB);

	$nextmonth = date("Y-m", strtotime("+1 month"));
	$smarty->assign("nextmonth",$nextmonth);

/* WORK LIST */
$work_prd_list_sql   = "
		SELECT
			k_name, k_name_code, k_parent
		FROM kind
		WHERE k_code='product' AND display = 1
		ORDER BY priority ASC
	";

$work_list_query = mysqli_query($my_db, $work_prd_list_sql);
$work_list       = [];
$work_k_names	 = [];
while($work = mysqli_fetch_array($work_list_query))
{
	$work_list[$work['k_parent']][] = array(
		'k_name'        => $work['k_name'],
		'k_name_code'   => $work['k_name_code'],
		'k_parent' 		=> $work['k_parent']
	);
	$work_k_names[] = $work['k_name_code'];
}
$smarty->assign("work_list", $work_list);

/* WORK PRD LIST */

$work_k_names = implode(',',$work_k_names);
$word_prd_list_sql   = "
			SELECT
				title, prd_no, k_name_code
			FROM product
			WHERE display='1' AND k_name_code IN({$work_k_names})";

$work_prd_list_query = mysqli_query($my_db, $word_prd_list_sql);
$work_prd_list       = [];
while($work_prd = mysqli_fetch_array($work_prd_list_query))
{
	$work_prd_list[$work_prd['k_name_code']][] = array(
		'title'       => $work_prd['title'],
		'prd_no'      => $work_prd['prd_no'],
		'k_name_code' => $work_prd['k_name_code']
	);
}
$smarty->assign("work_prd_list", $work_prd_list);

function folderCheck($folder)
{
	$rootImgFolderPath	 = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$folder;
	$folderNameY = date("Y"); //폴더 년도 생성
	$folderNameM = date("n"); //폴더 월 생성
	$folderName  = $folderNameY."/".$folderNameM;

	if(!is_dir($rootImgFolderPath."/".$folderNameY)){
		mkdir($rootImgFolderPath."/".$folderNameY, 0777);
	}

	if(!is_dir($rootImgFolderPath."/".$folderNameY."/".$folderNameM)){
		mkdir(($rootImgFolderPath."/".$folderNameY."/".$folderNameM), 0777);
	}

	return $folderName;
}

$holiday_sql 	= "SELECT * FROM holiday_info";
$holiday_query 	= mysqli_query($my_db, $holiday_sql);
$holiday_list		= [];
$holiday_name_list	= [];

while($holiday = mysqli_fetch_array($holiday_query))
{
	$holiday_list[$holiday['h_date']]['title']	= $holiday['h_name'];
	$holiday_name_list[$holiday['h_date']]      = $holiday['h_name'];
}

$smarty->assign('holiday_name_list', $holiday_name_list);
$smarty->assign('holiday_list', json_encode($holiday_list));

# 입금 타입 리스트
$sch_mod_type_list = array(
    "modify" => "수정요청",
    "refund" => "환불요청"
);

?>
