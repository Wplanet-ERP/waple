<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Approval extends Model
{
    protected $_main_table = "approval_form";
    protected $_main_key   = "af_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Approval();
    }

    public function setReportTable()
    {
        $this->_main_table  = "approval_report";
        $this->_main_key    = "ar_no";
    }

    public function setReportPermissionTable()
    {
        $this->_main_table  = "approval_report_permission";
        $this->_main_key    = "arp_no";
    }

    public function getApprovalFormList()
    {
        $approval_form_sql   = "SELECT af_no, `title` FROM approval_form WHERE display='1' ORDER BY af_no";
        $approval_form_query = mysqli_query($this->my_db, $approval_form_sql);
        $approval_form_list  = [];
        while($approval_form = mysqli_fetch_assoc($approval_form_query)){
            $approval_form_list[$approval_form['af_no']] = $approval_form['title'];
        }

        return $approval_form_list;
    }

    public function checkApprovalState($type, $req_s_no)
    {
        $result = "";

        if($type == "wait_approval")
        {
            $check_sql   = "SELECT arp.ar_no, arp.priority FROM approval_report_permission arp LEFT JOIN approval_report ar ON ar.ar_no=arp.ar_no WHERE ar.ar_state='2' AND arp.arp_s_no='{$req_s_no}' AND arp.arp_type='approval' AND arp.arp_state='1'";
            $check_query = mysqli_query($this->my_db, $check_sql);
            $check_list  = [];

            while ($check = mysqli_fetch_assoc($check_query))
            {
                $priority_sql    = "SELECT count(arp.arp_no) as cnt FROM approval_report_permission arp WHERE arp.ar_no='{$check['ar_no']}' AND arp.arp_type IN('approval','conference') AND arp.arp_state='1' AND arp.priority < '{$check['priority']}'";
                $priority_query  = mysqli_query($this->my_db, $priority_sql);
                $priority_result = mysqli_fetch_assoc($priority_query);

                if(isset($priority_result['cnt']) && $priority_result['cnt'] == 0){
                    $check_list[] = $check['ar_no'];
                }
            }
        }
        elseif($type == "wait_conference")
        {
            $check_sql   = "SELECT arp.ar_no, arp.priority FROM approval_report_permission arp LEFT JOIN approval_report ar ON ar.ar_no=arp.ar_no WHERE ar.ar_state='2' AND arp.arp_s_no='{$req_s_no}' AND arp.arp_type='conference' AND arp.arp_state='1'";
            $check_query = mysqli_query($this->my_db, $check_sql);
            $check_list  = [];

            while ($check = mysqli_fetch_assoc($check_query))
            {
                $priority_sql    = "SELECT count(arp.arp_no) as cnt FROM approval_report_permission arp WHERE arp.ar_no='{$check['ar_no']}' AND arp.arp_type IN('approval','conference') AND arp.arp_state='1' AND arp.priority < '{$check['priority']}'";
                $priority_query  = mysqli_query($this->my_db, $priority_sql);
                $priority_result = mysqli_fetch_assoc($priority_query);

                if(isset($priority_result['cnt']) && $priority_result['cnt'] == 0){
                    $check_list[] = $check['ar_no'];
                }
            }
        }
        elseif($type == "approval")
        {
            $check_sql   = "SELECT arp.ar_no, arp.arp_type, arp.arp_state, arp.priority FROM approval_report_permission arp LEFT JOIN approval_report ar ON ar.ar_no=arp.ar_no WHERE ar.ar_state='2' AND arp.arp_type IN('approval','conference') AND arp.arp_s_no='{$req_s_no}' AND arp.arp_state IN('1','2','3')";
            $check_query = mysqli_query($this->my_db, $check_sql);
            $check_list  = [];

            while ($check = mysqli_fetch_assoc($check_query))
            {
                if($check['arp_state'] == '2'){
                    $check_list[] = $check['ar_no'];
                }else{
                    $priority_sql    = "SELECT count(arp.arp_no) as cnt FROM approval_report_permission arp WHERE arp.ar_no='{$check['ar_no']}' AND arp.arp_type IN('approval','conference') AND arp.arp_state='1' AND arp.priority < '{$check['priority']}'";
                    $priority_query  = mysqli_query($this->my_db, $priority_sql);
                    $priority_result = mysqli_fetch_assoc($priority_query);

                    if($priority_result['cnt'] > 0){
                        $check_list[] = $check['ar_no'];
                    }
                }
            }
        }

        if(!empty($check_list)){
            $result = implode(",", $check_list);
        }

        return $result;
    }

    public function changeArState($ar_no, $req_s_no, $team)
    {
        $team_parent_list  = $this->getTeamParent($req_s_no);
        $team_parent_where = "'{$team}'";
        if(!empty($team_parent_list)){
            $team_parent_where = implode(',', $team_parent_list);
        }

        $check_sql   = "SELECT arp.ar_no, arp.arp_type, arp.arp_state, arp.priority FROM approval_report_permission arp WHERE arp.ar_no='{$ar_no}' AND arp.arp_state='1'
                                                                                                  AND (arp.arp_s_no='{$req_s_no}' OR (arp.arp_team IN({$team_parent_where}) AND arp.arp_s_no < 1)) ORDER BY priority";
        $check_query = mysqli_query($this->my_db, $check_sql);
        $chk_ar_state = "2_3";
        while($check_result = mysqli_fetch_assoc($check_query))
        {
            if($check_result['arp_type'] == 'approval')
            {
                $priority_sql    = "SELECT count(arp.arp_no) as cnt FROM approval_report_permission arp WHERE arp.ar_no='{$check_result['ar_no']}' 
                                                                      AND arp.arp_type IN('approval','conference') AND arp.arp_state='1' AND arp.priority < '{$check_result['priority']}'";
                $priority_query  = mysqli_query($this->my_db, $priority_sql);
                $priority_result = mysqli_fetch_assoc($priority_query);

                if(isset($priority_result['cnt']) && $priority_result['cnt'] == 0){
                    $chk_ar_state = "2_1";
                    break;
                }
            }
            elseif($check_result['arp_type'] == 'conference')
            {
                $priority_sql    = "SELECT count(arp.arp_no) as cnt FROM approval_report_permission arp WHERE arp.ar_no='{$check_result['ar_no']}' 
                                                                      AND arp.arp_type IN('approval','conference') AND arp.arp_state='1' AND arp.priority < '{$check_result['priority']}'";
                $priority_query  = mysqli_query($this->my_db, $priority_sql);
                $priority_result = mysqli_fetch_assoc($priority_query);

                if(isset($priority_result['cnt']) && $priority_result['cnt'] == 0){
                    $chk_ar_state = "2_2";
                    break;
                }
            }
        }

        return $chk_ar_state;
    }

    public function getTeamParent($s_no)
    {
        $staff_sql    = "SELECT team_list FROM staff WHERE s_no='{$s_no}'";
        $staff_query  = mysqli_query($this->my_db, $staff_sql);
        $staff_result = mysqli_fetch_assoc($staff_query);

        $team_list     = isset($staff_result['team_list']) ? $staff_result['team_list'] : "";
        $team_chk_list = [];
        if(!empty($team_list))
        {
            $team_chk_list_val = "";

            $team_chk_sql    = "SELECT team_code_parent, (SELECT max(sub_t.depth) FROM team sub_t WHERE sub_t.team_group=t.team_group LIMIT 1) as max_depth FROM team t WHERE team_code IN({$team_list})";
            $team_chk_query  = mysqli_query($this->my_db, $team_chk_sql);
            $team_chk_list   = explode(',', $team_list);
            while($team_chk_result = mysqli_fetch_assoc($team_chk_query))
            {
                $team_parent        = $team_chk_result['team_code_parent'];
                $team_chk_max_depth = $team_chk_result['max_depth'];

                if(!empty($team_parent)){
                    $team_chk_list[] = $team_parent;
                }

                for($i=0;$i<$team_chk_max_depth;$i++)
                {
                    if(empty($team_parent)){
                        break;
                    }

                    $chk_sql    = "SELECT team_code_parent FROM team WHERE team_code='{$team_parent}'";
                    $chk_query  = mysqli_query($this->my_db, $chk_sql);
                    $chk_result = mysqli_fetch_assoc($chk_query);

                    $team_parent = isset($chk_result['team_code_parent']) ? $chk_result['team_code_parent'] : "";

                    if(!empty($team_parent)){
                        $team_chk_list[] = $team_parent;
                    }
                }
            };

            if(!empty($team_chk_list)){
                $team_chk_list = array_unique($team_chk_list);
            }
        }

        return $team_chk_list;
    }

    public function approvalAllConfirm($w_no, $price)
    {
        $result = false;

        $work_sql    = "SELECT (SELECT p.af_price FROM product p WHERE p.prd_no=w.prd_no) as af_price FROM `work` w WHERE w_no='{$w_no}'";
        $work_query  = mysqli_query($this->my_db, $work_sql);
        $work_result = mysqli_fetch_assoc($work_query);

        if($work_result['af_price'] > 0 && $price > 0 && ($work_result['af_price'] > $price)){
            $result = true;
        }

        return $result;
    }

    public function checkApprovalModify($ar_no, $req_s_no)
    {
        $result = "NOT";

        $check_sql    = "SELECT arp.ar_no, arp.arp_type, arp.arp_state, arp.priority FROM approval_report_permission arp LEFT JOIN approval_report ar ON ar.ar_no=arp.ar_no WHERE ar.ar_state='2' AND arp.ar_no='{$ar_no}' AND arp.arp_type IN('approval','conference') AND arp.arp_s_no='{$req_s_no}'";
        $check_query  = mysqli_query($this->my_db, $check_sql);
        $check_result = mysqli_fetch_assoc($check_query);

        if(isset($check_result['ar_no']) && !empty($check_result['ar_no']))
        {
            if($check_result['arp_state'] == '2')
            {
                $priority_sql    = "SELECT count(arp.arp_no) as cnt FROM approval_report_permission arp WHERE arp.ar_no='{$check_result['ar_no']}' AND arp.arp_type IN('approval','conference') AND arp.arp_state='2' AND arp.priority > '{$check_result['priority']}'";
                $priority_query  = mysqli_query($this->my_db, $priority_sql);
                $priority_result = mysqli_fetch_assoc($priority_query);

                if($priority_result['cnt'] == 0){
                    $result = "PRE";
                }
            }
            else
            {
                $priority_sql    = "SELECT count(arp.arp_no) as cnt FROM approval_report_permission arp WHERE arp.ar_no='{$check_result['ar_no']}' AND arp.arp_type IN('approval','conference') AND arp.arp_state='1' AND arp.priority < '{$check_result['priority']}'";
                $priority_query  = mysqli_query($this->my_db, $priority_sql);
                $priority_result = mysqli_fetch_assoc($priority_query);

                if($priority_result['cnt'] == 0){
                    $result = "NOW";
                }
            }
        }

        return $result;
    }

    public function checkApprovalMe($ar_no, $req_s_no)
    {
        $appr_me_sql    = "SELECT arp.arp_no FROM approval_report_permission arp WHERE arp.ar_no='{$ar_no}' AND arp.arp_type IN('approval','conference') AND arp.arp_s_no='{$req_s_no}' AND arp.arp_state != '0'";
        $appr_me_query  = mysqli_query($this->my_db, $appr_me_sql);
        $appr_me_result = mysqli_fetch_assoc($appr_me_query);
        $appr_me        = isset($appr_me_result['arp_no']) ? $appr_me_result['arp_no'] : "";

        return $appr_me;
    }

    public function checkApprovalFinal($ar_no, $arp_no)
    {
        $result = false;

        $appr_final_sql   = "SELECT arp.arp_no FROM approval_report_permission arp WHERE arp.arp_state='1' AND arp.ar_no='{$ar_no}' AND arp.arp_type IN('approval','conference') ORDER BY priority DESC LIMIT 1";
        $appr_final_query = mysqli_query($this->my_db, $appr_final_sql);
        $appr_final       = mysqli_fetch_assoc($appr_final_query);

        if(isset($appr_final['arp_no']) && $appr_final['arp_no'] == $arp_no)
        {
            $result = true;
        }

        return $result;
    }
}
