<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Advertising extends Model
{
    protected $_main_table = "advertising_management";
    protected $_main_key   = "am_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Advertising();
    }

    public function setResultTable()
    {
        $this->_main_table  = "advertising_result";
        $this->_main_key    = "ar_no";
    }

    public function setEventTable()
    {
        $this->_main_table  = "advertising_event";
        $this->_main_key    = "ae_no";
    }

    public function getAdvertisingItem($main_val)
    {
        $item_sql = "
            SELECT 
                *,  
                DATE_FORMAT(`am`.adv_s_date, '%Y-%m-%d') as adv_s_day,
                DATE_FORMAT(`am`.adv_s_date, '%H:%i') as adv_s_time,
                DATE_FORMAT(`am`.adv_e_date, '%Y-%m-%d') as adv_e_day,
                DATE_FORMAT(`am`.adv_e_date, '%H:%i') as adv_e_time
            FROM advertising_management as `am` WHERE `am_no`='{$main_val}'
        ";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function checkApplication($am_no, $s_no)
    {
        $chk_sql    = "SELECT COUNT(aa_no) as cnt FROM advertising_application WHERE am_no='{$am_no}' AND s_no='{$s_no}'";
        $chk_query  = mysqli_query($this->my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        return $chk_result['cnt'] > 0 ? true : false;
    }

    public function checkEvent($am_no)
    {
        $chk_sql    = "SELECT COUNT(ae_no) as cnt FROM advertising_event WHERE am_no='{$am_no}'";
        $chk_query  = mysqli_query($this->my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        return $chk_result['cnt'] > 0 ? true : false;
    }

    public function checkEventSms($ae_no)
    {
        $chk_sql    = "SELECT COUNT(aes_no) as cnt FROM advertising_event_sms WHERE ae_no='{$ae_no}'";
        $chk_query  = mysqli_query($this->my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        return $chk_result['cnt'] > 0 ? true : false;
    }

    public function confirmEventSms($ae_no)
    {
        $chk_sql    = "SELECT COUNT(aes_no) as cnt FROM advertising_event_sms WHERE ae_no='{$ae_no}' AND sms_state='3'";
        $chk_query  = mysqli_query($this->my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        return $chk_result['cnt'] > 0 ? true : false;
    }
}
