<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class CommerceOrder extends Model
{
    protected $_main_table  = "commerce_order_set";
    protected $_sub_table   = "commerce_order";
    protected $_group_table = "commerce_order_group";
    protected $_etc_table   = "commerce_order_etc";
    protected $_main_key    = "no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new CommerceOrder();
    }

    public function getOrderItem($no)
    {
        $commerce_order_sql 	= "
            SELECT 
               *, 
               (SELECT cos.sup_loc_type FROM commerce_order_set cos WHERE cos.`no`=co.`set_no`) as sup_loc_type 
            FROM {$this->_sub_table} co 
            WHERE co.`no`='{$no}'
        ";
        $commerce_order_query   = mysqli_query($this->my_db, $commerce_order_sql);
        $commerce_order         = mysqli_fetch_assoc($commerce_order_query);

        return $commerce_order;
    }

    public function getCommerceOrderSetUnit($set_no)
    {
        $product_cms_unit_sql="
            SELECT
                pcu.`no`,
                pcu.option_name,
                pcu.sup_price,
                pcu.sup_price_vat
            FROM product_cms_unit pcu
            WHERE pcu.display='1' AND pcu.`no` IN(SELECT sub.option_no FROM {$this->_sub_table} sub WHERE sub.`set_no`='{$set_no}')
            ORDER BY pcu.priority ASC
        ";

        $product_cms_unit_query = mysqli_query($this->my_db, $product_cms_unit_sql);
        $product_cms_unit_list  = [];
        while($product_cms_unit_array = mysqli_fetch_array($product_cms_unit_query)){
            $product_cms_unit_list[] = array(
                "no"			=> $product_cms_unit_array['no'],
                "sup_c_no"		=> $product_cms_unit_array['sup_c_no'],
                "option_name"	=> $product_cms_unit_array['option_name'],
                "sup_price"		=> $product_cms_unit_array['sup_price'],
                "sup_price_vat"	=> $product_cms_unit_array['sup_price_vat']
            );
        }

        return $product_cms_unit_list;
    }

    public function getMaxGroup($set_no)
    {
        $group_sql        = "SELECT MAX(`group`) as group_no FROM {$this->_sub_table} WHERE `type`='supply' AND `set_no`='{$set_no}'";
        $group_query      = mysqli_query($this->my_db, $group_sql);
        $group_result     = mysqli_fetch_assoc($group_query);

        return ($group_result['group_no'] > 0) ? $group_result['group_no']+1 : "1";
    }

    public function updatePriority($no, $set_no, $priority)
    {
        $upd_data   = [];
        $upd_data[] = array("no" => $no, "priority" => $priority);

        $f_priority_new 	= $priority;
        $chk_priority_sql 	= "SELECT * FROM {$this->_sub_table} WHERE set_no='{$set_no}' AND `no` !='{$no}' AND priority >= {$priority} AND `type`='request' ORDER BY priority ASC, `no` ASC";
        $chk_priority_query = mysqli_query($this->my_db, $chk_priority_sql);
        while($chk_priority = mysqli_fetch_assoc($chk_priority_query))
        {
            $f_priority_new++;
            $upd_data[] = array("no" => $chk_priority['no'], "priority" => $f_priority_new);
        }

        if($this->multiUpdate($upd_data)){
            return true;
        }else{
            return false;
        }
    }

    public function deleteCommercePriority($set_no, $priority)
    {
        $upd_data           = [];
        $f_priority_new 	= $priority;
        $chk_priority_sql 	= "SELECT * FROM {$this->_sub_table} WHERE set_no='{$set_no}' AND priority >= {$priority} AND `type`='request' ORDER BY priority ASC, `no` ASC";
        $chk_priority_query = mysqli_query($this->my_db, $chk_priority_sql);
        while($chk_priority = mysqli_fetch_assoc($chk_priority_query))
        {
            $upd_data[] = array("no" => $chk_priority['no'], "priority" => $f_priority_new);
            $f_priority_new++;
        }

        if($this->multiUpdate($upd_data)){
            return true;
        }else{
            return false;
        }
    }

    public function getMaxOrderCount($sup_c_no, $req_date)
    {
        $chk_req_year = date("Y", strtotime($req_date));
        $chk_req_s_date = $chk_req_year . "-01-01";
        $chk_req_e_date = $chk_req_year . "-12-31";

        $order_cnt_sql = "SELECT MAX(cos.order_count) as ord_cnt FROM commerce_order_set cos WHERE cos.sup_c_no = '{$sup_c_no}' AND cos.display='1' AND cos.req_date BETWEEN '{$chk_req_s_date}' AND '{$chk_req_e_date}'";
        $order_cnt_query = mysqli_query($this->my_db, $order_cnt_sql);
        $order_cnt_result = mysqli_fetch_assoc($order_cnt_query);

        return isset($order_cnt_result['ord_cnt']) && !empty($order_cnt_result['ord_cnt']) ? $order_cnt_result['ord_cnt'] + 1 : 1;
    }

    public function getChildCount($set_no)
    {
        $childe_cnt_sql      = "SELECT COUNT(*) as cnt FROM commerce_order co WHERE co.set_no = '{$set_no}' AND co.type='request'";
        $childe_cnt_query    = mysqli_query($this->my_db, $childe_cnt_sql);
        $childe_cnt_result   = mysqli_fetch_assoc($childe_cnt_query);

        return isset($childe_cnt_result['cnt']) && !empty($childe_cnt_result['cnt']) ? $childe_cnt_result['cnt'] : 0;
    }
}
