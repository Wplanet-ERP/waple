<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Agency extends Model
{
    protected $_main_table = "agency_settlement";
    protected $_main_key   = "as_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Agency();
    }

    public function getLastGroupNo($settle_month)
    {
        $agency_month   = str_replace('-', '', $settle_month);
        $agency_group   = "{$agency_month}_agency_";

        $agency_sql     = "SELECT REPLACE(group_no, '{$agency_group}', '') as group_no FROM {$this->_main_table} WHERE `group_no` LIKE '%{$agency_group}%' ORDER BY group_no DESC LIMIT 1";
        $agency_query   = mysqli_query($this->my_db, $agency_sql);
        $agency_result  = mysqli_fetch_assoc($agency_query);

        $last_group_no  = (isset($agency_result['group_no']) && !empty($agency_result['group_no'])) ? (int)$agency_result['group_no']+1 : 1;

        return $last_group_no;
    }

    public function setChargeTable() {
        $this->_main_table  = "agency_charge";
        $this->_main_key    = "ac_no";
    }

    public function getChargeItem($ac_no)
    {
        $charge_sql = "
            SELECT
                *,
                (SELECT s.s_name FROM staff s WHERE s.s_no=`ac`.manager) AS s_name,         
                (SELECT t.team_name FROM team t WHERE t.team_code=`ac`.manager_team) AS t_name
            FROM agency_charge AS `ac`
            WHERE ac_no='{$ac_no}'
        ";
        $charge_query = mysqli_query($this->my_db, $charge_sql);
        $charge_item  = mysqli_fetch_assoc($charge_query);

        return $charge_item;
    }

    public function updateChargePrice($upd_data)
    {
        $this->update($upd_data);

        $charge_item        = $this->getChargeItem($upd_data['ac_no']);
        $check_charge_sql   = "SELECT ac_no, base_mon FROM agency_charge WHERE base_mon > '{$charge_item['base_mon']}' AND partner ='{$charge_item['partner']}' AND agency='{$charge_item['agency']}' AND media='{$charge_item['media']}'";
        $check_charge_query = mysqli_query($this->my_db, $check_charge_sql);
        while($check_charge = mysqli_fetch_assoc($check_charge_query))
        {
            $charge_month   = $check_charge['base_mon'];
            $prev_month     = date("Y-m", strtotime("{$check_charge['base_mon']} -1 months"));
            $charge_sql     = "
                SELECT 
                    `ac`.ac_no,
                    `ac`.ad_charge_price,
                    (SELECT SUM(wd.wd_money) AS total FROM withdraw wd LEFT JOIN `work` w ON w.w_no=wd.w_no WHERE wd.wd_state='3' AND wd.w_no > 0 AND w.prd_no='12' AND wd.c_no='814' AND DATE_FORMAT(wd.wd_date, '%Y-%m')='{$charge_month}' AND w.c_no=`ac`.partner) as charge_price,
                    (SELECT sub.total_price FROM agency_charge sub WHERE sub.base_mon='{$prev_month}' AND `sub`.agency=`ac`.agency AND `sub`.media=`ac`.media AND `sub`.partner=`ac`.partner) as prev_total
                FROM agency_charge `ac`
                WHERE ac_no='{$check_charge['ac_no']}'
            ";
            $charge_query   = mysqli_query($this->my_db, $charge_sql);
            $charge_result  = mysqli_fetch_assoc($charge_query);
            $total_price    = $charge_result['prev_total']+(int)$charge_result['charge_price']-(int)$charge_result['ad_total_price']+(int)$charge_result['ad_charge_price'];
            $charge_data = array(
                "ac_no"          => $charge_result['ac_no'],
                "charge_price"   => (int)$charge_result['charge_price'],
                "prev_price"     => $charge_result['prev_total'],
                "total_price"    => $total_price,
            );

            $this->update($charge_data);
        }

        return true;
    }

    function getPartnerList()
    {
        $as_partner_sql     = "SELECT DISTINCT partner, partner_name FROM agency_settlement WHERE partner > 0 ORDER BY partner_name";
        $as_partner_query   = mysqli_query($this->my_db, $as_partner_sql);
        $as_partner_list    = [];
        while($as_partner = mysqli_fetch_assoc($as_partner_query))
        {
            $as_partner_list[$as_partner['partner']] = $as_partner['partner_name'];
        }

        return $as_partner_list;
    }
}
