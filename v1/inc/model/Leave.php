<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Leave extends Model
{
    protected $_main_table  = "leave_management";
    protected $_main_key    = "lm_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Leave();
    }

    public function setStaffLeaveTable()
    {
        $this->_main_table  = "leave_staff_management";
        $this->_main_key    = "lsm_no";
    }

    public function setCalendarTable()
    {
        $this->_main_table  = "leave_calendar";
        $this->_main_key    = "lc_no";
    }

    public function getHolidayList()
    {
        $holiday_sql 	= "SELECT * FROM holiday_info ORDER BY h_date ASC";
        $holiday_query 	= mysqli_query($this->my_db, $holiday_sql);
        $holiday_list	= [];
        while($holiday = mysqli_fetch_assoc($holiday_query)){
            $holiday_list[$holiday['h_date']] = $holiday['h_name'];
        }

        return $holiday_list;
    }

    public function getDefaultLeaveData($lm_no, $s_no)
    {
        $staff_leave_sql    = "SELECT * FROM leave_staff_management WHERE lm_no='{$lm_no}' AND s_no='{$s_no}' AND active='1'";
        $staff_leave_query  = mysqli_query($this->my_db, $staff_leave_sql);
        $staff_leave_result = mysqli_fetch_assoc($staff_leave_query);

        return $staff_leave_result;
    }

    public function getCheckLeaveDay($ar_no, $s_no, $s_date, $e_date)
    {
        $chk_dup_sql = "
            SELECT 
               COUNT(*) as cnt
            FROM approval_report_leave AS arl
            LEFT JOIN approval_report AS ar ON ar.ar_no=arl.ar_no
            WHERE arl.s_no='{$s_no}' AND ar.ar_state IN(2,3) AND arl.ar_no != '{$ar_no}' AND (
                (leave_s_date BETWEEN '{$s_date}' AND '{$e_date}') OR 
                (leave_e_date BETWEEN '{$s_date}' AND '{$e_date}') OR
                ('{$s_date}' BETWEEN  leave_s_date AND leave_e_date) OR
                ('{$e_date}' BETWEEN  leave_s_date AND leave_e_date)
            )
        ";
        $chk_dup_query  = mysqli_query($this->my_db, $chk_dup_sql);
        $chk_dup_result = mysqli_fetch_assoc($chk_dup_query);

        return $chk_dup_result['cnt'] > 0 ? true : false;
    }

    public function useLeaveDay($lsm_no, $leave_value)
    {
        $staff_leave_sql    = "SELECT * FROM leave_staff_management WHERE lsm_no='{$lsm_no}'";
        $staff_leave_query  = mysqli_query($this->my_db, $staff_leave_sql);
        $staff_leave_result = mysqli_fetch_assoc($staff_leave_query);

        $staff_leave_item   = !empty($staff_leave_result) ? $staff_leave_result : [];
        if(!empty($staff_leave_result)){
            $cal_use_day    = $staff_leave_item['use_day']+$leave_value;
            $upd_use_sql    = "UPDATE leave_staff_management SET use_day='{$cal_use_day}' WHERE lsm_no='{$lsm_no}'";
            mysqli_query($this->my_db, $upd_use_sql);
        }
    }

    public function getCalendarItem($lc_no)
    {
        $calendar_sql       = "SELECT lc.*, (SELECT lm.is_deducted FROM leave_management AS lm WHERE lm.lm_no=lsm.lm_no) as is_deducted FROM leave_calendar AS lc LEFT JOIN leave_staff_management AS lsm ON lc.lsm_no=lsm.lsm_no WHERE lc_no='{$lc_no}'";
        $calendar_query     = mysqli_query($this->my_db, $calendar_sql);
        $calendar_result    = mysqli_fetch_assoc($calendar_query);

        return $calendar_result;
    }
}

