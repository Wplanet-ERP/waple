<?php
ini_set("display_errors", -1);

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Evaluation extends Model
{
    protected $_main_table  = "evaluation_system";
    protected $_main_key    = "ev_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Evaluation();
    }

    public function getEvaluationSetList($active)
    {
        $add_where      = "1=1";
        if(!empty($active)) {
            $add_where .= " AND active='{$active}'";
        }

        $ev_set_sql     = "SELECT ev_u_set_no, subject FROM evaluation_unit_set WHERE {$add_where}";
        $ev_set_query   = mysqli_query($this->my_db, $ev_set_sql);
        $ev_set_list    = [];
        while($ev_set = mysqli_fetch_assoc($ev_set_query))
        {
            $ev_set_list[$ev_set['ev_u_set_no']] = $ev_set['subject'];
        }

        return $ev_set_list;
    }

    public function getEvManagerList($ev_no)
    {
        $ev_manager_sql     = "SELECT DISTINCT er.manager, (SELECT s.s_name FROM staff s WHERE s.s_no=er.manager) as manager_name FROM evaluation_relation er WHERE er.ev_no ='{$ev_no}' GROUP BY manager";
        $ev_manager_query   = mysqli_query($this->my_db, $ev_manager_sql);
        $ev_manager_list    = [];
        while($ev_manager = mysqli_fetch_assoc($ev_manager_query))
        {
            $ev_manager_list[$ev_manager['manager']] = $ev_manager['manager_name'];
        }

        return $ev_manager_list;
    }

    public function getRecManagerList($ev_no)
    {
        $ev_staff_sql     = "SELECT receiver_s_no, manager FROM evaluation_relation er WHERE ev_no='{$ev_no}' GROUP BY receiver_s_no";
        $ev_staff_query   = mysqli_query($this->my_db, $ev_staff_sql);
        $ev_staff_list    = [];
        while($ev_staff = mysqli_fetch_assoc($ev_staff_query))
        {
            $ev_staff_list[$ev_staff['receiver_s_no']] = $ev_staff['manager'];
        }

        return $ev_staff_list;
    }

    public function getRecStaffList($ev_no)
    {
        $ev_staff_sql     = "SELECT s.s_no, s.s_name FROM staff s WHERE s.s_no NOT IN(SELECT DISTINCT receiver_s_no FROM evaluation_relation WHERE ev_no='{$ev_no}') AND s.staff_state = '1' ORDER BY s_no ASC";
        $ev_staff_query   = mysqli_query($this->my_db, $ev_staff_sql);
        $ev_staff_list    = [];
        while($ev_staff = mysqli_fetch_assoc($ev_staff_query))
        {
            $ev_staff_list[$ev_staff['s_no']] = $ev_staff['s_name'];
        }

        return $ev_staff_list;
    }

    public function getEvaluationSystemListByStaff($s_no)
    {
        $cur_date = date("Y-m-d");
        if($s_no != "1" && $s_no != '28'){
            $ev_no_sql          = "SELECT ev_no FROM evaluation_system WHERE admin_s_no='{$s_no}'";
            $ev_relation_sql    = "
                SELECT 
                    ev_no 
                FROM evaluation_system 
                WHERE ev_no IN(SELECT distinct ev_no FROM evaluation_relation WHERE evaluator_s_no = {$s_no})
                AND (
                    ((DATE_FORMAT(mod_s_date,'%Y-%m-%d') <= '{$cur_date}' AND DATE_FORMAT(mod_s_date,'%Y-%m-%d') >= '{$cur_date}') OR (DATE_FORMAT(mod_e_date,'%Y-%m-%d') >= '{$cur_date}' AND DATE_FORMAT(mod_e_date,'%Y-%m-%d') >= '{$cur_date}'))
                    OR ((DATE_FORMAT(chk_s_date,'%Y-%m-%d') <= '{$cur_date}' AND DATE_FORMAT(chk_s_date,'%Y-%m-%d') >= '{$cur_date}') OR (DATE_FORMAT(chk_e_date,'%Y-%m-%d') >= '{$cur_date}' AND DATE_FORMAT(chk_e_date,'%Y-%m-%d') >= '{$cur_date}'))
                    OR ((DATE_FORMAT(ev_s_date,'%Y-%m-%d') <= '{$cur_date}' AND DATE_FORMAT(ev_s_date,'%Y-%m-%d') >= '{$cur_date}') OR (DATE_FORMAT(ev_e_date,'%Y-%m-%d') >= '{$cur_date}' AND DATE_FORMAT(ev_e_date,'%Y-%m-%d') >= '{$cur_date}'))
                )
            ";
        }else{
            $ev_no_sql  = "SELECT * FROM evaluation_system";
        }

        $ev_no_query    = mysqli_query($this->my_db, $ev_no_sql);
        $ev_no_list     = [];
        $ev_list        = [];
        while($ev_no_result = mysqli_fetch_assoc($ev_no_query))
        {
            $ev_no_list[$ev_no_result['ev_no']] = $ev_no_result['ev_no'];
        }

        if(!empty($ev_relation_sql)){
            $ev_relation_query = mysqli_query($this->my_db, $ev_relation_sql);
            while($ev_relation = mysqli_fetch_assoc($ev_relation_query))
            {
                $ev_no_list[$ev_relation['ev_no']] = $ev_relation['ev_no'];
            }
        }

        if(!empty($ev_no_list)){
            $ev_nos     = implode(',', $ev_no_list);
            $ev_sql     = "SELECT * FROM evaluation_system WHERE ev_no IN({$ev_nos})";
            $ev_query   = mysqli_query($this->my_db, $ev_sql);
            while($ev_result = mysqli_fetch_assoc($ev_query)){
                $ev_list[$ev_result['ev_no']] = $ev_result['subject'];
            }
        }

        return $ev_list;
    }

    public function getEvaluationSystemCompleteListByStaff($s_no)
    {
        $add_where = " AND (subject NOT LIKE '%test%' AND subject NOT LIKE '%테스트%' AND subject NOT LIKE '%복제%')";
        if($s_no != "1" && $s_no != '28'){
            $add_where = " AND admin_s_no='{$s_no}'";
        }

        $ev_sql     = "SELECT ev_no, subject FROM evaluation_system WHERE ev_state IN(2,3,4) {$add_where} ORDER BY is_multi_rater DESC, ev_no DESC";
        $ev_query   = mysqli_query($this->my_db, $ev_sql);
        $ev_list    = [];
        while($ev_system = mysqli_fetch_assoc($ev_query))
        {
            $ev_list[$ev_system['ev_no']] = $ev_system['subject'];
        }

        return $ev_list;
    }

    public function getEvSystemMultiRaterList($s_no)
    {
        $add_where = " AND (subject NOT LIKE '%test%' AND subject NOT LIKE '%테스트%' AND subject NOT LIKE '%복제%')";
        if($s_no != "1" && $s_no != '28'){
            $add_where = " AND admin_s_no='{$s_no}'";
        }

        $ev_sql     = "SELECT ev_no, subject FROM evaluation_system WHERE ev_state IN(2,3,4) AND is_multi_rater='1' {$add_where} ORDER BY is_multi_rater DESC, ev_no DESC";
        $ev_query   = mysqli_query($this->my_db, $ev_sql);
        $ev_list    = [];
        while($ev_system = mysqli_fetch_assoc($ev_query))
        {
            $ev_list[$ev_system['ev_no']] = $ev_system['subject'];
        }

        return $ev_list;
    }

    public function getStaffEvMultiRaterList()
    {
        $add_where = " AND (subject NOT LIKE '%test%' AND subject NOT LIKE '%테스트%' AND subject NOT LIKE '%복제%')";
        $ev_sql     = "SELECT ev_no, subject FROM evaluation_system WHERE ev_state IN(4) AND is_multi_rater='1' {$add_where} ORDER BY is_multi_rater DESC, ev_no DESC";
        $ev_query   = mysqli_query($this->my_db, $ev_sql);
        $ev_list    = [];
        while($ev_system = mysqli_fetch_assoc($ev_query))
        {
            $ev_list[$ev_system['ev_no']] = $ev_system['subject'];
        }

        return $ev_list;
    }

    public function getEvalQuestionList()
    {
        $ev_unit_sql            = "SELECT ev_u_no, ev_u_set_no, question FROM evaluation_unit WHERE evaluation_state='1' ORDER BY ev_u_set_no ASC, `order` ASC";
        $ev_unit_query          = mysqli_query($this->my_db, $ev_unit_sql);
        $ev_unit_question_list  = [];
        while($ev_unit = mysqli_fetch_assoc($ev_unit_query))
        {
            if($ev_unit['ev_u_set_no'] != 9){
                $question_exp = isset($ev_unit['question'])? explode('.', $ev_unit['question']) : "";
                $question     = isset($question_exp[1]) ? $question_exp[1] : (isset($question_exp[1]) ? $question_exp[1] : "");
            }else{
                $question = $ev_unit['question'];
            }

            $question     = trim($question);

            if($question == "업무처리 결과(업무의 질/문제해결) 만족도"){
                $question = '산출물 만족도';
            }elseif($question == "업무관련 의사소통 만족도"){
                $question = '의사소통 만족도';
            }

            $ev_unit_question_list[$ev_unit['ev_u_no']] = trim($question);
        }

        return $ev_unit_question_list;
    }

    public function duplicate($ev_no, $s_no)
    {
        $dup_sql = "
            INSERT IGNORE INTO evaluation_system (subject, ev_state, description, ev_u_set_no, admin_s_no)
            (SELECT CONCAT(subject,'_복제'), '1', description, ev_u_set_no, '{$s_no}' FROM evaluation_system WHERE ev_no = '{$ev_no}')
        ";

        if(mysqli_query($this->my_db, $dup_sql)){
            return true;
        }else{
            return false;
        }
    }

    public function duplicateRelation($ev_no, $new_ev_no)
    {
        $dup_sql = "
            INSERT IGNORE INTO evaluation_relation (ev_no, manager, receiver_s_no, receiver_team, evaluator_s_no, evaluator_team, ev_u_set_no, group_rate, rate)
            (SELECT '{$new_ev_no}', manager, receiver_s_no, receiver_team, evaluator_s_no, evaluator_team, ev_u_set_no, group_rate, rate FROM evaluation_relation WHERE ev_no = '{$ev_no}')
        ";

        if(mysqli_query($this->my_db, $dup_sql)){
            return true;
        }else{
            return false;
        }
    }

    public function updateReceiver($ev_no, $ev_r_no, $prev_receiver, $new_receiver, $new_receiver_team)
    {
        $upd_data   = [];

        $ev_relation_sql      = "SELECT * FROM evaluation_relation WHERE ev_r_no='{$ev_r_no}'";
        $ev_relation_query    = mysqli_query($this->my_db, $ev_relation_sql);
        $ev_relation_result   = mysqli_fetch_assoc($ev_relation_query);
        $ev_u_set_no          = $ev_relation_result['ev_u_set_no'];

        # 핵심가치 일 경우
        if($ev_u_set_no == '9')
        {
            if(!empty($prev_receiver))
            {
                $chk_relation_sql   = "SELECT * FROM evaluation_relation WHERE ev_no='{$ev_no}' AND receiver_s_no='{$prev_receiver}' AND receiver_s_no=evaluator_s_no";
                $chk_relation_query = mysqli_query($this->my_db, $chk_relation_sql);
                $chk_relation       = mysqli_fetch_assoc($chk_relation_query);

                $upd_data[] = array(
                    "ev_r_no"           => $chk_relation['ev_r_no'],
                    "receiver_s_no"     => $new_receiver,
                    "receiver_team"     => $new_receiver_team,
                    "evaluator_s_no"    => $new_receiver,
                    "evaluator_team"    => $new_receiver_team,
                );
            }else{
                $upd_data[] = array(
                    "ev_r_no"           => $ev_r_no,
                    "receiver_s_no"     => $new_receiver,
                    "receiver_team"     => $new_receiver_team,
                    "evaluator_s_no"    => $new_receiver,
                    "evaluator_team"    => $new_receiver_team,
                );
            }
        }
        else
        {
            $upd_data[] = array(
                "ev_r_no"           => $ev_r_no,
                "receiver_s_no"     => $new_receiver,
                "receiver_team"     => $new_receiver_team
            );
        }


        if($this->multiUpdate($upd_data)){
            return true;
        }else{
            return false;
        }
    }

    public function checkEvaluator($ev_no, $new_evaluator)
    {
        $eval_relation_sql      = "SELECT COUNT(ev_r_no) as cnt FROM evaluation_relation WHERE ev_no='{$ev_no}' AND receiver_s_no='{$new_evaluator}'";
        $eval_relation_query    = mysqli_query($this->my_db, $eval_relation_sql);
        $eval_relation_result   = mysqli_fetch_assoc($eval_relation_query);

        return ($eval_relation_result['cnt'] > 0)  ? true : false;
    }

    public function updateEvaluator($ev_no, $ev_r_no, $new_evaluator, $s_no)
    {
        $upd_data   = [];
        $ins_data   = [];

        if(!empty($ev_r_no))
        {
            $rec_relation_sql       = "SELECT * FROM evaluation_relation WHERE ev_r_no='{$ev_r_no}' LIMIT 1";
            $rec_relation_query     = mysqli_query($this->my_db, $rec_relation_sql);
            $rec_relation_result    = mysqli_fetch_assoc($rec_relation_query);
            $ev_u_set_no            = $rec_relation_result['ev_u_set_no'];

            if($ev_u_set_no == '9')
            {
                $eval_relation_sql      = "SELECT * FROM evaluation_relation WHERE ev_no='{$ev_no}' AND receiver_s_no='{$new_evaluator}' AND evaluator_s_no='{$new_evaluator}'";
                $eval_relation_query    = mysqli_query($this->my_db, $eval_relation_sql);
                $eval_relation_result   = mysqli_fetch_assoc($eval_relation_query);

                if(isset($rec_relation_result['ev_r_no']) && $new_evaluator == '1')
                {
                    $upd_data = array(
                        "ev_r_no"           => $rec_relation_result['ev_r_no'],
                        "reg_s_no"          => $s_no,
                        "evaluator_s_no"    => $new_evaluator,
                        "evaluator_team"    => "00200",
                    );
                }
                elseif(isset($rec_relation_result['ev_r_no']) && $eval_relation_result['ev_r_no'])
                {
                    $chk_rec_relation_sql       = "SELECT COUNT(ev_r_no) as cnt FROM evaluation_relation WHERE ev_no='{$ev_no}' AND receiver_s_no='{$rec_relation_result['receiver_s_no']}' AND evaluator_s_no='{$new_evaluator}'";
                    $chk_rec_relation_query     = mysqli_query($this->my_db, $chk_rec_relation_sql);
                    $chk_rec_relation_result    = mysqli_fetch_assoc($chk_rec_relation_query);

                    if($chk_rec_relation_result['cnt'] == 0)
                    {
                        $upd_data = array(
                            "ev_r_no"           => $rec_relation_result['ev_r_no'],
                            "reg_s_no"          => $s_no,
                            "evaluator_s_no"    => $new_evaluator,
                            "evaluator_team"    => $eval_relation_result['evaluator_team'],
                        );
                    }

                    $chk_eval_relation_sql       = "SELECT COUNT(ev_r_no) as cnt FROM evaluation_relation WHERE ev_no='{$ev_no}' AND receiver_s_no='{$new_evaluator}' AND evaluator_s_no='{$rec_relation_result['receiver_s_no']}'";
                    $chk_eval_relation_query     = mysqli_query($this->my_db, $chk_eval_relation_sql);
                    $chk_eval_relation_result    = mysqli_fetch_assoc($chk_eval_relation_query);

                    if($chk_eval_relation_result['cnt'] == 0)
                    {
                        $ins_data = array(
                            "ev_no"             => $ev_no,
                            "manager"           => $eval_relation_result['manager'],
                            "reg_s_no"          => $s_no,
                            "receiver_s_no"     => $new_evaluator,
                            "receiver_team"     => $eval_relation_result['evaluator_team'],
                            "evaluator_s_no"    => $rec_relation_result['receiver_s_no'],
                            "evaluator_team"    => $rec_relation_result['receiver_team'],
                            "ev_u_set_no"       => $rec_relation_result['ev_u_set_no'],
                        );
                    }
                }
            }
            else
            {
                $eval_staff_sql     = "SELECT * FROM staff WHERE s_no='{$new_evaluator}'";
                $eval_staff_query   = mysqli_query($this->my_db, $eval_staff_sql);
                $eval_staff_result  = mysqli_fetch_assoc($eval_staff_query);

                $upd_data = array(
                    "ev_r_no"           => $rec_relation_result['ev_r_no'],
                    "reg_s_no"          => $s_no,
                    "rec_is_review"     => '1',
                    "eval_is_review"    => '1',
                    "evaluator_s_no"    => $new_evaluator,
                    "evaluator_team"    => $eval_staff_result['team'],
                );
            }
        }

        if($this->update($upd_data)){
            if(!empty($ins_data)){
                $this->insert($ins_data);
            }
            return true;
        }else{
            return false;
        }
    }

    public function updateGroupRate($ev_no, $ev_r_no, $receiver, $group_rate)
    {
        $upd_data   = [];

        if(!empty($group_rate))
        {
            $relation_sql       = "SELECT COUNT(ev_r_no) as cnt FROM evaluation_relation WHERE ev_no='{$ev_no}' AND rec_is_review='1' AND eval_is_review='1' 
                                                        AND receiver_s_no='{$receiver}' AND evaluator_s_no != '{$receiver}' AND group_rate = '{$group_rate}'";
            $relation_query     = mysqli_query($this->my_db, $relation_sql);
            $relation_result    = mysqli_fetch_assoc($relation_query);

            if($relation_result['cnt'] > 0)
            {
                $chk_relation_sql       = "SELECT ev_r_no FROM evaluation_relation WHERE ev_no='{$ev_no}' AND rec_is_review='1' AND eval_is_review='1' AND receiver_s_no='{$receiver}' AND evaluator_s_no != '{$receiver}' AND group_rate = '{$group_rate}'";
                $chk_relation_query     = mysqli_query($this->my_db, $chk_relation_sql);
                $chk_rate_cnt           = $relation_result['cnt']+1;
                $chk_rate               = $group_rate/$chk_rate_cnt;
                while($chk_relation_result = mysqli_fetch_assoc($chk_relation_query)){
                    $upd_data[] = array(
                        "ev_r_no"       => $chk_relation_result['ev_r_no'],
                        "group_rate"    => $group_rate,
                        "rate"          => $chk_rate,
                    );
                }

                $upd_data[] = array(
                    "ev_r_no"       => $ev_r_no,
                    "group_rate"    => $group_rate,
                    "rate"          => $chk_rate,
                );
            }else{
                $upd_data[] = array(
                    "ev_r_no"       => $ev_r_no,
                    "group_rate"    => $group_rate,
                    "rate"          => $group_rate,
                );
            }
        }

        if($this->multiUpdate($upd_data)){
            return true;
        }else{
            return false;
        }
    }

    public function addPrevEvaluationRelation($ev_no, $prev_ev_no)
    {
        $ins_data   = [];

        if(!empty($ev_no) && !empty($prev_ev_no))
        {
            $chk_already_sql        = "SELECT COUNT(ev_r_no) AS cnt FROM evaluation_relation WHERE ev_no='{$ev_no}' AND receiver_s_no != evaluator_s_no";
            $chk_already_query      = mysqli_query($this->my_db, $chk_already_sql);
            $chk_already_result     = mysqli_fetch_assoc($chk_already_query);

            if($chk_already_result['cnt'] > 0){
                return false;
            }

            $chk_cur_chk_sql        = "SELECT COUNT(ev_r_no) as cnt FROM evaluation_relation er WHERE ev_no='{$ev_no}' AND er.receiver_s_no != er.evaluator_s_no";
            $chk_cur_chk_query      = mysqli_query($this->my_db, $chk_cur_chk_sql);
            $chk_cur_chk_result     = mysqli_fetch_assoc($chk_cur_chk_query);

            if($chk_cur_chk_result['cnt'] > 0){
                return false;
            }

            $cur_ev_system_sql      = "SELECT * FROM evaluation_system WHERE ev_no='{$ev_no}'";
            $cur_ev_system_query    = mysqli_query($this->my_db, $cur_ev_system_sql);
            $cur_ev_system          = mysqli_fetch_assoc($cur_ev_system_query);

            $receiver_list          = [];
            $chk_cur_relation_sql   = "SELECT DISTINCT receiver_s_no, receiver_team, manager FROM evaluation_relation WHERE ev_no='{$ev_no}'";
            $chk_cur_relation_query = mysqli_query($this->my_db, $chk_cur_relation_sql);
            while($chk_cur_relation = mysqli_fetch_assoc($chk_cur_relation_query))
            {
                $receiver_list[$chk_cur_relation['receiver_s_no']] = $chk_cur_relation;
            }

            if(!empty($receiver_list))
            {
                $receiver_s_no_list = array_keys($receiver_list);
                $receiver_s_no_text = implode(",", $receiver_s_no_list);
                foreach($receiver_list as $rec_s_no => $rec_data)
                {
                    $prev_ev_relation_sql    = "
                        SELECT
                            (SELECT sub.manager FROM evaluation_relation sub WHERE sub.ev_no='{$ev_no}' AND sub.receiver_s_no=er.evaluator_s_no AND sub.receiver_s_no=sub.evaluator_s_no) as eval_manager,
                            evaluator_s_no,
                            (SELECT s.team FROM staff s WHERE s.s_no = er.evaluator_s_no) as eval_team,
                            (SELECT COUNT(sub.ev_r_no) FROM evaluation_relation sub WHERE sub.ev_no='{$ev_no}' AND sub.receiver_s_no={$rec_s_no} AND sub.evaluator_s_no=er.evaluator_s_no) as chk_rec,
                            (SELECT COUNT(sub.ev_r_no) FROM evaluation_relation sub WHERE sub.ev_no='{$ev_no}' AND sub.receiver_s_no=er.evaluator_s_no AND sub.evaluator_s_no='{$rec_s_no}') as chk_eval
                        FROM evaluation_relation er  
                        WHERE er.ev_no='{$prev_ev_no}' AND er.receiver_s_no='{$rec_s_no}' AND (rec_is_review = '1' AND eval_is_review = '1') AND evaluator_s_no IN({$receiver_s_no_text})
                    ";
                    $prev_ev_relation_query = mysqli_query($this->my_db, $prev_ev_relation_sql);
                    while($prev_ev_relation = mysqli_fetch_assoc($prev_ev_relation_query))
                    {
                        if($prev_ev_relation['receiver_s_no'] != $prev_ev_relation['evaluator_s_no'])
                        {
                            if(!isset($chk_cur_chk_list[$rec_s_no][$prev_ev_relation['evaluator_s_no']]) && $prev_ev_relation['chk_rec'] == 0)
                            {
                                $ins_data[] = array(
                                    "ev_no"             => $ev_no,
                                    "ev_u_set_no"       => $cur_ev_system['ev_u_set_no'],
                                    "manager"           => $rec_data['manager'],
                                    "receiver_s_no"     => $rec_s_no,
                                    "receiver_team"     => $rec_data['receiver_team'],
                                    "evaluator_s_no"    => $prev_ev_relation['evaluator_s_no'],
                                    "evaluator_team"    => $prev_ev_relation['eval_team']
                                );

                                $chk_cur_chk_list[$rec_s_no][$prev_ev_relation['evaluator_s_no']] = 1;
                            }

                            if(!isset($chk_cur_chk_list[$prev_ev_relation['evaluator_s_no']][$rec_s_no]) && $prev_ev_relation['chk_eval'] == 0){
                                $ins_data[] = array(
                                    "ev_no"             => $ev_no,
                                    "ev_u_set_no"       => $cur_ev_system['ev_u_set_no'],
                                    "manager"           => $prev_ev_relation['eval_manager'],
                                    "receiver_s_no"     => $prev_ev_relation['evaluator_s_no'],
                                    "receiver_team"     => $prev_ev_relation['eval_team'],
                                    "evaluator_s_no"    => $rec_s_no,
                                    "evaluator_team"    => $rec_data['receiver_team']
                                );

                                $chk_cur_chk_list[$prev_ev_relation['evaluator_s_no']][$rec_s_no] = 1;
                            }
                        }
                    }
                }
            }
        }

        if($this->multiInsert($ins_data)){
            return true;
        }else{
            return false;
        }
    }

    public function deleteEvaluationRelation($del_list)
    {
        $chk_del_list   = $del_list;
        $chk_result     = false;
        if(!empty($del_list))
        {
            foreach($del_list as $del_no)
            {
                $chk_item  = $this->getItem($del_no);
                $chk_sql 	= "SELECT ev_r_no FROM evaluation_relation WHERE ev_no='{$chk_item['ev_no']}' AND receiver_s_no='{$chk_item['evaluator_s_no']}' AND evaluator_s_no='{$chk_item['receiver_s_no']}'";
                $chk_query	= mysqli_query($this->my_db, $chk_sql);
                $chk_result = mysqli_fetch_assoc($chk_query);
                if(isset($chk_result['ev_r_no'])){
                    $chk_del_list[] = $chk_result['ev_r_no'];
                }
            }
        }

        if(!empty($chk_del_list)){
            $chk_del_text = implode(",", $chk_del_list);
            if(mysqli_query($this->my_db, "DELETE FROM evaluation_relation WHERE ev_r_no IN({$chk_del_text})")){
                $chk_result = true;
            }
        }

        return $chk_result;
    }

    public function checkEvResultCreate($ev_no, $ev_u_set_no, $ev_r_no, $receiver, $evaluator)
    {
        $chk_result_sql = "
            SELECT 
               *,
               (SELECT er.rate FROM evaluation_relation er WHERE er.ev_r_no='{$ev_r_no}') as er_rate,
               (SELECT esr.ev_result_no FROM evaluation_system_result esr WHERE esr.ev_no='{$ev_no}' AND esr.ev_u_no=eu.ev_u_no AND esr.receiver_s_no='{$receiver}' AND esr.evaluator_s_no='{$evaluator}') as result_no
            FROM evaluation_unit eu 
            WHERE eu.ev_u_set_no='{$ev_u_set_no}' AND eu.active='1' AND eu.evaluation_state NOT IN(99,100)
        ";
        $chk_result_query   = mysqli_query($this->my_db, $chk_result_sql);
        $chk_ins_data       = [];
        $regdate            = date("Y-m-d H:i:s");
        while($chk_result = mysqli_fetch_assoc($chk_result_query))
        {
            if(empty($chk_result['result_no']))
            {
                $er_rate = 0;
                if($receiver != $evaluator){
                    $er_rate = $chk_result['er_rate'];
                }

                $ins_data = array(
                    "ev_no"             => $ev_no,
                    "ev_r_no"           => $ev_r_no,
                    "ev_u_set_no"       => $ev_u_set_no,
                    "receiver_s_no"     => $receiver,
                    "evaluator_s_no"    => $evaluator,
                    "ev_u_no"           => $chk_result['ev_u_no'],
                    "order"             => $chk_result['order'],
                    "kind"              => $chk_result['kind'],
                    "question"          => $chk_result['question'],
                    "description"       => $chk_result['description'],
                    "rate"              => $er_rate,
                    "evaluation_state"  => $chk_result['evaluation_state'],
                    "evaluation_value"  => "",
                    "choice_items"      => $chk_result['choice_items'],
                    "essential"         => $chk_result['essential'],
                    "regdate"           => $regdate,
                );

                $this->insert($ins_data);
            }
        }
    }

    public function getEvSumRate($ev_no, $receiver)
    {
        $chk_ev_rate_sql    = "SELECT SUM(rate) as total_rate FROM evaluation_relation WHERE ev_no='{$ev_no}' AND rec_is_review='1' AND eval_is_review='1' AND receiver_s_no='{$receiver}' AND evaluator_s_no!='{$receiver}'";
        $chk_ev_rate_query  = mysqli_query($this->my_db, $chk_ev_rate_sql);
        $chk_ev_rate_result = mysqli_fetch_assoc($chk_ev_rate_query);

        return $chk_ev_rate_result['total_rate'];
    }

    public function checkComplete($ev_r_no, $ev_u_set_no)
    {
        $chk_ev_question_sql    = "SELECT COUNT(ev_u_no) as cnt FROM evaluation_unit WHERE ev_u_set_no='{$ev_u_set_no}' AND `active`='1' AND evaluation_state NOT IN(99,100) AND essential='1'";
        $chk_ev_question_query  = mysqli_query($this->my_db, $chk_ev_question_sql);
        $chk_ev_question_result = mysqli_fetch_assoc($chk_ev_question_query);
        $chk_ev_question_cnt    = $chk_ev_question_result['cnt'];

        $chk_ev_result_sql      = "SELECT COUNT(ev_result_no) as cnt FROM evaluation_system_result WHERE ev_r_no='{$ev_r_no}' AND evaluation_state NOT IN(99,100) AND evaluation_value != '' AND evaluation_value IS NOT NULL AND evaluation_value != '0' AND essential='1'";
        $chk_ev_result_query    = mysqli_query($this->my_db, $chk_ev_result_sql);
        $chk_ev_result_result   = mysqli_fetch_assoc($chk_ev_result_query);
        $chk_ev_result_cnt      = $chk_ev_result_result['cnt'];

        if($chk_ev_question_cnt == $chk_ev_result_cnt){
            $this->update(array('ev_r_no' => $ev_r_no, "is_complete" => 1));
        }else{
            $this->update(array('ev_r_no' => $ev_r_no, "is_complete" => 0));
        }
    }
}
