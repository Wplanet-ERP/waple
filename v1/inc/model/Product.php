<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Product extends Model
{
    protected $_main_table      = "product";
    protected $_relation_table  = "product_relation";
    protected $_main_key        = "prd_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Product();
    }

    public function insertRelation($insert_data)
    {
        $result = false;

        if(!empty($insert_data))
        {
            $ins_sql = "INSERT INTO {$this->_relation_table} SET ";
            $comma   = "";
            foreach($insert_data as $key => $data){
                if($data == 'NULL'){
                    $ins_sql .= "{$comma} `{$key}`=NULL";
                }else{
                    $ins_sql .= "{$comma} `{$key}`='{$data}'";
                }

                $comma    = " , ";
            }

            if(mysqli_query($this->my_db, $ins_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function deleteRelation($prd_no, $related_prd)
    {
        $related_del_sql = "DELETE FROM `product_relation` WHERE `prd_no`='{$prd_no}' AND `related_prd`='{$related_prd}'";
        mysqli_query($this->my_db, $related_del_sql);
    }

    function getProductRelationList($prd_no)
    {
        $related_product_sql 	= "SELECT pr.no, prd.prd_no, prd.title as prd_name, (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code)) AS prd_g1_name, (SELECT k.k_name FROM kind k WHERE k.k_name_code=prd.k_name_code) AS prd_g2_name, pr.priority FROM {$this->_relation_table} pr LEFT JOIN {$this->_main_table} prd ON prd.prd_no = pr.related_prd WHERE pr.prd_no='{$prd_no}' ORDER BY pr.`priority` ASC, pr.no ASC";
        $related_product_query	= mysqli_query($this->my_db, $related_product_sql);
        $related_product_list   = [];

        while($related_product = mysqli_fetch_array($related_product_query))
        {
            $related_product_list[$related_product['prd_no']] = array(
                'pr_no'			=> $related_product['no'],
                'prd_no'		=> $related_product['prd_no'],
                'prd_name'		=> $related_product['prd_name'],
                'prd_g1_name'	=> $related_product['prd_g1_name'],
                'prd_g2_name'	=> $related_product['prd_g2_name'],
                'priority'		=> $related_product['priority']
            );
        }

        return $related_product_list;
    }

    public function getDepositWorkProduct()
    {
        $dp_product_sql = "
            SELECT 
                prd.title, 
                prd.prd_no,
                (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT k.k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code)) AS k_name_1,
                (SELECT k.k_name FROM kind k WHERE k.k_name_code=prd.k_name_code) AS k_name_2
            FROM {$this->_main_table} prd
            WHERE (prd.wd_dp_state='2' OR prd.wd_dp_state='4') AND prd.display='1'
            ORDER BY prd.k_name_code ASC
        ";
        $dp_product_query  = mysqli_query($this->my_db, $dp_product_sql);
        $dp_product_option = [];
        while($dp_product = mysqli_fetch_assoc($dp_product_query))
        {
            $dp_product_option[$dp_product['prd_no']] = $dp_product;
        }

        return $dp_product_option;
    }

    public function getWithdrawWorkProduct()
    {
        $wd_product_sql = "
            SELECT 
               prd.title, prd.prd_no, 
               (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT k.k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code)) AS k_name_1,
               (SELECT k.k_name FROM kind k WHERE k.k_name_code=prd.k_name_code) AS k_name_2
            FROM {$this->_main_table} prd
            WHERE (prd.wd_dp_state='3' OR prd.wd_dp_state='4') AND prd.display='1'
            ORDER BY prd.k_name_code ASC
        ";
        $wd_product_query  = mysqli_query($this->my_db, $wd_product_sql);
        $wd_product_list   = [];
        while($product_data = mysqli_fetch_array($wd_product_query))
        {
            $wd_product_list[$product_data['prd_no']] = $product_data['k_name_1']." > ".$product_data['k_name_2']." > ".$product_data['title'];
        }

        return $wd_product_list;
    }

    public function getPrdWorkExtra($prd_no)
    {
        $product_sql    = "SELECT work_format_extra FROM {$this->_main_table} p WHERE p.prd_no='{$prd_no}'";
        $product_query  = mysqli_query($this->my_db, $product_sql);
        $product_result = mysqli_fetch_assoc($product_query);

        return isset($product_result['work_format_extra']) ? $product_result['work_format_extra'] : "";
    }

    public function getDdayList()
    {
        $product_sql    = "SELECT prd_no FROM {$this->_main_table} WHERE is_task_dday='1' ORDER BY {$this->_main_key} ASC";
        $product_query  = mysqli_query($this->my_db, $product_sql);
        $product_list   = [];
        while($product_result = mysqli_fetch_assoc($product_query))
        {
            $product_list[] = $product_result['prd_no'];
        }

        return $product_list;
    }

    public function getTaskRunStaffCnt($prd_no)
    {
        $run_staff_cnt_sql 	    = "SELECT (LENGTH(task_run_staff)-LENGTH(REPLACE(task_run_staff,',',''))+1) AS cnt FROM product WHERE display = 1 AND prd_no='{$prd_no}' AND ((task_run_staff != '' AND task_run_staff IS NOT NULL) OR task_run_staff_type='2');";
        $run_staff_cnt_query 	= mysqli_query($this->my_db, $run_staff_cnt_sql);
        $run_staff_cnt_result   = mysqli_fetch_assoc($run_staff_cnt_query);

        return isset($run_staff_cnt_result['cnt']) ? $run_staff_cnt_result['cnt'] : 0;
    }
}
