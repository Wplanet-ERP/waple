<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Staff extends Model
{
    protected $_main_table = "staff";
    protected $_main_key   = "s_no";

    private $_list          = [];
    private $_name_list     = [];
    private $_slack_list    = [];

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Staff();
    }

    public function getStaff($s_no)
    {
        $staff_sql      = "SELECT *, (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=s.my_c_no) as my_c_name, (SELECT CONCAT(mc.address1,' ',mc.address2) FROM my_company mc WHERE mc.my_c_no=s.my_c_no) as my_c_addr, (SELECT t.team_name FROM team t WHERE t.team_code=s.team) as t_name FROM {$this->_main_table} as s WHERE `s_no`='{$s_no}' LIMIT 1";
        $staff_query    = mysqli_query($this->my_db, $staff_sql);
        $staff_result   = mysqli_fetch_assoc($staff_query);

        return isset($staff_result) ? $staff_result : [];
    }

    public function getStaffWhereName($s_name)
    {
        $staff_sql      = "SELECT *, (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=s.my_c_no) as my_c_name, (SELECT CONCAT(mc.address1,' ',mc.address2) FROM my_company mc WHERE mc.my_c_no=s.my_c_no) as my_c_addr, (SELECT t.team_name FROM team t WHERE t.team_code=s.team) as t_name FROM {$this->_main_table} as s WHERE `s_name`='{$s_name}' AND staff_state IN('1','2') LIMIT 1";
        $staff_query    = mysqli_query($this->my_db, $staff_sql);
        $staff_result   = mysqli_fetch_assoc($staff_query);

        return isset($staff_result) ? $staff_result : [];
    }

    public function getStaffName($s_no)
    {
        $staff_sql      = "SELECT s_name FROM {$this->_main_table} WHERE `s_no`='{$s_no}' LIMIT 1";
        $staff_query    = mysqli_query($this->my_db, $staff_sql);
        $staff_result   = mysqli_fetch_assoc($staff_query);

        return isset($staff_result['s_name']) ? $staff_result['s_name'] : "";
    }

    public function setList()
    {
        $staff_list_sql   = "SELECT s_no, s_name, team, slack_id FROM {$this->_main_table} WHERE staff_state < '3' ORDER BY s_no";
        $staff_list_query = mysqli_query($this->my_db, $staff_list_sql);
        $staff_all_list	  = [];
        $staff_name_list  = [];
        $staff_slack_list = [];
        while($staff_info = mysqli_fetch_array($staff_list_query))
        {
            $staff_all_list[$staff_info['s_no']] = array(
                's_name' => $staff_info['s_name'],
                'team' 	 => $staff_info['team'],
            );
            $staff_name_list[$staff_info['s_no']] = $staff_info['s_name'];
            $staff_slack_list[$staff_info['s_no']]  = $staff_info['slack_id'];
        }

        $this->_list        = $staff_all_list;
        $this->_name_list   = $staff_name_list;
        $this->_slack_list  = $staff_slack_list;
    }

    public function getStaffList(){
        return $this->_list;
    }

    public function getStaffNameList($order="s_no", $type="2")
    {
        if(empty($this->_name_list))
        {
            $staff_list_sql     = "SELECT s_no, s_name FROM {$this->_main_table} WHERE staff_state <= {$type} ORDER BY {$order}";
            $staff_list_query   = mysqli_query($this->my_db, $staff_list_sql);
            $this->_name_list   = [];
            while($staff = mysqli_fetch_assoc($staff_list_query))
            {
                $this->_name_list[$staff['s_no']] = $staff['s_name'];
            }
        }

        return $this->_name_list;
    }

    public function getStaffAndTeamNameList($order="s_no", $type="2")
    {
        $staff_list_sql         = "SELECT s_no, s_name, (SELECT t.team_name FROM team t WHERE t.team_code=s.team) as t_name FROM {$this->_main_table} as s WHERE staff_state <= {$type} ORDER BY {$order}";
        $staff_list_query       = mysqli_query($this->my_db, $staff_list_sql);
        $staff_team_name_list   = [];
        while($staff = mysqli_fetch_assoc($staff_list_query))
        {
            $staff_team_name_list[$staff['s_no']] = $staff['s_name']." ({$staff['t_name']})";
        }

        return $staff_team_name_list;
    }

    public function getStaffSlackList(){
        return $this->_slack_list;
    }

    public function getActiveTeamStaff($team)
    {
        $team_staff_sql     = "SELECT s_no, s_name FROM {$this->_main_table} WHERE `team`='{$team}' AND staff_state='1'";
        $team_staff_query   = mysqli_query($this->my_db, $team_staff_sql);
        $team_staff_list    = [];
        while($team_staff_result = mysqli_fetch_assoc($team_staff_query))
        {
            $team_staff_list[$team_staff_result['s_no']] = $team_staff_result['s_name'];
        }

        return $team_staff_list;
    }

    public function getActiveTeamNameStaff($team)
    {
        $team_staff_sql     = "SELECT s_no, s_name, (SELECT t.team_name FROM team t WHERE t.team_code=s.team) as t_name FROM {$this->_main_table} as s WHERE `team`='{$team}' AND staff_state='1'";
        $team_staff_query   = mysqli_query($this->my_db, $team_staff_sql);
        $team_staff_list    = [];
        while($team_staff_result = mysqli_fetch_assoc($team_staff_query))
        {
            $team_staff_list[$team_staff_result['s_no']] = $team_staff_result['s_name']."({$team_staff_result['t_name']})";
        }

        return $team_staff_list;
    }

    public function getTeamWhereStaff($team_where)
    {
        $staff_sql      = "SELECT s.s_no, s.s_name FROM {$this->_main_table} s WHERE ({$team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
        $staff_result   = mysqli_query($this->my_db, $staff_sql);
        $staff_list     = [];
        while($staff = mysqli_fetch_array($staff_result))
        {
            $staff_list[$staff['s_no']] = trim($staff['s_name']);
        }

        return $staff_list;
    }

    public function getManagerList()
    {
        $staff_sql      = "SELECT * FROM staff WHERE `position` != '팀원' AND staff_state='1'";
        $staff_result   = mysqli_query($this->my_db, $staff_sql);
        $staff_list     = [];
        while($staff = mysqli_fetch_array($staff_result))
        {
            $staff_list[$staff['s_no']] = trim($staff['s_name']);
        }

        return $staff_list;
    }
}
