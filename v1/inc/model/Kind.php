<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Kind extends Model
{
    protected $_main_table  = "kind";
    protected $_main_key    = "k_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Kind();
    }

    public function getKindParent($k_name_code)
    {
        $kind_sql       = "SELECT *, (SELECT kp.k_name FROM kind kp WHERE kp.k_name_code=k.k_parent LIMIT 1) as k_parent_name FROM {$this->_main_table} k WHERE k.k_name_code='{$k_name_code}' AND k.display='1' LIMIT 1";
        $kind_query     = mysqli_query($this->my_db, $kind_sql);
        $kind_result    = mysqli_fetch_assoc($kind_query);
        $kind_item      = !empty($kind_result) ? $kind_result : [];

        return $kind_item;
    }

    public function getKindParentList($k_code)
    {
        $kind_sql   = "SELECT k_name, k_name_code FROM {$this->_main_table} WHERE k_code='{$k_code}' AND (k_parent='0' OR k_parent is null) AND display='1'";
        $kind_query = mysqli_query($this->my_db, $kind_sql);
        $kind_list  = [];
        while($kind = mysqli_fetch_array($kind_query))
        {
            $kind_list[$kind['k_name_code']] = $kind['k_name'];
        }

        return $kind_list;
    }

    public function getKindChildList($k_code)
    {
        $kind_child_sql   = "SELECT k_name, k_name_code FROM {$this->_main_table} WHERE k_code='{$k_code}' AND k_parent is not null AND display='1'";
        $kind_child_query = mysqli_query($this->my_db, $kind_child_sql);
        $kind_child_list  = [];
        while($kind_child = mysqli_fetch_array($kind_child_query))
        {
            $kind_child_list[$kind_child['k_name_code']] = $kind_child['k_name'];
        }

        return $kind_child_list;
    }

    public function getKindGroupList($k_code, $display=1)
    {
        $add_where = "k_code = '{$k_code}'";
        if($display == '1'){
            $add_where .= " AND display='1'";
        }

        $kind_group_sql = "
            SELECT 
                k_name,
                k_name_code,
                k_parent
            FROM {$this->_main_table} 
            WHERE {$add_where} ORDER BY priority ASC
        ";
        $kind_group_query  = mysqli_query($this->my_db, $kind_group_sql);
        $kind_group_list   = [];
        while($kind_group = mysqli_fetch_array($kind_group_query))
        {
            $kind_group_list[$kind_group['k_parent']][] = array(
                "k_name"	  => trim($kind_group['k_name']),
                "k_name_code" => trim($kind_group['k_name_code'])
            );
        }

        return $kind_group_list;
    }

    public function getKindGroupChildList($k_code, $k_parent)
    {
        $kind_group_sql = "
            SELECT 
                k_name,
                k_name_code,
                priority
            FROM {$this->_main_table} 
            WHERE k_code='{$k_code}' AND k_parent='{$k_parent}' AND display='1' ORDER BY priority ASC
        ";

        $kind_group_list   = [];
        $kind_group_query  = mysqli_query($this->my_db, $kind_group_sql);
        while($kind_group = mysqli_fetch_array($kind_group_query))
        {
            $kind_group_list[$kind_group['k_name_code']] = array(
                "k_name"	  => trim($kind_group['k_name']),
                "k_name_code" => trim($kind_group['k_name_code']),
                "priority"    => $kind_group['priority']
            );
        }

        return $kind_group_list;
    }

    public function getKindChartData($k_code)
    {
        $kind_data_list         = [];
        $kind_group_list        = [];
        $kind_group_name_list   = [];
        $kind_group_code_list   = [];

        $kind_group_sql = "
            SELECT 
                k_name,
                k_name_code,
                k_parent
            FROM {$this->_main_table} 
            WHERE k_code='{$k_code}' AND display='1' ORDER BY priority ASC
        ";
        $kind_group_query  = mysqli_query($this->my_db, $kind_group_sql);
        while($kind_group = mysqli_fetch_array($kind_group_query))
        {
            $kind_group_code_list[] = $kind_group['k_name_code'];
            $kind_group_list[$kind_group['k_parent']][] = array(
                "k_name"	  => trim($kind_group['k_name']),
                "k_name_code" => trim($kind_group['k_name_code'])
            );
            $kind_group_name_list[$kind_group['k_name_code']] = $kind_group['k_name'];
        }

        $kind_data_list['kind_group_list']  = $kind_group_list;
        $kind_data_list['kind_group_name']  = $kind_group_name_list;
        $kind_data_list['kind_group_code']  = $kind_group_code_list;

        return $kind_data_list;
    }

    public function getKindDisplayNameList($k_code)
    {
        $kind_sql = "
            SELECT
                *
            FROM {$this->_main_table} k
            WHERE k_code='{$k_code}' ORDER BY k_name ASC
        ";
        $kind_query         = mysqli_query($this->my_db, $kind_sql);
        $k_parent_list      = [];
        $k_child_list       = [];
        $k_total_list       = [];
        $kind_total_list    = [];
        while($kind_result = mysqli_fetch_array($kind_query))
        {
            $k_name_code   = trim($kind_result['k_name_code']);
            $k_name_title  = ($kind_result['display'] == '2') ? $kind_result['k_name']." [OFF]" : $kind_result['k_name'];
            $k_total_list[$kind_result['k_parent']][$k_name_code] = $k_name_title;

            if(empty($kind_result['k_parent'])){
                $k_parent_list[$k_name_code] = $k_name_title;
            }else{
                $k_child_list[$k_name_code] = $k_name_title;
            }
        }

        $kind_total_list['k_parent_list'] = $k_parent_list;
        $kind_total_list['k_child_list']  = $k_child_list;
        $kind_total_list['k_total_list']  = $k_total_list;

        return $kind_total_list;
    }

    public function getKindLastNo($k_code)
    {
        $last_kind_sql    = "SELECT MAX(k_name_code) as last_kind_no FROM {$this->_main_table} WHERE k_code='{$k_code}'";
        $last_kind_query  = mysqli_query($this->my_db, $last_kind_sql);
        $last_kind_result = mysqli_fetch_assoc($last_kind_query);
        $last_kind_no     = sprintf('%05d', $last_kind_result['last_kind_no'] +1);

        return $last_kind_no;
    }

    public function getBrandCompanyList()
    {
        $brand_company_sql = "
            SELECT 
                main.k_parent, 
                (SELECT k.k_name FROM kind k WHERE k.k_name_code=main.k_parent) AS k_parent_name, 
                main.k_name_code, 
                main.k_name,
                c.c_no,
                c.c_name,
                (SELECT s.s_name FROM staff s WHERE s.s_no=c.s_no) AS s_name,
                (SELECT t.team_name FROM team t WHERE t.team_code =(SELECT s.team FROM staff s WHERE s.s_no=c.s_no)) AS t_name
            FROM kind main 
            LEFT JOIN company c ON c.brand=main.k_name_code
            WHERE main.k_code='brand' AND main.k_parent IS NOT NULL AND main.display='1'
            ORDER BY main.k_parent, main.priority, c.c_no
        ";
        $brand_company_query    = mysqli_query($this->my_db, $brand_company_sql);
        $brand_g1_list          = [];
        $brand_g2_list          = [];
        $brand_list             = [];
        $brand_dom_list         = [];
        $brand_total_list       = [];
        $brand_info_list        = [];
        $brand_g2_parent_list   = [];
        $brand_parent_list      = [];
        while($brand_company = mysqli_fetch_assoc($brand_company_query))
        {
            $brand_g1_list[$brand_company['k_parent']] = $brand_company['k_parent_name'];
            $brand_g2_list[$brand_company['k_parent']][$brand_company['k_name_code']]   = $brand_company['k_name'];
            $brand_list[$brand_company['k_name_code']][$brand_company['c_no']]          = $brand_company['c_name'];

            $brand_total_list[$brand_company['k_parent']][$brand_company['c_no']]       = $brand_company['c_no'];
            $brand_total_list[$brand_company['k_name_code']][$brand_company['c_no']]    = $brand_company['c_no'];
            $brand_total_list[$brand_company['c_no']][$brand_company['c_no']]           = $brand_company['c_no'];

            if($brand_company['c_no'] != "5513" && $brand_company['c_no'] != "5514" && $brand_company['c_no'] != "5979" && $brand_company['c_no'] != "6044")
            {
                $brand_dom_list[$brand_company['k_name_code']][$brand_company['c_no']] = $brand_company['c_name'];
                $brand_info_list[$brand_company['c_no']]  = array(
                    "g_name"    => $brand_company['k_parent_name']." > ".$brand_company['k_name'],
                    "c_no"      => $brand_company['c_no'],
                    "c_name"    => $brand_company['c_name'],
                    "s_name"    => $brand_company['s_name'],
                    "t_name"    => $brand_company['t_name'],
                );
            }

            $brand_g2_parent_list[$brand_company['k_name_code']] = array("brand_g1" => $brand_company['k_parent']);
            $brand_parent_list[$brand_company['c_no']] = array("brand_g1" => $brand_company['k_parent'], "brand_g2" => $brand_company['k_name_code']);
        }

        $brand_data['brand_g1_list']        = $brand_g1_list;
        $brand_data['brand_g2_list']        = $brand_g2_list;
        $brand_data['brand_list']           = $brand_list;
        $brand_data['brand_dom_list']       = $brand_dom_list;
        $brand_data['brand_info_list']      = $brand_info_list;
        $brand_data['brand_g2_parent_list'] = $brand_g2_parent_list;
        $brand_data['brand_parent_list']    = $brand_parent_list;
        $brand_data['brand_total_list']     = $brand_total_list;

        return $brand_data;
    }

    public function getKindName($k_name_code)
    {
        $kind_sql       = "SELECT k_name FROM {$this->_main_table} WHERE k_name_code='{$k_name_code}' LIMIT 1";
        $kind_query     = mysqli_query($this->my_db, $kind_sql);
        $kind_result    = mysqli_fetch_assoc($kind_query);

        return !empty($kind_result) ? $kind_result['k_name'] : "";
    }
}
