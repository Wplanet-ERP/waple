<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class MyQuick extends Model
{
    protected $_main_table  = "quick_search";
    protected $_main_key    = "q_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new MyQuick();
    }

    public function isMyQuick($type, $prd_no, $s_no)
    {
        $chk_sql    = "SELECT count(q_no) as cnt FROM quick_search WHERE `type`='{$type}' AND prd_no='{$prd_no}' AND s_no='{$s_no}'";
        $chk_query  = mysqli_query($this->my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        return ($chk_result['cnt'] > 0) ?  true : false;
    }

    public function getMyQuickItem($type, $prd_no, $s_no)
    {
        $chk_sql    = "SELECT * FROM quick_search WHERE `type`='{$type}' AND prd_no='{$prd_no}' AND s_no='{$s_no}'";
        $chk_query  = mysqli_query($this->my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        return $chk_result;
    }

}
