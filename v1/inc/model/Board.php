<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Board extends Model
{
    protected $_main_table  = "board_manager";
    protected $_main_key    = "no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Board();
    }

    public function setBoardManager()
    {
        $this->_main_table  = "board_manager";
        $this->_main_key    = "no";
    }

    public function setBoardNotice()
    {
        $this->_main_table  = "board_notice";
        $this->_main_key    = "no";
    }

    public function setBoardNormal()
    {
        $this->_main_table  = "board_normal";
        $this->_main_key    = "no";
    }

    public function setBoardGuide()
    {
        $this->_main_table  = "board_guide";
        $this->_main_key    = "b_no";
    }

    public function getBoardType()
    {
        $board_type_sql   = "SELECT * FROM board_manager WHERE display='1' AND comment_format='1'";
        $board_type_query = mysqli_query($this->my_db, $board_type_sql);
        $board_type_list  = array('personal_expenses' => "개인경비");
        while($board_type = mysqli_fetch_assoc($board_type_query))
        {
            $board_type_list[$board_type['b_id']] = $board_type['board_name'];
        }

        return $board_type_list;
    }

    public function getBoardTopFiveList($type)
    {
        $board_top_sql = "
            SELECT
                *,
                (SELECT s_name FROM staff s where s.s_no=brd_nm.s_no) AS writer,
                DATE_FORMAT(regdate, '%Y/%m/%d') as reg_date,
                DATE_FORMAT(regdate, '%H:%i') as reg_time
            FROM board_normal as brd_nm
            WHERE b_id='{$type}'
            ORDER BY top_view DESC, no DESC
            LIMIT 5
        ";
        $board_top_query = mysqli_query($this->my_db, $board_top_sql);
        $board_top_list  = [];
        while($board_top = mysqli_fetch_assoc($board_top_query))
        {
            $board_top_list[] = $board_top;
        }

        return $board_top_list;
    }
}
