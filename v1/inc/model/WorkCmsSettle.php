<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class WorkCmsSettle extends Model
{
    protected $_main_table  = "work_cms_settlement";
    protected $_vat_table   = "work_cms_vat";
    protected $_main_key    = "no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new WorkCmsSettle();
    }

    public function getSettleVatCompanyList($prev_month)
    {
        $company_sql = "
            (SELECT DISTINCT vat_dp_c_no AS dp_c_no, vat_dp_c_name AS dp_c_name, (SELECT c.priority FROM company c WHERE c.c_no=wcv.vat_dp_c_no) as priority FROM work_cms_vat wcv WHERE wcv.vat_month='{$prev_month}') 
            UNION
            (SELECT DISTINCT settle_dp_c_no AS dp_c_no, settle_dp_c_name AS dp_c_name, (SELECT c.priority FROM company c WHERE c.c_no=wcs.settle_dp_c_no) as priority FROM work_cms_settlement wcs WHERE wcs.settle_month='{$prev_month}')
            ORDER BY priority ASC, dp_c_name ASC
        ";
        $company_query = mysqli_query($this->my_db, $company_sql);
        $company_list = [];
        while ($company_result = mysqli_fetch_assoc($company_query)) {
            $company_list[$company_result["dp_c_no"]] = $company_result["dp_c_name"];
        }

        return $company_list;
    }

    public function getSettleVatCompanyTotalList()
    {
        $company_sql = "
            (SELECT DISTINCT vat_dp_c_no AS dp_c_no, vat_dp_c_name AS dp_c_name, (SELECT c.priority FROM company c WHERE c.c_no=wcv.vat_dp_c_no) as priority FROM work_cms_vat wcv) 
            UNION
            (SELECT DISTINCT settle_dp_c_no AS dp_c_no, settle_dp_c_name AS dp_c_name, (SELECT c.priority FROM company c WHERE c.c_no=wcs.settle_dp_c_no) as priority FROM work_cms_settlement wcs)
            ORDER BY priority ASC, dp_c_name ASC
        ";
        $company_query = mysqli_query($this->my_db, $company_sql);
        $company_list  = [];
        while($company_result = mysqli_fetch_assoc($company_query))
        {
            $company_list[$company_result["dp_c_no"]] = $company_result["dp_c_name"];
        }

        return $company_list;
    }
}
