<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class WiseCsm extends Model
{
    protected $_main_table  = "csm_return";
    protected $_main_key    = "cr_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new WiseCsm();
    }

    public function getReturnStateOption()
    {
        $return_state_sql    = "SELECT DISTINCT `return_state` FROM {$this->_main_table} ORDER BY return_state ASC";
        $return_state_query  = mysqli_query($this->my_db, $return_state_sql);
        $return_state_option = [];
        while($return_state = mysqli_fetch_assoc($return_state_query))
        {
            $return_state_option[] = $return_state['return_state'];
        }

        return $return_state_option;
    }

    public function getReturnCompanyOption()
    {
        $company_sql    = "SELECT `c_no`, c_name FROM {$this->_main_table} WHERE c_no > 0 GROUP BY c_no ORDER BY c_name ASC";
        $company_query  = mysqli_query($this->my_db, $company_sql);
        $company_option = [];
        while($company = mysqli_fetch_assoc($company_query))
        {
            $company_option[$company['c_no']] = $company['c_name'];
        }

        return $company_option;
    }

    public function getReturnReasonOption()
    {
        $return_reason_sql    = "SELECT DISTINCT return_reason FROM {$this->_main_table} WHERE return_reason != '' ORDER BY return_reason ASC";
        $return_reason_query  = mysqli_query($this->my_db, $return_reason_sql);
        $return_reason_option = [];
        while($return_reason = mysqli_fetch_assoc($return_reason_query))
        {
            $return_reason_option[] = $return_reason['return_reason'];
        }

        return $return_reason_option;
    }
}
