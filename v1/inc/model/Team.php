<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Team extends Model
{
    protected $_main_table = "team";
    protected $_main_key   = "team_code";

    private $_list          = [];

    private $_name_list     = [];

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Team();
    }

    public function getTeamAllList()
    {
        $team_list_sql   	    = "SELECT team_name, team_code, team_code_parent, `depth` FROM {$this->_main_table} WHERE display = 1 ORDER BY priority ASC";
        $team_list_query 	    = mysqli_query($this->my_db, $team_list_sql);
        $team_all_list   	    = [];
        $team_name_list   	    = [];
        $staff_team_all_list    = [];
        $staff_all_list   	    = [];
        while($team_result = mysqli_fetch_array($team_list_query))
        {
            $team_all_list[] = array(
                'team_name'        => $team_result['team_name'],
                'team_code'        => $team_result['team_code'],
                'team_code_parent' => $team_result['team_code_parent']
            );

            $team_name_list[$team_result['team_code']] = $team_result['team_name'];

            $team_where        = $this->getTeamCodeWhere($team_result['team_code']);
            $staff_team_sql    = "SELECT s_no, s_name from staff WHERE staff_state='1' AND `team` IN({$team_where}) ORDER BY s_no ASC";
            $staff_team_query  = mysqli_query($this->my_db, $staff_team_sql);

            while($staff_team_result = mysqli_fetch_assoc($staff_team_query)){
                $staff_all_list['all'][$staff_team_result['s_no']]      = $staff_team_result['s_name'];
                $staff_team_all_list['all'][$staff_team_result['s_no']] = "{$staff_team_result['s_name']}({$team_result['team_name']})";

                $staff_all_list[$team_result['team_code']][$staff_team_result['s_no']]      = $staff_team_result['s_name'];
                $staff_team_all_list[$team_result['team_code']][$staff_team_result['s_no']] = "{$staff_team_result['s_name']}({$team_result['team_name']})";
            }
        }

        ksort($staff_all_list['all']);

        return array("team_list" => $team_all_list, "team_name_list" => $team_name_list, "staff_team_list" => $staff_team_all_list, "staff_list" => $staff_all_list);
    }

    public function getTeamChkList()
    {
        $team_list_sql      = "SELECT team_name, team_code, team_code_parent FROM {$this->_main_table} WHERE display = 1 ORDER BY priority";
        $team_list_query    = mysqli_query($this->my_db, $team_list_sql);
        $team_list          = [];
        while($team_result = mysqli_fetch_assoc($team_list_query)){
            $team_list[$team_result['team_code']] = array("t_name" => $team_result['team_name'], "t_parent" => $team_result['team_code_parent']);
        }

        return $team_list;
    }

    public function getLastTeamCode()
    {
        $last_team_code_sql = "SELECT (MAX(team_code)+1) as last_t_code FROM {$this->_main_table}";
        $last_team_query    = mysqli_query($this->my_db, $last_team_code_sql);
        $last_team_result   = mysqli_fetch_assoc($last_team_query);

        return $last_team_result['last_t_code'];
    }

    public function getMaxPriority()
    {
        $priority_max_sql    = "SELECT MAX(priority) as p_max FROM {$this->_main_table} WHERE `display`='1'";
        $priority_max_query  = mysqli_query($this->my_db, $priority_max_sql);
        $priority_max_result = mysqli_fetch_assoc($priority_max_query);
        $priority_max        = (isset($priority_max_result['p_max']) && $priority_max_result['p_max'] > 0) ?$priority_max_result['p_max']+1 : 1;

        return $priority_max;
    }

    public function getTeamChildDepth($team_code)
    {
        $depth_val_sql    = "SELECT `depth` FROM {$this->_main_table} WHERE `team_code`='{$team_code}'";
        $depth_val_query  = mysqli_query($this->my_db, $depth_val_sql);
        $depth_val_result = mysqli_fetch_assoc($depth_val_query);
        $depth_val        = isset($depth_val_result['depth']) ? $depth_val_result['depth']+1 : "2";

        return $depth_val;
    }

    public function getTeamGroup($team_code)
    {
        $team_group_sql     = "SELECT `team_group` FROM {$this->_main_table} WHERE `team_code`='{$team_code}'";
        $team_group_query   = mysqli_query($this->my_db, $team_group_sql);
        $team_group_result  = mysqli_fetch_assoc($team_group_query);

        return isset($team_group_result['team_group']) ? $team_group_result['team_group'] : "";
    }

    public function updatePriority($team_code, $priority)
    {
        if(!empty($team_code)){
            $priority_chk_sql    = "SELECT * FROM {$this->_main_table} WHERE `display`='1' AND `priority` >='{$priority}' AND `team_code` != '{$team_code}' ORDER BY `priority`";
        }else{
            $priority_chk_sql    = "SELECT * FROM {$this->_main_table} WHERE `display`='1' AND `priority` >='{$priority}' ORDER BY `priority`";
        }

        $priority_chk_query  = mysqli_query($this->my_db, $priority_chk_sql);
        $upd_priority        = $priority;
        while($priority_result = mysqli_fetch_assoc($priority_chk_query)){
            if($priority_result['team_code']){
                $upd_priority++;
                $upd_priority_sql = "UPDATE team SET `priority`='{$upd_priority}' WHERE `team_code` ='{$priority_result['team_code']}'";
                mysqli_query($this->my_db, $upd_priority_sql);
            }
        }
    }

    public function getTeamNameList()
    {
        $team_list_sql      = "SELECT team_name, team_code, team_code_parent FROM {$this->_main_table} WHERE display = 1 ORDER BY priority";
        $team_list_query    = mysqli_query($this->my_db, $team_list_sql);
        $team_name_list     = [];
        while($team = mysqli_fetch_array($team_list_query))
        {
            $team_name_list[$team['team_code']] = $team['team_name'];
        }

        return $team_name_list;
    }

    public function getTeamFullNameList()
    {
        $team_list_sql       = "SELECT team_name, team_code, team_code_parent, `depth` FROM {$this->_main_table} WHERE display = 1 ORDER BY priority";
        $team_list_query     = mysqli_query($this->my_db, $team_list_sql);
        $team_full_name_list = array("all" => ":: 전체 ::");
        while($team = mysqli_fetch_array($team_list_query))
        {
            $team_prev = "";
            if($team['depth'] > 1){
                for($i=1;$i<$team['depth'];$i++){
                    $team_prev .= ">";
                }
            }
            $team_full_name_list[$team['team_code']] = empty($team_prev) ? $team['team_name'] : " {$team_prev} {$team['team_name']}";
        }

        return $team_full_name_list;
    }

    public function getTeamCodeWhere($team_code)
    {
        $team_chk_list_val = "";

        $team_chk_sql    = "SELECT team_group, depth, (SELECT max(sub_t.depth) FROM {$this->_main_table} sub_t WHERE sub_t.`team_group`=t.`team_group` LIMIT 1) as max_depth FROM {$this->_main_table} t WHERE `team_code`='{$team_code}' LIMIT 1";
        $team_chk_query  = mysqli_query($this->my_db, $team_chk_sql);
        $team_chk_result = mysqli_fetch_assoc($team_chk_query);

        $team_chk_list 		= array($team_code);
        $team_chk_max_depth = $team_chk_result['max_depth'];
        $team_chk_depth     = $team_chk_result['depth'];
        if($team_chk_depth <  $team_chk_max_depth)
        {
            $team_parent_list = array($team_code);
            for($i=$team_chk_depth;$i<$team_chk_max_depth;$i++)
            {
                $team_parent_list_val = implode(",", $team_parent_list);
                $team_parent_list = [];
                $chk_sql = "SELECT team_code FROM {$this->_main_table} WHERE `team_code_parent` IN({$team_parent_list_val})";
                $chk_query = mysqli_query($this->my_db, $chk_sql);
                while($chk_result = mysqli_fetch_assoc($chk_query)){
                    $team_parent_list[] = $chk_result['team_code'];
                    $team_chk_list[] = $chk_result['team_code'];
                }
            }
        }

        if($team_chk_list){
            $team_chk_list_val = implode(",", $team_chk_list);
        }

        return $team_chk_list_val;
    }

    public function getIncentiveTeamAndStaffList()
    {
        $team_where_first   = $this->getTeamCodeWhere("00251");
        $team_where_second  = $this->getTeamCodeWhere("00256");
        $team_where_third   = $this->getTeamCodeWhere("00264");

        $team_sql   = "SELECT team_name, team_code, team_code_parent, `depth` FROM {$this->_main_table} WHERE (`{$this->_main_key}` IN({$team_where_first}) OR `{$this->_main_key}` IN({$team_where_second}) OR `{$this->_main_key}` IN({$team_where_third})) ORDER BY priority";
        $team_query = mysqli_query($this->my_db, $team_sql);
        $team_where_list     = [];
        $team_full_name_list = array("all" => ":: 전체 ::");
        $staff_team_list     = [];
        while($team_result = mysqli_fetch_assoc($team_query))
        {
            $team_prev = "";
            if($team_result['depth'] > 2){
                for($i=2;$i<$team_result['depth'];$i++){
                    $team_prev .= ">";
                }
            }
            $team_full_name_list[$team_result['team_code']] = empty($team_prev) ? $team_result['team_name'] : " {$team_prev} {$team_result['team_name']}";

            $team_where        = $this->getTeamCodeWhere($team_result['team_code']);
            $staff_team_sql    = "SELECT s_no, s_name from staff WHERE staff_state='1' AND team IN({$team_where})";
            $staff_team_query  = mysqli_query($this->my_db, $staff_team_sql);

            while($staff_team_result = mysqli_fetch_assoc($staff_team_query)){
                $staff_team_list[$team_result['team_code']][$staff_team_result['s_no']] = $staff_team_result['s_name'];
            }

            $team_where_list[] = "'{$team_result['team_code']}'";
        }

        $incentive_data_list["team_list"]   = $team_full_name_list;
        $incentive_data_list["staff_list"]  = $staff_team_list;
        $incentive_data_list["team_where"]  = $team_where_list;

        return $incentive_data_list;
    }

    public function getChildTeamList()
    {
        $parent_team_sql    = "SELECT DISTINCT sub.team_code_parent FROM team sub WHERE sub.display='1' AND sub.team_code IS NOT NULL ORDER BY priority, team_code";
        $parent_team_query  = mysqli_query($this->my_db, $parent_team_sql);
        $parent_team_list   = [];
        while($parent_team = mysqli_fetch_assoc($parent_team_query))
        {
            $parent_team_list[] = "'{$parent_team['team_code_parent']}'";
        }

        $child_team_list = [];
        if(!empty($parent_team_list))
        {
            $parent_team_val    = implode(',', $parent_team_list);
            $child_team_sql     = "SELECT * FROM team WHERE team_code NOT IN({$parent_team_val}) AND display='1'";
            $child_team_query   = mysqli_query($this->my_db, $child_team_sql);
            while($child_team = mysqli_fetch_assoc($child_team_query))
            {
                $child_team_list[$child_team['team_code']] = $child_team['team_name'];
            }
        }

        return $child_team_list;
    }

    public function getEvTeamList($leader_s_no, $staff_s_no)
    {
        $chk_leader_staff = $leader_s_no;
        if($leader_s_no == '28')
        {
            $staff_chk_sql      = "SELECT team_leader FROM `team` WHERE display = 1 AND team_code=(SELECT s.team FROm staff s WHERE s.s_no='{$staff_s_no}')";
            $staff_chk_query    = mysqli_query($this->my_db, $staff_chk_sql);
            $staff_chk_result   = mysqli_fetch_assoc($staff_chk_query);

            if(isset($staff_chk_result['team_leader'])){
                $chk_leader_staff = $staff_chk_result['team_leader'];
            }
        }

        $team_list_sql       = "SELECT team_name, team_code, team_code_parent, `depth` FROM {$this->_main_table} WHERE display = 1 AND team_leader='{$chk_leader_staff}' AND team_code != '00218' ORDER BY priority";
        $team_list_query     = mysqli_query($this->my_db, $team_list_sql);
        $team_full_name_list = [];
        while($team = mysqli_fetch_array($team_list_query))
        {
            $team_full_name_list[$team['team_code']] =$team['team_name'];
        }

        return $team_full_name_list;
    }

    public function getEvTeamStaffList($leader_s_no, $staff_s_no)
    {
        $chk_leader_staff = $leader_s_no;
        if($leader_s_no == '28')
        {
            $staff_chk_sql      = "SELECT team_leader FROM `team` WHERE display = 1 AND team_code=(SELECT s.team FROm staff s WHERE s.s_no='{$staff_s_no}')";
            $staff_chk_query    = mysqli_query($this->my_db, $staff_chk_sql);
            $staff_chk_result   = mysqli_fetch_assoc($staff_chk_query);

            if(isset($staff_chk_result['team_leader'])){
                $chk_leader_staff = $staff_chk_result['team_leader'];
            }
        }

        $team_list_sql   	    = "SELECT team_name, team_code, team_code_parent, `depth` FROM {$this->_main_table} WHERE display = 1 AND team_leader='{$chk_leader_staff}' ORDER BY priority ASC";
        $team_list_query 	    = mysqli_query($this->my_db, $team_list_sql);
        $staff_all_list   	    = [];
        while($team_result = mysqli_fetch_array($team_list_query))
        {
            $team_where        = $this->getTeamCodeWhere($team_result['team_code']);
            $staff_team_sql    = "SELECT s_no, s_name from staff WHERE staff_state='1' AND `team` IN({$team_where}) ORDER BY s_no ASC";
            $staff_team_query  = mysqli_query($this->my_db, $staff_team_sql);

            while($staff_team_result = mysqli_fetch_assoc($staff_team_query)){
                $staff_all_list['all'][$staff_team_result['s_no']]      = $staff_team_result['s_name'];
                $staff_all_list[$team_result['team_code']][$staff_team_result['s_no']]      = $staff_team_result['s_name'];
            }
        }

        ksort($staff_all_list['all']);

        return $staff_all_list;
    }

    public function getActiveTeamList()
    {
        $team_code_list = [];
        $team_top_sql   = "
            SELECT
                team_code,
                team_name,
                (SELECT max(sub_t.depth) FROM `team` sub_t WHERE sub_t.`team_group`=t.`team_group` LIMIT 1) as max_depth
            FROM `team` AS t
            WHERE display='1' AND `depth`='1' AND team_code_parent IS NULL
            ORDER BY priority ASC
        ";
        $team_top_query = mysqli_query($this->my_db, $team_top_sql);
        while($team_top = mysqli_fetch_assoc($team_top_query))
        {
            if($team_top['max_depth'] == '1'){
                $team_code_list[$team_top['team_code']] = $team_top['team_name'];
            }
            elseif($team_top['max_depth'] == '2'){
                $team_child_sql     = "SELECT team_code, team_name FROM team WHERE team_code_parent='{$team_top['team_code']}' AND display='1' AND `depth`='2' ORDER BY priority ASC";
                $team_child_query   = mysqli_query($this->my_db, $team_child_sql);
                while($team_child = mysqli_fetch_assoc($team_child_query)){
                    if($team_child['team_code'] != "00236"){
                        $team_code_list[$team_child['team_code']] = $team_child['team_name'];
                    }
                }
            }else{
                $team_child_sql     = "SELECT team_code, team_name, (SELECT COUNT(sub_t.id) FROM `team` sub_t WHERE sub_t.`team_code_parent`=t.`team_code`) as chk_child FROM team AS t WHERE team_code_parent='{$team_top['team_code']}' AND display='1' AND `depth`='2' ORDER BY priority ASC";
                $team_child_query   = mysqli_query($this->my_db, $team_child_sql);
                while($team_child = mysqli_fetch_assoc($team_child_query)){
                    if($team_child['chk_child'] > 0){
                        $team_final_sql     = "SELECT team_code, team_name FROM team AS t WHERE team_code_parent='{$team_child['team_code']}' AND display='1' AND `depth`='3' ORDER BY priority ASC";
                        $team_final_query   = mysqli_query($this->my_db, $team_final_sql);
                        while($team_final = mysqli_fetch_assoc($team_final_query)){
                            $team_code_list[$team_final['team_code']] = $team_final['team_name'];
                        }
                    }else{
                        $team_code_list[$team_child['team_code']] = $team_child['team_name'];
                    }
                }
            }
        }

        return $team_code_list;
    }
}
