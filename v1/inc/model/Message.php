<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Message extends Model
{
    protected $_main_table = "EASY_SEND_LOG";
    protected $_main_key   = "MSG_ID";

    protected $_easy_id    = "wplanet";
    protected $_easy_key   = "212043e73b6908c5eeaa8507d0ad4da240e6485b";

    protected $_set_table           = "crm_set";
    protected $_set_key             = "crm_no";

    protected $_template_table      = "crm_template";
    protected $_template_key        = "t_no";

    protected $_sales_reservation_table  = "crm_sales_reservation";
    protected $_sales_reservation_key    = "r_no";

    protected $_cert_reservation_table   = "crm_cert_reservation";
    protected $_cert_reservation_key     = "cr_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Message();
    }

    public function setEasyId($easy_id)
    {
        $this->_easy_id = $easy_id;
    }

    public function getEasyId()
    {
        return $this->_easy_id;
    }

    public function setEasyKey($easy_key)
    {
        $this->_easy_key = $easy_key;
    }

    public function getEasyKey()
    {
        return $this->_easy_key;
    }

    public function setCrmSetInit()
    {
        $this->_main_table  = $this->_set_table;
        $this->_main_key    = $this->_set_key;
    }

    public function setCrmTemplateInit()
    {
        $this->_main_table  = $this->_template_table;
        $this->_main_key    = $this->_template_key;
    }

    public function setCrmSalesReservationInit()
    {
        $this->_main_table  = $this->_sales_reservation_table;
        $this->_main_key    = $this->_sales_reservation_key;
    }

    public function setCrmCertReservationInit()
    {
        $this->_main_table  = $this->_cert_reservation_table;
        $this->_main_key    = $this->_cert_reservation_key;
    }

    public function getCrmTemplateList()
    {
        $list_sql   = "SELECT t_no, title FROM crm_template WHERE active='1' ORDER BY t_no";
        $list_query = mysqli_query($this->my_db, $list_sql);
        $temp_list  = [];
        while($list_result = mysqli_fetch_assoc($list_query)){
            $temp_list[$list_result['t_no']] = $list_result['title'];
        }

        return $temp_list;
    }

    public function getCrmSetList()
    {
        $list_sql   = "SELECT crm_no, title FROM crm_set WHERE crm_state='2'";
        $list_query = mysqli_query($this->my_db, $list_sql);
        $set_list   = [];
        while($list_result = mysqli_fetch_assoc($list_query)){
            $set_list[$list_result['crm_no']] = $list_result['title'];
        }

        return $set_list;
    }

    function sendMessage($message_list)
    {
        $sender_key     = $this->_easy_key;
        $sender_id      = $this->_easy_id;
        $send_url       = "https://alimtalk-api.sweettracker.net/v2/{$sender_key}/sendMessage";
        $send_time_val  = date("YmdHis");
        $send_data      = [];

        foreach($message_list as $message)
        {
            $reserved_time  = isset($message['reserved_time']) ? $message['reserved_time'] : $send_time_val;
            $send_data[]    = array(
                "msgid"         => $message['msg_id'],
                "profile_key"   => $sender_key,
                "receiver_num"  => $message['receiver_hp'],
                "reserved_time" => $reserved_time,
                "sms_only"      => "Y",
                "sms_kind"      => $message['msg_type'],
                "sender_num"    => $message['sender_hp'],
                "sms_title"     => addslashes($message['title']),
                "sms_message"   => addslashes($message['content']),
            );
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $send_url);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Accept:application/json",
            "Content-Type: application/json",
            "userid: {$sender_id}"
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($send_data));

        $curl_result = curl_exec($curl);
        curl_close($curl);

        $result_data_list   = json_decode($curl_result, true);
        $ins_data           = [];
        $chk_result_data    = array("result" => (!empty($result_data_list) ? true : false), "success" => 0, "error" => 0);

        foreach($result_data_list as $result_data)
        {
            $message_data   = $message_list[$result_data["msgid"]];
            $msg_type       = ($message_data['msg_type'] == "L") ? "LMS" : "SMS";
            $reserved_time  = isset($message_data['reserved_time']) ? date("Y-m-d H:i:s", strtotime($message_data['reserved_time'])) : $result_data["sendtime"];

            if($result_data['result'] == "N"){
                $result_status = "3";
                $chk_result_data["error"]++;
            }else{
                $result_status = "1";
                $chk_result_data["success"]++;
            }

            $ins_data[] = array(
                "MSG_ID"        => $result_data["msgid"],
                "MSG_TYPE"      => $msg_type,
                "STATUS"        => $result_status,
                "CALL_STATUS"   => $result_data["code"],
                "CALL_MSG"      => $result_data["error"],
                "REQUEST_TIME"  => $result_data["sendtime"],
                "SEND_TIME"     => $reserved_time,
                "SEND_NAME"     => isset($message_data["sender"]) ? addslashes($message_data["sender"]) : "",
                "SEND_PHONE"    => $message_data['sender_hp'],
                "DEST_NAME"     => isset($message_data["receiver"]) ? addslashes($message_data["receiver"]) : "",
                "DEST_PHONE"    => $message_data['receiver_hp'],
                "SUBJECT"       => isset($message_data['title']) ? addslashes($message_data['title']) : "'",
                "MSG_BODY"      => addslashes($message_data['content']),
                "NATION_CODE"   => "82",
                "SENDER_KEY"    => $sender_key,
                "CINFO"         => $message_data['cinfo'],
                "CINFO_DETAIL"  => isset($message_data['cinfo_detail']) ? $message_data['cinfo_detail'] : "NULL",
                "USER_ID"       => $sender_id
            );
        }

        if(!empty($ins_data)){
            $this->multiInsert($ins_data);
        }

        return $chk_result_data;
    }

    function sendKakaoMessage($message_list)
    {
        $sender_key     = $this->_easy_key;
        $sender_id      = $this->_easy_id;
        $send_url       = "https://alimtalk-api.sweettracker.net/v2/{$sender_key}/sendMessage";
        $send_time_val  = date("YmdHis");
        $send_data      = [];

        foreach($message_list as $message)
        {
            $reserved_time  = isset($message['reserved_time']) ? $message['reserved_time'] : $send_time_val;
            $send_tmp_data  = array(
                "msgid"         => $message['msg_id'],
                "message_type"  => $message['msg_type'],
                "profile_key"   => $sender_key,
                "template_code" => $message['temp_key'],
                "receiver_num"  => $message['receiver_hp'],
                "reserved_time" => $reserved_time,
                "message"       => $message['message'],
            );

            if(!empty($message['btn_content'])){
                $btn_tmp_data   = json_decode($message['btn_content'], true);

                foreach($btn_tmp_data as $key => $btn_data){
                    $send_tmp_data[$key] = $btn_data;
                }
            }

            $send_data[] = $send_tmp_data;
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $send_url);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Accept:application/json",
            "Content-Type: application/json",
            "userid: {$sender_id}"
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($send_data));

        $curl_result = curl_exec($curl);
        curl_close($curl);

        $result_data_list   = json_decode($curl_result, true);
        $ins_data           = [];
        $chk_result         = !empty($result_data_list) ? true : false;

        foreach($result_data_list as $result_data)
        {
            $message_data   = $message_list[$result_data["msgid"]];
            $msg_type       = $message_data['msg_type'];
            $reserved_time  = isset($message_data['reserved_time']) ? date("Y-m-d H:i:s", strtotime($message_data['reserved_time'])) : $result_data["sendtime"];

            if($result_data['result'] == "N"){
                $result_status = "3";
            }else{
                $result_status = "1";
            }

            $ins_data[] = array(
                "MSG_ID"        => $result_data["msgid"],
                "MSG_TYPE"      => $msg_type,
                "STATUS"        => $result_status,
                "CALL_STATUS"   => $result_data["code"],
                "CALL_MSG"      => $result_data["error"],
                "REQUEST_TIME"  => $result_data["sendtime"],
                "SEND_TIME"     => $reserved_time,
                "SEND_NAME"     => isset($message_data["sender"]) ? addslashes($message_data["sender"]) : "",
                "SEND_PHONE"    => $message_data['sender_hp'],
                "DEST_NAME"     => isset($message_data["receiver"]) ? addslashes($message_data["receiver"]) : "",
                "DEST_PHONE"    => $message_data['receiver_hp'],
                "SUBJECT"       => isset($message_data['title']) ? addslashes($message_data['title']) : "'",
                "MSG_BODY"      => addslashes($message_data['message']),
                "NATION_CODE"   => "82",
                "SENDER_KEY"    => $sender_key,
                "TEMPLATE_CODE" => $message_data['temp_key'],
                "BUTTON_KEY"    => $message_data['btn_key'],
                "CINFO"         => $message_data['cinfo'],
                "CINFO_DETAIL"  => isset($message_data['cinfo_detail']) ? $message_data['cinfo_detail'] : "NULL",
                "USER_ID"       => $sender_id
            );
        }

        if(!empty($ins_data)){
            $this->multiInsert($ins_data);
        }

        return $chk_result;
    }

    function checkMessage($check_list)
    {
        $sender_key     = $this->_easy_key;
        $sender_id      = $this->_easy_id;
        $report_time    = date("Y-m-d H:i:s");
        $send_url       = "https://alimtalk-api.sweettracker.net/v2/{$sender_key}/response";
        $send_data      = [];

        foreach($check_list as $check_data)
        {
            $send_data[]    = array("msgid" => $check_data['msg_id']);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $send_url);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Accept:application/json",
            "Content-Type: application/json",
            "userid: {$sender_id}"
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($send_data));

        $curl_result = curl_exec($curl);
        curl_close($curl);

        $result_data_list   = json_decode($curl_result, true);
        $upd_data           = [];

        foreach($result_data_list as $result_data)
        {
            $result_status = "2";
            if(!empty($result_data['error'])){
                $result_status = "3";
            }

            $upd_data[$result_data['msgid']] = array(
                "MSG_ID"        => $result_data['msgid'],
                "REPORT_TIME"   => $report_time,
                "STATUS"        => $result_status,
                "CALL_STATUS"   => $result_data['code'],
                "CALL_MSG"      => $result_data['error']
            );
        }

        foreach($check_list as $check_data)
        {
            $chk_msg_id = $check_data['msg_id'];

            if(!empty($chk_msg_id) && !isset($upd_data[$chk_msg_id])){
                $upd_data[$chk_msg_id] = array(
                    "MSG_ID"        => $chk_msg_id,
                    "REPORT_TIME"   => $report_time,
                    "STATUS"        => "4"
                );
            }
        }

        if(!empty($upd_data)){
            $this->multiUpdate($upd_data);
        }
    }
}
