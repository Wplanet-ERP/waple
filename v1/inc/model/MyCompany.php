<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class MyCompany extends Model
{
    protected $_main_table  = "my_company";
    protected $_main_key    = "my_c_no";

    private $_list          = [];
    private $_name_list     = [];

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new MyCompany();
    }

    public function setList()
    {
        $my_company_sql     = "SELECT * FROM {$this->_main_table} WHERE active_state='1' ORDER BY my_c_no ASC";
        $my_company_query   = mysqli_query($this->my_db, $my_company_sql);
        $my_company_list        = [];
        $my_company_name_list   = [];
        while($my_company = mysqli_fetch_assoc($my_company_query))
        {
            $my_company_list[$my_company['my_c_no']] = array('c_name' => $my_company['c_name'], 'color' => $my_company['kind_color'], 'initial' => $my_company['initial']);
            $my_company_name_list[$my_company['my_c_no']] = $my_company['c_name'];
        }

        $this->_list        = $my_company_list;
        $this->_name_list   = $my_company_name_list;
    }

    public function getList()
    {
        if(empty($this->_list))
        {
            $my_company_sql     = "SELECT * FROM {$this->_main_table} WHERE active_state='1' ORDER BY my_c_no ASC";
            $my_company_query   = mysqli_query($this->my_db, $my_company_sql);
            $this->_list        = [];
            while($my_company = mysqli_fetch_assoc($my_company_query))
            {
                $this->_list[$my_company['my_c_no']] = array('c_name' => $my_company['c_name'], 'color' => $my_company['kind_color'], 'initial' => $my_company['initial']);
            }
        }
        return $this->_list;
    }

    public function getNameList()
    {
        if(empty($this->_name_list))
        {
            $my_company_sql     = "SELECT * FROM {$this->_main_table} WHERE active_state='1' ORDER BY my_c_no ASC";
            $my_company_query   = mysqli_query($this->my_db, $my_company_sql);
            $this->_name_list   = [];
            while($my_company = mysqli_fetch_assoc($my_company_query))
            {
                $this->_name_list[$my_company['my_c_no']] = $my_company['c_name'];
            }
        }
        return $this->_name_list;
    }
}
