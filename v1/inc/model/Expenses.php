<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Expenses extends Model
{
    protected $_main_table      = "personal_expenses";
    protected $_main_key        = "pe_no";

    function __construct() {
        parent::__construct();
    }
    public static function Factory()
    {
        return new Expenses();
    }
}
