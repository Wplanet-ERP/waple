<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Deposit extends Model
{
    protected $_main_table      = "deposit";
    protected $_main_key        = "dp_no";

    protected $_confirm_table   = "deposit_confirm";
    protected $_confirm_key     = "dpc_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Deposit();
    }

    function checkDepositSlack($dp_no)
    {
        $result = false;
        $dp_subject = "";
        if(!empty($dp_no))
        {
            $dp_sql    = "SELECT dp_no, my_c_no, dp_subject FROM deposit WHERE `{$this->_main_key}`='{$dp_no}'";
            $dp_query  = mysqli_query($this->my_db, $dp_sql);
            $dp_result = mysqli_fetch_assoc($dp_query);

            if(isset($dp_result['dp_no']) && $dp_result['my_c_no'] == '3')
            {
                $result = true;
                $dp_subject = $dp_result['dp_subject'];
            }
        }

        return array("result" => $result, "dp_subject" => $dp_subject);
    }

    function getConfirmItem($dpc_no)
    {
        $dp_confirm_sql     = "SELECT * FROM deposit_confirm WHERE `dpc_no`='{$dpc_no}'";
        $dp_confirm_query   = mysqli_query($this->my_db, $dp_confirm_sql);
        $dp_confirm_result  = mysqli_fetch_assoc($dp_confirm_query);

        return $dp_confirm_result;
    }

    function updateConfirm($update_data)
    {
        $result = false;
        if(!empty($update_data))
        {
            $upd_sql = "UPDATE {$this->_confirm_table} SET ";
            $comma   = "";
            foreach($update_data as $key => $data){
                if($key != $this->_confirm_key){
                    if($data == 'NULL'){
                        $upd_sql .= "{$comma} `{$key}`=NULL";
                    }else{
                        $upd_sql .= "{$comma} `{$key}`='{$data}'";
                    }
                    $comma    = " , ";
                }
            }
            $upd_sql .= " WHERE `{$this->_confirm_key}`='{$update_data[$this->_confirm_key]}'";

            if(mysqli_query($this->my_db, $upd_sql)){
                $result = true;
            }
        }

        return $result;
    }

}
