<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Logistics extends Model
{
    protected $_main_table  = "logistics_management";
    protected $_prd_table   = "logistics_product";
    protected $_addr_table  = "logistics_address";

    protected $_main_key    = "lm_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Logistics();
    }

    public function getNewDocNo($prd_no)
    {
        $doc_date   = date('Ymd');
        $doc_sql    = "SELECT REPLACE(doc_no, '{$doc_date}-{$prd_no}-', '') as doc_idx FROM {$this->_main_table} WHERE doc_no LIKE '{$doc_date}-{$prd_no}-%' ORDER BY doc_no DESC LIMIT 1";
        $doc_query  = mysqli_query($this->my_db, $doc_sql);
        $doc_result = mysqli_fetch_assoc($doc_query);

        $last_doc_idx = (isset($doc_result['doc_idx']) && !empty($doc_result['doc_idx'])) ? (int)$doc_result['doc_idx']+1 : 1;
        $doc_no       = $doc_date."-{$prd_no}-".sprintf('%03d', $last_doc_idx);

        return $doc_no;
    }

    public function getNewDocIdx($prd_no)
    {
        $doc_date   = date('Ymd');
        $doc_sql    = "SELECT REPLACE(doc_no, '{$doc_date}-{$prd_no}-', '') as doc_idx FROM {$this->_main_table} WHERE doc_no LIKE '{$doc_date}-{$prd_no}-%' ORDER BY doc_no DESC LIMIT 1";
        $doc_query  = mysqli_query($this->my_db, $doc_sql);
        $doc_result = mysqli_fetch_assoc($doc_query);

        $last_doc_idx = (isset($doc_result['doc_idx']) && !empty($doc_result['doc_idx'])) ? (int)$doc_result['doc_idx']+1 : 1;

        return $last_doc_idx;
    }

    public function getLastMulNo()
    {
        $cur_date  = date('Ymd');

        $last_mul_ord_no_sql    = "SELECT REPLACE(order_number, '{$cur_date}_MUL_', '') as ord_no FROM work_cms WHERE order_number LIKE '%{$cur_date}_MUL_%' ORDER BY order_number DESC LIMIT 1";
        $last_mul_ord_no_query  = mysqli_query($this->my_db, $last_mul_ord_no_sql);
        $last_mul_ord_no_result = mysqli_fetch_assoc($last_mul_ord_no_query);

        $last_mul_ord_no = (isset($last_mul_ord_no_result['ord_no']) && !empty($last_mul_ord_no_result['ord_no'])) ? (int)$last_mul_ord_no_result['ord_no'] : 0;

        return $last_mul_ord_no;
    }

    public function saveProduct($lm_no, $prd_type, $origin_product, $products_data)
    {
        if($prd_type == '1'){
            $del_sql = "DELETE FROM {$this->_prd_table} WHERE `{$this->_main_key}`='{$lm_no}' AND prd_type='2'";
        }elseif($prd_type == '2'){
            $del_sql = "DELETE FROM {$this->_prd_table} WHERE `{$this->_main_key}`='{$lm_no}' AND prd_type='1'";
        }
        mysqli_query($this->my_db, $del_sql);

        if(!empty($products_data))
        {
            $upd_key  = "lp_no";
            $upd_list = [];
            foreach($products_data as $product_data)
            {
                if(!empty($product_data[$upd_key]))
                {
                    $upd_sql = "UPDATE {$this->_prd_table} SET ";
                    $comma   = "";
                    foreach($product_data as $key => $data){
                        if($key != $upd_key){
                            if($data == 'NULL'){
                                $upd_sql .= "{$comma} `{$key}`=NULL";
                            }else{
                                $upd_sql .= "{$comma} `{$key}`='{$data}'";
                            }
                            $comma    = " , ";
                        }
                    }
                    $upd_sql    .= " WHERE `{$upd_key}`='{$product_data[$upd_key]}'";
                    $upd_list[]  = $product_data[$upd_key];
                    mysqli_query($this->my_db, $upd_sql);
                }
                else
                {
                    $ins_sql = "INSERT INTO {$this->_prd_table} SET ";
                    $comma   = "";
                    foreach($product_data as $key => $data){
                        if($key != $upd_key){
                            if($data == 'NULL'){
                                $ins_sql .= "{$comma} `{$key}`=NULL";
                            }else{
                                $ins_sql .= "{$comma} `{$key}`='{$data}'";
                            }
                            $comma    = " , ";
                        }
                    }

                    mysqli_query($this->my_db, $ins_sql);
                }
            }

            if(!empty($origin_product))
            {
                $diff_list = array_diff($origin_product, $upd_list);
                if(!empty($diff_list)){
                    foreach($diff_list as $diff_no){
                        $diff_sql = "DELETE FROM {$this->_prd_table} WHERE `{$upd_key}`='{$diff_no}'";
                        mysqli_query($this->my_db, $diff_sql);
                    }
                }
            }
        }else{
            $this->deleteProduct($lm_no);
        }
    }

    public function deleteProduct($lm_no)
    {
        $del_sql = "DELETE FROM {$this->_prd_table} WHERE `{$this->_main_key}` ='{$lm_no}'";
        mysqli_query($this->my_db, $del_sql);
    }

    public function saveAddress($lm_no, $origin_address, $address_data)
    {
        if(!empty($address_data))
        {
            $upd_key  = "la_no";
            $upd_list = [];
            foreach($address_data as $address)
            {
                if(!empty($address[$upd_key]))
                {
                    $upd_sql = "UPDATE {$this->_addr_table} SET ";
                    $comma   = "";
                    foreach($address as $key => $data){
                        if($key != $upd_key){
                            if($data == 'NULL'){
                                $upd_sql .= "{$comma} `{$key}`=NULL";
                            }else{
                                $upd_sql .= "{$comma} `{$key}`='{$data}'";
                            }
                            $comma    = " , ";
                        }
                    }
                    $upd_sql    .= " WHERE `{$upd_key}`='{$address[$upd_key]}'";
                    $upd_list[]  = $address[$upd_key];
                    mysqli_query($this->my_db, $upd_sql);
                }
                else
                {
                    $ins_sql = "INSERT INTO {$this->_addr_table} SET ";
                    $comma   = "";
                    foreach($address as $key => $data){
                        if($key != $upd_key){
                            if($data == 'NULL'){
                                $ins_sql .= "{$comma} `{$key}`=NULL";
                            }else{
                                $ins_sql .= "{$comma} `{$key}`='{$data}'";
                            }
                            $comma    = " , ";
                        }
                    }

                    mysqli_query($this->my_db, $ins_sql);
                }
            }

            if(!empty($origin_address))
            {
                $diff_list = array_diff($origin_address, $upd_list);
                if(!empty($diff_list)){
                    foreach($diff_list as $diff_no){
                        $diff_sql = "DELETE FROM {$this->_addr_table} WHERE `{$upd_key}`='{$diff_no}'";
                        mysqli_query($this->my_db, $diff_sql);
                    }
                }
            }
        }else{
            $this->deleteAddress($lm_no);
        }
    }

    public function deleteAddress($lm_no)
    {
        $del_sql = "DELETE FROM {$this->_addr_table} WHERE `{$this->_main_key}`='{$lm_no}'";
        mysqli_query($this->my_db, $del_sql);
    }

    public function getLogisticsByDoc($doc)
    {
        $chk_sql    = "SELECT * FROM logistics_management WHERE doc_no='{$doc}'";
        $chk_query  = mysqli_query($this->my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        return isset($chk_result['lm_no']) ? $chk_result : [];
    }

    public function delLogisticsTotal($lm_no)
    {
        $this->setMainInit("logistics_management", "lm_no");
        if($this->delete($lm_no)){
            $this->deleteAddress($lm_no);
            $this->deleteProduct($lm_no);
            return true;
        }else{
            return false;
        }
    }
}
