<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Promotion extends Model
{
    protected $_main_table          = "promotion";
    protected $_application_table   = "application";
    protected $_report_table        = "report";

    protected $_main_key            = "p_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Promotion();
    }
}
