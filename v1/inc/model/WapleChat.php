<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class WapleChat extends Model
{
    protected $_main_table      = "waple_chat";
    protected $_user_table      = "waple_chat_user";
    protected $_content_table   = "waple_chat_content";
    protected $_read_table      = "waple_chat_read";
    protected $_main_key        = "wc_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new WapleChat();
    }

    public function readWapleChat($wcc_no, $s_no)
    {
        $ins_sql = "INSERT INTO {$this->_read_table} SET wcc_no='{$wcc_no}', read_s_no='{$s_no}', read_date=now()";

        mysqli_query($this->my_db, $ins_sql);
    }
}
