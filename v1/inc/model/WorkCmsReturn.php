<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class WorkCmsReturn extends Model
{
    protected $_main_table  = "work_cms_return";
    protected $_main_key    = "r_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new WorkCmsReturn();
    }

    public function getOrderItem($ord_no)
    {
        $item_sql       = "SELECT * FROM {$this->_main_table} WHERE order_number='{$ord_no}' ORDER BY `{$this->_main_key}` ASC LIMIT 1";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function getLastRmaNo()
    {
        $cur_date  = date('Ymd');

        $last_rma_ord_no_sql    = "SELECT REPLACE(order_number, '{$cur_date}_RMA_', '') as ord_no FROM {$this->_main_table} WHERE order_number LIKE '%{$cur_date}_RMA_%' ORDER BY order_number DESC LIMIT 1";
        $last_rma_ord_no_query  = mysqli_query($this->my_db, $last_rma_ord_no_sql);
        $last_rma_ord_no_result = mysqli_fetch_assoc($last_rma_ord_no_query);

        $last_rma_ord_no = (isset($last_rma_ord_no_result['ord_no']) && !empty($last_rma_ord_no_result['ord_no'])) ? (int)$last_rma_ord_no_result['ord_no'] : 0;

        return $last_rma_ord_no;
    }
}
