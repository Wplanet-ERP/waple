<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class WorkCms extends Model
{
    protected $_main_table  = "work_cms";
    protected $_main_key    = "w_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new WorkCms();
    }

    public function updateOrder($order_number, $update_data)
    {
        $result = false;
        if(!empty($update_data))
        {
            $upd_sql = "UPDATE {$this->_main_table} SET ";
            $comma   = "";
            foreach($update_data as $key => $data){
                if($key != "order_number" && $key != $this->_main_key){
                    if((string)$data == 'NULL'){
                        $upd_sql .= "{$comma} `{$key}`=NULL";
                    }else{
                        $upd_sql .= "{$comma} `{$key}`='{$data}'";
                    }
                    $comma    = " , ";
                }
            }
            $upd_sql .= " WHERE order_number='{$order_number}'";

            if(mysqli_query($this->my_db, $upd_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function getOrderItem($ord_no)
    {
        $item_sql       = "SELECT * FROM {$this->_main_table} WHERE order_number='{$ord_no}' ORDER BY `{$this->_main_key}` ASC LIMIT 1";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function getWithOrderItem($ord_no)
    {
        $item_sql       = "SELECT * FROM work_cms WHERE order_number='{$ord_no}' AND log_c_no='2809' ORDER BY `{$this->_main_key}` ASC LIMIT 1";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function getTempWithOrderItem($ord_no)
    {
        $item_sql       = "SELECT * FROM work_cms_temporary WHERE order_number='{$ord_no}' AND log_c_no='2809' ORDER BY `{$this->_main_key}` ASC LIMIT 1";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function getLastCsNo()
    {
        $cur_date  = date('Ymd');

        $last_cs_ord_no_sql    = "SELECT REPLACE(order_number, '{$cur_date}_CS_', '') as ord_no FROM {$this->_main_table} WHERE order_number LIKE '%{$cur_date}_CS_%' ORDER BY order_number DESC LIMIT 1";
        $last_cs_ord_no_query  = mysqli_query($this->my_db, $last_cs_ord_no_sql);
        $last_cs_ord_no_result = mysqli_fetch_assoc($last_cs_ord_no_query);

        $last_cs_ord_no = (isset($last_cs_ord_no_result['ord_no']) && !empty($last_cs_ord_no_result['ord_no'])) ? (int)$last_cs_ord_no_result['ord_no'] : 0;

        return $last_cs_ord_no;
    }

    public function getSpecificLastCsNo($set_date)
    {
        $cur_date  = date('Ymd', strtotime($set_date));

        $last_cs_ord_no_sql    = "SELECT REPLACE(order_number, '{$cur_date}_CS_', '') as ord_no FROM {$this->_main_table} WHERE order_number LIKE '%{$cur_date}_CS_%' ORDER BY order_number DESC LIMIT 1";
        $last_cs_ord_no_query  = mysqli_query($this->my_db, $last_cs_ord_no_sql);
        $last_cs_ord_no_result = mysqli_fetch_assoc($last_cs_ord_no_query);

        $last_cs_ord_no = (isset($last_cs_ord_no_result['ord_no']) && !empty($last_cs_ord_no_result['ord_no'])) ? (int)$last_cs_ord_no_result['ord_no'] : 0;

        return $last_cs_ord_no;
    }

    public function chkExistOrder($add_where)
    {
        $ord_chk_sql    = "SELECT COUNT(w_no) as cnt FROM work_cms WHERE {$add_where}";
        $ord_chk_query  = mysqli_query($this->my_db, $ord_chk_sql);
        $ord_chk_result = mysqli_fetch_assoc($ord_chk_query);

        return ($ord_chk_result['cnt'] > 0) ? true : false;
    }

    public function getLastQuickNo()
    {
        $cur_date  = date('Ymd');

        $last_cs_ord_no_sql    = "SELECT REPLACE(order_number, '{$cur_date}_GDS_', '') as ord_no FROM work_cms_quick WHERE order_number LIKE '%{$cur_date}_GDS_%' ORDER BY order_number DESC LIMIT 1";
        $last_cs_ord_no_query  = mysqli_query($this->my_db, $last_cs_ord_no_sql);
        $last_cs_ord_no_result = mysqli_fetch_assoc($last_cs_ord_no_query);

        $last_cs_ord_no = (isset($last_cs_ord_no_result['ord_no']) && !empty($last_cs_ord_no_result['ord_no'])) ? (int)$last_cs_ord_no_result['ord_no'] : 0;

        return $last_cs_ord_no;
    }

    public function chkExistDeliveryNo($ord_no, $delivery_no)
    {
        $deli_chk_sql    = "SELECT count(`no`) as cnt FROM work_cms_delivery WHERE order_number !='{$ord_no}' AND delivery_no = '{$delivery_no}'";
        $deli_chk_query  = mysqli_query($this->my_db, $deli_chk_sql);
        $deli_chk_result = mysqli_fetch_assoc($deli_chk_query);

        if(isset($deli_chk_result['cnt']) && $deli_chk_result['cnt'] > 0) {
            return true;
        }

        return false;
    }
}
