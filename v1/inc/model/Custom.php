<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Custom extends Model
{
    protected $_main_table = "";
    protected $_main_key   = "";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Custom();
    }
}
