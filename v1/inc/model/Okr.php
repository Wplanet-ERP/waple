<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Okr extends Model
{
    protected $_main_table = "okr_obj";
    protected $_main_key   = "oo_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Okr();
    }

    public function setOkrKr(){
        $this->_main_table  = "okr_kr";
        $this->_main_key    = "ok_no";
    }
}
