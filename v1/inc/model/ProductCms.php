<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class ProductCms extends Model
{
    protected $_main_table  = "product_cms";
    protected $_main_key    = "prd_no";

    protected $_load_table  = "product_cms_load";
    protected $_load_key    = "pl_no";

    protected $_load_unit_table  = "product_cms_load_unit";
    protected $_load_unit_key    = "plu_no";

    protected $_outsourcing_table  = "product_cms_outsourcing";
    protected $_outsourcing_key    = "no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new ProductCms();
    }

    public function checkDuplicate($prd_code)
    {
        $prd_dup_sql    = "SELECT COUNT(prd_no) as cnt FROM product_cms WHERE prd_code='{$prd_code}'";
        $prd_dup_query  = mysqli_query($this->my_db, $prd_dup_sql);
        $prd_dup_result = mysqli_fetch_assoc($prd_dup_query);

        return isset($prd_dup_result['cnt']) ? $prd_dup_result['cnt'] : 0;
    }

    public function getPrdGroupChartData($kind_group_code)
    {
        $prd_data_sql           = "SELECT prd_no, title, prd_type, display, k_name_code, (SELECT k.k_name FROM kind k WHERE k.k_name_code=p.k_name_code) as k_name, (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub.k_parent FROM kind sub WHERE sub.k_name_code=p.k_name_code)) as k_parent_name  FROM {$this->_main_table} as p WHERE k_name_code IN({$kind_group_code})";
        $prd_data_query         = mysqli_query($this->my_db, $prd_data_sql);
        $prd_data_list          = [];
        $prd_total_list         = [];
        $prd_only_total_list    = [];
        $prd_name_list          = [];
        $prd_info_list          = [];

        while($prd_data = mysqli_fetch_assoc($prd_data_query))
        {
            $prd_total_list[$prd_data['k_name_code']][] = array(
                'prd_no'        => trim($prd_data['prd_no']),
                'title'         => trim($prd_data['title']),
                'k_name_code'   => trim($prd_data['k_name_code'])
            );

            if($prd_data['prd_type'] != "2" && $prd_data['display'] == "1"){
                $prd_only_total_list[$prd_data['k_name_code']][] = array(
                    'prd_no'        => trim($prd_data['prd_no']),
                    'title'         => trim($prd_data['title']),
                    'k_name_code'   => trim($prd_data['k_name_code'])
                );
            }

            $prd_info_list[$prd_data['prd_no']] = array("title" => $prd_data['title'], "group_name" => $prd_data['k_parent_name']." > ".$prd_data['k_name']);
            $prd_name_list[$prd_data['prd_no']] = $prd_data['title'];
        }

        $prd_data_list['prd_total']         = $prd_total_list;
        $prd_data_list['prd_only_total']    = $prd_only_total_list;
        $prd_data_list['prd_name']          = $prd_name_list;
        $prd_data_list['prd_info_list']     = $prd_info_list;

        return $prd_data_list;
    }

    public function getPrdGroupData()
    {
        $prd_total_list= [];
        $prd_total_sql = "
            SELECT
                prd_no,
                title,
                k_name_code
            FROM {$this->_main_table} p
            WHERE k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_code='product_cms' AND sub.display='1' ORDER BY priority ASC) AND display='1'
        ";

        $prd_total_query = mysqli_query($this->my_db, $prd_total_sql);
        while($prd_data  = mysqli_fetch_array($prd_total_query))
        {
            $prd_total_list[$prd_data['k_name_code']][] = array(
                'prd_no'        => trim($prd_data['prd_no']),
                'title'         => trim($prd_data['title']),
                'k_name_code'   => trim($prd_data['k_name_code'])
            );
        }

        return $prd_total_list;
    }

    public function getPrdBrandOption()
    {
        $brand_sql      = "SELECT DISTINCT c_no, (SELECT c.c_name FROM company `c` WHERE `c`.c_no=main.c_no) as c_name FROM {$this->_main_table} as main WHERE main.c_no > 0 AND main.display='1' ORDER BY c_name ASC";
        $brand_query    = mysqli_query($this->my_db, $brand_sql);
        $brand_option   = [];
        while($brand_result = mysqli_fetch_assoc($brand_query))
        {
            $brand_option[$brand_result['c_no']] = $brand_result['c_name'];
        }

        return $brand_option;
    }

    public function getLoadBrandOption()
    {
        $brand_sql      = "SELECT DISTINCT brand, (SELECT `c`.c_name FROM company as `c` WHERE `c`.c_no=main.brand) as brand_name FROM {$this->_load_table} as main ORDER BY brand_name ASC";
        $brand_query    = mysqli_query($this->my_db, $brand_sql);
        $brand_option   = [];
        while($brand_result = mysqli_fetch_assoc($brand_query))
        {
            $brand_option[$brand_result['brand']] = $brand_result['brand_name'];
        }

        return $brand_option;
    }

    public function getWorkCmsItem($value, $type="prd_no")
    {
        $item_sql       = "SELECT prd_cms.prd_no, prd_cms.title, prd_cms.manager, (SELECT s.team FROM staff s WHERE s.s_no = prd_cms.manager) as team, prd_cms.c_no, (SELECT c.c_name FROM company c WHERE c.c_no = prd_cms.c_no) as c_name FROM {$this->_main_table} as prd_cms WHERE `{$type}`='{$value}'";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function getRelationMaxPriority($prd_no)
    {
        $priority_sql       = "SELECT MAX(pcr.priority) as max_priority FROM product_cms_relation pcr WHERE pcr.prd_no = '{$prd_no}'";
        $priority_query     = mysqli_query($this->my_db, $priority_sql);
        $priority_result    = mysqli_fetch_assoc($priority_query);
        $max_priority       = empty($priority_result['max_priority']) ? 1 : $priority_result['max_priority']+1;

        return $max_priority;
    }

    public function setDuplicateRelation($new_prd_no, $prd_no)
    {
        $result = false;
        $dup_relation_sql = "INSERT INTO product_cms_relation(`prd_no`, `option_no`, `log_c_no`, `quantity`, `priority`, `display`) (SELECT '{$new_prd_no}', `option_no`, `log_c_no`, `quantity`, `priority`, `display` FROM product_cms_relation WHERE prd_no = '{$prd_no}' AND display='1')";
        if(mysqli_query($this->my_db, $dup_relation_sql)){
            $result = true;
        }

        return $result;
    }

    public function checkLoadUnit($pl_no, $chk_units)
    {
        $chk_sql = "
            SELECT 
               count({$this->_load_unit_key}) as cnt 
            FROM {$this->_load_unit_table} as plu 
            LEFT JOIN {$this->_load_table} as pl ON pl.`{$this->_load_key}`=plu.`{$this->_load_key}` 
            WHERE pl.display='1' AND plu.`{$this->_load_key}` != '{$pl_no}' AND unit_no IN({$chk_units})
        ";
        $chk_query  = mysqli_query($this->my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        return ($chk_result['cnt'] > 0) ? false : true;
    }

    public function insertLoad($insert_data)
    {
        $result = false;
        if(!empty($insert_data))
        {
            $ins_sql = "INSERT INTO {$this->_load_table} SET ";
            $comma   = "";
            foreach($insert_data as $key => $data)
            {
                if ($key != $this->_load_key) {
                    if ($data == 'NULL') {
                        $ins_sql .= "{$comma} `{$key}`=NULL";
                    } else {
                        $ins_sql .= "{$comma} `{$key}`='{$data}'";
                    }
                    $comma = " , ";
                }
            }

            if(mysqli_query($this->my_db, $ins_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function updateLoad($update_data)
    {
        $result = false;
        if(!empty($update_data))
        {
            $upd_sql = "UPDATE {$this->_load_table} SET ";
            $comma   = "";
            foreach($update_data as $key => $data){
                if($key != $this->_load_key){
                    if($data == 'NULL'){
                        $upd_sql .= "{$comma} `{$key}`=NULL";
                    }else{
                        $upd_sql .= "{$comma} `{$key}`='{$data}'";
                    }
                    $comma    = " , ";
                }
            }
            $upd_sql .= " WHERE `{$this->_load_key}`='{$update_data[$this->_load_key]}'";

            if(mysqli_query($this->my_db, $upd_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function insertLoadUnit($insert_data)
    {
        $result = false;
        if(!empty($insert_data))
        {
            $ins_sql = "INSERT INTO {$this->_load_unit_table} SET ";
            $comma   = "";
            foreach($insert_data as $key => $data)
            {
                if ($key != $this->_load_unit_key) {
                    if ($data == 'NULL') {
                        $ins_sql .= "{$comma} `{$key}`=NULL";
                    } else {
                        $ins_sql .= "{$comma} `{$key}`='{$data}'";
                    }
                    $comma = " , ";
                }
            }

            if(mysqli_query($this->my_db, $ins_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function updateLoadUnit($update_data)
    {
        $result = false;
        if(!empty($update_data))
        {
            $upd_sql = "UPDATE {$this->_load_unit_table} SET ";
            $comma   = "";
            foreach($update_data as $key => $data){
                if($key != $this->_load_unit_key){
                    if($data == 'NULL'){
                        $upd_sql .= "{$comma} `{$key}`=NULL";
                    }else{
                        $upd_sql .= "{$comma} `{$key}`='{$data}'";
                    }
                    $comma    = " , ";
                }
            }
            $upd_sql .= " WHERE `{$this->_load_unit_key}`='{$update_data[$this->_load_unit_key]}'";

            if(mysqli_query($this->my_db, $upd_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function deleteLoadUnit($del_val)
    {
        $result = false;

        if(!empty($del_val))
        {
            $del_sql = "DELETE FROM {$this->_load_unit_table} WHERE `{$this->_load_unit_key}`='{$del_val}'";

            if(mysqli_query($this->my_db, $del_sql)){
                $result = true;
            }
        }

        return $result;
    }

    public function getOutsourcingItem($c_no, $prd_code)
    {
        $out_item_sql       = "SELECT * FROM product_cms_outsourcing WHERE c_no='{$c_no}' AND `value`='{$prd_code}' LIMIT 1";
        $out_item_query     = mysqli_query($this->my_db, $out_item_sql);
        $out_item_result    = mysqli_fetch_assoc($out_item_query);
        $out_item           = isset($out_item_result['prd_no']) ? $out_item_result : [];

        return $out_item;
    }

    public function saveOutsourcingData($prd_no, $origin_out_data, $outsourcing_data)
    {
        if(!empty($outsourcing_data))
        {
            $upd_list = [];
            foreach($outsourcing_data as $out_data)
            {
                if(!empty($out_data[$this->_outsourcing_key]))
                {
                    $upd_sql = "UPDATE {$this->_outsourcing_table} SET ";
                    $comma   = "";
                    foreach($out_data as $key => $data){
                        if($key != $this->_outsourcing_key){
                            if($data == 'NULL'){
                                $upd_sql .= "{$comma} `{$key}`=NULL";
                            }else{
                                $upd_sql .= "{$comma} `{$key}`='{$data}'";
                            }
                            $comma    = " , ";
                        }
                    }
                    $upd_sql    .= " WHERE `{$this->_outsourcing_key}`='{$out_data[$this->_outsourcing_key]}'";
                    $upd_list[]  = $out_data[$this->_outsourcing_key];
                    mysqli_query($this->my_db, $upd_sql);
                }
                else
                {
                    $ins_sql = "INSERT INTO {$this->_outsourcing_table} SET ";
                    $comma   = "";
                    foreach($out_data as $key => $data){
                        if($key != $this->_outsourcing_key){
                            if($data == 'NULL'){
                                $ins_sql .= "{$comma} `{$key}`=NULL";
                            }else{
                                $ins_sql .= "{$comma} `{$key}`='{$data}'";
                            }
                            $comma    = " , ";
                        }
                    }

                    mysqli_query($this->my_db, $ins_sql);
                }
            }

            if(!empty($origin_out_data))
            {
                $diff_list = array_diff($origin_out_data, $upd_list);
                if(!empty($diff_list)){
                    foreach($diff_list as $diff_no){
                        $diff_sql = "DELETE FROM {$this->_outsourcing_table} WHERE `{$this->_outsourcing_key}`='{$diff_no}'";
                        mysqli_query($this->my_db, $diff_sql);
                    }
                }
            }
        }else{
            $this->deleteOutsourcingData($prd_no);
        }
    }

    public function deleteOutsourcingData($prd_no)
    {
        $del_sql = "DELETE FROM {$this->_outsourcing_table} WHERE prd_no='{$prd_no}'";
        mysqli_query($this->my_db, $del_sql);
    }

    public function checkUnitLogCompany($prd_no)
    {
        $chk_sql    = "SELECT log_c_no, COUNT(DISTINCT log_c_no) as cnt FROM product_cms_relation WHERE prd_no='{$prd_no}' AND display='1'";
        $chk_query  = mysqli_query($this->my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        if(!empty($chk_result['log_c_no']))
        {
            $chk_log_c_no   = ($chk_result['cnt'] == '1') ?$chk_result['log_c_no'] : 0;
            $upd_sql        = "UPDATE product_cms SET log_c_no='{$chk_log_c_no}' WHERE prd_no='{$prd_no}'";

            mysqli_query($this->my_db, $upd_sql);
        }
    }
}
