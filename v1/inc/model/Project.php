<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Project extends Model
{
    protected $_main_table      = "project";
    protected $_main_key        = "pj_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Project();
    }

    public function setExpensesTable(){
        $this->_main_table  = "project_expenses";
        $this->_main_key    = "pj_e_no";
    }
}
