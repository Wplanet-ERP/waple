<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class ProductCmsUnit extends Model
{
    protected $_main_table = "product_cms_unit";
    protected $_main_key   = "no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new ProductCmsUnit();
    }

    public function getBaseItem($no)
    {
        $unit_sql       = "
            SELECT 
                pcu.*
            FROM product_cms_unit pcu
            WHERE pcu.no='{$no}'
        ";
        $unit_query     = mysqli_query($this->my_db, $unit_sql);
        $unit_result    = mysqli_fetch_assoc($unit_query);

        return !empty($unit_result) ? $unit_result : [];
    }

    public function getItem($no, $log_c_no='2809')
    {
        $unit_sql       = "
            SELECT 
                pcu.*,
                pcum.log_c_no,
                pcum.sku,
                pcum.warehouse,
                pcum.qty_alert,
                pcum.qty_hp,
                (SELECT c.c_name FROM company c WHERE c.c_no=pcu.brand) as brand_name 
            FROM product_cms_unit pcu
            LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=pcu.`no`
            WHERE pcum.log_c_no='{$log_c_no}' AND pcu.no='{$no}'
        ";
        $unit_query     = mysqli_query($this->my_db, $unit_sql);
        $unit_result    = mysqli_fetch_assoc($unit_query);

        return !empty($unit_result) ? $unit_result : [];
    }

    public function getSkuItem($sku, $log_c_no)
    {
        $unit_sql       = "
            SELECT 
                pcu.*,
                pcum.*,
                (SELECT c.c_name FROM company c WHERE c.c_no=pcu.brand) as brand_name,
                (SELECT c.c_name FROM company c WHERE c.c_no=pcu.sup_c_no) as sup_c_name   
            FROM product_cms_unit pcu
            LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=pcu.`no`
            WHERE pcum.log_c_no='{$log_c_no}' AND pcum.sku='{$sku}'
        ";
        $unit_query     = mysqli_query($this->my_db, $unit_sql);
        $unit_result    = mysqli_fetch_assoc($unit_query);

        return !empty($unit_result) ? $unit_result : [];
    }

    public function getNameItem($option_name, $log_c_no)
    {
        $unit_sql       = "
            SELECT 
                pcu.*,
                pcum.*,
                (SELECT c.c_name FROM company c WHERE c.c_no=pcu.brand) as brand_name 
            FROM product_cms_unit pcu
            LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=pcu.`no`
            WHERE pcum.log_c_no='{$log_c_no}' AND pcu.option_name='{$option_name}'
        ";
        $unit_query     = mysqli_query($this->my_db, $unit_sql);
        $unit_result    = mysqli_fetch_assoc($unit_query);

        return !empty($unit_result) ? $unit_result : [];
    }

    public function getUnitList()
    {
        $unit_sql      = "SELECT `no`, option_name FROM product_cms_unit pcu WHERE pcu.display=1";
        $unit_query    = mysqli_query($this->my_db, $unit_sql);
        $unit_list     = [];
        while($unit = mysqli_fetch_assoc($unit_query))
        {
            $unit_list[$unit['no']] = $unit['option_name'];
        }

        return $unit_list;
    }

    public function getChartUnitData()
    {
        $sup_c_sql      = "SELECT Distinct pcu.sup_c_no, pcu.ord_s_no, pcu.ord_sub_s_no, pcu.ord_third_s_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no), '(주)','') as c_name FROM product_cms_unit pcu WHERE pcu.display=1 ORDER BY c_name ASC";
        $sup_c_query    = mysqli_query($this->my_db, $sup_c_sql);
        $sup_c_init     = [];
        $sup_ord_c_list = [];
        $chart_total_list = [];

        while($sup_c = mysqli_fetch_assoc($sup_c_query))
        {
            $sup_c_init[$sup_c['sup_c_no']]  = array('title' => $sup_c['c_name'], 'imp_qty' => 0, 'exp_qty' => 0,'deli_qty' => 0, 'total_qty' => 0);
            $sup_ord_c_list[$sup_c['ord_s_no']][$sup_c['sup_c_no']] = $sup_c['c_name'];

            if(!empty($sup_c['ord_sub_s_no'])){
                $sup_ord_c_list[$sup_c['ord_sub_s_no']][$sup_c['sup_c_no']] = $sup_c['c_name'];
            }

            if(!empty($sup_c['ord_third_s_no'])){
                $sup_ord_c_list[$sup_c['ord_third_s_no']][$sup_c['sup_c_no']] = $sup_c['c_name'];
            }
        }

        $chart_total_list['sup_c_init']     = $sup_c_init;
        $chart_total_list['sup_ord_c_list'] = $sup_ord_c_list;

        return $chart_total_list;
    }

    public function getDistinctUnitCompanyData($type)
    {
        $unit_sql      = "SELECT Distinct pcu.{$type}, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.{$type}), '(주)','') as c_name FROM product_cms_unit pcu WHERE pcu.display=1 ORDER BY c_name ASC";
        $unit_query    = mysqli_query($this->my_db, $unit_sql);
        $unit_list     = [];
        while($unit = mysqli_fetch_assoc($unit_query))
        {
            $unit_list[$unit[$type]] = $unit['c_name'];
        }

        return $unit_list;
    }

    public function getOrdStaffData()
    {
        $unit_sql      = "SELECT pcu.ord_s_no, pcu.ord_sub_s_no, pcu.ord_third_s_no, (SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_s_no) as s_name, (SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_sub_s_no) as sub_s_name, (SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_third_s_no) as third_s_name FROM product_cms_unit pcu WHERE pcu.display=1 ORDER BY s_name ASC";
        $unit_query    = mysqli_query($this->my_db, $unit_sql);
        $unit_list     = [];
        while($unit = mysqli_fetch_assoc($unit_query))
        {
            $unit_list[$unit['ord_s_no']] = $unit['s_name'];
            if(!empty($unit['ord_sub_s_no']))
            {
                $unit_list[$unit['ord_sub_s_no']] = $unit['sub_s_name'];
            }

            if(!empty($unit['ord_third_s_no']))
            {
                $unit_list[$unit['ord_third_s_no']] = $unit['third_s_name'];
            }
        }

        return $unit_list;
    }

    public function getDistinctUnitOrdCompanyData($type)
    {
        $unit_sql   = "SELECT Distinct pcu.{$type}, pcu.ord_s_no, pcu.ord_sub_s_no, pcu.ord_third_s_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.{$type}), '(주)','') as c_name FROM product_cms_unit pcu WHERE pcu.display=1 ORDER BY c_name ASC";
        $unit_query = mysqli_query($this->my_db, $unit_sql);
        $unit_list  = [];

        while($unit_data = mysqli_fetch_assoc($unit_query))
        {
            if(!empty($unit_data['c_name'])){
                $unit_list[$unit_data['ord_s_no']][$unit_data[$type]] = $unit_data['c_name'];

                if(!empty($unit_data['ord_sub_s_no'])){
                    $unit_list[$unit_data['ord_sub_s_no']][$unit_data[$type]] = $unit_data['c_name'];
                }

                if(!empty($unit_data['ord_third_s_no'])){
                    $unit_list[$unit_data['ord_third_s_no']][$unit_data[$type]] = $unit_data['c_name'];
                }
            }
        }

        return $unit_list;
    }

    public function getDistinctUnitOrdCompanyLocData($type)
    {
        $unit_sql   = "SELECT Distinct pcu.{$type}, pcu.ord_s_no, pcu.ord_sub_s_no, pcu.ord_third_s_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.{$type}), '(주)','') as c_name, (SELECT c.license_type FROM company c WHERE c.c_no = pcu.sup_c_no) as loc_type FROM product_cms_unit pcu WHERE pcu.display=1 ORDER BY c_name ASC";
        $unit_query = mysqli_query($this->my_db, $unit_sql);
        $unit_list  = [];

        while($unit_data = mysqli_fetch_assoc($unit_query))
        {
            $loc_type = ($unit_data['loc_type'] == '4') ? "2" : "1";
            $loc_text = ($unit_data['loc_type'] == '4') ? "해외" : "국내";

            $unit_list[$unit_data['ord_s_no']][$unit_data['sup_c_no']] = array('name' => $unit_data['c_name'], 'loc_type' => $loc_type, 'loc_text' => $loc_text);

            if(!empty($unit_data['ord_sub_s_no'])){
                $unit_list[$unit_data['ord_sub_s_no']][$unit_data['sup_c_no']] = array('name' => $unit_data['c_name'], 'loc_type' => $loc_type, 'loc_text' => $loc_text);
            }

            if(!empty($unit_data['ord_third_s_no'])){
                $unit_list[$unit_data['ord_third_s_no']][$unit_data['sup_c_no']] = array('name' => $unit_data['c_name'], 'loc_type' => $loc_type, 'loc_text' => $loc_text);
            }
        }

        return $unit_list;
    }

    public function getBrandData()
    {
        $cms_group_list = [];
        $cms_group_sql  = "
            SELECT 
                DISTINCT pcu.brand,
                (SELECT c.c_name FROM company c WHERE c.c_no=pcu.brand) as brand_name
            FROM product_cms_unit pcu
            WHERE display='1'
        ";
        $cms_group_query = mysqli_query($this->my_db, $cms_group_sql);
        while($cms_group = mysqli_fetch_array($cms_group_query))
        {
            $cms_group_list[$cms_group['brand']] = $cms_group['brand_name'];
        }

        return $cms_group_list;
    }

    public function getTeamSupList($team_code)
    {
        $sup_list_sql    = "SELECT Distinct pcu.sup_c_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no), '(주)','') as c_name FROM product_cms_unit pcu WHERE pcu.display=1 AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE s.team='{$team_code}') OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE s.team='{$team_code}') OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE s.team='{$team_code}')) ORDER BY c_name ASC";
        $sup_list_query  = mysqli_query($this->my_db, $sup_list_sql);
        $sup_list        = [];

        while($sup_list_result = mysqli_fetch_assoc($sup_list_query))
        {
            $sup_list[$sup_list_result['sup_c_no']] = $sup_list_result['c_name'];
        }

        return $sup_list;
    }

    public function getLogisticsUnitList()
    {
        $logistics_sql = "
            (SELECT pcr.option_no AS prd_no, (SELECT pcu.option_name FROM product_cms_unit AS pcu WHERE pcu.`no`=pcr.option_no) AS option_name FROM logistics_product lp LEFT JOIN product_cms_relation pcr ON pcr.prd_no=lp.prd_no WHERE lp.prd_type = '1' AND pcr.display='1') 
            UNION 
            (SELECT lp.prd_no, (SELECT pcu.option_name FROM product_cms_unit AS pcu WHERE pcu.`no`=lp.prd_no) AS option_name FROM logistics_product lp WHERE lp.prd_type='2')
            ORDER BY option_name ASC
        ";
        $logistics_query     = mysqli_query($this->my_db, $logistics_sql);
        $logistics_unit_list = [];
        while($logistics_result = mysqli_fetch_assoc($logistics_query))
        {
            $logistics_unit_list[$logistics_result['prd_no']]  = $logistics_result['option_name'];
        }

        return $logistics_unit_list;
    }

    public function getSupUnitList()
    {
        $sup_unit_sql   = "SELECT Distinct pcu.sup_c_no, `no`, option_name FROM product_cms_unit pcu WHERE pcu.display=1 ORDER BY option_name ASC";
        $sup_unit_query = mysqli_query($this->my_db, $sup_unit_sql);
        $sup_unit_list  = [];

        while($sup_unit = mysqli_fetch_assoc($sup_unit_query))
        {
            $sup_unit_list[$sup_unit['sup_c_no']][$sup_unit['no']] = $sup_unit['option_name'];
        }

        return $sup_unit_list;
    }

    public function checkLogCompanySku($log_c_no, $sku)
    {
        $chk_sku_sql    = "SELECT COUNT(pcum.`no`) as cnt FROM product_cms_unit_management pcum LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcum.prd_unit WHERE pcum.log_c_no='{$log_c_no}' AND pcum.sku='{$sku}' AND pcu.display='1'";
        $chk_sku_query  = mysqli_query($this->my_db, $chk_sku_sql);
        $chk_sku_result = mysqli_fetch_assoc($chk_sku_query);

        return $chk_sku_result['cnt'] == 0 ? true : false;
    }

    public function getWarehouseOption()
    {
        $warehouse_sql    = "SELECT DISTINCT pcum.warehouse FROM product_cms_unit_management pcum LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcum.prd_unit WHERE pcu.display='1' ORDER BY pcum.warehouse ASC";
        $warehouse_query  = mysqli_query($this->my_db, $warehouse_sql);
        $warehouse_list        = [];

        while($warehouse_result = mysqli_fetch_assoc($warehouse_query))
        {
            $warehouse_list[] = $warehouse_result['warehouse'];
        }

        return $warehouse_list;
    }

    public function getBrandSupData($brand)
    {
        $cms_group_list = [];
        $cms_group_sql  = "
            SELECT 
                DISTINCT pcu.sup_c_no,
                REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no), '(주)','') as c_name
            FROM product_cms_unit pcu
            WHERE display='1' AND pcu.brand='{$brand}'
        ";
        $cms_group_query = mysqli_query($this->my_db, $cms_group_sql);
        while($cms_group = mysqli_fetch_array($cms_group_query))
        {
            $cms_group_list[$cms_group['sup_c_no']] = $cms_group['c_name'];
        }

        return $cms_group_list;
    }

    public function getSubcontractSupCompanyData()
    {
        $unit_sql      = "SELECT Distinct pcu.sup_c_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no), '(주)','') as c_name FROM product_cms_unit pcu WHERE pcu.display='1' AND pcu.`type`='2' ORDER BY c_name ASC";
        $unit_query    = mysqli_query($this->my_db, $unit_sql);
        $unit_list     = [];
        while($unit = mysqli_fetch_assoc($unit_query))
        {
            $unit_list[$unit['sup_c_no']] = $unit['c_name'];
        }

        return $unit_list;
    }
}
