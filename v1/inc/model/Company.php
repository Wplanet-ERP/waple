<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Company extends Model
{
    protected $_main_table      = "company";
    protected $_main_key        = "c_no";

    private $_list          = [];
    private $_name_list     = [];

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Company();
    }

    public function setList()
    {
        $company_sql     = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=`c`.s_no) as s_name FROM {$this->_main_table} WHERE active_state='1' ORDER BY c_no ASC";
        $company_query   = mysqli_query($this->my_db, $company_sql);
        $company_list        = [];
        $company_name_list   = [];
        while($company = mysqli_fetch_assoc($company_query))
        {
            $company_list[$company['c_no']] = array('c_name' => $company['c_name'], 'color' => $company['kind_color'], 'initial' => $company['initial']);
            $company_name_list[$company['c_no']]  = $company['c_name'];
        }

        $this->_list        = $company_list;
        $this->_name_list   = $company_name_list;
    }

    public function getList()
    {
        return $this->_list;
    }

    public function getLabelList(){
        return $this->_name_list;
    }

    public function getItem($main_val)
    {
        $item_sql       = "SELECT *, (SELECT my_c.c_name FROM my_company my_c WHERE my_c.my_c_no=main.my_c_no) as my_c_name, (SELECT team FROM staff s WHERE s.s_no=main.s_no) as team, (SELECT t.team_name FROM team t WHERE t.team_code IN(SELECT team FROM staff s WHERE s.s_no=main.s_no)) as team_name, (SELECT s.s_name FROM staff s WHERE s.s_no=main.s_no) as s_name FROM {$this->_main_table} as main WHERE `{$this->_main_key}`='{$main_val}'";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function getNameWhereItem($c_name)
    {
        $item_sql       = "SELECT *, (SELECT team FROM staff s WHERE s.s_no=main.s_no) as team FROM {$this->_main_table} as main WHERE `c_name`='{$c_name}'";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function getCompanyLabel($c_no)
    {
        $comp_sql       = "SELECT `c`.c_name, (SELECT s.s_name FROM staff s WHERE s.s_no=`c`.s_no) as s_name FROM {$this->_main_table} c WHERE `c`.c_no='{$c_no}'";
        $comp_query     = mysqli_query($this->my_db, $comp_sql);
        $comp_result    = mysqli_fetch_assoc($comp_query);

        $comp_label     = isset($comp_result['c_name']) ? $comp_result['c_name']." :: ".$comp_result['s_name'] : "";

        return $comp_label;
    }

    public function getDpTotalList()
    {
        $dp_cp_sql      = "SELECT c_no, c_name FROM {$this->_main_table} WHERE dp_e_type='1' ORDER BY display ASC, priority ASC, c_name ASC";
        $dp_cp_query    = mysqli_query($this->my_db, $dp_cp_sql);
        $dp_cp_list     = [];
        while($dp_cp_result = mysqli_fetch_assoc($dp_cp_query))
        {
            if(isset($dp_cp_list[$dp_cp_result['c_no']])){
                continue;
            }

            $dp_cp_list[$dp_cp_result['c_no']] = $dp_cp_result['c_name'];
        }

        return $dp_cp_list;
    }

    public function getDpList()
    {
        $dp_cp_sql      = "SELECT c_no, c_name FROM {$this->_main_table} WHERE dp_e_type='1' AND display='1' ORDER BY priority ASC, c_name ASC";
        $dp_cp_query    = mysqli_query($this->my_db, $dp_cp_sql);
        $dp_cp_list     = [];
        while($dp_cp_result = mysqli_fetch_assoc($dp_cp_query))
        {
            if(isset($dp_cp_list[$dp_cp_result['c_no']])){
                continue;
            }

            $dp_cp_list[$dp_cp_result['c_no']] = $dp_cp_result['c_name'];
        }

        return $dp_cp_list;
    }

    public function getDpDisplayList()
    {
        $dp_cp_sql      = "SELECT c_no, c_name, display FROM {$this->_main_table} WHERE dp_e_type='1' ORDER BY display ASC, priority ASC, c_name ASC";
        $dp_cp_query    = mysqli_query($this->my_db, $dp_cp_sql);
        $dp_cp_list     = [];
        while($dp_cp_result = mysqli_fetch_assoc($dp_cp_query))
        {
            if(isset($dp_cp_list[$dp_cp_result['c_no']])){
                continue;
            }

            $dp_cp_name = ($dp_cp_result['display'] == '2') ? $dp_cp_result['c_name']."[OFF]" : $dp_cp_result['c_name'];
            $dp_cp_list[$dp_cp_result['c_no']] = array("display" => $dp_cp_result['display'], "title" => $dp_cp_name);
        }

        return $dp_cp_list;
    }

    public function getDpEtcList()
    {
        $dp_cp_sql      = "SELECT c_no, c_name FROM {$this->_main_table} WHERE dp_e_type='1' AND display='1' AND (c_name NOT LIKE '%CS%' AND c_name NOT LIKE '%교환%' AND c_name NOT LIKE '%WISE%' AND c_name NOT LIKE '%아임웹%') ORDER BY priority ASC, c_name ASC";
        $dp_cp_query    = mysqli_query($this->my_db, $dp_cp_sql);
        $dp_cp_list     = [];
        while($dp_cp_result = mysqli_fetch_assoc($dp_cp_query))
        {
            if(isset($dp_cp_list[$dp_cp_result['c_no']])){
                continue;
            }

            $dp_cp_list[$dp_cp_result['c_no']] = $dp_cp_result['c_name'];
        }

        return $dp_cp_list;
    }

    public function getCompanyName($dp_c_no)
    {
        $dp_comp_sql    = "SELECT `c`.c_name FROM {$this->_main_table} c WHERE `c`.c_no='{$dp_c_no}'";
        $dp_comp_query  = mysqli_query($this->my_db, $dp_comp_sql);
        $dp_comp_result = mysqli_fetch_assoc($dp_comp_query);

        if($dp_comp_result['c_name'] == '시티파이'){
            $dp_c_name = "시티파이(아임웹)";
        }else{
            $dp_c_name = $dp_comp_result['c_name'];
        }

        return $dp_c_name;
    }

    public function getAgencyTypeItem($type, $type_val)
    {
        $item_sql       = "SELECT my_c_no, c_no, c_name, s_no, (SELECT s.team FROM staff s WHERE s.s_no=c.s_no) as team FROM {$this->_main_table} c WHERE `{$type}`='{$type_val}'";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function getInfluencerItem($main_val)
    {
        $item_sql       = "SELECT * FROM company_influencer as main WHERE ci_no='{$main_val}'";
        $item_query     = mysqli_query($this->my_db, $item_sql);
        $item_result    = mysqli_fetch_assoc($item_query);

        return $item_result;
    }

    public function getLogisticsList()
    {
        $logistics_sql      = "SELECT c_no, c_name FROM {$this->_main_table} WHERE log_e_type='1' AND display='1' ORDER BY log_priority ASC, c_name ASC";
        $logistics_query    = mysqli_query($this->my_db, $logistics_sql);
        $logistics_list     = [];
        while($logistics_result = mysqli_fetch_assoc($logistics_query))
        {
            if(isset($logistics_list[$logistics_result['c_no']])){
                continue;
            }

            if($logistics_result['c_name'] == '와이즈플래닛 미디어커머스'){
                $logistics_result['c_name'] = "와이즈미디어커머스";
            }

            $logistics_list[$logistics_result['c_no']] = $logistics_result['c_name'];
        }

        return $logistics_list;
    }

    public function getReceiptLogisticsList()
    {
        $logistics_sql      = "
            SELECT
                *
            FROM
            (
                SELECT 
                    `c`.c_no, 
                    `c`.c_name,
                    (SELECT pcsw.log_type FROM product_cms_stock_warehouse pcsw WHERE pcsw.log_c_no=`c`.c_no LIMIT 1) AS log_type,
                    (SELECT pcsw.priority FROM product_cms_stock_warehouse pcsw WHERE pcsw.log_c_no=`c`.c_no LIMIT 1) AS priority
                FROM company `c`
                WHERE `c`.log_e_type='1' AND `c`.display='1'
            ) AS rs
            ORDER BY log_type, priority ASC
        ";
        $logistics_query    = mysqli_query($this->my_db, $logistics_sql);
        $logistics_list     = [];
        while($logistics_result = mysqli_fetch_assoc($logistics_query))
        {
            if(isset($logistics_list[$logistics_result['c_no']])){
                continue;
            }

            if($logistics_result['c_name'] == '와이즈플래닛 미디어커머스'){
                $logistics_result['c_name'] = "와이즈미디어커머스";
            }

            $logistics_list[$logistics_result['log_type']][$logistics_result['c_no']] = $logistics_result['c_name'];
        }

        return $logistics_list;
    }

    public function getSettleCompanyList($display=1)
    {
        if($display > 0){
            $settle_company_sql = "SELECT c_no, c_name, display FROM {$this->_main_table} WHERE settle_e_type='1' AND display='{$display}' ORDER BY display ASC, c_name ASC";
        }else{
            $settle_company_sql = "SELECT c_no, c_name, display FROM {$this->_main_table} WHERE settle_e_type='1' ORDER BY display ASC, c_name ASC";
        }
        $settle_company_query   = mysqli_query($this->my_db, $settle_company_sql);
        $settle_company_list    = [];
        while($settle_company = mysqli_fetch_assoc($settle_company_query))
        {
            if(isset($settle_company_list[$settle_company['c_no']])){
                continue;
            }

            if($settle_company['c_name'] == '와이즈플래닛 미디어커머스'){
                $settle_company['c_name'] = "와이즈미디어커머스";
            }

            $settle_company_name = ($settle_company['display'] == 2) ? "{$settle_company['c_name']} [OFF]" : $settle_company['c_name'];
            $settle_company_list[$settle_company['c_no']] = array("display" => $settle_company['display'], "title" => $settle_company_name);
        }

        return $settle_company_list;
    }

    public function getSettleDpCompanyList()
    {
        $settle_company_sql     = "SELECT * FROM company WHERE c_no IN(SELECT DISTINCT settle_dp_c_no FROM work_cms_settlement) AND dp_e_type=1 ORDER BY priority";
        $settle_company_query   = mysqli_query($this->my_db, $settle_company_sql);
        $settle_company_list    = [];
        while($settle_company = mysqli_fetch_assoc($settle_company_query))
        {
            $settle_company_list[$settle_company['c_no']] = $settle_company['c_name'];
        }

        return $settle_company_list;
    }

    public function getBrandSettleDpCompanyList($brands, $settle_s_date, $settle_e_date)
    {
        $settle_company_sql     = "SELECT * FROM company WHERE c_no IN(SELECT DISTINCT settle_dp_c_no FROM work_cms_settlement wcs WHERE wcs.c_no IN({$brands}) AND wcs.settle_price > 0 AND wcs.settle_date BETWEEN '{$settle_s_date}' AND '{$settle_e_date}') AND dp_e_type=1 ORDER BY priority";
        $settle_company_query   = mysqli_query($this->my_db, $settle_company_sql);
        $settle_company_list    = [];
        while($settle_company = mysqli_fetch_assoc($settle_company_query))
        {
            $settle_company_list[$settle_company['c_no']] = $settle_company['c_name'];
        }

        return $settle_company_list;
    }
}
