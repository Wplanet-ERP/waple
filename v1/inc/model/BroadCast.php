<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class BroadCast extends Model
{
    protected $_main_table  = "broadcast_advertisement";
    protected $_main_key    = "ba_no";

    protected $_ba_type_option           = [];
    protected $_ba_detail_media_option   = [];
    protected $_ba_subs_gubun_option     = [];
    protected $_ba_delivery_gubun_option = [];

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new BroadCast();
    }

    public function getBaTypeOption()
    {
        return $this->_ba_type_option;
    }

    public function getBaDetailMediaOption()
    {
        return $this->_ba_detail_media_option;
    }

    public function getBaSubsOption()
    {
        return $this->_ba_subs_gubun_option;
    }

    public function getBaDeliveryOption()
    {
        return $this->_ba_delivery_gubun_option;
    }

    public function setBaAllTypeOption()
    {
        $ba_type_option_sql     = "SELECT `type`, detail_media, subs_gubun, delivery_gubun FROM {$this->_main_table}";
        $ba_type_option_query   = mysqli_query($this->my_db, $ba_type_option_sql);
        $ba_type_option             = [];
        $ba_detail_media_option     = [];
        $ba_subs_gubun_option       = [];
        $ba_delivery_gubun_option   = [];
        while($ba_type = mysqli_fetch_assoc($ba_type_option_query))
        {
            $ba_type_option[$ba_type['type']]                     = $ba_type['type'];
            $ba_detail_media_option[$ba_type['detail_media']]     = $ba_type['detail_media'];
            $ba_subs_gubun_option[$ba_type['subs_gubun']]         = $ba_type['subs_gubun'];
            $ba_delivery_gubun_option[$ba_type['delivery_gubun']] = $ba_type['delivery_gubun'];
        }

        $this->_ba_type_option             = array_filter($ba_type_option);
        $this->_ba_detail_media_option     = array_filter($ba_detail_media_option);
        $this->_ba_subs_gubun_option       = array_filter($ba_subs_gubun_option);
        $this->_ba_delivery_gubun_option   = array_filter($ba_delivery_gubun_option);

        ksort($this->_ba_type_option);
        ksort($this->_ba_detail_media_option);
        ksort($this->_ba_subs_gubun_option);
        ksort($this->_ba_delivery_gubun_option);
    }

    public function getAdvertiseOption()
    {
        $ad_sql    = "SELECT DISTINCT advertiser, adv_no FROM {$this->_main_table} ORDER BY advertiser ASC";
        $ad_query  = mysqli_query($this->my_db, $ad_sql);
        $type_advertiser_option = [];

        while($ad_result = mysqli_fetch_assoc($ad_query))
        {
            $type_advertiser_option[$ad_result['adv_no']] = $ad_result['advertiser'];
        }

        return $type_advertiser_option;
    }

    public function getAdvertisementTotal($add_where)
    {
        $broadcast_total_sql    = "SELECT count(ba_no) as cnt FROM (SELECT `ba`.ba_no FROM {$this->_main_table} `ba` WHERE {$add_where}) AS cnt";
        $broadcast_total_query	= mysqli_query($this->my_db, $broadcast_total_sql);
        $broadcast_total_result = mysqli_fetch_array($broadcast_total_query);
        $broadcast_total        = $broadcast_total_result['cnt'];

        return $broadcast_total;
    }

    public function multiDeleteAdvertisement($ba_no_list)
    {
        $del_sql    = "DELETE FROM {$this->_main_table} WHERE ba_no IN({$ba_no_list})" ;
        $return_msg = "선택된 ID값이 없습니다.";

        if(!!$ba_no_list){
            if (!mysqli_query($this->my_db, $del_sql)){
                $return_msg = "삭제에 실패했습니다. 담당자에게 문의해주세요.";
            }else{
                $return_msg = "모두 삭제했습니다.";
            }
        }

        return $return_msg;
    }

    function getBaSchType($sub_s_date, $sub_e_date, $sch_type, $sch_main_type, $type_day, $type_media, $type_date, $type_time, $type_advertiser)
    {
        $type_main_list  = [];
        $type_title      = "";
        $type_name_list  = [];
        $type_total_list = [];

        switch($sch_type)
        {
            case 'day':
                $type_title      = "날짜";
                $type_main_list  = $type_day;

                if($sch_main_type == '1') //월간
                {
                    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sub_s_date}' AND '{$sub_e_date}'";
                    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
                    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m')";
                }
                elseif($sch_main_type == '2') //주간
                {
                    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sub_s_date}' AND '{$sub_e_date}'";
                    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL (DAYOFWEEK(`allday`.Date)-1) DAY), '%Y%m%d')";
                    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL (DAYOFWEEK(`allday`.Date)-1) DAY), '%Y-%m-%d'),' ~ ', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL (DAYOFWEEK(`allday`.Date)-7) DAY), '%Y-%m-%d'))";
                }
                else //일간
                {
                    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sub_s_date}' AND '{$sub_e_date}'";
                    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
                    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";
                }

                $all_date_sql = "
                SELECT
                    {$all_date_key} as chart_key,
                    {$all_date_title} as chart_title
                FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
                        (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
                        (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
                        (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
                        (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
                        (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
                as allday
                WHERE {$all_date_where}
                GROUP BY chart_key
                ORDER BY chart_key
            ";

                $all_date_query = mysqli_query($this->my_db, $all_date_sql);
                while($date = mysqli_fetch_array($all_date_query))
                {
                    $type_total_list[$date['chart_key']] = 0;
                    $type_name_list[$date['chart_key']]  = $date['chart_title'];
                }

                break;

            case 'media':
                $type_title      = "매체";
                $type_main_list  = $type_media;
                if($sch_main_type){
                    $type_name_list[$sch_main_type]  = $type_media[$sch_main_type];
                    $type_total_list[$sch_main_type] = 0;
                }else{
                    foreach($type_media as $key => $value){
                        $type_total_list[$key] = 0;
                        $type_name_list[$key]  = $value;
                    }
                }
                break;

            case 'advertiser':
                $type_title      = "광고주";
                $type_main_list  = $type_advertiser;
                if($sch_main_type){
                    $type_name_list[$sch_main_type]  = $type_advertiser[$sch_main_type];
                    $type_total_list[$sch_main_type] = 0;
                }else{
                    foreach($type_advertiser as $key => $value){
                        $type_total_list[$key] = 0;
                        $type_name_list[$key]  = $value;
                    }
                }
                break;

            case 'date':
                $type_title      = "요일";
                $type_main_list  = $type_date;
                if($sch_main_type){
                    $type_name_list[$sch_main_type]  = $type_date[$sch_main_type];
                    $type_total_list[$sch_main_type] = 0;
                }else{
                    foreach($type_date as $key => $value){
                        $type_total_list[$key] = 0;
                        $type_name_list[$key]  = $value;
                    }
                }
                break;

            case 'time':
                $type_title      = "시간대";
                $type_main_list  = $type_time;
                if($sch_main_type){
                    $type_name_list[$sch_main_type]  = $type_time[$sch_main_type];
                    $type_total_list[$sch_main_type] = 0;
                }else{
                    foreach($type_time as $key => $value){
                        $type_total_list[$key] = 0;
                        $type_name_list[$key]  = $value;
                    }
                }
                break;
        }

        return array(
            "type_title"       => $type_title,
            "type_main_list"   => $type_main_list,
            "type_name_list"   => $type_name_list,
            "type_total_list"  => $type_total_list
        );
    }
}
