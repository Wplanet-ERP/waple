<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Corporation extends Model
{
    protected $_main_table = "corp_account";
    protected $_card_table = "corp_card";
    protected $_main_key   = "co_no";


    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Corporation();
    }

    public function setCorpCard()
    {
        $this->_main_table  = $this->_card_table;
        $this->_main_key    = "cc_no";
    }

    public function getDpAccountList()
    {
        $group_sql = "SELECT * FROM {$this->_main_table} WHERE `type`='1' AND display='1'";
        $group_query = mysqli_query($this->my_db, $group_sql);

        $group_list     = [];
        while($group_result = mysqli_fetch_assoc($group_query)){
            $group_list[$group_result['co_no']] = $group_result['name'];
        }

        return $group_list;
    }

    public function getAccountList($type)
    {
        $group_sql = "SELECT * FROM {$this->_main_table} WHERE `type`='{$type}' AND display='1'";
        $group_query = mysqli_query($this->my_db, $group_sql);

        $group_list     = [];
        while($group_result = mysqli_fetch_assoc($group_query)){
            $group_list[$group_result['my_c_no']][$group_result['co_no']] = $group_result['name'];
        }

        return $group_list;
    }

    public function getTotalAccountList()
    {
        $group_sql = "SELECT * FROM {$this->_main_table} WHERE display='1'";
        $group_query = mysqli_query($this->my_db, $group_sql);

        $group_list     = [];
        while($group_result = mysqli_fetch_assoc($group_query)){
            $group_list[$group_result['co_no']] = $group_result['name'];
        }

        return $group_list;
    }

    public function getCardItem($cc_no)
    {
        $card_sql = "
            SELECT 
               *, 
               (SELECT t.depth FROM team t WHERE t.team_code=`cc`.team) as depth, 
               DATE_FORMAT(expired_date, '%Y-%m') as expired_date, 
               (SELECT s_name FROM staff s WHERE s.s_no=`cc`.manager) as s_name 
            FROM {$this->_card_table} `cc` 
            WHERE `cc`.cc_no = '{$cc_no}'
        ";
        $card_query     = mysqli_query($this->my_db, $card_sql);
        $card_result    = mysqli_fetch_assoc($card_query);

        return isset($card_result['cc_no']) ? $card_result : [];
    }
}
