<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Navigation extends Model
{
    protected $_main_table  = "navigation";
    protected $_main_key    = "no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Navigation();
    }

    public function getParentNavigationList()
    {
        $nav_sql    = "SELECT * FROM {$this->_main_table} WHERE (parent_nav_code='0' OR parent_nav_code IS NULL) AND display='1' AND nav_kind='waple' AND `no` != '1'";
        $nav_query  = mysqli_query($this->my_db, $nav_sql);
        $nav_list   = [];
        while($nav_result = mysqli_fetch_assoc($nav_query))
        {
            $nav_list[$nav_result['no']] = $nav_result['nav_name'];
        }

        return $nav_list;
    }
}
