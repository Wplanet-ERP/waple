<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class WorkCertificate extends Model
{
    protected $_main_table = "work_certificate";
    protected $_main_key   = "wc_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new WorkCertificate();
    }

    public function getNewDocNo()
    {
        $doc_date   = date('Ymd');
        $doc_sql    = "SELECT REPLACE(doc_no, '{$doc_date}_C', '') as doc_idx FROM {$this->_main_table} WHERE doc_no LIKE '{$doc_date}_C%' ORDER BY doc_no DESC LIMIT 1";
        $doc_query  = mysqli_query($this->my_db, $doc_sql);
        $doc_result = mysqli_fetch_assoc($doc_query);

        $last_doc_idx = (isset($doc_result['doc_idx']) && !empty($doc_result['doc_idx'])) ? (int)$doc_result['doc_idx']+1 : 1;
        $doc_no       = $doc_date."_C".sprintf('%03d', $last_doc_idx);

        return $doc_no;
    }
}
