<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';
require_once BASEPATH.'/v1/inc/model/Message.php';

class Calendar extends Model
{
    protected $_main_table  = "calendar_manager";
    protected $_main_key    = "cal_no";

    protected $_main_permission_table   = "calendar_manager_permission";
    protected $_main_permission_key     = "cmp_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Calendar();
    }

    public function setScheduleTable()
    {
        $this->_main_table  = "calendar_schedule";
        $this->_main_key    = "cs_no";
    }

    public function deletePermission($cal_no)
    {
        $result     = false;
        $del_sql    = "DELETE FROM {$this->_main_permission_table} WHERE cal_no='{$cal_no}'";

        if(mysqli_query($this->my_db, $del_sql)){
            $result = true;
        }

        return $result;
    }

    public function checkPermission($permission_type, $cal_no, $s_no)
    {
        $cal_permission_sql    = "SELECT * FROM calendar_manager_permission WHERE cal_no='{$cal_no}' AND cmp_type='{$permission_type}'";
        $cal_permission_query  = mysqli_query($this->my_db, $cal_permission_sql);
        $cal_permission_staff_list = [];
        while($cal_permission = mysqli_fetch_assoc($cal_permission_query))
        {
            if($cal_permission['cmp_s_no'] == 0)
            {
                $team_list_val = getTeamWhere($this->my_db, $cal_permission['cmp_team']);
                $team_list     = !empty($team_list_val) ? explode(',', $team_list_val) : [];

                if(!empty($team_list))
                {
                    foreach($team_list as $team)
                    {
                        $staff_sql   = "SELECT s_no FROM staff s WHERE s.team_list like '%{$team}%' AND staff_state='1'";
                        $staff_query = mysqli_query($this->my_db, $staff_sql);
                        while($staff = mysqli_fetch_assoc($staff_query))
                        {
                            $cal_permission_staff_list[] = $staff['s_no'];
                        }
                    }
                }
            }else{
                $cal_permission_staff_list[] = $cal_permission['cmp_s_no'];
            }
        }

        $cal_permission_staff_list = array_unique($cal_permission_staff_list);

        if(in_array($s_no, $cal_permission_staff_list)){
            return true;
        }

        return false;
    }

    public function checkSchedulePermission($cs_no, $s_no)
    {
        $cal_permission_sql    = "SELECT * FROM calendar_schedule_permission WHERE cs_no='{$cs_no}'";
        $cal_permission_query  = mysqli_query($this->my_db, $cal_permission_sql);
        $cal_permission_staff_list = [];
        while($cal_permission = mysqli_fetch_assoc($cal_permission_query))
        {
            if($cal_permission['csp_s_no'] == 0)
            {
                $team_list_val = getTeamWhere($this->my_db, $cal_permission['csp_team']);
                $team_list     = !empty($team_list_val) ? explode(',', $team_list_val) : [];

                if(!empty($team_list))
                {
                    foreach($team_list as $team)
                    {
                        $staff_sql   = "SELECT s_no FROM staff s WHERE s.team_list like '%{$team}%' AND staff_state='1'";
                        $staff_query = mysqli_query($this->my_db, $staff_sql);
                        while($staff = mysqli_fetch_assoc($staff_query))
                        {
                            $cal_permission_staff_list[] = $staff['s_no'];
                        }
                    }
                }
            }else{
                $cal_permission_staff_list[] = $cal_permission['csp_s_no'];
            }
        }

        $cal_permission_staff_list = array_unique($cal_permission_staff_list);

        if(in_array($s_no, $cal_permission_staff_list)){
            return true;
        }

        return false;
    }

    public function sendScheduleHp($cs_no)
    {
        $schedule_sql    = "SELECT *, (SELECT sub.s_name FROM staff sub WHERE sub.s_no=main.cs_s_no) as cs_s_name FROM calendar_schedule as main WHERE cs_no='{$cs_no}' LIMIT 1";
        $schedule_query  = mysqli_query($this->my_db, $schedule_sql);
        $schedule_result = mysqli_fetch_assoc($schedule_query);

        $cal_id         = $schedule_result['cal_id'];
        $sel_date       = date('Y-m-d', strtotime($schedule_result['cs_s_date']));
        $send_data_list = [];

        if($schedule_result['cs_alert'] == '1')
        {
            $cs_alert_date    = date("YmdHis", strtotime($schedule_result['cs_alert_date']));
            $cs_alert_hp_list = explode('$', $schedule_result['cs_alert_hp']);
            $sms_msg          = "";

            $cs_all     = $schedule_result['cs_all'];
            $cs_s_date  = date('Y-m-d', strtotime($schedule_result['cs_s_date']));
            $cs_e_date  = date('Y-m-d', strtotime($schedule_result['cs_e_date']));

            $sms_msg .= "{$schedule_result['cs_s_name']}님의 일정알림\r\n";
            if($cs_s_date == $cs_e_date)
            {
                if($cs_all == '1')
                {
                    $cs_s_date_content = date('m/d', strtotime($schedule_result['cs_s_date']));
                    $sms_msg .= "{$cs_s_date_content}\r\n";
                }
                else
                {
                    $cs_s_date_content = date('m/d H:i', strtotime($schedule_result['cs_s_date']));
                    $sms_msg .= "{$cs_s_date_content}\r\n";
                }
            }else{
                if($cs_all == '1')
                {
                    $cs_s_date_content = date('m/d', strtotime($schedule_result['cs_s_date']));
                    $cs_e_date_content = date('m/d', strtotime($schedule_result['cs_e_date']));
                    $sms_msg .= "{$cs_s_date_content}~{$cs_e_date_content}\r\n";
                }
                else
                {
                    $cs_s_date_content = date('m/d H:i', strtotime($schedule_result['cs_s_date']));
                    $cs_e_date_content = date('m/d H:i', strtotime($schedule_result['cs_e_date']));
                    $sms_msg .= "{$cs_s_date_content}~{$cs_e_date_content}\r\n";
                }
            }

            $sms_msg .=  "'".$schedule_result['cs_title']."' 일정을 확인해 주세요.\r\n";
            $sms_msg .=  "https://work.wplanet.co.kr/v1/calendar/calendar_schedule.php?cal_id={$cal_id}&sel_date={$sel_date}";

            $cur_datetime	= date("YmdHis");
            $cm_id          = 1;
            $sms_title      = "[와플 알림 문자]";
            $sms_msg_len    = mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
            $msg_type       = ($sms_msg_len > 90) ? "L" : "S";
            $send_name 	    = "와이즈플래닛";
            $send_phone     = "02-830-1912";
            $c_info         = "12"; #일정관리
            $cinfo_detail   = "CAL".$cs_no;

            foreach($cs_alert_hp_list as $cs_alert_hp)
            {
                $msg_id     = "W{$cur_datetime}".sprintf('%04d', $cm_id++);

                $send_data_list[$msg_id] = array(
                    "msg_id"        => $msg_id,
                    "msg_type"      => $msg_type,
                    "sender"        => $send_name,
                    "sender_hp"     => $send_phone,
                    "receiver_hp"   => $cs_alert_hp,
                    "reserved_time" => $cs_alert_date,
                    "title"         => $sms_title,
                    "content"       => $sms_msg,
                    "cinfo"         => $c_info,
                    "cinfo_detail"  => $cinfo_detail,
                );
            }
        }

        if(!empty($send_data_list)){
            $message_model = Message::Factory();
            $message_model->sendMessage($send_data_list);
        }
    }

    public function repeatSchedule($cs_no)
    {
        $repeat_sql     = "SELECT * FROM calendar_schedule WHERE cs_no='{$cs_no}' LIMIT 1";
        $repeat_query   = mysqli_query($this->my_db, $repeat_sql);
        $schedule       = mysqli_fetch_assoc($repeat_query);

        if($schedule['date_repeat'] == '1' && $schedule['date_repeat_type'] > 0)
        {
            $cs_s_date      = new DateTime(date('Y-m-d', strtotime($schedule['cs_s_date'])));
            $cs_e_date      = new DateTime(date('Y-m-d', strtotime($schedule['cs_e_date'])));
            $gap            = date_diff($cs_s_date, $cs_e_date);
            $cs_date_term   = $gap->days;

            $cs_alert           = $schedule['cs_alert'];
            $cs_alert_repeat    = $schedule['cs_alert_repeat'];
            $cs_alert_date_time = date('H:i:s', strtotime($schedule['cs_alert_date']));
            $cs_alert_hp        = $schedule['cs_alert_hp'];

            $cs_s_date_ext      = date('H:i:s', strtotime($schedule['cs_s_date']));
            $cs_e_date_ext      = date('H:i:s', strtotime($schedule['cs_e_date']));
            $date_repeat_val    = $schedule['date_repeat_val'];
            $date_repeat_start  = $schedule['date_repeat_start'];
            $date_repeat_end    = $schedule['date_repeat_end'];

            $repeat_add_column  = "date_repeat='{$schedule['date_repeat']}', date_repeat_type='{$schedule['date_repeat_type']}', date_repeat_val='{$schedule['date_repeat_val']}', date_repeat_week='{$schedule['date_repeat_week']}', date_repeat_start='{$schedule['date_repeat_start']}', date_repeat_end='{$schedule['date_repeat_end']}',";

            if($cs_alert == '1' && $cs_alert_repeat == '1'){
                $alert_add_column   = "";
            }else{
                $alert_add_column   = "cs_alert='0',";
            }

            switch ($schedule['date_repeat_type'])
            {
                case '1':
                    $cs_date         = date("Y-m-d", strtotime($schedule['cs_s_date']));
                    $cs_repeat_start = date("Y-m-d", strtotime($date_repeat_start));
                    $cs_date_idx     = 0;

                    for($i=0; $cs_repeat_start<$date_repeat_end; $i++)
                    {
                        $date_repeat_term = $date_repeat_val * $cs_date_idx;
                        $cs_date_idx++;

                        $cs_repeat_start = date('Y-m-d', strtotime("+{$date_repeat_term} days {$date_repeat_start}"));
                        $cs_repeat_end   = date('Y-m-d', strtotime("+{$cs_date_term} days {$cs_repeat_start}"));

                        if($cs_date == $cs_repeat_start){
                            continue;
                        }

                        if($cs_repeat_start > $date_repeat_end){
                            break;
                        }

                        $ins_cs_s_date = $cs_repeat_start." ".$cs_s_date_ext;
                        $ins_cs_e_date = $cs_repeat_end." ".$cs_e_date_ext;

                        if($cs_alert == '1' && $cs_alert_repeat == '1'){
                            $cs_alert_date      = $cs_repeat_start." ".$cs_alert_date_time;
                            $alert_add_column   = "cs_alert='1', cs_alert_repeat='1', cs_alert_date='{$cs_alert_date}', cs_alert_hp='{$cs_alert_hp}',";
                        }

                        $ins_sql = "
                            INSERT INTO calendar_schedule SET 
                                parent_cs_no='{$cs_no}', 
                                cs_type='repeat', 
                                cs_category='{$schedule['cs_category']}',
                                cal_no='{$schedule['cal_no']}',
                                cal_id='{$schedule['cal_id']}',
                                cs_title='{$schedule['cs_title']}',
                                cs_important='{$schedule['cs_important']}',
                                cs_s_no='{$schedule['cs_s_no']}',
                                cs_all = '{$schedule['cs_all']}',
                                cs_s_date='{$ins_cs_s_date}',
                                cs_e_date='{$ins_cs_e_date}',
                                asset_reservation_list='{$schedule['asset_reservation_list']}',
                                cs_permission='{$schedule['cs_permission']}',
                                cs_content='{$schedule['cs_content']}',
                                file_path='{$schedule['file_path']}',
                                file_name='{$schedule['file_name']}',
                                {$repeat_add_column}
                                {$alert_add_column}
                                regdate=now()
                        ";

                        mysqli_query($this->my_db, $ins_sql);

                        $new_cs_no = mysqli_insert_id($this->my_db);

                        $ins_per_sql = "INSERT INTO calendar_schedule_permission(`cs_no`, `csp_s_no`, `csp_team`) (SELECT '{$new_cs_no}',csp_s_no, csp_team FROM calendar_schedule_permission WHERE cs_no='{$cs_no}')";
                        mysqli_query($this->my_db, $ins_per_sql);

                        if(!empty($schedule['asset_reservation_list'])) {
                            $this->reservationAsset($new_cs_no);
                        }

                        if($cs_alert == '1' && $cs_alert_repeat == '1') {
                            $this->sendScheduleHp($new_cs_no);
                        }
                    }
                    break;

                case '2':
                    $cs_week_val = $schedule['date_repeat_week'];
                    $date_repeat_week_list = [];
                    for($i=0;$i<strlen($cs_week_val);$i++){
                        $date_repeat_week_list[] = substr ($cs_week_val, $i, 1);
                    }

                    $cs_date         = date("Y-m-d", strtotime($schedule['cs_s_date']));
                    $cs_repeat_start = date("Y-m-d", strtotime($date_repeat_start));
                    $cs_week_idx     = 0;
                    $cs_week_day     = date('w', strtotime($cs_repeat_start));

                    for($i=0; $cs_repeat_start<$date_repeat_end; $i++)
                    {
                        $date_repeat_term = $date_repeat_val * $cs_week_idx;
                        $cs_week_idx++;

                        $cs_repeat_start       = date('Y-m-d', strtotime("+{$date_repeat_term} weeks {$date_repeat_start}"));
                        $cs_repeat_start_first = date('Y-m-d', strtotime("-{$cs_week_day}days {$cs_repeat_start}"));

                        for($daily_idx=0; $daily_idx < 7; $daily_idx++)
                        {
                            $cs_repeat_start = date('Y-m-d', strtotime("+{$daily_idx} days {$cs_repeat_start_first}"));
                            $cs_repeat_end   = date('Y-m-d', strtotime("+{$cs_date_term} days {$cs_repeat_start}"));

                            if($cs_repeat_start < $date_repeat_start){
                                continue;
                            }

                            if($cs_date == $cs_repeat_start){
                                continue;
                            }

                            if($cs_repeat_start > $date_repeat_end){
                                break;
                            }

                            if($date_repeat_week_list[$daily_idx] == '1')
                            {
                                $ins_cs_s_date = $cs_repeat_start." ".$cs_s_date_ext;
                                $ins_cs_e_date = $cs_repeat_end." ".$cs_e_date_ext;

                                if($cs_alert == '1' && $cs_alert_repeat == '1'){
                                    $cs_alert_date      = $cs_repeat_start." ".$cs_alert_date_time;
                                    $alert_add_column   = "cs_alert='1', cs_alert_repeat='1', cs_alert_date='{$cs_alert_date}', cs_alert_hp='{$cs_alert_hp}',";
                                }

                                $ins_sql = "
                                    INSERT INTO calendar_schedule SET 
                                        parent_cs_no='{$cs_no}', 
                                        cs_type='repeat', 
                                        cs_category='{$schedule['cs_category']}',
                                        cal_no='{$schedule['cal_no']}',
                                        cal_id='{$schedule['cal_id']}',
                                        cs_title='{$schedule['cs_title']}',
                                        cs_important='{$schedule['cs_important']}',
                                        cs_s_no='{$schedule['cs_s_no']}',
                                        cs_all='{$schedule['cs_all']}',
                                        cs_s_date='{$ins_cs_s_date}',
                                        cs_e_date='{$ins_cs_e_date}',
                                        asset_reservation_list='{$schedule['asset_reservation_list']}',
                                        cs_permission='{$schedule['cs_permission']}',
                                        cs_content='{$schedule['cs_content']}',
                                        file_path='{$schedule['file_path']}',
                                        file_name='{$schedule['file_name']}',
                                        {$repeat_add_column}
                                        {$alert_add_column}
                                        regdate=now()
                                ";

                                mysqli_query($this->my_db, $ins_sql);

                                $new_cs_no = mysqli_insert_id($this->my_db);

                                $ins_per_sql = "INSERT INTO calendar_schedule_permission(`cs_no`, `csp_s_no`, `csp_team`) (SELECT '{$new_cs_no}',csp_s_no, csp_team FROM calendar_schedule_permission WHERE cs_no='{$cs_no}')";
                                mysqli_query($this->my_db, $ins_per_sql);

                                if(!empty($schedule['asset_reservation_list'])) {
                                    $this->reservationAsset($new_cs_no);
                                }

                                if($cs_alert == '1' && $cs_alert_repeat == '1') {
                                    $this->sendScheduleHp($new_cs_no);
                                }
                            }
                        }
                    }

                    break;

                case '3':
                    $cs_s_day = date('d', strtotime($schedule['cs_s_date']));
                    $cs_e_day = date('d', strtotime($schedule['cs_e_date']));

                    $cs_date         = date("Y-m-d", strtotime($schedule['cs_s_date']));
                    $cs_repeat_start = date("Y-m-d", strtotime($date_repeat_start));
                    $cs_month_idx    = 0;

                    for($i=0; $cs_repeat_start<$date_repeat_end; $i++)
                    {
                        $date_repeat_term = $date_repeat_val * $cs_month_idx;
                        $cs_month_idx++;

                        $cs_repeat_month = date('Y-m', strtotime("+{$date_repeat_term} months {$date_repeat_start}"));
                        $cs_repeat_start = date('Y-m-d', strtotime($cs_repeat_month."-".$cs_s_day));
                        $cs_repeat_end   = date('Y-m-d', strtotime($cs_repeat_month."-".$cs_e_day));

                        if($cs_date == $cs_repeat_start){
                            continue;
                        }

                        if($cs_repeat_start > $date_repeat_end){
                            break;
                        }

                        $ins_cs_s_date = $cs_repeat_start." ".$cs_s_date_ext;
                        $ins_cs_e_date = $cs_repeat_end." ".$cs_e_date_ext;

                        if($cs_alert == '1' && $cs_alert_repeat == '1'){
                            $cs_alert_date      = $cs_repeat_start." ".$cs_alert_date_time;
                            $alert_add_column   = "cs_alert='1', cs_alert_repeat='1', cs_alert_date='{$cs_alert_date}', cs_alert_hp='{$cs_alert_hp}',";
                        }

                        $ins_sql = "
                            INSERT INTO calendar_schedule SET 
                                parent_cs_no='{$cs_no}', 
                                cs_type='repeat', 
                                cs_category='{$schedule['cs_category']}',
                                cal_no='{$schedule['cal_no']}',
                                cal_id='{$schedule['cal_id']}',
                                cs_title='{$schedule['cs_title']}',
                                cs_important='{$schedule['cs_important']}',
                                cs_s_no='{$schedule['cs_s_no']}',
                                cs_all='{$schedule['cs_all']}',
                                cs_s_date='{$ins_cs_s_date}',
                                cs_e_date='{$ins_cs_e_date}',
                                asset_reservation_list='{$schedule['asset_reservation_list']}',
                                cs_permission='{$schedule['cs_permission']}',
                                cs_content='{$schedule['cs_content']}',
                                file_path='{$schedule['file_path']}',
                                file_name='{$schedule['file_name']}',
                                {$repeat_add_column}
                                {$alert_add_column}
                                regdate=now()
                        ";

                        mysqli_query($this->my_db, $ins_sql);

                        $new_cs_no = mysqli_insert_id($this->my_db);

                        $ins_per_sql = "INSERT INTO calendar_schedule_permission(`cs_no`, `csp_s_no`, `csp_team`) (SELECT '{$new_cs_no}',csp_s_no, csp_team FROM calendar_schedule_permission WHERE cs_no='{$cs_no}')";
                        mysqli_query($this->my_db, $ins_per_sql);

                        if(!empty($schedule['asset_reservation_list'])) {
                            $this->reservationAsset($new_cs_no);
                        }

                        if($cs_alert == '1' && $cs_alert_repeat == '1') {
                            $this->sendScheduleHp($new_cs_no);
                        }
                    }
                    break;
            }
        }
    }

    public function reservationAsset($cs_no)
    {
        $reservation_sql    = "SELECT *, (SELECT sub.s_name FROM staff sub WHERE sub.s_no=main.cs_s_no) as cs_s_name FROM calendar_schedule as main WHERE cs_no='{$cs_no}' LIMIT 1";
        $reservation_query  = mysqli_query($this->my_db, $reservation_sql);
        $reservation        = mysqli_fetch_assoc($reservation_query);

        if(!empty($reservation['asset_reservation_list']))
        {
            $asset_list = explode(',', $reservation['asset_reservation_list']);

            foreach($asset_list as $as_no)
            {

                $asset_sql    = "SELECT * FROM asset WHERE as_no='{$as_no}' LIMIT 1";
                $asset_query  = mysqli_query($this->my_db, $asset_sql);
                $asset        = mysqli_fetch_assoc($asset_query);

                $r_s_date = date('Y-m-d H:i', strtotime($reservation['cs_s_date'])).":00";
                $r_e_date = date('Y-m-d H:i', strtotime($reservation['cs_e_date'])).":00";

                $add_where = "`asr`.as_no = '{$as_no}' AND `asr`.work_state IN('1','2') AND ((`asr`.r_s_date >= '{$r_s_date}' AND `asr`.r_s_date < '{$r_e_date}') OR (`asr`.r_e_date > '{$r_s_date}' AND `asr`.r_e_date <= '{$r_e_date}') OR (`asr`.r_s_date < '{$r_s_date}' AND `asr`.r_e_date >= '{$r_e_date}'))";

                // 리스트 쿼리
                $asset_reservation_sql      = " SELECT count(as_r_no) as cnt FROM asset_reservation `asr` WHERE {$add_where}";
                $asset_reservation_query    = mysqli_query($this->my_db, $asset_reservation_sql);
                $asset_reservation          = mysqli_fetch_assoc($asset_reservation_query);

                if($asset_reservation['cnt'] > 0)
                {
                    continue;
                }

                $ins_sql = "INSERT INTO `asset_reservation` SET
                    `work_state`  ='2',
                    `as_no`       ='{$as_no}',
                    `management`  ='{$asset['management']}',
                    `manager`     ='{$asset['manager']}',
                    `regdate`     = now(),
                    `req_no`      ='{$reservation['cs_s_no']}',
                    `req_name`    ='{$reservation['cs_s_name']}',
                    `req_date`    = now(),
                    `r_s_date`    = '{$r_s_date}',
                    `r_e_date`    = '{$r_e_date}',
                    `run_no`      = '{$reservation['cs_s_no']}',
                    `run_name`    = '{$reservation['cs_s_name']}', 
                    `run_date`    = now(),
                    `task_run`    = '캘린더일정 자동등록({$cs_no})'
                ";

                mysqli_query($this->my_db, $ins_sql);
            }
        }
    }

    public function getActiveList()
    {
        $cal_active_sql     = "SELECT * FROM calendar_manager WHERE display='1'";
        $cal_active_query   = mysqli_query($this->my_db, $cal_active_sql);
        $cal_active_list    = [];
        while($cal_active = mysqli_fetch_assoc($cal_active_query)) {
            $cal_active_list[$cal_active['cal_no']] = $cal_active;
        }

        return $cal_active_list;
    }
}
