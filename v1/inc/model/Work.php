<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Work extends Model
{
    protected $_main_table = "work";
    protected $_main_key   = "w_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Work();
    }

    public function getExtraItem($w_no)
    {
        $work_data_sql   = "SELECT w.*, p.work_format_type AS prd_work_type, p.work_format_extra AS prd_work_extra, p.title AS prd_name, p.price_apply FROM {$this->_main_table} w LEFT JOIN product p ON p.prd_no=w.prd_no WHERE w.`{$this->_main_key}` = '{$w_no}' LIMIT 1";
        $work_data_query = mysqli_query($this->my_db, $work_data_sql);
        $work_data       = mysqli_fetch_assoc($work_data_query);

        return $work_data;
    }

    public function getSetMaxPriority($set_tag)
    {
        $chk_work_set_sql    = "SELECT MAX(priority) as max_priority FROM {$this->_main_table} WHERE `set_tag` = '{$set_tag}'";
        $chk_work_set_query  = mysqli_query($this->my_db, $chk_work_set_sql);
        $chk_work_set_result = mysqli_fetch_assoc($chk_work_set_query);
        $chk_priority		 = isset($chk_work_set_result['max_priority']) ? $chk_work_set_result['max_priority']+1 : 1;

        return $chk_priority;
    }

    public function getTaskReqDdayCount($prd_no, $task_req_dday)
    {
        $task_req_dday_cnt_sql  = "SELECT count(w_no) as cnt FROM `{$this->_main_table}` w WHERE w.`prd_no`='{$prd_no}' AND w.`task_req_dday`='{$task_req_dday}'";
        $task_req_dday_cnt_data = mysqli_fetch_array(mysqli_query($this->my_db, $task_req_dday_cnt_sql));

        return $task_req_dday_cnt_data['cnt'];
    }

    public function getDeadLine()
    {
        $deadline_sql 	= "SELECT DATE_FORMAT(task_run_dday, '%y-%m') as ym, task_run_dday, w_no FROM `work` WHERE prd_no = '179' AND work_state IN('3','4','5','6') ORDER BY task_run_dday DESC";
        $deadline_query = mysqli_query($this->my_db, $deadline_sql);
        $deadline_list  = [];

        while($deadline = mysqli_fetch_array($deadline_query)) {
            $deadline_list[$deadline['ym']] = array('dday' => $deadline['task_run_dday'], 'w_no' => $deadline['w_no']);
        }
        ksort($deadline_list);

        return $deadline_list;
    }
}
