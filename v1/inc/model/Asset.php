<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class Asset extends Model
{
    protected $_main_table  = "asset";
    protected $_main_key    = "as_no";

    protected $_reservation_table   = "asset_reservation";
    protected $_reservation_key     = "as_r_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new Asset();
    }

    public function getReservationItem($as_r_no)
    {
        $reserv_sql    = "SELECT * FROM {$this->_reservation_table} WHERE `{$this->_reservation_key}`='{$as_r_no}'";
        $reserv_query  = mysqli_query($this->my_db, $reserv_sql);
        $reserv_result = mysqli_fetch_assoc($reserv_query);

        return $reserv_result;
    }

    public function getAssetShareList()
    {
        $asset_share_sql   = "SELECT as_no, `name` FROM {$this->_main_table} WHERE share_type='3' AND asset_state='1' AND as_no IS NOT NULL ORDER BY priority ASC";
        $asset_share_query = mysqli_query($this->my_db, $asset_share_sql);
        $asset_share_list  = [];
        while($asset_share_result = mysqli_fetch_assoc($asset_share_query))
        {
            $asset_share_list[$asset_share_result['as_no']] = $asset_share_result['name'];
        }

        return $asset_share_list;
    }

    public function getLastManagementNo($my_c_ini)
    {
        $asset_chk_sql      = "SELECT REPLACE(management, '{$my_c_ini}', '') as manage_no FROM asset WHERE management like '{$my_c_ini}%' ORDER BY management DESC LIMIT 1";
        $asset_chk_query    = mysqli_query($this->my_db, $asset_chk_sql);
        $asset_chk_result   = mysqli_fetch_assoc($asset_chk_query);

        return isset($asset_chk_result['manage_no']) ? $asset_chk_result['manage_no'] : 1;
    }

}
