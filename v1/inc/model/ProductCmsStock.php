<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class ProductCmsStock extends Model
{
    protected $_main_table      = "product_cms_stock";
    protected $_report_table    = "product_cms_stock_report";
    protected $_transfer_table  = "product_cms_stock_transfer";
    protected $_confirm_table   = "product_cms_stock_confirm";
    protected $_main_key        = "no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new ProductCmsStock();
    }

    public function setStockReport()
    {
        $this->_main_table  = $this->_report_table;
        $this->_main_key    = "no";
    }

    public function getEndItem($confirm_no)
    {
        $start_item     = $this->getItem($confirm_no);
        $chk_end_sql    = "SELECT * FROM {$this->_confirm_table} WHERE base_mon='{$start_item['base_mon']}' AND base_type='end' AND prd_unit='{$start_item['prd_unit']}' AND log_c_no='{$start_item['log_c_no']}' AND warehouse='{$start_item['warehouse']}'";
        $chk_end_query  = mysqli_query($this->my_db, $chk_end_sql);
        $chk_end_item   = mysqli_fetch_assoc($chk_end_query);

        return $chk_end_item;
    }

    public function getStockStateOption()
    {
        $state_sql      = "SELECT DISTINCT `state` FROM {$this->_report_table}";
        $state_query    = mysqli_query($this->my_db, $state_sql);
        $state_option   = array(
            "발주입고", "보상입고", "기타입고", "내부이동", "정상반출", "매장반출", "판매반출", "타계정반출", "이동반출", "기타반출", "AS입고", "오배송입고", "오배송반출"
        );
        while($state_result = mysqli_fetch_assoc($state_query)){
            if(!in_array($state_result['state'], $state_option)){
                $state_option[] = $state_result['state'];
            }
        }

        return $state_option;
    }

    public function getTransferTypeOption()
    {
        $type_sql      = "SELECT DISTINCT `t_type` FROM {$this->_transfer_table}";
        $type_query    = mysqli_query($this->my_db, $type_sql);
        $type_option   = [];
        while($type_result = mysqli_fetch_assoc($type_query)){
            $type_option[] = $type_result['t_type'];
        }

        return $type_option;
    }

    public function getTransferStateOption()
    {
        $state_sql      = "SELECT DISTINCT `t_state` FROM {$this->_transfer_table}";
        $state_query    = mysqli_query($this->my_db, $state_sql);
        $state_option   = [];
        while($state_result = mysqli_fetch_assoc($state_query)){
            $state_option[] = $state_result['t_state'];
        }

        return $state_option;
    }

    public function getStockReportItem($stock_no)
    {
        $stock_sql      = "SELECT *, (SELECT pcu.sup_price_vat FROM product_cms_unit pcu WHERE pcu.no=pcsr.prd_unit) as unit_price, (SELECT pcu.expiration FROM product_cms_unit pcu WHERE pcu.no=pcsr.prd_unit) as expiration FROM product_cms_stock_report pcsr WHERE pcsr.`no`='{$stock_no}'";
        $stock_query    = mysqli_query($this->my_db, $stock_sql);
        $stock_data     = mysqli_fetch_assoc($stock_query);

        return $stock_data;
    }

    public function setConfirmInitData($insert_data_list)
    {
        $result = true;
        if(!empty($insert_data_list))
        {
            $multi_ins_sql = "";
            foreach($insert_data_list as $insert_data)
            {
                $ins_sql = "INSERT INTO {$this->_confirm_table} SET ";
                if (!empty($insert_data))
                {

                    $comma = "";
                    foreach ($insert_data as $key => $data)
                    {
                        if ((string)$data == "NULL") {
                            $ins_sql .= "{$comma} `{$key}`=NULL";
                        } else {
                            $ins_sql .= "{$comma} `{$key}`='{$data}'";
                        }

                        $comma = " , ";
                    }
                }
                $ins_sql .= ";";

                $multi_ins_sql .= $ins_sql;
            }

            if (!mysqli_multi_query($this->my_db, $multi_ins_sql)) {
                $result = false;
            }
        }

        return $result;
    }

    public function updConfirmInitData($upd_data_list)
    {
        $result = true;
        if(!empty($upd_data_list))
        {
            $multi_upd_sql = "";
            foreach($upd_data_list as $upd_data)
            {
                if (!empty($upd_data))
                {
                    $upd_sql    = "UPDATE {$this->_confirm_table} SET ";
                    $comma      = "";
                    $upd_where  = "";
                    foreach ($upd_data as $key => $data)
                    {
                        if($key == 'base_mon' || $key == 'base_type' || $key == 'prd_unit')
                        {
                            $upd_where .= !empty($upd_where) ? " AND `{$key}`='{$data}'" : "`{$key}`='{$data}'";
                        }
                        else
                        {
                            if ((string)$data == "NULL") {
                                $upd_sql .= "{$comma} `{$key}`=NULL";
                            } else {
                                $upd_sql .= "{$comma} `{$key}`='{$data}'";
                            }
                            $comma = " , ";
                        }
                    }

                    if(!empty($upd_where)){
                        $upd_sql .= " WHERE {$upd_where};";
                        $multi_upd_sql .= $upd_sql;
                    }
                }
            }

            if (!mysqli_multi_query($this->my_db, $multi_upd_sql)) {
                $result = false;
            }
        }

        return $result;
    }

    public function updateConfirmPrice($no, $value)
    {
        $confirm_item   = $this->getItem($no);
        $upd_sql        = "UPDATE {$this->_confirm_table} SET price='{$value}' WHERE base_mon='{$confirm_item['base_mon']}' AND prd_unit='{$confirm_item['prd_unit']}' AND `base_type`='start'";

        if(mysqli_query($this->my_db, $upd_sql)){
            return true;
        }else{
            return false;
        }
    }

    public function getStockCenterList()
    {
        $center_sql     = "SELECT DISTINCT pcs.stock_center FROM product_cms_stock pcs WHERE pcs.stock_center IS NOT NULL ORDER BY stock_center ASC";
        $center_query   = mysqli_query($this->my_db, $center_sql);
        $center_list    = [];
        while($center = mysqli_fetch_assoc($center_query))
        {
            $center_list[] = $center['stock_center'];
        }

        return $center_list;
    }

    public function getWarehouseList()
    {
        $stock_warehouse_sql    = "SELECT * FROM product_cms_stock_warehouse ORDER BY warehouse";
        $stock_warehouse_query  = mysqli_query($this->my_db, $stock_warehouse_sql);
        $stock_warehouse_list   = [];
        while($stock_warehouse = mysqli_fetch_assoc($stock_warehouse_query)){
            $stock_warehouse_list[$stock_warehouse['warehouse']] = $stock_warehouse['warehouse'];
        }

        return $stock_warehouse_list;
    }

    public function getWarehouseBaseList()
    {
        $stock_warehouse_sql    = "SELECT * FROM product_cms_stock_warehouse WHERE `type`='1'";
        $stock_warehouse_query  = mysqli_query($this->my_db, $stock_warehouse_sql);
        $stock_warehouse_list   = [];
        while($stock_warehouse = mysqli_fetch_assoc($stock_warehouse_query)){
            $stock_warehouse_list[$stock_warehouse['log_c_no']][$stock_warehouse['warehouse']] = $stock_warehouse['warehouse'];
        }

        return $stock_warehouse_list;
    }
}
