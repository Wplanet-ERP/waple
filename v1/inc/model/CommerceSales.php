<?php

if (!defined('BASEPATH') && $_SERVER['DOCUMENT_ROOT'] != '') {
    define('BASEPATH', $_SERVER['DOCUMENT_ROOT']);
}elseif(!defined('BASEPATH')){
    define('BASEPATH', "/home/master/wplanet");
}
require_once BASEPATH.'/v1/inc/core/Model.php';

class CommerceSales extends Model
{
    protected $_main_table  = "commerce_report";
    protected $_main_key    = "cr_no";

    function __construct() {
        parent::__construct();
    }

    public static function Factory()
    {
        return new CommerceSales();
    }

    public function existReportData($where_data)
    {
        $add_where = "1=1";
        if(!empty($where_data)){
            foreach($where_data as $w_key => $w_data){
                $add_where .= " AND `{$w_key}`='{$w_data}'";
            }
        }

        $exist_sql    = "SELECT count(*) as cnt FROM {$this->_main_table} WHERE {$add_where}";
        $exist_query  = mysqli_query($this->my_db, $exist_sql);
        $exist_result = mysqli_fetch_assoc($exist_query);

        return $exist_result['cnt'] > 0 ? true : false;
    }

    public function getChartReportData()
    {
        $commerce_group_sql = "
            SELECT
                k_code,
                k_name,
                k_name_code,
                k_parent,
                display
            FROM kind
            WHERE k_code='sales' OR k_code='cost' ORDER BY k_code ASC, priority ASC
        ";

        $comm_total_list = [];
        $commerce_group_query = mysqli_query($this->my_db, $commerce_group_sql);
        while($commerce_group = mysqli_fetch_array($commerce_group_query))
        {
            if(empty($commerce_group['k_parent'])){
                $comm_total_list["comm_g1_list"][] = array(
                    "k_name"	  => trim($commerce_group['k_name']),
                    "k_name_code" => trim($commerce_group['k_name_code'])
                );

                $commerce_g1_title = ($commerce_group['display'] == '2') ? $commerce_group['k_name']." [OFF]" : $commerce_group['k_name'];
                $comm_total_list["comm_g1_init"][$commerce_group['k_name_code']] = array('title' => $commerce_g1_title, 'price' => 0);
                $comm_total_list["comm_g1_title"][$commerce_group['k_name_code']] = array('title' => $commerce_g1_title, 'type' => $commerce_group['k_code']);
            }else{
                $comm_total_list["comm_g2_list"][$commerce_group['k_parent']][] = array(
                    "k_name"	  => trim($commerce_group['k_name']),
                    "k_name_code" => trim($commerce_group['k_name_code'])
                );

                $commerce_g2_title = ($commerce_group['display'] == '2') ? $commerce_group['k_name']." [OFF]" : $commerce_group['k_name'];
                $comm_total_list["comm_g2_init"][$commerce_group['k_parent']][$commerce_group['k_name_code']] = array('title' => $commerce_g2_title, 'price' => 0);
                $comm_total_list["comm_g2_title"][$commerce_group['k_parent']][$commerce_group['k_name_code']] = array('title' => $commerce_g2_title, 'type' => $commerce_group['k_code']);
            }
        }

        return $comm_total_list;
    }

    public function getChartAutoReportData()
    {
        $commerce_group_sql = "
            SELECT
                k_code,
                k_name,
                k_name_code,
                k_parent,
                display
            FROM kind
            WHERE (k_code='sales' OR k_code='cost')
            ORDER BY k_code ASC, priority ASC
        ";
        $commerce_group_query   = mysqli_query($this->my_db, $commerce_group_sql);
        $comm_total_list        = [];
        while($commerce_group = mysqli_fetch_array($commerce_group_query))
        {
            if(empty($commerce_group['k_parent'])){
                $comm_total_list["comm_g1_list"][] = array(
                    "k_name"	  => trim($commerce_group['k_name']),
                    "k_name_code" => trim($commerce_group['k_name_code'])
                );

                $commerce_g1_title = ($commerce_group['display'] == '2') ? $commerce_group['k_name']." [OFF]" : $commerce_group['k_name'];
                $comm_total_list["comm_g1_init"][$commerce_group['k_name_code']] = array('title' => $commerce_g1_title, 'price' => 0);
                $comm_total_list["comm_g1_title"][$commerce_group['k_name_code']] = array('title' => $commerce_g1_title, 'type' => $commerce_group['k_code']);
            }else{
                $comm_total_list["comm_g2_list"][$commerce_group['k_parent']][] = array(
                    "k_name"	  => trim($commerce_group['k_name']),
                    "k_name_code" => trim($commerce_group['k_name_code'])
                );

                $commerce_g2_title = ($commerce_group['display'] == '2') ? $commerce_group['k_name']." [OFF]" : $commerce_group['k_name'];
                $comm_total_list["comm_g2_init"][$commerce_group['k_parent']][$commerce_group['k_name_code']] = array('title' => $commerce_g2_title, 'price' => 0);
                $comm_total_list["comm_g2_title"][$commerce_group['k_parent']][$commerce_group['k_name_code']] = array('title' => $commerce_g2_title, 'type' => $commerce_group['k_code']);
            }
        }

        return $comm_total_list;
    }
}
