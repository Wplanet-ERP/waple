<?php
require('common.php');
require('helper/_upload.php');

$file   = $_FILES["file"];
$folder = "dropzone_tmp";

if ($file) {
    $result     = true;
    $save_name  = move_dropzone_file($file, $folder);

    if(strpos($file['type'][0], "image") !== false){
        $file_type = "img";
    }else{
        $file_type = "file";
    }

    if($save_name == 'fail'){
        $result = false;
    }

    $return_data = array("result" => $result, "filePath" => $save_name, "fileName" => $file["name"][0], "fileType" => $file_type);

    echo json_encode($return_data);
}