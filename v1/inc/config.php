<?php
extract($_POST);
extract($_GET);

$ip= $_SERVER["REMOTE_ADDR"] ;
$pub_date = date("Y-m-d G:i:s");
$tgname = date("YmdGis");
$fdate = date("Y-m-d");

$dbconfigurl = $baseurl.'libs/dbconfig.php';

if(file_exists($dbconfigurl)) {
	require($dbconfigurl);
	if (trim($user)=="") {
		echo "libs/dbconfig.php 파일이 존재하지 않습니다.";
	}
} else {
	echo "libs/dbconfig.php 파일이 존재하지 않습니다.";
	exit;
}

require($baseurl.'v1/inc/lib_query.php');
require($baseurl.'v1/inc/lib.php');				// 공통 function 사용

// 스마티 클래스 연결
require($baseurl.'libs/SmartyBC.class.php');
$smarty = new SmartyBC();
$smarty->left_delimiter	=	"<{";
$smarty->right_delimiter	=	"}>";
$smarty->template_dir	= $baseur2."smarty/templates/".$second_forder;
$smarty->compile_dir	= $baseur2."smarty/templates_c/";

$smarty->caching	=	false;
//$smarty->cache_dir	=	"smarty/cache/";
$smarty->cache_lifetime	=	40;//30;
$smarty->compile_check = true;

$smarty->assign(array(	"baseurl"=>$baseurl,
						"baseur2"=>$baseur2,
						"second_forder"=>$second_forder,
						"today"=>$fdate,
						"fday"=>date("Y-m")."-01",
						"fmon"=>date("Y")."-01-01",
						"fyear"=>((int)date("Y")-1)."-01-01"
));
$tpl_url = $baseur2."smarty/templates/".$second_forder;

?>
