<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/ProductCms.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);


$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:A2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("B1:B2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("C1:C2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("D1:D2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("E1:E2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("F1:F2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("G1:K1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("L1:P1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("Q1:Q2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("R1:R2");

$lfcr = chr(10) ;

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "NO")
    ->setCellValue('B1', "Display")
    ->setCellValue('C1', "업체명/브랜드명")
    ->setCellValue('D1', "적재정보명")
    ->setCellValue('E1', "구성품목명")
    ->setCellValue('F1', "최근 변경일{$lfcr}수정자")
    ->setCellValue('G1', "카톤박스")
    ->setCellValue('G2', "가로(mm)")
    ->setCellValue('H2', "세로(mm)")
    ->setCellValue('I2', "높이(mm)")
    ->setCellValue('J2', "중량(g)")
    ->setCellValue('K2', "입수량")
    ->setCellValue('L1', "단품")
    ->setCellValue('L2', "가로(mm)")
    ->setCellValue('M2', "세로(mm)")
    ->setCellValue('N2', "높이(mm)")
    ->setCellValue('O2', "중량(g)")
    ->setCellValue('P2', "입수량")
    ->setCellValue('Q1', "비고")
    ->setCellValue('R1', "작성일{$lfcr}작성자")
;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getStyle('A1:R2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF999999');
$objPHPExcel->getActiveSheet()->getStyle("A1:R2")->getFont()->setColor($fontColor);

$add_where      = "1=1";
$sch_brand      = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_no         = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_display    = isset($_GET['sch_display']) ? $_GET['sch_display'] : "";
$sch_load_title = isset($_GET['sch_load_title']) ? $_GET['sch_load_title'] : "";
$sch_memo       = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";

if(!empty($sch_brand)){
    $add_where   .= " AND `pl`.brand = '{$sch_brand}'";
}

if(!empty($sch_no)){
    $add_where   .= " AND `pl`.pl_no = '{$sch_no}'";
}

if(!empty($sch_display)){
    $add_where   .= " AND `pl`.display = '{$sch_display}'";
}

if(!empty($sch_load_title)){
    $add_where   .= " AND `pl`.load_title LIKE '%{$sch_load_title}%'";
}

if(!empty($sch_memo)){
    $add_where   .= " AND `pl`.memo LIKE '%{$sch_memo}%'";
}

$product_cms_load_sql = "
    SELECT 
        *,
        DATE_FORMAT(`pl`.regdate, '%Y-%m-%d') as reg_day,
        DATE_FORMAT(`pl`.upd_date, '%Y-%m-%d') as upd_day,
        (SELECT s_name FROM staff s WHERE s.s_no=`pl`.reg_s_no) AS reg_name,
        (SELECT s_name FROM staff s WHERE s.s_no=`pl`.upd_s_no) AS upd_name
    FROM product_cms_load `pl`
    WHERE {$add_where}
    ORDER BY `pl_no` DESC
";
$product_cms_load_query = mysqli_query($my_db, $product_cms_load_sql);
$cms_model              = ProductCms::Factory();
$brand_option           = $cms_model->getLoadBrandOption();
$idx = 3;
while($product_cms_load = mysqli_fetch_assoc($product_cms_load_query))
{
    $load_unit_sql      = "SELECT * FROM product_cms_load_unit as plu WHERE plu.pl_no='{$product_cms_load['pl_no']}'";
    $load_unit_query    = mysqli_query($my_db, $load_unit_sql);
    $load_unit_text     = "";
    while($load_unit = mysqli_fetch_assoc($load_unit_query)){
        $load_unit_text .= empty($load_unit_text) ? $load_unit['sku'] : $lfcr.$load_unit['sku'];
    }

    $display_name   = ($product_cms_load['display'] == "1") ? "ON" : "OFF";
    $brand_name     = isset($brand_option[$product_cms_load['brand']]) ? $brand_option[$product_cms_load['brand']] : "";
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$idx}", $product_cms_load['pl_no'])
        ->setCellValue("B{$idx}", $display_name)
        ->setCellValue("C{$idx}", $brand_name)
        ->setCellValue("D{$idx}", $product_cms_load['load_title'])
        ->setCellValue("E{$idx}", $load_unit_text)
        ->setCellValue("F{$idx}", $product_cms_load['upd_day'].$lfcr.$product_cms_load['upd_name'])
        ->setCellValue("G{$idx}", number_format($product_cms_load['box_width']))
        ->setCellValue("H{$idx}", number_format($product_cms_load['box_length']))
        ->setCellValue("I{$idx}", number_format($product_cms_load['box_height']))
        ->setCellValue("J{$idx}", number_format($product_cms_load['box_weight']))
        ->setCellValue("K{$idx}", number_format($product_cms_load['box_amount']))
        ->setCellValue("L{$idx}", number_format($product_cms_load['single_width']))
        ->setCellValue("M{$idx}", number_format($product_cms_load['single_length']))
        ->setCellValue("N{$idx}", number_format($product_cms_load['single_height']))
        ->setCellValue("O{$idx}", number_format($product_cms_load['single_weight']))
        ->setCellValue("P{$idx}", number_format($product_cms_load['single_amount']))
        ->setCellValue("Q{$idx}", $product_cms_load['memo'])
        ->setCellValue("R{$idx}", $product_cms_load['reg_day'].$lfcr.$product_cms_load['reg_name'])
    ;

    $idx++;
}
$idx--;

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:R{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:R2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:R{$idx}")->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A1:R{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:R{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("A1:R2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("A3:A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("B3:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("C3:C{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("D3:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("E3:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("F3:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("Q3:Q{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("R3:R{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("D3:D{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("E3:E{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("F1:F{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("Q3:Q{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("R1:R{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);

$excel_filename = "적재정보";
$objPHPExcel->getActiveSheet()->setTitle($excel_filename);
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.date("Ymd")."_".$excel_filename.".xlsx");

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save('php://output');
?>
