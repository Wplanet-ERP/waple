<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');

if(!permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자")){
    $smarty->display('access_error.html');
    exit;
}

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where = "1=1";
$sch_keyword  = isset($_GET['sch_keyword'])?$_GET['sch_keyword']:"";
$sch_strength = isset($_GET['sch_strength'])?$_GET['sch_strength']:"";

if(!empty($sch_keyword)) {
	$add_where.=" AND keyword like '%".$sch_keyword."%'";
	$smarty->assign("sch_keyword", $sch_keyword);
}

if(!empty($sch_strength)) {
	$add_where.=" AND strength like '%".$sch_strength."%'";
	$smarty->assign("sch_strength", $sch_strength);
}


// 리스트 쿼리
$strength_sql = "
	SELECT
		*
	FROM staff_strength_keyword
	WHERE $add_where
	ORDER BY k_no DESC
";

// 전체 게시물 수
$cnt_sql 	= "SELECT count(*) FROM (".$strength_sql.") AS cnt";
$result		= mysqli_query($my_db, $cnt_sql);
$cnt_data	= mysqli_fetch_array($result);
$total_num 	= $cnt_data[0];

// 페이에 따른 limit 설정
$pages 	= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 	= 10;
$offset = ($pages-1) * $num;

$smarty->assign("total_num",$total_num);
$pagenum = ceil($total_num/$num);

// 페이징
if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

// 검색 조건
$search_url = getenv("QUERY_STRING");

$smarty->assign("search_url",$search_url);
$page=pagelist($pages, "staff_strength_list.php", $pagenum, $search_url);
$smarty->assign("pagelist",$page);


// 리스트 쿼리 결과(데이터)
$strength_sql  .= "LIMIT $offset, $num";
$result			= mysqli_query($my_db, $strength_sql);
$strength_list	= [];

while($strength = mysqli_fetch_array($result))
{
	$strength_list[] = $strength;
}

$smarty->assign("strength_list", $strength_list);
$smarty->display('staff_strength_list.html');
?>
