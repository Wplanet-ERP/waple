<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/company.php');
require('inc/helper/project.php');
require('inc/model/MyCompany.php');
require('inc/model/Kind.php');
require('inc/model/Staff.php');

# Process 처리
$proc				= isset($_POST['process']) ? $_POST['process'] : "";

if($proc == "write")
{
	// 아이디 자동생성 20160325 김윤영 Start
	$c_no_sql	 ="SELECT max(c_no) FROM company";
	$c_no_query  = mysqli_query($my_db, $c_no_sql);
	$c_no_max 	 = mysqli_fetch_row($c_no_query);
	$c_no_max[0] = $c_no_max[0]+1;

	$now_date 	 = date("Ymd");
	$company_id  = $now_date . "_" . $c_no_max[0];

	$f_c_name_post	= addslashes(trim($_POST['f_c_name']));

	$check_sql		="select count(c_no) as cnt from company where id='{$company_id}' or c_name='{$f_c_name_post}'";
	$check_query	= mysqli_query($my_db, $check_sql);
	$check_data		= mysqli_fetch_array($check_query);
	if($check_data['cnt'] != 0)
		exit("<script>alert('동일한 업체명이 이미 등록되어 있습니다.');history.back();</script>");

	$tels[]	= trim($_POST['f_tel_0']);
	$tels[]	= trim($_POST['f_tel_1']);
	$tels[]	= trim($_POST['f_tel_2']);
	$tel	= implode("-",$tels);

	$faxs[]	= trim($_POST['f_fax_0']);
	$faxs[]	= trim($_POST['f_fax_1']);
	$faxs[]	= trim($_POST['f_fax_2']);
	$fax	= implode("-",$faxs);

	$f_emails[]		= trim($_POST['f_email_0']);
	$f_emails[]		= trim($_POST['f_email_1']);
	$f_email		= implode("@",$f_emails);

	$tx_emails[]	= trim($_POST['tx_email_0']);
	$tx_emails[]	= trim($_POST['tx_email_1']);
	$tx_email		= implode("@",$tx_emails);

	$registdoc_file 	 = $_FILES["registdoc_file"];
	$registdoc_file_name = (!empty($registdoc_file['name'])) ? add_store_file($registdoc_file, "company") : "";

	$accdoc_file 		 = $_FILES["accdoc_file"];
	$accdoc_file_name 	 = (!empty($accdoc_file['name'])) ? add_store_file($accdoc_file, "company") : "";

	$etc_file			 = $_FILES["etc_file"];
	$etc_file_name		 = (!empty($etc_file['name'])) ? add_store_file($etc_file, "company") : "";

	$upload_file1 = $registdoc_file["name"] ? $registdoc_file["name"] : $_POST['f_o_registdoc'];
    $upload_file2 = $accdoc_file["name"] ? $accdoc_file["name"] : $_POST['f_o_accdoc'];
    $upload_file3 = $etc_file["name"] ? $etc_file["name"] : $_POST['f_o_etc'];

	$save_name[0] = $registdoc_file_name ? $registdoc_file_name : $_POST['f_r_registdoc'];
	$save_name[1] = $accdoc_file_name ? $accdoc_file_name : $_POST['f_r_accdoc'];
	$save_name[2] = $etc_file_name ? $etc_file_name : $_POST['f_r_etc'];

	$dp_e_type 	= isset($_POST['f_dp_e_type']) ? $_POST['f_dp_e_type'] : "0";
	$birthday  	= empty($_POST['f_birthday']) ? "birthday=NULL," : "birthday='{$_POST['f_birthday']}',";
	$f_priority = !empty($_POST['f_priority']) ? $_POST['f_priority'] : 9999;

	$f_adv_fee 	= !empty($_POST['f_adv_fee']) ? $_POST['f_adv_fee'] : 0;
	$f_adv_type = !empty($_POST['f_adv_type']) ? $_POST['f_adv_type'] : 0;

	$f_keyword	= $_POST['f_keyword'] ? implode(',', $_POST['f_keyword']) : "";

	$ins_sql 	= "
		INSERT INTO company SET
			my_c_no 		= '{$_POST['f_my_c_no']}',
			s_no 			= '{$_POST['f_s_no']}',
			display 		= '{$_POST['f_display']}',
			corp_kind 		= '{$_POST['f_corp_kind']}',
			id 				= '{$company_id}',
			c_name 			= '".addslashes(trim($_POST['f_c_name']))."',
			license_type 	= '{$_POST['f_license_type']}',
			cate1 			= '{$_POST['f_cate1']}',
			cate2 			= '{$_POST['f_cate2']}',
			about 			= '".addslashes($_POST['f_about'])."',
			location1 		= '{$_POST['f_location1']}',
			location2 		= '{$_POST['f_location2']}',
			zipcode 		= '{$_POST['f_zipcode']}',
			address1 		= '{$_POST['f_address1']}',
			address2 		= '".addslashes($_POST['f_address2'])."',
			tel				= '{$tel}',
			fax 			= '{$fax}',
			tel_text 		= '".addslashes($_POST['f_tel_text'])."',
			email 			= '{$f_email}',
			parking 		= '".addslashes($_POST['f_parking'])."',
			site 			= '".addslashes($_POST['f_site'])."',
			{$birthday}
			tx_company 			= '{$_POST['f_tx_company']}',
			tx_company_number 	= '".addslashes($_POST['f_tx_company_number'])."',
			tx_company_ceo 		= '".addslashes($_POST['f_tx_company_ceo'])."',
			tx_manager 			= '{$_POST['f_tx_manager']}',
			tx_call1 			= '{$_POST['f_tx_call1']}',
			tx_call2 			= '{$_POST['f_tx_call2']}',
			tx_email 			= '{$tx_email}',
			bk_title 			= '{$_POST['f_bk_title']}',
			bk_num 				= '{$_POST['f_bk_num']}',
			bk_name 			= '{$_POST['f_bk_name']}',
			bk_manager 			= '{$_POST['f_bk_manager']}',
			regdate 			= now(),
			ip 					= '".ip2long($_SERVER['REMOTE_ADDR'])."',
			o_registration 		= '".addslashes($upload_file1)."',
			r_registration 		= '{$save_name[0]}',
			o_accdoc 			= '".addslashes($upload_file2)."',
			r_accdoc 			= '{$save_name[1]}',
			o_etc 				= '".addslashes($upload_file3)."',			
			r_etc 				= '{$save_name[2]}',
			dp_e_type 			= '{$dp_e_type}',
			adv_type			= '{$f_adv_type}',
			adv_fee				= '{$f_adv_fee}',
			keyword				= '".addslashes($f_keyword)."',
			naver				= '{$_POST['f_naver']}',
			naver_gfa			= '{$_POST['f_naver_gfa']}',
			naver_nosp			= '{$_POST['f_naver_nosp']}',
			kakao				= '{$_POST['f_kakao']}',
			google				= '{$_POST['f_google']}',
			priority			= '{$f_priority}'
	";

	if(permissionNameCheck($session_permission, "마스터관리자")){
		$ins_sql .= ",brand = '{$_POST['f_brand']}'";
	}

    $resource_type = isset($_POST['f_resource_type']) ? $_POST['f_resource_type'] : "";
    if(!empty($resource_type)){
        $resource_type_val 	= implode(",", $resource_type);
        $ins_sql .= ",resource_type='{$resource_type_val}";
    }else{
        $ins_sql .= ",resource_type=NULL";
	}

	if(mysqli_query($my_db, $ins_sql))
	{
		$new_c_no = mysqli_insert_id($my_db);
		if(!empty($_POST['f_cc_no'])){
			$upd_sql = "UPDATE company_contact SET c_no='{$new_c_no}', is_recontact='2' WHERE cc_no='{$_POST['f_cc_no']}'";
			mysqli_query($my_db, $upd_sql);
		}

		$comp_name 		= addslashes(trim($_POST['f_c_name']));
		$staff_model 	= Staff::Factory();
		$staff_name 	= $staff_model->getStaffName($_POST['f_s_no']);
		$comp_msg  		= "[파트너 등록 알림] `{$comp_name}` ({$staff_name}) 생성되었습니다.\r\nhttps://work.wplanet.co.kr/v1/company_regist.php?no={$new_c_no}";
		$comp_msg 		= addslashes($comp_msg);

		$chat_ins_sql   = "INSERT INTO waple_chat_content SET wc_no='2', s_no='18', content='{$comp_msg}', alert_type='41', regdate=now()";
		mysqli_query($my_db, $chat_ins_sql);

        exit("<script>alert('등록했습니다.');location.href='company_list.php';</script>");
	}else{
        exit("<script>alert('등록에 실패했습니다');history.back();</script>");
	}
}
elseif($proc == "modify")
{
    $tels[]	= trim($_POST['f_tel_0']);
    $tels[]	= trim($_POST['f_tel_1']);
    $tels[]	= trim($_POST['f_tel_2']);
    $tel	= implode("-",$tels);

    $faxs[]	= trim($_POST['f_fax_0']);
    $faxs[]	= trim($_POST['f_fax_1']);
    $faxs[]	= trim($_POST['f_fax_2']);
    $fax	= implode("-",$faxs);

	$f_emails[]		= trim($_POST['f_email_0']);
	$f_emails[]		= trim($_POST['f_email_1']);
	$f_email		= implode("@",$f_emails);

    $tx_emails[]	= trim($_POST['tx_email_0']);
    $tx_emails[]	= trim($_POST['tx_email_1']);
    $tx_email		= implode("@",$tx_emails);

    $registdoc_file 	 = $_FILES["registdoc_file"];
    $registdoc_file_name = (!empty($registdoc_file['name'])) ? add_store_file($registdoc_file, "company") : "";

    $accdoc_file 		 = $_FILES["accdoc_file"];
    $accdoc_file_name 	 = (!empty($accdoc_file['name'])) ? add_store_file($accdoc_file, "company") : "";

    $etc_file			 = $_FILES["etc_file"];
    $etc_file_name		 = (!empty($etc_file['name'])) ? add_store_file($etc_file, "company") : "";

    $upload_file1 = $registdoc_file["name"] ? $registdoc_file["name"] : $_POST['f_o_registdoc'];
    $upload_file2 = $accdoc_file["name"] ? $accdoc_file["name"] : $_POST['f_o_accdoc'];
    $upload_file3 = $etc_file["name"] ? $etc_file["name"] : $_POST['f_o_etc'];

    $save_name[0] = $registdoc_file_name ? $registdoc_file_name : $_POST['f_r_registdoc'];
    $save_name[1] = $accdoc_file_name ? $accdoc_file_name : $_POST['f_r_accdoc'];
    $save_name[2] = $etc_file_name ? $etc_file_name : $_POST['f_r_etc'];

	$birthday  		= empty($_POST['f_birthday']) ? "birthday=NULL," : "birthday='{$_POST['f_birthday']}',";
    $resource_type 	= isset($_POST['f_resource_type']) ? $_POST['f_resource_type'] : "";

	$add_dp_type	= "";
	if(permissionNameCheck($session_permission, "마스터관리자"))
	{
		if(isset($_POST['f_dp_e_type']) || !empty($_POST['f_org_dp_e_type'])){
			$dp_e_type   = !empty($_POST['f_dp_e_type']) ? $_POST['f_dp_e_type'] : "0";
			$add_dp_type = "dp_e_type='{$dp_e_type}',";
		}
	}

    $resource_upd  	= ",resource_type=NULL";

    if(!empty($resource_type)){
        $resource_type_val 	= implode(",", $resource_type);
    	$resource_upd  		= ",resource_type='{$resource_type_val}'";
	}

	$f_priority = !empty($_POST['f_priority']) ? $_POST['f_priority'] : 9999;

	$brand_etc_upd 	= "";
	$f_adv_fee 		= !empty($_POST['f_adv_fee']) ? $_POST['f_adv_fee'] : 0;
	$f_adv_type 	= !empty($_POST['f_adv_type']) ? $_POST['f_adv_type'] : 0;
	if(permissionNameCheck($session_permission, "마스터관리자")){
		$brand_etc_upd = ",c_name = '".addslashes(trim($_POST['f_c_name']))."', brand = '{$_POST['f_brand']}', adv_type= '{$f_adv_type}', adv_fee = '{$f_adv_fee}', naver = '{$_POST['f_naver']}',naver_gfa = '{$_POST['f_naver_gfa']}', naver_nosp = '{$_POST['f_naver_nosp']}', kakao = '{$_POST['f_kakao']}', google = '{$_POST['f_google']}'";
	}

	$add_lector_data = "";
	if($_POST['f_corp_kind'] == '3' && !empty($_POST['f_lector_vat_type'])){
		$add_lector_data = "lector_vat_type='{$_POST['f_lector_vat_type']}',";
	}

	$f_keyword	= $_POST['f_keyword'] ? implode(',', $_POST['f_keyword']) : "";

	$upd_sql = "
		UPDATE company SET
			my_c_no 		= '{$_POST['f_my_c_no']}',
			s_no 			= '{$_POST['f_s_no']}',
			display 		= '{$_POST['f_display']}',
			corp_kind 		= '{$_POST['f_corp_kind']}',
			license_type 	= '{$_POST['f_license_type']}',
			cate1 			= '{$_POST['f_cate1']}',
			cate2 			= '{$_POST['f_cate2']}',
			about 			= '".addslashes($_POST['f_about'])."',
			location1 		= '{$_POST['f_location1']}',
			location2 		= '{$_POST['f_location2']}',
			zipcode 		= '{$_POST['f_zipcode']}',
			address1 		= '{$_POST['f_address1']}',
			address2 		= '".addslashes($_POST['f_address2'])."',
			tel				= '{$tel}',
			fax 			= '{$fax}',
			tel_text 		= '".addslashes($_POST['f_tel_text'])."',
			email 			= '{$f_email}',
			parking 		= '".addslashes($_POST['f_parking'])."',
			site 			= '".addslashes($_POST['f_site'])."',		
			{$birthday}
			tx_company 			= '{$_POST['f_tx_company']}',
			tx_company_number 	= '".addslashes($_POST['f_tx_company_number'])."',
			tx_company_ceo 		= '".addslashes($_POST['f_tx_company_ceo'])."',
			tx_manager 			= '{$_POST['f_tx_manager']}',
			tx_call1 			= '{$_POST['f_tx_call1']}',
			tx_call2 			= '{$_POST['f_tx_call2']}',
			tx_email 			= '{$tx_email}',
			bk_title 			= '{$_POST['f_bk_title']}',
			bk_num 				= '{$_POST['f_bk_num']}',
			bk_name 			= '{$_POST['f_bk_name']}',
			bk_manager 			= '{$_POST['f_bk_manager']}',
			o_registration 		= '".addslashes($upload_file1)."',
			r_registration 		= '{$save_name[0]}',
			o_accdoc 			= '".addslashes($upload_file2)."',
			r_accdoc 			= '{$save_name[1]}',
			o_etc 				= '".addslashes($upload_file3)."',
			r_etc 				= '{$save_name[2]}',
			keyword				= '".addslashes($f_keyword)."',
			{$add_lector_data}
			{$add_dp_type}
			priority			= '{$f_priority}'
			{$resource_upd}
			{$brand_etc_upd}
		WHERE
			c_no='{$_POST['idx']}'
	";

    if(mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('등록했습니다.');location.href='company_regist.php?no={$_POST['idx']}';</script>");
    }else{
        exit("<script>alert('등록에 실패했습니다');history.back();</script>");
    }
}
elseif($proc == "memo")
{
	$c_no 		= (isset($_POST['idx'])) ? $_POST['idx'] : "";
	$value 		= (isset($_POST['f_c_memo']))?addslashes($_POST['f_c_memo']):"";

	$upd_sql  	= "UPDATE company SET c_memo='".addslashes(trim($value))."', c_memo_date = '".date("Y-m-d H:i:s")."' where c_no='{$c_no}'";

	if(mysqli_query($my_db, $upd_sql)) {
        exit("<script>alert('메모를 등록하였습니다');location.href='company_regist.php?no={$c_no}';</script>");
	}else{
        exit("<script>alert('메모 등록에 실패했습니다');history.back();</script>");
	}
}
elseif($proc=="down_file")
{
	$c_no 		= (isset($_POST['idx'])) ? $_POST['idx'] : "";
	$kind 		= (isset($_POST['kind'])) ? $_POST['kind'] : "";

	$file_sql	= "";
	$filename 	= "";
	$fileread 	= "";

	if ($kind == 'registdoc'){
		$file_sql	= "SELECT o_registration, r_registration FROM company WHERE c_no = '{$c_no}'";
	} else if ($kind == 'accdoc'){
		$file_sql	= "SELECT o_accdoc, r_accdoc FROM company WHERE c_no = '{$c_no}'";
	} else if ($kind == 'etc'){
		$file_sql 	= "SELECT o_etc, r_etc FROM company WHERE c_no = '{$c_no}'";
	}

	$file_query = mysqli_query($my_db, $file_sql);
	$file_info 	= mysqli_fetch_array($file_query);

	if ($kind == 'registdoc'){
		$filename = stripslashes($file_info['o_registration']);
		$fileread = $file_info['r_registration'];
	} else if ($kind == 'accdoc'){
		$filename = stripslashes($file_info['o_accdoc']);
		$fileread = $file_info['r_accdoc'];
	} else if ($kind == 'etc'){
		$filename = stripslashes($file_info['o_etc']);
		$fileread = $file_info['r_etc'];
	}

	echo "File UP Name : {$fileread}<br>";
	echo "File Downloading...<br>File DN Name : {$filename}<br>";
	exit("<script>location.href='popup/file_download.php?file_dn_name=".urlencode($filename)."&file_up_name={$fileread}';</script>");

}
elseif($proc=="del_file")
{
	$c_no 		= (isset($_POST['idx'])) ? $_POST['idx'] : "";
	$kind 		= (isset($_POST['kind'])) ? $_POST['kind'] : "";

	$file_sql	= "";
    $del_sql 	= "";
    $fileread 	= "";

	if ($kind == 'registdoc'){
		$file_sql	= "SELECT o_registration, r_registration FROM company WHERE c_no = '{$c_no}'";
		$del_sql 	= "UPDATE company SET o_registration='', r_registration='' WHERE c_no = '{$c_no}'";
	} else if ($kind == 'accdoc'){
		$file_sql	= "SELECT o_accdoc, r_accdoc FROM company WHERE c_no = '{$c_no}'";
		$del_sql 	= "UPDATE company SET o_accdoc='', r_accdoc=''  WHERE c_no = '{$c_no}'";
	} else if ($kind == 'etc'){
		$file_sql 	= "SELECT o_etc, r_etc FROM company WHERE c_no = '{$c_no}'";
		$del_sql	= "UPDATE company SET o_etc='', r_etc=''  WHERE c_no = '{$c_no}'";
	}

	$file_query = mysqli_query($my_db, $file_sql);
	$file_info 	= mysqli_fetch_array($file_query);

	if ($kind == 'registdoc')
		$fileread = "uploads/".$file_info['r_registration'];
	else if ($kind == 'accdoc')
		$fileread = "uploads/".$file_info['r_accdoc'];
	else if ($kind == 'etc')
		$fileread = "uploads/".$file_info['r_etc'];


	unlink("./".$fileread);

	$my_db->query($del_sql);
	alert("선택한 파일을 삭제 하였습니다.","company_regist.php?no={$c_no}");
}
elseif($proc == "pw_reset")
{
	$c_no = (isset($_POST['idx'])) ? $_POST['idx'] : "";
	$f_company_pw_new	  = (isset($_POST['company_pw_new'])) ? $_POST['company_pw_new'] : "";
	$f_company_pw_confirm = (isset($_POST['company_pw_confirm'])) ? $_POST['company_pw_confirm'] : "";

	if($f_company_pw_new == $f_company_pw_confirm)
	{
		$upd_sql = "UPDATE company SET `pass`='".substr(md5($f_company_pw_new),8,16)."' WHERE c_no='{$c_no}'";
		mysqli_query($my_db, $upd_sql);

		exit("<script>alert('비밀번호가 변경되었습니다.');location.href='company_regist.php?no={$c_no}';</script>");
	}else{
		exit("<script>alert('변경 비밀번호 확인이 일치하지 않습니다.');location.href='company_regist.php?no={$c_no}';</script>");
	}
}
elseif($proc == "save_id")
{
	$c_no 	 = (isset($_POST['idx'])) ? $_POST['idx'] : "";
	$f_id 	 = (isset($_POST['f_id'])) ? addslashes($_POST['f_id']) : "";

	$upd_sql = "UPDATE company SET id='{$f_id}' WHERE c_no='{$c_no}'";

	if(!mysqli_query($my_db, $upd_sql)){
		echo ("<script>alert('아이디 저장에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('아이디를 등록하였습니다');</script>");
	}
	exit("<script>location.href='company_regist.php?no={$c_no}';</script>");
}


# c_no 값
$idx 	= isset($_GET['no']) ? $_GET['no'] : "";
$mode	= (empty($idx)) ? "write" : "modify";

$kind_model 		= Kind::Factory();
$brand_total_list 	= $kind_model->getKindGroupList("brand");
$brand_parent_list 	= $brand_total_list[''];
$brand_child_list	= [];

# MODE 처리
if($mode == "modify")
{
	$company_sql	="SELECT *, (SELECT k.k_parent FROM kind k WHERE k.k_name_code=c.brand) as brand_g1, (select s_name from staff b where s_no=`c`.s_no) as s_name, (select team from staff b where s_no=`c`.s_no) as team FROM company `c` WHERE c.c_no='{$idx}'";
	$company_query  = mysqli_query($my_db, $company_sql);
	$company 		= mysqli_fetch_array($company_query);

	if(!empty($company['brand_g1'])){
		$brand_child_list = $brand_total_list[$company['brand_g1']];
	}

	$smarty->assign(
		array(
			"c_no"		 	=> $company['c_no'],
			"f_my_c_no"	 	=> $company['my_c_no'],
			"f_s_no"	 	=> $company['s_no'],
			"f_team"	 	=> $company['team'],
			"id"		 	=> $company['id'],
			"f_c_name"	 	=> stripslashes($company['c_name']),
			"f_brand_g1"	=> $company['brand_g1'],
			"f_brand"		=> $company['brand'],
			"f_about"	 	=> $company['about'],
			"f_cate1"	 	=> $company['cate1'],
			"cate1_name" 	=> $company['cate1_name'],
			"f_cate2"	 	=> $company['cate2'],
			"cate2_name" 	=> $company['cate2_name'],
			"f_location1"	=> $company['location1'],
			"loca1_name"	=> $company['loca1_name'],
			"f_location2"	=> $company['location2'],
			"loca2_name"	=> $company['loca2_name'],
			"tel"			=> $company['tel'],
			"f_tel_text"	=> stripslashes($company['tel_text']),
			"fax"			=> $company['fax'],
			"zipcode"		=> $company['zipcode'],
			"f_address1"	=> $company['address1'],
			"f_address2"	=> stripslashes($company['address2']),
			"f_parking"		=> stripslashes($company['parking']),
			"f_site"		=> stripslashes($company['site']),
			"f_tx_company"	=> $company['tx_company'],
			"f_tx_company_number" =>stripslashes($company['tx_company_number']),
			"f_tx_company_ceo"	  =>stripslashes($company['tx_company_ceo']),
			"f_tx_manager"	=> $company['tx_manager'],
			"f_tx_call1"	=> $company['tx_call1'],
			"f_tx_call2"	=> $company['tx_call2'],
			"tx_email"		=> $company['tx_email'],
			"f_bk_title"	=> $company['bk_title'],
			"f_bk_num"		=> $company['bk_num'],
			"f_bk_name"		=> $company['bk_name'],
			"f_bk_manager"	=> $company['bk_manager'],
			"f_o_registdoc" => stripslashes($company['o_registration']),
			"f_r_registdoc" => $company['r_registration'],
			"f_o_accdoc" 	=> stripslashes($company['o_accdoc']),
			"f_r_accdoc" 	=> $company['r_accdoc'],
			"f_o_etc" 		=> stripslashes($company['o_etc']),
			"f_r_etc" 		=> $company['r_etc'],
			"f_corp_kind" 	=> $company['corp_kind'],
			"f_license_type"=> $company['license_type'],
			"f_c_memo"		=> $company['c_memo'],
			"f_c_memo_day"	=> date("Y/m/d",strtotime($company['c_memo_date'])),
			"f_c_memo_time"	=> date("H:i",strtotime($company['c_memo_date'])),
			"f_display" 	=> $company['display'],
			"f_dp_e_type" 	=> $company['dp_e_type'],
			"f_birthday" 	=> $company['birthday'],
			"f_adv_type"	=> $company['adv_type'],
			"f_adv_fee" 	=> $company['adv_fee'],
			"f_keyword"		=> $company['keyword'],
			"f_naver" 		=> $company['naver'],
			"f_naver_gfa" 	=> $company['naver_gfa'],
			"f_naver_nosp" 	=> $company['naver_nosp'],
			"f_kakao" 		=> $company['kakao'],
			"f_google" 		=> $company['google'],
			"f_priority"	=> $company['priority'],
			"f_lector_vat_type"	=> $company['lector_vat_type'],
		)
	);

	// 데이터 변환
	$tels = explode("-",$company['tel']);
	$smarty->assign(
		array(
			"f_tel_0"=>$tels[0],
			"f_tel_1"=>$tels[1],
			"f_tel_2"=>$tels[2]
		)
	);

	// 데이터 변환
	$faxs = explode("-",$company['fax']);
	$smarty->assign(
		array(
			"f_fax_0"=>$faxs[0],
			"f_fax_1"=>$faxs[1],
			"f_fax_2"=>$faxs[2]
		)
	);

	$f_emails = explode("@", $company['email']);
	$smarty->assign(
		array(
			"f_email_0"=>$f_emails[0],
			"f_email_1"=>$f_emails[1]
		)
	);

	$tx_emails = explode("@",$company['tx_email']);
	$smarty->assign(
		array(
			"tx_email_0"=>$tx_emails[0],
			"tx_email_1"=>$tx_emails[1]
		)
	);

	if(!empty($company['location1']))
	{
		$edit_loc_sql	= "SELECT k_name, k_name_code, k_parent from kind where k_code='location' and k_parent='{$company['location1']}'";
		$edit_loc_query = mysqli_query($my_db, $edit_loc_sql);
		$editlocation   = [];

		while($kind=mysqli_fetch_array($edit_loc_query)) {
			$editlocation[] = array(
				"location_name"	  => trim($kind['k_name']),
				"location_code"	  => trim($kind['k_name_code']),
				"location_parent" => trim($kind['k_parent'])
			);
		}

		$smarty->assign("editlocation", $editlocation);
	}

	if($company['corp_kind'] == '1')
	{
		$editjob_code  = "job";
		$corp_job_list = $kind_model->getKindParentList($editjob_code);
	}
	elseif($company['corp_kind'] == '2')
	{
		$editjob_code  = "company_job";
		$corp_job_list = $kind_model->getKindParentList($editjob_code);
	}

	if(!empty($company['cate1']))
	{
		$editjob_sql   = "select k_name, k_name_code, k_parent from kind where k_code='{$editjob_code}' and k_parent='{$company['cate1']}'";
		$editjob_query = mysqli_query($my_db, $editjob_sql);
		$editjob_list  = [];
		while($kind=mysqli_fetch_array($editjob_query))
		{
			$editjob_list[]=array(
				"job_name"	 => trim($kind['k_name']),
				"job_code"	 => trim($kind['k_name_code']),
				"job_parent" => trim($kind['k_parent'])
			);
		}

		$smarty->assign("editjob_list", $editjob_list);
	}

	$f_resource_type_list = [];
	if(!empty($company['resource_type'])){
		$f_resource_type_list = explode(',', $company['resource_type']);
	}
	$smarty->assign("f_resource_type_list", $f_resource_type_list);

}
elseif($mode == "write")
{
	$cc_no 			= isset($_GET['cc_no']) ? $_GET['cc_no'] : "";
	$prev_url_val 	= str_replace("https://work.wplanet.co.kr/v1/", "", $_SERVER['HTTP_REFERER']);
	$prev_url_tmp 	= explode('?', $prev_url_val);
	$prev_url 		= $prev_url_tmp[0];

	if(!empty($cc_no))
	{
		if($prev_url != "company_contact_management.php" && $prev_url != "company_regist.php"){
			alert("잘못된 경로로 접근하셨습니다.", "main.php");
		}

		$contact_sql 	= "SELECT * FROM company_contact WHERE cc_no='{$cc_no}'";
		$contact_query 	= mysqli_query($my_db, $contact_sql);
		$contact 		= mysqli_fetch_assoc($contact_query);
		$company		= [];

		$contact_type_option = getContactTypeOption();
		if(!empty($contact))
		{
			$company['f_cc_no'] 	= $cc_no;
			$company['f_my_c_no'] 	= $session_my_c_no;
			$company['f_c_name'] 	= $contact['c_name'];
			$company['f_site'] 		= $contact['homepage'];
			$company['f_address1'] 	= $contact['address'];
			$company['f_tx_company_ceo']	= $contact['ceo'];

			$f_about = "";
			if(!empty($contact['type'])){
				$f_about .= "컨택구분: ".$contact_type_option[$contact['type']];
			}

			if(!empty($contact['category'])){
				$f_about .= !empty($f_about) ? "\r\n" : "";
				$f_about .= "업종: ".$contact['category'];
			}

			if(!empty($contact['product'])){
				$f_about .= !empty($f_about) ? "\r\n" : "";
				$f_about .= "제품: ".$contact['product'];
			}
			$company['f_about'] = $f_about;

			if(!empty($contact['hp']))
			{
				$base_hp 	= $contact['hp'];
				$second_hp 	= "";
				if(strpos($contact['hp'], ',') !== false)
				{
					$contact_hps = explode(',', $contact['hp']);
					$base_hp 	 = trim($contact_hps[0]);
					$second_hp 	 = trim($contact_hps[1]);
				}
				$tels = explode("-", $base_hp);
				$company['f_tel_0'] = $tels[0];
				$company['f_tel_1'] = $tels[1];
				$company['f_tel_2'] = $tels[2];
				$company['f_tx_call1'] = $base_hp;
				$company['f_tx_call2'] = $second_hp;
			}

			if(!empty($contact['email']))
			{
				$emails = explode("@", $contact['email']);
				$company['f_email_0'] 	= $emails[0];
				$company['f_email_1'] 	= $emails[1];
				$company['tx_email_0'] 	= $emails[0];
				$company['tx_email_1'] 	= $emails[1];
			}


			$smarty->assign($company);
		}
	}

	$corp_job_list = $kind_model->getKindParentList("job");
}

# 초기 데이터값
$smarty->assign("mode", $mode);
$smarty->assign("idx", $idx);

$location_list 		= $kind_model->getKindParentList("location");
$total_tel_option 	= array_merge(array("없음" => "없음"), getHpOption(), getTelOption());
$my_company_model 	= MyCompany::Factory();
$staff_model 		= Staff::Factory();
$my_company_option  = $my_company_model->getNameList();
$staff_name_list 	= $staff_model->getStaffNameList("s_name", "1");

# 데이터용 리스트값
$smarty->assign("my_company_list", $my_company_option);
$smarty->assign("staff_list", $staff_name_list);
$smarty->assign("display_list", getDisplayOption());
$smarty->assign("bk_title_list", getBkTitleOption());
$smarty->assign("corp_kind_option", getCorpKindOption());
$smarty->assign("license_type_option", getLicenseTypeOption());
$smarty->assign("corp_job_list", $corp_job_list);
$smarty->assign("location_list", $location_list);
$smarty->assign("total_tel_option", $total_tel_option);
$smarty->assign("brand_parent_list", $brand_parent_list);
$smarty->assign("brand_child_list", $brand_child_list);
$smarty->assign("adv_type_option", getAdvTypeOption());
$smarty->assign("adv_fee_option", getAdvFeeOption());
$smarty->assign("resource_type_list", getResourceTypeOption());
$smarty->assign("lector_vat_type_option", getLectorVatTypeOption());

$smarty->display('company_regist.html');
?>
