<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_return.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("H1:I1");

$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "r_no")
	->setCellValue('B1', "작성일")
	->setCellValue('C1', "회수요청일")
	->setCellValue('D1', "본사/제조사 발송일")
	->setCellValue('E1', "발송 택배사")
	->setCellValue('F1', "발송 송장번호")
	->setCellValue('G1', "사유구분")
    ->setCellValue('H1', "불량사유")
    ->setCellValue('J1', "요청메세지")
    ->setCellValue('K1', "요청자")
    ->setCellValue('L1', "업체명/브랜드명")
    ->setCellValue('M1', "주문번호")
    ->setCellValue('N1', "고객명")
    ->setCellValue('O1', "연락처")
    ->setCellValue('P1', "상품명")
    ->setCellValue('Q1', "수량")
;

$add_where = "1=1";

//검색 처리
$return_with_s_date   = isset($_GET['sch_with_s_date']) ? $_GET['sch_with_s_date'] : "";
$return_with_e_date   = isset($_GET['sch_with_e_date']) ? $_GET['sch_with_e_date'] : "";
$excel_title    	= "업체본사 회수리스트";

$add_where .= " AND (r.return_req_date BETWEEN '{$return_with_s_date}' AND '{$return_with_e_date}') AND return_type IN('2','4')";

$cms_ord_sql    = "SELECT DISTINCT r.order_number, count(r_no) as r_count FROM work_cms_return r WHERE {$add_where} AND r.order_number is not null GROUP BY r.order_number ORDER BY r.r_no DESC LIMIT 10000";
$cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);

$order_number_list  = [];
$order_count_list   = [];
while($order_number = mysqli_fetch_assoc($cms_ord_query)){
	$order_number_list[] =  "'".$order_number['order_number']."'";
    $order_count_list[$order_number['order_number']] = $order_number['r_count'];
}

$order_numbers = implode(',', $order_number_list);

// 리스트 쿼리
$with_sql = "
	SELECT
		r.r_no,
		r.parent_order_number,
        DATE_FORMAT(r.regdate, '%Y-%m-%d') as reg_day,
		r.return_req_date,
		r.return_with_date,
	    r.return_reason,
	    (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT sub.k_parent FROM kind sub WHERE sub.k_name_code=r.bad_reason)) AS bad_reason_g1_name,
	    (SELECT k_name FROM kind k WHERE k.k_name_code=r.bad_reason) AS bad_reason_name,
	    r.req_memo,
	    (SELECT s.s_name FROM staff s WHERE s.s_no=r.req_s_no) as req_s_name,
        r.c_name,
        r.recipient,
        r.recipient_hp,
		(SELECT title from product_cms prd_cms where prd_cms.prd_no=r.prd_no) as prd_name,
		r.quantity
	FROM
		work_cms_return r
	WHERE {$add_where} AND r.order_number IN({$order_numbers})
	ORDER BY r.r_no DESC, r.prd_no ASC
";

$result	= mysqli_query($my_db, $with_sql);
$idx = 2;
if(!!$result)
{
    $lfcr                	= chr(10) ;
	$return_reason_option 	= getReturnReasonOption();
    while($work_cms = mysqli_fetch_array($result))
    {
        $return_reason = $return_reason_option[$work_cms['return_reason']];

		$delivery_sql   	= "SELECT delivery_type, delivery_no FROM work_cms_delivery WHERE order_number = '{$work_cms['parent_order_number']}' GROUP BY delivery_no";
		$delivery_query 	= mysqli_query($my_db, $delivery_sql);
		$delivery_no_txt    = "";
		$delivery_type_txt  = "";
		while($delivery = mysqli_fetch_assoc($delivery_query))
		{
			$delivery_type_txt 	.= !empty($delivery_type_txt) ? $lfcr.$delivery['delivery_type'] : $delivery['delivery_type'];
			$delivery_no_txt 	.= !empty($delivery_no_txt) ? $lfcr.$delivery['delivery_no'] : $delivery['delivery_no'];
		}

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $work_cms['r_no'])
            ->setCellValue("B{$idx}", $work_cms['reg_day'])
            ->setCellValue("C{$idx}", $work_cms['return_req_date'])
            ->setCellValue("D{$idx}", $work_cms['return_with_date'])
            ->setCellValue("E{$idx}", $delivery_type_txt)
            ->setCellValueExplicit("F{$idx}", $delivery_no_txt, PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("G{$idx}", $return_reason)
            ->setCellValue("H{$idx}", $work_cms['bad_reason_g1_name'])
            ->setCellValue("I{$idx}", $work_cms['bad_reason_name'])
            ->setCellValue("J{$idx}", $work_cms['req_memo'])
            ->setCellValue("K{$idx}", $work_cms['req_s_name'])
            ->setCellValue("L{$idx}", $work_cms['c_name'])
            ->setCellValueExplicit("M{$idx}", $work_cms['parent_order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("N{$idx}", $work_cms['recipient'])
            ->setCellValue("O{$idx}", $work_cms['recipient_hp'])
            ->setCellValue("P{$idx}", $work_cms['prd_name'])
            ->setCellValue("Q{$idx}", $work_cms['quantity'])
        ;

        $idx++;
    }
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');

$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00A6A6A6');
$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("A2:Q{$idx}")->getFont()->setSize(8);;
$objPHPExcel->getActiveSheet()->getStyle("A2:Q{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("H2:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getStyle("A1:Q{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(6);


$objPHPExcel->getActiveSheet()->setTitle($excel_title);
$objPHPExcel->getActiveSheet()->getStyle("A1:Q{$idx}")->applyFromArray($styleArray);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$excel_title}.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
