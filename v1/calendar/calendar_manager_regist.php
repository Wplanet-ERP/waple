<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/calendar.php');
require('../inc/model/Asset.php');
require('../inc/model/Kind.php');
require('../inc/model/Team.php');
require('../inc/model/Calendar.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

if(!permissionNameCheck($session_permission, "마스터관리자")){
    $smarty->display('access_error.html');
    exit;
}

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$cal_model      = Calendar::Factory();
$cal_per_model  = Calendar::Factory();
$cal_per_model->setMainInit("calendar_manager_permission", "cmp_no");

if($process == 'new_calendar_manager') # 생성
{
    $cal_id     = isset($_POST['cal_id']) ? $_POST['cal_id'] : "";
    $display    = isset($_POST['display']) ? $_POST['display'] : 2;
    $cal_title  = isset($_POST['cal_title']) ? trim(addslashes($_POST['cal_title'])) : "";

    $cal_id_chk_sql    = "SELECT * FROM calendar_manager WHERE cal_id = '{$cal_id}'";
    $cal_id_chk_query  = mysqli_query($my_db, $cal_id_chk_sql);
    $cal_id_chk_result = mysqli_fetch_assoc($cal_id_chk_query);

    if(isset($cal_id_chk_result['cal_no']))
    {
        exit("<script>alert('존재하는 Calendar Manager 입니다');history.back();</script>");
    }

    # 목록 및 상세 표시
    $view_date                  = isset($_POST['view_date']) ? $_POST['view_date'] : 0;
    $view_s_date                = isset($_POST['view_s_date_list']) ? 1 : (isset($_POST['view_s_date_detail']) ? 2 : 0);
    $view_e_date                = isset($_POST['view_e_date_list']) ? 1 : (isset($_POST['view_e_date_detail']) ? 2 : 0);
    $view_important             = isset($_POST['view_important_list']) ? 1 : (isset($_POST['view_important_detail']) ? 2 : 0);
    $view_brand                 = isset($_POST['view_brand_list']) ? 1 : (isset($_POST['view_brand_detail']) ? 2 : 0);
    $view_brand_type            = isset($_POST['view_brand_type']) ? $_POST['view_brand_type'] : "0";
    $view_brand_company_list    = isset($_POST['view_brand_company_list']) ? implode(',', $_POST['view_brand_company_list']) : "";
    $view_category              = isset($_POST['view_category_list']) ? 1 : (isset($_POST['view_category_detail']) ? 2 : 0);
    $view_category_name         = isset($_POST['view_category_name']) ? trim(addslashes($_POST['view_category_name'])) : "";
    $view_title                 = isset($_POST['view_title_list']) ? 1 : (isset($_POST['view_title_detail']) ? 2 : 0);
    $view_write_s_no            = isset($_POST['view_write_s_no_list']) ? 1 : 2;
    $view_file                  = isset($_POST['view_file_list']) ? 1 : (isset($_POST['view_file_detail']) ? 2 : 0);
    $view_comment               = isset($_POST['view_comment_list']) ? 1 : (isset($_POST['view_comment_detail']) ? 2 : 0);

    # 상세만 표시
    $view_write_date                = isset($_POST['view_write_date_detail']) ? 2 : 0;
    $view_asset_reservation         = isset($_POST['view_asset_reservation_detail']) ? 2 : 0;
    $view_asset_reservation_list    = isset($_POST['view_asset_reservation_list']) ? implode(',', $_POST['view_asset_reservation_list']) : "";
    $view_schedule_permission       = isset($_POST['view_schedule_permission_detail']) ? 2 : 0;
    $view_schedule_permission_set   = isset($_POST['view_schedule_permission_set']) ? $_POST['view_schedule_permission_set'] : 0;
    $view_content                   = isset($_POST['view_content_detail']) ? 2 : 0;
    $view_content_format            = isset($_POST['view_content_format']) ? trim(addslashes($_POST['view_content_format'])) : "";
    $view_date_repeat               = isset($_POST['view_date_repeat_detail']) ? 2 : 0;
    $view_alert                     = isset($_POST['view_alert_detail']) ? 2 : 0;

    # 퍼미션 관련 작업
    $read_permission   = isset($_POST['read_permission']) ? $_POST['read_permission'] : "";
    $write_permission  = isset($_POST['write_permission']) ? $_POST['write_permission'] : "";

    $ins_data = array(
        "cal_id"                        => $cal_id,
        "cal_title"                     => $cal_title,
        "read_permission"               => $read_permission,
        "write_permission"              => $write_permission,
        "view_important"                => $view_important,
        "view_brand"                    => $view_brand,
        "view_brand_type"               => $view_brand_type,
        "view_brand_company_list"       => $view_brand_company_list,
        "view_category"                 => $view_category,
        "view_category_name"            => $view_category_name,
        "view_title"                    => $view_title,
        "view_write_s_no"               => $view_write_s_no,
        "view_write_date"               => $view_write_date,
        "view_asset_reservation"        => $view_asset_reservation,
        "view_asset_reservation_list"   => $view_asset_reservation_list,
        "view_schedule_permission"      => $view_schedule_permission,
        "view_schedule_permission_set"  => $view_schedule_permission_set,
        "view_content"                  => $view_content,
        "view_content_format"           => $view_content_format,
        "view_date_repeat"              => $view_date_repeat,
        "view_alert"                    => $view_alert,
        "view_file"                     => $view_file,
        "view_comment"                  => $view_comment,
        "regdate"                       => date("Y-m-d H:i:s"),
        "display"                       => $display
    );

    $add_date_column    = "";
    if($view_date == '2') {
        $ins_data["view_date"]   = $view_date;
        $ins_data["view_s_date"] = 0;
        $ins_data["view_e_date"] = 0;
    }else{
        $ins_data["view_date"]   = $view_date;
        $ins_data["view_s_date"] = $view_s_date;
        $ins_data["view_e_date"] = $view_e_date;
    }

    $add_permission_list = [];

    if($read_permission == '2')
    {
        $read_permission_team_list  = isset($_POST['read_permission_team']) ? $_POST['read_permission_team'] : "";
        $read_permission_staff_list = isset($_POST['read_permission_staff']) ? $_POST['read_permission_staff'] : "";

        if(!empty($read_permission_team_list))
        {
            foreach($read_permission_team_list as $key => $read_permission_team){
                $add_permission_list[] = array(
                    "cmp_type" => "read",
                    "cmp_team" => $read_permission_team,
                    "cmp_s_no" => isset($read_permission_staff_list[$key]) && !empty($read_permission_staff_list[$key]) ? $read_permission_staff_list[$key] : 0
                );
            }
        }
    }

    if($write_permission == '2')
    {
        $write_permission_team_list  = isset($_POST['write_permission_team']) ? $_POST['write_permission_team'] : "";
        $write_permission_staff_list = isset($_POST['write_permission_staff']) ? $_POST['write_permission_staff'] : "";

        if(!empty($write_permission_team_list))
        {
            foreach($write_permission_team_list as $key => $write_permission_team){
                $add_permission_list[] = array(
                    "cmp_type" => "write",
                    "cmp_team" => $write_permission_team,
                    "cmp_s_no" => isset($write_permission_staff_list[$key]) && !empty($write_permission_staff_list[$key]) ? $write_permission_staff_list[$key] : 0
                );
            }
        }
    }

    $manager_permission_team_list  = isset($_POST['manager_permission_team']) ? $_POST['manager_permission_team'] : "";
    $manager_permission_staff_list = isset($_POST['manager_permission_staff']) ? $_POST['manager_permission_staff'] : "";
    if(!empty($manager_permission_team_list))
    {
        foreach($manager_permission_team_list as $key => $manager_permission_team){
            $add_permission_list[] = array(
                "cmp_type" => "manager",
                "cmp_team" => $manager_permission_team,
                "cmp_s_no" => isset($manager_permission_staff_list[$key]) && !empty($manager_permission_staff_list[$key]) ? $manager_permission_staff_list[$key] : 0
            );
        }
    }

    if($view_schedule_permission_set == '2')
    {
        $view_schedule_permission_team_list  = isset($_POST['view_schedule_permission_team']) ? $_POST['view_schedule_permission_team'] : "";
        $view_schedule_permission_staff_list = isset($_POST['view_schedule_permission_staff']) ? $_POST['view_schedule_permission_staff'] : "";

        if(!empty($view_schedule_permission_team_list))
        {
            foreach($view_schedule_permission_team_list as $key => $view_schedule_permission_team){
                $add_permission_list[] = array(
                    "cmp_type" => "schedule",
                    "cmp_team" => $view_schedule_permission_team,
                    "cmp_s_no" => isset($view_schedule_permission_staff_list[$key]) && !empty($view_schedule_permission_staff_list[$key]) ? $view_schedule_permission_staff_list[$key] : 0
                );
            }
        }
    }

    if($cal_model->insert($ins_data))
    {
        if(!empty($add_permission_list))
        {
            $new_cal_no  = $cal_model->getInsertId();
            $ins_per_sql = "INSERT INTO calendar_manager_permission(`cal_no`, `cmp_type`, `cmp_team`, `cmp_s_no`) VALUES";
            $ins_per_data = [];
            foreach($add_permission_list as $add_permission)
            {
                $ins_per_data[] = array(
                    "cal_no"    => $new_cal_no,
                    "cmp_type"  => $add_permission['cmp_type'],
                    "cmp_team"  => $add_permission['cmp_team'],
                    "cmp_s_no"  => $add_permission['cmp_s_no'],
                );
            }

            if(!empty($ins_per_data))
            {
                $cal_per_model->multiInsert($ins_per_data);
            }
        }

        exit("<script>alert('Calendar Manager 등록에 성공했습니다');location.href='calendar_manager_list.php'</script>");
    }else{
        exit("<script>alert('Calendar Manager 등록에 실패했습니다');history.back();</script>");
    }
}
elseif($process == 'upd_calendar_manager') # 수정
{
    $cal_no     = isset($_POST['cal_no']) ? $_POST['cal_no'] : "";
    $cal_id     = isset($_POST['cal_id']) ? $_POST['cal_id'] : "";
    $display    = isset($_POST['display']) ? $_POST['display'] : 2;
    $cal_title  = isset($_POST['cal_title']) ? trim(addslashes($_POST['cal_title'])) : "";

    # 목록 및 상세 표시
    $view_date                  = isset($_POST['view_date']) ? $_POST['view_date'] : 0;
    $view_s_date                = isset($_POST['view_s_date_list']) ? 1 : (isset($_POST['view_s_date_detail']) ? 2 : 0);
    $view_e_date                = isset($_POST['view_e_date_list']) ? 1 : (isset($_POST['view_e_date_detail']) ? 2 : 0);
    $view_important             = isset($_POST['view_important_list']) ? 1 : (isset($_POST['view_important_detail']) ? 2 : 0);
    $view_brand                 = isset($_POST['view_brand_list']) ? 1 : (isset($_POST['view_brand_detail']) ? 2 : 0);
    $view_brand_type            = isset($_POST['view_brand_type']) ? $_POST['view_brand_type'] : "0";
    $view_brand_company_list    = isset($_POST['view_brand_company_list']) ? implode(',', $_POST['view_brand_company_list']) : "";
    $view_category              = isset($_POST['view_category_list']) ? 1 : (isset($_POST['view_category_detail']) ? 2 : 0);
    $view_category_name         = isset($_POST['view_category_name']) ? trim(addslashes($_POST['view_category_name'])) : "";
    $view_title                 = isset($_POST['view_title_list']) ? 1 : (isset($_POST['view_title_detail']) ? 2 : 0);
    $view_write_s_no            = isset($_POST['view_write_s_no_list']) ? 1 : 2;
    $view_file                  = isset($_POST['view_file_list']) ? 1 : (isset($_POST['view_file_detail']) ? 2 : 0);
    $view_comment               = isset($_POST['view_comment_list']) ? 1 : (isset($_POST['view_comment_detail']) ? 2 : 0);


    # 상세만 표시
    $view_write_date                = isset($_POST['view_write_date_detail']) ? 2 : 0;
    $view_asset_reservation         = isset($_POST['view_asset_reservation_detail']) ? 2 : 0;
    $view_asset_reservation_list    = isset($_POST['view_asset_reservation_list']) ? implode(',', $_POST['view_asset_reservation_list']) : "";
    $view_schedule_permission       = isset($_POST['view_schedule_permission_detail']) ? 2 : 0;
    $view_schedule_permission_set   = isset($_POST['view_schedule_permission_set']) ? $_POST['view_schedule_permission_set'] : 0;
    $view_content                   = isset($_POST['view_content_detail']) ? 2 : 0;
    $view_content_format            = isset($_POST['view_content_format']) ? trim(addslashes($_POST['view_content_format'])) : "";
    $view_date_repeat               = isset($_POST['view_date_repeat_detail']) ? 2 : 0;
    $view_alert                     = isset($_POST['view_alert_detail']) ? 2 : 0;

    # 퍼미션 관련 작업
    $read_permission   = isset($_POST['read_permission']) ? $_POST['read_permission'] : "";
    $write_permission  = isset($_POST['write_permission']) ? $_POST['write_permission'] : "";

    $upd_data = array(
        "cal_no"                        => $cal_no,
        "cal_id"                        => $cal_id,
        "cal_title"                     => $cal_title,
        "read_permission"               => $read_permission,
        "write_permission"              => $write_permission,
        "view_important"                => $view_important,
        "view_brand"                    => $view_brand,
        "view_brand_type"               => $view_brand_type,
        "view_brand_company_list"       => $view_brand_company_list,
        "view_category"                 => $view_category,
        "view_category_name"            => $view_category_name,
        "view_title"                    => $view_title,
        "view_write_s_no"               => $view_write_s_no,
        "view_write_date"               => $view_write_date,
        "view_asset_reservation"        => $view_asset_reservation,
        "view_asset_reservation_list"   => $view_asset_reservation_list,
        "view_schedule_permission"      => $view_schedule_permission,
        "view_schedule_permission_set"  => $view_schedule_permission_set,
        "view_content"                  => $view_content,
        "view_content_format"           => $view_content_format,
        "view_date_repeat"              => $view_date_repeat,
        "view_alert"                    => $view_alert,
        "view_file"                     => $view_file,
        "view_comment"                  => $view_comment,
        "display"                       => $display
    );

    if($view_date == '2') {
        $upd_data["view_date"]   = $view_date;
        $upd_data["view_s_date"] = 0;
        $upd_data["view_e_date"] = 0;
    }else{
        $upd_data["view_date"]   = $view_date;
        $upd_data["view_s_date"] = $view_s_date;
        $upd_data["view_e_date"] = $view_e_date;
    }

    $add_permission_list = [];
    $upd_permission_list = [];
    $del_permission_list = [];
    $upd_apply_list      = [];

    # 읽기 Permission
    $origin_read_cmp_no = isset($_POST['origin_read_cmp_no']) && !empty($_POST['origin_read_cmp_no']) ? explode(",", $_POST['origin_read_cmp_no']) : [];
    if($read_permission == '2')
    {
        $read_cmp_no_list            = isset($_POST['read_cmp_no']) ? $_POST['read_cmp_no'] : "";
        $read_permission_team_list   = isset($_POST['read_permission_team']) ? $_POST['read_permission_team'] : "";
        $read_permission_staff_list  = isset($_POST['read_permission_staff']) ? $_POST['read_permission_staff'] : "";

        if(!empty($read_permission_team_list))
        {
            foreach($read_permission_team_list as $key => $read_permission_team)
            {
                if(isset($read_cmp_no_list[$key]) && !empty($read_cmp_no_list[$key]))
                {
                    $upd_apply_list[] = $read_cmp_no_list[$key];
                    $upd_permission_list[] = array(
                        "cmp_no"   => $read_cmp_no_list[$key],
                        "cmp_team" => $read_permission_team,
                        "cmp_s_no" => isset($read_permission_staff_list[$key]) && !empty($read_permission_staff_list[$key]) ? $read_permission_staff_list[$key] : 0
                    );
                }else{
                    $add_permission_list[] = array(
                        "cmp_type" => "read",
                        "cmp_team" => $read_permission_team,
                        "cmp_s_no" => isset($read_permission_staff_list[$key]) && !empty($read_permission_staff_list[$key]) ? $read_permission_staff_list[$key] : 0
                    );
                }
            }
        }
    }

    if(!empty($origin_read_cmp_no))
    {
        foreach($origin_read_cmp_no as $read_cmp_no)
        {
            if(in_array($read_cmp_no, $upd_apply_list)){
               continue;
            }

            $del_permission_list[] = array('cmp_no' => $read_cmp_no);
        }
    }

    # 쓰기 Permission
    $upd_apply_list     = [];
    $origin_write_cmp_no = isset($_POST['origin_write_cmp_no']) && !empty($_POST['origin_write_cmp_no']) ? explode(",", $_POST['origin_write_cmp_no']) : [];
    if($write_permission == '2')
    {
        $write_cmp_no_list            = isset($_POST['write_cmp_no']) ? $_POST['write_cmp_no'] : "";
        $write_permission_team_list   = isset($_POST['write_permission_team']) ? $_POST['write_permission_team'] : "";
        $write_permission_staff_list  = isset($_POST['write_permission_staff']) ? $_POST['write_permission_staff'] : "";

        if(!empty($write_permission_team_list))
        {
            foreach($write_permission_team_list as $key => $write_permission_team)
            {
                if(isset($write_cmp_no_list[$key]) && !empty($write_cmp_no_list[$key]))
                {
                    $upd_apply_list[] = $write_cmp_no_list[$key];
                    $upd_permission_list[] = array(
                        "cmp_no"   => $write_cmp_no_list[$key],
                        "cmp_team" => $write_permission_team,
                        "cmp_s_no" => isset($write_permission_staff_list[$key]) && !empty($write_permission_staff_list[$key]) ? $write_permission_staff_list[$key] : 0
                    );
                }else{
                    $add_permission_list[] = array(
                        "cmp_type" => "write",
                        "cmp_team" => $write_permission_team,
                        "cmp_s_no" => isset($write_permission_staff_list[$key]) && !empty($write_permission_staff_list[$key]) ? $write_permission_staff_list[$key] : 0
                    );
                }
            }
        }
    }

    if(!empty($origin_write_cmp_no))
    {
        foreach($origin_write_cmp_no as $write_cmp_no)
        {
            if(in_array($write_cmp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('cmp_no' => $write_cmp_no);
        }
    }

    # 관리자 Permission
    $upd_apply_list = [];
    $origin_manager_cmp_no         = isset($_POST['origin_manager_cmp_no']) && !empty($_POST['origin_manager_cmp_no']) ? explode(",", $_POST['origin_manager_cmp_no']) : [];
    $manager_cmp_no_list           = isset($_POST['manager_cmp_no']) ? $_POST['manager_cmp_no'] : "";
    $manager_permission_team_list  = isset($_POST['manager_permission_team']) ? $_POST['manager_permission_team'] : "";
    $manager_permission_staff_list = isset($_POST['manager_permission_staff']) ? $_POST['manager_permission_staff'] : "";
    if(!empty($manager_permission_team_list))
    {
        foreach($manager_permission_team_list as $key => $manager_permission_team)
        {
            if(isset($manager_cmp_no_list[$key]) && !empty($manager_cmp_no_list[$key]))
            {
                $upd_apply_list[] = $manager_cmp_no_list[$key];
                $upd_permission_list[] = array(
                    "cmp_no"   => $manager_cmp_no_list[$key],
                    "cmp_team" => $manager_permission_team,
                    "cmp_s_no" => isset($manager_permission_staff_list[$key]) && !empty($manager_permission_staff_list[$key]) ? $manager_permission_staff_list[$key] : 0
                );
            }else{
                $add_permission_list[] = array(
                    "cmp_type" => "manager",
                    "cmp_team" => $manager_permission_team,
                    "cmp_s_no" => isset($manager_permission_staff_list[$key]) && !empty($manager_permission_staff_list[$key]) ? $manager_permission_staff_list[$key] : 0
                );
            }
        }
    }

    if(!empty($origin_manager_cmp_no))
    {
        foreach($origin_manager_cmp_no as $manager_cmp_no)
        {
            if(in_array($manager_cmp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('cmp_no' => $manager_cmp_no);
        }
    }

    # 일정 Permission
    $upd_apply_list = [];
    $origin_schedule_cmp_no = isset($_POST['origin_view_schedule_cmp_no']) && !empty($_POST['origin_view_schedule_cmp_no']) ? explode(",", $_POST['origin_view_schedule_cmp_no']) : [];
    if($view_schedule_permission_set == '2')
    {
        $view_schedule_cmp_no_list           = isset($_POST['view_schedule_cmp_no']) ? $_POST['view_schedule_cmp_no'] : "";
        $view_schedule_permission_team_list  = isset($_POST['view_schedule_permission_team']) ? $_POST['view_schedule_permission_team'] : "";
        $view_schedule_permission_staff_list = isset($_POST['view_schedule_permission_staff']) ? $_POST['view_schedule_permission_staff'] : "";

        if(!empty($view_schedule_permission_team_list))
        {
            foreach($view_schedule_permission_team_list as $key => $view_schedule_permission_team)
            {
                if(isset($view_schedule_cmp_no_list[$key]) && !empty($view_schedule_cmp_no_list[$key]))
                {
                    $upd_apply_list[] = $view_schedule_cmp_no_list[$key];
                    $upd_permission_list[] = array(
                        "cmp_no"   => $view_schedule_cmp_no_list[$key],
                        "cmp_team" => $view_schedule_permission_team,
                        "cmp_s_no" => isset($view_schedule_permission_staff_list[$key]) && !empty($view_schedule_permission_staff_list[$key]) ? $view_schedule_permission_staff_list[$key] : 0
                    );
                }else{
                    $add_permission_list[] = array(
                        "cmp_type" => "schedule",
                        "cmp_team" => $view_schedule_permission_team,
                        "cmp_s_no" => isset($view_schedule_permission_staff_list[$key]) && !empty($view_schedule_permission_staff_list[$key]) ? $view_schedule_permission_staff_list[$key] : 0
                    );
                }

            }
        }
    }

    if(!empty($origin_schedule_cmp_no))
    {
        foreach($origin_schedule_cmp_no as $schedule_cmp_no)
        {
            if(in_array($schedule_cmp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('cmp_no' => $schedule_cmp_no);
        }
    }

    if($cal_model->update($upd_data))
    {
        if(!empty($add_permission_list))
        {
            $ins_per_sql = "INSERT INTO calendar_manager_permission(`cal_no`, `cmp_type`, `cmp_team`, `cmp_s_no`) VALUES";
            $comma       = "";
            foreach($add_permission_list as $add_permission)
            {
                $ins_per_sql .= $comma."('{$cal_no}', '{$add_permission['cmp_type']}', '{$add_permission['cmp_team']}', '{$add_permission['cmp_s_no']}')";
                $comma = " , ";
            }

            mysqli_query($my_db, $ins_per_sql);
        }

        if(!empty($upd_permission_list))
        {
            foreach($upd_permission_list as $upd_permission)
            {
                $upd_per_sql = "UPDATE calendar_manager_permission SET cmp_team = '{$upd_permission['cmp_team']}', cmp_s_no = '{$upd_permission['cmp_s_no']}' WHERE cmp_no='{$upd_permission['cmp_no']}'";
                mysqli_query($my_db, $upd_per_sql);
            }
        }

        if(!empty($del_permission_list))
        {
            foreach($del_permission_list as $del_permission)
            {
                $del_per_sql = "DELETE FROM calendar_manager_permission WHERE cmp_no='{$del_permission['cmp_no']}'";
                mysqli_query($my_db, $del_per_sql);
            }
        }

        exit("<script>alert('Calendar Manager 수정에 성공했습니다');location.href='calendar_manager_list.php'</script>");
    }else{
        exit("<script>alert('Calendar Manager 수정에 실패했습니다');history.back();</script>");
    }
}
elseif($process == 'del_calendar_manager') # 삭제
{
    $cal_no  = isset($_POST['cal_no']) ? $_POST['cal_no'] : "";

    if($cal_model->delete($cal_no))
    {
        $cal_per_model->deletePermission($cal_no);

        exit("<script>alert('Calendar Manager 삭제에 성공했습니다');location.href='calendar_manager_list.php'</script>");
    }else {
        exit("<script>alert('Calendar Manager 삭제에 실패했습니다');history.back();</script>");
    }
}

$cal_no   = isset($_GET['cal_no']) ? $_GET['cal_no'] : "";
$title	  = "등록하기";
$calendar_manager  = [];

if(!empty($cal_no))
{
    $title = "수정하기";

    $calendar_manager_sql   = "SELECT * FROM calendar_manager WHERE cal_no='{$cal_no}'";
    $calendar_manager_query = mysqli_query($my_db, $calendar_manager_sql);
    $calendar_manager       = mysqli_fetch_assoc($calendar_manager_query);

    if(!empty($calendar_manager['view_asset_reservation_list'])){
        $calendar_manager['view_asset_reservation_list'] = explode(',', $calendar_manager['view_asset_reservation_list']);
    }

    if(!empty($calendar_manager['view_brand_company_list'])){
        $calendar_manager['view_brand_company_list'] = explode(',', $calendar_manager['view_brand_company_list']);
    }

    $read_permission_list = $write_permission_list = $manager_permission_list = $schedule_permission_list = [];
    $origin_read_cmp_no = $origin_write_cmp_no = $origin_manager_cmp_no = $origin_schedule_cmp_no = [];

    $permission_sql = "SELECT * FROM calendar_manager_permission WHERE cal_no='{$cal_no}' ORDER BY cmp_no ASC";
    $permission_query = mysqli_query($my_db, $permission_sql);
    while($permission = mysqli_fetch_assoc($permission_query))
    {
        switch($permission['cmp_type'])
        {
            case 'read':
                $read_permission_list[] = $permission;
                $origin_read_cmp_no[] = $permission['cmp_no'];
                break;

            case 'write':
                $write_permission_list[] = $permission;
                $origin_write_cmp_no[] = $permission['cmp_no'];
                break;

            case 'manager':
                $manager_permission_list[] = $permission;
                $origin_manager_cmp_no[] = $permission['cmp_no'];
                break;

            case 'schedule':
                $schedule_permission_list[] = $permission;
                $origin_schedule_cmp_no[] = $permission['cmp_no'];
                break;
        }
    }

    $calendar_manager['read_permission_list']          = $read_permission_list;
    $calendar_manager['write_permission_list']         = $write_permission_list;
    $calendar_manager['manager_permission_list']       = $manager_permission_list;
    $calendar_manager['view_schedule_permission_list'] = $schedule_permission_list;

    $calendar_manager['origin_read_cmp_no']          = !empty($read_permission_list) ? implode(",", $origin_read_cmp_no) : "";
    $calendar_manager['origin_write_cmp_no']         = !empty($write_permission_list) ? implode(",", $origin_write_cmp_no) : "";
    $calendar_manager['origin_manager_cmp_no']       = !empty($manager_permission_list) ? implode(",", $origin_manager_cmp_no) : "";
    $calendar_manager['origin_view_schedule_cmp_no'] = !empty($schedule_permission_list) ? implode(",", $origin_schedule_cmp_no) : "";

    $smarty->assign($calendar_manager);
}

$asset_model        = Asset::Factory();
$asset_share_list   = $asset_model->getAssetShareList();

$team_model         = Team::Factory();
$team_all_list      = $team_model->getTeamAllList();
$team_name_list     = $team_all_list['team_name_list'];
$staff_team_list    = $team_all_list['staff_list'];

$kind_model         = Kind::Factory();
$brand_total_list   = $kind_model->getBrandCompanyList();
$brand_info_list    = $brand_total_list['brand_info_list'];

# 프리랜서용
$team_name_list['99999']    = "Freelancer";
$staff_team_list['99999']   = array("179" => "위드플레이스", "249" => "위드3");

$smarty->assign("title", $title);
$smarty->assign("display_option", getDisplayOption());
$smarty->assign("permission_option", getCalendarPermissionOption());
$smarty->assign("date_option", getCalendarDateOption());
$smarty->assign("view_schedule_permission_set_option", getViewSchedulePermissionSetOption());
$smarty->assign("brand_type_option", getBrandTypeOption());
$smarty->assign("team_list", $team_name_list);
$smarty->assign("staff_list", $staff_team_list);
$smarty->assign("brand_info_list", $brand_info_list);
$smarty->assign("asset_share_list", $asset_share_list);

$smarty->display('calendar/calendar_manager_regist.html');
?>
