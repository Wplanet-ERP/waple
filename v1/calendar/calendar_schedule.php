<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_date.php');
require('../inc/helper/calendar.php');
require('../inc/helper/approval.php');
require('../inc/model/MyQuick.php');
require('../inc/model/Team.php');
require('../inc/model/Calendar.php');
require('../inc/model/Company.php');
require('../inc/model/ProductCmsUnit.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "add_inout_calendar")
{
    $req_staff  = isset($_POST['req_staff']) ? $_POST['req_staff'] : "";
    $add_cal_id = isset($_POST['cal_id']) ? $_POST['cal_id'] : "";
    $cs_s_day   = isset($_POST['inout_date']) ? $_POST['inout_date'] : "";
    $cs_s_hour  = isset($_POST['inout_date_hour']) ? $_POST['inout_date_hour'] : "";
    $cs_s_min   = isset($_POST['inout_date_min']) ? $_POST['inout_date_min'] : "";
    $cs_s_date  = $cs_s_day." {$cs_s_hour}:{$cs_s_min}:00";
    $cs_e_date  = date("Y-m-d H:i:s", strtotime("{$cs_s_date} +1 hours"));

    $unit_model         = ProductCmsUnit::Factory();
    $cs_content         = "";
    $inout_unit_name    = isset($_POST['inout_unit_name']) ? $_POST['inout_unit_name'] : "";
    $inout_unit_sku     = isset($_POST['inout_unit_sku']) ? $_POST['inout_unit_sku'] : "";
    $inout_unit_qty     = isset($_POST['inout_unit_qty']) ? $_POST['inout_unit_qty'] : "";
    $first_unit_sku     = $inout_unit_sku[0];
    $first_unit_item    = $unit_model->getSkuItem($first_unit_sku, "2809");
    $cs_title           = "{$first_unit_item['brand_name']}::{$first_unit_item['sup_c_name']}";

    foreach($inout_unit_sku as $unit_idx => $unit_sku){
        $cs_content .= !empty($cs_content) ? "\r\n" : "";
        $cs_content .= "{$unit_sku} * {$inout_unit_qty[$unit_idx]}";
    }

    $ins_data   = array(
        "cs_type"       => "normal",
        "cs_category"   => "입고요청",
        "cal_no"        => "8",
        "cal_id"        => "inoutwith",
        "cs_title"      => addslashes($cs_title),
        "cs_content"    => addslashes($cs_content),
        "cs_permission" => 1,
        "cs_s_no"       => $req_staff,
        "cs_s_date"     => $cs_s_date,
        "cs_e_date"     => $cs_e_date,
        "regdate"       => date("Y-m-d H:i:s"),
    );

    $schedule_model = Calendar::Factory();
    $schedule_model->setScheduleTable();

    if($schedule_model->insert($ins_data)){
        exit("<script>alert('캘린더를 저장했습니다');location.href='calendar_schedule.php?cal_id=inoutwith';</script>");
    }else{
        exit("<script>alert('캘린더 저장에 실패했습니다');location.href='calendar_schedule.php?cal_id=inoutwith';</script>");
    }
}

#해당 캘린더 정보
$cal_model      = Calendar::Factory();
$cal_model->setMainInit("calendar_manager", "cal_id");
$company_model  = Company::Factory();
$cal_id         = isset($_GET['cal_id']) ? $_GET['cal_id'] : "";
$calendar       = $cal_model->getItem($cal_id);

if(!isset($calendar['cal_no']))
{
    exit("<script>alert('해당 캘린더 정보가 없습니다');location.href='/v1/main.php';</script>");
}

if($cal_id != 'inoutwith' && $session_staff_state == '2'){
    exit("<script>alert('읽기 권한이 없습니다');location.href='/v1/work_list_cms.php';</script>");
}

$chk_manager_permission = $cal_model->checkPermission("manager", $calendar['cal_no'], $session_s_no);
$chk_read_permission    = true;
$chk_write_permission   = true;

if($chk_manager_permission){
    $chk_read_permission    = true;
    $chk_write_permission   = true;
}else{
    if($calendar['read_permission'] == '2')
    {
        $chk_read_permission = $cal_model->checkPermission("read", $calendar['cal_no'], $session_s_no);
    }

    if($calendar['write_permission'] == '2')
    {
        $chk_write_permission = $cal_model->checkPermission("write", $calendar['cal_no'], $session_s_no);
    }
}

if(!$chk_read_permission)
{
    exit("<script>alert('읽기 권한이 없습니다');location.href='/v1/main.php';</script>");
}

$smarty->assign("chk_write_permission", $chk_write_permission);
$smarty->assign("chk_manager_permission", $chk_manager_permission);
$smarty->assign("calendar", $calendar);

$cur_date   = date('Y-m-d');
$cur_hour   = date('H');
$cur_min    = (int)(date('i')/10)*10;
$sel_date   = isset($_GET['sel_date']) ? $_GET['sel_date'] : $cur_date;

$smarty->assign('cur_date', $sel_date);
$smarty->assign('cur_hour', $cur_hour);
$smarty->assign('cur_min', $cur_min);

# Navigation & My Quick
if($cal_id == 'biz_sup' || $cal_id == 'home_w_r' || $cal_id == 'inoutwith' || $cal_id == 'dp_event')
{
    $nav_prd_no  = "";
    switch($cal_id){
        case "biz_sup":
            $nav_prd_no = "20";
            break;
        case "home_w_r":
            $nav_prd_no = "103";
            break;
        case "inoutwith":
            $nav_prd_no = "207";
            break;
        case "dp_event":
            $nav_prd_no = "232";
            break;
    }

    $quick_model = MyQuick::Factory();
    $is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

    $smarty->assign("nav_prd_no", $nav_prd_no);
    $smarty->assign("is_my_quick", $is_my_quick);
}

# 검색 리스트, 옵션
$team_model             = Team::Factory();
$team_full_name_list    = $team_model->getTeamFullNameList();
$team_total_list	    = $team_model->getTeamAllList();
$staff_all_list		    = $team_total_list['staff_list'];
$sch_brand_list         = [];
$sch_category_option    = [];
$sch_staff_list         = $staff_all_list['all'];

if(!empty($calendar['view_brand_company_list'])){
    $brand_company_tmp_list = explode(",", $calendar['view_brand_company_list']);
    foreach($brand_company_tmp_list as $brand_no){
        $brand_item = $company_model->getItem($brand_no);
        $sch_brand_list[$brand_no] = $brand_item['c_name'];
    }
}

if(!empty($calendar['view_category_name'])){
    $category_tmp_list = explode("$", $calendar['view_category_name']);
    foreach($category_tmp_list as $category){
        $sch_category_option[] = $category;
    }
}

$add_where = "cal_id='{$cal_id}'";
if($cal_id == 'biz_sup' || $cal_id == 'nosp' || $cal_id == 'mc_event')
{
    $sch_team           = isset($_GET['sch_team']) ? $_GET['sch_team'] : $session_team;
    $sch_s_name         = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
    $sch_s_no           = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
    $sch_search_type    = isset($_GET['sch_search_type']) ? $_GET['sch_search_type'] : "";
    $sch_search_text    = isset($_GET['sch_search_text']) ? $_GET['sch_search_text'] : "";

    if(!empty($sch_team))
    {
        if($sch_team != 'all')
        {
            $sch_team_code_where = getTeamWhere($my_db, $sch_team);
            if($sch_team_code_where){
                $add_where .= " AND cs_s_no IN (SELECT s.s_no FROM staff s WHERE s.team IN ({$sch_team_code_where}))";
            }
        }

        $smarty->assign("sch_team", $sch_team);
    }

    if(!empty($sch_s_name)){
        $add_where .= " AND cs_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_s_name}%')";
        $smarty->assign("sch_s_name", $sch_s_name);
    }

    if(!empty($sch_s_no)){
        $add_where .= " AND cs_s_no='{$sch_s_no}'";
        $smarty->assign("sch_s_no", $sch_s_no);
    }

    if(!empty($sch_search_type) && !empty($sch_search_text))
    {
        if($sch_search_type == '1'){
            $add_where .= " AND (cs_title like '%{$sch_search_text}%' OR cs_content like '%{$sch_search_text}%')";
        }elseif($sch_search_type == '2'){
            $add_where .= " AND cs_title like '%{$sch_search_text}%'";
        }elseif($sch_search_type == '3'){
            $add_where .= " AND cs_content like '%{$sch_search_text}%'";
        }

        $smarty->assign("sch_search_type", $sch_search_type);
        $smarty->assign("sch_search_text", $sch_search_text);
    }
}

$add_hr_where       = "1=1";
$sch_cs_brand       = isset($_GET['sch_cs_brand']) ? $_GET['sch_cs_brand'] : "";
$sch_cs_category    = isset($_GET['sch_cs_category']) ? $_GET['sch_cs_category'] : "";
$sch_writer_team    = isset($_GET['sch_writer_team']) ? $_GET['sch_writer_team'] : "";
$sch_writer_s_no    = isset($_GET['sch_writer_s_no']) ? $_GET['sch_writer_s_no'] : "";
$sch_writer_s_name  = isset($_GET['sch_writer_s_name']) ? $_GET['sch_writer_s_name'] : "";
$sch_writer_my      = isset($_GET['sch_writer_my']) ? $_GET['sch_writer_my'] : "";

if($cal_id == "hr_wide")
{
    if(!isset($_GET['sch_writer_team'])){
        $sch_writer_team = $session_team;
    }
    $sch_category_option = getLeaveTypeList();
}

if(!empty($sch_cs_brand)){
    $add_where .= " AND cs_brand='{$sch_cs_brand}'";
    $smarty->assign("sch_cs_brand", $sch_cs_brand);
}

if(!empty($sch_cs_category)){
    $add_where      .= " AND cs_category='{$sch_cs_category}'";
    $add_hr_where   .= " AND leave_type='{$sch_cs_category}'";
    $smarty->assign("sch_cs_category", $sch_cs_category);
}

if(!empty($sch_writer_team))
{
    if ($sch_writer_team != "all") {
        $sch_staff_list      = $staff_all_list[$sch_writer_team];
        $sch_team_code_where = getTeamWhere($my_db, $sch_writer_team);
        $add_where          .= " AND cs_s_no IN (SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where}))";
        $add_hr_where       .= " AND lc_s_no IN (SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where}))";
    }

    $smarty->assign("sch_writer_team", $sch_writer_team);
}

if(!empty($sch_writer_s_no)){
    $add_where      .= " AND cs_s_no = '{$sch_writer_s_no}'";
    $add_hr_where   .= " AND lc_s_no = '{$sch_writer_s_no}'";
    $smarty->assign("sch_writer_s_no", $sch_writer_s_no);
}

if(!empty($sch_writer_my)){
    $sch_writer_s_name = $session_name;
}

if(!empty($sch_writer_s_name)){
    $add_where      .= " AND cs_s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_writer_s_name}%')";
    $add_hr_where   .= " AND lc_s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_writer_s_name}%')";
    $smarty->assign("sch_writer_s_name", $sch_writer_s_name);
}

$smarty->assign("sch_writer_my", $sch_writer_my);
$smarty->assign("sch_brand_list", $sch_brand_list);
$smarty->assign("sch_category_option", $sch_category_option);

# 캘린더 일정
$calendar_schedule_list = [];

if($cal_id == "hr_wide")
{
    $calendar_schedule_sql = "
        SELECT 
            lc_title,
            lc_s_no,
            (SELECT sub.s_name FROM staff sub WHERE sub.s_no=lc.lc_s_no LIMIT 1) as lc_s_name,
            lc_state,
            leave_type,
            leave_s_date,
            leave_e_date,
            leave_value
        FROM leave_calendar lc 
        WHERE {$add_hr_where}
        ORDER BY leave_s_date ASC
    ";
    $calendar_schedule_query = mysqli_query($my_db, $calendar_schedule_sql);
    while($calendar_schedule = mysqli_fetch_assoc($calendar_schedule_query)){
        $calendar_schedule['color']     = ($calendar_schedule['lc_state'] == "1") ? "#8392A5" : "#BE2020";
        $calendar_schedule['is_all']    = ($calendar_schedule['leave_value'] == "1") ? true : false;

        $calendar_schedule_list[] = $calendar_schedule;
    }
}
else
{
    $calendar_schedule_sql = "
        SELECT 
            cs.cs_no,
            cs_title,
            cs_s_no,
            cs_all,
            cs_s_date,
            cs_e_date,
            cs_s_no,
            cs_brand,
            cs_category,
            cs_important,
            cs_permission,
            (SELECT c.c_name FROM company c WHERE c.c_no=cs.cs_brand) as cs_brand_name,
            (SELECT sub.s_name FROM staff sub WHERE sub.s_no=cs.cs_s_no LIMIT 1) as cs_s_name,
            (SELECT count(sub.csc_no) FROM calendar_schedule_comment sub WHERE sub.cs_no=cs.cs_no AND sub.display='1') as csc_cnt
        FROM calendar_schedule cs 
        WHERE {$add_where}
        ORDER BY cs_s_date ASC
    ";
    $colors             = getColors();
    $colors_idx         = 0;
    $cate_color_option  = array("입고요청" => "#3788d8", "입고일정확정" => "#009900", "입고확인서 완료(종료)" => "#FF0000", "입고완료" => "#000000");
    $calendar_schedule_query = mysqli_query($my_db, $calendar_schedule_sql);
    while($calendar_schedule = mysqli_fetch_assoc($calendar_schedule_query))
    {
        $title = "";
        if($cal_id == "dp_event"){
            $calendar_schedule['color'] = $colors[$colors_idx++];
        }else{
            $calendar_schedule['color'] = $colors[$calendar_schedule['cs_s_no']];
        }

        if($cal_id == "inoutwith"){
            $calendar_schedule['color'] = $cate_color_option[$calendar_schedule['cs_category']];
        }

        $cs_s_date = date('Y-m-d', strtotime($calendar_schedule['cs_s_date']));
        $cs_e_date = date('Y-m-d', strtotime($calendar_schedule['cs_e_date']));

        if($cs_s_date == $cs_e_date)
        {
            if($calendar_schedule['cs_all'] == '1'){

            }else{
                if($calendar['view_s_date'] == '1') {
                    $title_s_date = date('H:i', strtotime($calendar_schedule['cs_s_date']));
                    $title .= (!empty($title)) ? "~{$title_s_date}" : "{$title_s_date}";
                }

                if($calendar['view_e_date'] == '1') {
                    $title_e_date = date('H:i', strtotime($calendar_schedule['cs_e_date']));
                    $title .= (!empty($title)) ? "~{$title_e_date}" : "{$title_e_date}";
                }
            }
        }else{
            if($calendar['view_s_date'] == '1' && $calendar_schedule['cs_all'] != '1') {
                $title_s_date = date('H:i', strtotime($calendar_schedule['cs_s_date']));
                if($title_s_date != "00:00") {
                    $title .= (!empty($title)) ? "~{$title_s_date}" : "{$title_s_date}";
                }
            }
        }

        if($calendar['view_brand'] == '1' && !empty($calendar_schedule['cs_brand'])) {
            $title .= (!empty($title)) ? " [{$calendar_schedule['cs_brand_name']}]" : "[{$calendar_schedule['cs_brand_name']}]";
        }

        if($calendar['view_category'] == '1' && !empty($calendar_schedule['cs_category'])) {
            $title .= (!empty($title)) ? " [{$calendar_schedule['cs_category']}]" : "[{$calendar_schedule['cs_category']}]";
        }

        if($calendar['view_important'] == '1' && $calendar_schedule['cs_important'] == '1') {
            $title .= (!empty($title)) ? " ★" : "★";
        }

        if($calendar['view_title'] == '1' && !empty($calendar_schedule['cs_title'])) {
            $title .= (!empty($title)) ? " {$calendar_schedule['cs_title']}" : "{$calendar_schedule['cs_title']}";
        }

        if($calendar['view_write_s_no'] == '1' ) {
            $title .= (!empty($title)) ? " ({$calendar_schedule['cs_s_name']})" : "({$calendar_schedule['cs_s_name']})";
        }

        if($calendar['view_comment'] == '1' ) {
            $title .= (!empty($title)) ? " ({$calendar_schedule['csc_cnt']})" : "({$calendar_schedule['csc_cnt']})";
        }

        $calendar_schedule['title'] = $title;

        if($calendar_schedule['cs_permission'] == '0')
        {
            if($calendar_schedule['cs_s_no'] == $session_s_no){
                $calendar_schedule_list[] = $calendar_schedule;
            }
        }
        elseif($calendar_schedule['cs_permission'] == '2')
        {
            if($calendar_schedule['cs_s_no'] == $session_s_no || $cal_model->checkSchedulePermission($calendar_schedule['cs_no'], $session_s_no))
            {
                $calendar_schedule_list[] = $calendar_schedule;
            }
        }
        else
        {
            $calendar_schedule_list[] = $calendar_schedule;
        }
    }
}

# 발주 담당자
$commerce_staff_sql     = "SELECT DISTINCT req_s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=req_s_no) as reg_name FROM commerce_order_set WHERE state='2' AND display='1' ORDER BY reg_name";
$commerce_staff_query   = mysqli_query($my_db, $commerce_staff_sql);
$commerce_staff_list    = [];
while($commerce_staff = mysqli_fetch_assoc($commerce_staff_query)){
    $commerce_staff_list[$commerce_staff['req_s_no']] = $commerce_staff['reg_name'];
}

$smarty->assign('team_full_name_list', $team_full_name_list);
$smarty->assign('sch_staff_list', $sch_staff_list);
$smarty->assign('search_type_option', getSearchTypeOption());
$smarty->assign('hour_option', getHourOption());
$smarty->assign('min_option', getMinOption());
$smarty->assign('commerce_staff_list', $commerce_staff_list);
$smarty->assign('calendar_schedule_list', json_encode($calendar_schedule_list));

if($cal_id == "dp_event"){
    $smarty->display('calendar/calendar_schedule_google.html');
}elseif($cal_id == "hr_wide"){
    $smarty->display('calendar/leave_calendar.html');
}
else{
    $smarty->display('calendar/calendar_schedule.html');
}
?>
