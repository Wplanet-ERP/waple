<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_upload.php');
require('../inc/helper/_date.php');
require('../inc/helper/calendar.php');
require('../inc/model/Asset.php');
require('../inc/model/Team.php');
require('../inc/model/Calendar.php');
require('../inc/model/Company.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

$cal_id         = isset($_REQUEST['cal_id']) ? $_REQUEST['cal_id'] : "";
$cs_no          = isset($_GET['cs_no']) ? $_GET['cs_no'] : "";
$cs_sel_day     = isset($_GET['cs_sel_day']) ? $_GET['cs_sel_day'] : "";
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$btn_permission = false;

# Calendar Manager
$cal_model          = Calendar::Factory();
$cal_model->setMainInit("calendar_manager", "cal_id");
$cal_schedule_model = Calendar::Factory();
$cal_schedule_model->setScheduleTable();

$calendar_item      = $cal_model->getItem($cal_id);

if(!isset($calendar_item['cal_no']))
{
    exit("<script>alert('해당 캘린더 정보가 없습니다');location.href='/test_dev/main.php';</script>");
}

$list_url  = "calendar_schedule_list.php";
$btn_title = "작성완료";
$calendar_schedule = [];

if($process == 'add_calendar_schedule')
{
    $cal_id         = isset($_POST['cal_id']) ? $_POST['cal_id'] : "";
    $cal_no         = isset($_POST['cal_no']) ? $_POST['cal_no'] : "";
    $cs_sel_day     = isset($_POST['cs_sel_day']) ? $_POST['cs_sel_day'] : "";
    $cs_brand       = isset($_POST['cs_brand']) ? $_POST['cs_brand'] : 0;
    $cs_category    = isset($_POST['cs_category']) ? $_POST['cs_category'] : 1;
    $cs_important   = isset($_POST['cs_important']) ? $_POST['cs_important'] : 0;
    $cs_title       = isset($_POST['cs_title']) ? trim(addslashes($_POST['cs_title'])) : "";
    $cs_s_no        = isset($_POST['cs_s_no']) ? $_POST['cs_s_no'] : "";

    # 날짜컬럼 체크
    $add_date_column = "";
    $cs_s_day       = isset($_POST['cs_s_day']) ? $_POST['cs_s_day'] : "";
    $cs_e_day       = isset($_POST['cs_e_day']) ? $_POST['cs_e_day'] : "";
    $cs_all         = isset($_POST['cs_all']) ? 1 : 0;

    if($cs_all == '1'){
        $cs_s_date        = $cs_s_day." 00:00:00";
        $cs_e_date        = $cs_e_day." 23:50:00";
        $add_date_column .= "cs_all='{$cs_all}', cs_s_date='{$cs_s_date}', cs_e_date='{$cs_e_date}',";
    }else{
        $cs_s_hour      = isset($_POST['cs_s_hour']) ? $_POST['cs_s_hour'] : "";
        $cs_s_min       = isset($_POST['cs_s_min']) ? $_POST['cs_s_min'] : "";
        $cs_s_date      = $cs_s_day." ".$cs_s_hour.":".$cs_s_min.":00";

        $cs_e_hour      = isset($_POST['cs_e_hour']) ? $_POST['cs_e_hour'] : "";
        $cs_e_min       = isset($_POST['cs_e_min']) ? $_POST['cs_e_min'] : "";
        $cs_e_date      = $cs_e_day." ".$cs_e_hour.":".$cs_e_min.":00";

        $add_date_column .= "cs_all='{$cs_all}', cs_s_date='{$cs_s_date}', cs_e_date='{$cs_e_date}',";
    }

    $asset_reservation_list = isset($_POST['asset_reservation_list']) ? implode(',', $_POST['asset_reservation_list']) : "";
    $new_asset_reservation  = isset($_POST['new_asset_reservation']) ? $_POST['new_asset_reservation'] : "";
    $cs_permission          = isset($_POST['cs_permission']) ? $_POST['cs_permission'] : 0;
    $cs_content             = isset($_POST['cs_content']) ? trim(addslashes($_POST['cs_content'])) : "";

    if(!empty($new_asset_reservation)){
        $asset_reservation_list = !empty($asset_reservation_list) ? $new_asset_reservation.",".$asset_reservation_list : $new_asset_reservation;
    }

    # 일정반복 체크
    $date_repeat  = isset($_POST['cs_repeat']) ? $_POST['cs_repeat'] : 0;
    $add_repeat_column = "";
    if($date_repeat == '1')
    {
        $date_repeat_type  = isset($_POST['date_repeat_type']) ? $_POST['date_repeat_type'] : 0;
        $date_repeat_val   = isset($_POST['date_repeat_val']) ? $_POST['date_repeat_val'] : "";
        $date_repeat_start = isset($_POST['date_repeat_start']) ? $_POST['date_repeat_start'] : "";
        $date_repeat_end   = isset($_POST['date_repeat_end']) ? $_POST['date_repeat_end'] : "";

        $date_repeat_week = "";
        for($i=0; $i<7; $i++){
            $date_repeat_week_val = isset($_POST["date_repeat_week_{$i}"]) ? "1" : "0";
            $date_repeat_week .= $date_repeat_week_val;
        }

        $add_repeat_column = "date_repeat='{$date_repeat}', date_repeat_type='{$date_repeat_type}', date_repeat_val='{$date_repeat_val}', date_repeat_week='{$date_repeat_week}', date_repeat_start='{$date_repeat_start}', date_repeat_end='{$date_repeat_end}',";
    }else{
        $add_repeat_column = "date_repeat='{$date_repeat}', date_repeat_type=NULL, date_repeat_val=NULL, date_repeat_week=NULL, date_repeat_start=NULL, date_repeat_end=NULL,";
    }

    # 알림 체크
    $cs_alert         = isset($_POST['cs_alert']) ? $_POST['cs_alert'] : 0;
    $cs_alert_repeat  = isset($_POST['cs_alert_repeat']) ? $_POST['cs_alert_repeat'] : 0;
    $add_alert_column = "";
    if($cs_alert == '1'){
        $cs_alert_day       = isset($_POST['cs_alert_day']) ? $_POST['cs_alert_day'] : "";
        $cs_alert_hour      = isset($_POST['cs_alert_hour']) ? $_POST['cs_alert_hour'] : "";
        $cs_alert_min       = isset($_POST['cs_alert_min']) ? $_POST['cs_alert_min'] : "";
        $cs_alert_hp        = isset($_POST['cs_alert_hp']) ? trim(addslashes($_POST['cs_alert_hp'])) : "";

        if($cs_alert_repeat == '1' && $date_repeat == '1') {
            $cs_alert_day = $_POST['cs_s_day'];
        }else{
            $cs_alert_repeat = '0';
        }
        $cs_alert_date      = $cs_alert_day." ".$cs_alert_hour.":".$cs_alert_min.":00";

        $add_alert_column = "cs_alert='{$cs_alert}', cs_alert_repeat='{$cs_alert_repeat}', cs_alert_date='{$cs_alert_date}', cs_alert_hp='{$cs_alert_hp}',";
    }else{
        $add_alert_column = "cs_alert='{$cs_alert}', cs_alert_repeat='{$cs_alert_repeat}', cs_alert_date=NULL, cs_alert_hp=NULL,";
    }

    # 파일첨부
    $file_names = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_paths = isset($_POST['file_path']) ? $_POST['file_path'] : "";

    $move_file_name = "";
    $move_file_path = "";
    if(!empty($file_names) && !empty($file_paths))
    {
        $move_file_paths = move_store_files($file_paths, "dropzone_tmp", "calendar");
        $move_file_name  = implode(',', $file_names);
        $move_file_path  = implode(',', $move_file_paths);
    }

    $add_file_column = "";
    if(!empty($move_file_path) && !empty($move_file_name)){
        $add_file_column = "file_path='{$move_file_path}', file_name='{$move_file_name}',";
    }

    $ins_sql = "
        INSERT INTO calendar_schedule SET
            cs_brand        = '{$cs_brand}',
            cs_category     = '{$cs_category}',
            cal_no          = '{$cal_no}',
            cal_id          = '{$cal_id}',
            cs_title        = '{$cs_title}',
            cs_important    = '{$cs_important}',
            cs_s_no         = '{$cs_s_no}',
            {$add_date_column}
            asset_reservation_list  = '{$asset_reservation_list}',
            cs_permission           = '{$cs_permission}',
            cs_content              = '{$cs_content}',
            {$add_repeat_column}
            {$add_alert_column}
            {$add_file_column}
            regdate=now()
    ";

    # 퍼미션 관련 작업
    $add_permission_list = [];

    if($cs_permission == '2')
    {
        $calendar_permission_team_list  = isset($_POST['calendar_permission_team']) ? $_POST['calendar_permission_team'] : "";
        $calendar_permission_staff_list = isset($_POST['calendar_permission_staff']) ? $_POST['calendar_permission_staff'] : "";

        $new_permission_team            = isset($_POST['new_permission_team']) ? $_POST['new_permission_team'] : "";
        $new_permission_staff           = isset($_POST['new_permission_staff']) ? $_POST['new_permission_staff'] : "";

        if(!empty($new_permission_team) || !empty($new_permission_staff))
        {
            $add_permission_list[] = array(
                "csp_team" => $new_permission_team,
                "csp_s_no" => !empty($new_permission_staff) ? $new_permission_staff : 0
            );
        }

        if(!empty($calendar_permission_team_list))
        {
            foreach($calendar_permission_team_list as $key => $calendar_permission_team)
            {
                $add_permission_list[] = array(
                    "csp_team" => $calendar_permission_team,
                    "csp_s_no" => isset($calendar_permission_staff_list[$key]) && !empty($calendar_permission_staff_list[$key]) ? $calendar_permission_staff_list[$key] : 0
                );
            }
        }
    }


    if(mysqli_query($my_db, $ins_sql))
    {
        $new_cs_no   = mysqli_insert_id($my_db);

        # Permission 추가
        if(!empty($add_permission_list))
        {
            $ins_per_sql = "INSERT INTO calendar_schedule_permission(`cs_no`, `csp_team`, `csp_s_no`) VALUES";
            $comma       = "";
            foreach($add_permission_list as $add_permission)
            {
                $ins_per_sql .= $comma."('{$new_cs_no}', '{$add_permission['csp_team']}', '{$add_permission['csp_s_no']}')";
                $comma = " , ";
            }

            mysqli_query($my_db, $ins_per_sql);
        }

        if(!empty($asset_reservation_list))
        {
            $cal_schedule_model->reservationAsset($new_cs_no);
        }

        # Permission 추가
        if($cs_alert == '1') {
            $cal_schedule_model->sendScheduleHp($new_cs_no);
        }
        
        if($date_repeat == '1')
        {
            $cal_schedule_model->repeatSchedule($new_cs_no);
        }

        exit("<script>alert('Calendar 일정 등록에 성공했습니다');parent.location.href='calendar_schedule.php?cal_id={$cal_id}&sel_date={$cs_sel_day}';</script>");
    }else{
        exit("<script>alert('Calendar 일정 등록에 실패했습니다');history.back();</script>");
    }

}
elseif($process == 'upd_calendar_schedule') # 수정
{
    $cs_no          = isset($_POST['cs_no']) ? $_POST['cs_no'] : "";
    $cal_id         = isset($_POST['cal_id']) ? $_POST['cal_id'] : "";
    $cs_sel_day     = isset($_POST['cs_sel_day']) ? $_POST['cs_sel_day'] : "";
    $cs_brand       = isset($_POST['cs_brand']) ? $_POST['cs_brand'] : 0;
    $cs_category    = isset($_POST['cs_category']) ? $_POST['cs_category'] : 1;
    $cs_important   = isset($_POST['cs_important']) ? $_POST['cs_important'] : 0;
    $cs_title       = isset($_POST['cs_title']) ? trim(addslashes($_POST['cs_title'])) : "";

    $add_date_column = "";
    $cs_s_day       = isset($_POST['cs_s_day']) ? $_POST['cs_s_day'] : "";
    $cs_e_day       = isset($_POST['cs_e_day']) ? $_POST['cs_e_day'] : "";
    $cs_all         = isset($_POST['cs_all']) ? 1 : 0;

    if($cs_all == '1'){
        $cs_s_date        = $cs_s_day." 00:00:00";
        $cs_e_date        = $cs_e_day." 23:50:00";
        $add_date_column .= "cs_all='{$cs_all}', cs_s_date='{$cs_s_date}', cs_e_date='{$cs_e_date}',";
    }else{
        $cs_s_hour      = isset($_POST['cs_s_hour']) ? $_POST['cs_s_hour'] : "";
        $cs_s_min       = isset($_POST['cs_s_min']) ? $_POST['cs_s_min'] : "";
        $cs_s_date      = $cs_s_day." ".$cs_s_hour.":".$cs_s_min.":00";

        $cs_e_hour      = isset($_POST['cs_e_hour']) ? $_POST['cs_e_hour'] : "";
        $cs_e_min       = isset($_POST['cs_e_min']) ? $_POST['cs_e_min'] : "";
        $cs_e_date      = $cs_e_day." ".$cs_e_hour.":".$cs_e_min.":00";

        $add_date_column .= "cs_all='{$cs_all}', cs_s_date='{$cs_s_date}', cs_e_date='{$cs_e_date}',";
    }

    $cs_permission          = isset($_POST['cs_permission']) ? $_POST['cs_permission'] : 0;
    $cs_content             = isset($_POST['cs_content']) ? trim(addslashes($_POST['cs_content'])) : "";

    # 파일첨부
    $file_origin_path   = isset($_POST['file_origin_path']) ? $_POST['file_origin_path'] : "";
    $file_origin_name   = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_names         = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_paths         = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_chk           = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    $move_file_path  = "";
    $move_file_name  = "";
    $add_file_column = "";
    if($file_chk)
    {
        if(!empty($file_paths) && !empty($file_names))
        {
            $move_file_paths = move_store_files($file_paths, "dropzone_tmp", "calendar");
            $move_file_name  = implode(',', $file_names);
            $move_file_path  = implode(',', $move_file_paths);
        }

        if(!empty($file_origin_path) && !empty($file_origin_name))
        {
            if(!empty($file_paths) && !empty($file_names)){
                $del_files = array_diff(explode(',', $file_origin_path), $file_paths);
            }else{
                $del_files = explode(',', $file_origin_path);
            }

            if(!empty($del_files))
            {
                del_files($del_files);
            }
        }

        if(!empty($move_file_path) && !empty($move_file_name)){
            $add_file_column = "file_path='{$move_file_path}', file_name='{$move_file_name}',";
        }else{
            $add_file_column = "file_path='', file_name='',";
        }
    }

    $upd_sql = "
        UPDATE calendar_schedule SET 
            cs_brand        = '{$cs_brand}', 
            cs_category     = '{$cs_category}', 
            cs_title        = '{$cs_title}',
            cs_important    = '{$cs_important}',
            {$add_date_column}
            {$add_file_column}
            cs_permission   = '{$cs_permission}',
            cs_content      = '{$cs_content}'
        WHERE cs_no='{$cs_no}'
    ";

    $add_permission_list = [];
    $upd_permission_list = [];
    $del_permission_list = [];
    $upd_apply_list      = [];

    $origin_csp_no = isset($_POST['origin_csp_no']) && !empty($_POST['origin_csp_no']) ? explode(",", $_POST['origin_csp_no']) : [];
    if($cs_permission == '2')
    {
        $calendar_csp_no_list            = isset($_POST['calendar_csp_no']) ? $_POST['calendar_csp_no'] : "";
        $calendar_permission_team_list   = isset($_POST['calendar_permission_team']) ? $_POST['calendar_permission_team'] : "";
        $calendar_permission_staff_list  = isset($_POST['calendar_permission_staff']) ? $_POST['calendar_permission_staff'] : "";

        $new_permission_team            = isset($_POST['new_permission_team']) ? $_POST['new_permission_team'] : "";
        $new_permission_staff           = isset($_POST['new_permission_staff']) ? $_POST['new_permission_staff'] : "";

        if(!empty($new_permission_team) || !empty($new_permission_staff))
        {
            $add_permission_list[] = array(
                "csp_team" => $new_permission_team,
                "csp_s_no" => !empty($new_permission_staff) ? $new_permission_staff : 0
            );
        }

        if(!empty($calendar_permission_team_list))
        {
            foreach($calendar_permission_team_list as $key => $cs_permission_team)
            {
                if(isset($calendar_csp_no_list[$key]) && !empty($calendar_csp_no_list[$key]))
                {
                    $upd_apply_list[] = $calendar_csp_no_list[$key];
                    $upd_permission_list[] = array(
                        "csp_no"    => $calendar_csp_no_list[$key],
                        "csp_team" => $cs_permission_team,
                        "csp_s_no" => isset($calendar_permission_staff_list[$key]) && !empty($calendar_permission_staff_list[$key]) ? $calendar_permission_staff_list[$key] : 0
                    );
                }else{
                    $add_permission_list[] = array(
                        "csp_team" => $cs_permission_team,
                        "csp_s_no" => isset($calendar_permission_staff_list[$key]) && !empty($calendar_permission_staff_list[$key]) ? $calendar_permission_staff_list[$key] : 0
                    );
                }
            }
        }
    }

    if(!empty($origin_csp_no))
    {
        foreach($origin_csp_no as $csp_no)
        {
            if(in_array($csp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('csp_no' => $csp_no);
        }
    }

    if(mysqli_query($my_db, $upd_sql))
    {
        if(!empty($add_permission_list))
        {
            $ins_per_sql = "INSERT INTO calendar_schedule_permission(`cs_no`,`csp_team`, `csp_s_no`) VALUES";
            $comma       = "";
            foreach($add_permission_list as $add_permission)
            {
                $ins_per_sql .= $comma."('{$cs_no}',  '{$add_permission['csp_team']}', '{$add_permission['csp_s_no']}')";
                $comma = " , ";
            }

            mysqli_query($my_db, $ins_per_sql);
        }

        if(!empty($upd_permission_list))
        {
            foreach($upd_permission_list as $upd_permission)
            {
                $upd_per_sql = "UPDATE calendar_schedule_permission SET csp_team = '{$upd_permission['csp_team']}', csp_s_no = '{$upd_permission['csp_s_no']}' WHERE csp_no='{$upd_permission['csp_no']}'";
                mysqli_query($my_db, $upd_per_sql);
            }
        }

        if(!empty($del_permission_list))
        {
            foreach($del_permission_list as $del_permission)
            {
                $del_per_sql = "DELETE FROM calendar_schedule_permission WHERE csp_no='{$del_permission['csp_no']}'";
                mysqli_query($my_db, $del_per_sql);
            }
        }

        exit("<script>alert('Calendar 일정 수정에 성공했습니다');parent.location.href='calendar_schedule.php?cal_id={$cal_id}&sel_date={$cs_sel_day}';</script>");
    }else{
        exit("<script>alert('Calendar 일정 수정에 실패했습니다');history.back();</script>");
    }


}
elseif($process == 'del_calendar_schedule') # 삭제
{
    $cs_no      = isset($_POST['cs_no']) ? $_POST['cs_no'] : "";
    $cs_sel_day = isset($_POST['cs_sel_day']) ? $_POST['cs_sel_day'] : "";
    $cal_id     = isset($_POST['cal_id']) ? $_POST['cal_id'] : "";
    $del_sql    = "DELETE FROM calendar_schedule WHERE cs_no='{$cs_no}'";

    if(!$cs_no || !mysqli_query($my_db, $del_sql))
    {
        exit("<script>alert('Calendar 일정 삭제에 실패했습니다');parent.location.href='calendar_schedule.php?cal_id={$cal_id}&sel_date={$cs_sel_day}';</script>");
    }else{
        $del_report_sql = "DELETE FROM calendar_schedule_permission WHERE cs_no='{$cs_no}'";
        mysqli_query($my_db, $del_report_sql);

        $del_comment_sql = "DELETE FROM calendar_schedule_comment WHERE cs_no='{$cs_no}'";
        mysqli_query($my_db, $del_comment_sql);

        $c_info = "CAL".$cs_no;
        $del_msg_sql = "DELETE FROM EASY_SEND_LOG WHERE CINFO_DETAIL='{$c_info}' AND STATUS IN(0,1)";
        mysqli_query($my_db, $del_msg_sql);

        exit("<script>alert('Calendar 일정 삭제에 성공했습니다');parent.location.href='calendar_schedule.php?cal_id={$cal_id}&sel_date={$cs_sel_day}';</script>");
    }
}
elseif($process == 'comment_add')
{
    $cal_no 	 = (isset($_POST['cal_no'])) ? $_POST['cal_no'] : "";
    $cal_id 	 = (isset($_POST['cal_id'])) ? $_POST['cal_id'] : "";
    $cs_no 		 = (isset($_POST['cs_no'])) ? $_POST['cs_no'] : "";
    $cs_sel_day  = (isset($_POST['cs_sel_day'])) ? $_POST['cs_sel_day'] : "";
    $comment_new = (isset($_POST['f_comment_new'])) ? $_POST['f_comment_new'] : "";
    $is_secret 	 = (isset($_POST['is_secret'])) ? 1 : 0;
    $is_unknown  = (isset($_POST['is_unknown'])) ? 1 : 0;
    $regdate     = date("Y-m-d H:i:s");

    $img_add_set = "";
    $f_comm_img     = $_FILES['f_comm_img'];
    if(isset($f_comm_img['name']) && !empty($f_comm_img['name'])){
        $img_path    = add_store_file($f_comm_img, "calendar_comment");
        $img_name    = $f_comm_img['name'];
        $img_add_set = ", `img_path`='{$img_path}', `img_name`='{$img_name}'";
    }

    $ins_sql = "INSERT INTO `calendar_schedule_comment` SET
					`cal_no` = '{$cal_no}',
					`cs_no` = '{$cs_no}',
					`comment` = '".addslashes($comment_new)."',
					team = (SELECT s.team	FROM staff s WHERE s.s_no = '{$session_s_no}'),
					s_no = '{$session_s_no}',
					regdate = '{$regdate}',
					is_secret = {$is_secret},
					is_unknown = {$is_unknown}
					{$img_add_set}
		";


    if(!mysqli_query($my_db, $ins_sql)) {
        echo("<script>alert('코멘트 추가에 실패 하였습니다');history.back();</script>");
    }
    exit ("<script>location.href='calendar_schedule_regist.php?cal_id={$cal_id}&cs_sel_day={$cs_sel_day}&cs_no={$cs_no}';</script>");
}
elseif ($process == "comment_delete")
{
    $csc_no = (isset($_POST['main_comm_no'])) ? $_POST['main_comm_no'] : "";

    $del_sql = "UPDATE `calendar_schedule_comment` SET display = '2' WHERE csc_no = '{$csc_no}'";

    if(mysqli_query($my_db, $del_sql)) {
        $comment_sub_del_sql = "UPDATE `calendar_schedule_comment` SET `display`='2' WHERE group_no='{$csc_no}'";
        mysqli_query($my_db, $comment_sub_del_sql);

        echo("<script>alert('삭제 하였습니다');history.back();</script>");
    } else {
        echo("<script>alert('삭제에 실패 하였습니다');history.back();</script>");
    }

    exit;

} elseif ($process == "comment_main_mod") {
    $cal_id 	 = (isset($_POST['cal_id'])) ? $_POST['cal_id'] : "";
    $cs_sel_day  = (isset($_POST['cs_sel_day'])) ? $_POST['cs_sel_day'] : "";
    $cs_no 		 = (isset($_POST['cs_no'])) ? $_POST['cs_no'] : "";

    $main_comm_no = (isset($_POST['main_comm_no'])) ? $_POST['main_comm_no'] : "";
    $comment_val  = (isset($_POST['f_comment'])) ? addslashes($_POST['f_comment']) : "";

    if(empty($main_comm_no)){
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }

    $upd_sql = "UPDATE `calendar_schedule_comment` SET comment='{$comment_val}' WHERE `csc_no`='{$main_comm_no}'";

    if(!mysqli_query($my_db, $upd_sql)) {
        exit ("<script>alert('코멘트 수정에 실패했습니다');history.back();</script>");
    }else{
        exit ("<script>alert('코멘트 수정에 성공했습니다');location.href='calendar_schedule_regist.php?cal_id={$cal_id}&cs_sel_day={$cs_sel_day}&cs_no={$cs_no}#sub-comment-wrapper-{$main_comm_no}';</script>");
    }

}
elseif ($process == "comment_sub_add")
{
    $cal_no 	 = (isset($_POST['cal_no'])) ? $_POST['cal_no'] : "";
    $cal_id 	 = (isset($_POST['cal_id'])) ? $_POST['cal_id'] : "";
    $cs_sel_day  = (isset($_POST['cs_sel_day'])) ? $_POST['cs_sel_day'] : "";
    $cs_no 		 = (isset($_POST['cs_no'])) ? $_POST['cs_no'] : "";
    $group_no 	 = (isset($_POST['group_no'])) ? $_POST['group_no'] : "";
    $parent_no 	 = (isset($_POST['parent_no'])) ? $_POST['parent_no'] : "";
    $depth 	     = (isset($_POST['depth'])) ? $_POST['depth']+1 : "1";
    $max_depth 	 = (isset($_POST['max_depth'])) ? $_POST['max_depth'] : "";
    $seq_val     = (isset($_POST['seq'])) ? $_POST['seq'] : "";
    $comment_new = (isset($_POST['f_comment_new'])) ? $_POST['f_comment_new'] : "";
    $is_secret 	 = (isset($_POST['is_secret'])) ? 1 : 0;
    $is_unknown  = (isset($_POST['is_unknown'])) ? 1 : 0;
    $regdate     = date("Y-m-d H:i:s");

    if(empty($seq_val)){
        $seq_max_sql    = "SELECT MAX(seq) as max_seq FROM `calendar_schedule_comment` WHERE display = '1' AND group_no='{$group_no}'";
        $seq_max_query  = mysqli_query($my_db, $seq_max_sql);
        $seq_max_result = mysqli_fetch_array($seq_max_query);
        $seq            = isset($seq_max_result['max_seq']) && !empty($seq_max_result['max_seq']) ? $seq_max_result['max_seq']+1 : 1;
    }else{

        if($max_depth == $depth){
            $max_chk_no = $parent_no;
        }else{
            $max_chk_no = $parent_no;
            for($i=$depth; $i<$max_depth; $i++)
            {
                if(empty($max_chk_no)){
                    break;
                }
                $max_chk_sql    = "SELECT `csc_no`, seq FROM `calendar_schedule_comment` WHERE display = '1' AND group_no='{$group_no}' AND parent_no='{$max_chk_no}' ORDER BY seq DESC LIMIT 1";
                $max_chk_query  = mysqli_query($my_db, $max_chk_sql);
                $max_chk_result = mysqli_fetch_assoc($max_chk_query);
                $max_chk_no     = isset($max_chk_result['csc_no']) && !empty($max_chk_result['csc_no']) ? $max_chk_result['csc_no'] : $parent_no;
                $seq_val        = isset($max_chk_result['seq']) && !empty($max_chk_result['seq']) ? $max_chk_result['seq'] : $seq_val;
            }
        }

        $seq_max_sql    = "SELECT MAX(seq) as max_seq FROM `calendar_schedule_comment` WHERE display = '1' AND group_no='{$group_no}' AND parent_no='{$max_chk_no}'";
        $seq_max_query  = mysqli_query($my_db, $seq_max_sql);
        $seq_max_result = mysqli_fetch_array($seq_max_query);
        $seq_max_value  = isset($seq_max_result['max_seq']) && !empty($seq_max_result['max_seq']) ? $seq_max_result['max_seq'] : $seq_val;
        $seq            = $seq_max_value+1;

        $seq_chk_sql    = "SELECT `csc_no` FROM `calendar_schedule_comment` WHERE display = '1' AND group_no='{$group_no}' AND seq > '{$seq_max_value}' ORDER BY seq ASC";
        $seq_chk_query  = mysqli_query($my_db, $seq_chk_sql);
        $seq_chk_up     = $seq;
        while($seq_chk_result = mysqli_fetch_array($seq_chk_query))
        {
            if($seq_chk_result['csc_no']){
                $seq_chk_up++;
                $seq_upd_sql = "UPDATE `calendar_schedule_comment` SET seq='{$seq_chk_up}' WHERE `csc_no`='{$seq_chk_result['csc_no']}'";
                mysqli_query($my_db, $seq_upd_sql);
            }
        }
    }

    $ins_sql = "INSERT INTO `calendar_schedule_comment` SET
					`cal_no`   = '{$cal_no}',
					`cs_no`    = '{$cs_no}',
					group_no   = '{$group_no}',
					parent_no  = '{$parent_no}',
					`depth`    = '{$depth}',
					`seq`      = '{$seq}',
					`comment`  = '".addslashes($comment_new)."',
					team       = (SELECT s.team	FROM staff s WHERE s.s_no = '{$session_s_no}'),
					s_no       = '{$session_s_no}',
					regdate    = '{$regdate}',
					is_secret  = {$is_secret},
					is_unknown = {$is_unknown}
		";

    if(!mysqli_query($my_db, $ins_sql)) {
        echo("<script>alert('코멘트 추가에 실패 하였습니다');history.back();</script>");
    }else{
        $sub_comm_no = mysqli_insert_id($my_db);
        exit ("<script>location.href='calendar_schedule_regist.php?cal_id={$cal_id}&cs_sel_day={$cs_sel_day}&cs_no={$cs_no}#sub-comment-wrapper-{$sub_comm_no}';</script>");
    }

} elseif ($process == "comment_sub_mod") {
    $cal_no 	 = (isset($_POST['cal_no'])) ? $_POST['cal_no'] : "";
    $cal_id 	 = (isset($_POST['cal_id'])) ? $_POST['cal_id'] : "";
    $cs_sel_day  = (isset($_POST['cs_sel_day'])) ? $_POST['cs_sel_day'] : "";
    $cs_no 		 = (isset($_POST['cs_no'])) ? $_POST['cs_no'] : "";
    $sub_comm_no = (isset($_POST['sub_comm_no'])) ? $_POST['sub_comm_no'] : "";
    $comment_val = (isset($_POST['f_comment'])) ? addslashes($_POST['f_comment']) : "";

    if(empty($sub_comm_no)){
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }

    $upd_sql = "UPDATE `calendar_schedule_comment` SET comment='{$comment_val}' WHERE `csc_no`='{$sub_comm_no}'";

    if(!mysqli_query($my_db, $upd_sql)) {
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }else{
        exit ("<script>location.href='calendar_schedule_regist.php?cal_id={$cal_id}&cs_sel_day={$cs_sel_day}&cs_no={$cs_no}#sub-comment-wrapper-{$sub_comm_no}';</script>");
    }

} elseif ($process == "comment_sub_delete") {
    $cal_no 	 = (isset($_POST['cal_no'])) ? $_POST['cal_no'] : "";
    $cal_id 	 = (isset($_POST['cal_id'])) ? $_POST['cal_id'] : "";
    $cs_sel_day  = (isset($_POST['cs_sel_day'])) ? $_POST['cs_sel_day'] : "";
    $cs_no 		 = (isset($_POST['cs_no'])) ? $_POST['cs_no'] : "";
    $group_no    = (isset($_POST['group_no'])) ? $_POST['group_no'] : "";
    $csc_no      = (isset($_POST['parent_no'])) ? $_POST['parent_no'] : "";
    $depth       = (isset($_POST['depth'])) ? $_POST['depth'] : "";
    $max_depth   = (isset($_POST['max_depth'])) ? $_POST['max_depth'] : "";

    $sql = "UPDATE `calendar_schedule_comment` SET display = '2' WHERE csc_no = '{$csc_no}'";

    if (mysqli_query($my_db, $sql)) {
        if ($max_depth > $depth) {
            $sub_del_no_list[] = $csc_no;
            for ($i = $depth; $i < $max_depth; $i++) {
                $sub_de_no_chk = implode(",", $sub_del_no_list);
                $sub_de_no_chk_sql = "SELECT csc_no FROM calendar_schedule_comment WHERE parent_no IN({$sub_de_no_chk}) AND display='1' AND group_no='{$group_no}'";
                $sub_de_no_chk_query = mysqli_query($my_db, $sub_de_no_chk_sql);
                while ($sub_de_no_chk_result = mysqli_fetch_assoc($sub_de_no_chk_query)) {
                    $sub_del_no_list[] = $sub_de_no_chk_result['csc_no'];
                }
            }

            if (!empty($sub_del_no_list)) {
                $sub_del_no_list = array_unique($sub_del_no_list);
                $sub_de_no_chk = implode(",", $sub_del_no_list);
                $comment_sub_del_sql = "UPDATE `calendar_schedule_comment` SET `display`='2' WHERE `csc_no` IN({$sub_de_no_chk})";
                mysqli_query($my_db, $comment_sub_del_sql);
            }
        }

        echo("<script>alert('삭제 하였습니다');history.back();</script>");
    } else {
        echo("<script>alert('삭제에 실패 하였습니다');history.back();</script>");
    }

    exit;
}

$team_model         = Team::Factory();
$team_all_list      = $team_model->getTeamAllList();
$team_name_list     = $team_all_list['team_name_list'];
$staff_team_list    = $team_all_list['staff_list'];

if(!empty($cs_no))
{
    $btn_title      = "수정완료";
    $btn_permission = false;
    $calendar_schedule_sql   = "SELECT *, (SELECT sub.s_name FROM staff sub WHERE sub.s_no=main.cs_s_no) as cs_s_name, (SELECT sub.team FROM staff sub WHERE sub.s_no=main.cs_s_no) as cs_team, (SELECT count(*) FROM calendar_schedule as sub WHERE sub.parent_cs_no='{$cs_no}') as child_cnt FROM calendar_schedule as main WHERE cs_no='{$cs_no}'";
    $calendar_schedule_query = mysqli_query($my_db, $calendar_schedule_sql);
    $calendar_schedule       = mysqli_fetch_assoc($calendar_schedule_query);

    $calendar_schedule['cs_s_name']  = $calendar_schedule['cs_s_name']."({$team_name_list[$calendar_schedule['cs_team']]})";

    if(!empty($calendar_schedule['cs_s_date']))
    {
        $cs_s_date = $calendar_schedule['cs_s_date'];
        $calendar_schedule['cs_s_day']   = date('Y-m-d', strtotime($cs_s_date));
        $calendar_schedule['cs_s_hour']  = date('H', strtotime($cs_s_date));
        $calendar_schedule['cs_s_min']   = date('i', strtotime($cs_s_date));
    }else{
        $calendar_schedule['cs_s_day']   = date('Y-m-d', strtotime($cs_sel_day));
        $calendar_schedule['cs_s_hour']  = date('H', strtotime('+1 hours'));
        $calendar_schedule['cs_s_min']   = ceil(date('i')/10)*10;
    }

    if(!empty($calendar_schedule['cs_e_date'])) {
        $cs_e_date = $calendar_schedule['cs_e_date'];
        $calendar_schedule['cs_e_day']   = date('Y-m-d', strtotime($cs_e_date));
        $calendar_schedule['cs_e_hour']  = date('H', strtotime($cs_e_date));
        $calendar_schedule['cs_e_min']   = date('i', strtotime($cs_e_date));
    }else{
        $calendar_schedule['cs_e_day']   = date('Y-m-d', strtotime($cs_sel_day));
        $calendar_schedule['cs_e_hour']  = date('H', strtotime("+2 hours"));
        $calendar_schedule['cs_e_min']   = ceil(date('i')/10)*10;
    }

    if(!empty($calendar_schedule['asset_reservation_list'])){
        $calendar_schedule['asset_reservation_list'] = explode(',', $calendar_schedule['asset_reservation_list']);
    }

    $file_paths = $calendar_schedule['file_path'];
    $file_names = $calendar_schedule['file_name'];
    if(!empty($file_paths) && !empty($file_names))
    {
        $calendar_schedule['file_paths'] = explode(',', $file_paths);
        $calendar_schedule['file_names'] = explode(',', $file_names);
        $calendar_schedule['file_origin_path'] = $file_paths;
        $calendar_schedule['file_origin_name'] = $file_names;
    }

    $cs_week_val = $calendar_schedule['date_repeat_week'];
    if(!empty($cs_week_val))
    {
        $date_repeat_week_list = [];
        for($i=0;$i<strlen($cs_week_val);$i++){
            $date_repeat_week_list[] = substr ($cs_week_val, $i, 1);
        }
        $smarty->assign("date_repeat_week_list", $date_repeat_week_list);
    }

    if(!empty($calendar_schedule['cs_alert']) && $calendar_schedule['cs_alert'] == '1')
    {
        $calendar_schedule['cs_alert_day'] = date('Y-m-d', strtotime("{$calendar_schedule['cs_alert_date']}"));
        $calendar_schedule['cs_alert_hour'] = date('H', strtotime("{$calendar_schedule['cs_alert_date']}"));
        $calendar_schedule['cs_alert_min'] = date('i', strtotime("{$calendar_schedule['cs_alert_date']}"));
    }


    $calendar_permission_list   = [];
    $origin_csp_no_list         = [];
    $cs_permission_sql    = "SELECT*FROM calendar_schedule_permission where cs_no='{$calendar_schedule['cs_no']}'";
    $cs_permission_query  = mysqli_query($my_db, $cs_permission_sql);
    while($cs_permission_result = mysqli_fetch_assoc($cs_permission_query))
    {
        $calendar_permission_list[] = $cs_permission_result;
        $origin_csp_no_list[] = $cs_permission_result['csp_no'];
    }

    $origin_csp_no = !empty($calendar_permission_list) ? implode(",", $origin_csp_no_list) : "";
    $smarty->assign('calendar_permission_list', $calendar_permission_list);
    $smarty->assign('origin_csp_no', $origin_csp_no);


    $comment_list = [];
    $comment_sql  = "
            SELECT
                *,
                (SELECT s_name FROM staff s where s.s_no=cmt.s_no) AS s_name,
                (SELECT count(sub_cmt.`csc_no`) FROM calendar_schedule_comment sub_cmt WHERE sub_cmt.group_no = cmt.csc_no AND sub_cmt.`display`='1') as sub_comm_cnt,
                (SELECT MAX(sub_cmt.`depth`) FROM calendar_schedule_comment sub_cmt WHERE sub_cmt.group_no = cmt.csc_no AND sub_cmt.`display`='1') as sub_max_depth
            FROM `calendar_schedule_comment` cmt
            WHERE cmt.cs_no='{$cs_no}' AND cmt.cal_no='{$calendar_schedule['cal_no']}' AND cmt.display = '1' AND (cmt.parent_no is null OR cmt.parent_no = '0')
            ORDER BY cmt.regdate DESC
        ";

    $result= mysqli_query($my_db, $comment_sql);
    while($comment_array = mysqli_fetch_array($result))
    {
        $sub_comm_list = [];
        if($comment_array['sub_comm_cnt'] > 0)
        {
            $sub_comm_where = "cmt.cs_no ='{$cs_no}' AND cmt.display = '1' AND cmt.group_no = '{$comment_array['csc_no']}'";
            $sub_comm_sql = "
                        SELECT
                            *,
                            (SELECT s_name FROM staff s where s.s_no=cmt.s_no) AS s_name
                        FROM calendar_schedule_comment cmt WHERE {$sub_comm_where} ORDER BY cmt.seq ASC, cmt.regdate DESC";

            $sub_comm_query = mysqli_query($my_db, $sub_comm_sql);
            while($sub_comm_result = mysqli_fetch_assoc($sub_comm_query))
            {
                $sub_comm_result['comment']         = str_replace("\r\n","<br />",htmlspecialchars($sub_comm_result['comment']));
                $sub_comm_result['comment_mod']     = $sub_comm_result['comment'];
                $sub_comm_result['regdate_day']     = date("Y/m/d",strtotime($sub_comm_result['regdate']));
                $sub_comm_result['regdate_time']    = date("H:i",strtotime($sub_comm_result['regdate']));
                $sub_comm_result['depth_width']     = ($sub_comm_result['depth']*30)."px";
                $sub_comm_list[] = $sub_comm_result;
            }
        }

        $comment_array['comment']       = str_replace("\r\n","<br />",htmlspecialchars($comment_array['comment']));
        $comment_array['comment_mod']   = $comment_array['comment'];
        $comment_array['regdate_day']   = date("Y/m/d",strtotime($comment_array['regdate']));
        $comment_array['regdate_time']  = date("H:i",strtotime($comment_array['regdate']));
        $comment_array['sub_comm_list'] = $sub_comm_list;

        $comment_list[] = $comment_array;
    }

    $smarty->assign("comment_list", $comment_list);


    # Manager 권한 체크
    $manager_permission = $cal_schedule_model->checkPermission("manager", $calendar_item['cal_no'], $session_s_no);

    if($session_s_no == $calendar_schedule['cs_s_no'] || $manager_permission){
        $btn_permission = true;
    }
}
else
{
    if($calendar_item['view_date'] == '2'){
        $calendar_schedule['cs_all'] = 1;
    }

    $calendar_schedule['cs_type']        = 'normal';
    $calendar_schedule['cs_permission']  = $calendar_item['view_schedule_permission_set'];
    $calendar_schedule['cs_content']     = $calendar_item['view_content_format'];

    if($calendar_item['view_schedule_permission_set'] == '2')
    {
        $calendar_permission_sql   = "SELECT * FROM calendar_manager_permission WHERE cmp_type='schedule' AND cal_no='{$calendar_item['cal_no']}'";
        $calendar_permission_query = mysqli_query($my_db, $calendar_permission_sql);
        $calendar_permission_list  = [];
        while($calendar_permission = mysqli_fetch_assoc($calendar_permission_query))
        {
            $calendar_permission['csp_team'] = $calendar_permission['cmp_team'];
            $calendar_permission['csp_s_no'] = $calendar_permission['cmp_s_no'];
            $calendar_permission_list[] = $calendar_permission;
        }

        $calendar_schedule['calendar_permission_list'] = $calendar_permission_list;

    }

    if($calendar_item['view_schedule_permission_set'] != '0'){
        $btn_permission = true;
    }

    $calendar_schedule['cs_s_no']    = $session_s_no;
    $calendar_schedule['cs_s_name']  = $session_name."({$team_name_list[$session_team]})";
    $calendar_schedule['cs_s_day']   = $calendar_schedule['cs_e_day'] = $calendar_schedule['date_repeat_start'] = date('Y-m-d', strtotime($cs_sel_day));
    $calendar_schedule['cs_s_hour']  = date('H', strtotime('+1 hours'));
    $calendar_schedule['cs_e_hour']  = date('H', strtotime("+2 hours"));
    $calendar_schedule['cs_s_min']   = $calendar_schedule['cs_e_min'] = ceil(date('i')/10)*10;
}


if(!isset($calendar_schedule['file_paths']) || !isset($calendar_schedule['file_names']))
{
    $calendar_schedule['file_paths'] = "";
    $calendar_schedule['file_names'] = "";
}

$file_count = !empty($calendar_schedule['file_paths']) ? count($calendar_schedule['file_paths']) : 0;
$smarty->assign("file_count", $file_count);

# 카테고리 옵션
$company_model          = Company::Factory();
$cs_brand_company_list  = [];
if(isset($calendar_item['view_brand_company_list']) && !empty($calendar_item['view_brand_company_list']))
{
    $cs_brand_tmp_list = explode(",", $calendar_item['view_brand_company_list']);
    foreach($cs_brand_tmp_list as $brand_no){
        $brand_item = $company_model->getItem($brand_no);
        $cs_brand_company_list[$brand_no] = $brand_item['c_name'];
    }
}

# 카테고리 옵션
$cs_category_list = [];
if(isset($calendar_item['view_category_name']) && !empty($calendar_item['view_category_name']))
{
    $cs_category_list = explode("$", $calendar_item['view_category_name']);
}

# 공유자산 옵션
$asset_model          = Asset::Factory();
$asset_share_list     = $asset_model->getAssetShareList();
$new_asset_share_list = [];
if(!empty($calendar_item['view_asset_reservation_list'])){
    $new_asset_share_val = explode(',', $calendar_item['view_asset_reservation_list']);

    foreach($new_asset_share_val as $new_asset_no){
        $new_asset_share_list[$new_asset_no] = $asset_share_list[$new_asset_no];
    }
}

$smarty->assign("calendar", $calendar_item);
$smarty->assign($calendar_schedule);
$smarty->assign("cal_id", $cal_id);
$smarty->assign("btn_title", $btn_title);
$smarty->assign("cs_sel_day", $cs_sel_day);
$smarty->assign('cs_brand_company_list', $cs_brand_company_list);
$smarty->assign('cs_category_list', $cs_category_list);
$smarty->assign('hour_list', getHourOption());
$smarty->assign('min_list', getMinOption());
$smarty->assign('day_list', getDayOption());
$smarty->assign('repeat_type_list', getRepeatTypeOption());
$smarty->assign('repeat_val_list', getRepeatValOption());
$smarty->assign('new_asset_share_list', $new_asset_share_list);
$smarty->assign('cs_permission_list', getViewSchedulePermissionSetOption());
$smarty->assign('team_list', $team_name_list);
$smarty->assign('staff_list', $staff_team_list);
$smarty->assign('btn_permission', $btn_permission);

$smarty->display('calendar/calendar_schedule_regist.html');
?>