<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/calendar.php');
require('../inc/model/Calendar.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

if(!permissionNameCheck($session_permission, "마스터관리자")){
    $smarty->display('access_error.html');
    exit;
}

# 프로세스 처리
$process        = (isset($_POST['process']))?$_POST['process']:"";
$calendar_model = Calendar::Factory();

if($process == 'save_memo')
{
    $cal_no = (isset($_POST['cal_no'])) ? $_POST['cal_no'] : "";
    $memo   = (isset($_POST['memo'])) ? addslashes($_POST['memo']) : "";

    if(empty($cal_no)){
        exit("<script>alert('캘린더 번호가 없습니다. 다시 시도해주세요');location.href='calendar_manger_list.php';</script>");
    }

    if($calendar_model->update(array("cal_no" => $cal_no, "memo" => $memo))){
        exit("<script>alert('메모 저장했습니다');location.href='calendar_manager_list.php';</script>");
    }else{
        exit("<script>alert('메모 저장에 실패했습니다');location.href='calendar_manager_list.php';</script>");
    }
}

$calendar_manager_sql   = "SELECT * FROM calendar_manager ORDER BY cal_no DESC";
$calendar_manager_query = mysqli_query($my_db, $calendar_manager_sql);
$calendar_manager_list  = [];
$display_option         = getDisplayOption();
$view_schedule_permission_set_option = getViewSchedulePermissionSetOption();
while($calendar_manager = mysqli_fetch_assoc($calendar_manager_query))
{
    $calendar_manager['display_status'] = $display_option[$calendar_manager['display']];
    $calendar_manager['view_schedule_permission_set_option'] = $view_schedule_permission_set_option[$calendar_manager['view_schedule_permission_set']];

    $calendar_manager_list[] = $calendar_manager;
}

$smarty->assign("calendar_manager_list", $calendar_manager_list);

$smarty->display('calendar/calendar_manager_list.html');
?>
