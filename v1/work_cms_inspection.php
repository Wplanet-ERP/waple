<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_cms.php');
require('inc/helper/advertising.php');
require('inc/model/Custom.php');
require('inc/model/ProductCms.php');
require('inc/model/WorkCms.php');

# Model Init
$product_model  = ProductCms::Factory();

# 프로세스 처리
$process = (isset($_POST['process'])) ? $_POST['process'] : "";

/////////////////////////* 자동 저장 처리 부분 *//////////////////////
if($process == "cms_apply_delivery")
{
    # 단독상품 변경처리
    $solo_prd_sql    = "SELECT prd_no, solo_prd FROM product_cms WHERE solo_prd != '' AND display='1'";
    $solo_prd_query  = mysqli_query($my_db, $solo_prd_sql);
    while($solo_prd = mysqli_fetch_assoc($solo_prd_query))
    {
        $prd_no         = $solo_prd['prd_no'];
        $solo_prd_no    = $solo_prd['solo_prd'];
        $chk_solo_sql   = "SELECT t_no FROM work_cms_temporary WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_temporary WHERE prd_no='{$prd_no}') AND unit_price > 0 GROUP BY order_number HAVING COUNT(*) = 1";
        $chk_solo_query = mysqli_query($my_db, $chk_solo_sql);
        while($chk_solo_result = mysqli_fetch_assoc($chk_solo_query))
        {
            $chk_solo_upd   = "UPDATE work_cms_temporary SET prd_no = '{$solo_prd_no}' WHERE t_no='{$chk_solo_result['t_no']}'";
            mysqli_query($my_db, $chk_solo_upd);
        }
    }

    # 사은품설정(A) 처리
    $gift_prd_sql        = "SELECT * FROM product_cms WHERE gift_date_type='1' AND gift_prd != '' AND display='1'";
    $gift_prd_query      = mysqli_query($my_db, $gift_prd_sql);
    $gift_prd_list       = [];
    $gift_prd_where_list = [];
    while($gift_prd = mysqli_fetch_assoc($gift_prd_query))
    {
        $gift_chk_prd       = [];
        $gift_chk_dp        = [];

        if(!empty($gift_prd['gift_prd']))
        {
            $gift_chk_prd_tmp   = explode(',', $gift_prd['gift_prd']);
            foreach($gift_chk_prd_tmp as $gift_chk_prd_val_tmp)
            {
                $gift_chk_prd_val = trim($gift_chk_prd_val_tmp);
                if(!empty($gift_chk_prd_val)){
                    $gift_chk_prd[] = trim($gift_chk_prd_val);
                }
            }
        }

        if(!empty($gift_prd['gift_dp']))
        {
            $gift_chk_dp_tmp   = explode(',', $gift_prd['gift_dp']);
            foreach($gift_chk_dp_tmp as $gift_chk_dp_val_tmp)
            {
                $gift_chk_dp_val = trim($gift_chk_dp_val_tmp);
                if(!empty($gift_chk_dp_val)){
                    $gift_chk_dp[] = $gift_chk_dp_val;
                }
            }
        }

        $gift_prd_list[$gift_prd['prd_no']] = array(
            "prd_list"      => $gift_chk_prd,
            "dp_list"       => $gift_chk_dp,
            "gift_s_date"   => $gift_prd['gift_s_date'],
            "gift_e_date"   => $gift_prd['gift_e_date']
        );
        $gift_prd_where_list[$gift_prd['prd_no']] = $gift_prd['prd_no'];
    }

    # 사은품설정(A) 처리
    if(!empty($gift_prd_where_list))
    {
        $gift_prd_where     = implode(',', $gift_prd_where_list);
        $gift_prd_chk_sql   = "SELECT t_no, order_number, prd_no, quantity, order_date, dp_c_no, dp_c_name, is_wait FROM work_cms_temporary as main WHERE main.order_number IN(SELECT DISTINCT order_number FROM work_cms_temporary wct WHERE wct.prd_no IN({$gift_prd_where})) AND is_gift='0' ORDER BY main.t_no ASC";
        $gift_prd_chk_query = mysqli_query($my_db, $gift_prd_chk_sql);

        while($gift_prd_chk_result = mysqli_fetch_assoc($gift_prd_chk_query))
        {
            if(isset($gift_prd_list[$gift_prd_chk_result['prd_no']]) && !empty($gift_prd_list[$gift_prd_chk_result['prd_no']]))
            {
                $gift_chk_t_no          = $gift_prd_chk_result['t_no'];
                $gift_chk_ord_no        = $gift_prd_chk_result['order_number'];
                $gift_chk_prd_no        = $gift_prd_chk_result['prd_no'];
                $gift_chk_quantity      = $gift_prd_chk_result['quantity'];
                $gift_chk_order_date    = $gift_prd_chk_result['order_date'];
                $gift_chk_dp_c_no       = $gift_prd_chk_result['dp_c_no'];
                $gift_chk_dp_c_name     = $gift_prd_chk_result['dp_c_name'];

                $gift_prd_list_option   = $gift_prd_list[$gift_prd_chk_result['prd_no']]['prd_list'];
                $gift_dp_list_option    = $gift_prd_list[$gift_prd_chk_result['prd_no']]['dp_list'];
                $gift_s_date_option     = $gift_prd_list[$gift_prd_chk_result['prd_no']]['gift_s_date'];
                $gift_e_date_option     = $gift_prd_list[$gift_prd_chk_result['prd_no']]['gift_e_date'];

                if(!empty($gift_dp_list_option))
                {
                    if(!in_array($gift_chk_dp_c_name, $gift_dp_list_option)){
                        continue;
                    }
                }

                if(!empty($gift_s_date_option) && !empty($gift_e_date_option)){
                    if($gift_chk_order_date < $gift_s_date_option || $gift_chk_order_date >= $gift_e_date_option) {
                        continue;
                    }
                }elseif(!empty($gift_s_date_option)){
                    if($gift_chk_order_date < $gift_s_date_option) {
                        continue;
                    }
                }elseif(!empty($gift_e_date_option)){
                    if($gift_chk_order_date >= $gift_e_date_option) {
                        continue;
                    }
                }

                $gift_chk_wait = $gift_prd_chk_result['is_wait'];

                foreach($gift_prd_list_option as $gift_prd)
                {
                    $gift_product  = trim($gift_prd);
                    $gift_pro_data = $product_model->getWorkCmsItem($gift_product);

                    $ins_sql = "
                        INSERT INTO work_cms_temporary(t_type, c_no, c_name, log_c_no, s_no, `team`, prd_no, stock_date, quantity,
                        order_type, order_number, parent_order_number, order_date, payment_date, recipient, recipient_addr, recipient_hp, recipient_hp2, zip_code,
                        dp_c_name, dp_c_no, manager_memo, regdate, write_date, task_req, task_req_s_no, task_req_team, task_run_regdate, task_run_s_no,
                        task_run_team, delivery_memo, payment_type, `account`, `is_wait`, `out_date`)

                        (SELECT t_type, '{$gift_pro_data['c_no']}', '{$gift_pro_data['c_name']}', log_c_no, '{$gift_pro_data['manager']}', '{$gift_pro_data['team']}', '{$gift_product}', stock_date, '{$gift_chk_quantity}',
                        order_type, order_number, parent_order_number, order_date, payment_date, recipient, recipient_addr, recipient_hp, recipient_hp2, zip_code,
                        dp_c_name, dp_c_no, manager_memo, regdate, write_date, task_req, task_req_s_no, task_req_team, task_run_regdate, task_run_s_no,
                        task_run_team, delivery_memo, payment_type, `account`, '{$gift_chk_wait}', out_date
                        FROM work_cms_temporary WHERE t_no='{$gift_chk_t_no}')
                    ";

                    if(mysqli_query($my_db, $ins_sql)){
                        $chk_upd_sql = "UPDATE work_cms_temporary SET is_gift='1' WHERE t_no='{$gift_chk_t_no}'";
                        mysqli_query($my_db, $chk_upd_sql);
                    }else{
                        echo "사은품(A) 추가 중 오류가 발생했습니다. 개발팀(임태형)에게 문의 부탁드립니다.";
                        exit;
                    }
                }
            }
        }
    }

    # 사은품설정(B) 처리
    $sub_gift_prd_sql        = "SELECT * FROM product_cms WHERE sub_gift_date_type='1' AND sub_gift_prd != '' AND display='1'";
    $sub_gift_prd_query      = mysqli_query($my_db, $sub_gift_prd_sql);
    $sub_gift_prd_list       = [];
    $sub_gift_prd_where_list = [];
    while($sub_gift_prd = mysqli_fetch_assoc($sub_gift_prd_query))
    {
        $sub_gift_chk_prd       = [];
        $sub_gift_chk_dp        = [];

        if(!empty($sub_gift_prd['sub_gift_prd']))
        {
            $sub_gift_chk_prd_tmp   = explode(',', $sub_gift_prd['sub_gift_prd']);
            foreach($sub_gift_chk_prd_tmp as $sub_gift_chk_prd_val_tmp)
            {
                $sub_gift_chk_prd_val = trim($sub_gift_chk_prd_val_tmp);
                if(!empty($sub_gift_chk_prd_val)){
                    $sub_gift_chk_prd[] = trim($sub_gift_chk_prd_val);
                }
            }
        }

        if(!empty($sub_gift_prd['sub_gift_dp']))
        {
            $sub_gift_chk_dp_tmp   = explode(',', $sub_gift_prd['sub_gift_dp']);
            foreach($sub_gift_chk_dp_tmp as $sub_gift_chk_dp_val_tmp)
            {
                $sub_gift_chk_dp_val = trim($sub_gift_chk_dp_val_tmp);
                if(!empty($sub_gift_chk_dp_val)){
                    $sub_gift_chk_dp[] = $sub_gift_chk_dp_val;
                }
            }
        }

        $sub_gift_prd_list[$sub_gift_prd['prd_no']] = array(
            "prd_list"      => $sub_gift_chk_prd,
            "dp_list"       => $sub_gift_chk_dp,
            "gift_s_date"   => $sub_gift_prd['sub_gift_s_date'],
            "gift_e_date"   => $sub_gift_prd['sub_gift_e_date'],
        );
        $sub_gift_prd_where_list[$sub_gift_prd['prd_no']] = $sub_gift_prd['prd_no'];
    }


    # 사은품설정(B) 처리
    if(!empty($sub_gift_prd_where_list))
    {
        $sub_gift_prd_where     = implode(',', $sub_gift_prd_where_list);
        $sub_gift_prd_chk_sql   = "SELECT t_no, order_number, prd_no, quantity, order_date, dp_c_no, dp_c_name, is_wait FROM work_cms_temporary as main WHERE main.order_number IN(SELECT DISTINCT order_number FROM work_cms_temporary wct WHERE wct.prd_no IN({$sub_gift_prd_where})) AND is_sub_gift='0' ORDER BY main.t_no ASC";
        $sub_gift_prd_chk_query = mysqli_query($my_db, $sub_gift_prd_chk_sql);

        while($sub_gift_prd_chk_result = mysqli_fetch_assoc($sub_gift_prd_chk_query))
        {
            if(isset($sub_gift_prd_list[$sub_gift_prd_chk_result['prd_no']]) && !empty($sub_gift_prd_list[$sub_gift_prd_chk_result['prd_no']]))
            {
                $sub_gift_chk_t_no          = $sub_gift_prd_chk_result['t_no'];
                $sub_gift_chk_ord_no        = $sub_gift_prd_chk_result['order_number'];
                $sub_gift_chk_prd_no        = $sub_gift_prd_chk_result['prd_no'];
                $sub_gift_chk_quantity      = $sub_gift_prd_chk_result['quantity'];
                $sub_gift_chk_order_date    = $sub_gift_prd_chk_result['order_date'];
                $sub_gift_chk_dp_c_no       = $sub_gift_prd_chk_result['dp_c_no'];
                $sub_gift_chk_dp_c_name     = $sub_gift_prd_chk_result['dp_c_name'];

                $sub_gift_prd_list_option   = $sub_gift_prd_list[$sub_gift_prd_chk_result['prd_no']]['prd_list'];
                $sub_gift_dp_list_option    = $sub_gift_prd_list[$sub_gift_prd_chk_result['prd_no']]['dp_list'];
                $sub_gift_s_date_option     = $sub_gift_prd_list[$sub_gift_prd_chk_result['prd_no']]['gift_s_date'];
                $sub_gift_e_date_option     = $sub_gift_prd_list[$sub_gift_prd_chk_result['prd_no']]['gift_e_date'];

                if(!empty($sub_gift_dp_list_option))
                {
                    if(!in_array($sub_gift_chk_dp_c_name, $sub_gift_dp_list_option)){
                        continue;
                    }
                }

                if(!empty($sub_gift_s_date_option) && !empty($sub_gift_e_date_option)){
                    if($sub_gift_chk_order_date < $sub_gift_s_date_option || $sub_gift_chk_order_date >= $sub_gift_e_date_option) {
                        continue;
                    }
                }elseif(!empty($sub_gift_s_date_option)){
                    if($sub_gift_chk_order_date < $sub_gift_s_date_option) {
                        continue;
                    }
                }elseif(!empty($sub_gift_e_date_option)){
                    if($sub_gift_chk_order_date >= $sub_gift_e_date_option) {
                        continue;
                    }
                }

                $sub_gift_chk_wait = $sub_gift_prd_chk_result['is_wait'];

                foreach($sub_gift_prd_list_option as $sub_gift_prd)
                {
                    $sub_gift_product   = trim($sub_gift_prd);
                    $sub_gift_pro_data  = $product_model->getWorkCmsItem($sub_gift_product);
                    $sub_ins_sql = "
                        INSERT INTO work_cms_temporary(t_type, c_no, c_name, log_c_no, s_no, `team`, prd_no, stock_date, quantity,
                        order_type, order_number, parent_order_number, order_date, payment_date, recipient, recipient_addr, recipient_hp, recipient_hp2, zip_code,
                        dp_c_name, dp_c_no, manager_memo, regdate, write_date, task_req, task_req_s_no, task_req_team, task_run_regdate, task_run_s_no,
                        task_run_team, delivery_memo, payment_type, `account`, `is_wait`, `out_date`)

                        (SELECT t_type, '{$sub_gift_pro_data['c_no']}', '{$sub_gift_pro_data['c_name']}', log_c_no, '{$sub_gift_pro_data['manager']}', '{$sub_gift_pro_data['team']}', '{$sub_gift_product}', stock_date, '{$sub_gift_chk_quantity}',
                        order_type, order_number, parent_order_number, order_date, payment_date, recipient, recipient_addr, recipient_hp, recipient_hp2, zip_code,
                        dp_c_name, dp_c_no, manager_memo, regdate, write_date, task_req, task_req_s_no, task_req_team, task_run_regdate, task_run_s_no, 
                        task_run_team, delivery_memo, payment_type, `account`, '{$sub_gift_chk_wait}', out_date
                        FROM work_cms_temporary WHERE t_no='{$sub_gift_chk_t_no}')
                    ";

                    if(mysqli_query($my_db, $sub_ins_sql)) {
                        $chk_sub_upd_sql = "UPDATE work_cms_temporary SET is_sub_gift='1' WHERE t_no='{$sub_gift_chk_t_no}'";
                        mysqli_query($my_db, $chk_sub_upd_sql);
                    }else{
                        echo "사은품(B) 추가 중 오류가 발생했습니다. 개발팀(임태형)에게 문의 부탁드립니다.";
                        exit;
                    }
                }
            }
        }
    }

    # 합배송 상품정보
    $combined_prd_sql        = "SELECT * FROM product_cms WHERE combined_pack='2' AND combined_except != '' AND combined_product != '' AND display='1'";
    $combined_prd_query      = mysqli_query($my_db, $combined_prd_sql);
    $combined_except_list    = [];
    $combined_product_list   = [];
    $combined_prd_where_list = [];
    while($combined_prd = mysqli_fetch_assoc($combined_prd_query))
    {
        $combined_product_list[$combined_prd['prd_no']] = array(
            'except'    => explode(',', $combined_prd['combined_except']),
            'combined'  => $combined_prd['combined_product']
        );

        $combined_prd_where_list[$combined_prd['prd_no']] = $combined_prd['prd_no'];
    }

    # 합배송 처리
    if(!empty($combined_prd_where_list))
    {
        # 1 : 합배송 관련 상품 주문번호 추출
        $combined_prd_where = implode(',', $combined_prd_where_list);
        $combined_chk_sql   = "SELECT * FROM work_cms_temporary as main WHERE main.order_number IN(SELECT DISTINCT order_number FROM work_cms_temporary wct WHERE wct.prd_no IN({$combined_prd_where})) AND main.order_number NOT IN(SELECT DISTINCT order_number FROM work_cms_temporary wct WHERE wct.prd_no IN(SELECT combined_product FROM product_cms WHERE combined_pack='2' AND combined_product !='' AND display='1')) ORDER BY main.t_no ASC";
        $combined_chk_query = mysqli_query($my_db, $combined_chk_sql);
        $combined_ord_list  = [];
        $combined_chg_list  = [];
        while($combined_chk = mysqli_fetch_assoc($combined_chk_query))
        {
            $combined_ord_list[$combined_chk['order_number']][] = array(
                't_no'                  => $combined_chk['t_no'],
                'prd_no'                => $combined_chk['prd_no'],
                'qty'                   => $combined_chk['quantity'],
                'price'                 => $combined_chk['dp_price'],
                'price_vat'             => $combined_chk['dp_price_vat'],
                'unit_delivery_price'   => $combined_chk['unit_delivery_price'],
                'coupon_price'          => $combined_chk['coupon_price'],
            );
        }

        # 2 : 합배송 부자재 여부 확인
        if(!empty($combined_ord_list))
        {
            foreach($combined_ord_list as $ord_no => $combined_products)
            {
                $combined_products = array_reverse($combined_products);
                foreach($combined_products as $combined_product)
                {
                    $comb_except  = isset($combined_product_list[$combined_product['prd_no']]) ? $combined_product_list[$combined_product['prd_no']]['except'] : "";
                    $comb_pro_no  = isset($combined_product_list[$combined_product['prd_no']]) ? $combined_product_list[$combined_product['prd_no']]['combined'] : "";

                    if((!empty($comb_except) && !empty($comb_pro_no)) && combinedExceptCheck($combined_products, $comb_except))
                    {
                        $combined_chg_list[$ord_no] = array('t_no' => $combined_product['t_no'], 'prd_no' => $combined_product['prd_no'], "comb_prd_no" => $comb_pro_no, 'org_prd' => $combined_product);
                        break;
                    }
                }
            }
        }

        # 3 : 합배송 해당 상품 교체
        if(!empty($combined_chg_list))
        {
            foreach($combined_chg_list as $combined_ord_no => $combined_chg_data)
            {
                $combined_org_prd = $combined_chg_data['org_prd'];
                if($combined_org_prd['qty'] > 1)
                {
                    $combine_price_val          = $combined_org_prd['price']/$combined_org_prd['qty'];
                    $combine_price_vat_val      = $combined_org_prd['price_vat']/$combined_org_prd['qty'];
                    $combine_unit_delivery_val  = ($combined_org_prd['unit_delivery_price'] > 0) ? $combined_org_prd['unit_delivery_price']/$combined_org_prd['qty'] : 0;
                    $combine_coupon_val         = ($combined_org_prd['coupon_price'] > 0) ? $combined_org_prd['coupon_price']/$combined_org_prd['qty'] : 0;

                    $combine_price          = (int)$combine_price_val;
                    $combine_price_vat      = (int)$combine_price_vat_val;
                    $combine_unit_delivery  = (int)$combine_unit_delivery_val;
                    $combine_coupon_price   = (int)$combine_coupon_val;

                    $org_upd_qty            = $combined_org_prd['qty'] - 1;
                    $org_upd_price          = $combined_org_prd['price']-$combine_price;
                    $org_upd_price_vat      = $combined_org_prd['price_vat']-$combine_price_vat;
                    $ord_upd_unit_delivery  = $combined_org_prd['unit_delivery_price'] - $combine_unit_delivery_val;
                    $ord_upd_coupon_price   = $combined_org_prd['coupon_price'] - $combine_coupon_price;
                    $combined_shop_ord_no   = $combined_ord_no."_".$combined_chg_data['comb_prd_no'];

                    $comb_ins_sql = "
                        INSERT INTO work_cms_temporary(t_type, c_no, c_name, log_c_no, s_no, `team`, prd_no, stock_date, quantity, order_type, order_number, parent_order_number, shop_ord_no, order_date, payment_date, recipient, recipient_addr, recipient_hp, recipient_hp2, zip_code, dp_price, dp_price_vat, dp_c_name, dp_c_no, manager_memo, regdate, write_date, task_req, task_req_s_no, task_req_team, task_run_regdate, task_run_s_no, task_run_team, notice, delivery_memo, payment_type, `account`, coupon_price, final_price, unit_price, unit_delivery_price, `page_idx`)
                        (SELECT t_type, c_no, c_name, log_c_no, s_no, `team`, '{$combined_chg_data['comb_prd_no']}', stock_date, '1', order_type, order_number, parent_order_number, '{$combined_shop_ord_no}', order_date, payment_date, recipient, recipient_addr, recipient_hp, recipient_hp2, zip_code, '{$combine_price}', '{$combine_price_vat}', dp_c_name, dp_c_no, '합포장수정', regdate, write_date, task_req, task_req_s_no, task_req_team, task_run_regdate, task_run_s_no, task_run_team, notice, delivery_memo, payment_type, `account`, '{$combine_coupon_price}', final_price, '{$combine_price_vat}', '{$ord_upd_unit_delivery}', `page_idx`
                        FROM work_cms_temporary WHERE t_no='{$combined_chg_data['t_no']}')
                    ";

                    if(mysqli_query($my_db, $comb_ins_sql)){
                        $upd_sql = "UPDATE work_cms_temporary SET quantity='{$org_upd_qty}', dp_price='{$org_upd_price}', dp_price_vat='{$org_upd_price_vat}', unit_price='{$org_upd_price_vat}', unit_delivery_price='{$ord_upd_unit_delivery}', coupon_price='{$ord_upd_coupon_price}' WHERE t_no='{$combined_chg_data['t_no']}'";
                        mysqli_query($my_db, $upd_sql);
                    }else{
                        echo "합배송 처리 중 오류가 발생했습니다. 개발팀(임태형)에게 문의 부탁드립니다.";
                        exit;
                    }
                }else{
                    $upd_sql = "UPDATE work_cms_temporary SET prd_no='{$combined_chg_data['comb_prd_no']}', manager_memo='합포장수정' WHERE t_no='{$combined_chg_data['t_no']}'";
                    mysqli_query($my_db, $upd_sql);
                }
            }
        }
    }

    # 닥터피엘 이벤트
    $gift_order_list            = [];
    $gift_special_order_list    = [];
    $gift_order_product_info    = array(
        "434"   => array('c_no' => '1314', 'c_name' => '닥터피엘(샤워기/세면대/주방용/세탁기)', 's_no' => '42', 'team'=> '00245'),
        "903"   => array('c_no' => '4140', 'c_name' => '닥터피엘 인텐시브 칫솔', 's_no' => '6', 'team'=> '00246'),
        "1092"  => array('c_no' => '1314', 'c_name' => '닥터피엘(샤워기/세면대/주방용/세탁기)', 's_no' => '42', 'team'=> '00245'),
        "1271"  => array('c_no' => '4140', 'c_name' => '닥터피엘 인텐시브 칫솔', 's_no' => '6', 'team'=> '00246'),
        "1616"  => array('c_no' => '1314', 'c_name' => '닥터피엘(샤워기/세면대/주방용/세탁기)', 's_no' => '42', 'team'=> '00245'),
        "1871"  => array('c_no' => '5434', 'c_name' => '닥터피엘 여행용', 's_no' => '67', 'team'=> '00257'),
        "1908"  => array('c_no' => '1314', 'c_name' => '닥터피엘(샤워기/세면대/주방용/세탁기)', 's_no' => '42', 'team'=> '00245'),
        "2009"  => array('c_no' => '1314', 'c_name' => '닥터피엘(샤워기/세면대/주방용/세탁기)', 's_no' => '42', 'team'=> '00245'),
        "2202"  => array('c_no' => '1314', 'c_name' => '닥터피엘(샤워기/세면대/주방용/세탁기)', 's_no' => '42', 'team'=> '00245'),
    );

    # 이벤트 테이블 닥터피엘 데이터 가져오기
    $event_limit_e_date = date("Y-m-d", strtotime("-10 days"));
    $event_doc_sql      = "SELECT * FROM advertising_event WHERE event_no IN(1,2,3,4,8,10,14,19,21,23,24,27,30,31,37,39,43) AND is_active='1' AND (event_e_date IS NULL OR event_e_date >= '{$event_limit_e_date}') ORDER BY event_no, event_s_date ASC";
    $event_doc_query    = mysqli_query($my_db, $event_doc_sql);
    $event_doc_option   = getEventProductOption();
    $event_doc_list     = [];
    while($event_doc_result = mysqli_fetch_assoc($event_doc_query))
    {
        $doc_dp_list = explode(",", $event_doc_result['dp_list']);
        foreach($doc_dp_list as $doc_dp_val) {
            $event_doc_list[$doc_dp_val][] = array("event_no" => $event_doc_result['event_no'], "s_date" => $event_doc_result['event_s_date'], "e_date" => $event_doc_result['event_e_date']);
        }
    }

    if($event_doc_list)
    {
        foreach($event_doc_list as $doc_dp_no => $event_doc_data)
        {
            foreach($event_doc_data as $event_doc)
            {
                $chk_add_where  = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE k_name_code IN('04003','04004','04005','04006','04015','04026','04012','04017','04048','04018','04034','04035','04050','04058','04060') AND display='1')";
                $chk_date_where = "";
                $chk_sum_having = " > 0";
                $chk_doc_gift   = isset($event_doc_option[$event_doc['event_no']]) ? $event_doc_option[$event_doc['event_no']] : "1092";
                switch($event_doc['event_no']){
                    case '1': # 닥터피엘 상시 이벤트(장바구니) 6만원
                        $chk_sum_having = " >= 60000";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date < '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '43': # 닥터피엘 상시 이벤트(장바구니) 7만원
                        $chk_add_where  = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE k_name_code IN('04003','04004','04005','04006','04015','04026','04012','04017','04048','04018','04034','04035','04050','04058','04060') AND display='1') AND task_req NOT LIKE '%땡철이마%'";
                        $chk_sum_having = " >= 70000";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date < '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '2': # 닥터피엘 상시 이벤트(칫솔)
                        $chk_sum_having = " >= 60000";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date < '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '3': # 닥터피엘 Special DA(장바구니)
                        $chk_sum_having = " > 0";
                        $chk_date_where = " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date <= '{$event_doc['e_date']}')";
                        break;
                    case '4': # 닥터피엘 Special DA(칫솔)
                        $chk_sum_having = " > 0";
                        $chk_date_where = " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date <= '{$event_doc['e_date']}')";
                        break;
                    case '8': # 닥터피엘 여행용 상시이벤트
                        $chk_add_where  = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE prd_code IN('DP0625','DP0624','DP0623','DP0554','DP540','DP539') AND display='1')";
                        $chk_sum_having = " >= 60000";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date < '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '10': # 닥터피엘 라이브커머스_202403
                        $chk_add_where  = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE prd_code IN('DP0566','DP0215','DP0231','DP0232','DP0641','DP0642','DP0554','DP0451','DPH011','DPH012','DPH013','DPH014','DPH015','DP0638','DP0315','DP0049','DP0190','DP0207','DP0209','DP0513','DP0514','DP0010','DP0029','DP0283','DP0390','DP0005','DP0608','DP0611','DP0191','DP0208','DP0210','DP0389','DP0387','DP0365','CUV0005','CUV0006','CUV0007','CUV0008','CUV0009','CUV0010','CUV0021','CUV0022','CUV0023','CUV0024','CUV0025','CUV0026','CUV0027','CUV0028','CUV0029','CUV0030','CUV0031','CUV0032','CUV0033','CUV0034','CUV0035') AND display='1')";
                        $chk_sum_having = " > 0";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date <= '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '14': # 닥터피엘 전체상품(장바구니)
                    case '23': # 닥터피엘 전체상품(스티커)
                        $chk_sum_having = " > 0";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date < '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '19': # 닥터피엘 라이브커머스_202405
                        $chk_add_where  = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE prd_code IN('DP0701','DP0215','DP0231','DP0232','DP0641','DP0642','DP0554','DP0451','DPH011','DPH012','DPH013','DPH014','DPH015','DP0638','DP0315','DP0049','DP0190','DP0207','DP0209','DP0513','DP0514','DP0010','DP0029','DP0283','DP0390','DP0005','DP0608','DP0611','DP0191','DP0208','DP0210','DP0389','DP0387','DP0365','CUV0005','CUV0006','CUV0007','CUV0008','CUV0009','CUV0010','CUV0021','CUV0022','CUV0023','CUV0024','CUV0025','CUV0026','CUV0027','CUV0028','CUV0029','CUV0030','CUV0031','CUV0032','CUV0033','CUV0034','CUV0035') AND display='1')";
                        $chk_sum_having = " > 0";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date <= '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '21': # 닥터피엘 에코피처형 이벤트
                        $chk_add_where  = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE k_name_code IN('04035') AND display='1')";
                        $chk_sum_having = " > 0";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date <= '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '24': # 닥터피엘 여행용 상시이벤트
                        $chk_add_where  = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE prd_code IN('DP0738','DP0740','DP0737','DP0739','DP0554','DP0623','DP0624','DP540','DP0625','DP539') AND display='1')";
                        $chk_sum_having = " > 0";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date < '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '27': # 닥터피엘 네쇼페 라이브커머스(칫솔 4P 증정)
                        $chk_add_where  = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE prd_code IN('DP0283','DP0208','DP0210','DP0207','DP0209','DP0388','DP0389','DP0386','DP0737','DP0739','DP0231','DP0232','CUV0003','CUV0002','CUV0001','CUV0010','CUV0009','CUV0008','CUV0007','CUV0006','CUV0005','CUV0035','CUV0034','CUV0033','CUV0032','CUV0031','CUV0030','CUV0029','CUV0028','CUV0027','CUV0026','CUV0025','CUV0024','CUV0023','CUV0022','CUV0021') AND display='1')";
                        $chk_sum_having = " >= 100000";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date <= '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '30': # 닥터피엘 카카오 쇼핑 라이브
                        $chk_add_where  = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE prd_code IN('DP0778','DP0779','DP0777','DP0215','DP0231','DP0232','DPH013','DPH014','DPH015','DP0769','DP0010','DP0049','DP0207','DP0209','DP0283','DP0190','DP0029','DP0737','DPH011','DPH012') AND display='1')";
                        $chk_sum_having = " > 0";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date <= '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '31': # 11월 닥터피엘 세일 페스타
                        $chk_sum_having = " >= 10000";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date < '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '37': # 닥터피엘 상시 이벤트(핫팩)
                        $chk_sum_having = " >= 50000";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date < '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                    case '39': # 닥터피엘 상시 이벤트(핫팩)
                        $chk_sum_having = " >= 70000";
                        $chk_date_where = (!empty($event_doc['e_date'])) ? " AND (wct.order_date >= '{$event_doc['s_date']}' AND wct.order_date < '{$event_doc['e_date']}')" : " AND (wct.order_date >= '{$event_doc['s_date']})'";
                        break;
                }

                $gift_order_sql = "
                    SELECT
                        wct.order_number,
                        wct.order_date,
                        SUM(wct.dp_price_vat) as total_price
                    FROM work_cms_temporary wct
                    WHERE wct.dp_c_no ='{$doc_dp_no}' {$chk_add_where}
                        {$chk_date_where}
                    GROUP BY order_number HAVING total_price {$chk_sum_having}
                ";
                $gift_order_query = mysqli_query($my_db, $gift_order_sql);
                while($gift_order_result = mysqli_fetch_assoc($gift_order_query))
                {
                    $gift_special_order_list[$gift_order_result['order_number']][$chk_doc_gift] = $chk_doc_gift;
                }
            }
        }
    }

    # 아이레놀 이벤트
    $gift_ilenol_order_list   = [];
    $gift_ilenol_product_info = array(
        '281'   => array('c_no' => '2402', 'c_name' => '프라이웰', 's_no' => '59', 'team'=> '00252'),
        '308'   => array('c_no' => '2388', 'c_name' => '아이레놀', 's_no' => '15', 'team'=> '00240'), # 아이레놀R
        '450'   => array('c_no' => '2402', 'c_name' => '프라이웰', 's_no' => '59', 'team'=> '00252'),
        '1101'  => array('c_no' => '4440', 'c_name' => '아이레놀R 페이스업리프터', 's_no' => '15', 'team'=> '00240'),
        '1103'  => array('c_no' => '4809', 'c_name' => '아이레놀(개기름컨트롤러)', 's_no' => '15', 'team'=> '00240'), # 아이레놀 개기름 컨트롤러 1EA
        '1194'  => array('c_no' => '4440', 'c_name' => '아이레놀R 페이스업리프터', 's_no' => '15', 'team'=> '00240'),
        '1197'  => array('c_no' => '4378', 'c_name' => '닥터부헌', 's_no' => '59', 'team'=> '00252'), # 페이셜 크림 150ML 1EA
        '1198'  => array('c_no' => '4378', 'c_name' => '닥터부헌', 's_no' => '59', 'team'=> '00252'), # 페이셜 로션 200ML 1EA
        '1327'  => array('c_no' => '2388', 'c_name' => '아이레놀', 's_no' => '15', 'team'=> '00240'), # 아이레놀 튜브 스퀴저
        '1336'  => array('c_no' => '4809', 'c_name' => '아이레놀(개기름컨트롤러)', 's_no' => '15', 'team'=> '00240'), # 아이레놀 개기름 컨트롤러 미니어쳐
        '1617'  => array('c_no' => '2388', 'c_name' => '아이레놀', 's_no' => '15', 'team'=> '00240'), # 아이레놀 샘플 4종 파우치
        '1772'  => array('c_no' => '2388', 'c_name' => '아이레놀', 's_no' => '15', 'team'=> '00240'), # 아이레놀 믹싱팔레트 1EA
        '1885'  => array('c_no' => '5759', 'c_name' => '아이레놀 쌩얼보정 선크림', 's_no' => '15', 'team'=> '00240'), # 아이레놀 선케어 3종 파우치
        '2018'  => array('c_no' => '5759', 'c_name' => '아이레놀 쌩얼보정 선크림', 's_no' => '15', 'team'=> '00240'),  # 아이레놀 선케어 4종 파우치
        '1898'  => array('c_no' => '5932', 'c_name' => '아이레놀 생병풀 선크림', 's_no' => '15', 'team'=> '00240'), # 아이레놀 생병풀 수분가득 선세럼 1EA
        '2008'  => array('c_no' => '2388', 'c_name' => '아이레놀', 's_no' => '15', 'team'=> '00240')  # 아이레놀 쇼핑백(20cm)
    );

    # 이벤트 테이블 아이레놀 데이터 가져오기
    $event_eye_sql      = "SELECT * FROM advertising_event WHERE event_no IN(7,11,18) AND is_active='1' AND event_e_date >= '{$event_limit_e_date}' ORDER BY event_no, event_s_date ASC";
    $event_eye_query    = mysqli_query($my_db, $event_eye_sql);
    $event_eye_list     = [];
    while($event_eye_result = mysqli_fetch_assoc($event_eye_query))
    {
        $eye_dp_list = explode(",", $event_eye_result['dp_list']);
        foreach($eye_dp_list as $eye_dp_val) {
            $event_eye_list[$eye_dp_val][] = array("event_no" => $event_eye_result['event_no'], "s_date" => $event_eye_result['event_s_date'], "e_date" => $event_eye_result['event_e_date']);
        }
    }

    if($event_eye_list)
    {
        foreach($event_eye_list as $eye_dp_no => $event_eye_data)
        {
            foreach ($event_eye_data as $event_eye)
            {
                $chk_add_where      = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE k_name_code IN('04010','04021','04036','04054') AND display='1')";
                $chk_date_where     = "";
                $chk_having_where   = "";
                $chk_gift_price_1   = 0;
                $chk_gift_price_2   = 0;
                $chk_gift_1         = '';
                $chk_gift_2         = '';
                switch($event_eye['event_no']){
                    case '7':
                        $chk_gift_price_2   = 30000;
                        $chk_gift_2         = '1336';
                        $chk_date_where     = " AND (wct.order_date >= '{$event_eye['s_date']}' AND wct.order_date < '{$event_eye['e_date']}')";
                        $chk_having_where   = "total_price >= {$chk_gift_price_2}";
                        break;
                    case '11':
                        $chk_gift_price_1   = 50000;
                        $chk_gift_price_2   = 30000;
                        $chk_gift_1         = '1198';
                        $chk_gift_2         = '1336';
                        $chk_date_where     = " AND (wct.order_date >= '{$event_eye['s_date']}' AND wct.order_date < '{$event_eye['e_date']}')";
                        $chk_having_where   = "total_price >= {$chk_gift_price_2}";
                        break;
                    case '18':
                        $chk_add_where      = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE k_name_code IN('04010','04021','04036') AND display='1')";
                        $chk_gift_price_2   = 0;
                        $chk_gift_2         = '1336';
                        $chk_date_where     = " AND (wct.order_date >= '{$event_eye['s_date']}' AND wct.order_date < '{$event_eye['e_date']}')";
                        $chk_having_where   = "total_price > {$chk_gift_price_2}";
                        break;
                }

                $gift_ilenol_order_sql   = "
                    SELECT
                        wct.order_number, 
                        wct.order_date,
                        wct.dp_c_no,
                        SUM(wct.dp_price_vat) as total_price
                    FROM work_cms_temporary wct
                    WHERE  wct.dp_c_no = '{$eye_dp_no}' {$chk_add_where}
                     {$chk_date_where}
                    GROUP BY order_number HAVING {$chk_having_where}
                ";
                $gift_ilenol_order_query = mysqli_query($my_db, $gift_ilenol_order_sql);
                while($gift_ilenol_order_result = mysqli_fetch_assoc($gift_ilenol_order_query))
                {
                    if($chk_gift_price_1 != 0 && ($gift_ilenol_order_result['total_price'] >= $chk_gift_price_1 && !empty($chk_gift_1))){
                        $gift_ilenol_order_list[$gift_ilenol_order_result['order_number']]["main"] = $chk_gift_1;
                    }elseif($gift_ilenol_order_result['total_price'] >= $chk_gift_price_2 && !empty($chk_gift_2)){
                        $gift_ilenol_order_list[$gift_ilenol_order_result['order_number']]["main"] = $chk_gift_2;
                    }
                }
            }
        }
    }

    # 이벤트 아이레놀 파우치 관련
    $event_eye_base_sql     = "SELECT * FROM advertising_event WHERE event_no IN(25,26,29,33,34,35,38,41,42,44) AND is_active='1' AND event_e_date >= '{$event_limit_e_date}' ORDER BY event_no, event_s_date ASC";
    $event_eye_base_query   = mysqli_query($my_db, $event_eye_base_sql);
    $event_eye_base_list    = [];
    while($event_eye_base_result = mysqli_fetch_assoc($event_eye_base_query))
    {
        $eye_base_dp_list = explode(",", $event_eye_base_result['dp_list']);
        foreach($eye_base_dp_list as $eye_base_dp_val) {
            $event_eye_base_list[$eye_base_dp_val][] = array("event_no" => $event_eye_base_result['event_no'], "s_date" => $event_eye_base_result['event_s_date'], "e_date" => $event_eye_base_result['event_e_date']);
        }
    }

    # 아이레놀 파우치
    $gift_ilenol_base_order_list  = [];
    if($event_eye_base_list)
    {
        foreach($event_eye_base_list as $eye_base_dp_no => $event_eye_base_data)
        {
            foreach ($event_eye_base_data as $event_eye_base)
            {
                $chk_add_where  = "wct.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='04027' AND k.display='1') AND p.display='1') AND wct.unit_price > 0 AND (wct.order_date >= '{$event_eye_base['s_date']}' AND wct.order_date < '{$event_eye_base['e_date']}')";
                switch($event_eye_base['event_no'])
                {
                    case "25":
                        $gift_ilenol_base_order_sql = "
                            SELECT
                                wct.order_number, 
                                wct.order_date,
                                wct.dp_c_no,
                                SUM(wct.dp_price_vat) as total_price
                            FROM work_cms_temporary wct
                            WHERE {$chk_add_where} AND wct.dp_c_no='{$eye_base_dp_no}'
                            GROUP BY order_number HAVING total_price > 0
                        ";
                        $gift_ilenol_base_order_query = mysqli_query($my_db, $gift_ilenol_base_order_sql);
                        while($gift_ilenol_base_order_result = mysqli_fetch_assoc($gift_ilenol_base_order_query))
                        {
                            $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1885]++;

                            if($gift_ilenol_base_order_result['total_price'] >= 30000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1617]++;
                            }

                            if($gift_ilenol_base_order_result['total_price'] >= 50000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1336]++;
                            }
                        }
                        break;

                    case "26":
                        $gift_ilenol_base_order_sql = "
                            SELECT
                                wct.order_number, 
                                wct.order_date,
                                SUM(wct.dp_price_vat) as total_price,
                                (SELECT COUNT(t_no) as cnt FROM work_cms_temporary sub WHERE sub.order_number=wct.order_number AND sub.prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.option_no IN('26','40','62','329') AND pcr.display='1') AND sub.unit_price > 0) as base_cnt,
                                (SELECT COUNT(t_no) as cnt FROM work_cms_temporary sub WHERE sub.order_number=wct.order_number AND sub.prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.option_no IN('447') AND pcr.display='1') AND sub.unit_price > 0) as serum_cnt,
                                (SELECT COUNT(t_no) as cnt FROM work_cms_temporary sub WHERE sub.order_number=wct.order_number AND sub.prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.option_no='218' AND pcr.display='1') AND sub.unit_price > 0) as con_cnt 
                            FROM work_cms_temporary wct
                            WHERE {$chk_add_where} AND wct.dp_c_no='{$eye_base_dp_no}'
                            GROUP BY order_number HAVING total_price > 0
                        ";
                        $gift_ilenol_base_order_query = mysqli_query($my_db, $gift_ilenol_base_order_sql);
                        while($gift_ilenol_base_order_result = mysqli_fetch_assoc($gift_ilenol_base_order_query))
                        {
                            $base_item_cnt      = $gift_ilenol_base_order_result['base_cnt'];
                            $serum_item_cnt     = $gift_ilenol_base_order_result['serum_cnt'];
                            $control_item_cnt   = $gift_ilenol_base_order_result['con_cnt'];

                            if($serum_item_cnt > 0){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1885]++;
                            }

                            if($base_item_cnt > 0){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1617]++;
                            }

                            if($control_item_cnt > 0){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1336]++;
                            }
                        }
                        break;

                    case "29":
                        $gift_ilenol_base_order_sql = "
                            SELECT
                                wct.order_number, 
                                wct.order_date,
                                wct.dp_c_no,
                                SUM(wct.dp_price_vat) as total_price
                            FROM work_cms_temporary wct
                            WHERE {$chk_add_where} AND wct.dp_c_no='{$eye_base_dp_no}'
                            GROUP BY order_number HAVING total_price > 0
                        ";
                        $gift_ilenol_base_order_query = mysqli_query($my_db, $gift_ilenol_base_order_sql);
                        while($gift_ilenol_base_order_result = mysqli_fetch_assoc($gift_ilenol_base_order_query))
                        {
                            $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1617]++;
                            $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1885]++;

                            if($gift_ilenol_base_order_result['total_price'] >= 30000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1336]++;
                            }

                            if($gift_ilenol_base_order_result['total_price'] >= 50000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1898]++;
                            }
                        }
                        break;

                    case "33":
                        $gift_ilenol_base_order_sql = "
                            SELECT
                                wct.order_number, 
                                wct.order_date,
                                wct.dp_c_no,
                                SUM(wct.dp_price_vat) as total_price
                            FROM work_cms_temporary wct
                            WHERE {$chk_add_where} AND wct.dp_c_no='{$eye_base_dp_no}'
                            GROUP BY order_number HAVING total_price > 0
                        ";
                        $gift_ilenol_base_order_query = mysqli_query($my_db, $gift_ilenol_base_order_sql);
                        while($gift_ilenol_base_order_result = mysqli_fetch_assoc($gift_ilenol_base_order_query))
                        {
                            if($gift_ilenol_base_order_result['total_price'] >= 40000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1898]++;
                            }
                            elseif($gift_ilenol_base_order_result['total_price'] >= 30000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1336]++;
                            }else{
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1617]++;
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1885]++;
                            }
                        }
                        break;

                    case "34":
                        $gift_ilenol_base_order_sql = "
                            SELECT
                                wct.order_number, 
                                wct.order_date,
                                SUM(wct.dp_price_vat) as total_price,
                                (SELECT COUNT(t_no) as cnt FROM work_cms_temporary sub WHERE sub.order_number=wct.order_number AND sub.prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.option_no IN('26','40','62','329','218') AND pcr.display='1') AND sub.unit_price > 0) as base_cnt,
                                (SELECT COUNT(t_no) as cnt FROM work_cms_temporary sub WHERE sub.order_number=wct.order_number AND sub.prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.option_no IN('427','428','447') AND pcr.display='1') AND sub.unit_price > 0) as sun_cnt 
                            FROM work_cms_temporary wct
                            WHERE {$chk_add_where} AND wct.dp_c_no='{$eye_base_dp_no}'
                            GROUP BY order_number HAVING total_price > 0
                        ";
                        $gift_ilenol_base_order_query = mysqli_query($my_db, $gift_ilenol_base_order_sql);
                        while($gift_ilenol_base_order_result = mysqli_fetch_assoc($gift_ilenol_base_order_query))
                        {
                            $base_item_cnt  = $gift_ilenol_base_order_result['base_cnt'];
                            $sun_item_cnt   = $gift_ilenol_base_order_result['sun_cnt'];

                            if($sun_item_cnt > 0){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1885]++;
                            }

                            if($base_item_cnt > 0){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1617]++;
                            }
                        }
                        break;

                    case "35":
                        $gift_ilenol_base_order_sql = "
                            SELECT
                                wct.order_number, 
                                wct.order_date,
                                wct.dp_c_no,
                                SUM(wct.dp_price_vat) as total_price
                            FROM work_cms_temporary wct
                            WHERE {$chk_add_where} AND wct.dp_c_no='{$eye_base_dp_no}'
                            GROUP BY order_number HAVING total_price >= 30000
                        ";
                        $gift_ilenol_base_order_query = mysqli_query($my_db, $gift_ilenol_base_order_sql);
                        while($gift_ilenol_base_order_result = mysqli_fetch_assoc($gift_ilenol_base_order_query))
                        {
                            if($gift_ilenol_base_order_result['total_price'] >= 70000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1898]++;
                            }
                            elseif($gift_ilenol_base_order_result['total_price'] >= 50000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1617]++;
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1885]++;
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1336]++;
                            }else{
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1885]++;
                            }
                        }
                        break;

                    case "38":
                        $gift_ilenol_base_order_sql = "
                            SELECT
                                wct.order_number, 
                                wct.order_date,
                                wct.dp_c_no,
                                SUM(wct.dp_price_vat) as total_price
                            FROM work_cms_temporary wct
                            WHERE {$chk_add_where} AND wct.dp_c_no='{$eye_base_dp_no}'
                            GROUP BY order_number HAVING total_price > 0
                        ";
                        $gift_ilenol_base_order_query = mysqli_query($my_db, $gift_ilenol_base_order_sql);
                        while($gift_ilenol_base_order_result = mysqli_fetch_assoc($gift_ilenol_base_order_query))
                        {
                            $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1617]++;

                            if($gift_ilenol_base_order_result['total_price'] >= 30000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1885]++;
                            }

                            if($gift_ilenol_base_order_result['total_price'] >= 50000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1336]++;
                            }

                        }
                        break;

                    case "41":
                        $gift_ilenol_base_order_sql = "
                            SELECT
                                wct.order_number, 
                                wct.order_date,
                                wct.dp_c_no,
                                SUM(wct.dp_price_vat) as total_price
                            FROM work_cms_temporary wct
                            WHERE {$chk_add_where} AND wct.dp_c_no='{$eye_base_dp_no}'
                            GROUP BY order_number HAVING total_price > 0
                        ";
                        $gift_ilenol_base_order_query = mysqli_query($my_db, $gift_ilenol_base_order_sql);
                        while($gift_ilenol_base_order_result = mysqli_fetch_assoc($gift_ilenol_base_order_query))
                        {
                            $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1885]++;

                            if($gift_ilenol_base_order_result['total_price'] >= 30000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1617]++;
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][2008]++;
                            }

                            if($gift_ilenol_base_order_result['total_price'] >= 50000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1336]++;
                            }

                        }
                        break;

                    case "42":
                        $gift_ilenol_base_order_sql = "
                            SELECT
                                wct.order_number, 
                                wct.order_date,
                                wct.dp_c_no,
                                SUM(wct.dp_price_vat) as total_price
                            FROM work_cms_temporary wct
                            WHERE {$chk_add_where} AND wct.dp_c_no='{$eye_base_dp_no}'
                            GROUP BY order_number HAVING total_price > 0
                        ";
                        $gift_ilenol_base_order_query = mysqli_query($my_db, $gift_ilenol_base_order_sql);
                        while($gift_ilenol_base_order_result = mysqli_fetch_assoc($gift_ilenol_base_order_query))
                        {
                            $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][2018]++;

                            if($gift_ilenol_base_order_result['total_price'] >= 30000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][2018]++;
                            }

                            if($gift_ilenol_base_order_result['total_price'] >= 50000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1336]++;
                            }

                        }
                        break;
                    case "44":
                        $gift_ilenol_base_order_sql = "
                            SELECT
                                wct.order_number, 
                                wct.order_date,
                                wct.dp_c_no,
                                SUM(wct.dp_price_vat) as total_price
                            FROM work_cms_temporary wct
                            WHERE {$chk_add_where} AND wct.dp_c_no='{$eye_base_dp_no}'
                            GROUP BY order_number HAVING total_price > 0
                        ";
                        $gift_ilenol_base_order_query = mysqli_query($my_db, $gift_ilenol_base_order_sql);
                        while($gift_ilenol_base_order_result = mysqli_fetch_assoc($gift_ilenol_base_order_query))
                        {
                            $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][2018]++;

                            if($gift_ilenol_base_order_result['total_price'] >= 30000){
                                $gift_ilenol_base_order_list[$gift_ilenol_base_order_result['order_number']][1336]++;
                            }
                        }
                        break;
                }
            }
        }
    }

    #OURPICK 파우치 증정
    $gift_ilenol_our_order_sql = "
        SELECT
            wct.order_number, 
            wct.order_date,
            wct.dp_c_no,
            SUM(wct.dp_price_vat) as total_price
        FROM work_cms_temporary wct
        WHERE wct.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='04027' AND k.display='1') AND p.display='1') AND wct.unit_price > 0 
          AND (wct.order_date >= '2025-02-19 20:00:00' AND wct.order_date < '2025-02-21 00:00:00') AND wct.dp_c_no='5847'
        GROUP BY order_number HAVING total_price > 0
    ";
    $gift_ilenol_our_order_query = mysqli_query($my_db, $gift_ilenol_our_order_sql);
    while($gift_ilenol_our_order = mysqli_fetch_assoc($gift_ilenol_our_order_query)){
        $gift_ilenol_base_order_list[$gift_ilenol_our_order['order_number']][2018]++;
    }

    # 누잠 이벤트
    $gift_nuzam_order_list      = [];
    $gift_nuzam_product_info    = array(
        '1027' => array('c_no' => '2827', 'c_name' => '누잠매트리스', 's_no' => '4', 'team'=> '00241'),
        '1028' => array('c_no' => '2827', 'c_name' => '누잠매트리스', 's_no' => '4', 'team'=> '00241'),
        '1167' => array('c_no' => '2827', 'c_name' => '누잠매트리스', 's_no' => '4', 'team'=> '00241'),
        '1600' => array('c_no' => '2827', 'c_name' => '누잠매트리스', 's_no' => '4', 'team'=> '00241'),
        '1625' => array('c_no' => '5415', 'c_name' => '누잠 슬립미스트', 's_no' => '4', 'team'=> '00241'),
    );

    # 누잠 Special DA 이벤트 제외
    $event_nuzam_special_sql    = "SELECT * FROM advertising_event WHERE event_no IN(9,32) AND is_active='1' AND (event_e_date IS NULL OR event_e_date >= '{$event_limit_e_date}') ORDER BY event_no, event_s_date ASC";
    $event_nuzam_special_query  = mysqli_query($my_db, $event_nuzam_special_sql);
    $event_nuzam_special_list   = [];
    while($event_nuzam_special_result = mysqli_fetch_assoc($event_nuzam_special_query))
    {
        $nuzam_special_dp_list = explode(",", $event_nuzam_special_result['dp_list']);
        foreach($nuzam_special_dp_list as $nuzam_special_dp_val) {
            $event_nuzam_special_list[$nuzam_special_dp_val][] = array("event_no" => $event_nuzam_special_result['event_no'], "s_date" => $event_nuzam_special_result['event_s_date'], "e_date" => $event_nuzam_special_result['event_e_date']);
        }
    }

    $nuzam_special_ord_list = [];
    if($event_nuzam_special_list)
    {
        foreach($event_nuzam_special_list as $nuzam_special_dp_no => $event_nuzam_special_data)
        {
            foreach($event_nuzam_special_data as $event_nuzam_special)
            {
                $gift_nuzam_special_order_sql = "
                     SELECT
                        wct.order_number,
                        wct.order_date,
                        SUM(wct.dp_price_vat) as total_price
                    FROM work_cms_temporary wct
                    WHERE wct.dp_c_no ='{$nuzam_special_dp_no}' AND (wct.order_date >= '{$event_nuzam_special['s_date']}' AND wct.order_date < '{$event_nuzam_special['e_date']}')
                    AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='04022') AND display='1')
                    GROUP BY order_number HAVING total_price > 0
                ";
                $gift_nuzam_special_order_query = mysqli_query($my_db, $gift_nuzam_special_order_sql);
                while($gift_nuzam_special_order = mysqli_fetch_assoc($gift_nuzam_special_order_query))
                {
                    $nuzam_special_ord_list[$gift_nuzam_special_order['order_number']] = $gift_nuzam_special_order['order_number'];
                }
            }
        }
    }

    # 이벤트 테이블 누잠 데이터 가져오기
    $event_nuzam_sql      = "SELECT * FROM advertising_event WHERE event_no IN(5,6,12,13,15,16,17,20,22,40) AND is_active='1' AND (event_e_date IS NULL OR event_e_date >= '{$event_limit_e_date}') ORDER BY event_no, event_s_date ASC";
    $event_nuzam_query    = mysqli_query($my_db, $event_nuzam_sql);
    $event_nuzam_list     = [];
    while($event_nuzam_result = mysqli_fetch_assoc($event_nuzam_query))
    {
        $nuzam_dp_list = explode(",", $event_nuzam_result['dp_list']);
        foreach($nuzam_dp_list as $nuzam_dp_val) {
            $event_nuzam_list[$nuzam_dp_val][] = array("event_no" => $event_nuzam_result['event_no'], "s_date" => $event_nuzam_result['event_s_date'], "e_date" => $event_nuzam_result['event_e_date']);
        }
    }

    if($event_nuzam_list)
    {
        foreach($event_nuzam_list as $nuzam_dp_no => $event_nuzam_data)
        {
            foreach($event_nuzam_data as $event_nuzam)
            {
                $chk_add_where      = " AND wct.prd_no IN(SELECT prd_no FROM product_cms WHERE k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='04022') AND display='1')";
                $chk_date_where     = "";
                $chk_gift_price_1   = 0;
                $chk_gift_price_2   = 0;
                $chk_gift_1         = '';
                $chk_gift_2         = '';
                switch($event_nuzam['event_no']){
                    case '5':
                        $chk_date_where     = (!empty($event_nuzam['e_date'])) ? " AND (wct.order_date >= '{$event_nuzam['s_date']}' AND wct.order_date < '{$event_nuzam['e_date']}')" : " AND (wct.order_date >= '{$event_nuzam['s_date']}')";
                        $chk_gift_price_1   = 240000;
                        $chk_gift_price_2   = 180000;
                        $chk_gift_1         = '1625';
                        $chk_gift_2         = '1600';
                        break;
                    case '6':
                        $chk_date_where     = " AND (wct.order_date >= '{$event_nuzam['s_date']}' AND wct.order_date <= '{$event_nuzam['e_date']}')";
                        $chk_gift_price_1   = 230000;
                        $chk_gift_price_2   = 170000;
                        $chk_gift_1         = '1625';
                        $chk_gift_2         = '1600';
                        break;
                    case '12':
                        $chk_add_where      = " 
                            AND (
                                    wct.prd_no IN(SELECT prd_no FROM product_cms WHERE prd_code IN('NU0049','NU0050','NU0051','NU0052','NU0053','NU0054','NU0092','NU0094','NU0110','NU0250','NU0324','NU0111','NU0251','NU0325','NU0112','NU0252','NU0326','NU0113','NU0253','NU0327','BE0420','BE0421','BE0422','BE0423','NU0114','NU0115','NU0116','NU0117','NU0118','NU0119','NU0120','NU0121','NU0248') AND display='1')
                                    OR wct.prd_no IN(SELECT combined_product FROM product_cms WHERE prd_code IN('NU0049','NU0050','NU0051','NU0052','NU0053','NU0054','NU0092','NU0094','NU0110','NU0250','NU0324','NU0111','NU0251','NU0325','NU0112','NU0252','NU0326','NU0113','NU0253','NU0327','BE0420','BE0421','BE0422','BE0423','NU0114','NU0115','NU0116','NU0117','NU0118','NU0119','NU0120','NU0121','NU0248') AND display='1' AND combined_product != '' AND combined_product IS NOT NULL)
                                )
                        ";
                        $chk_date_where     = " AND (wct.order_date >= '{$event_nuzam['s_date']}' AND wct.order_date < '{$event_nuzam['e_date']}')";
                        $chk_gift_price_1   = 100000;
                        $chk_gift_price_2   = 100000;
                        $chk_gift_1         = '';
                        $chk_gift_2         = '1600';
                        break;
                    case '13':
                        $chk_add_where      = " 
                            AND (
                                    wct.prd_no IN(SELECT prd_no FROM product_cms WHERE prd_code IN('NU0049','NU0050','NU0051','NU0052','NU0053','NU0054','NU0092','NU0094','NU0110','NU0250','NU0324','NU0111','NU0251','NU0325','NU0112','NU0252','NU0326','NU0113','NU0253','NU0327','BE0420','BE0421','BE0422','BE0423','NU0114','NU0115','NU0116','NU0117','NU0118','NU0119','NU0120','NU0121','NU0248') AND display='1')
                                    OR wct.prd_no IN(SELECT combined_product FROM product_cms WHERE prd_code IN('NU0049','NU0050','NU0051','NU0052','NU0053','NU0054','NU0092','NU0094','NU0110','NU0250','NU0324','NU0111','NU0251','NU0325','NU0112','NU0252','NU0326','NU0113','NU0253','NU0327','BE0420','BE0421','BE0422','BE0423','NU0114','NU0115','NU0116','NU0117','NU0118','NU0119','NU0120','NU0121','NU0248') AND display='1' AND combined_product != '' AND combined_product IS NOT NULL)
                                )
                        ";
                        $chk_date_where     = " AND (wct.order_date >= '{$event_nuzam['s_date']}' AND wct.order_date < '{$event_nuzam['e_date']}')";
                        $chk_gift_price_1   = 160000;
                        $chk_gift_price_2   = 100000;
                        $chk_gift_1         = '1600';
                        $chk_gift_2         = '1167';
                        break;
                    case '15':
                        $chk_add_where      = " 
                            AND (
                                    wct.prd_no IN(SELECT prd_no FROM product_cms WHERE prd_code IN('NU0049','NU0050','NU0051','NU0052','NU0053','NU0054','NU0092','NU0094','NU0110','NU0250','NU0324','NU0111','NU0251','NU0325','NU0112','NU0252','NU0326','NU0113','NU0253','NU0327','BE0420','BE0421','BE0422','BE0423','NU0114','NU0115','NU0116','NU0117','NU0118','NU0119','NU0120','NU0121','NU0248') AND display='1')
                                    OR wct.prd_no IN(SELECT combined_product FROM product_cms WHERE prd_code IN('NU0049','NU0050','NU0051','NU0052','NU0053','NU0054','NU0092','NU0094','NU0110','NU0250','NU0324','NU0111','NU0251','NU0325','NU0112','NU0252','NU0326','NU0113','NU0253','NU0327','BE0420','BE0421','BE0422','BE0423','NU0114','NU0115','NU0116','NU0117','NU0118','NU0119','NU0120','NU0121','NU0248') AND display='1' AND combined_product != '' AND combined_product IS NOT NULL)
                                )
                        ";
                        $chk_date_where     = " AND (wct.order_date >= '{$event_nuzam['s_date']}' AND wct.order_date < '{$event_nuzam['e_date']}')";
                        $chk_gift_price_1   = 150000;
                        $chk_gift_price_2   = 150000;
                        $chk_gift_1         = '';
                        $chk_gift_2         = '1600';
                        break;
                    case '16':
                        $chk_add_where      = " 
                            AND (
                                    wct.prd_no IN(SELECT prd_no FROM product_cms WHERE prd_code IN('NU0049','NU0050','NU0051','NU0052','NU0053','NU0054','NU0118','NU0119','NU0120','NU0121','BE0420','BE0421','BE0422','BE0423','NU0114','NU0115','NU0116','NU0117','BE0232','BE0233','NU0234','NU0096') AND display='1')
                                    OR wct.prd_no IN(SELECT combined_product FROM product_cms WHERE prd_code IN('NU0049','NU0050','NU0051','NU0052','NU0053','NU0054','NU0118','NU0119','NU0120','NU0121','BE0420','BE0421','BE0422','BE0423','NU0114','NU0115','NU0116','NU0117','BE0232','BE0233','NU0234','NU0096') AND display='1' AND combined_product != '' AND combined_product IS NOT NULL)
                                )
                        ";
                        $chk_date_where     = " AND (wct.order_date >= '{$event_nuzam['s_date']}' AND wct.order_date < '{$event_nuzam['e_date']}')";
                        $chk_gift_price_1   = 180000;
                        $chk_gift_price_2   = 100000;
                        $chk_gift_1         = '1625';
                        $chk_gift_2         = '1600';
                        break;
                    case '17':
                        $chk_date_where     = " AND (wct.order_date >= '{$event_nuzam['s_date']}' AND wct.order_date < '{$event_nuzam['e_date']}')";
                        $chk_gift_price_1   = 170000;
                        $chk_gift_price_2   = 100000;
                        $chk_gift_1         = '1625';
                        $chk_gift_2         = '1600';
                        break;
                    case '20':
                        $chk_date_where     = " AND (wct.order_date >= '{$event_nuzam['s_date']}' AND wct.order_date < '{$event_nuzam['e_date']}')";
                        $chk_gift_price_1   = 100000;
                        $chk_gift_price_2   = 100000;
                        $chk_gift_1         = '';
                        $chk_gift_2         = '1600';
                        break;
                    case '22':
                        $chk_date_where     = " AND (wct.order_date >= '{$event_nuzam['s_date']}' AND wct.order_date <= '{$event_nuzam['e_date']}')";
                        $chk_gift_price_1   = 210000;
                        $chk_gift_price_2   = 120000;
                        $chk_gift_1         = '1625';
                        $chk_gift_2         = '1600';
                        break;
                    case '40':
                        $chk_date_where     = " AND (wct.order_date >= '{$event_nuzam['s_date']}' AND wct.order_date < '{$event_nuzam['e_date']}')";
                        $chk_gift_price_1   = 300000;
                        $chk_gift_price_2   = 200000;
                        $chk_gift_1         = '1625';
                        $chk_gift_2         = '1600';
                        break;
                }

                $gift_nuzam_order_sql = "
                     SELECT
                        wct.order_number,
                        wct.order_date,
                        SUM(wct.dp_price_vat) as total_price
                    FROM work_cms_temporary wct
                    WHERE wct.dp_c_no ='{$nuzam_dp_no}' {$chk_add_where}
                        {$chk_date_where}
                    GROUP BY order_number HAVING total_price >= {$chk_gift_price_2}
                ";
                $gift_nuzam_order_query = mysqli_query($my_db, $gift_nuzam_order_sql);
                while($gift_nuzam_order = mysqli_fetch_assoc($gift_nuzam_order_query))
                {
                    if(isset($nuzam_special_ord_list[$gift_nuzam_order['order_number']])){
                        continue;
                    }

                    if($gift_nuzam_order['total_price'] >= $chk_gift_price_1 && !empty($chk_gift_1)){
                        $gift_nuzam_order_list[$gift_nuzam_order['order_number']] = $chk_gift_1;
                    }
                    elseif($gift_nuzam_order['total_price'] >= $chk_gift_price_2  && !empty($chk_gift_2)){
                        $gift_nuzam_order_list[$gift_nuzam_order['order_number']] = $chk_gift_2;
                    }
                }
            }
        }
    }

    # 누잠 경추심 솜, 커버 처리 28일 부자재 처리
    $chk_only_pillow_sub_sql    = "
        SELECT 
            wct.t_no            
        FROM work_cms_temporary wct
        WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_temporary WHERE prd_no IN(SELECT p.prd_no FROM product_cms_relation p WHERE p.option_no IN(459) AND p.display='1') AND is_wait='1')
        AND (task_req LIKE '%28 출고%' OR prd_no IN(2244,2245,2246,2235)) AND unit_price > 0 AND is_wait='0'
    ";
    $chk_only_pillow_sub_query  = mysqli_query($my_db, $chk_only_pillow_sub_sql);
    while($chk_only_pillow_sub = mysqli_fetch_assoc($chk_only_pillow_sub_query)){
        $upd_pillow_sql = "UPDATE work_cms_temporary SET is_wait=1 WHERE t_no='{$chk_only_pillow_sub['t_no']}'";
        mysqli_query($my_db, $upd_pillow_sql);
    }

    # 베개만 구매했는지 체크
    $chk_only_pillow_wait_sql    = "
        SELECT 
            wct.order_number,
            (SELECT COUNT(sub.t_no) FROM work_cms_temporary sub WHERE sub.is_wait='0' AND sub.unit_price > 0 AND sub.order_number=wct.order_number AND sub.prd_no NOT IN(1166)) AS exp_cnt
        FROM work_cms_temporary wct
        WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_temporary WHERE prd_no IN(SELECT p.prd_no FROM product_cms_relation p WHERE p.option_no IN(459) AND p.display='1') AND is_wait='1')
        GROUP BY order_number
    ";
    $chk_only_pillow_wait_query  = mysqli_query($my_db, $chk_only_pillow_wait_sql);
    while($chk_only_pillow_wait = mysqli_fetch_assoc($chk_only_pillow_wait_query)){
        if($chk_only_pillow_wait['exp_cnt'] == 0){
            $upd_chk_sql = "UPDATE work_cms_temporary SET is_wait=1 WHERE order_number='{$chk_only_pillow_wait['order_number']}'";
            mysqli_query($my_db, $upd_chk_sql);
        }
    }


    /**
    # 더블업 Q 구매관련 사계절커버 처리
    $chk_only_double_cover_sql    = "
        SELECT 
            wct.t_no            
        FROM work_cms_temporary wct
        WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_temporary WHERE prd_no IN(SELECT p.prd_no FROM product_cms_relation p WHERE p.option_no IN(473,475) AND p.display='1') AND is_wait='1')
        AND prd_no IN(1166,1600,1258,1631,1790) AND unit_price > 0        
    ";
    $chk_only_double_cover_query  = mysqli_query($my_db, $chk_only_double_cover_sql);
    while($chk_only_double_cover = mysqli_fetch_assoc($chk_only_double_cover_query)){
        $upd_cover_sql = "UPDATE work_cms_temporary SET is_wait=1 WHERE t_no='{$chk_only_double_cover['t_no']}'";
        mysqli_query($my_db, $upd_cover_sql);
    }

    # 더블업 Q만 구매했는지 체크
    $chk_only_double_wait_sql    = "
        SELECT 
            wct.order_number,
            (SELECT COUNT(sub.t_no) FROM work_cms_temporary sub WHERE sub.is_wait='0' AND sub.unit_price > 0 AND sub.order_number=wct.order_number) AS exp_cnt
        FROM work_cms_temporary wct
        WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_temporary WHERE prd_no IN(SELECT p.prd_no FROM product_cms_relation p WHERE p.option_no IN(473,475) AND p.display='1') AND is_wait='1')
        GROUP BY order_number
    ";
    $chk_only_double_wait_query  = mysqli_query($my_db, $chk_only_double_wait_sql);
    while($chk_only_double_wait = mysqli_fetch_assoc($chk_only_double_wait_query)){
        if($chk_only_double_wait['exp_cnt'] == 0){
            $upd_chk_sql = "UPDATE work_cms_temporary SET is_wait=1 WHERE order_number='{$chk_only_double_wait['order_number']}'";
            mysqli_query($my_db, $upd_chk_sql);
        }
    }
    */

    # 사용 리스트
    $additional_list    = [];
    $product_ord_list   = [];
    $coupon_price_list  = [];
    $chk_coupon_list    = [];
    $wait_order_list    = [];
    $add_order_list     = [];

    # 주문번호 체크하면서 등록하기
    $sel_order_sql   = "SELECT * FROM work_cms_temporary wct ORDER BY wct.t_no ASC";
    $sel_order_query = mysqli_query($my_db, $sel_order_sql);
    while($data = mysqli_fetch_assoc($sel_order_query))
    {
        $t_no           = $data['t_no'];
        $order_number   = isset($data['order_number']) ? $data['order_number'] : "";
        $prd_no         = isset($data['prd_no']) ? $data['prd_no'] : "";
        $quantity       = isset($data['quantity']) ? $data['quantity'] : 1;
        $stock_date     = isset($data['stock_date']) ? $data['stock_date'] : date('Y-m-d');
        $task_req       = isset($data['task_req']) ? addslashes($data['task_req']) : "";
        $recipient      = isset($data['recipient']) ? addslashes($data['recipient']) : "";
        $recipient_addr = isset($data['recipient_addr']) ? addslashes($data['recipient_addr']) : "";
        $delivery_memo  = isset($data['delivery_memo']) ? addslashes($data['delivery_memo']) : "";
        $manager_memo   = isset($data['manager_memo']) ? addslashes($data['manager_memo']) : "";
        $notice         = isset($data['notice']) ? addslashes($data['notice']) : "";
        $payment_code   = isset($data['payment_code']) ? addslashes($data['payment_code']) : "";
        $payment_type   = isset($data['payment_type']) ? addslashes($data['payment_type']) : "";
        $order_date     = isset($data['order_date']) ? $data['order_date'] : "";
        $account        = isset($data['account']) ? $data['account'] : "";
        $dp_c_no        = isset($data['dp_c_no']) ? $data['dp_c_no'] : "";
        $chk_is_wait    = isset($data['is_wait']) ? $data['is_wait'] : "";
        $delivery_state = 1;

        if($data['coupon_price'] != 0)
        {
            $data['shop_ord_no'] = empty($data['shop_ord_no']) ? $order_number."_".$prd_no : $data['shop_ord_no'];

            if(!isset($chk_coupon_list[$order_number][$data['shop_ord_no']]))
            {
                $coupon_price_list[] = array(
                    "order_number"  => $order_number,
                    "shop_ord_no"   => $data['shop_ord_no'],
                    "coupon_type"   => 1,
                    "coupon_price"  => $data['coupon_price']
                );
                
                $chk_coupon_list[$order_number][$data['shop_ord_no']] = 1;
            }
        }

        if(!$order_number)
        {
            echo "업무리스트 반영에 실패하였습니다.<br>주문번호가 없습니다.<br>
                    T_NO     : {$t_no}<br>
                    주문번호 : {$order_number}<br>
                    상품번호 : {$prd_no}<br>";
            exit;
        }

        $work_cms_sql   = "SELECT count(w_no) FROM work_cms WHERE order_number = '{$order_number}' and prd_no ='{$prd_no}'";
        $work_cms_query = mysqli_query($my_db, $work_cms_sql);
        $work_cms       = mysqli_fetch_array($work_cms_query);

        if($work_cms[0] > 0){
            echo "업무리스트 반영에 실패하였습니다.<br>기존에 업로드한 내역이 있는지 확인하세요.<br>
                    T_NO     : {$t_no}<br>
                    주문번호 : {$order_number}<br>
                    상품번호 : {$prd_no}<br>";
            exit;
        }

        if(!isset($product_ord_list[$prd_no]))
        {
            $pro_last_ord_sql    = "SELECT order_date FROM product_cms WHERE prd_no='{$prd_no}' LIMIT 1";
            $pro_last_ord_query  = mysqli_query($my_db, $pro_last_ord_sql);
            $pro_last_ord_result = mysqli_fetch_assoc($pro_last_ord_query);
            $pro_last_ord_date   = $pro_last_ord_result['order_date'];
            $product_ord_list[$prd_no] = $pro_last_ord_date;
        }

        if(isset($product_ord_list[$prd_no]) && $order_date > $product_ord_list[$prd_no]){
            $product_ord_list[$prd_no] = $order_date;
        }

        $order_init_data = array(
            "delivery_state"            => $delivery_state,
            "c_no"                      => $data['c_no'],
            "c_name"                    => $data['c_name'],
            "log_c_no"                  => $data['log_c_no'],
            "s_no"                      => $data['s_no'],
            "team"                      => $data['team'],
            "prd_no"                    => $prd_no,
            "stock_date"                => $stock_date,
            "quantity"                  => $quantity,
            "order_type"                => $data["order_type"],
            "order_number"              => $order_number,
            "order_date"                => $data['order_date'],
            "payment_date"              => $data['payment_date'],
            "recipient"                 => $recipient,
            "recipient_addr"            => $recipient_addr,
            "recipient_hp"              => $data['recipient_hp'],
            "recipient_hp2"             => $data['recipient_hp2'],
            "zip_code"                  => $data['zip_code'],
            "dp_price"                  => $data['dp_price'],
            "dp_price_vat"              => $data['dp_price_vat'],
            "dp_c_no"                   => $dp_c_no,
            "dp_c_name"                 => $data['dp_c_name'],
            "regdate"                   => $data['regdate'],
            "write_date"                => $data['write_date'],
            "task_req"                  => $task_req,
            "task_req_s_no"             => $data['task_req_s_no'],
            "task_req_team"             => $data['task_req_team'],
            "task_run_regdate"          => $data['task_run_regdate'],
            "task_run_s_no"             => $data['task_run_s_no'],
            "task_run_team"             => $data['task_run_team'],
            "delivery_memo"             => $delivery_memo,
            "manager_memo"              => $manager_memo,
            "parent_order_number"       => $data['parent_order_number'],
            "section_ord_no"            => $data['section_ord_no'],
            "shop_ord_no"               => $data['shop_ord_no'],
            "notice"                    => $notice,
            "payment_code"              => $payment_code,
            "payment_type"              => $payment_type,
            "subs_application_times"    => $data['subs_application_times'],
            "subs_progression_times"    => $data['subs_progression_times'],
            "delivery_price"            => $data['delivery_price'],
            "account"                   => $account,
            "coupon_price"              => $data['coupon_price'],
            "final_price"               => $data['final_price'],
            "unit_price"                => $data['unit_price'],
            "unit_delivery_price"       => $data['unit_delivery_price'],
            "origin_ord_no"             => $data['origin_ord_no'],
            "page_idx"                  => $data['page_idx'],
        );

        # 상품 품절 수정소스
        if($chk_is_wait == "1") {
            $wait_order_data = $order_init_data;
            if(!empty($data['out_date'])){
                $wait_order_data['out_date'] = $data['out_date'];
            }
            $wait_order_list[]  = $wait_order_data;
        } else {
            $add_order_list[]   = $order_init_data;
        }

        if(!empty($data['page_idx'])){
            unset($order_init_data['page_idx']);
        }

        if(!empty($data['payment_code'])){
            unset($order_init_data['payment_code']);
        }

        $event_init_data = $order_init_data;
        $event_init_data["dp_price"]                = 0;
        $event_init_data["dp_price_vat"]            = 0;
        $event_init_data["shop_ord_no"]             = "NULL";
        $event_init_data["notice"]                  = "NULL";
        $event_init_data["payment_type"]            = "NULL";
        $event_init_data["subs_application_times"]  = 0;
        $event_init_data["subs_progression_times"]  = 0;
        $event_init_data["delivery_price"]          = 0;
        $event_init_data["coupon_price"]            = 0;
        $event_init_data["unit_price"]              = 0;
        $event_init_data["unit_delivery_price"]     = 0;

        if(isset($gift_order_list[$order_number]))
        {
            $doc_event_tmp_data = $event_init_data;
            $gift_prd_no        = !empty($gift_order_list[$order_number]) ? $gift_order_list[$order_number] : "1271";
            $gift_prd_info      = isset($gift_order_product_info[$gift_prd_no]) ? $gift_order_product_info[$gift_prd_no] : "";

            $doc_event_tmp_data["c_no"]     = $gift_prd_info['c_no'];
            $doc_event_tmp_data["c_name"]   = $gift_prd_info['c_name'];
            $doc_event_tmp_data["s_no"]     = $gift_prd_info['s_no'];
            $doc_event_tmp_data["team"]     = $gift_prd_info['team'];
            $doc_event_tmp_data["prd_no"]   = $gift_prd_no;
            $doc_event_tmp_data["quantity"] = 1;

            $add_order_list[] = $doc_event_tmp_data;
            unset($gift_order_list[$order_number]);
        }

        if(isset($gift_special_order_list[$order_number]))
        {
            foreach($gift_special_order_list[$order_number] as $gift_special_prd_no)
            {
                $doc_special_event_tmp_data = $event_init_data;
                $gift_special_prd_info      = isset($gift_order_product_info[$gift_special_prd_no]) ? $gift_order_product_info[$gift_special_prd_no] : "";

                $doc_special_event_tmp_data["c_no"]     = $gift_special_prd_info['c_no'];
                $doc_special_event_tmp_data["c_name"]   = $gift_special_prd_info['c_name'];
                $doc_special_event_tmp_data["s_no"]     = $gift_special_prd_info['s_no'];
                $doc_special_event_tmp_data["team"]     = $gift_special_prd_info['team'];
                $doc_special_event_tmp_data["prd_no"]   = $gift_special_prd_no;
                $doc_special_event_tmp_data["quantity"] = 1;

                if($chk_is_wait == "1") {
                    $wait_order_list[]  = $doc_special_event_tmp_data;
                }else{
                    $add_order_list[]   = $doc_special_event_tmp_data;
                }
            }

            unset($gift_special_order_list[$order_number]);
        }

        if(isset($gift_ilenol_base_order_list[$order_number]))
        {
            foreach($gift_ilenol_base_order_list[$order_number] as $gift_ilenol_base_prd_no => $base_qty)
            {
                $ilenol_base_event_tmp_data = $event_init_data;
                $gift_ilenol_base_info      = isset($gift_ilenol_product_info[$gift_ilenol_base_prd_no]) ? $gift_ilenol_product_info[$gift_ilenol_base_prd_no] : "";

                $ilenol_base_event_tmp_data["c_no"]     = $gift_ilenol_base_info['c_no'];
                $ilenol_base_event_tmp_data["c_name"]   = $gift_ilenol_base_info['c_name'];
                $ilenol_base_event_tmp_data["s_no"]     = $gift_ilenol_base_info['s_no'];
                $ilenol_base_event_tmp_data["team"]     = $gift_ilenol_base_info['team'];
                $ilenol_base_event_tmp_data["prd_no"]   = $gift_ilenol_base_prd_no;
                $ilenol_base_event_tmp_data["quantity"] = $base_qty;

                $add_order_list[] = $ilenol_base_event_tmp_data;
            }

            unset($gift_ilenol_base_order_list[$order_number]);
        }

        if(isset($gift_ilenol_order_list[$order_number]))
        {
            foreach($gift_ilenol_order_list[$order_number] as $gift_ilenol_prd_no)
            {
                $ilenol_event_tmp_data  = $event_init_data;
                $gift_ilenol_info       = isset($gift_ilenol_product_info[$gift_ilenol_prd_no]) ? $gift_ilenol_product_info[$gift_ilenol_prd_no] : "";

                $ilenol_event_tmp_data["c_no"]      = $gift_ilenol_info['c_no'];
                $ilenol_event_tmp_data["c_name"]    = $gift_ilenol_info['c_name'];
                $ilenol_event_tmp_data["s_no"]      = $gift_ilenol_info['s_no'];
                $ilenol_event_tmp_data["team"]      = $gift_ilenol_info['team'];
                $ilenol_event_tmp_data["prd_no"]    = $gift_ilenol_prd_no;
                $ilenol_event_tmp_data["quantity"]  = 1;

                $add_order_list[] = $ilenol_event_tmp_data;
            }

            unset($gift_ilenol_order_list[$order_number]);
        }

        if(isset($gift_nuzam_order_list[$order_number]))
        {
            $nuzam_event_tmp_data   = $event_init_data;
            $gift_nuzam_prd_no      = !empty($gift_nuzam_order_list[$order_number]) ? $gift_nuzam_order_list[$order_number] : "";
            $gift_nuzam_info        = isset($gift_nuzam_product_info[$gift_nuzam_prd_no]) ? $gift_nuzam_product_info[$gift_nuzam_prd_no] : "";

            $nuzam_event_tmp_data["c_no"]       = $gift_nuzam_info['c_no'];
            $nuzam_event_tmp_data["c_name"]     = $gift_nuzam_info['c_name'];
            $nuzam_event_tmp_data["s_no"]       = $gift_nuzam_info['s_no'];
            $nuzam_event_tmp_data["team"]       = $gift_nuzam_info['team'];
            $nuzam_event_tmp_data["prd_no"]     = $gift_nuzam_prd_no;
            $nuzam_event_tmp_data["quantity"]   = 1;

            if($chk_is_wait == "1") {
                $wait_order_list[]  = $nuzam_event_tmp_data;
            } else {
                $add_order_list[]   = $nuzam_event_tmp_data;
            }

            unset($gift_nuzam_order_list[$order_number]);
        }
    }

    $cms_model = WorkCms::Factory();
    if(!$cms_model->multiInsert($add_order_list)){
        exit("<script>alert('등록에 실패했습니다');location.href='work_cms_inspection.php';</script>");
    }else{

        if(!empty($product_ord_list)){
            foreach($product_ord_list as $product_no => $product_ord_date)
            {
                $prd_order_date_upd = "UPDATE product_cms SET order_date='{$product_ord_date}' WHERE prd_no='{$product_no}'";
                mysqli_query($my_db, $prd_order_date_upd);
            }
        }

        if(!empty($coupon_price_list))
        {
            $coupon_model = WorkCms::Factory();
            $coupon_model->setMainInit("work_cms_coupon", "order_number");
            $coupon_model->multiInsert($coupon_price_list);
        }

        if(!empty($wait_order_list)){
            $wait_model = WorkCms::Factory();
            $wait_model->setMainInit("work_cms_reservation", "w_no");
            $wait_model->multiInsert($wait_order_list);
        }

        exit("<script>alert('등록에 성공했습니다');window.close();</script>");
    }
}
else
{
    # 자동정리 건수
    $temporary_cnt_sql      = "SELECT count(*) as cnt FROM work_cms_temporary";
    $temporary_cnt_query    = mysqli_query($my_db, $temporary_cnt_sql);
    $temporary_cnt_result   = mysqli_fetch_assoc($temporary_cnt_query);
    $temporary_total_cnt    = $temporary_cnt_result['cnt'];

    # 물류업체 2개이상 체크
    $multi_logistics_sql    = "
        SELECT 
            order_number,
            COUNT(DISTINCT log_c_no) AS log_cnt
        FROM 
        (
            SELECT 
                order_number, 
                (SELECT p.log_c_no FROM product_cms p WHERE p.prd_no=w.prd_no) AS log_c_no
            FROM work_cms_temporary AS w
        ) AS result
        GROUP BY order_number
        HAVING log_cnt > 1
    ";
    $multi_logistics_query = mysqli_query($my_db, $multi_logistics_sql);
    $multi_logistics_list  = [];
    while($multi_logistics = mysqli_fetch_assoc($multi_logistics_query))
    {
        $multi_logistics_list[$multi_logistics['order_number']] = $multi_logistics['order_number'];
    }
    $smarty->assign("multi_logistics_list", $multi_logistics_list);

    # 중복 주문번호+상품 건수
    $inspection_chk_sql     = "SELECT order_number, prd_no FROM work_cms_temporary WHERE dp_price_vat > 0 GROUP BY order_number, order_date, prd_no HAVING COUNT(*) > 1";
    $inspection_chk_query   = mysqli_query($my_db, $inspection_chk_sql);
    $duplication_ord_list   = [];
    $inspection_chk_list    = [];
    while($inspection_chk = mysqli_fetch_assoc($inspection_chk_query)){
        $duplication_ord_list[$inspection_chk['order_number']]  = $inspection_chk['order_number'];
        $inspection_chk_list[$inspection_chk['order_number']]   = $inspection_chk['prd_no'];
    }

    $inspection_list    = [];
    $is_inspection_chk  = false;
    $temporary_where    = "";

    if(!empty($duplication_ord_list))
    {
        $temporary_where = implode(",", $duplication_ord_list);

        $temporary_sql = "
            SELECT
                wct.t_no,
                wct.prd_no,
                DATE_FORMAT(wct.regdate, '%Y-%m-%d') as reg_date,
                DATE_FORMAT(wct.regdate, '%H:%i') as reg_time,
                wct.stock_date,
                wct.order_date,
                DATE_FORMAT(wct.order_date, '%Y-%m-%d') as ord_date,
                DATE_FORMAT(wct.order_date, '%H:%i') as ord_time,
                wct.order_number,
                wct.recipient,
                wct.recipient_hp,
                wct.recipient_addr,
                wct.zip_code,
                wct.delivery_memo,
                wct.c_name,
                (SELECT s.s_name FROM staff s WHERE s.s_no=wct.s_no) as s_name,
                (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no))) AS k_prd1_name,
                (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no)) AS k_prd2_name,
                (SELECT title from product_cms prd_cms where prd_cms.prd_no=wct.prd_no) as prd_name,
                (SELECT s.s_name FROM staff s WHERE s.s_no=wct.task_req_s_no) as task_req_s_name,
                wct.task_req,
                (SELECT s_name FROM staff s where s.s_no=wct.task_run_s_no) as task_run_s_name,
                wct.quantity,
                wct.dp_price_vat,
                DATE_FORMAT(wct.payment_date, '%Y-%m-%d') as pay_date,
                DATE_FORMAT(wct.payment_date, '%H:%i') as pay_time,
                wct.dp_c_no,
                wct.dp_c_name,
                wct.manager_memo,
                wct.notice
            FROM work_cms_temporary wct
            WHERE order_number IN({$temporary_where})
            ORDER BY wct.order_number DESC
        ";
        $temporary_query = mysqli_query($my_db, $temporary_sql);
        while($temporary_data = mysqli_fetch_array($temporary_query))
        {
            $temporary_data['dp_price_vat'] = number_format($temporary_data['dp_price_vat']);
            $inspection_list[] = $temporary_data;
        }
        $is_inspection_chk = true;

    }else{
        // 리스트 쿼리
        $temporary_sql = "
            SELECT
                wct.t_no,
                wct.prd_no,
                DATE_FORMAT(wct.regdate, '%Y-%m-%d') as reg_date,
                DATE_FORMAT(wct.regdate, '%H:%i') as reg_time,
                wct.stock_date,
                wct.order_date,
                DATE_FORMAT(wct.order_date, '%Y-%m-%d') as ord_date,
                DATE_FORMAT(wct.order_date, '%H:%i') as ord_time,
                wct.order_number,
                wct.recipient,
                wct.recipient_hp,
                wct.recipient_addr,
                wct.zip_code,
                wct.delivery_memo,
                wct.c_name,
                (SELECT s.s_name FROM staff s WHERE s.s_no=wct.s_no) as s_name,
                (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no))) AS k_prd1_name,
                (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no)) AS k_prd2_name,
                (SELECT title from product_cms prd_cms where prd_cms.prd_no=wct.prd_no) as prd_name,
                (SELECT s.s_name FROM staff s WHERE s.s_no=wct.task_req_s_no) as task_req_s_name,
                wct.task_req,
                (SELECT s_name FROM staff s where s.s_no=wct.task_run_s_no) as task_run_s_name,
                wct.quantity,
                wct.dp_price_vat,
                DATE_FORMAT(wct.payment_date, '%Y-%m-%d') as pay_date,
                DATE_FORMAT(wct.payment_date, '%H:%i') as pay_time,
                wct.dp_c_no,
                wct.dp_c_name,
                wct.manager_memo,
                wct.notice
            FROM work_cms_temporary wct
            WHERE wct.dp_price_vat > 0
            ORDER BY wct.t_no DESC
        ";
        $temporary_dp_list  = [];
        $temporary_query    = mysqli_query($my_db, $temporary_sql);
        if(!!$temporary_query)
        {
            while($temporary_data = mysqli_fetch_array($temporary_query))
            {
                $temporary_data['dp_price_vat'] = number_format($temporary_data['dp_price_vat']);
                $temporary_dp_list[$temporary_data['dp_c_no']][] = $temporary_data;
            }
        }

        foreach($temporary_dp_list as $dp_c_no => $dp_data)
        {
            if(!empty($dp_data))
            {
                $max_cnt = count($dp_data)-1;
                $min_cnt = 0;
                $rand_idx1 = getRandIdx($max_cnt, $min_cnt);
                $rand_idx2 = getRandIdx($max_cnt, $min_cnt);

                if($rand_idx1 == $rand_idx2 && $rand_idx1 == 0){
                    $rand_idx2 = 1;
                }elseif($rand_idx1 == $rand_idx2 && $rand_idx1 > 0){
                    $rand_idx2 = 0;
                }

                if($max_cnt == 0){
                    $inspection_list[] = $dp_data[$rand_idx1];
                }else{
                    $inspection_list[] = $dp_data[$rand_idx1];
                    $inspection_list[] = $dp_data[$rand_idx2];
                }
            }
        }
    }

    $smarty->assign("total_cnt", $temporary_total_cnt);
    $smarty->assign("is_inspection_chk", $is_inspection_chk);
    $smarty->assign("temporary_where", $temporary_where);
    $smarty->assign("inspection_chk_list", $inspection_chk_list);
    $smarty->assign("inspection_list", $inspection_list);

    $smarty->display('work_cms_inspection.html');
}

?>
