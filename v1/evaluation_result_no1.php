<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');

$ev_no   = isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$smarty->assign("ev_no",$ev_no);

// 평가 시스템 쿼리
$evaluation_system_sql = "
    SELECT
        ev_system.subject,
        ev_system.ev_s_date,
        ev_system.ev_e_date,
        ev_system.ev_u_set_no,
        (SELECT subject FROM evaluation_unit_set WHERE ev_u_set_no = ev_system.ev_u_set_no) AS ev_u_set_subject,
        ev_system.admin_s_no
    FROM evaluation_system ev_system
    WHERE ev_system.ev_no='{$ev_no}'
";
$evaluation_system_query    = mysqli_query($my_db, $evaluation_system_sql);
$evaluation_system          = mysqli_fetch_array($evaluation_system_query);

$smarty->assign(
	array(
		"f_subject"         => trim($evaluation_system['subject']),
		"f_ev_s_date"       => trim($evaluation_system['ev_s_date']),
		"f_ev_e_date"       => trim($evaluation_system['ev_e_date']),
        "f_ev_u_set_no"     => trim($evaluation_system['ev_u_set_no']),
        "ev_u_set_subject"  => trim($evaluation_system['ev_u_set_subject']),
		"f_admin_s_no"      => trim($evaluation_system['admin_s_no'])
	)
);

// 접근 권한
if (!(permissionNameCheck($session_permission, "대표") || $session_s_no == '28' || $session_s_no == $evaluation_system['admin_s_no'])){
    $smarty->display('access_company_error.html');
    exit;
}else{
    if($_SESSION["security_pw"] == "PW_ACCESS"){
        unset($_SESSION["security_pw"]);
    }else{
        exit("<script>location.href='security_pw.php?ev_no={$ev_no}';</script>");
    }
}

// 평가지 항목 쿼리
$evaluation_unit_sql = "
    SELECT
        ev_u.ev_u_no,
        IF(ev_u.evaluation_state > 4 , 'TEXT', 'NUM') as type,
        ev_u.question,
        ev_u.description,
        ev_u.evaluation_state
    FROM evaluation_unit ev_u
    WHERE ev_u.ev_u_set_no=(SELECT ev_system.ev_u_set_no FROM evaluation_system ev_system WHERE ev_system.ev_no='{$ev_no}')
    AND ev_u.evaluation_state >= '1' AND ev_u.evaluation_state < '99' AND ev_u.active='1'
    ORDER BY`order` ASC
";

$evaluation_unit_query = mysqli_query($my_db, $evaluation_unit_sql);
$evaluation_unit_list  = [];
$ev_num_idx = 0;
while($evaluation_unit_result = mysqli_fetch_array($evaluation_unit_query))
{
    if($evaluation_unit_result['type'] == 'NUM'){
        $ev_num_idx++;
    }

    $evaluation_unit_list[$evaluation_unit_result['ev_u_no']] = array(
        "type"              => $evaluation_unit_result['type'],
        "title"             => mb_substr($evaluation_unit_result['question'], 0, 4, 'UTF-8'),
        "question"          => $evaluation_unit_result['question'],
        "description"       => $evaluation_unit_result['description'],
        "evaluation_state"  => $evaluation_unit_result['evaluation_state'],
    );
}

$smarty->assign("ev_unit_count",count($evaluation_unit_list)+1);
$smarty->assign("ev_unit_cols_count",count($evaluation_unit_list));
$smarty->assign("ev_unit_list", $evaluation_unit_list);


#평가자 리스트
$evaluation_sql = "
    SELECT 
        DISTINCT esr.receiver_s_no,
        (SELECT s_name FROM staff s WHERE s.s_no=esr.receiver_s_no) AS receiver_s_name
    FROM evaluation_system_result esr
    LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
    WHERE esr.ev_no='{$ev_no}' AND er.is_complete='1'
    ORDER BY receiver_s_name ASC
";
$evaluation_query = mysqli_query($my_db, $evaluation_sql);
$evaluation_list            = [];
$evaluation_sqrt_list       = [];
$evaluation_total_list      = [];
$evaluation_total_tmp_list  = [];
$evaluation_ev_data_list    = [];
while($evaluation = mysqli_fetch_array($evaluation_query))
{
    $evaluation_sqrt_list[$evaluation['receiver_s_no']] = array(
        "receiver"  => $evaluation['receiver_s_no'],
        "name"      => $evaluation['receiver_s_name'],
        "total"     => 0
    );

    $evaluator_result_sql = "
        SELECT
            esr.evaluator_s_no,
            esr.ev_r_no,
            (SELECT s.s_name FROM staff s WHERE s.s_no=esr.evaluator_s_no) AS evaluator_s_name,
            esr.ev_u_no,
            (SELECT eu.evaluation_state FROM evaluation_unit eu WHERE eu.ev_u_no = esr.ev_u_no) as ev_state,
            esr.evaluation_value,
            esr.rate,
            esr.sqrt
        FROM evaluation_system_result esr
        LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
        WHERE esr.ev_no='{$ev_no}' AND esr.receiver_s_no='{$evaluation['receiver_s_no']}' AND er.is_complete='1'
        ORDER BY evaluator_s_name ASC, ev_r_no ASC, ev_u_no ASC
    ";
    $evaluator_result_query = mysqli_query($my_db, $evaluator_result_sql);
    while($evaluator_result = mysqli_fetch_assoc($evaluator_result_query))
    {
        if(!isset($evaluation_total_tmp_list[$evaluation['receiver_s_no']][$evaluator_result['evaluator_s_no']]))
        {
            $evaluation_total_tmp_list[$evaluation['receiver_s_no']][$evaluator_result['evaluator_s_no']] = array(
                "name"      => $evaluator_result['evaluator_s_name'],
                "rate"      => $evaluator_result['rate'],
                "rate_val"  => $evaluator_result['rate']/100,
                "total"     => 0,
                "sqrt"      => 0,
                "rate_sqrt" => 0,
            );
        }

        $evaluation_list[$evaluation['receiver_s_no']][$evaluator_result['evaluator_s_no']][$evaluator_result['ev_u_no']] = array(
            'ev_state'  => $evaluator_result['ev_state'],
            'ev_value'  => $evaluator_result['evaluation_value'],
            'ev_r_no'   => $evaluator_result['ev_r_no'],
            'sqrt'      => $evaluator_result['sqrt'],
        );

        $evaluation_total_tmp_list[$evaluation['receiver_s_no']][$evaluator_result['evaluator_s_no']]['total'] += $evaluator_result['sqrt'];
    }
}

if(!empty($evaluation_total_tmp_list) && !empty($evaluation_list) && !empty($evaluation_sqrt_list))
{
    foreach($evaluation_total_tmp_list as $receiver => $receiver_data)
    {
        foreach($receiver_data as $evaluator => $evaluator_data)
        {
            $total_avg  = round($evaluator_data['total']/$ev_num_idx, 2);
            $total_sqrt = round($total_avg*$evaluator_data['rate_val'], 2);
            $evaluation_total_list[$receiver][$evaluator] = array(
                "ev_name"       => $evaluation_sqrt_list[$receiver]['name'],
                "rec_name"      => $evaluator_data['name'],
                "rate"          => !empty($evaluator_data['rate']) ? $evaluator_data['rate']."%" : "",
                "total_avg"     => $total_avg,
                "total_sqrt"    => $total_sqrt
            );

            $ev_u_data = isset($evaluation_list[$receiver][$evaluator]) ? $evaluation_list[$receiver][$evaluator] : [];

            if(!empty($ev_u_data))
            {
                foreach($ev_u_data as $ev_u_no => $ev_data)
                {
                    $ev_u_value = $ev_u_no."_ev_value";
                    $ev_u_sqrt  = $ev_u_no."_ev_sqrt";
                    $evaluation_ev_data_list[$receiver][$evaluator][$ev_u_no] = array(
                      'state'   => $ev_data['ev_state'],
                      'value'   => $ev_data['ev_value'],
                      'ev_r_no' => $ev_data['ev_r_no'],
                      'sqrt'    => $ev_data['sqrt'],
                    );
                }
            }

            $evaluation_sqrt_list[$receiver]['total'] += $total_sqrt;
        }
    }
}

usort($evaluation_sqrt_list, "twoDimensionTotalSortDesc");

$smarty->assign("evaluation_sqrt_list", $evaluation_sqrt_list);
$smarty->assign("evaluation_total_list", $evaluation_total_list);
$smarty->assign("evaluation_ev_data_list", $evaluation_ev_data_list);

$smarty->display('evaluation_result_no1.html');

?>
