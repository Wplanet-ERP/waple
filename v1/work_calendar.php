<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');
require('inc/model/MyQuick.php');

$proc=(isset($_POST['process']))?$_POST['process']:"";
//$sch_get=(isset($_GET['sch']))?$_GET['sch']:"";


if ($proc == "f_task_run_regdate") { //업무완료일 자동저장

	/////////////////////////* fom action 처리 부분 *//////////////////////

} else if ($proc == "proc_quick_prd") {
//	require('inc/common.php');
//	require('ckadmin.php');

	$mode = $_POST['mode'];
	$quick_prd_req = $_POST['quick_prd'];

	if ($session_id) {
		$sql = "
			SELECT prd_no, title
			  FROM product
		";

		$result = mysqli_query($my_db, $sql);

		while ($row = mysqli_fetch_assoc($result)) {
			$arr_prd[$row['prd_no']] = $row['title'];
		}

		$sql = "
			SELECT quick_prd
			  FROM staff
			 where id = '{$session_id}'
		";
		$result = mysqli_query($my_db, $sql);
		$row = mysqli_fetch_array($result);
		$staff_quick_prd = $row['quick_prd'];
		$arr_q_prd = explode(",", $staff_quick_prd);

		if ($mode == "add" && in_array($quick_prd_req, $arr_q_prd)) {
			$r_msg = "err_exist_prd";
			echo $r_msg;
			exit;
		}

		if ($mode == "del") {
			$key = array_search($quick_prd_req, $arr_q_prd);
			unset($arr_q_prd[$key]);
		} else {
			array_push($arr_q_prd, $quick_prd_req);
		}

		$quick_prd = implode(",", $arr_q_prd);

		if (substr($quick_prd, 0, 1) == ",")
			$quick_prd = substr($quick_prd, 1);

		if ($mode != "display") {
			$sql = "
				UPDATE staff
				   set quick_prd = '{$quick_prd}'
				 where id = '{$session_id}'
			";
			mysqli_query($my_db, $sql);
		}

		$r_msg = "ok";

		mysqli_free_result($result);
		mysqli_close($my_db);
	}

	echo $r_msg;

	$arr_q_prd = explode(",", $quick_prd);
	echo "|";
	echo "<table cellpadding=\"0\" cellspacing=\"0\" style=\"min-width: 440px; max-width:1800px;\"><tr><td style=\"width: 115px;text-align:center;\">";
	echo "<sapn style=\"margin-top:10px;font-size:14px;\">[자주하는 업무]</span>";
	echo "<br><button type=\"button\" onclick=\"window.open('popup/quick_prd_set.php','자주하는 업무설정하기','width=644, height=500, toolbar=no, menubar=no, scrollbars=yes, resizable=yes');return false;\" class=\"btn_small\" style=\"padding:1px 8px;font-weight:bold;\">*설정하기*</button>";
	echo "</td><td>";
	$cnt = 0;
	$sch_prd_get = isset($_POST['sch_prd'])?$_POST['sch_prd']:""; // sch_prd 값 가져오기
	$sch_prd_g1_get = isset($_POST['sch_prd_g1'])?$_POST['sch_prd_g1']:"";
	$sch_prd_g2_get = isset($_POST['sch_prd_g2'])?$_POST['sch_prd_g2']:"";

	foreach  ($arr_q_prd as $key => $value) {
		if( $cnt == '0' ){ // 자주하는 업무에 첫번째에 전체 표기
			if ($sch_prd_get) {
				echo "<span>";
				echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./work_calendar.php';\" style=\"padding: 4px 6px;margin-left: 0px;\">::전체::</button>";
				echo "</span> ";
			}elseif(empty($sch_prd_g1_get) && empty($sch_prd_g1_get)){
				echo "<span>";
				echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./work_calendar.php';\" style=\"padding: 4px 6px;margin-left: 0px; background-color: #c73333;\">::전체::</button>";
				echo "</span> ";
			}else{
				echo "<span>";
				echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./work_calendar.php';\" style=\"padding: 4px 6px;margin-left: 0px;\">::전체::</button>";
				echo "</span> ";
			}
		}

		if ($value) {
			echo "<span>";
			if($sch_prd_get != $value)
				echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./work_calendar.php?sch_prd_g1=".$sch_prd_g1_get."&sch_prd_g2=".$sch_prd_g2_get."&sch_prd=".$value."';\" style=\"padding: 4px 6px;margin-left: 0px;\">" . $arr_prd[$value] . "</button>";
			else
				echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./work_calendar.php?sch_prd_g1=".$sch_prd_g1_get."&sch_prd_g2=".$sch_prd_g2_get."&sch_prd=".$value."';\" style=\"padding: 4px 6px;margin-left: 0px; background-color: #c73333;\">" . $arr_prd[$value] . "</button>";
			//echo "<button type=\"button\" class=\"btn_small\" onclick=\"proc_quick_prd('del', '". $value . "');\">X</button>";
			echo "</span> ";
			//if ($cnt % 4 == 3) echo "<br>";
		}
		$cnt++;
	}
	echo "</td></tr></table>";
	exit;

} else {

	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where="1=1";
	$sch_prd_g1_get=isset($_GET['sch_prd_g1'])?$_GET['sch_prd_g1']:"";
	$sch_prd_g2_get=isset($_GET['sch_prd_g2'])?$_GET['sch_prd_g2']:"";
	$sch_prd_get=isset($_GET['sch_prd'])?$_GET['sch_prd']:"";
	$sch_date_type_get=isset($_GET['sch_date_type'])?$_GET['sch_date_type']:"1";
	$sch_view_type_get=isset($_GET['sch_view_type'])?$_GET['sch_view_type']:"1";
	$sch_s_name_get=isset($_GET['sch_s_name'])?$_GET['sch_s_name']:"";
	$sch_req_s_name_get=isset($_GET['sch_req_s_name'])?$_GET['sch_req_s_name']:"";
	$sch_run_s_name_get=isset($_GET['sch_run_s_name'])?$_GET['sch_run_s_name']:"";
	$sch_run_direct_get=isset($_GET['sch_run_direct'])?$_GET['sch_run_direct']:"";
	$sch_wd_c_no_get=isset($_GET['sch_wd_c_no'])?$_GET['sch_wd_c_no']:"";
	$sch_target_keyword_get=isset($_GET['sch_target_keyword'])?$_GET['sch_target_keyword']:"";

	// 달력 정보 가져오기 [START]
	$sch_date_get=isset($_GET['sch_date'])?$_GET['sch_date']:date('Y-m-d'); // get 값이 없는 경우 오늘을 기준으로 함
	$smarty->assign("sch_date",$sch_date_get);

	$day_token=explode("-",$sch_date_get); // 들어온 날짜를 년,월,일로 분할해 변수로 저장합니다.
	$base_year=$day_token[0]; // 지정 년도
	$base_month=$day_token[1]; // 지정된 월
	$base_day=$day_token[2]; // 지정된 일

	$smarty->assign("base_year",$base_year);
	$smarty->assign("base_month",$base_month);
	$smarty->assign("base_day",$base_day);

	//echo $base_year."<br>";
	//echo $base_month."<br>";
	//echo $base_day."<br>";

	$day_length = date("t",mktime(0,0,0,$base_month,$base_day,$base_year)); // 지정된 달은 몇일까지 있을까요?
	$first_week = date("N",mktime(0,0,0,$base_month,1,$base_year)); // 지정된 달의 첫날은 무슨요일일까요? 1~7 월~일
	$first_empty_week = $first_week%7; // 지정된 달 1일 앞의 공백 숫자.

	$smarty->assign("day_length",$day_length);
	$smarty->assign("first_week",$first_week);
	$smarty->assign("first_empty_week",$first_empty_week);

	//echo $day_length."<br>";
	//echo $first_week."<br>";
	//echo $first_empty_week."<br>";

	$week_length=($day_length+$first_empty_week)/7; $week_length=ceil($week_length); $week_length=$week_length; // 지정된 달은 총 몇주로 라인을 그어야 하나?

	$smarty->assign("week_length",$week_length);

	//echo $week_length."<br>";

	// 날짜와 요일 정보 담기
	$date_i = 1;
	for($i = 1 ; $i <= $first_empty_week ; $i++){
		$calendar_date[]=array(
				'day'=>'',
				'date'=>'',
				'week'=>''
		);
	}
	while($date_i <= $day_length) {
		if($date_i < 10)
			$cal_day = '0'.$date_i;
		else {
			$cal_day = $date_i;
		}

        $chk_date = $base_year.$base_month.$cal_day;
        $holiday_title = isset($holiday_name_list[$chk_date]) ? $holiday_name_list[$chk_date]:'';

        $calendar_date[]=array(
            'day'  => $cal_day,
            'date' => date('Y-m-d', strtotime($base_year.'-'.$base_month.'-'.$date_i)),
            'week' => date('w', strtotime($base_year.'-'.$base_month.'-'.$date_i)),
            'holiday' => $holiday_title
        );
        $date_i++;
	}
	//print_r($calendar_date);
	$smarty->assign("calendar_date",$calendar_date);


	$next_day= date("Y-m-d",mktime(0,0,0,$base_month,$base_day+1,$base_year)); // 다음날
	$prev_dday= date("Y-m-d",mktime(0,0,0,$base_month,$base_day-1,$base_year)); // 이전날
	$next_month= date("Y-m-d",mktime(0,0,0,$base_month+1,$base_day,$base_year)); // 다음달
	$prev_month= date("Y-m-d",mktime(0,0,0,$base_month-1,$base_day,$base_year)); // 지난달
	$next_year= date("Y-m-d",mktime(0,0,0,$base_month,$base_day,$base_year+1)); // 내년
	$prev_year= date("Y-m-d",mktime(0,0,0,$base_month,$base_day,$base_year-1)); // 작년

	$smarty->assign("next_day",$next_day);
	$smarty->assign("prev_dday",$prev_dday);
	$smarty->assign("next_month",$next_month);
	$smarty->assign("prev_month",$prev_month);
	$smarty->assign("next_year",$next_year);
	$smarty->assign("prev_year",$prev_year);

	//echo $next_day."<br>";
	//echo $prev_dday."<br>";
	//echo $next_month."<br>";
	//echo $prev_month."<br>";
	//echo $next_year."<br>";
	//echo $prev_year."<br>";
	// 달력 정보 가져오기 [END]



	// 상품에 따른 그룹 코드 설정
	for ($arr_i = 1 ; $arr_i < count($product_list) ; $arr_i++ ){
		if($product_list[$arr_i]['prd_no'] == $sch_prd_get){
			$sch_prd_g1_get = $product_list[$arr_i]['g1_code'];
			$sch_prd_g2_get = $product_list[$arr_i]['g2_code'];
			break;
		}
	}

	$smarty->assign("sch_prd_g1",$sch_prd_g1_get);
	$smarty->assign("sch_prd_g2",$sch_prd_g2_get);


	if(!empty($sch_s_name_get)) { // 업체 담당자
		$add_where.=" AND w.s_no IN (SELECT s_no FROM staff WHERE s_name LIKE '%{$sch_s_name_get}%')";
	}
	$smarty->assign("sch_s_name",$sch_s_name_get);

	if(!empty($sch_req_s_name_get)) { // 업무 요청자
		$add_where.=" AND w.task_req_s_no IN (SELECT s_no FROM staff WHERE s_name LIKE '%{$sch_req_s_name_get}%')";
	}
	$smarty->assign("sch_req_s_name",$sch_req_s_name_get);

	if(!empty($sch_run_direct_get)){ // 상세검색 여부
		$smarty->assign("sch_run_direct", $sch_run_direct_get);
	}

	if(!empty($sch_run_s_name_get)) { // 업무 처리자
        $add_where .=  " AND w.task_run_s_no IN(SELECT s_no FROM staff WHERE s_name like '%{$sch_run_s_name_get}%')";

		$smarty->assign("sch_run_s_name", $sch_run_s_name_get);
	}

	if(!empty($sch_wd_c_no_get)) { // 출금업체(지급처)
		if($sch_wd_c_no_get != "null"){
			$add_where.=" AND w.wd_c_no='".$sch_wd_c_no_get."'";
		}else{
			$add_where.=" AND (w.wd_c_no IS NULL OR w.wd_c_no ='0')";
		}

		$smarty->assign("sch_wd_c_no",$sch_wd_c_no_get);
	}

	if(!empty($sch_prd_get)) { // 상품
		$add_where.=" AND w.prd_no='".$sch_prd_get."'";
	}else{
		if($sch_prd_g2_get){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
			$add_where.=" AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.k_name_code='{$sch_prd_g2_get}')";
		}elseif($sch_prd_g1_get){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
			$add_where.=" AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1_get}'))";
		}
	}
	$smarty->assign("sch_prd",$sch_prd_get); // 전체 업무내역에 자주하는 업무가 나타게 하기위해 empty 밖에서 처리함

	if(!empty($sch_date_type_get)) { // 조회일정
		$where_s_date = $base_year."-".$base_month."-01"; // where 조건 지정 월 시작날짜
		$where_e_date = date("Y-m", strtotime("+1 month", strtotime($where_s_date))); // where 조건 지정 월 시작날짜의 다음달

		if($sch_date_type_get == '1'){ // 예정 완료일
			$add_where.=" AND DATE_FORMAT(w.task_run_dday, '%Y-%m-%d') BETWEEN '$where_s_date' AND '$where_e_date' ";
		}elseif($sch_date_type_get == '2'){ // 업무 완료일
			$add_where.=" AND DATE_FORMAT(w.task_run_regdate, '%Y-%m-%d') BETWEEN '$where_s_date' AND '$where_e_date' ";
		}elseif($sch_date_type_get == '3'){ // 희망 완료일
			$add_where.=" AND DATE_FORMAT(w.task_req_dday, '%Y-%m-%d') BETWEEN '$where_s_date' AND '$where_e_date' ";
		}

		$smarty->assign("sch_date_type",$sch_date_type_get);
	}

	if(!empty($sch_view_type_get)) { // 표시방법
        $smarty->assign("sch_view_type",$sch_view_type_get);
    }

    if(!empty($sch_target_keyword_get)) { // 타겟키워드
        $add_where.=" AND w.t_keyword like '%{$sch_target_keyword_get}%'";
        $smarty->assign("sch_target_keyword",$sch_target_keyword_get);
    }


	// 상품 구분 가져오기(1차)
	$prd_g1_sql="
			SELECT
					k_name,k_name_code
			FROM kind
			WHERE
					k_code='product' AND (k_parent='0' OR k_parent is null) AND display='1'
			ORDER BY
					priority ASC
			";
	$prd_g1_result=mysqli_query($my_db,$prd_g1_sql);

	while($prd_g1=mysqli_fetch_array($prd_g1_result)) {
		$prd_g1_list[]=array(
			"k_name"=>trim($prd_g1['k_name']),
			"k_name_code"=>trim($prd_g1['k_name_code'])
		);
	}
	$smarty->assign("prd_g1_list",$prd_g1_list);


	// 상품 구분 가져오기(2차)
	if(empty($sch_prd_get) && $sch_prd_g2_get){ // 상품 선택 없이 상품 그룹2로 검색시 그룹2 가져오기
		$prd_g2_sql="
		    SELECT
		        k_name,k_name_code,k_parent
		    FROM kind
		    WHERE
		        k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code='".$sch_prd_g2_get."') AND display='1'
		    ORDER BY
		        priority ASC
		    ";
	}elseif(empty($sch_prd_get) && $sch_prd_g1_get){ // 상품 선택 없이 상품 그룹1로 검색시 그룹2 가져오기
		$prd_g2_sql="
				SELECT
						k_name,k_name_code,k_parent
				FROM kind
				WHERE
						k_code='product' AND k_parent='".$sch_prd_g1_get."' AND display='1'
				ORDER BY
						priority ASC
				";
	}else{ // 그 외 상품에 따른 그룹2 가져오기
		$prd_g2_sql="
		    SELECT
		        k_name,k_name_code,k_parent
		    FROM kind
		    WHERE
		        k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='".$sch_prd_get."')) AND display='1'
		    ORDER BY
		        priority ASC
		    ";
	}

	$prd_g2_result=mysqli_query($my_db,$prd_g2_sql);
	while($prd_g2=mysqli_fetch_array($prd_g2_result)) {
		$prd_g2_list[]=array(
			"k_name"=>trim($prd_g2['k_name']),
			"k_name_code"=>trim($prd_g2['k_name_code'])
		);
	}
	$smarty->assign("prd_g2_list",$prd_g2_list);

	// 상품 목록 가져오기
	if(empty($sch_prd_get) && $sch_prd_g2_get){ // 상품 선택 없이 상품 그룹2로 검색시 상품 가져오기
		$prd_sql="
		    SELECT
		        title,prd_no,k_name_code
		    FROM product
		    WHERE
						display='1' AND
		        k_name_code='".$sch_prd_g2_get."'
		    ORDER BY
		        priority ASC
		    ";
	}else{
		$prd_sql="
		    SELECT
		        title,prd_no,k_name_code
		    FROM product
		    WHERE
						display='1' AND
		        k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='".$sch_prd_get."')
		    ORDER BY
		        priority ASC
		    ";
	}

	$prd_result=mysqli_query($my_db,$prd_sql);
	while($prd=mysqli_fetch_array($prd_result)) {
		$sch_prd_list[]=array(
			"w_no"=>trim($work_array['w_no']),
			"title"=>trim($prd['title']),
			"prd_no"=>trim($prd['prd_no']),
			"k_name_code"=>trim($prd['k_name_code'])
		);
	}
	$smarty->assign("sch_prd_list",$sch_prd_list);

	// 직원가져오기
	$add_where_permission = str_replace("0", "_", $permission_name['마케터']);

	$staff_sql="select s_no,s_name from staff where staff_state < '2' AND permission like '".$add_where_permission."'";
	$staff_query=mysqli_query($my_db,$staff_sql);

	while($staff_data=mysqli_fetch_array($staff_query)) {
		$staff[]=array(
				'no'=>$staff_data['s_no'],
				'name'=>$staff_data['s_name']
		);
		$smarty->assign("staff",$staff);
	}


	// 정렬순서 토글 & 필드 지정
	$add_orderby="";

	if($q_sch_get == "5"){ // 퀵서치의 연장여부 확인 요청건은 \연장여부 마감일순으로 정렬
		$add_orderby.=" extension_date asc";
	}else{
		//$add_orderby.=" w_no desc";
		$add_orderby.=" regdate desc";
	}

	// 리스트 쿼리
	$work_sql="
			SELECT "
			."w_no, " // w_no
			."work_state, " // 진행상태
			."task_req_dday, " // 희망 완료일
			."task_run_dday, " // 예정 완료일
			."task_run_regdate, " // 업무 완료일
			."c_name, " // 업체명
			."s_no, " // 마케팅 담당자
			."(SELECT s_name FROM staff s where s.s_no=w.s_no) as s_name, " // 마케팅 담당자
			."prd_no, " // 상품명
			."(SELECT title from product prd where prd.prd_no=w.prd_no) as prd_no_name, " // 상품명
			."t_keyword, " // 타겟키워드
			."task_run_s_no, " // 업무처리 담당자
			."(SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name " // 업무처리 담당자
			."
		FROM
			work w
		WHERE
			$add_where
			AND (w.work_state = '3' OR w.work_state = '4' OR w.work_state = '5' OR w.work_state = '6')
		ORDER BY
			$add_orderby
		";

	//echo $work_sql;// exit;
	$smarty->assign("query",$work_sql);

	// 전체 게시물 수
	$cnt_sql = "SELECT count(*) FROM (".$work_sql.") AS cnt";
	$cnt_query= mysqli_query($my_db, $cnt_sql);
	$cnt_data=mysqli_fetch_array($cnt_query);
	$total_num = $cnt_data[0];

	$smarty->assign(array(
		"total_num"=>number_format($total_num)
	));

	$smarty->assign("total_num",$total_num);

	// 검색 조건
	$search_url = "sch_prd_g1=".$sch_prd_g1_get.
								"&sch_prd_g2=".$sch_prd_g2_get.
								"&sch_prd=".$sch_prd_get.
								"&sch_date_type=".$sch_date_type_get.
								"&sch_view_type=".$sch_view_type_get.
								"&sch_date=".$sch_date_get.
								"&sch_s_name=".$sch_s_name_get.
								"&sch_req_s_name=".$sch_req_s_name_get.
								"&sch_run_s_name=".$sch_run_s_name_get.
								"&sch_wd_c_no=".$sch_wd_c_no_get.
								"&sch_task_req=".$sch_task_req_get.
								"&sch_task_run=".$sch_task_run_get;

	$smarty->assign("search_url",$search_url);

	// 리스트 쿼리 결과(데이터)
	$result= mysqli_query($my_db, $work_sql);
	while($work_array = mysqli_fetch_array($result)){

		if(!empty($work_array['price'])){
			$price_value=number_format($work_array['price']);
		}else{
			$price_value="";
		}
		if(!empty($work_array['price_vat'])){
			$price_vat_value=number_format($work_array['price_vat']);
		}else{
			$price_vat_value="";
		}
		if(!empty($work_array['regdate'])) {
			$regdate_day_value = date("Y/m/d",strtotime($work_array['regdate']));
			$regdate_time_value = date("H:i",strtotime($work_array['regdate']));
		}else{
			$regdate_day_value="";
			$regdate_time_value="";
		}


		$req_run_intval = "";

		if(!empty($work_array['task_run_regdate'])){
			$task_run_regdate_val = date("Y-m-d",strtotime($work_array['task_run_regdate']));
		}else{
			$task_run_regdate_val = "";
		}

		//예정 완료일 - 희망 완료일
		if($sch_date_type_get == '1' && $work_array['task_run_dday'] && $work_array['task_req_dday']){
			$s_day = date("Y-m-d", strtotime($work_array['task_run_dday']));
			$e_day = date("Y-m-d", strtotime($work_array['task_req_dday']));

			$req_run_intval = intval((strtotime($s_day) - strtotime($e_day)) / 86400);
		//업무 완료일 - 희망 완료일
		}else if($sch_date_type_get == '2' && $work_array['task_run_regdate'] && $work_array['task_req_dday']){
			$s_day = date("Y-m-d", strtotime($work_array['task_run_regdate']));
			$e_day = date("Y-m-d", strtotime($work_array['task_req_dday']));

			$req_run_intval = intval((strtotime($s_day) - strtotime($e_day)) / 86400);
		}

		$work_lists[] = array(
			"w_no"=>$work_array['w_no'],
			"work_state"=>$work_array['work_state'],
			"task_req_dday"=>$work_array['task_req_dday'],
			"task_run_dday"=>$work_array['task_run_dday'],
			"task_run_regdate"=>$task_run_regdate_val,
			"req_run_intval"=>$req_run_intval,
			"c_name"=>$work_array['c_name'],
			"s_no"=>$work_array['s_no'],
			"s_name"=>$work_array['s_name'],
			"prd_no"=>$work_array['prd_no'],
			"prd_no_name"=>$work_array['prd_no_name'],
			"t_keyword"=>is_null($work_array['t_keyword']) ? "Empty data" : ($work_array['t_keyword'] != "" ? $work_array['t_keyword'] : "Empty data"),
			"task_run_s_no"=>is_null($work_array['task_run_s_no']) ? "Empty data" : ($work_array['task_run_s_no'] != "" ? $work_array['task_run_s_no'] : "Empty data"),
			"task_run_s_name"=>is_null($work_array['task_run_s_name']) ? "Empty data" : ($work_array['task_run_s_name'] != "" ? $work_array['task_run_s_name'] : "Empty data")
		);
	}

	# Navigation & My Quick
	$nav_prd_no  = "";
	$is_my_quick = "";
	if($sch_prd_get == "17"){
		$nav_prd_no = "15";
	}elseif($sch_prd_g2_get == "01016"){
		$nav_prd_no = "14";
	}

	if(!empty($nav_prd_no)){
		$quick_model = MyQuick::Factory();
		$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
	}

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_title", $nav_title);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	$smarty->assign(array("work_list"=>$work_lists));
	$smarty->display('work_calendar.html');
}

?>
