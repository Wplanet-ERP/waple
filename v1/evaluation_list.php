<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');
require('inc/model/MyQuick.php');


$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

$proc=(isset($_POST['process']))?$_POST['process']:"";

// 직원가져오기
$add_where_permission = str_replace("0", "_", $permission_name['마케터']);
$staff_sql = "select s_no, s_name from staff where staff_state < '3' AND permission like '".$add_where_permission."'";
$staff_query = mysqli_query($my_db, $staff_sql);

while ($staff_data = mysqli_fetch_array($staff_query)) {
	$staffs[] = array(
		'no' => $staff_data['s_no'],
		'name' =>$staff_data['s_name']
	);
	$smarty->assign("staff",$staffs);
}

// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query", $save_query);

// 선정상태 텍스트
$a_state_text=array(1=>'선정',2=>'탈락',3=>'선정취소');
foreach($a_state_text as $key => $value) {
	$state_stmt[]=array
		(
			"no"=>$key,
			"name"=>$value
		);
	$smarty->assign("select",$state_stmt);
}

// 프로모션 구분 텍스트
$kind_name = array(
	'1' => '체험단',
	'2' => '기자단',
	'3' => '배송체험',
	'4' => '체험단(+보상)',
	'5' => '배송체험단(+보상)'
);

$smarty->assign('kind_list', $kind_name);

// (광고주)지역 분류 가져오기
$sql="select k_name,k_name_code from kind where k_code='location' and k_parent is null";
$sql=mysqli_query($my_db,$sql);
while($result=mysqli_fetch_array($sql)) {
	$location[]=array(
		"location_name"=>trim($result['k_name']),
		"location_code"=>trim($result['k_name_code'])
	);
	$smarty->assign("location",$location);
}

// (광고주)업종 분류 가져오기
$sql="select k_name,k_name_code from kind where k_code='job' and k_parent is null";
$sql=mysqli_query($my_db,$sql);
while($result=mysqli_fetch_array($sql)) {
	$job[]=array(
		"job_name"=>trim($result['k_name']),
		"job_code"=>trim($result['k_name_code'])
	);
	$smarty->assign("job",$job);
}

// 단순히 상태값을 텍스트 표현하기 위해 배열에 담은 정보
$state_name=array(1=>'접수대기', 2=>'접수완료',3=>'모집중',4=>'진행중',5=>'마감');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";

$search_kind 	 	= isset($_GET['skind']) ? $_GET['skind'] : "1";
$f_location1_get 	= isset($_GET['f_location1']) ? $_GET['f_location1'] : "";
$f_location2_get 	= isset($_GET['f_location2']) ? $_GET['f_location2'] : "";
$f_cate1_get	 	= isset($_GET['f_cate1']) ? $_GET['f_cate1'] : "";
$f_cate2_get	 	= isset($_GET['f_cate2']) ? $_GET['f_cate2'] : "";
$search_scode 	 	= isset($_GET['scode']) ? $_GET['scode'] : "";
$search_sreg_sdate1 = isset($_GET['sreg_sdate1']) ? $_GET['sreg_sdate1'] : "";
$search_sreg_sdate2 = isset($_GET['sreg_sdate2']) ? $_GET['sreg_sdate2'] : "";
$search_scompany 	= isset($_GET['scompany']) ? $_GET['scompany'] : "";
$search_sno			= isset($_GET['sno']) ? $_GET['sno'] : "";

if ($search_kind === "")
	$search_kind = "1";

if(!empty($search_kind)) {
	$add_where .= " AND p.kind = '".$search_kind."'";
	$smarty->assign("skind", $search_kind);
}

if(!empty($f_location1_get)) {
	$add_where .= " AND p.c_no in (select c_no from company where location1='".$f_location1_get."')";
	$smarty->assign("f_location1", $f_location1_get);
}

if(!empty($f_location2_get)) {
	$add_where .= " AND p.c_no in (select c_no from company where location2='".$f_location2_get."')";
	$smarty->assign("f_location2", $f_location2_get);
}

if(!empty($f_cate1_get)) {
	$add_where .= " AND p.c_no in (select c_no from company where cate1='".$f_cate1_get."')";
	$smarty->assign("f_cate1",$f_cate1_get);
}
if(!empty($f_cate2_get)) {
	$add_where .= " AND p.c_no in (select c_no from company where cate2='".$f_cate2_get."')";
	$smarty->assign("f_cate2",$f_cate2_get);
}

if(!empty($search_scode)) {
	$add_where.=" AND p.promotion_code like '%".$search_scode."'";
	$smarty->assign("scode",$search_scode);
}

if(!empty($search_sreg_sdate1)) {
	$add_where.=" AND p.reg_sdate >= '".$search_sreg_sdate1."'";
	$smarty->assign("sreg_sdate1",$search_sreg_sdate1);
}

if(!empty($search_sreg_sdate2)) {
	$add_where.=" AND p.reg_sdate <= '".$search_sreg_sdate2."'";
	$smarty->assign("sreg_sdate2",$search_sreg_sdate2);
}

if(!empty($search_scompany)) {
	$add_where.=" AND p.company like '%".$search_scompany."%'";
	$smarty->assign("scompany",$search_scompany);
}

if(!empty($search_sno)) {
	if ($search_sno != "all") {
		$add_where.=" AND p.s_no = '".$search_sno."'";
	}
	$smarty->assign("sno",$search_sno);
} else if(empty($search_sno) && permissionNameCheck($session_permission, "마케터")){
	$add_where.=" AND p.s_no='".$session_s_no."'";
}

// (광고주)2차 분류에 값이 있는 경우 2차 지역 분류 가져오기(2차)
if($f_location2_get != "" || $f_location1_get != ""){
	$sql="select k_name,k_name_code,k_parent from kind where k_code='location' and k_parent='".$f_location1_get."'";
	$sql=mysqli_query($my_db,$sql);
	while($result=mysqli_fetch_array($sql)) {
		$editlocation[]=array(
			"location_name"=>trim($result['k_name']),
			"location_code"=>trim($result['k_name_code']),
			"location_parent"=>trim($result['k_parent'])
		);
		$smarty->assign("editlocation",$editlocation);
	}
}

// (광고주)2차 분류에 값이 있는 경우 2차 업종 분류 가져오기(2차)
if($f_cate2_get != "" || $f_cate1_get != ""){
	$sql="select k_name,k_name_code,k_parent from kind where k_code='job' and k_parent='".$f_cate1_get."'";
	$sql=mysqli_query($my_db,$sql);
	while($result=mysqli_fetch_array($sql)) {
		$editjob[]=array(
			"job_name"=>trim($result['k_name']),
			"job_code"=>trim($result['k_name_code']),
			"job_parent"=>trim($result['k_parent'])
		);
		$smarty->assign("editjob",$editjob);
	}
}

// 정렬순서 토글 & 필드 지정
$add_orderby = " e.e_no DESC";

// 페이에 따른 limit 설정
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num = 20;
$offset = ($pages-1) * $num;

$sql="
	SELECT p.reg_sdate, p.reg_edate, p.p_state AS p_state, p.kind AS kind, p.promotion_code AS promotion_code, p.c_no AS c_no, p.name AS staff_name, p.pres_reward AS pres_reward,
		(select k_name from kind k where k.k_name_code=(select location1 from company c where c.c_no=p.c_no)) as location1,
		(select k_name from kind k where k.k_name_code=(select location2 from company c where c.c_no=p.c_no)) as location2,
		(select k_name from kind c where k_name_code=(select cate1 from company c where c.c_no=p.c_no)) as cate1_name,
		(select k_name from kind c where k_name_code=(select cate2 from company c where c.c_no=p.c_no)) as cate2_name,
		e.e_no,
		e.q1,
		e.q2,
		e.q3,
		e.q4,
		e.q5,
		e.q6,
		e.q7,
		e.q_opinion,
		e.q_contact,
		e.regdate
	FROM promotion p, evaluation e
	WHERE
		p.p_no = e.p_no
		{$add_where}
	ORDER BY
		$add_orderby
	";

//echo $sql."<br>";

// 평균
$avg_sql = "
	select round(avg(nullif(q1, 0)), 1) q1,
				 round(avg(nullif(q2, 0)), 1) q2,
				 round(avg(nullif(q3, 0)), 1) q3,
				 round(avg(nullif(q4, 0)), 1) q4,
				 round(avg(nullif(q5, 0)), 1) q5,
				 round(avg(nullif(q6, 0)), 1) q6,
				 round(avg(nullif(q7, 0)), 1) q7
		from promotion p, evaluation e
	 where p.p_no = e.p_no {$add_where}
";

//echo $avg_sql;
$avg_result = mysqli_query($my_db, $avg_sql);
$avg_data = mysqli_fetch_row($avg_result);

if ($search_kind == "2") {
	unset($avg_data[5]);
	unset($avg_data[6]);
} else if ($search_kind == "3") {
	unset($avg_data[6]);
}

$arr_star = array(
	"",
	"<span style='color:red'>★☆☆☆☆</span>",
	"<span style='color:red'>★★☆☆☆</span>",
	"★★★☆☆",
	"★★★★☆",
	"★★★★★"
);

foreach ($avg_data as $key => $val) {
	if ($val)
		$avg_data[$key] = $arr_star[floor($val)] . " (" . $val .")";
}

if ($search_kind == "2")
	$arr_q_title = array("1.제품/서비스 매력도", "2.가이드<br>(이해)", "3.가이드<br>(요구사항)", "4.보상금액", "5.전체 만족도");
else if ($search_kind == "3")
	$arr_q_title = array("1.품질/매력도", "2.판매가격", "3.배송/포장", "4.가이드<br>(이해)", "5.가이드<br>(요구사항)", "6.전체 만족도");
else if ($search_kind == "4" || $search_kind == '5')
    $arr_q_title = "";
else
	$arr_q_title = array("1.맛/품질", "2.서비스/친절도", "3.분위기/시설", "4.판매가격", "5.가이드<br>(이해)", "6.가이드<br>(요구사항)", "7.전체 만족도");

$smarty->assign("q_title", $arr_q_title);
$smarty->assign("q_avg", $avg_data);

/// 전체 게시물 수
$cnt_sql = "SELECT count(*) FROM (".$sql.") AS cnt";
$cnt_query= mysqli_query($my_db, $cnt_sql);
$cnt_data=mysqli_fetch_array($cnt_query);
$total_num = $cnt_data[0];

$smarty->assign(array(
	"total_num"=>number_format($total_num)
));

$smarty->assign("total_num",$total_num);
$pagenum = ceil($total_num/$num);

// 페이징
if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$url = "skind=".$search_kind."&f_location1=".$f_location1_get."&f_location2=".$f_location2_get."&f_cate1=".$f_cate1_get."&f_cate2=".$f_cate2_get."&sreg_sdate1=".$search_sreg_sdate1."&sreg_sdate2=".$search_sreg_sdate2."&scompany=".$search_scompany."&sno=".$search_sno;
$page=pagelist($pages, "evaluation_list.php", $pagenum, $url);
$smarty->assign("pagelist",$page);//
$noi = $totalnum - ($pages-1)*$num;
$offset = ($pages-1) * $num;
$smarty->assign("totalnum",$totalnum);


// 리스트 쿼리 결과(데이터)
$sql .= "LIMIT $offset, $num";

$query = mysqli_query($my_db,$sql);

while($data=mysqli_fetch_array($query)) {

	$regdate = substr($data['regdate'], 0, 16);

	/* c_no로 업체명 가져오기 Start */
	$company_sql="select c_name from company where c_no='".$data['c_no']."'";
	$company_query=mysqli_query($my_db,$company_sql);
	$company_data=mysqli_fetch_array($company_query); //$company_data['c_name']
	/* c_no로 업체명 가져오기 End */
//echo $company_sql;
	$reg_sdate = str_replace("-", "/", $data['reg_sdate']);

	$stmt[]=array
		(
			"i"=>$i,
			"e_no"=>$data['e_no'],
			"p_no"=>$data['p_no'],
			"state_name"=>$state_name[$data['p_state']],
			"kind"=>$data['kind'],
			"kind_name"=>$kind_name[$data['kind']],
			"promotion_code"=>$data['promotion_code'],
			"location1"=>$data['location1'],
			"location2"=>$data['location2'],
			"f_cate1_name"=>$data['cate1_name'],
			"f_cate2_name"=>$data['cate2_name'],
			"company"=>$company_data['c_name'],
			"staff_name"=>$data['staff_name'],
			"reg_sdate"=>$reg_sdate,
			"q1" => $arr_star[$data['q1']],
			"q2" => $arr_star[$data['q2']],
			"q3" => $arr_star[$data['q3']],
			"q4" => $arr_star[$data['q4']],
			"q5" => $arr_star[$data['q5']],
			"q6" => $arr_star[$data['q6']],
			"q7" => $arr_star[$data['q7']],
			"q_opinion" => $data['q_opinion'],
			"q_contact" => $data['q_contact'],
			"ip" => $data['ip'],
			"regdate"=>$regdate
		);
	$smarty->assign("blogger",$stmt);
	$i++;
}

# Navigation & My Quick
$nav_prd_no  = "";
$is_my_quick = "";
switch($search_kind)
{
	case '1':
		$nav_prd_no  = "115";
		break;
	case '2':
		$nav_prd_no  = "116";
		break;
	case '3':
		$nav_prd_no  = "117";
		break;
	case '4':
		$nav_prd_no  = "118";
		break;
	case '5':
		$nav_prd_no  = "119";
		break;
}

if(!empty($nav_prd_no))
{
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
}

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_prd_no", $nav_prd_no);

$smarty->display('evaluation_list.html');

?>