<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");


//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "기업코드")
	->setCellValue('B1', "기업명")
	->setCellValue('C1', "종목코드")
;

$corp_info_sql = "
		SELECT
			corp_code,
			corp_name,
			stock_code
		FROM corp_info
		WHERE reg_year='2020'
		ORDER BY corp_no ASC
";

$result	= mysqli_query($my_db, $corp_info_sql);
$idx = 2;
if(!!$result)
{
    while($corp_info = mysqli_fetch_array($result))
    {
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$idx, $corp_info['corp_code'])
            ->setCellValue('B'.$idx, $corp_info['corp_name'])
            ->setCellValue('C'.$idx, $corp_info['stock_code'])
		;

        $idx++;
    }
}
$idx--;

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');


$objPHPExcel->getActiveSheet()->getStyle("A1:D{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A1:A'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('B1:B'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('C1:C'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle('A1:C'.$idx)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

$objPHPExcel->getActiveSheet()->setTitle('기업코드');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',"20210426_기업코드정보.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
