<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/advertising.php');

# 검색조건
$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);

$add_where          = "1=1 AND `am`.state IN(3,5)";
$add_date_where     = "1=1";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_adv_s_date     = isset($_GET['sch_adv_s_date']) ? $_GET['sch_adv_s_date'] : $month_val;
$sch_adv_e_date     = isset($_GET['sch_adv_e_date']) ? $_GET['sch_adv_e_date'] : $today_val;
$sch_adv_date_type  = isset($_GET['sch_adv_date_type']) ? $_GET['sch_adv_date_type'] : "month";
$sch_date_kind      = isset($_GET['sch_date_kind']) ? $_GET['sch_date_kind'] : "";
$sch_date_s_time    = isset($_GET['sch_date_s_time']) ? $_GET['sch_date_s_time'] : "";
$sch_date_e_time    = isset($_GET['sch_date_e_time']) ? $_GET['sch_date_e_time'] : "";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_media          = 265;
$sch_product        = isset($_GET['sch_product']) ? $_GET['sch_product'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_is_complete    = isset($_GET['sch_is_complete']) ? $_GET['sch_is_complete'] : "";
$sch_in_date_type   = isset($_GET['sch_in_date_type']) ? $_GET['sch_in_date_type'] : 3;

if(empty($sch_adv_s_date) || empty($sch_adv_e_date))
{
    echo "날짜 검색 값이 없으면 차트 생성이 안됩니다.";
    exit;
}

if(!empty($sch_adv_s_date)){
    $adv_s_datetime  = $sch_adv_s_date." 00:00:00";
    $add_where      .= " AND am.adv_s_date >= '{$adv_s_datetime}'";
    $smarty->assign("sch_adv_s_date", $sch_adv_s_date);
}

if(!empty($sch_adv_e_date)){
    $adv_e_datetime  = $sch_adv_e_date." 23:59:59";
    $add_where      .= " AND am.adv_s_date <= '{$adv_e_datetime}'";
    $smarty->assign("sch_adv_e_date", $sch_adv_e_date);
}

if($sch_date_kind != ""){
    $add_date_where .= " AND chk_s_day = '{$sch_date_kind}'";
    $smarty->assign("sch_date_kind", $sch_date_kind);
}

if($sch_date_s_time != ""){
    $add_date_where .= " AND chk_s_time = '{$sch_date_s_time}'";
    $smarty->assign("sch_date_s_time", $sch_date_s_time);
}

if($sch_date_e_time != ""){
    $add_date_where .= " AND chk_e_time = '{$sch_date_e_time}'";
    $smarty->assign("sch_date_e_time", $sch_date_e_time);
}

if(!empty($sch_no)){
    $add_where .= " AND am.am_no = '{$sch_no}'";
    $smarty->assign("sch_no", $sch_no);
}

if(!empty($sch_media)){
    $add_where .= " AND am.media = '{$sch_media}'";
    $smarty->assign("sch_media", $sch_media);
}

if(!empty($sch_product)){
    $add_where .= " AND am.product = '{$sch_product}'";
    $smarty->assign("sch_product", $sch_product);
}

if(!empty($sch_state)){
    $add_where .= " AND am.state = '{$sch_state}'";
    $smarty->assign("sch_state", $sch_state);
}

if(!empty($sch_agency)){
    $add_where .= " AND am.agency = '{$sch_agency}'";
    $smarty->assign("sch_agency", $sch_agency);
}

if(!empty($sch_is_complete)){
    if($sch_is_complete == '1'){
        $add_where .= " AND (`am`.price - (SELECT SUM(`ar`.adv_price) FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no) != 0)";
    }elseif($sch_is_complete == '2'){
        $add_where .= " AND (`am`.price - (SELECT SUM(`ar`.adv_price) FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no) = 0)";
    }
    $smarty->assign("sch_is_complete", $sch_is_complete);
}

# 브랜드 옵션
if(!empty($sch_brand)) {
    $add_where         .= " AND `ar`.brand = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $add_where         .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $add_where         .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);


$all_date_where = "";
$all_date_title = "";
$all_date_key   = "";
$add_key_column = "";
if($sch_in_date_type == '4') //연간
{
    $sch_s_year      = date("Y", strtotime($sch_adv_s_date));
    $sch_e_year      = date("Y", strtotime($sch_adv_e_date));

    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y')";
    $add_key_column  = "DATE_FORMAT(`am`.adv_s_date, '%Y')";
}
elseif($sch_in_date_type == '3') //월간
{
    $sch_s_month     = date("Y-m", strtotime($sch_adv_s_date));
    $sch_e_month     = date("Y-m", strtotime($sch_adv_e_date));
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";
    $add_key_column  = "DATE_FORMAT(`am`.adv_s_date, '%Y%m')";
}
elseif($sch_in_date_type == '2') //주간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_adv_s_date}' AND '{$sch_adv_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";
    $add_key_column  = "DATE_FORMAT(DATE_SUB(`am`.adv_s_date, INTERVAL(IF(DAYOFWEEK(`am`.adv_s_date)=1,8,DAYOFWEEK(`am`.adv_s_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_adv_s_date}' AND '{$sch_adv_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";
    $add_key_column  = "DATE_FORMAT(`am`.adv_s_date, '%Y%m%d')";
}
$smarty->assign("sch_in_date_type", $sch_in_date_type);

$adv_result_date_list       = [];
$adv_result_table_list      = [];
$adv_result_chart_list      = array(
    array(
        "title" => "광고비",
        "type"  => "bar",
        "color" => "#BFBFFF",
        "data"  => array(),
    ),
    array(
        "title" => "클릭수",
        "type"  => "bar",
        "color" => "#BDBEBD",
        "data"  => array(),
    ),
    array(
        "title" => "CPC",
        "type"  => "line",
        "color" => "#FF0000",
        "data"  => array(),
    )
);

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($all_date = mysqli_fetch_assoc($all_date_query))
{
    $adv_result_date_list[$all_date['chart_key']]   = $all_date['chart_title'];
    $adv_result_table_list[$all_date['chart_key']]  = array("adv_price" => 0, "click_cnt" => 0, "adv_cpc" => 0, "impressions" => 0, "click_per" => 0);
}


$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);


$adv_result_sql = "
    SELECT
        *
    FROM
    (
        SELECT
            `ar`.am_no,
            `ar`.brand,
            `am`.fee_per,
            `am`.price,
            `ar`.impressions,
            `ar`.click_cnt,
            `ar`.adv_price,
            {$add_key_column} as key_date,
            DATE_FORMAT(`am`.adv_s_date, '%w') as chk_s_day,
            DATE_FORMAT(`am`.adv_s_date, '%H') as chk_s_time,
            DATE_FORMAT(`am`.adv_e_date, '%H') as chk_e_time
        FROM advertising_result `ar`
        LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
        WHERE {$add_where}
    ) AS `am`
    WHERE {$add_date_where}
    ORDER BY key_date ASC
";
$adv_result_query       = mysqli_query($my_db, $adv_result_sql);
$adv_result_tmp_list    = [];
while($adv_result = mysqli_fetch_assoc($adv_result_query))
{
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["key_date"]            = $adv_result['key_date'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["fee_per"]             = $adv_result['fee_per'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_price"]         = $adv_result['price'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_impressions"]  += $adv_result['impressions'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_click_cnt"]    += $adv_result['click_cnt'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_adv_price"]    += $adv_result['adv_price'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_cnt"]++;
}

$adv_result_total_list = array("total_price" => 0, "total_click_cnt" => 0, "adv_cpc" => 0);
foreach($adv_result_tmp_list as $am_no => $adv_result_tmp)
{
    foreach($adv_result_tmp as $brand => $adv_brand_data)
    {
        $fee_price      = $adv_brand_data['total_adv_price']*($adv_brand_data['fee_per']/100);
        $cal_price      = $adv_brand_data["total_adv_price"]-$fee_price;

        $adv_result_total_list['total_click_cnt']   += $adv_brand_data["total_click_cnt"];
        $adv_result_total_list['total_price']       += $cal_price;

        $adv_result_table_list[$adv_brand_data['key_date']]['adv_price']    += $cal_price;
        $adv_result_table_list[$adv_brand_data['key_date']]['click_cnt']    += $adv_brand_data["total_click_cnt"];
        $adv_result_table_list[$adv_brand_data['key_date']]['impressions']  += $adv_brand_data["total_impressions"];
    }
}
$adv_result_total_list["adv_cpc"] = $adv_result_total_list['total_price']/$adv_result_total_list['total_click_cnt'];

foreach($adv_result_table_list as $key_date => $key_data){
    $adv_result_table_list[$key_date]["adv_cpc"]    = ($key_data['click_cnt'] == 0) ? 0 : $key_data['adv_price']/$key_data['click_cnt'];
    $adv_result_table_list[$key_date]["click_per"]  = ($key_data['impressions'] == 0) ? $key_data['impressions'] : $key_data['click_cnt']/$key_data['impressions']*100;

    $adv_result_chart_list[0]['data'][] = (int)$key_data['adv_price'];
    $adv_result_chart_list[1]['data'][] = (int)$key_data['click_cnt'];
    $adv_result_chart_list[2]['data'][] = ($key_data['click_cnt'] == 0) ? 0 : round($key_data['adv_price']/$key_data['click_cnt'], 1);
}

$smarty->assign("date_type_option", getDateTypeOption());
$smarty->assign("adv_result_date_list", $adv_result_date_list);
$smarty->assign("adv_result_date_cnt", count($adv_result_date_list));
$smarty->assign("adv_result_chart_name_list", json_encode($adv_result_date_list));
$smarty->assign("adv_result_chart_list", json_encode($adv_result_chart_list));
$smarty->assign("adv_result_table_list", $adv_result_table_list);
$smarty->assign("adv_result_total_list", $adv_result_total_list);

$smarty->display('advertising_result_stats_date_iframe.html');
?>
