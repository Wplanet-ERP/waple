<?php
require('inc/common.php');
require('ckadmin.php');

# 초기 add_where 설정
$add_where 			= " dp.display = '1'";
$sch_incentive_type = isset($_GET['sch_incentive_type']) ? $_GET['sch_incentive_type'] : "1";
$sch_incentive_date = isset($_GET['sch_incentive_date']) ? $_GET['sch_incentive_date'] : date('Y-m');

if ($sch_incentive_type == '2') {
	$sch_incentive_s_date = $sch_incentive_date."-01-01";
	$sch_incentive_e_date = $sch_incentive_date."-12-31";
	$add_where .= " AND dp_date BETWEEN '{$sch_incentive_s_date}' AND '{$sch_incentive_e_date}'";
}else{
	$sch_incentive_s_date = $sch_incentive_date."-01";
	$end_day          	  = date('t', strtotime($sch_incentive_date));
	$sch_incentive_e_date = $sch_incentive_date."-".$end_day;
	$add_where .= " AND dp_date BETWEEN '{$sch_incentive_s_date}' AND '{$sch_incentive_e_date}'";
}

$smarty->assign("sch_incentive_type", $sch_incentive_type);
$smarty->assign("sch_incentive_date", $sch_incentive_date);

# 미디어 커머스 제외
$add_where .= " AND dp.my_c_no <> '3' ";

# 입금 상태 조건
$add_where .= " AND ((dp.dp_method = '2' AND (dp.dp_state = '2' OR dp.dp_state = '3' OR dp.dp_state = '4')) "; // 현금일 경우 입금완료 or 부분입금
$add_where .= " OR (dp.dp_method = '1' AND (dp.dp_state = '1' OR dp.dp_state = '2' OR dp.dp_state = '4')) "; // 카드일 경우 입금대기 or 입금완료
$add_where .= " OR (dp.dp_method = '3' AND dp.dp_state = '2')) "; // 월정산 경우 입금완료

# 마케터 검색조건
$add_where_permission = str_replace("0", "_", $permission_name['마케터']);

# 데이터 정리
$staff_sql    		= "select s_no, s_name FROM staff where staff_state != '2' AND permission like '{$add_where_permission}'";
$staff_query 		= mysqli_query($my_db, $staff_sql);
$marketer_deposit 	= [];
$total_sum_dp_money = 0;
while($marketer_array = mysqli_fetch_array($staff_query))
{
	$marketer_add_where = "";
	if(!empty($marketer_array['s_no'])) {
		$marketer_add_where.=" AND dp.s_no='".$marketer_array['s_no']."'";
	}

	if($marketer_array['s_no'] == '22') // 소연님 예외처리
		continue;

	// 리스트 쿼리
	$marketer_deposit_sql = "
		SELECT
			dp_date,
			(select s_name from staff where s_no = dp.s_no) as s_name,
			(select permission from staff where s_no = dp.s_no) as permission,
			SUM(dp_money) as sum_dp_money,
			s_no,
			(SELECT t.team_code_parent FROM team t WHERE t.team_code=dp.team) as team_parent
		FROM deposit dp
		WHERE {$add_where}
			{$marketer_add_where}
	";
	$marketer_deposit_query = mysqli_query($my_db, $marketer_deposit_sql);
	while ($marketer_deposit_array = mysqli_fetch_array($marketer_deposit_query)) {
		$total_sum_dp_money = $total_sum_dp_money + $marketer_deposit_array['sum_dp_money'];

		$marketer_deposit[] = array(
			"incentive_s_date" 	=> $sch_incentive_s_date,
			"incentive_e_date" 	=> $sch_incentive_e_date,
			"s_name" 		 	=> $marketer_array['s_name'],
			"permission" 	 	=> $marketer_deposit_array['permission'],
			"sum_dp_money" 	 	=> $marketer_deposit_array['sum_dp_money'],
			"s_no" 			 	=> number_format($marketer_deposit_array['s_no']),
			"team_parent" 	 	=> $marketer_deposit_array['team_parent']
		);
	}

	# 입금액 정렬
	foreach ($marketer_deposit as $key => $row) {
		$sum_dp_money[$key]  = $row['sum_dp_money'];
	}
	array_multisort($sum_dp_money, SORT_DESC, $marketer_deposit);
}

$cnt_i = 0;
foreach($marketer_deposit as $out){ // sum_dp_money 에 number_format 적용하기
	$marketer_deposit[$cnt_i++]['sum_dp_money'] = number_format($out['sum_dp_money']);
}

$smarty->assign("total_sum_dp_money", number_format($total_sum_dp_money));
$smarty->assign("marketer_deposit", $marketer_deposit);


$smarty->display('deposit_sum_list.html');
?>
