<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/agency.php');
require('inc/helper/work.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Team.php');
require('inc/model/MyCompany.php');
require('inc/model/Deposit.php');
require('inc/model/Withdraw.php');
require('inc/model/Work.php');
require('inc/model/Agency.php');
require('inc/model/Kind.php');

# Model Init
$work_model     = Work::Factory();
$agency_model   = Agency::Factory();
$company_model  = Company::Factory();

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
if($process == "add_multi_work_settlement")
{
    $cur_date       = date("Y-m-d H:i:s");
    $cur_day        = date("Y-m-d");
    $chk_as_no_list = isset($_POST['chk_as_no_list']) ? explode(",", $_POST['chk_as_no_list']) : "";
    $chk_wd_date    = isset($_POST['chk_run_date']) && !empty($_POST['chk_run_date']) ? $_POST['chk_run_date'] : $cur_day;
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $success_cnt    = 0;

    foreach ($chk_as_no_list as $chk_as_no)
    {
        $agency_data    = $agency_model->getItem($chk_as_no);

        if($agency_data['status'] == '6' && empty($agency_data['w_no']) && !empty($agency_data['partner']))
        {
            $agency_company = $company_model->getItem($agency_data["agency"]);
            $prd_no         = $agency_data['media'];

            $wd_price_vat   = round(getPriceVat($my_db, $agency_data["agency"], $agency_data["wd_price"], $prd_no));
            $dp_price_vat   = round(getPriceVat($my_db, $agency_data["agency"], $agency_data["dp_price"], $prd_no));

            $insert_data = array(
                "work_state"        => $agency_data["status"],
                "c_no"              => $agency_data["partner"],
                "c_name"            => $agency_data["partner_name"],
                "s_no"              => $agency_data["manager"],
                "team"              => $agency_data["manager_team"],
                "prd_no"            => $prd_no,
                "quantity"          => 1,
                "regdate"           => $cur_date,
                "task_req_s_no"     => $session_s_no,
                "task_req_team"     => $session_team,
                "task_run_s_no"     => $agency_data["run_s_no"],
                "task_run_team"     => $agency_data["run_team"],
                "task_run"          => addslashes($agency_data["task_run"]),
                "linked_no"         => $agency_data["as_no"],
                "linked_table"      => "agency_settlement"
            );

            if($agency_data["wd_price"] != 0)
            {
                $insert_data["wd_price"]        = $agency_data["wd_price"];
                $insert_data["wd_price_vat"]    = $wd_price_vat;
                $insert_data["wd_c_no"]         = $agency_data["agency"];
                $insert_data["wd_c_name"]       = $agency_company["c_name"];
            }

            if($agency_data["dp_price"] != 0)
            {
                $insert_data["dp_price"]        = $agency_data["dp_price"];
                $insert_data["dp_price_vat"]    = $dp_price_vat;
                $insert_data["dp_c_no"]         = $agency_data["agency"];
                $insert_data["dp_c_name"]       = $agency_company["c_name"];
            }

            if(!empty($agency_data["run_date"])){
                $insert_data["task_run_regdate"] = $agency_data["run_date"];
            }

            if($work_model->insert($insert_data))
            {
                $new_w_no       = $work_model->getInsertId();

                $work_upd_data  = array("w_no" => $new_w_no);
                $dp_account     = "";
                $wd_account     = "";
                if($agency_data["my_c_no"] == '1'){
                    $dp_account = 4;
                    $wd_account = 5;
                }elseif($agency_data["my_c_no"] == '3'){
                    $dp_account = 1;
                    $wd_account = 3;
                }

                if($agency_data["wd_price"] != 0)
                {
                    $withdraw_model = Withdraw::Factory();
                    $withdraw_data  = array(
                        "my_c_no"           => $agency_data["my_c_no"],
                        "wd_subject"        => "[{$agency_data['settle_month']}] {$agency_company['c_name']}",
                        "wd_state"          => "3",
                        "wd_method"         => "4",
                        "wd_date"           => $chk_wd_date,
                        "wd_account"        => $wd_account,
                        "w_no"              => $new_w_no,
                        "c_no"              => $agency_data["agency"],
                        "c_name"            => $agency_company["c_name"],
                        "s_no"              => $agency_data["manager"],
                        "team"              => $agency_data["manager_team"],
                        "supply_cost"       => $agency_data["wd_price"],
                        "vat_choice"        => "3",
                        "supply_cost_vat"   => 0,
                        "cost"              => $wd_price_vat,
                        "wd_tax_state"      => "4",
                        "regdate"           => $cur_date,
                        "incentive_state"   => 1,
                        "reg_s_no"          => $session_s_no,
                        "reg_team"          => $session_team,
                        "run_s_no"          => $session_s_no,
                        "run_team"          => $session_team,
                    );

                    if($agency_data["agency"] == '4004')
                    {
                        $withdraw_data['incentive_state'] = 3;
                    }

                    $withdraw_model->insert($withdraw_data);

                    $work_upd_data['wd_no'] = $withdraw_model->getInsertId();
                }

                if($agency_data["dp_price"] != 0)
                {
                    $deposit_model   = Deposit::Factory();
                    $deposit_my_c_no = ($agency_data["agency"] == '4004' || $agency_data["agency"] == '4472') ? 1 : $agency_data["my_c_no"];
                    $deposit_account = ($agency_data["agency"] == '4004' || $agency_data["agency"] == '4472') ? 4 : $dp_account;
                    $deposit_data  = array(
                        "my_c_no"           => $deposit_my_c_no,
                        "dp_subject"        => "[{$agency_data['settle_month']}] {$agency_company["c_name"]}",
                        "dp_state"          => "1",
                        "dp_method"         => "3",
                        "dp_account"        => $deposit_account,
                        "w_no"              => $new_w_no,
                        "c_no"              => $agency_data["agency"],
                        "c_name"            => $agency_company["c_name"],
                        "s_no"              => $agency_data["manager"],
                        "team"              => $agency_data["manager_team"],
                        "supply_cost"       => $agency_data["dp_price"],
                        "vat_choice"        => "3",
                        "supply_cost_vat"   => 0,
                        "cost"              => $dp_price_vat,
                        "dp_tax_state"      => "4",
                        "regdate"           => $cur_date,
                        "reg_s_no"          => $session_s_no,
                        "reg_team"          => $session_team,
                    );
                    $deposit_model->insert($deposit_data);

                    $work_upd_data['dp_no'] = $deposit_model->getInsertId();
                }

                if(isset($work_upd_data['dp_no']) || isset($work_upd_data['wd_no']))
                {
                    $work_model->update($work_upd_data);
                }

                $agency_model->update(array("as_no" => $chk_as_no, "w_no" => $new_w_no));

                $success_cnt++;
            }
        }
    }

    exit("<script>alert('업무리스트의 {$success_cnt}건 추가했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
}
elseif($process == "add_work_settlement")
{
    $chk_as_no  = isset($_POST['chk_as_no']) ? $_POST['chk_as_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $agency_data = $agency_model->getItem($chk_as_no);

    if($agency_data['status'] == '6' && empty($agency_data['w_no']) && !empty($agency_data['partner']))
    {
        $prd_no         = $agency_data['media'];
        $cur_date       = date("Y-m-d H:i:s");
        $cur_day        = date("Y-m-d");
        $agency_company = $company_model->getItem($agency_data["agency"]);

        $wd_price_vat   = round(getPriceVat($my_db, $agency_data["agency"], $agency_data["wd_price"], $prd_no));
        $dp_price_vat   = round(getPriceVat($my_db, $agency_data["agency"], $agency_data["dp_price"], $prd_no));

        $insert_data = array(
            "work_state"        => $agency_data["status"],
            "c_no"              => $agency_data["partner"],
            "c_name"            => $agency_data["partner_name"],
            "s_no"              => $agency_data["manager"],
            "team"              => $agency_data["manager_team"],
            "prd_no"            => $prd_no,
            "quantity"          => 1,
            "regdate"           => $cur_date,
            "task_req_s_no"     => $session_s_no,
            "task_req_team"     => $session_team,
            "task_run_s_no"     => $agency_data["run_s_no"],
            "task_run_team"     => $agency_data["run_team"],
            "task_run"          => addslashes($agency_data["run_memo"]),
            "linked_no"         => $agency_data["as_no"],
            "linked_table"      => "agency_settlement"
        );

        if($agency_data["wd_price"] != 0)
        {
            $insert_data["wd_price"]        = $agency_data["wd_price"];
            $insert_data["wd_price_vat"]    = $wd_price_vat;
            $insert_data["wd_c_no"]         = $agency_data["agency"];
            $insert_data["wd_c_name"]       = $agency_company["c_name"];
        }

        if($agency_data["dp_price"] != 0)
        {
            $insert_data["dp_price"]        = $agency_data["dp_price"];
            $insert_data["dp_price_vat"]    = $dp_price_vat;
            $insert_data["dp_c_no"]         = $agency_data["agency"];
            $insert_data["dp_c_name"]       = $agency_company["c_name"];
        }

        if(!empty($agency_data["run_date"])){
            $insert_data["task_run_regdate"] = $agency_data["run_date"];
        }

        if($work_model->insert($insert_data))
        {
            $new_w_no       = $work_model->getInsertId();

            $work_upd_data  = array("w_no" => $new_w_no);
            $dp_account     = "";
            $wd_account     = "";
            if($agency_data["my_c_no"] == '1'){
                $dp_account = 4;
                $wd_account = 5;
            }elseif($agency_data["my_c_no"] == '3'){
                $dp_account = 1;
                $wd_account = 3;
            }

            if($agency_data["wd_price"] != 0)
            {
                $withdraw_model = Withdraw::Factory();
                $withdraw_data  = array(
                    "my_c_no"           => $agency_data["my_c_no"],
                    "wd_subject"        => "[{$agency_data['settle_month']}] {$agency_company['c_name']}",
                    "wd_state"          => "2",
                    "wd_method"         => "4",
                    "wd_account"        => $wd_account,
                    "w_no"              => $new_w_no,
                    "c_no"              => $agency_data["agency"],
                    "c_name"            => $agency_company["c_name"],
                    "s_no"              => $agency_data["manager"],
                    "team"              => $agency_data["manager_team"],
                    "supply_cost"       => $agency_data["wd_price"],
                    "vat_choice"        => "3",
                    "supply_cost_vat"   => 0,
                    "cost"              => $wd_price_vat,
                    "wd_tax_state"      => "4",
                    "regdate"           => $cur_date,
                    "incentive_state"   => 1,
                    "reg_s_no"          => $session_s_no,
                    "reg_team"          => $session_team,
                );

                if($agency_data["agency"] == '4004')
                {
                    $withdraw_data['incentive_state'] = 3;
                }
                
                $withdraw_model->insert($withdraw_data);

                $work_upd_data['wd_no'] = $withdraw_model->getInsertId();
            }

            if($agency_data["dp_price"] != 0)
            {
                $deposit_model   = Deposit::Factory();
                $deposit_my_c_no = ($agency_data["agency"] == '4004' || $agency_data["agency"] == '4472') ? 1 : $agency_data["my_c_no"];
                $deposit_account = ($agency_data["agency"] == '4004' || $agency_data["agency"] == '4472') ? 4 : $dp_account;
                $deposit_data  = array(
                    "my_c_no"           => $deposit_my_c_no,
                    "dp_subject"        => "[{$agency_data['settle_month']}] {$agency_company["c_name"]}",
                    "dp_state"          => "1",
                    "dp_method"         => "3",
                    "dp_account"        => $deposit_account,
                    "w_no"              => $new_w_no,
                    "c_no"              => $agency_data["agency"],
                    "c_name"            => $agency_company["c_name"],
                    "s_no"              => $agency_data["manager"],
                    "team"              => $agency_data["manager_team"],
                    "supply_cost"       => $agency_data["dp_price"],
                    "vat_choice"        => "3",
                    "supply_cost_vat"   => 0,
                    "cost"              => $dp_price_vat,
                    "dp_tax_state"      => "4",
                    "regdate"           => $cur_date,
                    "reg_s_no"          => $session_s_no,
                    "reg_team"          => $session_team,
                );
                $deposit_model->insert($deposit_data);

                $work_upd_data['dp_no'] = $deposit_model->getInsertId();
            }

            if(isset($work_upd_data['dp_no']) || isset($work_upd_data['wd_no']))
            {
                $work_model->update($work_upd_data);
            }

            $agency_model->update(array("as_no" => $chk_as_no, "w_no" => $new_w_no));

            exit("<script>alert('업무리스트 추가에 성공했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('업무리스트 추가에 실패했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('업무리스트 추가에 실패했습니다. 추가할 수 있는 상태가 아닙니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }
}
elseif($process == "add_agency_settlement")
{
    $new_settle_month       = isset($_POST['new_settle_month']) ? $_POST['new_settle_month'] : date("Y-m", strtotime("-1 months"));
    $new_status             = isset($_POST['new_status']) ? $_POST['new_status'] : 1;
    $new_agency             = isset($_POST['new_agency']) ? $_POST['new_agency'] : "";
    $new_company            = isset($_POST['new_company']) ? $_POST['new_company'] : "";
    $new_media              = isset($_POST['new_media']) ? $_POST['new_media'] : "";
    $new_prd_name           = isset($_POST['new_prd_name']) ? addslashes(trim($_POST['new_prd_name'])) : "";
    $new_prd_detail         = isset($_POST['new_prd_detail']) ? addslashes(trim($_POST['new_prd_detail'])) : "";
    $new_advertiser_name    = isset($_POST['new_advertiser_name']) ? addslashes(trim($_POST['new_advertiser_name'])) : "";
    $new_advertiser_id      = isset($_POST['new_advertiser_id']) ? $_POST['new_advertiser_id'] : "";
    $new_supply_price       = isset($_POST['new_supply_price']) ? str_replace(",", "", $_POST['new_supply_price']) : 0;
    $new_supply_fee_per     = isset($_POST['new_supply_fee_per']) ? $_POST['new_supply_fee_per'] : 0;
    $new_supply_fee         = isset($_POST['new_supply_fee']) ? str_replace(",", "", $_POST['new_supply_fee']) : 0;
    $search_url             = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $last_idx           = $agency_model->getLastGroupNo($new_settle_month);
    $settle_month_val   = date("Ym",strtotime($new_settle_month));
    $group_no           = $settle_month_val."_agency_".sprintf('%04d', $last_idx);

    $agency_data = array(
        'status'            => $new_status,
        'agency'            => $new_agency,
        'settle_month'      => $new_settle_month,
        'group_no'          => $group_no,
        'company'           => $new_company,
        'media'             => $new_media,
        'prd_name'          => $new_prd_name,
        'prd_detail'        => $new_prd_detail,
        'advertiser_name'   => $new_advertiser_name,
        'advertiser_id'     => $new_advertiser_id,
        'supply_price'      => $new_supply_price,
        'supply_fee'        => $new_supply_fee,
        'supply_fee_per'    => $new_supply_fee_per,
        'wd_price'          => $new_supply_price,
        'dp_price'          => $new_supply_fee,
        'run_s_no'          => $session_s_no,
        'run_team'          => $session_team,
        'regdate'           => date("Y-m-d H:i:s")
    );

    if($agency_model->insert($agency_data)){
        exit("<script>alert('추가에 성공했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('추가에 실패했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }
}
elseif($process == "change_status")
{
    $chk_as_no_list = isset($_POST['chk_as_no_list']) ? $_POST['chk_as_no_list'] : "";
    $chk_status     = isset($_POST['chk_status']) ? $_POST['chk_status'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if($chk_status == '6'){
        $upd_sql    = "UPDATE agency_settlement SET status='{$chk_status}', run_date=now() WHERE as_no IN({$chk_as_no_list})";
    }else{
        $upd_sql    = "UPDATE agency_settlement SET status='{$chk_status}' WHERE as_no IN({$chk_as_no_list})";
    }

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('진행상태 변경에 실패했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('진행상태 변경했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }
}
elseif($process == "change_run_date")
{
    $chk_as_no_list = isset($_POST['chk_as_no_list']) ? $_POST['chk_as_no_list'] : "";
    $chk_run_date   = isset($_POST['chk_run_date']) ? $_POST['chk_run_date'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_sql    = "UPDATE agency_settlement SET run_date='{$chk_run_date}' WHERE as_no IN({$chk_as_no_list})";

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('업무완료일 변경에 실패했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('업무완료일 변경했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }
}
elseif($process == "del_agency_settle")
{
    $chk_as_no  = isset($_POST['chk_as_no']) ? $_POST['chk_as_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(!$agency_model->delete($chk_as_no)){
        exit("<script>alert('삭제에 실패했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }
}
elseif($process == "add_partner_settlement")
{
    $chk_as_no   = isset($_POST['chk_as_no']) ? $_POST['chk_as_no'] : "";
    $search_url  = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $agency_item = $agency_model->getItem($chk_as_no);

    $agency_data = array(
        "status"        => '1',
        "group_no"      => $agency_item['group_no'],
        "agency"        => $agency_item['agency'],
        "settle_month"  => $agency_item['settle_month'],
        "company"       => $agency_item['company'],
        "media"         => $agency_item['media'],
        "prd_name"      => $agency_item['prd_name'],
        "prd_detail"    => $agency_item['prd_detail'],
        "advertiser_name"   => $agency_item['advertiser_name'],
        "advertiser_id"     => $agency_item['advertiser_id'],
        "supply_price"      => $agency_item['supply_price'],
        "supply_fee"        => $agency_item['supply_fee'],
        "supply_fee_per"    => $agency_item['supply_fee_per'],
        "dp_price"          => 0,
        "wd_price"          => 0,
        "run_s_no"          => $session_s_no,
        "run_team"          => $session_team,
        "regdate"           => date("Y-m-d H:i:s"),
    );

    if(!$agency_model->insert($agency_data)){
        exit("<script>alert('업체 추가에 실패했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('업체를 추가했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }
}
elseif($process == "f_status")
{
    $as_no      = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $val        = isset($_POST['val']) ? $_POST['val'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if($val == '6'){
        $upd_data   = array("as_no" => $as_no, "status" => $val ,"run_date" => date("Y-m-d H:i:s"));
    }else{
        $upd_data   = array("as_no" => $as_no, "status" => $val);
    }

    if(!$agency_model->update($upd_data)){
        echo "진행상태 변경에 실패했습니다.";
    }else{
        echo "진행상태를 변경했습니다.";
    }
    exit;
}
elseif($process == "f_run_date")
{
    $as_no      = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $val        = isset($_POST['val']) ? $_POST['val'] : "";
    $upd_data   = array("as_no" => $as_no, "run_date" => $val);

    if(!$agency_model->update($upd_data)){
        echo "업무완료일 변경에 실패했습니다.";
    }else{
        echo "업무완료일을 변경했습니다.";
    }
    exit;
}
elseif($process == "f_partner")
{
    $as_no = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $val   = isset($_POST['val']) ? $_POST['val'] : "";

    $agency_company = $company_model->getItem($val);

    $upd_data    = [];
    if(!empty($agency_company)) {
        $upd_data    = array("as_no" => $as_no, "my_c_no" => $agency_company['my_c_no'], "partner" => $agency_company['c_no'], "partner_name" => $agency_company['c_name'], "manager" => $agency_company['s_no'], "manager_team" => $agency_company['team']);
    }else{
        echo "업체 변경에 실패했습니다.";
        exit;
    }

    if(!$agency_model->update($upd_data)){
        echo "업체 변경에 실패했습니다.";
    }else{
        echo "업체를 변경했습니다.";
    }
    exit;
}
elseif($process == "f_run_memo")
{
    $as_no       = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $val         = isset($_POST['val']) ? addslashes(trim($_POST['val'])) : "";

    $agency_item = $agency_model->getItem($as_no);
    $upd_data    = array("as_no" => $as_no, "run_memo" => $val);

    if($agency_model->update($upd_data))
    {
        if($agency_item['w_no'] > 0){
            $work_model->update(array("w_no" => $agency_item['w_no'], "task_run" => $val));
        }
        echo "업무진행이 저장됬습니다.";

    }else{
        echo "업무진행 변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "save_advertise_id")
{
    $as_no          = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $partner        = isset($_POST['partner']) ? $_POST['partner'] : "";
    $type           = isset($_POST['type']) ? $_POST['type'] : "";
    $val            = isset($_POST['val']) ? addslashes(trim($_POST['val'])) : "";

    $company_data   = array("c_no" => $partner, $type => $val);

    if(!$company_model->update($company_data)){
        echo "광고주 ID 저장에 실패했습니다.";
    }else{
        echo "광고주 ID가 저장됬습니다.";
    }
    exit;
}
elseif($process == "save_wd_price")
{
    $as_no      = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $val        = isset($_POST['val']) ? str_replace(',','', addslashes(trim($_POST['val']))) : "";
    $dp_price   = isset($_POST['dp_price']) ? str_replace(',','', addslashes(trim($_POST['dp_price']))) : "";
    $upd_data   = array("as_no" => $as_no, "wd_price" => $val, "dp_price" => $dp_price);

    if(!$agency_model->update($upd_data)){
        echo "출금액 변경에 실패했습니다.";
    }else{
        echo "출금액을 변경했습니다.";
    }
    exit;
}
elseif($process == "f_dp_price")
{
    $as_no      = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $val        = isset($_POST['val']) ? str_replace(',','', addslashes(trim($_POST['val']))) : "";
    $upd_data   = array("as_no" => $as_no, "dp_price" => $val);

    if(!$agency_model->update($upd_data)){
        echo "입금액 변경에 실패했습니다.";
    }else{
        echo "입금액을 변경했습니다.";
    }
    exit;
}
elseif($process == "del_multi_settlement")
{
    $chk_as_no_list = isset($_POST['chk_as_no_list']) ? $_POST['chk_as_no_list'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $del_sql        = "DELETE FROM agency_settlement WHERE as_no IN({$chk_as_no_list}) AND status='1'";

    if(!mysqli_query($my_db, $del_sql)){
        exit("<script>alert('삭제 처리에 실패했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제했습니다');location.href='agency_settlement.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "65";
$nav_title   = "마케팅 비용 및 수수료 정산";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 기본 변수 값 설정
$prev_settle_month  = date('Y-m', strtotime("-1 months"));

# 검색조건
$add_where          = "1=1";
$sch_settle_month   = isset($_GET['sch_settle_month']) ? $_GET['sch_settle_month'] : $prev_settle_month;
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_status         = isset($_GET['sch_status']) ? $_GET['sch_status'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_media          = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_is_partner     = isset($_GET['sch_is_partner']) ? $_GET['sch_is_partner'] : "";
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_partner_name   = isset($_GET['sch_partner_name']) ? $_GET['sch_partner_name'] : "";
$sch_manager_team   = isset($_GET['sch_manager_team']) ? $_GET['sch_manager_team'] : $session_team;
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_run_name       = isset($_GET['sch_run_name']) ? $_GET['sch_run_name'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ori_ord_type       = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "";

#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
$url_check_str  = $_SERVER['QUERY_STRING'];
$url_chk_result = false;
if(empty($url_check_str)){
    $url_check_team_where   = getTeamWhere($my_db, $session_team);
    $url_check_sql    		=  "SELECT count(`as_no`) as cnt FROM agency_settlement `as` WHERE `as`.settle_month = '{$sch_settle_month}' AND manager_team IN({$url_check_team_where})";
    $url_check_query  		= mysqli_query($my_db, $url_check_sql);
    $url_check_result 		= mysqli_fetch_assoc($url_check_query);

    if($url_check_result['cnt'] == 0){
        $sch_manager_team   = "all";
        $url_chk_result     = true;
    }
}

if(!empty($sch_settle_month))
{
    $add_where .= " AND `as`.settle_month = '{$sch_settle_month}'";
    $smarty->assign('sch_settle_month', $sch_settle_month);
}

if(!empty($sch_no))
{
    $add_where .= " AND `as`.as_no = '{$sch_no}'";
    $smarty->assign('sch_no', $sch_no);
}

if(!empty($sch_status)){
    $add_where   .= " AND `as`.status = '{$sch_status}'";
    $smarty->assign('sch_status', $sch_status);
}

if(!empty($sch_agency)){
    $add_where   .= " AND `as`.agency = '{$sch_agency}'";
    $smarty->assign('sch_agency', $sch_agency);
}

if(!empty($sch_media)){
    $add_where   .= " AND `as`.media = '{$sch_media}'";
    $smarty->assign('sch_media', $sch_media);
}

if(!empty($sch_is_partner))
{
    if($sch_is_partner == '1'){
        $add_where   .= " AND `as`.partner > 0";
    }elseif($sch_is_partner == '2'){
        $add_where   .= " AND (`as`.partner is null OR `as`.partner < 1)";
    }
    $smarty->assign('sch_is_partner', $sch_is_partner);
}

if(!empty($sch_my_c_no)){
    $add_where   .= " AND `as`.my_c_no = '{$sch_my_c_no}'";
    $smarty->assign('sch_my_c_no', $sch_my_c_no);
}

if(!empty($sch_partner_name)){
    $add_where   .= " AND `as`.partner_name LIKE '%{$sch_partner_name}%'";
    $smarty->assign('sch_partner_name', $sch_partner_name);
}

$team_model         = Team::Factory();
$team_all_list      = $team_model->getTeamAllList();
$staff_team_list    = $team_all_list['staff_list'];
$team_full_name_list= $team_model->getTeamFullNameList();

$sch_staff_list      = [];
if (!empty($sch_manager_team))
{
    if ($sch_manager_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_manager_team);
        $add_where 		 .= " AND `as`.manager_team IN({$sch_team_code_where})";
        $sch_staff_list   = $staff_team_list[$sch_manager_team];
    }
    $smarty->assign("sch_manager_team", $sch_manager_team);
}

if (!empty($sch_manager))
{
    if ($sch_manager != "all") {
        $add_where 		 .= " AND `as`.manager='{$sch_manager}'";
    }
    $smarty->assign("sch_manager", $sch_manager);
}

if(!empty($sch_run_name)){
    $add_where   .= " AND `as`.run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_run_name}%')";
    $smarty->assign('sch_run_name', $sch_run_name);
}

# 브랜드 검색
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where      .= " AND `as`.partner = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where      .= " AND `as`.partner IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

    $add_where      .= " AND `as`.partner IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

$add_order_by = "";
$ord_type_by  = "";
$order_by_val = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

    if(!empty($ord_type_by))
    {
        $ord_type_list = [];
        if($ord_type == 'team_manager'){
            $ord_type_list[] = "team_manager";
        }

        if($ori_ord_type == $ord_type)
        {
            if($ord_type_by == '1'){
                $order_by_val = "ASC";
            }elseif($ord_type_by == '2'){
                $order_by_val = "DESC";
            }
        }else{
            $ord_type_by  = '2';
            $order_by_val = "DESC";
        }

        foreach($ord_type_list as $ord_type_val){

            if($ord_type_val == 'team_manager'){
                $add_order_by .= "team_priority {$order_by_val}, manager_name ASC, ";
            }else{
                $add_order_by .= "{$ord_type_val} {$order_by_val}, ";
            }
        }
    }
}

$add_order_by .= "group_no DESC";

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);


// 전체 게시물 수
$agency_total_sql	 = "SELECT count(DISTINCT group_no) as cnt, SUM(dp_price) as dp_price_total, SUM(wd_price) as wd_price_total FROM agency_settlement `as` WHERE {$add_where}";
$agency_total_query  = mysqli_query($my_db, $agency_total_sql);
$agency_total_result = mysqli_fetch_array($agency_total_query);
$agency_total 	     = $agency_total_result['cnt'];
$agency_dp_total     = $agency_total_result['dp_price_total'];
$agency_wd_total     = $agency_total_result['wd_price_total'];

$smarty->assign("agency_dp_total", $agency_dp_total);
$smarty->assign("agency_wd_total", $agency_wd_total);

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($agency_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

if(empty($url_check_str) && $url_chk_result){
    $search_url = "sch_manager_team=all";
}else{
    $search_url = getenv("QUERY_STRING");
}
$page_list	= pagelist($pages, "agency_settlement.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $agency_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 주문 그룹화
$agency_group_sql   = "SELECT DISTINCT group_no, (SELECT t.priority FROM team t WHERE t.team_code=`as`.manager_team) AS team_priority, (SELECT s_name FROM staff s WHERE s.s_no=`as`.manager) AS manager_name FROM agency_settlement `as` WHERE {$add_where} GROUP BY group_no ORDER BY {$add_order_by} LIMIT {$offset}, {$num}";
$agency_group_query = mysqli_query($my_db, $agency_group_sql);
$add_group_where_list = [];
while($add_group = mysqli_fetch_assoc($agency_group_query))
{
    $add_group_where_list[] = "'{$add_group['group_no']}'";
}
$add_group_where     = implode(",", $add_group_where_list);


$agency_settle_list  = [];
$agency_option       = getSettleAgencyOption();
$media_option        = getAdvertiseMediaOption();
if(!empty($add_group_where))
{
    $agency_settle_sql = "
        SELECT
            *,
            DATE_FORMAT(`as`.regdate, '%Y/%m/%d') as reg_day,
            DATE_FORMAT(`as`.regdate, '%H:%i') as reg_hour,
            (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=`as`.my_c_no) AS my_c_name,
            (SELECT t.priority FROM team t WHERE t.team_code=`as`.manager_team) AS team_priority,
            (SELECT t.team_name FROM team t WHERE t.team_code=`as`.manager_team) AS manager_team_name,
            (SELECT s_name FROM staff s WHERE s.s_no=`as`.manager) AS manager_name,
            (SELECT s_name FROM staff s WHERE s.s_no=`as`.run_s_no) AS run_name,
            (SELECT task_run_file_origin FROM work w WHERE w.w_no=`as`.w_no) AS run_file
        FROM agency_settlement `as`
        WHERE group_no IN({$add_group_where})
        ORDER BY {$add_order_by}
    ";
    $agency_settle_query = mysqli_query($my_db, $agency_settle_sql);
    while($agency_settle = mysqli_fetch_assoc($agency_settle_query))
    {
        $agency_settle['run_file_count'] = 0;
        if(!empty($agency_settle['run_file'])){
            $run_file_list = explode(",", $agency_settle['run_file']);
            $agency_settle['run_file_count'] = count($run_file_list);
        }
        $agency_settle['agency_name'] = $agency_option[$agency_settle['agency']];
        $agency_settle['media_name']  = $media_option[$agency_settle['media']];
        $agency_settle_list[$agency_settle['group_no']][] = $agency_settle;
    }
}

$my_company_model       = MyCompany::Factory();
$my_company_list        = $my_company_model->getList();
$my_company_name_list   = $my_company_model->getNameList();

$smarty->assign("my_company_name_list", $my_company_name_list);
$smarty->assign("status_option", getWorkStateOption());
$smarty->assign("status_color_option", getWorkStateOptionColor());
$smarty->assign("is_partner_option", getSettleIsParteOption());
$smarty->assign("agency_option", $agency_option);
$smarty->assign("media_option", $media_option);
$smarty->assign("advertise_type_option", getAdvertiseTypeOption());
$smarty->assign('page_type_list', getPageTypeOption(4));
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("my_company_list", $my_company_list);
$smarty->assign("prev_settle_month", $prev_settle_month);
$smarty->assign("agency_settle_list", $agency_settle_list);

$smarty->display('agency_settlement.html');
?>
