<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/approval.php');
require('inc/helper/withdraw.php');
require('inc/model/MyQuick.php');
require('inc/model/Approval.php');
require('inc/model/Kind.php');
require('inc/model/Team.php');
require('inc/model/Leave.php');

# Model 설정
$form_model         = Approval::Factory();
$report_model       = Approval::Factory();
$report_model->setReportTable();
$leave_model        = Leave::Factory();
$leave_cal_model    = Leave::Factory();
$leave_cal_model->setCalendarTable();

# 프로세스 처리
$process    = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'cancel_approval')
{
    $ar_no      = isset($_POST['ar_no']) ? $_POST['ar_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $approval_item = $report_model->getItem($ar_no);

    if(!isset($approval_item['ar_no']) || empty($approval_item['ar_no']))
    {
        exit("<script>alert('존재하지 않는 결재입니다.');location.href='approval_report_list.php';</script>");
    }

    if($report_model->update(array("ar_no" => $ar_no, "ar_state" => 1, "lc_no" => "NULL", "req_date" => "NULL")))
    {
        mysqli_query($my_db, "UPDATE approval_report_permission SET arp_date=NULL WHERE ar_no='{$approval_item['ar_no']}' AND arp_s_no='{$session_s_no}' AND arp_state='0'");
        $leave_cal_item = $leave_cal_model->getCalendarItem($approval_item['lc_no']);
        $leave_cal_model->delete($approval_item['lc_no']);

        if(!empty($leave_cal_item) && $leave_cal_item['lsm_no']){
            $leave_model->useLeaveDay($leave_cal_item['lsm_no'], -$leave_cal_item['leave_value']);
        }

        exit("<script>alert('상신취소 됬습니다.');location.href='approval_report_list.php';</script>");
    }else{
        exit("<script>alert('상신취소에 실패했습니다.');location.href='approval_report_list.php';</script>");
    }
}
elseif($process == "mod_reading_permission")
{
    $ar_no = (isset($_POST['ar_no'])) ? $_POST['ar_no'] : "";

    $add_permission_list = [];
    $upd_permission_list = [];
    $del_permission_list = [];
    $upd_apply_list      = [];

    $origin_reading_arp_no       = isset($_POST['origin_reading_arp_no']) && !empty($_POST['origin_reading_arp_no']) ? explode(",", $_POST['origin_reading_arp_no']) : [];
    $read_permission_arp_no_list = isset($_POST['reading_arp_no']) ? $_POST['reading_arp_no'] : "";
    $read_permission_team_list   = isset($_POST['reading_permission_team']) ? $_POST['reading_permission_team'] : "";
    $read_permission_staff_list  = isset($_POST['reading_permission_staff']) ? $_POST['reading_permission_staff'] : "";
    if(!empty($read_permission_team_list))
    {
        foreach($read_permission_team_list as $key => $read_permission_team)
        {
            $read_no    = isset($read_permission_arp_no_list[$key]) ? $read_permission_arp_no_list[$key] : "";
            $read_team  = !empty($read_permission_team) ? $read_permission_team : 0;
            $read_staff = isset($read_permission_staff_list[$key]) && !empty($read_permission_staff_list[$key]) ? $read_permission_staff_list[$key] : 0;

            if(!empty($read_no))
            {
                $upd_apply_list[] = $read_no;
                $upd_permission_list[] = array(
                    'arp_no'    => $read_no,
                    'type'      => "reading",
                    'priority'  => ($key+1),
                    'team'      => $read_team,
                    'staff'     => $read_staff,
                    'state'     => 3
                );
            }else{
                $add_permission_list[] = array(
                    'type'      => "reading",
                    'priority'  => ($key+1),
                    'team'      => $read_team,
                    'staff'     => $read_staff,
                    'state'     => 3
                );
            }
        }
    }

    if(!empty($origin_reading_arp_no))
    {
        foreach($origin_reading_arp_no as $reading_arp_no)
        {
            if(in_array($reading_arp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('arp_no' => $reading_arp_no);
        }
    }

    if(!empty($add_permission_list))
    {
        $ins_per_sql = "INSERT INTO approval_report_permission(`ar_no`, `arp_type`, `arp_team`, `arp_s_no`, `arp_state`, `priority`) VALUES";
        $comma       = "";
        foreach($add_permission_list as $add_permission)
        {
            $ins_per_sql .= $comma."('{$ar_no}', '{$add_permission['type']}', '{$add_permission['team']}', '{$add_permission['staff']}', '{$add_permission['state']}', '{$add_permission['priority']}')";
            $comma = " , ";
        }

        mysqli_query($my_db, $ins_per_sql);
    }

    if(!empty($upd_permission_list))
    {
        foreach($upd_permission_list as $upd_permission)
        {
            $upd_per_sql = "UPDATE approval_report_permission SET arp_team = '{$upd_permission['team']}', arp_type='{$upd_permission['type']}', arp_s_no = '{$upd_permission['staff']}', arp_state='{$upd_permission['state']}', priority='{$upd_permission['priority']}' WHERE arp_no='{$upd_permission['arp_no']}'";
            mysqli_query($my_db, $upd_per_sql);
        }
    }

    if(!empty($del_permission_list))
    {
        foreach($del_permission_list as $del_permission)
        {
            $del_per_sql = "DELETE FROM approval_report_permission WHERE arp_no='{$del_permission['arp_no']}'";
            mysqli_query($my_db, $del_per_sql);
        }
    }

    exit("<script>alert('열람자를 변경했습니다.');location.href='approval_report_view.php?ar_no={$ar_no}';</script>");
}
elseif($process == "add_approval_comment")
{
    $ar_no 	     = (isset($_POST['ar_no'])) ? $_POST['ar_no'] : "";
    $comment_new = (isset($_POST['f_comment_new'])) ? $_POST['f_comment_new'] : "";
    $regdate     = date("Y-m-d H:i:s");

    # 코멘트 이미지 업로드
    $img_add_set = "";
    $f_comm_img  = $_FILES['f_comm_img'];
    if(isset($f_comm_img['name']) && !empty($f_comm_img['name'])){
        $img_path    = add_store_file($f_comm_img, "approval_report_comment");
        $img_name    = $f_comm_img['name'];
        $img_add_set = ", `img_path`='{$img_path}', `img_name`='{$img_name}'";
    }

    $comm_sql = "
        INSERT INTO `approval_report_comment` SET
            ar_no = '{$ar_no}',
            `comment` = '".addslashes($comment_new)."',
            team = '{$session_team}',
            s_no = '{$session_s_no}',
            regdate = '{$regdate}'
            {$img_add_set}
    ";

    if(!mysqli_query($my_db, $comm_sql)) {
        echo("<script>alert('코멘트 추가에 실패 하였습니다');history.back();</script>");
    }
    exit ("<script>location.href='approval_report_view.php?ar_no={$ar_no}';</script>");
}
elseif($process == "mod_approval_comment")
{
    $ar_no       = (isset($_POST['ar_no'])) ? $_POST['ar_no'] : "";
    $arc_no      = (isset($_POST['arc_no'])) ? $_POST['arc_no'] : "";
    $comment_val = (isset($_POST['f_comment'])) ? addslashes($_POST['f_comment']) : "";

    if(empty($arc_no)){
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }

    $comm_sql = "UPDATE `approval_report_comment` SET comment='{$comment_val}' WHERE arc_no='{$arc_no}'";

    if(!mysqli_query($my_db, $comm_sql)) {
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }else{
        exit ("<script>location.href='approval_report_view.php?ar_no={$ar_no}';</script>");
    }
}
elseif($process == "del_approval_comment")
{
    $arc_no   = (isset($_POST['arc_no'])) ? $_POST['arc_no'] : "";
    $comm_sql = "DELETE FROM `approval_report_comment` WHERE arc_no = '{$arc_no}'";

    if(mysqli_query($my_db, $comm_sql)) {
        echo("<script>alert('삭제 하였습니다');history.back();</script>");
    } else {
        echo("<script>alert('삭제에 실패 하였습니다');history.back();</script>");
    }
    exit;
}

# 변수 리스트
$team_model             = Team::Factory();
$kind_model             = Kind::Factory();
$team_all_list	        = $team_model->getTeamAllList();
$team_name_list	        = $team_all_list["team_name_list"];
$staff_all_list	        = $team_all_list["staff_list"];
$sch_staff_list         = $staff_all_list['all'];
$approval_total_list    = $kind_model->getKindChartData("approval");
$approval_g_total_list  = $approval_total_list['kind_group_name'];
$leave_type_list	    = getLeaveTypeList();
$wd_state_option        = getWdStateOption();

$ar_no                  = isset($_GET['ar_no']) ? $_GET['ar_no'] : "";
$approval_report        = [];
$approval_permission_me = [];
$approval_withdraw      = [];

if(empty($ar_no))
{
    exit("<script>alert('존재하지 않는 결재입니다.');location.href='approval_report_list.php';</script>");
}
else
{
    $approval_report_sql    = "
        SELECT 
            *,
            (SELECT k_parent FROM kind k WHERE k.k_name_code=ar.k_name_code) as appr_g1,
            (SELECT count(arr.arr_no) FROM approval_report_read arr WHERE arr.ar_no=ar.ar_no AND arr.read_s_no='{$session_s_no}' AND arr.ar_state=ar.ar_state) as read_cnt
        FROM approval_report as ar 
        WHERE ar.ar_no='{$ar_no}' AND ar.ar_state > 1
    ";
    $approval_report_query  = mysqli_query($my_db, $approval_report_sql);
    $approval_report_result = mysqli_fetch_assoc($approval_report_query);

    if(!isset($approval_report_result['ar_no']) || empty($approval_report_result['ar_no']))
    {
        exit("<script>alert('존재하지 않는 결재입니다.');location.href='approval_report_list.php';</script>");
    }

    if($approval_report_result['req_s_no'] != $session_s_no)
    {
        exit("<script>alert('열람 권한이 없습니다.');location.href='approval_report_list.php';</script>");
    }

    $form_item = $form_model->getItem($approval_report_result['af_no']);

    $approval_report_result['is_mod_permission'] = $form_item['is_modify'];
    $approval_report_result['is_withdraw']       = $form_item['is_withdraw'];
    $approval_report_result['is_calendar']       = $form_item['is_calendar'];
    $approval_report_result['title']             = $form_item['title'];
    $approval_report_result['ar_type_name']      = "{$approval_g_total_list[$approval_report_result['appr_g1']]} > {$approval_g_total_list[$approval_report_result['k_name_code']]}";

    $approval_state_html = "작성중 > ";
    switch($approval_report_result['ar_state'])
    {
        case '2':
            $approval_state_html .= "<b>진행중<span style='color: blue;'>[결재대기]</span></b> > 결재완료";
            break;
        case '3':
            $approval_state_html .= "진행중 > <b>결재완료</b>";
            break;
        case '4':
            $approval_state_html .= "<b>진행중<span style='color: red;'>[반려]</span></b> > 결재완료";
            break;
        case '5':
            $approval_state_html .= "<b>진행중<span style='color: orange;'>[보류]</span></b> > 결재완료";
            break;
        default:

    }
    $approval_report_result['ar_state_name'] = $approval_state_html;

    # 휴가처리
    if($approval_report_result['is_calendar'] == "1"){
        $approval_calendar_sql      = "SELECT * FROM approval_report_leave WHERE ar_no='{$ar_no}'";
        $approval_calendar_query    = mysqli_query($my_db, $approval_calendar_sql);
        $approval_calendar_result   = mysqli_fetch_assoc($approval_calendar_query);

        if(!empty($approval_calendar_result)){
            $approval_report_result['leave_type']   = $approval_calendar_result['leave_type'];
            $approval_report_result['leave_s_date'] = date("Y-m-d", strtotime($approval_calendar_result['leave_s_date']));
            $approval_report_result['leave_e_date'] = date("Y-m-d", strtotime($approval_calendar_result['leave_e_date']));
        }
    }

    $approval_report = $approval_report_result;

    # Permission 가져오기
    $approval_is_modify        = true;
    $approval_permission_list  = $conference_permission_list = $reading_permission_list = $reference_permission_list  = [];
    $origin_reading_arp_no     = [];
    $appr_reading_idx          = 1;
    $approval_permission_sql   = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=main.arp_s_no) as s_name, (SELECT t.team_name FROM team t WHERE t.team_code=main.arp_team) as t_name FROM approval_report_permission main WHERE main.ar_no='{$ar_no}' ORDER BY priority";
    $approval_permission_query = mysqli_query($my_db, $approval_permission_sql);
    while($approval_permission_result = mysqli_fetch_assoc($approval_permission_query))
    {
        if($approval_permission_result['arp_type'] == 'approval')
        {
            if($approval_permission_result['arp_type'] == 'approval' && $approval_permission_result['arp_state'] == '0')
            {
                $approval_permission_me = array(
                    'arp_s_name'    => $approval_permission_result['s_name'],
                    'arp_team_name' => $approval_permission_result['t_name'],
                    'arp_date' => date('m/d', strtotime($approval_permission_result['arp_date']))
                );
            }
            else
            {
                if($approval_permission_result['arp_state'] == '2')
                {
                    $approval_is_modify = false;

                    if($approval_permission_result['arp_date']){
                        $approval_permission_result['arp_date_name'] = "승인 ".date('m/d', strtotime($approval_permission_result['arp_date']));
                    }
                }
                $approval_permission_list[] = $approval_permission_result;
            }
        }
        elseif($approval_permission_result['arp_type'] == 'conference')
        {
            if($approval_permission_result['arp_state'] == '2')
            {
                $approval_is_modify = false;

                if($approval_permission_result['arp_date']){
                    $approval_permission_result['arp_date_name'] = "합의 ".date('m/d', strtotime($approval_permission_result['arp_date']));
                }
            }
            $conference_permission_list[] = $approval_permission_result;
        }
        elseif($approval_permission_result['arp_type'] == 'reading')
        {
            $reading_permission_list[] = $approval_permission_result;
            $appr_reading_idx = $approval_permission_result['priority']+1;
            $origin_reading_arp_no[] = $approval_permission_result['arp_no'];
        }
        elseif($approval_permission_result['arp_type'] == 'reference')
        {
            $reference_permission_list[] = $approval_permission_result;
        }
    }

    if($approval_report['ar_state'] != '2'){
        $approval_is_modify = false;
    }

    $approval_report['appr_reading_idx']            = $appr_reading_idx;
    $approval_report['origin_reading_arp_no']       = !empty($reading_permission_list) ? implode(",", $origin_reading_arp_no) : "";

    $approval_report['approval_permission_list']    = $approval_permission_list;
    $approval_report['conference_permission_list']  = $conference_permission_list;
    $approval_report['reading_permission_list']     = $reading_permission_list;
    $approval_report['reference_permission_list']   = $reference_permission_list;

    # 업무처리 & 발주처리
    if(!empty($approval_report['linked_no']))
    {
        if($approval_report['linked_table'] == 'work')
        {
            $work_sql = "
            SELECT 
                w.w_no,
                (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1,
                (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2,
                (SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no) as prd_name
            FROM `work` w WHERE w_no='{$approval_report['linked_no']}'";
            $work_query  = mysqli_query($my_db, $work_sql);
            $work_result = mysqli_fetch_assoc($work_query);

            if(isset($work_result['w_no']))
            {
                $approval_report['work_name'] = $work_result['k_prd1']." > ".$work_result['k_prd2']." > ".$work_result['prd_name'];
                $approval_report['work_link'] = "https://work.wplanet.co.kr/v1/work_list.php?sch_w_no={$work_result['w_no']}";
            }
        }
        elseif($approval_report['linked_table'] == 'commerce_order')
        {
            $commerce_order_sql     = "SELECT *, (SELECT cos.c_no FROM commerce_order_set cos WHERE cos.`no`=co.set_no) as c_no, (SELECT cos.c_name FROM commerce_order_set cos WHERE cos.`no`=co.set_no) as c_name, (SELECT cos.order_count FROM commerce_order_set cos WHERE cos.`no`=co.set_no) as ord_cnt FROM commerce_order co WHERE `no` ='{$approval_report['linked_no']}' AND `type`='withdraw'";
            $commerce_order_query   = mysqli_query($my_db, $commerce_order_sql);
            $commerce_order_result  = mysqli_fetch_assoc($commerce_order_query);

            $approval_report['work_name'] = "발주관리";
            $approval_report['work_link'] = "http://work.wplanet.co.kr/v1/commerce_order_regist.php?no={$commerce_order_result['set_no']}";
        }
    }

    # 출금처리
    if(!empty($approval_report['wd_no']))
    {
        $withdraw_sql    = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=wd.s_no) as s_name, (SELECT t.team_name FROM team t WHERE t.team_code=wd.team) as t_name FROM withdraw wd WHERE wd_no='{$approval_report['wd_no']}'";
        $withdraw_query  = mysqli_query($my_db, $withdraw_sql);
        $withdraw_result = mysqli_fetch_assoc($withdraw_query);

        if(isset($withdraw_result['wd_no']))
        {
            $approval_withdraw = array(
                'wd_c_name'       => $withdraw_result['c_name'],
                'wd_state'        => $wd_state_option[$withdraw_result['wd_state']],
                'wd_link'         => "/v1/withdraw_regist.php?wd_no={$withdraw_result['wd_no']}",
                'wd_manager_name' => "{$withdraw_result['s_name']} ({$withdraw_result['t_name']})",
                'wd_vat_choice'   => $withdraw_result['vat_choice'],
                'wd_title'        => $withdraw_result['bk_title'],
                'wd_account'      => $withdraw_result['bk_name'],
                'wd_num'          => $withdraw_result['bk_num'],
                'wd_vat_tax_choice'   => $withdraw_result['vat_choice'],
                'wd_cost'             => $withdraw_result['cost'],
                'wd_supply_cost'      => $withdraw_result['supply_cost'],
                'wd_supply_cost_vat'  => $withdraw_result['supply_cost_vat'],
                'wd_biz_tax'          => $withdraw_result['biz_tax'],
                'wd_local_tax'        => $withdraw_result['local_tax'],
            );
        }
    }

    # 첨부파일 처리
    $approval_file_list = [];
    if(isset($approval_report['file_path']) && !empty($approval_report['file_path']))
    {
        $file_names = explode(',', $approval_report['file_name']);
        $file_paths = explode(',', $approval_report['file_path']);
        foreach ($file_paths as $key => $file_path)
        {
            $file_origin = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$file_path}";
            $file_size   = 0;
            $file_name   = isset($file_names[$key]) ? $file_names[$key] : "";
            if(file_exists($file_origin)){
                $file_size = filesize($file_origin)/1024;
                $file_size = floor($file_size);
            }

            $approval_file_list[] = array(
                'path' => $file_path,
                'name' => $file_name,
                'size' => $file_size
            );
        }
    }
    $approval_report['file_list'] = $approval_file_list;

    # 댓글 처리
    $approval_comment_sql = "
        SELECT
            *,
            (SELECT team_name FROM team t where t.team_code=arc.team) AS t_name,
            (SELECT s_name FROM staff s where s.s_no=arc.s_no) AS s_name                
        FROM approval_report_comment arc
        WHERE ar_no = '{$ar_no}'
        ORDER BY regdate DESC
    ";

    $approval_comment_query = mysqli_query($my_db, $approval_comment_sql);
    $approval_comment_list  = [];
    while($approval_comment = mysqli_fetch_assoc($approval_comment_query))
    {
        $approval_comment['comment_value']  = str_replace("\r\n","<br />",htmlspecialchars($approval_comment['comment'])); // 코멘트 개행문자 처리
        $approval_comment['regdate_day']    = date("Y/m/d",strtotime($approval_comment['regdate']));
        $approval_comment['regdate_hour']   = date("Y/m/d",strtotime($approval_comment['regdate']));

        $approval_comment_list[] = $approval_comment;
    }

    $smarty->assign("approval_comment_list", $approval_comment_list);

    # 읽기 처리
    if($approval_report['read_cnt'] == 0){
        $read_ins_sql = "INSERT INTO approval_report_read SET ar_no='{$ar_no}', ar_state='{$approval_report['ar_state']}', read_s_no='{$session_s_no}', read_date=now()";
        mysqli_query($my_db, $read_ins_sql);
    }
}

$smarty->assign("approval_is_modify", $approval_is_modify);
$smarty->assign($approval_report);
$smarty->assign("approval_permission_me", $approval_permission_me);
$smarty->assign($approval_withdraw);
$smarty->assign("team_name_list", $team_name_list);
$smarty->assign('appr_form_team_list', $team_name_list);
$smarty->assign('appr_form_staff_list', $staff_all_list);
$smarty->assign('leave_type_list', $leave_type_list);

$smarty->display("approval_report_view.html");
?>