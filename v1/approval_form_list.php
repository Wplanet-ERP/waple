<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/approval.php');
require('inc/model/MyQuick.php');
require('inc/model/Approval.php');
require('inc/model/Kind.php');
require('inc/model/Team.php');

if(!permissionNameCheck($session_permission, '재무관리자') && !permissionNameCheck($session_permission, '마스터관리자')) {
    $smarty->display('access_error.html');
    exit;
}

# Navigation & My Quick
$nav_prd_no     = "228";
$nav_title      = "결재양식 관리";
$quick_model    = MyQuick::Factory();
$is_my_quick    = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 변수 리스트
$team_model             = Team::Factory();
$kind_model             = Kind::Factory();
$team_all_list	        = $team_model->getTeamAllList();
$team_full_name_list    = $team_model->getTeamFullNameList();
$staff_all_list	        = $team_all_list["staff_list"];
$sch_staff_list         = $staff_all_list['all'];
$approval_group_list    = $kind_model->getKindGroupList("approval");
$sch_appr_g1_list       = [];
$sch_appr_g2_list       = [];
$approval_g1_list       = [];
$approval_g2_list       = [];

foreach($approval_group_list as $key => $appr_data)
{
    if(!$key){
        $approval_g1_list = $appr_data;
    }else{
        $approval_g2_list[$key] = $appr_data;
    }
}
$sch_appr_g1_list = $approval_g1_list;

# 검색조건
$add_where      = "1=1";
$sch_appr_g1    = isset($_GET['sch_appr_g1']) ? $_GET['sch_appr_g1'] : "";
$sch_appr_g2    = isset($_GET['sch_appr_g2']) ? $_GET['sch_appr_g2'] : "";
$sch_af_no      = isset($_GET['sch_af_no']) ? $_GET['sch_af_no'] : "";
$sch_display    = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$sch_title      = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_team       = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_manager    = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";

if(!empty($sch_appr_g1)) {
    $add_where  .= " AND af.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_code='approval' AND sub.k_parent='{$sch_appr_g1}')";
    $sch_appr_g2_list = isset($approval_g2_list[$sch_appr_g1]) ? $approval_g2_list[$sch_appr_g1] : [];
}

if(!empty($sch_appr_g2)) {
    $add_where  .= " AND af.k_name_code='{$sch_appr_g2}'";
}

$smarty->assign("sch_appr_g1", $sch_appr_g1);
$smarty->assign("sch_appr_g2", $sch_appr_g2);
$smarty->assign("sch_appr_g1_list", $sch_appr_g1_list);
$smarty->assign("sch_appr_g2_list", $sch_appr_g2_list);

if(!empty($sch_af_no)) {
    $add_where  .= " AND af.af_no='{$sch_af_no}'";
    $smarty->assign("sch_af_no", $sch_af_no);
}

if(!empty($sch_display)) {
    $add_where  .= " AND af.display='{$sch_display}'";
    $smarty->assign("sch_display", $sch_display);
}

if(!empty($sch_title)) {
    $add_where  .= " AND af.title like '%{$sch_title}%'";
    $smarty->assign("sch_title", $sch_title);
}

if (!empty($sch_team))
{
    if($sch_team != 'all')
    {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        if($sch_team_code_where){
            $add_where .= " AND `af`.team IN ({$sch_team_code_where})";
        }
        $sch_staff_list = $staff_all_list[$sch_team];
    }

    $smarty->assign("sch_team",$sch_team);
}

if(!empty($sch_manager)) {
    $add_where .= " AND af.manager = '{$sch_manager}'";
    $smarty->assign("sch_manager", $sch_manager);
}

# 페이징
$approval_total_sql     = "SELECT count(af_no) as cnt FROM approval_form af WHERE {$add_where}";
$approval_total_query   = mysqli_query($my_db, $approval_total_sql);
$approval_total_result  = mysqli_fetch_assoc($approval_total_query);
$approval_total         = isset($approval_total_result['cnt']) ? $approval_total_result['cnt'] : 0;

$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 10;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($approval_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "approval_form_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $approval_total);
$smarty->assign("page_list", $page_list);

# 결재양식 리스트
$approval_list_sql = "
    SELECT 
        af.af_no,
        af.display,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=af.k_name_code)) AS k_parent_appr_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=af.k_name_code) AS k_appr_name,
        af.title,
        af.explain,
        (SELECT s.s_name FROM staff s WHERE s.s_no=af.manager) as manager_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=af.team) as team_name,
        DATE_FORMAT(af.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(af.regdate, '%H:%i') as reg_hour
    FROM approval_form as af
    WHERE {$add_where}
    ORDER BY af_no DESC
";
$approval_list_query = mysqli_query($my_db, $approval_list_sql);
$approval_list       = [];
$display_option      = getDisplayOption();
while($approval_result = mysqli_fetch_assoc($approval_list_query))
{
    $approval_result['display_name'] = $display_option[$approval_result['display']];
    $approval_list[] = $approval_result;
}

$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("display_option", $display_option);
$smarty->assign("approval_list", $approval_list);

$smarty->display('approval_form_list.html');
?>
