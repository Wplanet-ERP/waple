<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_return.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:A2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("B1:B2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("C1:C2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("D1:F1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("G1:L1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("M1:O1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("P1:P2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("Q1:Q2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("R1:R2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("S1:W1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("X1:AB1");

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "r_no\r\n작성일")
    ->setCellValue('B1', "회수요청일")
    ->setCellValue('C1', "회수 진행상태")
    ->setCellValue('D1', "이전 주문 정보")
    ->setCellValue('D2', "이전 주문번호")
    ->setCellValue('E2', "원택배사\r\n원운송장번호")
    ->setCellValue('F2', "이전 구매처")
    ->setCellValue('G1', "고객요청 회수 정보")
    ->setCellValue('G2', "고객 요청일시")
    ->setCellValue('H2', "회수 주문번호\r\n회수자명\r\n회수자 전화")
    ->setCellValue('I2', "회수지")
    ->setCellValue('J2', "업체명/브랜드명")
    ->setCellValue('K2', "상품명")
    ->setCellValue('L2', "수량")
    ->setCellValue('M1', "회수 요청")
    ->setCellValue('M2', "구분")
    ->setCellValue('N2', "요청 메세지")
    ->setCellValue('O2', "요청자")
    ->setCellValue('P1', "회수접수 택배사")
    ->setCellValue('Q1', "회수 운송장번호")
    ->setCellValue('R1', "(추가)회수 운송장번호")
    ->setCellValue('S1', "워드플레이스 처리결과")
    ->setCellValue('S2', "회수처리일")
    ->setCellValue('T2', "재고관리코드(SKU)/수량/처리구분")
    ->setCellValue('U2', "메모")
    ->setCellValue('V2', "위드처리자")
    ->setCellValue('W2', "택배비\r\n(동봉금)")
    ->setCellValue('X1', "와이즈 처리결과")
    ->setCellValue('X2', "처리확인\r\n날짜")
    ->setCellValue('Y2', "처리여부")
    ->setCellValue('Z2', "처리내용")
    ->setCellValue('AA2', "사유구분(결과)")
    ->setCellValue('AB2', "처리담당자")
;

$add_where          = "1=1";
$sch_step           = isset($_GET['sch_step']) ? $_GET['sch_step'] : "";
$sch_req_s_date 	= isset($_GET['sch_req_s_date']) ? $_GET['sch_req_s_date'] : "";
$sch_req_e_date 	= isset($_GET['sch_req_e_date']) ? $_GET['sch_req_e_date'] : "";
$sch_return_state   = isset($_GET['sch_return_state']) ? $_GET['sch_return_state'] : "";
$sch_parent_ord_no  = isset($_GET['sch_parent_ord_no']) ? $_GET['sch_parent_ord_no'] : "";
$sch_parent_dp_c_no = isset($_GET['sch_parent_dp_c_no']) ? $_GET['sch_parent_dp_c_no'] : "";
$sch_wise_dp        = isset($_GET['sch_wise_dp']) ? $_GET['sch_wise_dp'] : "";
$sch_recipient 		= isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp 	= isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_return_purpose = isset($_GET['sch_return_purpose']) ? $_GET['sch_return_purpose'] : "";
$sch_return_type    = isset($_GET['sch_return_type']) ? $_GET['sch_return_type'] : "";
$sch_req_name 		= isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : "";
$sch_run_s_date     = isset($_GET['sch_run_s_date']) ? $_GET['sch_run_s_date'] : "";
$sch_run_e_date 	= isset($_GET['sch_run_e_date']) ? $_GET['sch_run_e_date'] : "";
$sch_run_date_null  = isset($_GET['sch_run_date_null']) ? $_GET['sch_run_date_null'] : "";
$sch_with_date 	    = isset($_GET['sch_with_date']) ? $_GET['sch_with_date'] : "";
$sch_with_date_null = isset($_GET['sch_with_date_null']) ? $_GET['sch_with_date_null'] : "";
$sch_return_img	    = isset($_GET['sch_return_img']) ? $_GET['sch_return_img'] : "";
$sch_delivery_type  = isset($_GET['sch_delivery_type']) ? $_GET['sch_delivery_type'] : "";
$sch_delivery_no    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
$sch_delivery_no_null   = isset($_GET['sch_delivery_no_null']) ? $_GET['sch_delivery_no_null'] : "";
$sch_run_state      = isset($_GET['sch_run_state']) ? $_GET['sch_run_state'] : "";
$sch_run_memo       = isset($_GET['sch_run_memo']) ? $_GET['sch_run_memo'] : "";
$sch_run_name       = isset($_GET['sch_run_name']) ? $_GET['sch_run_name'] : "";
$sch_with_name      = isset($_GET['sch_with_name']) ? $_GET['sch_with_name'] : "";
$sch_quick_prd      = isset($_GET['sch_quick_prd']) ? $_GET['sch_quick_prd'] : "";
$sch_quick_mon      = isset($_GET['sch_quick_mon']) ? $_GET['sch_quick_mon'] : "";

if(!empty($sch_quick_prd) && !empty($sch_quick_prd))
{
    $add_where .= " AND r.return_state='6' AND DATE_FORMAT(r.return_date, '%Y-%m')='{$sch_quick_mon}' AND r.order_number IN(SELECT DISTINCT sub.order_number FROM work_cms_return_unit sub WHERE sub.`type` IN(1,2,3,4) AND sub.`option`='{$sch_quick_prd}')";
}

if(!empty($sch_req_s_date)){
    $add_where .= " AND r.return_req_date >= '{$sch_req_s_date}'";
}

if(!empty($sch_req_e_date)){
    $add_where .= " AND r.return_req_date <= '{$sch_req_e_date}'";
}

if(!empty($sch_return_state)){
    $add_where .= " AND r.return_state = '{$sch_return_state}'";
}

if(!empty($sch_parent_ord_no)){
    $add_where .= " AND r.parent_order_number = '{$sch_parent_ord_no}'";
}

if(!empty($sch_wise_dp)){
    $add_where .= " AND r.dp_c_no IN(2280, 5468, 5469, 5475, 5476, 5477, 5478 ,5479, 5480, 5481, 5482, 5939, 6002)";
}else{
    if(!empty($sch_parent_dp_c_no)){
        $add_where .= " AND r.dp_c_no='{$sch_parent_dp_c_no}'";
    }
}

if(!empty($sch_recipient)){
    $add_where .= " AND r.recipient like '%{$sch_recipient}%'";
}

if(!empty($sch_recipient_hp)){
    $add_where .= " AND r.recipient_hp LIKE '%{$sch_recipient_hp}%'";
}

if(!empty($sch_return_purpose)){
    $add_where .= " AND r.return_purpose = '{$sch_return_purpose}'";
}

if(isset($_GET['sch_return_type']) && $sch_return_type != ""){
    $add_where .= " AND r.return_type = '{$sch_return_type}'";
}

if(!empty($sch_req_name)){
    $add_where .= " AND r.s_no IN(SELECT s.s_no FROM staff s WHERE s.name like '%{$sch_req_name}%')";
}

if(!empty($sch_run_date_null)){
    $add_where .= " AND (r.return_date IS NULL OR r.return_date = '')";
}else{
    if(!empty($sch_run_s_date)){
        $add_where .= " AND r.return_date >='{$sch_run_s_date}'";
    }

    if(!empty($sch_run_e_date)){
        $add_where .= " AND r.return_date <='{$sch_run_e_date}'";
    }
}

if(!empty($sch_with_date_null)){
    $add_where .= " AND (r.return_with_date IS NULL OR r.return_with_date = '')";
}else{
    if(!empty($sch_with_date)){
        $add_where .= " AND r.return_with_date ='{$sch_with_date}'";
    }
}

if(!empty($sch_return_img)){
    if($sch_return_img == '1'){
        $add_where .= " AND (r.return_img_path_1 IS NOT NULL AND r.return_img_path_2 IS NOT NULL)";
    }elseif($sch_return_img == '2'){
        $add_where .= " AND (r.return_img_path_1 IS NOT NULL OR r.return_img_path_2 IS NOT NULL)";
    }elseif($sch_return_img == '3'){
        $add_where .= " AND ((r.return_img_path_1 IS NULL OR r.return_img_path_1 = '') AND r.return_img_path_2 != '') OR (r.return_img_path_1 != '' AND (r.return_img_path_2 IS NULL OR r.return_img_path_2 = ''))";
    }elseif($sch_return_img == '4'){
        $add_where .= " AND ((r.return_img_path_1 IS NULL OR r.return_img_path_1 = '') AND (r.return_img_path_2 IS NULL OR r.return_img_path_2 = ''))";
    }
}

if(!empty($sch_delivery_type)){
    $add_where .= " AND r.return_delivery_type='{$sch_delivery_type}'";
}

if(!empty($sch_delivery_no_null))
{
    $add_where .= " AND (r.return_delivery_no IS NULL OR r.return_delivery_no = '')";
}else{
    if(!empty($sch_delivery_no)){
        $add_where .= " AND (r.return_delivery_no like '%{$sch_delivery_no}%' OR r.return_delivery_no2 like '%{$sch_delivery_no}%')";
    }
}

if(!empty($sch_delivery_fee)){
    if($sch_delivery_fee == '1'){
        $add_where .= " AND r.return_delivery_fee > 0";
    }elseif($sch_delivery_fee == '2'){
        $add_where .= " AND r.return_delivery_fee = 0";
    }
}

if(!empty($sch_run_state)){
    $add_where .= " AND r.run_state = '{$sch_run_state}'";
}

if(!empty($sch_run_memo)){
    $add_where .= " AND r.run_memo like '%{$sch_run_memo}%'";
}

if(!empty($sch_run_name)){
    $add_where .= " AND r.run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_run_name}%')";
}

if(!empty($sch_with_name)){
    $add_where .= " AND r.run_with_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_with_name}%')";
}

#상세검색[상품등]
$sch_prd_g1	  = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	  = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	  = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_prd_name = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_default  = isset($_GET['sch_default']) ? $_GET['sch_default'] : "Y";
$sch_detail   = isset($_GET['sch_detail']) ? $_GET['sch_detail'] : "N";

if (!empty($sch_prd) && $sch_prd != "0") { // 상품
    $add_where .= " AND r.prd_no='".$sch_prd."'";
}else{
    if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND r.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND r.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

if(!empty($sch_prd_name)){
    $add_where .= " AND r.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.title like '%{$sch_prd_name}%')";
}

# 불량사유 검색
$sch_bad_reason_g1      = isset($_GET['sch_bad_reason_g1']) ? $_GET['sch_bad_reason_g1'] : "";
$sch_bad_reason         = isset($_GET['sch_bad_reason']) ? $_GET['sch_bad_reason'] : "";

if(!empty($sch_bad_reason_g1))
{
    $add_where .= " AND r.bad_reason IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_bad_reason_g1}' AND k.display='1')";
}

if(!empty($sch_bad_reason))
{
    $add_where .= " AND r.bad_reason = '{$sch_bad_reason}'";
}

$sch_r_no 	        = isset($_GET['sch_r_no']) ? $_GET['sch_r_no'] : "";
$sch_order_number   = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_brand_name     = isset($_GET['sch_brand_name']) ? $_GET['sch_brand_name'] : "";
$sch_return_reason  = isset($_GET['sch_return_reason']) ? $_GET['sch_return_reason'] : "";
$sch_result_reason  = isset($_GET['sch_result_reason']) ? $_GET['sch_result_reason'] : "";

if(!empty($sch_r_no)){
    $add_where .= " AND r.r_no='{$sch_r_no}'";
}

if(!empty($sch_order_number)){
    $add_where .= " AND r.order_number='{$sch_order_number}'";
}

if(!empty($sch_brand_name)){
    $add_where .= " AND r.c_name like '%{$sch_brand_name}%'";
}

if(!empty($sch_return_reason)){
    $add_where .= " AND r.return_reason='{$sch_return_reason}'";
}

if(!empty($sch_result_reason)){
    $add_where .= " AND r.result_reason='{$sch_result_reason}'";
}

#Step 03 처리
$sch_run_s_no = isset($_GET['sch_run_s_no']) ? $_GET['sch_run_s_no'] : "";
if(!empty($sch_run_s_no)){
    $add_where .= " AND r.run_s_no='{$sch_run_s_no}'";
}

// Step 날짜 체크
$step_02_01_date = $step_02_02_date = $step_05_date = $step_07_date = $step_09_date = $step_10_s_date = $step_10_e_date = $cur_date = date('Y-m-d');
$step_03_run_s_no = "";
$step_04_delivery_no = $step_04_order_number = $step_04_return_deli_no = $step_04_recipient = $step_04_recipient_hp = $step_04_zipcode = $step_04_recipient_addr = "";

if(!empty($sch_step))
{
    switch($sch_step){
        case "step02_01": $step_02_01_date = $sch_req_s_date; break;
        case "step02_02": $step_02_02_date = $sch_run_s_date; break;
        case "step03":
            $step_03_run_s_no = $sch_run_s_no;
            break;
        case "step04":
            $step_04_delivery_no    = isset($_GET['step_04_delivery_no']) ? $_GET['step_04_delivery_no'] : "";
            $step_04_return_deli_no = isset($_GET['step_04_return_deli_no']) ? $_GET['step_04_return_deli_no'] : "";
            $step_04_order_number   = isset($_GET['step_04_order_number']) ? $_GET['step_04_order_number'] : "";
            $step_04_recipient      = isset($_GET['step_04_recipient']) ? $_GET['step_04_recipient'] : "";
            $step_04_recipient_hp   = isset($_GET['step_04_recipient_hp']) ? $_GET['step_04_recipient_hp'] : "";
            $step_04_zipcode        = isset($_GET['step_04_zipcode']) ? $_GET['step_04_zipcode'] : "";
            $step_04_recipient_addr = isset($_GET['step_04_recipient_addr']) ? $_GET['step_04_recipient_addr'] : "";

            if(!empty($step_04_delivery_no)){
                $add_where .= " AND r.parent_order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery WHERE delivery_no='{$step_04_delivery_no}')";
            }

            if(!empty($step_04_return_deli_no)){
                $add_where .= " AND (r.return_delivery_no='{$step_04_return_deli_no}' OR r.return_delivery_no2='{$step_04_return_deli_no}')";
            }

            if(!empty($step_04_order_number)){
                $add_where .= " AND r.parent_order_number='{$step_04_order_number}'";
            }

            if(!empty($step_04_recipient)){
                $add_where .= " AND r.recipient LIKE '%{$step_04_recipient}%'";
            }

            if(!empty($step_04_recipient_hp)){
                $add_where .= " AND r.recipient_hp LIKE '%{$step_04_recipient_hp}%'";
            }

            if(!empty($step_04_zipcode)){
                $add_where .= " AND r.zip_code='{$step_04_zipcode}'";
            }

            if(!empty($step_04_recipient_addr)){
                $add_where .= " AND r.recipient_addr LIKE '%{$step_04_recipient_addr}%'";
            }

            break;
        case "step05":
            $step_date    = isset($_GET['step_date']) ? $_GET['step_date'] : "";
            $step_05_date = $step_date;
            $add_where   .= " AND r.return_req_date ='{$step_05_date}' AND return_state='1'";
            break;
        case "step07":
            $step_date    = isset($_GET['step_date']) ? $_GET['step_date'] : "";
            $step_07_date = $step_date;

            $add_where .= " AND r.return_req_date ='{$step_07_date}' AND (r.return_delivery_no IS NULL OR r.return_delivery_no ='')";
            break;
        case "step09_01":
            $add_where .= " AND r.return_type='2' AND (r.return_with_date IS NULL OR r.return_with_date = '')";
            break;
        case "step09_02":
            $step_date    = isset($_GET['step_date']) ? $_GET['step_date'] : "";
            $step_09_date = $step_date;

            $add_where .= " AND r.return_with_date ='{$step_09_date}'";
            break;
        case "step10":
            $step_10_s_date = $sch_req_s_date;
            $step_10_e_date = $sch_req_e_date;

            $add_where .= " AND return_type IN('2','4')";
            break;
    }
}

if($session_staff_state == '2'){
    $ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "return_date";
    $ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";
}else{
    $ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
    $ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";
}

$add_orderby    = "r.r_no DESC, r.prd_no ASC";
if(!empty($ord_type))
{
    if($ord_type_by == '1'){
        $orderby_val = "ASC";
    }else{
        $orderby_val = "DESC";
    }

    if($orderby_val == "DESC"){
        $add_orderby = "r.{$ord_type} IS NULL DESC, r.{$ord_type} {$orderby_val}, r.r_no DESC, r.prd_no ASC";
    }else{
        $add_orderby = "r.{$ord_type} {$orderby_val}, r.r_no DESC, r.prd_no ASC";
    }
}

$cms_ord_sql    = "SELECT DISTINCT r.order_number, count(r_no) as r_count FROM work_cms_return r WHERE {$add_where} AND r.order_number is not null GROUP BY r.order_number ORDER BY {$add_orderby} LIMIT 2000";
$cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);

$order_number_list  = [];
$order_count_list   = [];
$return_unit_list   = [];
$return_unit_type_option= getReturnUnitTypeOption();
while($cms_ord = mysqli_fetch_assoc($cms_ord_query)){
    $order_number_list[] =  "'".$cms_ord['order_number']."'";
    $order_count_list[$cms_ord['order_number']] = $cms_ord['r_count'];

    $return_unit_sql    = "SELECT wcr.u_no, wcr.type, wcr.option, wcr.sku, wcr.quantity FROM work_cms_return_unit wcr WHERE wcr.order_number = '{$cms_ord['order_number']}' ";
    $return_unit_query  = mysqli_query($my_db, $return_unit_sql);
    while($return_unit  = mysqli_fetch_assoc($return_unit_query))
    {
        $r_unit_type = isset($return_unit_type_option[$return_unit['type']]) ? $return_unit_type_option[$return_unit['type']] : "";
        $unit_text   = "{$return_unit['sku']}/{$return_unit['quantity']}/{$r_unit_type}";
        $return_unit_list[$cms_ord['order_number']][] = $unit_text;
    }
}

$order_numbers = implode(',', $order_number_list);

// 리스트 쿼리
$with_sql = "
	SELECT
		*,
		DATE_FORMAT(r.regdate, '%Y-%m-%d') as reg_day,
		(SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=r.dp_c_no) as dp_c_name,
		(SELECT title from product_cms prd_cms where prd_cms.prd_no=r.prd_no) as prd_name,
		(SELECT s.s_name FROM staff s WHERE s.s_no=r.req_s_no) as req_s_name,
		(SELECT s.s_name FROM staff s WHERE s.s_no=r.run_s_no) as run_s_name,
		(SELECT s.s_name FROM staff s WHERE s.s_no=r.run_with_no) as with_name
	FROM
		work_cms_return r
	WHERE {$add_where} AND r.order_number IN({$order_numbers})
	ORDER BY {$add_orderby}
    LIMIT 2000
";

$result	= mysqli_query($my_db, $with_sql);
$idx = 3;
$color_idx = 1;
if(!!$result)
{
    $return_state_option = getReturnStateOption();
    $run_state_option    = getRunStateOption();
    $return_type_option  = getReturnTypeOption();
    $return_reason_option= getReturnReasonOption();
    $lfcr                = chr(10) ;
    while($work_cms = mysqli_fetch_array($result))
    {
        $recipient_addr_val = trim($work_cms['recipient_addr']);
        $recipient_addr     = preg_replace('/\r\n|\r|\n/','',$recipient_addr_val);
        $return_state_name  = $return_state_option[$work_cms['return_state']];
        $run_state_name     = $run_state_option[$work_cms['run_state']];
        $return_type_name   = $return_type_option[$work_cms['return_type']];
        $result_reason_name = $return_reason_option[$work_cms['result_reason']];
        $return_unit_text   = implode($lfcr, $return_unit_list[$work_cms['order_number']]);
        $return_prd_count   = 0;

        if(isset($order_count_list[$work_cms['order_number']])){
            $return_prd_count = $order_count_list[$work_cms['order_number']];
            unset($order_count_list[$work_cms['order_number']]);
        }

        if($return_prd_count > 0)
        {
            $merge_idx = $idx+$return_prd_count-1;
            if($return_prd_count > 1)
            {
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A{$idx}:A{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("B{$idx}:B{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("C{$idx}:C{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("D{$idx}:D{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("E{$idx}:E{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("F{$idx}:F{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("G{$idx}:G{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("H{$idx}:H{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("I{$idx}:I{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("N{$idx}:N{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("O{$idx}:O{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("P{$idx}:P{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("Q{$idx}:Q{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("R{$idx}:R{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("S{$idx}:S{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("T{$idx}:T{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("U{$idx}:U{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("V{$idx}:V{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("W{$idx}:W{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("X{$idx}:X{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("Y{$idx}:Y{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("Z{$idx}:Z{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("AA{$idx}:AA{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("AB{$idx}:AB{$merge_idx}");
            }

            if($color_idx%2 == 0){
                $objPHPExcel->getActiveSheet()->getStyle("A{$idx}:AB{$merge_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00CCCCCC');
            }

            $color_idx++;
        }

        $delivery_sql   = "SELECT delivery_type, delivery_no FROM work_cms_delivery WHERE order_number = '{$work_cms['parent_order_number']}' GROUP BY delivery_no";
        $delivery_query = mysqli_query($my_db, $delivery_sql);
        $delivery_text  = "";
        while($delivery = mysqli_fetch_assoc($delivery_query))
        {
            $delivery_text .= !empty($delivery_text) ? $lfcr.$delivery['delivery_type'].$lfcr.$delivery['delivery_no'] : $delivery['delivery_type'].$lfcr.$delivery['delivery_no'];
        }

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $work_cms['r_no'].$lfcr.$work_cms['reg_day'])
            ->setCellValue("B{$idx}", $work_cms['return_req_date'])
            ->setCellValue("C{$idx}", $return_state_name)
            ->setCellValueExplicit("D{$idx}", $work_cms['parent_order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("E{$idx}", $delivery_text)
            ->setCellValue("F{$idx}", $work_cms['dp_c_name'])
            ->setCellValue("G{$idx}", $work_cms['return_cus_date'])
            ->setCellValue("H{$idx}", $work_cms['order_number'].$lfcr.$work_cms['recipient'].$lfcr.$work_cms['recipient_hp'])
            ->setCellValue("I{$idx}", $recipient_addr)
            ->setCellValue("J{$idx}", $work_cms['c_name'])
            ->setCellValue("K{$idx}", $work_cms['prd_name'])
            ->setCellValue("L{$idx}", $work_cms['quantity'])
            ->setCellValue("M{$idx}", $return_type_name)
            ->setCellValue("N{$idx}", $work_cms['req_memo'])
            ->setCellValue("O{$idx}", $work_cms['req_s_name'])
            ->setCellValue("P{$idx}", $work_cms['return_delivery_type'])
            ->setCellValueExplicit("Q{$idx}", $work_cms['return_delivery_no'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValueExplicit("R{$idx}", $work_cms['return_delivery_no2'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("S{$idx}", $work_cms['return_date'])
            ->setCellValue("T{$idx}", $return_unit_text)
            ->setCellValue("U{$idx}", $work_cms['return_memo'])
            ->setCellValue("V{$idx}", $work_cms['with_name'])
            ->setCellValue("W{$idx}", $work_cms['delivery_fee'])
            ->setCellValue("X{$idx}", $work_cms['return_run_date'])
            ->setCellValue("Y{$idx}", $run_state_name)
            ->setCellValue("Z{$idx}", $work_cms['run_memo'])
            ->setCellValue("AA{$idx}", $result_reason_name)
            ->setCellValue("AB{$idx}", $work_cms['run_s_name'])
        ;

        $idx++;
    }
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');

$objPHPExcel->getActiveSheet()->getStyle('A1:AB2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00A6A6A6');
$objPHPExcel->getActiveSheet()->getStyle('A1:AB2')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:AB2")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("A1:AB2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:AB2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("A3:AB{$idx}")->getFont()->setSize(7);;
$objPHPExcel->getActiveSheet()->getStyle("A3:A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("I3:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("K3:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("N3:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("T3:T{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("U3:U{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("Z3:Z{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


$objPHPExcel->getActiveSheet()->getStyle("A1:A2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("E1:E2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("H1:H2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("W1:W2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("X1:X2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("A3:A{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("E3:E{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("H3:H{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("I3:I{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("K3:K{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("N3:N{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("T3:T{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("U3:U{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("Z3:Z{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getStyle("A1:AB{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(9);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(10);


$objPHPExcel->getActiveSheet()->getStyle("A1:AB2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$excel_title = "회수리스트";
$objPHPExcel->getActiveSheet()->setTitle($excel_title);
$objPHPExcel->getActiveSheet()->getStyle("A1:AB{$idx}")->applyFromArray($styleArray);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$excel_title}.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
