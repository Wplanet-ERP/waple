<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');
require('inc/model/Custom.php');
require('inc/model/Staff.php');

$to_do_model = Custom::Factory();
$to_do_model->setMainInit("to_do_list", "td_no");
$process = (isset($_POST['process'])) ? $_POST['process'] : "";

if ($process == "f_state")
{
	$td_no	= (isset($_POST['td_no'])) ? $_POST['td_no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$to_do_model->update(array("td_no" => $td_no, "state" => $value)))
		echo "진행상태 저장에 실패 하였습니다.";
	else{
		echo "진행상태가 저장 되었습니다.";
	}
	exit;
}
elseif ($process == "f_td_important")
{
	$td_no	= (isset($_POST['td_no'])) ? $_POST['td_no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$to_do_model->update(array("td_no" => $td_no, "important" => $value))){
		echo "중요함 설정에 실패 하였습니다.";
	}else{
		if($value == '1')
			echo "중요함으로 설정 되었습니다.";
		else
			echo "중요함 설정이 해지 되었습니다.";
	}
	exit;
}
elseif ($process == "f_contents")
{
	$td_no	= (isset($_POST['td_no'])) ? $_POST['td_no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$to_do_model->update(array("td_no" => $td_no, "contents" => $value)))
		echo "할일 저장에 실패 하였습니다.";
	else{
		echo "할일이 저장 되었습니다.";
	}
	exit;
}
elseif ($process == "add_to_do_list")
{
	$td_date	= (isset($_POST['td_date'])) ? $_POST['td_date'] : "";
	$search_url	= isset($_POST['search_url']) ? $_POST['search_url'] : "";

	if (!$to_do_model->insert(array("td_date" => $td_date, "s_no" => $session_s_no))){
		exit("<script>alert('할일을 생성하지 못했습니다. 반복 발생시 관리자에게 문의해 주세요.');location.href='to_do_list_calendar.php?{$search_url}';</script>");
	}else{
		exit("<script>location.href='to_do_list_calendar.php?{$search_url}';</script>");
	}
}
elseif ($process == "del_to_do_list")
{
	$td_no		= (isset($_POST['td_no'])) ? $_POST['td_no'] : "";
	$search_url	= isset($_POST['search_url']) ? $_POST['search_url'] : "";

	if (!$to_do_model->delete($td_no)){
		exit("<script>alert('할일을 삭제하지 못했습니다. 반복 발생시 관리자에게 문의해 주세요.');location.href='to_do_list_calendar.php?{$search_url}';</script>");
	}else{
		exit("<script>location.href='to_do_list_calendar.php?{$search_url}';</script>");
	}
}

# Navigation & My Quick
$nav_prd_no  = "81";
$nav_title   = "나의 할일 (MY TO_DO_LIST)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 옵션
$staff_model		= Staff::Factory();
$sch_staff_list		= $staff_model->getStaffNameList();
$to_do_state_option	= array("1" => array("title" => "진행중", "color" => "red"), "2" => array("title" => "완료", "color" => "black"), "3" => array("title" => "보류", "color" => "#A9A9A9"), "4" => array("title" => "중단", "color" => "#A9A9A9"));
$td_important_option= array("1" => "중요(O)", "2" => "중요(X)");

# 검색쿼리
$cur_date			= date("Y-m-d");
$add_where			= "1=1";
$sch_work_state		= isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_td_important	= isset($_GET['sch_td_important']) ? $_GET['sch_td_important'] : "";
$sch_s_no			= isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : $session_s_no;
$sch_date			= isset($_GET['sch_date']) ? $_GET['sch_date'] : $cur_date;
$search_url			= getenv("QUERY_STRING");

# 요일 처리
$base_month			= date("Y-m", strtotime($sch_date));
$base_mon			= date("Ym", strtotime($sch_date));
$prev_month			= date("Y-m", strtotime("{$sch_date} -1 months"))."-01";
$next_month			= date("Y-m", strtotime("{$sch_date} +1 months"))."-01";
$base_e_day			= date("t", strtotime($sch_date));
$base_s_date		= "{$base_month}-01";
$base_e_date		= "{$base_month}-{$base_e_day}";
$first_week 		= date("N", strtotime($base_s_date));
$first_empty_week 	= $first_week%7;
$week_length 		= ($base_e_day + $first_empty_week)/7;
$week_length 		= ceil($week_length);
$calendar_date_list	= [];

# 날짜와 요일 정보 담기
$date_i = 1;
for($i = 1 ; $i <= $first_empty_week ; $i++){
	$calendar_date_list[] = array(
		'day'	=> '',
		'date'	=> '',
		'week'	=> ''
	);
}

while($date_i <= $base_e_day)
{
	$cal_day 		= sprintf('%02d', $date_i);
	$chk_date 		= $base_mon.$cal_day;
	$holiday_title 	= isset($holiday_name_list[$chk_date]) ? $holiday_name_list[$chk_date] : "";

	$calendar_date_list[] = array(
		'day'		=> $cal_day,
		'date'		=> date('Y-m-d', strtotime($chk_date)),
		'week'		=> date('w', strtotime($chk_date)),
		'holiday'	=> $holiday_title
	);
	$date_i++;
}

# 검색 처리
$add_where .= " AND DATE_FORMAT(td.td_date, '%Y-%m-%d') BETWEEN '{$base_s_date}' AND '{$base_e_date}' ";
$smarty->assign("sch_date", $sch_date);

if(!empty($sch_work_state)) {
	$add_where .= " AND td.state='{$sch_work_state}'";
	$smarty->assign("sch_work_state", $sch_work_state);
}

if(!empty($sch_td_important)) {
	$add_where .= " AND td.important='{$sch_work_state}'";
	$smarty->assign("sch_td_important", $sch_td_important);
}

if(!empty($sch_s_no)) {
	if($sch_s_no != "all"){
		$add_where .= " AND td.s_no='{$sch_s_no}'";
	}
	$smarty->assign("sch_s_no", $sch_s_no);
}

# 전체 게시물 수
$to_do_cnt_sql 		= "SELECT count(*) as cnt FROM to_do_list AS td WHERE {$add_where}";
$to_do_cnt_query 	= mysqli_query($my_db, $to_do_cnt_sql);
$to_do_cnt_result	= mysqli_fetch_array($to_do_cnt_query);
$to_do_cnt_total 	= $to_do_cnt_result['cnt'];

# 리스트 쿼리 결과
$to_do_list_sql 	= "	SELECT * FROM to_do_list AS td WHERE {$add_where} ORDER BY td_date ASC, important ASC, td_no ASC";
$to_do_list_query	= mysqli_query($my_db, $to_do_list_sql);
$to_do_list			= [];
while($to_do_list_result = mysqli_fetch_array($to_do_list_query))
{
	$to_do_list_result['contents'] 	= htmlspecialchars($to_do_list_result['contents']);
	$to_do_list[] 					= $to_do_list_result;
}

$smarty->assign("cur_date", $cur_date);
$smarty->assign("base_month", $base_month);
$smarty->assign("prev_month", $prev_month);
$smarty->assign("next_month", $next_month);
$smarty->assign("total_num", $to_do_cnt_total);
$smarty->assign("search_url", $search_url);
$smarty->assign("to_do_state_option", $to_do_state_option);
$smarty->assign("td_important_option", $td_important_option);
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("calendar_date_list", $calendar_date_list);
$smarty->assign("to_do_list", $to_do_list);

$smarty->display('to_do_list_calendar.html');

?>
