<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/company.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

$lfcr = chr(10);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "NO")
	->setCellValue('B1', "담당자{$lfcr}(부서)")
	->setCellValue('C1', "컨택구분")
	->setCellValue('D1', "컨택일자")
	->setCellValue('E1', "영업 진행 내용")
	->setCellValue('F1', "재컨택가능여부")
	->setCellValue('G1', "브랜드명")
	->setCellValue('H1', "업종(카테고리)")
	->setCellValue('I1', "제품")
	->setCellValue('J1', "대표명")
	->setCellValue('K1', "전화번호")
	->setCellValue('L1', "주소")
	->setCellValue('M1', "쇼핑몰 URL")
	->setCellValue('N1', "이메일")
	->setCellValue('O1', "비고")
;

$sch_cc_no          = isset($_GET['sch_cc_no']) ? $_GET['sch_cc_no'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_type           = isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_s_date         = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : "";
$sch_e_date         = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : "";
$sch_recontact      = isset($_GET['sch_recontact']) ? $_GET['sch_recontact'] : "";
$sch_hp1            = isset($_GET['sch_hp1']) ? $_GET['sch_hp1'] : "";
$sch_hp2            = isset($_GET['sch_hp2']) ? $_GET['sch_hp2'] : "";
$sch_email          = isset($_GET['sch_email']) ? $_GET['sch_email'] : "";
$sch_brand_company  = isset($_GET['sch_brand_company']) ? $_GET['sch_brand_company'] : "";
$warning_msg        = "";

$add_where          = "1=1 AND display='1'";

if(!empty($sch_cc_no)) {
    $add_where  .= " AND cc.cc_no = '{$sch_cc_no}'";
}

if (!empty($sch_manager_team))
{
	if ($sch_manager_team != "all") {
		$sch_team_code_where = getTeamWhere($my_db, $sch_manager_team);
		$add_where 		 .= " AND `cc`.manager_team IN({$sch_team_code_where})";
		$sch_staff_list   = $staff_team_list[$sch_manager_team];
	}
}

if (!empty($sch_manager))
{
	if ($sch_manager != "all") {
		$add_where 		 .= " AND `cc`.manager='{$sch_manager}'";
	}
}

if(!empty($sch_type)) {
    $add_where  .= " AND cc.`type` = '{$sch_type}'";
}

if(!empty($sch_s_date)){
    $sch_s_date_val = $sch_s_date."-01";
    $add_where  .= " AND cc.c_date >= '{$sch_s_date_val}'";
}

if(!empty($sch_e_date)){
    $sch_e_date_val = $sch_e_date."-01";
    $add_where  .= " AND cc.c_date <= '{$sch_e_date_val}'";
}

if(!empty($sch_recontact)) {
    $add_where  .= " AND cc.`is_recontact` = '{$sch_recontact}'";
}

if(!empty($sch_hp1) || !empty($sch_hp2))
{
    if(!empty($sch_hp1) && !empty($sch_hp2)){
        $add_where  .= " AND (REPLACE(cc.hp,'-','') LIKE '%{$sch_hp1}%' OR REPLACE(cc.hp,'-','') LIKE '%{$sch_hp2}%')";
    }elseif(!empty($sch_hp1)){
        $add_where  .= " AND REPLACE(cc.hp,'-','') LIKE '%{$sch_hp1}%'";
    }elseif(!empty($sch_hp2)){
        $add_where  .= " AND REPLACE(cc.hp,'-','') LIKE '%{$sch_hp2}%'";
    }
}

if(!empty($sch_email)) {
    $add_where  .= " AND cc.email LIKE '%{$sch_email}%'";
}

if(!empty($sch_brand_company)) {
    $add_where  .= " AND (cc.c_name LIKE '%{$sch_brand_company}%' OR cc.brand LIKE '%{$sch_brand_company}%')";
}

$contact_sql = "
    SELECT 
        *,
        (SELECT s.s_name FROM staff s WHERE s.s_no=cc.manager) as manager_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=cc.manager_team) as manager_team_name
    FROM `company_contact` as cc
    WHERE {$add_where}
    ORDER BY `cc_no` DESC
";

$idx = 2;
$contact_query      = mysqli_query($my_db, $contact_sql);
$type_option        = getContactTypeOption();

while($contact = mysqli_fetch_assoc($contact_query))
{
    $type_name = $type_option[$contact['type']];

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$idx}", $contact['cc_no'])
        ->setCellValue("B{$idx}", $contact['manager_name'].$lfcr."({$contact['manager_team_name']})")
        ->setCellValue("C{$idx}", $type_name)
        ->setCellValue("D{$idx}", $contact['c_date'])
        ->setCellValue("E{$idx}", $contact['task_run'])
        ->setCellValue("F{$idx}", $contact['c_name'])
        ->setCellValue("G{$idx}", $contact['brand'])
        ->setCellValue("H{$idx}", $contact['category'])
        ->setCellValue("I{$idx}", $contact['product'])
        ->setCellValue("J{$idx}", $contact['ceo'])
        ->setCellValue("K{$idx}", $contact['hp'])
        ->setCellValue("L{$idx}", $contact['address'])
        ->setCellValue("M{$idx}", $contact['homepage'])
        ->setCellValue("N{$idx}", $contact['email'])
        ->setCellValue("O{$idx}", $contact['notice'])
    ;

    $idx++;
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:O'.$idx)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC0C0C0');

$objPHPExcel->getActiveSheet()->getStyle("A2:O{$idx}")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("A2:O{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("A2:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("B1:B{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("M2:M{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);

$objPHPExcel->getActiveSheet()->setTitle("파트너 컨택리스트");


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_파트너 컨택리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
