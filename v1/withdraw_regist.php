<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/withdraw.php');
require('inc/model/Corporation.php');
require('inc/model/Company.php');
require('inc/model/Custom.php');
require('inc/model/MyCompany.php');
require('inc/model/Withdraw.php');

# 접근 권한
if (!(permissionNameCheck($session_permission, "마케터") || permissionNameCheck($session_permission, "대표") || permissionNameCheck($session_permission, "재무관리자") || permissionNameCheck($session_permission, "외주관리자"))){
	$smarty->display('access_error.html');
	exit;
}

# Process 처리
$process 		= isset($_POST['process']) ? $_POST['process'] : "";
$company_model	= Company::Factory();
$withdraw_model	= Withdraw::Factory();
$comment_model	= Custom::Factory();
$comment_model->setMainInit("linked_comment", "no");

# Form Action 처리
if ($process == "tx_info")
{
	$chk_c_no 		= isset($_POST['c_no']) ? $_POST['c_no'] : "";
	$company_item 	= $company_model->getItem($chk_c_no);

	$company_item['tx_company_number'] 	= stripslashes($company_item['tx_company_number']);
	$company_item['tx_company_ceo'] 	= stripslashes($company_item['tx_company_ceo']);
	$company_item['o_registration'] 	= stripslashes($company_item['o_registration']);
	$company_item['o_accdoc'] 			= stripslashes($company_item['o_accdoc']);
	$company_item['o_etc'] 				= stripslashes($company_item['o_etc']);

	exit(json_encode($company_item));
}
elseif ($process == "down_file")
{
	$chk_c_no  = (isset($_POST['c_no']))?$_POST['c_no']:"";
	$chk_wd_no = (isset($_POST['wd_no']))?$_POST['wd_no']:"";
	$chk_kind  = (isset($_POST['kind']))?$_POST['kind']:"";

	$company_item 	= $company_model->getItem($chk_c_no);
	$withdraw_item 	= $withdraw_model->getItem($chk_wd_no);
	$file_name		= "";
	$file_path		= "";
	switch($chk_kind)
	{
		case "registration":
			$file_name = stripslashes($company_item['o_registration']);
			$file_path = $company_item['r_registration'];
			break;
		case "accdoc":
			$file_name = stripslashes($company_item['o_accdoc']);
			$file_path = $company_item['r_accdoc'];
			break;
		case "etc":
			$file_name = stripslashes($company_item['o_etc']);
			$file_path = $company_item['r_etc'];
			break;
		case "withdraw":
			$file_name 	= stripslashes($withdraw_item['file_origin']);
			$file_path 	= $withdraw_item['file_read'];
			break;
	}

	echo "File UP Name : {$file_path}<br>";
	echo "File Downloading...<br>File DN Name : {$file_name}<br>";
	exit("<script>location.href='popup/file_download.php?file_dn_name=".urlencode($file_name)."&file_up_name={$file_path}';</script>");
}
elseif ($process == "del_file")
{
	$chk_wd_no 		= (isset($_POST['wd_no']))?$_POST['wd_no']:"";
	$chk_kind 		= (isset($_POST['kind']))?$_POST['kind']:"";
	$withdraw_item	= $withdraw_model->getItem($chk_wd_no);
	$del_file_data	= [];
	$file_path 		= "";

    if($chk_kind == "withdraw") {
	    if(!empty($withdraw_item['file_read'])) {
			$file_path  	= $withdraw_item['file_read'];
			$del_file_data 	= array("wd_no" => $chk_wd_no, "file_origin" => "NULL", "file_read" => "NULL");
		}
    }

    if(!empty($del_file_data))
    {
    	if($withdraw_model->update($del_file_data)){
			del_file($file_path);
			exit("<script>alert('삭제하였습니다');history.back();</script>");
		}
	}

	exit("<script>alert('삭제에 실패했습니다');history.back();</script>");
}
elseif ($process == "write") # 출금등록
{
	$supply_cost_value 		= str_replace(",","",trim($_POST['supply_cost']));
	$supply_cost_vat_value 	= str_replace(",","",trim($_POST['supply_cost_vat']));
	$biz_tax_value 			= str_replace(",","",trim($_POST['biz_tax']));
	$local_tax_value 		= str_replace(",","",trim($_POST['local_tax']));
	$cost_value 			= str_replace(",","",trim($_POST['cost']));
	$wd_money_value 		= str_replace(",","",trim($_POST['wd_money']));
	$regdate				= date("Y-m-d H:i:s");
	$search_url 			= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$withdraw_ins_data 	= array(
		"my_c_no" 			=> $_POST['my_c_no'],
		"wd_account" 		=> $_POST['wd_account'],
		"wd_subject" 		=> addslashes($_POST['wd_subject']),
		"wd_state" 			=> $_POST['wd_state'],
		"wd_method" 		=> $_POST['wd_method'],
		"c_no" 				=> $_POST['c_no'],
		"c_name" 			=> addslashes($_POST['c_name']),
		"s_no" 				=> $_POST['s_no'],
		"team" 				=> $_POST['team'],
		"vat_choice" 		=> $_POST['vat_choice'],
		"supply_cost" 		=> $supply_cost_value,
		"supply_cost_vat" 	=> $supply_cost_vat_value,
		"biz_tax" 			=> $biz_tax_value,
		"local_tax" 		=> $local_tax_value,
		"cost" 				=> $cost_value,
		"wd_money" 			=> $wd_money_value,
		"wd_tax_state" 		=> $_POST['wd_tax_state'],
		"regdate" 			=> $regdate,
		"reg_s_no" 			=> $_POST['reg_s_no'],
		"reg_team" 			=> $_POST['reg_team'],
		"bk_title" 			=> $_POST['bk_title'],
		"bk_name" 			=> $_POST['bk_name'],
		"bk_num" 			=> $_POST['bk_num'],
		"wd_date" 			=> !empty($_POST['wd_date']) ? $_POST['wd_date'] : "NULL",
		"wd_tax_date" 		=> !empty($_POST['wd_tax_date']) ? $_POST['wd_tax_date'] : "NULL",
	);

	if(!empty($_POST['cost_info'])){
		$withdraw_ins_data["cost_info"] = addslashes($_POST['cost_info']);
	}

	if(!empty($_POST['wd_tax_item'])){
		$withdraw_ins_data["wd_tax_item"] = addslashes($_POST['wd_tax_item']);
	}

	if(isset($_POST['wd_detail'])){
		$withdraw_ins_data["wd_detail"] = addslashes($_POST['wd_detail']);
	}

	#파일첨부
	$withdraw_file = $_FILES["withdraw_file"];
	if($withdraw_file)
	{
		$file_path = add_store_file($withdraw_file, "withdraw");
		$file_name = addslashes($withdraw_file['name']);

		if(!empty($file_path)) {
			$withdraw_ins_data['file_read']   = $file_path;
			$withdraw_ins_data['file_origin'] = $file_name;
		}
	}

	if($_POST['wd_state'] == 3){
		$withdraw_ins_data['run_s_no'] = $session_s_no;
		$withdraw_ins_data['run_team'] = $session_team;
	}

	if($withdraw_model->insert($withdraw_ins_data)) {
		exit("<script>alert('등록하였습니다');location.href='withdraw_list.php?{$search_url}';</script>");
	} else {
		exit("<script>alert('등록에 실패 하였습니다');history.back();</script>");
	}
}
elseif ($process == "modify") # 출금수정
{
	$wd_no 					= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$search_url 			= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$supply_cost_value 		= str_replace(",","",trim($_POST['supply_cost']));
	$supply_cost_vat_value 	= str_replace(",","",trim($_POST['supply_cost_vat']));
	$biz_tax_value 			= str_replace(",","",trim($_POST['biz_tax']));
	$local_tax_value 		= str_replace(",","",trim($_POST['local_tax']));
	$cost_value 			= str_replace(",","",trim($_POST['cost']));
	$wd_money_value 		= str_replace(",","",trim($_POST['wd_money']));

	$withdraw_upd_data 		= array(
		"wd_no"				=> $wd_no,
		"my_c_no" 			=> $_POST['my_c_no'],
		"wd_account" 		=> $_POST['wd_account'],
		"wd_subject" 		=> addslashes($_POST['wd_subject']),
		"wd_state" 			=> $_POST['wd_state'],
		"wd_method" 		=> $_POST['wd_method'],
		"c_no" 				=> $_POST['c_no'],
		"c_name" 			=> addslashes($_POST['c_name']),
		"s_no" 				=> $_POST['s_no'],
		"team" 				=> $_POST['team'],
		"vat_choice" 		=> $_POST['vat_choice'],
		"supply_cost" 		=> $supply_cost_value,
		"supply_cost_vat" 	=> $supply_cost_vat_value,
		"biz_tax" 			=> $biz_tax_value,
		"local_tax" 		=> $local_tax_value,
		"cost" 				=> $cost_value,
		"wd_money" 			=> $wd_money_value,
		"wd_tax_state"		=> $_POST['wd_tax_state'],
		"reg_s_no" 			=> $_POST['reg_s_no'],
		"reg_team" 			=> $_POST['reg_team'],
		"bk_title" 			=> $_POST['bk_title'],
		"bk_name" 			=> $_POST['bk_name'],
		"bk_num" 			=> $_POST['bk_num'],
		"wd_date" 			=> !empty($_POST['wd_date']) ? $_POST['wd_date'] : "NULL",
		"wd_tax_date" 		=> !empty($_POST['wd_tax_date']) ? $_POST['wd_tax_date'] : "NULL",
	);

	if(!empty($_POST['incentive_date'])){
		$withdraw_upd_data["incentive_date"] = $_POST['incentive_date'];
	}

	if(!empty($_POST['cost_info'])){
		$withdraw_upd_data["cost_info"] = addslashes($_POST['cost_info']);
	}

	if(!empty($_POST['wd_tax_item'])){
		$withdraw_upd_data["wd_tax_item"] = addslashes($_POST['wd_tax_item']);
	}

	if(isset($_POST['wd_detail'])){
		$withdraw_upd_data["wd_detail"] = addslashes($_POST['wd_detail']);
	}

	#파일첨부
	$withdraw_file = $_FILES["withdraw_file"];
	if($withdraw_file)
	{
		$file_path = add_store_file($withdraw_file, "withdraw");
		$file_name = addslashes($withdraw_file['name']);

		if(!empty($file_path)) {
			$withdraw_upd_data['file_read']   = $file_path;
			$withdraw_upd_data['file_origin'] = $file_name;
		}
	}

	if($_POST['wd_state'] == 3){
		$withdraw_upd_data['run_s_no'] = $session_s_no;
		$withdraw_upd_data['run_team'] = $session_team;
	}

	if($withdraw_model->update($withdraw_upd_data)) {
		exit("<script>alert('수정하였습니다');location.href='withdraw_list.php?{$search_url}';</script>");
	} else {
		exit("<script>alert('수정에 실패 하였습니다');history.back();</script>");
	}
}
elseif ($process == "save_company") #업체저장
{
	$wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$upd_data	= array(
		"wd_no" 	=> $wd_no,
		"c_no" 		=> $_POST['c_no'],
		"c_name" 	=> addslashes($_POST['c_name']),
		"s_no" 		=> $_POST['s_no'],
		"team" 		=> $_POST['team'],
	);

	if(!$withdraw_model->update($upd_data)){
		exit("<script>alert('업체정보와 담당자정보 저장에 실패 하였습니다');>history.back();</script>");
	}else{
		exit("<script>alert('업체정보와 담당자정보를 저장하였습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
	}
}
elseif ($process == "add_comment")
{
	$wd_no 			= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$comment_new 	= (isset($_POST['comment_new'])) ? addslashes($_POST['comment_new']) : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$ins_data 		= array(
		"linked_table"	=> "withdraw",
		"linked_no"		=> $wd_no,
		"comment"		=> $comment_new,
		"team"			=> $session_team,
		"s_no"			=> $session_s_no,
		"regdate"		=> date("Y-m-d H:i:s")

	);

	$comment_file = $_FILES["comm_file"];
	if($comment_file['tmp_name'])
	{
		$file_path = add_store_file($comment_file, "linked_comment");

		if(!empty($file_path)) {
			$ins_data['file_path'] = $file_path;
		}

		if(!empty($comment_file["name"])) {
			$ins_data['file_name'] = $comment_file["name"];
		}
	}

	if($comment_model->insert($ins_data)){
		exit ("<script>alert('코멘트 등록에 성공했습니다.'); location.href='withdraw_regist.php?wd_no={$wd_no}&{$search_url}';</script>");
	}else{
		exit ("<script>alert('코멘트 등록에 실패했습니다.'); location.href='withdraw_regist.php?wd_no={$wd_no}&{$search_url}';</script>");
	}
}
elseif ($process == "del_comment")
{
	$wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$comment_no = (isset($_POST['comment_no'])) ? $_POST['comment_no'] : "";
	$search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$upd_data	= array("no" => $comment_no, "display" => "2");

	if($comment_model->update($upd_data)){
		exit ("<script>alert('코멘트 삭제에 성공했습니다.'); location.href='withdraw_regist.php?wd_no={$wd_no}&{$search_url}';</script>");
	}else{
		exit ("<script>alert('코멘트 삭제에 실패했습니다.'); location.href='withdraw_regist.php?wd_no={$wd_no}&{$search_url}';</script>");
	}
}

# mode 페이지 설정
$wd_no		= isset($_GET['wd_no']) ? $_GET['wd_no'] : "";
$mode		= (!$wd_no) ? "write" : "modify";
$submit_btn = ($mode == "write") ? "등록하기" : "수정하기";

# 리스트 페이지 검색조건
$sch_array_key	= explode("&", getenv("QUERY_STRING"));
foreach($sch_array_key as $sch_key => $sch_val) {
	if(strpos($sch_val, "wd_no=") !== false) {
		unset($sch_array_key[$sch_key]);
	}
}
$search_url = implode("&", $sch_array_key);
$smarty->assign("search_url", $search_url);
$smarty->assign("submit_btn", $submit_btn);

if($mode == "modify")
{
	$withdraw_sql="
		SELECT
			wd.*,
			(SELECT s_name FROM staff s WHERE s.s_no=wd.s_no) AS s_name,
			(SELECT depth FROM team t WHERE t.team_code=wd.team) AS t_depth,
			(SELECT s_name FROM staff s WHERE s.s_no=wd.reg_s_no) AS reg_s_name,
			(SELECT depth FROM team t WHERE t.team_code=wd.reg_team) AS reg_t_depth,
			c.tx_company,
			c.tx_company_number,
			c.tx_company_ceo,
			c.o_registration,
			c.r_registration,
			c.o_accdoc,
			c.r_accdoc,
			c.o_etc,
			c.r_etc
		FROM withdraw wd
		LEFT OUTER JOIN company c on wd.c_no = c.c_no
		WHERE wd.wd_no='{$wd_no}'
	";

	$withdraw_result = mysqli_query($my_db, $withdraw_sql);
	$withdraw 		 = mysqli_fetch_array($withdraw_result);

	$t_label 	 	= getTeamFullName($my_db, $withdraw['t_depth'], $withdraw['team']);
	$reg_t_label 	= getTeamFullName($my_db, $withdraw['reg_t_depth'], $withdraw['reg_team']);

	$s_label 		= ($t_label) ? "{$withdraw['s_name']} ({$t_label})" : "";
	$reg_s_label 	= ($reg_t_label) ? "{$withdraw['reg_s_name']} ({$reg_t_label})" : "";

	$withdraw['wd_subject'] 		= stripslashes($withdraw['wd_subject']);
	$withdraw['c_name'] 			= stripslashes($withdraw['c_name']);
	$withdraw['cost_info'] 			= stripslashes($withdraw['cost_info']);
	$withdraw['wd_tax_item'] 		= stripslashes($withdraw['wd_tax_item']);
	$withdraw['file_origin'] 		= stripslashes($withdraw['file_origin']);
	$withdraw['tx_company_number'] 	= stripslashes($withdraw['tx_company_number']);
	$withdraw['tx_company_ceo'] 	= stripslashes($withdraw['tx_company_ceo']);
	$withdraw['o_registration'] 	= stripslashes($withdraw['o_registration']);
	$withdraw['o_accdoc'] 			= stripslashes($withdraw['o_accdoc']);
	$withdraw['o_etc'] 				= stripslashes($withdraw['o_etc']);
	$withdraw['s_name'] 			= $s_label;
	$withdraw['supply_cost'] 		= number_format($withdraw['supply_cost']);
	$withdraw['supply_cost_vat']	= number_format($withdraw['supply_cost_vat']);
	$withdraw['local_tax'] 			= number_format($withdraw['local_tax']);
	$withdraw['biz_tax'] 			= number_format($withdraw['biz_tax']);
	$withdraw['cost'] 				= number_format($withdraw['cost']);
	$withdraw['wd_money'] 			= number_format($withdraw['wd_money']);

	$smarty->assign($withdraw);

	$check_msg 	= "";
	$check 		= array();

	if (!$withdraw['o_registration'])
		$check[] = "사업자등록증";
	if (!$withdraw['o_accdoc'])
		$check[] = "통장사본";

	if (count($check) > 0) {
		$msg = implode(", ", $check);
		$check_msg = "자료없음 (" . $msg . ")";
	}

	$smarty->assign("check_msg", $check_msg);

	# Comment
	$linked_comment_sql 	= "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=lc.s_no) as s_name, (SELECT t.team_name FROM team t WHERE t.team_code=lc.team) as t_name  FROM linked_comment lc WHERE lc.linked_table='withdraw' AND lc.linked_no='{$wd_no}' AND lc.display=1";
	$linked_comment_query	= mysqli_query($my_db, $linked_comment_sql);
	$linked_comment_list 	= [];
	while($linked_comment = mysqli_fetch_assoc($linked_comment_query))
	{
		$linked_comment['comment'] = str_replace("\r\n","<br />", htmlspecialchars($linked_comment['comment']));
		$linked_comment_list[] = $linked_comment;
	}
	$smarty->assign("linked_comment_list", $linked_comment_list);
}
elseif($mode == 'write')
{
	$reg_t_label = getTeamFullName($my_db, 0, $session_team);
	$reg_s_label = "{$session_name} ({$reg_t_label})";

	$smarty->assign("my_c_no", 1);
	$smarty->assign("vat_choice", 1);
	$smarty->assign("reg_s_no", $session_s_no);
	$smarty->assign("reg_team", $session_team);
	$smarty->assign("reg_s_name", $reg_s_label);
}

$my_company_model 		= MyCompany::Factory();
$corp_model 			= Corporation::Factory();
$my_company_list		= $my_company_model->getList();
$my_company_name_list	= $my_company_model->getNameList();
$corp_account_option	= $corp_model->getAccountList('2');

$smarty->assign("mode", $mode);
$smarty->assign("my_company_list", $my_company_list);
$smarty->assign("my_company_name_list", $my_company_name_list);
$smarty->assign("wd_state_option", getWdStateOption());
$smarty->assign("wd_method_option", getWdMethodOption());
$smarty->assign("wd_tax_state_option", getWdTaxStateOption());
$smarty->assign("wd_vat_choice_option", getWdVatChoiceOption());
$smarty->assign("corp_account_option", $corp_account_option);

$smarty->display('withdraw_regist.html');
?>
