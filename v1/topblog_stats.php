<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "135";
$nav_title   = "탑블로그 통계정보";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);


$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);


//조회기간 시작날짜(현재날짜 -12개월)와 현재날짜 가져오기
$search_end_month = date("Y/m");
$search_start_month = date("Y/m", strtotime("-12 month"));

$smarty->assign("ssmonth",$search_start_month);
$smarty->assign("semonth",$search_end_month);


// 직원가져오기
$staff_sql="SELECT s_no,s_name FROM staff WHERE staff_state < '3' AND permission LIKE '1______'";
$staff_query=mysqli_query($my_db,$staff_sql);

while($staff_data=mysqli_fetch_array($staff_query)) {
	$staffs[]=array(
			'no'=>$staff_data['s_no'],
			'name'=>$staff_data['s_name']
	);
	$smarty->assign("staff",$staffs);
}

// 지역 분류 가져오기
$sql="select * from kind where k_code='location' and k_parent is null";
$query=mysqli_query($my_db,$sql);
while($result=mysqli_fetch_array($query)) {
	$location[]=array(
		"location_name"=>trim($result['k_name']),
		"location_code"=>trim($result['k_name_code'])
	);
	$smarty->assign("location",$location);
}

// 2차 분류에 값이 있는 경우 2차 지역 분류 가져오기(2차)
if($search_slocation2 != ""){
	$sql="select k_name,k_name_code,k_parent from kind where k_code='location' and k_parent<>''";
	$query=mysqli_query($my_db,$sql);
	//echo "<br>".$sql;
	while($result=mysqli_fetch_array($query)) {
		$editlocation[]=array(
			"location_name"=>trim($result['k_name']),
			"location_code"=>trim($result['k_name_code']),
			"location_parent"=>trim($result['k_parent'])
		);
		$smarty->assign("editlocation",$editlocation);
	}
}

/* 테이블과 where 조건을 토대로 쿼리값을 불러옴 */
function promotion_sql($column_f, $table_name_f, $add_where_f){
	$sql_f="
		SELECT
			$column_f
		FROM
			$table_name_f
		WHERE
			$add_where_f
	";
	return $sql_f;
}


$promotion_total; //총 진행건수
$promotion_end; //총 진행마감 p_state = '5'
$promotion_pause; //총 진행중단 p_state1 = '1'
$promotion_ongoing; //총 진행중 p_state = '4'
$promotion_recruiting; //총 모집중 p_state = '3'
$promotion_accepting; //총 접수중 p_state = '2' || 1'1


/* 총 진행건수 */
$sql=promotion_sql("COUNT(*)","promotion p","p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_total = $data[0];

$sql=promotion_sql("COUNT(*)","promotion_old p","p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_total += $data[0];

$smarty->assign("p_total",$promotion_total);

/* 총 진행마감 */
$sql=promotion_sql("COUNT(*)","promotion p","p.p_state = '5' AND p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_end = $data[0];

$sql=promotion_sql("COUNT(*)","promotion_old p","p.p_state = '5' AND p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_end += $data[0];

$smarty->assign("p_end",$promotion_end);

/* 총 진행중단 */
$sql=promotion_sql("COUNT(*)","promotion p","p.p_state1 = '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_pause = $data[0];

$sql=promotion_sql("COUNT(*)","promotion_old p","p.p_state1 = '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_pause += $data[0];

$smarty->assign("p_pause",$promotion_pause);

/* 총 진행중 */
$sql=promotion_sql("COUNT(*)","promotion p","p.p_state = '4' AND p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_ongoing = $data[0];

$sql=promotion_sql("COUNT(*)","promotion_old p","p.p_state = '4' AND p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_ongoing += $data[0];

$smarty->assign("p_ongoing",$promotion_ongoing);

/* 총 모집중 */
$sql=promotion_sql("COUNT(*)","promotion p","p.p_state = '3' AND p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_recruiting = $data[0];

$sql=promotion_sql("COUNT(*)","promotion_old p","p.p_state = '3' AND p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_recruiting += $data[0];

$smarty->assign("p_recruiting",$promotion_recruiting);

/* 총 접수중 */
$sql=promotion_sql("COUNT(*)","promotion p","(p.p_state = '1' OR p.p_state = '2') AND p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_accepting = $data[0];

$sql=promotion_sql("COUNT(*)","promotion_old p","(p.p_state = '1' OR p.p_state = '2') AND p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_accepting += $data[0];

$smarty->assign("p_accepting",$promotion_accepting);



$promotion_total_people; //총 모집인원 column = SUM(reg_num)
$promotion_recruit_people; //총 신청자수 table = application
$promotion_recruit_rate; // 총 평균 신청율
$promotion_cancel_people; //총 취소수 table = application, a_state ='3'
$promotion_select_people; //총 선정수 table = application, a_state ='3' OR a_state ='1'
$promotion_cancel_rate; // 총 평균 취소율

/* 총 모집인원 */
$sql=promotion_sql("SUM(reg_num)","promotion p","p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_total_people = $data[0];

$sql=promotion_sql("SUM(reg_num)","promotion_old p","p.p_state1 != '1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_total_people += $data[0];

$smarty->assign("p_total_people",$promotion_total_people);

/* 총 신청자수 */
$sql=promotion_sql("COUNT(*)","application a","1=1");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_recruit_people = $data[0];

$sql=promotion_sql("COUNT(*)","application_old","1=1");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_recruit_people += $data[0];

$smarty->assign("p_recruit_people",$promotion_recruit_people);


/* 총 평균신청율 */
$promotion_recruit_rate = number_format($promotion_recruit_people/$promotion_total_people*100, 2, ',', ' ');

$smarty->assign("p_recruit_rate",$promotion_recruit_rate);


/* 총 취소수 */
$sql=promotion_sql("COUNT(*)","application a","a_state ='3'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_cancel_people = $data[0];

$sql=promotion_sql("COUNT(*)","application_old","a_state ='3'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_cancel_people += $data[0];

$smarty->assign("p_cancel_people",$promotion_cancel_people);


/* 총 선정수 */
$sql=promotion_sql("COUNT(*)","application a","a_state ='3' OR a_state ='1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_select_people = $data[0];

$sql=promotion_sql("COUNT(*)","application_old","a_state ='3' OR a_state ='1'");
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);

$promotion_select_people += $data[0];


/* 총 평균취소율 */
$promotion_cancel_rate = number_format($promotion_cancel_people/$promotion_select_people*100, 2, ',', ' ');

$smarty->assign("p_cancel_rate",$promotion_cancel_rate);



//월별 통계 불러오기
$search_month=isset($_GET['smonth'])?$_GET['smonth']:date("Y/m", strtotime("-1 month"));
$smarty->assign("smonth",$search_month);


$promotion_m_total; //월 진행건수
$promotion_m_total_people; //월 모집인원 column = SUM(reg_num)
$promotion_m_recruit_people; //월 신청자수 table = application
$promotion_m_ecruit_rate; // 월 신청율
$promotion_m_cancel_people; //월 취소자수 table = application, a_state ='3'
$promotion_m_select_people; //월 선정수 table = application, a_state ='3' OR a_state ='1'
$promotion_m_cancel_rate; // 월 취소율

$sql_where_month = "DATE_FORMAT(p.reg_sdate, '%Y-%m') BETWEEN DATE_FORMAT('".$search_month."/01', '%Y-%m') AND DATE_FORMAT(DATE_ADD(DATE_ADD('".$search_month."/01',INTERVAL 1 MONTH),INTERVAL -1 DAY), '%Y-%m')";


/* 총 진행건수 */
if($search_month >= "2016/02"){
	$sql=promotion_sql("COUNT(*)","promotion p",$sql_where_month." AND p.p_state1 != '1'");
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);
	$promotion_m_total = $data[0];
}

if($search_month <= "2016/02"){
	$sql=promotion_sql("COUNT(*)","promotion_old p",$sql_where_month." AND p.p_state1 != '1'");
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);

	$promotion_m_total += $data[0];
}

$smarty->assign("p_m_total",$promotion_m_total);


// 월 모집인원
if($search_month >= "2016/02"){
	$sql=promotion_sql("SUM(reg_num)","promotion p",$sql_where_month);
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);

	$promotion_m_total_people = $data[0];
}

if($search_month <= "2016/02"){
	$sql=promotion_sql("SUM(reg_num)","promotion_old p",$sql_where_month);
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);

	$promotion_m_total_people += $data[0];
}

$smarty->assign("p_m_total_people",$promotion_m_total_people);


$sql=promotion_sql("COUNT(*)","application a LEFT JOIN promotion p ON a.p_no=p.p_no ",$sql_where_month);

// 월 신청자수
if($search_month >= "2016/02"){
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);

	$promotion_m_recruit_people = $data[0];
}

if($search_month <= "2016/02"){
	$sql=promotion_sql("COUNT(*)","application_old a LEFT JOIN promotion_old p ON a.p_no=p.p_no ",$sql_where_month);
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);

	$promotion_m_recruit_people += $data[0];
}

$smarty->assign("p_m_recruit_people",$promotion_m_recruit_people);


// 월 신청율
$promotion_m_recruit_rate = number_format($promotion_m_recruit_people/$promotion_m_total_people*100, 2, ',', ' ');

$smarty->assign("p_m_recruit_rate",$promotion_m_recruit_rate);


// 월 취소수
if($search_month >= "2016/02"){
	$sql=promotion_sql("COUNT(*)","application a LEFT JOIN promotion p ON a.p_no=p.p_no ",$sql_where_month." AND a.a_state ='3' AND p.p_state1 != '1'");
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);

	$promotion_m_cancel_people = $data[0];
}

if($search_month <= "2016/02"){
	$sql=promotion_sql("COUNT(*)","application_old a LEFT JOIN promotion_old p ON a.p_no=p.p_no ",$sql_where_month." AND a.a_state ='3' AND p.p_state1 != '1'");
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);

	$promotion_m_cancel_people += $data[0];
}

$smarty->assign("p_m_cancel_people",$promotion_m_cancel_people);

// 월 선정수
if($search_month >= "2016/02"){
	$sql=promotion_sql("COUNT(*)","application a LEFT JOIN promotion p ON a.p_no=p.p_no ",$sql_where_month." AND (a.a_state ='3' OR a.a_state ='1') ");
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);
	//echo $sql."<br>";
	$promotion_m_select_people = $data[0];
}

if($search_month <= "2016/02"){
	$sql=promotion_sql("COUNT(*)","application_old a LEFT JOIN promotion_old p ON a.p_no=p.p_no ",$sql_where_month." AND (a.a_state ='3' OR a.a_state ='1') ");
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);
	//echo $sql."<br>";
	$promotion_m_select_people += $data[0];
}

$smarty->assign("p_m_select_people",$promotion_m_select_people);

// 총 평균취소율
$promotion_m_cancel_rate = number_format($promotion_m_cancel_people/$promotion_m_select_people*100, 2, ',', ' ');

$smarty->assign("p_m_cancel_rate",$promotion_m_cancel_rate);
$smarty->display('topblog_stats.html');

?>
