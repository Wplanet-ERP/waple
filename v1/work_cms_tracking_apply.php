<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '3000');
ini_set('memory_limit', '5G');

require('inc/common.php');
require('ckadmin.php');

$billing_date       = (isset($_POST['billing_date'])) ? $_POST['billing_date'] : "";
$tracking_sql       = "SELECT track_type, order_number, delivery_no, total_fee, deducted_fee FROM work_cms_tracking WHERE billing_date='{$billing_date}' AND (order_number IS NOT NULL AND order_number != '') GROUP BY delivery_no";
$tracking_query     = mysqli_query($my_db, $tracking_sql);
$tracking_list      = [];
$dp_company_list    = array(2005,2007,2008,5469,5494,5127,5128,5134,5135,5475,5476,5477,5478,5479,5482,5939,6002);
while($tracking_result = mysqli_fetch_assoc($tracking_query))
{
    $track_type     =  $tracking_result['track_type'];
    $delivery_no    =  $tracking_result['delivery_no'];
    $order_number   =  $tracking_result['order_number'];
    $cal_price      =  $tracking_result['total_fee']-$tracking_result['deducted_fee'];

    $tracking_list[$delivery_no] = array(
        "type"      => $track_type,
        "dp_c_no"   => 0,
        "qty"       => 0,
        "price"     => $cal_price,
        "prd_fee"   => 0
    );

    if($track_type == '1')
    {
        $order_sql      = "SELECT quantity, unit_price, dp_c_no FROM work_cms WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery WHERE delivery_no='{$delivery_no}')";
        $order_query    = mysqli_query($my_db, $order_sql);
        while($order = mysqli_fetch_assoc($order_query))
        {
            $tracking_list[$delivery_no]["dp_c_no"] = $order['dp_c_no'];

            if(array_search($order['dp_c_no'], $dp_company_list) !== false){
                $tracking_list[$delivery_no]["qty"] += $order['quantity'];
            }elseif($order['unit_price'] > 0){
                $tracking_list[$delivery_no]["qty"] += $order['quantity'];
            }
        }
    }
    elseif($track_type == '2')
    {
        $order_sql      = "SELECT quantity, dp_c_no FROM work_cms_return WHERE order_number='{$order_number}'";
        $order_query    = mysqli_query($my_db, $order_sql);
        while($order = mysqli_fetch_assoc($order_query))
        {
            $tracking_list[$delivery_no]["dp_c_no"] = $order['dp_c_no'];
            $tracking_list[$delivery_no]["qty"]    += $order['quantity'];
        }
    }

}

$prd_fee_list = [];
foreach ($tracking_list as $deli_no => $cal_data)
{
    $tracking_list[$deli_no]["prd_fee"] = ($cal_data['qty'] > 0) ? $cal_data['price']/$cal_data['qty'] : 0;
}

if(!empty($tracking_list))
{

    foreach($tracking_list as $track_no => $track_data)
    {
        $track_sql      = "";
        if($track_data['type'] == '2')
        {
            $track_sql = "SELECT * FROM work_cms_tracking WHERE delivery_no='{$track_no}'";
        }
        elseif($track_data['type'] == '1')
        {
            if(array_search($track_data['dp_c_no'], $dp_company_list) !== false){
                $track_sql = "SELECT * FROM work_cms_tracking WHERE delivery_no='{$track_no}'";
            }else{
                $track_sql = "SELECT * FROM work_cms_tracking WHERE delivery_no='{$track_no}' AND prd_price > 0";
            }
        }


        if(!empty($track_sql))
        {
            $upd_track_data     = [];
            $final_track_price  = $track_data['price'];

            $track_query        = mysqli_query($my_db, $track_sql);
            while($track_result = mysqli_fetch_assoc($track_query))
            {
                $prd_fee = round($track_result['qty']*$track_data['prd_fee']);
                $final_track_price -= $prd_fee;
                $upd_track_data[$track_result['t_no']] = $prd_fee;
            }

            if(!empty($upd_track_data))
            {
                foreach($upd_track_data as $t_no => $prd_fee)
                {
                    $upd_prd_fee = $prd_fee;
                    if($final_track_price > 0){
                        $upd_prd_fee        += $final_track_price;
                        $final_track_price   = 0;
                    }

                    $upd_sql = "UPDATE work_cms_tracking SET prd_fee = '{$upd_prd_fee}' WHERE t_no='{$t_no}'";
                    mysqli_query($my_db, $upd_sql);
                }
            }
        }

    }
}

exit("<script>alert('상품별 운임비가 반영 되었습니다.');location.href='work_cms_tracking_list.php?sch_billing_date={$billing_date}';</script>");
?>