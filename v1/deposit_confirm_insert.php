<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

require('inc/common.php');
require('ckadmin.php');
require('inc/model/Deposit.php');
require('Classes/PHPExcel.php');

# 파일 변수
$dp_account     = (isset($_POST['dp_account']))?$_POST['dp_account'] : "";
$dpc_company    = (isset($_POST['dpc_company']))?$_POST['dpc_company'] : "";
$file_name      = $_FILES["deposit_file"]["tmp_name"];
$regdate        = date("Y-m-d H:i:s");
$deposit_model  = Deposit::Factory();
$deposit_model->setMainInit("deposit_confirm", "dpc_no");


# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$ins_cnt        = 0;
$ins_data_list  = [];
$chk_data_list  = [];
for ($i = 3; $i <= $totalRow; $i++)
{
    #변수 초기화
    $price = 0;
    $dp_date = $dpc_type = $bk_title = $bk_name = $acc_name = $juck_yo = $reg_s_no = $reg_team = "";

    $dp_date        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 일자
    $bk_title       = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 상대은행
    $bk_name        = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 입금처(출금처)
    $price          = (string)trim(addslashes(str_replace(",", "", $objWorksheet->getCell("G{$i}")->getValue())));   // 금액

    if(empty($dp_date)){
        continue;
    }

    $chk_data_list[$dp_date][$bk_title][$bk_name][$price]++;
}

for ($i = 3; $i < $totalRow; $i++)
{
    $dp_date        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 일자
    $dpc_type       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 구분
    $acc_name       = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 계좌명
    $bk_title       = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 상대은행
    $bk_name        = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 입금처(출금처)
    $juck_yo        = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 적요
    $price          = (string)trim(addslashes(str_replace(",", "", $objWorksheet->getCell("G{$i}")->getValue())));   // 금액

    if(empty($dp_date)){
        continue;
    }

    # 검수
    $chk_sql    = "SELECT count(*) as cnt FROM deposit_confirm WHERE dp_date='{$dp_date}' AND bk_name='{$bk_name}' AND price='{$price}' AND bk_title='{$bk_title}'";
    $chk_query  = mysqli_query($my_db, $chk_sql);
    $chk_result = mysqli_fetch_assoc($chk_query);

    if($chk_result['cnt'] > 0)
    {
        if($chk_data_list[$dp_date][$bk_title][$bk_name][$price] == $chk_result['cnt']){
            continue;
        }else{
            $chk_data_list[$dp_date][$bk_title][$bk_name][$price]--;
        }
    }

    $ins_data_list[] = array(
        "work_state"    => "3",
        "dp_account"    => $dp_account,
        "dp_date"       => $dp_date,
        "dpc_type"      => $dpc_type,
        "dpc_company"   => $dpc_company,
        "bk_title"      => $bk_title,
        "bk_name"       => $bk_name,
        "acc_name"      => $acc_name,
        "juck_yo"       => $juck_yo,
        "price"         => $price,
        "reg_s_no"      => $session_s_no,
        "reg_team"      => $session_team,
        "regdate"       => $regdate,
    );
    $ins_cnt++;
}

if($ins_cnt == 0)
{
    exit("<script>alert('새로 등록될 데이터가 없습니다.');location.href='deposit_confirm_management.php';</script>");
}
else
{
    if (!$deposit_model->multiInsert($ins_data_list)){
        echo "입금확인 데이터 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
        exit;
    }else{
        exit("<script>alert('입금확인 데이터가 반영 되었습니다.');location.href='deposit_confirm_management.php';</script>");
    }
}

?>
