<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');
require('inc/model/MyQuick.php');

// 초기값 세팅
$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

$proc=(isset($_POST['process']))?$_POST['process']:"";
$search_url_val = isset($_POST['search_url']) ? $_POST['search_url'] : "";

// 직원가져오기
$staff_sql="select s_no,s_name from staff where staff_state < '2'";
$staff_query=mysqli_query($my_db,$staff_sql);

while($staff_data=mysqli_fetch_array($staff_query)) {
	$staffs[]=array(
			'no'=>$staff_data['s_no'],
			'name'=>$staff_data['s_name']
	);
	$smarty->assign("staff",$staffs);
}


if($proc=="reload") {
	//print_r($_POST);
	$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$sql="update blog set reload='".$nowdate."' where b_no='".$mno."'";
	//exit("<hr>".$sql);

	//echo "<br><br>".$sql;exit;
	if(!mysqli_query($my_db,$sql)){
		echo ("<script>alert('재평가 저장에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('재평가를 저장 하였습니다');</script>");
	}

	exit("<script>location.href='review_total_list.php?{$search_url_val}';</script>");

}

if($proc=="p_code") {
	//print_r($_POST);
	$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$mtxt=(isset($_POST['p_code'.$mno]))?$_POST['p_code'.$mno]:"";
	$mtxt_array=explode('-',$mtxt);
	//print_r($mtxt_array);
	$sql="update report set p_code='".$mtxt."', p_no = '".$mtxt_array[2]."' where r_no='".$mno."'";
	//exit("<hr>".$sql);
	//echo "<br><br><br><br>".$sql;exit;

	if(!mysqli_query($my_db,$sql)){
		echo ("<script>alert('코드 수정에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('코드를 수정 하였습니다');</script>");
	}

	exit("<script>location.href='review_total_list.php?{$search_url_val}';</script>");

}

if($proc=="memo") {
	//print_r($_POST);
	$mno=(isset($_POST['b_save_no']))?$_POST['b_save_no']:"";
	$mno1=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$mtxt=(isset($_POST['memo'.$mno1]))?$_POST['memo'.$mno1]:"";
	$sql="update blog set b_memo='".addslashes($mtxt)."' where b_no='".$mno."'";
	//exit("<hr>".$sql);

	if(!mysqli_query($my_db,$sql)){
		echo ("<script>alert('메모 저장에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('메모를 저장 하였습니다');</script>");
	}

	exit("<script>location.href='review_total_list.php?{$search_url_val}';</script>");

}

if($proc=="limit_yn") {
	//print_r($_POST);
	$mno=(isset($_POST['a_no']))?$_POST['a_no']:"";
	$mtxt=(isset($_POST['limit_yn'.$mno]))?addslashes($_POST['limit_yn'.$mno]):"";
	if ($mtxt != '3')
		$sql="update application set limit_yn='".$mtxt."' where a_no='".$mno."'";
	else
		$sql="update application set limit_yn='3', a_state = '3' where a_no='".$mno."'";

	if(!mysqli_query($my_db,$sql)){
		echo ("<script>alert('마감준수여부 저장에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('마감준수여부를 저장 하였습니다');</script>");
	}

	exit("<script>location.href='review_total_list.php?{$search_url_val}';</script>");

}

# Navigation & My Quick
$nav_prd_no  = "111";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_prd_no", $nav_prd_no);

// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query",$save_query);


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where=" 1=1";

$search_url=(isset($_POST['search_url']))?$_POST['search_url']:"";
$search_kind=isset($_GET['skind'])?$_GET['skind']:"";
$sch_channel_get=isset($_GET['schannel'])?$_GET['schannel']:"";
$search_scompany=isset($_GET['scompany'])?$_GET['scompany']:"";
$search_sname=isset($_GET['sname'])?$_GET['sname']:"0";
$search_snick=isset($_GET['snick'])?$_GET['snick']:"";
$search_susername=isset($_GET['susername'])?$_GET['susername']:"";
$search_spost_title=isset($_GET['spost_title'])?$_GET['spost_title']:"";
$search_spost_url=isset($_GET['spost_url'])?$_GET['spost_url']:"";
$identified=isset($_GET['identified'])?$_GET['identified']:"";


if(!empty($search_kind)) {
	$add_where.=" AND kind='".$search_kind."'";
	$smarty->assign("skind",$search_kind);
}

if(!empty($sch_channel_get)) {
	$add_where.=" AND channel='".$sch_channel_get."'";
	$smarty->assign("schannel",$sch_channel_get);
}

if(!empty($search_scompany)) {
	$add_where.=" AND company like '%".$search_scompany."%'";
	$smarty->assign("scompany",$search_scompany);
}

if(!empty($search_sname)) {
	$add_where.=" AND p.name like '%".$search_sname."%'";
	$smarty->assign("sname",$search_sname);
}

if(!empty($search_snick)) {
	$add_where.=" AND b.nick like '%".$search_snick."%'";
	$smarty->assign("snick",$search_snick);
}

if(!empty($search_susername)) {
	$add_where.=" AND b.username like '%".$search_susername."%'";
	$smarty->assign("susername",$search_susername);
}

if(!empty($search_spost_title)) {
	$add_where.=" AND r.post_title like '%".$search_spost_title."%'";
	$smarty->assign("spost_title",$search_spost_title);
}

if(!empty($search_spost_url)) {
	$add_where.=" AND r.post_url like '%".$search_spost_url."%'";
	$smarty->assign("spost_url",$search_spost_url);
}

if(!empty($identified)) {
	if($identified == 'n'){
		$add_where.=" AND r.a_no='0'";
		$smarty->assign("identified",$identified);
	}
}


// 페이에 따른 limit 설정
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num = 20;
$offset = ($pages-1) * $num;


$report_sql="
				select r.r_no, r.r_datetime, r.post_title, r.post_url, r.memo, r.p_no, r.a_no, r.b_no, r.p_code, r.bk_name,
				(select kind from promotion p where p.p_no=r.p_no) as kind,
				(select channel from promotion p where p.p_no=r.p_no) as channel,
				(select company from promotion p where p.p_no=r.p_no) as company_name,
				(select name from promotion p where p.p_no=r.p_no) as staff_name,
				(select reg_edate from promotion p where p.p_no=r.p_no) as reg_edate,
				(select nick from blog b where b.b_no=r.b_no) as nick,
				(select username from blog b where b.b_no=r.b_no) as username,
				(select b_memo from blog b where b.b_no=r.b_no) as b_memo,
				(select reload from blog b where b.b_no=r.b_no) as b_reload
				from
				(	report r
				left join
					promotion p
					on r.p_no=p.p_no
				)
				left join
					blog b
					on r.b_no=b.b_no
				WHERE
				$add_where
				order by r.r_datetime desc
				";

//echo $report_sql;exit;

// 전체 게시물 수
$cnt_sql = "SELECT count(*) FROM (".$report_sql.") AS cnt";
$cnt_query= mysqli_query($my_db, $cnt_sql);
$cnt_data=mysqli_fetch_array($cnt_query);
$total_num = $cnt_data[0];

$smarty->assign(array(
	"total_num"=>number_format($total_num)
));

$smarty->assign("total_num",$total_num);
$pagenum = ceil($total_num/$num);

// 페이징
if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}


// 검색 조건
$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url",$search_url);

$page=pagelist($pages, "review_total_list.php", $pagenum, $search_url);
$smarty->assign("pagelist",$page);

// 리스트 쿼리 결과(데이터)
$report_sql .= "LIMIT $offset,$num";
$report_query= mysqli_query($my_db, $report_sql);
while($report_array = mysqli_fetch_array($report_query)){

	$application_sql="select limit_yn from application where a_no='".$report_array['a_no']."'";
	$application_query= mysqli_query($my_db, $application_sql);
	$application_array = mysqli_fetch_array($application_query);

	$reports[] = array(
		"r_no"=>$report_array['r_no'],
		"r_date_ymd"=>date("Y.m.d",strtotime($report_array['r_datetime'])),
		"r_date_hi"=>date("H:i",strtotime($report_array['r_datetime'])),
		"post_title"=>$report_array['post_title'],
		"post_url"=>$report_array['post_url'],
		"memo"=>$report_array['memo'],
		"p_no"=>$report_array['p_no'],
		"a_no"=>$report_array['a_no'],
		"b_no"=>$report_array['b_no'],
		"p_code"=>$report_array['p_code'],
		"kind"=>$report_array['kind'],
		"kind_name"=>$promotion_kind[$report_array['kind']][0],
		"channel"=>$report_array['channel'],
		"company"=>$report_array['company_name'],
		"staff_name"=>$report_array['staff_name'],
		"reg_edate"=>$report_array['reg_edate'],
		"nick"=>$report_array['nick'],
		"username"=>$report_array['username'],
		"b_memo"=>$report_array['b_memo'],
		"reload"=>((empty($report_array['b_reload']))?"____/__/__":date("Y/m/d H:i:s",strtotime($report_array['b_reload']))),
		"limit_yn"=>$application_array['limit_yn'],
		"bk_name" => $report_array['bk_name']
	);
	//print_r($reports);
	$smarty->assign(array(
		"report"=>$reports
	));
}

$smarty->display('review_total_list.html');
?>
