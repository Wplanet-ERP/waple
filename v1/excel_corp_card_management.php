<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/corporation.php');
require('inc/model/MyCompany.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "NO")
    ->setCellValue('B1', "사업자")
    ->setCellValue('C1', "카드사")
    ->setCellValue('D1', "카드번호")
    ->setCellValue('E1', "CVC")
    ->setCellValue('F1', "개인/공용")
    ->setCellValue('G1', "카드명")
    ->setCellValue('H1', "부서")
    ->setCellValue('I1', "이름")
    ->setCellValue('J1', "유효기간")
    ->setCellValue('K1', "DISPLAY")
    ->setCellValue('L1', "재무관리자 메모");

$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');

# 검색 초기화 및 조건 생성
$add_where          = "1=1";
$cur_date           = date('Y-m');
$exp_date           = date('Y-m',strtotime("+3 month"));
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_card_kind      = isset($_GET['sch_card_kind']) ? $_GET['sch_card_kind'] : "";
$sch_card_type      = isset($_GET['sch_card_type']) ? $_GET['sch_card_type'] : "";
$sch_card_type_exc  = isset($_GET['sch_card_type_exc']) ? $_GET['sch_card_type_exc'] : "1";
$sch_card_num       = isset($_GET['sch_card_num']) ? $_GET['sch_card_num'] : "";
$sch_share_card     = isset($_GET['sch_share_card']) ? $_GET['sch_share_card'] : "";
$sch_team           = isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_expired        = isset($_GET['sch_expired']) ? $_GET['sch_expired'] : "";
$sch_display        = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";

if(!empty($sch_my_c_no))
{
    $add_where .= " AND `cc`.my_c_no = '{$sch_my_c_no}'";
}

if(!empty($sch_card_kind))
{
    $add_where .= " AND `cc`.card_kind LIKE '%{$sch_card_kind}%'";
}

if(!empty($sch_card_type))
{
    $add_where .= " AND `cc`.card_type = '{$sch_card_type}'";
}

if(!empty($sch_card_type_exc))
{
    $add_where .= " AND `cc`.card_type != '3'";
}

if(!empty($sch_card_num))
{
    $add_where .= " AND `cc`.card_num LIKE '%{$sch_card_num}%'";
}

if(!empty($sch_share_card))
{
    $add_where .= " AND `cc`.share_card LIKE '%{$sch_share_card}%'";
}

if(!empty($sch_team)) {
    if ($sch_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        $add_where          .= " AND `cc`.team IN ({$sch_team_code_where})";
    }
}

if(!empty($sch_manager))
{
    $add_where .= " AND `cc`.manager IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_manager}%')";
}

if(!empty($sch_expired))
{
    if($sch_expired == '1'){
        $add_where .= " AND DATE_FORMAT(`cc`.expired_date, '%Y-%m') BETWEEN '{$cur_date}' AND '{$exp_date}'";
    }elseif($sch_expired == '2'){
        $add_where .= " AND DATE_FORMAT(`cc`.expired_date, '%Y-%m') < '{$cur_date}'";
    }
}

if(!empty($sch_display)) {
    $add_where .= " AND `cc`.display='{$sch_display}'";
}

# 가이드 리스트 쿼리
$corp_card_sql  = "
    SELECT
        `cc`.cc_no,
        `cc`.`my_c_no`,
        `cc`.card_kind,
        `cc`.card_num,
        `cc`.card_type,
        `cc`.share_card,
        `cc`.cvc,
        (SELECT t.team_name FROM team t WHERE t.team_code=(SELECT sub_t.team_code_parent FROM team sub_t WHERE sub_t.team_code=`cc`.team LIMIT 1)) AS parent_group_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=`cc`.team) AS group_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`cc`.manager) AS manager_name,
        DATE_FORMAT(`cc`.expired_date, '%m/%y') AS expired_my,
        DATE_FORMAT(`cc`.expired_date, '%Y-%m') AS expired_ym_chk,
        `cc`.display,
        `cc`.memo
    FROM corp_card `cc`
    WHERE {$add_where}
    ORDER BY cc_no DESC
";
$idx = 2;
$corp_card_list     = [];
$corp_card_query    = mysqli_query($my_db, $corp_card_sql);
$card_type_option   = getCardTypeOption();
$my_company_model   = MyCompany::Factory();
$my_company_option  = $my_company_model->getNameList();
while($corp_card = mysqli_fetch_assoc($corp_card_query))
{
    $corp_card['my_c_name']      = isset($my_company_option[$corp_card['my_c_no']]) ? $my_company_option[$corp_card['my_c_no']] : "";
    $corp_card['card_type_name'] = isset($card_type_option[$corp_card['card_type']]) ? $card_type_option[$corp_card['card_type']] : "";
    $corp_card['team_name']      = !empty($corp_card['parent_group_name']) ? $corp_card['parent_group_name']." > ".$corp_card['group_name'] : $corp_card['group_name'];
    $corp_card['display_name']   = ($corp_card['display'] == "1") ? "ON" : "OFF";

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$idx}", $corp_card['cc_no'])
        ->setCellValue("B{$idx}", $corp_card['my_c_name'])
        ->setCellValue("C{$idx}", $corp_card['card_kind'])
        ->setCellValue("D{$idx}", $corp_card['card_num'])
        ->setCellValue("E{$idx}", $corp_card['cvc'])
        ->setCellValue("F{$idx}", $corp_card['card_type_name'])
        ->setCellValue("G{$idx}", $corp_card['share_card'])
        ->setCellValue("H{$idx}", $corp_card['team_name'])
        ->setCellValue("I{$idx}", $corp_card['manager_name'])
        ->setCellValue("J{$idx}", $corp_card['expired_my'])
        ->setCellValue("K{$idx}", $corp_card['display_name'])
        ->setCellValue("L{$idx}", $corp_card['memo'])
    ;

    $idx++;
}
$idx--;

$objPHPExcel->getActiveSheet()->getStyle('A1:L'.$idx)->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:L'.$idx)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:L'.$idx)->getFont()->setSize(10);

$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A2:A'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B2:B'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('C2:C'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D2:D'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E2:E'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('F2:F'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G2:G'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('H2:H'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('I2:I'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J2:J'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K2:K'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('L2:L'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(40);
$objPHPExcel->getActiveSheet()->setTitle('법인카드 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="'.$today.'_법인카드 리스트.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
