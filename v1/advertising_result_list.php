<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/_upload.php');
require('inc/helper/advertising.php');
require('inc/model/MyQuick.php');
require('inc/model/Advertising.php');
require('inc/model/Kind.php');

# Model Init
$advertise_model    = Advertising::Factory();
$result_model       = Advertising::Factory();
$result_model->setResultTable();

# 프로세스 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "add_result")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $am_no          = isset($_POST['am_no']) ? $_POST['am_no'] : "";
    $f_result_txt   = isset($_POST['f_result_txt']) ? $_POST['f_result_txt'] : "";
    $advertise_item = $advertise_model->getAdvertisingItem($am_no);

    $f_new_array    = explode("\n", $f_result_txt);
    $ins_multi_data = [];
    $regdate        = date("Y-m-d H:i:s");

    foreach($f_new_array as $index =>$f_new_array_value)
    {
        if(!!trim($f_new_array_value))
        {
            $ins_data       = array("am_no" => $am_no, "regdate" => $regdate, "reg_s_no" => $session_s_no);
            $f_new_array_tt = explode("\t", $f_new_array_value);

            foreach($f_new_array_tt as $index2 =>$f_new_array_tt_value)
            {
                $f_new_array_tt_value = trim($f_new_array_tt_value);

                switch($index2)
                {
                    case '0' : # 광고상품
                        $ins_data['adv_product'] = addslashes(trim($f_new_array_tt_value));;
                        break;
                    case '1': # 광고상품유형
                        $ins_data['adv_prd_type'] = addslashes(trim($f_new_array_tt_value));;
                        break;
                    case '2' : # 광고소재(브랜드 체크)
                        $ins_data['adv_material'] = addslashes(trim($f_new_array_tt_value));
                        if(strpos($f_new_array_tt_value, "큐빙") !== false){
                            $ins_data['brand']      = "3386";
                            $ins_data['brand_name'] = "닥터피엘 큐빙";
                        }elseif(strpos($f_new_array_tt_value, "여름이불") !== false){
                            $ins_data['brand']      = "5201";
                            $ins_data['brand_name'] = "누잠 여름이불";
                        }elseif(strpos($f_new_array_tt_value, "겨울이불") !== false){
                            $ins_data['brand']      = "5642";
                            $ins_data['brand_name'] = "누잠 겨울이불";
                        }elseif(strpos($f_new_array_tt_value, "더블업") !== false){
                            $ins_data['brand']      = "4878";
                            $ins_data['brand_name'] = "누잠 더블업토퍼";
                        }elseif(strpos($f_new_array_tt_value, "베개") !== false){
                            $ins_data['brand']      = "6026";
                            $ins_data['brand_name'] = "누잠 베개";
                        }elseif(strpos($f_new_array_tt_value, "바디필로우") !== false){
                            $ins_data['brand']      = "5810";
                            $ins_data['brand_name'] = "누잠 바디필로우";
                        }elseif(strpos($f_new_array_tt_value, "누잠") !== false){
                            $ins_data['brand']      = "2827";
                            $ins_data['brand_name'] = "누잠";
                        }elseif(strpos($f_new_array_tt_value, "쌩얼크림") !== false){
                            $ins_data['brand']      = "5759";
                            $ins_data['brand_name'] = "아이레놀 쌩얼보정 선크림";
                        }elseif(strpos($f_new_array_tt_value, "아이레놀") !== false){
                            $ins_data['brand']      = "2388";
                            $ins_data['brand_name'] = "아이레놀";
                        }elseif(strpos($f_new_array_tt_value, "시티파이") !== false){
                            $ins_data['brand']      = "4333";
                            $ins_data['brand_name'] = "시티파이";
                        }elseif(strpos($f_new_array_tt_value, "에코호스") !== false){
                            $ins_data['brand']      = "3303";
                            $ins_data['brand_name'] = "닥터피엘 에코호스";
                        }elseif(strpos($f_new_array_tt_value, "여행용") !== false){
                            $ins_data['brand']      = "5434";
                            $ins_data['brand_name'] = "닥터피엘 여행용";
                        }elseif(strpos($f_new_array_tt_value, "퓨어팟") !== false){
                            $ins_data['brand']      = "2863";
                            $ins_data['brand_name'] = "닥터피엘 퓨어팟";
                        }elseif(strpos($f_new_array_tt_value, "피처형") !== false){
                            $ins_data['brand']      = "4446";
                            $ins_data['brand_name'] = "닥터피엘 에코 피처형 정수기";
                        }elseif(strpos($f_new_array_tt_value, "아토") !== false){
                            $ins_data['brand']      = "5812";
                            $ins_data['brand_name'] = "아토샤워헤드";
                        }elseif(strpos($f_new_array_tt_value, "버블세면대") !== false){
                            $ins_data['brand']      = "5910";
                            $ins_data['brand_name'] = "버블세면대";
                        }elseif(strpos($f_new_array_tt_value, "주방용 프리미엄") !== false){
                            $ins_data['brand']      = "5368";
                            $ins_data['brand_name'] = "닥터피엘 주방용 프리미엄";
                        }elseif(strpos($f_new_array_tt_value, "닥터피엘") !== false){
                            $ins_data['brand']      = "1314";
                            $ins_data['brand_name'] = "닥터피엘";
                        }else{
                            exit("<script>alert('{$f_new_array_tt_value} 없는 브랜드 입니다.');location.href='advertising_result_list.php?{$search_url}';</script>");
                        }
                        break;
                    case '3' : # 노출수
                        $ins_data['impressions'] = $f_new_array_tt_value;
                        break;
                    case '4' : # 클릭수
                        $ins_data['click_cnt'] = $f_new_array_tt_value;
                        break;
                    case '5' : # 클릭률
                        $ins_data['click_per'] = $f_new_array_tt_value;
                        break;
                    case '6': # 광고비
                        $ins_data['adv_price'] = $f_new_array_tt_value;
                        break;
                }
            }

            if(!empty($ins_data)){
                $ins_multi_data[] = $ins_data;
            }
        }
    }

    if($result_model->multiInsert($ins_multi_data))
    {
        $advertise_model->update(array("am_no" => $am_no, "state" => "5"));
        $adv_product_option = getAdvertisingProductOption();
        $adv_date_option    = getDayShortOption();
        $adv_media_option   = getAdvertisingMediaOption();
        $adv_s_date_text    = $advertise_item['adv_s_day']." (".$adv_date_option[date("w", strtotime($advertise_item['adv_s_day']))].") ".$advertise_item['adv_s_time'];
        $adv_media_name     = $adv_media_option[$advertise_item['media']];
        $adv_product_name   = $adv_product_option[$advertise_item['product']];

        $adv_application_sql    = "SELECT aa_no, s_no, s_name FROM advertising_application WHERE am_no='{$am_no}'";
        $adv_application_query  = mysqli_query($my_db, $adv_application_sql);
        while($adv_application = mysqli_fetch_assoc($adv_application_query))
        {
            $adv_msg     = "[NOSP 광고 결과보고 알림] {$adv_media_name}, {$adv_product_name} {$adv_s_date_text}";
            $adv_msg    .= "\r\nhttps://work.wplanet.co.kr/v1/advertising_result_list.php?sch_no={$am_no}";
            $adv_msg     = addslashes($adv_msg);
            $chk_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$adv_application['s_no']}', content='{$adv_msg}', alert_type='62', alert_check='ADVERTISE_RESULT_{$am_no}', regdate=now()";
            mysqli_query($my_db, $chk_ins_sql);
        }

        exit("<script>alert('결과 등록에 성공했습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('결과 등록에 실패했습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
    }
}
elseif($process == "cal_adv_price")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $am_no              = isset($_POST['am_no']) ? $_POST['am_no'] : "";

    $advertise_item     = $advertise_model->getItem($am_no);
    $result_total_sql   = "SELECT SUM(adv_price) as total_adv_price FROM advertising_result WHERE am_no='{$am_no}'";
    $result_total_query = mysqli_query($my_db, $result_total_sql);
    $result_total_data  = mysqli_fetch_assoc($result_total_query);

    $total_price        = $advertise_item['price'];
    $result_total       = isset($result_total_data['total_adv_price']) ? $result_total_data['total_adv_price'] : 0;
    $cal_adv_price      = $total_price - $result_total;

    $chk_result_sql     = "SELECT ar_no, adv_price FROM advertising_result WHERE am_no='{$am_no}' AND brand='1314' ORDER BY ar_no ASC LIMIT 1";
    $chk_result_query   = mysqli_query($my_db, $chk_result_sql);
    $chk_result_data    = mysqli_fetch_assoc($chk_result_query);

    if(!isset($chk_result_data['ar_no'])){
        exit("<script>alert('차액 처리에 실패했습니다. 닥터피엘 결과가 없습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
    }

    $upd_data = array("ar_no" => $chk_result_data['ar_no'], "adv_price" => $chk_result_data['adv_price']+$cal_adv_price);

    if($result_model->update($upd_data)) {
        exit("<script>alert('차액 처리에 성공했습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('차액 처리에 실패했습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
    }
}
elseif($process == "del_result")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $am_no          = isset($_POST['am_no']) ? $_POST['am_no'] : "";

    if($result_model->deleteToArray(array("am_no" => $am_no))){
        exit("<script>alert('결과 초기화에 성공했습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('결과 초기화에 실패했습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
    }
}
elseif($process == "f_result_price")
{
    $ar_no          = isset($_POST['ar_no']) ? $_POST['ar_no'] : "";
    $result_price   = isset($_POST['val']) ? str_replace(",", "", $_POST['val']) : "";

    if(!$result_model->update(array("ar_no" => $ar_no, "adv_price" => $result_price))){
        echo "광고비 변경에 실패했습니다.";
    }else{
        echo "광고비를 변경했습니다.";
    }
    exit;
}
elseif($process == "f_am_price")
{
    $am_no   = isset($_POST['ar_no']) ? $_POST['ar_no'] : "";
    $price   = isset($_POST['val']) ? str_replace(",", "", $_POST['val']) : "";

    if(!$advertise_model->update(array("am_no" => $am_no, "price" => $price))){
        echo "단가 변경에 실패했습니다.";
    }else{
        echo "단가를 변경했습니다.";
    }
    exit;
}
elseif($process == "adv_img_path")
{
    $ar_no          = isset($_POST['ar_no']) ? $_POST['ar_no'] : "";
    $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $adv_img_file   = $_FILES["adv_img_path"];
    $adv_img_path   = add_store_image($adv_img_file, "advertisement");
    $adv_img_name   = $adv_img_file['name'];

    if ($result_model->update(array("ar_no" => $ar_no, "adv_img_path" => $adv_img_path, "adv_img_name" => $adv_img_name))){
        exit("<script>alert('이미지가 등록되었습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
    } else{
        exit("<script>alert('이미지 등록에 실패했습니다');location.href='advertising_result_list.php?{$search_url}';</script>");
    }
}


# Navigation & My Quick
$nav_prd_no  = "181";
$nav_title   = "NOSP 광고 결과보고";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);

$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

$add_where          = "1=1 AND `am`.state IN(3,5)";
$add_date_where     = "1=1";
$add_ar_where       = "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_adv_s_date     = isset($_GET['sch_adv_s_date']) ? $_GET['sch_adv_s_date'] : $month_val;
$sch_adv_e_date     = isset($_GET['sch_adv_e_date']) ? $_GET['sch_adv_e_date'] : $today_val;
$sch_adv_date_type  = isset($_GET['sch_adv_date_type']) ? $_GET['sch_adv_date_type'] : "month";
$sch_date_kind      = isset($_GET['sch_date_kind']) ? $_GET['sch_date_kind'] : "";
$sch_date_s_time    = isset($_GET['sch_date_s_time']) ? $_GET['sch_date_s_time'] : "";
$sch_date_e_time    = isset($_GET['sch_date_e_time']) ? $_GET['sch_date_e_time'] : "";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_media          = 265;
$sch_product        = isset($_GET['sch_product']) ? $_GET['sch_product'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_is_complete    = isset($_GET['sch_is_complete']) ? $_GET['sch_is_complete'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_adv_s_date)){
    $adv_s_datetime  = $sch_adv_s_date." 00:00:00";
    $add_where      .= " AND am.adv_s_date >= '{$adv_s_datetime}'";
    $smarty->assign("sch_adv_s_date", $sch_adv_s_date);
}

if(!empty($sch_adv_e_date)){
    $adv_e_datetime  = $sch_adv_e_date." 23:59:59";
    $add_where      .= " AND am.adv_s_date <= '{$adv_e_datetime}'";
    $smarty->assign("sch_adv_e_date", $sch_adv_e_date);
}
$smarty->assign("sch_adv_date_type", $sch_adv_date_type);

if($sch_date_kind != ""){
    $add_date_where .= " AND chk_s_day = '{$sch_date_kind}'";
    $smarty->assign("sch_date_kind", $sch_date_kind);
}

if($sch_date_s_time != ""){
    $add_date_where .= " AND chk_s_time = '{$sch_date_s_time}'";
    $smarty->assign("sch_date_s_time", $sch_date_s_time);
}

if($sch_date_e_time != ""){
    $add_date_where .= " AND chk_e_time = '{$sch_date_e_time}'";
    $smarty->assign("sch_date_e_time", $sch_date_e_time);
}

if(!empty($sch_no)){
    $add_where .= " AND am.am_no = '{$sch_no}'";
    $smarty->assign("sch_no", $sch_no);
}

if(!empty($sch_media)){
    $add_where .= " AND am.media = '{$sch_media}'";
    $smarty->assign("sch_media", $sch_media);
}

if(!empty($sch_product)){
    $add_where .= " AND am.product = '{$sch_product}'";
    $smarty->assign("sch_product", $sch_product);
}

if(!empty($sch_state)){
    $add_where .= " AND am.state = '{$sch_state}'";
    $smarty->assign("sch_state", $sch_state);
}

if(!empty($sch_agency)){
    $add_where .= " AND am.agency = '{$sch_agency}'";
    $smarty->assign("sch_agency", $sch_agency);
}

if(!empty($sch_is_complete)){
    if($sch_is_complete == '1'){
        $add_where .= " AND (`am`.price - (SELECT SUM(`ar`.adv_price) FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no) != 0)";
    }elseif($sch_is_complete == '2'){
        $add_where .= " AND (`am`.price - (SELECT SUM(`ar`.adv_price) FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no) = 0)";
    }
    $smarty->assign("sch_is_complete", $sch_is_complete);
}

if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where      .= " AND `am`.am_no IN(SELECT `ar`.am_no FROM advertising_result `ar` WHERE `ar`.brand = '{$sch_brand}')";
    $add_ar_where   .= " AND `ar`.brand = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where      .= " AND `am`.am_no IN(SELECT `ar`.am_no FROM advertising_result `ar` WHERE `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}'))";
    $add_ar_where   .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

    $add_where      .= " AND `am`.am_no IN(SELECT `ar`.am_no FROM advertising_result `ar` WHERE `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}')))";
    $add_ar_where   .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 전체 게시물 수
$adv_total_sql	    = "SELECT count(am_no) as cnt FROM advertising_management `am` WHERE {$add_where}";
$adv_total_query    = mysqli_query($my_db, $adv_total_sql);
$adv_total_result   = mysqli_fetch_array($adv_total_query);
$adv_total 	        = $adv_total_result['cnt'];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "50";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($adv_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "advertising_result_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $adv_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 정렬기능
$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "adv_s_date";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

$add_orderby    = "ar_cnt ASC";
if(!empty($ord_type))
{
    if($ord_type_by == '1'){
        $orderby_val = "ASC";
    }else{
        $orderby_val = "DESC";
    }

    $add_orderby .= ", `am`.adv_s_date {$orderby_val}";
}
$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);

$adv_management_sql = "
    SELECT
        *
    FROM
    (
        SELECT
            *,
            DATE_FORMAT(`am`.regdate, '%Y/%m/%d') as reg_day,
            DATE_FORMAT(`am`.regdate, '%H:%i') as reg_hour,
            DATE_FORMAT(`am`.adv_s_date, '%Y-%m-%d') as adv_s_day,
            DATE_FORMAT(`am`.adv_s_date, '%H:%i') as adv_s_time,
            DATE_FORMAT(`am`.adv_e_date, '%Y-%m-%d') as adv_e_day,
            DATE_FORMAT(`am`.adv_e_date, '%H:%i') as adv_e_time,
            (SELECT s_name FROM staff s WHERE s.s_no=`am`.reg_s_no) AS reg_s_name,
            IF((SELECT COUNT(`ar`.ar_no) as cnt FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no)=0, 1, 2) as ar_cnt,
            (SELECT SUM(`ar`.adv_price) FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no) as total_adv_price,
            DATE_FORMAT(`am`.adv_s_date, '%w') as chk_s_day,
            DATE_FORMAT(`am`.adv_s_date, '%H') as chk_s_time,
            DATE_FORMAT(`am`.adv_e_date, '%H') as chk_e_time
        FROM advertising_management `am`
        WHERE {$add_where}
    ) AS `am`
    WHERE {$add_date_where}
    ORDER BY {$add_orderby}
    LIMIT {$offset}, {$num} 
";
$adv_management_query       = mysqli_query($my_db, $adv_management_sql);
$adv_management_list        = [];
$adv_state_option       = getAdvertisingStateOption();
$adv_state_color_option = getAdvertisingStateColorOption();
$adv_media_option       = getAdvertisingMediaOption();
$adv_agency_option      = getAdvertisingAgencyOption();
$adv_product_option     = getAdvertisingProductOption();
$adv_date_option        = getDayShortOption();
$adv_result_tmp_list    = [];
while($adv_management = mysqli_fetch_assoc($adv_management_query))
{
    $adv_management['state_name']       = isset($adv_state_option[$adv_management['state']]) ? $adv_state_option[$adv_management['state']] : "";
    $adv_management['state_color']      = isset($adv_state_color_option[$adv_management['state']]) ? $adv_state_color_option[$adv_management['state']] : "";
    $adv_management['media_name']       = isset($adv_media_option[$adv_management['media']]) ? $adv_media_option[$adv_management['media']] : "";
    $adv_management['agency_name']      = isset($adv_agency_option[$adv_management['agency']]) ? $adv_agency_option[$adv_management['agency']] : "";
    $adv_management['product_name']     = isset($adv_product_option[$adv_management['product']]) ? $adv_product_option[$adv_management['product']] : "";
    $adv_management['adv_s_date_text']  = $adv_management['adv_s_day']." (".$adv_date_option[date("w", strtotime($adv_management['adv_s_day']))].") ".$adv_management['adv_s_time'];
    $adv_management['adv_e_date_text']  = $adv_management['adv_e_day']." (".$adv_date_option[date("w", strtotime($adv_management['adv_e_day']))].") ".$adv_management['adv_e_time'];
    $adv_management['cal_adv_price']    = $adv_management['price']-$adv_management['total_adv_price'];

    $adv_result_sql         = "SELECT * FROM advertising_result `ar` WHERE `ar`.am_no='{$adv_management['am_no']}' {$add_ar_where} ORDER BY brand";
    $adv_result_query       = mysqli_query($my_db, $adv_result_sql);
    $adv_result_list        = [];
    while($adv_result = mysqli_fetch_assoc($adv_result_query)){
        $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["brand_name"]         = $adv_result['brand_name'];
        $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["fee_per"]            = $adv_management['fee_per'];
        $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_price"]        = $adv_management['price'];
        $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_impressions"] += $adv_result['impressions'];
        $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_click_cnt"]   += $adv_result['click_cnt'];
        $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_adv_price"]   += $adv_result['adv_price'];
        $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_cnt"]++;
        $adv_result_list[] = $adv_result;
    }
    $adv_management['result_list'] = $adv_result_list;

    $adv_management_list[] = $adv_management;
}


$adv_result_brand_list  = [];
$adv_result_total_list  = array("total_impressions" => 0, "total_click_cnt" => 0, "total_click_per" => 0, "avg_click_per" => 0, "total_adv_price" => 0, "total_fee_price" => 0, "total_cal_price" => 0, "total_cnt" => 0);
foreach($adv_result_tmp_list as $am_no => $adv_result_tmp)
{
    foreach($adv_result_tmp as $brand => $adv_brand_data)
    {
        $fee_price      = $adv_brand_data['total_adv_price']*($adv_brand_data['fee_per']/100);
        $cal_price      = $adv_brand_data["total_adv_price"]-$fee_price;
        $cal_price_vat  = $cal_price*1.1;
        $adv_result_brand_list[$am_no][$brand] = array(
            "brand_name"            => $adv_brand_data["brand_name"],
            "impressions"           => $adv_brand_data["total_impressions"],
            "click_cnt"             => $adv_brand_data["total_click_cnt"],
            "click_per"             => $adv_brand_data["total_click_cnt"]/$adv_brand_data["total_impressions"]*100,
            "adv_price"             => $adv_brand_data["total_adv_price"],
            "fee_price"             => $fee_price,
            "cal_price"             => $cal_price,
            "cal_price_vat"         => $cal_price_vat,
            "cnt"                   => $adv_brand_data["total_cnt"],
        );

        $adv_result_total_list['total_impressions']     += $adv_brand_data["total_impressions"];
        $adv_result_total_list['total_click_cnt']       += $adv_brand_data["total_click_cnt"];
        $adv_result_total_list['total_adv_price']       += $adv_brand_data["total_adv_price"];
        $adv_result_total_list['total_fee_price']       += $fee_price;
        $adv_result_total_list['total_cal_price']       += $cal_price;
        $adv_result_total_list['total_cal_price_vat']   += $cal_price_vat;
        $adv_result_total_list['total_cnt']             += $adv_brand_data["total_cnt"];
    }
}

if(!empty($adv_result_total_list))
{
    $adv_result_total_list['avg_click_per'] = ($adv_result_total_list['total_impressions'] > 0) ? $adv_result_total_list['total_click_cnt']/$adv_result_total_list['total_impressions']*100 : 0;
}

$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("date_kind_option", getDateChartOption());
$smarty->assign("hour_kind_option", getHourOption());
$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("adv_media_option", $adv_media_option);
$smarty->assign("adv_product_option", $adv_product_option);
$smarty->assign("adv_state_option", $adv_state_option);
$smarty->assign("adv_state_color_option", $adv_state_color_option);
$smarty->assign("adv_agency_option", $adv_agency_option);
$smarty->assign("adv_management_list", $adv_management_list);
$smarty->assign("adv_result_brand_list", $adv_result_brand_list);
$smarty->assign("adv_result_total_list", $adv_result_total_list);

$smarty->display('advertising_result_list.html');
?>
