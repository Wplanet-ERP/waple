<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/work_cms.php');
require('inc/model/Custom.php');

# 기본 변수
$url_model          = Custom::Factory();
$url_model->setMainInit("commerce_url", "url_no");

$dp_self_imweb_list     = getSelfDpImwebCompanyList();
$dp_self_imweb_text     = implode(",", $dp_self_imweb_list);

# 마지막 날짜
$max_convert_date_sql     = "SELECT convert_date FROM commerce_conversion WHERE dp_c_no > 0 AND brand > 0 ORDER BY convert_date DESC LIMIT 1";
$max_convert_date_query   = mysqli_query($my_db, $max_convert_date_sql);
$max_convert_date_result  = mysqli_fetch_assoc($max_convert_date_query);
$max_convert_date         = isset($max_convert_date_result['convert_date']) ? date("Y-m-d", strtotime($max_convert_date_result['convert_date'])) : date('Y-m-d', strtotime("-3 days"));

# 검색조건
$add_where          = "brand > 0 AND dp_c_no > 0";
$add_cms_where      = "delivery_state = '4' AND w.unit_price > 0 AND w.page_idx > 0 AND w.dp_c_no IN({$dp_self_imweb_text})";
$today_val          = date('Y-m-d');
$prev_val           = date('Y-m-d', strtotime("{$max_convert_date} -13 days"));
$sch_traffic_type   = isset($_GET['sch_traffic_type']) ? $_GET['sch_traffic_type'] : "day";
$sch_convert_s_date = isset($_GET['sch_convert_s_date']) ? $_GET['sch_convert_s_date'] : $prev_val;
$sch_convert_e_date = isset($_GET['sch_convert_e_date']) ? $_GET['sch_convert_e_date'] : $max_convert_date;
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_dp_company     = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";
$sch_url            = isset($_GET['sch_url']) ? $_GET['sch_url'] : "";

if(empty($sch_convert_s_date) || empty($sch_convert_e_date)) {
    echo "날짜 검색 값이 없으면 차트 생성이 안됩니다.";
    exit;
}

if($sch_convert_e_date > $max_convert_date){
    $sch_convert_e_date = $max_convert_date;
}

if(empty($sch_url)) {
    echo "페이지 검색 후 이용하실 수 있습니다.";
    exit;
}

if(!empty($sch_brand)) {
    $add_where         .= " AND `cc`.brand = '{$sch_brand}'";
    $add_cms_where     .= " AND `w`.c_no='{$sch_brand}'";
}

if(!empty($sch_dp_company)) {
    $add_where      .= " AND `cc`.dp_c_no='{$sch_dp_company}'";
    $add_cms_where  .= " AND `w`.dp_c_no='{$sch_dp_company}'";
}

if(!empty($sch_url))
{
    $chk_url_item   = $url_model->getItem($sch_url);
    $sch_url_cms    = str_replace("idx=","", $chk_url_item['url']);

    $add_where      .= " AND `cc`.url_idx = '{$chk_url_item['url']}' AND `cc`.dp_c_no='{$chk_url_item['dp_c_no']}'";
    $add_cms_where  .= " AND `w`.page_idx='{$sch_url_cms}' AND `w`.dp_c_no='{$chk_url_item['dp_c_no']}'";
}

$sch_convert_s_datetime = $sch_convert_s_date." 00:00:00";
$sch_convert_e_datetime = $sch_convert_e_date." 23:59:59";
$add_where             .= " AND convert_date BETWEEN '{$sch_convert_s_datetime}' AND '{$sch_convert_e_datetime}'";
$add_cms_where         .= " AND order_date BETWEEN '{$sch_convert_s_datetime}' AND '{$sch_convert_e_datetime}'";

# 사용 Array
$date_hour_option           = getHourOption();
$convert_week_list          = [];
$convert_date_list          = [];
$convert_hour_list          = [];
$convert_count_init         = array("traffic" => 0, "conversion" => 0, "conversion_rate" => 0);
$convert_live_init          = array("total_price" => 0, "total_cnt" => 0, "avg_price" => 0);
$convert_count_week_list    = [];
$convert_count_date_list    = [];
$convert_count_hour_list    = [];
$convert_live_week_list     = [];
$convert_live_date_list     = [];
$convert_live_hour_list     = [];
$cms_chk_order_list         = [];

# 날짜 생성
$all_date_sql = "
    SELECT
        DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d') as week_key,
        CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d')) as week_title,
        DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key,
        DATE_FORMAT(`allday`.Date, '%Y-%m-%d') as date_title
    FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
    as allday
    WHERE DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_convert_s_date}' AND '{$sch_convert_e_date}'
    ORDER BY date_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($all_date = mysqli_fetch_assoc($all_date_query))
{
    $convert_week_list[$all_date['week_key']]       = $all_date['week_title'];
    $convert_date_list[$all_date['date_key']]       = $all_date['date_title'];
    $convert_count_week_list[$all_date['week_key']] = $convert_count_init;
    $convert_count_date_list[$all_date['date_key']] = $convert_count_init;
    $convert_live_week_list[$all_date['week_key']]  = $convert_live_init;
    $convert_live_date_list[$all_date['date_key']]  = $convert_live_init;
}

foreach($date_hour_option as $hour){
    $hour = (int)$hour;
    $convert_hour_list[$hour]       = $hour."시";
    $convert_count_hour_list[$hour] = $convert_count_init;
    $convert_live_hour_list[$hour]  = $convert_live_init;
}

# 유입/전환 값
$convert_sql = "
    SELECT
        DATE_FORMAT(DATE_SUB(`cc`.convert_date, INTERVAL(IF(DAYOFWEEK(`cc`.convert_date)=1,8,DAYOFWEEK(`cc`.convert_date))-2) DAY), '%Y%m%d') as week_key,
        DATE_FORMAT(`cc`.convert_date, '%Y%m%d') as date_key,
        DATE_FORMAT(`cc`.convert_date, '%H') as hour_key,
        `cc`.traffic_count,
        `cc`.conversion_count
    FROM commerce_conversion as `cc`
    WHERE {$add_where}
    ORDER BY date_key ASC
";
$convert_query = mysqli_query($my_db, $convert_sql);
while($convert_result = mysqli_fetch_assoc($convert_query)){
    $traffic_count  = $convert_result['traffic_count'];
    $convert_count  = $convert_result['conversion_count'];
    $week_key       = $convert_result['week_key'];
    $date_key       = $convert_result['date_key'];
    $hour_key       = (int)$convert_result['hour_key'];

    $convert_count_week_list[$week_key]["traffic"]      += $traffic_count;
    $convert_count_date_list[$date_key]["traffic"]      += $traffic_count;
    $convert_count_hour_list[$hour_key]["traffic"]      += $traffic_count;
    $convert_count_week_list[$week_key]["conversion"]   += $convert_count;
    $convert_count_date_list[$date_key]["conversion"]   += $convert_count;
    $convert_count_hour_list[$hour_key]["conversion"]   += $convert_count;
}

$add_cms_column  = "DATE_FORMAT(DATE_SUB(`w`.order_date, INTERVAL(IF(DAYOFWEEK(`w`.order_date)=1,8,DAYOFWEEK(`w`.order_date))-2) DAY), '%Y%m%d')";
$add_cms_column  = "DATE_FORMAT(`w`.order_date, '%H')";
$add_cms_column  = "DATE_FORMAT(`w`.order_date, '%Y%m%d')";

$cms_sql = "
    SELECT
        `w`.order_number,
        DATE_FORMAT(DATE_SUB(`w`.order_date, INTERVAL(IF(DAYOFWEEK(`w`.order_date)=1,8,DAYOFWEEK(`w`.order_date))-2) DAY), '%Y%m%d') as week_key,
        DATE_FORMAT(`w`.order_date, '%Y%m%d') as date_key,
        DATE_FORMAT(`w`.order_date, '%H') as hour_key,
        `w`.unit_price
    FROM work_cms as `w`
    WHERE {$add_cms_where}
    ORDER BY date_key ASC
";
$cms_query = mysqli_query($my_db, $cms_sql);
while($cms_result = mysqli_fetch_assoc($cms_query))
{
    $week_key       = $cms_result['week_key'];
    $date_key       = $cms_result['date_key'];
    $hour_key       = (int)$cms_result['hour_key'];

    if(!isset($cms_chk_order_list[$cms_result['order_number']]))
    {
        $convert_live_week_list[$week_key]["total_cnt"]++;
        $convert_live_date_list[$date_key]["total_cnt"]++;
        $convert_live_hour_list[$hour_key]["total_cnt"]++;

        $cms_chk_order_list[$cms_result['order_number']] = 1;
    }

    $convert_live_week_list[$week_key]["total_price"]  += $cms_result['unit_price'];
    $convert_live_date_list[$date_key]["total_price"]  += $cms_result['unit_price'];
    $convert_live_hour_list[$hour_key]["total_price"]  += $cms_result['unit_price'];
}

$convert_count_week_chart_list   = [];
if(!empty($convert_count_week_list))
{
    foreach($convert_count_week_list as $week_key => $week_data)
    {
        $total_traffic_count = $week_data['traffic'];
        $total_convert_count = $week_data['conversion'];
        $total_convert_rate  = ($total_traffic_count > 0) ? round((($total_convert_count/$total_traffic_count)*100),2) : 0;

        $convert_count_week_list[$week_key]["conversion_rate"] = $total_convert_rate;

        $convert_count_week_chart_list[0]['title']  = "유입";
        $convert_count_week_chart_list[0]['type']   = "bar";
        $convert_count_week_chart_list[0]['color']  = "rgba(0,176,80)";
        $convert_count_week_chart_list[0]['data'][] = $total_traffic_count;
        $convert_count_week_chart_list[1]['title']  = "전환";
        $convert_count_week_chart_list[1]['type']   = "bar";
        $convert_count_week_chart_list[1]['color']  = "rgba(255,192,0)";
        $convert_count_week_chart_list[1]['data'][] = $total_convert_count;
        $convert_count_week_chart_list[2]['title']  = "전환율";
        $convert_count_week_chart_list[2]['type']   = "line";
        $convert_count_week_chart_list[2]['color']  = "rgba(0,176,240)";
        $convert_count_week_chart_list[2]['data'][] = $total_convert_rate;
    }
}

$convert_count_date_chart_list   = [];
if(!empty($convert_count_date_list))
{
    foreach($convert_count_date_list as $date_key => $date_data)
    {
        $total_traffic_count = $date_data['traffic'];
        $total_convert_count = $date_data['conversion'];
        $total_convert_rate  = ($total_traffic_count > 0) ? round((($total_convert_count/$total_traffic_count)*100),2) : 0;

        $convert_count_date_list[$date_key]["conversion_rate"] = $total_convert_rate;

        $convert_count_date_chart_list[0]['title']  = "유입";
        $convert_count_date_chart_list[0]['type']   = "bar";
        $convert_count_date_chart_list[0]['color']  = "rgba(0,176,80)";
        $convert_count_date_chart_list[0]['data'][] = $total_traffic_count;
        $convert_count_date_chart_list[1]['title']  = "전환";
        $convert_count_date_chart_list[1]['type']   = "bar";
        $convert_count_date_chart_list[1]['color']  = "rgba(255,192,0)";
        $convert_count_date_chart_list[1]['data'][] = $total_convert_count;
        $convert_count_date_chart_list[2]['title']  = "전환율";
        $convert_count_date_chart_list[2]['type']   = "line";
        $convert_count_date_chart_list[2]['color']  = "rgba(0,176,240)";
        $convert_count_date_chart_list[2]['data'][] = $total_convert_rate;
    }
}

$convert_count_hour_chart_list   = [];
if(!empty($convert_count_hour_list))
{
    foreach($convert_count_hour_list as $hour_key => $hour_data)
    {
        $total_traffic_count = $hour_data['traffic'];
        $total_convert_count = $hour_data['conversion'];
        $total_convert_rate  = ($total_traffic_count > 0) ? round((($total_convert_count/$total_traffic_count)*100),2) : 0;

        $convert_count_hour_list[$hour_key]["conversion_rate"] = $total_convert_rate;

        $convert_count_hour_chart_list[0]['title']  = "유입";
        $convert_count_hour_chart_list[0]['type']   = "bar";
        $convert_count_hour_chart_list[0]['color']  = "rgba(0,176,80)";
        $convert_count_hour_chart_list[0]['data'][] = $total_traffic_count;
        $convert_count_hour_chart_list[1]['title']  = "전환";
        $convert_count_hour_chart_list[1]['type']   = "bar";
        $convert_count_hour_chart_list[1]['color']  = "rgba(255,192,0)";
        $convert_count_hour_chart_list[1]['data'][] = $total_convert_count;
        $convert_count_hour_chart_list[2]['title']  = "전환율";
        $convert_count_hour_chart_list[2]['type']   = "line";
        $convert_count_hour_chart_list[2]['color']  = "rgba(0,176,240)";
        $convert_count_hour_chart_list[2]['data'][] = $total_convert_rate;
    }
}

$convert_live_week_chart_list = [];
if(!empty($convert_live_week_list))
{
    foreach($convert_live_week_list as $week_live_key => $week_live_data)
    {
        $total_price        = $week_live_data['total_price'];
        $total_ord_count    = $week_live_data['total_cnt'];
        $total_avg_price    = ($total_ord_count > 0) ? round($total_price/$total_ord_count) : 0;

        $convert_live_week_list[$week_live_key]["avg_price"] = $total_avg_price;

        $convert_live_week_chart_list[0]['title']   = "아임웹 매출";
        $convert_live_week_chart_list[0]['type']    = "line";
        $convert_live_week_chart_list[0]['color']   = "rgba(128,100,162)";
        $convert_live_week_chart_list[0]['data'][]  = $total_price;
        $convert_live_week_chart_list[1]['title']   = "주문건당 단가";
        $convert_live_week_chart_list[1]['type']    = "line";
        $convert_live_week_chart_list[1]['color']   = "rgba(255,51,204)";
        $convert_live_week_chart_list[1]['data'][]  = $total_avg_price;
    }
}

$convert_live_date_chart_list = [];
if(!empty($convert_live_date_list))
{
    foreach($convert_live_date_list as $date_live_key => $date_live_data)
    {
        $total_price        = $date_live_data['total_price'];
        $total_ord_count    = $date_live_data['total_cnt'];
        $total_avg_price    = ($total_ord_count > 0) ? round($total_price/$total_ord_count) : 0;

        $convert_live_date_list[$date_live_key]["avg_price"] = $total_avg_price;

        $convert_live_date_chart_list[0]['title']   = "아임웹 매출";
        $convert_live_date_chart_list[0]['type']    = "line";
        $convert_live_date_chart_list[0]['color']   = "rgba(128,100,162)";
        $convert_live_date_chart_list[0]['data'][]  = $total_price;
        $convert_live_date_chart_list[1]['title']   = "주문건당 단가";
        $convert_live_date_chart_list[1]['type']    = "line";
        $convert_live_date_chart_list[1]['color']   = "rgba(255,51,204)";
        $convert_live_date_chart_list[1]['data'][]  = $total_avg_price;
    }
}

$convert_live_hour_chart_list = [];
if(!empty($convert_live_hour_list))
{
    foreach($convert_live_hour_list as $hour_live_key => $hour_live_data)
    {
        $total_price        = $hour_live_data['total_price'];
        $total_ord_count    = $hour_live_data['total_cnt'];
        $total_avg_price    = ($total_ord_count > 0) ? round($total_price/$total_ord_count) : 0;

        $convert_live_hour_list[$hour_live_key]["avg_price"] = $total_avg_price;

        $convert_live_hour_chart_list[0]['title']   = "아임웹 매출";
        $convert_live_hour_chart_list[0]['type']    = "line";
        $convert_live_hour_chart_list[0]['color']   = "rgba(128,100,162)";
        $convert_live_hour_chart_list[0]['data'][]  = $total_price;
        $convert_live_hour_chart_list[1]['title']   = "주문건당 단가";
        $convert_live_hour_chart_list[1]['type']    = "line";
        $convert_live_hour_chart_list[1]['color']   = "rgba(255,51,204)";
        $convert_live_hour_chart_list[1]['data'][]  = $total_avg_price;
    }
}

$smarty->assign("convert_week_list", $convert_week_list);
$smarty->assign("convert_date_list", $convert_date_list);
$smarty->assign("convert_hour_list", $convert_hour_list);
$smarty->assign("convert_count_week_list", $convert_count_week_list);
$smarty->assign("convert_count_week_cnt", count($convert_count_week_list));
$smarty->assign("convert_count_date_list", $convert_count_date_list);
$smarty->assign("convert_count_date_cnt", count($convert_count_date_list));
$smarty->assign("convert_count_hour_list", $convert_count_hour_list);
$smarty->assign("convert_count_hour_cnt", count($convert_count_hour_list));
$smarty->assign("convert_live_week_list", $convert_live_week_list);
$smarty->assign("convert_live_week_cnt", count($convert_live_week_list));
$smarty->assign("convert_live_date_list", $convert_live_date_list);
$smarty->assign("convert_live_date_cnt", count($convert_live_date_list));
$smarty->assign("convert_live_hour_list", $convert_live_hour_list);
$smarty->assign("convert_live_hour_cnt", count($convert_live_hour_list));
$smarty->assign("convert_week_chart_name_list", json_encode($convert_week_list));
$smarty->assign("convert_date_chart_name_list", json_encode($convert_date_list));
$smarty->assign("convert_hour_chart_name_list", json_encode($convert_hour_list));
$smarty->assign("convert_count_week_chart_list", json_encode($convert_count_week_chart_list));
$smarty->assign("convert_count_date_chart_list", json_encode($convert_count_date_chart_list));
$smarty->assign("convert_count_hour_chart_list", json_encode($convert_count_hour_chart_list));
$smarty->assign("convert_live_week_chart_list", json_encode($convert_live_week_chart_list));
$smarty->assign("convert_live_date_chart_list", json_encode($convert_live_date_chart_list));
$smarty->assign("convert_live_hour_chart_list", json_encode($convert_live_hour_chart_list));

$smarty->display('work_cms_stats_traffic_conversion_page_iframe.html');
?>
