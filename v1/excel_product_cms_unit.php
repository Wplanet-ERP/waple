<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/product_cms.php');
require('inc/model/Kind.php');
require('Classes/PHPExcel.php');

if(!permissionNameCheck($session_permission,"마스터관리자") && !permissionNameCheck($session_permission,"물류관리자") && ($session_s_no != '22' && $session_s_no != '135' && $session_s_no != '102')){
    $smarty->display('access_company_error.html');
    exit;
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "No")
	->setCellValue('B3', "업체명/브랜드명")
	->setCellValue('C3', "공급업체")
	->setCellValue('D3', "발주담당자")
	->setCellValue('E3', "Display")
	->setCellValue('F3', "우선순위")
	->setCellValue('G3', "구성품목명")
	->setCellValue('H3', "물류업체")
	->setCellValue('I3', "구분")
  	->setCellValue('J3', "창고")
	->setCellValue('K3', "재고관리코드(SKU)")
	->setCellValue('L3', "공급단가(VAT 별도)")
	->setCellValue('M3', "공급단가(VAT 포함)")
	->setCellValue('N3', "재고수량")
	->setCellValue('O3', "재고알림 설정수량")
	->setCellValue('P3', "재고알림 연락처")
	->setCellValue('Q3', "Description")
	->setCellValue('R3', "불량사유");


# 검색쿼리
$add_where 			= " 1=1 ";
$sch_no 		 	= isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_brand  		= isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_team 	  	 	= isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no 	  	 	= isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$sch_sup_c_no 	 	= isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_display	 	= isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$sch_option_name 	= isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_wise_sku 		= isset($_GET['sch_wise_sku']) ? $_GET['sch_wise_sku'] : "";
$sch_type 	 		= isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_is_control		= isset($_GET['sch_is_control']) ? $_GET['sch_is_control'] : "";
$sch_log_c_no		= isset($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "";
$sch_sku		 	= isset($_GET['sch_sku']) ? $_GET['sch_sku'] : "";
$sch_description 	= isset($_GET['sch_description']) ? $_GET['sch_description'] : "";
$sch_bad_reason 	= isset($_GET['sch_bad_reason']) ? $_GET['sch_bad_reason'] : "";
$sch_is_load		= isset($_GET['sch_is_load']) ? $_GET['sch_is_load'] : "";
$sch_is_except_stock= isset($_GET['sch_is_except_stock']) ? $_GET['sch_is_except_stock'] : "";
$sch_is_in_out		= isset($_GET['sch_is_in_out']) ? $_GET['sch_is_in_out'] : "";

#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
$url_check_str 	= $_SERVER['QUERY_STRING'];
$url_chk_result = false;
if(empty($url_check_str)){
	$url_check_team_where   = getTeamWhere($my_db, $session_team);
	$url_check_sql    		= "SELECT count(*) as cnt FROM product_cms_unit pcu  WHERE (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$url_check_team_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$url_check_team_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$url_check_team_where}))) AND pcu.display = '1'";
	$url_check_query  		= mysqli_query($my_db, $url_check_sql);
	$url_check_result 		= mysqli_fetch_assoc($url_check_query);

	if($url_check_result['cnt'] == 0){
		$sch_team 		= "all";
		$url_chk_result = true;
	}
}

if (!empty($sch_no)) {
	$add_where .= " AND pcu.no = '{$sch_no}'";
}

if (!empty($sch_brand)) {
	$add_where .= " AND pcu.brand = '{$sch_brand}'";
}

$sch_team_code_where = "";
if (!empty($sch_team))
{
	if ($sch_team != "all") {
		$sch_team_code_where = getTeamWhere($my_db, $sch_team);
		$add_where 		 .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where})))";
	}
}else{
	$sch_team_code_where = getTeamWhere($my_db, $session_team);
	$add_where 		 .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$sch_team_code_where})))";
}

if (!empty($sch_s_no))
{
	if ($sch_s_no != "all") {
		$add_where 		 .= " AND (pcu.ord_s_no = '{$sch_s_no}' OR pcu.ord_sub_s_no = '{$sch_s_no}' OR pcu.ord_third_s_no = '{$sch_s_no}')";
	}
}else{
	if($sch_team == $session_team){
		$add_where		 .=" AND (pcu.ord_s_no = '{$session_s_no}' OR pcu.ord_sub_s_no = '{$session_s_no}' OR pcu.ord_third_s_no = '{$session_s_no}')";
	}
}

if (!empty($sch_sup_c_no)) {
	$add_where .= " AND pcu.sup_c_no = '{$sch_sup_c_no}'";
}

if (!empty($sch_display)) {
	$add_where .= " AND pcu.display = '{$sch_display}'";
}

if (!empty($sch_option_name)) {
	$add_where .= " AND pcu.option_name LIKE '%{$sch_option_name}%'";
}

if (!empty($sch_wise_sku)) {
	$add_where .= " AND pcu.wise_sku LIKE '%{$sch_wise_sku}%'";
}

if (!empty($sch_type)) {
	$add_where .= " AND pcu.type = '{$sch_type}'";
}

if (!empty($sch_is_control)) {
	$add_where .= " AND pcu.is_control = '{$sch_is_control}'";
}

if (!empty($sch_log_c_no)) {
	$add_where .= " AND pcu.`no` IN(SELECT pcum.prd_unit FROM product_cms_unit_management pcum WHERE pcum.log_c_no ='{$sch_log_c_no}')";
}

if (!empty($sch_sku)) {
	$add_where .= " AND pcu.`no` IN(SELECT pcum.prd_unit FROM product_cms_unit_management pcum WHERE pcum.sku LIKE '%{$sch_sku}%')";
}

if (!empty($sch_description)) {
	$add_where .= " AND pcu.description LIKE '%{$sch_description}%'";
}

if (!empty($sch_bad_reason)) {
	$add_where .= " AND pcu.bad_reason = '{$sch_bad_reason}'";
}

if (!empty($sch_is_load)) {
	if($sch_is_load == '1'){
		$add_where .= " AND (SELECT sub.pl_no FROM product_cms_load as sub WHERE sub.pl_no IN(SELECT DISTINCT plu.pl_no FROM product_cms_load_unit as plu WHERE plu.unit_no=pcu.`no`) AND sub.display='1' LIMIT 1) != ''";
	}elseif($sch_is_load == '2'){
		$add_where .= " AND (SELECT sub.pl_no FROM product_cms_load as sub WHERE sub.pl_no IN(SELECT DISTINCT plu.pl_no FROM product_cms_load_unit as plu WHERE plu.unit_no=pcu.`no`) AND sub.display='1' LIMIT 1) IS NULL";
	}
}

if (!empty($sch_is_except_stock)) {
	$add_where .= " AND pcu.`no` IN(SELECT sub.prd_unit FROM product_cms_unit_management as sub WHERE sub.is_except_stock='1')";
}

if (!empty($sch_is_in_out)) {
	$add_where .= " AND pcu.is_in_out='2'";
}

// 정렬순서 토글 & 필드 지정
$add_orderby = "pcu.no DESC";
$order		 = isset($_GET['od'])?$_GET['od']:"";
$order_type	 = isset($_GET['by'])?$_GET['by']:"";

if($order_type == '2'){
	$toggle = "DESC";
}else{
	$toggle = "ASC";
}
$order_field = array('','priority','option_name','sup_c_name');

if($order && $order<4)
{
	$add_orderby = " ISNULL({$order_field[$order]}) ASC, {$order_field[$order]} {$toggle}";
}

$last_stock_sql    = "SELECT stock_date FROM product_cms_stock ORDER BY stock_date DESC LIMIT 1";
$last_stock_query  = mysqli_query($my_db, $last_stock_sql);
$last_stock_result = mysqli_fetch_assoc($last_stock_query);
$last_stock_date   = isset($last_stock_result['stock_date']) ? $last_stock_result['stock_date'] : "";


# 리스트 쿼리
$product_cms_unit_sql = "
		SELECT
			*,
		   	(SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcu.sup_c_no) as sup_c_name,
		   	(SELECT sub_c.license_type FROM company as sub_c WHERE sub_c.c_no = pcu.sup_c_no) as license_type,
		   	(SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcu.brand) as brand_name,
			(SELECT s.team FROM staff s WHERE s.s_no=pcu.ord_s_no) as ord_team,
   			(SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_s_no) as ord_s_name,
			(SELECT s.team FROM staff s WHERE s.s_no=pcu.ord_sub_s_no) as ord_sub_team,
			(SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_sub_s_no) as ord_sub_s_name,
			(SELECT s.team FROM staff s WHERE s.s_no=pcu.ord_third_s_no) as ord_third_team,
			(SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_third_s_no) as ord_third_s_name,
		    (SELECT sub.pl_no FROM product_cms_load as sub WHERE sub.pl_no IN(SELECT DISTINCT plu.pl_no FROM product_cms_load_unit as plu WHERE plu.unit_no=pcu.`no`) AND sub.display='1' LIMIT 1) as pl_no
		FROM product_cms_unit pcu
		WHERE {$add_where}
		ORDER BY {$add_orderby}
";
$product_cms_unit_query = mysqli_query($my_db, $product_cms_unit_sql);
if(!!$product_cms_unit_query)
{
	$unit_type_option 		= getUnitTypeOption();
	$kind_model				= Kind::Factory();
	$bad_reason_option 		= $kind_model->getKindParentList("bad_reason");
	$product_cms_unit_list 	= [];
    while($product_cms_unit = mysqli_fetch_array($product_cms_unit_query))
    {
		$display = "";

		if($product_cms_unit['display'] == '2'){
			$display = "OFF";
		}elseif($product_cms_unit['display'] == '1'){
			$display = "ON";
		}

		$bad_reason = isset($bad_reason_option[$product_cms_unit['bad_reason']]) ? $bad_reason_option[$product_cms_unit['bad_reason']] : "";

		$manage_list = [];
		$unit_manage_sql 	= "
			SELECT
			    *,
			    (SELECT c.c_name FROM company c WHERE c.c_no=pcum.log_c_no) as log_c_name,
			    (SELECT SUM(pcs.qty) as stock FROM product_cms_stock pcs WHERE pcs.prd_unit=pcum.prd_unit AND pcs.stock_date='{$last_stock_date}' AND pcs.warehouse = pcum.warehouse) as stock
			FROM product_cms_unit_management pcum
			WHERE prd_unit = '{$product_cms_unit['no']}'
		";
		$unit_manage_query 	= mysqli_query($my_db, $unit_manage_sql);
		while($unit_manage = mysqli_fetch_assoc($unit_manage_query))
		{
			$unit_manage['warehouse_option'] = isset($log_warehouse_list[$unit_manage['log_c_no']]) ? $log_warehouse_list[$unit_manage['log_c_no']] : [];


			$product_cms_unit_list[] = array(
				"no" 			=> $product_cms_unit['no'],
				"brand_name" 	=> $product_cms_unit['brand_name'],
				"sup_c_name" 	=> $product_cms_unit['sup_c_name'],
				"ord_s_name" 	=> $product_cms_unit['ord_s_name'],
				"display" 		=> $display,
				"priority" 		=> $product_cms_unit['priority'],
				"option_name" 	=> $product_cms_unit['option_name'],
				"log_c_name" 	=> $unit_manage['log_c_name'],
				"type" 			=> $unit_type_option[$product_cms_unit['type']],
				"sku" 			=> $unit_manage['sku'],
				"sup_price" 	=> number_format($product_cms_unit['sup_price']),
				"stock" 		=> number_format($unit_manage['stock']),
				"qty_alert" 	=> number_format($unit_manage['qty_alert']),
				"qty_hp" 		=> $unit_manage['qty_hp'],
				"description" 	=> $product_cms_unit['description'],
				"bad_reason" 	=> $bad_reason,
        "warehouse" 	=> $unit_manage['warehouse'],
			);
		}
    }

	$idx = 4;
	foreach($product_cms_unit_list as $product_cms_unit)
	{
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$idx, $product_cms_unit['no'])
			->setCellValue('B'.$idx, $product_cms_unit['brand_name'])
			->setCellValue('C'.$idx, $product_cms_unit['sup_c_name'])
			->setCellValue('D'.$idx, $product_cms_unit['ord_s_name'])
			->setCellValue('E'.$idx, $product_cms_unit['display'])
			->setCellValue('F'.$idx, $product_cms_unit['priority'])
			->setCellValue('G'.$idx, $product_cms_unit['option_name'])
			->setCellValue('H'.$idx, $product_cms_unit['log_c_name'])
			->setCellValue('I'.$idx, $product_cms_unit['type'])
      ->setCellValue('J'.$idx, $product_cms_unit['warehouse'])
			->setCellValue('K'.$idx, $product_cms_unit['sku'])
			->setCellValue('L'.$idx, number_format($product_cms_unit['sup_price']))
			->setCellValue('M'.$idx, number_format($product_cms_unit['sup_price_vat']))
			->setCellValue('N'.$idx, number_format($product_cms_unit['stock']))
			->setCellValue('O'.$idx, number_format($product_cms_unit['qty_alert']))
			->setCellValue('P'.$idx, $product_cms_unit['qty_hp'])
			->setCellValue('Q'.$idx, $product_cms_unit['description'])
			->setCellValue('R'.$idx, $product_cms_unit['bad_reason'])
		;

		$idx++;
	}
}


$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "커머스 상품 구성품목 리스트");
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:R3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:R3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:R3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC0C0C0');

$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('I4:I'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('O4:O'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('P4:P'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Q4:Q'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('R4:R'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(29);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(29);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);


$objPHPExcel->getActiveSheet()->setTitle('커머스 상품 구성품목 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."커머스 상품 구성품목 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
