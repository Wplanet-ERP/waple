<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/visit.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "진행상태")
    ->setCellValue('B1', "접수일자")
    ->setCellValue('C1', "발주단계")
    ->setCellValue('D1', "고객명")
    ->setCellValue('E1', "연락처")
    ->setCellValue('F1', "주소1")
    ->setCellValue('G1', "주소2")
    ->setCellValue('H1', "방문코드")
    ->setCellValue('I1', "완료일자")
    ->setCellValue('J1', "유/무상")
    ->setCellValue('K1', "수금액")
;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00343a40');
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


# 검색조건 처리
$add_where          = "1=1";
$sch_v_no           = isset($_GET['sch_v_no']) ? $_GET['sch_v_no'] : "";
$sch_visit_state    = isset($_GET['sch_visit_state']) ? $_GET['sch_visit_state'] : "";
$sch_req_s_date     = isset($_GET['sch_req_s_date']) ? $_GET['sch_req_s_date'] : "";
$sch_req_e_date     = isset($_GET['sch_req_e_date']) ? $_GET['sch_req_e_date'] : "";
$sch_req_num        = isset($_GET['sch_req_num']) ? $_GET['sch_req_num'] : "";
$sch_visit_step     = isset($_GET['sch_visit_step']) ? $_GET['sch_visit_step'] : "";
$sch_customer       = isset($_GET['sch_customer']) ? $_GET['sch_customer'] : "";
$sch_prd_code       = isset($_GET['sch_prd_code']) ? $_GET['sch_prd_code'] : "";
$sch_visit_code     = isset($_GET['sch_visit_code']) ? $_GET['sch_visit_code'] : "";
$sch_req_content    = isset($_GET['sch_req_content']) ? $_GET['sch_req_content'] : "";
$sch_run_s_date     = isset($_GET['sch_run_s_date']) ? $_GET['sch_run_s_date'] : "";
$sch_run_e_date     = isset($_GET['sch_run_e_date']) ? $_GET['sch_run_e_date'] : "";
$sch_visit_free     = isset($_GET['sch_visit_free']) ? $_GET['sch_visit_free'] : "";
$sch_run_content    = isset($_GET['sch_run_content']) ? $_GET['sch_run_content'] : "";
$sch_center         = isset($_GET['sch_center']) ? $_GET['sch_center'] : "";
$sch_order_number   = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";

if(!empty($sch_v_no)) {
    $add_where .= " AND wcv.v_no='{$sch_v_no}'";
}

if(!empty($sch_visit_state)) {
    $add_where .= " AND wcv.visit_state='{$sch_visit_state}'";
}

if(!empty($sch_req_s_date)) {
    $add_where .= " AND wcv.req_date >= '{$sch_req_s_date}'";
}

if(!empty($sch_req_e_date)) {
    $add_where .= " AND wcv.req_date <= '{$sch_req_e_date}'";
}

if(!empty($sch_req_num)) {
    $add_where .= " AND wcv.req_num='{$sch_req_num}'";
}

if(!empty($sch_visit_step)) {
    $add_where .= " AND wcv.visit_step='{$sch_visit_step}'";
}

if(!empty($sch_customer)) {
    $add_where .= " AND wcv.customer like '%{$sch_customer}%'";
}

if(!empty($sch_prd_code)) {
    $add_where .= " AND wcv.prd_code='{$sch_prd_code}'";
}

if(!empty($sch_visit_code)) {
    $add_where .= " AND wcv.visit_code='{$sch_visit_code}'";
}

if(!empty($sch_req_content)) {
    $add_where .= " AND wcv.req_content like '%{$sch_req_content}%'";
}

if(!empty($sch_run_s_date)) {
    $add_where .= " AND wcv.run_date >= '{$sch_run_s_date}'";
}

if(!empty($sch_run_e_date)) {
    $add_where .= " AND wcv.run_date <= '{$sch_run_e_date}'";
}

if(!empty($sch_visit_free)) {
    $add_where .= " AND wcv.visit_free like '%{$sch_visit_free}%'";
}

if(!empty($sch_run_content)) {
    $add_where .= " AND wcv.run_content like '%{$sch_run_content}%'";
}

if(!empty($sch_center)) {
    $add_where .= " AND wcv.center like '%{$sch_center}%'";
}

if(!empty($sch_order_number)) {
    $add_where .= " AND wcv.order_number like '%{$sch_order_number}%'";
}

// 리스트 쿼리
$cms_visit_sql  = "
    SELECT 
        *
    FROM work_cms_visit `wcv`
    WHERE {$add_where}
    ORDER BY `wcv`.v_no DESC
";
$cms_visit_list     = [];
$cms_visit_query    = mysqli_query($my_db, $cms_visit_sql);
$visit_state_option = getVisitStateOption();
$idx                = 2;
if(!!$cms_visit_query)
{
    while($cms_visit = mysqli_fetch_assoc($cms_visit_query))
    {
        $visit_state_name = $visit_state_option[$cms_visit['visit_state']];

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $visit_state_name)
            ->setCellValue("B{$idx}", $cms_visit['req_date'])
            ->setCellValue("C{$idx}", $cms_visit['visit_step'])
            ->setCellValue("D{$idx}", $cms_visit['customer'])
            ->setCellValue("E{$idx}", $cms_visit['hp'])
            ->setCellValue("F{$idx}", $cms_visit['addr1'])
            ->setCellValue("G{$idx}", $cms_visit['addr2'])
            ->setCellValue("H{$idx}", $cms_visit['prd_code'])
            ->setCellValue("I{$idx}", $cms_visit['run_date'])
            ->setCellValue("J{$idx}", $cms_visit['visit_free'])
            ->setCellValue("K{$idx}", $cms_visit['visit_price'])
        ;
        $idx++;
    }
}
$idx--;

// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getStyle("A2:K{$idx}")->getFont()->setSize(9);;
$objPHPExcel->getActiveSheet()->getStyle("A2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A2:K{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);

$objPHPExcel->getActiveSheet()->setTitle("방문지원리스트");
$objPHPExcel->getActiveSheet()->getStyle("A1:K{$idx}")->applyFromArray($styleArray);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_방문지원리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
