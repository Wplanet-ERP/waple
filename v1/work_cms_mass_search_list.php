<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/_navigation.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');

# Navigation & My Quick
$nav_prd_no  = "56";
$nav_title   = "대량구매 조회";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
// [상품(업무) 종류]
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_get	= isset($_GET['sch'])?$_GET['sch']:"Y";

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);

if(!empty($sch_get)) {
    $smarty->assign("sch", $sch_get);
}

$kind_model     = Kind::Factory();
$product_model  = ProductCms::Factory();
$cms_code       = "product_cms";
$cms_group_list = $kind_model->getKindGroupList($cms_code);
$prd_total_list = $product_model->getPrdGroupData();
$prd_g1_list = $prd_g2_list = $prd_g3_list = [];

foreach($cms_group_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}

$prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
$sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);

// 동일 검색 및 수량
$sch_date               = isset($_GET['sch_date']) ? $_GET['sch_date'] : "";
$sch_recipient          = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp       = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_zipcode            = isset($_GET['sch_zipcode']) ? $_GET['sch_zipcode'] : "";
$sch_recipient_addr     = isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
$sch_dp_price           = isset($_GET['sch_dp_price']) ? $_GET['sch_dp_price'] : "1";
$sch_same_quantity_type = isset($_GET['sch_same_quantity_type']) ? $_GET['sch_same_quantity_type'] : "1";
$sch_same_quantity_1    = isset($_GET['sch_same_quantity_1']) ? $_GET['sch_same_quantity_1'] : "10";
$sch_same_quantity_2    = isset($_GET['sch_same_quantity_2']) ? $_GET['sch_same_quantity_2'] : "10";
$sch_same_quantity      = ($sch_same_quantity_type == '1') ? $sch_same_quantity_1 : $sch_same_quantity_2;
$sch_not_sel_same       = "";

if(empty($sch_recipient) && empty($sch_recipient_hp) && empty($sch_zipcode) && empty($sch_recipient_addr))
{
    $sch_not_sel_same   = '1';
    $sch_recipient      = '1';
    $sch_recipient_hp   = '1';
}

# 조건(where, group, having)
$add_where      = "1=1";
$add_group_list = [];
$add_group      = "";
$add_having     = "";
$add_order      = "ORDER BY sum_quantity DESC";
$btn_url_type_list = [];


// 상품(업무) 종류
if (!empty($sch_prd) && $sch_prd != "0") { // 상품
    $add_where .= " AND m.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND m.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND m.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

if(!empty($sch_recipient))
{
    $btn_url_type_list[] = "recipient";
    $add_group_list[]    = "recipient";
    $add_where .= " AND (m.recipient IS NOT NULL AND m.recipient != '')";
}

if(!empty($sch_recipient_hp))
{
    $btn_url_type_list[] = "recipient_hp";
    $add_group_list[]    = "recipient_hp";
    $add_where .= " AND (m.recipient_hp IS NOT NULL AND m.recipient_hp != '')";
}

if(!empty($sch_zipcode))
{
    $btn_url_type_list[] = "zip_code";
    $add_group_list[]    = "zip_code";
    $add_where .= " AND (m.zip_code IS NOT NULL AND m.zip_code != '')";
}

if(!empty($sch_recipient_addr))
{
    $btn_url_type_list[] = "recipient_addr";
    $add_group_list[]    = "recipient_addr";
    $add_where .= " AND (m.recipient_addr IS NOT NULL AND m.recipient_addr != '')";
}

if(!empty($sch_dp_price) && $sch_dp_price == '1')
{
    $add_where .= " AND m.sum_price > 0";
}

if(!empty($add_group_list))
{
    $add_group  = implode(', ', $add_group_list);
}

if(!!$add_group)
{
    $add_group .= ($sch_same_quantity_type == '1') ? ", prd_no" : ", order_number, prd_no";
    $add_where .= " AND `group` = '{$add_group}'";
    $add_where .= " AND sum_quantity >= {$sch_same_quantity}";
    if($sch_same_quantity_type == '1'){
        $btn_url_type_list[] = "prd_no";
    }elseif($sch_same_quantity_type == '2'){
        $btn_url_type_list[] = "order_number";
        $btn_url_type_list[] = "prd_no";
        $add_where .= " AND (m.order_number IS NOT NULL AND m.order_number != '')";
    }
}

$mass_cms_sql = "
    SELECT
    *
    FROM
    (SELECT 
        m.recipient,
        m.recipient_hp,
        m.zip_code,
        m.recipient_addr,
        m.c_name,
        m.prd_no,
        m.order_number,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=m.prd_no))) AS k_prd1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=m.prd_no)) AS k_prd2_name,
        (SELECT title from product_cms prd_cms where prd_cms.prd_no=m.prd_no) as prd_name,
        m.sum_quantity,
        m.sum_price
    FROM  mass_cms m
    WHERE 
    {$add_where}
    ) AS search
    {$add_order}
";

// 전체 게시물 수
$mass_cms_total_query	= mysqli_query($my_db, $mass_cms_sql);
if(!!$mass_cms_total_query)
    $mass_cms_total_result = mysqli_fetch_array($mass_cms_total_query);

$mass_cms_total = mysqli_num_rows($mass_cms_total_query);

//페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($mass_cms_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "work_cms_mass_search_list.php", $pagenum, $search_url);
$smarty->assign("total_num", number_format($mass_cms_total));
$smarty->assign("search_url", $search_url);
$smarty->assign("pagelist", $page);
$smarty->assign("ord_page_type", $page_type);

$mass_cms_sql .= "LIMIT {$offset}, {$num}";

$mass_cms_list  = [];
$mass_cms_query = mysqli_query($my_db, $mass_cms_sql);

while($mass_cms = mysqli_fetch_assoc($mass_cms_query))
{
    if(!empty($btn_url_type_list))
    {
        $btn_url = "https://work.wplanet.co.kr/v1/work_cms_mass_search_window.php?";
        foreach($btn_url_type_list as $btn_url_type)
        {
            $btn_url .= "{$btn_url_type}={$mass_cms[$btn_url_type]}&";
        }
        $mass_cms['btn_url'] = substr($btn_url, 0, -1);
    }

    $mass_cms_list[] = $mass_cms;
}

$total_num = !empty($mass_cms_list) ? count($mass_cms_list) : 0;

$smarty->assign("page_type_option", getPageTypeOption(3));
$smarty->assign("sch_recipient", $sch_recipient);
$smarty->assign("sch_recipient_hp", $sch_recipient_hp);
$smarty->assign("sch_zipcode", $sch_zipcode);
$smarty->assign("sch_recipient_addr", $sch_recipient_addr);
$smarty->assign("sch_dp_price", $sch_dp_price);
$smarty->assign("sch_same_quantity_type", $sch_same_quantity_type);
$smarty->assign("sch_same_quantity_1", $sch_same_quantity_1);
$smarty->assign("sch_same_quantity_2", $sch_same_quantity_2);
$smarty->assign("mass_cms_list", $mass_cms_list);

$smarty->display('work_cms_mass_search_list.html');

?>
