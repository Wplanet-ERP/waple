<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/withdraw.php');
require('inc/model/MyCompany.php');
require('Classes/PHPExcel.php');

# Create new PHPExcel object & SET document
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

# 상단타이틀 설정
$work_sheet			= $objPHPExcel->setActiveSheetIndex(0);
$work_sheet->mergeCells('A1:S1');
$work_sheet->setCellValue('A1', "출금 리스트");

$header_title_list  = array("wd_no","작성일","작성자","출금상태","출금완료일","출금 하는 업체명","지급처","은행명","예금주","계좌번호","관련상품","계열사","업체명","업체담당자","출금명","공급가액(VAT/소득세 별도)","출금요청액(VAT/소득세 포함)","출금완료액(VAT/소득세 포함)","관리자메모");
$head_idx           = 0;
foreach(range('A', 'S') as $columnID){
	$work_sheet->setCellValue("{$columnID}3", $header_title_list[$head_idx]);
	$head_idx++;
}

$is_search  = false;
if(permissionNameCheck($session_permission, "재무관리자") || permissionNameCheck($session_permission, "마스터관리자")
	|| ($session_team == '00222' || $session_team == '00244' || $session_team == '00211' || $session_team == '00221' || $session_s_no == '62'))
{
	$is_search = true;
}

# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where 			= " 1=1 AND display = 1";
$sch_wd_no 			= isset($_GET['sch_wd_no']) ? $_GET['sch_wd_no'] : "";
$sch_w_req_date 	= isset($_GET['sch_w_req_date']) ? $_GET['sch_w_req_date'] : "";
$sch_wd_state 		= isset($_GET['sch_wd_state']) ? $_GET['sch_wd_state'] : "";
$sch_wd_method 		= isset($_GET['sch_wd_method']) ? $_GET['sch_wd_method'] : $base_wd_method;
$sch_s_wd_date 		= isset($_GET['sch_s_wd_date']) ? $_GET['sch_s_wd_date'] : "";
$sch_e_wd_date 		= isset($_GET['sch_e_wd_date']) ? $_GET['sch_e_wd_date'] : "";
$sch_wd_tax_state 	= isset($_GET['sch_wd_tax_state']) ? $_GET['sch_wd_tax_state'] : "";
$sch_reg_s_no 		= isset($_GET['sch_reg_s_no']) ? $_GET['sch_reg_s_no'] : "";
$sch_wd_etc 		= isset($_GET['sch_wd_etc']) ? $_GET['sch_wd_etc'] : "";
$sch_my_c_no 		= isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_wd_account		= isset($_GET['sch_wd_account']) ? $_GET['sch_wd_account'] : "";
$sch_prd_no			= isset($_GET['sch_prd_no'])?$_GET['sch_prd_no']:"";
$sch_c_name 		= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_c_no 			= isset($_GET['sch_c_no'])?$_GET['sch_c_no']:"";
$sch_subject 		= isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
$sch_w_my_c_no 		= isset($_GET['sch_w_my_c_no']) ? $_GET['sch_w_my_c_no'] : "";
$sch_w_c_no 		= isset($_GET['sch_w_c_no'])?$_GET['sch_w_c_no']:"";
$sch_w_c_name 		= isset($_GET['sch_w_c_name']) ? $_GET['sch_w_c_name'] : "";
$sch_w_c_equal 		= isset($_GET['sch_w_c_equal']) ? $_GET['sch_w_c_equal'] : "";
$sch_w_team 		= isset($_GET['sch_w_team']) ? $_GET['sch_w_team'] : "";
$sch_w_s_name 		= isset($_GET['sch_w_s_name']) ? $_GET['sch_w_s_name'] : "";
$sch_is_inspection 	= isset($_GET['sch_is_inspection']) ? $_GET['sch_is_inspection'] : "";
$sch_is_approve 	= isset($_GET['sch_is_approve']) ? $_GET['sch_is_approve'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

if($session_team != '00211' && $session_s_no != '62') {
	$add_where .= " AND (wd.s_no ='{$session_s_no}' OR wd.reg_s_no='{$session_s_no}')";
}

if(!$is_search && !isset($_GET['sch_w_s_name'])){
	$sch_w_s_name = $session_name;
}

if($session_s_no == '62'){
	$sch_w_my_c_no_get = 1;
}

if(!empty($sch_brand)) {
	$add_where .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
	$add_where .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
	$add_where .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

if (!empty($sch_wd_no)) {
	$add_where .= " AND wd.wd_no='{$sch_wd_no}'";
}

if (!empty($sch_w_req_date)) {
	$add_where .= " AND wd.regdate like '%{$sch_w_req_date}%'";
}

if (!empty($sch_wd_state)) {
	$add_where .= " AND wd.wd_state='{$sch_wd_state}'";
}

if (!empty($sch_wd_method)) {
	$add_where .= " AND wd.wd_method='{$sch_wd_method}'";
}

if (!empty($sch_s_wd_date)) {
	$add_where .= " AND wd.wd_date >= '{$sch_s_wd_date}'";
}

if (!empty($sch_e_wd_date)) {
	$add_where .= " AND wd.wd_date <= '{$sch_e_wd_date}'";
}

if (!empty($sch_wd_tax_state)) {
	$add_where .= " AND wd.wd_tax_state='{$sch_wd_tax_state}'";
}

if (!empty($sch_reg_s_no)) {
	$add_where .= " AND wd.reg_s_no IN (SELECT sub.s_no FROM staff sub WHERE sub.s_name like '%{$sch_reg_s_no}%')";
}

if (!empty($sch_wd_etc)) {
	if ($sch_wd_etc == "1") {
		$add_where .= " AND  ";
	} elseif ($sch_wd_etc == "2") {
		$add_where .= " AND  ";
	}
}

if (!empty($sch_my_c_no)) {
	$add_where .= " AND wd.my_c_no = '{$sch_my_c_no}'";
}

if (!empty($sch_wd_account)) {
	$add_where .= " AND wd.wd_account = '{$sch_wd_account}'";
}

if(!empty($sch_prd_no)) { // 상품
	$add_where.=" AND `w`.prd_no='{$sch_prd_no}'";
}

if($sch_c_no == 'null') {
	$add_where.=" AND wd.c_no ='0'";
}

if (!empty($sch_c_name)) {
	$add_where .= " AND wd.c_name like '%{$sch_c_name}%'";
}

if (!empty($sch_subject)) {
	$add_where .= " AND wd.wd_subject like '%{$sch_subject}%'";
}

if($sch_w_c_no) {
	$add_where .= " AND `w`.c_no = '0' AND `w`.task_run_regdate >= '2020-01-01 00:00:00'";
}
else
{
	if(!empty($sch_w_my_c_no))
	{
		$add_where .= " AND (SELECT cp.my_c_no FROM company cp WHERE cp.c_no =`w`.c_no) = '{$sch_w_my_c_no}'";
	}

	if (!empty($sch_w_c_name))
	{
		if(!empty($sch_w_c_equal)){
			$add_where .= " AND `w`.c_name = '{$sch_w_c_name}'";
		}else{
			$add_where .= " AND `w`.c_name LIKE '%{$sch_w_c_name}%'";
		}
	}
}

if(!$is_search){
	$sch_w_team = $session_team;
}

if(!empty($sch_w_team))
{
	if ($sch_w_team != "all")
	{
		if($sch_w_team == "00257"){
			$team_where_1 	= getTeamWhere($my_db, $sch_w_team);
			$team_where_2 	= getTeamWhere($my_db, "00252");
			$team_where 	= $team_where_1.",".$team_where_2;
		}else{
			$team_where = getTeamWhere($my_db, $sch_w_team);
		}

		$add_where .= " AND wd.team IN({$team_where})";
	}
}

if(!empty($sch_w_s_name)) {
	$add_where .= " AND wd.s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_w_s_name}%')";
}

if (!empty($sch_is_inspection)){
	$add_where .= " AND wd.is_inspection='{$sch_is_inspection}'";
}

if (!empty($sch_is_approve)) {
	$add_where .= " AND wd.is_approve='{$sch_is_approve}'";
}

if ($sch_wd_state == "2" || $sch_wd_state == "3")
	$add_orderby = " wd.wd_date DESC ";
else {
	if (permissionNameCheck($session_permission, "마케터"))
		$add_orderby = " case when wd.s_no = 0 then 99999999 + wd.wd_no else wd.wd_no end DESC ";
	else
		$add_orderby = " wd.wd_no DESC ";
}

# 리스트 쿼리
$withdraw_sql = "
	SELECT
		wd.wd_no,
	   	wd.my_c_no,
		wd.s_no,
		(SELECT s_name FROM staff s WHERE s.s_no=wd.s_no) AS s_name,
		wd.c_no,
		wd.c_name,
		wd.wd_state,
		wd.wd_method,
		wd.w_no,
		(SELECT title FROM product prd WHERE prd.prd_no=w.prd_no) AS product_title,
		wd.wd_date,
		wd.wd_subject,
		wd.supply_cost,
		wd.supply_cost_vat,
		wd.cost,
		wd.wd_money,
		wd.wd_tax_state,
		wd.wd_tax_date,
		wd.wd_tax_item,
		wd.bk_title,
		wd.bk_name,
		wd.bk_num,
		wd.regdate,
		(SELECT s_name FROM staff s WHERE s.s_no=wd.reg_s_no) AS reg_s_name,
		(select concat(o_registration, '||', o_accdoc, '||', bk_title, '||', bk_num, '||', bk_name) from company where c_no = wd.c_no) tx_info,
		wd.manager_memo,
		(SELECT cp.my_c_no FROM company cp WHERE cp.c_no = w.c_no) as w_my_c_no,
		w.c_name as w_c_name,
		wd.display
	FROM withdraw wd
	LEFT JOIN `work` AS w ON w.wd_no=wd.wd_no
	WHERE {$add_where}
	ORDER BY {$add_orderby}
	LIMIT 2000
";
$withdraw_query		= mysqli_query($my_db, $withdraw_sql);
$wd_state_option 	= getWdStateOption();
$my_comp_model		= MyCompany::Factory();
$my_comp_list 		= $my_comp_model->getList();
$non_display_list 	= [];
$idx 				= 4;
while ($withdraw_array = mysqli_fetch_array($withdraw_query))
{
	$supply_cost_value 	= "";
	$cost_value			= "";
	$wd_money_value 	= "";

	if(!empty($withdraw_array['supply_cost'])){
		$supply_cost_value = ($session_id == "ykkim11_" || $session_id == "ydh7834") ? $withdraw_array['supply_cost'] : number_format($withdraw_array['supply_cost']);
	}

	if(!empty($withdraw_array['cost'])){
		$cost_value = ($session_id == "ykkim11_" || $session_id == "ydh7834") ? $withdraw_array['cost'] : number_format($withdraw_array['cost']);
	}

	if(!empty($withdraw_array['wd_money'])){
		$wd_money_value = ($session_id == "ykkim11_" || $session_id == "ydh7834") ? $withdraw_array['wd_money'] : number_format($withdraw_array['wd_money']);
	}

	$my_c_name 	= "";
	$my_c_color = "#000000";
	if(!empty($withdraw_array['my_c_no'])) {
		$my_c_name  = isset($my_comp_list[$withdraw_array['my_c_no']]) ? $my_comp_list[$withdraw_array['my_c_no']]['c_name'] : "";
		$my_c_color = ($my_c_name != "") ? $my_comp_list[$withdraw_array['my_c_no']]['color'] : "#000000";
	}

	$w_my_c_name  = "";
    $w_my_c_color = "#000000";
	if(!empty($withdraw_array['w_my_c_no'])) {
        $w_my_c_name  = isset($my_comp_list[$withdraw_array['w_my_c_no']]) ? $my_comp_list[$withdraw_array['w_my_c_no']]['c_name'] : "";
        $w_my_c_color = ($w_my_c_name != "") ? $my_comp_list[$withdraw_array['w_my_c_no']]['color'] : "#000000";
	}

	$manager_memo = $withdraw_array['manager_memo'];
	if(isset($withdraw_array['display']) && $withdraw_array['display'] == '2'){
		$manager_memo 	   .= "삭제내역";
		$non_display_list[]	= $idx;
	}

	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue("A{$idx}", $withdraw_array['wd_no'])
		->setCellValue("B{$idx}", $withdraw_array['regdate'])
		->setCellValue("C{$idx}", $withdraw_array['reg_s_name'])
		->setCellValue("D{$idx}", $wd_state_option[$withdraw_array['wd_state']])
		->setCellValue("E{$idx}", isset($withdraw_array['wd_date']) ? date("Y-m-d", strtotime($withdraw_array['wd_date'])) : "")
		->setCellValue("F{$idx}", $my_c_name)
		->setCellValue("G{$idx}", $withdraw_array['c_name'])
		->setCellValue("H{$idx}", $withdraw_array['bk_title'])
		->setCellValue("I{$idx}", $withdraw_array['bk_name'])
		->setCellValueExplicit("J{$idx}", $withdraw_array['bk_num'], PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValue("K{$idx}", $withdraw_array['product_title'])
		->setCellValue("L{$idx}", $w_my_c_name)
		->setCellValue("M{$idx}", $withdraw_array['w_c_name'])
		->setCellValue("N{$idx}", $withdraw_array['s_name'])
		->setCellValue("O{$idx}", $withdraw_array['wd_subject'])
		->setCellValue("P{$idx}", $supply_cost_value)
		->setCellValue("Q{$idx}", $cost_value)
		->setCellValue("R{$idx}", $wd_money_value)
		->setCellValue("S{$idx}", $manager_memo)
	;

	$objPHPExcel->getActiveSheet()->getStyle("F{$idx}")->getFont()->getColor()->setRGB($my_c_color);
	$objPHPExcel->getActiveSheet()->getStyle("L{$idx}")->getFont()->getColor()->setRGB($w_my_c_color);

	$idx++;
}
$idx--;

$objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A3:S{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:S3")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A3:S3')->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("A4:S{$idx}")->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A3:S3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00c4bd97');
$objPHPExcel->getActiveSheet()->getStyle("A3:S{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A3:S{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("L4:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("M4:M{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("O4:O{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("P4:R{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("S4:S{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

if($session_id == "ykkim11_" || $session_id == "ydh7834"){
	$objPHPExcel->getActiveSheet()->getStyle("P4:Q{$idx}")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
}

$i2_length 	= $idx;
$i2			= 3;
while($i2 <= $i2_length)
{
	$objPHPExcel->getActiveSheet()->getRowDimension($i2)->setRowHeight(20);

	if($i2 > 3 && $i2 % 2 == 1)
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i2.':R'.$i2)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00EBF1DE');

	$i2++;
}

if(!empty($non_display_list))
{
	foreach($non_display_list as $non_display){
		$objPHPExcel->getActiveSheet()->getStyle("A{$non_display}:R{$non_display}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FF0000');
	}
}

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(40);

$objPHPExcel->getActiveSheet()->setTitle('출금 리스트');
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename = iconv('UTF-8','EUC-KR',date("Ymd")."_".$session_name."_출금 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
