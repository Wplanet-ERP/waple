<?php
require('inc/common.php');
require('ckadmin.php');

$process    = isset($_POST['process']) ? $_POST['process'] : "";
$quick_prd  = isset($_POST['quick_prd']) ? $_POST['quick_prd'] : "";
$quick_type = isset($_POST['quick_type']) ? $_POST['quick_type'] : "";

$msg        = "변경되지 않았습니다";
$status     = "";
$result     = false;

if($quick_prd && $quick_type)
{
    if($process == "add_quick_prd")
    {
        $chk_sql    = "SELECT * FROM quick_search WHERE s_no='{$session_s_no}' AND `type`='{$quick_type}' AND prd_no='{$quick_prd}'";
        $chk_query  = mysqli_query($my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        if(empty($chk_result))
        {
            $chk_priority_sql    = "SELECT MAX(priority) as max_priority FROM quick_search WHERE s_no='{$session_s_no}' AND `type`='{$quick_type}'";
            $chk_priority_query  = mysqli_query($my_db, $chk_priority_sql);
            $chk_priority_result = mysqli_fetch_assoc($chk_priority_query);
            $chk_priority        = !empty($chk_priority_result) ? $chk_priority_result['max_priority']+1 : 1;

            $ins_sql    = "INSERT INTO quick_search SET s_no='{$session_s_no}', `type`='{$quick_type}', prd_no='{$quick_prd}', priority='{$chk_priority}', regdate=now()";
            $status     = "add";
            $msg        = "MY QUICK SET 으로 추가됬습니다.";

            if(mysqli_query($my_db, $ins_sql)) {
                $result = true;
            }
        }
    }
    elseif($process == "del_quick_prd")
    {
        $del_sql = "DELETE FROM quick_search WHERE s_no='{$session_s_no}' AND `type`='{$quick_type}' AND prd_no='{$quick_prd}'";
        $status  = "del";
        $msg     = "MY QUICK SET 에서 제외됬습니다.";

        if(mysqli_query($my_db, $del_sql)){
            $result = true;
        }
    }
    elseif($process == "sort_quick_prd")
    {
        $sort_quick_sql     = "SELECT * FROM quick_search WHERE s_no='{$session_s_no}' AND `type`='{$quick_type}' ORDER BY priority ASC";
        $sort_quick_query   = mysqli_query($my_db, $sort_quick_sql);
        $sort_quick_list    = [];
        while($sort_quick_result = mysqli_fetch_assoc($sort_quick_query))
        {
            $sort_quick_list[] = $sort_quick_result['q_no'];
        }

        $priority = 1;
        foreach($sort_quick_list as $sort_quick)
        {
            $upd_sql = "UPDATE quick_search SET priority='{$priority}' WHERE q_no='{$sort_quick}'";
            $priority++;

            mysqli_query($my_db, $upd_sql);
        }
    }
}


echo json_encode(array('result' => $result, 'status' => $status, 'msg' => $msg));

?>