<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

include_once('inc/phpExcelReader/reader.php');
require('inc/common.php');

$file = 'ttt.xls';

$data = new Spreadsheet_Excel_Reader();

$data->setOutputEncoding('UTF-8');

$data->read($file);


$p_no = "14863";
$kind = "5";
$c_no = "4446";

$ins_sql = "INSERT INTO application(p_no, c_no, kind, blog_url, nick, username, hp, email, cafe_id, zipcode, address1, address2, memo, ip, regdate) VALUES";
$comma = "";

for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++)
{
    $id	        = (string)trim(addslashes($data->sheets[0]['cells'][$i][1]));
    $nick 		= (string)trim(addslashes($data->sheets[0]['cells'][$i][2]));
    $username 	= (string)trim(addslashes($data->sheets[0]['cells'][$i][3]));
    $hp 		= (string)trim(addslashes($data->sheets[0]['cells'][$i][4]));
//    $blog_url 	= (string)trim(addslashes($data->sheets[0]['cells'][$i][5]));
    $email 		= (string)trim(addslashes($data->sheets[0]['cells'][$i][5]));
    $zipcode    = (string)trim(addslashes($data->sheets[0]['cells'][$i][6]));
    $addr1      = (string)trim(addslashes($data->sheets[0]['cells'][$i][7]));
    $addr2      = (string)trim(addslashes($data->sheets[0]['cells'][$i][8]));
    $memo       = (string)trim(addslashes($data->sheets[0]['cells'][$i][9]));
    $regdate	= date('Y-m-d H:i:s');
    $ip         = ip2long($_SERVER['REMOTE_ADDR']);
    $cafe_id    = "";


    if(!empty($id))
    {
        $blog_sql   = "SELECT cafe_id FROM application WHERE blog_url='{$id}' AND cafe_id is not null LIMIT 1";
        $blog_query = mysqli_query($my_db, $blog_sql);
        $blog_data  = mysqli_fetch_array($blog_query);
        $cafe_id    = isset($blog_data['cafe_id']) ? $blog_data['cafe_id'] : "";

        $ins_sql    .= $comma."('{$p_no}', '{$c_no}', '{$kind}', '{$id}', '{$nick}', '{$username}', '{$hp}', '{$email}', '{$cafe_id}','{$zipcode}','{$addr1}', '{$addr2}','{$memo}', '{$ip}', '{$regdate}')";
        $comma      = ' , ';
    }
}

mysqli_query($my_db, $ins_sql);
?>
