<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "oo_no")
	->setCellValue('B3', "진행상태")
	->setCellValue('C3', "진행기간(시작)")
	->setCellValue('D3', "진행기간(종료)")
	->setCellValue('E3', "구분")
	->setCellValue('F3', "OBJECT NAME")
	->setCellValue('G3', "부서/팀")
	->setCellValue('H3', "담당자")
	->setCellValue('I3', "1차점검일")
	->setCellValue('J3', "1차점검(완료점수)")
	->setCellValue('K3', "1차점검 진행현황 및 점검내용(면담내용)")
	->setCellValue('L3', "2차점검일")
	->setCellValue('M3', "2차점검(완료점수)")
	->setCellValue('N3', "2차점검 진행현황 및 점검내용(면담내용)")
	->setCellValue('O3', "완료점수/목표점수")
	->setCellValue('P3', "완료율")
	->setCellValue('Q3', "ok_no")
	->setCellValue('R3', "순번")
	->setCellValue('S3', "KEY RESULT NAME")
	->setCellValue('T3', "KEY RESULT 설정사유 및 근거")
	->setCellValue('U3', "품질평가")
	->setCellValue('V3', "품질등급")
	->setCellValue('W3', "KR완료율")
	->setCellValue('X3', "과정 및 결과");


	// 리스트 내용
	$i=4;
	$ttamount=0;


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where = " 1=1 AND oo.display=1 AND (SELECT s.staff_state FROM staff s WHERE s.s_no=oo.s_no)='1'";
$add_orderby = "oo.oo_no DESC, ok.priority ASC, ok.ok_no ASC";

$sch_oo_no_get = isset($_GET['sch_oo_no']) ? $_GET['sch_oo_no'] : "";
$sch_state_get = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_kind_get = isset($_GET['sch_kind']) ? $_GET['sch_kind'] : "";
$sch_team_get = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no_get = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$sch_s_name_get = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
$sch_obj_name = isset($_GET['sch_obj_name']) ? $_GET['sch_obj_name'] : "";
$sch_s_date_term = isset($_GET['sch_s_date_term']) ? $_GET['sch_s_date_term'] : "";
$sch_e_date_term = isset($_GET['sch_e_date_term']) ? $_GET['sch_e_date_term'] : "";

if (!empty($sch_s_date_term)) {
	$add_where .= " AND oo.sdate='".$sch_s_date_term."'";
	$smarty->assign("sch_s_date_term", $sch_s_date_term);
}

if (!empty($sch_e_date_term)) {
	$add_where .= " AND oo.edate='".$sch_e_date_term."'";
	$smarty->assign("sch_e_date_term", $sch_e_date_term);
}

if (!empty($sch_oo_no_get)) {
	$add_where .= " AND oo.oo_no='" . $sch_oo_no_get . "'";
	$smarty->assign("sch_oo_no", $sch_oo_no_get);
}

if (!empty($sch_state_get)) {
	$add_where .= " AND oo.state='" . $sch_state_get . "'";
	$smarty->assign("sch_state", $sch_state_get);
}

if (!empty($sch_kind_get)) {
	$add_where .= " AND oo.kind='" . $sch_kind_get . "'";
	$smarty->assign("sch_kind", $sch_kind_get);
}

if (!empty($sch_team_get)) {
    if ($sch_team_get != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team_get);
        $add_where .= " AND team IN ({$sch_team_code_where})";
    }
}else{
    $sch_team_code_where = getTeamWhere($my_db, $session_team);
    $add_where .= " AND team IN ({$sch_team_code_where})";
}

if (!empty($sch_s_no_get)) {
	if ($sch_s_no_get != "all") {
		$add_where .= " AND (oo.s_no = '" . $sch_s_no_get . "' or oo.s_no = 0) ";
	}
	$smarty->assign("sch_s_no",$sch_s_no_get);
}else{
	if($sch_team_get == $session_team){
		$add_where .= " AND oo.s_no = '{$session_s_no}' ";
		$smarty->assign("sch_s_no", $session_s_no);
	}
}

if (!empty($sch_s_name_get)) {
    $add_where .= " AND oo.s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_s_name_get}%')";
    $smarty->assign("sch_s_name", $sch_s_name_get);
}

if (!empty($sch_obj_name)) {
	$add_where .= " AND oo.obj_name like '%" . $sch_obj_name . "%'";
	$smarty->assign("sch_obj_name", $sch_obj_name);
}

# 부서별 우선순위
$sch_team_sort 	 = isset($_GET['sch_team_sort']) ? $_GET['sch_team_sort'] : "";
if(!empty($sch_team_sort)){
    $add_orderby = "t_priority ASC, oo.oo_no DESC, ok.priority ASC, ok.ok_no ASC";
    $smarty->assign("sch_team_sort", $sch_team_sort);
}

// 리스트 쿼리

// OKR OBJECT 리스트 쿼리
$okr_sql = "
	SELECT
		oo.oo_no,
		oo.state,
		oo.sdate,
		oo.edate,
		oo.kind,
		oo.obj_name,
		oo.team,
		(SELECT team_name FROM team t where t.team_code=oo.team) AS team_name,
		(SELECT t.priority FROM team as t WHERE t.team_code=oo.team) as t_priority,
		oo.s_no,
		(SELECT s_name FROM staff s where s.s_no=oo.s_no) AS s_name,
		round((SELECT AVG(ok.importance) FROM okr_kr ok where ok.oo_no=oo.oo_no),1) AS importance,
		round((SELECT AVG(ok.difficulty) FROM okr_kr ok where ok.oo_no=oo.oo_no),1) AS difficulty,
		round((SELECT AVG(ok.challenge) FROM okr_kr ok where ok.oo_no=oo.oo_no),1) AS challenge,
		oo.check_1_date,
		oo.check_1_rate,
		oo.check_1_memo,
		oo.check_2_date,
		oo.check_2_rate,
		oo.check_2_memo,
		round((SELECT AVG(ok.complete_rate) FROM okr_kr ok WHERE ok.oo_no=oo.oo_no),1) AS avg_complete_rate,
		(SELECT COUNT(*) FROM okr_kr WHERE oo_no=oo.oo_no) AS ok_count,
		ok.ok_no,
		ok.kr_name,
		ok.kr_reason,
		ok.importance AS ok_importance,
		ok.difficulty AS ok_difficulty,
		ok.challenge AS ok_challenge,
		ok.complete_rate,
		ok.kr_result,
		ok.priority
	FROM okr_obj oo LEFT JOIN okr_kr ok
	ON oo.oo_no = ok.oo_no
	WHERE $add_where
	ORDER BY $add_orderby
";

// 리스트 최대치 400
$okr_sql .= "LIMIT 400";

$idx_no=1;
$result= mysqli_query($my_db, $okr_sql);
$first_oo_no = "";
for ($i = 4; $okr_array = mysqli_fetch_array($result); $i++)
{
    $quantity 	= ($okr_array['ok_importance']*2*3)+($okr_array['ok_difficulty']*2*3)+($okr_array['ok_challenge']*2*4);
    $grade_val  = floor($quantity/10);
    $grade 		= "F";
    switch($grade_val)
    {
        case 10: case 9: $grade="S"; break;
        case 8: $grade="A"; break;
        case 7: $grade="B"; break;
        case 6: $grade="C"; break;
        case 5: $grade="D"; break;
        case 4: $grade="E"; break;
        case 3: case 2: case 1: $grade="F"; break;
        default: $grade = "F";
    }

    #OKR 목표점수 및 완료점수 계산
    $okr_kr_sql = "SELECT importance, difficulty, challenge, complete_rate FROM okr_kr ok where ok.oo_no='{$okr_array['oo_no']}'";
    $okr_kr_query = mysqli_query($my_db, $okr_kr_sql);
    $purpose_quality  = 0;
    $complete_quality = 0;
    while($okr_kr_result = mysqli_fetch_assoc($okr_kr_query))
    {
        $importance_val 	= $okr_kr_result['importance'];
        $difficulty_val 	= $okr_kr_result['difficulty'];
        $challenge_val  	= $okr_kr_result['challenge'];
        $complete_rate_val  = $okr_kr_result['complete_rate'];

        $purpose_quality += round((($importance_val*0.3) + ($difficulty_val*0.3) + ($challenge_val*0.4)), 1);

        if($complete_rate_val > 0){
            $complete_rate = $complete_rate_val/100;
            $complete_quality += round((($importance_val*0.3*$complete_rate) + ($difficulty_val*0.3*$complete_rate) + ($challenge_val*0.4*$complete_rate)), 1);
        }
    }

	if($first_oo_no != $okr_array['oo_no'])
	{
		/* object 셀 합치기 */
		$merge_num = 0;
		if($okr_array['ok_count'] > 1){
			$merge_num = $i + $okr_array['ok_count'] - 1;
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':A'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$i.':B'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C'.$i.':C'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$i.':D'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E'.$i.':E'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$i.':F'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G'.$i.':G'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$i.':H'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I'.$i.':I'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$i.':J'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K'.$i.':K'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$i.':L'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('M'.$i.':M'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N'.$i.':N'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O'.$i.':O'.$merge_num);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P'.$i.':P'.$merge_num);
		}
		/* object 셀 합치기 */

		$first_oo_no = $okr_array['oo_no'];

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $okr_array['oo_no'])
		->setCellValue('B'.$i, $okr_obj_state[$okr_array['state']][0])
		->setCellValue('C'.$i, $okr_array['sdate'])
		->setCellValue('D'.$i, $okr_array['edate'])
		->setCellValue('E'.$i, $okr_obj_kind[$okr_array['kind']][0])
		->setCellValue('F'.$i, $okr_array['obj_name'])
		->setCellValue('G'.$i, $okr_array['team_name'])
		->setCellValue('H'.$i, $okr_array['s_name'])
		->setCellValue('I'.$i, $okr_array['check_1_date'])
		->setCellValue('J'.$i, $okr_array['check_1_rate'])
		->setCellValue('K'.$i, $okr_array['check_1_memo'])
		->setCellValue('L'.$i, $okr_array['check_2_date'])
		->setCellValue('M'.$i, $okr_array['check_2_rate'])
		->setCellValue('N'.$i, $okr_array['check_2_memo'])
		->setCellValue('O'.$i, $complete_quality." / ".$purpose_quality)
		->setCellValue('P'.$i, $okr_array['avg_complete_rate'])
		->setCellValue('Q'.$i, $okr_array['ok_no'])
		->setCellValue('R'.$i, $okr_array['priority'])
		->setCellValue('S'.$i, $okr_array['kr_name'])
		->setCellValue('T'.$i, $okr_array['kr_reason'])
		->setCellValue('U'.$i, $quantity)
		->setCellValue('V'.$i, $grade)
		->setCellValue('W'.$i, $okr_array['complete_rate'])
		->setCellValue('X'.$i, $okr_array['kr_result']);


	}else{ // 두번째 이상 OBJ인 경우
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('Q'.$i, $okr_array['ok_no'])
		->setCellValue('R'.$i, $okr_array['priority'])
		->setCellValue('S'.$i, $okr_array['kr_name'])
		->setCellValue('T'.$i, $okr_array['kr_reason'])
		->setCellValue('U'.$i, $quantity)
		->setCellValue('V'.$i, $grade)
		->setCellValue('W'.$i, $okr_array['complete_rate'])
		->setCellValue('X'.$i, $okr_array['kr_result']);
	}

}

if($i > 1)
	$i = $i-1;

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:X1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "OKR 리스트");
$objPHPExcel->getActiveSheet()->getStyle('A1:X1')->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle('A1:X1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


$objPHPExcel->getActiveSheet()->getStyle('A3:X'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:X'.$i)->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:X3')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle('A4:X'.$i)->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle('A3:X3')->getFont()->setSize(8);

$objPHPExcel->getActiveSheet()->getStyle('A3:X3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
															->getStartColor()->setARGB('00c4bd97');
$objPHPExcel->getActiveSheet()->getStyle('A3:X3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:X'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('I4:I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('O4:O'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('P4:P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Q4:Q'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('R4:R'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('S4:S'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('T4:T'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('U4:U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('V4:V'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('W4:W'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('X4:X'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$i)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$i)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('S4:S'.$i)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('T4:T'.$i)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('X4:X'.$i)->getAlignment()->setWrapText(true);
/*
$result= mysqli_query($my_db, $okr_sql);
for ($i2 = 4; $okr_array = mysqli_fetch_array($result); $i2++) {
	if($okr_array['ok_count'] > 1)
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:A9');
		//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i2.':A'.$i2+$okr_array['ok_count']);
}
*/
$i2_length = $i;
$i2=1;

while($i2_length){
	if($i2 > 2)
		$objPHPExcel->getActiveSheet()->getRowDimension($i2)->setRowHeight(20);

	if($i2 > 3 && $i2 % 2 == 1)
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i2.':X'.$i2)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ebf1de');

	$i2++;
	$i2_length--;
}

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(40);

$objPHPExcel->getActiveSheet()->setTitle('OKR 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);



$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_".$session_name."_OKR 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
