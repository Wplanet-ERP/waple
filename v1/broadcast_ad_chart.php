<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/broadcast.php');
require('inc/model/BroadCast.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "17";
$nav_title   = "방송광고 차트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 기본 검색
$sch_media      = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_advertiser = isset($_GET['sch_advertiser']) ? $_GET['sch_advertiser'] : "";
$sch_state      = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_title      = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_sub_s_date = isset($_GET['sch_sub_s_date']) && !empty($_GET['sch_sub_s_date']) ? $_GET['sch_sub_s_date'] : date('Y-m')."-01";
$sch_sub_e_date = isset($_GET['sch_sub_e_date']) && !empty($_GET['sch_sub_e_date']) ? $_GET['sch_sub_e_date'] : date('Y-m')."-31";
$sch_type       = isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_notice     = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";

$sch_detail_media   = isset($_GET['sch_detail_media']) ? $_GET['sch_detail_media'] : "";
$sch_subs_gubun     = isset($_GET['sch_subs_gubun']) ? $_GET['sch_subs_gubun'] : "";
$sch_delivery_gubun = isset($_GET['sch_delivery_gubun']) ? $_GET['sch_delivery_gubun'] : "";

$add_where = "1=1";
if(!empty($sch_media))
{
    $add_where .= " AND `ba`.media = '{$sch_media}'";
    $smarty->assign('sch_media', $sch_media);
}

if(!empty($sch_advertiser))
{
    $add_where .= " AND `ba`.advertiser like '%{$sch_advertiser}%'";
    $smarty->assign('sch_advertiser', $sch_advertiser);
}

if(!empty($sch_state))
{
    $add_where .= " AND `ba`.state = '{$sch_state}'";
    $smarty->assign('sch_state', $sch_state);
}

if(!empty($sch_title))
{
    $add_where .= " AND `ba`.program_title LIKE '%{$sch_title}%'";
    $smarty->assign('sch_title', $sch_title);
}

if(!empty($sch_sub_s_date))
{
    $add_where .= " AND `ba`.sub_s_date >= '{$sch_sub_s_date}'";
    $smarty->assign('sch_sub_s_date', $sch_sub_s_date);
}

if(!empty($sch_sub_e_date))
{
    $add_where .= " AND `ba`.sub_e_date <= '{$sch_sub_e_date}'";
    $smarty->assign('sch_sub_e_date', $sch_sub_e_date);
}

if(!empty($sch_type))
{
    $add_where .= " AND `ba`.`type` = '{$sch_type}'";
    $smarty->assign('sch_type', $sch_type);
}

if(!empty($sch_notice))
{
    $add_where .= " AND `ba`.notice LIKE '%{$sch_notice}%'";
    $smarty->assign('sch_notice', $sch_notice);
}

if(!empty($sch_detail_media))
{
    $add_where .= " AND `ba`.detail_media = '{$sch_detail_media}'";
    $smarty->assign('sch_detail_media', $sch_detail_media);
}

if(!empty($sch_subs_gubun))
{
    $add_where .= " AND `ba`.subs_gubun = '{$sch_subs_gubun}'";
    $smarty->assign('sch_subs_gubun', $sch_subs_gubun);
}

if(!empty($sch_delivery_gubun))
{
    $add_where .= " AND `ba`.delivery_gubun = '{$sch_delivery_gubun}'";
    $smarty->assign('sch_delivery_gubun', $sch_delivery_gubun);
}

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

# X축(day: 날짜, media: 매체, date: 요일, time: 시간대, advertiser: 광고주)
$sch_x_type       = isset($_GET['sch_x_type']) ? $_GET['sch_x_type'] : "day";
$sch_x_type_main  = isset($_GET['sch_x_type_main']) ? $_GET['sch_x_type_main'] : "";

if($sch_x_type == 'day' && $sch_x_type_main == ''){
    $sch_x_type_main = '1';
}

# Model & Helper 처리
$ba_state_option        = getBaStateOption();
$ba_media_option        = getBaMediaOption();
$type_option            = getBaXyTypeOption();
$z_type_option          = getBaZtypeOption();
$ord_type_option        = getBaOrdTypeOption();
$type_day_option        = getDayChartOption();
$type_media_option      = getBaMediaOption();
$type_date_option       = getDateChartOption();
$type_date_desc_option  = getDateDescChartOption();
$type_time_option       = getTimeChartOption();

$broadcastModel = BroadCast::Factory();
$broadcastModel->setBaAllTypeOption();
$ba_type_option             = $broadcastModel->getBaTypeOption();
$ba_detail_media_option     = $broadcastModel->getBaDetailMediaOption();
$ba_subs_gubun_option       = $broadcastModel->getBaSubsOption();
$ba_delivery_gubun_option   = $broadcastModel->getBaDeliveryOption();
$type_advertiser_option     = $broadcastModel->getAdvertiseOption();

$sch_x_type_list  = $broadcastModel->getBaSchType($sch_sub_s_date, $sch_sub_e_date, $sch_x_type, $sch_x_type_main, $type_day_option, $type_media_option, $type_date_option, $type_time_option, $type_advertiser_option);

$x_type_title      = $sch_x_type_list['type_title'];
$x_type_main_list  = $sch_x_type_list['type_main_list'];
$x_name_list       = $sch_x_type_list['type_name_list'];
$broadcast_x_total_list = $sch_x_type_list['type_total_list'];

$smarty->assign('x_type_option', $type_option);
$smarty->assign('x_type_main_option', $x_type_main_list);
$smarty->assign('sch_x_type', $sch_x_type);
$smarty->assign('sch_x_type_main', $sch_x_type_main);


# Y축(day: 날짜, media: 매체, date: 요일, time: 시간대, advertiser: 광고주)
$sch_y_type       = isset($_GET['sch_y_type']) ? $_GET['sch_y_type'] : "media";
$sch_y_type_main  = isset($_GET['sch_y_type_main']) ? $_GET['sch_y_type_main'] : "";

if($sch_y_type == 'day' && $sch_y_type_main == ''){
    $sch_y_type_main = '1';
}

$sch_y_type_list   = $broadcastModel->getBaSchType($sch_sub_s_date, $sch_sub_e_date, $sch_y_type, $sch_y_type_main, $type_day_option, $type_media_option, $type_date_option, $type_time_option, $type_advertiser_option);

$y_type_title      = $sch_y_type_list['type_title'];
$y_type_main_list  = $sch_y_type_list['type_main_list'];
$y_name_list       = $sch_y_type_list['type_name_list'];
$broadcast_y_total_list = $sch_x_type_list['type_total_list'];

$smarty->assign('y_type_option', $type_option);
$smarty->assign('y_type_main_option', $y_type_main_list);
$smarty->assign('sch_y_type', $sch_y_type);
$smarty->assign('sch_y_type_main', $sch_y_type_main);

# Z축 비교, 비율
$sch_chart_type = isset($_GET['sch_chart_type']) ? $_GET['sch_chart_type'] : "compare";
$smarty->assign('sch_chart_type', $sch_chart_type);

# Z축(1.원(광고료), 2.건(횟수))
$sch_z_type         = isset($_GET['sch_z_type']) ? $_GET['sch_z_type'] : "1";
$z_type_title       = ($sch_z_type == '3') ? "횟수" : "광고료";
$z_type_value_title = ($sch_z_type == '3') ? "건" : "원";

$smarty->assign('z_type_option', $z_type_option);
$smarty->assign('z_type_value_title', $z_type_value_title);
$smarty->assign('sch_z_type', $sch_z_type);

# 정렬
$sch_ord_type = isset($_GET['sch_ord_type']) ? $_GET['sch_ord_type'] : "0";
$smarty->assign('ord_type_option', $ord_type_option);
$smarty->assign('sch_ord_type', $sch_ord_type);

$broadcast_sql  = "
    SELECT
      media,
      adv_no,
      advertiser,
      unit_price,
      total_price,
      billing_price,
      date_w,
      DATE_FORMAT(`ba`.s_time, '%H') as s_time,
      DATE_FORMAT(`ba`.sub_s_date, '%Y%m') as sub_s_mon,
      DATE_FORMAT(`ba`.sub_e_date, '%Y%m') as sub_e_mon,
      DATE_FORMAT(DATE_SUB(`ba`.sub_s_date, INTERVAL (DAYOFWEEK(`ba`.sub_s_date)-1) DAY), '%Y%m%d') as sub_s_week,
      DATE_FORMAT(DATE_SUB(`ba`.sub_e_date, INTERVAL (DAYOFWEEK(`ba`.sub_e_date)-1) DAY), '%Y%m%d') as sub_e_week,
      DATE_FORMAT(`ba`.sub_s_date, '%Y%m%d') as sub_s_date,
      DATE_FORMAT(`ba`.sub_e_date, '%Y%m%d') as sub_e_date
    FROM broadcast_advertisement `ba`
    WHERE {$add_where}
    ORDER BY sub_s_date, s_time ASC
";

$broadcast_result = [];
$broadcast_query = mysqli_query($my_db, $broadcast_sql);
while($broadcast = mysqli_fetch_assoc($broadcast_query))
{
    if($sch_z_type == '1'){
        $broadcast['z_type'] = (isset($broadcast['total_price']) && $broadcast['total_price']>0) ? $broadcast['unit_price'] : 0;
    }elseif($sch_z_type == '2'){
        $broadcast['z_type'] = (isset($broadcast['billing_price']) && $broadcast['billing_price']>0) ? $broadcast['unit_price'] : 0;
    }elseif($sch_z_type == '3'){
        $broadcast['z_type'] = '1';
    }

    $broadcast_result[] = $broadcast;
}

$broadcast_list = [];
$broadcast_sum_list = [];
$broadcast_compare_chart_list = [];
$broadcast_percent_chart_list = [];
$broadcast_percent_chart_sort_list = [];
$broadcast_ord_sum_list = [];

$max   = 0;
$x_list_cnt = 2;

if($x_name_list)
{
    switch ($sch_ord_type){
        case '0': case '1': break;
        case '2':
            if($sch_x_type == 'date'){
                $x_name_list = $type_date_desc_option;
            }else{
                arsort($x_name_list);
            }
            break;
        case '3':
            if($sch_x_type == 'date'){
                $x_name_list = $type_date_option;
            }else{
                asort($x_name_list);
            }
            break;
        case '4':
            if($sch_y_type == 'date'){
                $y_name_list = $type_date_desc_option;
            }else{
                arsort($y_name_list);
            }
            break;
        case '5':
            if($sch_y_type == 'date'){
                $y_name_list = $type_date_option;
            }else{
                asort($y_name_list);
            }
            break;
    }

    $x_list_cnt += count($x_name_list);
    if($broadcast_result)
    {
        foreach($x_name_list as $x_type => $x_type_value)
        {
            if(!isset($broadcast_sum_list[$x_type])){
                $broadcast_sum_list[$x_type] = 0;
            }

            foreach($y_name_list as $y_type => $y_type_value)
            {
                if(!isset($broadcast_list[$y_type][$x_type])){
                    $broadcast_list[$y_type][$x_type] = 0;
                }

                foreach($broadcast_result as $bc_data)
                {
                    if(calBcData($bc_data, $x_type, $sch_x_type, $sch_x_type_main, $type_date_option) && calBcData($bc_data, $y_type, $sch_y_type, $sch_y_type_main, $type_date_option))
                    {
                        $broadcast_list[$y_type][$x_type] += $bc_data['z_type'];
                        $broadcast_sum_list[$x_type] += $bc_data['z_type'];
                        $broadcast_ord_sum_list[$y_type] += $bc_data['z_type'];
                    }else{
                        $broadcast_ord_sum_list[$y_type] += 0;
                    }
                }
            }
        }

        if(($sch_ord_type == '0' || $sch_ord_type == '1') && $broadcast_ord_sum_list)
        {
            if($sch_ord_type == '0'){
                arsort($broadcast_ord_sum_list);
            }elseif($sch_ord_type == '1'){
                asort($broadcast_ord_sum_list);
            }

            $sort_keys = array_keys($broadcast_ord_sum_list);
            $sort_name_list = [];

            foreach ($sort_keys as $key) {
                $sort_name_list[$key] = $y_name_list[$key];
            }

            if($sort_name_list){
                $y_name_list = $sort_name_list;
            }
        }


        $x_idx = 0;
        foreach($x_name_list as $x_type => $x_type_value)
        {
            $y_idx = 0;
            foreach ($y_name_list as $y_type => $y_type_value) {
                $value  = $broadcast_list[$y_type][$x_type];
                if($max < $value){
                    $max = $value;
                }
                $broadcast_compare_chart_list[] = array($x_idx, $y_idx, $value);
                $broadcast_percent_chart_list[$y_type]['title']  = $y_type_value;
                $broadcast_percent_chart_list[$y_type]['data'][] = $value;
                $y_idx++;
            }
            $x_idx++;
        }

        if($broadcast_percent_chart_list)
        {
            foreach ($broadcast_percent_chart_list as $chart_list) {
                $broadcast_percent_chart_sort_list[] = $chart_list;
            }
        }
        $broadcast_total = count($broadcast_result);
    }
}else{
    $broadcast_total = 0;
}

$x_name_legend_list = array_values($x_name_list);
$y_name_legend_list = array_values($y_name_list);

$smarty->assign('x_name_list', $x_name_list);
$smarty->assign('y_name_list', $y_name_list);
$smarty->assign('x_legend_list', json_encode($x_name_legend_list));
$smarty->assign('y_legend_list', json_encode($y_name_legend_list));
$smarty->assign('x_type_title', $x_type_title);
$smarty->assign('y_type_title', $y_type_title);
$smarty->assign('z_type_title', $z_type_title);
$smarty->assign('z_type_value_title',$z_type_value_title);
$smarty->assign('x_list_cnt', $x_list_cnt);
$smarty->assign('max_value', $max);

$smarty->assign('ba_media_option', $ba_media_option);
$smarty->assign('ba_state_option', $ba_state_option);
$smarty->assign('ba_type_option', $ba_type_option);
$smarty->assign('ba_detail_media_option', $ba_detail_media_option);
$smarty->assign('ba_subs_gubun_option', $ba_subs_gubun_option);
$smarty->assign('ba_delivery_gubun_option', $ba_delivery_gubun_option);

$smarty->assign('broadcast_list', $broadcast_list);
$smarty->assign('broadcast_sum_list', $broadcast_sum_list);
$smarty->assign('broadcast_total', $broadcast_total);
$smarty->assign('broadcast_compare_chart_list', json_encode($broadcast_compare_chart_list));
$smarty->assign('broadcast_percent_chart_list', json_encode($broadcast_percent_chart_sort_list));

$smarty->display('broadcast_ad_chart.html');
?>
