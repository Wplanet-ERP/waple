<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Staff.php');

# 날짜별 검색
$today_s_w		 = date('w')-1;
$sch_date_type   = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$sch_date        = isset($_GET['sch_date']) ? $_GET['sch_date'] : date('Y-m');
$sch_s_month     = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m');
$sch_e_month     = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week      = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week      = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date      = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-10 day"));
$sch_e_date      = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_date', $sch_date);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);

# 조건
$add_where      = "1=1 AND work_state='6' AND task_run_s_no > 0 AND prd_no = 187";
$sch_s_no       = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$staff_model    = Staff::Factory();
$cs_staff_list  = $staff_model->getActiveTeamStaff("00244");
$cs_staff_list['295'] = '아웃소싱';
$cs_staff_list['309'] = '이채하';

if(!empty($sch_s_no))
{
    $add_where  .= " AND task_run_s_no='{$sch_s_no}'";
    $picker_list[$sch_s_no] = $cs_staff_list[$sch_s_no];
    $smarty->assign('sch_s_no', $sch_s_no);
}else{
    $picker_list = $cs_staff_list;
}

# CHART
# 전체 기간 조회 및 누적데이터 조회
$all_date_where   = "";
$all_date_key	  = "";
$add_date_where   = "";
$add_date_column  = "";
$stats_list       = [];

if($sch_date_type == '3') //월간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where  = " AND DATE_FORMAT(w.task_run_regdate, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_date_column = "DATE_FORMAT(w.task_run_regdate, '%Y%m')";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}')";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where  = " AND DATE_FORMAT(w.task_run_regdate, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_date_column = "DATE_FORMAT(DATE_SUB(w.task_run_regdate, INTERVAL(IF(DAYOFWEEK(w.task_run_regdate)=1,8,DAYOFWEEK(w.task_run_regdate))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '$sch_e_date')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_date_where  = " AND DATE_FORMAT(w.task_run_regdate, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '$sch_e_date' ";
    $add_date_column = "DATE_FORMAT(w.task_run_regdate, '%Y%m%d')";
}

$all_date_where .= " AND DATE_FORMAT(`allday`.Date, '%Y-%m-%d') NOT IN (SELECT DATE_FORMAT(h.h_date, '%Y-%m-%d') FROM holiday_info h) AND (DATE_FORMAT(`allday`.Date, '%w') != 0 AND DATE_FORMAT(`allday`.Date, '%w') != 6)";

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";

# 기본 STAT 리스트 Init 및 x key 및 타이틀 설정
$x_label_list       = [];
$x_table_th_list    = [];
$stats_list         = [];
if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        $x_label_list[$date['chart_key']]    = "'".$chart_title."'";
        $x_table_th_list[$date['chart_key']] = $chart_title;

        foreach($picker_list as $s_no => $label){
            $staff_key = "imweb_".$s_no;
            $stats_list[$date['chart_key']][$staff_key] = 0;
        }

        foreach($picker_list as $s_no => $label){
            $staff_key = "kakao_".$s_no;
            $stats_list[$date['chart_key']][$staff_key] = 0;
        }
    }
}

# Y 컬럼 Label
$y_label_list   = [];
$y_label_title  = [];
foreach($picker_list as $s_no => $s_name)
{
    $staff_key = "imweb_".$s_no;
    $y_label_list[$staff_key]  = $s_name;
    $y_label_title[$staff_key] = "아임웹+사방넷+네이버+구매평관리:".$s_name;
    $y_label_title['all'] = "합산";
}

foreach($picker_list as $s_no => $s_name)
{
    $staff_key = "kakao_".$s_no;
    $y_label_list[$staff_key]  = $s_name;
    $y_label_title[$staff_key] = "카카오톡+네이버톡톡+채널톡:".$s_name;
}

$stats_sql = "
    SELECT
        {$add_date_column} as key_date,
        w.task_run_s_no,
        w.k_name_code,
        w.quantity as qty,
        (SELECT s.staff_state FROM staff s WHERE s.s_no=w.task_run_s_no) as staff_state
    FROM `work` as w
    WHERE {$add_where} 
    {$add_date_where}
    ORDER BY key_date ASC
";
$stats_query = mysqli_query($my_db, $stats_sql);
while($stats = mysqli_fetch_assoc($stats_query))
{
    $imweb_key = "imweb_".$stats['task_run_s_no'];
    $kakao_key = "kakao_".$stats['task_run_s_no'];

    if($stats['k_name_code'] == '02235' || $stats['k_name_code'] == '02234' || $stats['k_name_code'] == '02417'){
        $stats_list[$stats['key_date']][$kakao_key] +=  $stats['qty'];
    }else{
        $stats_list[$stats['key_date']][$imweb_key] +=  $stats['qty'];
    }
}

$full_data = [];
foreach($stats_list as $sales_date => $stats_work_data)
{
    $date_qty_sum = 0;
    if($stats_work_data)
    {
        $qty_sum = 0;
        foreach ($stats_work_data as $key => $qty)
        {
            $full_data["each"]["line"][$key]['title']   = $y_label_list[$key];
            $full_data["each"]["line"][$key]['data'][]  = $qty;

            $full_data["each"]["bar"][$key]['title']  = $y_label_list[$key];
            $full_data["each"]["bar"][$key]['data'][] = $qty;
            $qty_sum += $qty;
            $date_qty_sum += $qty;
        }
    }

    $full_data["sum"]["line"]["all"]['title'] = "합산";
    $full_data["sum"]["line"]["all"]['data'][] = $date_qty_sum;
    $full_data["sum"]["bar"]["all"]['title']  = "합산";
    $full_data["sum"]["bar"]["all"]['data'][] = $date_qty_sum;
}

$smarty->assign('cs_staff_list', $cs_staff_list);
$smarty->assign('x_label_list', implode(',', $x_label_list));
$smarty->assign('x_table_th_list', json_encode($x_table_th_list));
$smarty->assign('legend_list', json_encode($y_label_list));
$smarty->assign('y_label_title', json_encode($y_label_title));
$smarty->assign('picker_list', $picker_list);
$smarty->assign('full_data', json_encode($full_data));

$smarty->display('wise_csm_work_chart_online.html');
?>
