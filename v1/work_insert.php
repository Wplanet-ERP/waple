<?php
ini_set('max_execution_time', '3600');
ini_set('memory_limit', '5G');

require('Classes/PHPExcel.php');
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/model/Company.php');
require('inc/model/Custom.php');
require('inc/model/Work.php');

# 파일 변수
$work_model     = Work::Factory();
$company_model  = Company::Factory();
$work_file      = $_FILES["log_work_file"];
$reg_date       = date("Y-m-d H:i:s");
$reg_s_no       = $session_s_no;
$work_date      = isset($_POST['log_work_date']) ? $_POST['log_work_date'] : $reg_date;

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($work_file['name']) && !empty($work_file['name'])){
    $upload_file_path = add_store_file($work_file, "upload_management");
    $upload_file_name = $work_file['name'];
}

$upload_data  = array(
    "upload_type"   => "5",
    "upload_date"   => date("Y-m-d", strtotime($work_date)),
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $reg_s_no,
    "regdate"       => $reg_date,
);
$upload_model->insert($upload_data);

$file_no         = $upload_model->getInsertId();
$excel_file_path = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($excel_file_path);
$excel->setActiveSheetIndex(0);

$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$ins_data_list              = [];

for ($i = 2; $i <= $totalRow; $i++)
{
    $brand_name     = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 업체명/브랜드명
    $c_no           = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 업체번호
    $prd_no         = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));    // 업무상품번호
    $wd_c_no        = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));    // 출금업체
    $wd_c_name      = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));    // 출금업체명
    $task_run       = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));    // 업무진행
    $wd_price       = (int)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));    // price
    $wd_price_vat   = (int)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    // price

    if(empty($prd_no)){
        break;
    }

    if($c_no){
        $brand_item = $company_model->getItem($c_no);
    }else{
        if($brand_name == '와이즈미디어커머스'){
            $brand_item = array("c_no" => 1113, "c_name" => $brand_name);
        }else{
            $brand_item = $company_model->getNameWhereItem($brand_name);
        }
    }

    $ins_data_list[]    = array(
        "work_state"        => 6,
        "file_no"           => $file_no,
        "regdate"           => $reg_date,
        "c_no"              => $brand_item['c_no'],
        "c_name"            => $brand_item['c_name'],
        "s_no"              => $brand_item['s_no'],
        "team"              => $brand_item['team'],
        "prd_no"            => $prd_no,
        "wd_price"          => $wd_price,
        "wd_price_vat"      => $wd_price_vat,
        "wd_c_name"         => $wd_c_name,
        "wd_c_no"           => $wd_c_no,
        "task_run"          => $task_run,
        "task_req_s_no"     => $reg_s_no,
        "task_req_team"     => $session_team,
        "task_run_s_no"     => $reg_s_no,
        "task_run_team"     => $session_team,
        "task_run_regdate"  => $work_date,
    );
}

if (!$work_model->multiInsert($ins_data_list)){
    echo "업무리스트 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    exit;
}else{
    exit("<script>alert('업무리스트에 반영 되었습니다.');location.href='work_list.php?sch_file_no={$file_no}';</script>");
}

?>
