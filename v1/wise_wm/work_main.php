<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_date.php');
require('../inc/helper/wise_wm.php');
require('../inc/helper/logistics.php');
require('../inc/helper/work.php');
require('../inc/model/MyQuick.php');
require('../inc/model/Calendar.php');
require('../inc/model/Team.php');
require('../inc/model/Staff.php');
require('../inc/model/Kind.php');

$dir_location = "../";
$smarty->assign("dir_location", $dir_location);

$wm_work_option    = getWorkDetailOption();
$sch_work          = isset($_GET['sch_work']) && !empty($_GET['sch_work']) ? $_GET['sch_work'] : "wm";
$sel_work_data     = isset($wm_work_option[$sch_work]) ? $wm_work_option[$sch_work] : [];

# Navigation & My Quick
$nav_prd_no     = $sel_work_data['nav_no'];
$wm_type_name   = $sel_work_data['title'];
$nav_title      = "WISE WM :: {$wm_type_name}";
$quick_model    = MyQuick::Factory();
$is_my_quick    = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

$date_name_option   = getDateChartOption();
$cur_date           = date("Y-m-d");
$cur_day            = date("w");
$cur_day_name       = $date_name_option[$cur_day];
$cur_date_name      = date("m/d")."({$cur_day_name})";

# MY 와플 업무현황
if($sch_work == "wm")
{
    $team_model         = Team::Factory();
    $staff_model        = Staff::Factory();
    $team_all_list      = $team_model->getTeamAllList();
    $staff_all_list     = $team_all_list['staff_list'];
    $sch_staff_list     = $staff_all_list['all'];
    $staff_all_list['00221']['167'] = "임태형";
    $team_name_list     = $team_model->getActiveTeamList();
    $work_quick_state   = getWorkStateQuickOption();

    $sch_team           = isset($_GET['sch_team']) ? $_GET['sch_team'] : (($session_team != "00236") ? $session_team : "00221");
    $sch_s_no           = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : $session_s_no;
    $sch_reg_s_date     = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : date("Y-m-d", strtotime("-1 weeks"));
    $sch_reg_e_date     = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : date("Y-m-d");
    $sch_reg_date_type  = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "week";
    $sch_work_quick     = isset($_GET['sch_work_quick']) && !empty($_GET['sch_work_quick']) ? $_GET['sch_work_quick'] : "2";
    $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
    $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
    $work_prev_date     = date("Y-m-d", strtotime("-10 days"));
    $work_prev_url_date = date("Y-m-d", strtotime("{$work_prev_date} -1 days"));
    $work_prev_datetime = "{$work_prev_date} 00:00:00";

    $add_work_where     = "work_state IN(3,4,5,6)";
    $add_null_where     = "work_state IN(3,4,5,6) AND (task_run_s_no=0 OR task_run_s_no IS NULL)";
    $add_prev_where     = "regdate < '{$work_prev_datetime}' AND `w`.work_state IN(3,4,5)";
    $add_prev_yet_where = "work_state IN(3,4,5,6) AND (task_run_s_no=0 OR task_run_s_no IS NULL) AND regdate < '{$work_prev_datetime}'";
    $add_req_where      = $add_work_where;
    $add_run_where      = "1=1";

    if(!empty($sch_reg_s_date)){
        $add_work_where     .= ($sch_work_quick == "2") ? " AND ((work_state='6' AND task_run_regdate >= '{$sch_reg_s_datetime}') OR (work_state IN(3,4,5) AND regdate >= '{$sch_reg_s_datetime}'))" : " AND regdate >= '{$sch_reg_s_datetime}'";
        $add_req_where      .= " AND regdate >= '{$sch_reg_s_datetime}'";
        $add_run_where      .= " AND ((work_state='6' AND task_run_regdate >= '{$sch_reg_s_datetime}') OR (work_state IN(3,4,5) AND regdate >= '{$sch_reg_s_datetime}'))";
        $add_null_where      .= " AND regdate >= '{$sch_reg_s_datetime}'";

        $smarty->assign("sch_reg_s_date", $sch_reg_s_date);
    }

    if(!empty($sch_reg_e_date)){
        $add_work_where     .= ($sch_work_quick == "2") ? " AND ((work_state='6' AND task_run_regdate <= '{$sch_reg_e_datetime}') OR (work_state IN(3,4,5) AND regdate <= '{$sch_reg_e_datetime}'))" : " AND regdate <= '{$sch_reg_e_datetime}'";
        $add_req_where      .= " AND regdate <= '{$sch_reg_e_datetime}'";
        $add_run_where      .= " AND ((work_state='6' AND task_run_regdate <= '{$sch_reg_e_datetime}') OR (work_state IN(3,4,5) AND regdate <= '{$sch_reg_e_datetime}'))";
        $add_null_where      .= " AND regdate <= '{$sch_reg_e_datetime}'";
        $smarty->assign("sch_reg_e_date", $sch_reg_e_date);
    }
    $smarty->assign("sch_work_quick", $sch_work_quick);
    $smarty->assign("sch_reg_date_type", $sch_reg_date_type);

    $work_staff_url  = "";
    if (!empty($sch_team))
    {
        if($sch_team != 'all')
        {
            $sch_team_code_where = getTeamWhere($my_db, $sch_team);
            if($sch_team == "00221"){
                $sch_team_code_where .= ",00236";
            }

            if($sch_team_code_where){
                $add_req_where  .= " AND `w`.task_req_team IN ({$sch_team_code_where})";
                $add_run_where  .= " AND `w`.task_run_team IN ({$sch_team_code_where})";

                if($sch_work_quick == "1"){
                    $add_work_where .= " AND `w`.task_req_team IN ({$sch_team_code_where})";
                    $add_prev_where .= " AND `w`.task_req_team IN ({$sch_team_code_where})";
                }
                elseif($sch_work_quick == "2"){
                    $add_work_where .= " AND `w`.task_run_team IN ({$sch_team_code_where})";
                    $add_prev_where .= " AND `w`.task_run_team IN ({$sch_team_code_where})";
                }
            }

            if($sch_work_quick == "1"){
                $work_staff_url .= "&sch_req_team={$sch_team}";
            }elseif($sch_work_quick == "2"){
                $work_staff_url .= "&sch_run_team={$sch_team}";
            }

            $sch_staff_list = $staff_all_list[$sch_team];
        }

        $smarty->assign("sch_team", $sch_team);
    }

    if(!empty($sch_s_no)) {
        $add_req_where  .= " AND `w`.task_req_s_no = '{$sch_s_no}'";
        $add_run_where  .= " AND `w`.task_run_s_no = '{$sch_s_no}'";

        if($sch_work_quick == "1"){
            $add_work_where .= " AND `w`.task_req_s_no='{$sch_s_no}'";
            $add_prev_where .= " AND `w`.task_req_s_no='{$sch_s_no}'";
        }
        elseif($sch_work_quick == "2"){
            $add_work_where .= " AND `w`.task_run_s_no='{$sch_s_no}'";
            $add_prev_where .= " AND `w`.task_run_s_no='{$sch_s_no}'";
        }

        $sch_s_name = $staff_model->getStaffName($sch_s_no);

        if($sch_work_quick == "1"){
            $work_staff_url .= "&sch_req_s_name={$sch_s_name}";
        }elseif($sch_work_quick == "2"){
            $work_staff_url .= "&sch_run_s_name={$sch_s_name}";
        }

        $smarty->assign("sch_s_no", $sch_s_no);
    }

    # 업무 관리자 등록된 상품
    $add_req_staff = "1=1 AND display='1' AND (task_req_staff IS NOT NULL AND task_req_staff != '')";
    if(!empty($sch_team) && !empty($sch_staff_list))
    {
        if(!empty($sch_s_no)){
            $add_req_staff .= " AND task_req_staff LIKE '%{$sch_s_no}_{$sch_team}%'";
        }else{
            $add_req_staff .= " AND (";
            $req_count  = count($sch_staff_list);
            $req_idx    = 1;
            foreach($sch_staff_list as $s_no => $s_name)
            {
                $add_req_staff .= " (task_req_staff LIKE '%{$s_no}_{$sch_team}%')";

                if($req_idx < $req_count){
                    $add_req_staff .= " OR ";
                }
                $req_idx++;
            }
            $add_req_staff .= ")";
        }
    }

    $manager_prd_sql    = "SELECT * FROM product WHERE {$add_req_staff}";
    $manager_prd_query  = mysqli_query($my_db, $manager_prd_sql);
    $manager_prd_list   = [];
    while($manager_prd  = mysqli_fetch_assoc($manager_prd_query)){
        $manager_prd_list[] = $manager_prd['prd_no'];
    }

    if(!empty($manager_prd_list)){
        $manager_prd_text    = implode(",", $manager_prd_list);
        $add_null_where      .= "AND w.prd_no IN({$manager_prd_text})";
        if($sch_work_quick == "3"){
            $add_work_where     .= " AND w.prd_no IN({$manager_prd_text})";
            $add_prev_yet_where .= " AND w.prd_no IN({$manager_prd_text})";
        }
    }else{
        $add_null_where .= "AND 1!=1";
        if($sch_work_quick == "3"){
            $add_work_where     .= " AND 1!=1";
            $add_prev_yet_where .= " AND 1!=1";
        }
    }

    $work_state_req_sql     = "SELECT COUNT(w_no) as cnt FROM `work` as w WHERE {$add_req_where}";
    $work_state_req_query   = mysqli_query($my_db, $work_state_req_sql);
    $work_state_req_result  = mysqli_fetch_assoc($work_state_req_query);
    $work_quick_state["1"]["cnt"]  = $work_state_req_result['cnt'];

    $work_state_run_sql     = "SELECT COUNT(w_no) as cnt FROM `work` as w WHERE {$add_run_where}";
    $work_state_run_query   = mysqli_query($my_db, $work_state_run_sql);
    $work_state_run_result  = mysqli_fetch_assoc($work_state_run_query);
    $work_quick_state["2"]["cnt"]  = $work_state_run_result['cnt'];

    $work_state_null_sql    = "SELECT * FROM `work` as w WHERE {$add_null_where}";
    $work_state_null_query  = mysqli_query($my_db, $work_state_null_sql);
    while($work_state_null  = mysqli_fetch_assoc($work_state_null_query))
    {
        $work_quick_state["3"]["cnt"]++;
    }

    # 업무 요청,처리별 현황
    $check_staff        = "";
    $table_staff        = "";
    $work_prev_text     = "";
    $work_prev_url      = "/v1/work_list.php?sch=Y{$work_staff_url}";
    if($sch_work_quick == "1"){
        $check_staff     = "task_run_s_no";
        $table_staff     = "처리자";
        $work_prev_text  = "[주의] 10일 이상 지난 미완료 업무 : ";
        $work_prev_url  .= "&sch_reg_e_date={$work_prev_url_date}&q_sch=6";
    }elseif($sch_work_quick == "2"){
        $check_staff     = "task_req_s_no";
        $table_staff     = "요청자";
        $work_prev_text  = "[주의] 10일 이상 지난 미완료 업무 : ";
        $work_prev_url  .= "&sch_reg_e_date={$work_prev_url_date}&q_sch=6";
    }elseif($sch_work_quick == "3"){
        $table_staff     = "요청자";
        $check_staff     = "task_req_s_no";
        $add_work_where .= " AND (task_run_s_no=0 OR task_run_s_no IS NULL)";
    }

    $work_parent_list       = [];
    $work_parent_cnt_list   = [];
    $work_parent_sql        = "
        SELECT
            (SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd1,
            (SELECT CONCAT(k_name_code) FROM product prd WHERE prd.prd_no=w.prd_no) AS k_prd2,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2_name
        FROM `work` as w 
        WHERE {$add_work_where} 
        GROUP BY k_prd2
    ";
    $work_parent_query   = mysqli_query($my_db, $work_parent_sql);
    while($work_parent = mysqli_fetch_assoc($work_parent_query)){
        $work_parent_list[$work_parent['k_prd2']] = array("k_prd1" => $work_parent['k_prd1'], "title" => $work_parent['k_prd2_name']);
    }

    $work_group_list    = [];
    $work_group_sql     = "
        SELECT
            (SELECT CONCAT(k_name_code) FROM product prd WHERE prd.prd_no=w.prd_no) AS k_prd2,
            w.prd_no,
            (SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_name,
            w.{$check_staff} as staff_no,
            (SELECT s.s_name FROM staff s WHERE s.s_no=w.{$check_staff}) AS staff_name,
            COUNT(w.w_no) as total_qty
        FROM `work` as w 
        WHERE {$add_work_where} 
        GROUP BY prd_no, staff_no
        ORDER BY total_qty DESC, prd_name, staff_no ASC
    ";
    $work_group_query   = mysqli_query($my_db, $work_group_sql);
    while($work_group = mysqli_fetch_assoc($work_group_query)){
        $work_group_list[$work_group['k_prd2']][] = array(
            "prd_no"        => $work_group['prd_no'],
            "prd_title"     => $work_group['prd_name'],
            "staff_no"      => $work_group["staff_no"],
            "staff_title"   => $work_group['staff_name'],
            "search_url"    => $work_staff_url.(($sch_work_quick == "1") ? "&sch_run_s_name={$work_group['staff_name']}" : "&sch_req_s_name={$work_group['staff_name']}"),
            "cnt"           => $work_group['total_qty']
        );
        $work_parent_cnt_list[$work_group['k_prd2']]++;
    }

    $work_all_list      = [];
    $work_all_cnt_list  = [];
    $work_count_list    = array("progress" => 0, "finish" => 0, "yet" => 0);
    $work_all_sql       = "
        SELECT
            w.w_no,
            w.work_state,
            w.prd_no,
            (SELECT CONCAT(k_name_code) FROM product prd WHERE prd.prd_no=w.prd_no) AS k_prd2,   
            w.{$check_staff} as staff_no,
            (SELECT s.s_name FROM staff s WHERE s.s_no=w.{$check_staff}) AS req_name,
            w.task_run_s_no
        FROM `work` as w 
        WHERE {$add_work_where}
    ";
    $work_all_query = mysqli_query($my_db, $work_all_sql);
    while($work_all = mysqli_fetch_assoc($work_all_query))
    {
        $work_state = ($sch_work_quick == "3") ? "yet" : $work_all['work_state'];

        $work_all_list[$work_all['prd_no']][$work_all['staff_no']][$work_state]++;
        $work_all_cnt_list[$work_all['work_state']]++;

        if($work_all['work_state'] != '6'){
            $work_all_cnt_list['progress']++;
        }else{
            $work_all_cnt_list['finish']++;
        }
        $work_all_cnt_list['total']++;
    }

    $work_yet_prev_list = [];
    $work_prev_cnt      = 0;
    if($sch_work_quick != "3")
    {
        # 10일 이상 미완료건
        $work_prev_sql      = "SELECT COUNT(w_no) as cnt FROM `work` as `w` WHERE {$add_prev_where}";
        $work_prev_query    = mysqli_query($my_db, $work_prev_sql);
        $work_prev_result   = mysqli_fetch_assoc($work_prev_query);
        $work_prev_cnt      = isset($work_prev_result['cnt']) ? $work_prev_result['cnt'] : 0;
        if($work_prev_cnt > 0){
            $work_prev_text .= "{$work_prev_cnt}건";
        }
    }
    else{
        # 10일이상 처리자 미설정건
        $work_yet_prev_sql      = "
            SELECT 
                w.prd_no,
                (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) AS prd_name,
                COUNT(w.w_no) AS work_cnt
            FROM `work` as w 
            WHERE {$add_prev_yet_where}
            GROUP BY prd_no
            ORDER BY work_cnt DESC
        ";
        $work_yet_prev_query    = mysqli_query($my_db, $work_yet_prev_sql);
        $work_yet_prev_list     = [];
        while($work_yet_prev    = mysqli_fetch_assoc($work_yet_prev_query))
        {
            $work_yet_prev_list[] = $work_yet_prev;
        }
    }

    $smarty->assign("team_name_list", $team_name_list);
    $smarty->assign("sch_staff_list", $sch_staff_list);
    $smarty->assign("table_staff", $table_staff);
    $smarty->assign("work_prev_cnt", $work_prev_cnt);
    $smarty->assign("work_prev_url", $work_prev_url);
    $smarty->assign("work_prev_text", $work_prev_text);
    $smarty->assign("work_prev_url_date", $work_prev_url_date);
    $smarty->assign("work_quick_state", $work_quick_state);
    $smarty->assign("work_parent_list", $work_parent_list);
    $smarty->assign("work_parent_cnt_list", $work_parent_cnt_list);
    $smarty->assign("work_group_list", $work_group_list);
    $smarty->assign("work_all_list", $work_all_list);
    $smarty->assign("work_all_cnt_list", $work_all_cnt_list);
    $smarty->assign("manager_prd_list", $manager_prd_list);
    $smarty->assign("work_yet_prev_list", $work_yet_prev_list);


    # 팀별 업무 현황
    $team_work_quick_state  = getTeamWorkStateQuickOption();
    $sch_date_type_option   = getDateTypeOption();
    $smarty->assign("sch_date_type_option", $sch_date_type_option);
    $smarty->assign("team_work_quick_state", $team_work_quick_state);

    # 검색 필드
    $add_team_where         = "1=1 AND p.display='1'";
    $add_team_prev_where    = "regdate < '{$work_prev_datetime}' AND `w`.work_state IN(3,4,5)";
    $today_s_w		        = date('w')-1;
    $today_s_w_date         = date("Y-m-d", strtotime("-{$today_s_w} day"));
    $sch_work_team          = isset($_GET['sch_work_team']) ? $_GET['sch_work_team'] : (($session_team != "00236") ? $session_team : "00221");
    $sch_team_date_type     = isset($_GET['sch_team_date_type']) ? $_GET['sch_team_date_type'] : "2";
    $sch_team_work_quick    = isset($_GET['sch_team_work_quick']) && !empty($_GET['sch_team_work_quick']) ? $_GET['sch_team_work_quick'] : "2";
    $sch_year_s_date 	    = isset($_GET['sch_year_s_date']) ? $_GET['sch_year_s_date'] : date("Y", strtotime("-2 years"));
    $sch_year_e_date 	    = isset($_GET['sch_year_e_date']) ? $_GET['sch_year_e_date'] : date("Y");
    $sch_mon_s_date 	    = isset($_GET['sch_mon_s_date']) ? $_GET['sch_mon_s_date'] : date("Y-m", strtotime("-6 months"));
    $sch_mon_e_date 	    = isset($_GET['sch_mon_e_date']) ? $_GET['sch_mon_e_date'] : date("Y-m");
    $sch_week_s_date 	    = isset($_GET['sch_week_s_date']) ? $_GET['sch_week_s_date'] : date("Y-m-d", strtotime("{$today_s_w_date} -6 week"));
    $sch_week_e_date 	    = isset($_GET['sch_week_e_date']) ? $_GET['sch_week_e_date'] : date("Y-m-d", strtotime("{$today_s_w_date} +6 day"));
    $sch_day_s_date 	    = isset($_GET['sch_day_s_date']) ? $_GET['sch_day_s_date'] : date("Y-m-d", strtotime("-10 day"));
    $sch_day_e_date 	    = isset($_GET['sch_day_e_date']) ? $_GET['sch_day_e_date'] : date("Y-m-d");

    $smarty->assign("sch_work_team", $sch_work_team);
    $smarty->assign("sch_team_work_quick", $sch_team_work_quick);
    $smarty->assign("sch_team_date_type", $sch_team_date_type);
    $smarty->assign("sch_year_s_date", $sch_year_s_date);
    $smarty->assign("sch_year_e_date", $sch_year_e_date);
    $smarty->assign("sch_mon_s_date", $sch_mon_s_date);
    $smarty->assign("sch_mon_e_date", $sch_mon_e_date);
    $smarty->assign("sch_week_s_date", $sch_week_s_date);
    $smarty->assign("sch_week_e_date", $sch_week_e_date);
    $smarty->assign("sch_day_s_date", $sch_day_s_date);
    $smarty->assign("sch_day_e_date", $sch_day_e_date);

    # 날짜 처리
    $all_date_where 		= "";
    $all_date_key			= "";
    $all_date_title 		= "";
    $chk_team_date          = ($sch_team_work_quick == "1") ? "regdate" : "task_run_regdate";

    if($sch_team_date_type == "4"){
        $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_year_s_date}' AND '{$sch_year_e_date}'";
        $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y')";
        $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y')";
        $chk_team_date_key  = "DATE_FORMAT(`w`.{$chk_team_date}, '%Y')";

        $chk_team_s_date    = "{$sch_year_s_date}-01-01 00:00:00";
        $chk_team_e_date    = "{$sch_year_e_date}-12-31 23:59:59";
    }
    elseif($sch_team_date_type == "3"){
        $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_mon_s_date}' AND '{$sch_mon_e_date}'";
        $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m')";
        $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y-%m')";
        $chk_team_date_key  = "DATE_FORMAT(`w`.{$chk_team_date}, '%Y%m')";

        $sch_mon_e_day      = date("t", strtotime($sch_mon_e_date));
        $chk_team_s_date    = "{$sch_mon_s_date}-01 00:00:00";
        $chk_team_e_date    = "{$sch_mon_e_date}-{$sch_mon_e_day} 23:59:59";
    }
    elseif($sch_team_date_type == "2"){
        $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_week_s_date}' AND '{$sch_week_e_date}'";
        $all_date_key 	    = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
        $all_date_title     = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";
        $chk_team_date_key  = "DATE_FORMAT(DATE_SUB(`w`.{$chk_team_date}, INTERVAL(IF(DAYOFWEEK(`w`.{$chk_team_date})=1,8,DAYOFWEEK(`w`.{$chk_team_date}))-2) DAY), '%Y%m%d')";

        $chk_team_s_date    = "{$sch_week_s_date} 00:00:00";
        $chk_team_e_date    = "{$sch_week_e_date} 23:59:59";
    }
    else{
        $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_day_s_date}' AND '{$sch_day_e_date}'";
        $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
        $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";
        $chk_team_date_key  = "DATE_FORMAT(`w`.{$chk_team_date}, '%Y%m%d')";

        $chk_team_s_date    = "{$sch_day_s_date} 00:00:00";
        $chk_team_e_date    = "{$sch_day_e_date} 23:59:59";
    }

    # 검색 처리
    $work_team_staff_list   = $staff_all_list[$sch_work_team];
    $sch_work_team_where    = "1=1";
    $work_team_staff_url    = "";
    if(!empty($sch_work_team))
    {
        $sch_work_team_where    = getTeamWhere($my_db, $sch_work_team);
        $work_team_staff_url    = ($sch_team_work_quick == "1") ? "&sch_req_team={$sch_work_team}" : "&sch_run_team={$sch_work_team}";
        $add_team_prev_where   .= ($sch_team_work_quick == "1") ? " AND `w`.task_req_team IN ({$sch_work_team_where})" : " AND `w`.task_run_team IN ({$sch_work_team_where})";
        if($sch_work_team == "00221"){
            $sch_work_team_where .= ",00236";
        }

        if($sch_team_work_quick == "1"){
            $add_team_where    .= " AND w.work_state IN(3,4,5,6) AND `w`.task_req_team IN ({$sch_work_team_where}) AND w.regdate BETWEEN '{$chk_team_s_date}' AND '{$chk_team_e_date}'";
            $chk_team_staff     = "task_req_s_no";
            $chk_staff_name     = "업무요청자";
        }else{
            $add_team_where    .= " AND w.work_state='6' AND `w`.task_run_team IN ({$sch_work_team_where}) AND w.task_run_regdate BETWEEN '{$chk_team_s_date}' AND '{$chk_team_e_date}'";
            $chk_team_staff     = "task_run_s_no";
            $chk_staff_name     = "업무처리자";
        }
    }
    else
    {
        $work_team_staff_list = $team_name_list;
        if($sch_team_work_quick == "1"){
            $add_team_where    .= " AND w.work_state IN(3,4,5,6) AND w.regdate BETWEEN '{$chk_team_s_date}' AND '{$chk_team_e_date}'";
            $chk_team_staff     = "task_req_team";
            $chk_staff_name     = "업무요청부서";
        }else{
            $add_team_where    .= " AND w.work_state='6' AND w.task_run_regdate BETWEEN '{$chk_team_s_date}' AND '{$chk_team_e_date}'";
            $chk_team_staff     = "task_run_team";
            $chk_staff_name     = "업무처리부서";
        }
    }

    # 팀별 현황 리스트
    $work_team_prd_list     = [];
    $team_prd_sql           = "SELECT w.prd_no, (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) AS prd_name FROM work w LEFT JOIN product p ON p.prd_no=w.prd_no WHERE {$add_team_where}";
    $team_prd_query = mysqli_query($my_db, $team_prd_sql);
    while($team_prd = mysqli_fetch_assoc($team_prd_query)){
        $work_team_prd_list[$team_prd['prd_no']] = $team_prd['prd_name'];
    }

    # 날짜 검색
    $work_team_date_list        = [];
    $work_team_staff_data_list  = [];
    $work_team_prd_data_list    = [];
    $work_team_date_total_list  = [];

    $all_date_sql = "
        SELECT
            {$all_date_key} as chart_key,
            {$all_date_title} as chart_title
        FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
        as allday
        WHERE {$all_date_where}
        GROUP BY chart_key
        ORDER BY chart_key
    ";
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    while($date = mysqli_fetch_array($all_date_query))
    {
        $work_team_date_list[$date['chart_key']]        = $date['chart_title'];
        $work_team_date_total_list[$date['chart_key']]  = 0;

        foreach($work_team_staff_list as $team_s_no => $team_s_name){
            $work_team_staff_data_list[$date['chart_key']][$team_s_no] = 0;
        }

        foreach($work_team_prd_data_list as $team_prd_no => $team_prd_label){
            $work_team_prd_data_list[$date['chart_key']][$team_prd_no] = 0;
        }
    }

    # 업무 데이터 처리
    $is_team_work_data = false;
    $work_team_data_sql = "
        SELECT
            w.prd_no,
            {$chk_team_staff} as staff_no,
            {$chk_team_date_key} as key_date            
        FROM work w 
        LEFT JOIN product AS p ON p.prd_no=w.prd_no
        WHERE {$add_team_where}
    ";
    $work_team_data_query = mysqli_query($my_db, $work_team_data_sql);
    while($work_team_data = mysqli_fetch_assoc($work_team_data_query)){
        $is_team_work_data = true;
        $work_team_staff_data_list[$work_team_data['key_date']][$work_team_data['staff_no']]++;
        $work_team_prd_data_list[$work_team_data['key_date']][$work_team_data['prd_no']]++;
        $work_team_date_total_list[$work_team_data['key_date']]++;
    }

    $work_team_chart_list = [];
    foreach($work_team_staff_list as $s_no => $s_name)
    {
        $work_team_chart_init_data = array(
            "title" => $s_name,
            "type"  => "bar",
            "data"  => array(),
        );

        foreach($work_team_date_list as $key_date => $date_label){
            $work_team_chart_init_data["data"][] = $work_team_staff_data_list[$key_date][$s_no];
        }

        $work_team_chart_list[] = $work_team_chart_init_data;
    }

    # 10일 이상 미완료건
    $work_team_prev_text    = "[주의] 10일 이상 지난 미완료 업무 : ";
    $work_team_prev_url     = "/v1/work_list.php?sch=Y{$work_team_staff_url}&sch_reg_e_date={$work_prev_url_date}&q_sch=6";
    $work_team_prev_sql     = "SELECT COUNT(w_no) as cnt FROM `work` as `w` WHERE {$add_team_prev_where}";
    $work_team_prev_query   = mysqli_query($my_db, $work_team_prev_sql);
    $work_team_prev_result  = mysqli_fetch_assoc($work_team_prev_query);
    $work_team_prev_cnt     = isset($work_team_prev_result['cnt']) ? $work_team_prev_result['cnt'] : 0;
    if($work_team_prev_cnt > 0){
        $work_team_prev_text .= "{$work_team_prev_cnt}건";
    }

    $smarty->assign("is_team_work_data", $is_team_work_data);
    $smarty->assign("chk_staff_name", $chk_staff_name);
    $smarty->assign("work_team_date_cnt", count($work_team_date_list)+1);
    $smarty->assign("work_team_date_list", $work_team_date_list);
    $smarty->assign("work_team_staff_list", $work_team_staff_list);
    $smarty->assign("work_team_prd_list", $work_team_prd_list);
    $smarty->assign("work_team_staff_data_list", $work_team_staff_data_list);
    $smarty->assign("work_team_prd_data_list", $work_team_prd_data_list);
    $smarty->assign("work_team_date_total_list", $work_team_date_total_list);
    $smarty->assign("work_team_chart_list", json_encode($work_team_chart_list));
    $smarty->assign("work_team_chart_name_list", json_encode($work_team_date_list));
    $smarty->assign("work_team_staff_name_list", json_encode($work_team_staff_list));
    $smarty->assign("work_team_prev_cnt", $work_team_prev_cnt);
    $smarty->assign("work_team_prev_text", $work_team_prev_text);
    $smarty->assign("work_team_prev_url", $work_team_prev_url);
}
elseif($sch_work == "product")
{
    # 입고관리 캘린더
    $calendar_model             = Calendar::Factory();
    $calendar_model->setMainInit("calendar_manager", "cal_id");
    $inout_cal_id               = "inoutwith";
    $calendar_item              = $calendar_model->getItem($inout_cal_id);
    $inout_category_option      = getInOutCalendarCategoryOption();
    $inout_category_count_list  = [];
    foreach($inout_category_option as $inout_category){
        $inout_category_count_list[] = array("title" => $inout_category, "count" => 0);
    }

    $cal_s_date         = date("Y-m-d", strtotime("{$cur_date} -10 days"))." 00:00:00";
    $cal_e_date         = date("Y-m-d", strtotime("{$cur_date} +10 days"))." 23:59:59";
    $inout_calendar_sql = "
        SELECT
            cs.cs_no,
            cs.cs_title,
            cs.cs_s_date,  
            cs.cs_category,
            cs.file_path,
            cs.file_name,
            (SELECT count(sub.csc_no) FROM calendar_schedule_comment sub WHERE sub.cs_no=cs.cs_no AND sub.display='1') as csc_cnt,
            (SELECT count(sub.csc_no) FROM calendar_schedule_comment sub WHERE sub.cs_no=cs.cs_no AND sub.display='1' AND sub.comment LIKE '%해외 제조사 입고 배차정보 알림%') as sms_comment
        FROM calendar_schedule as cs
        WHERE cs.cal_id='{$inout_cal_id}' AND cs.cs_s_date BETWEEN '{$cal_s_date}' AND '{$cal_e_date}'
        ORDER BY cs_s_date DESC
    ";
    $inout_calendar_query   = mysqli_query($my_db, $inout_calendar_sql);
    $inout_calendar_list    = [];
    while($inout_calendar_result = mysqli_fetch_assoc($inout_calendar_query))
    {
        $inout_category     = "";
        $inout_category_val = $inout_calendar_result['cs_category'];

        if(!empty($inout_category_val))
        {
            switch($inout_category_val){
                case "입고일정확정":
                    $inout_category = 0;
                    break;
                case "입고확인서 완료(종료)":
                    $inout_category = 1;
                    break;
                case "입고완료":
                    $inout_category = 2;
                    break;
                case "입고요청":
                    $inout_category = 3;
                    break;
            }
            $inout_category_count_list[$inout_category]["count"]++;

            $inout_calendar_day                     = date("w", strtotime($inout_calendar_result['cs_s_date']));
            $inout_calendar_result['cs_date']       = date("Y-m-d", strtotime($inout_calendar_result['cs_s_date']));
            $inout_calendar_result['cs_date_name']  = date("m/d", strtotime($inout_calendar_result['cs_s_date']))."({$date_name_option[$inout_calendar_day]})";
            $inout_calendar_result['cs_date_time']  = date("H:i", strtotime($inout_calendar_result['cs_s_date']));

            $inout_file_list = [];
            if(!empty($inout_calendar_result['file_path'])){
                $inout_file_tmp_paths = explode(",", $inout_calendar_result['file_path']);
                $inout_file_tmp_names = explode(",", $inout_calendar_result['file_name']);

                foreach($inout_file_tmp_paths as $key => $inout_file_tmp_path){
                    $inout_file_list[] = array("file_path" => $inout_file_tmp_path, "file_name" => $inout_file_tmp_names[$key]);
                }
            }
            $inout_calendar_result['file_list'] = $inout_file_list;

            $inout_calendar_list[$inout_category][] = $inout_calendar_result;
        }
    }

    # 입고예정 임박
    $chk_cal_day = 2;
    switch ($cur_day){
        case '0':
            $chk_cal_day = 3;
            break;
        case '1':
        case '2':
        case '3':
            $chk_cal_day = 2;
            break;
        case '4':
        case '5':
        case '6':
            $chk_cal_day = 4;
            break;
    }

    $chk_cal_s_date     = "{$cur_date} 00:00:00";
    $chk_cal_e_date     = date("Y-m-d", strtotime("{$cur_date} +{$chk_cal_day} days"))." 23:59:59";
    $chk_inout_calendar_sql = "
        SELECT
            cs.*,
            (SELECT c.c_name FROM company c WHERE c.c_no=cs.cs_brand) as cs_brand_name,
            (SELECT sub.s_name FROM staff sub WHERE sub.s_no=cs.cs_s_no LIMIT 1) as cs_s_name,
            (SELECT count(sub.csc_no) FROM calendar_schedule_comment sub WHERE sub.cs_no=cs.cs_no AND sub.display='1') as csc_cnt
        FROM calendar_schedule as cs
        WHERE cs.cal_id='{$inout_cal_id}' AND cs.cs_s_date BETWEEN '{$chk_cal_s_date}' AND '{$chk_cal_e_date}'
        ORDER BY cs_s_date ASC
    ";
    $chk_inout_calendar_query   = mysqli_query($my_db, $chk_inout_calendar_sql);
    $chk_inout_calendar_list    = [];
    while($chk_inout_calendar = mysqli_fetch_assoc($chk_inout_calendar_query))
    {
        $title      = "";
        $cs_s_date  = date('Y-m-d', strtotime($chk_inout_calendar['cs_s_date']));
        $cs_e_date  = date('Y-m-d', strtotime($chk_inout_calendar['cs_e_date']));

        $chk_inout_calendar['is_today'] = false;
        if($cs_s_date == $cs_e_date)
        {
            $chk_inout_calendar['is_today'] = ($cs_s_date == $cur_date) ? true : false;

            if($chk_inout_calendar['cs_all'] == '1'){

            }else{
                if($calendar_item['view_s_date'] == '1') {
                    $title_s_date = date('H:i', strtotime($chk_inout_calendar['cs_s_date']));
                    $title .= (!empty($title)) ? "~{$title_s_date}" : "{$title_s_date}";
                }

                if($calendar_item['view_e_date'] == '1') {
                    $title_e_date = date('H:i', strtotime($chk_inout_calendar['cs_e_date']));
                    $title .= (!empty($title)) ? "~{$title_e_date}" : "{$title_e_date}";
                }
            }
        }else{
            if($calendar_item['view_s_date'] == '1' && $chk_inout_calendar['cs_all'] != '1') {
                $title_s_date = date('H:i', strtotime($chk_inout_calendar['cs_s_date']));
                if($title_s_date != "00:00") {
                    $title .= (!empty($title)) ? "~{$title_s_date}" : "{$title_s_date}";
                }
            }
        }

        if($calendar_item['view_brand'] == '1' && !empty($chk_inout_calendar['cs_brand'])) {
            $title .= (!empty($title)) ? " [{$chk_inout_calendar['cs_brand_name']}]" : "[{$chk_inout_calendar['cs_brand_name']}]";
        }

        if($calendar_item['view_category'] == '1' && !empty($chk_inout_calendar['cs_category'])) {
            $title .= (!empty($title)) ? " [{$chk_inout_calendar['cs_category']}]" : "[{$chk_inout_calendar['cs_category']}]";
        }

        if($calendar_item['view_important'] == '1' && $chk_inout_calendar['cs_important'] == '1') {
            $title .= (!empty($title)) ? " ★" : "★";
        }

        if($calendar_item['view_title'] == '1' && !empty($chk_inout_calendar['cs_title'])) {
            $title .= (!empty($title)) ? " {$chk_inout_calendar['cs_title']}" : "{$chk_inout_calendar['cs_title']}";
        }

        if($calendar_item['view_write_s_no'] == '1' ) {
            $title .= (!empty($title)) ? " ({$chk_inout_calendar['cs_s_name']})" : "({$chk_inout_calendar['cs_s_name']})";
        }

        if($calendar_item['view_comment'] == '1' ) {
            $title .= (!empty($title)) ? " ({$chk_inout_calendar['csc_cnt']})" : "({$chk_inout_calendar['csc_cnt']})";
        }
        $chk_inout_calendar['title']    = $title;

        $chk_inout_calendar_day             = date("w", strtotime($chk_inout_calendar['cs_s_date']));
        $chk_inout_calendar['cs_date_name'] = date("m/d", strtotime($chk_inout_calendar['cs_s_date']))."({$date_name_option[$chk_inout_calendar_day]})";

        $chk_inout_calendar_list[]  = $chk_inout_calendar;
    }
    $smarty->assign("cur_date_name", $cur_date_name);
    $smarty->assign("inout_category_count_list", $inout_category_count_list);
    $smarty->assign("inout_calendar_list", $inout_calendar_list);
    $smarty->assign("chk_inout_calendar_list", $chk_inout_calendar_list);

    # 진행중 게시글 20개
    $board_content_sql      = "SELECT *, (SELECT s_name FROM staff s where s.s_no=brd_nm.s_no) AS writer, DATE_FORMAT(regdate, '%Y/%m/%d') as reg_date, DATE_FORMAT(regdate, '%H:%i') as reg_time FROM board_normal as brd_nm WHERE b_id='new_product' AND display='1' AND category='진행중' ORDER BY regdate DESC LIMIT 20";
    $board_content_query    = mysqli_query($my_db, $board_content_sql);
    $board_content_list     = [];
    while($board_content = mysqli_fetch_assoc($board_content_query))
    {
        $board_content_list[] = $board_content;
    }
    $smarty->assign("board_total_cnt", count($board_content_list));
    $smarty->assign("board_content_list", $board_content_list);


    # 물류요청처리
    $add_log_request_where = "lm.work_state IN(3,4,5,7) AND lm.delivery_type != '4'";
    $log_request_sql = "
        SELECT 
            lm_no,
            work_state,
            stock_type,
            subject,
            run_c_no,
            DATE_FORMAT(regdate, '%m/%d') as reg_day,
            DATE_FORMAT(quick_req_date, '%m/%d') as req_day,
            (SELECT s.s_name FROM staff s WHERE s.s_no=lm.req_s_no) as req_s_name
        FROM logistics_management AS lm
        WHERE {$add_log_request_where}
        ORDER BY regdate DESC
    ";
    $log_request_query  = mysqli_query($my_db, $log_request_sql);
    $log_request_list   = [];
    $log_state_option   = getWorkStateOption();
    $log_state_color    = getWorkStateOptionColor();
    $log_stock_option   = getStockTypeOption();
    $log_subject_option = getSubjectNameOption();
    $log_company_option = getAddrCompanyOption();
    while($log_request  = mysqli_fetch_assoc($log_request_query))
    {
        $log_request['state_name']      = !empty($log_request['work_state']) ? $log_state_option[$log_request['work_state']] : "";
        $log_request['state_color']     = !empty($log_request['work_state']) ? $log_state_color[$log_request['work_state']] : "";
        $log_request['stock_name']      = !empty($log_request['stock_type']) ? $log_stock_option[$log_request['stock_type']] : "";
        $log_request['subject_name']    = !empty($log_request['subject']) ? $log_subject_option[$log_request['subject']] : "";
        $log_request['run_c_name']      = !empty($log_request['run_c_no']) ? $log_company_option[$log_request['run_c_no']] : "";

        $log_request_list[] = $log_request;
    }

    $chk_log_request_sql = "
       SELECT 
	        quick_req_date,
            COUNT(lm_no) as cnt
        FROM logistics_management AS lm 
        WHERE lm.delivery_type != '4' AND work_state IN(3,4,5,7) AND quick_req_date BETWEEN '{$chk_cal_s_date}' AND '{$chk_cal_e_date}'
        GROUP BY quick_req_date
        ORDER BY regdate ASC
    ";
    $chk_log_request_query  = mysqli_query($my_db, $chk_log_request_sql);
    $chk_log_request_list   = [];
    $set_cur_date 	        = new DateTime($cur_date);
    while($chk_log_request = mysqli_fetch_assoc($chk_log_request_query))
    {
        $req_datetime 	= new DateTime($chk_log_request['quick_req_date']);
        $req_left_date 	= $set_cur_date->diff($req_datetime);
        $req_left_day   = $req_left_date->days;

        $chk_log_request_day                = date("w", strtotime($chk_log_request['quick_req_date']));
        $chk_log_request['req_date_name']   = date("m/d", strtotime($chk_log_request['quick_req_date']))."({$date_name_option[$chk_log_request_day]})";
        $chk_log_request['req_left_day']    = $req_left_day;

        $chk_log_request_list[] = $chk_log_request;
    }

    # 발주요청처리
    $add_log_order_where = "lm.work_state IN(3,4,5,7) AND lm.delivery_type = '4'";
    $log_order_sql = "
        SELECT 
            lm_no,
            work_state,
            DATE_FORMAT(regdate, '%m/%d') as reg_day,
            (SELECT s.s_name FROM staff s WHERE s.s_no=lm.req_s_no) as req_s_name
        FROM logistics_management AS lm
        WHERE {$add_log_order_where}
        ORDER BY regdate DESC
    ";
    $log_order_query  = mysqli_query($my_db, $log_order_sql);
    $log_order_list   = [];
    while($log_order = mysqli_fetch_assoc($log_order_query))
    {
        $log_order['state_name']    = !empty($log_order['work_state']) ? $log_state_option[$log_order['work_state']] : "";
        $log_order['state_color']   = !empty($log_order['work_state']) ? $log_state_color[$log_order['work_state']] : "";

        $log_order_prd_sql      = "SELECT pcu.option_name, COUNT(lp.lp_no) AS cnt FROM logistics_product AS lp LEFT JOIN product_cms_unit pcu ON pcu.no=lp.prd_no WHERE lp.lm_no='{$log_order['lm_no']}' AND lp.prd_type='2'";
        $log_order_prd_query    = mysqli_query($my_db, $log_order_prd_sql);
        $log_order_prd          = mysqli_fetch_assoc($log_order_prd_query);
        $log_order['prd_name']  = !empty($log_order_prd) ? $log_order_prd['option_name'] : "";
        $log_order['prd_cnt']   = !empty($log_order_prd) ? $log_order_prd['cnt']-1 : 0;

        $log_order_list[] = $log_order;
    }

    $smarty->assign("log_request_list", $log_request_list);
    $smarty->assign("chk_log_request_list", $chk_log_request_list);
    $smarty->assign("log_order_list", $log_order_list);

    # 타계정 반출
    $kind_model                 = Kind::Factory();
    $brand_company_total_list   = $kind_model->getBrandCompanyList();
    $sch_other_brand_g1_list    = $brand_company_total_list['brand_g1_list'];
    $brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
    $brand_list                 = $brand_company_total_list['brand_dom_list'];
    $brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
    $brand_parent_list          = $brand_company_total_list['brand_parent_list'];
    $sch_other_brand_g2_list    = [];
    $sch_other_brand_list       = [];

    # 검색
    $add_other_where            = "1=1 AND log_subject IN(1,2,3,4,5,9) AND confirm_state='10'";
    $sch_other_move_mon         = isset($_GET['sch_other_move_mon']) ? $_GET['sch_other_move_mon'] : date('Y-m', strtotime('-1 months'));
    $sch_other_move_s_date      = $sch_other_move_mon."-01";
    $sch_other_move_end_day     = date("t", strtotime($sch_other_move_mon));
    $sch_other_move_e_date      = $sch_other_move_mon."-{$sch_other_move_end_day}";
    $sch_other_brand_g1         = isset($_GET['sch_other_brand_g1']) ? $_GET['sch_other_brand_g1'] : "";
    $sch_other_brand_g2         = isset($_GET['sch_other_brand_g2']) ? $_GET['sch_other_brand_g2'] : "";
    $sch_other_brand            = isset($_GET['sch_other_brand']) ? $_GET['sch_other_brand'] : "";
    $other_search_url           = "sch_move_mon={$sch_other_move_mon}";

    # 브랜드 parent 매칭
    if(!empty($sch_other_brand)) {
        $sch_other_brand_g1 = $brand_parent_list[$sch_other_brand]["brand_g1"];
        $sch_other_brand_g2 = $brand_parent_list[$sch_other_brand]["brand_g2"];
        $other_search_url  .= "&sch_brand_g1={$sch_other_brand_g1}&sch_brand_g2={$sch_other_brand_g2}&sch_brand={$sch_other_brand}";
    }
    elseif(!empty($sch_other_brand_g2)) {
        $sch_other_brand_g1 = $brand_g2_parent_list[$sch_other_brand_g2]["brand_g1"];
        $other_search_url  .= "&sch_brand_g1={$sch_other_brand_g1}&sch_brand_g2={$sch_other_brand_g2}";
    }
    elseif(!empty($sch_other_brand_g1)) {
        $other_search_url  .= "&sch_brand_g1={$sch_other_brand_g1}";
    }

    if(!empty($sch_other_move_mon))
    {
        $add_other_where  .=  " AND pcsr.regdate BETWEEN '{$sch_other_move_s_date}' AND '{$sch_other_move_e_date}'";
    }
    $smarty->assign("sch_other_move_mon", $sch_other_move_mon);
    $smarty->assign("sch_other_move_s_date", $sch_other_move_s_date);
    $smarty->assign("sch_other_move_e_date", $sch_other_move_e_date);


    # 브랜드 옵션
    if(!empty($sch_other_brand))
    {
        $sch_other_brand_g2_list  = $brand_company_g2_list[$sch_other_brand_g1];
        $sch_other_brand_list     = $brand_list[$sch_other_brand_g2];
        $add_other_where         .= " AND `pcu`.brand = '{$sch_other_brand}'";
    }
    elseif(!empty($sch_other_brand_g2))
    {
        $sch_other_brand_g2_list  = $brand_company_g2_list[$sch_other_brand_g1];
        $sch_other_brand_list     = $brand_list[$sch_other_brand_g2];
        $add_other_where         .= " AND `pcu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_other_brand_g2}')";
    }
    elseif(!empty($sch_other_brand_g1))
    {
        $sch_other_brand_g2_list  = $brand_company_g2_list[$sch_other_brand_g1];
        $add_other_where         .= " AND `pcu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_other_brand_g1}'))";
    }

    $smarty->assign("sch_other_brand_g1", $sch_other_brand_g1);
    $smarty->assign("sch_other_brand_g2", $sch_other_brand_g2);
    $smarty->assign("sch_other_brand", $sch_other_brand);
    $smarty->assign("sch_other_brand_g1_list", $sch_other_brand_g1_list);
    $smarty->assign("sch_other_brand_g2_list", $sch_other_brand_g2_list);
    $smarty->assign("sch_other_brand_list", $sch_other_brand_list);

    $stock_other_report_list  = [];
    $stock_other_total_list   = array("1" => 0, "2" => 0, "3" => 0, "4" => 0, "5" => 0, "9" => 0, "total" => 0);
    $stock_other_report_sql   = "
        SELECT 
            pcsr.option_name,
            pcu.brand,
            pcsr.sku,
            pcsr.prd_unit,
            pcsr.log_subject,
            SUM(pcsr.stock_qty) as total_qty
        FROM product_cms_stock_report pcsr
        LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcsr.prd_unit
        WHERE {$add_other_where}
        GROUP BY pcsr.prd_unit, pcsr.log_subject
        ORDER BY pcsr.sku
    ";
    $stock_other_report_query = mysqli_query($my_db, $stock_other_report_sql);
    while($stock_other_report = mysqli_fetch_assoc($stock_other_report_query))
    {
        if(!isset($stock_report_list[$stock_other_report['prd_unit']]))
        {
            $stock_other_report_list[$stock_other_report['prd_unit']] = array(
                "name"          => $stock_other_report['option_name'],
                "sku"           => $stock_other_report['sku'],
                "prd_unit"      => $stock_other_report['prd_unit'],
                "1"             => 0,
                "2"             => 0,
                "3"             => 0,
                "4"             => 0,
                "5"             => 0,
                "9"             => 0,
                "total"         => 0,
            );
        }

        $total_qty      = $stock_other_report['total_qty']*-1;
        $log_subject    = $stock_other_report['log_subject'];

        $stock_other_report_list[$stock_other_report['prd_unit']][$log_subject] += $total_qty;
        $stock_other_report_list[$stock_other_report['prd_unit']]["total"]      += $total_qty;

        $stock_other_total_list[$log_subject]   += $total_qty;
        $stock_other_total_list["total"]        += $total_qty;
    }

    $smarty->assign("other_search_url", $other_search_url);
    $smarty->assign("stock_other_report_list", $stock_other_report_list);
    $smarty->assign("stock_other_total_list", $stock_other_total_list);
}
elseif($sch_work == "operate")
{
    # 출금 승인 현황
    $approve_prev_date  = date("Y-m-d", strtotime("-9 days"));
    $approve_prev_time  = $approve_prev_date." 00:00:00";
    $approve_cur_date   = date("Y-m-d");
    $withdraw_prd_list  = getWmWithdrawProductOption();
    $withdraw_init      = array("total" => 0, "wait" => 0);
    $withdraw_date_list = [];
    $withdraw_list      = [];

    # 날짜 생성
    $all_date_sql       = "
        SELECT
            DATE_FORMAT(`allday`.Date, '%m/%d_%w') as chart_title,
            DATE_FORMAT(`allday`.Date, '%Y%m%d') as chart_key
        FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
        as allday
        WHERE DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$approve_prev_date}' AND '$approve_cur_date'
        ORDER BY chart_key
    ";
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    while($all_date = mysqli_fetch_assoc($all_date_query))
    {
        $date_convert   = explode('_', $all_date['chart_title']);
        $date_name      = isset($date_convert[1]) ? $date_name_option[$date_convert[1]] : "";
        $chart_title    = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];

        if(!isset($withdraw_date_list[$all_date['chart_key']]))
        {
            $withdraw_date_list[$all_date['chart_key']] = array("title" => $chart_title, "date" => date("Y-m-d", strtotime($all_date['chart_key'])));

            foreach($withdraw_prd_list as $prd_no => $prd_title){
                $withdraw_list[$prd_no][$all_date['chart_key']] = $withdraw_init;

                if(!isset($withdraw_list[$prd_no])){
                    $withdraw_list[$prd_no]["total"] = $withdraw_init;
                }
            }
        }
    }

    # 출금 승인 현황 리스트 쿼리
    $withdraw_approve_sql   = "
        SELECT 
            DATE_FORMAT(wd.regdate, '%Y%m%d') as wd_reg_day,
            w.prd_no,
            (SELECT p.prd_no FROM product p WHERE p.prd_no=w.prd_no) as prd_name,
            COUNT(wd.wd_no) as total_cnt, 
            SUM(IF(wd.is_approve='2',1,0)) as wait_cnt 
        FROM withdraw AS wd 
        LEFT JOIN `work` AS w ON w.w_no=wd.w_no
        WHERE wd.regdate >= '{$approve_prev_time}' AND wd.reg_s_no='61' AND wd.wd_state NOT IN(4) AND wd.wd_method='1' 
        GROUP BY wd_reg_day, prd_no
    ";
    $withdraw_approve_query  = mysqli_query($my_db, $withdraw_approve_sql);
    while($withdraw_approve = mysqli_fetch_assoc($withdraw_approve_query))
    {
        $withdraw_list[$withdraw_approve['prd_no']][$withdraw_approve['wd_reg_day']]['total']   += $withdraw_approve['total_cnt'];
        $withdraw_list[$withdraw_approve['prd_no']][$withdraw_approve['wd_reg_day']]['wait']    += $withdraw_approve['wait_cnt'];
        $withdraw_list[$withdraw_approve['prd_no']]["total"]['total']   += $withdraw_approve['total_cnt'];
        $withdraw_list[$withdraw_approve['prd_no']]["total"]['wait']    += $withdraw_approve['wait_cnt'];
    }

    $smarty->assign("withdraw_date_list", $withdraw_date_list);
    $smarty->assign("withdraw_prd_list", $withdraw_prd_list);
    $smarty->assign("withdraw_list", $withdraw_list);
}
elseif($sch_work == "doc")
{
    # 입고관리 캘린더
    $calendar_model             = Calendar::Factory();
    $calendar_model->setMainInit("calendar_manager", "cal_id");
    $inout_cal_id               = "inoutwith";
    $calendar_item              = $calendar_model->getItem($inout_cal_id);

    # 입고예정 임박
    $chk_doc_day = 2;
    switch ($cur_day){
        case '0':
            $chk_doc_day = 3;
            break;
        case '1':
        case '2':
        case '3':
            $chk_doc_day = 2;
            break;
        case '4':
        case '5':
        case '6':
            $chk_doc_day = 4;
            break;
    }

    $chk_doc_s_date             = "{$cur_date} 00:00:00";
    $chk_doc_e_date             = date("Y-m-d", strtotime("{$cur_date} +{$chk_doc_day} days"))." 23:59:59";
    $chk_doc_inout_calendar_sql = "
        SELECT
            cs.*,
            (SELECT c.c_name FROM company c WHERE c.c_no=cs.cs_brand) as cs_brand_name,
            (SELECT sub.s_name FROM staff sub WHERE sub.s_no=cs.cs_s_no LIMIT 1) as cs_s_name,
            (SELECT count(sub.csc_no) FROM calendar_schedule_comment sub WHERE sub.cs_no=cs.cs_no AND sub.display='1') as csc_cnt
        FROM calendar_schedule as cs
        WHERE cs.cal_id='{$inout_cal_id}' AND cs.cs_s_date BETWEEN '{$chk_doc_s_date}' AND '{$chk_doc_e_date}' AND cs_brand='1314'
        ORDER BY cs_s_date ASC
    ";
    $chk_doc_inout_calendar_query   = mysqli_query($my_db, $chk_doc_inout_calendar_sql);
    $chk_doc_inout_calendar_list    = [];
    while($chk_doc_inout = mysqli_fetch_assoc($chk_doc_inout_calendar_query))
    {
        $doc_title      = "";
        $doc_cs_s_date  = date('Y-m-d', strtotime($chk_doc_inout['cs_s_date']));
        $doc_cs_e_date  = date('Y-m-d', strtotime($chk_doc_inout['cs_e_date']));

        $chk_doc_inout['is_today'] = false;
        if($doc_cs_s_date == $doc_cs_e_date)
        {
            $chk_doc_inout['is_today'] = ($doc_cs_s_date == $cur_date) ? true : false;

            if($chk_doc_inout['cs_all'] == '1'){

            }else{
                if($calendar_item['view_s_date'] == '1') {
                    $doc_title_s_date = date('H:i', strtotime($chk_doc_inout['cs_s_date']));
                    $doc_title .= (!empty($doc_title)) ? "~{$doc_title_s_date}" : "{$doc_title_s_date}";
                }

                if($calendar_item['view_e_date'] == '1') {
                    $doc_title_e_date = date('H:i', strtotime($chk_doc_inout['cs_e_date']));
                    $doc_title .= (!empty($doc_title)) ? "~{$doc_title_e_date}" : "{$doc_title_e_date}";
                }
            }
        }else{
            if($calendar_item['view_s_date'] == '1' && $chk_doc_inout['cs_all'] != '1') {
                $doc_title_s_date = date('H:i', strtotime($chk_doc_inout['cs_s_date']));
                if($doc_title_s_date != "00:00") {
                    $doc_title .= (!empty($doc_title)) ? "~{$doc_title_s_date}" : "{$doc_title_s_date}";
                }
            }
        }

        if($calendar_item['view_brand'] == '1' && !empty($chk_doc_inout['cs_brand'])) {
            $doc_title .= (!empty($doc_title)) ? " [{$chk_doc_inout['cs_brand_name']}]" : "[{$chk_doc_inout['cs_brand_name']}]";
        }

        if($calendar_item['view_category'] == '1' && !empty($chk_doc_inout['cs_category'])) {
            $doc_title .= (!empty($doc_title)) ? " [{$chk_doc_inout['cs_category']}]" : "[{$chk_doc_inout['cs_category']}]";
        }

        if($calendar_item['view_important'] == '1' && $chk_doc_inout['cs_important'] == '1') {
            $doc_title .= (!empty($doc_title)) ? " ★" : "★";
        }

        if($calendar_item['view_title'] == '1' && !empty($chk_doc_inout['cs_title'])) {
            $doc_title .= (!empty($doc_title)) ? " {$chk_doc_inout['cs_title']}" : "{$chk_doc_inout['cs_title']}";
        }

        if($calendar_item['view_write_s_no'] == '1' ) {
            $doc_title .= (!empty($doc_title)) ? " ({$chk_doc_inout['cs_s_name']})" : "({$chk_doc_inout['cs_s_name']})";
        }

        if($calendar_item['view_comment'] == '1' ) {
            $doc_title .= (!empty($doc_title)) ? " ({$chk_doc_inout['csc_cnt']})" : "({$chk_doc_inout['csc_cnt']})";
        }
        $chk_doc_inout['title']    = $doc_title;

        $chk_doc_inout_day             = date("w", strtotime($chk_doc_inout['cs_s_date']));
        $chk_doc_inout['cs_date_name'] = date("m/d", strtotime($chk_doc_inout['cs_s_date']))."({$date_name_option[$chk_doc_inout_day]})";

        $chk_doc_inout_calendar_list[]  = $chk_doc_inout;
    }
    $smarty->assign("cur_date_name", $cur_date_name);
    $smarty->assign("chk_doc_inout_calendar_list", $chk_doc_inout_calendar_list);

    # DOC 이벤트 캘린더
    $doc_cur_s_date   = date("Y-m-d", strtotime("-10 days"))." 00:00:00";
    $doc_cur_e_date   = date("Y-m-d", strtotime("+10 days"))." 23:59:59";

    $doc_event_calendar_sql = "
        SELECT
           cs.cs_category,
           cs.cs_s_date,
           cs.cs_e_date,
           cs.cs_title
        FROM calendar_schedule as cs
        WHERE cs.cal_id='dp_event'
            AND 
            (
              (cs_s_date >= '{$doc_cur_s_date}' AND cs_s_date <= '{$doc_cur_e_date}') OR
              (cs_e_date >= '{$doc_cur_s_date}' AND cs_e_date <= '{$doc_cur_e_date}') OR
              ('{$doc_cur_s_date}' >= cs_s_date AND '{$doc_cur_s_date}' <= cs_e_date) OR
              ('{$doc_cur_e_date}' >= cs_s_date AND '{$doc_cur_e_date}' <= cs_e_date) 
            )
        ORDER BY cs_s_date ASC
    ";
    $doc_event_calendar_query   = mysqli_query($my_db, $doc_event_calendar_sql);
    $doc_event_calendar_list    = [];
    while($doc_event_calendar = mysqli_fetch_assoc($doc_event_calendar_query))
    {
        $title          = "";
        $cs_s_date      = date('Y-m-d', strtotime($doc_event_calendar['cs_s_date']));
        $cs_s_day_val   = date("w", strtotime($doc_event_calendar['cs_s_date']));
        $cs_s_day_name  = $date_name_option[$cs_s_day_val];
        $cs_s_day_text  = date("m/d", strtotime($doc_event_calendar['cs_s_date']))."({$cs_s_day_name})";
        $cs_s_hour      = date('H:i', strtotime($doc_event_calendar['cs_s_date']));

        $cs_e_date      = date('Y-m-d', strtotime($doc_event_calendar['cs_e_date']));
        $cs_e_day_val   = date("w", strtotime($doc_event_calendar['cs_e_date']));
        $cs_e_day_name  = $date_name_option[$cs_e_day_val];
        $cs_e_day_text  = date("m/d", strtotime($doc_event_calendar['cs_e_date']))."({$cs_e_day_name})";
        $cs_e_hour      = date('H:i', strtotime($doc_event_calendar['cs_e_date']));


        $cs_s_date_text = $cs_e_date_text = "";
        if($cs_s_date == $cs_e_date) {
            if($cs_s_hour != "00:00"){
                $cs_s_date_text = $cs_s_day_text." ".$cs_s_hour;
            }elseif($cs_e_hour != "23:50"){
                $cs_s_date_text = $cs_s_day_text." ".$cs_s_hour;
            }else{
                $cs_s_date_text = $cs_s_day_text;
            }

            if($cs_e_hour != "23:50" && $cs_e_hour != "00:00"){
                $cs_e_date_text = $cs_e_day_text." ".$cs_e_hour;
            }
        }
        else
        {
            if($cs_s_hour != "00:00"){
                $cs_s_date_text = $cs_s_day_text." ".$cs_s_hour;
            }else{
                $cs_s_date_text = $cs_s_day_text;
            }

            if($cs_e_hour != "23:50" && $cs_e_hour != "00:00"){
                $cs_e_date_text = $cs_e_day_text." ".$cs_e_hour;
            }else{
                $cs_e_date_text = $cs_e_day_text;
            }
        }

        $doc_event_calendar['cs_s_date_text'] = $cs_s_date_text;
        $doc_event_calendar['cs_e_date_text'] = $cs_e_date_text;

        $doc_event_calendar_list[]  = $doc_event_calendar;
    }
    $smarty->assign("doc_event_calendar_list", $doc_event_calendar_list);
}

$smarty->assign("sch_work", $sch_work);
$smarty->display("wise_wm/work_main_{$sch_work}.html");
?>
