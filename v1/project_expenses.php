<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/project.php');
require('inc/helper/company.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

$excel_editable = false;
$select_editable = false;
if(in_array($session_team, $permission_team_list) || permissionNameCheck($session_permission, "재무관리자")){
    $excel_editable = true;
}

# 경영지원실만 전체 수정권한
if((permissionNameCheck($session_permission, "재무관리자") && $session_team == '00211')){
    $select_editable = true;
}

# 프로세스 처리
$process  = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "add_new_expenses")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $sch_pj_no  = isset($_POST['sch_pj_no']) ? $_POST['sch_pj_no'] : "";
    $appr_date_val = date('w');
    $appr_date     = date('Y-m-d',strtotime('+'.(5 -$appr_date_val).'days'));

    if(!empty($sch_pj_no))
    {
        $pj_sql     ="SELECT pj_name FROM project WHERE pj_no='{$sch_pj_no}'";
        $pj_query   = mysqli_query($my_db, $pj_sql);
        $pj_result  = mysqli_fetch_assoc($pj_query);
        $pj_name    = $pj_result['pj_name'];
        $ins_sql    = "INSERT INTO project_expenses SET pj_no='{$sch_pj_no}', pj_name='{$pj_name}', spend_method='1', cal_method='2', bankbook='1', req_s_no='{$session_s_no}', req_team='{$session_team}', req_date=now(), regdate=now(), appr_date='{$appr_date}'";
    }else{
        $ins_sql    = "INSERT INTO project_expenses SET spend_method='1', cal_method='2', bankbook='1', req_s_no='{$session_s_no}', req_team='{$session_team}', req_date=now(), regdate=now(), appr_date='{$appr_date}'";
    }

    if(!mysqli_query($my_db, $ins_sql)){
        exit("<script>alert('사업비 지출관리 추가등록에 실패했습니다');location.href='project_expenses.php?{$search_url}';</script>");
    }else{
        exit("<script>location.href='project_expenses.php?{$search_url}';</script>");
    }
}
elseif($process == "row_delete")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_sql    = "UPDATE project_expenses SET display='2', pe_no=null WHERE pj_e_no='{$pj_e_no}' AND display='1'";
    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('사업비 지출관리 삭제에 실패했습니다');location.href='project_expenses.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('사업비 지출관리 삭제했습니다');location.href='project_expenses.php?{$search_url}';</script>");
    }
}
elseif($process == "row_duplicate")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $dup_sql    = "INSERT INTO project_expenses(`pj_no`, `pj_name`,`expenses_status`,`kind`,`cal_method`,`spend_method`,`req_s_no`,`req_team`,`req_date`,`appr_date`,`regdate`,`display`,`bankbook`)
                    (SELECT `pj_no`, `pj_name`, '1',`kind`,`cal_method`,`spend_method`,`req_s_no`,`req_team`,`req_date`,`appr_date`,NOW(), '1',`bankbook` FROM project_expenses WHERE pj_e_no='{$pj_e_no}')";

    if(!mysqli_query($my_db, $dup_sql)){
        exit("<script>alert('사업비 지출관리 복제에 실패했습니다');location.href='project_expenses.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('사업비 지출관리 복제했습니다');location.href='project_expenses.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_pj_no")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $pj_no      = (isset($_POST['pj_no'])) ? $_POST['pj_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(empty($pj_no)){
        exit("<script>alert('선택된 프로젝트가 없습니다. 내부통용_사업명 변경에 실패했습니다.');location.href='project_expenses.php?{$search_url}#pj_name_{$pj_e_no}';</script>");
    }

    $pj_name_sql    = "SELECT pj_name FROM project WHERE pj_no='{$pj_no}' LIMIT 1";
    $pj_name_query  = mysqli_query($my_db, $pj_name_sql);
    $pj_name_result = mysqli_fetch_assoc($pj_name_query);
    $pj_name        = isset($pj_name_result['pj_name']) ? $pj_name_result['pj_name'] : "";

    if(empty($pj_name)){
        exit("<script>alert('사업명이 없습니다. 내부통용_사업명 변경에 실패했습니다.');location.href='project_expenses.php?{$search_url}#pj_name_{$pj_e_no}';</script>");
    }

    $upd_sql    = "UPDATE project_expenses SET pj_no='{$pj_no}', pj_name='{$pj_name}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('내부통용_사업명 변경에 실패했습니다');location.href='project_expenses.php?{$search_url}#pj_name_{$pj_e_no}';</script>");
    }else{
        exit("<script>location.href='project_expenses.php?{$search_url}#pj_name_{$pj_e_no}';</script>");
    }
}
elseif($process == "modify_kind")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? $_POST['value'] : "";
    $upd_sql    = "UPDATE project_expenses SET kind='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "구분 변경에 실패 하였습니다.";
    else
        echo "구분이 변경되었습니다.";
    exit;
}
elseif($process == "modify_cal_method")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? $_POST['value'] : "";
    $upd_sql    = "UPDATE project_expenses SET cal_method='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "정산방법 변경에 실패 하였습니다.";
    else
        echo "정산방법이 변경되었습니다.";
    exit;
}
elseif($process == "modify_spend_method")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? $_POST['value'] : "";
    $upd_sql    = "UPDATE project_expenses SET spend_method='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "정산방법 변경에 실패 하였습니다.";
    else
        echo "정산방법이 변경되었습니다.";
    exit;
}
elseif($process == "modify_req_s_no")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? $_POST['value'] : "";
    $sub_value  = (isset($_POST['sub_value'])) ? addslashes(trim($_POST['sub_value'])) : "";
    $upd_sql    = "UPDATE project_expenses SET req_s_no='{$value}', req_team='{$sub_value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "제출자 변경에 실패 하였습니다.";
    else
        echo "제출자가 변경되었습니다.";
    exit;
}
elseif($process == "modify_req_date")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? $_POST['value'] : "";
    $upd_sql    = "UPDATE project_expenses SET req_date='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "작성일 변경에 실패 하였습니다.";
    else
        echo "작성일이 변경되었습니다.";
    exit;
}
elseif($process == "modify_appr_date")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? $_POST['value'] : "";
    $upd_sql    = "UPDATE project_expenses SET appr_date='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "승인일 변경에 실패 하였습니다.";
    else
        echo "승인일이 변경되었습니다.";
    exit;
}
elseif($process == "modify_supply_price")
{
    $pj_e_no        = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $supply_price   = (isset($_POST['value']) && !empty($_POST['value'])) ? str_replace(",", "", $_POST['value']) : 0;
    $upd_sql        = "UPDATE project_expenses SET supply_price='{$supply_price}' WHERE pj_e_no='{$pj_e_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "공급가 변경에 실패 하였습니다.";
    else
        echo "공급가가 변경되었습니다.";
    exit;
}
elseif($process == "modify_tax_price")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $tax_price  = (isset($_POST['value']) && !empty($_POST['value'])) ? str_replace(",", "", $_POST['value']) : 0;
    $upd_sql    = "UPDATE project_expenses SET tax_price='{$tax_price}' WHERE pj_e_no='{$pj_e_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "세액 변경에 실패 하였습니다.";
    else
        echo "세액이 변경되었습니다.";
    exit;
}
elseif($process == "modify_total_price")
{
    $pj_e_no        = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $total_price    = (isset($_POST['value']) && !empty($_POST['value'])) ? str_replace(",", "", $_POST['value']) : 0;
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_sql    = "UPDATE project_expenses SET total_price='{$total_price}' WHERE pj_e_no='{$pj_e_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "지출액 변경에 실패 하였습니다.";
    else
        echo "지출액이 변경되었습니다.";
    exit;
}
elseif($process == "modify_memo")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? addslashes(trim($_POST['value'])) : "";
    $upd_sql    = "UPDATE project_expenses SET memo='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "비고 변경에 실패 하였습니다.";
    else
        echo "비고가 변경되었습니다.";
    exit;
}
elseif($process == "modify_c_no")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $c_no       = isset($_POST['c_no']) ? $_POST['c_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $c_name_sql    = "SELECT c_name, bk_title, bk_name, bk_num, lector_vat_type FROM company WHERE c_no='{$c_no}' LIMIT 1";
    $c_name_query  = mysqli_query($my_db, $c_name_sql);
    $c_name_result = mysqli_fetch_assoc($c_name_query);
    $c_name        = isset($c_name_result['c_name']) ? $c_name_result['c_name'] : "";
    $bk_title      = isset($c_name_result['bk_title']) ? $c_name_result['bk_title'] : $_POST['bk_title'];
    $bk_name       = isset($c_name_result['bk_name']) ? $c_name_result['bk_name'] : $_POST['bk_name'];
    $bk_num        = isset($c_name_result['bk_num']) ? $c_name_result['bk_num'] : $_POST['bk_num'];
    $lec_vat_type  = isset($c_name_result['lector_vat_type']) ? $c_name_result['lector_vat_type'] : $_POST['lec_vat_type'];

    if(empty($c_name)){
        exit("<script>alert('업체명/서명이 없습니다. 업체명/성명 변경에 실패했습니다.');location.href='project_expenses.php?{$search_url}#c_name_{$pj_e_no}';</script>");
    }

    $upd_sql    = "UPDATE project_expenses SET c_no='{$c_no}', c_name='{$c_name}', bk_title='{$bk_title}', bk_name='{$bk_name}', bk_num='{$bk_num}', lec_vat_type='{$lec_vat_type}' WHERE pj_e_no='{$pj_e_no}'";

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('업체명/성명 변경에 실패했습니다');location.href='project_expenses.php?{$search_url}#c_name_{$pj_e_no}';</script>");
    }else{
        exit("<script>location.href='project_expenses.php?{$search_url}#c_name_{$pj_e_no}';</script>");
    }
}
elseif($process == "modify_bankbook")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? $_POST['value'] : "";
    $upd_sql    = "UPDATE project_expenses SET bankbook='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "출금통장 변경에 실패 하였습니다.";
    else
        echo "출금통장이 변경되었습니다.";
    exit;
}
elseif($process == "modify_bk_title")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? $_POST['value'] : "";
    $upd_sql    = "UPDATE project_expenses SET bk_title='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "은행명 변경에 실패 하였습니다.";
    else
        echo "은행명이 변경되었습니다.";
    exit;
}
elseif($process == "modify_bk_num")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? $_POST['value'] : "";
    $upd_sql    = "UPDATE project_expenses SET bk_num='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "계좌번호 변경에 실패 하였습니다.";
    else
        echo "계좌번호가 변경되었습니다.";
    exit;
}
elseif($process == "modify_bk_name")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? addslashes(trim($_POST['value'])) : "";
    $upd_sql    = "UPDATE project_expenses SET bk_name='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "예금주 변경에 실패 하였습니다.";
    else
        echo "예금주가 변경되었습니다.";
    exit;
}
elseif($process == "modify_acc_info")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? addslashes(trim($_POST['value'])) : "";
    $upd_sql    = "UPDATE project_expenses SET acc_info='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "예금주정보 변경에 실패 하였습니다.";
    else
        echo "예금주정보 변경되었습니다.";
    exit;
}
elseif($process == "modify_status")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? addslashes(trim($_POST['value'])) : "";
    $upd_sql    = "UPDATE project_expenses SET expenses_status='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "진행상태 변경에 실패 하였습니다.";
    else
        echo "진행상태가 변경되었습니다.";
    exit;
}
elseif($process == "chk_multi_status")
{
    $chk_pj_e_no_list = (isset($_POST['chk_pj_e_no_list'])) ? $_POST['chk_pj_e_no_list'] : "";
    $chk_status       = (isset($_POST['chk_status'])) ? $_POST['chk_status'] : "0";
    $search_url       = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $pj_e_no_val  = explode("||", $chk_pj_e_no_list);
    $pj_e_no_list = implode(',', $pj_e_no_val);

    $upd_sql = "UPDATE project_expenses SET expenses_status='{$chk_status}' WHERE pj_e_no IN({$pj_e_no_list})";

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('진행상태 변경에 실패 하였습니다.');location.href='project_expenses.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('진행상태가 변경되었습니다.');location.href='project_expenses.php?{$search_url}';</script>");
    }
}
elseif($process == "chk_multi_delete")
{
    $chk_pj_e_no_list = (isset($_POST['chk_pj_e_no_list'])) ? $_POST['chk_pj_e_no_list'] : "";
    $search_url       = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $pj_e_no_val  = explode("||", $chk_pj_e_no_list);
    $pj_e_no_list = implode(',', $pj_e_no_val);

    $upd_sql = "UPDATE project_expenses SET display='2', pe_no=null WHERE pj_e_no IN({$pj_e_no_list})";

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('사업비 지출관리 삭제에 실패했습니다');location.href='project_expenses.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('사업비 지출관리 삭제했습니다');location.href='project_expenses.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_vat_type")
{
    $pj_e_no    = isset($_POST['pj_e_no']) ? $_POST['pj_e_no'] : "";
    $value      = (isset($_POST['value'])) ? addslashes(trim($_POST['value'])) : "";
    $upd_sql    = "UPDATE project_expenses SET lec_vat_type='{$value}' WHERE pj_e_no='{$pj_e_no}' AND display='1'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "원천세 신고방식 변경에 실패 하였습니다.";
    else
        echo "원천세 신고방식이 변경되었습니다.";
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "21";
$nav_title   = "사업비 지출관리";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사업비 지출관리 검색조건
$add_where          = "pje.display='1'";
$sch_pj_no          = isset($_GET['sch_pj_no']) ? $_GET['sch_pj_no'] : "";
$sch_pj_name        = isset($_GET['sch_pj_name']) ? $_GET['sch_pj_name'] : "";
$sch_cal_method     = isset($_GET['sch_cal_method']) ? $_GET['sch_cal_method'] : "";
$sch_spend_method   = isset($_GET['sch_spend_method']) ? $_GET['sch_spend_method'] : "1";
$sch_req_team       = isset($_GET['sch_req_team']) ? $_GET['sch_req_team'] : "";
$sch_req_name       = isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : $session_name;
$sch_req_s_date     = isset($_GET['sch_req_s_date']) ? $_GET['sch_req_s_date'] : date('Y-m-d',strtotime("-6 day"));
$sch_req_e_date     = isset($_GET['sch_req_e_date']) ? $_GET['sch_req_e_date'] : date('Y-m-d');
$sch_req_all        = isset($_GET['sch_req_all']) ? $_GET['sch_req_all'] : "";
$sch_appr_s_date    = isset($_GET['sch_appr_s_date']) ? $_GET['sch_appr_s_date'] : "";
$sch_appr_e_date    = isset($_GET['sch_appr_e_date']) ? $_GET['sch_appr_e_date'] : "";
$sch_c_name         = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_pj_kind        = isset($_GET['sch_pj_kind']) ? $_GET['sch_pj_kind'] : "";
$sch_bankbook       = isset($_GET['sch_bankbook']) ? $_GET['sch_bankbook'] : "";
$sch_bk_name        = isset($_GET['sch_bk_name']) ? $_GET['sch_bk_name'] : "";
$sch_memo           = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
$sch_payment_contents  = isset($_GET['sch_payment_contents']) ? $_GET['sch_payment_contents'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "req_date";
$ori_ord_type       = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "req_date";
$sch_exp_status     = isset($_GET['sch_exp_status']) ? $_GET['sch_exp_status'] : "";

if(!empty($sch_pj_no)) {
    $add_where .= " AND pje.pj_no = '{$sch_pj_no}'";
    $smarty->assign("sch_pj_no",$sch_pj_no);
}

if(!empty($sch_pj_name)) {
    $add_where .= " AND pje.pj_name like '%{$sch_pj_name}%'";
    $smarty->assign("sch_pj_name", $sch_pj_name);
}

if(!empty($sch_cal_method)) {
    $add_where .= " AND pje.cal_method = '{$sch_cal_method}'";
    $smarty->assign("sch_cal_method",$sch_cal_method);
}

if(!empty($sch_spend_method)) {
    $add_where .= " AND pje.spend_method = '{$sch_spend_method}'";
    $smarty->assign("sch_spend_method", $sch_spend_method);
}

if (!empty($sch_req_team))
{
    if($sch_req_team != 'all')
    {
        $sch_team_code_where = getTeamWhere($my_db, $sch_req_team);
        if($sch_team_code_where){
            $add_where .= " AND `pje`.req_team IN ({$sch_team_code_where})";
        }
    }

    $smarty->assign("sch_req_team",$sch_req_team);
}else{

    $sch_team_code_where = getTeamWhere($my_db, $session_team);
    if($sch_team_code_where){
        $add_where .= " AND `pje`.req_team IN ({$sch_team_code_where})";
    }
    $smarty->assign("sch_req_team",$session_team);
}

if(!empty($sch_req_name)) {
    $add_where .= " AND pje.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_req_name}%')";
    $smarty->assign("sch_req_name", $sch_req_name);
}

if($sch_req_all != 'all') { // sch month not alll
    if(!empty($sch_req_s_date))
    {
        $add_where .= " AND pje.req_date >= '{$sch_req_s_date}'";

    }

    if(!empty($sch_req_e_date))
    {
        $add_where .= " AND pje.req_date <= '{$sch_req_e_date}'";
    }
}
$smarty->assign("sch_req_all",$sch_req_all);
$smarty->assign("sch_req_s_date", $sch_req_s_date);
$smarty->assign("sch_req_e_date", $sch_req_e_date);

if(!empty($sch_appr_s_date))
{
    $add_where .= " AND pje.appr_date >= '{$sch_appr_s_date}'";
    $smarty->assign("sch_appr_s_date", $sch_appr_s_date);
}

if(!empty($sch_appr_e_date))
{
    $add_where .= " AND pje.appr_date <= '{$sch_appr_e_date}'";
    $smarty->assign("sch_appr_e_date", $sch_appr_e_date);
}

if(!empty($sch_c_name)) {
    $add_where .= " AND pje.c_name like '%{$sch_c_name}%'";
    $smarty->assign("sch_c_name", $sch_c_name);
}

if(!empty($sch_pj_kind)) {
    $add_where .= " AND pje.kind IN(SELECT pek.pj_k_no FROM project_expenses_kind pek WHERE pek.k_name like '%{$sch_pj_kind}%')";
    $smarty->assign("sch_pj_kind", $sch_pj_kind);
}

if(!empty($sch_bankbook)) {
    $add_where .= " AND pje.bankbook = '{$sch_bankbook}'";
    $smarty->assign("sch_bankbook", $sch_bankbook);
}

if(!empty($sch_bk_name)) {
    $add_where .= " AND pje.bk_name like '%{$sch_bk_name}%'";
    $smarty->assign("sch_bk_name", $sch_bk_name);
}

if(!empty($sch_memo)) {
    $add_where .= " AND pje.memo like '%{$sch_memo}%'";
    $smarty->assign("sch_memo", $sch_memo);
}

if(!empty($sch_payment_contents)) {
    $add_where .= " AND pje.pe_no IN(SELECT pe.pe_no FROM personal_expenses pe WHERE pe.payment_contents like '%{$sch_payment_contents}%')";
    $smarty->assign("sch_payment_contents", $sch_payment_contents);
}

if(!empty($sch_exp_status)) {
    $add_where .= " AND pje.expenses_status = '{$sch_exp_status}'";
    $smarty->assign("sch_exp_status", $sch_exp_status);
}

$add_orderby = "";
$ord_type_by = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";
    if(!empty($ord_type_by))
    {
        $ord_type_list = [];
        if($ord_type == 'appr_date'){
            $ord_type_list[] = "appr_date";
        }
        elseif($ord_type == 'kind')
        {
            $ord_type_list[] = "pj_no";
            $ord_type_list[] = "kind";
        }
        elseif($ord_type == 'pj_name')
        {
            $ord_type_list[] = "pj_name";
        }
        elseif($ord_type == 'req_name')
        {
            $ord_type_list[] = "req_name";
        }
        elseif($ord_type == 'req_date')
        {
            $ord_type_list[] = "req_date";
        }

        if($ori_ord_type == $ord_type)
        {
            if($ord_type_by == '1'){
                $orderby_val = "ASC";
            }elseif($ord_type_by == '2'){
                $orderby_val = "DESC";
            }
        }else{
            $ord_type_by = '2';
            $orderby_val = "DESC";
        }

        foreach($ord_type_list as $ord_type_val){
            $add_orderby .= "{$ord_type_val} {$orderby_val}, ";
        }
    }else{
        $add_orderby .= "pj_e_no DESC, ";
    }
}
$add_orderby .= "pj_e_no DESC";

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);

# 지출 합계 & 게시물 수
$total_sql      = "SELECT count(pj_e_no) as total_cnt, SUM(total_price) as total_price FROM project_expenses pje WHERE {$add_where}";
$total_query    = mysqli_query($my_db, $total_sql);
$total_result   = mysqli_fetch_assoc($total_query);
$total_num      = isset($total_result['total_cnt']) ? $total_result['total_cnt'] : 0;
$total_price    = isset($total_result['total_price']) ? $total_result['total_price'] : 0;

# 페이징 처리
$pages      = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num        = 20;
$pagenum    = ceil($total_num/$num);
if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}
$offset = ($pages-1) * $num;
$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "project_expenses.php", $pagenum, $search_url);

$smarty->assign("total_num", number_format($total_num));
$smarty->assign("total_price", number_format($total_price));
$smarty->assign("page", $pages);
$smarty->assign("search_url",$search_url);
$smarty->assign("pagelist", $pagelist);

# 리스트 쿼리
$project_expenses_sql = "
    SELECT
        *,
        (SELECT s.s_name FROM staff s WHERE s.s_no=pje.req_s_no LIMIT 1) as req_name,
        (SELECT pe.payment_contents FROM personal_expenses pe WHERE pe.pe_no=pje.pe_no LIMIT 1) as pe_content,
        (SELECT `c`.corp_kind FROM company `c` WHERE `c`.c_no=pje.c_no) as corp_kind
    FROM project_expenses pje
    WHERE {$add_where}
    ORDER BY {$add_orderby}
    LIMIT {$offset},{$num}
";
$project_expenses_query = mysqli_query($my_db, $project_expenses_sql);
$project_expenses_list = [];

while($project_expenses = mysqli_fetch_assoc($project_expenses_query))
{
    $pj_kind_list = [];
    $pj_no = $project_expenses['pj_no'];

    if(!empty($pj_no))
    {
        $pj_kind_sql = "SELECT * FROM project_expenses_kind WHERE pj_no='{$pj_no}' ORDER BY k_priority ASC";
        $pj_kind_query = mysqli_query($my_db, $pj_kind_sql);
        while($pj_kind = mysqli_fetch_assoc($pj_kind_query))
        {
            $pj_kind_list[$pj_kind['pj_k_no']] = $pj_kind['k_name'];
        }
    }
    $project_expenses['kind_list'] = $pj_kind_list;
    $project_expenses['editable']  = false;

    $project_expenses['wd_content'] = "";
    if(!empty($project_expenses['wd_no']))
    {
        $wd_content_sql     = "SELECT w.c_name FROM `work` w WHERE w.wd_no = '{$project_expenses['wd_no']}' LIMIT 1";
        $wd_content_query   = mysqli_query($my_db, $wd_content_sql);
        $wd_content_result  = mysqli_fetch_assoc($wd_content_query);
        if(isset($wd_content_result['c_name'])){
            $project_expenses['wd_content'] = $wd_content_result['c_name'];
        }
    }

    if($project_expenses['spend_method'] == '2' || $project_expenses['spend_method'] == '3'){
        if($session_s_no == '62'){
            $project_expenses['editable'] = true;
        }
    }
    else
    {
        if($project_expenses['expenses_status'] == '1')
        {
            if($project_expenses['req_s_no'] == $session_s_no || $session_s_no == '62'){
                $project_expenses['editable'] = true;
            }
        }
        elseif($project_expenses['expenses_status'] == '2' || $project_expenses['expenses_status'] == '3')
        {
            if($session_s_no == '62'){
                $project_expenses['editable'] = true;
            }
        }
    }

    $project_expenses_list[] = $project_expenses;
}

$team_model = Team::Factory();
$team_full_name_list = $team_model->getTeamFullNameList();

$smarty->assign("excel_editable", $excel_editable);
$smarty->assign("select_editable", $select_editable);
$smarty->assign("cur_date", date("Y-m-d"));
$smarty->assign("cal_method_option", getCalMethodOption());
$smarty->assign("spend_method_option", getSpendMethodOption());
$smarty->assign("bankbook_option", getBankBookOption());
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("expenses_status_option", getExpensesStatusOption());
$smarty->assign("lec_vat_type_option", getLectorVatTypeOption());
$smarty->assign("project_expenses_list", $project_expenses_list);

$smarty->display('project_expenses.html');
?>
