<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/_price.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');
require('inc/model/CommerceOrder.php');

# 발주관리 Model Init & Process 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$set_model      = CommerceOrder::Factory();
$commerce_model = CommerceOrder::Factory();
$commerce_model->setMainInit("commerce_order", "no");

$unit_model     = ProductCmsUnit::Factory();
$report_model   = ProductCmsStock::Factory();
$report_model->setStockReport();

if($process == "f_option_no")
{
    $comm_no        = (isset($_POST['no'])) ? $_POST['no'] : "";
    $unit_no        = (isset($_POST['val'])) ? $_POST['val'] : "";

    $unit_item      = $unit_model->getItem($unit_no);
    $unit_price     = $unit_item['sup_price_vat'];

    $comm_ord_item  = $commerce_model->getOrderItem($comm_no);
    $set_no         = $comm_ord_item['set_no'];
    $quantity       = $comm_ord_item['quantity'];
    $sup_loc_type   = $comm_ord_item['sup_loc_type'];
    $group_no       = $comm_ord_item['group'];

    $total_price    = $unit_price*$quantity;
    $supply_price   = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1, 0);
    $vat_price      = ($sup_loc_type == '2') ? 0 : ($total_price-$supply_price);

    $upd_data       = array(
        "no"            => $comm_no,
        "option_no"     => $unit_no,
        "unit_price"    => $unit_price,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price,
        "total_price"   => $supply_price
    );

    if (!$commerce_model->update($upd_data)){
        echo "품명 저장에 실패 하였습니다.";
    } else {
        exit("<script>location.href='commerce_order_iframe_supply.php?no={$set_no}';</script>");
    }
    exit;
}
elseif($process == "f_quantity")
{
    $comm_no        = (isset($_POST['no'])) ? $_POST['no'] : "";
    $quantity       = (isset($_POST['val'])) ? $_POST['val'] : "";
    $quantity       = str_replace(",","",trim($quantity));

    $comm_ord_item  = $commerce_model->getOrderItem($comm_no);
    $set_no         = $comm_ord_item['set_no'];
    $sup_loc_type   = $comm_ord_item['sup_loc_type'];
    $group_no       = $comm_ord_item['group'];

    $total_price    = $quantity*$comm_ord_item['unit_price'];
    $supply_price   = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1, 0);
    $vat_price      = ($sup_loc_type == '2') ? 0 : ($total_price-$supply_price);

    $upd_data       = array(
        "no"            => $comm_no,
        "quantity"      => $quantity,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price,
        "total_price"   => $supply_price
    );

    if (!$commerce_model->update($upd_data)){
        echo "수량 저장에 실패 하였습니다.";
    }else {
        exit("<script>location.href='commerce_order_iframe_supply.php?no={$set_no}';</script>");
    }
    exit;
}
elseif($process == "f_sup_date")
{
    $no         = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value      = (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$commerce_model->update(array("no" => $no, "sup_date" => $value)))
        echo "공급완료일 저장에 실패 하였습니다.";
    else
        echo "공급완료일이 저장 되었습니다.";
    exit;
}
elseif($process == "f_limit_date")
{
    $no         = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value      = (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$commerce_model->update(array("no" => $no, "limit_date" => $value)))
        echo "유통기한 저장에 실패 하였습니다.";
    else
        echo "유통기한이 저장 되었습니다.";
    exit;
}
elseif($process == "f_unit_price")
{
    $comm_no        = (isset($_POST['no'])) ? $_POST['no'] : "";
    $unit_price     = (isset($_POST['val'])) ? $_POST['val'] : "";
    $unit_price     = str_replace(",","",trim($unit_price));

    $comm_ord_item  = $commerce_model->getOrderItem($comm_no);
    $set_no         = $comm_ord_item['set_no'];
    $sup_loc_type   = $comm_ord_item['sup_loc_type'];
    $group_no       = $comm_ord_item['group'];

    $total_price    = $unit_price*$comm_ord_item['quantity'];
    $supply_price   = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1, 0);
    $vat_price      = ($sup_loc_type == '2') ? 0 : ($total_price-$supply_price);

    $upd_data       = array(
        "no"            => $comm_no,
        "unit_price"    => $unit_price,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price,
        "total_price"   => $supply_price
    );

    if (!$commerce_model->update($upd_data)){
        echo "단가(VAT 포함) 저장에 실패 하였습니다.";
    }else{
        exit("<script>location.href='commerce_order_iframe_supply.php?no={$set_no}';</script>");
    }
    exit;
}
elseif($process == "f_supply_price")
{
    $comm_no        = (isset($_POST['no'])) ? $_POST['no'] : "";
    $supply_price   = (isset($_POST['val'])) ? $_POST['val'] : "";
    $supply_price   = str_replace(",", "", trim($supply_price));

    $comm_ord_item  = $commerce_model->getOrderItem($comm_no);
    $set_no         = $comm_ord_item['set_no'];
    $sup_loc_type   = $comm_ord_item['sup_loc_type'];
    $group_no       = $comm_ord_item['group'];
    $total_price    = $supply_price+$comm_ord_item['vat'];

    $upd_data = array(
        "no"            => $comm_no,
        "supply_price"  => $supply_price
    );

    if (!$commerce_model->update($upd_data)){
        echo "공급가 저장에 실패 하였습니다.";
    }else {
        echo "공급가가 저장 되었습니다.";
    }
    exit;
}
elseif($process == "f_vat")
{
    $comm_no        = (isset($_POST['no'])) ? $_POST['no'] : "";
    $vat_price      = (isset($_POST['val'])) ? $_POST['val'] : "";
    $vat_price      = str_replace(",", "", trim($vat_price));

    $upd_data = array(
        "no"    => $comm_no,
        "vat"   => $vat_price
    );

    if (!$commerce_model->update($upd_data))
        echo "부가세 저장에 실패 하였습니다.";
    else
        echo "부가세가 저장 되었습니다.";
    exit;
}
elseif($process == "f_memo")
{
    $no         = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$commerce_model->update(array("no" => $no, "memo" => $value)))
        echo "비고 저장에 실패 하였습니다.";
    else
        echo "비고가 저장 되었습니다.";
    exit;
}
elseif($process == "f_total_price")
{
    $comm_no        = (isset($_POST['no'])) ? $_POST['no'] : "";
    $total_price    = (isset($_POST['val'])) ? $_POST['val'] : "";
    $total_price   = str_replace(",", "", trim($total_price));

    $upd_data = array(
        "no"            => $comm_no,
        "total_price"   => $total_price
    );

    if (!$commerce_model->update($upd_data)){
        echo "총 입고액 저장에 실패 하였습니다.";
    }else {
        echo "총 입고액이 저장 되었습니다.";
    }
    exit;
}
elseif($process == "add_order_supply") # 입고완료 직접추가
{
    $set_no         = $_POST['set_no'];
    $sup_loc_type   = $_POST['sup_loc_type'];
    $group_no       = $_POST['group_new'];

    if(empty($set_no)|| empty($_POST['sup_option_no_new']) || empty($_POST['sup_quantity_new'])){
        exit("<script>alert('필수 데이터가 없습니다. 입고완료 상세내역 추가에 실패했습니다.');history.back();</script>");
    }

    $unit_no        = $_POST['sup_option_no_new'];
    $unit_item      = $unit_model->getItem($unit_no);

    if(!isset($unit_item['no']) || empty($unit_item['no']))
    {
        exit("<script>alert('해당 구성품목이 없습니다. 입고완료 상세내역 추가에 실패했습니다.');history.back();</script>");
    }

    $sup_quantity_new   = str_replace(",", "", trim($_POST['sup_quantity_new'])); // 컴마 제거하기
    $unit_price         = $unit_item['sup_price_vat'];
    $total_price        = $unit_price*$sup_quantity_new;
    $supply_price       = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1,0);
    $vat_price          = ($sup_loc_type == '2') ? 0 : $total_price-$supply_price;

    $ins_data   = array(
        "set_no"        => $set_no,
        "type"          => "supply",
        "option_no"     => $unit_no,
        "group"         => $group_no,
        "unit_price"    => $unit_price,
        "quantity"      => $sup_quantity_new,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price,
        "regdate"       => date("Y-m-d H:i:s"),
    );

    if(!empty($_POST['sup_date_new'])){
        $ins_data["sup_date"] = $_POST['sup_date_new'];
    }

    if(!empty($_POST['limit_date_new'])){
        $ins_data["limit_date"] = $_POST['limit_date_new'];
    }else{
        if(isset($unit_item['expiration']) && $unit_item['expiration'] > 0)
        {
            if(!empty($_POST['sup_date_new'])){
                $limit_date_new = date('Y-m-d', strtotime($_POST['sup_date_new']." +{$unit_item['expiration']} months"));
                $ins_data["limit_date"] = $limit_date_new;
            }
        }
    }

    if($commerce_model->insert($ins_data)){
        exit("<script>alert('입고완료 상세내역 추가에 성공했습니다');location.href='commerce_order_iframe_supply.php?no={$set_no}';</script>");
    } else {
        exit("<script>alert('입고완료 상세내역 추가에 실패했습니다');history.back();</script>");
    }
}
elseif($process == "supply_delete")
{
    $set_no 	    = (isset($_POST['set_no'])) ? $_POST['set_no'] : "";
    $sup_loc_type	= (isset($_POST['sup_loc_type'])) ? $_POST['sup_loc_type'] : "";
    $group_no	    = (isset($_POST['group_no'])) ? $_POST['group_no'] : "";
    $co_no 	        = (isset($_POST['co_no'])) ? $_POST['co_no'] : "";
    $comm_ord_item  = $commerce_model->getOrderItem($co_no);

    if(empty($set_no) || empty($co_no)){
        echo("<script>alert('삭제에 실패 하였습니다.');</script>");
    }
    else
    {
        if (!$commerce_model->delete($co_no)) {
            echo("<script>alert('삭제에 실패 하였습니다.');</script>");
        }else{
            if(!empty($comm_ord_item['stock_no'])){
                $report_model->update(array("no" => $comm_ord_item['stock_no'], "ord_no" => 0));
            }
        }
    }

    exit ("<script>location.href='commerce_order_iframe_supply.php?no={$set_no}';</script>");
}
elseif($process == 'add_etc_group')
{
    $set_no	    = (isset($_POST['set_no'])) ? $_POST['set_no'] : "";
    $group_no 	= (isset($_POST['group_no'])) ? $_POST['group_no'] : "";

    $commerce_model->setMainInit("commerce_order_etc", "etc_no");
    $commerce_model->insert(array("set_no" => $set_no, "group_no" => $group_no, "regdate" => date("Y-m-d H:i:s")));

    echo $commerce_model->getInsertId();
    exit;
}
elseif($process == 'del_etc_group')
{
    $etc_no  = (isset($_POST['etc_no'])) ? $_POST['etc_no'] : "";

    $commerce_model->setMainInit("commerce_order_etc", "etc_no");
    $commerce_model->delete($etc_no);

    exit;
}
elseif($process == 'f_etc_price')
{
    $no         = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value      = (isset($_POST['val'])) ? str_replace(',','', $_POST['val']) : "";

    $commerce_model->setMainInit("commerce_order_etc", "etc_no");

    if (!$commerce_model->update(array("etc_no" => $no, "price" => $value)))
        echo "기타 금액 저장에 실패 하였습니다.";
    else
        echo "기타 금액이 저장 되었습니다.";
    exit;
}
elseif($process == 'f_etc_vat')
{
    $no         = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value      = (isset($_POST['val'])) ? str_replace(',','', $_POST['val']) : "";

    $commerce_model->setMainInit("commerce_order_etc", "etc_no");

    if (!$commerce_model->update(array("etc_no" => $no, "vat" => $value)))
        echo "기타 부가세 저장에 실패 하였습니다.";
    else
        echo "기타 부가세가 저장 되었습니다.";
    exit;
}
elseif($process == 'f_etc_memo')
{
    $no         = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    $commerce_model->setMainInit("commerce_order_etc", "etc_no");

    if (!$commerce_model->update(array("etc_no" => $no, "memo" => $value)))
        echo "기타 비고 저장에 실패 하였습니다.";
    else
        echo "기타 비고가 저장 되었습니다.";
    exit;
}
elseif($process == 'f_group')
{
    $comm_no        = (isset($_POST['no'])) ? $_POST['no'] : "";
    $group_no       = (isset($_POST['val'])) ? $_POST['val'] : "";

    $comm_ord_item  = $commerce_model->getOrderItem($comm_no);
    $set_no         = $comm_ord_item['set_no'];
    $sup_loc_type   = $comm_ord_item['sup_loc_type'];

    if (!$commerce_model->update(array("no" => $comm_no, "group" => $group_no))){
        echo "그룹 변경에 실패 하였습니다.";
    }else {
        exit("<script>location.href='commerce_order_iframe_supply.php?no={$set_no}';</script>");
    }
    exit;
}
elseif($process == "f_confirm_memo") # 입고완료 메모
{
    $set_no = $_POST['no'];
    $value  = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    $commerce_model->setMainInit("commerce_order_set", "no");

    if (!$commerce_model->update(array("no" => $set_no, "confirm_memo" => $value))){
        echo "입고완료 메모를 저장에 실패했습니다.";
    }else {
        echo "입고완료 메모를 저장했습니다.";
    }
    exit;
}
elseif($process == "add_stock_supply") # 입고완료 외부물류 추가
{
    $set_no         = $_POST['set_no'];
    $option_no      = $_POST['option_no'];
    $stock_date     = $_POST['stock_date'];
    $stock_qty      = $_POST['stock_qty'];

    if(empty($set_no) || empty($option_no) || empty($stock_date) || empty($stock_qty)){
        exit("<script>alert('필수 데이터가 없습니다. 입고완료 상세내역 추가에 실패했습니다.');history.back();</script>");
    }

    $set_item       = $set_model->getItem($set_no);
    $group_no       = $set_model->getMaxGroup($set_no);
    $sup_loc_type   = $set_item['sup_loc_type'];
    $log_c_no       = $set_item['log_c_no'];
    $unit_item      = $unit_model->getItem($option_no, $log_c_no);

    if(!isset($unit_item['sku'])){
        $unit_item = $unit_model->getItem($option_no);
    }

    if(!isset($unit_item['no']) || empty($unit_item['no'])) {
        exit("<script>alert('해당 구성품목이 없습니다. 입고완료 상세내역 추가에 실패했습니다.');history.back();</script>");
    }

    $unit_price         = $unit_item['sup_price_vat'];
    $total_price        = $unit_price*$stock_qty;
    $supply_price       = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1,0);
    $vat_price          = ($sup_loc_type == '2') ? 0 : $total_price-$supply_price;
    $regdate            = date("Y-m-d H:i:s");

    $ins_data   = array(
        "set_no"        => $set_no,
        "type"          => "supply",
        "option_no"     => $option_no,
        "group"         => $group_no,
        "unit_price"    => $unit_price,
        "sup_date"      => $stock_date,
        "quantity"      => $stock_qty,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price,
        "regdate"       => $regdate,
    );

    $warehouse_sql      = "SELECT warehouse FROM product_cms_stock_warehouse WHERE log_c_no='{$log_c_no}' LIMIT 1";
    $warehouse_query    = mysqli_query($my_db, $warehouse_sql);
    $warehouse_result   = mysqli_fetch_assoc($warehouse_query);
    $warehouse          = isset($warehouse_result['warehouse']) ? $warehouse_result['warehouse'] : $unit_item['warehouse'];

    $report_data = array(
        'regdate'       => $stock_date,
        'report_type'   => "3",
        'type'          => "1",
        'order'         => 10000,
        'is_order'      => 1,
        "client"        => "와이즈플래닛컴퍼니",
        'state'         => "발주입고",
        'subs_state'    => "일반입고",
        'is_confirm'    => "아니오",
        'confirm_state' => "1",
        'brand'         => $unit_item['brand'],
        'prd_kind'      => ($unit_item['type'] == '1') ? "상품" : "부속품",
        'prd_unit'      => $unit_item['no'],
        'sup_c_no'      => $unit_item['sup_c_no'],
        'log_c_no'      => $log_c_no,
        'option_name'   => $unit_item['option_name'],
        'sku'           => $unit_item['sku'],
        'prd_name'      => $unit_item['option_name'],
        'stock_type'    => $warehouse,
        'stock_qty'     => $stock_qty,
        'memo'          => "발주번호 : {$set_no} 입고완료 직접추가",
        'reg_s_no'      => $session_s_no,
        'report_date'   => $regdate,
    );

    if($commerce_model->insert($ins_data))
    {
        $new_ord_no = $commerce_model->getInsertId();
        $report_data['ord_no'] = $new_ord_no;
        $report_model->insert($report_data);

        $new_report_no = $report_model->getInsertId();
        $commerce_model->update(array("no" => $new_ord_no, "stock_no" => $new_report_no));

        exit("<script>alert('입고완료 상세내역 추가에 성공했습니다');location.href='commerce_order_iframe_supply.php?no={$set_no}';</script>");
    } else {
        exit("<script>alert('입고완료 상세내역 추가에 실패했습니다');history.back();</script>");
    }
}


# 발주서 입고내역
$set_no = isset($_GET['no']) ? $_GET['no'] : "";

$commerce_order_set_sql = "
    SELECT
        cos.no,
        cos.state,
        cos.sup_c_no,
        cos.sup_loc_type,
        (SELECT c.license_type FROM company c WHERE c.c_no=cos.sup_c_no) as license_type,
        cos.log_c_no,
        cos.req_s_no,
        cos.file_path,
        cos.file_name,
        cos.confirm_memo
    FROM commerce_order_set cos
    WHERE cos.no='{$set_no}'
";
$commerce_order_set_query = mysqli_query($my_db, $commerce_order_set_sql);
$commerce_order_set       = mysqli_fetch_array($commerce_order_set_query);

$file_list = [];
if(!empty($commerce_order_set['file_path']) && !empty($commerce_order_set['file_name']))
{
    $idx = 0;
    $file_path_list = explode(",", $commerce_order_set['file_path']);
    $file_name_list = explode(",", $commerce_order_set['file_name']);
    foreach($file_path_list as $file_path)
    {
        $file_name = isset($file_name_list[$idx]) ? $file_name_list[$idx] : "";
        $file_list[] = array("name" => $file_name, "path" => $file_path);

        $idx++;
    }
}
$commerce_order_set['file_list'] = $file_list;

$commerce_is_usd     = ($commerce_order_set['sup_loc_type'] == '2') ? true : false;
$commerce_price_type = ($commerce_is_usd) ? "$" : "₩";

# 권한 체크
$commerce_chk_sql    = "SELECT COUNT(*) as cnt FROM commerce_order_set `cos` WHERE `cos`.display='1' AND `cos`.state = '2' AND `cos`.`no`='{$set_no}' AND (`cos`.req_s_no = '{$session_s_no}' OR `cos`.sup_c_no IN(SELECT DISTINCT sup_c_no FROM product_cms_unit WHERE ord_s_no='{$session_s_no}' OR ord_sub_s_no='{$session_s_no}' OR ord_third_s_no='{$session_s_no}'))";
$commerce_chk_query  = mysqli_query($my_db, $commerce_chk_sql);
$commerce_chk_result = mysqli_fetch_assoc($commerce_chk_query);
$is_stock_editable   = false;

if(($commerce_order_set['log_c_no'] != '2809' && $commerce_order_set['state'] == '2') && (isset($commerce_chk_result['cnt']) && $commerce_chk_result['cnt'] > 0)){
    $is_stock_editable = true;
}

$smarty->assign("is_stock_editable", $is_stock_editable);

# 발주서 입고현황
$com_rep_sql 	= "SELECT co.option_no, (SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_name, SUM(co.quantity) as qty FROM commerce_order co WHERE co.set_no='{$set_no}' AND co.type='request' GROUP BY co.option_no";
$com_rep_query 	= mysqli_query($my_db, $com_rep_sql);
$com_rep_list 	= [];
while($com_rep_result = mysqli_fetch_array($com_rep_query))
{
    $com_rep_list[$com_rep_result['option_no']] = array(
        'option_name' => $com_rep_result['option_name'],
        'sup_qty' 	  => 0,
        'req_qty' 	  => $com_rep_result['qty'],
        'dem_qty' 	  => $com_rep_result['qty'],
        'option_per'  => 0,
    );
}

# 발주서 입고내역 그룹별 데이터
$commerce_group_list        = [];
$commerce_group_chk_list    = [];
$commerce_group_order_sql   = "
    SELECT 
        co.`group` as group_no, 
        count(co.no) as cnt, 
        SUM(co.supply_price) as supply_total,
        (SELECT SUM(vat) FROM commerce_order_etc coe WHERE coe.set_no='{$set_no}' AND coe.group_no=co.`group`) as etc_vat
    FROM commerce_order co 
    WHERE co.set_no='{$set_no}' AND co.type='supply' GROUP BY co.`group`
";
$commerce_group_order_query = mysqli_query($my_db, $commerce_group_order_sql);
while($commerce_group_order = mysqli_fetch_assoc($commerce_group_order_query))
{
    $commerce_group_list[$commerce_group_order['group_no']]     = $commerce_group_order;
    $commerce_group_chk_list[$commerce_group_order['group_no']] = array("idx" => 1, "cnt" => $commerce_group_order['cnt'], "supply_total" => $commerce_group_order['supply_total'], "vat_total" => $commerce_group_order['etc_vat'], "vat_check" => $commerce_group_order['etc_vat']);
}

# 발주서 입고내역 전체 내역
$commerce_order_sql = "
    SELECT
        co.no,
        co.stock_no,
        co.option_no,
        (SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_name,
        (SELECT pcu.priority FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_priority,
        co.quantity,
        co.unit_price,
        co.supply_price,
        co.total_price,
        co.vat,
        co.tax,   
        co.group,   
        co.memo,
        co.sup_date,
        co.limit_date
    FROM commerce_order co
    WHERE co.set_no='{$set_no}' AND co.type='supply'
    ORDER BY `group` ASC, sup_date ASC, no ASC
";
$commerce_order_query = mysqli_query($my_db, $commerce_order_sql);
$commerce_order_list  = [];
$commerce_etc_list    = [];

while($commerce_order_result = mysqli_fetch_array($commerce_order_query))
{
    if(isset($com_rep_list[$commerce_order_result['option_no']])){
        $com_rep_list[$commerce_order_result['option_no']]['sup_qty'] += $commerce_order_result['quantity'];
        $com_rep_list[$commerce_order_result['option_no']]['dem_qty'] -= $commerce_order_result['quantity'];
    }

    $quantity       = $commerce_order_result['quantity'];
    $supply_price   = $commerce_order_result['supply_price'];
    $vat_price      = $commerce_order_result['vat'];
    $total_price    = $commerce_order_result['total_price'];

    # 그룹별 데이터
    $group_etc_vat_total    = 0;
    $group_etc_vat_per      = 0;
    $group_vat_price        = 0;
    if(isset($commerce_group_list[$commerce_order_result['group']]))
    {
        $group_supply_total     = $commerce_group_chk_list[$commerce_order_result['group']]['supply_total'];
        $group_vat_total        = $commerce_group_chk_list[$commerce_order_result['group']]['vat_total'];
        $group_etc_vat_per      = round($supply_price/$group_supply_total, 2);
        $group_vat_price        = round($group_vat_total*$group_etc_vat_per);

        $commerce_group_chk_list[$commerce_order_result['group']]['vat_check'] -= $group_vat_price;

        if($commerce_group_chk_list[$commerce_order_result['group']]['idx'] == $commerce_group_chk_list[$commerce_order_result['group']]['cnt']){
            $group_vat_price += $commerce_group_chk_list[$commerce_order_result['group']]['vat_check'];
        }
        $commerce_group_chk_list[$commerce_order_result['group']]['idx']++;
    }

    $commerce_order_result['unit_price']   = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['unit_price']) : getKrwFormatPrice($commerce_order_result['unit_price']);
    $commerce_order_result['supply_price'] = ($commerce_is_usd) ? getUsdFormatPrice($supply_price) : getNumberFormatPrice($supply_price);
    $commerce_order_result['vat']          = ($commerce_is_usd) ? getUsdFormatPrice($vat_price) : getNumberFormatPrice($vat_price);
    $commerce_order_result['total_price']   = getNumberFormatPrice($total_price);


    $org_price  = $total_price/$quantity;
    $org_vat    = ($vat_price+$group_vat_price)/$quantity;
    $org_total  = round(($total_price*1.1)/$quantity);

    $commerce_order_result['org_price']    = getNumberFormatPrice(round($org_price));
    $commerce_order_result['org_vat']      = $org_vat > 0 ? getNumberFormatPrice(round($org_vat)) : 0;
    $commerce_order_result['org_total']    = getNumberFormatPrice(round($org_total));

    $commerce_order_list[] = $commerce_order_result;
}

if(!empty($com_rep_list)){
    foreach($com_rep_list as $key => $com_rep)
    {
        $com_rep['option_per'] = round($com_rep['sup_qty']/$com_rep['req_qty']*100, 2);
        $com_rep_list[$key] = $com_rep;
    }
}

if(!empty($commerce_group_list))
{
    foreach($commerce_group_list as $group_no => $commerce_group)
    {
        $etc_sql    = "SELECT * FROM commerce_order_etc WHERE set_no='{$commerce_order_set['no']}' AND `group_no`='{$group_no}' ORDER BY regdate ASC";
        $etc_query  = mysqli_query($my_db, $etc_sql);
        while($etc_result = mysqli_fetch_assoc($etc_query))
        {
            $etc_result['price'] = getNumberFormatPrice($etc_result['price']);
            $etc_result['vat']   = getNumberFormatPrice($etc_result['vat']);
            $commerce_etc_list[$group_no][] = $etc_result;
        }
    }
}

$smarty->assign('product_cms_unit_list', $commerce_model->getCommerceOrderSetUnit($commerce_order_set['no']));
$smarty->assign("commerce_is_usd", $commerce_is_usd);
$smarty->assign("commerce_price_type", $commerce_price_type);
$smarty->assign('com_rep_list', $com_rep_list);
$smarty->assign('commerce_order_set', $commerce_order_set);
$smarty->assign('commerce_group_list', $commerce_group_list);
$smarty->assign('commerce_etc_list', $commerce_etc_list);
$smarty->assign('commerce_order_list', $commerce_order_list);

$smarty->display('commerce_order_iframe_supply.html');

?>
