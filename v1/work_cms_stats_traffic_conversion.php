<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');

$process = isset($_POST['process']) ? $_POST['process'] : "";
if($process == "del_conversion")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $convert_date   = isset($_POST['convert_date']) ? $_POST['convert_date'] : "";

    if(empty($convert_date)){
        exit("<script>alert('삭제에 실패했습니다. 선택된 날짜가 없습니다');location.href='work_cms_stats_traffic_conversion.php?{$search_url}';</script>");
    }

    $convert_s_date = "{$convert_date} 00:00:00";
    $convert_e_date = "{$convert_date} 23:59:59";

    $del_sql = "DELETE FROM commerce_conversion WHERE convert_date BETWEEN '{$convert_s_date}' AND '{$convert_e_date}'";

    if(mysqli_query($my_db, $del_sql)){
        exit("<script>alert('삭제했습니다');location.href='work_cms_stats_traffic_conversion.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제에 실패했습니다');location.href='work_cms_stats_traffic_conversion.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "211";
$nav_title   = "커머스 유입/전환(통계)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$sch_brand_total_list       = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

# 마지막 날짜
$max_convert_date_sql     = "SELECT convert_date FROM commerce_conversion WHERE dp_c_no > 0 AND brand > 0 ORDER BY convert_date DESC LIMIT 1";
$max_convert_date_query   = mysqli_query($my_db, $max_convert_date_sql);
$max_convert_date_result  = mysqli_fetch_assoc($max_convert_date_query);
$max_convert_date         = isset($max_convert_date_result['convert_date']) ? date("Y-m-d", strtotime($max_convert_date_result['convert_date'])) : date('Y-m-d', strtotime("-3 days"));

# 검색조건
$add_where          = "display='1'";
$prev_val           = date('Y-m-d', strtotime("{$max_convert_date} -13 days"));
$search_url         = getenv("QUERY_STRING");
$sch_traffic_type   = isset($_GET['sch_traffic_type']) ? $_GET['sch_traffic_type'] : "day";
$sch_convert_s_date = isset($_GET['sch_convert_s_date']) ? $_GET['sch_convert_s_date'] : $prev_val;
$sch_convert_e_date = isset($_GET['sch_convert_e_date']) ? $_GET['sch_convert_e_date'] : $max_convert_date;
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_dp_company     = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";
$sch_url            = isset($_GET['sch_url']) ? $_GET['sch_url'] : "";

if(empty($search_url)){
    $search_url = "sch_traffic_type=day";
}

if($sch_convert_e_date > $max_convert_date){
    $sch_convert_e_date = $max_convert_date;
}
$smarty->assign("max_convert_date", $max_convert_date);

if(!empty($sch_convert_s_date)){
    $smarty->assign("sch_convert_s_date", $sch_convert_s_date);
}

if(!empty($sch_convert_e_date)){
    $smarty->assign("sch_convert_e_date", $sch_convert_e_date);
}

if(!empty($sch_dp_company)) {
    $add_where .= " AND dp_c_no='{$sch_dp_company}'";
    $smarty->assign("sch_dp_company", $sch_dp_company);
}

if(!empty($sch_url)) {
    $smarty->assign("sch_url", $sch_url);
}

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

# 브랜드 옵션
if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND brand = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where         .= " AND brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

# URL 리스트
$sch_url_list       = [];
$sch_page_url_list  = [];
$sch_url_sql        = "SELECT url_no, page_title, url, dp_c_no, REPLACE(url,'idx=','') as chk_url FROM commerce_url WHERE {$add_where} ORDER BY LENGTH(chk_url) ASC, chk_url";
$sch_url_query      = mysqli_query($my_db, $sch_url_sql);
while($sch_url_result = mysqli_fetch_assoc($sch_url_query))
{
    $sch_url_list[$sch_url_result['url_no']] = $sch_url_result['page_title']." :: ".$sch_url_result['url'];
    if(strpos($sch_url_result['url'],"idx=") !== false){
        $sch_page_url_list[$sch_url_result['url_no']] = $sch_url_result['page_title']." :: ".$sch_url_result['url'];
    }
}

# 최근 업로드일
$imweb_max_convert_date_sql     = "SELECT convert_date FROM commerce_conversion WHERE dp_c_no='1372' ORDER BY convert_date DESC LIMIT 1";
$imweb_max_convert_date_query   = mysqli_query($my_db, $imweb_max_convert_date_sql);
$imweb_max_convert_date_result  = mysqli_fetch_assoc($imweb_max_convert_date_query);
$imweb_max_convert_date         = isset($imweb_max_convert_date_result['convert_date']) ? date("Y-m-d, H", strtotime($imweb_max_convert_date_result['convert_date'])) : "";

$doc_max_convert_date_sql       = "SELECT convert_date FROM commerce_conversion WHERE dp_c_no='5800' ORDER BY convert_date DESC LIMIT 1";
$doc_max_convert_date_query     = mysqli_query($my_db, $doc_max_convert_date_sql);
$doc_max_convert_date_result    = mysqli_fetch_assoc($doc_max_convert_date_query);
$doc_max_convert_date           = isset($doc_max_convert_date_result['convert_date']) ? date("Y-m-d, H", strtotime($doc_max_convert_date_result['convert_date'])) : "";

$ilenol_max_convert_date_sql    = "SELECT convert_date FROM commerce_conversion WHERE dp_c_no='5958' ORDER BY convert_date DESC LIMIT 1";
$ilenol_max_convert_date_query  = mysqli_query($my_db, $ilenol_max_convert_date_sql);
$ilenol_max_convert_date_result = mysqli_fetch_assoc($ilenol_max_convert_date_query);
$ilenol_max_convert_date        = isset($ilenol_max_convert_date_result['convert_date']) ? date("Y-m-d, H", strtotime($ilenol_max_convert_date_result['convert_date'])) : "";

$nuzam_max_convert_date_sql     = "SELECT convert_date FROM commerce_conversion WHERE dp_c_no='6012' ORDER BY convert_date DESC LIMIT 1";
$nuzam_max_convert_date_query   = mysqli_query($my_db, $nuzam_max_convert_date_sql);
$nuzam_max_convert_date_result  = mysqli_fetch_assoc($nuzam_max_convert_date_query);
$nuzam_max_convert_date         = isset($nuzam_max_convert_date_result['convert_date']) ? date("Y-m-d, H", strtotime($nuzam_max_convert_date_result['convert_date'])) : "";

$smarty->assign("search_url", $search_url);
$smarty->assign("imweb_max_convert_date", $imweb_max_convert_date);
$smarty->assign("doc_max_convert_date", $doc_max_convert_date);
$smarty->assign("ilenol_max_convert_date", $ilenol_max_convert_date);
$smarty->assign("nuzam_max_convert_date", $nuzam_max_convert_date);
$smarty->assign("sch_traffic_type", $sch_traffic_type);
$smarty->assign("sch_traffic_type_option", getTrafficTypeOption());
$smarty->assign("sch_dp_company_option", getTrafficDpCompanyOption());
$smarty->assign("sch_url_list", $sch_url_list);
$smarty->assign("sch_page_url_list", $sch_page_url_list);
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);
$smarty->assign("sch_brand_total_list", $sch_brand_total_list);

if($sch_traffic_type == "page"){
    $smarty->display('work_cms_stats_traffic_conversion_page.html');
}else{
    $smarty->display('work_cms_stats_traffic_conversion.html');
}
?>
