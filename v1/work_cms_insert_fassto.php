<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

require('Classes/PHPExcel.php');
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/model/ProductCms.php');
require('inc/model/WorkCms.php');
require('inc/model/Custom.php');

# 기본 엑셀 변수 설정
$product_model  = ProductCms::Factory();
$order_model    = WorkCms::Factory();

$log_c_no       = "5484";
$dp_c_no        = "5427";
$dp_c_name      = "스마트스토어_닥터피엘";
$task_run_s_no  = $session_s_no;
$task_run_team  = $session_team;
$regdate        = date('Y-m-d H:i:s');
$fassto_file    = $_FILES["fassto_file"];

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($fassto_file['name']) && !empty($fassto_file['name'])){
    $upload_file_path = add_store_file($fassto_file, "upload_management");
    $upload_file_name = $fassto_file['name'];
}

$upload_kind   = isset($_POST['fassto_kind']) ? $_POST['fassto_kind'] : "1";

$upload_data  = array(
    "upload_type"   => "6",
    "upload_kind"   => $upload_kind,
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $task_run_s_no,
    "regdate"       => $regdate,
);
$upload_model->insert($upload_data);

$file_no         = $upload_model->getInsertId();
$excel_file_path = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($excel_file_path);
$excel->setActiveSheetIndex(0);

$objWorksheet       = $excel->getActiveSheet();
$totalRow           = $objWorksheet->getHighestRow();
$excel_data_list    = [];
$delivery_data      = [];

# DB 데이터 시작
for ($i = 2; $i <= $totalRow; $i++)
{
    #변수 초기화
    $order_number = $shop_ord_no = $sku = $prd_name = $prd_option = $origin_ord_no = $payment_type = $notice = "";
    $recipient = $recipient_hp = $recipient_addr = $recipient_hp2 = $zip_code =  $delivery_type = $delivery_no = "";
    $stock_date = $order_date = $payment_date = $stock_date_val = $order_date_val = "";
    $quantity = $delivery_price = $final_price = $dp_price_val = $dp_price = $dp_price_vat = $unit_price = $unit_delivery_price = 0;

    if($upload_kind == "1")
    {
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //쇼핑몰 주문번호
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //주문번호
        $stock_date_val     = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   //발송처리일
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("AC{$i}")->getValue()));  //주문일자
        $sku                = (string)trim(addslashes($objWorksheet->getCell("AM{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue()));  //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("AW{$i}")->getValue())); //수령자 HP
        $recipient_addr     = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue()));  //수령지
        $zip_code           = (string)trim(addslashes($objWorksheet->getCell("AZ{$i}")->getValue()));  //우편번호
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("AS{$i}")->getValue()));  //배송비
        $origin_ord_no      = $order_number;

        # 운송장 정보
        $delivery_type      = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //택배사
        $delivery_no        = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //송장번호

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false) {
            $order_date_convert   = str_replace('/','-', $order_date_val);
            $order_date           = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
        }else{
            $order_time_cal   = ($order_date_val-25569)*86400;
            $order_time       = date("Y-m-d H:i", $order_time_cal);
            $order_date       = date("Y-m-d H:i", strtotime("{$order_time} -9 hour"));
        }
        $payment_date = $order_date;

        if(!empty($stock_date_val))
        {
            if(strpos($stock_date_val, "/") !== false) {
                $stock_date           = $stock_date_val;
            }
            elseif(strpos($stock_date_val, "/") !== false) {
                $stock_date_convert   = str_replace('/','-', $stock_date_val);
                $stock_date           = $stock_date_convert ? date('Y-m-d', strtotime("{$stock_date_convert} -33 hours")) : "";
            }else{
                $stock_time_cal   = ($stock_date_val-25569)*86400;
                $stock_time       = date("Y-m-d", $stock_time_cal);
                $stock_date       = date("Y-m-d", strtotime("{$stock_time} -9 hour"));
            }
        }else{
            $stock_date = date("Y-m-d", strtotime($order_date));
        }
    }
    elseif($upload_kind == "2")
    {
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //쇼핑몰 주문번호
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //결제일
        $sku                = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("AE{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("AJ{$i}")->getValue()));  //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AO{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("BK{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("BL{$i}")->getValue())); //수령자 HP
        $recipient_addr_val = (string)trim(addslashes($objWorksheet->getCell("BO{$i}")->getValue()));  //수령지
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("AD{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("AR{$i}")->getValue()));  //배송비

        # 운송장 정보
        $delivery_type      = (string)trim(addslashes($objWorksheet->getCell("AV{$i}")->getValue()));  //택배사
        $delivery_no        = (string)trim(addslashes($objWorksheet->getCell("AW{$i}")->getValue()));  //송장번호

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false) {
            $order_date_convert   = str_replace('/','-', $order_date_val);
            $order_date           = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
        }else{
            $order_time_cal   = ($order_date_val-25569)*86400;
            $order_time       = date("Y-m-d H:i", $order_time_cal);
            $order_date       = date("Y-m-d H:i", strtotime("{$order_time} -9 hour"));
        }
        $payment_date   = $order_date;
        $stock_date     = date("Y-m-d", strtotime($order_date));

        $recipient_addr = "";
        $zip_code       = "";
        $notice         = "파스토 교환 주문건";

        if(!empty($recipient_addr_val)){
            $recipient_addr_tmp_list = explode(")", $recipient_addr_val);
            $zip_code       = isset($recipient_addr_tmp_list[0]) ? str_replace("(", "", $recipient_addr_tmp_list[0]) : "";
            $zip_code       = trim($zip_code);
            $recipient_addr = isset($recipient_addr_tmp_list[1]) ? trim($recipient_addr_tmp_list[1]) : "";
        }
    }
    elseif($upload_kind == "3")
    {
        $shop_ord_no        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //쇼핑몰 주문번호
        $order_number       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //주문번호
        $order_date_val     = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));  //결제일
        $sku                = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue()));  //상품코드
        $quantity           = (string)trim(addslashes($objWorksheet->getCell("AG{$i}")->getValue()));  //수량
        $dp_price_val       = (string)trim(addslashes($objWorksheet->getCell("AM{$i}")->getValue()));  //결제금액
        $recipient          = (string)trim(addslashes($objWorksheet->getCell("AQ{$i}")->getValue()));  //수령자
        $recipient_hp       = (string)trim(addslashes($objWorksheet->getCell("BE{$i}")->getValue()));  //수령자 전화번호
        $recipient_hp2      = (string)trim(addslashes($objWorksheet->getCell("BF{$i}")->getValue())); //수령자 HP
        $recipient_addr_val = (string)trim(addslashes($objWorksheet->getCell("BG{$i}")->getValue()));  //수령지
        $payment_type       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));  //결제수단
        $prd_name           = (string)trim(addslashes($objWorksheet->getCell("AD{$i}")->getValue()));  //상품명
        $prd_option         = (string)trim(addslashes($objWorksheet->getCell("AF{$i}")->getValue()));  //옵션정보
        $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("AT{$i}")->getValue()));  //배송비

        # 운송장 정보
        $delivery_type      = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue()));  //택배사
        $delivery_no        = (string)trim(addslashes($objWorksheet->getCell("AY{$i}")->getValue()));  //송장번호

        //엑셀 시간 형식 변경
        if(strpos($order_date_val, "/") !== false) {
            $order_date_convert   = str_replace('/','-', $order_date_val);
            $order_date           = $order_date_convert ? date('Y-m-d H:i:s', strtotime("{$order_date_convert} -33 hours")) : "";
        }else{
            $order_time_cal   = ($order_date_val-25569)*86400;
            $order_time       = date("Y-m-d H:i", $order_time_cal);
            $order_date       = date("Y-m-d H:i", strtotime("{$order_time} -9 hour"));
        }
        $payment_date   = $order_date;
        $stock_date     = date("Y-m-d", strtotime($order_date));

        $recipient_addr = "";
        $zip_code       = "";
        $notice         = "파스토 반품 주문건";

        if(!empty($recipient_addr_val)){
            $recipient_addr_tmp_list = explode(")", $recipient_addr_val);
            $zip_code       = isset($recipient_addr_tmp_list[0]) ? str_replace("(", "", $recipient_addr_tmp_list[0]) : "";
            $zip_code       = trim($zip_code);
            $recipient_addr = isset($recipient_addr_tmp_list[1]) ? trim($recipient_addr_tmp_list[1]) : "";
        }
    }

    if(empty($shop_ord_no)){
        break;
    }

    $dp_price       = (int)$dp_price_val / 1.1;
    $dp_price       = round($dp_price, -1); // 1의자리 반올림
    $dp_price_vat   = (int)$dp_price_val;
    $dp_price_vat   = round($dp_price_vat, -1); // 1의자리 반올림
    $unit_price     = $dp_price_vat;

    // 상품코드가 없는 경우
    if(!$sku){
        echo "ROW : {$i}<br/>";
        echo "택배리스트 반영에 실패하였습니다.<br>상품코드가 없습니다.<br>
            주문날짜    : {$order_date}<br>
            주문번호    : {$order_number}<br>
            수령자명    : {$recipient}<br>
            Excel 상품명: {$prd_name}<br>";
        exit;
    }

    $prd_cms_data   = $product_model->getWorkCmsItem($sku, "prd_code");
    if(empty($prd_cms_data))
    {
        echo "ROW : {$i}<br/>";
        echo "택배리스트 반영에 실패하였습니다.<br>해당 상품이 없습니다.<br>
            주문날짜    : {$order_date}<br>
            주문번호    : {$order_number}<br>
            수령자명    : {$recipient}<br>
            Excel 상품명: {$prd_name}<br>";
        exit;
    }

    $prd_no         = $prd_cms_data['prd_no'];
    $c_no           = $prd_cms_data['c_no'];
    $c_name         = $prd_cms_data['c_name'];
    $s_no           = $prd_cms_data['manager'];
    $team           = $prd_cms_data['team'];
    $task_req       = "상품명 :: {$prd_name}\r\n주문번호 :: {$order_number}\r\n구매처 :: {$dp_c_name}";
    $task_req_s_no  = $s_no;
    $task_req_team  = $team;

    if(!$order_model->chkExistOrder("order_number='{$order_number}' AND prd_no='{$prd_no}' AND log_c_no='{$log_c_no}'"))
    {
        $excel_data_list[] = array(
            "delivery_state"        => 4,
            "stock_date"            => $stock_date,
            "c_no"                  => $c_no,
            "c_name"                => $c_name,
            "log_c_no"              => $log_c_no,
            "s_no"                  => $s_no,
            "team"                  => $team,
            "prd_no"                => $prd_no,
            "quantity"              => $quantity,
            "order_number"          => $order_number,
            "order_date"            => $order_date,
            "payment_date"          => $payment_date,
            "recipient"             => $recipient,
            "recipient_hp"          => $recipient_hp,
            "recipient_hp2"         => $recipient_hp2,
            "recipient_addr"        => $recipient_addr,
            "zip_code"              => $zip_code,
            "dp_price"              => $dp_price,
            "dp_price_vat"          => $dp_price_vat,
            "dp_c_no"               => $dp_c_no,
            "dp_c_name"             => $dp_c_name,
            "regdate"               => $regdate,
            "write_date"            => $regdate,
            "notice"                => $notice,
            "task_req"              => $task_req,
            "task_req_s_no"         => $task_req_s_no,
            "task_req_team"         => $task_req_team,
            "task_run_regdate"      => !empty($order_date) ? $order_date : $regdate,
            "task_run_s_no"         => $task_run_s_no,
            "task_run_team"         => $task_run_team,
            "payment_type"          => $payment_type,
            "shop_ord_no"           => $shop_ord_no,
            "delivery_price"        => $delivery_price,
            "unit_price"            => $unit_price,
            "unit_delivery_price"   => $unit_delivery_price,
            "origin_ord_no"         => $origin_ord_no,
            "file_no"               => $file_no,
        );

        $delivery_data[] = array(
            "order_number"  => $order_number,
            "dp_c_no"       => $dp_c_no,
            "delivery_type" => $delivery_type,
            "delivery_no"   => $delivery_no
        );
    }
}

if(!empty($excel_data_list))
{
    if($order_model->multiInsert($excel_data_list))
    {
        if(!empty($delivery_data))
        {
            $delivery_model = WorkCms::Factory();
            $delivery_model->setMainInit("work_cms_delivery", "no");
            $delivery_model->multiInsert($delivery_data);
        }

        exit("<script>alert('택배리스트에 반영 되었습니다.');location.href='work_list_cms.php?sch_ord_bundle=1&sch_file_no={$file_no}';</script>");
    }else{
        exit("<script>alert('택배리스트 반영에 실패했습니다.');location.href='waple_upload_management.php';</script>");
    }
}

exit("<script>alert('택배리스트 반영할 데이터가 없습니다.');location.href='waple_upload_management.php';</script>");


?>
