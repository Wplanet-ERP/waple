<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_cms.php');
require('inc/helper/deposit.php');
require('inc/helper/withdraw.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');
require('inc/model/Work.php');
require('inc/model/WorkCms.php');

$is_freelancer      = false;
$is_with_editable   = false;
$is_csis_editable   = false;
if($session_staff_state == '2'){ // 프리랜서의 경우 기본 접근제한으로 설정
    $is_freelancer = true;
    if($session_my_c_type == "3"){
        $is_with_editable = true;
    }

    if($session_partner == "csis"){
        $is_csis_editable = true;
    }
}

$is_cms_staff   = false;
if($session_team == "00244" || permissionNameCheck($session_permission, "마스터관리자") || $session_s_no == "61"){
    $is_cms_staff = true;
}

# Model Init
$company_model  = Company::Factory();
$cms_model      = WorkCms::Factory();
$with_log_c_no  = '2809';

# Process Start
$process = (isset($_POST['process']))?$_POST['process']:"";

if($process == "memo")
{
    $w_no 		= (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$cms_model->update(array("w_no" => $w_no, "manager_memo" => addslashes($value))))
        echo "관리자 메모 저장에 실패 하였습니다.";
    else
        echo "관리자 메모가 저장 되었습니다.";
    exit;
}
elseif($process == "delivery_state")
{
    $w_no 		= (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$cms_model->update(array("w_no" => $w_no, "delivery_state" => $value)))
        echo "발송 진행상태 변경에 실패 했습니다.";
    else
        echo "발송 진행상태 변경에 성공 했습니다.";
    exit;
}
elseif($process == "delivery_no")
{
    $deli_no 	= (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $ord_no     = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		= (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    $cms_model->setMainInit("work_cms_delivery", "no");
    $delivery_item      = $cms_model->getItem($deli_no);
    $org_delivery_no    = $delivery_item['delivery_no'];

    if($value == "" || !$cms_model->chkExistDeliveryNo($ord_no, $value))
    {
        if (!$cms_model->updateToArray(array("delivery_no" => $value), array("order_number" => $ord_no, "delivery_no" => $org_delivery_no)))
            echo "운송장번호 변경에 실패 했습니다.";
        else {
            echo "운송장번호 변경에 성공 했습니다.";
        }
    }else{
        echo "운송장 변경에 실패 했습니다. 이미 사용중인 운송장번호 입니다.";
    }
    exit;
}
elseif($process == "notice")
{
    $w_no 		= (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$cms_model->update(array("w_no" => $w_no, "notice" => addslashes($value))))
        echo "특이사항 저장에 실패 했습니다.";
    else
        echo "특이사항 저장에 성공 했습니다.";
    exit;
}
elseif($process == "f_parent_order_number")
{
    $order_number 	= (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $value 		    = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(!empty($order_number))
    {
        if (!$cms_model->updateOrder($order_number, array("parent_order_number" => $value)))
            echo "parent 주문번호 저장에 실패 했습니다.";
        else
            echo "parent 주문번호 저장에 성공 했습니다.";
    }else{
        echo "parent 주문번호 저장에 실패 했습니다.";
    }
    exit;
}
elseif($process == "f_regdate")
{
    $w_no 	    = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$cms_model->update(array("w_no" => $w_no, "regdate" => $value)))
        echo "작성일 저장에 실패했습니다.";
    else
        echo "작성일 저장에 성공했습니다.";
    exit;
}
elseif($process == "f_stock_date")
{
    $order_number 	= (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $value 		    = (isset($_POST['val'])) ? $_POST['val'] : "";
    $search_url	    = isset($_POST['search_url'])?$_POST['search_url']:"";

    if (!$cms_model->updateOrder($order_number, array("stock_date" => $value)))
        echo "발송처리일 저장에 실패했습니다.";
    else
        echo "발송처리일 저장에 성공했습니다.";
    exit;
}
elseif($process == "new_order")
{
    $search_url       = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $ord_type         = isset($_POST['new_order_type']) ? $_POST['new_order_type'] : "택배";
    $ord_no           = isset($_POST['new_ord_number']) ? $_POST['new_ord_number'] : "";
    $log_c_no         = isset($_POST['new_log_c_no']) && !empty($_POST['new_log_c_no']) ? $_POST['new_log_c_no'] : "2809";
    $ord_date         = isset($_POST['new_ord_date']) ? $_POST['new_ord_date'] : "";
    $pay_date         = isset($_POST['new_pay_date']) ? $_POST['new_pay_date'] : "";
    $stock_date       = isset($_POST['new_stock_date']) ? $_POST['new_stock_date'] : "";
    $recipient        = isset($_POST['new_recipient']) ? $_POST['new_recipient'] : "";
    $recipient_hp     = isset($_POST['new_recipient_hp']) ? $_POST['new_recipient_hp'] : "";
    $zip_code         = isset($_POST['new_zipcode']) ? $_POST['new_zipcode'] : "";
    $recipient_addr1  = isset($_POST['new_recipient_addr1']) ? $_POST['new_recipient_addr1'] : "";
    $recipient_addr2  = isset($_POST['new_recipient_addr2']) ? $_POST['new_recipient_addr2'] : "";
    $recipient_addr   = $recipient_addr1." ".$recipient_addr2;
    $delivery_memo    = isset($_POST['new_delivery_memo']) ? $_POST['new_delivery_memo'] : "";
    $notice           = isset($_POST['new_notice']) ? $_POST['new_notice'] : "";
    $dp_c_no          = isset($_POST['new_dp_c_no']) ? $_POST['new_dp_c_no'] : "";
    $dp_c_name        = "";
    $manager_memo     = isset($_POST['new_manager_memo']) ? $_POST['new_manager_memo'] : "";
    $regdate          = date('Y-m-d H:i:s');
    $prd_no_list      = isset($_POST['new_prd_no']) ? $_POST['new_prd_no'] : "";
    $quantity_list    = isset($_POST['new_quantity']) ? $_POST['new_quantity'] : "";
    $prd_data_list    = [];
    $result           = false;
    $order_item       = $cms_model->getOrderItem($ord_no);

    if(empty($order_item)){
        exit("<script>alert('해당 주문번호는 존재하지 않습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }

    if($dp_c_no){
        $dp_c_name = $company_model->getCompanyName($dp_c_no);
    }

    $new_ins_list   = [];
    $upd_data       = [];
    $ins_data       = array(
        "order_number"          => $ord_no,
        "parent_order_number"   => $order_item['parent_order_number'],
        "order_type"            => $ord_type,
        "log_c_no"              => $log_c_no,
        "order_date"            => $ord_date,
        "payment_date"          => $pay_date,
        "stock_date"            => $stock_date,
        "delivery_memo"         => addslashes($delivery_memo),
        "zip_code"              => $zip_code,
        "recipient"             => $recipient,
        "recipient_addr"        => addslashes($recipient_addr),
        "recipient_hp"          => $recipient_hp,
        "notice"                => addslashes($notice),
        "dp_c_no"               => $dp_c_no,
        "dp_c_name"             => $dp_c_name,
        "manager_memo"          => addslashes($manager_memo),
        "regdate"               => $regdate,
        "write_date"            => $regdate,
        "task_run_s_no"         => $session_s_no,
        "task_run_team"         => $session_team,
        "task_run_regdate"      => $regdate,
    );

    # 상품리스트
    if(!empty($prd_no_list))
    {
        foreach($prd_no_list as $prd_idx => $prd_no)
        {
            if(!empty($prd_no)){
                $prd_data_list[$prd_no] = isset($quantity_list[$prd_idx]) ? $quantity_list[$prd_idx] : 1;
            }
        }

        if(!empty($prd_data_list))
        {
            foreach($prd_data_list as $p_no => $quantity)
            {
                $prd_sql        = "SELECT prd_cms.manager, (SELECT s.team FROM staff s WHERE s.s_no = prd_cms.manager) as team, prd_cms.c_no, (SELECT c.c_name FROM company c WHERE c.c_no = prd_cms.c_no) as c_name FROM product_cms prd_cms WHERE prd_cms.prd_no = '{$p_no}' ORDER BY prd_cms.prd_no DESC LIMIT 1";
                $prd_query      = mysqli_query($my_db, $prd_sql);
                $prd_result     = mysqli_fetch_assoc($prd_query);
                $prd_ins_data   = $ins_data;

                $prd_ins_data['s_no']           = isset($prd_result['manager']) ? $prd_result['manager'] : 0;
                $prd_ins_data['team']           = isset($prd_result['team']) ? $prd_result['team'] : 0;
                $prd_ins_data['task_req_s_no']  = isset($prd_result['manager']) ? $prd_result['manager'] : 0;
                $prd_ins_data['task_req_team']  = isset($prd_result['team']) ? $prd_result['team'] : 0;
                $prd_ins_data['c_no']           = isset($prd_result['c_no']) ? $prd_result['c_no'] : 0;
                $prd_ins_data['c_name']         = isset($prd_result['c_name']) ? $prd_result['c_name'] : "";
                $prd_ins_data['prd_no']         = $p_no;
                $prd_ins_data['quantity']       = $quantity;

                $new_ins_list[] = $prd_ins_data;
                $upd_data       = array(
                    "recipient"         => $recipient,
                    "recipient_addr"    => $recipient_addr,
                    "recipient_hp"      => $recipient_hp,
                    "zip_code"          => $zip_code,
                );
            }
        }
    }

    if($new_ins_list){
        if(!$cms_model->multiInsert($new_ins_list)){
            exit("<script>alert('등록 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
        }else{
            if(!empty($upd_data)){
                $cms_model->updateOrder($ord_no, $upd_data);
            }
            $result = true;
        }
    }

    if($result){
        exit("<script>alert('등록 성공했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('등록 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}
elseif($process == "upd_order")
{
    $search_url       = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $w_no             = isset($_POST['new_w_no']) ? $_POST['new_w_no'] : "";
    $ord_no           = isset($_POST['new_ord_number']) ? $_POST['new_ord_number'] : "";
    $recipient        = isset($_POST['new_recipient']) ? $_POST['new_recipient'] : "";
    $recipient_hp     = isset($_POST['new_recipient_hp']) ? $_POST['new_recipient_hp'] : "";
    $zip_code         = isset($_POST['new_zipcode']) ? $_POST['new_zipcode'] : "";
    $recipient_addr1  = isset($_POST['new_recipient_addr1']) ? trim($_POST['new_recipient_addr1']) : "";
    $recipient_addr2  = isset($_POST['new_recipient_addr2']) ? trim($_POST['new_recipient_addr2']) : "";
    $recipient_addr   = trim($recipient_addr1." ".$recipient_addr2);
    $notice           = isset($_POST['new_notice']) ? addslashes(trim($_POST['new_notice'])) : "";
    $manager_memo     = isset($_POST['new_manager_memo']) ? addslashes(trim($_POST['new_manager_memo'])) : "";
    $prd_no_list      = isset($_POST['new_prd_no']) ? $_POST['new_prd_no'] : "";
    $quantity_list    = isset($_POST['new_quantity']) ? $_POST['new_quantity'] : "";
    $result           = false;
    $prd_list         = [];

    if(empty($w_no) || empty($ord_no)){
        exit("<script>alert('해당 주문은 존재하지 않습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }

    if(empty($prd_no_list)){
        exit("<script>alert('해당 상품이 존재하지 않습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }

    if(!!$w_no && !!$ord_no){
        $ord_sql    = "SELECT count(order_number) as cnt FROM work_cms WHERE order_number = '{$ord_no}'";
        $ord_query  = mysqli_query($my_db, $ord_sql);
        $ord_result = mysqli_fetch_assoc($ord_query);

        if(!isset($ord_result['cnt']) || (isset($ord_result['cnt']) && $ord_result['cnt'] < 1)){
            exit("<script>alert('해당 주문번호는 존재하지 않습니다');location.href='work_list_cms.php?{$search_url}';</script>");
        }
    }

    $order_item     = $cms_model->getItem($w_no);
    $upd_ord_data   = array(
        "recipient"         => $recipient,
        "recipient_hp"      => $recipient_hp,
        "recipient_addr"    => $recipient_addr,
        "zip_code"          => $zip_code,
    );

    $prd_no         = $prd_no_list[0];
    $quantity       = isset($quantity_list[0]) ? $quantity_list[0] : 1;
    $product_model  = ProductCms::Factory();
    $product_item   = $product_model->getWorkCmsItem($prd_no);

    $upd_w_data = array(
        "w_no"          => $w_no,
        "prd_no"        => $prd_no,
        "quantity"      => $quantity,
        "s_no"          => isset($product_item['manager']) ? $product_item['manager'] : 0,
        "team"          => isset($product_item['team']) ? $product_item['team'] : 0,
        "c_no"          => isset($product_item['c_no']) ? $product_item['c_no'] : 0,
        "c_name"        => isset($product_item['c_name']) ? $product_item['c_name'] : "",
        "notice"        => $notice,
        "manager_memo"  => $manager_memo,
    );

    if($cms_model->updateOrder($ord_no, $upd_ord_data) && $cms_model->update($upd_w_data)){
        exit("<script>alert('수정 성공했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('수정 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_delivery_state")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $stock_date = isset($_POST['ds_stock_date']) ? $_POST['ds_stock_date'] : "";

    if(!$stock_date){
        exit("<script>alert('진행완료 처리에 실패했습니다. 날짜 데이터가 없습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }

    $stock_s_date = date("Y-m-d", strtotime("{$stock_date} -3 days"));
    $upd_sql      = "UPDATE work_cms set delivery_state = '4' WHERE (stock_date BETWEEN '{$stock_s_date}' AND '{$stock_date}') AND delivery_state ='8' AND log_c_no='{$with_log_c_no}'";

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('진행완료 처리에 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }else{
        $result = true;
    }

    if($result){
        exit("<script>alert('진행완료 처리했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('진행완료 처리에 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}
elseif($process == "add_total_visit")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $chk_cur_date   = date('Y-m-d');
    $regdate        = date('Y-m-d H:i:s');
    $visit_model    = WorkCms::Factory();
    $visit_model->setMainInit("work_cms_visit", "v_no");

    $visit_prd_option   = getVisitTotalOption();
    $visit_prd_list     = implode(",", $visit_prd_option);

    $costzero_cms_sql   = "SELECT order_number, prd_no, recipient, recipient_hp, recipient_addr, dp_c_no, dp_c_name, (SELECT count(sub.v_no) FROM work_cms_visit sub WHERE sub.order_number=w.order_number AND sub.visit_state IN('3','4')) as visit_cnt FROM work_cms w WHERE stock_date='{$chk_cur_date}' AND delivery_state='4' AND prd_no IN({$visit_prd_list})";
    $costzero_cms_query = mysqli_query($my_db, $costzero_cms_sql);
    $visit_ins_list     = [];
    while($costzero_cms = mysqli_fetch_assoc($costzero_cms_query))
    {
        if($costzero_cms['visit_cnt'] > 0){
            exit("<script>alert('방문지원 리스트에 이미 존재합니다');location.href='work_list_cms.php?{$search_url}';</script>");
        }

        $dp_c_name      = $costzero_cms['dp_c_name'];
        $addr_exp_list  = explode(' ', $costzero_cms['recipient_addr']);
        $addr1          = "";
        $addr2          = "";
        $idx            = 0;
        foreach($addr_exp_list as $addr_exp)
        {
            if($idx < 2){
                $addr1 .= !empty($addr1) ? " ".$addr_exp : $addr_exp;
            }else{
                $addr2 .= !empty($addr2) ? " ".$addr_exp : $addr_exp;
            }
            $idx++;
        }

        $prd_code = "코스트제로";
        $costzero_self = array('1019','1116','1262','1263','1439','1580','1594','1596');
        if(in_array($costzero_cms['prd_no'], $costzero_self)){
            $prd_code = "코스트제로 (자가설치)";
        }

        $recipient_hp_tmp = str_replace('-', '', $costzero_cms['recipient_hp']);
        $recipient_start_hp  = substr($recipient_hp_tmp,0,4);
        $recipient_middle_hp = "";
        $recipient_end_hp    = "";
        if($recipient_start_hp == '0502'){
            $recipient_middle_hp = substr($recipient_hp_tmp, 4, 4);
            $recipient_end_hp    = substr($recipient_hp_tmp, 8, 4);
        }else{
            $recipient_start_hp  = substr($recipient_hp_tmp,0,3);
            $recipient_middle_hp = substr($recipient_hp_tmp, 3, 4);
            $recipient_end_hp    = substr($recipient_hp_tmp, 7, 4);
        }
        $recipient_hp = $recipient_start_hp."-".$recipient_middle_hp."-".$recipient_end_hp;

        $visit_ins_list[] = array(
            "visit_state"   => "3",
            "customer"      => $costzero_cms['recipient'],
            "hp"            => $recipient_hp,
            "addr1"         => addslashes($addr1),
            "addr2"         => addslashes($addr2),
            "prd_code"      => $prd_code,
            "dp_c_name"     => $dp_c_name,
            "order_number"  => $costzero_cms['order_number'],
            "regdate"       => $regdate,
        );
    }

    if(!empty($visit_ins_list))
    {
        if($visit_model->multiInsert($visit_ins_list)){
            exit("<script>alert('방문지원 리스트에 추가했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('방문지원 리스트에 추가에 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('방문지원 주문내역이 없습니다. 방문지원 리스트에 추가에 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}
elseif($process == "add_unit_visit")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $w_no           = isset($_POST['w_no']) ? $_POST['w_no'] : "";
    $regdate        = date('Y-m-d H:i:s');
    $visit_model    = WorkCms::Factory();
    $visit_model->setMainInit("work_cms_visit", "v_no");

    if(empty($w_no)){
        exit("<script>alert('해당 주문은 존재하지 않습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }

    $costzero_cms = $cms_model->getItem($w_no);

    if(empty($costzero_cms['w_no'])){
        exit("<script>alert('해당 주문은 존재하지 않습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }

    $dp_c_name      = $costzero_cms['dp_c_name'];
    $addr_exp_list  = explode(' ', $costzero_cms['recipient_addr']);
    $addr1          = "";
    $addr2          = "";
    $idx            = 0;
    foreach($addr_exp_list as $addr_exp)
    {
        if($idx < 2){
            $addr1 .= !empty($addr1) ? " ".$addr_exp : $addr_exp;
        }else{
            $addr2 .= !empty($addr2) ? " ".$addr_exp : $addr_exp;
        }
        $idx++;
    }

    $prd_code       = "코스트제로";
    $costzero_self  = getVisitSelfOption();
    if(in_array($costzero_cms['prd_no'], $costzero_self)){
        $prd_code = "코스트제로 (자가설치)";
    }

    $recipient_hp_tmp = str_replace('-', '', $costzero_cms['recipient_hp']);
    $recipient_start_hp  = substr($recipient_hp_tmp,0,4);
    $recipient_middle_hp = "";
    $recipient_end_hp    = "";
    if($recipient_start_hp == '0502'){
        $recipient_middle_hp = substr($recipient_hp_tmp, 4, 4);
        $recipient_end_hp    = substr($recipient_hp_tmp, 8, 4);
    }else{
        $recipient_start_hp  = substr($recipient_hp_tmp,0,3);
        $recipient_middle_hp = substr($recipient_hp_tmp, 3, 4);
        $recipient_end_hp    = substr($recipient_hp_tmp, 7, 4);
    }
    $recipient_hp = $recipient_start_hp."-".$recipient_middle_hp."-".$recipient_end_hp;

    $visit_ins_data = array(
        "visit_state"   => "3",
        "customer"      => $costzero_cms['recipient'],
        "hp"            => $recipient_hp,
        "addr1"         => addslashes($addr1),
        "addr2"         => addslashes($addr2),
        "prd_code"      => $prd_code,
        "dp_c_name"     => $dp_c_name,
        "order_number"  => $costzero_cms['order_number'],
        "regdate"       => $regdate,
    );

    if(!empty($visit_ins_data))
    {
        if($visit_model->insert($visit_ins_data)){
            exit("<script>alert('방문지원 리스트에 추가했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('방문지원 리스트에 추가에 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('방문지원 리스트에 추가에 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_inspection_state")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $cur_date   = date("Y-m-d");

    $upd_sql    = "UPDATE work_cms as w set delivery_state = '1' WHERE delivery_state ='10' AND stock_date <= '{$cur_date}' AND log_c_no='2809' AND ((SELECT COUNT(dp_no) AS dp_count FROM `work` as sub WHERE sub.prd_no='260' AND sub.linked_table='work_cms' AND sub.linked_shop_no=w.order_number AND (SELECT dp.dp_state FROM deposit dp WHERE dp.dp_no=sub.dp_no)='2') > 0 OR (SELECT count(w_no) FROM `work` as sub WHERE sub.prd_no='260' AND sub.linked_table='work_cms' AND sub.linked_shop_no=w.order_number) = 0)";

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('진행요청 처리에 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('진행요청 처리했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_inspection_state_global")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $global_log_c_no    = isset($_POST['global_log_c_no']) ? $_POST['global_log_c_no'] : "";
    $global_chk_state   = isset($_POST['global_chk_state']) ? $_POST['global_chk_state'] : "10";
    $global_chg_state   = ($global_chk_state != "10") ? "4" : "1";
    $global_chk_msg     = ($global_chk_state != "10") ? "진행완료" : "진행요청";
    $cur_date           = date("Y-m-d");

    $upd_sql = "UPDATE work_cms as w SET delivery_state = '{$global_chg_state}' WHERE delivery_state ='{$global_chk_state}' AND stock_date <= '{$cur_date}' AND log_c_no='{$global_log_c_no}' AND ((SELECT COUNT(dp_no) AS dp_count FROM `work` as sub WHERE sub.prd_no='260' AND sub.linked_table='work_cms' AND sub.linked_shop_no=w.order_number AND (SELECT dp.dp_state FROM deposit dp WHERE dp.dp_no=sub.dp_no)='2') > 0 OR (SELECT count(w_no) FROM `work` as sub WHERE sub.prd_no='260' AND sub.linked_table='work_cms' AND sub.linked_shop_no=w.order_number) = 0)";

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('{$global_chk_msg} 처리에 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('{$global_chk_msg} 처리했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}
elseif($process == "del_order")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $w_no       = isset($_POST['w_no']) ? $_POST['w_no'] : "";
    $result     = false;

    if(!empty($w_no))
    {
        $order_item =  $cms_model->getItem($w_no);

        if($cms_model->delete($w_no))
        {
            $cms_model->setMainInit("work_cms_delivery", "no");
            $cms_model->deleteToArray(array('prd_no' => $order_item['prd_no'], 'order_number' => $order_item['order_number']));
            $result = true;
        }
    }

    if($result){
        exit("<script>alert('삭제했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제에 실패했습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}
elseif($process == "del_personal_info") # 개인정보 삭제
{
    $search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $prev_date  = date("Y-m-d", strtotime("-5 years"))." 00:00:00";
    $affect_cnt	= 0;

    $del_sql = "UPDATE work_cms SET recipient_hp=NULL, recipient_hp2=NULL, zip_code=NULL, recipient_addr=SUBSTR(recipient_addr,1,14) WHERE regdate < '{$prev_date}'";

    if(mysqli_query($my_db, $del_sql))
    {
        $affect_cnt += mysqli_affected_rows($my_db);

        $work_model     = Work::Factory();
        $wplanet_c_no  	= 1580;
        $company_item  	= $company_model->getItem($wplanet_c_no);
        $task_run 		= "[커머스 주문정보 개인정보 삭제]\r\n주문일로 부터 5년 이후의 연락처,우편번호,수령지주소(시/도에 해당하는 앞 10글자 제외) 삭제\r\n처리건 : ".number_format($affect_cnt)." 건 삭제완료";
        $regdate 		= date("Y-m-d H:i:s");
        $work_data 		= array(
            "work_state" 		=> "6",
            "k_name_code"		=> "02436",
            "c_no"				=> $company_item['c_no'],
            "c_name"            => $company_item["c_name"],
            "s_no"				=> $company_item['s_no'],
            "team"				=> $company_item['team'],
            "prd_no"            => "287",
            "task_req_s_no"     => $session_s_no,
            "task_req_team"     => $session_team,
            "quantity"			=> 1,
            "regdate"           => $regdate,
            "task_run"          => addslashes(trim($task_run)),
            "task_run_s_no"     => $session_s_no,
            "task_run_team"     => $session_team,
            "task_run_regdate"	=> $regdate,
        );
        $work_model->insert($work_data);

        exit ("<script>alert('커머스 주문정보, 개인정보를 삭제 하였습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }else{
        exit ("<script>alert('커머스 주문정보, 개인정보 삭제에 실패하였습니다');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}
else
{
    # Navigation & My Quick
    $nav_prd_no  = "";
    $is_my_quick = "";
    $nav_title   = "커머스 택배 리스트";
    if($session_staff_state == "1"){
        $nav_prd_no  = "26";
        $quick_model = MyQuick::Factory();
        $is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
    }

    $smarty->assign("is_my_quick", $is_my_quick);
    $smarty->assign("nav_title", $nav_title);
    $smarty->assign("nav_prd_no", $nav_prd_no);

    # 검색쿼리
    $sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
    $sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
    $sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
    $sch_get	= isset($_GET['sch'])?$_GET['sch']:"";

    $smarty->assign("sch_prd_g1", $sch_prd_g1);
    $smarty->assign("sch_prd_g2", $sch_prd_g2);
    $smarty->assign("sch_prd", $sch_prd);

    if(!empty($sch_get)) {
        $smarty->assign("sch", $sch_get);
    }

    $kind_model             = Kind::Factory();
    $product_model 	        = ProductCms::Factory();
    $kind_code		        = "product_cms";

    # 상품 검색
    $log_company_list       = $company_model->getLogisticsList();
    $kind_chart_data_list 	= $kind_model->getKindChartData($kind_code);
    $prd_group_list 		= $kind_chart_data_list['kind_group_list'];
    $prd_group_name_list	= $kind_chart_data_list['kind_group_name'];
    $prd_group_code_list	= $kind_chart_data_list['kind_group_code'];
    $prd_group_code 		= implode(",", $prd_group_code_list);
    $prd_data_list			= $product_model->getPrdGroupChartData($prd_group_code);
    $prd_total_list			= $prd_data_list['prd_total'];
    $prd_g1_list = $prd_g2_list = $prd_g3_list = [];

    foreach($prd_group_list as $key => $prd_data)
    {
        if(!$key){
            $prd_g1_list = $prd_data;
        }else{
            $prd_g2_list[$key] = $prd_data;
        }
    }

    $prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
    $sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

    $smarty->assign("prd_g1_list", $prd_g1_list);
    $smarty->assign("prd_g2_list", $prd_g2_list);
    $smarty->assign("sch_prd_list", $sch_prd_list);

    $add_where = "1=1";
    if (!empty($sch_prd) && $sch_prd != "0") { // 상품
        $add_where .= " AND w.prd_no='{$sch_prd}'";
    }else{
        if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
            $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code='{$sch_prd_g2}')";
        }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
            $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
        }
    }

    # 브랜드 검색
    $brand_company_total_list   = $kind_model->getBrandCompanyList();
    $brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
    $brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
    $brand_list                 = $brand_company_total_list['brand_dom_list'];
    $brand_total_list           = $brand_company_total_list['brand_total_list'];
    $brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
    $brand_parent_list          = $brand_company_total_list['brand_parent_list'];
    $sch_brand_g2_list          = [];
    $sch_brand_list             = [];

    # 브랜드 검색
    $sch_brand_g1               = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
    $sch_brand_g2               = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
    $sch_brand                  = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

    # 브랜드 parent 매칭
    if(!empty($sch_brand)) {
        $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
        $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
    }
    elseif(!empty($sch_brand_g2)) {
        $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
    }

    if(!empty($sch_brand)) {
        $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
        $sch_brand_list     = $brand_list[$sch_brand_g2];

        $add_where      .= " AND `w`.c_no = '{$sch_brand}'";
    }
    elseif(!empty($sch_brand_g2)) {
        $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
        $sch_brand_list     = $brand_list[$sch_brand_g2];

        $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    }
    elseif(!empty($sch_brand_g1)) {
        $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

        $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    }
    $smarty->assign("sch_brand_g1", $sch_brand_g1);
    $smarty->assign("sch_brand_g2", $sch_brand_g2);
    $smarty->assign("sch_brand", $sch_brand);
    $smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
    $smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
    $smarty->assign("sch_brand_list", $sch_brand_list);


    # 날짜 검색
    $today_val      = date('Y-m-d');
    $week_val       = date('Y-m-d', strtotime('-1 weeks'));
    $month_val      = date('Y-m-d', strtotime('-1 months'));
    $months_val     = date('Y-m-d', strtotime('-3 months'));
    $year_val       = date('Y-m-d', strtotime('-1 years'));
    $years_val      = date('Y-m-d', strtotime('-2 years'));

    $smarty->assign("today_val", $today_val);
    $smarty->assign("week_val", $week_val);
    $smarty->assign("month_val", $month_val);
    $smarty->assign("months_val", $months_val);
    $smarty->assign("year_val", $year_val);
    $smarty->assign("years_val", $years_val);

    //검색 처리
	$sch_w_no 			    = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
    $sch_reg_s_date         = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : $week_val;
    $sch_reg_e_date         = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : $today_val;
    $sch_reg_date_type      = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "week";
	$sch_stock_date 	    = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : "";
	$sch_delivery_state     = isset($_GET['sch_delivery_state']) ? $_GET['sch_delivery_state'] : "";
	$sch_order_s_date 	    = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : "";
	$sch_order_e_date 	    = isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : "";
	$sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
	$sch_recipient 		    = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
	$sch_recipient_hp 	    = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
	$sch_recipient_addr 	= isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
	$sch_delivery_no 	    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
	$sch_delivery_no_null   = isset($_GET['sch_delivery_no_null']) ? $_GET['sch_delivery_no_null'] : "";
	$sch_dp_c_no 		    = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
	$sch_prd_name 		    = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
    $sch_notice             = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
    $sch_task_req           = isset($_GET['sch_task_req']) ? $_GET['sch_task_req'] : "";
    $sch_step               = isset($_GET['sch_step']) ? $_GET['sch_step'] : "";
    $sch_step_data          = isset($_GET['sch_step_data']) ? $_GET['sch_step_data'] : "";
    $sch_delivery_price     = isset($_GET['sch_delivery_price']) ? $_GET['sch_delivery_price'] : "";
    $sch_unit_price         = isset($_GET['sch_unit_price']) ? $_GET['sch_unit_price'] : "";
    $sch_coupon_price       = isset($_GET['sch_coupon_price']) ? $_GET['sch_coupon_price'] : "";
    $sch_coupon_type        = isset($_GET['sch_coupon_type']) ? $_GET['sch_coupon_type'] : "";
    $sch_subscription       = isset($_GET['sch_subscription']) ? $_GET['sch_subscription'] : "";
    $sch_logistics          = isset($_GET['sch_logistics']) ? $_GET['sch_logistics'] : "";
    $sch_ord_bundle 	    = isset($_GET['sch_ord_bundle']) ? $_GET['sch_ord_bundle'] : "";
    $sch_wise_dp 	        = isset($_GET['sch_wise_dp']) ? $_GET['sch_wise_dp'] : "";
    $sch_order_type	        = isset($_GET['sch_order_type']) ? $_GET['sch_order_type'] : "";
    $sch_log_c_no	        = isset($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "";
    $sch_run_s_name	        = isset($_GET['sch_run_s_name']) ? $_GET['sch_run_s_name'] : "";
    $sch_option_sku	        = isset($_GET['sch_option_sku']) ? $_GET['sch_option_sku'] : "";
    $sch_file_no	        = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";

    if(empty(getenv("QUERY_STRING"))){
        $sch_ord_bundle = 1;
    }

	if(!empty($sch_w_no)){
        $add_where .= " AND w.w_no='{$sch_w_no}'";
        $smarty->assign('sch_w_no', $sch_w_no);
	}

    if(!empty($sch_reg_s_date) || !empty($sch_reg_e_date))
    {
        if(!empty($sch_reg_s_date)){
            $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
            $add_where .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
            $smarty->assign('sch_reg_s_date', $sch_reg_s_date);
        }

        if(!empty($sch_reg_e_date)){
            $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
            $add_where .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
            $smarty->assign('sch_reg_e_date', $sch_reg_e_date);
        }
    }
    $smarty->assign('sch_reg_date_type', $sch_reg_date_type);

    if(!empty($sch_stock_date)){
        $add_where .= " AND w.stock_date = '{$sch_stock_date}'";
        $smarty->assign('sch_stock_date', $sch_stock_date);
    }

    if(!empty($sch_delivery_state)){
        $add_where .= " AND w.delivery_state = '{$sch_delivery_state}'";
        $smarty->assign('sch_delivery_state', $sch_delivery_state);
    }

    if(!empty($sch_order_s_date) || !empty($sch_order_e_date))
    {
        if(!empty($sch_order_s_date)){
            $sch_ord_s_datetime = $sch_order_s_date." 00:00:00";
            $add_where .= " AND w.order_date >= '{$sch_ord_s_datetime}'";
            $smarty->assign('sch_order_s_date', $sch_order_s_date);
        }

        if(!empty($sch_order_e_date)){
            $sch_ord_e_datetime = $sch_order_e_date." 23:59:59";
            $add_where .= " AND w.order_date <= '{$sch_ord_e_datetime}'";
            $smarty->assign('sch_order_e_date', $sch_order_e_date);
        }
    }

    if(!empty($sch_order_number)){
        $add_where .= " AND w.order_number = '{$sch_order_number}'";
        $smarty->assign('sch_order_number', $sch_order_number);
    }

    if(!empty($sch_recipient)){
        $add_where .= " AND w.recipient like '%{$sch_recipient}%'";
        $smarty->assign('sch_recipient', $sch_recipient);
    }

    if(!empty($sch_recipient_hp)){
        $add_where .= " AND w.recipient_hp like '%{$sch_recipient_hp}%'";
        $smarty->assign('sch_recipient_hp', $sch_recipient_hp);
    }

    if(!empty($sch_recipient_addr)){
        $add_where .= " AND w.recipient_addr like '%{$sch_recipient_addr}%'";
        $smarty->assign('sch_recipient_addr', $sch_recipient_addr);
    }

    if(!empty($sch_delivery_no_null))
    {
        $add_where .= " AND (SELECT COUNT(DISTINCT delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) = 0";
        $smarty->assign('sch_delivery_no_null', $sch_delivery_no_null);
    }else{
        if(!empty($sch_delivery_no)){
            $add_where .= " AND w.order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery wcd WHERE wcd.delivery_no='{$sch_delivery_no}')";
            $smarty->assign('sch_delivery_no', $sch_delivery_no);
        }
    }

    if(!empty($sch_wise_dp)){
        $add_where .= " AND w.dp_c_no IN(2280, 5468, 5469, 5475, 5476, 5477, 5478 ,5479, 5480, 5481, 5482, 5939, 6002)";
        $smarty->assign('sch_wise_dp', $sch_wise_dp);
    }else{
        if(!empty($sch_dp_c_no)){
            $add_where .= " AND w.dp_c_no='{$sch_dp_c_no}'";
            $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
        }
    }

    if(!empty($sch_prd_name)){
        $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no from product_cms prd_cms where prd_cms.title like '%{$sch_prd_name}%')";
        $smarty->assign('sch_prd_name', $sch_prd_name);
    }

    if(!empty($sch_notice)){
        $add_where .= " AND w.notice like '%{$sch_notice}%'";
        $smarty->assign('sch_notice', $sch_notice);
    }

    if(!empty($sch_task_req)){
        $add_where .= " AND w.task_req like '%{$sch_task_req}%'";
        $smarty->assign('sch_task_req', $sch_task_req);
    }

    if(!empty($sch_delivery_price))
    {
        if($sch_delivery_price == '1'){
            $add_where .= " AND w.unit_delivery_price > 0";
        }else{
            $add_where .= " AND w.unit_delivery_price = 0";
        }
        $smarty->assign('sch_delivery_price', $sch_delivery_price);
    }

    if(!empty($sch_unit_price))
    {
        if($sch_unit_price == '1'){
            $add_where .= " AND w.unit_price > 0";
        }else{
            $add_where .= " AND w.unit_price = 0";
        }
        $smarty->assign('sch_unit_price', $sch_unit_price);
    }

    if(!empty($sch_coupon_price))
    {
        if($sch_coupon_price == '1'){
            $add_where .= " AND (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) > 0";
        }else{
            $add_where .= " AND (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) IS NULL";
        }
        $smarty->assign('sch_coupon_price', $sch_coupon_price);
    }

    if(!empty($sch_coupon_type))
    {
        if($sch_coupon_type == '1'){
            $add_where .= " AND w.dp_c_no = '1372'";
        }elseif($sch_coupon_type == '2'){
            $add_where .= " AND w.dp_c_no IN(3295,4629,5427,5588)";
        }elseif($sch_coupon_type == '3'){
            $add_where .= " AND w.dp_c_no = '5800'";
        }elseif($sch_coupon_type == '4'){
            $add_where .= " AND w.dp_c_no = '5958'";
        }elseif($sch_coupon_type == '5'){
            $add_where .= " AND w.dp_c_no = '6012'";
        }

        $smarty->assign('sch_coupon_type', $sch_coupon_type);
    }

    if(!empty($sch_subscription)){
        if($sch_subscription == '1'){
            $add_where .= " AND (w.subs_application_times > 0 OR w.subs_progression_times > 0)";
        }else{
            $add_where .= " AND ((w.subs_application_times = 0 OR w.subs_application_times IS NULL) AND (w.subs_progression_times = 0 OR w.subs_progression_times IS NULL))";
        }
        $smarty->assign('sch_subscription', $sch_subscription);
    }

    if(!empty($sch_logistics))
    {
        $logistics_sql    = "SELECT order_number FROM logistics_management WHERE lm_no='{$sch_logistics}'";
        $logistics_query  = mysqli_query($my_db, $logistics_sql);
        $logistics_result = mysqli_fetch_assoc($logistics_query);
        $logistics_list   = [];

        if(isset($logistics_result['order_number']) && !empty($logistics_result['order_number']))
        {
            $logistics_tmp = explode(",", $logistics_result['order_number']);
            foreach($logistics_tmp as $log_ord){
                $logistics_list[] = "'{$log_ord}'";
            }
        }

        if(!empty($logistics_list))
        {
            $logistics_where = implode(",", $logistics_list);
            $add_where .= " AND order_number IN ({$logistics_where})";
            $_GET['ord_page_type'] = 100;
        }
    }

    if(!empty($sch_order_type)){
        $add_where .= " AND w.order_type = '{$sch_order_type}'";
        $smarty->assign('sch_order_type', $sch_order_type);
    }

    if(!empty($sch_log_c_no)){
        $add_where .= " AND w.log_c_no='{$sch_log_c_no}'";
        $smarty->assign('sch_log_c_no', $sch_log_c_no);
    }

    if(!empty($sch_run_s_name)){
        $add_where .= " AND w.task_run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_run_s_name}%')";
        $smarty->assign('sch_run_s_name', $sch_run_s_name);
    }

    if(!empty($sch_option_sku))
    {
        $chk_option_sql   = "SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.`option_no` IN(SELECT pcum.prd_unit FROM product_cms_unit_management pcum WHERE pcum.sku='{$sch_option_sku}') AND pcr.display='1'";
        $chk_option_query = mysqli_query($my_db, $chk_option_sql);
        $chk_option_list  = [];
        while($chk_option_result = mysqli_fetch_assoc($chk_option_query)){
            $chk_option_list[] = "'{$chk_option_result['prd_no']}'";
        }

        if(!empty($chk_option_list)){
            $chk_option_text = implode(",", $chk_option_list);
            $add_where .= " AND w.prd_no IN({$chk_option_text})";
        }
        $smarty->assign('sch_option_sku', $sch_option_sku);
    }

    if(!empty($sch_file_no))
    {
        $chk_file_sql   = "SELECT DISTINCT wcc.shop_ord_no FROM work_cms_coupon wcc WHERE wcc.file_no='{$sch_file_no}'";
        $chk_file_query = mysqli_query($my_db, $chk_file_sql);
        $chk_file_list  = [];
        while($chk_file_result = mysqli_fetch_assoc($chk_file_query)){
            $chk_file_list[] = "'{$chk_file_result['shop_ord_no']}'";
        }

        if(!empty($chk_file_list)){
            $chk_file_text = implode(",", $chk_file_list);
            $add_where .= " AND (w.file_no='{$sch_file_no}' OR w.shop_ord_no IN($chk_file_text))";
        }else{
            $add_where .= " AND w.file_no='{$sch_file_no}'";
        }
        $smarty->assign('sch_file_no', $sch_file_no);
    }

    # 정렬기능
    $ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
    $ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";

    $add_orderby    = "w.w_no DESC, w.prd_no ASC";
    if(!empty($ord_type))
    {
        if($ord_type_by == '1'){
            $orderby_val = "ASC";
        }else{
            $orderby_val = "DESC";
        }

        $add_orderby = "w.{$ord_type} {$orderby_val}, w.prd_no ASC";
    }
    $smarty->assign('ord_type', $ord_type);
    $smarty->assign('ord_type_by', $ord_type_by);

    # Step 날짜 체크
    $step_01_date = $step_02_date = $step_03_date = $step_04_date = $step_date = $step_05_date = $step_09_date = $cur_date = date('Y-m-d');
    $prev_date    = date("Y-m-d", strtotime("-1 days"));
    $step_08_date = date('Y-m');
    if(!empty($sch_step))
    {
        switch($sch_step){
            case "step"  : $step_date    = $sch_stock_date; break;
            case "step01": $step_01_date = $sch_reg_e_date; break;
            case "step02": $step_02_date = $sch_reg_e_date; break;
            case "step03": $step_03_date = $sch_stock_date; break;
            case "step04": $step_04_date = $sch_stock_date; break;
            case "step05": $step_05_date = $sch_stock_date; break;
            case "step07":
                $prev_date  = date("Y-m-d", strtotime("-1 weeks"));
                $add_where .= " AND (w.stock_date BETWEEN '{$prev_date}' AND '{$cur_date}')";

                if($sch_step_data == 'true'){
                    $add_where .= " AND ((SELECT COUNT(dp_no) AS dp_count FROM `work` as sub WHERE sub.prd_no='260' AND sub.work_state='6' AND sub.linked_table='work_cms' AND sub.linked_shop_no=w.order_number AND (SELECT dp.dp_state FROM deposit dp WHERE dp.dp_no=sub.dp_no)='2') > 0 OR (SELECT count(w_no) FROM `work` as sub WHERE sub.prd_no='260' AND sub.work_state='6' AND sub.linked_table='work_cms' AND sub.linked_shop_no=w.order_number) = 0)";
                }
                $smarty->assign("step_07_check", $sch_step_data);
                break;
            case "step08":
                $sch_month_date = isset($_GET['sch_month_date']) ? $_GET['sch_month_date'] : "";
                $step_08_date   = $sch_month_date;
                $step_08_s_date = $sch_month_date."-01 00:00:00";
                $step_08_e_date = $sch_month_date."-31 23:59:59";
                $add_where .= " AND (w.stock_date BETWEEN '{$step_08_s_date}' AND '{$step_08_e_date}') AND dp_c_no IN(5127, 5128, 5134, 5135)";
                break;
            case "step09":
                $step_09_date   = $sch_stock_date;
                $add_where      .= " AND w.order_type NOT IN('택배', '택배(해외)')";
                break;
            case "step10":
                $step_date      = $sch_stock_date;
                $add_where      .= " AND order_number IN(SELECT DISTINCT order_number FROM work_cms WHERE stock_date='{$sch_stock_date}' AND delivery_state='8') AND delivery_state='1'";
                break;
            case "quick_step":
                $add_where .= " AND w.prd_no IN('1020','1117','1264','1265','1440','1581','1595','1597')";
                $smarty->assign('quick_step', '1');
                break;
            case "quick_combined":
                $_GET['ord_page_type'] = 100;
                $add_where .= " AND w.order_number IN(SELECT DISTINCT order_number FROM work_cms WHERE prd_no IN(585,586,1027,1028,1166,1167) AND order_number IN(SELECT main.order_number FROM work_cms main WHERE order_number NOT IN(SELECT sub.order_number FROM work_cms AS sub WHERE sub.stock_date='{$sch_stock_date}' AND sub.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='04022') AND p.display='1') AND manager_memo='합포장수정') AND stock_date='{$sch_stock_date}' AND prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='04022') AND p.display='1' AND p.combined_product > 0) GROUP BY order_number))";
                $smarty->assign('quick_combined', '1');
                break;
            case "quick_yet_order":
                $add_where .= " AND w.order_type NOT IN('택배', '택배(해외)') AND delivery_state NOT IN(4,5)";
                $smarty->assign('quick_yet_order', '1');
                break;
        }
    }

    $smarty->assign('cur_date', $cur_date);
    $smarty->assign('prev_date', $prev_date);
    $smarty->assign('step_date', $step_date);
    $smarty->assign('step_01_date', $step_01_date);
    $smarty->assign('step_02_date', $step_02_date);
    $smarty->assign('step_03_date', $step_03_date);
    $smarty->assign('step_04_date', $step_04_date);
    $smarty->assign('step_05_date', $step_05_date);
    $smarty->assign('step_08_date', $step_08_date);
    $smarty->assign('step_09_date', $step_09_date);

    # 택배리스트 자동정리 Count
    $delivery_state_list        = getDeliveryStateOption();
    $delivery_state_color_list  = getDeliveryStateColorOption();

    $temporary_total_sql        = "SELECT count(t_no) as cnt FROM work_cms_temporary";
    $temporary_total_query      = mysqli_query($my_db, $temporary_total_sql);
    $temporary_total_result     = mysqli_fetch_assoc($temporary_total_query);
    $temporary_total            = isset($temporary_total_result['cnt']) ? $temporary_total_result['cnt'] : 0;

    # 방문지원 기사설치 Count
    $quick_s_date  = $cur_date." 00:00:00";
    $quick_m_date  = $cur_date." 12:00:00";
    $quick_e_date  = $cur_date." 23:59:59";
    $quick_prd_am_sql    = "SELECT count(DISTINCT order_number) as cnt FROM work_cms WHERE (regdate >= '{$quick_s_date}' AND regdate < '{$quick_m_date}') AND prd_no IN('1020','1117','1264','1265','1440','1581','1595','1597')";
    $quick_prd_am_query  = mysqli_query($my_db, $quick_prd_am_sql);
    $quick_prd_am_result = mysqli_fetch_assoc($quick_prd_am_query);

    $quick_prd_pm_sql = "SELECT count(DISTINCT order_number) as cnt FROM work_cms WHERE (regdate >= '{$quick_m_date}' AND regdate <= '{$quick_e_date}') AND prd_no IN('1020','1117','1264','1265','1440','1581','1595','1597')";
    $quick_prd_pm_query  = mysqli_query($my_db, $quick_prd_pm_sql);
    $quick_prd_pm_result = mysqli_fetch_assoc($quick_prd_pm_query);

    $quick_prd_btn_text  = "기사설치 ".date('m/d')." 오전{$quick_prd_am_result['cnt']}건, 오후{$quick_prd_pm_result['cnt']}건";

    $is_quick_prd = false;
    if($session_team == "00244"){
        $is_quick_prd = ($quick_prd_am_result['cnt'] > 0 || $quick_prd_pm_result['cnt'] > 0) ? true : false;
    }

    # 방문지원 상품들
    $visit_product_option = array('1019','1020','1116','1117','1262','1263','1264','1265','1439','1440','1580','1581','1594','1595','1596','1597');

    $smarty->assign('is_quick_prd', $is_quick_prd);
    $smarty->assign('quick_prd_btn_text', $quick_prd_btn_text);
    $smarty->assign('visit_product_option', $visit_product_option);

    $dp_state_option = getDpStateOption();
    $wd_state_option = getWdStateOption();

    # 미완료 보기(방문수령, 배송없음)
    $yet_order_sql      = "SELECT COUNT(DISTINCT order_number) as cnt FROM work_cms AS w WHERE order_type NOT IN('택배', '택배(해외)') AND delivery_state NOT IN(4,5)";
    $yet_order_query    = mysqli_query($my_db,$yet_order_sql);
    $yet_order_result   = mysqli_fetch_assoc($yet_order_query);
    $smarty->assign("yet_order_cnt", $yet_order_result['cnt']);


    # 스타트 투데이 마지막 업로드일
    $last_start_sql     = "SELECT DATE_FORMAT(regdate, '%Y-%m-%d') as last_date FROM work_cms AS w WHERE log_c_no = '5659' ORDER BY regdate DESC LIMIT 1";
    $last_start_query   = mysqli_query($my_db,$last_start_sql);
    $last_start_result  = mysqli_fetch_assoc($last_start_query);
    $smarty->assign("last_start_date", $last_start_result['last_date']);

    # 총 금액 계산
    $total_price_sql    = "SELECT order_number, unit_price, (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price, unit_delivery_price FROM work_cms AS w WHERE {$add_where} ORDER BY order_number ASC";
    $total_price_query  = mysqli_query($my_db, $total_price_sql);
    $total_price_list   = array('total_price' => 0, 'total_delivery' => 0, 'total_coupon' => 0);
    $total_coupon_chk   = [];
    while($total_price_result = mysqli_fetch_assoc($total_price_query))
    {
        $total_price_list['total_price']    += $total_price_result['unit_price'];
        $total_price_list['total_delivery'] += $total_price_result['unit_delivery_price'];
        $total_price_list['total_coupon']   += $total_price_result['coupon_price'];
    }
    $smarty->assign("total_price_list", $total_price_list);

    # Work CMS LIST
    if((!empty($sch_ord_bundle) && $sch_ord_bundle == '1'))
    {
        # 전체 게시물 수
        $work_cms_total_sql		= "SELECT count(order_number) as cnt FROM (SELECT DISTINCT w.order_number FROM work_cms w WHERE {$add_where}) AS cnt";
        $work_cms_total_query	= mysqli_query($my_db, $work_cms_total_sql);
        $work_cms_total_result  = mysqli_fetch_array($work_cms_total_query);
        $work_cms_total         = $work_cms_total_result['cnt'];

        # 페이징처리
        $page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
        $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
        $num 		= $page_type;
        $offset 	= ($pages-1) * $num;
        $pagenum 	= ceil($work_cms_total/$num);

        if ($pages >= $pagenum){$pages = $pagenum;}
        if ($pages <= 0){$pages = 1;}

        $search_url = getenv("QUERY_STRING");
        if(empty($search_url)){
            $search_url = "sch_ord_bundle=1";
        }
        $pagelist	= pagelist($pages, "work_list_cms.php", $pagenum, $search_url);
        $smarty->assign("search_url", $search_url);
        $smarty->assign("total_num", $work_cms_total);
        $smarty->assign("pagelist", $pagelist);
        $smarty->assign("ord_page_type", $page_type);

        # 주문번호 뽑아내기
        $cms_ord_sql    = "SELECT DISTINCT w.order_number FROM work_cms w WHERE {$add_where} AND w.order_number is not null ORDER BY {$add_orderby} LIMIT {$offset},{$num}";
        $cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);
        $order_number_list  = [];
        $final_price_list   = [];
        while($order_number = mysqli_fetch_assoc($cms_ord_query)){
            $order_number_list[] =  "'".$order_number['order_number']."'";
        }
        $order_numbers   = implode(',', $order_number_list);
        $add_where_group = !empty($order_numbers) ? "w.order_number IN({$order_numbers})" : "1!=1";
    }
    else
    {
        # 전체 게시물 수
        $work_cms_total_sql		= "SELECT count(w_no) as cnt FROM (SELECT DISTINCT w_no FROM work_cms w WHERE {$add_where}) AS cnt";
        $work_cms_total_query	= mysqli_query($my_db, $work_cms_total_sql);
        $work_cms_total_result  = mysqli_fetch_array($work_cms_total_query);
        $work_cms_total         = $work_cms_total_result['cnt'];

        # 페이징처리
        $page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
        $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
        $num 		= $page_type;
        $offset 	= ($pages-1) * $num;
        $pagenum 	= ceil($work_cms_total/$num);

        if ($pages >= $pagenum){$pages = $pagenum;}
        if ($pages <= 0){$pages = 1;}

        $search_url = getenv("QUERY_STRING");
        $pagelist	= pagelist($pages, "work_list_cms.php", $pagenum, $search_url);
        $smarty->assign("search_url", $search_url);
        $smarty->assign("total_num", $work_cms_total);
        $smarty->assign("pagelist", $pagelist);
        $smarty->assign("ord_page_type", $page_type);

        # w_no 번호 뽑아내기
        $cms_ord_sql    = "SELECT DISTINCT w.w_no FROM work_cms w WHERE {$add_where} AND w.w_no IS NOT NULL ORDER BY {$add_orderby} LIMIT {$offset}, {$num}";
        $cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);
        $w_no_list      = [];
        while($cms_ord = mysqli_fetch_assoc($cms_ord_query)){
            $w_no_list[] =  "'".$cms_ord['w_no']."'";
        }
        $w_nos           = !empty($w_no_list) ? implode(',', $w_no_list) : '';
        $add_where_group = !empty($w_nos) ? "w.w_no IN({$w_nos})" : "1!=1";
    }

    $work_cms_sql = "
        SELECT
            *,
            DATE_FORMAT(w.regdate, '%Y-%m-%d') as reg_date,
            DATE_FORMAT(w.regdate, '%H:%i') as reg_time,
            DATE_FORMAT(w.order_date, '%Y-%m-%d') as ord_date,
            DATE_FORMAT(w.order_date, '%H:%i') as ord_time,
            (SELECT s.s_name FROM staff s WHERE s.s_no=w.s_no) as s_name,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
            (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
            (SELECT s.s_name FROM staff s WHERE s.s_no=w.task_req_s_no) as task_req_s_name,
            (SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name,
            (SELECT count(sub.v_no) FROM work_cms_visit sub WHERE sub.order_number=w.order_number AND sub.visit_state IN('3','4')) as visit_cnt
        FROM
        (
            SELECT
                w.w_no,
                w.delivery_state,
                w.regdate,
                w.stock_date,
                w.order_type,
                w.order_date,
                w.order_number,
                w.parent_order_number,
                w.recipient,
                w.recipient_hp,
                w.recipient_hp2,
                w.recipient_addr,
                IF(w.zip_code, CONCAT('[',w.zip_code,']'), '') as postcode,
                w.delivery_memo,
                w.s_no,
                w.log_c_no,
                w.c_name,
                w.prd_no,
                w.task_req,
                w.task_req_s_no,
                w.task_run_s_no,
                w.quantity,
                w.unit_price,
                w.dp_c_no,
                w.dp_c_name,
                w.manager_memo,
                w.notice,
                w.subs_application_times,
                w.subs_progression_times,
                w.shop_ord_no,
                w.unit_delivery_price,
                (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon as wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
                w.final_price
            FROM work_cms w
            WHERE {$add_where_group}
        ) as w
        ORDER BY {$add_orderby}
    ";
    $work_cms_query     = mysqli_query($my_db, $work_cms_sql);
    $work_cms_list      = [];
    $deposit_list       = [];
    $delivery_list      = [];
    $delivery_cnt_list  = [];
    $final_price_list   = [];
    $address_list       = [];
    while ($work_array = mysqli_fetch_array($work_cms_query))
    {
        $delivery_state_color   = isset($delivery_state_color_list[$work_array['delivery_state']]) ? $delivery_state_color_list[$work_array['delivery_state']]['color'] : "black";
        $delivery_state_bg      = isset($delivery_state_color_list[$work_array['delivery_state']]) ? $delivery_state_color_list[$work_array['delivery_state']]['bg'] : "white";

        $work_array['delivery_state_color'] = $delivery_state_color;
        $work_array['delivery_state_bg']    = $delivery_state_bg;

        $work_array['log_c_name']           = isset($log_company_list[$work_array["log_c_no"]]) ? $log_company_list[$work_array["log_c_no"]] : "";

        $refund_list = [];
        if(!empty($work_array['shop_ord_no']))
        {
            $refund_sql    = "SELECT w_no, wd_no, wd_price_vat, (SELECT wd.wd_state FROM withdraw wd WHERE wd.wd_no=w.wd_no) as wd_state FROM work w WHERE prd_no='229' AND linked_table='work_cms' AND linked_shop_no='{$work_array['shop_ord_no']}'";
            $refund_query  = mysqli_query($my_db, $refund_sql);
            while($refund = mysqli_fetch_assoc($refund_query))
            {
                $price = isset($refund['wd_price_vat']) && !empty($refund['wd_price_vat']) ? $refund['wd_price_vat']  : 0;
                $state = isset($refund['wd_state']) && !empty($refund['wd_state']) ? "[".$wd_state_option[$refund['wd_state']]."]" : "[대기]";
                $refund_list[$refund['w_no']] = array('price' => $price, 'state' => $state);
            }
        }
        $work_array['refund_list'] = $refund_list;

        if(!isset($deposit_list[$work_array['order_number']]))
        {
            $deposit_sql   = "SELECT w_no, dp_no, dp_price_vat, (SELECT dp.dp_state FROM deposit dp WHERE dp.dp_no=w.dp_no) as dp_state FROM work w WHERE prd_no='260' AND work_state='6' AND linked_table='work_cms' AND linked_shop_no='{$work_array['order_number']}'";
            $deposit_query = mysqli_query($my_db, $deposit_sql);
            while($deposit = mysqli_fetch_assoc($deposit_query))
            {
                $dp_price = isset($deposit['dp_price_vat']) && !empty($deposit['dp_price_vat']) ? $deposit['dp_price_vat']  : 0;
                $dp_state = (empty($deposit['dp_no'])) ? "[입금생성오류]" : (isset($deposit['dp_state']) && !empty($deposit['dp_state']) ? "[".$dp_state_option[$deposit['dp_state']]."]" : "[대기]");
                $deposit_list[$work_array['order_number']][$deposit['dp_no']] = array('price' => $dp_price, 'state' => $dp_state);
            }
        }

        if(!isset($delivery_list[$work_array['order_number']]))
        {
            $delivery_sql    = "SELECT `no`, delivery_no, delivery_type FROM work_cms_delivery WHERE order_number='{$work_array['order_number']}' GROUP BY delivery_no";
            $delivery_query  = mysqli_query($my_db, $delivery_sql);
            while ($delivery = mysqli_fetch_assoc($delivery_query)) {
                if(!isset($delivery_cnt_list[$work_array['order_number']])){
                    $delivery_cnt_list[$work_array['order_number']] = 1;
                }

                if($delivery_cnt_list[$work_array['order_number']] < 10){
                    $delivery_list[$work_array['order_number']][$delivery['no']] = array('delivery_no' => $delivery['delivery_no'], 'delivery_type' => $delivery['delivery_type']);
                }

                $delivery_cnt_list[$work_array['order_number']]++;
            }
        }

        if(!isset($final_price_list[$work_array['order_number']])){
            $final_price_list[$work_array['order_number']] = $work_array['final_price'];
        }elseif($final_price_list[$work_array['order_number']] <= 0 && $work_array['final_price'] > 0){
            $final_price_list[$work_array['order_number']] = $work_array['final_price'];
        }

        $unit_sql   = "SELECT (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='{$with_log_c_no}') as sku, pcr.quantity as qty FROM product_cms_relation as pcr WHERE pcr.prd_no ='{$work_array['prd_no']}' AND pcr.display='1'";
        $unit_query = mysqli_query($my_db, $unit_sql);
        $unit_list  = [];
        while($unit_result = mysqli_fetch_assoc($unit_query)){
            $unit_result['qty'] = $work_array['quantity']*$unit_result['qty'];
            $unit_list[] = $unit_result;
        }
        $work_array["unit_list"] = $unit_list;

        if(isset($work_array['recipient_hp']) && !empty($work_array['recipient_hp'])){
            $f_hp  = substr($work_array['recipient_hp'],0,4);
            $e_hp  = substr($work_array['recipient_hp'],7,15);
            $work_array['recipient_sc_hp'] = $f_hp."***".$e_hp;
        }

        if(isset($work_array['recipient_hp2']) && !empty($work_array['recipient_hp2'])){
            $f_hp2  = substr($work_array['recipient_hp2'],0,4);
            $e_hp2  = substr($work_array['recipient_hp2'],7,15);
            $work_array['recipient_sc_hp2'] = $f_hp2."***".$e_hp2;
        }

        if(!isset($address_list[$work_array['order_number']]))
        {
            $address_list[$work_array['order_number']] = array(
                'recipient'         => $work_array['recipient'],
                'recipient_hp'      => $work_array['recipient_hp'],
                'recipient_hp2'     => $work_array['recipient_hp2'],
                'recipient_sc_hp'   => $work_array['recipient_sc_hp'],
                'recipient_sc_hp2'  => $work_array['recipient_sc_hp2'],
                'recipient_addr'    => $work_array['recipient_addr'],
                'zip_code'          => $work_array['postcode'],
            );
        }else{
            if(empty($address_list[$work_array['order_number']]['recipient_hp']) && !empty($work_array['recipient_hp']))
            {
                $address_list[$work_array['order_number']]['recipient_hp'] = $work_array['recipient_hp'];
            }

            if(empty($address_list[$work_array['order_number']]['recipient_hp2']) && !empty($work_array['recipient_hp2']))
            {
                $address_list[$work_array['order_number']]['recipient_hp2'] = $work_array['recipient_hp2'];
            }

            if(empty($address_list[$work_array['order_number']]['recipient_sc_hp']) && !empty($work_array['recipient_sc_hp']))
            {
                $address_list[$work_array['order_number']]['recipient_sc_hp'] = $work_array['recipient_sc_hp'];
            }

            if(empty($address_list[$work_array['order_number']]['recipient_sc_hp2']) && !empty($work_array['recipient_sc_hp2']))
            {
                $address_list[$work_array['order_number']]['recipient_sc_hp2'] = $work_array['recipient_sc_hp2'];
            }

            if(empty($address_list[$work_array['order_number']]['recipient_addr']) && !empty($work_array['recipient_addr']))
            {
                $address_list[$work_array['order_number']]['recipient_addr'] = $work_array['recipient_addr'];
            }

            if(empty($address_list[$work_array['order_number']]['zip_code']) && !empty($work_array['zip_code']))
            {
                $address_list[$work_array['order_number']]['zip_code'] = $work_array['zip_code'];
            }
        }

        $work_cms_list[$work_array['order_number']][] = $work_array;
    }

    # 커머스 개인정보 삭제
    $work_last_sql 		= "SELECT DATE_FORMAT(MAX(task_run_regdate), '%Y-%m-%d') as last_date FROM `work` WHERE prd_no='287' AND work_state='6' AND k_name_code='02436'";
    $work_last_query 	= mysqli_query($my_db, $work_last_sql);
    $work_last_result	= mysqli_fetch_assoc($work_last_query);
    $work_last_date		= isset($work_last_result['last_date']) ? $work_last_result['last_date'] : "";
    $smarty->assign("work_last_date", $work_last_date);

    $smarty->assign("sch_ord_bundle", $sch_ord_bundle);
    $smarty->assign("is_freelancer", $is_freelancer);
    $smarty->assign("is_with_editable", $is_with_editable);
    $smarty->assign("is_csis_editable", $is_csis_editable);
    $smarty->assign("is_cms_staff", $is_cms_staff);
    $smarty->assign('sch_dp_company_option', $company_model->getDpDisplayList());
    $smarty->assign('dp_company_option', $company_model->getDpList());
    $smarty->assign('new_ord_date', date('Y-m-d'));
    $smarty->assign('temporary_total', number_format($temporary_total));
    $smarty->assign('delivery_state_list', $delivery_state_list);
    $smarty->assign('delivery_state_color_list', $delivery_state_color_list);
    $smarty->assign('page_type_option', getPageTypeOption(4));
    $smarty->assign('is_exist_option', getIsExistOption());
    $smarty->assign('order_type_option', getOrderTypeOption());
    $smarty->assign('coupon_type_option', getCouponTypeOption());
    $smarty->assign('logistics_company_list', $log_company_list);
    $smarty->assign("address_list", $address_list);
    $smarty->assign("delivery_list", $delivery_list);
    $smarty->assign("delivery_cnt_list", $delivery_cnt_list);
    $smarty->assign("deposit_list", $deposit_list);
    $smarty->assign("final_price_list", $final_price_list);
    $smarty->assign("work_cms_list", $work_cms_list);

    $smarty->display('work_list_cms.html');
}
?>
