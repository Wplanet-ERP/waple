<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_price.php');
require('inc/helper/product_cms.php');
require('inc/helper/product_cms_stock.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/Team.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');

# 구성품 모델 Init
$unit_model 		= ProductCmsUnit::Factory();
$unit_manage_model 	= ProductCmsUnit::Factory();
$unit_manage_model->setMainInit("product_cms_unit_management", "no");

# 프로세스 처리
$process = (isset($_POST['process'])) ? $_POST['process'] : "";
if ($process == "delete")
{
	$no  	 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	if($unit_model->update(array("no" => $no, "display" => "2"))) {
		exit("<script>alert('삭제(Display OFF) 하였습니다');location.href='product_cms_unit.php?{$search_url}';</script>");
	} else {
		exit("<script>alert('삭제(Display OFF)에 실패 하였습니다');location.href='product_cms_unit.php?{$search_url}';</script>");
	}
}
elseif($process == "add_record")
{
	$search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
	$ins_data 	= array(
		"brand"				=> $_POST['f_brand_new'],
		"type"				=> $_POST['f_type_new'],
		"sup_c_no"			=> $_POST['f_sup_c_no_new'],
		"ord_s_no"			=> $_POST['f_ord_s_no_new'],
		"priority"			=> $_POST['f_priority_new'],
		"option_name"		=> trim(addslashes($_POST['f_option_name_new'])),
		"chk_option_name"	=> trim(addslashes($_POST['f_option_name_new'])),
		"wise_sku"			=> trim(addslashes($_POST['f_wise_sku_new'])),
		"display"			=> !empty($_POST['f_display_new']) ? $_POST['f_display_new'] : 1,
	);

	if(!empty($_POST['f_ord_sub_s_no_new'])){
		$ins_data["ord_sub_s_no"] = $_POST['f_ord_sub_s_no_new'];
	}

	if(!empty($_POST['f_ord_third_s_no_new'])){
		$ins_data["ord_third_s_no"] = $_POST['f_ord_third_s_no_new'];
	}

	if(!empty($_POST['f_notice_new'])){
		$ins_data["notice"] = trim(addslashes($_POST['f_notice_new']));
	}

	$f_sup_price_new = 0;
	$f_sup_price_vat_new = 0;

	if($_POST['f_license_type_new'] == '4'){
		if(!empty($_POST['f_sup_price_usd_new'])){
			$f_sup_price_new = str_replace(",","",trim($_POST['f_sup_price_usd_new'])); // 컴마 제거하기
			$ins_data['sup_price'] 		= $f_sup_price_new;
			$ins_data['sup_price_vat']	= $f_sup_price_new;
		}
	}
	else
	{
		if(!empty($_POST['f_sup_price_new'])){
			$f_sup_price_new = str_replace(",","",trim($_POST['f_sup_price_new'])); // 컴마 제거하기
			$ins_data['sup_price'] 		= $f_sup_price_new;
		}

		if(!empty($_POST['f_sup_price_vat_new'])){
			$f_sup_price_vat_new = str_replace(",","",trim($_POST['f_sup_price_vat_new'])); // 컴마 제거하기
			$ins_data['sup_price_vat']	= $f_sup_price_vat_new;
		}
	}

	if(!empty($_POST['f_expiration_new'])){
		$ins_data['expiration']	= trim(addslashes($_POST['f_expiration_new']));
	}

	if(!empty($_POST['f_description_new'])){
		$ins_data['description'] = trim(addslashes($_POST['f_description_new']));
	}

	if(!empty($_POST['f_is_control_new'])){
		$ins_data['is_control']	= $_POST['f_is_control_new'];
	}

	if(!empty($_POST['f_bad_reason_new'])){
		$ins_data['bad_reason']	= $_POST['f_bad_reason_new'];
	}

	if($unit_model->insert($ins_data))
	{
		$new_prd_unit = $unit_model->getInsertId();
		$unit_model->update(array("no" => $new_prd_unit, "group_no" => $new_prd_unit));

		if($_POST['f_is_control'] == '1'){
			$ins_control_sql = "INSERT INTO product_cms_unit_cost_control SET prd_unit='{$new_prd_unit}', sup_c_no='{$_POST['f_sup_c_no_new']}', sup_price='{$f_sup_price_new}', sup_price_vat='{$f_sup_price_vat_new}', cost_date=now(), regdate=now()";
			mysqli_query($my_db, $ins_control_sql);
		}

		# SKU 등록
		$f_log_company 	= (isset($_POST['f_log_company_new'])) ? $_POST['f_log_company_new'] : "";
		$f_warehouse 	= (isset($_POST['f_warehouse_new'])) ? $_POST['f_warehouse_new'] : "";
		$f_sku 			= (isset($_POST['f_sku_new'])) ? $_POST['f_sku_new'] : "";
		$f_qty_alert	= (isset($_POST['f_qty_alert_new'])) ? $_POST['f_qty_alert_new'] : "";
		$f_qty_hp		= (isset($_POST['f_qty_hp_new'])) ? $_POST['f_qty_hp_new'] : "";
		$multi_ins_data = [];
		foreach($f_log_company as $key => $log_c_no)
		{
			if($unit_manage_model->checkLogCompanySku($log_c_no, $f_sku[$key]))
			{
				$ins_data = array(
					"prd_unit"	=> $new_prd_unit,
					"log_c_no"	=> $log_c_no,
					"sku"		=> $f_sku[$key],
					"warehouse"	=> $f_warehouse[$key]
				);

				if(!empty($f_qty_alert[$key])){
					$ins_data['qty_alert'] = $f_qty_alert[$key];
				}

				if(!empty($f_qty_hp[$key])){
					$ins_data['qty_hp'] = $f_qty_hp[$key];
				}

				$multi_ins_data[] = $ins_data;
			}
		}

		if(!empty($multi_ins_data)){
			$unit_manage_model->multiInsert($multi_ins_data);
		}

        exit("<script>alert('발주 품목을 추가 하였습니다');location.href='product_cms_unit.php?{$search_url}';</script>");
	} else {
        exit("<script>alert('발주 품목 추가에 실패 하였습니다');history.back();</script>");
	}
}
elseif($process == "modify_unit_sku")
{
	$no				= (isset($_POST['no'])) ? $_POST['no'] : "";
	$f_log_company 	= (isset($_POST['f_log_company_'.$no])) ? $_POST['f_log_company_'.$no] : "";
	$f_warehouse 	= (isset($_POST['f_warehouse_'.$no])) ? $_POST['f_warehouse_'.$no] : "";
	$f_sku 			= (isset($_POST['f_sku'])) ? $_POST['f_sku'] : "";
	$f_qty_alert	= (isset($_POST['f_qty_alert'])) ? $_POST['f_qty_alert'] : "";
	$f_qty_hp		= (isset($_POST['f_qty_hp'])) ? $_POST['f_qty_hp'] : "";
	$search_url		= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$multi_ins_data = [];
	foreach($f_log_company as $key => $log_c_no)
	{
		if($unit_manage_model->checkLogCompanySku($log_c_no, $f_sku[$key]))
		{
			$ins_data = array(
				"prd_unit"	=> $no,
				"log_c_no"	=> $log_c_no,
				"sku"		=> $f_sku[$key],
				"warehouse"	=> $f_warehouse[$key]
			);

			if(!empty($f_qty_alert[$key])){
				$ins_data['qty_alert'] = $f_qty_alert[$key];
			}

			if(!empty($f_qty_hp[$key])){
				$ins_data['qty_hp'] = $f_qty_hp[$key];
			}

			$multi_ins_data[] = $ins_data;
		}else{
			exit("<script>alert('이미 등록된 SKU({$f_sku[$key]}) 입니다.');history.back();</script>");
		}
	}

	if($unit_manage_model->multiInsert($multi_ins_data)){
		exit("<script>alert('데이터 등록에 성공했습니다.');location.href='product_cms_unit.php?{$search_url}';</script>");
	} else {
		exit("<script>alert('데이터 등록에 실패했습니다.');history.back();</script>");
	}
}
elseif($process == "f_sup_c_no")
{
	$no		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "sup_c_no" => $value))){
		echo "공급업체가 저장 되었습니다.";
	}else{
		echo "공급업체 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_ord_s_no")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

    if ($unit_model->update(array("no" => $no, "ord_s_no" => $value))){
        echo "발주담당자가 저장 되었습니다.";
    }else{
        echo "발주담당자 저장에 실패 하였습니다.";
    }
    exit;
}
elseif($process == "f_ord_sub_s_no")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "ord_sub_s_no" => $value))){
		echo "서브 발주담당자가 저장 되었습니다.";
	}else{
		echo "서브 발주담당자 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_ord_third_s_no")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "ord_third_s_no" => $value))){
		echo "서브 발주담당자가 저장 되었습니다.";
	}else{
		echo "서브 발주담당자 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_display")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "display" => $value))){
		echo "Display가 저장 되었습니다.";
	}else{
		echo "Display 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_priority")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "priority" => $value))){
		echo "우선순위가 저장 되었습니다.";
	}else{
		echo "우선순위 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_option_name")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "option_name" => $value))){
		echo "상품옵션명이 저장 되었습니다.";
	}else{
		echo "상품옵션명 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_wise_sku")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "wise_sku" => $value))){
		echo "WISE SKU 값이 저장 되었습니다.";
	}else{
		echo "WISE SKU 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "save_sup_price")
{
	$no 			 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$f_license_type  	= (isset($_POST['f_license_type'])) ? $_POST['f_license_type'] : "1";
    $f_sup_price 	 	= (isset($_POST['f_sup_price'])) ? str_replace(",", "", trim($_POST['f_sup_price'])) : "";
	$f_sup_price_vat 	= ($f_license_type != '4') ? str_replace(',', '', $_POST['f_sup_price_vat']) : $f_sup_price;
	$search_url 	 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$f_is_control 	 	= (isset($_POST['f_is_control'])) ? $_POST['f_is_control'] : "0";

	if($unit_model->update(array("no" => $no, "sup_price" => $f_sup_price, "sup_price_vat" => $f_sup_price_vat)))
	{
		if($f_is_control == '1')
		{
			$unit_item = $unit_model->getItem($no);

			$cost_model = ProductCmsUnit::Factory();
			$cost_model->setMainInit("product_cms_unit_cost_control", "cc_no");

			$cost_ins_data = array(
				"prd_unit" 		=> $no,
				"sup_c_no"		=> $unit_item['sup_c_no'],
				"sup_price"		=> $unit_item['sup_price'],
				"sup_price_vat"	=> $unit_item['sup_price_vat'],
				"cost_date"		=> date("Y-m-d H:i:s"),
				"regdate"		=> date("Y-m-d H:i:s")
			);
			$cost_model->insert($cost_ins_data);
		}

		exit("<script>alert('공급단가가 변경되었습니다');location.href='product_cms_unit.php?{$search_url}';</script>");
	} else {
		exit("<script>alert('공급단가 변경에 실패했습니다');location.href='product_cms_unit.php?{$search_url}';</script>");
	}
}
elseif($process == "f_description")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "description" => addslashes($value)))){
        echo "상품옵션소개가 저장 되었습니다.";
    } else {
        echo "상품옵션소개 저장에 실패 하였습니다.";
    }
    exit;
}
elseif ($process == "f_expiration")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "expiration" => $value))){
		echo "유통기한이 저장 되었습니다.";
	} else {
		echo "유통기한 저장에 실패 하였습니다.";
	}
	exit;
}
elseif ($process == "f_brand")
{
	$no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "brand" => $value))){
		echo "업체명/브랜드가 저장 되었습니다.";
	} else {
		echo "업체명/브랜드 저장에 실패 하였습니다.";
	}
	exit;
}
elseif ($process == "f_is_control")
{
	$no  	 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$value		= (isset($_POST['f_is_control'])) ? $_POST['f_is_control'] : "0";

	if ($unit_model->update(array("no" => $no, "is_control" => $value)))
	{
		if($value == '1')
		{
			$f_sup_price 	 = str_replace(',', '', $_POST['f_sup_price']);
			$f_license_type  = (isset($_POST['f_license_type'])) ? $_POST['f_license_type'] : "1";
			$f_sup_price_vat = ($f_license_type != '4') ? str_replace(',', '', $_POST['f_sup_price_vat']) : $f_sup_price;

			$unit_item = $unit_model->getItem($no);
			$cost_model = ProductCmsUnit::Factory();
			$cost_model->setMainInit("product_cms_unit_cost_control", "cc_no");

			$cost_ins_data = array(
				"prd_unit" 		=> $no,
				"sup_c_no"		=> $unit_item['sup_c_no'],
				"sup_price"		=> $f_sup_price,
				"sup_price_vat"	=> $f_sup_price_vat,
				"cost_date"		=> date("Y-m-d H:i:s"),
				"regdate"		=> date("Y-m-d H:i:s")
			);

			$cost_model->insert($cost_ins_data);
		}

		exit("<script>alert('발주원가관리가 변경되었습니다');location.href='product_cms_unit.php?{$search_url}';</script>");
	} else {
		exit("<script>alert('발주원가관리 변경에 실패했습니다');location.href='product_cms_unit.php?{$search_url}';</script>");
	}
}
elseif($process == "f_type")
{
	$no		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "type" => $value))){
		echo "구분이 저장 되었습니다.";
	}else{
		echo "구분 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_bad_reason")
{
	$no		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_model->update(array("no" => $no, "bad_reason" => $value))){
		echo "불량사유가 저장 되었습니다.";
	}else{
		echo "불량사유 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_warehouse")
{
	$no		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_manage_model->update(array("no" => $no, "warehouse" => $value))){
		echo "당기출고 창고가 저장 되었습니다.";
	}else{
		echo "당기출고 창고 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_qty_alert")
{
	$no		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_manage_model->update(array("no" => $no, "qty_alert" => $value))) {
		echo "재고알림 설정수량이 저장 되었습니다.";
	} else {
		echo "재고알림 설정수량 저장에 실패 하였습니다.";
	}
	exit;
}
elseif($process == "f_qty_hp")
{
	$no		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if ($unit_manage_model->update(array("no" => $no, "qty_hp" => addslashes($value)))) {
		echo "재고/입고 수신 연락처가 저장 되었습니다.";
	} else {
		echo "재고/입고 수신 연락처 저장에 실패 하였습니다.";
	}
	exit;
}
elseif ($process == "f_is_except_stock")
{
	$no  	 	= (isset($_POST['manager_no'])) ? $_POST['manager_no'] : "";
	$search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$value		= (isset($_POST['manager_value'])) ? $_POST['manager_value'] : "0";

	if ($unit_manage_model->update(array("no" => $no, "is_except_stock" => $value))) {
		exit("<script>alert('재고관리 상태 변경했습니다');location.href='product_cms_unit.php?{$search_url}';</script>");
	} else {
		exit("<script>alert('재고관리 상태 처리에 실패했습니다');location.href='product_cms_unit.php?{$search_url}';</script>");
	}
}
elseif ($process == "f_is_in_out")
{
	$no  	 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value		= (isset($_POST['val'])) ? $_POST['val'] : "2";

	if ($unit_model->update(array("no" => $no, "is_in_out" => $value))) {
		echo "입/출고 정지 상태가 저장 되었습니다.";
	} else {
		echo "입/출고 정지 상태 저장에 실패했습니다.";
	}
	exit;
}
elseif ($process == "f_in_out_msg")
{
	$no  	 	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value		= (isset($_POST['val'])) ? $_POST['val'] : "2";

	if ($unit_model->update(array("no" => $no, "in_out_msg" => $value))) {
		echo "입/출고 정지 메세지가 저장 되었습니다.";
	} else {
		echo "입/출고 정지 메세지 저장에 실패했습니다.";
	}
	exit;
}
else
{
	# Navigation & My Quick
	$nav_prd_no  = "69";
	$nav_title   = "커머스 구성품목(재고품목)";
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_title", $nav_title);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	# 검색쿼리
	$add_where 			= " 1=1 ";
	$sch_no 		 	= isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
    $sch_brand  		= isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
	$sch_team 	  	 	= isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
	$sch_s_no 	  	 	= isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
	$sch_sup_c_no 	 	= isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
	$sch_display	 	= isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
	$sch_option_name 	= isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
	$sch_wise_sku 		= isset($_GET['sch_wise_sku']) ? $_GET['sch_wise_sku'] : "";
	$sch_type 	 		= isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
	$sch_is_control		= isset($_GET['sch_is_control']) ? $_GET['sch_is_control'] : "";
	$sch_log_c_no		= isset($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "";
    $sch_sku		 	= isset($_GET['sch_sku']) ? $_GET['sch_sku'] : "";
	$sch_description 	= isset($_GET['sch_description']) ? $_GET['sch_description'] : "";
	$sch_bad_reason 	= isset($_GET['sch_bad_reason']) ? $_GET['sch_bad_reason'] : "";
	$sch_is_load		= isset($_GET['sch_is_load']) ? $_GET['sch_is_load'] : "";
	$sch_is_except_stock= isset($_GET['sch_is_except_stock']) ? $_GET['sch_is_except_stock'] : "";
	$sch_is_in_out		= isset($_GET['sch_is_in_out']) ? $_GET['sch_is_in_out'] : "";

	#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
	$url_check_str 	= $_SERVER['QUERY_STRING'];
	$url_chk_result = false;
	if(empty($url_check_str)){
		$url_check_team_where   = getTeamWhere($my_db, $session_team);
		$url_check_sql    		= "SELECT count(*) as cnt FROM product_cms_unit pcu  WHERE (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$url_check_team_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$url_check_team_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$url_check_team_where}))) AND pcu.display = '1'";
		$url_check_query  		= mysqli_query($my_db, $url_check_sql);
		$url_check_result 		= mysqli_fetch_assoc($url_check_query);

		if($url_check_result['cnt'] == 0){
			$sch_team 		= "all";
			$url_chk_result = true;
		}
	}

    if (!empty($sch_no)) {
        $add_where .= " AND pcu.no = '{$sch_no}'";
        $smarty->assign("sch_no", $sch_no);
    }

	if (!empty($sch_brand)) {
		$add_where .= " AND pcu.brand = '{$sch_brand}'";
		$smarty->assign("sch_brand", $sch_brand);
	}

	$team_model			 = Team::Factory();
	$team_all_list		 = $team_model->getTeamAllList();
	$staff_all_list		 = $team_all_list["staff_list"];
	$sch_sup_c_list 	 = $unit_model->getDistinctUnitCompanyData('sup_c_no');
	$sch_staff_list 	 = $staff_all_list['all'];
	$team_full_name_list = $team_model->getTeamFullNameList();
	$sch_team_code_where = "";

	if (!empty($sch_team))
	{
		if ($sch_team != "all") {
			$sch_team_code_where = getTeamWhere($my_db, $sch_team);
			$add_where 		 .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN({$sch_team_code_where})))";
			$sch_sup_c_list   = $unit_model->getTeamSupList($sch_team);
			$sch_staff_list   = $staff_all_list[$sch_team];
		}
		$smarty->assign("sch_team", $sch_team);
	}else{
		$sch_team_code_where = getTeamWhere($my_db, $session_team);
		$add_where 		 .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE s.team IN ({$sch_team_code_where})))";
		$sch_sup_c_list   = $unit_model->getTeamSupList($session_team);
		$sch_staff_list   = $staff_all_list[$session_team];
		$smarty->assign("sch_team", $session_team);
	}

	if (!empty($sch_s_no))
	{
		if ($sch_s_no != "all") {
			$add_where 		 .= " AND (pcu.ord_s_no = '{$sch_s_no}' OR pcu.ord_sub_s_no = '{$sch_s_no}' OR pcu.ord_third_s_no = '{$sch_s_no}')";
		}
		$smarty->assign("sch_s_no", $sch_s_no);
	}else{
		if($sch_team == $session_team){
			$add_where		 .=" AND (pcu.ord_s_no = '{$session_s_no}' OR pcu.ord_sub_s_no = '{$session_s_no}' OR pcu.ord_third_s_no = '{$session_s_no}')";
			$smarty->assign("sch_s_no", $session_s_no);
		}
	}

	if (!empty($sch_sup_c_no)) {
		$add_where .= " AND pcu.sup_c_no = '{$sch_sup_c_no}'";
		$smarty->assign("sch_sup_c_no", $sch_sup_c_no);
	}

	if (!empty($sch_display)) {
		$add_where .= " AND pcu.display = '{$sch_display}'";
		$smarty->assign("sch_display", $sch_display);
	}

	if (!empty($sch_option_name)) {
		$add_where .= " AND pcu.option_name LIKE '%{$sch_option_name}%'";
		$smarty->assign("sch_option_name", $sch_option_name);
	}

	if (!empty($sch_wise_sku)) {
		$add_where .= " AND pcu.wise_sku LIKE '%{$sch_wise_sku}%'";
		$smarty->assign("sch_wise_sku", $sch_wise_sku);
	}

	if (!empty($sch_type)) {
		$add_where .= " AND pcu.`type` = '{$sch_type}'";
		$smarty->assign("sch_type", $sch_type);
	}

	if (!empty($sch_is_control)) {
		$add_where .= " AND pcu.is_control = '{$sch_is_control}'";
		$smarty->assign("sch_is_control", $sch_is_control);
	}

	if (!empty($sch_log_c_no)) {
		$add_where .= " AND pcu.`no` IN(SELECT pcum.prd_unit FROM product_cms_unit_management pcum WHERE pcum.log_c_no ='{$sch_log_c_no}')";
		$smarty->assign("sch_log_c_no", $sch_log_c_no);
	}

    if (!empty($sch_sku)) {
		$add_where .= " AND pcu.`no` IN(SELECT pcum.prd_unit FROM product_cms_unit_management pcum WHERE pcum.sku LIKE '%{$sch_sku}%')";
        $smarty->assign("sch_sku", $sch_sku);
    }

	if (!empty($sch_description)) {
		$add_where .= " AND pcu.description LIKE '%{$sch_description}%'";
		$smarty->assign("sch_description", $sch_description);
	}

	if (!empty($sch_bad_reason)) {
		$add_where .= " AND pcu.bad_reason = '{$sch_bad_reason}'";
		$smarty->assign("sch_bad_reason", $sch_bad_reason);
	}

	if (!empty($sch_is_load)) {
		if($sch_is_load == '1'){
			$add_where .= " AND (SELECT sub.pl_no FROM product_cms_load as sub WHERE sub.pl_no IN(SELECT DISTINCT plu.pl_no FROM product_cms_load_unit as plu WHERE plu.unit_no=pcu.`no`) AND sub.display='1' LIMIT 1) != ''";
		}elseif($sch_is_load == '2'){
			$add_where .= " AND (SELECT sub.pl_no FROM product_cms_load as sub WHERE sub.pl_no IN(SELECT DISTINCT plu.pl_no FROM product_cms_load_unit as plu WHERE plu.unit_no=pcu.`no`) AND sub.display='1' LIMIT 1) IS NULL";
		}

		$smarty->assign("sch_is_load", $sch_is_load);
	}

	if (!empty($sch_is_except_stock)) {
		$add_where .= " AND pcu.`no` IN(SELECT sub.prd_unit FROM product_cms_unit_management as sub WHERE sub.is_except_stock='1')";
		$smarty->assign("sch_is_except_stock", $sch_is_except_stock);
	}

	if (!empty($sch_is_in_out)) {
		$add_where .= " AND pcu.is_in_out='2'";
		$smarty->assign("sch_is_in_out", $sch_is_in_out);
	}

	// 정렬순서 토글 & 필드 지정
	$add_orderby = "pcu.no DESC";
	$order		 = isset($_GET['od'])?$_GET['od']:"";
	$order_type	 = isset($_GET['by'])?$_GET['by']:"";

	if($order_type == '2'){
		$toggle = "DESC";
	}else{
		$toggle = "ASC";
	}
	$order_field = array('','priority','option_name','sup_c_name');

	if($order && $order<4)
	{
		$add_orderby = " ISNULL($order_field[$order]) ASC, $order_field[$order] $toggle";
		$smarty->assign("order",$order);
		$smarty->assign("order_type",$order_type);
	}

    $last_stock_sql    = "SELECT stock_date FROM product_cms_stock ORDER BY stock_date DESC LIMIT 1";
    $last_stock_query  = mysqli_query($my_db, $last_stock_sql);
    $last_stock_result = mysqli_fetch_assoc($last_stock_query);
    $last_stock_date   = isset($last_stock_result['stock_date']) ? $last_stock_result['stock_date'] : "";
    $smarty->assign("last_stock_date", $last_stock_date);

	# 옵션들
	$kind_model 		= Kind::Factory();
	$bad_reason_option 	= $kind_model->getKindParentList("bad_reason");
	$company_model		= Company::Factory();
	$log_company_list 	= $company_model->getLogisticsList();
	$stock_model		= ProductCmsStock::Factory();
	$log_warehouse_list	= $stock_model->getWarehouseBaseList();
	$unit_brand_option	= $unit_model->getBrandData();

	# 페이징 처리
	$cnt_sql 	 = "SELECT count(*) AS total_count FROM product_cms_unit pcu WHERE {$add_where}";
	$cnt_query	 = mysqli_query($my_db, $cnt_sql);
	$cnt_data	 = mysqli_fetch_array($cnt_query);
	$total_num 	 = $cnt_data['total_count'];

	$pages 		 = isset($_GET['page']) ?intval($_GET['page']) : 1;
	$od_list_num = isset($_GET['od_list_num']) ?intval($_GET['od_list_num']) : 20;

	$num 		 = $od_list_num;
	$offset 	 = ($pages-1) * $num;
	$pagenum 	 = ceil($total_num/$num);

	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	if(empty($url_check_str) && $url_chk_result){
		$search_url = "sch_team=all";
	}else{
		$search_url = getenv("QUERY_STRING");
	}
	$pagelist 	= pagelist($pages, "product_cms_unit.php", $pagenum, $search_url);

	$smarty->assign("page", $pages);
	$smarty->assign("od_list_num", $od_list_num);
	$smarty->assign(array("total_num"=>number_format($total_num)));
	$smarty->assign("search_url", $search_url);
	$smarty->assign("pagelist", $pagelist);

	# 기준일
	$last_stock_sql     = "SELECT * FROM (SELECT DISTINCT stock_c_no, stock_date FROM product_cms_stock ORDER BY stock_date DESC) as date_result GROUP BY stock_c_no";
	$last_stock_query   = mysqli_query($my_db, $last_stock_sql);
	$last_stock_list    = [];
	while($last_stock_result  = mysqli_fetch_assoc($last_stock_query)){
		$last_stock_list[$last_stock_result['stock_c_no']] = $last_stock_result['stock_date'];
	}

	# 리스트 쿼리
	$product_cms_unit_sql = "
		SELECT
			*,
		   	(SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcu.sup_c_no) as sup_c_name,
		   	(SELECT sub_c.license_type FROM company as sub_c WHERE sub_c.c_no = pcu.sup_c_no) as license_type,
		   	(SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcu.brand) as brand_name,
			(SELECT s.team FROM staff s WHERE s.s_no=pcu.ord_s_no) as ord_team,
   			(SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_s_no) as ord_s_name,
			(SELECT s.team FROM staff s WHERE s.s_no=pcu.ord_sub_s_no) as ord_sub_team,
			(SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_sub_s_no) as ord_sub_s_name,
			(SELECT s.team FROM staff s WHERE s.s_no=pcu.ord_third_s_no) as ord_third_team,
			(SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_third_s_no) as ord_third_s_name,
		    (SELECT sub.pl_no FROM product_cms_load as sub WHERE sub.pl_no IN(SELECT DISTINCT plu.pl_no FROM product_cms_load_unit as plu WHERE plu.unit_no=pcu.`no`) AND sub.display='1' LIMIT 1) as pl_no,
		    (SELECT COUNT(sub_unit) FROM product_cms_unit_subcontract pcus WHERE pcus.parent_unit=pcu.`no`) as sub_unit_cnt,
			(SELECT COUNT(sub_unit) FROM product_cms_unit_subcontract pcus WHERE pcus.sub_unit=pcu.`no`) as is_sub_unit
		FROM product_cms_unit pcu
		WHERE {$add_where}
		ORDER BY {$add_orderby}
		LIMIT {$offset}, {$num}
	";
    $product_cms_unit_query = mysqli_query($my_db, $product_cms_unit_sql);
	$product_cms_unit_list	= [];
	while($product_cms_unit = mysqli_fetch_array($product_cms_unit_query))
	{
		$sup_price 		= ($product_cms_unit['license_type'] == '4') ? getUsdFormatPrice($product_cms_unit['sup_price']) : getKrwFormatPrice($product_cms_unit['sup_price']);
        $sup_price_vat 	= getKrwFormatPrice($product_cms_unit['sup_price_vat']);
        $is_editable    = false;
        $is_search      = false;

		if(permissionNameCheck($session_permission,"물류관리자") || $session_s_no == $product_cms_unit['ord_s_no'] || $session_s_no == $product_cms_unit['ord_sub_s_no'] || $session_s_no == $product_cms_unit['ord_third_s_no']){
			$is_editable = true;
		}

		if(($session_s_no == '42' || $session_s_no == '265') && ($session_team == $product_cms_unit['ord_team'] || $session_team == $product_cms_unit['ord_sub_team']))
		{
			$is_search = true;
		}

		$manage_list = [];
		$unit_manage_sql 	= "
			SELECT 
			    *
			FROM product_cms_unit_management pcum 
			WHERE prd_unit = '{$product_cms_unit['no']}'
		";
		$unit_manage_query 	= mysqli_query($my_db, $unit_manage_sql);
		while($unit_manage = mysqli_fetch_assoc($unit_manage_query))
		{
			$unit_log_c_no 	= $unit_manage['log_c_no'];
			$unit_last_date = $last_stock_list[$unit_log_c_no];
			$unit_stock 	= 0;
			if(!empty($unit_last_date)){
				if($unit_log_c_no != '2809'){
					$unit_stock_sql = "SELECT SUM(qty) as stock_qty FROM product_cms_stock WHERE prd_unit='{$product_cms_unit['no']}' AND stock_date='{$unit_last_date}' AND stock_c_no='{$unit_log_c_no}'";
				}else{
					$unit_stock_sql = "SELECT SUM(qty) as stock_qty FROM product_cms_stock WHERE prd_unit='{$product_cms_unit['no']}' AND stock_date='{$unit_last_date}' AND stock_c_no='{$unit_log_c_no}' AND (warehouse LIKE '%정상창고' OR warehouse LIKE '%세이프인')";
				}
				$unit_stock_query 	= mysqli_query($my_db, $unit_stock_sql);
				$unit_stock_result 	= mysqli_fetch_assoc($unit_stock_query);
				$unit_stock			= isset($unit_stock_result['stock_qty']) ? $unit_stock_result['stock_qty'] : 0;
			}
			$unit_manage['stock_qty']		 = $unit_stock;
			$unit_manage['warehouse_option'] = isset($log_warehouse_list[$unit_log_c_no]) ? $log_warehouse_list[$unit_log_c_no] : [];
			$manage_list[] = $unit_manage;
		}

		$sub_unit_list 			= [];
		$unit_total_price 		= 0;
		$unit_total_price_vat 	= 0;
		$unit_abroad_price 		= 0;
		if($product_cms_unit['sub_unit_cnt'] > 0)
		{
			$unit_total_price 		= $product_cms_unit['sup_price'];
			$unit_total_price_vat 	= $product_cms_unit['sup_price_vat'];

			$sub_unit_sql 	= "
				SELECT 
					pcus.sub_unit_name, 
					pcus.qty, 
					(SELECT sub_c.license_type FROM company as sub_c WHERE sub_c.c_no = pcus.sup_c_no) as license_type,
					(SELECT pcu.sup_price FROM product_cms_unit pcu WHERE pcu.`no`=pcus.sub_unit) as sup_price, 
					(SELECT pcu.sup_price_vat FROM product_cms_unit pcu WHERE pcu.`no`=pcus.sub_unit) as sup_price_vat 
				FROM product_cms_unit_subcontract as pcus 
				WHERE pcus.parent_unit='{$product_cms_unit['no']}' 
			 	ORDER BY pcus.`no`
			";
			$sub_unit_query = mysqli_query($my_db, $sub_unit_sql);
			while($sub_unit = mysqli_fetch_assoc($sub_unit_query))
			{
				$sub_code				= $sub_unit['license_type'] == '4' ? "$" : "₩";
				$sub_price 				= $sub_unit["sup_price"]*$sub_unit['qty'];
				$sub_price_vat 			= $sub_unit["sup_price_vat"]*$sub_unit['qty'];

				if($sub_unit['license_type'] != 4){
					$unit_total_price  	   += $sub_price;
					$unit_total_price_vat  += $sub_price_vat;
				}else{
					$unit_abroad_price += $sub_price_vat;
				}
				$sub_unit_list[] = array("sub_name" => $sub_unit["sub_unit_name"], "sub_code" => $sub_code, "sub_price" => $sub_price_vat);
			}
		}

		if($product_cms_unit['is_sub_unit'] > 0){
			$chk_sub_sql = "
				SELECT
					(SELECT COUNT(*) FROM product_cms_unit_management sub WHERE sub.prd_unit=rs.sub_unit AND sub.log_c_no=rs.sup_c_no) AS log_cnt
				FROM
				(	
					SELECT 
						main.parent_unit,
						pcu.sup_c_no,
						main.sub_unit	 
					FROM product_cms_unit_subcontract AS main
					LEFT JOIN product_cms_unit AS pcu ON pcu.`no`=main.parent_unit
					WHERE main.sub_unit='{$product_cms_unit['no']}'
				) AS rs
			";
			$chk_sub_query 	= mysqli_query($my_db, $chk_sub_sql);
			$chk_sub_result = mysqli_fetch_assoc($chk_sub_query);

			if(empty($chk_sub_result['log_cnt'])){
				$product_cms_unit['is_need_sub'] = true;
			}
		}

		$product_cms_unit['sup_price'] 				= $sup_price;
		$product_cms_unit['sup_price_vat'] 			= $sup_price_vat;
		$product_cms_unit['is_editable'] 			= $is_editable;
		$product_cms_unit['is_search'] 				= $is_search;
		$product_cms_unit['manage_cnt'] 			= count($manage_list);
		$product_cms_unit['manage_list'] 			= $manage_list;
		$product_cms_unit['sub_unit_list'] 			= $sub_unit_list;
		$product_cms_unit['unit_total_price'] 		= $unit_total_price;
		$product_cms_unit['unit_total_price_vat'] 	= $unit_total_price_vat;
		$product_cms_unit['unit_abroad_price'] 		= $unit_abroad_price;

		$product_cms_unit_list[] = $product_cms_unit;
	}

	$smarty->assign("page_type_list", getPageTypeOption('5'));
	$smarty->assign("sch_sup_c_list", $sch_sup_c_list);
	$smarty->assign("sch_team_list", $team_full_name_list);
	$smarty->assign("sch_staff_list", $sch_staff_list);
	$smarty->assign("unit_type_option", getUnitTypeOption());
	$smarty->assign("bad_reason_option", $bad_reason_option);
	$smarty->assign("display_option", getDisplayOption());
	$smarty->assign("is_product_load_option", getIsProductLoadOption());
	$smarty->assign("log_company_list", $log_company_list);
	$smarty->assign("unit_brand_option", $unit_brand_option);
	$smarty->assign("product_cms_unit_list", $product_cms_unit_list);

	$smarty->display('product_cms_unit.html');
}
?>
