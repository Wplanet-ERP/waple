<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

if(!permissionNameCheck($session_permission,"마스터관리자")){
    $smarty->display('access_company_error.html');
    exit;
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "t_no")
	->setCellValue('B3', "작성일")
	->setCellValue('C3', "주문일시")
	->setCellValue('D3', "주문번호")
	->setCellValue('E3', "수령자명")
	->setCellValue('F3', "수령자전화")
	->setCellValue('G3', "우편번호")
	->setCellValue('H3', "수령지")
	->setCellValue('I3', "배송메모")
	->setCellValue('J3', "배송처리일")
	->setCellValue('K3', "택배사")
	->setCellValue('L3', "운송자번호")
	->setCellValue('M3', "업체명/브랜드명")
	->setCellValue('N3', "업체담당자")
	->setCellValue('O3', "커머스 상품명")
	->setCellValue('P3', "업무요청자")
	->setCellValue('Q3', "수량")
	->setCellValue('R3', "주문요약정보")
	->setCellValue('S3', "주문처리자")
	->setCellValue('T3', "결제금액")
	->setCellValue('U3', "결제일시")
	->setCellValue('V3', "구매처")
	->setCellValue('W3', "관리자메모");

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where			= "1=1";
$sch_order_number 	= isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_recipient 		= isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp 	= isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_c_name 		= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";                //업체명/브랜드명
$sch_s_name		    = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";                //업체담당자
$sch_dp_c_no 		= isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";          //구매처
$sch_prd_name 		= isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";

if(!empty($sch_order_number)){
    $add_where .= " AND wct.order_number = '{$sch_order_number}'";
    $smarty->assign('sch_order_number', $sch_order_number);
}

if(!empty($sch_recipient)){
    $add_where .= " AND wct.recipient like '%{$sch_recipient}%'";
    $smarty->assign('sch_recipient', $sch_recipient);
}

if(!empty($sch_recipient_hp)){
    $add_where .= " AND wct.recipient_hp like '%{$sch_recipient_hp}%'";
    $smarty->assign('sch_recipient_hp', $sch_recipient_hp);
}

if(!empty($sch_c_name)){
    $add_where .= " AND wct.c_name like '%{$sch_c_name}%'";
    $smarty->assign('sch_c_name', $sch_c_name);
}

if(!empty($sch_s_name)){
    $add_where .= " AND (SELECT s_name from staff s where s.s_no=wct.s_no) like '%{$sch_s_name}%'";
    $smarty->assign('sch_s_name', $sch_s_name);
}

if(!empty($sch_prd_name)){
    $add_where .= " AND wct.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.title like '%{$sch_prd_name}%')";
    $smarty->assign('sch_prd_name', $sch_prd_name);
}

if(!empty($sch_dp_c_no)){
    $add_where .= " AND wct.dp_c_no='{$sch_dp_c_no}'";
    $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
}

// 리스트 쿼리
$temporary_sql = "
	SELECT
		wct.t_no,
		DATE_FORMAT(wct.regdate, '%Y-%m-%d') as reg_date,
		DATE_FORMAT(wct.regdate, '%H:%i') as reg_time,
		wct.stock_date,
		wct.order_date,
		DATE_FORMAT(wct.order_date, '%Y-%m-%d') as ord_date,
		DATE_FORMAT(wct.order_date, '%H:%i') as ord_time,
		wct.order_number,
		wct.recipient,
		wct.recipient_hp,
		wct.recipient_addr,
		IF(wct.zip_code, CONCAT('[',wct.zip_code,']'), '') as postcode,
		wct.delivery_memo,
		wct.c_name,
		(SELECT s.s_name FROM staff s WHERE s.s_no=wct.s_no) as s_name,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no))) AS k_prd1_name,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wct.prd_no)) AS k_prd2_name,
		(SELECT title from product_cms prd_cms where prd_cms.prd_no=wct.prd_no) as prd_name,
		(SELECT s.s_name FROM staff s WHERE s.s_no=wct.task_req_s_no) as task_req_s_name,
		wct.task_req,
		(SELECT s_name FROM staff s where s.s_no=wct.task_run_s_no) as task_run_s_name,
		wct.quantity,
		wct.dp_price_vat,
		DATE_FORMAT(wct.payment_date, '%Y-%m-%d') as pay_date,
		DATE_FORMAT(wct.payment_date, '%H:%i') as pay_time,
		wct.dp_c_name,
		wct.manager_memo
	FROM work_cms_temporary wct
	WHERE {$add_where}
	ORDER BY wct.t_no DESC
";


$temporary_query	= mysqli_query($my_db, $temporary_sql);
$idx = 4;
if(!!$temporary_query)
{
    while($temporary = mysqli_fetch_array($temporary_query))
    {
        $dp_price_vat = number_format($temporary['dp_price_vat']);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$idx, $temporary['t_no'])
            ->setCellValue('B'.$idx, $temporary['reg_date'])
            ->setCellValue('C'.$idx, $temporary['ord_date'])
            ->setCellValueExplicit('D'.$idx, $temporary['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('E'.$idx, $temporary['recipient'])
            ->setCellValue('F'.$idx, $temporary['recipient_hp'])
            ->setCellValue('G'.$idx, $temporary['zip_code'])
            ->setCellValue('H'.$idx, $temporary['recipient_addr'])
			->setCellValueExplicit('I'.$idx, $temporary['delivery_memo'],PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('J'.$idx, $temporary['stock_date'])
            ->setCellValue('K'.$idx, "")
            ->setCellValueExplicit('L'.$idx, "", PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('M'.$idx, $temporary['c_name'])
            ->setCellValue('N'.$idx, $temporary['s_name'])
            ->setCellValue('O'.$idx, $temporary['prd_name'])
            ->setCellValue('P'.$idx, $temporary['task_req_s_name'])
            ->setCellValue('Q'.$idx, $temporary['quantity'])
            ->setCellValue('R'.$idx, $temporary['task_req'])
            ->setCellValue('S'.$idx, $temporary['task_run_s_name'])
            ->setCellValue('T'.$idx, $dp_price_vat)
            ->setCellValue('U'.$idx, $temporary['payment_date'])
            ->setCellValue('V'.$idx, $temporary['dp_c_name'])
            ->setCellValue('W'.$idx, $temporary['manager_memo']);

        $idx++;
    }
}
$idx--;

$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
$objPHPExcel->getActiveSheet()->setCellValue('A1', "택배 자동정리 리스트");
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setSize(18);
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A3:W3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A3:W3')->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle('A3:W3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:W3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:W3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');

$objPHPExcel->getActiveSheet()->getStyle("A4:W{$idx}")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('I4:I'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('O4:O'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('P4:P'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('Q4:Q'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('R4:R'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('S4:S'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('T4:T'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('U4:U'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('V4:V'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('W4:W'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('P4:P'.$idx)->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getStyle('A4:W'.$idx)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:W'.$idx)->applyFromArray($styleArray);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);

$objPHPExcel->getActiveSheet()->setTitle('택배 자동정리 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_택배 자동정리 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
