<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/agency.php');
require('inc/helper/work.php');
require('inc/model/MyCompany.php');
require('inc/model/Team.php');
require('inc/model/Kind.php');
require('inc/model/Agency.php');
require('inc/model/Company.php');

# 기본 변수 값 설정
$prev_settle_month  = date('Y-m', strtotime("-1 months"));
$base_settle_month  = date('Y-m', strtotime("-4 months"));

# 검색조건
$add_where          = "1=1 AND `as`.partner > 0";
$sch_settle_month   = isset($_GET['sch_settle_month']) ? $_GET['sch_settle_month'] : $prev_settle_month;
$sch_settle_s_month = isset($_GET['sch_settle_s_month']) ? $_GET['sch_settle_s_month'] : $base_settle_month;
$sch_settle_e_month = isset($_GET['sch_settle_e_month']) ? $_GET['sch_settle_e_month'] : $sch_settle_month;
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_status         = isset($_GET['sch_status']) ? $_GET['sch_status'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_media          = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_is_partner     = isset($_GET['sch_is_partner']) ? $_GET['sch_is_partner'] : "";
$sch_chart_type     = isset($_GET['sch_chart_type']) && !empty($_GET['sch_chart_type']) ? $_GET['sch_chart_type'] : "1";
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_partner        = isset($_GET['sch_partner']) ? $_GET['sch_partner'] : "";
$sch_manager_team   = isset($_GET['sch_manager_team']) ? $_GET['sch_manager_team'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_run_name       = isset($_GET['sch_run_name']) ? $_GET['sch_run_name'] : "";

$smarty->assign('sch_chart_type', $sch_chart_type);

if(!empty($sch_settle_s_month))
{
    $add_where .= " AND `as`.settle_month >= '{$sch_settle_s_month}'";
    $smarty->assign('sch_settle_s_month', $sch_settle_s_month);
}

if(!empty($sch_settle_e_month))
{
    $add_where .= " AND `as`.settle_month <= '{$sch_settle_e_month}'";
    $smarty->assign('sch_settle_e_month', $sch_settle_e_month);
}

if(!empty($sch_no))
{
    $add_where .= " AND `as`.as_no = '{$sch_no}'";
    $smarty->assign('sch_no', $sch_no);
}

if(!empty($sch_status)){
    $add_where   .= " AND `as`.status = '{$sch_status}'";
    $smarty->assign('sch_status', $sch_status);
}

if(!empty($sch_agency)){
    $add_where   .= " AND `as`.agency = '{$sch_agency}'";
    $smarty->assign('sch_agency', $sch_agency);
}

if(!empty($sch_media)){
    $add_where   .= " AND `as`.media = '{$sch_media}'";
    $smarty->assign('sch_media', $sch_media);
}

if(!empty($sch_is_partner))
{
    if($sch_is_partner == '1'){
        $add_where   .= " AND `as`.partner > 0";
    }elseif($sch_is_partner == '2'){
        $add_where   .= " AND (`as`.partner is null OR `as`.partner < 1)";
    }
    $smarty->assign('sch_is_partner', $sch_is_partner);
}

# 업체별 검색 관련 옵션
$team_model             = Team::Factory();
$my_company_model       = MyCompany::Factory();
$agency_model           = Agency::Factory();
$team_all_list          = $team_model->getTeamAllList();
$staff_team_list        = $team_all_list['staff_list'];
$team_name_list         = $team_all_list['team_name_list'];
$team_full_name_list    = $team_model->getTeamFullNameList();
$my_company_list        = $my_company_model->getList();
$my_company_name_list   = $my_company_model->getNameList();
$partner_list           = $agency_model->getPartnerList();
$agency_option          = getSettleAgencyOption();
$media_option           = getAdvertiseMediaOption();
$sch_staff_list         = [];

# 브랜드별 검색 관련 옵션
$kind_model                 = Kind::Factory();
$company_model              = Company::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$sch_chart_title            = "전체";
$sch_chart_data             = array(
    "my_c_name"     => "-",
    "c_name"        => "-",
    "team_name"     => "-",
    "manager_name"  => "-",
    "media_name"    => isset($media_option[$sch_media]) ? $media_option[$sch_media] : "전체",
    "total_cnt"     => 0,
    "total_price"   => 0,
    "total_fee"     => 0,
);

if($sch_chart_type == '1') # 업체별 검색
{
    if(!empty($sch_my_c_no)){
        $add_where      .= " AND `as`.my_c_no = '{$sch_my_c_no}'";
        $my_company_item = $my_company_model->getItem($sch_my_c_no);
        $sch_chart_data['my_c_name'] = $my_company_item['c_name'];
        $smarty->assign('sch_my_c_no', $sch_my_c_no);
    }

    if(!empty($sch_partner)){
        $add_where      .= " AND `as`.partner LIKE '%{$sch_partner}%'";
        $company_item    = $company_model->getItem($sch_partner);

        $sch_chart_data['my_c_name']    = $company_item['my_c_name'];
        $sch_chart_data['c_name']       = $company_item['c_name'];
        $sch_chart_data['team_name']    = $company_item['team_name'];
        $sch_chart_data['manager_name'] = $company_item['s_name'];
        $sch_chart_title                = $company_item['c_name'];

        $smarty->assign('sch_partner', $sch_partner);
    }

    if (!empty($sch_manager_team))
    {
        if ($sch_manager_team != "all") {
            $sch_team_code_where = getTeamWhere($my_db, $sch_manager_team);
            $add_where 		 .= " AND `as`.manager_team IN({$sch_team_code_where})";
            $sch_staff_list   = $staff_team_list[$sch_manager_team];
            $sch_chart_data['team_name'] = $team_name_list[$sch_manager_team];
        }
        $smarty->assign("sch_manager_team", $sch_manager_team);
    }

    if (!empty($sch_manager))
    {
        if ($sch_manager != "all") {
            $add_where .= " AND `as`.manager='{$sch_manager}'";
            $sch_chart_data['manager_name'] = $sch_staff_list[$sch_manager];
        }
        $smarty->assign("sch_manager", $sch_manager);
    }

    if(!empty($sch_run_name)){
        $add_where   .= " AND `as`.run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_run_name}%')";
        $smarty->assign('sch_run_name', $sch_run_name);
    }
}
elseif($sch_chart_type == '2') # 브랜드별 검색
{
    $sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
    $sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
    $sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

    # 브랜드 parent 매칭
    if(!empty($sch_brand)) {
        $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
        $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
    }
    elseif(!empty($sch_brand_g2)) {
        $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
    }

    if(!empty($sch_brand)) {
        $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
        $sch_brand_list     = $brand_list[$sch_brand_g2];
        $add_where         .= " AND `as`.partner='{$sch_brand}'";

        $kind_g1_name               = $kind_model->getKindName($sch_brand_g1);
        $kind_g2_name               = $kind_model->getKindName($sch_brand_g2);
        $company_item               = $company_model->getItem($sch_brand);
        $sch_chart_data['c_name']   = $kind_g1_name." > ".$kind_g2_name." > ".$company_item['c_name'];
        $sch_chart_title            = $company_item['c_name'];
    }
    elseif(!empty($sch_brand_g2))
    {
        $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
        $sch_brand_list     = $brand_list[$sch_brand_g2];
        $add_where         .= " AND `as`.partner IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";

        $kind_g1_name               = $kind_model->getKindName($sch_brand_g1);
        $kind_g2_name               = $kind_model->getKindName($sch_brand_g2);
        $sch_chart_data['c_name']   = $kind_g1_name." > ".$kind_g2_name." > 전체";
        $sch_chart_title            = $kind_g2_name;
    }
    elseif(!empty($sch_brand_g1))
    {
        $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
        $add_where         .= " AND `as`.partner IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";

        $kind_g1_name               = $kind_model->getKindName($sch_brand_g1);
        $sch_chart_data['c_name']   = $kind_g1_name." > 전체 > 전체";
        $sch_chart_title            = $kind_g1_name;
    }
    else
    {
        $sch_chart_data['c_name'] = "전체 > 전체 > 전체";
    }

    $smarty->assign("sch_brand_g1", $sch_brand_g1);
    $smarty->assign("sch_brand_g2", $sch_brand_g2);
    $smarty->assign("sch_brand", $sch_brand);
}

$all_date_sql = "
	SELECT
 		DATE_FORMAT(`allday`.Date, '%Y-%m') as chart_title,
 		DATE_FORMAT(`allday`.Date, '%Y-%m') as chart_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_settle_s_month}' AND '{$sch_settle_e_month}'
	GROUP BY chart_key
	ORDER BY chart_key
";
$all_date_query     = mysqli_query($my_db, $all_date_sql);
$month_name_list    = [];
$table_data_list    = [];
while($all_date_result = mysqli_fetch_assoc($all_date_query))
{
    $month_name_list[] = $all_date_result['chart_title'];
    $table_data_list[$all_date_result['chart_key']] = $sch_chart_data;
}

$chart_data_list    = array(
    array(
        "title" => "광고비",
        "type"  => "bar",
        "color" => "#D99694",
        "data"  => array(),
    ),
    array(
        "title" => "대행수수료",
        "type"  => "bar",
        "color" => "#95B3D7",
        "data"  => array(),
    ),
    array(
        "title" => "횟수",
        "type"  => "line",
        "color" => "#333333",
        "data"  => array(),
    )
);
$agency_data_sql    = "SELECT settle_month, COUNT(as_no) as total_cnt, SUM(wd_price) as total_price, SUM(dp_price) as total_fee FROM agency_settlement `as` WHERE {$add_where} GROUP BY settle_month ORDER BY settle_month";
$agency_data_query  = mysqli_query($my_db, $agency_data_sql);
while($agency_data = mysqli_fetch_assoc($agency_data_query))
{
    $table_data_list[$agency_data['settle_month']]['total_cnt']    = (int)$agency_data['total_cnt'];
    $table_data_list[$agency_data['settle_month']]['total_price']  = (int)$agency_data['total_price'];
    $table_data_list[$agency_data['settle_month']]['total_fee']    = (int)$agency_data['total_fee'];

    $chart_data_list[0]['data'][] = (int)$agency_data['total_price'];
    $chart_data_list[1]['data'][] = (int)$agency_data['total_fee'];
    $chart_data_list[2]['data'][] = (int)$agency_data['total_cnt'];
}

$smarty->assign("sch_chart_title", $sch_chart_title);
$smarty->assign("status_option", getWorkStateOption());
$smarty->assign("is_partner_option", getSettleIsParteOption());
$smarty->assign("agency_option", $agency_option);
$smarty->assign("media_option", $media_option);
$smarty->assign("advertise_type_option", getAdvertiseTypeOption());
$smarty->assign("my_company_name_list", $my_company_name_list);
$smarty->assign("sch_partner_list", $partner_list);
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);
$smarty->assign("month_name_list", json_encode($month_name_list));
$smarty->assign("table_data_list", $table_data_list);
$smarty->assign("chart_data_list", json_encode($chart_data_list));

$smarty->display('agency_chart.html');
?>
