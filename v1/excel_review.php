<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
),
),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "완료보고일")
->setCellValue('B1', "포스팅URL")
->setCellValue('C1', "포스팅제목")
->setCellValue('D1', "포스팅 후 한마디")
->setCellValue('E1', "CODE")
->setCellValue('F1', "구분")
->setCellValue('G1', "업체명")
->setCellValue('H1', "담당자")
->setCellValue('I1', "리뷰마감")
->setCellValue('J1', "카페아이디")
->setCellValue('K1', "닉네임")
->setCellValue('L1', "이름")
->setCellValue('M1', "메모");

// 초기값 세팅
$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

$proc=(isset($_POST['process']))?$_POST['process']:"";
$code=(isset($_GET['code']))?$_GET['code']:"";
$smarty->assign("category",$code);




// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";

$search_url=(isset($_POST['search_url']))?$_POST['search_url']:"";
$search_scompany=isset($_GET['scompany'])?$_GET['scompany']:"";
$search_sname=isset($_GET['sname'])?$_GET['sname']:"0";
$search_snick=isset($_GET['snick'])?$_GET['snick']:"";
$search_susername=isset($_GET['susername'])?$_GET['susername']:"";
$search_spost_title=isset($_GET['spost_title'])?$_GET['spost_title']:"";
$search_spost_url=isset($_GET['spost_url'])?$_GET['spost_url']:"";
$identified=isset($_GET['identified'])?$_GET['identified']:"";

$add_where=" where 1=1";

if(!empty($search_scompany)) {
	$add_where.=" AND company like '%".$search_scompany."%'";
	$smarty->assign("scompany",$search_scompany);
}

if(!empty($search_sname)) {
	$add_where.=" AND p.name like '%".$search_sname."%'";
	$smarty->assign("sname",$search_sname);
}

if(!empty($search_snick)) {
	$add_where.=" AND b.nick like '%".$search_snick."%'";
	$smarty->assign("snick",$search_snick);
}

if(!empty($search_susername)) {
	$add_where.=" AND b.username like '%".$search_susername."%'";
	$smarty->assign("susername",$search_susername);
}

if(!empty($search_spost_title)) {
	$add_where.=" AND r.post_title like '%".$search_spost_title."%'";
	$smarty->assign("spost_title",$search_spost_title);
}

if(!empty($search_spost_url)) {
	$add_where.=" AND r.post_url like '%".$search_spost_url."%'";
	$smarty->assign("spost_url",$search_spost_url);
}

if(!empty($identified)) {
	if($identified == 'n'){
		$add_where.=" AND r.a_no='0'";
		$smarty->assign("identified",$identified);
	}
}


// 리스트 내용
$i=2;
$ttamount=0;
//$rs = mysqli_query($my_db, "SELECT s_no,state,id,s_name,team,position,email,tel,hp FROM staff");
$report_sql="
				select r.r_no, r.r_datetime, r.post_title, r.post_url, r.memo, r.p_no, r.a_no, r.b_no, r.p_code,
				(select kind from promotion p where p.p_no=r.p_no) as kind,
				(select company from promotion p where p.p_no=r.p_no) as company_name,
				(select name from promotion p where p.p_no=r.p_no) as staff_name,
				(select reg_edate from promotion p where p.p_no=r.p_no) as reg_edate,
				(select nick from blog b where b.b_no=r.b_no) as nick,
				(select username from blog b where b.b_no=r.b_no) as username,
				(select b_memo from blog b where b.b_no=r.b_no) as b_memo,
				(select reload from blog b where b.b_no=r.b_no) as b_reload
				from
				(	report r
				left join
					promotion p
					on r.p_no=p.p_no
				)
				left join
					blog b
					on r.b_no=b.b_no
				$add_where
				order by r.r_datetime desc
				";
//exit($promotion_sql);
//echo "<br><br><br><br><br><br><br><br>";
//echo $report_sql;exit;

$report_query= mysqli_query($my_db, $report_sql);
while($dt = mysqli_fetch_array($report_query)){
	// Add some data
	switch ($dt[kind]) {
		case '1' :
					 $kind_name = "체험단";
					 break;
		case '2' :
					 $kind_name = "기자단";
					 break;
		case '3' :
					 $kind_name = "배송체험";
					 break;
	}
	switch ($dt[status]) {
		case '1' :
					 $status_name = "체크대기";
					 break;
		case '2' :
					 $status_name = "체크완료";
					 break;
	}
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$i, $dt[reg_edate])
	->setCellValue('B'.$i, $dt[post_url])
	->setCellValue('C'.$i, $dt[post_title])
	->setCellValue('D'.$i, $dt[memo])
	->setCellValue('E'.$i, $dt[p_code])
	->setCellValue('F'.$i, $kind_name)
	->setCellValue('G'.$i, $dt[company])
	->setCellValue('H'.$i, $dt[staff_name])
	->setCellValue('I'.$i, $dt[reg_edate])
	->setCellValue('J'.$i, $dt[cafe_id])
	->setCellValue('K'.$i, $dt[nick])
	->setCellValue('L'.$i, $dt[username])
	->setCellValue('M'.$i, $dt[b_memo]);
	$i = $i+1;
}
/*$reports[] = array(
		"no"=>$report_array['r_no'],
		"post_url"=>$report_array['post_url'],
		"post_title"=>$report_array['post_title'],
		"memo"=>$report_array['memo'],
		"r_date_ymd"=>date("Y.m.d",strtotime($report_array['r_datetime'])),
		"r_date_hi"=>date("H:i",strtotime($report_array['r_datetime'])),
		"p_no"=>$report_array['p_no'],
		"c_no"=>$report_array['c_no'],
		/*"reg_start"=>date("n/d(".$datey[date("w",strtotime($report_array['reg_sdate']))].")",strtotime($report_array['reg_sdate'])),
		"reg_end"=>date("n/d(".$datey[date("w",strtotime($report_array['reg_edate']))].")",strtotime($report_array['reg_edate'])),
		"awd_date"=>$report_array['awd_date'],
		"award"=>date("n/d(".$datey[date("w",strtotime($report_array['awd_date']))].")",strtotime($report_array['awd_date'])),
		"pres_reward"=>$pres_reward,
		"awd_complete"=>(($report_array['p_state']>3)?"10/10":"-"),
		"reg_num"=>$report_array['reg_num'],
		"s_no"=>$report_array['s_no'],
		"a_no"=>$report_array['a_no'],
		"b_no"=>$report_array['b_no'],
		"p_memo"=>$report_array['p_memo'],
		"kind"=>$report_array['kind'],
		"kind_name"=>$kind['name'][$report_array['kind']],
		"company"=>$report_array['company'],
		"staff_name"=>$report_array['staff_name'],
		"reg_edate"=>$report_array['reg_edate'],
		"cafe_id"=>$report_array['cafe_id'],
		"nick"=>$report_array['nick'],
		"username"=>$report_array['username'],
		"p_memo"=>$report_array['p_memo'],
		"r_memo"=>$report_array['r_memo'],
		"limit_yn"=>$report_array['limit_yn'],
		"reload"=>((empty($report_array['reload']))?"____/__/__":date("Y/m/d H:i:s",strtotime($report_array['reload']))),
	);*/

$objPHPExcel->getActiveSheet()->getStyle('A1:M'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('00F3F3F6');


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setTitle('리뷰 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="review_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
