<?php
include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');

$chk_ev_no = isset($_GET['ev_no']) ? $_GET['ev_no'] : "";

if(empty($chk_ev_no)){
   exit("<script>alert('평가 번호가 없습니다.');location.href='evaluation_setting_list.php';</script>");
}

$chk_relation_sql    = "SELECT ev_r_no, rate FROM evaluation_relation WHERE ev_no='{$chk_ev_no}'";
$chk_relation_query  = mysqli_query($my_db, $chk_relation_sql);
while($chk_relation = mysqli_fetch_assoc($chk_relation_query)){
   $upd_sql = "UPDATE evaluation_system_result SET `rate`='{$chk_relation['rate']}' WHERE ev_no='{$chk_ev_no}' AND ev_r_no='{$chk_relation['ev_r_no']}'";
   mysqli_query($my_db, $upd_sql);
}

exit("<script>alert('적용했습니다.');location.href='evaluation_setting_list.php';</script>");
?>
