<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
include_once('inc/model/Company.php');
include_once('inc/model/ProductCms.php');
include_once('inc/model/Work.php');
include_once('inc/model/Logistics.php');

# Init Model
$company_model      = Company::Factory();
$product_model      = ProductCms::Factory();
$work_model         = Work::Factory();
$logistics_model    = Logistics::Factory();
$folder             = "logistics";
$sub_folder_name    = folderCheck($folder);

# 기본 엑셀 변수 설정
$file_list      = $_FILES['roc_file']['tmp_name'];
$file_name_list = $_FILES['roc_file']['name'];
$work_c_no      = isset($_POST['work_c_no']) ? $_POST['work_c_no'] : "";
$search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
$work_company   = $company_model->getItem($work_c_no);
$regdate        = date('Y-m-d H:i:s');
$req_date       = date('Y-m-d');
$doc_date       = date('Ymd');
$prd_no         = 250;
$doc_idx        = $logistics_model->getNewDocIdx($prd_no);
$roc_c_no       = 5468;

$base_work_data     = array(
    "work_state"    => 3,
    "c_no"          => $work_c_no,
    "c_name"        => $work_company['c_name'],
    "s_no"          => $work_company['s_no'],
    "team"          => $work_company['team'],
    "prd_no"        => $prd_no,
    "quantity"      => 1,
    "task_req_s_no" => $session_s_no,
    "task_req_team" => $session_team,
    "task_run_s_no" => 9,
    "task_run_team" => "00249",
    "linked_table"  => "logistics_management",
    "linked_no"     => "NULL",
    "regdate"       => $regdate,
);

$base_logistic_data = array(
    "work_state"        => 3,
    "doc_no"            => "NULL",
    "w_no"              => "NULL",
    "roc_file_path"     => "NULL",
    "prd_no"            => $prd_no,
    "stock_type"        => 2,
    "prd_type"          => 1,
    "delivery_type"     => 2,
    "subject"           => 12,
    "run_c_no"          => $roc_c_no,
    "req_s_no"          => $session_s_no,
    "req_team"          => $session_team,
    "req_date"          => $req_date,
    "run_s_no"          => 9,
    "run_team"          => "00249",
    "regdate"           => $regdate,
    "quick_c_no"        => 2809,
    "is_quick_memo"     => 1,
    "quick_memo"        => "",
    "quick_req_memo"    => "",
    "reason"            => "쿠팡(로켓배송) 출고",
    "sender"            => "윤종혁",
    "sender_hp"         => "010-6500-1898",
    "sender_type"       => 2809,
    "sender_type_name"  => "위드플레이스",
    "sender_zipcode"    => "10864",
    "sender_addr"       => addslashes("경기도 파주시 교하로 1173 (오도동114-16)"),
    "sender_addr_detail"=> "위드플레이스",
    "receiver_type"     => 5468,
    "receiver_type_name"=> "쿠팡(로켓배송)"
);

$result             = false;
$fail_upload_list   = [];
$fail_prd_list      = [];
$dup_file_list      = [];
foreach($file_list as $file_idx => $file_tmp_name)
{
    $file_name = $file_name_list[$file_idx];

    # 엑셀 파일 읽기
    $excelReader    =   PHPExcel_IOFactory::createReaderForFile($file_tmp_name);
    $excelReader->setReadDataOnly(true);

    $excel          = $excelReader->load($file_tmp_name);
    $objWorksheet   = $excel->setActiveSheetIndex(0);
    $totalRow       = $objWorksheet->getHighestRow();

    $logistics_data = $base_logistic_data;
    $work_data      = $base_work_data;
    $product_data   = [];
    $address_data   = [];
    $quick_req_memo = "";
    $ware_center    = "";

    for($idx = 10; $idx <= $totalRow; $idx++)
    {
        $chk_cell_value = (string)trim(addslashes($objWorksheet->getCell("A{$idx}")->getValue()));

        if($chk_cell_value == '합계'){
            break;
        }

        if($idx == 10){
            $ware_center = (string)trim(addslashes($objWorksheet->getCell("C{$idx}")->getValue()));
            $quick_req_memo = "발주번호 : {$ware_center}";
        }

        if($idx == 13) {
            $address_data['la_no']      = "";
            $address_data['name']       = (string)trim(addslashes($objWorksheet->getCell("C{$idx}")->getValue()));
            $address_data['hp']         = (string)trim(addslashes($objWorksheet->getCell("I{$idx}")->getValue()));
            $address_data['address']    = (string)trim(addslashes($objWorksheet->getCell("D{$idx}")->getValue()));
            $exp_date                   = (string)trim(addslashes($objWorksheet->getCell("F{$idx}")->getValue()));

            $quick_req_memo .= "\r\n물류센터 : {$address_data['name']}\r\n입고예정일자 : {$exp_date}";
            $logistics_data['quick_req_memo'] = addslashes(trim($quick_req_memo));
        }

        if($idx > 21 && $idx % 2 == 0){
            $prd_value      = (string)trim(addslashes($objWorksheet->getCell("B{$idx}")->getValue()));
            $prd_price      = (string)trim(addslashes($objWorksheet->getCell("J{$idx}")->getValue()));
            $prd_qty        = (string)trim(addslashes($objWorksheet->getCell("G{$idx}")->getValue()));

            $product_out_item   = $product_model->getOutsourcingItem($roc_c_no, $prd_value);

            if(!empty($product_out_item))
            {
                $product_item = $product_model->getItem($product_out_item['prd_no']);

                $product_data[] = array(
                    "prd_type"  => 1,
                    "prd_no"    => $product_item['prd_no'],
                    "log_c_no"  => $product_item['log_c_no'],
                    "price"     => $prd_price,
                    "quantity"  => $prd_qty,
                );
            }
        }
    }
    
    if(!empty($product_data) && !empty($address_data))
    {
        if(!empty($ware_center)){
            $chk_logistics_sql      = "SELECT COUNT(*) as cnt FROM logistics_management WHERE quicK_req_memo LIKE '%발주번호 : {$ware_center}%'";
            $chk_logistics_query    = mysqli_query($my_db, $chk_logistics_sql);
            $chk_logistics_result   = mysqli_fetch_assoc($chk_logistics_query);
            $chk_logistics_count    = isset($chk_logistics_result['cnt']) ? $chk_logistics_result['cnt'] : 0;

            if($chk_logistics_count > 0){
                $dup_file_list[] = $file_name;
                continue;
            }
        }

        if($work_model->insert($work_data))
        {
            $new_w_no = $work_model->getInsertId();

            $logistics_data['w_no']         = $new_w_no;
            $logistics_data['doc_no']       = $doc_date."-{$prd_no}-".sprintf('%03d', $doc_idx);
            $logistics_data['quick_memo']   = addslashes("\$문서번호={$logistics_data['doc_no']}\$");

            $tmp_filename   = explode(".", $file_name);
            $r_saveName     = "";
            $md5filename    = md5(time().$tmp_filename[0]);
            $ext            = end($tmp_filename);
            $r_saveName     = $folder. "/" . $sub_folder_name . "/" . $md5filename.".".$ext;

            if(move_uploaded_file($file_tmp_name, $_SERVER['DOCUMENT_ROOT']."/v1/uploads/". $r_saveName)){
                $logistics_data['roc_file_path'] = $r_saveName;
            }

            if($logistics_model->insert($logistics_data))
            {
                $new_lm_no          = $logistics_model->getInsertId();
                $new_address_data   = [];
                $new_product_data   = [];

                $address_data['lm_no']  = $new_lm_no;
                $new_address_data[]     = $address_data;

                foreach($product_data as $product_item)
                {
                    $product_item['lm_no'] = $new_lm_no;
                    $new_product_data[] = $product_item;
                }

                $logistics_model->saveAddress($new_lm_no, [], $new_address_data);
                $logistics_model->saveProduct($new_lm_no, 1, [], $new_product_data);

                $work_model->update(array("w_no" => $new_w_no, "linked_no" => $new_lm_no));
                $result = true;
                $doc_idx++;
            }
        }else{
            $fail_upload_list[] = $file_name;
        }
    }else{
        $fail_prd_list[] = $file_name;
    }
}

if(!empty($fail_upload_list) || !empty($fail_prd_list) || !empty($dup_file_list))
{
    if(!empty($fail_upload_list)){
        echo "업로드 실패 파일들 : <br/>";
        foreach($fail_upload_list as $fail_file_name){
            echo $fail_file_name."<br/>";
        }
    }

    if(!empty($fail_prd_list)){
        echo "상품 등록 안된 파일들 : <br/>";
        foreach($fail_prd_list as $fail_file_name){
            echo $fail_file_name."<br/>";
        }
    }

    if(!empty($dup_file_list)){
        echo "<br/>중복 파일들 : <br/>";
        foreach($dup_file_list as $dup_file_name){
            echo $dup_file_name."<br/>";
        }
    }
}else{
    if($result){
        exit("<script>alert('로켓배송 데이터 반영했습니다.');location.href='logistics_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('로켓배송 데이터 반영에 실패했습니다.');location.href='logistics_management.php?{$search_url}';</script>");
    }
}
?>
