<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/product.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Staff.php');

# Navigation & My Quick
$nav_prd_no  = "67";
$nav_title   = "와이즈 업무 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# Model 선언
$kind_model 	= Kind::Factory();
$staff_model    = Staff::Factory();
$staff_list     = $staff_model->getStaffNameList();

# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where      = "1=1";
$add_rs_where   = "1=1";
$sch_display    = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$sch_prd_g1     = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2     = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd_name   = isset($_GET['sch_prd_name'])?$_GET['sch_prd_name']:"";
$sch_manager    = isset($_GET['sch_manager'])?$_GET['sch_manager']:"";
$sch_schedule   = isset($_GET['sch_schedule'])?$_GET['sch_schedule']:"";
$sch_wd_dp_state= isset($_GET['sch_wd_dp_state'])?$_GET['sch_wd_dp_state']:"";

if (!empty($sch_display)) {
    $add_where .= " AND display='{$sch_display}'";
    $smarty->assign("sch_display", $sch_display);
}

if (!empty($sch_prd_g1)) {
    $add_rs_where .= " AND k_prd1='{$sch_prd_g1}'";
    $smarty->assign("sch_prd_g1", $sch_prd_g1);
}

if (!empty($sch_prd_g2)) {
    $add_rs_where .= " AND k_prd2='{$sch_prd_g2}'";
    $smarty->assign("sch_prd_g2", $sch_prd_g2);
}

if(!empty($sch_prd_name)) {
    $add_where .= " AND title like '%{$sch_prd_name}%'";
    $smarty->assign("sch_prd_name",$sch_prd_name);
}

if(!empty($sch_manager)) {

    $chk_sql    = "SELECT s_no, team_list FROM staff wHERE s_name = '{$sch_manager}' LIMIT 1";
    $chk_query  = mysqli_query($my_db, $chk_sql);
    $chk_result = mysqli_fetch_assoc($chk_query);
    $chk_s_no   = isset($chk_result['s_no']) ? $chk_result['s_no'] : "";
    $chk_teams  = isset($chk_result['team_list']) ? $chk_result['team_list'] : "";

    if(!empty($chk_s_no) && !empty($chk_teams))
    {
        $add_manager   = "";
        $chk_team_list = explode(",", $chk_teams);
        foreach($chk_team_list as $chk_team){
            $chk_staff    = $chk_s_no."_".$chk_team;
            $add_manager .= empty($add_manager) ? "(FIND_IN_SET('{$chk_staff}', task_req_staff) > 0" : " OR FIND_IN_SET('{$chk_staff}', task_req_staff) > 0";
        }
        $add_manager  .= !empty($add_manager) ? ")" : "";
        $add_where    .= " AND {$add_manager}";
    }

    $smarty->assign("sch_manager",$sch_manager);
}

if(!empty($sch_schedule)) {
    if($sch_schedule == '1'){
        $add_where .= " AND is_task_dday = '1'";
    }else{
        $add_where .= " AND is_task_dday != '1'";
    }
    $smarty->assign("sch_schedule",$sch_schedule);
}

if(!empty($sch_wd_dp_state)) {
    $add_where .= " AND wd_dp_state = '{$sch_wd_dp_state}'";
    $smarty->assign("sch_wd_dp_state",$sch_wd_dp_state);
}

$k_code      = "product";
$prd_g1_list = $kind_model->getKindParentList($k_code);
$prd_g2_list = [];
if ($sch_prd_g2 != "" || $sch_prd_g1 != "") {
    $prd_g2_list = $kind_model->getKindGroupChildList($k_code, $sch_prd_g1);
}

$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);

$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ori_ord_type       = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "";

$add_order_by = "";
$ord_type_by  = "";
$order_by_val = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

    if(!empty($ord_type_by))
    {
        $ord_type_list = [];
        if($ord_type == 'work_cnt'){
            $ord_type_list[] = "work_cnt";
        }elseif($ord_type == 'work_date'){
            $ord_type_list[] = "work_date";
        }

        if($ori_ord_type == $ord_type)
        {
            if($ord_type_by == '1'){
                $order_by_val = "ASC";
            }elseif($ord_type_by == '2'){
                $order_by_val = "DESC";
            }
        }else{
            $ord_type_by  = '2';
            $order_by_val = "DESC";
        }

        foreach($ord_type_list as $ord_type_val){
            $add_orderby .= "{$ord_type_val} {$order_by_val}, ";
        }
    }
}

// 정렬순서 토글 & 필드 지정
$add_orderby .= "prd_no DESC";

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);

// 리스트 쿼리
$product_sql = "
    SELECT 
        *,
        (SELECT sub.q_no FROM quick_search as sub WHERE sub.`type`='work' AND sub.s_no='{$session_s_no}' AND sub.prd_no=rs.prd_no) AS quick_prd,    
        (SELECT COUNT(w_no) FROM `work` w WHERE w.prd_no=rs.prd_no) as work_cnt,
        (SELECT DATE_FORMAT(MAX(`regdate`), '%Y-%m-%d') FROM `work` w WHERE w.prd_no=rs.prd_no) as work_date
    FROM 
    (
        SELECT
            prd.*,
            (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code)) AS k_prd1,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code)) AS k_prd1_name,
            (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=prd.k_name_code) AS k_prd2,
            (SELECT k_name FROM kind k WHERE k.k_name_code=prd.k_name_code) AS k_prd2_name
        FROM product prd
        WHERE {$add_where}
    ) AS rs
    WHERE {$add_rs_where}
    ORDER BY {$add_orderby}
";

# 페이징 처리
$cnt_sql    = "SELECT count(*) FROM ({$product_sql}) AS cnt";
$result     = mysqli_query($my_db, $cnt_sql);
$cnt_data   = mysqli_fetch_array($result);
$total_num  = $cnt_data[0];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages      = isset($_GET['page']) ? intval($_GET['page']) : 1;
$num 		= $page_type;
$offset     = ($pages - 1) * $num;
$pagenum    = ceil($total_num / $num);

if ($pages >= $pagenum) {$pages = $pagenum;}
if ($pages <= 0) {$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "product_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $total_num);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

// 리스트 쿼리 결과(데이터)
$product_sql    .= "LIMIT {$offset},{$num}";

$product_query   = mysqli_query($my_db, $product_sql);
$product_list    = [];
$display_option  = getDisplayOption();
$schedule_option = getScheduleOption();
$wd_dp_option    = getWdDpStateOption();
while ($product_array = mysqli_fetch_array($product_query))
{
    $keyword_list       = isset($product_array['keyword']) && !empty($product_array['keyword']) ? explode(',', $product_array['keyword']) : "";
    $keyword            = empty($keyword_list) ? "" : "#".implode(" #", $keyword_list);
    $schedule_name      = ($product_array['is_task_dday'] == '1') ? "O" : "";
    $wd_dp_state_name   = $wd_dp_option[$product_array['wd_dp_state']];
    $display_name       = $display_option[$product_array['display']];

    $task_req_staffs    = explode(',', $product_array['task_req_staff']);
    $task_staff_list    = [];
    if(!empty($task_req_staffs))
    {
        foreach($task_req_staffs as $task_req_staff){
            $task_req_staff_val = explode('_', $task_req_staff);
            $task_req_staff_no  = $task_req_staff_val[0];
            $task_staff_list[]  = $staff_list[$task_req_staff_no];
        }
    }

    $product_list[] = array(
        "prd_no"        => $product_array['prd_no'],
        "display"       => $display_name,
        "k_prd1"        => $product_array['k_prd1'],
        "k_prd1_name"   => $product_array['k_prd1_name'],
        "k_prd2"        => $product_array['k_prd2'],
        "k_prd2_name"   => $product_array['k_prd2_name'],
        "title"         => $product_array['title'],
        "description"   => $product_array['description'],
		"keyword"	    => $keyword,
        "quick_prd"	    => $product_array['quick_prd'],
        "is_schedule"   => $schedule_name,
        "state_name"    => $wd_dp_state_name,
        "task_staff_type"=> $product_array['task_req_staff_type'],
        "task_staff"    => !empty($task_staff_list) ? $task_staff_list : "",
        "work_cnt"      => $product_array['work_cnt'],
        "work_date"     => $product_array['work_date'],
    );
}

$smarty->assign("product_list", $product_list);
$smarty->assign("display_option", $display_option);
$smarty->assign("schedule_option", $schedule_option);
$smarty->assign("wd_dp_option", $wd_dp_option);
$smarty->assign("page_type_option", getPageTypeOption(4));

$smarty->display('product_list.html');
?>
