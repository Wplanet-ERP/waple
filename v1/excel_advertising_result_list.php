<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_date.php');
require('inc/helper/advertising.php');

require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

$lfcr = chr(10) ;
//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "no{$lfcr}작성자")
	->setCellValue('B1', "광고매체")
	->setCellValue('C1', "상품명")
	->setCellValue('D1', "진행상태")
	->setCellValue('E1', "매체대행사")
	->setCellValue('F1', "수수료")
	->setCellValue('G1', "시작일시")
    ->setCellValue('H1', "종료일시")
	->setCellValue('I1', "단가{$lfcr}광고비")
	->setCellValue('J1', "브랜드")
	->setCellValue('K1', "노출수{$lfcr}클릭수{$lfcr}클릭률")
	->setCellValue('L1', "광고비{$lfcr}-대행수수료{$lfcr}VAT별도{$lfcr}(VAT포함)")
    ->setCellValue('M1', "광고상품")
	->setCellValue('N1', "상품유형")
	->setCellValue('O1', "광고소재")
	->setCellValue('P1', "노출수")
	->setCellValue('Q1', "클릭수")
	->setCellValue('R1', "클릭률")
	->setCellValue('S1', "광고비")
;

# 검색조건
$today_val  		= date('Y-m-d');
$month_val  		= date('Y-m-d', strtotime('-1 months'));
$add_where          = "1=1 AND `am`.state IN(3,5)";
$add_date_where		= "1=1";
$add_ar_where       = "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_adv_s_date     = isset($_GET['sch_adv_s_date']) ? $_GET['sch_adv_s_date'] : $month_val;
$sch_adv_e_date     = isset($_GET['sch_adv_e_date']) ? $_GET['sch_adv_e_date'] : $today_val;
$sch_adv_date_type  = isset($_GET['sch_adv_date_type']) ? $_GET['sch_adv_date_type'] : "month";
$sch_date_kind      = isset($_GET['sch_date_kind']) ? $_GET['sch_date_kind'] : "";
$sch_date_s_time    = isset($_GET['sch_date_s_time']) ? $_GET['sch_date_s_time'] : "";
$sch_date_e_time    = isset($_GET['sch_date_e_time']) ? $_GET['sch_date_e_time'] : "";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_media          = 265;
$sch_product        = isset($_GET['sch_product']) ? $_GET['sch_product'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_is_complete    = isset($_GET['sch_is_complete']) ? $_GET['sch_is_complete'] : "";

if(!empty($sch_adv_s_date)){
	$adv_s_datetime  = $sch_adv_s_date." 00:00:00";
	$add_where      .= " AND am.adv_s_date >= '{$adv_s_datetime}'";
}

if(!empty($sch_adv_e_date)){
	$adv_e_datetime  = $sch_adv_e_date." 23:59:59";
	$add_where      .= " AND am.adv_s_date <= '{$adv_e_datetime}'";
}

if($sch_date_kind != ""){
	$add_date_where .= " AND chk_s_day = '{$sch_date_kind}'";
}

if($sch_date_s_time != ""){
	$add_date_where .= " AND chk_s_time = '{$sch_date_s_time}'";
}

if($sch_date_e_time != ""){
	$add_date_where .= " AND chk_e_time = '{$sch_date_e_time}'";
}

if(!empty($sch_no)){
	$add_where .= " AND am.am_no = '{$sch_no}'";
}

if(!empty($sch_media)){
	$add_where .= " AND am.media = '{$sch_media}'";
}

if(!empty($sch_product)){
	$add_where .= " AND am.product = '{$sch_product}'";
}

if(!empty($sch_state)){
	$add_where .= " AND am.state = '{$sch_state}'";
}

if(!empty($sch_agency)){
	$add_where .= " AND am.agency = '{$sch_agency}'";
}

if(!empty($sch_is_complete)){
	if($sch_is_complete == '1'){
		$add_where .= " AND (`am`.price - (SELECT SUM(`ar`.adv_price) FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no) != 0)";
	}elseif($sch_is_complete == '2') {
		$add_where .= " AND (`am`.price - (SELECT SUM(`ar`.adv_price) FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no) = 0)";
	}
}

if(!empty($sch_brand))
{
	$add_where      .= " AND `am`.am_no IN(SELECT `ar`.am_no FROM advertising_result `ar` WHERE `ar`.brand = '{$sch_brand}')";
	$add_ar_where   .= " AND `ar`.brand = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
	$add_where      .= " AND `am`.am_no IN(SELECT `ar`.am_no FROM advertising_result `ar` WHERE `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}'))";
	$add_ar_where   .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
	$add_where      .= " AND `am`.am_no IN(SELECT `ar`.am_no FROM advertising_result `ar` WHERE `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}')))";
	$add_ar_where   .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}



# 정렬기능
$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "adv_s_date";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

$add_orderby    = "ar_cnt ASC";
if(!empty($ord_type))
{
	if($ord_type_by == '1'){
		$orderby_val = "ASC";
	}else{
		$orderby_val = "DESC";
	}

	$add_orderby .= ", `am`.adv_s_date {$orderby_val}";
}

# 쿼리
$adv_management_sql = "
	
    SELECT
		*
	FROM
 	(
		SELECT
			*,
			DATE_FORMAT(`am`.regdate, '%Y/%m/%d') as reg_day,
			DATE_FORMAT(`am`.regdate, '%H:%i') as reg_hour,
			DATE_FORMAT(`am`.adv_s_date, '%Y-%m-%d') as adv_s_day,
			DATE_FORMAT(`am`.adv_s_date, '%H:%i') as adv_s_time,
			DATE_FORMAT(`am`.adv_e_date, '%Y-%m-%d') as adv_e_day,
			DATE_FORMAT(`am`.adv_e_date, '%H:%i') as adv_e_time,
			(SELECT s_name FROM staff s WHERE s.s_no=`am`.reg_s_no) AS reg_s_name,
			IF((SELECT COUNT(`ar`.ar_no) as cnt FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no)=0, 1, 2) as ar_cnt,
			(SELECT SUM(`ar`.adv_price) FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no) as total_adv_price,
			DATE_FORMAT(`am`.adv_s_date, '%w') as chk_s_day,
			DATE_FORMAT(`am`.adv_s_date, '%H') as chk_s_time,
			DATE_FORMAT(`am`.adv_e_date, '%H') as chk_e_time
		FROM advertising_management `am`
		WHERE {$add_where}
	) AS `am`
	WHERE {$add_date_where}
    ORDER BY {$add_orderby}
";
$adv_management_query       = mysqli_query($my_db, $adv_management_sql);
$adv_management_list        = [];
$adv_state_option       = getAdvertisingStateOption();
$adv_state_color_option = getAdvertisingStateColorOption();
$adv_media_option       = getAdvertisingMediaOption();
$adv_agency_option      = getAdvertisingAgencyOption();
$adv_product_option     = getAdvertisingProductOption();
$adv_date_option        = getDayShortOption();
$adv_result_tmp_list    = [];
while($adv_management = mysqli_fetch_assoc($adv_management_query))
{
	$adv_management['state_name']       = isset($adv_state_option[$adv_management['state']]) ? $adv_state_option[$adv_management['state']] : "";
	$adv_management['state_color']      = isset($adv_state_color_option[$adv_management['state']]) ? $adv_state_color_option[$adv_management['state']] : "";
	$adv_management['media_name']       = isset($adv_media_option[$adv_management['media']]) ? $adv_media_option[$adv_management['media']] : "";
	$adv_management['agency_name']      = isset($adv_agency_option[$adv_management['agency']]) ? $adv_agency_option[$adv_management['agency']] : "";
	$adv_management['product_name']     = isset($adv_product_option[$adv_management['product']]) ? $adv_product_option[$adv_management['product']] : "";
	$adv_management['adv_s_date_text']  = $adv_management['adv_s_day']." (".$adv_date_option[date("w", strtotime($adv_management['adv_s_day']))].") ".$adv_management['adv_s_time'];
	$adv_management['adv_e_date_text']  = $adv_management['adv_e_day']." (".$adv_date_option[date("w", strtotime($adv_management['adv_e_day']))].") ".$adv_management['adv_e_time'];
	$adv_management['cal_adv_price']    = $adv_management['price']-$adv_management['total_adv_price'];
	$adv_management['row_cnt']			= 0;

	$adv_result_sql         = "SELECT * FROM advertising_result `ar` WHERE `ar`.am_no='{$adv_management['am_no']}' {$add_ar_where} ORDER BY brand";
	$adv_result_query       = mysqli_query($my_db, $adv_result_sql);
	$adv_result_list        = [];
	while($adv_result = mysqli_fetch_assoc($adv_result_query)){
		$adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["brand_name"]         = $adv_result['brand_name'];
		$adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["fee_per"]            = $adv_management['fee_per'];
		$adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_price"]        = $adv_management['price'];
		$adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_impressions"] += $adv_result['impressions'];
		$adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_click_cnt"]   += $adv_result['click_cnt'];
		$adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_adv_price"]   += $adv_result['adv_price'];
		$adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_cnt"]++;

		$adv_result_list[$adv_result['brand']][] = $adv_result;

		$adv_management['row_cnt']++;
	}
	$adv_management['result_list'] = $adv_result_list;

	$adv_management_list[$adv_management['am_no']] = $adv_management;
}

$adv_result_brand_list  = [];
foreach($adv_result_tmp_list as $am_no => $adv_result_tmp)
{
	foreach($adv_result_tmp as $brand => $adv_brand_data)
	{
		$fee_price      = $adv_brand_data['total_adv_price']*($adv_brand_data['fee_per']/100);
		$cal_price      = $adv_brand_data["total_adv_price"]-$fee_price;
		$cal_price_vat  = $cal_price*1.1;
		$adv_result_brand_list[$am_no][$brand] = array(
			"brand_name"            => $adv_brand_data["brand_name"],
			"impressions"           => $adv_brand_data["total_impressions"],
			"click_cnt"             => $adv_brand_data["total_click_cnt"],
			"click_per"             => $adv_brand_data["total_click_cnt"]/$adv_brand_data["total_impressions"]*100,
			"adv_price"             => $adv_brand_data["total_adv_price"],
			"fee_price"             => $fee_price,
			"cal_price"             => $cal_price,
			"cal_price_vat"         => $cal_price_vat,
			"cnt"                   => $adv_brand_data["total_cnt"]
		);
	}
}


$idx = 2;
$workSheet = $objPHPExcel->setActiveSheetIndex(0);
if($adv_management_list)
{
	$chk_idx 	 = 0;
	$chk_str_idx = 0;
	$chk_end_idx = 0;
	foreach($adv_management_list as $am_no => $adv_management)
	{
		$row_cnt 	 = ($adv_management['row_cnt'] == 0) ? 0 : ($adv_management['row_cnt']-1);
		$chk_str_idx = $idx;
		$chk_end_idx = $idx+$row_cnt;

		$workSheet->mergeCells("A{$chk_str_idx}:A{$chk_end_idx}");
		$workSheet->mergeCells("B{$chk_str_idx}:B{$chk_end_idx}");
		$workSheet->mergeCells("C{$chk_str_idx}:C{$chk_end_idx}");
		$workSheet->mergeCells("D{$chk_str_idx}:D{$chk_end_idx}");
		$workSheet->mergeCells("E{$chk_str_idx}:E{$chk_end_idx}");
		$workSheet->mergeCells("F{$chk_str_idx}:F{$chk_end_idx}");
		$workSheet->mergeCells("G{$chk_str_idx}:G{$chk_end_idx}");
		$workSheet->mergeCells("H{$chk_str_idx}:H{$chk_end_idx}");
		$workSheet->mergeCells("I{$chk_str_idx}:I{$chk_end_idx}");

		$total_price_text = number_format($adv_management['price']).$lfcr.number_format($adv_management['total_adv_price']);

		if ($adv_management['total_adv_price'] > 0 && $adv_management['cal_adv_price'] != 0) {
			$total_price_text .= $lfcr.number_format($adv_management['cal_adv_price']);
		}

		$workSheet
			->setCellValue("A{$chk_str_idx}", $am_no.$lfcr.$adv_management['reg_day'].$lfcr.$adv_management['reg_hour'])
			->setCellValue("B{$chk_str_idx}", $adv_management['media_name'])
			->setCellValue("C{$chk_str_idx}", $adv_management['product_name'])
			->setCellValue("D{$chk_str_idx}", $adv_management['state_name'])
			->setCellValue("E{$chk_str_idx}", $adv_management['agency_name'])
			->setCellValue("F{$chk_str_idx}", number_format($adv_management['fee_per'],1)."%")
			->setCellValue("G{$chk_str_idx}", $adv_management['adv_s_date_text'])
			->setCellValue("H{$chk_str_idx}", $adv_management['adv_e_date_text'])
			->setCellValue("I{$chk_str_idx}", $total_price_text)
		;

		if($row_cnt == 0){
			$workSheet->getRowDimension("{$chk_str_idx}")->setRowHeight(40);
		}

		if(!empty($adv_management['result_list']))
		{
			$adv_result_list = $adv_management['result_list'];
			$brand_str_idx 	 = $idx;
			$chk_brand		 = "";

			foreach($adv_result_list as $adv_brand => $adv_brand_data)
			{
				$brand_idx 	= 0;
				foreach ($adv_brand_data as $adv_brand_result)
				{
					if($brand_idx == 0)
					{
						$chk_brand		= $adv_brand;
						$brand_data		= $adv_result_brand_list[$am_no][$adv_brand];
						$brand_cnt 		= $brand_data['cnt'];
						$brand_row_cnt 	= $brand_cnt-1;
						$brand_end_idx 	= $brand_str_idx+$brand_row_cnt;

						$workSheet->mergeCells("J{$brand_str_idx}:J{$brand_end_idx}");
						$workSheet->mergeCells("K{$brand_str_idx}:K{$brand_end_idx}");
						$workSheet->mergeCells("L{$brand_str_idx}:L{$brand_end_idx}");

						$workSheet
							->setCellValue("J{$brand_str_idx}", $brand_data['brand_name'])
							->setCellValue("K{$brand_str_idx}", number_format($brand_data['impressions']).$lfcr.number_format($brand_data['click_cnt']).$lfcr.number_format($brand_data['click_per'],2)."%")
							->setCellValue("L{$brand_str_idx}", number_format($brand_data['adv_price']).$lfcr.number_format($brand_data['fee_price']).$lfcr.number_format($brand_data['cal_price']).$lfcr."(".number_format($brand_data['cal_price_vat']).")")
						;
					}

					$workSheet
						->setCellValue("M{$brand_str_idx}", $adv_brand_result['adv_product'])
						->setCellValue("N{$brand_str_idx}", $adv_brand_result['adv_prd_type'])
						->setCellValue("O{$brand_str_idx}", $adv_brand_result['adv_material'])
						->setCellValue("P{$brand_str_idx}", number_format($adv_brand_result['impressions']))
						->setCellValue("Q{$brand_str_idx}", number_format($adv_brand_result['click_cnt']))
						->setCellValue("R{$brand_str_idx}", number_format($adv_brand_result['click_per'],2))
						->setCellValue("S{$brand_str_idx}", number_format($adv_brand_result['adv_price']))
					;

					$workSheet->getRowDimension("{$brand_str_idx}")->setRowHeight(40);

					$brand_str_idx++;
					$brand_idx++;
				}
			}
		}

		$idx  = $chk_end_idx+1;
	}
}

$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00343a40');
$objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A2:S{$idx}")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("M2:M{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("Q2:Q{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("R2:R{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("S2:S{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("A1:A{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("I1:I{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("K1:K{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("L1:L{$idx}")->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(14);

$objPHPExcel->getActiveSheet()->getStyle("A2:S{$idx}")->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->setTitle('NOSP 광고결과리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_광고결과 리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
