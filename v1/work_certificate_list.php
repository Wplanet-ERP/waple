<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/Staff.php');
require('inc/helper/work.php');
require('inc/helper/work_extra.php');
require('inc/model/MyQuick.php');

# 경영지원실만 이용
if($session_team != "00211") {
    $smarty->display('access_error.html');
    exit;
}

# 프로세스 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "f_work_state")
{
    $w_no    = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $wc_no   = (isset($_POST['wc_no'])) ? $_POST['wc_no'] : "";
    $value   = (isset($_POST['val'])) ? $_POST['val'] : "";

    if($value == '6'){
        $upd_sql  = "UPDATE `work_certificate` SET work_state = '{$value}', run_date=now() WHERE wc_no = '{$wc_no}';";
        if(!empty($w_no)) {
            $upd_sql .= "UPDATE `work` SET work_state='{$value}', task_run_regdate=now() WHERE w_no='{$w_no}';";
        }
    }else{
        $upd_sql  = "UPDATE `work_certificate` SET work_state = '{$value}' WHERE wc_no = '{$wc_no}';";
        if(!empty($w_no)) {
            $upd_sql .= "UPDATE `work` SET work_state='{$value}' WHERE w_no='{$w_no}';";
        }
    }

    if (mysqli_multi_query($my_db, $upd_sql) == true) {
        echo "진행상태가 변경됬습니다.";
    } else{
        echo "진행상태 변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "f_run_s_no")
{
    $w_no    = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $wc_no   = (isset($_POST['wc_no'])) ? $_POST['wc_no'] : "";
    $value   = (isset($_POST['val'])) ? $_POST['val'] : "";

    $upd_sql  = "UPDATE `work_certificate` SET run_s_no = '{$value}', run_team='00211' WHERE wc_no = '{$wc_no}';";
    if(!empty($w_no)){
        $upd_sql .= "UPDATE `work` SET task_run_s_no='{$value}' WHERE w_no='{$w_no}';";
    }

    if (mysqli_multi_query($my_db, $upd_sql) == true) {
        echo "업무처리자가 변경됬습니다.";
    } else{
        echo "업무처리자 변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "f_task_run")
{
    $w_no    = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $wc_no   = (isset($_POST['wc_no'])) ? $_POST['wc_no'] : "";
    $value   = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    $upd_sql  = "UPDATE `work_certificate` SET task_run = '{$value}' WHERE wc_no = '{$wc_no}';";
    if(!empty($w_no)) {
        $upd_sql .= "UPDATE `work` SET task_run='{$value}' WHERE w_no='{$w_no}';";
    }

    if (mysqli_multi_query($my_db, $upd_sql) == true) {
        echo "업무진행이 변경됬습니다.";
    } else{
        echo "업무진행 변경에 실패했습니다.";
    }
    exit;
}
elseif($process == "f_manager_memo")
{
    $w_no    = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $wc_no   = (isset($_POST['wc_no'])) ? $_POST['wc_no'] : "";
    $value   = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    $upd_sql  = "UPDATE `work_certificate` SET manager_memo = '{$value}' WHERE wc_no = '{$wc_no}';";
    if(!empty($w_no)) {
        $upd_sql .= "UPDATE `work` SET manager_memo='{$value}'WHERE w_no='{$w_no}';";
    }

    if (mysqli_multi_query($my_db, $upd_sql) == true) {
        echo "관리자메모가 변경됬습니다.";
    } else{
        echo "관리자메모 변경에 실패했습니다.";
    }
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "78";
$nav_title   = "증명서 발급";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

#검색 조건
$add_where      = "1=1";

$sch_wc_no      = isset($_GET['sch_wc_no']) ? $_GET['sch_wc_no'] : "";
$sch_work_state = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_doc_no     = isset($_GET['sch_doc_no']) ? $_GET['sch_doc_no'] : "";
$sch_req_name   = isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : "";
$sch_doc_type   = isset($_GET['sch_doc_type']) ? $_GET['sch_doc_type'] : "";
$sch_purpose    = isset($_GET['sch_purpose']) ? $_GET['sch_purpose'] : "";
$sch_run_s_no   = isset($_GET['sch_run_s_no']) ? $_GET['sch_run_s_no'] : "";

if(!empty($sch_wc_no)) {
    $add_where  .= " AND wc.wc_no='{$sch_wc_no}'";
    $smarty->assign("sch_wc_no", $sch_wc_no);
}

if(!empty($sch_work_state)) {
    $add_where  .= " AND wc.work_state='{$sch_work_state}'";
    $smarty->assign("sch_work_state", $sch_work_state);
}

if(!empty($sch_doc_no)) {
    $add_where  .= " AND wc.doc_no LIKE '%{$sch_doc_no}%'";
    $smarty->assign("sch_doc_no", $sch_doc_no);
}

if(!empty($sch_req_name)) {
    $add_where  .= " AND wc.req_name LIKE '%{$sch_req_name}%'";
    $smarty->assign("sch_req_name", $sch_req_name);
}

if(!empty($sch_doc_type)) {
    $add_where  .= " AND wc.doc_type='{$sch_doc_type}'";
    $smarty->assign("sch_doc_type", $sch_doc_type);
}

if(!empty($sch_purpose)) {
    $add_where  .= " AND wc.purpose LIKE '%{$sch_purpose}%'";
    $smarty->assign("sch_purpose", $sch_purpose);
}

if(!empty($sch_run_s_no)) {
    $add_where  .= " AND wc.run_s_no='{$sch_run_s_no}'";
    $smarty->assign("sch_run_s_no", $sch_run_s_no);
}

$certificate_total_sql     = "SELECT count(wc_no) as cnt FROM work_certificate as wc WHERE {$add_where}";
$certificate_total_query   = mysqli_query($my_db, $certificate_total_sql);
$certificate_total_result  = mysqli_fetch_assoc($certificate_total_query);
$certificate_total         = isset($certificate_total_result['cnt']) ? $certificate_total_result['cnt'] : 0;

$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 10;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($certificate_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "work_certificate_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $certificate_total);
$smarty->assign("pagelist", $page);
$smarty->assign("ord_page_type", $page_type);


# 증명서 발급 리스트
$certificate_sql = "
    SELECT 
        *
    FROM work_certificate as wc
    WHERE {$add_where}
    ORDER BY wc_no DESC
    LIMIT {$offset}, {$num}
";
$certificate_query  = mysqli_query($my_db, $certificate_sql);
$doc_type_option    = getCertDocTypeOption();
$certificate_list   = [];
while($certificate_result = mysqli_fetch_assoc($certificate_query))
{
    $certificate_result['doc_type_name'] = isset($doc_type_option[$certificate_result['doc_type']]) ? $doc_type_option[$certificate_result['doc_type']] : "";
    $certificate_list[] = $certificate_result;
}

$staff_model            = Staff::Factory();
$cert_run_staff_list    = $staff_model->getActiveTeamNameStaff("00211");

$smarty->assign("page_option", getPageTypeOption('4'));
$smarty->assign("work_state_option", getWorkStateOption());
$smarty->assign("work_state_color_option", getWorkStateOptionColor());
$smarty->assign("doc_type_option", $doc_type_option);
$smarty->assign("cert_run_staff_option", $cert_run_staff_list);
$smarty->assign("certificate_list", $certificate_list);

$smarty->display('work_certificate_list.html');
?>
