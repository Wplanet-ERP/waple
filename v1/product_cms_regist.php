<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/product_cms.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');
require('inc/model/ProductCmsUnit.php');

# 목록 용 url
$search_url = getenv("QUERY_STRING");
if(!empty($search_url))
{
    $search_url_list = explode("&", $search_url);
    $chk_search_url = [];
    foreach($search_url_list as $sch_url){
        if(strpos($sch_url, "no") === false){
            $sch_url_val = explode("=", $sch_url, 2);
            if(!empty($sch_url_val[0])){
                $chk_search_url[$sch_url_val[0]] = "{$sch_url_val[0]}={$sch_url_val[1]}";
            }
        }
    }

    $search_url = implode("&", $chk_search_url);
}
$smarty->assign("search_url", $search_url);

# Process 처리
$product_model  = ProductCms::Factory();
$relation_model = ProductCms::Factory();
$relation_model->setMainInit("product_cms_relation", "no");
$unit_model     = ProductCmsUnit::Factory();

$process        = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "add_relation")
{
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $prd_no         = isset($_POST['prd_no']) ? $_POST['prd_no'] : "";
    $ins_data       = [];

    if(!empty($_POST['prd_no'])){
        $ins_data['prd_no'] = $_POST['prd_no'];
    }

    if(!empty($_POST['option_no_new'])){
        $unit_item = $unit_model->getItem($_POST['option_no_new']);

        $ins_data['option_no']  = $_POST['option_no_new'];
        $ins_data['log_c_no']   = $unit_item["log_c_no"];
    }

    if(!empty($_POST['quantity_new'])){
        $ins_data['quantity'] = $_POST['quantity_new'];
    }

    $ins_data['priority'] = (!empty($_POST['priority_new'])) ? $_POST['quantity_new'] : $relation_model->getRelationMaxPriority($prd_no);


    if(!$relation_model->insert($ins_data)) {
        echo("<script>alert('상품 구성 추가에 실패 하였습니다');history.back();</script>");
    } else {
        $relation_model->checkUnitLogCompany($prd_no);
        echo("<script>alert('상품 구성을 추가 하였습니다');location.href='product_cms_regist.php?no={$prd_no}&{$search_url}';</script>");
    }
    exit;
}
elseif($process == "f_option_no")
{
    $no         = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value      = (isset($_POST['val'])) ? $_POST['val'] : "";

    $unit_item  = $unit_model->getItem($value);
    $upd_data   = array("no" => $no, "option_no" => $value, "log_c_no" => $unit_item['log_c_no']);

    if (!$relation_model->update($upd_data)){
        echo "품명 저장에 실패 하였습니다.";
    }
    else{
        $relation_model->checkUnitLogCompany($unit_item['prd_no']);
        echo "품명이 저장 되었습니다.";
    }
    exit;
}
elseif($process == "f_quantity")
{
    $no         = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value      = (isset($_POST['val'])) ? $_POST['val'] : "";

    $upd_data   = array("no" => $no, "quantity" => $value);

    if (!$relation_model->update($upd_data))
        echo "수량 저장에 실패 하였습니다.";
    else
        echo "수량이 저장 되었습니다.";
    exit;
}
elseif($process == "f_priority")
{
    $no         = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value      = (isset($_POST['val'])) ? $_POST['val'] : "";

    $upd_data   = array("no" => $no, "priority" => $value);

    if (!$relation_model->update($upd_data))
        echo "우선순위 저장에 실패 하였습니다.";
    else
        echo "우선순위가 저장 되었습니다.";
    exit;
}
elseif($process == "f_delete")
{
    $prd_no     = (isset($_POST['prd_no'])) ? $_POST['prd_no'] : "";
    $rel_no     = (isset($_POST['rel_no'])) ? $_POST['rel_no'] : "";
    $search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $upd_data   = array("no" => $rel_no, "display" => 2);

    if (!$relation_model->update($upd_data)) {
        echo("<script>alert('삭제에 실패 하였습니다.');history.back();</script>");
    }else {
        $relation_model->checkUnitLogCompany($prd_no);
        echo("<script>alert('상품 구성을 삭제 하였습니다');location.href='product_cms_regist.php?no={$prd_no}&{$search_url}';</script>");
    }
    exit;
}
elseif($process == "write")
{
    $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $prd_code_chk   = $product_model->checkDuplicate($_POST['f_code']);
    $regdate        = date("Y-m-d H:i:s");

    if($prd_code_chk > 0){
        exit("<script>alert('중복된 상품코드입니다');location.href='product_cms_regist.php?no={$_POST['prd_no']}&{$search_url}';</script>");
    }

    $f_combined_pack        = isset($_POST['f_combined_pack']) ? "2" : "1";
    $f_combined_except      = isset($_POST['f_combined_except']) ? $_POST['f_combined_except'] : "";
    $f_combined_product     = isset($_POST['f_combined_product']) ? $_POST['f_combined_product'] : "";
    $f_gift_date_type       = isset($_POST['f_gift_date_type']) ? $_POST['f_gift_date_type'] : "2";
    $f_sub_gift_date_type   = isset($_POST['f_sub_gift_date_type']) ? $_POST['f_sub_gift_date_type'] : "2";
    $f_gift_prd             = isset($_POST['f_gift_prd']) ? $_POST['f_gift_prd'] : "";
    $f_gift_dp              = isset($_POST['f_gift_dp']) ? $_POST['f_gift_dp'] : "";
    $f_sub_gift_prd         = isset($_POST['f_sub_gift_prd']) ? $_POST['f_sub_gift_prd'] : "";
    $f_sub_gift_dp          = isset($_POST['f_sub_gift_dp']) ? $_POST['f_sub_gift_dp'] : "";

    $ins_prd_data = array(
        "display"           => $_POST['f_display'],
        "prd_type"          => $_POST['f_prd_type'],
        "k_name_code"       => $_POST['f_prd_g2'],
        "priority"          => $_POST['f_priority'],
        "title"             => addslashes(trim($_POST['f_title'])),
        "manager"           => $_POST['f_manager'],
        "manager_team"      => $_POST['f_manager_team'],
        "writer"            => $session_s_no,
        "writer_team"       => $session_team,
        "c_no"              => $_POST['f_c_no'],
        "prd_code"          => addslashes(trim($_POST['f_code'])),
        "description"       => addslashes(trim($_POST['f_description'])),
        "notice"            => addslashes(trim($_POST['f_notice'])),
        "solo_prd"          => $_POST['f_solo_prd'],
        "combined_pack"     => $f_combined_pack,
        "combined_except"   => $f_combined_except,
        "combined_product"  => $f_combined_product,
        "gift_prd"          => $f_gift_prd,
        "gift_dp"           => $f_gift_dp,
        "sub_gift_prd"      => $f_sub_gift_prd,
        "sub_gift_dp"       => $f_sub_gift_dp,
        "regdate"           => $regdate,
        "writer_date"       => $regdate,
    );

    # 사은품 설정(A)
    $ins_prd_data["gift_date_type"] = $f_gift_date_type;
    $ins_prd_data["gift_s_date"]    = "NULL";
    $ins_prd_data["gift_e_date"]    = "NULL";

    if($f_gift_date_type == '1')
    {
        $f_gift_s_day   = isset($_POST['f_gift_s_day']) ? $_POST['f_gift_s_day'] : "";
        $f_gift_s_hour  = isset($_POST['f_gift_s_hour']) ? $_POST['f_gift_s_hour'] : "";
        $f_gift_s_min   = isset($_POST['f_gift_s_min']) ? $_POST['f_gift_s_min'] : "";
        $f_gift_e_day   = isset($_POST['f_gift_e_day']) ? $_POST['f_gift_e_day'] : "";
        $f_gift_e_hour  = isset($_POST['f_gift_e_hour']) ? $_POST['f_gift_e_hour'] : "";
        $f_gift_e_min   = isset($_POST['f_gift_e_min']) ? $_POST['f_gift_e_min'] : "";

        if(!empty($f_gift_s_day)){
            $ins_prd_data["gift_s_date"] = "{$f_gift_s_day} {$f_gift_s_hour}:{$f_gift_s_min}:00";
        }

        if(!empty($f_gift_e_day)){
            $ins_prd_data["gift_e_date"] = "{$f_gift_e_day} {$f_gift_e_hour}:{$f_gift_e_min}:00";
        }
    }

    # 사은품설정(B)
    $ins_prd_data["sub_gift_date_type"] = $f_sub_gift_date_type;
    $ins_prd_data["sub_gift_s_date"]    = "NULL";
    $ins_prd_data["sub_gift_e_date"]    = "NULL";

    if($f_sub_gift_date_type == '1')
    {
        $f_sub_gift_s_day   = isset($_POST['f_sub_gift_s_day']) ? $_POST['f_sub_gift_s_day'] : "";
        $f_sub_gift_s_hour  = isset($_POST['f_sub_gift_s_hour']) ? $_POST['f_sub_gift_s_hour'] : "";
        $f_sub_gift_s_min   = isset($_POST['f_sub_gift_s_min']) ? $_POST['f_sub_gift_s_min'] : "";
        $f_sub_gift_e_day   = isset($_POST['f_sub_gift_e_day']) ? $_POST['f_sub_gift_e_day'] : "";
        $f_sub_gift_e_hour  = isset($_POST['f_sub_gift_e_hour']) ? $_POST['f_sub_gift_e_hour'] : "";
        $f_sub_gift_e_min   = isset($_POST['f_sub_gift_e_min']) ? $_POST['f_sub_gift_e_min'] : "";

        if(!empty($f_sub_gift_s_day)){
            $ins_prd_data["sub_gift_s_date"] = "{$f_sub_gift_s_day} {$f_sub_gift_s_hour}:{$f_sub_gift_s_min}:00";
        }

        if(!empty($f_sub_gift_e_day)){
            $ins_prd_data["sub_gift_e_date"] = "{$f_sub_gift_e_day} {$f_sub_gift_e_hour}:{$f_sub_gift_e_min}:00";
        }
    }

    if(!$product_model->insert($ins_prd_data)){
        exit("<script>alert('실패했습니다');location.href='product_cms_regist.php?{$search_url}';</script>");
    }
    else
    {
        $prd_no = $product_model->getInsertId();

        # 외주상품코드 저장
        $f_ori_out_no       = [];
        $f_out_no           = isset($_POST['f_out_no']) ? $_POST['f_out_no'] : "";
        $f_out_c_no         = isset($_POST['f_out_c_no']) ? $_POST['f_out_c_no'] : "";
        $f_out_prd_code     = isset($_POST['f_out_prd_code']) ? $_POST['f_out_prd_code'] : "";
        $outsourcing_data   = [];

        if(!empty($f_out_c_no))
        {
            $f_out_idx = 0;
            foreach($f_out_c_no as $out_c_no)
            {
                $out_no     = isset($f_out_no[$f_out_idx]) ? $f_out_no[$f_out_idx] : "";
                $prd_code   = isset($f_out_prd_code[$f_out_idx]) ? $f_out_prd_code[$f_out_idx] : "";

                $outsourcing_data[] = array(
                    "no"        => $out_no,
                    "prd_no"    => $prd_no,
                    "c_no"      => $out_c_no,
                    "type"      => 1,
                    "value"     => $prd_code
                );

                $f_out_idx++;
            }
        }

        if(!empty($outsourcing_data)){
            $product_model->saveOutsourcingData($prd_no, $f_ori_out_no, $outsourcing_data);
        }

        exit("<script>alert('등록하였습니다');location.href='product_cms_list.php?{$search_url}';</script>");
    }
}
elseif($process == "modify")
{
    $f_prd_no           = $_POST['prd_no'];
    $search_url         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $prd_code_chk_sql   = "SELECT prd_no FROM product_cms WHERE prd_code='{$_POST['f_code']}' LIMIT 1";
    $prd_code_chk_query = mysqli_query($my_db, $prd_code_chk_sql);
    $prd_code_chk       = mysqli_fetch_assoc($prd_code_chk_query);

    if(isset($prd_code_chk['prd_no']) && $prd_code_chk['prd_no'] != $f_prd_no){
        exit("<script>alert('중복된 상품코드입니다');location.href='product_cms_regist.php?no={$f_prd_no}&{$search_url}';</script>");
    }

    $is_super   = isset($_POST['is_super_editable']) ? $_POST['is_super_editable'] : "";
    $is_manager = isset($_POST['is_manager_editable']) ? $_POST['is_manager_editable'] : "";

    #합포장 처리
    $f_combined_pack    = isset($_POST['f_combined_pack']) ? "2" : "1";
    $f_combined_except  = isset($_POST['f_combined_except']) ? $_POST['f_combined_except'] : "";
    $f_combined_product = isset($_POST['f_combined_product']) ? $_POST['f_combined_product'] : "";

    # 사은품 설정(A)
    $add_gift_date          = "";
    $f_gift_prd             = isset($_POST['f_gift_prd']) ? $_POST['f_gift_prd'] : "";
    $f_gift_dp              = isset($_POST['f_gift_dp']) ? $_POST['f_gift_dp'] : "";
    $f_gift_date_type       = isset($_POST['f_gift_date_type']) ? $_POST['f_gift_date_type'] : "2";
    $f_gift_s_day           = isset($_POST['f_gift_s_day']) ? $_POST['f_gift_s_day'] : "";
    $f_gift_s_hour          = isset($_POST['f_gift_s_hour']) ? $_POST['f_gift_s_hour'] : "";
    $f_gift_s_min           = isset($_POST['f_gift_s_min']) ? $_POST['f_gift_s_min'] : "";
    $f_gift_e_day           = isset($_POST['f_gift_e_day']) ? $_POST['f_gift_e_day'] : "";
    $f_gift_e_hour          = isset($_POST['f_gift_e_hour']) ? $_POST['f_gift_e_hour'] : "";
    $f_gift_e_min           = isset($_POST['f_gift_e_min']) ? $_POST['f_gift_e_min'] : "";

    # 사은품 설정(B)
    $add_sub_gift_date      = "";
    $f_sub_gift_prd         = isset($_POST['f_sub_gift_prd']) ? $_POST['f_sub_gift_prd'] : "";
    $f_sub_gift_dp          = isset($_POST['f_sub_gift_dp']) ? $_POST['f_sub_gift_dp'] : "";
    $f_sub_gift_date_type   = isset($_POST['f_sub_gift_date_type']) ? $_POST['f_sub_gift_date_type'] : "2";
    $f_sub_gift_s_day       = isset($_POST['f_sub_gift_s_day']) ? $_POST['f_sub_gift_s_day'] : "";
    $f_sub_gift_s_hour      = isset($_POST['f_sub_gift_s_hour']) ? $_POST['f_sub_gift_s_hour'] : "";
    $f_sub_gift_s_min       = isset($_POST['f_sub_gift_s_min']) ? $_POST['f_sub_gift_s_min'] : "";
    $f_sub_gift_e_day       = isset($_POST['f_sub_gift_e_day']) ? $_POST['f_sub_gift_e_day'] : "";
    $f_sub_gift_e_hour      = isset($_POST['f_sub_gift_e_hour']) ? $_POST['f_sub_gift_e_hour'] : "";
    $f_sub_gift_e_min       = isset($_POST['f_sub_gift_e_min']) ? $_POST['f_sub_gift_e_min'] : "";

    if($is_manager || $is_super)
    {
        if($f_gift_date_type == '1')
        {
            $add_gift_date = ", gift_prd = '{$f_gift_prd}', gift_dp='{$f_gift_dp}', gift_date_type='{$f_gift_date_type}'";
            if(!empty($f_gift_s_day)){
                $gift_s_date   = "{$f_gift_s_day} {$f_gift_s_hour}:{$f_gift_s_min}:00";
                $add_gift_date .= ",gift_s_date='{$gift_s_date}'";
            }else{
                $add_gift_date .= ",gift_s_date=null";
            }

            if(!empty($f_gift_e_day)){
                $gift_e_date   = "{$f_gift_e_day} {$f_gift_e_hour}:{$f_gift_e_min}:00";
                $add_gift_date .= ",gift_e_date='{$gift_e_date}'";
            }else{
                $add_gift_date .= ",gift_e_date=null";
            }
        }else{
            $add_gift_date = ", gift_prd = '{$f_gift_prd}', gift_dp='{$f_gift_dp}', gift_date_type='{$f_gift_date_type}', gift_s_date=null, gift_e_date=null";
        }

        if($f_sub_gift_date_type == '1')
        {
            $add_sub_gift_date = ", sub_gift_prd = '{$f_sub_gift_prd}', sub_gift_dp='{$f_sub_gift_dp}', sub_gift_date_type='{$f_sub_gift_date_type}'";
            if(!empty($f_sub_gift_s_day)){
                $sub_gift_s_date    = "{$f_sub_gift_s_day} {$f_sub_gift_s_hour}:{$f_sub_gift_s_min}:00";
                $add_sub_gift_date .= ",sub_gift_s_date='{$sub_gift_s_date}'";
            }else{
                $add_sub_gift_date .= ",sub_gift_s_date=null";
            }

            if(!empty($f_sub_gift_e_day)){
                $sub_gift_e_date    = "{$f_sub_gift_e_day} {$f_sub_gift_e_hour}:{$f_sub_gift_e_min}:00";
                $add_sub_gift_date .= ",sub_gift_e_date='{$sub_gift_e_date}'";
            }else{
                $add_sub_gift_date .= ",sub_gift_e_date=null";
            }
        }else{
            $add_sub_gift_date = ", sub_gift_prd = '{$f_sub_gift_prd}', sub_gift_dp='{$f_sub_gift_dp}', sub_gift_date_type='{$f_sub_gift_date_type}', sub_gift_s_date=null, sub_gift_e_date=null";
        }
    }

    # 외주상품코드 저장
    $f_ori_out_no       = isset($_POST['f_ori_out_no']) ? explode(",",$_POST['f_ori_out_no']) : "";
    $f_out_no           = isset($_POST['f_out_no']) ? $_POST['f_out_no'] : "";
    $f_out_c_no         = isset($_POST['f_out_c_no']) ? $_POST['f_out_c_no'] : "";
    $f_out_prd_code     = isset($_POST['f_out_prd_code']) ? $_POST['f_out_prd_code'] : "";
    $outsourcing_data   = [];

    if(!empty($f_out_c_no))
    {
        $f_out_idx = 0;
        foreach($f_out_c_no as $out_c_no)
        {
            $out_no     = isset($f_out_no[$f_out_idx]) ? $f_out_no[$f_out_idx] : "";
            $prd_code   = isset($f_out_prd_code[$f_out_idx]) ? $f_out_prd_code[$f_out_idx] : "";

            $outsourcing_data[] = array(
                "no"        => $out_no,
                "prd_no"    => $f_prd_no,
                "c_no"      => $out_c_no,
                "type"      => 1,
                "value"     => $prd_code
            );

            $f_out_idx++;
        }
    }

    $upd_sql = "";
    if($is_super){
        $upd_sql = "
            UPDATE product_cms SET
                display         = '{$_POST['f_display']}',
                prd_type        = '{$_POST['f_prd_type']}',
                k_name_code     = '{$_POST['f_prd_g2']}',
                priority        = '{$_POST['f_priority']}',
                title           = '".addslashes(trim($_POST['f_title']))."',
                manager         = '{$_POST['f_manager']}',
                manager_team    = '{$_POST['f_manager_team']}',
                c_no            = '{$_POST['f_c_no']}',
                solo_prd        = '{$_POST['f_solo_prd']}',
                prd_code        = '".addslashes(trim($_POST['f_code']))."',
			    description     = '".addslashes(trim($_POST['f_description']))."',
			    notice          = '".addslashes(trim($_POST['f_notice']))."',
                combined_pack   ='{$f_combined_pack}',
                combined_except ='{$f_combined_except}',
                combined_product='{$f_combined_product}'
                {$add_gift_date}
                {$add_sub_gift_date}
            WHERE
                prd_no = '{$f_prd_no}'
        ";
    }elseif($is_manager){
        $upd_sql = "
            UPDATE product_cms SET
                display         = '{$_POST['f_display']}',
                prd_type        = '{$_POST['f_prd_type']}',
                k_name_code     = '{$_POST['f_prd_g2']}',
                priority        = '{$_POST['f_priority']}',
                manager         = '{$_POST['f_manager']}',
                manager_team    = '{$_POST['f_manager_team']}',
                description     = '".addslashes(trim($_POST['f_description']))."'
                {$add_gift_date}
                {$add_sub_gift_date}
            WHERE
                prd_no = '{$f_prd_no}'
        ";
    }

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('실패했습니다');location.href='product_cms_regist.php?no={$f_prd_no}&{$search_url}';</script>");
    }else{
        $product_model->saveOutsourcingData($f_prd_no, $f_ori_out_no, $outsourcing_data);
        exit("<script>alert('수정하였습니다');location.href='product_cms_regist.php?no={$f_prd_no}&{$search_url}';</script>");
    }
}
elseif($process == "memo")
{
    $prd_no     = (isset($_POST['prd_no'])) ? $_POST['prd_no'] : "";
    $mtxt       = (isset($_POST['f_prd_memo'])) ? addslashes($_POST['f_prd_memo']) : "";
    $search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $product_model->update(array("prd_no" => $prd_no, "prd_memo" => $mtxt, "prd_memo_date" => date("Y-m-d H:i:s")));

    exit("<script>alert('메모를 등록하였습니다');location.href='product_cms_regist.php?no={$prd_no}&{$search_url}';</script>");
}
else
{
    # 초기 설정
    $prd_no         = isset($_GET['no']) ? $_GET['no'] : "";
    $mode           = (!$prd_no) ? "write" : "modify";
    $kind_model     = Kind::Factory();
    $cms_code       = "product_cms";
    $prd_total_list = $kind_model->getKindGroupList($cms_code);
    $prd_g1_list    = $prd_total_list[''];
    $prd_g2_list    = [];

    # 권한
    $is_super_editable   = false;
    $is_manager_editable = false;

    if($mode == "modify")
    {
        $product_cms_sql = "
            SELECT
                *,
                (SELECT depth FROM team t WHERE t.team_code=prd_cms.manager_team) AS t_depth,
                (SELECT s_name FROM staff s WHERE s.s_no=prd_cms.manager) AS manager_name,
                (SELECT c.c_name FROM company c WHERE c.c_no=prd_cms.c_no) AS c_name,
                (SELECT c.c_name FROM company c WHERE c.c_no=prd_cms.log_c_no) AS log_c_name,
                (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd_cms.k_name_code)) AS k_prd1,
                (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=prd_cms.k_name_code) AS k_prd2,
                (SELECT sub.title FROM product_cms sub WHERE sub.prd_no=prd_cms.combined_product) as combined_prd_name,
                (SELECT sub.title FROM product_cms sub WHERE sub.prd_no=prd_cms.solo_prd) as solo_prd_name
            FROM product_cms prd_cms
            WHERE prd_cms.prd_no='{$prd_no}'
        ";
        $product_cms_query = mysqli_query($my_db, $product_cms_sql);
        $product_cms = mysqli_fetch_array($product_cms_query);

        # 2차 상품
        $prd_g2_list    = !empty($product_cms['k_prd1']) ? $prd_total_list[$product_cms['k_prd1']] : [];
        $t_label 	    = getTeamFullName($my_db, $product_cms['t_depth'], $product_cms['manager_team']);
        $manager_label 	= ($t_label) ? "{$product_cms['manager_name']} ({$t_label})" : "";

        # 외주상품코드
        $out_product_sql    = "SELECT *, (SELECT c.c_name FROM company c WHERE c.c_no=pco.c_no ) as c_name FROM product_cms_outsourcing pco WHERE prd_no='{$prd_no}' AND `type`='1'";
        $out_product_query  = mysqli_query($my_db, $out_product_sql);
        $out_product_list   = [];
        $ori_product_list   = [];
        while($out_product = mysqli_fetch_assoc($out_product_query))
        {
            $ori_product_list[] = $out_product['no'];
            $out_product_list[] = $out_product;
        }
        $f_ori_out_no   = !empty($ori_product_list) ? implode(",", $ori_product_list) : "";
        $out_key        = !empty($ori_product_list) ? count($ori_product_list) : 0;

        $smarty->assign(
            array(
                "f_prd_no"              => $product_cms['prd_no'],
                "f_display"             => $product_cms['display'],
                "f_prd_type"            => $product_cms['prd_type'],
                "f_prd_g1"              => $product_cms['k_prd1'],
                "f_prd_g2"              => $product_cms['k_prd2'],
                "f_priority"            => $product_cms['priority'],
                "f_title"               => stripslashes($product_cms['title']),
                "f_notice"              => $product_cms['notice'],
                "f_manager"             => $product_cms['manager'],
                "f_manager_team"        => $product_cms['manager_team'],
                "f_manager_label"       => $manager_label,
                "f_c_no"                => $product_cms['c_no'],
                "f_c_name"              => $product_cms['c_name'],
                "f_code"                => $product_cms['prd_code'],
                "f_description"         => stripslashes($product_cms['description']),
                "f_prd_memo"            => $product_cms['prd_memo'],
                "f_prd_memo_day"        => date("Y/m/d",strtotime($product_cms['prd_memo_date'])),
                "f_prd_memo_time"       => date("H:i",strtotime($product_cms['prd_memo_date'])),
                "f_combined_pack"       => $product_cms['combined_pack'],
                "f_combined_except"     => $product_cms['combined_except'],
                "f_combined_product"    => $product_cms['combined_product'],
                "f_combined_prd_name"   => $product_cms['combined_prd_name'],
                "f_solo_prd"            => $product_cms['solo_prd'],
                "f_solo_prd_name"       => $product_cms['solo_prd_name'],
                "f_gift_prd"            => $product_cms['gift_prd'],
                "f_gift_dp"             => $product_cms['gift_dp'],
                "f_gift_date_type"      => $product_cms['gift_date_type'],
                "f_sub_gift_prd"        => $product_cms['sub_gift_prd'],
                "f_sub_gift_dp"         => $product_cms['sub_gift_dp'],
                "f_sub_gift_date_type"  => $product_cms['sub_gift_date_type'],
                "f_log_c_no"            => $product_cms['log_c_no'],
                "f_log_c_name"          => ($product_cms['log_c_no'] > 0) ? $product_cms['log_c_name'] : "발송불가! 물류업체(발송지)를 동일하게 설정하세요.",
                "f_ori_out_no"          => $f_ori_out_no,
                "out_key"               => $out_key,
                "outsourcing_list"      => $out_product_list,
            )
        );

        if(!empty($product_cms['gift_s_date'])) {
            $smarty->assign("f_gift_s_day", date("Y-m-d", strtotime($product_cms['gift_s_date'])));
            $smarty->assign("f_gift_s_hour", date("H", strtotime($product_cms['gift_s_date'])));
            $smarty->assign("f_gift_s_min", date("i", strtotime($product_cms['gift_s_date'])));
        }

        if(!empty($product_cms['gift_e_date'])) {
            $smarty->assign("f_gift_e_day", date("Y-m-d", strtotime($product_cms['gift_e_date'])));
            $smarty->assign("f_gift_e_hour", date("H", strtotime($product_cms['gift_e_date'])));
            $smarty->assign("f_gift_e_min", date("i", strtotime($product_cms['gift_e_date'])));
        }

        if(!empty($product_cms['sub_gift_s_date'])) {
            $smarty->assign("f_sub_gift_s_day", date("Y-m-d", strtotime($product_cms['sub_gift_s_date'])));
            $smarty->assign("f_sub_gift_s_hour", date("H", strtotime($product_cms['sub_gift_s_date'])));
            $smarty->assign("f_sub_gift_s_min", date("i", strtotime($product_cms['sub_gift_s_date'])));
        }

        if(!empty($product_cms['sub_gift_e_date'])) {
            $smarty->assign("f_sub_gift_e_day", date("Y-m-d", strtotime($product_cms['sub_gift_e_date'])));
            $smarty->assign("f_sub_gift_e_hour", date("H", strtotime($product_cms['sub_gift_e_date'])));
            $smarty->assign("f_sub_gift_e_min", date("i", strtotime($product_cms['sub_gift_e_date'])));
        }

        // 발주 품목으로 구성된 상품구성 직원가져오기
        $product_relation_sql = "
            SELECT
                *,
                (SELECT c.c_name FROM company c WHERE c.c_no = pcr.log_c_no) AS log_c_name,
                (SELECT pcu.sup_c_no FROM product_cms_unit pcu WHERE pcu.no = pcr.option_no) AS sup_c_no,
                (SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.no = pcr.option_no) AS option_name
            FROM product_cms_relation pcr
            WHERE pcr.prd_no = '{$prd_no}' AND pcr.display = '1'
            ORDER BY pcr.priority ASC
        ";

        $product_relation_query = mysqli_query($my_db,$product_relation_sql);
        $product_relation_list  = [];
        while($product_relation_array = mysqli_fetch_array($product_relation_query)) {
            $product_relation_list[] = array(
                "no"            => $product_relation_array['no'],
                "prd_no"        => $product_relation_array['prd_no'],
                "sup_c_no"      => $product_relation_array['sup_c_no'],
                "log_c_no"      => $product_relation_array['log_c_no'],
                "log_c_name"    => $product_relation_array['log_c_name'],
                "option_no"     => $product_relation_array['option_no'],
                "option_name"   => $product_relation_array['option_name'],
                "quantity"      => $product_relation_array['quantity'],
                "priority"      => $product_relation_array['priority']
            );
        }
        $smarty->assign("product_relation_list", $product_relation_list);

        if(permissionNameCheck($session_permission,"마스터관리자") || $session_s_no == '17' || $session_s_no == '22'){
            $is_super_editable = true;
        }

        if($session_s_no == $product_cms['manager']){
            $is_manager_editable = true;
        }
    }
    else
    {
        $is_super_editable = true;
    }

    $supply_commerce_sql   = "SELECT DISTINCT pcu.sup_c_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no),'(주)','') as c_name FROM product_cms_unit pcu WHERE pcu.display = 1 ORDER BY c_name ASC";
    $supply_commerce_query = mysqli_query($my_db, $supply_commerce_sql);
    $supply_commerce_list  = [];
    while($supply_commerce = mysqli_fetch_assoc($supply_commerce_query)){
        $supply_commerce_list[] = array($supply_commerce['c_name'], $supply_commerce['sup_c_no']);
    }

    $smarty->assign("prd_no", $prd_no);
    $smarty->assign("mode", $mode);
    $smarty->assign("prd_g1_list", $prd_g1_list);
    $smarty->assign("prd_g2_list", $prd_g2_list);
    $smarty->assign('prd_type_option', getPrdTypeOption());
    $smarty->assign('day_list', getDayOption());
    $smarty->assign('hour_list', getHourOption());
    $smarty->assign('min_list', getMinOption());
    $smarty->assign("supply_commerce_list", $unit_model->getDistinctUnitCompanyData("sup_c_no"));
    $smarty->assign("product_cms_unit_list", $unit_model->getSupUnitList());
    $smarty->assign("is_super_editable", $is_super_editable);
    $smarty->assign("is_manager_editable", $is_manager_editable);
}

$smarty->display('product_cms_regist.html');
?>
