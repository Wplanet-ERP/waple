<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_return.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:A2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("B1:B2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("C1:C2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("D1:D2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("E1:E2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("F1:F2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("G1:M1");

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "원택배사\r\n원운송장번호")
    ->setCellValue('B1', "회수 주문번호\r\n회수자명\r\n회수자 전화")
    ->setCellValue('C1', "회수지")
    ->setCellValue('D1', "구분")
    ->setCellValue('E1', "회수접수 택배사")
    ->setCellValue('F1', "회수접수 운송장번호")
    ->setCellValue('G1', "위드플레이스 처리결과")
    ->setCellValue('G2', "회수처리일")
    ->setCellValue('H2', "재고관리코드(SKU)")
    ->setCellValue('I2', "<수량>")
    ->setCellValue('J2', "처리구분")
    ->setCellValue('K2', "메모")
    ->setCellValue('L2', "위드처리자")
    ->setCellValue('M2', "택배비\r\n(동봉금)")
;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');

$objPHPExcel->getActiveSheet()->getStyle('A1:F2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('0016365C');
$objPHPExcel->getActiveSheet()->getStyle('G2:M2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('0016365C');
$objPHPExcel->getActiveSheet()->getStyle('G1:M1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00A6A6A6');
$objPHPExcel->getActiveSheet()->getStyle('A1:M2')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:M2")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("A1:M2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:M2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:M2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


# 검색조건 처리
$add_where          = "1=1";

$sch_step           = isset($_GET['sch_step']) ? $_GET['sch_step'] : "";
$sch_req_s_date 	= isset($_GET['sch_req_s_date']) ? $_GET['sch_req_s_date'] : "";
$sch_req_e_date 	= isset($_GET['sch_req_e_date']) ? $_GET['sch_req_e_date'] : "";
$sch_return_state   = isset($_GET['sch_return_state']) ? $_GET['sch_return_state'] : "";
$sch_parent_ord_no  = isset($_GET['sch_parent_ord_no']) ? $_GET['sch_parent_ord_no'] : "";
$sch_parent_dp_c_no = isset($_GET['sch_parent_dp_c_no']) ? $_GET['sch_parent_dp_c_no'] : "";
$sch_wise_dp        = isset($_GET['sch_wise_dp']) ? $_GET['sch_wise_dp'] : "";
$sch_recipient 		= isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp 	= isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_return_purpose = isset($_GET['sch_return_purpose']) ? $_GET['sch_return_purpose'] : "";
$sch_return_type    = isset($_GET['sch_return_type']) ? $_GET['sch_return_type'] : "";
$sch_req_name 		= isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : "";
$sch_run_s_date     = isset($_GET['sch_run_s_date']) ? $_GET['sch_run_s_date'] : "";
$sch_run_e_date 	= isset($_GET['sch_run_e_date']) ? $_GET['sch_run_e_date'] : "";
$sch_run_date_null  = isset($_GET['sch_run_date_null']) ? $_GET['sch_run_date_null'] : "";
$sch_with_date 	    = isset($_GET['sch_with_date']) ? $_GET['sch_with_date'] : "";
$sch_with_date_null = isset($_GET['sch_with_date_null']) ? $_GET['sch_with_date_null'] : "";
$sch_return_img	    = isset($_GET['sch_return_img']) ? $_GET['sch_return_img'] : "";
$sch_delivery_type  = isset($_GET['sch_delivery_type']) ? $_GET['sch_delivery_type'] : "";
$sch_delivery_no    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
$sch_delivery_no_null   = isset($_GET['sch_delivery_no_null']) ? $_GET['sch_delivery_no_null'] : "";
$sch_run_state      = isset($_GET['sch_run_state']) ? $_GET['sch_run_state'] : "";
$sch_run_memo       = isset($_GET['sch_run_memo']) ? $_GET['sch_run_memo'] : "";
$sch_run_name       = isset($_GET['sch_run_name']) ? $_GET['sch_run_name'] : "";
$sch_with_name      = isset($_GET['sch_with_name']) ? $_GET['sch_with_name'] : "";

if(!empty($sch_req_s_date)){
    $add_where .= " AND r.return_req_date >= '{$sch_req_s_date}'";
}

if(!empty($sch_req_e_date)){
    $add_where .= " AND r.return_req_date <= '{$sch_req_e_date}'";
}

if(!empty($sch_return_state)){
    $add_where .= " AND r.return_state = '{$sch_return_state}'";
}

if(!empty($sch_parent_ord_no)){
    $add_where .= " AND r.parent_order_number = '{$sch_parent_ord_no}'";
}

if(!empty($sch_wise_dp)){
    $add_where .= " AND r.dp_c_no IN(2280, 5468, 5469, 5475, 5476, 5477, 5478 ,5479, 5480, 5481, 5482, 5939, 6002)";
}else{
    if(!empty($sch_parent_dp_c_no)){
        $add_where .= " AND r.dp_c_no='{$sch_parent_dp_c_no}'";
    }
}

if(!empty($sch_recipient)){
    $add_where .= " AND r.recipient like '%{$sch_recipient}%'";
}

if(!empty($sch_recipient_hp)){
    $add_where .= " AND r.recipient_hp LIKE '%{$sch_recipient_hp}%'";
}

if(!empty($sch_return_purpose)){
    $add_where .= " AND r.return_purpose = '{$sch_return_purpose}'";
}

if(isset($_GET['sch_return_type']) && $sch_return_type != ""){
    $add_where .= " AND r.return_type = '{$sch_return_type}'";
}

if(!empty($sch_req_name)){
    $add_where .= " AND r.s_no IN(SELECT s.s_no FROM staff s WHERE s.name like '%{$sch_req_name}%')";
}

if(!empty($sch_run_date_null)){
    $add_where .= " AND (r.return_date IS NULL OR r.return_date = '')";
}else{
    if(!empty($sch_run_s_date)){
        $add_where .= " AND r.return_date >='{$sch_run_s_date}'";
    }

    if(!empty($sch_run_e_date)){
        $add_where .= " AND r.return_date <='{$sch_run_e_date}'";
    }
}

if(!empty($sch_with_date_null)){
    $add_where .= " AND (r.return_with_date IS NULL OR r.return_with_date = '')";
}else{
    if(!empty($sch_with_date)){
        $add_where .= " AND r.return_with_date ='{$sch_with_date}'";
    }
}

if(!empty($sch_return_img)){
    if($sch_return_img == '1'){
        $add_where .= " AND (r.return_img_path_1 != '' AND r.return_img_path_2 != '')";
    }elseif($sch_return_img == '2'){
        $add_where .= " AND (r.return_img_path_1 != '' OR r.return_img_path_2 != '')";
    }elseif($sch_return_img == '3'){
        $add_where .= " AND (r.return_img_path_1 = '' AND r.return_img_path_2 != '') OR (r.return_img_path_1 != '' AND r.return_img_path_2 = '')";
    }elseif($sch_return_img == '4'){
        $add_where .= " AND (r.return_img_path_1 = '' AND r.return_img_path_2 = '')";
    }
}

if(!empty($sch_delivery_type)){
    $add_where .= " AND r.return_delivery_type='{$sch_delivery_type}'";
}

if(!empty($sch_delivery_no_null))
{
    $add_where .= " AND (r.return_delivery_no IS NULL OR r.return_delivery_no = '')";
}else{
    if(!empty($sch_delivery_no)){
        $add_where .= " AND (r.return_delivery_no like '%{$sch_delivery_no}%' OR r.return_delivery_no2 like '%{$sch_delivery_no}%')";
    }
}

if(!empty($sch_delivery_fee)){
    if($sch_delivery_fee == '1'){
        $add_where .= " AND r.return_delivery_fee > 0";
    }elseif($sch_delivery_fee == '2'){
        $add_where .= " AND r.return_delivery_fee = 0";
    }
}

if(!empty($sch_run_state)){
    $add_where .= " AND r.run_state = '{$sch_run_state}'";
}

if(!empty($sch_run_memo)){
    $add_where .= " AND r.run_memo like '%{$sch_run_memo}%'";
}

if(!empty($sch_run_name)){
    $add_where .= " AND r.run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_run_name}%')";
}

if(!empty($sch_with_name)){
    $add_where .= " AND r.run_with_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_with_name}%')";
}

#상세검색[상품등]
$sch_prd_g1	  = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	  = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	  = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_prd_name = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_default  = isset($_GET['sch_default']) ? $_GET['sch_default'] : "Y";
$sch_detail   = isset($_GET['sch_detail']) ? $_GET['sch_detail'] : "N";

if (!empty($sch_prd) && $sch_prd != "0") { // 상품
    $add_where .= " AND r.prd_no='".$sch_prd."'";
}else{
    if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND r.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND r.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

if(!empty($sch_prd_name)){
    $add_where .= " AND r.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.title like '%{$sch_prd_name}%')";
}

# 불량사유 검색
$sch_bad_reason_g1      = isset($_GET['sch_bad_reason_g1']) ? $_GET['sch_bad_reason_g1'] : "";
$sch_bad_reason         = isset($_GET['sch_bad_reason']) ? $_GET['sch_bad_reason'] : "";

if(!empty($sch_bad_reason_g1)){
    $add_where .= " AND r.bad_reason IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_bad_reason_g1}' AND k.display='1')";
}

if(!empty($sch_bad_reason)){
    $add_where .= " AND r.bad_reason = '{$sch_bad_reason}'";
}

$sch_r_no 	        = isset($_GET['sch_r_no']) ? $_GET['sch_r_no'] : "";
$sch_order_number   = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_brand_name     = isset($_GET['sch_brand_name']) ? $_GET['sch_brand_name'] : "";
$sch_return_reason  = isset($_GET['sch_return_reason']) ? $_GET['sch_return_reason'] : "";
$sch_result_reason  = isset($_GET['sch_result_reason']) ? $_GET['sch_result_reason'] : "";

if(!empty($sch_r_no)){
    $add_where .= " AND r.r_no='{$sch_r_no}'";
}

if(!empty($sch_order_number)){
    $add_where .= " AND r.order_number='{$sch_order_number}'";
}

if(!empty($sch_brand_name)){
    $add_where .= " AND r.c_name like '%{$sch_brand_name}%'";
}

if(!empty($sch_return_reason)){
    $add_where .= " AND r.return_reason='{$sch_return_reason}'";
}

if(!empty($sch_result_reason)){
    $add_where .= " AND r.result_reason='{$sch_result_reason}'";
}

#Step 03 처리
$sch_run_s_no = isset($_GET['sch_run_s_no']) ? $_GET['sch_run_s_no'] : "";

if(!empty($sch_run_s_no)){
    $add_where .= " AND r.run_s_no='{$sch_run_s_no}'";
}

if(!empty($sch_step))
{
    switch($sch_step){
        case "step04":
            $step_04_delivery_no    = isset($_GET['step_04_delivery_no']) ? $_GET['step_04_delivery_no'] : "";
            $step_04_return_deli_no = isset($_GET['step_04_return_deli_no']) ? $_GET['step_04_return_deli_no'] : "";
            $step_04_order_number   = isset($_GET['step_04_order_number']) ? $_GET['step_04_order_number'] : "";
            $step_04_recipient      = isset($_GET['step_04_recipient']) ? $_GET['step_04_recipient'] : "";
            $step_04_recipient_hp   = isset($_GET['step_04_recipient_hp']) ? $_GET['step_04_recipient_hp'] : "";
            $step_04_zipcode        = isset($_GET['step_04_zipcode']) ? $_GET['step_04_zipcode'] : "";
            $step_04_recipient_addr = isset($_GET['step_04_recipient_addr']) ? $_GET['step_04_recipient_addr'] : "";

            if(!empty($step_04_delivery_no)){
                $add_where .= " AND r.parent_order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery WHERE delivery_no='{$step_04_delivery_no}')";
            }

            if(!empty($step_04_return_deli_no)){
                $add_where .= " AND (r.return_delivery_no='{$step_04_return_deli_no}' OR r.return_delivery_no2='{$step_04_return_deli_no}')";
            }

            if(!empty($step_04_order_number)){
                $add_where .= " AND r.parent_order_number='{$step_04_order_number}'";
            }

            if(!empty($step_04_recipient)){
                $add_where .= " AND r.recipient LIKE '%{$step_04_recipient}%'";
            }

            if(!empty($step_04_recipient_hp)){
                $add_where .= " AND r.recipient_hp LIKE '%{$step_04_recipient_hp}%'";
            }

            if(!empty($step_04_zipcode)){
                $add_where .= " AND r.zip_code='{$step_04_zipcode}'";
            }

            if(!empty($step_04_recipient_addr)){
                $add_where .= " AND r.recipient_addr LIKE '%{$step_04_recipient_addr}%'";
            }

            break;
        case "step05":
            $step_date    = isset($_GET['step_date']) ? $_GET['step_date'] : "";
            $step_05_date = $step_date;
            $add_where   .= " AND r.return_req_date ='{$step_05_date}' AND return_state='1'";
            break;
        case "step07":
            $step_date    = isset($_GET['step_date']) ? $_GET['step_date'] : "";
            $step_07_date = $step_date;
            $add_where   .= " AND r.return_req_date ='{$step_07_date}' AND (r.return_delivery_no IS NULL OR r.return_delivery_no ='')";
            break;
        case "step09_01":
            $add_where .= " AND r.return_type='2' AND (r.return_with_date IS NULL OR r.return_with_date = '')";
            break;
        case "step09_02":
            $step_date    = isset($_GET['step_date']) ? $_GET['step_date'] : "";
            $step_09_date = $step_date;

            $add_where .= " AND r.return_with_date ='{$step_09_date}'";
            break;
        case "step10":
            $step_10_s_date = $sch_req_s_date;
            $step_10_e_date = $sch_req_e_date;

            $add_where .= " AND return_type IN('2','4')";
            break;
    }
}

if($session_staff_state == '2'){
    $ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "return_date";
    $ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";
}else{
    $ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
    $ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";
}

$add_orderby    = "r.r_no DESC, r.prd_no ASC";
if(!empty($ord_type))
{
    if($ord_type_by == '1'){
        $orderby_val = "ASC";
    }else{
        $orderby_val = "DESC";
    }

    if($orderby_val == "DESC"){
        $add_orderby = "r.{$ord_type} IS NULL DESC, r.{$ord_type} {$orderby_val}, r.r_no DESC, r.prd_no ASC";
    }else{
        $add_orderby = "r.{$ord_type} {$orderby_val}, r.r_no DESC, r.prd_no ASC";
    }
}

$cms_ord_sql    = "SELECT DISTINCT r.order_number FROM work_cms_return r WHERE {$add_where} AND r.order_number is not null GROUP BY r.order_number ORDER BY {$add_orderby} LIMIT 10000";
$cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);

$order_number_list      = [];
$order_count_list       = [];
$return_unit_list       = [];
$lfcr                   = chr(10);
$return_unit_type_option= getReturnUnitTypeOption();
while($order_number = mysqli_fetch_assoc($cms_ord_query)){
    $order_number_list[] =  "'".$order_number['order_number']."'";

    $return_unit_sql    = "SELECT wcr.type, wcr.sku, wcr.quantity FROM work_cms_return_unit wcr WHERE wcr.order_number = '{$order_number['order_number']}' AND wcr.quantity > 0 ORDER BY sku";
    $return_unit_query  = mysqli_query($my_db, $return_unit_sql);
    while($return_unit  = mysqli_fetch_assoc($return_unit_query))
    {
        $return_unit_list[$order_number['order_number']][] = array(
            "sku"   => $return_unit["sku"],
            "qty"   => $return_unit["quantity"],
            "type"  => isset($return_unit_type_option[$return_unit['type']]) ? $return_unit_type_option[$return_unit['type']] : "",
        );
    }

    $order_count_list[$order_number['order_number']] = !empty($return_unit_list[$order_number['order_number']]) ? count($return_unit_list[$order_number['order_number']]) : 0;
}

$order_numbers = implode(',', $order_number_list);

// 리스트 쿼리
$return_sql = "
	SELECT
		r.parent_order_number,
	    r.order_number,
		r.recipient,
		r.recipient_hp,
		r.recipient_addr,
        r.return_delivery_type,
		r.return_delivery_no,
		r.return_delivery_no2,
	    r.return_date,
	    r.return_memo,
		(SELECT s.s_name FROM staff s WHERE s.s_no=r.run_with_no) as with_name,
	    r.return_delivery_fee
	FROM work_cms_return r
	WHERE {$add_where} AND r.order_number IN({$order_numbers})
	GROUP BY r.order_number
	ORDER BY {$add_orderby}
";

$return_query	= mysqli_query($my_db, $return_sql);
$idx            = 3;
$color_idx      = 1;
if(!!$return_query)
{
    $return_type_option = getReturnTypeOption();
    while($work_cms = mysqli_fetch_array($return_query))
    {
        $recipient_addr_val = trim($work_cms['recipient_addr']);
        $recipient_addr     = preg_replace('/\r\n|\r|\n/','',$recipient_addr_val);
        $return_units       = $return_unit_list[$work_cms['order_number']];
        $return_prd_count   = 0;
        $return_delivery_no = $work_cms['return_delivery_no'];

        if(isset($order_count_list[$work_cms['order_number']])){
            $return_prd_count = $order_count_list[$work_cms['order_number']];
            unset($order_count_list[$work_cms['order_number']]);
        }

        if(!empty($work_cms['return_delivery_no2'])){
            $return_delivery_no .= $lfcr.$work_cms['return_delivery_no2'];
        }

        if($return_prd_count > 0)
        {
            $merge_idx = $idx+$return_prd_count-1;
            if($return_prd_count > 1)
            {
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("A{$idx}:A{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("B{$idx}:B{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("C{$idx}:C{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("D{$idx}:D{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("E{$idx}:E{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("F{$idx}:F{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("G{$idx}:G{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("K{$idx}:K{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("L{$idx}:L{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("M{$idx}:M{$merge_idx}");
            }

            if($color_idx%2 == 0){
                $objPHPExcel->getActiveSheet()->getStyle("A{$idx}:M{$merge_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00CCCCCC');
            }

            $color_idx++;
        }

        $prd_type_sql     = "SELECT return_type FROM work_cms_return r WHERE order_number = '{$work_cms['order_number']}' ORDER BY r.r_no DESC, r.prd_no ASC";
        $prd_type_query   = mysqli_query($my_db, $prd_type_sql);
        $return_type_name = "";
        while($prd_type_result = mysqli_fetch_assoc($prd_type_query))
        {
            $return_type_name .= !empty($return_type_name) ? $lfcr.$return_type_option[$prd_type_result['return_type']] : $return_type_option[$prd_type_result['return_type']];
        }

        $delivery_sql   = "SELECT delivery_type, delivery_no FROM work_cms_delivery WHERE order_number = '{$work_cms['parent_order_number']}' GROUP BY delivery_no";
        $delivery_query = mysqli_query($my_db, $delivery_sql);
        $delivery_text  = "";
        while($delivery = mysqli_fetch_assoc($delivery_query))
        {
            $delivery_text .= !empty($delivery_text) ? $lfcr.$delivery['delivery_type'].$lfcr.$delivery['delivery_no'] : $delivery['delivery_type'].$lfcr.$delivery['delivery_no'];
        }

        if($return_units)
        {
            foreach($return_units as $return_unit){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A{$idx}", $delivery_text)
                    ->setCellValue("B{$idx}", $work_cms['order_number'].$lfcr.$work_cms['recipient'].$lfcr.$work_cms['recipient_hp'])
                    ->setCellValue("C{$idx}", $recipient_addr)
                    ->setCellValue("D{$idx}", $return_type_name)
                    ->setCellValue("E{$idx}", $work_cms['return_delivery_type'])
                    ->setCellValueExplicit("F{$idx}", $return_delivery_no, PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue("G{$idx}", $work_cms['return_date'])
                    ->setCellValue("H{$idx}", $return_unit['sku'])
                    ->setCellValue("I{$idx}", $return_unit['qty'])
                    ->setCellValue("J{$idx}", $return_unit['type'])
                    ->setCellValue("K{$idx}", $work_cms['return_memo'])
                    ->setCellValue("L{$idx}", $work_cms['with_name'])
                    ->setCellValue("M{$idx}", $work_cms['return_delivery_fee'])
                ;

                $idx++;
            }
        }else{
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A{$idx}", $delivery_text)
                ->setCellValue("B{$idx}", $work_cms['order_number'].$lfcr.$work_cms['recipient'].$lfcr.$work_cms['recipient_hp'])
                ->setCellValue("C{$idx}", $recipient_addr)
                ->setCellValue("D{$idx}", $return_type_name)
                ->setCellValue("E{$idx}", $work_cms['return_delivery_type'])
                ->setCellValueExplicit("F{$idx}", $return_delivery_no, PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("G{$idx}", $work_cms['return_date'])
                ->setCellValue("H{$idx}", "")
                ->setCellValue("I{$idx}", "")
                ->setCellValue("J{$idx}", "")
                ->setCellValue("K{$idx}", $work_cms['return_memo'])
                ->setCellValue("L{$idx}", $work_cms['with_name'])
                ->setCellValue("M{$idx}", $work_cms['return_delivery_fee'])
            ;

            $idx++;
        }

    }
}
$idx--;

// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getStyle("A3:M{$idx}")->getFont()->setSize(9);;
$objPHPExcel->getActiveSheet()->getStyle("A3:M{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("I3:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A3:M{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:A2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("B1:B2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("M2:M2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("A3:A{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("B3:B{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("C3:C{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("D3:D{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("F3:F{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("H3:H{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("K3:K{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);

$objPHPExcel->getActiveSheet()->setTitle("회수리스트");
$objPHPExcel->getActiveSheet()->getStyle("A1:M{$idx}")->applyFromArray($styleArray);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_EMP반품등록.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
