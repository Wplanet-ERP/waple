<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '600');
ini_set('memory_limit', '5G');

require('inc/common.php');
require('ckadmin.php');
require('inc/model/WorkCms.php');
require('Classes/PHPExcel.php');

$file_name   = $_FILES["tracking_file"]["tmp_name"];
$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$billing_date   = (isset($_POST['billing_date']))?$_POST['billing_date']:"";
$reg_s_no       = $session_s_no;
$regdate        = date('Y-m-d H:i:s');
$logistics_company = "위드플레이스";

$comma   = "";
$ins_sql = " INSERT INTO `work_cms_tracking`
            (`track_type`,`logistics_company`,`c_no`,`c_name`,`order_number`,`delivery_no`,`delivery_type`,`total_qty`,`extra_delivery_qty`,`extra_delivery_no`,`prd_no`,`qty`,`prd_price`,`recipient`,`basic_fee`,`ferry_fee`,`jeju_fee`,`etc_fee`,`total_fee`,`billing_date`,`delivery_date`,`reg_s_no`,`regdate`)
          VALUES ";

for ($i = 3; $i <= $totalRow; $i++)
{
    $delivery_no = $delivery_type = $extra_delivery_no = $track_type = $recipient = $delivery_date = $memo = "";
    $prd_price = $basic_fee = $ferry_fee = $jeju_fee = $etc_fee = $total_fee = $total_qty = $extra_delivery_qty = 0;


    # 주문데이터 처리용
    $order_number = $prd_no = $c_no = $c_name = "";
    $qty = 0;

    $delivery_date_val  = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    //배달일자
    $track_type_val     = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    //예약구분
    $delivery_type      = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    //택배사
    $delivery_no_val    = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));    //운송장번호
    $total_qty          = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));    //총내품수량
    $extra_delivery_qty = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));    //추가송장갯수
    $extra_delivery_no  = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    //추가운송장
    $recipient          = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));    //받는분
    $basic_fee          = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));    //기본운임
    $ferry_fee          = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));    //도선료
    $jeju_fee           = (string)trim(addslashes(str_replace('-','',$objWorksheet->getCell("N{$i}")->getValue())));    //제주운임
    $etc_fee            = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));    //기타운임
    $total_fee          = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));    //총운임

    if(strpos($total_fee, "=SUM") !== false){
        $total_fee      = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getCalculatedValue()));    //총운임
    }

    if(!empty($delivery_date_val)){
        if(strpos($delivery_date_val, "/") !== false){
            $delivery_date = date("Y-m-d", strtotime("{$delivery_date_val}"));
        }elseif(strpos($delivery_date_val, "-") !== false) {
            $delivery_date = date("Y-m-d", strtotime("{$delivery_date_val}"));
        }elseif($delivery_date_val > 20000000) {
            $delivery_date = date("Y-m-d", strtotime("{$delivery_date_val}"));
        }else{
            $delivery_date     = date("Y-m-d", strtotime("{$delivery_date_val}"));
        }
    }

    // 운송장 번호가 없는 경우
    if(!$delivery_no_val){
        echo "ROW : {$i}<br/>";
        echo "배송비 내역 리스트 반영에 실패하였습니다.<br>운송장번호가 없습니다.<br>
            운송장번호  : {$delivery_no_val}<br>
            받는분      : {$recipient}<br>";
        exit;
    }else{
        $delivery_no = str_replace('-','', $delivery_no_val);
    }

    if(!empty($track_type_val)){
        if($track_type_val == '일반'){
            $track_type = '1';
        }elseif($track_type_val == '반품'){
            $track_type = '2';
        }
    }else{
        echo "ROW : {$i}<br/>";
        echo "배송비 내역 리스트 반영에 실패하였습니다.<br>예약구분 값이 없습니다.<br>
            운송장번호  : {$delivery_no_val}<br>
            예약구분    : {$track_type}<br>";
        exit;
    }

    # 배송비 내역리스트 체크
    $tracking_chk_sql    = "SELECT count(t_no) as cnt FROM work_cms_tracking WHERE delivery_no='{$delivery_no}'";
    $tracking_chk_query  = mysqli_query($my_db, $tracking_chk_sql);
    $tracking_chk_result = mysqli_fetch_assoc($tracking_chk_query);
    if($tracking_chk_result['cnt'] > 0){
        echo "ROW : {$i}<br/>";
        echo "운송장번호 : {$delivery_no}<br/>";
        echo "배송비 내역 리스트 반영에 실패하였습니다.<br> 동일한 운송장번호가 존재합니다.";
        exit;
    }

    # 운송장 번호로 주문번호 검색
    if($track_type == '2'){
        $order_sql = "SELECT order_number, prd_no, quantity, '0' as unit_price, c_no, c_name FROM work_cms_return WHERE (return_delivery_no='{$delivery_no}' OR return_delivery_no2='{$delivery_no}') ORDER BY r_no ASC";
    }else{
        $order_sql = "SELECT order_number, prd_no, quantity, unit_price, c_no, c_name FROM work_cms WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery WHERE delivery_no='{$delivery_no}') AND delivery_state='4' ORDER BY w_no ASC";
    }
    $order_query    = mysqli_query($my_db, $order_sql);
    $order_list     = [];

    if(!$order_query){
        echo "ROW : {$i}<br/>";
        echo "배송비 내역 리스트 반영에 실패하였습니다.<br> 주문 검색 DB 오류 입니다.<br>
        오류내용    : ".mysqli_error()."<br>";
        exit;
    }

    while($order_result = mysqli_fetch_array($order_query))
    {
        $order_list[] = $order_result;
    }

    # 입/출고 요청리스트 확인
    $is_quick = false;
    if(empty($order_list)){
        $order_chk_sql      = "SELECT order_number, prd_no, quantity, unit_price, c_no, c_name FROM work_cms_quick WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_quick_delivery WHERE delivery_no='{$delivery_no}') AND quick_state='4' ORDER BY w_no ASC";
        $order_chk_query    = mysqli_query($my_db, $order_chk_sql);
        while($order_chk_result = mysqli_fetch_array($order_chk_query))
        {
            $order_list[] = $order_chk_result;
            $is_quick = true;
        }
    }

    if(!empty($order_list)){
        foreach($order_list as $order)
        {
            $order_number = $order['order_number'];
            $prd_no       = $order['prd_no'];
            $qty          = $order['quantity'];
            $prd_price    = $order['unit_price'];
            $c_no         = $order['c_no'];
            $c_name       = $order['c_name'];

            $ins_sql .= $comma."('{$track_type}', '{$logistics_company}', '{$c_no}', '{$c_name}', '{$order_number}', '{$delivery_no}', '{$delivery_type}', '{$total_qty}', '{$extra_delivery_qty}', '{$extra_delivery_no}', '{$prd_no}', '{$qty}', '{$prd_price}', '{$recipient}', '{$basic_fee}', '{$ferry_fee}', '{$jeju_fee}', '{$etc_fee}', '{$total_fee}', '{$billing_date}', '{$delivery_date}', '{$reg_s_no}', '{$regdate}')";
            $comma    = ' , ';
        }
    }else{
        $ins_sql .= $comma."('{$track_type}', '{$logistics_company}', '{$c_no}', '{$c_name}', '{$order_number}', '{$delivery_no}', '{$delivery_type}', '{$total_qty}', '{$extra_delivery_qty}', '{$extra_delivery_no}', '{$prd_no}', '{$qty}', '{$prd_price}', '{$recipient}', '{$basic_fee}', '{$ferry_fee}', '{$jeju_fee}', '{$etc_fee}', '{$total_fee}', '{$billing_date}', '{$delivery_date}', '{$reg_s_no}', '{$regdate}')";
        $comma    = ' , ';
    }

    if($is_quick){
        $upd_delivery_sql = "UPDATE work_cms_quick_delivery SET delivery_fee='{$total_fee}' WHERE delivery_no='{$delivery_no}'";
    }else{
        $upd_delivery_sql = "UPDATE work_cms_delivery SET delivery_fee='{$total_fee}' WHERE delivery_no='{$delivery_no}'";
    }
    mysqli_query($my_db, $upd_delivery_sql);
}

if(!mysqli_query($my_db, $ins_sql)){
    echo "배송비 내역 리스트 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    echo "SQL : ".$ins_sql."<br/>";
    exit;
}else{
    exit("<script>alert('배송비 내역 리스트 반영 되었습니다.');location.href='work_cms_tracking_list.php';</script>");
}

?>
