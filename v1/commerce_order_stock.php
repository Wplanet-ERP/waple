<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_price.php');
require('inc/helper/product_cms_stock.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');
require('inc/model/MyCompany.php');
require('inc/model/Company.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/CommerceOrder.php');
require('inc/model/ProductCmsStock.php');

# Model Init
$stock_model    = ProductCmsStock::Factory();
$stock_model->setStockReport();

$commerce_model = CommerceOrder::Factory();
$commerce_model->setMainInit("commerce_order", "no");

# 프로세스 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "add_supply_report")
{
    $cur_date       = date("Y-m-d");
    $set_no 	    = $_POST['com_ord_no'];
    $stock_no 	    = isset($_POST['stock_no']) ? $_POST['stock_no'] : "";
    $sup_loc_type 	= isset($_POST['sup_loc_type']) ? $_POST['sup_loc_type'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $report_item    = $stock_model->getStockReportItem($stock_no);
    $unit_price     = $report_item['unit_price'];
    $sup_quantity   = $report_item['stock_qty'];

    $total_price    = $unit_price*$sup_quantity;
    $supply_price   = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1,0);
    $vat_price      = ($sup_loc_type == '2') ? 0 : $total_price-$supply_price;

    $group_no       = $commerce_model->getMaxGroup($set_no);

    $ins_data   = array(
        "set_no"        => $set_no,
        "stock_no"      => $stock_no,
        "type"          => "supply",
        "option_no"     => $report_item['prd_unit'],
        "group"         => $group_no,
        "unit_price"    => $unit_price,
        "quantity"      => $sup_quantity,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price,
        "total_price"   => $supply_price,
        "sup_date"      => $report_item['regdate'],
        "regdate"       => date("Y-m-d H:i:s"),
    );

    if(!empty($report_item['expiration']) && $report_item['expiration'] > 0)
    {
        $limit_date  = date('Y-m-d', strtotime($cur_date." +{$report_item['expiration']} months"));
        $ins_data["limit_date"] = $limit_date;
    }

    if($commerce_model->insert($ins_data))
    {
        $ord_no     = $commerce_model->getInsertId();
        $stock_model->update(array("no" => $stock_no, "ord_no" => $ord_no));

        exit("<script>alert('발주서 상세내역 추가에 성공했습니다');location.href='commerce_order_stock.php?{$search_url}';</script>");
    } else {
        exit("<script>alert('발주서 상세내역 추가에 실패했습니다');history.back();</script>");
    }
}
elseif($process == "f_unit_price")
{
    $comm_no        = (isset($_POST['no'])) ? $_POST['no'] : "";
    $unit_price     = (isset($_POST['val'])) ? $_POST['val'] : "";
    $unit_price     = str_replace(",","",trim($unit_price));

    $comm_ord_item  = $commerce_model->getOrderItem($comm_no);
    $set_no         = $comm_ord_item['set_no'];
    $sup_loc_type   = $comm_ord_item['sup_loc_type'];

    $total_price    = $unit_price*$comm_ord_item['quantity'];
    $supply_price   = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1, 0);
    $vat_price      = ($sup_loc_type == '2') ? 0 : ($total_price-$supply_price);

    $upd_data       = array(
        "no"            => $comm_no,
        "unit_price"    => $unit_price,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price,
        "total_price"   => $supply_price,
    );

    if (!$commerce_model->update($upd_data)){
        echo "단가(VAT 포함) 저장에 실패 하였습니다.";
    }else{
        echo "단가(VAT 포함) 저장에 성공했습니다.";
    }
    exit;
}
elseif($process == "f_supply_price")
{
    $comm_no        = (isset($_POST['no'])) ? $_POST['no'] : "";
    $supply_price   = (isset($_POST['val'])) ? $_POST['val'] : "";
    $supply_price   = str_replace(",", "", trim($supply_price));

    if (!$commerce_model->update(array("no"=> $comm_no, "supply_price"  => $supply_price))){
        echo "공급가 저장에 실패 하였습니다.";
    }else {
        echo "공급가가 저장 되었습니다.";
    }
    exit;
}
elseif($process == "f_vat")
{
    $comm_no        = (isset($_POST['no'])) ? $_POST['no'] : "";
    $vat_price      = (isset($_POST['val'])) ? $_POST['val'] : "";
    $vat_price      = str_replace(",", "", trim($vat_price));

    if (!$commerce_model->update(array("no"=> $comm_no, "vat"  => $vat_price)))
        echo "부가세 저장에 실패했습니다.";
    else
        echo "부가세 저장에 성공했습니다.";
    exit;
}
elseif($process == "f_is_order")
{
    $no 	= (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";

    if ($stock_model->update(array("no" => $no, "is_order" => $value))){
        echo "재고반영 구분이 변경되었습니다.";
    }else{
        echo "재고반영 구분 변경에 실패했습니다.";
    }
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "39";
$nav_title   = "입고관리";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 입고관리 리스트
$sch_get = isset($_GET['sch'])?$_GET['sch']:"Y";
$smarty->assign("sch", $sch_get);

$add_where          = "1=1 AND is_order='1'";
$sch_reg_mon        = isset($_GET['sch_reg_mon']) ? $_GET['sch_reg_mon'] : "";
$sch_reg_s_date     = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : date("Y-m")."-01";
$sch_reg_e_date     = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : date("Y-m")."-".date("t");
$sch_stock_no       = isset($_GET['sch_stock_no']) ? $_GET['sch_stock_no'] : "";
$sch_confirm_state  = isset($_GET['sch_confirm_state']) ? $_GET['sch_confirm_state'] : "";
$sch_sup_c_no       = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_log_company    = isset($_GET['sch_log_company']) ? $_GET['sch_log_company'] : "";
$sch_option_name    = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_prd_sku        = isset($_GET['sch_prd_sku']) ? $_GET['sch_prd_sku'] : "";
$yet_commerce       = isset($_GET['yet_commerce']) ? $_GET['yet_commerce'] : "";
$smarty->assign("yet_commerce", $yet_commerce);

# 발주 조건
$sch_multi_state    = isset($_GET['sch_multi_state']) ? $_GET['sch_multi_state'] : "";
$sch_team           = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no           = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_title          = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_order_count    = isset($_GET['sch_order_count']) ? $_GET['sch_order_count'] : "";
$sch_c_name         = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_month          = isset($_GET['sch_month']) ? $_GET['sch_month'] : "";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_reg_date_type  = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "";

$unit_model         = ProductCmsUnit::Factory();
$sup_ord_c_list 	= $unit_model->getDistinctUnitOrdCompanyData('sup_c_no');
$sup_c_list			= $unit_model->getDistinctUnitCompanyData('sup_c_no');;

$add_sup_c_list   	= $sup_ord_c_list[$session_s_no];
$sup_c_count_list 	= [];
$sch_sup_c_list 	= $sup_c_list;
$team_model         = Team::Factory();
$team_all_list      = $team_model->getTeamAllList();
$staff_team_list    = $team_all_list['staff_list'];
$sch_staff_list 	= $staff_team_list['all'];
$team_full_name_list= $team_model->getTeamFullNameList();
$team_name_list     = $team_all_list['team_name_list'];

if(!empty($add_sup_c_list)){
    foreach($add_sup_c_list as $key => $sup_c){
        $sup_c_sql = "SELECT count(*) as cnt FROM product_cms_stock_report pcsr WHERE pcsr.sup_c_no='{$key}' AND is_order='1' AND ord_no = ''";
        $sup_c_query = mysqli_query($my_db, $sup_c_sql);
        while($sup_c_result = mysqli_fetch_assoc($sup_c_query)){
            $sup_c_count_list[$key] = $sup_c_result['cnt'];
        }
    }
}

$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));
$years_val  = date('Y-m-d', strtotime('-2 years'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);
$smarty->assign("years_val", $years_val);
$smarty->assign("sch_reg_date_type", $sch_reg_date_type);


#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
$url_check_str  = $_SERVER['QUERY_STRING'];
$url_chk_result = false;
if(empty($url_check_str)){
    $url_check_team_where   = getTeamWhere($my_db, $session_team);
    $url_check_sql    		=  "SELECT count(*) as cnt FROM product_cms_stock_report as pcsr LEFT JOIN product_cms_unit as pcu ON pcu.no = pcsr.prd_unit WHERE is_order='1' AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$url_check_team_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$url_check_team_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$url_check_team_where})))";
    $url_check_query  		= mysqli_query($my_db, $url_check_sql);
    $url_check_result 		= mysqli_fetch_assoc($url_check_query);

    if($url_check_result['cnt'] == 0){
        $sch_team       = "all";
        $url_chk_result = true;
    }
}

# 입고/반출 관련 조건
if($yet_commerce == '1')
{
    $add_where .= " AND (ord_no = 0 OR ord_no IS NULL)";
    if (!empty($sch_sup_c_no)) {
        $add_where .= " AND pcsr.sup_c_no='{$sch_sup_c_no}'";
        $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
    }

    if (!empty($sch_log_company)) {
        $add_where .= " AND pcsr.log_c_no='{$sch_log_company}'";
        $smarty->assign("sch_log_company", $sch_log_company);
    }

    $smarty->assign("sch_reg_s_date", $sch_reg_s_date);
    $smarty->assign("sch_reg_e_date", $sch_reg_e_date);

    if (!empty($sch_team))
    {
        if ($sch_team != "all") {
            $sch_team_code_where = getTeamWhere($my_db, $sch_team);
            $add_where       .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})))";
            $sch_sup_c_list   = $unit_model->getTeamSupList($sch_team);
            $sch_staff_list   = $staff_team_list[$sch_team];
        }
        $smarty->assign("sch_team", $sch_team);
    }else{
        $sch_team_code_where = getTeamWhere($my_db, $session_team);
        $add_where       .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})))";
        $sch_sup_c_list   = $unit_model->getTeamSupList($session_team);
        $sch_staff_list   = $staff_team_list[$session_team];
        $smarty->assign("sch_team", $session_team);
    }

    if (!empty($sch_s_no))
    {
        if ($sch_s_no != "all") {
            $add_where .= " AND pcu.ord_s_no ='{$sch_s_no}'";
        }
        $smarty->assign("sch_s_no", $sch_s_no);
    }else{
        if($sch_team == $session_team){
            $add_where .= " AND pcu.ord_s_no ='{$session_s_no}'";
            $smarty->assign("sch_s_no", $session_s_no);
        }
    }
}
else
{
    if(!empty($sch_reg_mon)) {
        $sch_reg_s_date = $sch_reg_mon."-01";
        $sch_reg_e_date = $sch_reg_mon."-31";
    }

    if (!empty($sch_reg_s_date)) {
        $reg_s_date = $sch_reg_s_date." 00:00:00";
        $add_where .= " AND pcsr.regdate >='{$reg_s_date}'";
        $smarty->assign("sch_reg_s_date", $sch_reg_s_date);
    }

    if (!empty($sch_reg_e_date)) {
        $reg_e_date = $sch_reg_e_date." 23:59:59";
        $add_where .= " AND pcsr.regdate <= '{$reg_e_date}'";
        $smarty->assign("sch_reg_e_date", $sch_reg_e_date);
    }

    if (!empty($sch_stock_no)) {
        $add_where .= " AND pcsr.no='{$sch_stock_no}'";
        $smarty->assign("sch_stock_no", $sch_stock_no);
    }

    if(!empty($sch_multi_state)){
        $add_where .= " AND pcsr.confirm_state IN(1,2)";
    }

    if (!empty($sch_confirm_state)) {
        $add_where .= " AND pcsr.confirm_state='{$sch_confirm_state}'";
        $smarty->assign("sch_confirm_state", $sch_confirm_state);
    }

    if (!empty($sch_sup_c_no)) {
        $add_where .= " AND pcsr.sup_c_no='{$sch_sup_c_no}'";
        $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
    }

    if (!empty($sch_log_company)) {
        $add_where .= " AND pcsr.log_c_no='{$sch_log_company}'";
        $smarty->assign("sch_log_company", $sch_log_company);
    }

    if (!empty($sch_option_name)) {
        $add_where .= " AND pcu.option_name LIKE '%{$sch_option_name}%'";
        $smarty->assign("sch_option_name", $sch_option_name);
    }

    if (!empty($sch_prd_sku)) {
        $add_where .= " AND pcsr.sku = '{$sch_prd_sku}'";
        $smarty->assign("sch_prd_sku", $sch_prd_sku);
    }

    if (!empty($sch_team))
    {
        if ($sch_team != "all") {
            $sch_team_code_where = getTeamWhere($my_db, $sch_team);
            $add_where       .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})))";
            $sch_sup_c_list   = $unit_model->getTeamSupList($sch_team);
            $sch_staff_list   = $staff_team_list[$sch_team];
        }
        $smarty->assign("sch_team", $sch_team);
    }else{
        $sch_team_code_where = getTeamWhere($my_db, $session_team);
        $add_where       .= " AND (pcu.ord_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_sub_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})) OR pcu.ord_third_s_no IN(SELECT s.s_no FROM staff s WHERE team IN({$sch_team_code_where})))";
        $sch_sup_c_list   = $unit_model->getTeamSupList($session_team);
        $sch_staff_list   = $staff_team_list[$session_team];
        $smarty->assign("sch_team", $session_team);
    }

    if (!empty($sch_s_no))
    {
        if ($sch_s_no != "all") {
            $add_where .= " AND pcu.ord_s_no ='{$sch_s_no}'";
        }
        $smarty->assign("sch_s_no", $sch_s_no);
    }else{
        if($sch_team == $session_team){
            $add_where .= " AND pcu.ord_s_no ='{$session_s_no}'";
            $smarty->assign("sch_s_no", $session_s_no);
        }
    }

    if (!empty($sch_my_c_no)) {
        if ($sch_my_c_no != "all") {
            $add_where       .= " AND pcsr.ord_no IN(SELECT co.no FROM commerce_order co WHERE co.set_no IN(SELECT cos.`no` FROM commerce_order_set cos WHERE cos.my_c_no ='{$sch_my_c_no}'))";
        }
        $smarty->assign("sch_my_c_no", $sch_my_c_no);
    }

    if (!empty($sch_title)) {
        $add_where       .= " AND pcsr.ord_no IN(SELECT co.no FROM commerce_order co WHERE co.set_no IN(SELECT cos.`no` FROM commerce_order_set cos WHERE cos.title LIKE '%{$sch_title}%'))";
        $smarty->assign("sch_title", $sch_title);
    }

    if (!empty($sch_order_count)) {
        $add_where       .= " AND pcsr.ord_no IN(SELECT co.no FROM commerce_order co WHERE co.set_no IN(SELECT cos.`no` FROM commerce_order_set cos WHERE cos.order_count ='{$sch_order_count}'))";
        $smarty->assign("sch_order_count", $sch_order_count);
    }

    if (!empty($sch_c_name)) {
        $add_where       .= " AND pcsr.ord_no IN(SELECT co.no FROM commerce_order co WHERE co.set_no IN(SELECT cos.`no` FROM commerce_order_set cos WHERE cos.c_name LIKE '%{$sch_c_name}%'))";
        $smarty->assign("sch_c_name", $sch_c_name);
    }

    if (!empty($sch_month)) {
        $add_where       .= " AND pcsr.ord_no IN(SELECT co.no FROM commerce_order co WHERE co.set_no IN(SELECT cos.`no` FROM commerce_order_set cos WHERE DATE_FORMAT(cos.req_date, '%Y-%m') BETWEEN '{$sch_month}' AND '{$sch_month}'))";
        $smarty->assign("sch_month", $sch_month);
    }

    if (!empty($sch_no)) {
        $add_where       .= " AND pcsr.ord_no ='{$sch_no}'";
        $smarty->assign("sch_no", $sch_no);
    }
}

# 전체 게시물 수
$stock_report_total_sql         = "
    SELECT 
        pcsr.no,
        pcsr.ord_no,
        pcsr.stock_qty,
        pcsr.confirm_state
    FROM product_cms_stock_report pcsr 
    LEFT JOIN product_cms_unit as pcu ON pcu.no = pcsr.prd_unit
    WHERE {$add_where}";
$stock_report_total_query       = mysqli_query($my_db, $stock_report_total_sql);
$commerce_order_list            = [];
$stock_report_total             = 0;
$stock_report_total_qty         = 0;
$stock_report_total_price       = 0;
$stock_report_commerce_qty      = 0;
$stock_report_commerce_price    = 0;
$commerce_avg_price             = 0;
while($stock_report_total_result  = mysqli_fetch_array($stock_report_total_query))
{
    if($stock_report_total_result['ord_no'] > 0
        && ($stock_report_total_result['confirm_state'] == 1 || $stock_report_total_result['confirm_state'] == 2 || $stock_report_total_result['confirm_state'] == 4))
    {
        $commerce_order_sql     = "
            SELECT 
                co.no, 
                co.set_no, 
                cos.title, 
                cos.state, 
                cos.sup_c_name, 
                cos.order_count, 
                co.unit_price, 
                co.quantity, 
                co.supply_price, 
                co.total_price,
                co.vat, 
                co.group, 
                cos.sup_loc_type 
            FROM commerce_order co 
            LEFT JOIN commerce_order_set cos ON cos.no=co.set_no 
            WHERE co.`no`='{$stock_report_total_result['ord_no']}' AND co.`type`='supply'
        ";
        $commerce_order_query   = mysqli_query($my_db, $commerce_order_sql);
        $commerce_order_result  = mysqli_fetch_assoc($commerce_order_query);

        if($commerce_order_result['no'])
        {
            $commerce_is_usd            = ($commerce_order_result['sup_loc_type'] == '2') ? true : false;
            $total_price_text           = getNumberFormatPrice($commerce_order_result['total_price']);
            $unit_price_text            = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['unit_price']) : getKrwFormatPrice($commerce_order_result['unit_price']);
            $supply_price_text          = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['supply_price']) : getNumberFormatPrice($commerce_order_result['supply_price']);
            $org_price                  = round($commerce_order_result['total_price']/$commerce_order_result['quantity'],1);

            $commerce_order_list[$commerce_order_result['no']] = array(
                "set_no"            => $commerce_order_result['set_no'],
                "ord_name"          => $commerce_order_result['sup_c_name']." [{$commerce_order_result['order_count']}차]",
                "ord_title"         => $commerce_order_result['title'],
                "vat_price"         => $commerce_order_result['vat'],
                "ord_state"         => $commerce_order_result['state'],
                "total_price"       => $total_price_text,
                "unit_price"        => $unit_price_text,
                "supply_price"      => $supply_price_text,
                "sup_loc_type"      => $commerce_order_result['sup_loc_type'],
                "commerce_is_usd"   => $commerce_is_usd,
                "commerce_type"     => ($commerce_is_usd) ? "$" : "₩",
                "org_price"         => ($stock_report_total_result['confirm_state'] == '1') ? $org_price : 0,
            );

            $stock_report_commerce_qty      += $stock_report_total_result['stock_qty'];
            $stock_report_commerce_price    += ($stock_report_total_result['stock_qty']*$org_price);
            $stock_report_total_price       += $commerce_order_result['total_price'];
        }
    }

    $stock_report_total_qty += $stock_report_total_result['stock_qty'];
    $stock_report_total++;
}

if(!empty($stock_report_commerce_price) && !empty($stock_report_commerce_qty)){
    $commerce_avg_price = round($stock_report_commerce_price/$stock_report_commerce_qty, 1);
}
$smarty->assign("stock_report_total_qty", $stock_report_total_qty);
$smarty->assign("stock_report_total_price", $stock_report_total_price);
$smarty->assign("commerce_avg_price", $commerce_avg_price);

# 페이징 처리
$page_type	= (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num 	 = ceil($stock_report_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

if(empty($url_check_str) && $url_chk_result){
    $search_url = "sch_team=all";
}else{
    $search_url = getenv("QUERY_STRING");
}
$page_list = pagelist($pages, "commerce_order_stock.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("ord_page_type", $page_type);
$smarty->assign("total_num", $stock_report_total);
$smarty->assign("page_list", $page_list);

$report_sql = "
    SELECT
        pcsr.no,
        pcsr.regdate,
        pcsr.prd_unit,
        pcsr.confirm_state,
        (SELECT c.c_name FROM company c WHERE c.c_no=pcu.brand) as c_name,
        pcsr.sup_c_no,
        (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.sup_c_no) as sup_c_name,
        (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.log_c_no) as log_c_name,
        pcu.option_name,
        pcsr.sku,
        pcsr.stock_qty,
        pcsr.ord_no,
        pcsr.is_order,
        pcsr.memo
    FROM product_cms_stock_report as pcsr
    LEFT JOIN product_cms_unit as pcu ON pcu.no = pcsr.prd_unit
    WHERE {$add_where}
    ORDER BY pcsr.no DESC
    LIMIT {$offset},{$num}
";
$report_query = mysqli_query($my_db, $report_sql);
$confirm_state_option = getConfirmStateOption();
$report_list  = [];
while($report = mysqli_fetch_assoc($report_query))
{
    $report['confirm_state_name'] = $confirm_state_option[$report['confirm_state']];
    if(isset($commerce_order_list[$report['ord_no']]))
    {
        $report = array_merge($report, $commerce_order_list[$report['ord_no']]);
    }

    $co_list = [];
    if($report['is_order'] == '1'){
        $co_sql   = "
            SELECT 
                * 
            FROM commerce_order_set `cos` 
            WHERE `cos`.sup_c_no='{$report['sup_c_no']}' AND `cos`.display='1' AND `cos`.`state`='2' 
              AND `cos`.`no` IN(SELECT co.set_no FROM commerce_order co WHERE co.option_no = '{$report['prd_unit']}' AND co.`type`='request') 
            ORDER BY `cos`.sup_c_name, `cos`.order_count";
        $co_query = mysqli_query($my_db, $co_sql);
        while($co_result = mysqli_fetch_assoc($co_query))
        {
            $title = !empty($co_result['title']) ? "{$co_result['title']}" : "";
            $co_list[$co_result['no']] = ($co_result['order_count'] > 0) ? "{$title} {$co_result['order_count']}차 : ".$co_result['sup_c_name'] : $title." : ".$co_result['sup_c_name'];
        }
    }

    $report['co_list'] = $co_list;
    $report_list[] = $report;
}

$total_my_commerce_order_sql    = "SELECT `cos`.`no` as set_no, `cos`.log_c_no, (SELECT c.c_name FROM company c WHERE c.c_no=`cos`.log_c_no) as log_c_name FROM commerce_order_set `cos` WHERE `cos`.display='1' AND `cos`.state = '2' AND (`cos`.req_s_no = '{$session_s_no}' OR `cos`.sup_c_no IN(SELECT DISTINCT sup_c_no FROM product_cms_unit WHERE ord_s_no='{$session_s_no}' OR ord_sub_s_no='{$session_s_no}' OR ord_third_s_no='{$session_s_no}'))";
$total_my_commerce_order_query  = mysqli_query($my_db, $total_my_commerce_order_sql);
$total_my_commerce_base_cnt     = 0;
$total_my_commerce_other_list   = [];
while($total_my_commerce_order = mysqli_fetch_assoc($total_my_commerce_order_query))
{
    if($total_my_commerce_order['log_c_no'] == '2809'){
        $total_my_commerce_base_cnt++;
    }else{
        $total_my_commerce_other_list[$total_my_commerce_order['log_c_no']][] = $total_my_commerce_order;
    }
}

$my_company_model       = MyCompany::Factory();
$my_company_list        = $my_company_model->getList();
$my_company_name_list   = $my_company_model->getNameList();
$company_model			= Company::Factory();
$log_company_list     	= $company_model->getLogisticsList();

$smarty->assign("sup_c_list", $add_sup_c_list);
$smarty->assign("sch_sup_c_list", $sch_sup_c_list);
$smarty->assign("my_company_list", $my_company_list);
$smarty->assign("my_company_name_list", $my_company_name_list);
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("team_name_list", $team_name_list);
$smarty->assign("confirm_state_option",  $confirm_state_option);
$smarty->assign("is_order_option",  getStockIsOrderOption());
$smarty->assign("page_type_option",  getPageTypeOption('5'));
$smarty->assign("log_company_list",  $log_company_list);
$smarty->assign("total_my_commerce_base_cnt",  $total_my_commerce_base_cnt);
$smarty->assign("total_my_commerce_other_list",  $total_my_commerce_other_list);
$smarty->assign("report_list",  $report_list);

$smarty->display('commerce_order_stock.html');
?>
