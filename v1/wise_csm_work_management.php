<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Staff.php');

# 검색조건
$add_where      = "1=1";
$add_work_where = "";
$sch_cs_staff   = isset($_GET['sch_cs_staff']) ? $_GET['sch_cs_staff'] : $session_s_no;

if(!empty($sch_cs_staff))
{
    $add_work_where     = " AND task_run_s_no='{$sch_cs_staff}'";
    $add_return_where   = " AND task_req_s_no='{$sch_cs_staff}'";
    $smarty->assign("sch_cs_staff", $sch_cs_staff);
}

# 입금완료
$five_days              = date("Y-m-d", strtotime("-5 days"));
$deposit_complete_sql   = "SELECT (SELECT s.s_name FROM staff s WHERE s.s_no=dp.s_no) as s_name, confirm_date, bk_name, dp_money_vat, dp_no, (SELECT linked_shop_no FROM `work` w WHERE w.w_no=dp.w_no) as ord_no FROM deposit dp WHERE dp_no IN(SELECT dp_no FROM `work` WHERE dp_no > 0 AND work_state='6' AND prd_no='260' {$add_work_where}) AND dp_state='2' AND confirm_date >= '{$five_days}' AND display='1'";
$deposit_complete_query = mysqli_query($my_db, $deposit_complete_sql);
$deposit_complete_list  = [];
while($deposit_complete_result = mysqli_fetch_assoc($deposit_complete_query))
{
    $deposit_complete_list[] = $deposit_complete_result;
}

# 입금대기건
$deposit_wait_sql   = "SELECT (SELECT s.s_name FROM staff s WHERE s.s_no=dp.s_no) as s_name, DATE_FORMAT(regdate,'%Y-%m-%d') as reg_day, bk_name, cost, dp_no, (SELECT linked_shop_no FROM `work` w WHERE w.w_no=dp.w_no) as ord_no FROM deposit dp WHERE dp_no IN(SELECT dp_no FROM `work` WHERE dp_no > 0 AND work_state='6' AND prd_no='260' {$add_work_where}) AND dp_state='1' AND display='1'";
$deposit_wait_query = mysqli_query($my_db, $deposit_wait_sql);
$deposit_wait_list  = [];
while($deposit_wait_result = mysqli_fetch_assoc($deposit_wait_query))
{
    $deposit_wait_list[] = $deposit_wait_result;
}

# 환불완료
$refund_complete_sql   = "SELECT (SELECT s.s_name FROM staff s WHERE s.s_no=(SELECT w.task_req_s_no FROM `work` w WHERE w.w_no=wd.w_no)) as s_name, wd_date, bk_title, bk_name, wd_money, wd_no, (SELECT linked_no FROM `work` w WHERE w.w_no=wd.w_no) as ord_no FROM withdraw wd WHERE wd_no IN(SELECT wd_no FROM `work` WHERE wd_no > 0 AND work_state='6' AND prd_no='229' {$add_return_where}) AND wd_state='3' AND wd_date >= '{$five_days}' AND display='1'";
$refund_complete_query = mysqli_query($my_db, $refund_complete_sql);
$refund_complete_list  = [];
while($refund_complete_result = mysqli_fetch_assoc($refund_complete_query))
{
    $refund_complete_list[] = $refund_complete_result;
}

# 환불대기
$refund_wait_sql   = "SELECT (SELECT s.s_name FROM staff s WHERE s.s_no=(SELECT w.task_req_s_no FROM `work` w WHERE w.w_no=wd.w_no)) as s_name, DATE_FORMAT(regdate,'%Y-%m-%d') as reg_day, bk_title, bk_name, cost, wd_no, (SELECT linked_no FROM `work` w WHERE w.w_no=wd.w_no) as ord_no FROM withdraw wd WHERE wd_no IN(SELECT wd_no FROM `work` WHERE wd_no > 0 AND work_state='6' AND prd_no='229' {$add_return_where}) AND wd_state='2' AND display='1'";
$refund_wait_query = mysqli_query($my_db, $refund_wait_sql);
$refund_wait_list  = [];
while($refund_wait_result = mysqli_fetch_assoc($refund_wait_query))
{
    $refund_wait_list[] = $refund_wait_result;
}

# 환불접수대기
$refund_work_wait_sql   = "SELECT w_no, (SELECT s.s_name FROM staff s WHERE s.s_no=w.task_req_s_no) as s_name, DATE_FORMAT(regdate,'%Y-%m-%d') as reg_day, task_req, linked_no FROM `work` w WHERE prd_no='229' AND wd_no IS NULL AND work_state IN(3,4,5) {$add_return_where}";
$refund_work_wait_query = mysqli_query($my_db, $refund_work_wait_sql);
$refund_work_wait_list  = [];
while($refund_work_wait_result = mysqli_fetch_assoc($refund_work_wait_query))
{
    $refund_work_wait_result['task_req'] = str_replace("\r\n", "<br/>", $refund_work_wait_result['task_req']);
    $refund_work_wait_list[] = $refund_work_wait_result;
}

$staff_model   = Staff::Factory();
$cs_staff_list = $staff_model->getActiveTeamStaff("00244");

$smarty->assign('cs_staff_list', $cs_staff_list);
$smarty->assign('deposit_complete_list', $deposit_complete_list);
$smarty->assign('deposit_wait_list', $deposit_wait_list);
$smarty->assign('refund_complete_list', $refund_complete_list);
$smarty->assign('refund_wait_list', $refund_wait_list);
$smarty->assign('refund_work_wait_list', $refund_work_wait_list);

$smarty->display('wise_csm_work_management.html');
?>
