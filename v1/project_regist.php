<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_upload.php');
require('inc/helper/project.php');
require('inc/model/MyCompany.php');
require('inc/model/Team.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'new_project') # 생성
{
    // 필수정보
    $work_state     = isset($_POST['work_state']) ? $_POST['work_state'] : "";
    $my_c_no        = isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "";
    $pj_name        = isset($_POST['pj_name']) ? addslashes(trim($_POST['pj_name'])) : "";
    $contract_name  = isset($_POST['contract_name']) ? addslashes(trim($_POST['contract_name'])) : "";
    $contract_type  = isset($_POST['contract_type']) ? $_POST['contract_type'] : "";
    $agency         = isset($_POST['agency']) ? addslashes(trim($_POST['agency'])) : "";
    $description    = isset($_POST['description']) ? addslashes(trim($_POST['description'])) : "";
    $achievements   = isset($_POST['achievements']) ? addslashes(trim($_POST['achievements'])) : "";
    $participants   = isset($_POST['participants']) ? $_POST['participants'] : "";
    $expected_sales = isset($_POST['expected_sales']) ? str_replace(",","",$_POST['expected_sales']) : "";
    $budget_confirm = isset($_POST['budget_confirm']) ? $_POST['budget_confirm'] : "";
    $state          = isset($_POST['state']) ? $_POST['state'] : "";
    $team           = isset($_POST['team']) && !empty($_POST['team']) ? sprintf('%05d', $_POST['team']) : "";
    $manager        = isset($_POST['manager']) ? $_POST['manager'] : "";
    $pj_s_date      = isset($_POST['pj_s_date']) ? addslashes($_POST['pj_s_date']) : "";
    $pj_e_date      = isset($_POST['pj_e_date']) ? addslashes($_POST['pj_e_date']) : "";
    $cal_state      = isset($_POST['cal_state']) ? addslashes($_POST['cal_state']) : "";
    $satisfaction   = isset($_POST['satisfaction']) ? trim($_POST['satisfaction']) : "";
    $reg_date       = date("Y-m-d H:i:s");
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    # 필수정보 체크 및 저장
    if(empty($work_state) || empty($my_c_no) || empty($state) || empty($contract_type) || empty($team) || empty($pj_name) || empty($contract_name) || empty($manager) || empty($pj_s_date) || empty($pj_e_date) || empty($cal_state))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');history.back();</script>");
    }

    $ins_sql = "
        INSERT INTO `project` SET
            `my_c_no`        = '{$my_c_no}',
            `work_state`     = '{$work_state}',
            `budget_confirm` = '{$budget_confirm}',
            `state`          = '{$state}',
            `team`           = '{$team}',
            `req_s_no`       = '{$session_s_no}',
            `pj_name`        = '{$pj_name}',
            `contract_name`  = '{$contract_name}',
            `contract_type`  = '{$contract_type}',            
            `description`    = '{$description}',
            `achievements`   = '{$achievements}',
            `participants`   = '{$participants}',            
            `agency`         = '{$agency}',
            `expected_sales` = '{$expected_sales}',
            `manager`        = '{$manager}',
            `pj_s_date`      = '{$pj_s_date}',
            `pj_e_date`      = '{$pj_e_date}',
            `cal_state`      = '{$cal_state}',
            `regdate`        = '{$reg_date}'
    ";

    if(!empty($satisfaction)){
        $ins_sql .=  ", satisfaction='{$satisfaction}'";
    }

    //파일 업로드
    $file1 = $_FILES['file1'];
    if(isset($file1['name']) && !empty($file1['name'])){
        $file_path = add_store_file($file1, "project");
        $file_name = $file1['name'];
        $ins_sql  .= ", `file_path_1`='{$file_path}', `file_name_1`='{$file_name}'";
    }

    $file2 = $_FILES['file2'];
    if(isset($file2['name']) && !empty($file2['name'])){
        $file_path = add_store_file($file2, "project");
        $file_name = $file2['name'];
        $ins_sql  .= ", `file_path_2`='{$file_path}', `file_name_2`='{$file_name}'";
    }

    $file3 = $_FILES['file3'];
    if(isset($file3['name']) && !empty($file3['name'])){
        $file_path = add_store_file($file3, "project");
        $file_name = $file3['name'];
        $ins_sql  .= ", `file_path_3`='{$file_path}', `file_name_3`='{$file_name}'";
    }

    $file4 = $_FILES['file4'];
    if(isset($file4['name']) && !empty($file4['name'])){
        $file_path = add_store_file($file4, "project");
        $file_name = $file4['name'];
        $ins_sql  .= ", `file_path_4`='{$file_path}', `file_name_4`='{$file_name}'";
    }

    $file5 = $_FILES['file5'];
    if(isset($file5['name']) && !empty($file5['name'])){
        $file_path = add_store_file($file5, "project");
        $file_name = $file5['name'];
        $ins_sql  .= ", `file_path_5`='{$file_path}', `file_name_5`='{$file_name}'";
    }

    $file6 = $_FILES['file6'];
    if(isset($file6['name']) && !empty($file6['name'])){
        $file_path = add_store_file($file6, "project");
        $file_name = $file6['name'];
        $ins_sql  .= ", `file_path_6`='{$file_path}', `file_name_6`='{$file_name}'";
    }

    if(mysqli_query($my_db, $ins_sql)) {
        exit("<script>alert('프로젝트 등록에 성공했습니다');location.href='project_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('프로젝트 등록에 실패했습니다');location.href='project_regist.php?{$search_url}';</script>");
    }
}
elseif($process == 'modify_project') # 수정
{
    // 필수정보
    $pj_no           = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $work_state      = isset($_POST['work_state']) ? $_POST['work_state'] : "";
    $my_c_no         = isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "";
    $pj_name         = isset($_POST['pj_name']) ? addslashes($_POST['pj_name']) : "";
    $contract_name   = isset($_POST['contract_name']) ? addslashes(trim($_POST['contract_name'])) : "";
    $contract_type   = isset($_POST['contract_type']) ? $_POST['contract_type'] : "";
    $agency          = isset($_POST['agency']) ? addslashes(trim($_POST['agency'])) : "";
    $description     = isset($_POST['description']) ? addslashes(trim($_POST['description'])) : "";
    $achievements    = isset($_POST['achievements']) ? addslashes(trim($_POST['achievements'])) : "";
    $participants    = isset($_POST['participants']) ? $_POST['participants'] : "";
    $expected_sales  = isset($_POST['expected_sales']) ? str_replace(",","",$_POST['expected_sales']) : "";
    $budget_confirm  = isset($_POST['budget_confirm']) ? $_POST['budget_confirm'] : "";
    $state           = isset($_POST['state']) ? $_POST['state'] : "";
    $team            = isset($_POST['team']) && !empty($_POST['team']) ? sprintf('%05d', $_POST['team']) : "";
    $manager         = isset($_POST['manager']) ? $_POST['manager'] : "";
    $pj_s_date       = isset($_POST['pj_s_date']) ? addslashes($_POST['pj_s_date']) : "";
    $pj_e_date       = isset($_POST['pj_e_date']) ? addslashes($_POST['pj_e_date']) : "";
    $cal_state       = isset($_POST['cal_state']) ? addslashes($_POST['cal_state']) : "";
    $satisfaction    = isset($_POST['satisfaction']) ? trim($_POST['satisfaction']) : "";
    $search_url      = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    # 필수정보 체크 및 저장
    if(empty($pj_no) || empty($work_state) || empty($my_c_no) || empty($state) || empty($contract_type) || empty($team) || empty($pj_name) || empty($contract_name) || empty($manager) || empty($pj_s_date) || empty($pj_e_date) || empty($cal_state))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');location.href='project_regist.php?pj_no={$pj_no}';</script>");
    }

    $upd_sql = "
        UPDATE `project` SET
            `my_c_no`        = '{$my_c_no}',
            `work_state`     = '{$work_state}',
            `budget_confirm` = '{$budget_confirm}',
            `state`          = '{$state}',
            `team`           = '{$team}',
            `pj_name`        = '{$pj_name}',
            `contract_name`  = '{$contract_name}',
            `contract_type`  = '{$contract_type}',
            `agency`         = '{$agency}',
            `description`    = '{$description}',
            `achievements`   = '{$achievements}',
            `participants`   = '{$participants}', 
            `expected_sales` = '{$expected_sales}',
            `manager`        = '{$manager}',
            `pj_s_date`      = '{$pj_s_date}',
            `pj_e_date`      = '{$pj_e_date}',
            `cal_state`      = '{$cal_state}'
    ";

    if(!empty($satisfaction)){
        $upd_sql .=  ", satisfaction='{$satisfaction}'";
    }else{
        $upd_sql .=  ", satisfaction='0'";
    }

    #파일 업로드
    $file1 = $_FILES['file1'];
    if(isset($file1['name']) && !empty($file1['name'])){
        $file_path = add_store_file($file1, "project");
        $file_name = $file1['name'];
        $upd_sql  .= ", `file_path_1`='{$file_path}', `file_name_1`='{$file_name}'";
    }

    $file2 = $_FILES['file2'];
    if(isset($file2['name']) && !empty($file2['name'])){
        $file_path = add_store_file($file2, "project");
        $file_name = $file2['name'];
        $upd_sql  .= ", `file_path_2`='{$file_path}', `file_name_2`='{$file_name}'";
    }

    $file3 = $_FILES['file3'];
    if(isset($file3['name']) && !empty($file3['name'])){
        $file_path = add_store_file($file3, "project");
        $file_name = $file3['name'];
        $upd_sql  .= ", `file_path_3`='{$file_path}', `file_name_3`='{$file_name}'";
    }

    $file4 = $_FILES['file4'];
    if(isset($file4['name']) && !empty($file4['name'])){
        $file_path = add_store_file($file4, "project");
        $file_name = $file4['name'];
        $upd_sql  .= ", `file_path_4`='{$file_path}', `file_name_4`='{$file_name}'";
    }

    $file5 = $_FILES['file5'];
    if(isset($file5['name']) && !empty($file5['name'])){
        $file_path = add_store_file($file5, "project");
        $file_name = $file5['name'];
        $upd_sql  .= ", `file_path_5`='{$file_path}', `file_name_5`='{$file_name}'";
    }

    $file6 = $_FILES['file6'];
    if(isset($file6['name']) && !empty($file6['name'])){
        $file_path = add_store_file($file6, "project");
        $file_name = $file6['name'];
        $upd_sql  .= ", `file_path_6`='{$file_path}', `file_name_6`='{$file_name}'";
    }
    $upd_sql .= " WHERE pj_no='{$pj_no}'";

    if(mysqli_query($my_db, $upd_sql)) {
        exit("<script>alert('프로젝트 수정에 성공했습니다');location.href='project_regist.php?pj_no={$pj_no}&{$search_url}';</script>");
    }else{
        exit("<script>alert('프로젝트 수정에 실패했습니다');location.href='project_regist.php?pj_no={$pj_no}&{$search_url}';</script>");
    }
}
elseif($process == 'delete_project') # 삭제
{
    $pj_no      = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $del_sql    = "UPDATE project SET `display`='2' WHERE pj_no='{$pj_no}'";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(!$pj_no || !mysqli_query($my_db, $del_sql))
    {
        exit("<script>alert('프로젝트 삭제에 실패했습니다');location.href='project_list.php';</script>");
    }else{
        $del_report_sql = "UPDATE project_input_report SET active='2' WHERE pj_no='{$pj_no}'";
        mysqli_query($my_db, $del_report_sql);
        exit("<script>alert('프로젝트 삭제에 성공했습니다');location.href='project_list.php';</script>");
    }
}
elseif($process == 'del_file')
{
    $folder     = isset($_POST['folder']) ? $_POST['folder'] : "";
    $file_path  = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $pj_no      = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $del_sql = "UPDATE `project` SET `file_name_{$folder}`='', `file_path_{$folder}`='' WHERE pj_no = '{$pj_no}'";

    if(del_file($file_path) && mysqli_query($my_db, $del_sql)){
        exit("<script>alert('파일 삭제에 성공했습니다');location.href='project_regist.php?pj_no={$pj_no}&{$search_url}';</script>");
    }else{
        exit("<script>alert('프로젝트 수정에 실패했습니다');location.href='project_regist.php?pj_no={$pj_no}&{$search_url}';</script>");
    }
}

# 변수 설정
$pj_no              = isset($_GET['pj_no']) ? $_GET['pj_no'] : "";
$project            = [];
$title	            = "등록하기";
$work_state_option  = getProjectWorkStateOption();
$sch_page           = isset($_GET['page']) ? $_GET['page'] : "1";
$search_url         = "page={$sch_page}";
$sch_array_key      = array("ori_ord_type","ord_type","ord_type_by","sch_ps_display","sch_pj_no","sch_work_state","sch_my_c_no","sch_pj_name","sch_contract_type","sch_state","sch_team","sch_manager","sch_cal_state");
foreach($sch_array_key as $sch_key)
{
    $sch_val   = isset($_GET[$sch_key]) && !empty($_GET[$sch_key]) ? $_GET[$sch_key]: "";
    $search_url .= "&{$sch_key}={$sch_val}";
}
$smarty->assign("search_url", $search_url);

# 박미소님 권한부여
$is_admin = false;
if($session_s_no == "62"){
    $is_admin = true;
}
$smarty->assign("is_admin", $is_admin);

$my_company_model    = MyCompany::Factory();
$my_company_list     = $my_company_model->getList();
$team_model          = Team::Factory();
$team_full_name_list = $team_model->getTeamFullNameList();

if($pj_no)
{
    $project_sql = "
        SELECT 
          *, 
          (SELECT s.s_name FROM staff s WHERE s.s_no=p.manager LIMIT 1) as manager_name,
          (SELECT SUM(pe.supply_price) FROM project_expenses pe WHERE pe.pj_no='{$pj_no}' AND pe.display='1') as total_price
        FROM project `p` WHERE `p`.pj_no = {$pj_no} AND display='1' LIMIT 1"
    ;

    $project_query      = mysqli_query($my_db, $project_sql);
    $project 		    = mysqli_fetch_assoc($project_query);

    if(isset($project['pj_no']))
    {
        $title = "수정하기";
    }

    if(isset($project['work_state']) && !empty($project['work_state'])){
        $project['work_state_color'] = $work_state_option[$project['work_state']]['color'];
    }else{
        $project['work_state'] = '3';
        $project['work_state_color'] = 'red';
    }

    #운영진 평가결과
    $pj_ex_partner_list = [];
    $pj_ex_partner_sql = "
      SELECT 
        per.pj_er_no,
        per.pj_c_type, 
        per.pj_c_no,
        per.pj_c_name,
        (SELECT pere.score FROM project_external_report_evaluation as pere WHERE pere.pj_er_no = per.pj_er_no AND per.active='1' AND per.pj_participation='2') as cur_score,
        (SELECT pere.regdate FROM project_external_report_evaluation as pere WHERE pere.pj_er_no = per.pj_er_no AND per.active='1' ORDER BY pere.regdate DESC LIMIT 1) as cur_date
      FROM project_external_report per 
      WHERE per.pj_no='{$pj_no}' AND per.active='1'
    ";
    $pj_ex_partner_query  = mysqli_query($my_db, $pj_ex_partner_sql);
    $resource_type_option = getResourceTypeOption();
    while($pj_ex_partner = mysqli_fetch_assoc($pj_ex_partner_query))
    {
        $pj_ex_partner['pj_c_type_name'] = $resource_type_option[$pj_ex_partner['pj_c_type']];

        if(empty($pj_ex_partner['cur_score'])){
            $pj_ex_partner['cur_score'] = "0";
        }

        $total_score_sql     = "SELECT AVG(pere.score) as total_score FROM project_external_report per LEFT JOIN project_external_report_evaluation as `pere` ON `pere`.pj_er_no=`per`.pj_er_no WHERE per.active='1' AND per.pj_c_no='{$pj_ex_partner['pj_c_no']}' AND per.pj_c_type='{$pj_ex_partner['pj_c_type']}' AND per.pj_er_no!='{$pj_ex_partner['pj_er_no']}'";
        $total_score_query   = mysqli_query($my_db, $total_score_sql);
        $total_score_result  = mysqli_fetch_assoc($total_score_query);

        if(empty($total_score_result['total_score'])){
            $pj_ex_partner['total_score'] = "0";
            $pj_ex_partner['total_score_chk'] ="0";
        }else{

            $pj_ex_partner['total_score']     = $total_score_result['total_score'];
            $pj_ex_partner['total_score_chk'] = $pj_ex_partner['cur_score']-$total_score_result['total_score'];
        }

        #지난 만족도 계산
        $prev_pj_sql    = "SELECT sub.pj_no FROM project_external_report_evaluation sub WHERE sub.pj_no != '{$pj_no}' AND sub.pj_c_no='{$pj_ex_partner['pj_c_no']}' AND sub.regdate < '{$pj_ex_partner['cur_date']}' LIMIT 1";
        $prev_pj_query  = mysqli_query($my_db, $prev_pj_sql);
        $prev_pj_result = mysqli_fetch_assoc($prev_pj_query);

        if(!isset($prev_pj_result['pj_no'])){
            $pj_ex_partner['prev_score'] = "0";
            $pj_ex_partner['prev_score_chk'] ="0";
        }else{
            $prev_score_sql    = "SELECT prev.score as prev_score FROM project_external_report_evaluation prev WHERE prev.pj_er_no IN(SELECT sub.pj_er_no FROM project_external_report sub WHERE sub.active='1' AND sub.pj_no='{$prev_pj_result['pj_no']}' AND sub.pj_c_no='{$pj_ex_partner['pj_c_no']}')";
            $prev_score_query  = mysqli_query($my_db, $prev_score_sql);
            $prev_score_result = mysqli_fetch_assoc($prev_score_query);

            if(isset($prev_score_result['prev_score']) && !empty($prev_score_result['prev_score'])){
                $pj_ex_partner['prev_score'] = $prev_score_result['prev_score'];
                $pj_ex_partner['prev_score_chk'] = $pj_ex_partner['cur_score']-$prev_score_result['prev_score'];
            }else{
                $pj_ex_partner['prev_score'] = "0";
                $pj_ex_partner['prev_score_chk'] ="0";
            }
        }

        $pj_ex_partner_list[] = $pj_ex_partner;
    }

    $smarty->assign("pj_ex_partner_list", $pj_ex_partner_list);
}

if(!isset($project['my_c_no']) || empty($project['my_c_no'])){
    $project['my_c_no'] = 1;
}

$smarty->assign("title", $title);
$smarty->assign("pj_no", $pj_no);
$smarty->assign($project);
$smarty->assign('cur_date', date('Y-m-d'));
$smarty->assign('my_company_list', $my_company_list);
$smarty->assign("team_list", $team_full_name_list);
$smarty->assign('state_option', getProjectStateOption());
$smarty->assign('budget_confirm_option', getBudgetConfirmOption());
$smarty->assign('cal_state_option', getCalStateOption());
$smarty->assign('work_state_option', $work_state_option);
$smarty->assign('contract_type_option', getContractTypeOption());
$smarty->assign('permission_team_list', $permission_team_list);

$smarty->display('project_regist.html');
?>