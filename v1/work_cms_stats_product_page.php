<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Custom.php');
require('inc/model/ProductCms.php');

# Navigation & My Quick
$nav_prd_no  = "212";
$nav_title   = "옵션별 판매실적 비교";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수
$product_model              = ProductCms::Factory();
$url_model                  = Custom::Factory();
$url_model->setMainInit("commerce_url", "url_no");
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$sch_brand_total_list       = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$sch_url_list               = [];
$dp_self_imweb_list         = getSelfDpImwebCompanyList();
$dp_self_imweb_text         = implode(",", $dp_self_imweb_list);

# 검색조건
$add_where          = "`cu`.display = '1' AND `cu`.url LIKE 'idx=%'";
$add_cms_where      = "w.page_idx > 0 AND w.delivery_state='4' ANd w.unit_price > 0 AND w.dp_c_no IN({$dp_self_imweb_text})";
$sch_base_s_date    = isset($_GET['sch_base_s_date']) ? $_GET['sch_base_s_date'] : "";
$sch_base_e_date    = isset($_GET['sch_base_e_date']) ? $_GET['sch_base_e_date'] : "";
$sch_comp_s_date    = isset($_GET['sch_comp_s_date']) ? $_GET['sch_comp_s_date'] : "";
$sch_comp_e_date    = isset($_GET['sch_comp_e_date']) ? $_GET['sch_comp_e_date'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_dp_company     = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";
$sch_url            = isset($_GET['sch_url']) ? $_GET['sch_url'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_dp_company)) {
    $add_where      .= " AND `cu`.dp_c_no='{$sch_dp_company}'";
    $add_cms_where  .= " AND `w`.dp_c_no='{$sch_dp_company}'";
    $smarty->assign("sch_dp_company", $sch_dp_company);
}

if(!empty($sch_url))
{
    $chk_url_item   = $url_model->getItem($sch_url);
    $chk_page_idx   = str_replace("idx=","",$chk_url_item['url']);
    
    $add_cms_where .= " AND `w`.page_idx='{$chk_page_idx}' AND `w`.dp_c_no = '{$chk_url_item['dp_c_no']}'";
    $smarty->assign("sch_url", $sch_url);
}

# 브랜드 옵션
if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `cu`.brand = '{$sch_brand}'";
    $add_cms_where     .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `cu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_cms_where     .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where         .= " AND `cu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_cms_where     .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# URL 리스트
$sch_page_url_list  = [];
$sch_url_sql        = "SELECT url_no, page_title, url, REPLACE(url,'idx=','') as chk_url FROM commerce_url `cu` WHERE {$add_where} ORDER BY LENGTH(chk_url) ASC, chk_url";
$sch_url_query      = mysqli_query($my_db, $sch_url_sql);
while($sch_url_result = mysqli_fetch_assoc($sch_url_query))
{
    $sch_url_list[$sch_url_result['url_no']] = $sch_url_result['page_title']." :: ".$sch_url_result['url'];
}

# 정렬
$page_type      = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$ord_type       = isset($_GET['ord_type']) && !empty($_GET['ord_type']) ? $_GET['ord_type'] : "base_qty";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "DESC";
$add_order_by   = "{$ord_type} {$ord_type_by}, prd_name ASC";

$smarty->assign("ord_type", $ord_type);
$smarty->assign("ord_type_by", $ord_type_by);
$smarty->assign("ord_page_type", $page_type);

$product_page_list  = [];
if(!empty($sch_base_s_date) && !empty($sch_base_e_date) && !empty($sch_comp_s_date) && !empty($sch_comp_e_date))
{
    $sch_base_s_datetime = $sch_base_s_date." 00:00:00";
    $sch_base_e_datetime = $sch_base_e_date." 23:59:59";
    $sch_comp_s_datetime = $sch_comp_s_date." 00:00:00";
    $sch_comp_e_datetime = $sch_comp_e_date." 23:59:59";

    $smarty->assign("sch_base_s_date", $sch_base_s_date);
    $smarty->assign("sch_base_e_date", $sch_base_e_date);
    $smarty->assign("sch_comp_s_date", $sch_comp_s_date);
    $smarty->assign("sch_comp_e_date", $sch_comp_e_date);

    # 총 페이지수
    $product_page_total_sql = "
        SELECT
            COUNT(*) as total_cnt,
            SUM(base_qty) as total_base_qty,
            SUM(comp_qty) as total_comp_qty
        FROM
        (
            SELECT
                w.page_idx, 
                w.dp_c_no, 
                w.prd_no, 
                w.c_name,
                SUM(
                    IF(order_date BETWEEN '{$sch_base_s_datetime}' AND '{$sch_base_e_datetime}',quantity, 0)
                ) AS base_qty,
                SUM(
                    IF(order_date BETWEEN '{$sch_comp_s_datetime}' AND '{$sch_comp_e_datetime}',quantity, 0)
                ) AS comp_qty
            FROM work_cms w 
            WHERE {$add_cms_where} AND
            (order_date BETWEEN '{$sch_base_s_datetime}' AND '{$sch_base_e_datetime}' OR order_date BETWEEN '{$sch_comp_s_datetime}' AND '{$sch_comp_e_datetime}')
            GROUP BY page_idx, dp_c_no, prd_no
        ) AS w
    ";
    $product_page_total_query   = mysqli_query($my_db, $product_page_total_sql);
    $product_page_total_result  = mysqli_fetch_assoc($product_page_total_query);
    $product_page_total         = $product_page_total_result['total_cnt'];
    $total_base_qty             = $product_page_total_result['total_base_qty'];
    $total_comp_qty             = $product_page_total_result['total_comp_qty'];
    $total_comp_per             = ROUND((($total_base_qty-$total_comp_qty)/$total_comp_qty)*100, 1);

    $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= $page_type;
    $offset 	= ($pages-1) * $num;
    $page_num   = ceil($product_page_total/$num);

    if ($pages >= $page_num){$pages = $page_num;}
    if ($pages <= 0){$pages = 1;}

    $search_url = getenv("QUERY_STRING");
    $page_list	= pagelist($pages, "work_cms_stats_product_page.php", $page_num, $search_url);

    $smarty->assign("total_base_qty", $total_base_qty);
    $smarty->assign("total_comp_qty", $total_comp_qty);
    $smarty->assign("total_comp_per", $total_comp_per);
    $smarty->assign("page_list", $page_list);

    # 리스트 쿼리
    $product_page_sql    = "
        SELECT
            *,
            IF(
                comp_qty=0, 0, IF(base_qty=0, -100, ROUND(((base_qty-comp_qty)/comp_qty)*100,1))
            ) AS comp_per
        FROM
        (
            SELECT 
                w.page_idx, 
                w.dp_c_no, 
                w.prd_no, 
                w.c_name,
                (SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no) AS prd_name, 
                SUM(
                    IF(order_date BETWEEN '{$sch_base_s_datetime}' AND '{$sch_base_e_datetime}',quantity, 0)
                ) AS base_qty,
                SUM(
                    IF(order_date BETWEEN '{$sch_comp_s_datetime}' AND '{$sch_comp_e_datetime}',quantity, 0)
                ) AS comp_qty
            FROM work_cms w 
            WHERE {$add_cms_where} AND
            (order_date BETWEEN '{$sch_base_s_datetime}' AND '{$sch_base_e_datetime}' OR order_date BETWEEN '{$sch_comp_s_datetime}' AND '{$sch_comp_e_datetime}')
            GROUP BY page_idx, dp_c_no, prd_no
        ) AS w
        ORDER BY {$add_order_by}
        LIMIT {$offset}, {$num}
    ";
    $product_page_query = mysqli_query($my_db, $product_page_sql);
    while($product_page_result = mysqli_fetch_assoc($product_page_query))
    {
        $product_page_list[] = array("page_idx" => $product_page_result['page_idx'], "brand" => $product_page_result['c_name'], "prd_name" => $product_page_result['prd_name'], "base_qty" => $product_page_result['base_qty'], "comp_qty" => $product_page_result['comp_qty'], "comp_per" => $product_page_result['comp_per']);
    }

    $max_base_qty = $min_base_qty = 0;
    $max_comp_qty = $min_comp_qty = 0;
    $chk_page_idx = 0;
    foreach($product_page_list as $prd_page_data)
    {
        $base_qty = $prd_page_data["base_qty"];
        $comp_qty = $prd_page_data["comp_qty"];

        if($chk_page_idx == 0)
        {
            $max_base_qty = $base_qty;
            $min_base_qty = $base_qty;
            $max_comp_qty = $comp_qty;
            $min_comp_qty = $comp_qty;
        }

        if($max_base_qty < $base_qty){
            $max_base_qty = $base_qty;
        }

        if($min_base_qty > $base_qty){
            $min_base_qty = $base_qty;
        }

        if($max_comp_qty < $comp_qty){
            $max_comp_qty = $comp_qty;
        }

        if($min_comp_qty > $comp_qty){
            $min_comp_qty = $comp_qty;
        }

        $chk_page_idx++;
    }

    $smarty->assign("max_base_qty", $max_base_qty);
    $smarty->assign("min_base_qty", $min_base_qty);
    $smarty->assign("max_comp_qty", $max_comp_qty);
    $smarty->assign("min_comp_qty", $min_comp_qty);
}

$smarty->assign("page_type_option", getPageTypeOption(3));
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_dp_company_option", getTrafficDpCompanyOption());
$smarty->assign("sch_url_list", $sch_url_list);
$smarty->assign("product_page_list", $product_page_list);

$smarty->display('work_cms_stats_product_page.html');
?>
