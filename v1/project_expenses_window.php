<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/personal_expenses.php');
require('inc/helper/project.php');
require('inc/model/Kind.php');

if($session_s_no != "62"){
    $smarty->display('access_error.html');
    exit;
}

# 프로세스 처리
$process    = isset($_POST['process']) ? $_POST['process'] : "";
if($process == "add_new_expenses")
{
    $result      = false;
    $pj_no       = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_kind     = isset($_POST['pj_kind']) ? $_POST['pj_kind'] : "";
    $chk_pe_list = isset($_POST['chk_pe_no_list']) ? $_POST['chk_pe_no_list'] : "";
    $search_url  = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $cur_date    = date('Y-m-d');

    $pj_name_sql    = "SELECT pj_name FROM project WHERE pj_no='{$pj_no}' LIMIT 1";
    $pj_name_query  = mysqli_query($my_db, $pj_name_sql);
    $pj_name_result = mysqli_fetch_assoc($pj_name_query);
    $pj_name        = isset($pj_name_result['pj_name']) ? $pj_name_result['pj_name'] : "";

    $ins_sql = "INSERT INTO project_expenses(`pj_no`, `kind`, `pj_name`, `pe_no`, `cal_method`, `spend_method`, `req_s_no`, `req_team`, `supply_price`, `tax_price`, `total_price`, `req_date`, `appr_date`, `regdate`, `bankbook`) VALUES";
    $comma   = "";

    if(empty($pj_name)){
        exit("<script>alert('사업명이 없습니다. 내부통용_사업명 변경에 실패했습니다.');location.href='project_expenses.php?{$search_url}';</script>");
    }

    $pe_list_sql = "SELECT * FROM personal_expenses WHERE pe_no IN({$chk_pe_list})";
    $pe_list_query = mysqli_query($my_db, $pe_list_sql);
    while($pe = mysqli_fetch_assoc($pe_list_query))
    {
        $chk_pje_sql    = "SELECT count(pj_e_no) as cnt FROM project_expenses WHERE pe_no='{$pe['pe_no']}' AND display='1'";
        $chk_pje_query  = mysqli_query($my_db, $chk_pje_sql);
        $chk_pje_result = mysqli_fetch_assoc($chk_pje_query);

        $money       = $pe['money'];
        $appr_price  = 0;
        $tax_price   = 0;
        $total_price = 0;

        if($pe['wd_method'] == '5' || $pe['wd_method'] == '6'){
            $appr_price  = $money;
            $tax_price   = 0;
            $total_price = $appr_price;
        }else{
            $appr_price  = round(($money/1.1));
            $tax_price   = round(($appr_price*0.1));
            $total_price = $appr_price+$tax_price;
        }

        if($chk_pje_result['cnt'] > 0){
            $upd_sql = "UPDATE project_expenses SET pj_no='{$pj_no}',  kind='{$pj_kind}', pj_name='{$pj_name}',  spend_method='2', supply_price='{$appr_price}', tax_price='{$tax_price}', total_price='{$total_price}', appr_date='{$pe['payment_date']}', bankbook='1' WHERE pe_no='{$pe['pe_no']}'";
            if(mysqli_query($my_db, $upd_sql)){
                $result = true;
            }
        }else{
            $wd_method = $pe['wd_method'];
            $ins_sql .= $comma."('{$pj_no}', '{$pj_kind}', '{$pj_name}', '{$pe['pe_no']}', '2', '2', '{$pe['req_s_no']}', (SELECT s.team FROM staff s WHERE s.s_no='{$pe['req_s_no']}' LIMIT 1), '{$appr_price}', '{$tax_price}', '{$total_price}', '{$cur_date}', '{$pe['payment_date']}', now(), '1')";
            $comma    = " , ";
            $result   = true;
        }
    }

    if(!mysqli_query($my_db, $ins_sql) && !$result){
        exit("<script>alert('사업비 지출관리 추가등록에 실패했습니다');location.href='project_expenses_window.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('사업비 지출관리 추가등록 되었습니다');location.href='project_expenses_window.php?{$search_url}';</script>");
    }
}

# Kind Option
$kind_model             = Kind::Factory();
$account_code_option    = $kind_model->getKindChildList("account_code");
$pe_state_option        = getPeStateOption();
$wd_method_option       = getPeWdMethodOption();
$sch_team_name_list     = array("all" => ":: 전체 ::", "00251" => "입찰/마케팅 파트", "00252" => " >입찰/마케팅 1팀" , "00254" => " >입찰/마케팅 1팀");

# 검색커리 시작 & 그룹구분
$add_where          = " pe.display = 1 AND pe.state > 3 AND pe.account_code='03011'";
$auto_pj_no         = isset($_GET['auto_pj_no']) ? $_GET['auto_pj_no'] : "";
$sch_pj_name        = isset($_GET['sch_pj_name']) ? $_GET['sch_pj_name'] : "";
$sch_pj_name_null   = isset($_GET['sch_pj_name_null']) ? $_GET['sch_pj_name_null'] : "";
$sch_pe_no          = isset($_GET['sch_pe_no']) ? $_GET['sch_pe_no'] : "";
$sch_req_s_name     = isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_team           = isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$sch_share_card     = isset($_GET['sch_share_card']) ? $_GET['sch_share_card'] : "";
$sch_s_month        = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : ""; // 월간
$sch_e_month        = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : ""; // 월간
$sch_month_all      = isset($_GET['sch_month_all']) ? $_GET['sch_month_all'] : "all"; // 월간(전체)
$sch_payment_date   = isset($_GET['sch_payment_date']) ? $_GET['sch_payment_date'] : "";
$sch_account_code   = "03011";
$sch_wd_method      = isset($_GET['sch_wd_method']) ? $_GET['sch_wd_method'] : "";
$sch_card_num       = isset($_GET['sch_card_num']) ? $_GET['sch_card_num'] : "";
$sch_payment_contents = isset($_GET['sch_payment_contents']) ? $_GET['sch_payment_contents'] : "";
$sch_pj_kind        = isset($_GET['sch_pj_kind']) ? $_GET['sch_pj_kind'] : "";
$sch_month          = ($sch_s_month == $sch_e_month) ? $sch_e_month : "";
$sch_corp_my_c_no   = isset($_GET['sch_corp_my_c_no']) ? $_GET['sch_corp_my_c_no'] : "";
$pj_kind_list       = [];
$auto_pj_name       = "";

if(!empty($auto_pj_no))
{
    $auto_pj_sql    = "SELECT pj_name FROM project WHERE pj_no='{$auto_pj_no}'";
    $auto_pj_query  = mysqli_query($my_db, $auto_pj_sql);
    $auto_pj_result = mysqli_fetch_assoc($auto_pj_query);
    $auto_pj_name   = isset($auto_pj_result['pj_name']) ? $auto_pj_result['pj_name'] : "";

    $pj_kind_sql    = "SELECT * FROM project_expenses_kind WHERE pj_no='{$auto_pj_no}' ORDER BY k_priority ASC";
    $pj_kind_query  = mysqli_query($my_db, $pj_kind_sql);
    while($pj_kind  = mysqli_fetch_assoc($pj_kind_query))
    {
        $pj_kind_list[$pj_kind['pj_k_no']] = $pj_kind['k_name'];
    }
}
$smarty->assign("auto_pj_no", $auto_pj_no);
$smarty->assign("auto_pj_name", $auto_pj_name);
$smarty->assign("pj_kind_list", $pj_kind_list);


if(!empty($sch_corp_my_c_no)){
    if($sch_corp_my_c_no == 'default'){
        $add_where .= " AND pe.my_c_no IN(1,2,5,7)";
    }elseif($sch_corp_my_c_no == 'commerce'){
        $add_where .= " AND pe.my_c_no = 3";
    }
    $smarty->assign("sch_corp_my_c_no", $sch_corp_my_c_no);
}

if (!empty($sch_pj_name_null)){
    $add_where .= " AND pe.pe_no NOT IN(SELECT pje.pe_no FROM project_expenses as pje WHERE pje.pe_no != '')";
    $smarty->assign("sch_pj_name_null", $sch_pj_name_null);
}elseif (!empty($sch_pj_name)) {
    $add_where .= " AND pe.pe_no IN(SELECT pje.pe_no FROM project_expenses as pje WHERE pje.pj_name like '%{$sch_pj_name}%')";
    $smarty->assign("sch_pj_name", $sch_pj_name);
}

if (!empty($sch_pe_no)) {
    $add_where .= " AND pe.pe_no='" . $sch_pe_no . "'";
    $smarty->assign("sch_pe_no", $sch_pe_no);
}

if (!empty($sch_req_s_name)) {
    $add_where .= " AND pe.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_req_s_name}')";
    $smarty->assign("sch_req_s_name", $sch_req_s_name);
}

if (!empty($sch_state)) {
    $add_where .= " AND pe.state='" . $sch_state . "'";
    $smarty->assign("sch_state", $sch_state);
}

if(!empty($sch_team)) {
    if ($sch_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        $add_where          .= " AND pe.team IN ({$sch_team_code_where})";
    }
    $smarty->assign("sch_team", $sch_team);
}

if (!empty($sch_share_card)) {
    $add_where .= " AND pe.corp_card_no IN (SELECT `cc`.cc_no FROM corp_card `cc` WHERE `cc`.share_card like '%{$sch_share_card}%')";
    $smarty->assign("sch_share_card", $sch_share_card);
}

if($sch_month_all != 'all') { // sch month not alll
    if (!empty($sch_s_month)) {
        $add_where .= " AND DATE_FORMAT(pe.payment_date, '%Y-%m') >= '$sch_s_month'";
    }

    if (!empty($sch_e_month)) {
        $add_where .= " AND DATE_FORMAT(pe.payment_date, '%Y-%m') <= '$sch_e_month' ";
    }
}
$smarty->assign("sch_month_all",$sch_month_all);
$smarty->assign("sch_month", $sch_month);
$smarty->assign("sch_s_month", $sch_s_month);
$smarty->assign("sch_e_month", $sch_e_month);

if (!empty($sch_payment_date)) {
    $add_where .= " AND pe.payment_date='" . $sch_payment_date . "'";
    $smarty->assign("sch_payment_date", $sch_payment_date);
}

if (!empty($sch_account_code)) {
    $smarty->assign("sch_account_code", $sch_account_code);
}

if (!empty($sch_wd_method)) {
    $add_where .= " AND pe.wd_method='" . $sch_wd_method . "'";
    $smarty->assign("sch_wd_method", $sch_wd_method);
}

if (!empty($sch_card_num)) {
    $add_where .= " AND pe.card_num LIKE '%" . $sch_card_num . "%'";
    $smarty->assign("sch_card_num", $sch_card_num);
}

if (!empty($sch_payment_contents)) {
    $sch_payment_contents_list = explode(" ", $sch_payment_contents);
    $sch_payment_contents_trim_list = [];

    foreach($sch_payment_contents_list as $sch_payment_contents_word){
        $sch_payment_contents_trim_list[] =  trim($sch_payment_contents_word);
    }
    $sch_payment_contents_trim = implode("",$sch_payment_contents_trim_list);

    $add_where .= " AND (pe.payment_contents like '%" . $sch_payment_contents . "%' OR pe.payment_contents like '%{$sch_payment_contents_trim}%')";
    $smarty->assign("sch_payment_contents", $sch_payment_contents);
}

if (!empty($sch_pj_kind)) {
    $add_where .= " AND pe.pe_no IN(SELECT pje.pe_no FROM project_expenses pje WHERE pje.kind IN(SELECT pjk.pj_k_no FROM project_expenses_kind pjk WHERE pjk.k_name LIKE '%{$sch_pj_kind}%'))";
    $smarty->assign("sch_card_num", $sch_card_num);
}

# 정렬추가
$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "payment_date";
$ori_ord_type   = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "payment_date";
$add_orderby    = "";
$ord_type_by    = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";
    if(!empty($ord_type_by))
    {
        $ord_type_list = [];
        if($ord_type == 'payment_date'){
            $ord_type_list[] = "payment_date";
        }
        elseif($ord_type == 'pj_kind')
        {
            $ord_type_list[] = "pj_no";
            $ord_type_list[] = "pj_kind";
        }

        $orderby_val = "";
        if($ori_ord_type == $ord_type)
        {
            if($ord_type_by == '1'){
                $orderby_val = "ASC";
            }elseif($ord_type_by == '2'){
                $orderby_val = "DESC";
            }
        }else{
            $ord_type_by = '2';
            $orderby_val = "DESC";
        }

        foreach($ord_type_list as $ord_type_val){
            $add_orderby .= "{$ord_type_val} {$orderby_val}, ";
        }
    }else{
        $add_orderby .= "pe.pe_no DESC, ";
    }
}
$add_orderby .= "pe.pe_no DESC";

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);

# 전체 게시물 수
$cnt_sql    = "SELECT count(*) as cnt FROM personal_expenses pe WHERE {$add_where}";
$cnt_query  = mysqli_query($my_db, $cnt_sql);
$cnt_data   = mysqli_fetch_assoc($cnt_query);
$total_num  = $cnt_data['cnt'];

# 페이징 처리
$pages      = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num        = 20;
$pagenum    = ceil($total_num/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}
$offset = ($pages-1) * $num;

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "project_expenses_window.php", $pagenum, $search_url);

$smarty->assign("total_num", $total_num);
$smarty->assign("page", $pages);
$smarty->assign("search_url",$search_url);
$smarty->assign("pagelist", $pagelist);

# 리스트 쿼리
$personal_expenses_sql = "
    SELECT
        pe.*,
        (SELECT pje.pj_no FROM project_expenses pje WHERE pje.pe_no = pe.pe_no AND pje.display='1' LIMIT 1) as pj_no,
        (SELECT pje.pj_name FROM project_expenses pje WHERE pje.pe_no = pe.pe_no AND pje.display='1' LIMIT 1) as pj_name,
        (SELECT pje.kind FROM project_expenses pje WHERE pje.pe_no = pe.pe_no AND pje.display='1' LIMIT 1) as pj_kind,
        (SELECT my_c.c_name FROM my_company my_c WHERE my_c.my_c_no = pe.my_c_no) as my_c_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=(SELECT sub_t.team_code_parent FROM team sub_t WHERE sub_t.team_code=`pe`.team LIMIT 1)) as parent_group_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=pe.team) as group_name,
        (SELECT share_card FROM corp_card `cc` WHERE `cc`.cc_no = pe.corp_card_no LIMIT 1) as card_name,
        (SELECT s.staff_state FROM staff s where s.s_no=pe.s_no AND s.staff_state='1') AS s_display,
        (SELECT s.s_name FROM staff s where s.s_no=pe.s_no) AS s_name,
        (SELECT s.s_name FROM staff s where s.s_no=pe.req_s_no) AS req_s_name,
        (SELECT k_name FROM kind WHERE k_name_code=pe.account_code) AS account_code_name,
        (SELECT wd_state FROM withdraw wd where wd.wd_no=pe.wd_no) as wd_state,
        (SELECT regdate FROM withdraw wd where wd.wd_no=pe.wd_no) as wd_regdate,
        (SELECT wd_date FROM withdraw wd where wd.wd_no=pe.wd_no) as wd_date
    FROM personal_expenses pe
    WHERE {$add_where}
    ORDER BY {$add_orderby}
    LIMIT {$offset}, {$num}
";
$personal_expenses_query = mysqli_query($my_db, $personal_expenses_sql);
$personal_expenses_list  = [];
while($personal_expenses = mysqli_fetch_array($personal_expenses_query))
{
    if(!empty($personal_expenses['wd_regdate'])) {
        $personal_expenses['wd_regdate_day']  = date("Y/m/d",strtotime($personal_expenses['wd_regdate']));
        $personal_expenses['wd_regdate_time'] = date("H:i",strtotime($personal_expenses['wd_regdate']));
    }

    if(!empty($personal_expenses['regdate'])) {
        $personal_expenses['regdate_day']  = date("Y/m/d",strtotime($personal_expenses['regdate']));
        $personal_expenses['regdate_time'] = date("H:i",strtotime($personal_expenses['regdate']));
    }
    
    if(empty($personal_expenses['card_num'])){
        $empty_card_sql     = "SELECT `cc`.share_card, (SELECT t.team_name FROM team t WHERE t.team_code=(SELECT sub_t.team_code_parent FROM team sub_t WHERE sub_t.team_code=`cc`.team LIMIT 1)) as parent_group_name, (SELECT t.team_name FROM team t WHERE t.team_code=`cc`.team) as group_name FROM corp_card `cc` WHERE `cc`.card_type='3' AND `cc`.manager='{$personal_expenses['s_no']}'";
        $empty_card_query   = mysqli_query($my_db, $empty_card_sql);
        $empty_card_result  = mysqli_fetch_assoc($empty_card_query);

        if(isset($empty_card_result['share_card'])){
            $personal_expenses['card_name']         = $empty_card_result['share_card'];
            $personal_expenses['parent_group_name'] = $empty_card_result['parent_group_name'];
            $personal_expenses['group_name']        = $empty_card_result['group_name'];
        }
    }

    $pj_kind_name = "";
    if(!empty($personal_expenses['pj_kind'])){
        $pj_kind_sql    = "SELECT k_name FROM project_expenses_kind WHERE pj_k_no='{$personal_expenses['pj_kind']}'";
        $pj_kind_query  = mysqli_query($my_db, $pj_kind_sql);
        $pj_kind_result = mysqli_fetch_assoc($pj_kind_query);
        $pj_kind_name   = isset($pj_kind_result['k_name']) ? $pj_kind_result['k_name'] : "";
    }
    
    $personal_expenses['pj_kind_name']        = $pj_kind_name;
    $personal_expenses['state_name']          = isset($pe_state_option[$personal_expenses['state']]) ? $pe_state_option[$personal_expenses['state']] : "";
    $personal_expenses['wd_pay_date']         = isset($personal_expenses['wd_date']) ? date("Y-m-d",strtotime($personal_expenses['wd_date'])) : "";
    $personal_expenses['wd_method_name']      = isset($wd_method_option[$personal_expenses['wd_method']]) ? $wd_method_option[$personal_expenses['wd_method']] : "";
    $personal_expenses['payment_contents']    = htmlentities($personal_expenses['payment_contents']);

    $personal_expenses_list[] = $personal_expenses;
}

$smarty->assign("corp_my_company_option", getPeMyCompanyOption());
$smarty->assign("account_code_option", $account_code_option);
$smarty->assign("pe_state_option", $pe_state_option);
$smarty->assign("pe_wd_method_option", $wd_method_option);
$smarty->assign("sch_team_list", $sch_team_name_list);
$smarty->assign("personal_expenses_list", $personal_expenses_list);

$smarty->display('project_expenses_window.html');
?>