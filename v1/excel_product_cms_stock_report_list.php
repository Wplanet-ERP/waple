<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');
require('inc/helper/product_cms_stock.php');
require('inc/helper/logistics.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$lfcr = chr(10);
$work_sheet = $objPHPExcel->setActiveSheetIndex(0);

$work_sheet->mergeCells("A1:A2");
$work_sheet->mergeCells("B1:B2");
$work_sheet->mergeCells("C1:C2");
$work_sheet->mergeCells("D1:D2");
$work_sheet->mergeCells("E1:E2");
$work_sheet->mergeCells("F1:F2");
$work_sheet->mergeCells("G1:G2");
$work_sheet->mergeCells("H1:H2");
$work_sheet->mergeCells("I1:I2");
$work_sheet->mergeCells("J1:J2");
$work_sheet->mergeCells("K1:L1");
$work_sheet->mergeCells("M1:M2");
$work_sheet->mergeCells("N1:N2");
$work_sheet->mergeCells("O1:O2");
$work_sheet->mergeCells("P1:P2");
$work_sheet->mergeCells("Q1:Q2");
$work_sheet->mergeCells("R1:R2");
$work_sheet->mergeCells("S1:S2");
$work_sheet->mergeCells("T1:T2");

# H
$work_sheet
	->setCellValue('A1', "no")
	->setCellValue('B1', "일자")
	->setCellValue('C1', "입고/반출")
	->setCellValue('D1', "입고/반출형태")
	->setCellValue('E1', "확정 입고/반출형태")
	->setCellValue('F1', "문서번호")
	->setCellValue('G1', "계정과목")
	->setCellValue('H1', "납품업체")
	->setCellValue('I1', "공급업체")
	->setCellValue('J1', "와이즈 구성품명")
    ->setCellValue('K1', "물류창고")
    ->setCellValue('K2', "업체명")
    ->setCellValue('L2', "재고관리코드(SKU)")
    ->setCellValue('M1', "창고")
    ->setCellValue('N1', "수량")
	->setCellValue('O1', "메모")
	->setCellValue('P1', "재고 반영 구분")
	->setCellValue('Q1', "발주서")
	->setCellValue('R1', "입고단가(VAT포함)")
	->setCellValue('S1', "정상수량x입고단가")
	->setCellValue('T1', "유통기한(~까지)")
;


# 검색조건
$add_where = "1=1";
$add_order = "pcsr.no DESC";

$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));
$years_val  = date('Y-m-d', strtotime('-2 years'));

$sch_report_type    = isset($_GET['sch_report_type']) ? $_GET['sch_report_type'] : "";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_s_regdate      = isset($_GET['sch_s_regdate']) ? $_GET['sch_s_regdate'] : $months_val;
$sch_e_regdate      = isset($_GET['sch_e_regdate']) ? $_GET['sch_e_regdate'] : $today_val;
$sch_type           = isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_except_term    = isset($_GET['sch_except_term']) ? $_GET['sch_except_term'] : "";
$sch_confirm_state  = isset($_GET['sch_confirm_state']) ? $_GET['sch_confirm_state'] : "";
$sch_is_connect     = isset($_GET['sch_is_connect']) ? $_GET['sch_is_connect'] : "";
$sch_is_sub_connect = isset($_GET['sch_is_sub_connect']) ? $_GET['sch_is_sub_connect'] : "";
$sch_log_company    = isset($_GET['sch_log_company']) ? $_GET['sch_log_company'] : "";
$sch_option_name    = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_prd_sku        = isset($_GET['sch_prd_sku']) ? $_GET['sch_prd_sku'] : "";
$sch_sup_c_no       = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_stock_type     = isset($_GET['sch_stock_type']) ? $_GET['sch_stock_type'] : "";
$sch_memo           = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
$sch_is_order       = isset($_GET['sch_is_order']) ? $_GET['sch_is_order'] : "";
$sch_expiration     = isset($_GET['sch_expiration']) ? $_GET['sch_expiration'] : "";
$sch_reg_date_type  = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "months";
$add_stock_type     = isset($_GET['add_stock_type']) ? $_GET['add_stock_type'] : "";
$sch_log_doc_no     = isset($_GET['sch_log_doc_no']) ? $_GET['sch_log_doc_no'] : "";
$sch_is_log_doc     = isset($_GET['sch_is_log_doc']) ? $_GET['sch_is_log_doc'] : "";
$sch_log_subject    = isset($_GET['sch_log_subject']) ? $_GET['sch_log_subject'] : "";
$sch_log_run_c_no   = isset($_GET['sch_log_run_c_no']) ? $_GET['sch_log_run_c_no'] : "";
$sch_file_no        = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";

if (!empty($sch_report_type)) {
	$add_where .= " AND pcsr.report_type='{$sch_report_type}'";
}

if (!empty($sch_no)) {
	$add_where .= " AND pcsr.no='{$sch_no}'";
}

if (!empty($sch_s_regdate)) {
	$add_where .= " AND pcsr.regdate >= '{$sch_s_regdate}'";
}

if (!empty($sch_e_regdate)) {
	$add_where .= " AND pcsr.regdate <= '{$sch_e_regdate}'";
}

if (!empty($sch_type)) {
	$add_where .= " AND pcsr.type='{$sch_type}'";
	$smarty->assign("sch_type", $sch_type);
}

if (!empty($sch_state)) {
	$add_where .= " AND pcsr.state='{$sch_state}'";
}

if (!empty($sch_except_term)) {
	$add_where .= " AND pcsr.state NOT LIKE '%기간%'";
}

if (!empty($sch_confirm_state)) {
	$add_where .= " AND pcsr.confirm_state='{$sch_confirm_state}'";
}

if (!empty($sch_is_connect)) {
	if($sch_is_connect == '1'){
		$add_where .= " AND pcsr.connect_no > 0";
	}elseif($sch_is_connect == '2'){
		$add_where .= " AND pcsr.connect_no = 0";
	}
}

if (!empty($sch_is_sub_connect)) {
	if($sch_is_sub_connect == '1'){
		$add_where .= " AND (pcsr.sub_connect_no != '' AND pcsr.sub_connect_no IS NOT NULL)";
	}elseif($sch_is_sub_connect == '2'){
		$add_where .= " AND (pcsr.sub_connect_no IS NULL OR pcsr.sub_connect_no = '')";
	}
}

if (!empty($sch_option_name)) {
	$add_where .= " AND pcsr.option_name like '%{$sch_option_name}%'";
}

if (!empty($sch_log_company)) {
	$add_where .= " AND pcsr.log_c_no='{$sch_log_company}'";
}

if (!empty($sch_prd_sku)) {
	$add_where .= " AND pcsr.sku = '{$sch_prd_sku}'";
}

if (!empty($sch_sup_c_no)) {
	$add_where .= " AND pcsr.sup_c_no = '{$sch_sup_c_no}'";
}

if (!empty($sch_stock_type)) {
	$add_where .= " AND pcsr.stock_type like '%{$sch_stock_type}%'";
}

if (!empty($sch_memo)) {
	$add_where .= " AND pcsr.memo like '%{$sch_memo}%'";
}

if (!empty($sch_is_order)) {
	$add_where .= " AND pcsr.is_order ='{$sch_is_order}'";
}

if (!empty($sch_expiration)) {
	$add_where .= " AND pcsr.expiration = '{$sch_expiration}'";
}

if(!empty($sch_is_log_doc)){
	$add_where .= " AND (pcsr.log_doc_no IS NOT NULL AND pcsr.log_doc_no != '')";
}else{
	if (!empty($sch_log_doc_no)) {
		$add_where .= " AND pcsr.log_doc_no = '{$sch_log_doc_no}'";
	}
}

if (!empty($sch_log_subject)) {
	$add_where .= " AND pcsr.log_subject = '{$sch_log_subject}'";
}

if (!empty($sch_log_run_c_no)) {
	$add_where .= " AND lm.run_c_no = '{$sch_log_run_c_no}'";
}

if (!empty($sch_file_no)) {
	$add_where .= " AND pcsr.file_no = '{$sch_file_no}'";
}

# 쿼리
$report_sql = "
    SELECT
        pcsr.*,
        lm.run_c_no,
        (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.log_c_no) as log_c_name,
        (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.sup_c_no) as sup_c_name
    FROM product_cms_stock_report as pcsr
    LEFT JOIN logistics_management lm ON lm.doc_no=pcsr.log_doc_no
    WHERE {$add_where}
    ORDER BY {$add_order}
";
$report_query 			= mysqli_query($my_db, $report_sql);
$type_option        	= getTypeOption();
$log_subject_option 	= getSubjectNameOption();
$confirm_state_option  	= getConfirmStateOption();
$is_order_option  		= getStockIsOrderOption();
$log_run_company_list   = getAddrCompanyOption();
$idx = 3;
if(!!$report_query)
{
    while($report = mysqli_fetch_array($report_query))
    {
		$report['stock_type_name']  	= isset($type_option[$report['type']]) ? $type_option[$report['type']] : "";
		$report['confirm_state_name']  	= isset($confirm_state_option[$report['confirm_state']]) ? $confirm_state_option[$report['confirm_state']] : "";
		$report['is_order_name']  		= isset($is_order_option[$report['is_order']]) ? $is_order_option[$report['is_order']] : "";
		$report['log_subject_name'] 	= isset($log_subject_option[$report['log_subject']]) ? $log_subject_option[$report['log_subject']] : "";
		$report['log_run_c_name']   	= isset($log_run_company_list[$report['run_c_no']]) ? $log_run_company_list[$report['run_c_no']] : "";
		$report['ord_name'] 			= ($report['is_order'] == 1) ? "미등록" : "";
		$report['limit_date']			= "";
		$report['sup_loc_type']			= "";
		$report['unit_price']			= 0;
		$report['unit_sum_price']		= 0;

		if($report['ord_no'] > 0)
		{
			$commerce_order_sql     = "SELECT co.no, co.set_no, cos.sup_loc_type, cos.sup_c_name, cos.order_count, cos.title, co.quantity, co.unit_price, co.limit_date FROM commerce_order co LEFT JOIN commerce_order_set cos ON cos.no=co.set_no WHERE co.`no`='{$report['ord_no']}'";
			$commerce_order_query   = mysqli_query($my_db, $commerce_order_sql);
			$commerce_order_result  = mysqli_fetch_assoc($commerce_order_query);
			if($commerce_order_result['no'])
			{
				$report['set_no']           = $commerce_order_result['set_no'];
				$report['ord_name']         = $commerce_order_result['sup_c_name']." [{$commerce_order_result['order_count']}차]";
				$report['main_title']       = $commerce_order_result['title'];
				$report['unit_price']       = ($commerce_order_result['sup_loc_type'] == '1') ? $commerce_order_result['unit_price'] : "-";
				$report['quantity']         = $commerce_order_result['quantity'];
				$report['limit_date']       = $commerce_order_result['limit_date'];
				$report['sup_loc_type']     = $commerce_order_result['sup_loc_type'];
				$report['unit_sum_price']   = ($commerce_order_result['sup_loc_type'] == '1') ? $report['stock_qty']*$report['unit_price'] : 0;

				if($commerce_order_result['limit_date'])
				{
					$s_date_val = strtotime(date('Y-m-d'));
					$e_date_val = strtotime($commerce_order_result['limit_date']);
					$sub_day    = (int)(($e_date_val-$s_date_val)/86400);
					$report['sub_day'] = $sub_day;
				}
			}
		}

		$unit_price = ($report['sup_loc_type'] == '2') ? "-" : $report['unit_price'];

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $report['no'])
            ->setCellValue("B{$idx}", $report['regdate'])
            ->setCellValue("C{$idx}", $report['stock_type_name'])
            ->setCellValue("D{$idx}", $report['state'])
            ->setCellValue("E{$idx}", $report['confirm_state_name'])
            ->setCellValue("F{$idx}", $report['log_doc_no'])
            ->setCellValue("G{$idx}", $report['log_subject_name'])
			->setCellValue("H{$idx}", $report['log_run_c_name'])
			->setCellValue("I{$idx}", $report['sup_c_name'])
            ->setCellValue("J{$idx}", $report['option_name'])
            ->setCellValue("K{$idx}", $report['log_c_name'])
            ->setCellValue("L{$idx}", $report['sku'])
            ->setCellValue("M{$idx}", $report['stock_type'])
            ->setCellValue("N{$idx}", $report['stock_qty'])
            ->setCellValue("O{$idx}", $report['memo'])
            ->setCellValue("P{$idx}", $report['is_order_name'])
            ->setCellValue("Q{$idx}", $report['ord_name'])
            ->setCellValue("R{$idx}", $unit_price)
            ->setCellValue("S{$idx}", $report['unit_sum_price'])
            ->setCellValue("T{$idx}", $report['limit_date'])
		;
		$idx++;
    }
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:T2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00343a40');
$objPHPExcel->getActiveSheet()->getStyle("A1:T2")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle('A1:T2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:T2')->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("F3:H{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF9BBB59');
$objPHPExcel->getActiveSheet()->getStyle("J3:J{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFD4EAF9');
$objPHPExcel->getActiveSheet()->getStyle("K3:L{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFDDDDDD');
$objPHPExcel->getActiveSheet()->getStyle("N3:N{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFEE3');
$objPHPExcel->getActiveSheet()->getStyle("Q3:T{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFF2CC');

$objPHPExcel->getActiveSheet()->getStyle("A3:T{$idx}")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("A1:T{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:T{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("J3:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("L3:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("Q3:T{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("N3:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("J3:J{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("L3:L{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("O3:O{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("Q3:Q{$idx}")->getAlignment()->setWrapText(true);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);

$objPHPExcel->getActiveSheet()->getStyle("A1:T{$idx}")->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->setTitle('입고반출 리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_입고반출 리스트.xlsx");

header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
