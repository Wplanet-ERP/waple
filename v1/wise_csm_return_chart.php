<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/wise_csm.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');
require('inc/model/WiseCsm.php');

# Navigation & My Quick
$nav_prd_no  = "32";
$nav_title   = "반품/교환 현황";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$add_where          = "1=1 AND order_number != ''";
$company_model      = Company::Factory();
$csm_model          = WiseCsm::Factory();
$kind_model         = Kind::Factory();
$product_model 	    = ProductCms::Factory();
$cms_code		    = "product_cms";

$prd_group_list         = $kind_model->getKindGroupList($cms_code);
$prd_total_list         = $product_model->getPrdGroupData();
$dp_company_option      = $company_model->getDpList();
$company_option         = $csm_model->getReturnCompanyOption();
$return_reason_option   = $csm_model->getReturnReasonOption();

# X축 및 날짜검색 기본값
$yesterday_val  = date('Y-m-d', strtotime('-1 days'));
$week_val       = date('Y-m-d', strtotime('-1 weeks'));
$months_val     = date('Y-m-d', strtotime('-1 months'));
$to_month_val   = date('Y-m');
$months_val3    = date('Y-m', strtotime('-3 months'));
$months_val6    = date('Y-m', strtotime('-6 months'));
$year_val       = date('Y-m', strtotime('-1 years'));

$smarty->assign("yesterday_val", $yesterday_val);
$smarty->assign("to_month_val", $to_month_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("months_val3", $months_val3);
$smarty->assign("months_val6", $months_val6);
$smarty->assign("year_val", $year_val);

$sch_date_kind  = isset($_GET['sch_date_kind']) ? $_GET['sch_date_kind'] : "1";
$sch_date_type  = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "return_date";
$sch_date_val   = isset($_GET['sch_date_val']) ? $_GET['sch_date_val'] : ($sch_date_kind == '3' ? "months" : "week");
$today_s_w	    = date('w')-1;
$sch_s_month    = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m');
$sch_e_month    = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week     = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week     = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date     = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("{$yesterday_val} -1 weeks"));
$sch_e_date     = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : $yesterday_val;


$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);
$smarty->assign('sch_date_kind', $sch_date_kind);
$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_date_val', $sch_date_val);


# 상품 기본값 설정
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);

$prd_g1_list = $prd_g2_list = $prd_g3_list = [];
$prd_g1_name_list = $prd_g2_name_list = [];

foreach($prd_group_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
        $prd_g1_name_list[$prd_data['k_name_code']] = $prd_data['k_name'];
    }else{
        $prd_g2_list[$key]  = $prd_data;
        foreach($prd_data as $prd_tmp){
            $prd_g2_name_list[$key][$prd_tmp['k_name_code']] = $prd_tmp['k_name'];
        }
    }
}
$prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
$sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);


# 검색조건 처리
if (!empty($sch_prd) && $sch_prd != "0") { // 상품
    $add_where .= " AND cr.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND cr.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND cr.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

$sch_c_no            = isset($_GET['sch_c_no']) ? $_GET['sch_c_no'] : "";
$sch_dp_c_no         = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_return_reason   = isset($_GET['sch_return_reason']) ? $_GET['sch_return_reason'] : "";
$sch_return_type     = isset($_GET['sch_return_type']) ? $_GET['sch_return_type'] : "";

if(!empty($sch_c_no))
{
    $add_where .= " AND `cr`.c_no = '{$sch_c_no}'";
    $smarty->assign('sch_c_no', $sch_c_no);
}

if(!empty($sch_dp_c_no))
{
    $add_where .= " AND `cr`.dp_c_no = '{$sch_dp_c_no}'";
    $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
}

if(!empty($sch_return_reason))
{
    $add_where .= " AND `cr`.return_reason = '{$sch_return_reason}'";
    $smarty->assign('sch_return_reason', $sch_return_reason);
}

if(!empty($sch_return_type))
{
    $add_where .= " AND `cr`.return_type = '{$sch_return_type}'";
    $smarty->assign('sch_return_type', $sch_return_type);
}

# Y축 기본값
$sch_value_type  = isset($_GET['sch_value_type']) ? $_GET['sch_value_type'] : "1";
$sch_value_type_column = ($sch_value_type == '1') ? "COUNT(DISTINCT order_number)" : "SUM(cr.quantity)";
$smarty->assign('sch_value_type', $sch_value_type);


# 항목 기본값
$picker_list     = [];
$sch_label_type  = isset($_GET['sch_label_type']) ? $_GET['sch_label_type'] : "1";
$sch_label_type_column = "";
switch($sch_label_type){
    case '1':
        $sch_label_type_column = "cr.c_no";
        if(!empty($sch_c_no)){
            $picker_list = array($sch_c_no => $company_option[$sch_c_no]);
        }else{
            $picker_list = $company_option;
        }

        break;
    case '2':
        if($sch_prd){
            $prd_item    = $product_model->getItem($sch_prd);
            $picker_list = array($sch_prd => $prd_item['title']);
            $sch_label_type_column = "cr.prd_no";
        }else if($sch_prd_g2){
            $picker_list = $prd_g3_name_list[$sch_prd_g2];
            $sch_label_type_column = "cr.prd_no";
        }else if($sch_prd_g1){
            $picker_list = $prd_g2_name_list[$sch_prd_g1];
            $sch_label_type_column = "(SELECT p.k_name_code FROM product_cms p WHERE p.prd_no=cr.prd_no)";
        }else{
            $picker_list = $prd_g1_name_list;
            $sch_label_type_column = "(SELECT k.k_name_code FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=(SELECT p.k_name_code FROM product_cms p WHERE p.prd_no=cr.prd_no)))";
        }

        break;
    case '3':
        $sch_label_type_column = "cr.dp_c_no";
        if(!empty($sch_dp_c_no)){
            $picker_list = array($sch_dp_c_no => $dp_company_option[$sch_dp_c_no]);
        }else{
            $picker_list = $dp_company_option;
        }

        break;
    case '4':
        $sch_label_type_column = "cr.return_reason";
        if(!empty($sch_return_reason)){
            $picker_list = array($sch_return_reason => $return_reason_option[$sch_return_reason]);
        }else{
            $picker_list = $return_reason_option;
        }

        break;
    default:

}
$smarty->assign('sch_label_type', $sch_label_type);

# CHART
# 전체 기간 조회 및 누적데이터 조회
$all_date_where   = "";
$all_date_key	  = "";
$add_date_where   = "";
$add_date_column  = "";
$stats_list       = [];

if($sch_date_kind == '3') //월간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where  = " AND DATE_FORMAT(cr.{$sch_date_type}, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_date_column = "DATE_FORMAT(cr.{$sch_date_type}, '%Y%m')";
}
elseif($sch_date_kind == '2') //주간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}')";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where  = " AND DATE_FORMAT(cr.{$sch_date_type}, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_date_column = "DATE_FORMAT(DATE_SUB(cr.{$sch_date_type}, INTERVAL(IF(DAYOFWEEK(cr.{$sch_date_type})=1,8,DAYOFWEEK(cr.{$sch_date_type}))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_date_where  = " AND DATE_FORMAT(cr.{$sch_date_type}, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_date_column = "DATE_FORMAT(cr.{$sch_date_type}, '%Y%m%d')";
}

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";

# 기본 STAT 리스트 Init 및 x key 및 타이틀 설정
$x_label_list       = [];
$x_table_th_list    = [];
$stats_list         = [];
if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_kind == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        $x_label_list[$date['chart_key']]    = "'".$chart_title."'";
        $x_table_th_list[$date['chart_key']] = $chart_title;

        foreach($picker_list as $picker_key => $picker_label){
            $stats_list[$date['chart_key']][$picker_key] = 0;
        }
    }
}


# Y 컬럼 Label
$y_label_list       = [];
$y_total_label      = [];
$stats_total_list   = [];
foreach($picker_list as $picker_key => $picker_label)
{
    $y_total_label[$picker_key]     = $picker_label;
    $stats_total_list[$picker_key]  = 0;
}

$stats_sql = "
    SELECT
        {$add_date_column} as key_date,
        {$sch_label_type_column} as key_column,
        {$sch_value_type_column} as `cnt`
    FROM csm_return as cr
    WHERE {$add_where} 
    {$add_date_where}
    GROUP BY key_date, key_column
    ORDER BY key_date, key_column ASC
";

$stats_query = mysqli_query($my_db, $stats_sql);
while($stats = mysqli_fetch_assoc($stats_query))
{
    if($sch_label_type == '2'){
        if($sch_prd){
        }else if($sch_prd_g2){
        }else if($sch_prd_g1){
            $stats['key_column'] = sprintf('%05d', $stats['key_column']);
        }else{
            $stats['key_column'] = sprintf('%05d', $stats['key_column']);
        }
    }else if($sch_label_type == '4'){
        $stats['key_column'] = array_search($stats['key_column'], $return_reason_option);
    }

    $stats_list[$stats['key_date']][$stats['key_column']] = $stats['cnt'];
    $stats_total_list[$stats['key_column']] += $stats['cnt'];
}

arsort($stats_total_list);

foreach($stats_list as $key_date => $stats_data)
{
    if ($stats_data)
    {
        $idx_key = 0;
        foreach($stats_total_list as $sort_key => $sort_data)
        {
            $cnt = $stats_data[$sort_key];
            $full_data["each"]["line"][$idx_key]['title']  = $picker_list[$sort_key];
            $full_data["each"]["line"][$idx_key]['data'][] = $cnt;

            $full_data["each"]["bar"][$idx_key]['title']  = $picker_list[$sort_key];
            $full_data["each"]["bar"][$idx_key]['data'][] = $cnt;

            $y_label_list[$idx_key] = $picker_list[$sort_key];
            $idx_key++;
        }
    }
}

$smarty->assign("company_option", $company_option);
$smarty->assign("date_type_option", getCsmDateTypeOption());
$smarty->assign("dp_company_option", $dp_company_option);
$smarty->assign("return_reason_option", $return_reason_option);
$smarty->assign("return_type_option", getReturnTypeOption());
$smarty->assign('x_label_list', implode(',', $x_label_list));
$smarty->assign('x_table_th_list', json_encode($x_table_th_list));
$smarty->assign('legend_list', json_encode($y_label_list));
$smarty->assign('picker_list', $y_label_list);
$smarty->assign('full_data', json_encode($full_data));

$smarty->display('wise_csm_return_chart.html');

?>
