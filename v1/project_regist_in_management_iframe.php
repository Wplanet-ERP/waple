<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/project.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

# 변수 설정
$pj_no      = isset($_GET['pj_no']) ? $_GET['pj_no'] : "";
$pj_s_date  = isset($_GET['pj_s_date']) ? $_GET['pj_s_date'] : "";
$pj_e_date  = isset($_GET['pj_e_date']) ? $_GET['pj_e_date'] : "";
$editable   = false;


$cur_date = date('Y-m-d');
$project_sql = "
    SELECT
      team,
      confirm_content,
      resource_type,
      (SELECT COUNT(sub.pj_no) FROM project sub WHERE sub.pj_no='{$pj_no}' AND (sub.pj_s_date <= '{$cur_date}' AND sub.pj_e_date >= '{$cur_date}')) as active_pj_cnt,
      (SELECT COUNT(pere.pj_ere_no) FROM project_external_report_evaluation as pere WHERE pere.pj_er_no IN(SELECT sub.pj_er_no FROM project_external_report sub WHERE sub.active='1' AND sub.pj_no='{$pj_no}')) as cur_cnt
    FROM project `p` WHERE `p`.pj_no = {$pj_no} AND display='1' LIMIT 1"
;
$project_query   = mysqli_query($my_db, $project_sql);
$project 		 = mysqli_fetch_assoc($project_query);

$resource_type_val_list = [];
if(!empty($project['resource_type'])){
    $resource_type_val_list = explode(',', $project['resource_type']);
}
$project['resource_type_val_list'] = $resource_type_val_list;

$smarty->assign('confirm_content', $project['confirm_content']);
$smarty->assign('active_pj_cnt', $project['active_pj_cnt']);
$smarty->assign('cur_cnt', $project['cur_cnt']);
$smarty->assign('resource_type_val_list', $resource_type_val_list);

if($project['team'] == $session_team || $session_s_no == '62'){
    $editable = true;
}

$smarty->assign('pj_no', $pj_no);
$smarty->assign('pj_s_date', $pj_s_date);
$smarty->assign('pj_e_date', $pj_e_date);
$smarty->assign('editable', $editable);

$smarty->display('project_regist_in_management_iframe.html');
?>
