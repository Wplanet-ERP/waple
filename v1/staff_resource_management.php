<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/staff.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');

// 접근 권한
if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자")) {
    $smarty->display('access_company_error.html');
    exit;
}

# 프로세스 처리
$process  = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "add_new_resource")
{
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $new_post_date      = isset($_POST['new_post_date']) ? $_POST['new_post_date'] : "";
    $new_s_no           = isset($_POST['new_s_no']) ? $_POST['new_s_no'] : "";
    $new_s_name         = isset($_POST['new_s_name']) ? addslashes(trim($_POST['new_s_name'])) : "";
    $new_type           = isset($_POST['new_type']) ? $_POST['new_type'] : "";
    $new_pre_my_c_no    = isset($_POST['new_pre_my_c_no']) ? $_POST['new_pre_my_c_no'] : "";
    $new_pre_team       = isset($_POST['new_pre_team']) ? addslashes(trim($_POST['new_pre_team'])) : "";
    $new_pre_addr       = isset($_POST['new_pre_addr']) ? $_POST['new_pre_addr'] : "";
    $new_pre_position   = isset($_POST['new_pre_position']) ? addslashes(trim($_POST['new_pre_position'])) : "";
    $new_post_my_c_no   = isset($_POST['new_post_my_c_no']) ? $_POST['new_post_my_c_no'] : "";
    $new_post_team      = isset($_POST['new_post_team']) ? addslashes(trim($_POST['new_post_team'])) : "";
    $new_post_addr      = isset($_POST['new_post_addr']) ? $_POST['new_post_addr'] : "";
    $new_post_position  = isset($_POST['new_post_position']) ? addslashes(trim($_POST['new_post_position'])) : "";
    $new_notice         = isset($_POST['new_notice']) ? addslashes(trim($_POST['new_notice'])) : "";

    if($new_type == '10'){
        $ins_sql    = "INSERT INTO staff_resource SET s_no='{$new_s_no}', s_name='{$new_s_name}', `type`='{$new_type}', pre_my_c_no='{$new_pre_my_c_no}', pre_team='{$new_pre_team}', pre_addr='{$new_pre_addr}', pre_position='{$new_pre_position}', notice='{$new_notice}', post_date='{$new_post_date}', regdate=now()";
    }else{
        $ins_sql    = "INSERT INTO staff_resource SET s_no='{$new_s_no}', s_name='{$new_s_name}', `type`='{$new_type}', pre_my_c_no='{$new_pre_my_c_no}', pre_team='{$new_pre_team}', pre_addr='{$new_pre_addr}', pre_position='{$new_pre_position}', post_my_c_no='{$new_post_my_c_no}', post_team='{$new_post_team}', post_addr='{$new_post_addr}', post_position='{$new_post_position}', notice='{$new_notice}', post_date='{$new_post_date}', regdate=now()";
    }

    if(!mysqli_query($my_db, $ins_sql)){
        exit("<script>alert('추가등록에 실패했습니다');location.href='staff_resource_management.php?{$search_url}';</script>");
    }else{

        $staff_sql    = "SELECT s_no, team, team_list FROM staff WHERE s_no='{$new_s_no}'";
        $staff_query  = mysqli_query($my_db, $staff_sql);
        $staff_result = mysqli_fetch_assoc($staff_query);

        if(isset($staff_result['s_no']) && !empty($staff_result['s_no']))
        {
            $upd_staff_data = [];
            if($new_type == '1') {
                $new_team_list = $staff_result['team_list'].",".$new_post_team;
                $upd_staff_data["team_list"] = $new_team_list;
            }elseif($new_type == '10') {
                $team_list_val  = explode(',', $staff_result['team_list']);
                $team_list      = [];

                foreach($team_list_val as $team_val){
                    if($team_val != $new_pre_team){
                        $team_list[] = $team_val;
                    }
                }

                if($staff_result['team'] == $new_pre_team) {
                    $upd_staff_data["team"] = "00000";
                }
                $new_team_list = implode(',', $team_list);
                $upd_staff_data["team_list"] = $new_team_list;
            }else{
                if($new_pre_team != $new_post_team)
                {
                    $team_list_val  = explode(',', $staff_result['team_list']);
                    $team_list      = [];

                    foreach($team_list_val as $team_val){
                        if($team_val == $new_pre_team){
                            $team_list[] = $new_post_team;
                        }else{
                            $team_list[] = $team_val;
                        }
                    }

                    $new_team_list = implode(',', $team_list);

                    if($staff_result['team'] == $new_pre_team) {
                        $upd_staff_data["team"] = $new_post_team;
                    }
                    $upd_staff_data["team_list"] = $new_post_team;
                }
            }

            if($new_type != '10')
            {
                if($new_pre_my_c_no != $new_post_my_c_no){
                    $upd_staff_data["my_c_no"] = $new_post_my_c_no;
                }

                if($new_pre_addr != $new_post_addr){
                    $upd_staff_data["addr"] = $new_post_addr;
                }

                if($new_pre_position != $new_post_position){
                    $upd_staff_data["position"] = $new_post_position;
                }
            }

            if(!empty($upd_staff_data))
            {
                $upd_staff_sql = "UPDATE staff SET ";
                foreach($upd_staff_data as $key => $data)
                {
                    $upd_staff_sql .= "{$key}='{$data}', ";
                }

                $upd_staff_sql = rtrim($upd_staff_sql, ', ');
                $upd_staff_sql .= " WHERE s_no='{$new_s_no}'";
                mysqli_query($my_db, $upd_staff_sql);
            }

            /* 업무 변경 */
            $prev_s_team = $new_s_no."_".$new_pre_team;
            $new_s_team  = $new_s_no."_".$new_post_team;

            if($new_type == '10')
            {
                $req_staff_sql   = "SELECT prd_no, task_req_staff FROM product WHERE FIND_IN_SET('{$prev_s_team}', task_req_staff) > 0";
                $req_staff_query = mysqli_query($my_db, $req_staff_sql);
                while($req_staff_result = mysqli_fetch_assoc($req_staff_query))
                {
                    $req_prd_no     = $req_staff_result['prd_no'];
                    $req_staffs     = explode(",", $req_staff_result['task_req_staff']);
                    $new_req_staffs = [];

                    foreach($req_staffs as $req_staff){
                        if($req_staff != $prev_s_team){
                            $new_req_staffs[] = $req_staff;
                        }
                    }

                    $upd_req_staffs = implode(",", $new_req_staffs);
                    $upd_req_sql    = "UPDATE product SET task_req_staff='{$upd_req_staffs}' WHERE prd_no='{$req_prd_no}'";
                    if(!empty($req_prd_no) && !empty($upd_req_staffs))
                    {
                        mysqli_query($my_db, $upd_req_sql);
                    }
                }

                $run_staff_sql   = "SELECT prd_no, task_run_staff FROM product WHERE FIND_IN_SET('{$prev_s_team}', task_run_staff) > 0";
                $run_staff_query = mysqli_query($my_db, $run_staff_sql);
                while($run_staff_result = mysqli_fetch_assoc($run_staff_query))
                {
                    $run_prd_no     = $run_staff_result['prd_no'];
                    $run_staffs     = explode(",", $run_staff_result['task_run_staff']);
                    $new_run_staffs = [];

                    foreach($run_staffs as $run_staff){
                        if($run_staff != $prev_s_team){
                            $new_run_staffs[] = $run_staff;
                        }
                    }

                    $upd_run_staffs = implode(",", $new_run_staffs);
                    $upd_run_sql    = "UPDATE product SET task_run_staff='{$upd_run_staffs}' WHERE prd_no='{$run_prd_no}'";
                    if(!empty($run_prd_no) && !empty($upd_run_staffs))
                    {
                        mysqli_query($my_db, $upd_run_sql);
                    }
                }

                $run_default_sql   = "SELECT prd_no, task_run_staff_default FROM product WHERE task_run_staff_default ='{$prev_s_team}'";
                $run_default_query = mysqli_query($my_db, $run_default_sql);
                while($run_default_result = mysqli_fetch_assoc($run_default_query))
                {
                    $def_prd_no     = $run_default_result['prd_no'];
                    $upd_def_sql    = "UPDATE product SET task_run_staff_default=null WHERE prd_no='{$def_prd_no}'";

                    if(!empty($def_prd_no))
                    {
                        mysqli_query($my_db, $upd_def_sql);
                    }
                }
            }else
            {
                $req_staff_sql   = "SELECT prd_no, task_req_staff FROM product WHERE FIND_IN_SET('{$prev_s_team}', task_req_staff) > 0";
                $req_staff_query = mysqli_query($my_db, $req_staff_sql);
                while($req_staff_result = mysqli_fetch_assoc($req_staff_query))
                {
                    $req_prd_no     = $req_staff_result['prd_no'];
                    $req_staffs     = str_replace($prev_s_team, $new_s_team, $req_staff_result['task_req_staff']);

                    $upd_req_sql    = "UPDATE product SET task_req_staff='{$req_staffs}' WHERE prd_no='{$req_prd_no}'";
                    if(!empty($req_prd_no) && !empty($req_staffs))
                    {
                        mysqli_query($my_db, $upd_req_sql);
                    }
                }

                $run_staff_sql   = "SELECT prd_no, task_run_staff FROM product WHERE FIND_IN_SET('{$prev_s_team}', task_run_staff) > 0";
                $run_staff_query = mysqli_query($my_db, $run_staff_sql);
                while($run_staff_result = mysqli_fetch_assoc($run_staff_query))
                {
                    $run_prd_no     = $run_staff_result['prd_no'];
                    $run_staffs     = str_replace($prev_s_team, $new_s_team, $run_staff_result['task_run_staff']);

                    $upd_run_sql    = "UPDATE product SET task_run_staff='{$run_staffs}' WHERE prd_no='{$run_prd_no}'";
                    if(!empty($run_prd_no) && !empty($run_staffs))
                    {
                        mysqli_query($my_db, $upd_run_sql);
                    }
                }

                $run_default_sql   = "SELECT prd_no, task_run_staff_default FROM product WHERE task_run_staff_default ='{$prev_s_team}'";
                $run_default_query = mysqli_query($my_db, $run_default_sql);
                while($run_default_result = mysqli_fetch_assoc($run_default_query))
                {
                    $def_prd_no     = $run_default_result['prd_no'];
                    $def_staffs     = $new_s_team;

                    $upd_def_sql    = "UPDATE product SET task_run_staff_default='{$def_staffs}' WHERE prd_no='{$def_prd_no}'";
                    if(!empty($def_prd_no) && !empty($def_staffs))
                    {
                        mysqli_query($my_db, $upd_def_sql);
                    }
                }
            }

        }

        exit("<script>alert('추가등록 되었습니다');location.href='staff_resource_management.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "77";
$nav_title   = "인사발령 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 인사발령 리스트
$add_where      = "1=1";
$sch_type       = isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_my_c_no    = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_team       = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_addr       = isset($_GET['sch_addr']) ? $_GET['sch_addr'] : "";
$sch_name       = isset($_GET['sch_name']) ? $_GET['sch_name'] : "";
$sch_position   = isset($_GET['sch_position']) ? $_GET['sch_position'] : "";
$sch_notice     = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";

if(!empty($sch_type)) {
    $add_where .= " AND sr.type='{$sch_type}'";
    $smarty->assign("sch_type", $sch_type);
}

if(!empty($sch_my_c_no)) {
    $add_where .= " AND (sr.pre_my_c_no='{$sch_my_c_no}' OR sr.post_my_c_no='{$sch_my_c_no}')";
    $smarty->assign("sch_my_c_no", $sch_my_c_no);
}

if(!empty($sch_team))
{
    if($sch_team != 'all')
    {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        if($sch_team_code_where){
            $add_where .= " AND (sr.pre_team IN ({$sch_team_code_where}) OR sr.post_team IN ({$sch_team_code_where}))";
        }
    }
    $smarty->assign("sch_team", $sch_team);
}

if(!empty($sch_addr)) {
    $add_where .= " AND (sr.pre_addr='{$sch_addr}' OR sr.post_addr='{$sch_addr}')";
    $smarty->assign("sch_addr", $sch_addr);
}

if(!empty($sch_name)) {
    $add_where .= " AND sr.s_name like '%{$sch_name}%'";
    $smarty->assign("sch_name", $sch_name);
}

if(!empty($sch_position)) {
    $add_where .= " AND (sr.pre_position like '%{$sch_position}%' OR sr.post_position like '%{$sch_position}%')";
    $smarty->assign("sch_position", $sch_position);
}

if(!empty($sch_notice)) {
    $add_where .= " AND sr.notice like '%{$sch_notice}%'";
    $smarty->assign("sch_notice", $sch_notice);
}

# 전체 게시물 수
$staff_resource_total_sql    = "SELECT count(sr_no) as cnt FROM staff_resource as sr WHERE {$add_where}";
$staff_resource_total_query  = mysqli_query($my_db, $staff_resource_total_sql);
$staff_resource_total_result = mysqli_fetch_array($staff_resource_total_query);
$staff_resource_total        = $staff_resource_total_result['cnt'];

# 페이징
$pages      = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num        = 20;
$pagenum    = ceil($staff_resource_total/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}
$offset     = ($pages-1) * $num;

# 검색 조건
$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "staff_resource_management.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $staff_resource_total);
$smarty->assign("pagelist", $pagelist);

# 리스트 쿼리
$staff_resource_sql = "
    SELECT
        *,
        DATE_FORMAT(`sr`.`post_date`, '%Y.%m.%d') as pos_date,
        (SELECT t.depth FROM team t WHERE t.team_code=sr.pre_team) as pre_t_depth,
        (SELECT t.depth FROM team t WHERE t.team_code=sr.post_team) as post_t_depth
    FROM staff_resource as sr
    WHERE {$add_where}
    ORDER BY sr.sr_no DESC
    LIMIT {$offset},{$num}
";
$staff_resource_query    = mysqli_query($my_db, $staff_resource_sql);
$staff_resource_list     = [];
$resource_type_option    = getStaffResourceTypeOption();
$staff_my_company_option = getStaffMyCompanyOption();
$company_addr_option     = getCompanyAddrOption();
$team_model              = Team::Factory();
$sch_team_name_list      = $team_model->getTeamFullNameList();
while($staff_resource = mysqli_fetch_assoc($staff_resource_query))
{
    $staff_resource['type_name']      = isset($resource_type_option[$staff_resource['type']]) ? $resource_type_option[$staff_resource['type']] : "";
    $staff_resource['pre_my_c_name']  = isset($staff_my_company_option[$staff_resource['pre_my_c_no']]) ? $staff_my_company_option[$staff_resource['pre_my_c_no']] : "";
    $staff_resource['post_my_c_name'] = isset($staff_my_company_option[$staff_resource['post_my_c_no']]) ? $staff_my_company_option[$staff_resource['post_my_c_no']] : "";
    $staff_resource['pre_team_name']  = getTeamFullName($my_db, $staff_resource['pre_t_depth'], $staff_resource['pre_team']);
    $staff_resource['post_team_name'] = !empty($staff_resource['post_t_depth']) ? getTeamFullName($my_db, $staff_resource['post_t_depth'], $staff_resource['post_team']) : "";
    $staff_resource['pre_addr_name']  = isset($company_addr_option[$staff_resource['pre_addr']]) ? $company_addr_option[$staff_resource['pre_addr']] : "";
    $staff_resource['post_addr_name'] = isset($company_addr_option[$staff_resource['post_addr']]) ? $company_addr_option[$staff_resource['post_addr']] : "";
    $staff_resource['color']          =  ($staff_resource['type'] == '8') ? 'red' : 'blue';

    $staff_resource_list[] = $staff_resource;
}

$smarty->assign("sch_team_list", $sch_team_name_list);
$smarty->assign("resource_type_option", $resource_type_option);
$smarty->assign("my_company_option", $staff_my_company_option);
$smarty->assign("company_addr_option", $company_addr_option);
$smarty->assign("staff_resource_list", $staff_resource_list);

$smarty->display('staff_resource_management.html');
?>
