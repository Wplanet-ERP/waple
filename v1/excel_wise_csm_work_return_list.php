<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/wise_csm.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "wd_no")
	->setCellValue('B1', "작성일")
	->setCellValue('C1', "CS처리자")
	->setCellValue('D1', "브랜드")
	->setCellValue('E1', "w_no")
	->setCellValue('F1', "주문번호")
	->setCellValue('G1', "예금주명")
	->setCellValue('H1', "사유")
	->setCellValue('I1', "금액")
	->setCellValue('J1', "출금완료일")
;

# 검색 쿼리
$add_where          = "w.prd_no='229' AND w.work_state='6'";
$sch_reg_s_date     = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : date("Y-m")."-01";
$sch_reg_e_date     = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : date("Y-m-d");
$sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
$sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
$sch_cs_staff       = isset($_GET['sch_cs_staff']) ? $_GET['sch_cs_staff'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_task_req       = isset($_GET['sch_task_req']) ? $_GET['sch_task_req'] : "";
$sch_wd_s_date     = isset($_GET['sch_wd_s_date']) ? $_GET['sch_wd_s_date'] : "";
$sch_wd_e_date     = isset($_GET['sch_wd_e_date']) ? $_GET['sch_wd_e_date'] : "";

if(!empty($sch_reg_s_date)) {
	$add_where .= " AND `w`.regdate >= '{$sch_reg_s_datetime}'";
}

if(!empty($sch_reg_e_date)) {
	$add_where .= " AND `w`.regdate <= '{$sch_reg_e_datetime}'";
}

if(!empty($sch_cs_staff)) {
	$add_where .= " AND `w`.task_req_s_no = '{$sch_cs_staff}'";
}

if(!empty($sch_brand)) {
	$add_where         .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
	$add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
	$add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

if(!empty($sch_task_req)) {
	$add_where .= " AND `w`.task_req LIKE '%사유 : {$sch_task_req}%'";
}

if(!empty($sch_wd_s_date)) {
	$add_where .= " AND `wd`.wd_date >= '{$sch_wd_s_date}'";
}

if(!empty($sch_wd_e_date)) {
	$add_where .= " AND `wd`.wd_date <= '{$sch_wd_e_date}'";
}

# 리스트 쿼리
$csm_work_return_sql  = "
    SELECT
        wd.wd_no,
        w.c_no,
        w.regdate,
        DATE_FORMAT(w.regdate,'%Y-%m-%d') as reg_day,
        w.task_req_s_no,
        (SELECT s.s_name FROM staff s WHERE s.s_no=w.task_req_s_no) AS req_s_name,
        (SELECT c.c_name FROM company c WHERE c.c_no=w.c_no) AS brand_name,
        w.w_no,
        wd.bk_name,
        w.task_req,
        wd.wd_money,
        wd.wd_date,
        (SELECT w.order_number FROM work_cms w WHERE w.w_no=w.linked_no) as ord_no
    FROM `work` w
    LEFT JOIN withdraw wd ON wd.wd_no=w.wd_no
    WHERE {$add_where}
    ORDER BY wd.wd_no DESC
";
$csm_work_return_query      = mysqli_query($my_db, $csm_work_return_sql);
$csm_work_return_list       = [];
$idx = 2;
while($csm_work_return = mysqli_fetch_assoc($csm_work_return_query))
{
	$task_req_tmp   = explode("사유 : ", $csm_work_return['task_req']);
	$reason 	 	= isset($task_req_tmp[1]) ? $task_req_tmp[1] : "";

	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit("A{$idx}", $csm_work_return['wd_no'], PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValueExplicit("B{$idx}", $csm_work_return['reg_day'], PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValueExplicit("C{$idx}", $csm_work_return['req_s_name'], PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValueExplicit("D{$idx}", $csm_work_return['brand_name'], PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValueExplicit("E{$idx}", $csm_work_return['w_no'], PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValueExplicit("F{$idx}", $csm_work_return['ord_no'], PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValueExplicit("G{$idx}", $csm_work_return['bk_name'], PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValueExplicit("H{$idx}", $reason, PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValue("I{$idx}", $csm_work_return['wd_money'])
		->setCellValue("J{$idx}", $csm_work_return['wd_date'])
	;
	$idx++;
}
$idx--;

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:J{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');

$objPHPExcel->getActiveSheet()->getStyle("A1:J{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:J{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("D2:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setWrapText(true);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
$objPHPExcel->getActiveSheet()->setTitle('커머스 환불 리스트');

$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_커머스 환불 리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
