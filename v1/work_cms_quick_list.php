<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/logistics.php');
require('inc/helper/work_cms.php');
require('inc/helper/wise_bm.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Staff.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');
require('inc/model/WorkCms.php');

$is_freelancer  = false;
if($session_staff_state == '2'){ // 프리랜서의 경우 기본 접근제한으로 설정
    $is_freelancer = true;
}

$is_cms_staff   = false;
if($session_team == "00244" || permissionNameCheck($session_permission, "마스터관리자")){
    $is_cms_staff = true;
}

# Model Init
$company_model  = Company::Factory();
$quick_model    = WorkCms::Factory();
$quick_model->setMainInit("work_cms_quick", "order_number");

# Process Start
$process = (isset($_POST['process']))?$_POST['process']:"";

if($process == "f_quick_state")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? $_POST['val'] : "";
    $upd_data       = array("order_number" => $order_number, "quick_state" => $value);
    $fail_cnt       = 0;

    if($value == "2") {
        $nuzam_brands       = getNuzamBrandList();
        $chk_brand_list     = array("nuzam" => 0, "other" => 0);
        $chk_brand_sql      = "SELECT * FROM work_cms_quick WHERE order_number='{$order_number}'";
        $chk_brand_query    = mysqli_query($my_db, $chk_brand_sql);
        while($chk_brand = mysqli_fetch_assoc($chk_brand_query))
        {
            if(in_array($chk_brand["c_no"], $nuzam_brands)){
                $chk_brand_list["nuzam"]++;
            }else{
                $chk_brand_list["other"]++;
            }
        }

        if(!empty($chk_brand_list))
        {
            if($chk_brand_list["nuzam"] > 0 && $chk_brand_list["other"] == 0){
                $upd_data['run_s_no'] = 311;
            }elseif($chk_brand_list["nuzam"] == 0 && $chk_brand_list["other"] > 0){
                $upd_data['run_s_no'] = 312;
            }else{
                $upd_data['run_s_no'] = 0;
                $fail_cnt++;
            }
        }
    }elseif($value == "4"){
        $chk_delivery_method_sql    = "SELECT delivery_method FROM work_cms_quick WHERE order_number='{$order_number}' LIMIT 1";
        $chk_delivery_method_query  = mysqli_query($my_db, $chk_delivery_method_sql);
        $chk_delivery_method_result = mysqli_fetch_assoc($chk_delivery_method_query);

        if(isset($chk_delivery_method_result['delivery_method']) && $chk_delivery_method_result['delivery_method'] == "2"){
            $upd_data['run_date'] = date("Y-m-d");
        }
    }

    if ($quick_model->update($upd_data)) {
        $chk_msg = ($fail_cnt > 0) ? "진행상태 변경에 성공했습니다. 누잠과 다른 브랜드가 혼합되어 작업지시자(납품처리자)를 지정하지 못한 건 입니다" : "진행상태 변경에 성공했습니다.";
        echo $chk_msg;
    }else {
        echo "진행상태 저장에 실패 하였습니다.";
    }
    exit;
}
elseif($process == "f_req_date")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$quick_model->update(array("order_number" => $order_number, "req_date" => $value)))
        echo "출고요청일 저장에 실패 하였습니다.";
    else
        echo "출고요청일이 저장 되었습니다.";
    exit;
}
elseif($process == "f_quick_memo")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$quick_model->update(array("order_number" => $order_number, "quick_memo" => $value)))
        echo "삽입요청 메모 저장에 실패 하였습니다.";
    else
        echo "삽입요청 메모가 저장 되었습니다.";
    exit;
}
elseif($process == "f_req_memo")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$quick_model->update(array("order_number" => $order_number, "req_memo" => $value)))
        echo "요청사항 저장에 실패 하였습니다.";
    else
        echo "요청사항이 저장 되었습니다.";
    exit;
}
elseif($process == "f_run_date")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$quick_model->update(array("order_number" => $order_number, "run_date" => $value)))
        echo "출고완료일 저장에 실패 하였습니다.";
    else
        echo "출고완료일이 저장 되었습니다.";
    exit;
}
elseif($process == "f_quick_info")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$quick_model->update(array("order_number" => $order_number, "quick_info" => $value)))
        echo "배차정보 저장에 실패 하였습니다.";
    else
        echo "배차정보가 저장 되었습니다.";
    exit;
}
elseif($process == "f_is_quick_issue")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$quick_model->update(array("order_number" => $order_number, "is_quick_issue" => $value)))
        echo "이슈발생 저장에 실패 하였습니다.";
    else
        echo "이슈발생이 저장 되었습니다.";
    exit;
}
elseif($process == "f_quick_issue")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$quick_model->update(array("order_number" => $order_number, "quick_issue" => $value)))
        echo "이슈사항 저장에 실패 하였습니다.";
    else
        echo "이슈사항이 저장 되었습니다.";
    exit;
}
elseif($process == "f_manager_memo")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$quick_model->update(array("order_number" => $order_number, "manager_memo" => $value)))
        echo "관리자메모 저장에 실패 하였습니다.";
    else
        echo "관리자메모가 저장 되었습니다.";
    exit;
}
elseif($process == "f_delivery_method")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$quick_model->update(array("order_number" => $order_number, "delivery_method" => $value)))
        echo "전달방식 저장에 실패 하였습니다.";
    else
        echo "전달방식이 저장 되었습니다.";
    exit;
}
elseif($process == "f_run_staff")
{
    $order_number   = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		    = (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$quick_model->update(array("order_number" => $order_number, "run_s_no" => $value)))
        echo "납품처리자 저장에 실패 하였습니다.";
    else
        echo "납품처리자가 저장 되었습니다.";
    exit;
}
elseif($process == "add_delivery")
{
    $order_number   = (isset($_POST['f_order_number'])) ? $_POST['f_order_number'] : "";
    $delivery_nos   = (isset($_POST['f_delivery_no'])) ? $_POST['f_delivery_no'] : "";
    $delivery_type  = (isset($_POST['f_delivery_type'])) ? $_POST['f_delivery_type'] : "";
    $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $delivery_no_tmp_list   = explode("\r\n", $delivery_nos);
    $delivery_no_list       = [];
    foreach($delivery_no_tmp_list as $delivery_no_tmp){
        $delivery_no_tmp_val = trim($delivery_no_tmp);

        if(!empty($delivery_no_tmp_val) && !isset($delivery_no_list[$delivery_no_tmp_val])){
            $delivery_no_list[$delivery_no_tmp_val] = $delivery_no_tmp_val;
        }
    }

    foreach($delivery_no_list as $delivery_no){
        $delivery_ins_data[] = array(
            "order_number"      => $order_number,
            "delivery_type"     => $delivery_type,
            "delivery_no"       => $delivery_no,
        );
    }

    $quick_upd_data = array(
        "order_number"  => $order_number,
        "quick_state"   => 4,
        "run_date"      => date("Y-m-d")
    );

    $del_sql        = "DELETE FROM work_cms_quick_delivery WHERE order_number='{$order_number}'";
    mysqli_query($my_db, $del_sql);

    if(!empty($delivery_ins_data))
    {
        $delivery_model = WorkCms::Factory();
        $delivery_model->setMainInit("work_cms_quick_delivery", "no");

        if(!$delivery_model->multiInsert($delivery_ins_data)){
            exit("<script>alert('등록 실패했습니다');location.href='work_cms_quick_list.php?{$search_url}';</script>");
        }else{
            $quick_model->update($quick_upd_data);
            exit("<script>alert('등록했습니다');location.href='work_cms_quick_list.php?{$search_url}';</script>");
        }
    }

    exit("<script>alert('등록 실패했습니다');location.href='work_cms_quick_list.php?{$search_url}';</script>");
}
elseif($process == "change_status")
{
    $chk_quick_list = isset($_POST['chk_quick_list']) ? explode(",", $_POST['chk_quick_list']) : "";
    $chk_value      = isset($_POST['chk_value']) && !empty($_POST['chk_value']) ? $_POST['chk_value'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $nuzam_brands   = getNuzamBrandList();
    $upd_data       = [];
    $chk_brand_list = [];
    $fail_cnt       = 0;

    if($chk_value == "2"){
        foreach ($chk_quick_list as $chk_quick)
        {
            $chk_brand_list   = array("nuzam" => 0, "other" => 0);
            $chk_brand_sql    = "SELECT * FROM work_cms_quick WHERE order_number='{$chk_quick}'";
            $chk_brand_query  = mysqli_query($my_db, $chk_brand_sql);
            while($chk_brand = mysqli_fetch_assoc($chk_brand_query))
            {
                if(in_array($chk_brand["c_no"], $nuzam_brands)){
                    $chk_brand_list["nuzam"]++;
                }else{
                    $chk_brand_list["other"]++;
                }
            }

            if(!empty($chk_brand_list))
            {
                if($chk_brand_list["nuzam"] > 0 && $chk_brand_list["other"] == 0){
                    $upd_data[] = array(
                        "order_number"  => $chk_quick,
                        "quick_state"   => $chk_value,
                        "run_s_no"      => 311
                    );
                }elseif($chk_brand_list["nuzam"] == 0 && $chk_brand_list["other"] > 0){
                    $upd_data[] = array(
                        "order_number"  => $chk_quick,
                        "quick_state"   => $chk_value,
                        "run_s_no"      => 312
                    );
                }else{
                    $upd_data[] = array(
                        "order_number"  => $chk_quick,
                        "quick_state"   => $chk_value,
                        "run_s_no"      => 0
                    );

                    $fail_cnt++;
                }
            }
        }
    }
    else{
        foreach ($chk_quick_list as $chk_quick)
        {
            $upd_data[] = array(
                "order_number"  => $chk_quick,
                "quick_state"   => $chk_value
            );
        }
    }

    if(!empty($upd_data))
    {
        if($quick_model->multiUpdate($upd_data)){
            $chk_msg = ($fail_cnt > 0) ? "진행상태 변경에 성공했습니다.\\n누잠과 다른 브랜드가 혼합되어\\n작업지시자(납품처리자)를 지정하지 못한 건이 {$fail_cnt}건 있습니다." : "진행상태 변경에 성공했습니다.";
            exit("<script>alert('{$chk_msg}');location.href='work_cms_quick_list.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('진행상태 변경에 실패했습니다');location.href='work_cms_quick_list.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('진행상태 변경에 실패했습니다');location.href='work_cms_quick_list.php?{$search_url}';</script>");
    }
}
else
{
    # Navigation & My Quick
    $nav_prd_no  = "";
    $is_my_quick = "";
    $nav_title   = "입/출고요청 리스트";
    if($session_staff_state == "1"){
        $nav_prd_no  = "169";
        $quick_model = MyQuick::Factory();
        $is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
    }

    $smarty->assign("is_my_quick", $is_my_quick);
    $smarty->assign("nav_title", $nav_title);
    $smarty->assign("nav_prd_no", $nav_prd_no);

    # 검색쿼리
    $sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
    $sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
    $sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
    $sch_get	= isset($_GET['sch'])?$_GET['sch']:"";

    $smarty->assign("sch_prd_g1", $sch_prd_g1);
    $smarty->assign("sch_prd_g2", $sch_prd_g2);
    $smarty->assign("sch_prd", $sch_prd);

    if(!empty($sch_get)) {
        $smarty->assign("sch", $sch_get);
    }

    $kind_model             = Kind::Factory();
    $product_model 	        = ProductCms::Factory();
    $kind_code		        = "product_cms";

    $loc_subject_option 	= getReportSubjectNameOption();
    $kind_chart_data_list 	= $kind_model->getKindChartData($kind_code);
    $prd_group_list 		= $kind_chart_data_list['kind_group_list'];
    $prd_group_name_list	= $kind_chart_data_list['kind_group_name'];
    $prd_group_code_list	= $kind_chart_data_list['kind_group_code'];
    $prd_group_code 		= implode(",", $prd_group_code_list);
    $prd_data_list			= $product_model->getPrdGroupChartData($prd_group_code);
    $prd_total_list			= $prd_data_list['prd_total'];
    $brand_option           = $product_model->getPrdBrandOption();
    $prd_g1_list = $prd_g2_list = $prd_g3_list = [];

    foreach($prd_group_list as $key => $prd_data)
    {
        if(!$key){
            $prd_g1_list = $prd_data;
        }else{
            $prd_g2_list[$key] = $prd_data;
        }
    }

    $prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
    $sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

    $smarty->assign("prd_g1_list", $prd_g1_list);
    $smarty->assign("prd_g2_list", $prd_g2_list);
    $smarty->assign("sch_prd_list", $sch_prd_list);

    $add_where       = "1=1";
    $add_total_where = "1=1 AND w.quick_state IN(1,2,3)";

    if (!empty($sch_prd) && $sch_prd != "0") { // 상품
        $add_where .= " AND (w.prd_type='1' AND w.prd_no='{$sch_prd}')";
    }else{
        if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
            $add_where .= " AND (w.prd_type='1' AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code='{$sch_prd_g2}'))";
        }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
            $add_where .= " AND (w.prd_type='1' AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}')))";
        }
    }

    $today_val      = date('Y-m-d');
    $week_val       = date('Y-m-d', strtotime('-1 weeks'));
    $month_val      = date('Y-m-d', strtotime('-1 months'));
    $months_val     = date('Y-m-d', strtotime('-3 months'));
    $year_val       = date('Y-m-d', strtotime('-1 years'));
    $years_val      = date('Y-m-d', strtotime('-2 years'));

    $smarty->assign("today_val", $today_val);
    $smarty->assign("week_val", $week_val);
    $smarty->assign("month_val", $month_val);
    $smarty->assign("months_val", $months_val);
    $smarty->assign("year_val", $year_val);
    $smarty->assign("years_val", $years_val);

    # 검색 처리
    $addr_company_option = getAddrCompanyOption();
    $search_url          = getenv("QUERY_STRING");

    if($is_freelancer)
    {
        $addr_company_option = array("2809"  => "위드플레이스");
        $sch_free_quick_type = isset($_GET['sch_free_quick_type']) ? $_GET['sch_free_quick_type'] : "1";

        if($sch_free_quick_type == '1')
        {
            $sch_sender_type    = '2809';
            $sch_reg_s_date     = isset($_GET[' sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : "";
            $sch_reg_e_date     = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : "";
            $sch_reg_date_type  = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "";
            $add_total_where   .= " AND w.sender_type = '2809'";
        }
        elseif($sch_free_quick_type == '2')
        {
            $sch_receiver_type  = '2809';
            $sch_reg_s_date     = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : "";
            $sch_reg_e_date     = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : "";
            $sch_reg_date_type  = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "";
            $add_total_where   .= " AND w.receiver_type = '2809'";
        }

        if(empty(getenv("QUERY_STRING"))){
            $search_url = "sch_free_quick_type={$sch_free_quick_type}";
        }

        $smarty->assign("sch_free_quick_type", $sch_free_quick_type);

    }else{
        $sch_reg_s_date         = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : $week_val;
        $sch_reg_e_date         = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : $today_val;
        $sch_reg_date_type      = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "week";
        $sch_sender_type 	    = isset($_GET['sch_sender_type']) ? $_GET['sch_sender_type'] : "";
        $sch_receiver_type 	    = isset($_GET['sch_receiver_type']) ? $_GET['sch_receiver_type'] : "";
    }

	$sch_w_no 			    = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
	$sch_quick_state        = isset($_GET['sch_quick_state']) ? $_GET['sch_quick_state'] : "";
    $sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
	$sch_req_memo 	        = isset($_GET['sch_req_memo']) ? $_GET['sch_req_memo'] : "";
    $sch_req_s_name	        = isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : "";
	$sch_req_date 		    = isset($_GET['sch_req_date']) ? $_GET['sch_req_date'] : "";
	$sch_run_s_date 	    = isset($_GET['sch_run_s_date']) ? $_GET['sch_run_s_date'] : "";
	$sch_run_e_date 	    = isset($_GET['sch_run_e_date']) ? $_GET['sch_run_e_date'] : "";
	$sch_delivery_method    = isset($_GET['sch_delivery_method']) ? $_GET['sch_delivery_method'] : "";
	$sch_delivery_no        = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
    $sch_is_quick_issue	    = isset($_GET['sch_is_quick_issue']) ? $_GET['sch_is_quick_issue'] : "";
    $sch_quick_issue 		= isset($_GET['sch_quick_issue']) ? $_GET['sch_quick_issue'] : "";
    $sch_run_c_no 		    = isset($_GET['sch_run_c_no']) ? $_GET['sch_run_c_no'] : "";
    $sch_c_no 		        = isset($_GET['sch_c_no']) ? $_GET['sch_c_no'] : "";
    $sch_prd_name 		    = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
    $sch_logistics          = isset($_GET['sch_logistics']) ? $_GET['sch_logistics'] : "";
    $sch_receiver_name      = isset($_GET['sch_receiver_name']) ? $_GET['sch_receiver_name'] : "";
    $sch_sku_name           = isset($_GET['sch_sku_name']) ? $_GET['sch_sku_name'] : "";
    $sch_quick_search 	    = isset($_GET['sch_quick_search']) ? $_GET['sch_quick_search'] : "";
    $sch_is_coupang 	    = isset($_GET['sch_is_coupang']) ? $_GET['sch_is_coupang'] : "";
    $sch_is_price 	        = isset($_GET['sch_is_price']) ? $_GET['sch_is_price'] : "";
    $sch_loc_subject        = isset($_GET['sch_loc_subject']) ? $_GET['sch_loc_subject'] : "";

	if(!empty($sch_w_no)){
        $add_where .= " AND w.w_no='{$sch_w_no}'";
        $smarty->assign('sch_w_no', $sch_w_no);
	}

    if(!empty($sch_reg_s_date) || !empty($sch_reg_e_date))
    {
        if(!empty($sch_reg_s_date)){
            $sch_reg_s_datetime = "{$sch_reg_s_date} 00:00:00";
            $add_where      .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
            $smarty->assign('sch_reg_s_date', $sch_reg_s_date);
        }

        if(!empty($sch_reg_e_date)){
            $sch_reg_e_datetime = "{$sch_reg_e_date} 23:59:59";
            $add_where      .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
            $smarty->assign('sch_reg_e_date', $sch_reg_e_date);
        }
    }
    $smarty->assign('sch_reg_date_type', $sch_reg_date_type);

    if(!empty($sch_quick_state)){
        $add_where .= " AND w.quick_state='{$sch_quick_state}'";
        $smarty->assign('sch_quick_state', $sch_quick_state);
    }

    if(!empty($sch_sender_type)){
        $add_where .= " AND w.sender_type='{$sch_sender_type}'";
        $smarty->assign('sch_sender_type', $sch_sender_type);
    }

    if(!empty($sch_receiver_type)){
        $add_where .= " AND w.receiver_type='{$sch_receiver_type}'";
        $smarty->assign('sch_receiver_type', $sch_receiver_type);
    }

    if(!empty($sch_order_number)){
        $add_where .= " AND w.order_number='{$sch_order_number}'";
        $smarty->assign('sch_order_number', $sch_order_number);
    }

    if(!empty($sch_req_memo)){
        $add_where .= " AND w.req_memo LIKE '%{$sch_req_memo}%'";
        $smarty->assign('sch_req_memo', $sch_req_memo);
    }

    if(!empty($sch_req_s_name)){
        $add_where .= " AND w.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_req_s_name}%')";
        $smarty->assign('sch_req_s_name', $sch_req_s_name);
    }

    if(!empty($sch_req_date)){
        $add_where .= " AND w.req_date='{$sch_req_date}'";
        $smarty->assign('sch_req_date', $sch_req_date);
    }

    if(!empty($sch_run_s_date)){
        $add_where .= " AND w.run_date >= '{$sch_run_s_date}'";
        $smarty->assign('sch_run_s_date', $sch_run_s_date);
    }

    if(!empty($sch_run_e_date)){
        $add_where .= " AND w.run_date <= '{$sch_run_e_date}'";
        $smarty->assign('sch_run_e_date', $sch_run_e_date);
    }

    if(!empty($sch_delivery_method)){
        $add_where .= " AND w.delivery_method = '{$sch_delivery_method}'";
        $smarty->assign('sch_delivery_method', $sch_delivery_method);
    }

    if(!empty($sch_delivery_no)){
        $add_where .= " AND w.order_number IN(SELECT DISTINCT order_number FROM work_cms_quick_delivery WHERE delivery_no = '{$sch_delivery_no}')";
        $smarty->assign('sch_delivery_no', $sch_delivery_no);
    }

    if(!empty($sch_is_quick_issue)){
        $add_where .= " AND w.is_quick_issue = 1";
        $smarty->assign('sch_is_quick_issue', $sch_is_quick_issue);
    }

    if(!empty($sch_quick_issue)){
        $add_where .= " AND w.quick_issue LIKE '%{$sch_quick_issue}%'";
        $smarty->assign('sch_quick_issue', $sch_quick_issue);
    }

    if(!empty($sch_run_c_no)){
        $add_where .= " AND w.run_c_no='{$sch_run_c_no}'";
        $smarty->assign('sch_run_c_no', $sch_run_c_no);
    }

    if(!empty($sch_c_no)){
        $add_where .= " AND w.c_no='{$sch_c_no}'";
        $smarty->assign('sch_c_no', $sch_c_no);
    }

    if(!empty($sch_prd_name)){
        $add_where .= " AND (w.prd_type='1' AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.title LIKE '%{$sch_prd_name}%'))";
        $smarty->assign('sch_prd_name', $sch_prd_name);
    }

    if(!empty($sch_receiver_name)){
        $add_where .= " AND w.receiver='{$sch_receiver_name}'";
        $smarty->assign('sch_receiver_name', $sch_receiver_name);
    }

    if(!empty($sch_sku_name)){
        $add_where .= " AND ( 
        (w.prd_type='2' AND w.prd_no IN(SELECT DISTINCT pcum.prd_unit FROM product_cms_unit_management pcum WHERE sku LIKE '%{$sch_sku_name}%')) 
        OR 
        (w.prd_type='1' AND w.prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.option_no IN(SELECT pcum.prd_unit FROM product_cms_unit_management pcum WHERE sku LIKE '%{$sch_sku_name}%') AND pcr.display='1')) 
        )";
        $smarty->assign('sch_sku_name', $sch_sku_name);
    }

    if(!empty($sch_logistics)){
        $logistics_sql    = "SELECT order_number FROM logistics_management WHERE lm_no='{$sch_logistics}'";
        $logistics_query  = mysqli_query($my_db, $logistics_sql);
        $logistics_result = mysqli_fetch_assoc($logistics_query);
        $logistics_list   = [];

        if(isset($logistics_result['order_number']) && !empty($logistics_result['order_number']))
        {
            $logistics_tmp = explode(",", $logistics_result['order_number']);
            foreach($logistics_tmp as $log_ord){
                $logistics_list[] = "'{$log_ord}'";
            }
        }

        if(!empty($logistics_list))
        {
            $logistics_where = implode(",", $logistics_list);
            $add_where .= " AND w.order_number IN ({$logistics_where})";
            $_GET['ord_page_type'] = 100;
        }
    }

    if(!empty($sch_quick_search)){
        if($sch_quick_search == '1'){
            $add_where .= " AND w.run_c_no NOT IN(5468, 5469)";
        }

        $smarty->assign('sch_quick_search', $sch_quick_search);
    }

    if(!empty($sch_is_coupang)){
        if($sch_is_coupang == '1'){
            $add_where .= " AND w.run_c_no NOT IN(5468, 5469)";
        }

        $smarty->assign('sch_is_coupang', $sch_is_coupang);
    }

    if(!empty($sch_is_price)){
        if($sch_is_price == '1'){
            $add_where .= " AND w.unit_price > 0";
        }elseif($sch_is_price == '2'){
            $add_where .= " AND w.unit_price = 0";
        }

        $smarty->assign('sch_is_price', $sch_is_price);
    }

    if(!empty($sch_loc_subject)){
        $add_where .= " AND lm.subject='{$sch_loc_subject}'";
        $smarty->assign('sch_loc_subject', $sch_loc_subject);
    }

    # 정렬기능
    $ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
    $ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";

    $add_orderby    = "w.w_no DESC, w.prd_no ASC";
    if(!empty($ord_type))
    {
        if($ord_type_by == '1'){
            $orderby_val = "ASC";
        }else{
            $orderby_val = "DESC";
        }

        $add_orderby = "w.{$ord_type} {$orderby_val}, w.prd_no ASC";
    }
    $smarty->assign('ord_type', $ord_type);
    $smarty->assign('ord_type_by', $ord_type_by);

    # 진행상태별 cnt
    $cms_state_cnt_sql   = "SELECT * FROM work_cms_quick w WHERE {$add_total_where} GROUP BY order_number";
    $cms_state_cnt_query = mysqli_query($my_db, $cms_state_cnt_sql);
    $cms_state_cnt_list  = array('1' => 0, '2' => 0, '3' => 0, 'issue' => 0, 'roc' => 0, 'zet' => 0, 'etc' => 0);
    $cms_state_area_list = [];
    while($cms_state_cnt_result = mysqli_fetch_assoc($cms_state_cnt_query))
    {
        $cms_state_cnt_list[$cms_state_cnt_result['quick_state']]++;

        if($cms_state_cnt_result['quick_state'] == '1')
        {
            if($cms_state_cnt_result['run_c_no'] == '5468'){
                $cms_state_cnt_list['roc']++;
            }elseif($cms_state_cnt_result['run_c_no'] == '5469'){
                $cms_state_cnt_list['zet']++;
            }else{
                $cms_state_cnt_list['etc']++;
            }

            if(!empty($cms_state_cnt_result['receiver'])){
                $cms_state_area_list[$cms_state_cnt_result['receiver']]++;
            }
        }

        if($cms_state_cnt_result['is_quick_issue'] == '1') {
            $cms_state_cnt_list['issue']++;
        }
    }
    $smarty->assign("cms_state_cnt_list", $cms_state_cnt_list);
    $smarty->assign("cms_state_area_list", $cms_state_area_list);

    # Work CMS LIST
    $add_where_group = "1=1";

    # 전체 게시물 수
    $cms_quick_total_sql	= "SELECT count(order_number) as cnt FROM (SELECT DISTINCT w.order_number FROM work_cms_quick w LEFT JOIN logistics_management AS lm ON lm.doc_no=w.doc_no WHERE {$add_where}) AS cnt";
    $cms_quick_total_query	= mysqli_query($my_db, $cms_quick_total_sql);
    $cms_quick_total_result = mysqli_fetch_array($cms_quick_total_query);
    $cms_quick_total        = $cms_quick_total_result['cnt'];

    # 페이징처리
    $page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
    $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= $page_type;
    $offset 	= ($pages-1) * $num;
    $pagenum 	= ceil($cms_quick_total/$num);

    if ($pages >= $pagenum){$pages = $pagenum;}
    if ($pages <= 0){$pages = 1;}


    $pagelist	= pagelist($pages, "work_cms_quick_list.php", $pagenum, $search_url);

    $smarty->assign("search_url", $search_url);
    $smarty->assign("total_num", $cms_quick_total);
    $smarty->assign("pagelist", $pagelist);
    $smarty->assign("ord_page_type", $page_type);

    # 주문번호 뽑아내기
    $cms_ord_sql    = "SELECT DISTINCT w.order_number FROM work_cms_quick w LEFT JOIN logistics_management AS lm ON lm.doc_no=w.doc_no WHERE {$add_where} AND w.order_number is not null ORDER BY {$add_orderby} LIMIT {$offset},{$num}";
    $cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);
    $order_number_list  = [];
    while($cms_ord_result = mysqli_fetch_assoc($cms_ord_query)){
        $order_number_list[] =  "'".$cms_ord_result['order_number']."'";
    }
    $order_numbers   = implode(',', $order_number_list);
    if(!empty($order_numbers)){
        $add_where_group .= " AND w.order_number IN({$order_numbers})";
    }else{
        $add_where_group .= " AND w.order_number IS NULL";
    }

    # 리스트 쿼리
    $cms_quick_sql = "
        SELECT
            *,
            DATE_FORMAT(w.regdate, '%Y-%m-%d') as reg_date,
            DATE_FORMAT(w.regdate, '%H:%i') as reg_time,
            (SELECT s.s_name FROM staff s WHERE s.s_no=w.req_s_no) as req_s_name,
            (SELECT s.s_name FROM staff s WHERE s.s_no=w.run_s_no) as run_s_name,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
            (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
            (SELECT title FROM product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
            (SELECT lm_no FROM logistics_management lm where lm.doc_no=w.doc_no) as lm_no,
            (SELECT quick_req_file_path FROM logistics_management lm where lm.doc_no=w.doc_no) as quick_file
        FROM
        (
            SELECT
                w.*,
                IF(w.zip_code, CONCAT('[',w.zip_code,']'), '') as postcode
            FROM work_cms_quick w
            WHERE {$add_where_group}
        ) as w
        ORDER BY {$add_orderby}
    ";
    $cms_quick_query    = mysqli_query($my_db, $cms_quick_sql);
    $cms_quick_list     = [];
    $final_price_list   = [];
    $total_qty_list     = [];
    $quick_delivery_list= [];

    $run_company_option         = getAddrCompanyOption();
    $quick_state_option         = getQuickStateOption();
    $quick_state_color_option   = getQuickStateColorOption();
    $with_log_c_no              = '2809';
    while ($cms_quick = mysqli_fetch_array($cms_quick_query))
    {
        $cms_quick['quick_state_color'] = isset($quick_state_color_option[$cms_quick['quick_state']]) ? $quick_state_color_option[$cms_quick['quick_state']]['color'] : "black";
        $cms_quick['quick_state_bg']    = isset($quick_state_color_option[$cms_quick['quick_state']]) ? $quick_state_color_option[$cms_quick['quick_state']]['bg'] : "white";;
        $cms_quick['req_date']          = ($cms_quick['req_date'] == '0000-00-00') ? "" : $cms_quick['req_date'];

        if(!isset($final_price_list[$cms_quick['order_number']])){
            $final_price_list[$cms_quick['order_number']] = $cms_quick['final_price'];
        }elseif($final_price_list[$cms_quick['order_number']] <= 0 && $cms_quick['final_price'] > 0){
            $final_price_list[$cms_quick['order_number']] = $cms_quick['final_price'];
        }

        $total_qty_list[$cms_quick['order_number']] += $cms_quick['quantity'];

        $unit_list = [];
        if($cms_quick['prd_type'] == '1'){
            $unit_sql   = "SELECT (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='{$with_log_c_no}') as sku, pcr.quantity as qty FROM product_cms_relation as pcr WHERE pcr.prd_no ='{$cms_quick['prd_no']}' AND pcr.display='1'";
            $unit_query = mysqli_query($my_db, $unit_sql);
            while($unit_result = mysqli_fetch_assoc($unit_query)){
                $unit_result['qty'] = $cms_quick['quantity']*$unit_result['qty'];
                $unit_list[] = $unit_result;
            }
            $cms_quick["unit_list"] = $unit_list;
        }else{
            $unit_sql       = "SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit='{$cms_quick['prd_no']}' AND pcum.log_c_no='{$with_log_c_no}'";
            $unit_query     = mysqli_query($my_db, $unit_sql);
            $unit_result    = mysqli_fetch_assoc($unit_query);
            $cms_quick["unit_list"][] = array("sku" => $unit_result['sku'], "qty" => $cms_quick['quantity']);
        }

        if(isset($cms_quick['receiver_hp']) && !empty($cms_quick['receiver_hp'])){
            $f_hp  = substr($cms_quick['receiver_hp'],0,4);
            $e_hp  = substr($cms_quick['receiver_hp'],7,15);
            $cms_quick['receiver_sc_hp'] = $f_hp."***".$e_hp;
        }

        $cms_quick['is_run_editable'] = ($is_freelancer) ? true : false;

        if(!empty($cms_quick['quick_file'])){
            $quick_file_list = explode(",", $cms_quick['quick_file']);
            $cms_quick['quick_file_cnt'] = count($quick_file_list);
        }

        if(!isset($quick_delivery_list[$cms_quick['order_number']])){
            $quick_delivery_sql     = "SELECT * FROM work_cms_quick_delivery WHERE order_number='{$cms_quick['order_number']}' ORDER BY `no`";
            $quick_delivery_query   = mysqli_query($my_db, $quick_delivery_sql);
            while($quick_delivery = mysqli_fetch_assoc($quick_delivery_query)){
                $cms_quick['delivery_type'] = $quick_delivery['delivery_type'];
                $quick_delivery_list[$cms_quick['order_number']][] = $quick_delivery['delivery_no'];
            }

            if(!empty($quick_delivery_list[$cms_quick['order_number']])){
                $cms_quick['delivery_text'] = implode("\n", $quick_delivery_list[$cms_quick['order_number']]);
            }
        }

        $cms_quick_list[$cms_quick['order_number']][] = $cms_quick;
    }

    $smarty->assign("is_freelancer", $is_freelancer);
    $smarty->assign('page_type_option', getPageTypeOption(4));
    $smarty->assign('is_exist_option', getIsExistOption());
    $smarty->assign('loc_subject_option', $loc_subject_option);
    $smarty->assign("brand_option", $brand_option);
    $smarty->assign("quick_state_option", $quick_state_option);
    $smarty->assign("quick_state_color_option", $quick_state_color_option);
    $smarty->assign("addr_company_option", $addr_company_option);
    $smarty->assign("run_company_option", $run_company_option);
    $smarty->assign("delivery_method_option", getQuickDeliveryMethodOption());
    $smarty->assign("quick_run_staff_list", getQuickRunStaffList());
    $smarty->assign("final_price_list", $final_price_list);
    $smarty->assign("total_qty_list", $total_qty_list);
    $smarty->assign("quick_delivery_list", $quick_delivery_list);
    $smarty->assign("cms_quick_list", $cms_quick_list);

    $smarty->display('work_cms_quick_list.html');
}
?>
