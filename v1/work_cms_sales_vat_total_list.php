<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/model/MyQuick.php');
require('inc/helper/commerce_sales.php');
require('inc/model/WorkCmsSettle.php');

# Navigation & My Quick
$nav_prd_no  = "174";
$nav_title   = "커머스 세무(부가세)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$check_year         = date("Y");
$check_mon          = date("m");
$chk_vat_s_mon      = "";
$chk_vat_e_mon      = "";
$chk_vat_mon_type   = "";

switch ($check_mon)
{
    case "1":
    case "2":
    case "3":
        $chk_vat_s_mon = $check_year."-01";
        $chk_vat_e_mon = $check_year."-03";
        break;
    case "4":
    case "5":
    case "6":
        $chk_vat_s_mon = $check_year."-04";
        $chk_vat_e_mon = $check_year."-06";
        break;
    case "7":
    case "8":
    case "9":
        $chk_vat_s_mon = $check_year."-07";
        $chk_vat_e_mon = $check_year."-09";
        break;
    case "10":
    case "11":
    case "12":
        $chk_vat_s_mon = $check_year."-10";
        $chk_vat_e_mon = $check_year."-12";
        break;
}

$add_where          = "1=1";
$sch_vat_s_mon      = isset($_GET['sch_vat_s_mon']) ? $_GET['sch_vat_s_mon'] : $chk_vat_s_mon;
$sch_vat_e_mon      = isset($_GET['sch_vat_e_mon']) ? $_GET['sch_vat_e_mon'] : $chk_vat_e_mon;

if(!empty($sch_vat_s_mon))
{
    $add_where .= " AND vat_month >= '{$sch_vat_s_mon}'";
    $smarty->assign("sch_vat_s_mon", $sch_vat_s_mon);
}

if(!empty($sch_vat_e_mon)){
    $add_where .= " AND vat_month <= '{$sch_vat_e_mon}'";
    $smarty->assign("sch_vat_e_mon", $sch_vat_e_mon);
}

$chk_s_mon = date("m", strtotime($sch_vat_s_mon));
if($chk_s_mon == '1'){
    $sch_vat_mon_type = 1;
}elseif($chk_s_mon == '4'){
    $sch_vat_mon_type = 2;
}elseif($chk_s_mon == '7'){
    $sch_vat_mon_type = 3;
}elseif($chk_s_mon == '10'){
    $sch_vat_mon_type = 4;
}
$smarty->assign("check_year", $check_year);
$smarty->assign("sch_vat_mon_type", $sch_vat_mon_type);

# 부가세 정산 판매처 리스트
$vat_model          = WorkCmsSettle::Factory();
$dp_company_list    = $vat_model->getSettleVatCompanyTotalList();
$main_brand_option  = getMainBrandOption();

# 부가세 리스트 Init
$dp_company_vat_list            = [];
$main_brand_vat_list            = [];
$dp_main_brand_vat_list         = [];
$dp_main_brand_vat_total_list   = [];
foreach($dp_company_list as $dp_c_no => $dp_c_name){
    $dp_company_vat_list[$dp_c_no] = array(
        "c_name"        => $dp_c_name,
        "card_price"    => 0,
        "cash_price"    => 0,
        "etc_price"     => 0,
        "tax_price"     => 0,
        "total_price"   => 0,
    );

    $dp_main_brand_vat_total_list[$dp_c_no] = array(
        "c_name"        => $dp_c_name,
        "card_price"    => 0,
        "cash_price"    => 0,
        "etc_price"     => 0,
        "tax_price"     => 0,
        "total_price"   => 0,
    );

    foreach($main_brand_option as $main_brand => $main_brand_data) {
        $main_brand_vat_list[$main_brand] = array(
            "brand_name"    => $main_brand_data['main_brand']." ({$main_brand_data['manager']})",
            "brand_exp"     => $main_brand_data['explain'],
            "card_price"    => 0,
            "cash_price"    => 0,
            "etc_price"     => 0,
            "tax_price"     => 0,
            "total_price"   => 0,
        );

        $dp_main_brand_vat_list[$dp_c_no][$main_brand] = array(
            "c_name"        => $dp_c_name,
            "brand_name"    => $main_brand_data['main_brand']." ({$main_brand_data['manager']})",
            "brand_exp"     => $main_brand_data['explain'],
            "card_price"    => 0,
            "cash_price"    => 0,
            "etc_price"     => 0,
            "tax_price"     => 0,
            "total_price"   => 0,
        );
    }
}

# 부가세 전체 합계
$vat_total_list = array(
    "card_price"    => 0,
    "cash_price"    => 0,
    "etc_price"     => 0,
    "tax_price"     => 0,
    "total_price"   => 0,
);

$brand_vat_total_list = array(
    "card_price"    => 0,
    "cash_price"    => 0,
    "etc_price"     => 0,
    "tax_price"     => 0,
    "total_price"   => 0,
);

# 부가세 계산
$vat_total_sql = "
    SELECT 
        vat_dp_c_no AS dp_c_no,
        c_no AS brand,
        (card_price/1.1) AS total_card_price,
        (cash_price/1.1) AS total_cash_price,
        (etc_price/1.1) AS total_etc_price,
        (tax_price/1.1) AS total_tax_price
    FROM work_cms_vat 
    WHERE {$add_where}
";
$vat_total_query    = mysqli_query($my_db, $vat_total_sql);
while($vat_total = mysqli_fetch_assoc($vat_total_query))
{
    $total_price    = $vat_total['total_card_price']+$vat_total['total_cash_price']+$vat_total['total_etc_price']+$vat_total['total_tax_price'];
    $main_brand_val = "";

    if($vat_total['brand'] > 0){
        $main_brand_val = checkMainBrandOption($vat_total['brand']);
    }

    $dp_company_vat_list[$vat_total['dp_c_no']]['card_price']  += $vat_total['total_card_price'];
    $dp_company_vat_list[$vat_total['dp_c_no']]['cash_price']  += $vat_total['total_cash_price'];
    $dp_company_vat_list[$vat_total['dp_c_no']]['etc_price']   += $vat_total['total_etc_price'];
    $dp_company_vat_list[$vat_total['dp_c_no']]['tax_price']   += $vat_total['total_tax_price'];
    $dp_company_vat_list[$vat_total['dp_c_no']]['total_price'] += $total_price;

    if(!empty($main_brand_val))
    {
        $main_brand_vat_list[$main_brand_val]['card_price']  += $vat_total['total_card_price'];
        $main_brand_vat_list[$main_brand_val]['cash_price']  += $vat_total['total_cash_price'];
        $main_brand_vat_list[$main_brand_val]['etc_price']   += $vat_total['total_etc_price'];
        $main_brand_vat_list[$main_brand_val]['tax_price']   += $vat_total['total_tax_price'];
        $main_brand_vat_list[$main_brand_val]['total_price'] += $total_price;

        $dp_main_brand_vat_list[$vat_total['dp_c_no']][$main_brand_val]['card_price']  += $vat_total['total_card_price'];
        $dp_main_brand_vat_list[$vat_total['dp_c_no']][$main_brand_val]['cash_price']  += $vat_total['total_cash_price'];
        $dp_main_brand_vat_list[$vat_total['dp_c_no']][$main_brand_val]['etc_price']   += $vat_total['total_etc_price'];
        $dp_main_brand_vat_list[$vat_total['dp_c_no']][$main_brand_val]['tax_price']   += $vat_total['total_tax_price'];
        $dp_main_brand_vat_list[$vat_total['dp_c_no']][$main_brand_val]['total_price'] += $total_price;

        $brand_vat_total_list['card_price']  += $vat_total['total_card_price'];
        $brand_vat_total_list['cash_price']  += $vat_total['total_cash_price'];
        $brand_vat_total_list['etc_price']   += $vat_total['total_etc_price'];
        $brand_vat_total_list['tax_price']   += $vat_total['total_tax_price'];
        $brand_vat_total_list['total_price'] += $total_price;

        $dp_main_brand_vat_total_list[$vat_total['dp_c_no']]['card_price']  += $vat_total['total_card_price'];
        $dp_main_brand_vat_total_list[$vat_total['dp_c_no']]['cash_price']  += $vat_total['total_cash_price'];
        $dp_main_brand_vat_total_list[$vat_total['dp_c_no']]['etc_price']   += $vat_total['total_etc_price'];
        $dp_main_brand_vat_total_list[$vat_total['dp_c_no']]['tax_price']   += $vat_total['total_tax_price'];
        $dp_main_brand_vat_total_list[$vat_total['dp_c_no']]['total_price'] += $total_price;
    }

    $vat_total_list['card_price']  += $vat_total['total_card_price'];
    $vat_total_list['cash_price']  += $vat_total['total_cash_price'];
    $vat_total_list['etc_price']   += $vat_total['total_etc_price'];
    $vat_total_list['tax_price']   += $vat_total['total_tax_price'];
    $vat_total_list['total_price'] += $total_price;
}

foreach($dp_main_brand_vat_list as $dp_c_no => $dp_vat_data)
{
    $dp_total = 0;
    foreach($dp_vat_data as $brand => $brand_vat_data)
    {
        if($brand_vat_data['card_price'] == 0 && $brand_vat_data['cash_price'] == 0 &&
            $brand_vat_data['etc_price'] == 0 && $brand_vat_data['tax_price'] == 0 && $brand_vat_data['total_price'] == 0){
            unset($dp_main_brand_vat_list[$dp_c_no][$brand]);
        }

        $dp_total += $brand_vat_data['total_price'];
    }

    if($dp_total == 0){
        unset($dp_main_brand_vat_list[$dp_c_no]);
    }
}

$smarty->assign("vat_total_list", $vat_total_list);
$smarty->assign("dp_company_vat_list", $dp_company_vat_list);
$smarty->assign("brand_vat_total_list", $brand_vat_total_list);
$smarty->assign("main_brand_vat_list", $main_brand_vat_list);
$smarty->assign("dp_main_brand_vat_list", $dp_main_brand_vat_list);
$smarty->assign("dp_main_brand_vat_total_list", $dp_main_brand_vat_total_list);

$smarty->display('work_cms_sales_vat_total_list.html');
?>
