<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
),
),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "블로그URL")
->setCellValue('A2', "신청수")
->setCellValue('A3', "선정수")
->setCellValue('C3', "선정율")
->setCellValue('A4', "취소건")
->setCellValue('A5', "마감 미준수")
->setCellValue('C5', "미준수율")
->setCellValue('A6', "블로거 평가")
->setCellValue('A7', "마지막 평가")
->setCellValue('A8', "신청일")
->setCellValue('B8', "닉네임")
->setCellValue('C8', "이름")
->setCellValue('D8', "휴대폰")
->setCellValue('E8', "이메일")
->setCellValue('F8', "카페아이디")
->setCellValue('G8', "구분")
->setCellValue('H8', "업체명")
->setCellValue('I8', "담당자")
->setCellValue('J8', "선택옵션")
->setCellValue('K8', "신청사연")
->setCellValue('L8', "선정여부")
->setCellValue('M8', "제목 키워드")
->setCellValue('N8', "포스팅제목")
->setCellValue('O8', "포스팅 URL")
->setCellValue('P8', "체험후 한마디")
->setCellValue('Q8', "마감준수여부")
->setCellValue('R8', "우편번호")
->setCellValue('S8', "주소")
->setCellValue('T8', "수령자 이름")
->setCellValue('U8', "수령자 핸드폰")
->setCellValue('V8', "배송 메세지")
->setCellValue('W8', "주민번호")
->setCellValue('X8', "은행명")
->setCellValue('Y8', "계좌번호")
->setCellValue('Z8', "예금주");

// 리스트 내용
$i=9;
$ttamount=0;

$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

$proc=(isset($_POST['process']))?$_POST['process']:"";
$idx=(isset($_GET['no']))?$_GET['no']:"";
$blog_url=(isset($_GET['blog_url']))?$_GET['blog_url']:"";
if (!empty($blog_url))
$smarty->assign("blog_url",$blog_url);

// 전체 게시물 수
$total_num = getcnt("application");
$totalnum = $total_num;

// 페이징
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;

$num = 20;

$pagenum = ceil($totalnum/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

//$page=pagelist($pages, "blogger_detail.php", $pagenum, $url);
//$smarty->assign("pagelist",$page);//
$noi = $totalnum - ($pages-1)*$num;
$offset = ($pages-1) * $num;
$smarty->assign("totalnum",$totalnum);
$total_num = getcnt("company");
$smarty->assign(array(
	"total_num"=>number_format($total_num)
));

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";
$field=array();
$keyword=array();
$where=array();
$opt_search=isset($_GET['opt_search'])?$_GET['opt_search']:"";
$search_text=isset($_GET['search_text'])?$_GET['search_text']:"";

if(!empty($search_text)&&!empty($opt_search)){
		switch($opt_search){
			case 1 : 
							$field[]="nick";
							$keyword[]=$search_text;
							$where[]="nick like '%".$search_text."%'";
							break;
			case 2 : 
							$field[]="username";
							$keyword[]=$search_text;
							$where[]="username like '%".$search_text."%'";
							break;
			case 3 : 
							$field[]="cafe_id";
							$keyword[]=$search_text;
							$where[]="cafe_id like '%".$search_text."%'";
							break;
			case 4 : 
							$field[]="hp";
							$keyword[]=$search_text;
							$where[]="hp like '%".$search_text."%'";
							break;
		}
}
$add_where=" where blog_url='".$blog_url."'";
if(sizeof($where)>0) {
	for($i=0;$i<sizeof($where);$i++) {
		$add_where.=" and ".$where[$i];
		$smarty->assign($field[$i],$keyword[$i]);
	}

}

//Blog_url로 제출한 신청 횟수 및 선정, 취소 조회

$application_sql="
			SELECT count(*) as apply_count,
			(select count(*) from application a where a.blog_url='".$data['blog_url']."' and a.a_state='1') as select_count,
			(select count(*) from application a where a.blog_url='".$data['blog_url']."' and a.a_state='3') as cancel_count
			FROM application  
		$add_where";
//echo "<br><br><br> application_query : ".$application_sql;

$idx_no=1;
$application_query=mysqli_query($my_db,$application_sql);
$application_data=mysqli_fetch_array($application_query);

if ($application_data['apply_count'] > 0)
		$select_percent=($application_data['select_count']/$application_data['apply_count'])*100;
	else
		$select_percent = '0';

$a_state_text=array(1=>'선정',2=>'탈락',3=>'선정취소');

foreach($a_state_text as $key => $value) {
	$state_stmt[]=array
		(
			"no"=>$key,
			"name"=>$value
		);
	$smarty->assign("select",$state_stmt);
}
//print_r($state_stmt);

//echo "<br><br><br>application_Count : ";print_r($application_data);
$smarty->assign("apply_count",$application_data['apply_count']);
$smarty->assign("select_count",$application_data['select_count']);
$smarty->assign("select_percent",$select_percent);
$smarty->assign("cancel_count",$application_data['cancel_count']);

//Blog_url로 Blog_no 추출
$blog_info_sql = "
			SELECT b_no as b_no, 
			b_memo as b_memo,
			reload as reload
			FROM blog  
			where blog_url = '".$blog_url."'
";
$blog_info_query=mysqli_query($my_db,$blog_info_sql);
$blog_info_data=mysqli_fetch_array($blog_info_query);
$smarty->assign("b_no",$blog_info_data['b_no']);
$smarty->assign("b_memo",$blog_info_data['b_memo']);
$reload=((empty($blog_info_data['reload']))?"____/__/__":date("Y/m/d H:i:s",strtotime($blog_info_data['reload'])));
$smarty->assign("reload",$reload);

//echo "<br><br><br>blog_info_Data : ";print_r($blog_info_data);

//추출한 Blog_no로 제출한 완료 보고 정보 조회
$total_report_sql = "SELECT count(*)  as total_report_count FROM report where b_no= '".$blog_info_data['b_no']."'";
$total_report_query=mysqli_query($my_db,$total_report_sql);
$total_report_data=mysqli_fetch_array($total_report_query);

//echo "<br><br><br>Total Report Count Query : ".$total_report_sql."<br>";
//echo "<br><br><br>";print_r($total_report_data);
//echo "<br><br><br>Total Report Count : ".$total_report_data['total_report_count']."<br>";
$report_info_sql = "							
						select count(*) as report_count
						from 
							promotion p 
						left join 
							report r
							on p.p_no = r.p_no
						where 
							p.reg_edate < r.r_datetime
							and b_no='".$blog_info_data['b_no']."'
";
$report_info_query=mysqli_query($my_db,$report_info_sql);
$report_info_data=mysqli_fetch_array($report_info_query);

//echo "<br><br><br>Report_sql : ".$report_info_sql."<br>";
//echo "<br><br><br>";print_r($report_info_data);
//echo "<br><br><br>late Report Count : ".$report_info_data['report_count']."<br>";

	if ($report_info_data['report_count'] > 0 && $total_report_data['total_report_count'] > 0)
		$report_percent=($report_info_data['report_count']/$total_report_data['total_report_count'])*100;
	else
		$report_percent = '0';
$smarty->assign("report_count",$report_info_data['report_count']);
$smarty->assign("total_report_count",$total_report_data['total_report_count']);
$smarty->assign("report_percent",$report_percent);

//echo "<br><br>Report_Data : ";print_r($report_info_data);

$sql="select count(*) from application".$add_where ;
//echo $sql;exit;
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);
 $total_num = $data[0];
$smarty->assign("total_num",$data[0]);

if($proc=="reload") {
	//print_r($_POST);
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['b_no']))?$_POST['b_no']:"";
	$sql="update blog set reload='".$nowdate."' where b_no='".$mno."'";
	//exit("<hr>".$sql);
	//echo $sql;exit;
	

	mysqli_query($my_db,$sql);

	exit("<script>alert('재평가를 완료하였습니다');location.href='blogger_detail.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
}


if($proc=="memo") {
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['b_no']))?$_POST['b_no']:"";
	$mtxt=(isset($_POST['b_memo']))?$_POST['b_memo']:"";
	$sql="update blog set b_memo='".addslashes($mtxt)."' where b_no='".$mno."'";
	//exit("<hr>".$sql);
	//echo $sql;exit;

	mysqli_query($my_db,$sql);

	exit("<script>alert('메모를 등록하였습니다');location.href='blogger_detail.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
}

// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query",$save_query);

$a_state_text=array(1=>'선정',2=>'탈락',3=>'선정취소');
foreach($a_state_text as $key => $value) {
	$state_stmt[]=array
		(
			"no"=>$key,
			"name"=>$value
		);
	$smarty->assign("select",$state_stmt);
}


$sql="
		select	*
		from application
		$add_where
		limit $offset,$num";
//echo "<br><br><br> application_query : ".$sql;

$idx_no=1;
$query=mysqli_query($my_db,$sql);
while($data=mysqli_fetch_array($query)) {
	//echo "<br><br><br> Application Data : ";print_r($data);

	$promotion_sql = " select * from promotion where p_no = '".$data['p_no']."'";
	$promotion_query=mysqli_query($my_db,$promotion_sql);
	$promotion_data=mysqli_fetch_array($promotion_query);
	//echo "<br><br><br> promotion Data : ";print_r($promotion_data);

	$report_sql = " select * from report where p_no = '".$data['p_no']."' and a_no='".$data['a_no']."'";
//echo "<br><br><br> Report sql : ".$report_sql;
	$report_query=mysqli_query($my_db,$report_sql);
	//$report_data=mysqli_fetch_array($report_query);
	while($report_info=mysqli_fetch_array($report_query)){
		$report_data[]=array
		(
			"r_no"=>$report_info['r_no'],
			"a_no"=>$data['a_no'],
			"post_title"=>$report_info['post_title'],
			"post_url"=>$report_info['post_url'],
			"r_memo"=>nl2br($report_info['memo']),
		);
	}
	//echo "<br><br><br> Report Data : ";print_r($report_data);
	$hp = explode("-",$data['hp']);
	$email = explode("@",$data['email']);

switch ($data['kind']){
	case 1:
		$data['kind_name'] = '체험';
		break;
	case 2:
		$data['kind_name'] = '기자';
		break;
	case 3:
		$data['kind_name'] = '배송체험';
		break;
}

switch ($data['a_state']){
	case 1:
		$a_state_name = '선정';
		break;
	case 2:
		$a_state_name = '탈락';
		break;
	case 3:
		$a_state_name = '선정취소';
		break;
}

switch ($report_data['limit_yn']){
	case 1:
		$limit_yn_name = '마감 준수';
		break;
	case 2:
		$limit_yn_name = '마감 미준수';
		break;
	case 3:
		$limit_yn_name = '완료 보고 미체출';
		break;
}
$opt_answer = "선택옵션 A : ".$data['opt_a_answer']."\n"."선택옵션 B : ".$data['opt_b_answer']."\n"."선택옵션 C : ".$data['opt_c_answer']."\n"."선택옵션 D : ".$data['opt_d_answer']."\n"."선택옵션 E : ".$data['opt_e_answer'];

foreach ($report_data as $key =>$value){

	if ($data['a_no']== $value['a_no']){

		$tmp_post_title[$data['a_no']][] = $value['post_title'];
		$tmp_post_url[$data['a_no']][] = $value['post_url'];
		$tmp_r_memo[$data['a_no']][] = $value['r_memo'];
	}

}
$post_title = nl2br($tmp_post_title[$data['a_no']][0])."\n".nl2br($tmp_post_title[$data['a_no']][1])."\n".nl2br($tmp_post_title[$data['a_no']][2]);
$post_url = $tmp_post_url[$data['a_no']][0]."\n".$tmp_post_url[$data['a_no']][1]."\n".$tmp_post_url[$data['a_no']][2];
$r_memo = $tmp_r_memo[$data['a_no']][0]."\n".$tmp_r_memo[$data['a_no']][1]."\n".$tmp_r_memo[$data['a_no']][2];
//echo "<br><br><br> post_title Data : ";print_r($post_title);
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('B1', $blog_url)
	->setCellValue('B2', $application_data['apply_count']."회")
	->setCellValue('B3', $application_data['select_count']."회")
	->setCellValue('D3', $select_percent."%")
	->setCellValue('B4', $application_data['cancel_count']."회")
	->setCellValue('B5', $report_info_data['report_count']."회")
	->setCellValue('D5', $report_percent."%")
	->setCellValue('B6', $blog_info_data['b_memo'])
	->setCellValue('B7', $reload)
	->setCellValue('A'.$i, $data['regdate'])
	->setCellValue('B'.$i, $data['nick'])
	->setCellValue('C'.$i, $data['username'])
	->setCellValue('D'.$i, $data['hp'])
	->setCellValue('E'.$i, $data['email'])
	->setCellValue('F'.$i, $data['cafe_id'])
	->setCellValue('G'.$i, $data['kind_name'])
	->setCellValue('H'.$i, $promotion_data['company'])
	->setCellValue('I'.$i, $promotion_data['name'])
	->setCellValue('J'.$i, $opt_answer)
	->setCellValue('K'.$i, $data['memo'])
	->setCellValue('L'.$i, $a_state_name)
	->setCellValue('M'.$i, $promotion_data['keyword_subject'])
	->setCellValue('N'.$i, $post_title)
	->setCellValue('O'.$i, $post_url)
	->setCellValue('P'.$i, nl2br($r_memo))
	->setCellValue('Q'.$i, $limit_yn_name)
	->setCellValue('R'.$i, $data['zipcode'])
	->setCellValue('S'.$i, $data['address1'].$data['address2'])
	->setCellValue('T'.$i, $data['username'])
	->setCellValue('U'.$i, $data['hp'])
	->setCellValue('V'.$i, $data['delivery_memo'])
	->setCellValue('X'.$i, $data['bk_jumin'])
	->setCellValue('W'.$i, $data['bk_title'])
	->setCellValue('Y'.$i, $data['bk_num'])
	->setCellValue('Z'.$i, $data['bk_name']);


	$objPHPExcel->setActiveSheetIndex(0)->getStyle('G'.$i)->getAlignment()->setWrapText(true);
	$objPHPExcel->setActiveSheetIndex(0)->getStyle('J'.$i)->getAlignment()->setWrapText(true);
	$i = $i+1;
}

$objPHPExcel->getActiveSheet()->getStyle('A8:Z'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A8:Z1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('00F3F3F6');
$objPHPExcel->getActiveSheet()->getStyle('A1:Z'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:Z'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setTitle('블로거 상세 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="excel_blogger_detail.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

exit;