<?php
require('../inc/common.php');
require('../inc/data_state.php');
require('../ckadmin.php');
require('../inc/helper/promotion.php');


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="1=1";

$p_no_get=isset($_GET['p_no'])?$_GET['p_no']:"";
if(!empty($p_no_get)) {
	$add_where.=" AND p.p_no = '".$p_no_get."'";
	$smarty->assign("p_no",$p_no_get);
}else{
	$smarty->display('access_error.html');
	exit;
}


$promotion_sql = "
	SELECT
		p.title,
		p.p_state,
		p.p_state1,
		(SELECT k.k_name FROM kind k WHERE k.k_code='location' and k_name_code=(SELECT c.location1 FROM company c WHERE c.c_no=p.c_no)) AS location1_name,
		(SELECT k.k_name FROM kind k WHERE k.k_code='location' and k_name_code=(SELECT c.location2 FROM company c WHERE c.c_no=p.c_no)) AS location2_name,
		p.company,
		(SELECT c.address1 FROM company c WHERE c.c_no=p.c_no) AS address1,
		(SELECT c.address2 FROM company c WHERE c.c_no=p.c_no) AS address2,
		(SELECT c.tel FROM company c WHERE c.c_no=p.c_no) AS tel,
		(SELECT c.parking FROM company c WHERE c.c_no=p.c_no) AS parking,
		p.kind,
		p.channel,
		p.exp_sdate,
		p.exp_edate,
		p.posting_num,
		p.reg_num,
		p.re_apply,
		p.subject_option,
		p.subject_option_memo,
		p.product,
		p.keyword_subject,
		p.pres_guide_email,
		p.refer_url1,
		p.refer_url2,
		p.cons_exp_time,
		p.cons_contact,
		p.cons_tel,
		p.cons_tel_perm,
		p.pres_reward,
		p.pres_reason,
		p.pres_money,
		p.pres_purpose,
		p.deli_shipping,
		p.deli_zone,
		p.deli_type,
		p.cafe_url
	FROM promotion p
	WHERE {$add_where}
";
//echo $promotion_sql; echo "<br>";//exit;

$promotion_result = mysqli_query($my_db, $promotion_sql);
$promotion = mysqli_fetch_array($promotion_result);

$promotion_channel_option = getPromotionChannelOption();
$promotion_kind_option    = getPromotionKindOption();

$channel_name = $promotion_channel_option[$promotion['channel']];
$kind_name 	  = $promotion_kind_option[$promotion['kind']];

//인스타그램 visit_num
if($promotion['channel'] == '2'){
	$smarty->assign('visit_num', $insta_visit_num);
}

// 요일배열
$datey=array('일','월','화','수','목','금','토');

// 줄바꿈 치환
$product_line_break;
$product_line_break = nl2br($promotion['product']);

$keyword_subject_line_break;
$keyword_subject_line_break = nl2br($promotion['keyword_subject']);

$pres_guide_email_line_break;
$pres_guide_email_line_break = nl2br($promotion['pres_guide_email']);

$promotion_title = (isset($promotion['title']) && !empty($promotion['title'])) ? $promotion['title'] : $promotion['company'];

$smarty->assign(
	array(
        "title"=>$promotion_title,
		"p_state"=>$promotion['p_state'],
		"p_state1"=>$promotion['p_state1'],
		"location1_name"=>$promotion['location1_name'],
		"location2_name"=>$promotion['location2_name'],
		"company"=>$promotion['company'],
		"address1"=>$promotion['address1'],
		"address2"=>$promotion['address2'],
		"tel"=>$promotion['tel'],
		"parking"=>$promotion['parking'],
		"channel"=>$promotion['channel'],
		"channel_name"=>$channel_name,
		"kind"=>$promotion['kind'],
		"kind_name"=>$kind_name,
		"exp_sdate"=>date("m/d(".$datey[date("w",strtotime($promotion['exp_sdate']))].")",strtotime($promotion['exp_sdate'])),
		"exp_edate"=>date("m/d(".$datey[date("w",strtotime($promotion['exp_edate']))].")",strtotime($promotion['exp_edate'])),
		"pres_num"=>$promotion['posting_num']/$promotion['reg_num'],
		"re_apply"=>$promotion['re_apply'],
		"subject_option"=>$promotion['subject_option'],
		"subject_option_memo"=>$promotion['subject_option_memo'],
		"product"=>$product_line_break,
		"keyword_subject"=>$keyword_subject_line_break,
		"pres_guide_email"=>$pres_guide_email_line_break,
		"refer_url1"=>$promotion['refer_url1'],
		"refer_url2"=>$promotion['refer_url2'],
		"cons_exp_time"=>$promotion['cons_exp_time'],
		"cons_contact"=>$promotion['cons_contact'],
		"cons_tel"=>$promotion['cons_tel'],
		"cons_tel_perm"=>$promotion['cons_tel_perm'],
		"pres_reward"=>date("m/d(".$datey[date("w",strtotime($promotion['pres_reward']))].")",strtotime($promotion['pres_reward'])),
		"pres_reason"=>$promotion['pres_reason'],
		"pres_money"=>number_format($promotion['pres_money']),
		"pres_purpose"=>$promotion['pres_purpose'],
		"deli_shipping"=>$promotion['deli_shipping'],
		"deli_zone"=>$promotion['deli_zone'],
		"deli_type"=>$promotion['deli_type'],
		"cafe_url"=>htmlspecialchars($promotion['cafe_url'])
	)
);

// 선정자 정보
$application_sql = "SELECT
											(SELECT b_memo FROM blog b WHERE b.blog_url=a.blog_url ORDER BY b_no DESC LIMIT 1) AS memo_split,
											a.a_state,
											a.nick,
											a.username,
											a.visit_num,
											a.email
										FROM
											application a
										LEFT JOIN
											blog b
											ON a.a_no=b.a_no
										LEFT JOIN
											report r
											ON a.a_no = r.a_no
										WHERE
											a.p_no = '{$p_no_get}'
											AND (a.a_state='1' OR a.a_state='3')
										GROUP BY
											a.a_no
										ORDER BY
											a.a_state ASC, a.visit_num DESC
								";
//echo $application_sql; echo "<br>";//exit;

$application_result = mysqli_query($my_db, $application_sql);
if(!!$application_result){
	while($application = mysqli_fetch_array($application_result)) {
		$selected_list[]=array(
			"memo_split"=>$application['memo_split'],
			"a_state"=>$application['a_state'],
			"nick"=>$application['nick'],
			"username"=>$application['username'],
			"visit_num"=>$application['visit_num'],
			"email"=>$application['email']
		);
	}
}

$smarty->assign("selected_list",$selected_list);

$smarty->display('popup/campaign_guide.html');
?>
