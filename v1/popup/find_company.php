<?php
require('../inc/common.php');
require('../inc/data_state.php');
require('../ckadmin.php');

// 접근 권한
if (!$session_s_no){
	$smarty->display('access_error.html');
	exit;
}

// GET : opener로 반환할 form name 저장
$f_name_get=isset($_GET['f_name'])?$_GET['f_name']:"";
$smarty->assign("f_name",$f_name_get);

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="1=1";
$f_corp_kind_get=isset($_GET['f_corp_kind'])?$_GET['f_corp_kind']:"";
$f_s_no_get=isset($_GET['f_s_no'])?$_GET['f_s_no']:"";
$f_cate1_get=isset($_GET['f_cate1'])?$_GET['f_cate1']:"";
$f_cate2_get=isset($_GET['f_cate2'])?$_GET['f_cate2']:"";
$sch_s_no_get=isset($_GET['sch_s_no'])?$_GET['sch_s_no']:"";
$f_location1_get=isset($_GET['f_location1'])?$_GET['f_location1']:"";
$f_location2_get=isset($_GET['f_location2'])?$_GET['f_location2']:"";
$f_c_name_get=isset($_GET['f_c_name'])?$_GET['f_c_name']:"";
$f_c_memo_get=isset($_GET['f_c_memo'])?$_GET['f_c_memo']:"";
$f_display_get=isset($_GET['f_display'])?$_GET['f_display']:"";
$f_license_type_get=isset($_GET['f_license_type'])?$_GET['f_license_type']:"";
	
if(!empty($f_corp_kind_get)) {
	$add_where.=" AND corp_kind='".$f_corp_kind_get."'";
	$smarty->assign("f_corp_kind",$f_corp_kind_get);
}
if(!empty($f_s_no_get)) {
	$add_where.=" AND s_no='".$f_s_no_get."'";
	$smarty->assign("f_s_no",$f_s_no_get);
}
if(!empty($f_c_name_get)) {
	$add_where.=" AND c_name like '%".$f_c_name_get."%'";
	$smarty->assign("f_c_name",$f_c_name_get);
}
if(!empty($f_c_memo_get)) {
	$add_where.=" AND c_memo like '%".$f_c_memo_get."%'";
	$smarty->assign("f_c_memo",$f_c_memo_get);
}
if(!empty($f_cate1_get)) {
	$add_where.=" AND cate1='".$f_cate1_get."'";
	$smarty->assign("f_cate1",$f_cate1_get);
}
if(!empty($f_cate2_get)) {
	$add_where.=" AND cate2='".$f_cate2_get."'";
	$smarty->assign("f_cate2",$f_cate2_get);
}
if(!empty($sch_s_no_get)) {
	if($sch_s_no_get != "all"){
		$add_where.=" AND s_no='".$sch_s_no_get."'";
	}
	$smarty->assign("sch_s_no",$sch_s_no_get);
}elseif(empty($sch_s_no_get) && permissionNameCheck($session_permission, "외주관리자")){
	$add_where.=" AND s_no='".$session_s_no."'";
}
if(!empty($f_location1_get)) {
	$add_where.=" AND location1='".$f_location1_get."'";
	$smarty->assign("f_location1",$f_location1_get);
}
if(!empty($f_location2_get)) {
	$add_where.=" AND location2='".$f_location2_get."'";
	$smarty->assign("f_location2",$f_location2_get);
}
if(!empty($f_display_get)) {
	$add_where.=" AND display='".$f_display_get."'";
	$smarty->assign("f_display",$f_display_get);
}
if(!empty($f_license_type_get)) {
	$add_where.=" AND license_type='".$f_license_type_get."'";
	$smarty->assign("f_license_type",$f_license_type_get);
}

/* 대표와 김윤영 아닌 경우 외주업체중 대표 담당자 건은 노출 제외 */
if($session_s_no!='1' && $session_s_no!='18'){
	if($f_corp_kind_get=='2'){
		$add_where.=" and not(s_no='1')";
	}
}

// 외주관리자 가져오기
$add_where_permission = str_replace("0", "_", $permission_name['외주관리자']);
	
$staff_sql="select s_no,s_name from staff where staff_state < '3' AND permission like '".$add_where_permission."'";
$staff_query=mysqli_query($my_db,$staff_sql);

while($staff_data=mysqli_fetch_array($staff_query)) {
	$staff[]=array(
			'no'=>$staff_data['s_no'],
			'name'=>$staff_data['s_name']
	);
	$smarty->assign("staff",$staff);
}

    if( $f_corp_kind_get == 1 ){
	    // (광고주)업종 분류 가져오기
	    $sql="select * from kind where k_code='job' and k_parent is null";
	    $result=mysqli_query($my_db,$sql);
	    while($kind=mysqli_fetch_array($result)) {
		    $job[]=array(
			    "job_name"=>trim($kind['k_name']),
			    "job_code"=>trim($kind['k_name_code'])
		    );
		    $smarty->assign("job",$job);
	    }
    } else {
        // (관계사)업종 분류 가져오기
	    $sql="select * from kind where k_code='company_job' and k_parent is null";
	    $result=mysqli_query($my_db,$sql);
	    while($kind=mysqli_fetch_array($result)) {
		    $job[]=array(
			    "job_name"=>trim($kind['k_name']),
			    "job_code"=>trim($kind['k_name_code'])
		    );
		    $smarty->assign("job",$job);
	    }
    }


    if( $f_corp_kind_get == 1 ){
	    // (광고주)2차 분류에 값이 있는 경우 2차 업종 분류 가져오기(2차)
	    if($f_cate2_get != ""){
		    $sql="select k_name,k_name_code,k_parent from kind where k_code='job' and k_parent='".$f_cate1_get."'";
		    $result=mysqli_query($my_db,$sql);
		    while($kind=mysqli_fetch_array($result)) {
			    $editjob[]=array(
				    "job_name"=>trim($kind['k_name']),
				    "job_code"=>trim($kind['k_name_code']),
				    "job_parent"=>trim($kind['k_parent'])
			    );
			    $smarty->assign("editjob",$editjob);
		    }
	    }
    } else {
        // (관계사)2차 분류에 값이 있는 경우 2차 업종 분류 가져오기(2차)
	    if($f_cate2_get != ""){
		    $sql="select k_name,k_name_code,k_parent from kind where k_code='company_job' and k_parent='".$f_cate1_get."'";
		    $result=mysqli_query($my_db,$sql);
		    while($kind=mysqli_fetch_array($result)) {
			    $editjob[]=array(
				    "job_name"=>trim($kind['k_name']),
				    "job_code"=>trim($kind['k_name_code']),
				    "job_parent"=>trim($kind['k_parent'])
			    );
			    $smarty->assign("editjob",$editjob);
		    }
        }
	}

// 정렬순서 토글 & 필드 지정
$add_orderby.=" c_no desc";

// 페이에 따른 limit 설정
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num = 10;
$offset = ($pages-1) * $num;

// 리스트 쿼리
$company_sql="
			SELECT 
				a.c_no,
				a.s_no,
				(select s_name from staff b where s_no=a.s_no) as s_name,
				a.id,
				a.c_name,
				a.cate1,
				(select k_name from kind c where k_name_code=a.cate1) as cate1_name,
				a.cate2,
				(select k_name from kind c where k_name_code=a.cate2) as cate2_name,
				a.display,
				a.license_type,
				a.c_memo,
				a.c_memo_date
			FROM 
				company a 
			WHERE 
				$add_where 
			ORDER BY 
				$add_orderby  
			";			
//echo "$company_sql<br>"; exit;


// 전체 게시물 수
$cnt_sql = "SELECT count(*) FROM (".$company_sql.") AS cnt";
$result= mysqli_query($my_db, $cnt_sql);
$cnt_data=mysqli_fetch_array($result);
$total_num = $cnt_data[0];

$smarty->assign(array(
	"total_num"=>number_format($total_num)
));

$smarty->assign("total_num",$total_num);
$pagenum = ceil($total_num/$num);


// 페이징
if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$search_url = "f_corp_kind=".$f_corp_kind_get."&f_display=".$f_display_get."&f_license_type=".$f_license_type_get."&f_s_no=".$f_s_no_get."&f_cate1=".$f_cate1_get."&f_cate2=".$f_cate2_get."&f_location1=".$f_location1_get."&f_location2=".$f_location2_get."&f_c_name=".$f_c_name_get."&f_c_memo=".$f_c_memo_get."&sch_s_no=".$sch_s_no_get."&f_name=".$f_name_get;

$smarty->assign("search_url",$search_url);
$page=pagelist($pages, "find_company.php", $pagenum, $search_url);
$smarty->assign("pagelist",$page);
	
// 리스트 쿼리 결과(데이터)
$company_sql .= "LIMIT $offset,$num";
//echo "$company_sql<br>"; exit;

$company_query= mysqli_query($my_db, $company_sql);
while($company_array = mysqli_fetch_array($company_query)){
	
	$companys[] = array(
		"c_no"=>$company_array['c_no'],
		"company"=>$company_array['c_name'],
		"s_no"=>$company_array['s_no'],
		"name"=>$company_array['s_name'],
		"id"=>$company_array['id'],
		"f_cate1"=>$company_array['cate1'],
		"f_cate2"=>$company_array['cate2'],
		"f_location1"=>$company_array['location1'],
		"f_location2"=>$company_array['location2'],
		"f_cate1_name"=>$company_array['cate1_name'],
		"f_cate2_name"=>$company_array['cate2_name'],
		"f_loca1_name"=>$company_array['loca1_name'],
		"f_loca2_name"=>$company_array['loca2_name'],
		"display"=>$display[$company_array['display']][0],
		"license_type"=>$license_type[$company_array['license_type']][0],
		"c_memo"=>$company_array['c_memo'],
		"c_memo_date"=>$company_array['c_memo_date']
	);
	$smarty->assign(array(
		"company"=>$companys
	));
}
$smarty->display('popup/find_company.html');
?>