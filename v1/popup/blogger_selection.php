<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/data_state.php');


$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);


$proc=(isset($_POST['process']))?$_POST['process']:"";
$idx=(isset($_GET['no']))?$_GET['no']:"";
$sort_type=(isset($_GET['sort_type']))?$_GET['sort_type']:"";
$smarty->assign("p_no",$idx);

if ($proc == "selection_all_save")  // 선정여부 전체 저장
{
	$qstr           = (isset($_POST['list_query']))?$_POST['list_query']:"";
	$a_no_all_str   = (isset($_POST['a_no_all_str'])) ? $_POST['a_no_all_str'] : "";
	$select_all_str = (isset($_POST['select_all_str'])) ? $_POST['select_all_str'] : "";

	$select_arr     = explode('|', $select_all_str);
	$a_no_arr       = explode('|', $a_no_all_str);


	for($i = 0 ; $i < count($a_no_arr)-1 ; $i++){
		$sql="UPDATE application SET a_state='".$select_arr[$i]."' WHERE a_no='".$a_no_arr[$i]."'";

		if (!mysqli_query($my_db, $sql))
			echo "<script>alert('선정여부 저장에 일부 실패 하였습니다.');</script>";
	}
	exit("<script>alert('선정여부 일괄저장을 완료하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
}


if($proc=="reload") {
	//print_r($_POST);
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$sql="update blog set reload='".$nowdate."' where b_no='".$mno."'";
	//exit("<hr>".$sql);


	mysqli_query($my_db,$sql);

	exit("<script>alert('재평가를 완료하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."#tr_{$mno}';</script>");

}


if($proc=="memo") {
	//print_r($_POST);
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$ano=(isset($_POST['a_no']))?$_POST['a_no']:"";
	$mtxt=(isset($_POST['memo']))?$_POST['memo']:"";
	$sql="update blog set b_memo='".addslashes($mtxt)."' where b_no='".$mno."'";
	//exit("<hr>".$sql);
	//echo "<br><br>sql = ".$sql;exit;

	mysqli_query($my_db,$sql);

	exit("<script>alert('메모를 등록하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."#tr_{$ano}';</script>");

}

if($proc=="update") {
	//print_r($_POST);
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$sql="update application set a_state='".$_POST['selection']."' where a_no='".$mno."'";
	//echo $sql;exit;
	//exit("<hr>".$sql);


	mysqli_query($my_db,$sql);

	exit("<script>alert('선정상태를 저장 하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");

}

/* 미선정 블로거 탈락으로 일괄변경 20160325 김윤영 Start */
if($proc=="out_update") {
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['p_no']))?$_POST['p_no']:"";
	$sql="update application set a_state='2' where p_no='".$mno."' AND a_state='0'";

	mysqli_query($my_db,$sql);
	exit("<script>alert('미선정자를 모두 탈락으로 변경 하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."#tr_{$mno}';</script>");


}
/* 미선정 블로거 탈락으로 일괄변경 20160325 김윤영 End */
if($proc=="save_all") {
	//echo "<br><br><br>";
	//print_r($_POST);
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$mno_1=(isset($_POST['application_no']))?$_POST['application_no']:"";
	$a_no=(isset($_POST['a_no']))?$_POST['a_no']:"";
	$b_no=(isset($_POST['b_no']))?$_POST['b_no']:"";
//	$r_no=(isset($_POST['r_no']))?$_POST['r_no']:"";
	$r_no_01=(isset($_POST['r_no_01']))?$_POST['r_no_01']:"";
	$r_no_02=(isset($_POST['r_no_02']))?$_POST['r_no_02']:"";
	$r_no_03=(isset($_POST['r_no_03']))?$_POST['r_no_03']:"";
	$mtxt=(isset($_POST['memo']))?$_POST['memo']:"";
	//$zipcode = $_POST['mb_zip1']."-".$_POST['mb_zip2'];
	$zipcode = $_POST['mb_zip'];


	if ($us_id =='admin' || $us_id =='tblog' || permissionCheck($session_permission, "010001")){
		$sql="update application set
											cafe_id='".addslashes($_POST['cafe_id'])."',
											nick='".addslashes($_POST['nick'])."',
											username='".addslashes($_POST['uname'])."',
											blog_url='".addslashes($_POST['blog_url'])."',
											hp='".$_POST['hp1']."',
											email='".addslashes($_POST['email'])."',
											zipcode='".$zipcode."',
											address1='".addslashes($_POST['mb_addr1'])."',
											address2='".addslashes($_POST['mb_addr2'])."'
										where a_no='".$a_no."'";

		if ($a_no !=''){
			mysqli_query($my_db,$sql);
//			echo $sql2."<br>";
//			exit;
		}

		exit("<script>alert('메모,포스팅정보,선정여부,보상완료 외 레코드를 저장 하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."#tr_{$a_no}';</script>");
	}

	exit("<script>alert('저장이 되지 않았습니다.');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");

}

if($proc=="del_blogger_record") {
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$a_no=(isset($_POST['a_no']))?$_POST['a_no']:"";

	$sql = "DELETE FROM `application` WHERE a_no = '$a_no'";
	mysqli_query($my_db,$sql);

	exit("<script>alert('a_no = $a_no 신청 레코드를 삭제하였습니다.');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
}


if($proc=="regist") {
	//print_r($_POST);
	//exit;
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$mtxt=(isset($_POST['memo']))?$_POST['memo']:"";




	$sql="select blog_url,cafe_id,nick,username,hp,email from application where a_no='".$mno."'";
	//echo "<br><br><br><br><br><br>sql = ".$sql;
	$query=mysqli_query($my_db,$sql);
	$data=mysqli_fetch_array($query);

	$check_sql="select count(b_no) as cnt from blog where blog_url='".$data['blog_url']."'";
	//echo "<br><br><br><br><br><br>sql = ".$check_sql;
	$check_query=mysqli_query($my_db,$check_sql);
	$check_data=mysqli_fetch_array($check_query);
//print_r($check_data);exit;
	if ($check_data['cnt'] != 0) {
		exit("<script>alert('이미 블로거 등록이 되어 있습니다.');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
	} else {
		$insert="insert into blog set
						cafe_id='".addslashes($data['cafe_id'])."',
						nick='".addslashes($data['nick'])."',
						username='".addslashes($data['username'])."',
						blog_url='".addslashes($data['blog_url'])."',
						blog_md5='".md5($data['blog_url'])."',
						hp='".$data['hp']."',
						email='".addslashes($data['email'])."',
						regdate='".$nowdate."',
						b_memo='".addslashes($mtxt)."',
						reload='".$nowdate."',
						a_no='".$mno."'
				";

		//exit("<hr>".$insert);
		//echo "<br><br><br><br><br><br>sql = ".$insert;exit;
		mysqli_query($my_db,$insert);

		exit("<script>alert('블로거를 등록 하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."#tr_{$mno}';</script>");
	}
}

if($proc=="save_report") {
	//print_r($_POST);
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$mtxt=(isset($_POST['memo']))?$_POST['memo']:"";
	$a_no=(isset($_POST['a_no']))?$_POST['a_no']:"";
	$b_no=(isset($_POST['b_no']))?$_POST['b_no']:"";
	$p_no=(isset($_POST['p_no']))?$_POST['p_no']:"";
	$r_no=(isset($_POST['r_no']))?$_POST['r_no']:"";
	$blog_url_origin=(isset($_POST['blog_url_origin']))?addslashes($_POST['blog_url_origin']):"";
	$seq=(isset($_POST['seq']))?$_POST['seq']:"";

	switch ($seq){
		case '1' : $post_title = addslashes($_POST['post_title1']);
			$post_url = addslashes($_POST['post_url1']);
			break;
		case '2' : $post_title = addslashes($_POST['post_title2']);
			$post_url = addslashes($_POST['post_url2']);
			break;
		case '3' : $post_title = addslashes($_POST['post_title3']);
			$post_url = addslashes($_POST['post_url3']);
			break;
	}

	if ($post_title =='' && $post_url=='') // 빈칸 저장 시 삭제
	{
		$delete_sql="delete from report where r_no='".$r_no."'";
		$my_db->query($delete_sql);
	}

	if ($r_no != "r_no" && $post_title !='' && $post_url!='') // 수정시 변경
	{
		$sql="update report set post_title='".$post_title."', post_url='".$post_url."', blog_url='".$blog_url_origin."' where r_no='".$r_no."'";

		mysqli_query($my_db,$sql);
	}
	else if ($r_no == "r_no" && $post_title !='' && $post_url!='')
	{
		$select_sql     = "select promotion_code, c_no, s_no from promotion where p_no='".$p_no."'";
		$query          = mysqli_query($my_db,$select_sql);
		$promotion_info = mysqli_fetch_array($query);

		$sql="insert into report set
					p_no = '".$p_no."',
					p_code = '".$promotion_info['promotion_code']."',
					c_no = '".$promotion_info['c_no']."',
					b_no = '".$b_no."',
					s_no = '".$promotion_info['s_no']."',
					a_no = '".$a_no."',
					post_url = '".$post_url."',
					post_title = '".$post_title."',
					blog_url = '".$blog_url_origin."',
					r_datetime = '".date("Y-m-d H:i:s")."',
					r_ip = '".ip2long($_SERVER['REMOTE_ADDR'])."'
				";

		mysqli_query($my_db,$sql);
	}

	if(!$b_no)
		exit("<script>alert('블로거 등록을 먼저 하세요.');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");

	exit("<script>alert('포스팅을 저장하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."#tr_{$a_no}';</script>");
}

if($proc == 'copy_blogger_list')
{
    $msg          = "";
    $qstr         = (isset($_POST['list_query']))?$_POST['list_query']:"";
    $ori_p_no     = isset($_POST['ori_p_no']) ? $_POST['ori_p_no'] : "";
    $copy_p_no    = isset($_POST['copy_p_no']) ? $_POST['copy_p_no'] : "";
    $copy_method  = isset($_POST['copy_method']) ? $_POST['copy_method'] : "all";
    $copy_posting = isset($_POST['copy_posting']) ? $_POST['copy_posting'] : "";
    $copy_where   = "p_no = '{$copy_p_no}'";
    $copy_regdate = date('Y-m-d H:i:s');

    $orig_promo_sql    = "SELECT * FROM promotion WHERE p_no='{$ori_p_no}' LIMIT 1";
    $orig_promo_query  = mysqli_query($my_db, $orig_promo_sql);
    $orig_promo_result = mysqli_fetch_assoc($orig_promo_query);
    $orig_promo        = isset($orig_promo_result['p_no']) ? $orig_promo_result : "";

    if($orig_promo)
    {
        if($copy_method == 'all'){
            $copy_where .= "";
        }elseif($copy_method == 'select'){
            $copy_where .= " AND a_state='1'";
        }elseif($copy_method == 'cancel'){
            $copy_where .= " AND a_state='2'";
        }

        $copy_promo_sql    = "SELECT count(*) as cnt FROM promotion WHERE p_no='{$copy_p_no}' LIMIT 1";
        $copy_promo_query  = mysqli_query($my_db, $copy_promo_sql);
        $copy_promo_result = mysqli_fetch_assoc($copy_promo_query);

        if($copy_promo_result && (isset($copy_promo_result['cnt']) && $copy_promo_result['cnt'] > 0))
        {
            $ins_sql = "INSERT INTO application(`p_no`, `c_no`, `a_state`, `kind`, `blog_url`, `nick`, `username`, `hp`, `email`, `cafe_id`, `visit_num`, `bk_title`, `bk_num`, `bk_name`, `bk_jumin`, `zipcode`, `address1`, `address2`, `delivery_memo`, `memo`, `opt_a_answer`, `opt_b_answer`, `opt_c_answer`,`opt_d_answer`,`opt_e_answer`, `regdate`, `ip`, `limit_yn`) 
                        (SELECT '{$orig_promo['p_no']}', '{$orig_promo['c_no']}', a_state, '{$orig_promo['kind']}', `blog_url`, `nick`, `username`, `hp`, `email`, `cafe_id`, `visit_num`, `bk_title`, `bk_num`, `bk_name`, `bk_jumin`, `zipcode`, `address1`, `address2`, `delivery_memo`, `memo`, `opt_a_answer`, `opt_b_answer`, `opt_c_answer`,`opt_d_answer`,`opt_e_answer`, '{$copy_regdate}', `ip`, `limit_yn` FROM application WHERE {$copy_where} ORDER BY a_no)";

            if(mysqli_query($my_db, $ins_sql))
            {
                $msg = "블로거 리스트 복사에 성공했습니다";

                if($copy_posting){
                    $report_sql  = "INSERT INTO `report`(`post_url`, `post_title`, `r_datetime`, `r_ip`, `p_no`, `c_no`, `s_no`, `a_no`, `b_no`, `status`, `blog_url`, `p_code`, `file_name`, `file_path`) 
                                    (SELECT `post_url`, `post_title`, `r_datetime`, `r_ip`, '{$orig_promo['p_no']}', '{$orig_promo['c_no']}', '{$orig_promo['s_no']}', (SELECT a.a_no FROM application a WHERE a.p_no='{$orig_promo['p_no']}' AND a.blog_url=r.blog_url LIMIT 1), (SELECT b.b_no FROM blog b WHERE b.blog_url=r.blog_url LIMIT 1), `status`, `blog_url`, '{$orig_promo['promotion_code']}',`file_name`, `file_path` FROM report r WHERE r.p_no = '{$copy_p_no}')";

                    if(mysqli_query($my_db, $report_sql)){
                        $msg .= "\r\n포스팅 복사에 성공했습니다";
                    }else{
                        $msg .= "\r\n포스팅 복사에 실패했습니다";
                    }
                }

            }else{
                $msg = "블로거 리스트 복사에 실패했습니다";
            }
        }else{
            $msg = "복사할 프로모션 정보가 없습니다.";
        }
    }else{
        $msg = "현재 프로모션 데이터가 없습니다.";
    }

    exit("<script>alert('{$msg}');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
}


if($sort_type == "1") { // 선정된 블로거만 골라보기
	$add_where      = " and (a.a_state='1' or a.a_state='3')";
	$add_order_by   = " a.a_state ASC, a.visit_num DESC, blog_id ASC";
}elseif($sort_type=="2") { // 평가일 오래된순 정렬(지수체크용)
	$add_where      = "";
	$add_order_by   = " b_reload ASC";
}else{
	$add_where      = "";
	$add_order_by   = " a.a_state ASC, a.visit_num DESC, blog_id ASC";
}

$smarty->assign("sort_type",$sort_type);


if(!is_numeric($idx))
	exit("<script>alert('잘못된 접근입니다.');history.back();</script>");

// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query",$save_query);


$a_state_text = array(1=>'선정',2=>'탈락',3=>'선정취소');
foreach($a_state_text as $key => $value) {
	$state_stmt[] = array
	(
		"no"=>$key,
		"name"=>$value
	);
	$smarty->assign("select",$state_stmt);
}

/* 프로모션 정보 20160322 김윤영 Start */

$promotion_sql = "
    SELECT
	STRAIGHT_JOIN
		p.p_no,
		p.s_no,
		p.c_no,
		p.kind,
		p.channel,
		p.promotion_code,
		p.reg_num,
		p.posting_num,
		c.company,
		IFNULL(a.app_num, 0) as app_num,
		IFNULL(ROUND((p.posting_num /p.reg_num)*a.select_posting_num,0),0) as select_posting_num,
		IFNULL(a.cancel_num, 0) as cancel_num, 
		IFNULL(r.report_num,0) as report_num,
		IFNULL(a.total_refund, 0) as total_refund
	FROM promotion as p 
	LEFT JOIN (SELECT c_no, c_name as company FROM company) as c ON c.c_no = p.c_no
	LEFT JOIN (SELECT sub_a.p_no, SUM(sub_a.refund_count) as total_refund, COUNT(sub_a.a_no) as app_num, COUNT(CASE WHEN sub_a.a_state = 1 OR sub_a.a_state=3 THEN 1 END) as select_posting_num, COUNT(CASE WHEN sub_a.a_state = 3 THEN 1 END) as cancel_num FROM application as sub_a WHERE sub_a.p_no = '".$idx."') as a ON a.p_no = p.p_no
	LEFT JOIN (SELECT sub_r.p_no, COUNT(sub_r.r_no) as report_num FROM report as sub_r WHERE sub_r.p_no = '".$idx."') as r ON r.p_no = p.p_no
	WHERE p.p_no='".$idx."'
";

$promotion_query = mysqli_query($my_db,$promotion_sql);
$promotion_data  = mysqli_fetch_array($promotion_query);

//인스타그램 visit_num
if($promotion_data['channel'] == '2'){
    $smarty->assign('visit_num', $insta_visit_num);
}

$smarty->assign("channel",$promotion_data['channel']);
$smarty->assign("promotion_code",$promotion_data['promotion_code']);
$smarty->assign("company",$promotion_data['company']);
$smarty->assign("category",$promotion_data['kind']);
$smarty->assign("app_num",$promotion_data['app_num']);
$smarty->assign("reg_num",$promotion_data['reg_num']);
$smarty->assign("report_num",$promotion_data['report_num']);
$smarty->assign("cancel_num",$promotion_data['cancel_num']);
$smarty->assign("select_posting_num", $promotion_data['select_posting_num']);
$smarty->assign("posting_num",$promotion_data['posting_num']);
$smarty->assign("total_refund",$promotion_data['total_refund']);
$smarty->assign("c_no",$promotion_data['c_no']);

/* 프로모션 정보 20160322 김윤영 End */

/* 해당 프로모션 신청서 정보 START */
$application_sql = "
    SELECT
        a.a_no,
        a.p_no,
        a.c_no,
        a.a_state,
        a.kind,
        a.regdate,
        a.cafe_id,
        a.nick,
        a.username,
        a.blog_url,
        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(a.blog_url,'http://',''),'https://',''),'www.',''),'instagram.com/',''),'blog.naver.com/',''),'.blog.me',''),'/','') as blog_id,
        a.visit_num,
        COUNT(CASE WHEN b.b_no THEN 1 END) as blog_check,
        b.idx as blog_idx,
        b.b_no as blog_no,
        b.b_memo as b_memo,
        b.reload as b_reload,
        a.hp,
        a.email,
        a.memo,
        a.opt_a_answer,
        a.opt_b_answer,
        a.opt_c_answer,
        a.opt_d_answer,
        a.opt_e_answer,
        a.zipcode,
        a.address1,
        a.address2,
        a.real_visit_num,
        r.r_no,
        (SELECT sub_a.visit_num FROM application sub_a WHERE sub_a.blog_url=a.blog_url AND sub_a.visit_num > 0 ORDER BY sub_a.a_no DESC LIMIT 1) as con_visit_num
    FROM application as a
    LEFT JOIN blog as b ON b.blog_url = a.blog_url
    LEFT JOIN report as r ON r.a_no = a.a_no
    WHERE a.p_no='".$idx."'	$add_where
    GROUP BY a.a_no
    ORDER BY $add_order_by
";

$blog_urls = $a_nos = $blog_ids = $overlaps = []; //검색용 Url, a_no 값, 중복신청자
$stmt = $application_total_data = $application_info_data = $report_info_data = []; //b_no로 각종 데이터 처리 후 stmt로 출력
$a_state_num = $i = 0; //선정 인원수, Index 값

$blog_real_visit_blog_url = [];
$application_query = mysqli_query($my_db,$application_sql);
while($application_data = mysqli_fetch_array($application_query))
{
    $blog_url     = strtolower(trim($application_data['blog_url']));
    $blog_urls[]  = "'".$blog_url."'";
    $a_nos[] 	  = "'".$application_data['a_no']."'";
	$hp			  = explode('-',$application_data['hp']);
	$blog_id      = strtolower($application_data['blog_id']);

	if(in_array($blog_id, $blog_ids)){
	    $overlaps[] = $blog_id;
    }

	if($application_data['a_state'] == '1')
		$a_state_num++;

	if($b_no = $application_data['blog_no'])
        $report_info_data[$b_no] = [];

	if($blog_url)
	{
        $blog_real_visit     = $application_data['real_visit_num'];
        $blog_real_visit_val = 0;
        $blog_real_visit_avg = 0;
        $blog_real_visit_avg_name = $blog_real_visit."명";
        if($blog_real_visit == '0' && ($promotion_data['channel'] == '1' && $blog_id))
        {
            $url = "http://blog.naver.com/NVisitorgp4Ajax.nhn?blogId={$blog_id}";
            $blog_visit_data = file_get_contents($url);
            $blog_visit_xml = simplexml_load_string($blog_visit_data);

            if($blog_visit_xml !== false)
            {
                $blog_real_visit_val += $blog_visit_xml->visitorcnt[0]['cnt'];
                $blog_real_visit_val += $blog_visit_xml->visitorcnt[1]['cnt'];
                $blog_real_visit_val += $blog_visit_xml->visitorcnt[2]['cnt'];
                $blog_real_visit_val += $blog_visit_xml->visitorcnt[3]['cnt'];

                if($blog_real_visit_val > 0){
                    $blog_real_visit_avg = (int)($blog_real_visit_val/4);
                    $blog_real_visit_avg_name = $blog_real_visit_avg."명";
                }

                $blog_real_visit = $blog_real_visit_avg;

                $upd_real_sql = "UPDATE application SET real_visit_num='{$blog_real_visit}' WHERE a_no='{$application_data['a_no']}'";
                mysqli_query($my_db, $upd_real_sql);
            }else{
                $blog_real_visit_avg_name = "해당 블로그 없음";
            }
        }

        $blog_real_visit_blog_url[$application_data['a_state']][$blog_url] = $blog_real_visit;

        $is_influencer = true;
        if($promotion_data['channel'] == '7')
        {
			$page_result 	= @file_get_contents($blog_url);
            $is_influencer 	= ($page_result === false) ? false : true;
        }

        $application_total_data[$blog_url][] = Array
        (
            "idx"               => $i,
            "no"				=> $application_data['a_no'],
            "p_no"				=> $application_data['p_no'],
            "c_no"				=> $application_data['c_no'],
            "blog_no"			=> $application_data['blog_no'],
            "a_state"			=> $application_data['a_state'],
            "kind"				=> $application_data['kind'],
            "date_ymd"			=> date("Y.m.d",strtotime($application_data['regdate'])),
            "date_hi"			=> date("H:i",strtotime($application_data['regdate'])),
            "cafe_id"			=> $application_data['cafe_id'],
            "nick"				=> $application_data['nick'],
            "username"			=> $application_data['username'],
            "blog_url_origin"	=> $blog_url,
            "blog_url"			=> $blog_url,
            "visit_num"			=> $application_data['visit_num'],
            "con_visit_num"	    => $application_data['con_visit_num'],
            "blog_check"		=> $application_data['blog_check'],
            "b_memo"			=> $application_data['b_memo'],
            "reload"			=> ((empty($application_data['b_reload']))?"____/__/__":date("Y/m/d H:i:s",strtotime($application_data['b_reload']))),
            "hp"				=> $application_data['hp'],
            "hp0"				=> $hp[0],
            "hp1"				=> $hp[1],
            "hp2"				=> $hp[2],
            "email"				=> $application_data['email'],
            "memo"				=> $application_data['memo'],
            "answer_a"			=>"선택옵션 A : ".$application_data['opt_a_answer'],
            "answer_b"			=>"선택옵션 B : ".$application_data['opt_b_answer'],
            "answer_c"			=>"선택옵션 C : ".$application_data['opt_c_answer'],
            "answer_d"			=>"선택옵션 D : ".$application_data['opt_d_answer'],
            "answer_e"			=>"선택옵션 E : ".$application_data['opt_e_answer'],
            "a_state_text"		=> $a_state_text[$application_data['a_state']],
            "zipcode"			=> $application_data['zipcode'],
            "address1"			=> $application_data['address1'],
            "address2"			=> $application_data['address2'],
            "blog_real_visit"   => $blog_real_visit_avg_name,
            "is_influencer"   	=> $is_influencer,
        );

        $blog_ids[] = $blog_id;
        $i++;
    }
}
/* 해당 프로모션 신청서 정보 END */

/* 실방문자수 정렬 적용 START */
if($promotion_data['channel'] == '1'){
    $blog_urls = [];
    $real_idx  = 0;
    foreach($blog_real_visit_blog_url as $blog_real_visit_data){
        arsort($blog_real_visit_data);
        foreach($blog_real_visit_data as $blog_real_visit_url => $blog_real_visit) {
            $blog_urls[] = "'".$blog_real_visit_url."'";
            $application_total_data[$blog_real_visit_url][0]['idx'] = $real_idx;
            $real_idx++;
        }
    }
}
/* 실방문자수 정렬 적용 END */

/* 해당 프로모션 신청한 Blogger 정보 및 Report 정보 START */
$add_app_where   = $blog_urls ? "WHERE a.blog_url IN (" . implode(",", $blog_urls) . ")" : "WHERE a.blog_url = ''" ;
$application_count_sql = "
    SELECT 
        a.blog_url as blog_url,
        COUNT(*) as apply_count,
        COUNT(CASE WHEN a.a_state='1' OR a.a_state='3' THEN 1 END) as select_count,
        COUNT(CASE WHEN a.a_state='3' THEN 1 END) as cancel_count,
        COUNT(CASE WHEN p.p_state='4' AND p.p_state1='0' AND a.a_state='1' THEN 1 END) as proceed_count,
        IF(b.b_no, (SELECT COUNT(r.r_no) FROM report as r WHERE r.p_no ='{$idx}' AND r.b_no = b.b_no), 0) as r_count
    FROM application as a
    LEFT JOIN promotion as p ON p.p_no = a.p_no
    LEFT JOIN blog as b ON b.blog_url = a.blog_url
    {$add_app_where}
    GROUP BY a.blog_url
";
$application_count_query = mysqli_query($my_db,$application_count_sql);
while($application_info	 = mysqli_fetch_array($application_count_query))
{
    if($blog_url = strtolower(trim($application_info['blog_url']))) {
        $application_info_data = $application_total_data[$blog_url];
        $percent = round($application_info['apply_count'] > 0 ? ($application_info['select_count'] / $application_info['apply_count']) * 100 : '0', 0);

        if(!empty($application_info_data)) {
            foreach ($application_info_data as $application)
            {
                $application["select_count"]    = $application_info['select_count'];
                $application["apply_count"]     = $application_info['apply_count'];
                $application["percent"]         = $percent;
                $application["cancel_count"]    = $application_info['cancel_count'];
                $application["proceed_count"]   = $application_info['proceed_count'];
                $application["report_count"]    = $application_info['r_count'];

                $stmt[$application['idx']]      = $application;
            }
        }
    }
}

$add_report_where = $a_nos ? "WHERE r.p_no = '".$idx."' AND r.a_no IN(".implode(",", $a_nos).")" : "WHERE r.p_no = ''";
$report_sql       = "
    SELECT
        r.r_no,
        r.p_no,
        r.a_no,
        r.b_no,
        r.post_url,
        r.post_title,
        (SELECT COUNT(r_no) FROM report as sub_r WHERE sub_r.p_no = '".$idx."' AND sub_r.a_no = r.a_no) as report_cnt
    FROM report r
    $add_report_where
";

$report_query	= mysqli_query($my_db, $report_sql);
while($report_info	= mysqli_fetch_array($report_query))
{
	if($b_no = $report_info['b_no']){
		$report_info['post_title'] = htmlspecialchars($report_info['post_title']);

		$report_info_data[$b_no][] = $report_info;
	}
}
/* 해당 프로모션 신청한 Blogger 정보 및 Report 정보 END */

$smarty->assign("report", $report_info_data);

$smarty->assign("overlap_count", count($overlaps));
$smarty->assign("overlap", implode(',',$overlaps));
$smarty->assign("a_state_num",$a_state_num);
$smarty->assign("blogger", $stmt);

$smarty->display('popup/blogger_selection.html');
?>