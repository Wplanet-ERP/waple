<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/staff.php');
require('../inc/model/Team.php');

# Process 처리
$proc = (isset($_POST['process']))?$_POST['process']:"";

if($proc == "save_job_assignment")
{
	$j_a_id	= (isset($_POST['j_a_id'])) ? $_POST['j_a_id'] : "";
	$s_no	= (isset($_POST['s_no'])) ? $_POST['s_no'] : "";
	$type	= (isset($_POST['type'])) ? $_POST['type'] : "";

	if($type == 'MAIN'){
		$job_assignment_sql = "UPDATE job_assignment j_s SET j_s.main_s_no = IF(ISNULL(j_s.main_s_no), '#{$s_no}', CONCAT(j_s.main_s_no,'#{$s_no}')) WHERE j_s.id = '{$j_a_id}'";
	}elseif($type == 'SUB'){
		$job_assignment_sql = "UPDATE job_assignment j_s SET j_s.sub_s_no = IF(ISNULL(j_s.sub_s_no), '#{$s_no}', CONCAT(j_s.sub_s_no,'#{$s_no}')) WHERE j_s.id = '{$j_a_id}'";
	}else{
		$job_assignment_sql = "";
	}

	if(!mysqli_query($my_db, $job_assignment_sql)){
		exit("<script>alert('{$type} 담당자 설정이 실패 하였습니다');history.back();</script>");
	}else{
		exit("<script>opener.location.reload();history.back();</script>");
	}
}
elseif($proc == "remove_job_assignment")
{
	$j_a_id	= (isset($_POST['j_a_id'])) ? $_POST['j_a_id'] : "";
	$s_no	= (isset($_POST['s_no'])) ? $_POST['s_no'] : "";
	$type	= (isset($_POST['type'])) ? $_POST['type'] : "";

	if($type == 'MAIN'){
		$job_assignment_sql = "UPDATE job_assignment j_s SET j_s.main_s_no = REPLACE(j_s.main_s_no, '#{$s_no}', '') WHERE j_s.id = '{$j_a_id}'";
	}elseif($type == 'SUB'){
		$job_assignment_sql = "UPDATE job_assignment j_s SET j_s.sub_s_no = REPLACE(j_s.sub_s_no, '#{$s_no}', '') WHERE j_s.id = '{$j_a_id}'";
	}else{
		$job_assignment_sql = "";
	}

	if(!mysqli_query($my_db,$job_assignment_sql)){
		exit("<script>alert('{$type} 담당자 설정이 실패 하였습니다');history.back();</script>");
	}else{
		exit("<script>opener.location.reload();history.back();</script>");
	}
}
else
{
	$add_where			 = "1=1";
	$j_a_id_get			 = isset($_GET['j_a_id']) ? $_GET['j_a_id'] : "";
	$sch_staff_state = isset($_GET['sch_staff_state']) ? $_GET['sch_staff_state'] : "1";
	$sch_permission	 = isset($_GET['sch_permission']) ? $_GET['sch_permission'] : "";
	$sch_team		 = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
	$sch_s_name		 = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";

	if(!empty($j_a_id))
	{
		// 업무분장의 메인/서브 담당자 리스트
		$job_assignment_sql = "
				SELECT
					j_s.main_s_no,
					j_s.sub_s_no
				FROM job_assignment j_s
				WHERE j_s.id = '{$j_a_id}'
		";

		$result = mysqli_query($my_db, $job_assignment_sql);
		$job_assignment_array = mysqli_fetch_array($result);

		// main_s_no, sub_s_no를 #단위로 tokenized 하여 array로 가져옴
		$main_s_no_token = array();
		$sub_s_no_token  = array();
		$main_s_no_token = stringTokenized($job_assignment_array['main_s_no'],'#');
		$sub_s_no_token  = stringTokenized($job_assignment_array['sub_s_no'],'#');

		$smarty->assign("j_a_id", $j_a_id);
		$smarty->assign("main_s_no",$main_s_no_token);
		$smarty->assign("sub_s_no",$sub_s_no_token);
	}

	if(!empty($sch_staff_state)) {
		$add_where.=" AND staff_state='".$sch_staff_state."'";
		$smarty->assign("sch_staff_state",$sch_staff_state);
	}

	if(!empty($sch_permission)) {
		$add_where_permission = str_replace("0", "_", $sch_permission);
		$add_where.=" AND permission like '".$add_where_permission."'";
		$smarty->assign("sch_permission",$sch_permission);
	}

	if(!empty($sch_team)) {
		$add_where.=" AND team='".$sch_team."'";
		$smarty->assign("sch_team",$sch_team);
	}

	if(!empty($sch_s_name)) {
		$add_where.=" AND s_name LIKE '%".$sch_s_name."%'";
		$smarty->assign("sch_s_name",$sch_s_name);
	}

	// 정렬순서 토글 & 필드 지정
	$add_orderby =" s_no desc";

	// 리스트 쿼리
	$staff_sql = "
		SELECT
			s.s_no,
				s.staff_state,
				s.id,
				s.team,
				(SELECT t.team_name FROM team t WHERE t.team_code=s.team) AS team_name,
				(SELECT t2.team_name FROM team t2 WHERE t2.team_code=(SELECT t.team_code_parent FROM team t WHERE t.team_code=s.team)) AS team_parent_name,
				s.s_name,
				s.position,
				s.email
		FROM staff s
		WHERE {$add_where}
		ORDER BY {$add_orderby}
	";

	# 전체 게시물 수
	$cnt_sql 	= "SELECT count(*) FROM (".$staff_sql.") AS cnt";
	$result		= mysqli_query($my_db, $cnt_sql);
	$cnt_data	= mysqli_fetch_array($result);
	$total_num 	= $cnt_data[0];

	# 페이징처리
	$pages  	= isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num 		= 20;
	$offset 	= ($pages-1) * $num;
	$pagenum 	= ceil($total_num/$num);

	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	$search_url = getenv("QUERY_STRING");
	$pagelist	= pagelist($pages, "find_staff.php", $pagenum, $search_url);

	$smarty->assign("search_url", $search_url);
	$smarty->assign("total_num", $total_num);
	$smarty->assign("pagelist", $pagelist);


	# 리스트 쿼리 결과(데이터)
	$staff_sql .= "LIMIT $offset,$num";
	$result		= mysqli_query($my_db, $staff_sql);
	$staffs 	= [];
	$staff_state_option = getStaffStateOption();
	$team_model			= Team::Factory();
	$team_name_list		= $team_model->getTeamChkList();
	while($staff_array = mysqli_fetch_array($result))
	{
		$staffs[] = array(
			"s_no"				=> $staff_array['s_no'],
			"staff_state"		=> $staff_array['staff_state'],
			"state_name"		=> $staff_state_option[$staff_array['staff_state']],
			"id"				=> $staff_array['id'],
			"team"				=> $staff_array['team'],
			"team_name"			=> $staff_array['team_name'],
			"team_parent_name" 	=> $staff_array['team_parent_name'],
			"name"				=> $staff_array['s_name'],
			"date"				=> date("Y.m.d H:i",strtotime($staff_array['p_regdate'])),
			"position"			=> $staff_array['position'],
			"email"				=> $staff_array['email']
		);
	}

	$smarty->assign("staff_state_option", $staff_state_option);
	$smarty->assign("team_name_list", $team_name_list);
	$smarty->assign("staff", $staffs);

	$smarty->display('popup/find_staff.html');
}
?>
