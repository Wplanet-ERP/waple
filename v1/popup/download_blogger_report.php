<?php

require_once '../inc/common.php';


$p_no       = (isset($_GET['no']))?$_GET['no']:"";
$search_url = http_build_query($_GET);
$zip_file_list = [];

// 선정된 블로거만 골라보기 김윤영 Start
$add_where = "a.p_no='{$p_no}' AND (r.file_path IS NOT NULL AND r.file_path != '') AND ( a.a_state='1' OR a.a_state='3')";

$sql = "
		SELECT
			REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(a.blog_url,'http://',''),'https://',''),'www.',''),'instagram.com/',''),'blog.naver.com/',''),'.blog.me',''),'/','') as blog_id,
			r.file_path
		FROM
			application a
		LEFT JOIN report r ON a.a_no = r.a_no
		WHERE {$add_where}
		order by a.a_state ASC, a.visit_num DESC, blog_id ASC
";

$query = mysqli_query($my_db, $sql);
while($report = mysqli_fetch_array($query)) {

    $zip_file_path_list = explode(',', $report['file_path']);
    $idx = 1;
    foreach($zip_file_path_list as $zip_file_path)
    {
        $zip_file_ext = explode('.', $zip_file_path);
        $zip_file_name = "{$report['blog_id']}_{$idx}.{$zip_file_ext[1]}";
        $zip_file_list[] = array('filename'=>$zip_file_name, 'filepath' =>"../uploads/{$zip_file_path}");
        $idx++;
    }
}

if(!empty($zip_file_list))
{
    $file_name  = "promotion{$p_no}_report.zip";
    $file_path  = "../uploads/zip_tmp/{$file_name}";
    if(file_exists($file_path)){
        unlink($file_path);
    }

    $zip = new ZipArchive;
    $res = $zip->open($file_path, ZipArchive::CREATE);
    if ($res === TRUE) {
        foreach ($zip_file_list as $zip_file) {
            echo $zip_file['filename']."<br/>";

            if(file_exists($zip_file['filepath'])){
                $zip->addFile($zip_file['filepath'], $zip_file['filename']) or die ("ERROR: Could not add file: {$zip_file['filepath']}");
            }
        }
        $zip->close();

        echo "<script>location.href='/v1/uploads/zip_tmp/{$file_name}'</script>";
    } else {
        echo "에러 코드: ".$res;
    }
    exit;
}else{
    echo "첨부파일이 없습니다";
    exit;
}

