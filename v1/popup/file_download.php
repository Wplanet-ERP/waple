<?php

function utf2euc($str) {
    return iconv("UTF-8","cp949//IGNORE", $str);
}

$file_dn_name_get = $_GET['file_dn_name'];
$file_up_name_get = $_GET['file_up_name'];


$file_dn_name = utf2euc($file_dn_name_get);
//echo $file_dn_name_get; exit;

$filepath = "../uploads/".$file_up_name_get;

$fileinfo = pathinfo($filepath);
$ext = strtolower($fileinfo['extension']);


// Determine Content Type 
switch ($ext) 
{ 
    case "pdf":
        $ctype = "application/pdf";
        break;
    case "exe":
        $ctype = "application/octet-stream";
        break;
    case "zip":
        $ctype = "application/zip";
        break;
    case "doc":
        $ctype = "application/msword";
        break;
    case "xls":
        $ctype = "application/vnd.ms-excel";
        break;
    case "ppt":
        $ctype = "application/vnd.ms-powerpoint";
        break;
    case "gif":
        $ctype = "image/gif";
        break;
    case "png":
        $ctype = "image/png";
        break;
    case "jpeg":
        $ctype = "image/jpg";
        break;
    case "jpg":
        $ctype = "image/jpg";
        break;
    case "mp3":
        $ctype = "audio/mp3";
        break;
    case "wav":
        $ctype = "audio/x-wav";
        break;
    case "wma":
        $ctype = "audio/x-wav";
        break;
    case "mpeg":
        $ctype = "video/mpeg";
        break;
    case "mpg":
        $ctype = "video/mpeg";
        break;
    case "mpe":
        $ctype = "video/mpeg";
        break;
    case "mov":
        $ctype = "video/quicktime";
        break;
    case "avi":
        $ctype = "video/x-msvideo";
        break;
    case "src":
        $ctype = "plain/text";
        break;
    default:
        $ctype = "application/force-download";
} 
        
if ( file_exists($filepath) )	{

    if($fd=fopen($filepath, "rb")){
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-type: ".$ctype);
        header("Content-Disposition: attachment; filename=\"".$file_dn_name."\";");
        header("Content-length: ".@filesize($filepath));

        while(!feof($fd)) {
            print(fread($fd, 4096));
            flush();
        }
    }
    fclose($fd);
} else {
	echo "<script>alert('존재하지 않는 파일입니다.');history.back();</script>";
}
?>