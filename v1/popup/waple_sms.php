<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/agency.php');
require('../inc/model/Staff.php');
require('../inc/model/Agency.php');
require('../inc/model/Message.php');

if ($_POST)
{
	$message_model 	= Message::Factory();
	$sms_title		= isset($_POST['title']) ? $_POST['title'] : "";
	$receiver_list	= isset($_POST['receiver_list']) ? $_POST['receiver_list'] : "";
	$sms_msg		= isset($_POST['sms_msg']) ? $_POST['sms_msg'] : "";
	$send_phone		= isset($_POST['send_phone']) ? $_POST['send_phone'] : "";
	$send_data_list = [];

	$receiver_list  	= str_replace(" ", "", $receiver_list);
	$receiver_list  	= preg_replace("/\r\n|\r|\n/", "||", $receiver_list);
	$receiver_hp_list 	= explode("||", $receiver_list);

	if (!$send_phone) {
		$send_hp = "02-830-1912";
	}

	$sms_msg  		= addslashes($sms_msg);
	$sms_msg_len 	= mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
	$msg_type 		= ($sms_msg_len > 90) ? "L" : "S";
	$send_name 		= $session_name;

	$cur_datetime	= date("YmdHis");
	$cm_id 			= 1;
	$c_info 		= "3"; # 직원간 SMS

	foreach ($receiver_hp_list as $receiver_hp)
	{
		if(empty($receiver_hp)){
			continue;
		}

		$chk_hp 	 = str_replace("-","", $receiver_hp);
		$msg_id 	 = "W{$cur_datetime}".sprintf('%04d', $cm_id++);

		$chk_sql	 = "SELECT s_name FROM staff WHERE hp IS NOT NULL AND staff_state='1' AND REPLACE(hp,'-','')='{$chk_hp}' LIMIT 1";
		$chk_query	 = mysqli_query($my_db, $chk_sql);
		$chk_result  = mysqli_fetch_assoc($chk_query);
		$receiver  	 = isset($chk_result['s_name']) ? $chk_result['s_name'] : "";

		$send_data_list[$msg_id] = array(
			"msg_id"        => $msg_id,
			"msg_type"      => $msg_type,
			"sender"        => $send_name,
			"sender_hp"     => $send_phone,
			"receiver"      => $receiver,
			"receiver_hp"   => $receiver_hp,
			"title"         => $sms_title,
			"content"       => $sms_msg,
			"cinfo"         => $c_info,
		);
	}

	if(!empty($send_data_list))
	{
		$result_data = $message_model->sendMessage($send_data_list);

		if ($result_data['result']) {
			exit("<script>alert('문자 메시지를 발송하였습니다.'); if (self) self.close();</script>");
		}
	}

	exit("<script>alert('문자 메세지 발송에 실패했습니다.');history.back();</script>");
}

$title 				= "";
$sms_text 			= "";
$init_caller 		= "";
$init_receiver_list = [];

$chk_type  = isset($_GET['type']) ? $_GET['type'] : "";
if($chk_type == "agency")
{
    $chk_as_no      	= isset($_GET['as_no']) ? $_GET['as_no'] : "";

    $chk_agency_sql     = "SELECT *, (SELECT s.hp FROM staff s WHERE s.s_no=`as`.manager) as hp FROM agency_settlement `as` WHERE as_no='{$chk_as_no}'";
    $chk_agency_query   = mysqli_query($my_db, $chk_agency_sql);
    $chk_agency_result  = mysqli_fetch_assoc($chk_agency_query);

    if(!$chk_agency_result['partner']){
    	echo "<script>alert('등록된 업체가 없습니다.');self.close()</script>";
    	exit;
	}

    $title 				= "CPC 계정등록 SMS안내";
    $sms_text 			= "[경영지원팀]\r\n원활한 수수료정산을 위해\r\n‘{$chk_agency_result['partner_name']}’ 파트너정보의 수수료 계정 업데이트 부탁드립니다.";
    $init_caller   		= "02-830-1912";
    $init_receiver_list = array($chk_agency_result['hp']);
}
elseif($chk_type == "asset")
{
	$chk_as_no      	= isset($_GET['as_no']) ? $_GET['as_no'] : "";
	$chk_asset_sql     	= "SELECT * FROM asset `as` WHERE as_no='{$chk_as_no}'";
	$chk_asset_query   	= mysqli_query($my_db, $chk_asset_sql);
	$chk_asset_result  	= mysqli_fetch_assoc($chk_asset_query);

	if(!$chk_asset_result['as_no']){
		echo "<script>alert('등록된 자산이 없습니다.');self.close()</script>";
		exit;
	}

	$asset_use_person_sql = "SELECT (SELECT s.hp FROm staff s WHERE s.s_no=ar.req_no) as hp FROM asset_reservation ar WHERE as_no='{$chk_asset_result['as_no']}' AND work_state='2' GROUP BY req_no";
	$asset_use_person_query = mysqli_query($my_db, $asset_use_person_sql);
	while($asset_use = mysqli_fetch_assoc($asset_use_person_query)){
		if(!empty($asset_use['hp'])){
			$init_receiver_list[] = $asset_use['hp'];
		}
	}

	if(empty($init_receiver_list)){
		echo "<script>alert('자산을 사용중인 사람이 없습니다.');self.close()</script>";
		exit;
	}

	$title 				= "자산 비밀번호 변경 알림";
	$sms_text 			= "[와플 자산 변경 알림]\r\n자산명 : {$chk_asset_result['name']}\r\n비밀번호가 변경되었습니다. 와플 자산정보에서 확인 바랍니다.";
	$init_caller   		= "02-830-1912";
}
elseif($chk_type == "reservation")
{
	$chk_as_r_no      	= isset($_GET['as_r_no']) ? $_GET['as_r_no'] : "";
	$chk_state          = isset($_GET['state']) ? $_GET['state'] : "";
	$chk_asset_sql     	= "SELECT as_r_no, (SELECT a.name FROM asset a WHERE a.as_no=asr.as_no) as as_name, (SELECT s.hp FROM staff s WHERE s.s_no=asr.req_no) AS req_hp, (SELECT s.hp FROM staff s WHERE s.s_no=asr.manager) AS manager_hp FROM asset_reservation `asr` WHERE as_r_no='{$chk_as_r_no}'";
	$chk_asset_query   	= mysqli_query($my_db, $chk_asset_sql);
	$chk_asset_result  	= mysqli_fetch_assoc($chk_asset_query);

	if(!$chk_asset_result['as_r_no']){
		echo "<script>alert('자산내역이 없습니다.');self.close()</script>";
		exit;
	}

	$init_caller 		= "02-830-1912";
	switch($chk_state){
		case 'approve':
			$title 	     = "자산 사용요청 결과 알림";
			$sms_text 	 = "[와플 자산 사용요청 결과 알림]\r\n자산명 : {$chk_asset_result['as_name']}\r\n사용승인이 완료되었습니다.";
			$init_receiver_list[] = $chk_asset_result['req_hp'];
			break;
		case 'refuse':
			$title 	     = "자산 사용요청 결과 알림";
			$sms_text 	 = "[와플 자산 사용요청 결과 알림]\r\n자산명 : {$chk_asset_result['as_name']}\r\n사용불가 처리되었습니다.";
			$init_receiver_list[] = $chk_asset_result['req_hp'];
			break;
		case 'return':
			$title 	     = "자산 반납요청 알림";
			$sms_text 	 = "[와플 자산 반납요청 알림]\r\n자산명 : {$chk_asset_result['as_name']}\r\n반납요청 되었습니다.";
			$init_receiver_list[] = $chk_asset_result['manager_hp'];
			break;
	}
}
elseif($chk_type == "charge")
{
	$chk_ac_no      	= isset($_GET['ac_no']) ? $_GET['ac_no'] : "";
	$chk_charge_sql    	= "SELECT * FROM agency_charge WHERE ac_no='{$chk_ac_no}'";
	$chk_charge_query  	= mysqli_query($my_db, $chk_charge_sql);
	$chk_charge_result 	= mysqli_fetch_assoc($chk_charge_query);

	if(!$chk_charge_result['ac_no']){
		echo "<script>alert('충전금 내역이 없습니다.');self.close()</script>";
		exit;
	}

	$media_option   	= getAdvertiseMediaOption();
	$media_name     	= $media_option[$chk_charge_result['media']];
	$staff_model    	= Staff::Factory();
	$staff_item     	= $staff_model->getItem($chk_charge_result["manager"]);

	$title				= "{$media_name} 충전금 확인요청";
	$chat_content_url   = "https://work.wplanet.co.kr/v1/work_list.php?sch_prd_g1=01014&sch_prd_g2=01053&sch_prd=279&sch_w_no={$chk_charge_result['w_no']}";
	$chat_content       = "[{$title} 알림]\r\n{$staff_item['s_name']}님,\r\n{$chk_charge_result['base_mon']} `{$chk_charge_result["partner_name"]}`의\r\n{$media_name} 잔액 이상이 확인되었습니다.\r\n아래 URL(업무요청) 내용 보신 후 담당자에게 연락 바랍니다.\r\n{$chat_content_url}";
	$init_caller  		= "02-830-1912";
	$sms_text 	 		= $chat_content;


	$init_receiver_list[] 	= $staff_item['hp'];
}

$smarty->assign("title", $title);
$smarty->assign("sms_text", $sms_text);
$smarty->assign("init_caller", $init_caller);
$smarty->assign("init_receiver_list", $init_receiver_list);

$smarty->display('popup/waple_sms.html');
?>
