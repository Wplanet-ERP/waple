<?php
ini_set("display_errors", -1);
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

require('../inc/common.php');
require('../ckadmin.php');
include_once('../Classes/PHPExcel.php');

$file_name      = $_FILES["application_file"]["tmp_name"];
$promotion_no   = (isset($_POST['promotion_no'])) ? $_POST['promotion_no'] : "";
$a_state        = (isset($_POST['promotion_state'])) ? $_POST['promotion_state'] : 0;

$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();


$promotion_sql      = "SELECT p_no, kind, c_no FROM promotion WHERE p_no='{$promotion_no}'";
$promotion_query    = mysqli_query($my_db, $promotion_sql);
$promotion_result   = mysqli_fetch_array($promotion_query);

if(!isset($promotion_result['p_no']) && empty($promotion_result['p_no'])){
    exit("<script>alert('해당 프로모션이 존재하지 않습니다.');location.href='blogger_selection.php?no={$promotion_no}';</script>");
}

$p_no = $promotion_result['p_no'];
$kind = $promotion_result['kind'];
$c_no = $promotion_result['c_no'];

$ins_sql = "INSERT INTO application(p_no, c_no, kind, blog_url, nick, username, hp, email, cafe_id, zipcode, address1, address2, memo, a_state, ip, regdate) VALUES";
$comma = "";

for ($i = 2; $i <= $totalRow; $i++)
{
    $id	        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));
    $nick 		= (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));
    $username 	= (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));
    $hp 		= (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));
    $email 		= (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));
    $zipcode    = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));
    $addr1      = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));
    $addr2      = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));
    $memo       = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));
    $regdate	= date('Y-m-d H:i:s');
    $ip         = ip2long($_SERVER['REMOTE_ADDR']);
    $cafe_id    = "";


    if(!empty($id))
    {
        $id         = str_replace("https:", "http:", $id);
        $blog_sql   = "SELECT cafe_id FROM application WHERE blog_url='{$id}' AND cafe_id is not null LIMIT 1";
        $blog_query = mysqli_query($my_db, $blog_sql);
        $blog_data  = mysqli_fetch_array($blog_query);
        $cafe_id    = isset($blog_data['cafe_id']) ? $blog_data['cafe_id'] : "";

        $ins_sql    .= $comma."('{$p_no}', '{$c_no}', '{$kind}', '{$id}', '{$nick}', '{$username}', '{$hp}', '{$email}', '{$cafe_id}','{$zipcode}','{$addr1}', '{$addr2}','{$memo}', '{$a_state}', '{$ip}', '{$regdate}')";
        $comma      = ' , ';
    }
}

if (mysqli_query($my_db, $ins_sql)){
    exit("<script>alert('신청서 데이터 적용시켰습니다');location.href='blogger_selection.php?no={$promotion_no}';</script>");
}else{
    exit("<script>alert('신청서 데이터 적용에 실패했습니다');location.href='blogger_selection.php?no={$promotion_no}';</script>");
}

?>
