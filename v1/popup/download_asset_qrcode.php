<?php

require_once '../inc/common.php';


$as_s_no       = (isset($_GET['as_s_no']))?$_GET['as_s_no']:"";
$as_e_no       = (isset($_GET['as_e_no']))?$_GET['as_e_no']:"";

$zip_file_list = [];


$sql = "
		SELECT
			qr_code
		FROM asset
		WHERE (as_no >='{$as_s_no}' AND as_no <= '{$as_e_no}') AND display='1' AND qr_code is not null AND qr_code != ''
		ORDER BY as_no ASC
";

$query = mysqli_query($my_db, $sql);
while($asset = mysqli_fetch_array($query)) {
    $file_name    = str_replace('uploads/qr_code/','',$asset['qr_code']);
    $zip_file_list[] = array('filename'=>$file_name, 'filepath' =>"../{$asset['qr_code']}");
}

if(!empty($zip_file_list))
{
    $file_name  = "asset_qrcode.zip";
    $file_path  = "../uploads/qr_code/{$file_name}";
    if(file_exists($file_path)){
        unlink($file_path);
    }

    $zip = new ZipArchive;
    $res = $zip->open($file_path, ZipArchive::CREATE);
    if ($res === TRUE) {
        foreach ($zip_file_list as $zip_file) {
            echo $zip_file['filename']."<br/>";
            $zip->addFile($zip_file['filepath'], $zip_file['filename']) or die ("ERROR: Could not add file: {$zip_file['filepath']}");
        }
        $zip->close();

        echo "<script>location.href='/v1/uploads/qr_code/{$file_name}'</script>";
    } else {
        echo "에러 코드: ".$res;
    }
    exit;
}else{
    echo "첨부파일이 없습니다";
    exit;
}

