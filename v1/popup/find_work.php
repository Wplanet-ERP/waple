<?php
require('../inc/common.php');
require('../inc/data_state.php');
require('../ckadmin.php');
require('../inc/helper/work.php');
require('../inc/model/Kind.php');
require('../inc/model/Team.php');


// 접근 권한
if (!$session_s_no){
	$smarty->display('../access_company_error.html');
	exit;
}

$reject = false;
if($session_staff_state == '2'){ // 프리랜서의 경우 기본 접근제한으로 설정
	$reject = true;
}

for($i=0 ; $i < sizeof($access_accept_list) ; $i++){ // 접근허용 리스트
	if($session_s_no == $access_accept_list[$i][1]){
		if($_GET['sch_prd'] == $access_accept_list[$i][2] || $access_accept_list[$i][2] == 'all'){
			$reject = false;
		}
	}
}

if($reject){
	for($i=0 ; $i < sizeof($access_reject_list) ; $i++){ // 접근제한 리스트
		if($session_s_no == $access_reject_list[$i][1]){
			if($_GET['sch_prd'] == $access_reject_list[$i][2] || $access_reject_list[$i][2] == 'all'){
				$reject = true;
			}
		}
	}
}
if($reject && !!$_GET['sch_prd']){
	$smarty->display('../access_error.html');
	exit;
}


$proc=(isset($_POST['process']))?$_POST['process']:"";
//$sch_get=(isset($_GET['sch']))?$_GET['sch']:"";

// 세트에 업무 추가하기
if($proc=="set_add_work") {
	$set_w_no=(isset($_POST['set_w_no']))?$_POST['set_w_no']:"";
	$w_no=(isset($_POST['add_w_no']))?$_POST['add_w_no']:"";
	$search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

    $chk_work_set_sql    = "SELECT MAX(priority) as max_priority FROM `work` as w WHERE w.set_tag like '%{$set_w_no}%'";
    $chk_work_set_query  = mysqli_query($my_db, $chk_work_set_sql);
    $chk_work_set_result = mysqli_fetch_assoc($chk_work_set_query);
    $priority			 = isset($chk_work_set_result['max_priority']) ? $chk_work_set_result['max_priority']+1 : 1;

    $sql = "UPDATE `work` w SET w.set_tag = IF(ISNULL(w.set_tag), '#{$set_w_no}', CONCAT(w.set_tag,'#{$set_w_no}')), w.priority='{$priority}'  WHERE w.w_no = '{$w_no}'";

	if(!mysqli_query($my_db,$sql)){
		exit("<script>alert('세트구성에 업무를 추가 하는데 실패 하였습니다');location.href='find_work.php?{$search_url_get}';</script>");
	}
	exit("<script>opener.location.reload(); history.back();</script>");

}elseif ($proc == "proc_quick_prd") {

	$mode = $_POST['mode'];
	$quick_prd_req = $_POST['quick_prd'];

	if ($session_id) {
		$sql = "
			SELECT prd_no, title
			  FROM product
		";

		$result = mysqli_query($my_db, $sql);

		while ($row = mysqli_fetch_assoc($result)) {
			$arr_prd[$row['prd_no']] = $row['title'];
		}

		$sql = "
			SELECT quick_prd
			  FROM staff
			 where id = '{$session_id}'
		";
		$result = mysqli_query($my_db, $sql);
		$row = mysqli_fetch_array($result);
		$staff_quick_prd = $row['quick_prd'];
		$arr_q_prd = explode(",", $staff_quick_prd);

		if ($mode == "add" && in_array($quick_prd_req, $arr_q_prd)) {
			$r_msg = "err_exist_prd";
			echo $r_msg;
			exit;
		}

		if ($mode == "del") {
			$key = array_search($quick_prd_req, $arr_q_prd);
			unset($arr_q_prd[$key]);
		} else {
			array_push($arr_q_prd, $quick_prd_req);
		}

		$quick_prd = implode(",", $arr_q_prd);

		if (substr($quick_prd, 0, 1) == ",")
			$quick_prd = substr($quick_prd, 1);

		if ($mode != "display") {
			$sql = "
				UPDATE staff
				   set quick_prd = '{$quick_prd}'
				 where id = '{$session_id}'
			";
			mysqli_query($my_db, $sql);
		}

		$r_msg = "ok";

		mysqli_free_result($result);
		mysqli_close($my_db);
	}

	echo $r_msg;

	$arr_q_prd = explode(",", $quick_prd);
	echo "|";
	echo "<table cellpadding=\"0\" cellspacing=\"0\" style=\"min-width: 440px; max-width:1800px;\"><tr><td style=\"width: 115px;text-align:center;\">";
	echo "<sapn style=\"margin-top:10px;font-size:14px;\">[자주하는 업무]</span>";
	echo "<br><button type=\"button\" onclick=\"window.open('quick_prd_set.php','자주하는 업무설정하기','width=644, height=500, toolbar=no, menubar=no, scrollbars=yes, resizable=yes');return false;\" class=\"btn_small\" style=\"padding:1px 8px;font-weight:bold;\">*설정하기*</button>";
	echo "</td><td>";
	$cnt = 0;
	$sch_prd_get = isset($_POST['sch_prd'])?$_POST['sch_prd']:""; // sch_prd 값 가져오기
	$sch_prd_g1_get = isset($_POST['sch_prd_g1'])?$_POST['sch_prd_g1']:"";
	$sch_prd_g2_get = isset($_POST['sch_prd_g2'])?$_POST['sch_prd_g2']:"";

	$set_w_no_get=isset($_POST['set_w_no'])?$_POST['set_w_no']:""; // set_w_no 값 가져오기

	foreach  ($arr_q_prd as $key => $value) {
		if( $cnt == '0' ){ // 자주하는 업무에 첫번째에 전체 표기
			if ($sch_prd_get) {
				echo "<span>";
				if(!!$set_w_no){
					echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./find_work.php?set_w_no={$set_w_no}';\" style=\"padding: 4px 6px;margin-left: 0px;\">::전체::</button>";
				}else{
					echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./find_work.php';\" style=\"padding: 4px 6px;margin-left: 0px;\">::전체::</button>";
				}
				echo "</span> ";
			}elseif(empty($sch_prd_g1_get) && empty($sch_prd_g1_get)){
				echo "<span>";
				if(!!$set_w_no){
					echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./find_work.php?set_w_no={$set_w_no}';\" style=\"padding: 4px 6px;margin-left: 0px; background-color: #c73333;\">::전체::</button>";
				}else{
					echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./find_work.php';\" style=\"padding: 4px 6px;margin-left: 0px; background-color: #c73333;\">::전체::</button>";
				}
				echo "</span> ";
			}else{
				echo "<span>";
				if(!!$set_w_no){
					echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./find_work.php?set_w_no={$set_w_no}';\" style=\"padding: 4px 6px;margin-left: 0px;\">::전체::</button>";
				}else{
					echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./find_work.php<{/if}>';\" style=\"padding: 4px 6px;margin-left: 0px;\">::전체::</button>";
				}
				echo "</span> ";
			}
		}

		if ($value) {
			echo "<span>";
			if($sch_prd_get != $value)
				echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./find_work.php?sch_prd_g1=".$sch_prd_g1_get."&sch_prd_g2=".$sch_prd_g2_get."&sch_prd=".$value."&set_w_no=".$set_w_no_get."';\" style=\"padding: 4px 6px;margin-left: 0px;\">" . $arr_prd[$value] . "</button>";
			else
				echo "<button type=\"button\" class=\"btn_small btn_quick_add\" onclick=\"location.href='./find_work.php?sch_prd_g1=".$sch_prd_g1_get."&sch_prd_g2=".$sch_prd_g2_get."&sch_prd=".$value."&set_w_no=".$set_w_no_get."';\" style=\"padding: 4px 6px;margin-left: 0px; background-color: #c73333;\">" . $arr_prd[$value] . "</button>";
			//echo "<button type=\"button\" class=\"btn_small\" onclick=\"proc_quick_prd('del', '". $value . "');\">X</button>";
			echo "</span> ";
			//if ($cnt % 4 == 3) echo "<br>";
		}
		$cnt++;
	}
	echo "</td></tr></table>";
	exit;

}
else
{
	# 검색 처리
	$add_where          = "1=1";
	$set_w_no			= isset($_GET['set_w_no']) ? $_GET['set_w_no']:"";
	$sch_set            = isset($_GET['sch_set']) ? $_GET['sch_set'] : "";
	$sch_set_w_no	   	= isset($_GET['sch_set_w_no']) ? $_GET['sch_set_w_no'] : "";
	$q_sch          	= isset($_GET['q_sch']) ? $_GET['q_sch'] : "";
	$sch            	= isset($_GET['sch']) ? $_GET['sch'] : "";

	# 브랜드 검색
	$kind_model                 = Kind::Factory();
	$team_model                 = Team::Factory();
	$brand_company_total_list   = $kind_model->getBrandCompanyList();
	$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
	$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
	$brand_list                 = $brand_company_total_list['brand_dom_list'];
	$brand_total_list           = $brand_company_total_list['brand_total_list'];
	$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
	$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
	$sch_team_list              = $team_model->getActiveTeamList();
	$sch_brand_g2_list          = [];
	$sch_brand_list             = [];

	# 브랜드 검색
	$sch_brand_g1               = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
	$sch_brand_g2               = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
	$sch_brand                  = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

	# 브랜드 parent 매칭
	if(!empty($sch_brand)) {
		$sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
		$sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
	}
	elseif(!empty($sch_brand_g2)) {
		$sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
	}

	if(!empty($sch_brand)) {
		$sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
		$sch_brand_list     = $brand_list[$sch_brand_g2];

		$add_where      .= " AND `w`.c_no = '{$sch_brand}'";
	}
	elseif(!empty($sch_brand_g2)) {
		$sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
		$sch_brand_list     = $brand_list[$sch_brand_g2];

		$add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
	}
	elseif(!empty($sch_brand_g1)) {
		$sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

		$add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
	}

	$smarty->assign("sch_brand_g1", $sch_brand_g1);
	$smarty->assign("sch_brand_g2", $sch_brand_g2);
	$smarty->assign("sch_brand", $sch_brand);
	$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
	$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
	$smarty->assign("sch_brand_list", $sch_brand_list);
	$smarty->assign("sch_team_list", $sch_team_list);

	$sch_prd_g1 			= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
	$sch_prd_g2 			= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
	$sch_prd    			= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
	$sch_reg_s_date      	= isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : "";
	$sch_reg_e_date       	= isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : "";
	$sch_task_run_s_date  	= isset($_GET['sch_task_run_s_date']) ? $_GET['sch_task_run_s_date'] : "";
	$sch_task_run_e_date  	= isset($_GET['sch_task_run_e_date']) ? $_GET['sch_task_run_e_date'] : "";
	$sch_work_state      	= isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
	$sch_wd_dp_no_state 	= isset($_GET['sch_wd_dp_no_state']) ? $_GET['sch_wd_dp_no_state'] : "";
	$sch_s_name       		= isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
	$sch_req_team        	= isset($_GET['sch_req_team']) ? $_GET['sch_req_team'] : "";
	$sch_req_s_name       	= isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : "";
	$sch_run_team        	= isset($_GET['sch_run_team']) ? $_GET['sch_run_team'] : "";
	$sch_run_s_name     	= isset($_GET['sch_run_s_name']) ? $_GET['sch_run_s_name'] : "";
	$sch_run_direct    		= isset($_GET['sch_run_direct']) ? $_GET['sch_run_direct'] : "";
	$sch_c_name    			= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
	$sch_task_req 			= isset($_GET['sch_task_req']) ? $_GET['sch_task_req'] : "";
	$sch_task_run      		= isset($_GET['sch_task_run']) ? $_GET['sch_task_run'] : "";
	$sch_file_no            = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";
	$sch_run_is_null        = isset($_GET['sch_run_is_null']) ? $_GET['sch_run_is_null'] : "";
	$sch_category           = isset($_GET['sch_category']) ? $_GET['sch_category'] : "";

	# QUICK SEARCH
	if (!empty($q_sch))
	{
		switch($q_sch){
			case "1":
				$add_where .= " AND (w.work_state='1' OR w.work_state='2')";
				break;
			case "2":
				$add_where .= " AND w.work_state='3'";
				break;
			case "3":
				$add_where .= " AND w.work_state IN(4,5)";
				break;
			case "4":
				$add_where .= " AND w.work_state='6'";
				break;
			case "5":
				$add_where .= " AND w.work_state='6' AND w.extension_date <= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL 10 DAY), '%Y-%m-%d') AND w.extension_date >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -5 DAY), '%Y-%m-%d') AND (w.extension='2' OR w.extension='0' OR w.extension IS NULL)";
				break;
			case "6":
				$add_where .= " AND w.work_state IN(3,4,5)";
				break;
		}
		$smarty->assign("q_sch", $q_sch);
	}

	if (!empty($sch)) { // 상세검색 펼치기
		$smarty->assign("sch", $sch);
	}

	for ($arr_i = 1; $arr_i < count($product_list); $arr_i++) {
		if ($product_list[$arr_i]['prd_no'] == $sch_prd) {
			$sch_prd_g1 = $product_list[$arr_i]['g1_code'];
			$sch_prd_g2 = $product_list[$arr_i]['g2_code'];
			break;
		}
	}
	$smarty->assign("sch_prd_g1", $sch_prd_g1);
	$smarty->assign("sch_prd_g2", $sch_prd_g2);

	if(!empty($set_w_no)) { // 업무세트
		$smarty->assign("set_w_no", $set_w_no);
	}

	if (!empty($sch_s_name)) { // 업체 담당자
		$add_where .= " AND w.s_no IN (SELECT s_no FROM staff WHERE s_name LIKE '%{$sch_s_name}%')";
		$smarty->assign("sch_s_name", $sch_s_name);
	}

	if (!empty($sch_req_team))
	{
		$sch_req_team_where = getTeamWhere($my_db, $sch_req_team);
		if($sch_req_team == "00221"){
			$sch_req_team_where .= ",00236";
		}
		$add_where .= " AND w.task_req_team IN({$sch_req_team_where})";

		$smarty->assign("sch_req_team", $sch_req_team);
	}

	if (!empty($sch_req_s_name)) { // 업무 요청자
		$add_where .= " AND w.task_req_s_no IN (SELECT s_no FROM staff WHERE s_name LIKE '%{$sch_req_s_name}%')";
		$smarty->assign("sch_req_s_name", $sch_req_s_name);
	}

	if (!empty($sch_prd)) { // 상품
		$add_where .= " AND w.prd_no='{$sch_prd}'";
	} else {
		if ($sch_prd_g2) { // 상품 선택 없이 두번째 그룹까지 설정한 경우
			$add_where .= " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.display='1' AND prd.k_name_code='{$sch_prd_g2}')";
		} elseif ($sch_prd_g1) { // 상품 선택 없이 찻번째 그룹까지 설정한 경우
			$add_where .= " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.display='1' AND prd.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
		}
	}
	$smarty->assign("sch_prd", $sch_prd);

	if (!empty($sch_reg_s_date)) { // 작성일
		$sch_reg_s_datetime = "{$sch_reg_s_date} 00:00:00";
		$add_where .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
		$smarty->assign("sch_reg_s_date", $sch_reg_s_date);
	}

	if (!empty($sch_reg_e_date)) { // 작성일
		$sch_reg_e_datetime = "{$sch_reg_e_date} 23:59:59";
		$add_where .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
		$smarty->assign("sch_reg_e_date", $sch_reg_e_date);
	}

	if (!empty($sch_task_run_s_date)) { // 업무완료일
		$sch_task_run_s_datetime = "{$sch_task_run_s_date} 00:00:00";
		$add_where .= " AND w.task_run_regdate >= '{$sch_task_run_s_date}'";
		$smarty->assign("sch_task_run_s_date", $sch_task_run_s_date);
	}

	if (!empty($sch_task_run_e_date)) { // 업무완료일
		$sch_task_run_e_datetime = "{$sch_task_run_e_date} 23:59:59";
		$add_where .= " AND w.task_run_regdate <= '{$sch_task_run_e_datetime}'";
		$smarty->assign("sch_task_run_e_date", $sch_task_run_e_date);
	}

	if (!empty($sch_work_state) && empty($q_sch)) { // 진행상태 [퀵서치를 안한경우]
		$add_where .= " AND w.work_state='{$sch_work_state}'";
		$smarty->assign("sch_work_state", $sch_work_state);
	}

	if($sch_run_is_null == "1"){
		$add_where .= " AND (w.task_run_s_no = 0 OR w.task_run_s_no IS NULL)";
	}
	else
	{
		if (!empty($sch_run_team))
		{
			$sch_run_team_where = getTeamWhere($my_db, $sch_run_team);
			if($sch_run_team == "00221"){
				$sch_run_team_where .= ",00236";
			}
			$add_where .= " AND w.task_run_team IN({$sch_run_team_where})";

			$smarty->assign("sch_run_team", $sch_run_team);
		}

		if (!empty($sch_run_s_name)) { // 업무 처리자
			$add_where .= " AND w.task_run_s_no IN(SELECT s_no FROM staff WHERE s_name like '%{$sch_run_s_name}%')";
			$smarty->assign("sch_run_s_name", $sch_run_s_name);
		}
	}
	$smarty->assign("sch_run_is_null", $sch_run_is_null);

	if (!empty($sch_c_name)) { // 업체명
		$add_where .= " AND w.c_name like '%{$sch_c_name}%'";
		$smarty->assign("sch_c_name", $sch_c_name);
	}

	if (!empty($sch_w_no)) { // 업무번호
		$add_where .= " AND w.w_no ='{$sch_w_no}'";
		$smarty->assign("sch_w_no", $sch_w_no);
	}
	if (!empty($sch_task_req)) { // 업무요청
		$add_where .= " AND w.task_req like '%{$sch_task_req}%'";
		$smarty->assign("sch_task_req", $sch_task_req);
	}
	if (!empty($sch_task_run)) { // 업무진행
		$add_where .= " AND w.task_run like '%{$sch_task_run}%'";
		$smarty->assign("sch_task_run", $sch_task_run);
	}

	if (!empty($sch_file_no)) { // 파일번호
		$add_where .= " AND w.file_no = '{$sch_file_no}'";
		$smarty->assign("sch_file_no", $sch_file_no);
	}

	if (!empty($sch_category)) { // 세부카테고리
		$add_where .= " AND w.k_name_code IN(SELECT sub.k_name_code FROM kind AS sub WHERE sub.k_code='work_task_run' AND sub.k_name LIKE '%{$sch_category}%' AND sub.display='1')";
		$smarty->assign("sch_category", $sch_category);
	}

	if(!!$sch_set_w_no){ // 업무 세트의 태그 조건
		$add_where.=" AND w.set_tag IS NOT NULL AND w.set_tag LIKE '%{$sch_set_w_no}%'";
		$smarty->assign("sch_set_w_no", $sch_set_w_no);
	}

	// 상품 구분 가져오기(1차)
	$prd_g1_sql = " 
		SELECT
	        k_name,
		    k_name_code
	    FROM kind
	    WHERE k_code='product' AND (k_parent='0' OR k_parent is null) AND display='1'
	    ORDER BY priority ASC
	";
	$prd_g1_query 	= mysqli_query($my_db, $prd_g1_sql);
	$prd_g1_list	= [];
	while($prd_g1 = mysqli_fetch_array($prd_g1_query)) {
		$prd_g1_list[] = array(
			"k_name"		=> trim($prd_g1['k_name']),
			"k_name_code"	=> trim($prd_g1['k_name_code'])
		);
	}
	$smarty->assign("prd_g1_list", $prd_g1_list);


	// 상품 구분 가져오기(2차)
	if(empty($sch_prd) && $sch_prd_g2){ // 상품 선택 없이 상품 그룹2로 검색시 그룹2 가져오기
		$prd_g2_sql = "
		    SELECT
		        k_name,k_name_code,k_parent
		    FROM kind
		    WHERE k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code='{$sch_prd_g2}') AND display='1'
		    ORDER BY priority ASC
		";
	}
	elseif(empty($sch_prd) && $sch_prd_g1){ // 상품 선택 없이 상품 그룹1로 검색시 그룹2 가져오기
		$prd_g2_sql = "
			SELECT
					k_name,k_name_code,k_parent
			FROM kind
			WHERE k_code='product' AND k_parent='{$sch_prd_g1}' AND display='1'
			ORDER BY priority ASC
		";
	}else{ // 그 외 상품에 따른 그룹2 가져오기
		$prd_g2_sql = "
		    SELECT
		        k_name,k_name_code,k_parent
		    FROM kind
		    WHERE k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='{$sch_prd}')) AND display='1'
		    ORDER BY priority ASC
		";
	}

	$prd_g2_query 	= mysqli_query($my_db, $prd_g2_sql);
	$prd_g2_list	= [];
	while($prd_g2 = mysqli_fetch_array($prd_g2_query)){
		$prd_g2_list[] = array(
			"k_name"		=> trim($prd_g2['k_name']),
			"k_name_code"	=> trim($prd_g2['k_name_code'])
		);
	}
	$smarty->assign("prd_g2_list",$prd_g2_list);

	// 상품 목록 가져오기
	if(empty($sch_prd_get) && $sch_prd_g2){ // 상품 선택 없이 상품 그룹2로 검색시 상품 가져오기
		$prd_sql = "
		    SELECT
		        title,prd_no,k_name_code
		    FROM product
		    WHERE k_name_code='{$sch_prd_g2}'
		    ORDER BY priority ASC
		";
	}else{
		$prd_sql="
		    SELECT
		        title,prd_no,k_name_code
		    FROM product
		    WHERE k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='{$sch_prd}')
		    ORDER BY priority ASC
		";
	}
	$prd_query		= mysqli_query($my_db, $prd_sql);
	$sch_prd_list 	= [];
	while($prd = mysqli_fetch_array($prd_query)) {
		$sch_prd_list[] = array(
			"title"			=> trim($prd['title']),
			"prd_no"		=> trim($prd['prd_no']),
			"k_name_code"	=> trim($prd['k_name_code'])
		);
	}
	$smarty->assign("sch_prd_list", $sch_prd_list);


	$staff_sql 		= "SELECT s_no,s_name FROM staff where staff_state < '3'";
	$staff_query	= mysqli_query($my_db,$staff_sql);
	$staff_list 	= [];
	while($staff_data=mysqli_fetch_array($staff_query)) {
		$staff_list[] = array(
			'no'	=> $staff_data['s_no'],
			'name'	=> $staff_data['s_name']
		);
		$smarty->assign("staff_list", $staff_list);
	}

	# 상품에 대한 입/출금 설정 가져오기 (1:미설정, 2:입금, 3:출금, 4:입금+출금) && work time mehtod 가져오기 (0:미사용, 1:사용(기본값 변경불가), 2:사용(기본값 변경가능)) && work time 기본값
	$product_sql	= "SELECT prd.wd_dp_state, prd.work_time_method, prd.work_time_default FROM product prd WHERE prd.prd_no = '{$sch_prd}'";
	$product_query 	= mysqli_query($my_db,$product_sql);
	$product_data 	= mysqli_fetch_array($product_query);

	$smarty->assign("wd_dp_state", $product_data['wd_dp_state']);
	$smarty->assign("work_time_method", $product_data['work_time_method']);
	$smarty->assign("work_time_default", $product_data['work_time_default']);

	# 정렬순서 토글 & 필드 지정
	$add_orderby	= "";
	$order			= isset($_GET['od'])?$_GET['od']:"";
	$order_type		= isset($_GET['by'])?$_GET['by']:"";

	$toggle			= $order_type ? "asc" : "desc";
	$order_field	= array('','task_req_dday','task_run_dday','extension_date');
	if($order && $order<4) {
		$add_orderby .= " ISNULL($order_field[$order]) ASC, $order_field[$order] $toggle,";
		$add_where .= " AND (DATE_FORMAT(NOW(), '%Y-%m-%d') <= $order_field[$order])";
		$smarty->assign("order",$order);
		$smarty->assign("order_type",$order_type);
	}

	if($q_sch == "5"){
		$add_orderby .= " extension_date asc";
	}else{
		$add_orderby .= " w_no desc";
	}

	# 전체 게시물 수
	$cnt_sql 	= "SELECT count(w.w_no) as cnt FROM `work` w WHERE {$add_where}";
	$cnt_query	= mysqli_query($my_db, $cnt_sql);
	$cnt_data	= mysqli_fetch_array($cnt_query);
	$total_num 	= $cnt_data['cnt'];

	# 페이징 처리
	$page 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num 		= 10;
	$offset 	= ($page-1) * $num;
	$page_num 	= ceil($total_num/$num);

	if ($page >= $page_num){$page = $page_num;}
	if ($page <= 0){$page = 1;}

	$search_url = getenv("QUERY_STRING");
	$page_list	= pagelist($page, "find_work.php", $page_num, $search_url);

	$smarty->assign("page", $page);
	$smarty->assign("page_list",$page_list);
	$smarty->assign("total_num",$total_num);
	$smarty->assign("search_url", $search_url);

	# 리스트 쿼리
	$work_sql = "
		SELECT
			*,
			(SELECT k_name FROM kind WHERE k_name_code=w.k_name_code) AS k_name,
			(SELECT regdate FROM withdraw wd where wd.wd_no=w.wd_no) as wd_regdate,
			(SELECT wd_date FROM withdraw wd where wd.wd_no=w.wd_no) as wd_date,
			(SELECT regdate FROM deposit dp where dp.dp_no=w.dp_no) as dp_regdate,
			(SELECT dp_date FROM deposit dp where dp.dp_no=w.dp_no) as dp_date,
			(SELECT s_name FROM staff s where s.s_no=w.task_req_s_no) as task_req_s_name,
			(SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name,
			(SELECT staff_state FROM staff s where s.s_no=w.task_run_s_no) as task_run_staff_state,
			(SELECT s_name from staff s where s.s_no=w.s_no) as s_no_name,
			(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1_name,
			(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2_name,
			(SELECT title from product prd where prd.prd_no=w.prd_no) as prd_no_name,
			(SELECT prd.wd_dp_state FROM product prd	WHERE prd.prd_no = w.prd_no) as wd_dp_state,
			(SELECT prd.work_time_method FROM product prd	WHERE prd.prd_no = w.prd_no) as work_time_method,
			(SELECT c_name from company c where c.c_no=w.wd_c_no) as wd_c_no_name,
			(SELECT c_name from company c where c.c_no=w.dp_c_no) as dp_c_no_name,
		    (SELECT t.team_name FROM team t WHERE t.team_code=w.team) as t_name, 
                (SELECT t.team_name FROM team t WHERE t.team_code=w.task_req_team) as req_t_name, 
                (SELECT t.team_name FROM team t WHERE t.team_code=w.task_run_team) as run_t_name 
		FROM work w
		WHERE {$add_where}
		ORDER BY {$add_orderby}
		LIMIT {$offset}, {$num}
	";
	$work_query 		= mysqli_query($my_db, $work_sql);
	$work_list 			= [];
	$work_state_option	= getWorkStateOption();
	while($work_array = mysqli_fetch_array($work_query))
	{
		$work_array['reg_day'] 		= !empty($work_array['regdate']) ? date("Y/m/d", strtotime($work_array['regdate'])) : "";
		$work_array['reg_time'] 	= !empty($work_array['regdate']) ?date("H:i", strtotime($work_array['regdate'])) : "";
		$work_array['wd_reg_day'] 	= !empty($work_array['wd_regdate']) ?date("Y/m/d", strtotime($work_array['wd_regdate'])) : "";
		$work_array['wd_reg_time'] 	= !empty($work_array['wd_regdate']) ?date("H:i", strtotime($work_array['wd_regdate'])) : "";
		$work_array['wd_pay_date'] 	= !empty($work_array['wd_date']) ?date("Y-m-d", strtotime($work_array['wd_date'])) : "";
		$work_array['dp_reg_day'] 	= !empty($work_array['dp_regdate']) ?date("Y/m/d", strtotime($work_array['dp_regdate'])) : "";
		$work_array['dp_reg_time'] 	= !empty($work_array['dp_regdate']) ?date("H:i", strtotime($work_array['dp_regdate'])) : "";
		$work_array['dp_pay_date'] 	= !empty($work_array['dp_date']) ?date("Y-m-d", strtotime($work_array['dp_date'])) : "";
		$work_array['task_run_day'] = !empty($work_array['task_run_regdate']) ?date("Y-m-d", strtotime($work_array['task_run_regdate'])) : "";

		$set_tag_token = "";
		if(!!$work_array['set_tag']){
			$set_tag_token = array();
			$set_tag_token = stringTokenized($work_array['set_tag'],'#');
		}
		$work_array['set_tag_token'] 	= $set_tag_token;
		$work_array['work_state_name'] 	= $work_state_option[$work_array['work_state']];

		//연장 마감일 D-Day
		$extension_d_day = "";
		if($work_array['extension_date'])
			$extension_d_day = intval((strtotime(date("Y-m-d",time()))-strtotime($work_array['extension_date'])) / 86400);

		// ±3일 이내 남은 연장여부 없는 것들만 보여줌
		if($work_array['extension'] != '1' && $work_array['extension'] != '3' && !empty($work_array['extension_date']) &&  $work_array['extension_date'] >= date('Y-m-d', strtotime('-3 day',time())) && $work_array['extension_date'] <= date('Y-m-d', strtotime('+3 day', time()))){
			$extension_alert="연장기한";
		// 3일 지난 연장여부 없는 것들만(연장기한초과)
		}elseif(empty($work_array['extension']) && !empty($work_array['extension_date']) && $work_array['extension_date'] < date('Y-m-d', strtotime('+3 day',time()))){
			$extension_alert="연장기한초과";
		}else{
			$extension_alert="";
		}

		$s_no_name          = $work_array['s_no_name'] . "({$work_array['t_name']})";
		$task_req_s_name    = $work_array['task_req_s_name'] . "({$work_array['req_t_name']})";
		$task_run_s_name    = $work_array['task_run_s_name'] . "({$work_array['run_t_name']})";

		$work_array['s_no_name'] 		= !empty($work_array['s_no']) ? $s_no_name : "";
		$work_array['task_req_s_name'] 	= !empty($work_array['task_req_s_no']) ? $task_req_s_name : "";
		$work_array['task_run_s_name'] 	= !empty($work_array['task_run_s_no']) ? $task_run_s_name : "";

		$work_list[] = $work_array;
	}

	$smarty->assign("work_list", $work_list);
	$smarty->display('popup/find_work.html');
}

?>
