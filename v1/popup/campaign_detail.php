<?php
require('../inc/common.php');
require('../inc/data_state.php');
require('../ckadmin.php');
require('../inc/helper/promotion.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";

$p_no_get=isset($_GET['p_no'])?$_GET['p_no']:"";
if(!empty($p_no_get)) {
	$add_where.=" AND p.p_no = '".$p_no_get."'";
	$smarty->assign("p_no",$p_no_get);
}else{
	$smarty->display('access_error.html');
	exit;
}


$promotion_sql = "
	SELECT
		p.title,
		p.p_state,
		p.p_state1,
		(SELECT k.k_name FROM kind k WHERE k.k_code='location' and k_name_code=(SELECT c.location1 FROM company c WHERE c.c_no=p.c_no)) AS location1_name,
		(SELECT k.k_name FROM kind k WHERE k.k_code='location' and k_name_code=(SELECT c.location2 FROM company c WHERE c.c_no=p.c_no)) AS location2_name,
		p.company,
		(SELECT c.address1 FROM company c WHERE c.c_no=p.c_no) AS address1,
		(SELECT c.address2 FROM company c WHERE c.c_no=p.c_no) AS address2,
		(SELECT c.parking FROM company c WHERE c.c_no=p.c_no) AS parking,
		p.kind,
		p.channel,
		p.reg_sdate,
		p.reg_edate,
		p.reg_edate,
		p.exp_sdate,
		p.exp_edate,
		p.awd_date,
		p.reg_num,
		p.posting_num,
		p.re_apply,
		p.product,
		p.refer_url1,
		p.refer_url2,
		p.cons_exp_time,
		p.pres_reward,
		p.pres_reason,
		p.pres_money,
		p.pres_visit,
	    p.pres_visit_view,
		p.pres_purpose,
		p.deli_shipping,
		p.deli_zone,
		p.deli_type
	FROM promotion p
	WHERE 1=1 {$add_where}
	";

$promotion_result = mysqli_query($my_db, $promotion_sql);
$promotion = mysqli_fetch_array($promotion_result);

$promotion_channel_option = getPromotionChannelOption();
$promotion_kind_option    = getPromotionKindOption();

// channel name
$channel_name = $promotion_channel_option[$promotion['channel']];
$kind_name 	  = $promotion_kind_option[$promotion['kind']];

// 요일배열
$datey=array('일','월','화','수','목','금','토');

// 업체명 사이 . 찍기, 캠페인명 추가
$promotion_title = (isset($promotion['title']) && !empty($promotion['title'])) ? $promotion['title'] : $promotion['company'];
$promotion_dot_name = "";
$company_dot_name = "";

for($i = 0 ; $i < mb_strlen($promotion_title, 'utf-8') ; $i++) {
	if($i > 0)
		$promotion_dot_name .= ".";
	$promotion_dot_name .= mb_substr($promotion_title,$i,1, 'utf-8');
}

for($i = 0 ; $i < mb_strlen($promotion['company'], 'utf-8') ; $i++) {
    if($i > 0)
        $company_dot_name .= ".";
    $company_dot_name .= mb_substr($promotion['company'],$i,1, 'utf-8');
}
// 줄바꿈 치환
$product_line_break = nl2br($promotion['product']);

$smarty->assign(
	array(
		"p_state"=>$promotion['p_state'],
		"p_state1"=>$promotion['p_state1'],
		"location1_name"=>$promotion['location1_name'],
		"location2_name"=>$promotion['location2_name'],
		"company"=>$promotion['company'],
		"company_dot_name"=>$company_dot_name,
		"promotion_dot_name"=>$promotion_dot_name,
		"address1"=>$promotion['address1'],
		"address2"=>$promotion['address2'],
		"parking"=>$promotion['parking'],
		"channel"=>$promotion['channel'],
		"channel_name"=>$channel_name,
		"kind"=>$promotion['kind'],
		"kind_name"=>$kind_name,
		"reg_sdate"=>date("m/d(".$datey[date("w",strtotime($promotion['reg_sdate']))].")",strtotime($promotion['reg_sdate'])),
		"reg_edate"=>date("m/d(".$datey[date("w",strtotime($promotion['reg_edate']))].")",strtotime($promotion['reg_edate'])),
		"exp_sdate"=>date("m/d(".$datey[date("w",strtotime($promotion['exp_sdate']))].")",strtotime($promotion['exp_sdate'])),
		"exp_edate"=>date("m/d(".$datey[date("w",strtotime($promotion['exp_edate']))].")",strtotime($promotion['exp_edate'])),
		"awd_date"=>date("m/d(".$datey[date("w",strtotime($promotion['awd_date']))].")",strtotime($promotion['awd_date'])),
		"reg_num"=>$promotion['reg_num'],
		"pres_num"=>$promotion['posting_num']/$promotion['reg_num'],
		"re_apply"=>$promotion['re_apply'],
		"product"=>$product_line_break,
		"refer_url1"=>$promotion['refer_url1'],
		"refer_url2"=>$promotion['refer_url2'],
		"cons_exp_time"=>$promotion['cons_exp_time'],
		"pres_reward"=>date("m/d(".$datey[date("w",strtotime($promotion['pres_reward']))].")",strtotime($promotion['pres_reward'])),
		"pres_reason"=>$promotion['pres_reason'],
		"pres_money"=>number_format($promotion['pres_money']),
		"pres_visit"=>$promotion['pres_visit'],
		"pres_visit_view"=>$promotion['pres_visit_view'],
		"pres_purpose"=>$promotion['pres_purpose'],
		"deli_shipping"=>$promotion['deli_shipping'],
		"deli_zone"=>$promotion['deli_zone'],
		"deli_type"=>$promotion['deli_type']
	)
);


$smarty->display('popup/campaign_detail.html');
?>
