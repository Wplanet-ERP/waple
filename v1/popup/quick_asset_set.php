<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/asset.php');
require('../inc/model/Kind.php');

# 자산 상품 리스트
$kind_model       = Kind::Factory();
$asset_group_list = $kind_model->getKindGroupList("asset");
$asset_g2_list = $asset_group_list['05044'];
$smarty->assign("asset_g2_list", $asset_g2_list);

$proc=(isset($_POST['process']))?$_POST['process']:"";
if ($proc == "proc_quick_asset")
{
	$mode = $_POST['mode'];
	$quick_asset_req = $_POST['quick_asset'];

	if ($session_id) {
		$sql = "
			SELECT 
				(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=`as`.k_name_code)) AS k_asset1_name,
        		(SELECT k_name FROM kind k WHERE k.k_name_code=`as`.k_name_code) AS k_asset2_name,
         		as_no, `name`, object_url, object_site  FROM asset `as`
		";

		$result = mysqli_query($my_db, $sql);

		while ($row = mysqli_fetch_assoc($result)) {
			$arr_asset[$row['as_no']] = array('name' => $row['name'], 'k_asset1_name' => $row['k_asset1_name'], 'k_asset2_name' => $row['k_asset2_name'], 'object_url' => $row['object_url'], 'object_site' => $row['object_site'],);
		}

		$sql = "
			SELECT quick_asset
			  FROM staff
			 where id = '{$session_id}'
		";
		$result = mysqli_query($my_db, $sql);
		$row = mysqli_fetch_array($result);
		$staff_quick_asset= $row['quick_asset'];
		$arr_q_asset = explode(",", $staff_quick_asset);

		if ($mode == "add" && in_array($quick_asset_req, $arr_q_asset)) {
			$r_msg = "err_exist_asset";
			echo $r_msg;
			exit;
		}

		if ($mode == "del") {
			$key = array_search($quick_asset_req, $arr_q_asset);
			unset($arr_q_asset[$key]);
		} else {
			array_push($arr_q_asset, $quick_asset_req);
		}

		$quick_asset = implode(",", $arr_q_asset);
		if (substr($quick_asset, 0, 1) == ",")
			$quick_asset = substr($quick_asset, 1);

		if($mode == "sort"){
			$select_quick_asset = isset($_POST['sort_asset']) && !empty($_POST['sort_asset']) ? $_POST['sort_asset'] : "";
			if($select_quick_asset != "") {
				$quick_asset = $select_quick_asset;
			}
		}

		if ($mode != "display") {
			$sql = "
				UPDATE staff
				   set quick_asset = '{$quick_asset}'
				 where id = '{$session_id}'
			";

			mysqli_query($my_db, $sql);
		}

		$r_msg = "ok";

		mysqli_free_result($result);
		mysqli_close($my_db);
	}
	echo $r_msg;

	$arr_q_asset = explode(",", $quick_asset);
	echo "|";
	foreach  ($arr_q_asset as $key => $value) {
		if ($value) {
			$title = "[{$arr_asset[$value]['k_asset1_name']} > {$arr_asset[$value]['k_asset2_name']} > {$arr_asset[$value]['name']}] {$arr_asset[$value]['object_site']} : {$arr_asset[$value]['object_url']}";
			echo "<span class='grid-square' style='display: block'>";
			echo "<input type='text' class='select_quick_asset' name='select_quick_asset' attr-asset='{$value}' readonly value='{$title}' />";
			echo "<img src=\"./../images/icon/delete_icon.png\" onclick=\"proc_quick_asset('del', '". $value . "');\" style=\"cursor:pointer;margin-right:10px;\" />";
			echo "</span>";
		}
	}
	exit;
}

$smarty->display('popup/quick_asset_set.html');

?>
