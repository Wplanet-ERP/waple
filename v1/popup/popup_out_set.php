<?php
require('../inc/common.php');
require('../inc/data_state.php');
require('../ckadmin.php');


$proc = isset($_POST['process']) ? $_POST['process'] : "";
$dp_no = isset($_GET['dp_no']) ? $_GET['dp_no'] : "";
$c_no = isset($_GET['c_no']) ? $_GET['c_no'] : "";

$smarty->assign("dp_no", $dp_no);
$smarty->assign("c_no", $c_no);

if ($proc == "save") {

	$w_no_list = $_POST['w_no_list'];

	if ($dp_no) {
		$sql = "
			update work set
				dp_no = null
			where
				dp_no = '{$dp_no}';
		" . PHP_EOL;


		if ($w_no_list) {

			$sql .= "
				update work set
					dp_no = {$dp_no}
				where
					w_no in ({$w_no_list})
			";
		}

		//echo $sql; exit;

		mysqli_query($my_db,$sql);

		if (mysqli_multi_query($my_db, $sql)) {
			exit("<script>alert('저장되었습니다.');parent.ifr_close_reload();</script>");
		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($my_db);
			//exit("<script>alert('에러가 발생하였습니다.');history.back();</script>");
		}
	}

} else {

	$sql = "
		select
			w_no, date_format(task_run_regdate, '%y-%m-%d') task_run_regdate, c_no, c_name,
			(select s_name from staff where s_no = work.s_no) as s_no_name,
			(select title from product where prd_no = work.prd_no) as prd_no_name,
			prd_no, t_keyword, r_keyword, task_run,
			price
		from
			work
		where
			dp_no = '{$dp_no}'
		order by prd_no_name
		";

	$query = mysqli_query($my_db, $sql);

	while ($data = mysqli_fetch_array($query)) {
		if ($data['prd_no'] == 7 || $data['prd_no'] == 30)
			$task = $data['t_keyword'] . " &gt; " . $data['r_keyword'];
		else
			$task = $data['task_run'];

		$arr = [$data['w_no'], $data['task_run_regdate'], $data['c_name'], $data['s_no_name'], $data['prd_no_name'], $task, "\\".number_format($data['price'])];
		$ori_str = implode(" | ", $arr);

		$c_name = mb_strimwidth($data['c_name'], '0', '20', '..', 'utf-8');
		$prd_no_name = mb_strimwidth($data['prd_no_name'], '0', '15', '..', 'utf-8');
		$task = mb_strimwidth($task, '0', '30', '..', 'utf-8');

		$om_list_l[] = array(
			"w_no" => $data['w_no'],
			"task_run_regdate" => $data['task_run_regdate'],
			"c_no" => $data['c_no'],
			"c_name" => $c_name,
			"s_no_name" => $data['s_no_name'],
			"prd_no_name" => $prd_no_name,
			"t_keyword" => $data['t_keyword'],
			"r_keyword" => $data['r_keyword'],
			"task_run" => $data['task_run'],
			"task" => $task,
			"price" => number_format($data['price']),
			"ori_str" => $ori_str
		);
	}

	$smarty->assign("om_list_l", $om_list_l);

	// 검색쿼리 & GET 초기화
	$add_where = " 1 = 1";
	$sch_prd_no_get = isset($_GET['sch_prd_no']) ? $_GET['sch_prd_no'] : "";
	$sch_c_name_get = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";

	if(!empty($sch_prd_no_get)) { // 상품
		$add_where.=" and prd_no='".$sch_prd_no_get."'";
		$smarty->assign("sch_prd_no",$sch_prd_no_get);

		for($i=0 ; $i < count($out_product) ; $i++){
			if($out_product[$i][1] == $sch_prd_no_get){
				$smarty->assign("sch_prd_no_value", $out_product[$i][0]);
			}
		}
	}

	if(!empty($sch_c_name_get)) { // 업체명
		$add_where.= " and c_name like '%".$sch_c_name_get."%'";
		$smarty->assign("sch_c_name", $sch_c_name_get);
	} else {
		$add_where.= " and c_no = '{$c_no}' ";
	}

	if (empty($sch_s_no_get) && !permissionNameCheck($session_permission, "대표") && permissionNameCheck($session_permission, "마케터")) {
		$add_where .= " and s_no = '" . $session_s_no . "'";
	}

	$sql = "
		select
			w_no, date_format(task_run_regdate, '%y-%m-%d') task_run_regdate, c_no, c_name,
			(select s_name from staff where s_no = work.s_no) as s_no_name,
			(select title from product where prd_no = work.prd_no) as prd_no_name,
			prd_no, t_keyword, r_keyword, task_run,
			price
		from
			work
		where
			$add_where
		and
			price > 0 and price_vat > 0
		and
			(dp_no = '' or dp_no is null)
		order by 1 desc
		";

	$query = mysqli_query($my_db, $sql);

	while ($data = mysqli_fetch_array($query)) {
		if ($data['prd_no'] == 7 || $data['prd_no'] == 30)
			$task = $data['t_keyword'] . " &gt; " . $data['r_keyword'];
		else
			$task = $data['task_run'];

		$arr = [$data['w_no'], $data['task_run_regdate'], $data['c_name'], $data['s_no_name'], $data['prd_no_name'], "\\".number_format($data['price']), $task];
		$ori_str = implode(" | ", $arr);

		$c_name = mb_strimwidth($data['c_name'], '0', '20', '..', 'utf-8');
		$prd_no_name = mb_strimwidth($data['prd_no_name'], '0', '20', '..', 'utf-8');
		$task = mb_strimwidth($task, '0', '30', '..', 'utf-8');

		$om_list_r[] = array(
			"w_no" => $data['w_no'],
			"task_run_regdate" => $data['task_run_regdate'],
			"c_no" => $data['c_no'],
			"c_name" => $c_name,
			"s_no_name" => $data['s_no_name'],
			"prd_no_name" => $prd_no_name,
			"price" => number_format($data['price']),
			"t_keyword" => $data['t_keyword'],
			"r_keyword" => $data['r_keyword'],
			"task_run" => $data['task_run'],
			"task" => $task,
			"ori_str" => $ori_str
		);
	}

	$smarty->assign("om_list_r", $om_list_r);

	$smarty->display('popup/popup_out_set.html');
}
?>
