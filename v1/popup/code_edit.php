<?php
require('../inc/common.php');
require('../ckadmin.php');

//echo "<br><br><br>";
// 초기값 세팅
$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

$proc=(isset($_POST['process']))?$_POST['process']:"";
$code=(isset($_GET['code']))?$_GET['code']:"";
$smarty->assign("category",$code);

$r_no=(isset($_GET['no']))?$_GET['no']:"";
$smarty->assign("r_no",$r_no);

$input_id=(isset($_GET['input']))?$_GET['input']:"";
$smarty->assign("input_id",$input_id);
//echo $code;
//exit;

if(!in_array($code,array(1,2,3)))
	$page_code="all";
else
	$page_code=$code;

$smarty->assign("page_code",$page_code);


if($proc=="memo") {
	//print_r($_POST);
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$mtxt=(isset($_POST['memo'.$mno]))?$_POST['memo'.$mno]:"";
	$sql="update promotion set p_memo='".$mtxt."' where p_no='".$mno."'";
	//exit("<hr>".$sql);
	

	mysqli_query($my_db,$sql);

	exit("<script>alert('메모를 등록하였습니다');location.href='main.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
}


// 요일배열
$datey=array('일','월','화','수','목','금','토');

// 직원가져오기
$staff_sql="select s_no,s_name from staff where staff_state < '3'";
$staff_query=mysqli_query($my_db,$staff_sql);

while($staff_data=mysqli_fetch_array($staff_query)) {
	$staffs[]=array(
			'no'=>$staff_data['s_no'],
			'name'=>$staff_data['s_name']
	);
	$smarty->assign("staff",$staffs);
}


// 단순히 상태값을 텍스트 표현하기 위해 배열에 담은 정보
$state=array(
				1=>'접수대기',
				2=>'접수완료',
				3=>'모집중',
				4=>'진행중',
				5=>'진행중단',
				6=>'마감'
			);
$state1=array(
				0=>'진행중',
				1=>'진행중단',
			);

// 목록에서 kind 값을 통해, 구분명과 수정페이지 url 앞단어를 가져옴
$kind=array(
				'name'=>array(
					'',
					'체험단',
					'기자단',
					'배송체험'
				),
				'url'=>array(
					'',
					'consumer',
					'press',
					'delivery'
				)
			);







// 전체 게시물 수
if ($code !="" && $code !="all")
	$cnt_query = "promotion where kind=".$code;
else
	$cnt_query = "promotion";
$total_num = getcnt($cnt_query);
$totalnum = $total_num;
$smarty->assign("total_num",$total_num);

// 페이징
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;

$num = 20;

$pagenum = ceil($totalnum/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$url = "code=".$code;
$page=pagelist($pages, "code_edit.php", $pagenum, $url);
$smarty->assign("pagelist",$page);//
$noi = $totalnum - ($pages-1)*$num;
$offset = ($pages-1) * $num;
$smarty->assign("totalnum",$totalnum);




// all 이 아니면? kind 포함시키기 : 기본검색으로 지정하지 않았을 경우 where 절을 출력하기 위해서 사용.
if($page_code!="all") {
	$add_where=" where kind=".$page_code." ";
	$_GET['skind']=$page_code;
}


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";
$field=array();
$keyword=array();
$where=array();
$search_state=isset($_GET['sstate'])?$_GET['sstate']:"";
$search_name=isset($_GET['sname'])?$_GET['sname']:"";
$search_kind=isset($_GET['skind'])?$_GET['skind']:"";
$search_company=isset($_GET['scompany'])?$_GET['scompany']:"";

if(!empty($search_state)) {
	$field[]="sstate";
	$keyword[]=$search_state;
	$where[]="p_state='".$search_state."'";
}

if(!empty($search_name)) {
	$field[]="sname";
	$keyword[]=$search_name;
	$where[]="(name='".$search_name."')";
}

if(!empty($search_kind)) {
	$field[]="skind";
	$keyword[]=$search_kind;
	$where[]="kind='".$search_kind."'";
}

if(!empty($search_company)) {
	$field[]="scompany";
	$keyword[]=$search_company;
	$where[]="company like '%".$search_company."%'";
}

if(sizeof($where)>0) {
	$add_where=" where ";
	for($i=0;$i<sizeof($where);$i++) {
		$add_where.=$where[$i].(($i<(sizeof($where)-1))?" and ":"");
		$smarty->assign($field[$i],$keyword[$i]);
	}

}




// 정렬순서 토글 & 필드 지정
$add_orderby="";
$order=isset($_GET['od'])?$_GET['od']:"";
$order_type=isset($_GET['by'])?$_GET['by']:"";
$toggle=$order_type?"asc":"desc";
$order_field=array('','reg_sdate','reg_edate','awd_date','exp_edate','pres_reward');
if($order && $order<6) {
	$add_orderby.="$order_field[$order] $toggle,";
	$smarty->assign("order",$order);
	$smarty->assign("order_type",$order_type);
}
$add_orderby.=" p_no desc";







// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query",$save_query);




$promotion_sql="
				select 
					a.p_no,
					a.s_no,
					a.kind,
					a.promotion_code,
					a.company,
					a.reg_num,
					a.p_state,
					a.p_state1,
					a.pres_reward,
					(select count(b.a_no) from application b where b.p_no=a.p_no) as app_num,
					(select count(b.a_no) from application b where b.p_no=a.p_no and b.a_state=1) as accept_num,
					(select count(r.r_no) from report r where r.p_no=a.p_no) as report_num,
					a.posting_num,a.name,a.reg_sdate,a.reg_edate,a.awd_date,a.exp_edate,a.p_memo,a.p_regdate 
				from 
					promotion a 
				$add_where 
				order by $add_orderby
				limit $offset,$num
				";
//exit($promotion_sql);
//echo "<br><br><br><br><br><br><br><br>";
//echo $promotion_sql;
//exit;
$promotion_query= mysqli_query($my_db, $promotion_sql);
while($promotion_array = mysqli_fetch_array($promotion_query)){
	// 포스팅 건수가 2건 이상인 경우는, 접수인원 x 포스팅 건수로 해주어야 한다. 
	// 50명이 접수하고, 한 사람당 2건씩 써야 하면 50 x 2 = 100 으로 화면에 표시
	if($promotion_array['posting_num']!=0)
		$posting_num = $promotion_array['posting_num'];
		//$posting_num = $promotion_array['posting_num'];
		if ($promotion_array['pres_reward'] != NULL)
			$pres_reward = date("n/d(".$datey[date("w",strtotime($promotion_array['pres_reward']))].")",strtotime($promotion_array['pres_reward']));
		else
			$pres_reward = "-";

		$total_refund_sql="select refund_count from application where p_no='".$promotion_array['p_no']."'";
		$total_refund_query= mysqli_query($my_db, $total_refund_sql);
		$total_refund=0;
		//echo "<br><br><br><br> refund_SQL = ".$total_refund_sql."<br>";
		while($total_refund_array = mysqli_fetch_array($total_refund_query)){
		//echo "<br> refund = ".$total_refund += $total_refund_array[0]."<br><br><br>";
		$total_refund += $total_refund_array[0];
		}
	$promotions[] = array(
		"no"=>$promotion_array['p_no'],
		"kind"=>$promotion_array['kind'],
		"kind_name"=>$kind['name'][$promotion_array['kind']],
		"kind_url"=>$kind['url'][$promotion_array['kind']],
		"date_ymd"=>date("Y.m.d",strtotime($promotion_array['p_regdate'])),
		"date_hi"=>date("H:i",strtotime($promotion_array['p_regdate'])),
		"name"=>$promotion_array['name'],
		"company"=>$promotion_array['company'],
		"reg_start"=>date("n/d(".$datey[date("w",strtotime($promotion_array['reg_sdate']))].")",strtotime($promotion_array['reg_sdate'])),
		"reg_end"=>date("n/d(".$datey[date("w",strtotime($promotion_array['reg_edate']))].")",strtotime($promotion_array['reg_edate'])),
		"awd_date"=>$promotion_array['awd_date'],
		"award"=>date("n/d(".$datey[date("w",strtotime($promotion_array['awd_date']))].")",strtotime($promotion_array['awd_date'])),
		"exp_edate"=>date("n/d(".$datey[date("w",strtotime($promotion_array['exp_edate']))].")",strtotime($promotion_array['exp_edate'])),
		"pres_reward"=>$pres_reward,
		"awd_complete"=>(($promotion_array['p_state']>3)?"10/10":"-"),
		"reg_num"=>$promotion_array['reg_num'],
		"app_num"=>$promotion_array['app_num'],
		"accept_num"=>$promotion_array['accept_num'],
		"p_memo"=>$promotion_array['p_memo'],
		"p_code"=>$promotion_array['promotion_code'],
		"posting"=>$posting_num,
		"total_refund"=>$total_refund,
		"report_num"=>$promotion_array['report_num'],
		"p_state"=>$promotion_array['p_state'],
		"p_state1"=>$promotion_array['p_state1'],
		"state_code"=>$state[$promotion_array['p_state']],
		"state_code1"=>$state1[$promotion_array['p_state1']]
	);
	//print_r($promotions);
	$smarty->assign(array(
		"promotion"=>$promotions
	));
}
$smarty->assign("p_code",$frm_zip2);
$smarty->display('popup/code_edit.html');
?>