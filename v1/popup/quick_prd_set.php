<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/quick_set.php');
require('../inc/model/Kind.php');
require('../inc/model/ProductCmsUnit.php');
require('../inc/model/Navigation.php');
require('../inc/model/Team.php');

$process = isset($_POST['process']) ? $_POST['process'] : "";
$is_with = ($session_my_c_type == '3') ? true : false;

if($process == "add_quick_prd")
{
    $prd_no     = isset($_POST['f_prd']) ? $_POST['f_prd'] : "";
    $prd_g2     = isset($_POST['f_prd_g2']) ? $_POST['f_prd_g2'] : "";
    $quick_type = isset($_POST['quick_type']) ? $_POST['quick_type'] : "";
    $priority   = isset($_POST['priority']) ? $_POST['priority']+1 : 1;

    $add_quick_type = $quick_type;
    if($quick_type == 'cms_with'){
        $add_quick_type = "cms";
    }elseif($quick_type == 'cms_unit_with'){
        $add_quick_type = "cms_unit";
    }elseif($quick_type == 'navigation'){
        if(empty($prd_no)){
            $prd_no = $prd_g2;
        }
    }

    $ins_sql   = "INSERT INTO quick_search SET s_no='{$session_s_no}', `type`='{$add_quick_type}', prd_no='{$prd_no}', priority='{$priority}', regdate=now()";

    if(!mysqli_query($my_db, $ins_sql)){
        exit("<script>alert('등록에 실패했습니다');location.href='quick_prd_set.php?sch_quick_type={$quick_type}';</script>");
    }else{
        exit("<script>alert('등록했습니다.');location.href='quick_prd_set.php?sch_quick_type={$quick_type}';</script>");
    }
}
elseif($process == "del_quick_prd")
{
    $quick_no   = isset($_POST['quick_no']) ? $_POST['quick_no'] : "";
    $quick_type = isset($_POST['quick_type']) ? $_POST['quick_type'] : "";
    $del_sql    = "";

    $chk_sql    = "SELECT * FROM quick_search WHERE q_no='{$quick_no}'";
    $chk_query  = mysqli_query($my_db, $chk_sql);
    $chk_result = mysqli_fetch_assoc($chk_query);

    if(!empty($quick_no)){
        $del_sql = "DELETE FROM quick_search WHERE q_no='{$quick_no}'";
    }
    mysqli_query($my_db, $del_sql);
    exit;
}
elseif($process == "sort_quick_prd")
{
    $sort_prd_list  = isset($_POST['sort_prd']) ? explode(',', $_POST['sort_prd']) : [];
    $quick_type     = isset($_POST['quick_type']) ? $_POST['quick_type'] : "";

    $add_quick_type = $quick_type;
    if($quick_type == 'cms_with'){
        $add_quick_type = "cms";
    }elseif($quick_type == 'cms_unit_with'){
        $add_quick_type = "cms_unit";
    }

    if(!empty($sort_prd_list))
    {
        $priority = 1;
        foreach($sort_prd_list as $sort_prd)
        {
            $upd_sql = "UPDATE quick_search SET priority='{$priority}' WHERE s_no='{$session_s_no}' AND `type`='{$add_quick_type}' AND prd_no='{$sort_prd}'";
            $priority++;

            mysqli_query($my_db, $upd_sql);
        }
    }

    exit;
}

$sch_s_no 	    = $session_s_no;
$sch_quick_type = isset($_REQUEST['sch_quick_type']) ? $_REQUEST['sch_quick_type'] : "work";
$sch_keyword    = isset($_REQUEST['sch_keyword']) ? $_REQUEST['sch_keyword'] : "";
$with_log_c_no  = '2809';

if($is_with && ($sch_quick_type != "cms_with" && $sch_quick_type != "cms_unit_with")){
    echo "<script>alert('접근할 수 없습니다');this.close()</script>";
    exit;
}

$prd_g1_list 	= [];
$my_quick_list 	= [];

$add_quick_field    = "";
$add_quick_where    = "";
$quick_title_where  = "";

$kind_model     = Kind::Factory();
$unit_model     = ProductCmsUnit::Factory();
$nav_model      = Navigation::Factory();
$team_model     = Team::Factory();

if($sch_quick_type == 'work'){
    $prd_g1_list        = $kind_model->getKindParentList("product");
    $quick_title_where  = "SELECT p.prd_no FROM product as p WHERE p.title";
    $add_quick_where    = " AND `type`='{$sch_quick_type}' ";
    $add_quick_field    = "
    		(SELECT p.prd_no FROM product as p WHERE p.prd_no=qs.prd_no) AS search,
    		(SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub.k_parent FROM kind sub WHERE sub.k_name_code=(SELECT p.k_name_code FROM product as p WHERE p.prd_no=qs.prd_no))) AS prd_g1_name,
			(SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT p.k_name_code FROM product as p WHERE p.prd_no=qs.prd_no)) AS prd_g2_name,
			(SELECT p.title FROM product as p WHERE p.prd_no=qs.prd_no) as prd_name,
			(SELECT p.display FROM product as p WHERE p.prd_no=qs.prd_no) as display
	";
}
elseif($sch_quick_type == 'cms'){
    $prd_g1_list        = $kind_model->getKindParentList("product_cms");
    $quick_title_where  = "SELECT p.prd_no FROM product_cms as p WHERE p.title";
    $add_quick_where    = " AND `type`='{$sch_quick_type}' ";
    $add_quick_field    = "
            (SELECT p.prd_code FROM product_cms as p WHERE p.prd_no=qs.prd_no) AS search,
    		(SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub.k_parent FROM kind sub WHERE sub.k_name_code=(SELECT p.k_name_code FROM product_cms as p WHERE p.prd_no=qs.prd_no))) AS prd_g1_name,
			(SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT p.k_name_code FROM product_cms as p WHERE p.prd_no=qs.prd_no)) AS prd_g2_name,
			(SELECT p.title FROM product_cms as p WHERE p.prd_no=qs.prd_no) as prd_name,
			(SELECT p.display FROM product_cms as p WHERE p.prd_no=qs.prd_no) as display
	";
}
elseif($sch_quick_type == 'cms_unit'){
	$prd_g1_list        = $unit_model->getBrandData();
    $quick_title_where  = "SELECT p.`no` FROM product_cms_unit p WHERE option_name";
    $add_quick_where    = " AND `type`='{$sch_quick_type}' ";
    $add_quick_field    = "
            (SELECT p.`no` FROM product_cms_unit as p WHERE p.`no`=qs.prd_no) AS search,
    		(SELECT REPLACE(c.c_name,'(주)','') FROM company c WHERE c.c_no=(SELECT p.brand FROM product_cms_unit as p WHERE p.no=qs.prd_no)) AS prd_g1_name,
			(SELECT p.option_name FROM product_cms_unit as p WHERE p.no=qs.prd_no) as prd_name,
			(SELECT p.display FROM product_cms_unit as p WHERE p.no=qs.prd_no) as display
	";
}
elseif($sch_quick_type == 'cms_with'){
    $prd_g1_list        = $kind_model->getKindParentList("product_cms");
    $quick_title_where  = "SELECT p.prd_no FROM product_cms as p WHERE p.title";
    $add_quick_where    = " AND `type`='cms' ";
    $add_quick_field    = "
            (SELECT p.prd_code FROM product_cms as p WHERE p.prd_no=qs.prd_no) AS search,
    		(SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub.k_parent FROM kind sub WHERE sub.k_name_code=(SELECT p.k_name_code FROM product_cms as p WHERE p.prd_no=qs.prd_no))) AS prd_g1_name,
			(SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT p.k_name_code FROM product_cms as p WHERE p.prd_no=qs.prd_no)) AS prd_g2_name,
			(SELECT p.title FROM product_cms as p WHERE p.prd_no=qs.prd_no) as prd_name,
			(SELECT p.display FROM product_cms as p WHERE p.prd_no=qs.prd_no) as display
	";
}
elseif($sch_quick_type == 'cms_unit_with'){
    $prd_g1_list        = $unit_model->getBrandData();
    $quick_title_where  = "SELECT pcum.prd_unit FROM product_cms_unit_management pcum WHERE pcum.log_c_no='{$with_log_c_no}' AND pcum.sku";
    $add_quick_where    = " AND `type`='cms_unit' ";
    $add_quick_field    = "
            (SELECT p.`no` FROM product_cms_unit as p WHERE p.`no`=qs.prd_no) AS search,
    		(SELECT REPLACE(c.c_name,'(주)','') FROM company c WHERE c.c_no=(SELECT p.brand FROM product_cms_unit as p WHERE p.no=qs.prd_no)) AS prd_g1_name,
			(SELECT pcum.sku FROM product_cms_unit_management as pcum WHERE pcum.prd_unit=qs.prd_no AND pcum.log_c_no='{$with_log_c_no}') as prd_name,
			(SELECT p.display FROM product_cms_unit as p WHERE p.no=qs.prd_no) as display
	";
}
elseif($sch_quick_type == 'navigation'){
    $prd_g1_list        = $nav_model->getParentNavigationList();
    $quick_title_where  = "SELECT p.`no` FROM navigation p WHERE nav_name";
    $add_quick_where    = " AND `type`='navigation' ";
    $add_quick_field    = "
            (SELECT p.`no` FROM navigation as p WHERE p.`no`=qs.prd_no) AS search,
    		(SELECT k.nav_name FROM navigation k WHERE k.nav_code=(SELECT sub.parent_nav_code FROM navigation sub WHERE sub.nav_code=(SELECT p.parent_nav_code FROM navigation as p WHERE p.`no`=qs.prd_no))) AS prd_g1_name,
			(SELECT k.nav_name FROM navigation k WHERE k.nav_code=(SELECT p.parent_nav_code FROM navigation as p WHERE p.`no`=qs.prd_no)) AS prd_g2_name,
			(SELECT p.nav_name FROM navigation as p WHERE p.`no`=qs.prd_no) as prd_name,
			(SELECT p.display FROM navigation as p WHERE p.`no`=qs.prd_no) as display
	";
}
elseif($sch_quick_type == 'staff'){
    $team_all_list      = $team_model->getTeamAllList();
    $prd_g1_list        = $team_all_list['team_name_list'];
    $quick_title_where  = "SELECT s.`s_no` FROM staff s WHERE s_name";
    $add_quick_where    = " AND `type`='staff' ";
    $add_quick_field    = "
            (SELECT s.`s_no` FROM staff as s WHERE s.s_no=qs.prd_no) AS search,
    		(SELECT t.team_name FROM team t WHERE t.team_code=(SELECT s.team FROM staff as s WHERE s.s_no=qs.prd_no)) AS prd_g1_name,
			(SELECT s.s_name FROM staff as s WHERE s.s_no=qs.prd_no) as prd_name,
			(SELECT s.staff_state FROM staff as s WHERE s.s_no=qs.prd_no) as display
	";
}

if(!empty($sch_keyword))
{
    $add_quick_where .= "AND qs.prd_no IN ({$quick_title_where} LIKE '%{$sch_keyword}%')";
    $smarty->assign("sch_keyword", $sch_keyword);
}


$my_quick_sql   = "SELECT qs.q_no, qs.prd_no, qs.priority, {$add_quick_field} FROM quick_search as qs WHERE s_no='{$sch_s_no}' {$add_quick_where} ORDER BY priority ASC";
$my_quick_query = mysqli_query($my_db, $my_quick_sql);
$my_quick_list  = [];

$max_priority = 0;
while($my_quick = mysqli_fetch_assoc($my_quick_query))
{
    $prd_name = "";

    if($my_quick['display'] == '2'){
        $prd_name = "[Display OFF : 사용불가!] ";
    }

    if(!empty($my_quick['prd_g1_name'])){
        $prd_name .= $my_quick['prd_g1_name'];
    }

    if(!empty($my_quick['prd_g2_name'])){
        $prd_name .= !empty($my_quick['prd_g1_name']) ? " > ".$my_quick['prd_g2_name'] : $my_quick['prd_g2_name'];
    }

    if(!empty($my_quick['prd_name'])){
        $prd_name .= (!empty($my_quick['prd_g1_name']) || !empty($my_quick['prd_g2_name'])) ? " > ".$my_quick['prd_name'] : $my_quick['prd_name'];
    }

    $my_quick_list[$my_quick['q_no']] = array(
        "prd_no"    => $my_quick['prd_no'],
        "prd_name"  => $prd_name,
        "display"   => $my_quick['display'],
        "search"    => $my_quick['search']
    );

    $max_priority = $my_quick['priority'];
}

$smarty->assign("sch_quick_type", $sch_quick_type);
$smarty->assign("is_with", $is_with);
$smarty->assign("type_option", getQuickTypeOption());
$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("max_priority", $max_priority);
$smarty->assign("my_quick_list", $my_quick_list);

$smarty->display('popup/quick_prd_set.html');

?>