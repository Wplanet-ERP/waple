<?php
require('../inc/common.php');
require('../ckadmin.php');

if (permissionCheck($session_permission, "010001")){

	// 초기값 세팅
	$nowdate = date("Y-m-d H:i:s");
	$smarty->assign("nowdate",$nowdate);
	
	$proc=(isset($_POST['process']))?$_POST['process']:"";
	//echo $proc."==proc<br>";
	
	if (isset($_GET['r_no']))
		$r_no=$_GET['r_no'];
	else if (isset($_POST['r_no']))
		$r_no=$_POST['r_no'];
	
	$smarty->assign("r_no",$r_no);
	
	if (isset($_GET['p_no']))
		$p_no=$_GET['p_no'];
	else if (isset($_POST['p_no']))
		$p_no=$_POST['p_no'];
	
	$smarty->assign("p_no",$p_no);
	
	//echo $r_no."==r_no<br>";
	//echo $p_no."==p_no<br>";
	
	
	
	if($proc=="update") {
	
		//print_r($_POST);exit;
		$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
		$r_no=(isset($_POST['r_no']))?$_POST['r_no']:"";
		$p_no=(isset($_POST['p_no']))?$_POST['p_no']:"";
		$blog_url=(isset($_POST['blog_url']))?$_POST['blog_url']:"";
		$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
		$mtxt=(isset($_POST['memo'.$mno]))?$_POST['memo'.$mno]:"";
	
		$application_sql="select * from application where blog_url='".$blog_url."' and p_no='".$p_no."'";
		$application_query=mysqli_query($my_db,$application_sql);
		$application_info=mysqli_fetch_array($application_query);
		
		if($application_info['bk_title'] == "" && $application_info['bk_num'] == "" && $application_info['bk_name'] == "" && $application_info['bk_jumin'] == ""){
			$report_sql="select * from report where r_no='".$r_no."'";
			$report_query=mysqli_query($my_db,$report_sql);
			$report_info=mysqli_fetch_array($report_query);
	
	//		echo $report_sql."==report_sql<br>";//////////////
			
			$query="UPDATE application SET
						bk_title = '".$report_info['bk_title']."',
						bk_num = '".$report_info['bk_num']."',
						bk_name = '".$report_info['bk_name']."',
						bk_jumin = '".$report_info['bk_jumin']."'
					WHERE
						p_no = '".$p_no."' AND 
						a_no = '".$application_info['a_no']."'
					";
			echo "계좌 저장하기 : ".$query;
			$my_db->query($query);
		}
		
	//	echo $application_sql."==select_sql<br>";//////////////
	
		$blog_sql="select * from blog where blog_url = '".$blog_url."'";
		$blog_query=mysqli_query($my_db,$blog_sql);
		$blog_info=mysqli_fetch_array($blog_query);
		
	//		echo $blog_sql."==blog_sql<br>";//////////////
		
		//echo $application_sql."<br><br>";
		$sql="update report set blog_url='".$blog_url."', a_no = '".$application_info['a_no']."', b_no = '".$blog_info['b_no']."' where r_no='".$r_no."'";
	
	//	echo $sql."==sql<br>";//////////////
		mysqli_query($my_db,$sql);
	
		exit("<script>alert('블로거 정보를 수정 하였습니다');opener.parent.location.reload();window.close();</script>");
	}
	
	// 요일배열
	$datey=array('일','월','화','수','목','금','토');
	

	
	// 단순히 상태값을 텍스트 표현하기 위해 배열에 담은 정보
	$state=array(
					1=>'접수대기',
					2=>'접수완료',
					3=>'모집중',
					4=>'진행중',
					5=>'진행중단',
					6=>'마감'
				);
	$state1=array(
					0=>'진행중',
					1=>'진행중단',
				);
	
	// 목록에서 kind 값을 통해, 구분명과 수정페이지 url 앞단어를 가져옴
	$kind=array(
					'name'=>array(
						'',
						'체험단',
						'기자단',
						'배송체험'
					),
					'url'=>array(
						'',
						'consumer',
						'press',
						'delivery'
					)
				);
	
	
	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where="";
	$field=array();
	$keyword=array();
	$where=array();
	
	$search_blog=isset($_GET['search_blog_url'])?$_GET['search_blog_url']:"";
	
	if(!empty($search_blog)) {
		$field[]="search_blog_url";
		$keyword[]=$search_blog;
		$where[]="blog_url like '%".$search_blog."%'";
	}
	
	$search_p_no=isset($_GET['search_p_no'])?$_GET['search_p_no']:"";
	
	if($p_no != "" && $search_p_no == "") { // 맨처음 로드될경우 p_no 기준 리스팅
		$search_p_no = $p_no;
	}
	
	if(!empty($search_p_no)) {
		$field[]="search_p_no";
		$keyword[]=$search_p_no;
		$where[]="p_no ='".$search_p_no."'";
	}
	
	if(sizeof($where)>0) {
		$add_where=" where ";
		for($i=0;$i<sizeof($where);$i++) {
			$add_where.=$where[$i].(($i<(sizeof($where)-1))?" and ":"");
			$smarty->assign($field[$i],$keyword[$i]);
	//		echo "smarty->assign(".$field[$i].",".$keyword[$i].")<br>";/////////////
		}
	}
	
		
	$report_sql  = "select * from report where r_no = '".$r_no."'";
	$report_query= mysqli_query($my_db, $report_sql);
	$report_info = mysqli_fetch_array($report_query);
    $report_info['blog_url'] = str_replace('https','http', $report_info['blog_url']);
	$smarty->assign("report_info",$report_info);
	//echo $report_sql."==report_sql<br>";/////////////


    $add_orderby.=" p_no desc";

    // 페이에 따른 limit 설정
    $pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num = 20;
    $offset = ($pages-1) * $num;

	// 리스트 쿼리
	$application_sql="
					select *
					from 
						application 
					$add_where 
					order by $add_orderby
					";
	//echo $application_sql."==application_query<br>";/////////////

	// 전체 게시물 수
    $cnt_sql = "SELECT count(*) FROM (".$application_sql.") AS cnt";
    $cnt_query= mysqli_query($my_db, $cnt_sql);
    $cnt_data=mysqli_fetch_array($cnt_query);
    $total_num = $cnt_data[0];

    $smarty->assign(array(
	    "total_num"=>number_format($total_num)
    ));

    $smarty->assign("total_num",$total_num);
    $pagenum = ceil($total_num/$num);


    // 페이징
    if ($pages>=$pagenum){$pages=$pagenum;}
    if ($pages<=0){$pages=1;}
    	
	$search_url = "r_no=".$r_no."&search_p_no=".$search_p_no."&search_blog_url=".$search_blog;
    $smarty->assign("search_url",$search_url);
	$page=pagelist($pages, "blogger_edit.php", $pagenum, $search_url);
	$smarty->assign("pagelist",$page);
	

	// 리스트 쿼리 결과(데이터)
    $application_sql .= "LIMIT $offset,$num";
	$result= mysqli_query($my_db, $application_sql);
	while($application_array = mysqli_fetch_array($result)){
	
		$promotion_sql="select *,
								(select s.s_name from staff s where s.s_no=p.s_no) as staff_name,
								(select count(r.r_no) from report r where r.p_no=p.p_no) as report_num
								from 
									promotion p
								where p_no = '".$application_array['p_no']."'";
		$promotion_query= mysqli_query($my_db, $promotion_sql);
		$promotion_array = mysqli_fetch_array($promotion_query);
		//echo $promotion_sql."<br>";
		//print_r($promotion_array);
		if($promotion_array['posting_num']!=0)
			$posting_num = $promotion_array['posting_num'];
	
		if ($promotion_array['pres_reward'] != NULL)
				$pres_reward = date("n/d(".$datey[date("w",strtotime($promotion_array['pres_reward']))].")",strtotime($promotion_array['pres_reward']));
			else
				$pres_reward = "-";
	
		if (	$promotion_array['p_state1'] != 1)
				$state_code = $state[$promotion_array['p_state']];
			else
				$state_code = "진행중단";
		$applications[] = array(
			"cafe_id"=>$application_array['cafe_id'],
			"nick"=>$application_array['nick'],
			"username"=>$application_array['username'],
			"blog_url"=>$application_array['blog_url'],
			"kind"=>$promotion_array['kind'],
			"kind_name"=>$kind['name'][$promotion_array['kind']],
			"kind_url"=>$kind['url'][$application_array['kind']],
			"state_code"=>$state_code,
			"p_code"=>$promotion_array['promotion_code'],
			"c_name"=>$promotion_array['company'],
			"p_no"=>$promotion_array['p_no'],
			"staff_name"=>$promotion_array['staff_name'],
			"report_num"=>$promotion_array['report_num'],
			"posting_num"=>$posting_num,
			"reg_start"=>date("n/d(".$datey[date("w",strtotime($promotion_array['reg_sdate']))].")",strtotime($promotion_array['reg_sdate'])),
			"reg_end"=>date("n/d(".$datey[date("w",strtotime($promotion_array['reg_edate']))].")",strtotime($promotion_array['reg_edate'])),
			"awd_date"=>$promotion_array['awd_date'],
			"award"=>date("n/d(".$datey[date("w",strtotime($promotion_array['awd_date']))].")",strtotime($promotion_array['awd_date'])),
			"exp_edate"=>date("n/d(".$datey[date("w",strtotime($promotion_array['exp_edate']))].")",strtotime($promotion_array['exp_edate'])),
			"pres_reward"=>$pres_reward,
			"r_no"=>$r_no
		);
		$smarty->assign(array(
			"application"=>$applications
		));
	}
	$smarty->assign("p_code",$frm_zip2);
	$smarty->display('popup/blogger_edit.html');
	
}else{ // 접근 권한이 없는경우
	$smarty->display('access_error.html');
}
?>