<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/data_state.php');
require ('../libs/simple_html_dom.php');

$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);


$proc=(isset($_POST['process']))?$_POST['process']:"";
$idx=(isset($_GET['no']))?$_GET['no']:"";
$smarty->assign("p_no",$idx);

/* 캠페인정보 가져오기 Start */
$promotion_sql="SELECT p.company,
											p.channel,
											p.kind
									FROM promotion p where p.p_no='".$idx."'";
$promotion_result=mysqli_query($my_db,$promotion_sql);
$promotion = mysqli_fetch_array($promotion_result);

// channel name
$channel_name="";
for($i=0 ; $i < sizeof($promotion_channel) ; $i++){
	if($promotion['channel'] == $promotion_channel[$i][1]){
		$channel_name = $promotion_channel[$i][0];
	}
}

// kind name
$kind_name="";
for($i=0 ; $i < sizeof($promotion_kind) ; $i++){
	if($promotion['kind'] == $promotion_kind[$i][1]){
		$kind_name = $promotion_kind[$i][0];
	}
}

$smarty->assign(
	array(
		"company"=>$promotion['company'],
		"channel"=>$promotion['channel'],
		"channel_name"=>$channel_name,
		"kind"=>$promotion['kind'],
		"kind_name"=>$kind_name
	)
);
/* 캠페인정보 가져오기 End */

//$sql="select count(*) from report where p_no='".$idx."'";
$sql="select count(*) from application where p_no='".$idx."' and (a_state='1' or a_state='3')";
$query=mysqli_query($my_db,$sql);
$data=mysqli_fetch_array($query);
$total_num = $data[0];
$smarty->assign("total_num",$data[0]);

// 페이징
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
$totalnum = $total_num;
$num = 20;
$url = "no=".$idx;

$pagenum = ceil($totalnum/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$page=pagelist($pages, "popup_posting_list.php", $pagenum, $url);
$smarty->assign("pagelist",$page);//
$noi = $totalnum - ($pages-1)*$num;
$offset = ($pages-1) * $num;
$smarty->assign("totalnum",$totalnum);

/* c_no로 업체명 가져오기 Start */
$company_sql="select c_name from company where c_no='".$promotion_info['c_no']."'";
$company_query=mysqli_query($my_db,$company_sql);
$company_data=mysqli_fetch_array($company_query); //$company_data['c_name']
$smarty->assign("company_name",$company_data['c_name']);
/* c_no로 업체명 가져오기 End */

//echo "proc = ".$proc."<br><br>";
if($proc=="reload") {
//	print_r($_POST);
	$s_state = $_POST['sstate'];
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['b_no']))?$_POST['b_no']:"";
	$sql="update blog set reload='".$nowdate."' where b_no='".$mno."'";
	//exit("<hr>".$sql);
	//echo $sql;exit;

	mysqli_query($my_db,$sql);

	exit("<script>alert('재평가를 완료하였습니다');location.href='popup_posting_list.php".((!empty($qstr))?"?".$qstr:"")."#tr_{$a_no}';</script>");
}

elseif($proc=="memo") {
		//print_r($_POST);

		$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
		$p_no=(isset($_POST['p_no']))?$_POST['p_no']:"";
		$mno=(isset($_POST['a_no']))?$_POST['a_no']:"";
		$mno1=(isset($_POST['b_no']))?$_POST['b_no']:"";
		$mtxt=(isset($_POST['b_memo'.$mno]))?addslashes($_POST['b_memo'.$mno]):"";
		$sql="update blog set b_memo='".$mtxt."' where b_no='".$mno1."'";
		//exit("<hr>".$sql);
		//echo "<br><br>".$sql;exit;


		if(!mysqli_query($my_db,$sql)){
			echo ("<script>alert('메모를 등록에 실패 하였습니다');</script>");
		}else{
			echo ("<script>alert('메모를 등록하였습니다');</script>");
		}

		/* 페이지 이동 없게 하기 위함 20160308 김윤영 Start */
		exit ("<script>location.href='popup_posting_list.php?no={$p_no}#tr_{$a_no}';</script>");

		/* 페이지 이동 없게 하기 위함 20160308 김윤영 backup
		exit("<script>alert('메모를 등록하였습니다');location.href='popup_posting_list.php".((!empty($qstr))?"?".$qstr:"")."';</script>");

		페이지 이동 없게 하기 위함 20160308 김윤영 End */
	}

elseif($proc=="limit_yn") {
		//print_r($_POST);
		$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
		$p_no=(isset($_POST['p_no']))?$_POST['p_no']:"";
		$mno=(isset($_POST['a_no']))?$_POST['a_no']:"";
		$mtxt=(isset($_POST['sstate'.$mno]))?addslashes($_POST['sstate'.$mno]):"";
		if ($mtxt != '3')
			$sql="update application set limit_yn='".$mtxt."' where a_no='".$mno."'";
		else
			$sql="update application set limit_yn='3', a_state = '3' where a_no='".$mno."'";
		//exit("<hr>".$sql);

		if(!mysqli_query($my_db,$sql)){
			echo ("<script>alert('마감준수여부 등록에 실패 하였습니다');</script>");
		}else{
			echo ("<script>alert('마감준수여부를 등록 등록하였습니다');</script>");
		}

		exit("<script>location.href='popup_posting_list.php".((!empty($qstr))?"?".$qstr:"")."#tr_{$a_no}';</script>");

	}

/* 미선정 블로거 탈락으로 일괄변경 20160325 김윤영 Start */
elseif($proc=="save_limit_yn") {
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['p_no']))?$_POST['p_no']:"";
	$sql="update application set limit_yn='1' where p_no='".$mno."' and a_state='1' and ((limit_yn='' OR limit_yn IS NULL) OR limit_yn='2')";

	mysqli_query($my_db,$sql);
	exit("<script>alert('마감준수여부에서 선택을 모두 준수로 변경 하였습니다');location.href='popup_posting_list.php".((!empty($qstr))?"?".$qstr:"")."#tr_{$a_no}';</script>");


}
/* 미선정 블로거 탈락으로 일괄변경 20160325 김윤영 End */

elseif($proc=="save_report") {
	//print_r($_POST);
	$qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno=(isset($_POST['save_no']))?$_POST['save_no']:"";
	$mtxt=(isset($_POST['memo']))?$_POST['memo']:"";
	$a_no=(isset($_POST['a_no']))?$_POST['a_no']:"";
	$b_no=(isset($_POST['b_no']))?$_POST['b_no']:"";
	$p_no=(isset($_POST['p_no']))?$_POST['p_no']:"";
	$r_no=(isset($_POST['r_no']))?$_POST['r_no']:"";
	$blog_url_origin=(isset($_POST['blog_url_origin']))?addslashes($_POST['blog_url_origin']):"";
	if($r_no == "r_no"){
		$post_title = addslashes($_POST['post_title'.$a_no]);
		$post_url = addslashes($_POST['post_url'.$a_no]);
	}else{
		$post_title = addslashes($_POST['post_title'.$r_no]);
		$post_url = addslashes($_POST['post_url'.$r_no]);

		if ($post_title == '' && $post_url == ''){ // 빈칸 저장 시 삭제
			$delete_sql="delete from report where r_no='".$r_no."'";

			if (!mysqli_query($my_db, $delete_sql)){
				exit("<script>alert('$r_no 삭제처리에 실패 하였습니다');history.back();</script>");
			}else{
			}
			exit ("<script>location.href='popup_posting_list.php?no={$p_no}#tr_{$a_no}';</script>");
		}
	}


	if ($r_no != "r_no" && $post_title !='' && $post_url!=''){ // 수정시 변경
		$sql="update report set post_title='".$post_title."', post_url='".$post_url."', blog_url='".$blog_url_origin."' where r_no='".$r_no."'";
		//echo "<br><br><br><br>".$sql;exit;

		mysqli_query($my_db,$sql);
	} else if ($r_no == "r_no" && $post_title !='' && $post_url!='') {
		$select_sql="select promotion_code, c_no, s_no from promotion where p_no='".$p_no."'";
		//echo "<br><br><br><br><br><br>sql = ".$sql;
		$query=mysqli_query($my_db,$select_sql);
		$promotion_info=mysqli_fetch_array($query);

		$sql="insert into report set
					p_no = '".$p_no."',
					p_code = '".$promotion_info['promotion_code']."',
					c_no = '".$promotion_info['c_no']."',
					b_no = '".$b_no."',
					s_no = '".$promotion_info['s_no']."',
					a_no = '".$a_no."',
					post_url = '".$post_url."',
					post_title = '".$post_title."',
					blog_url = '".$blog_url_origin."',
					r_datetime = '".date("Y-m-d H:i:s")."',
					r_ip = '".ip2long($_SERVER['REMOTE_ADDR'])."'
		";
		//echo "<br><br><br><br>".$sql;exit;
		mysqli_query($my_db,$sql);
	}

	//echo "<br>".$sql;exit;
	if(!$b_no)
		exit("<script>alert('블로거 등록을 먼저 하세요.');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");

	echo ("<script>alert('포스팅을 저장하였습니다');</script>");
	exit ("<script>location.href='popup_posting_list.php?no={$p_no}#tr_{$a_no}';</script>");

}
elseif($proc=="save_selection") {

	//print_r($_POST);
	$p_no=(isset($_POST['p_no']))?$_POST['p_no']:"";
	$a_no=(isset($_POST['a_no']))?$_POST['a_no']:"";
	$sql="update application set a_state='".$_POST['selection'.$a_no]."' where a_no='".$a_no."'";
	//echo $sql;exit;

	mysqli_query($my_db,$sql);
	if(!mysqli_query($my_db,$sql)){
		exit("<script>alert('선정상태 저장에 실패 하였습니다');history.back();</script>");
	}else{
		echo ("<script>alert('선정상태를 저장 하였습니다');</script>");
		exit ("<script>location.href='popup_posting_list.php?no={$p_no}#tr_{$a_no}';</script>");
	}

}

// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query",$save_query);

$a_state_text=array(1=>'선정',2=>'탈락',3=>'선정취소');
foreach($a_state_text as $key => $value) {
	$state_stmt[]=array
		(
			"no" 	=> $key,
			"name"	=> $value
		);
	$smarty->assign("select",$state_stmt);
}


$add_orderby = "ORDER BY a.a_state ASC, a.real_visit_num DESC, blog_id ASC";
if($promotion['channel'] == '2'){
    $add_orderby = "ORDER BY a.a_state ASC, a.visit_num DESC, blog_id ASC";
}

$sql="
	SELECT
		a.a_no,
		a.p_no,
		a.c_no,
		a.nick,
		a.blog_url,
		a.bk_title,
		a.bk_num,
		a.bk_name,
		a.memo,
		a.a_state,
		a.limit_yn,
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(a.blog_url,'http://',''),'https://',''),'www.',''),'instagram.com/',''),'blog.naver.com/',''),'.blog.me',''),'/','') as blog_id
	FROM application a
	WHERE a.p_no = '".$idx."' AND ( a.a_state='1' OR a.a_state='3' )
	{$add_orderby}
";

$idx_no=1;
$query=mysqli_query($my_db,$sql);
while($data = mysqli_fetch_array($query))
{
	$blog_sql = "select b_no,b_memo,nick,blog_url,reload from blog where blog_url='".$data['blog_url']."'";
	$blog_query=mysqli_query($my_db,$blog_sql);
	$blog_info=mysqli_fetch_array($blog_query);

	$report_sql = "select * from report where p_no='".$idx."' and a_no='".$data['a_no']."'";
	$report_query=mysqli_query($my_db,$report_sql);
	while($report_info=mysqli_fetch_array($report_query))
	{
		$blog_id 	 	= $post_id = "";
		$blog_txt_cnt  	= $blog_img_cnt = 0;
		$file_exist		= true;

		if(!empty($report_info['file_path']))
		{
			$file_paths = explode(",", $report_info['file_path']);
			foreach($file_paths as $file_path_val){
				$file_path = "../uploads/{$file_path_val}";
				if(!file_exists($file_path)){
					$file_exist = false;
				}
			}
		}

		if(($promotion['channel'] == '1' || $promotion['channel'] == '7') && $report_info['post_url'])
		{
			$post_url = $report_info['post_url'];
			$post_url = str_replace('http://','',$post_url);
			$post_url = str_replace('https://','',$post_url);
			if(strpos($post_url, '.me') !== false){
				$post_id_list = explode('/', $post_url);
				$blog_id_list = explode('.', $post_url);
				$blog_id = isset($blog_id_list[0]) ? $blog_id_list[0] : "";
				$post_id = isset($post_id_list[1]) ? $post_id_list[1] : "";
			}else{
				$blog_id_list = explode('/', $post_url);
				$blog_id = isset($blog_id_list[1]) ? $blog_id_list[1] : "";
				$post_id = isset($blog_id_list[2]) ? $blog_id_list[2] : "";
			}

			$is_blog = false;
			if(strpos($post_url, "blog") !== false){
				$is_blog = true;
			}

			if($blog_id && $post_id && $is_blog)
			{
				$blog_url = "https://blog.naver.com/PostView.nhn?blogId={$blog_id}&logNo={$post_id}";
				$blog_id_check = "post-view{$post_id}";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $blog_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$blog_str = curl_exec($ch);
				curl_close($ch);

				$html = new simple_html_dom();
				$html->load($blog_str);

				$error_chk = $html->find('div[id=error_content]');
				if(!$error_chk)
				{
					$blog_id_chk_html = $html->find("div[id={$blog_id_check}]", 0);

					if($blog_id_chk_html)
					{
						$blog_html = $html->find('div[class=se-main-container]', 0);
						if ($blog_html) {
							$blog_img = $blog_html->find('img[class=se-image-resource]');
							//$blog_text_wrap = $blog_html->find('div[class=se-module se-module-text]');
							$blog_text_span = $blog_html->find('span');
							$blog_img_cnt = count($blog_img);

							if ($blog_text_span) {
								$blog_txt_cnt = calculateBlogTxtCnt($blog_text_span);
							}
						} else {
							$blog_html = $html->find('div[id=postViewArea]', 0);
							if($blog_html){
                                $blog_img = $blog_html->find('img[class=_photoImage]');
                                $blog_p_text = $blog_html->find('p');
                                $blog_div_text = $blog_id_chk_html->find('div');
                                $blog_img_cnt = count($blog_img);
                                $blog_txt_cnt = calculateBlogTxtCnt($blog_p_text);
                                $blog_txt_cnt += calculateBlogTxtCnt($blog_div_text);
							}
						}
					}
				}
			}
		}

		$report_data[]=array
		(
			"r_no"			=> $report_info['r_no'],
			"a_no"			=> $data['a_no'],
			"post_title"	=> htmlspecialchars($report_info['post_title']),
			"post_url"		=> $report_info['post_url'],
			"r_memo"		=> $report_info['memo'],
			"blog_txt_cnt"	=> $blog_txt_cnt,
			"blog_img_cnt"	=> $blog_img_cnt,
			"file_exist"  	=> $file_exist,
			"file_path"  	=> $report_info['file_path']
		);
	}
	$smarty->assign("report", $report_data);
	$stmt[]=array
		(
			"idx_no"	=> $idx_no,
			"kind"		=> $promotion_info['kind'],
			"b_no"		=> $blog_info['b_no'],
			"p_no"		=> $data['p_no'],
			"a_no"		=> $data['a_no'],
			"r_no"		=> $report_data['r_no'],
			"c_no"		=> $data['c_no'],
			"nick"		=> $data['nick'],
			"blog_url"	=> $data['blog_url'],
			"bk_title"	=> $data['bk_title'],
			"bk_num"	=> $data['bk_num'],
			"bk_name"	=> $data['bk_name'],
			"b_memo"	=> $blog_info['b_memo'],
			"comment"	=> $data['memo'],
			"a_state"	=> $data['a_state'],
			"sstate"	=> $data['limit_yn'],
		"reload"=>((empty($blog_info['reload']))?"____/__/__":date("Y/m/d H:i:s",strtotime($blog_info['reload']))),
		);
	$idx_no++;
}

$smarty->assign("posting", $stmt);
$smarty->display('popup/popup_posting_list.html');

function calculateBlogTxtCnt($blog_text)
{
	$blog_txt_cnt = 0;
	foreach ($blog_text as $txt)
	{
		$str = trim($txt->plaintext);
		$str = str_replace(' ', '', $str);
		$str = str_replace('&nbsp;', '', $str);
		$str = str_replace('&#8203;', '', $str);
		$str = str_replace('&nbsp', '', $str);
		$str = str_replace('\r\n', '', $str);
		$str = str_replace('\n', '', $str);

		if (!empty($str)) {
			$blog_txt_cnt += mb_strlen($str, 'utf8');
		}
	}

	return $blog_txt_cnt;
}
?>
