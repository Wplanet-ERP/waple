<?php
require('../inc/common.php');
require('../inc/data_state.php');
require('../ckadmin.php');

if (permissionCheck($session_permission, "011011"))
{

    $nowdate = date("Y-m-d H:i:s");
    $smarty->assign("nowdate",$nowdate);

    $proc=(isset($_POST['process']))?$_POST['process']:"";
    $idx=(isset($_GET['no']))?$_GET['no']:"";
    $blog_url=(isset($_GET['blog_url']))?$_GET['blog_url']:"";

    if (!empty($blog_url))
        $smarty->assign("blog_url",$blog_url);

    // 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
    $add_where =" WHERE blog_url='".$blog_url."'";

    //Blog_url로 제출한 신청 횟수 및 선정, 취소 조회
    $application_sql="
        SELECT count(*) as apply_count,
            (select count(*) from application a where a.blog_url='".$blog_url."' and a.a_state='1') as select_count,
            (select count(*) from application a where a.blog_url='".$blog_url."' and a.a_state='3') as cancel_count,
            (select count(*) from application a where a.blog_url='".$blog_url."' and a.limit_yn='2') as limit_count
        FROM application
        {$add_where}
    ";

    $idx_no=1;
    $application_query=mysqli_query($my_db,$application_sql);
    $application_data=mysqli_fetch_array($application_query);

    if ($application_data['apply_count'] > 0)
        $select_percent=($application_data['select_count']/$application_data['apply_count'])*100;
    else
        $select_percent = '0';

    $a_state_text=array(1=>'선정',2=>'탈락',3=>'선정취소');

    foreach($a_state_text as $key => $value) {
        $state_stmt[]=array
        (
            "no"=>$key,
            "name"=>$value
        );
        $smarty->assign("select",$state_stmt);
    }

    $smarty->assign("apply_count",$application_data['apply_count']);
    $smarty->assign("select_count",$application_data['select_count']);
    $smarty->assign("select_percent",$select_percent);
    $smarty->assign("cancel_count",$application_data['cancel_count']);

    //Blog_url로 Blog_no 추출
    $blog_info_sql = "
				SELECT b_no as b_no,
				b_memo as b_memo,
				reload as reload
				FROM blog
				where blog_url = '".$blog_url."'
	";
    $blog_info_query=mysqli_query($my_db,$blog_info_sql);
    $blog_info_data=mysqli_fetch_array($blog_info_query);
    $smarty->assign("b_no",$blog_info_data['b_no']);
    $smarty->assign("b_memo",$blog_info_data['b_memo']);
    $reload=((empty($blog_info_data['reload']))?"____/__/__":date("Y/m/d H:i:s",strtotime($blog_info_data['reload'])));
    $smarty->assign("reload",$reload);

    //추출한 Blog_no로 제출한 완료 보고 정보 조회
    $total_report_sql = "SELECT count(*)  as total_report_count FROM report where b_no= '".$blog_info_data['b_no']."'";
    $total_report_query=mysqli_query($my_db,$total_report_sql);
    $total_report_data=mysqli_fetch_array($total_report_query);

    $report_sql = "
								select count(*)
									from
										application
									where
									limit_yn = '2'
									and a_no='".$data['a_no']."'";
    $report_query=mysqli_query($my_db,$report_sql);
    $report=mysqli_fetch_array($report_query);

    if ($application_data['limit_count'] > 0 && $application_data['apply_count'] > 0)
        $report_percent=($application_data['limit_count']/$application_data['apply_count'])*100;
    else
        $report_percent = '0';

    $smarty->assign("report_count",$application_data['limit_count']);
    $smarty->assign("total_report_count",$total_report_data['total_report_count']);
    $smarty->assign("report_percent",$report_percent);


    # 해당 Blogger 신청 리스트

    $sch_c_name    = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
    $sch_gubun    = isset($_GET['sch_gubun']) ? $_GET['sch_gubun'] : "";

    if(!empty($sch_c_name))
    {
        $add_where .= " AND c_no IN(SELECT c.c_no FROM company c WHERE c.c_name like '%{$sch_c_name}%')";
        $smarty->assign('sch_c_name', $sch_c_name);
    }

    if(!empty($sch_gubun))
    {
        $add_where .= " AND kind='{$sch_gubun}'";
        $smarty->assign('sch_gubun', $sch_gubun);
    }

    $sql    = "select count(*) from application {$add_where}";
    $query  = mysqli_query($my_db,$sql);
    $data   = mysqli_fetch_array($query);
    $total_num = $data[0];
    $smarty->assign("total_num",$data[0]);

    // 페이징
    $pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
    $totalnum = $total_num;
    $num = 20;

    $pagenum = ceil($totalnum/$num);

    if ($pages>=$pagenum){$pages=$pagenum;}
    if ($pages<=0){$pages=1;}

    $search_url = getenv("QUERY_STRING");
    $page=pagelist($pages, "blogger_detail.php", $pagenum, $search_url);
    $smarty->assign("pagelist",$page);//
    $noi = $totalnum - ($pages-1)*$num;
    $offset = ($pages-1) * $num;
    $smarty->assign("totalnum",$totalnum);
    $smarty->assign("search_url",$search_url);

    if($proc=="reload") {
        $qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
        $mno=(isset($_POST['b_no']))?$_POST['b_no']:"";
        $sql="update blog set reload='".$nowdate."' where b_no='".$mno."'";

        mysqli_query($my_db,$sql);

        exit("<script>alert('재평가를 완료하였습니다');location.href='blogger_detail.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
    }


    if($proc=="memo") {
        $qstr=(isset($_POST['list_query']))?$_POST['list_query']:"";
        $mno=(isset($_POST['b_no']))?$_POST['b_no']:"";
        $mtxt=(isset($_POST['b_memo']))?$_POST['b_memo']:"";
        $sql="update blog set b_memo='".addslashes($mtxt)."' where b_no='".$mno."'";
        mysqli_query($my_db,$sql);

        exit("<script>alert('메모를 등록하였습니다');location.href='blogger_detail.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
    }

    // 리스트 페이지 쿼리 저장
    $save_query=http_build_query($_GET);
    $smarty->assign("save_query",$save_query);

    $a_state_text=array(1=>'선정',2=>'탈락',3=>'선정취소');
    foreach($a_state_text as $key => $value) {
        $state_stmt[]=array
        (
            "no"=>$key,
            "name"=>$value
        );
        $smarty->assign("select",$state_stmt);
    }


    $sql = "
		SELECT	
			* 
		FROM application
		{$add_where}
		ORDER BY a_no DESC
		LIMIT {$offset},{$num}
	";

    $idx_no=1;
    $query=mysqli_query($my_db,$sql);
    while($data=mysqli_fetch_array($query))
    {
        $promotion_sql = " select * from promotion where p_no = '".$data['p_no']."'";
        $promotion_query=mysqli_query($my_db,$promotion_sql);
        $promotion_data=mysqli_fetch_array($promotion_query);

        $report_sql   = " select * from report where p_no = '".$data['p_no']."' and a_no='".$data['a_no']."'";
        $report_query = mysqli_query($my_db,$report_sql);
        while($report_info=mysqli_fetch_array($report_query)){
            $report_data[]=array
            (
                "r_no"=>$report_info['r_no'],
                "a_no"=>$data['a_no'],
                "post_title"=>$report_info['post_title'],
                "post_url"=>$report_info['post_url'],
                "r_memo"=>nl2br($report_info['memo']),
            );
        }
        $smarty->assign("report",$report_data);
        $hp = explode("-",$data['hp']);
        $email = explode("@",$data['email']);


        switch ($data['a_state']){
            case 1:
                $a_state_name = '선정';
                break;
            case 2:
                $a_state_name = '탈락';
                break;
            case 3:
                $a_state_name = '선정취소';
                break;
            default :
                $a_state_name = '-';
                break;
        }

        $limit_yn_name = "";
        switch ($data['limit_yn']){
            case 1:
                $limit_yn_name = '마감 준수';
                break;
            case 2:
                $limit_yn_name = '마감 미준수';
                break;
        }

        /* c_no로 업체명 가져오기 Start */
        $company_sql   = "select c_name from company where c_no='".$promotion_data['c_no']."'";
        $company_query = mysqli_query($my_db,$company_sql);
        $company_data  = mysqli_fetch_array($company_query);
        /* c_no로 업체명 가져오기 End */

        $stmt[]=array
        (
            "idx_no"=>$idx_no,
            "a_no"=>$data['a_no'],
            "p_no"=>$data['p_no'],
            "blog_url"=>$blog_url,
            "regdate"=>$data['regdate'],
            "date_ymd"=>date("Y.m.d",strtotime($data['regdate'])),
            "date_hi"=>date("H:i",strtotime($data['regdate'])),
            "nick"=>$data['nick'],
            "username"=>$data['username'],
            "hp"=>$data['hp'],
            "email"=>$data['email'],
            "cafe_id"=>$data['cafe_id'],
            "kind"=>$data['kind'],
            "kind_name"=>$promotion_kind[$data['kind']][0],
            "promotion_code"=>$promotion_data['promotion_code'],
            "company"=>$company_data['c_name'],
            "staff_name"=>$promotion_data['name'],
            "short_a_memo"=>stringCut($data['memo'],60),
            "a_memo"=>nl2br($data['memo']),
            "a_state_name"=>$a_state_name,
            "keyword"=>$promotion_data['keyword_subject'],
            "post_title"=>$report_data['post_title'],
            "post_url"=>$report_data['post_url'],
            "refund_count"=>$data['refund_count'],
            "bk_jumin"=>$data['bk_jumin'],
            "bk_title"=>$data['bk_title'],
            "bk_num"=>$data['bk_num'],
            "bk_name"=>$data['bk_name'],
            "limit_yn_name"=>$limit_yn_name,
            "pres_money"=>$promotion_data['pres_money'],
            "pres_num"=>$promotion_data['posting_num']/$promotion_data['reg_num'],
            "refund_count"=>$data['refund_count'],
        );
        $smarty->assign("blogger",$stmt);
        $idx_no++;
    }

    $smarty->display('popup/blogger_detail.html');

}else{ // 접근 권한이 없는경우
    $smarty->display('access_error.html');
}
?>