<?php
require('../inc/common.php');
require('../inc/data_state.php');
require('../ckadmin.php');

// 접근 권한
if (!$session_s_no){
	$smarty->display('access_error.html');
	exit;
}

$proc=(isset($_POST['process']))?$_POST['process']:"";
// 입금리스트 설정
if($proc=="save_deposit") {
	$dp_no=(isset($_POST['dp_no']))?$_POST['dp_no']:"";
	$w_no=(isset($_POST['w_no']))?$_POST['w_no']:"";

	$work_sql = "UPDATE `work` w SET
										w.dp_no = '{$dp_no}',
										w.dp_price = (SELECT dp.dp_money FROM deposit dp WHERE dp.dp_no = '{$dp_no}'),
										w.dp_price_vat = (SELECT dp_money_vat FROM deposit dp WHERE dp.dp_no = '{$dp_no}'),
										w.dp_c_name = (SELECT dp.c_name FROM deposit dp WHERE dp.dp_no = '{$dp_no}'),
										w.dp_c_no = (SELECT dp.c_no FROM deposit dp WHERE dp.dp_no = '{$dp_no}')
								WHERE w.w_no = '{$w_no}'";
	//echo $work_sql; exit;

	if(!mysqli_query($my_db,$work_sql)){
		exit("<script>alert('입금리스트를 설정 하는데 실패 하였습니다');history.back();</script>");
	}else{
		$deposit_sql = "UPDATE `deposit` dp SET dp.w_no = NULL WHERE dp.w_no = '{$w_no}';"; // 입금리스트에 업무설정이 있는경우 해지
		//echo $deposit_sql; exit;
		if(!mysqli_query($my_db,$deposit_sql)){
			exit("<script>alert('기존 설정된 입금리스트 해지가 실패 하였습니다');history.back();</script>");
		}else{
			$deposit_sql = "UPDATE `deposit` dp SET dp.w_no = '{$w_no}' WHERE dp.dp_no = '{$dp_no}';";
			//echo $deposit_sql; exit;
			if(!mysqli_query($my_db,$deposit_sql)){
				exit("<script>alert('입금리스트에 업무 번호를 저장 하는데 실패 하였습니다');history.back();</script>");
			}else{
				exit("<script>opener.location.reload(); window.close();</script>");
			}
		}
	}

}else{
	// GET : opener로 반환할 form name 저장
	$f_name_get=isset($_GET['f_name'])?$_GET['f_name']:"";
	$c_name_get=isset($_GET['c_name'])?$_GET['c_name']:"";
	$dp_no_get=isset($_GET['dp_no'])?$_GET['dp_no']:"";
	$smarty->assign("f_name",$f_name_get);
	$smarty->assign("c_name",$c_name_get);

	// 직원가져오기 배열
	$add_where_permission = str_replace("0", "_", $permission_name['마케터']);
	if(permissionNameCheck($session_permission,"재무관리자") || permissionNameCheck($session_permission,"대표") || permissionNameCheck($session_permission,"마스터관리자") || permissionNameCheck($session_permission,"인큐베이팅") || permissionNameCheck($session_permission,"마케터")){ // 재무관리자, 대표, 마스터관리자, 인큐베이팅본부, 마케터 는 모두 조회 가능
		$staff_sql="select s_no,s_name from staff where staff_state < '3' AND permission like '".$add_where_permission."' AND staff_state = '1'";
		$staff_query=mysqli_query($my_db,$staff_sql);

		while($staff_data=mysqli_fetch_array($staff_query)) {
			$staffs[]=array(
				'no'=>$staff_data['s_no'],
				'name'=>$staff_data['s_name']
			);
		}
	}

	$staffs[]=array(
		'no'=>"null",
		'name'=>"알수없음"
	);

	$smarty->assign("staff",$staffs);

	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where = " 1 = 1";

	$sch_dp_quick_get = isset($_GET['sch_dp_quick']) ? $_GET['sch_dp_quick'] : "";
	$sch_get=isset($_GET['sch'])?$_GET['sch']:"Y";
	$sch_dp_state_get = isset($_GET['sch_dp_state']) ? $_GET['sch_dp_state'] : "";
	$sch_dp_method_get = isset($_GET['sch_dp_method']) ? $_GET['sch_dp_method'] : "";

	$sch_dp_no_get = isset($_GET['sch_dp_no']) ? $_GET['sch_dp_no'] : "";
	$sch_dp_date_get = isset($_GET['sch_dp_date']) ? $_GET['sch_dp_date'] : "";
	$sch_dp_tax_state_get = isset($_GET['sch_dp_tax_state']) ? $_GET['sch_dp_tax_state'] : "";
	$sch_incentive_date_get = isset($_GET['sch_incentive_date']) ? $_GET['sch_incentive_date'] : "";
	$sch_s_no_get = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
	$sch_dp_etc_get = isset($_GET['sch_dp_etc']) ? $_GET['sch_dp_etc'] : "";
	$sch_my_c_no_get = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
	$sch_prd_no_get=isset($_GET['sch_prd_no'])?$_GET['sch_prd_no']:"";
	$sch_c_name_get = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
	$sch_subject_get = isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
	$w_no_get = isset($_GET['w_no']) ? $_GET['w_no'] : "";
	$smarty->assign("w_no",$w_no_get);

	$smarty->assign("sch_dp_quick", $sch_dp_quick_get);

	if(!empty($sch_get)) { // 상세검색 펼치기
		$smarty->assign("sch",$sch_get);
	}

	if (!empty($sch_dp_no_get)) {
		$add_where .= " AND dp.dp_no='" . $sch_dp_no_get . "'";
		$smarty->assign("sch_dp_no", $sch_dp_no_get);
	}

	if (!empty($sch_dp_state_get)) {
		$add_where .= " AND dp.dp_state='" . $sch_dp_state_get . "'";
		$smarty->assign("sch_dp_state", $sch_dp_state_get);
	}

	if (!empty($sch_dp_method_get)) {
		$add_where .= " AND dp.dp_method='" . $sch_dp_method_get . "'";
		$smarty->assign("sch_dp_method", $sch_dp_method_get);
	}

	if (!empty($sch_dp_date_get)) {
		$add_where .= " AND dp.dp_date='" . $sch_dp_date_get . "'";
		$smarty->assign("sch_dp_date", $sch_dp_date_get);
	}

	if (!empty($sch_dp_tax_state_get)) {
		$add_where .= " AND dp.dp_tax_state='" . $sch_dp_tax_state_get . "'";
		$smarty->assign("sch_dp_tax_state", $sch_dp_tax_state_get);
	}

	if (!empty($sch_incentive_date_get)) {
		$add_where .= " AND dp.incentive_date='" . $sch_incentive_date_get . "'";
		$smarty->assign("sch_incentive_date", $sch_incentive_date_get);
	}

	if (!empty($sch_s_no_get)) {
		if ($sch_s_no_get != "all") {
			$add_where .= " AND (s_no = '" . $sch_s_no_get . "' or s_no = 0) ";
		}
		$smarty->assign("sch_s_no",$sch_s_no_get);
	} else if (empty($sch_s_no_get) && !permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "외주관리자") && permissionNameCheck($session_permission, "마케터")){
		$add_where .= " AND (s_no = '" . $session_s_no . "' or s_no = 0) ";
		$smarty->assign("sch_s_no",$session_s_no);
	}

	if (!empty($sch_dp_etc_get)) {
		if ($sch_dp_etc_get === "1") {
			$add_where .= " AND (dp.incentive_date is null or dp.incentive_date = '') ";
		} else if ($sch_dp_etc_get === "2") {
			$add_where .= " AND dp.c_no in (SELECT c_no FROM company WHERE o_registration = '' OR o_registration IS NULL OR tx_company = '' OR tx_company IS NULL OR tx_company_number <= '' OR tx_company_number IS NULL OR tx_company_ceo <= '' OR tx_company_ceo IS NULL) ";
		}
		$smarty->assign("sch_dp_etc", $sch_dp_etc_get);
	}

	if (!empty($sch_my_c_no_get)) {
		$add_where .= " AND dp.my_c_no = '" . $sch_my_c_no_get . "'";
		$smarty->assign("sch_my_c_no", $sch_my_c_no_get);
	}

	if(!empty($sch_prd_no_get)) { // 상품
		$add_where.=" AND (SELECT w.prd_no FROM work w WHERE w.w_no=dp.w_no)='".$sch_prd_no_get."'";
		$smarty->assign("sch_prd_no",$sch_prd_no_get);

		for($i=0 ; $i < count($out_product) ; $i++){
			if($out_product[$i][1] == $sch_prd_no_get){
				$smarty->assign("sch_prd_no_value",$out_product[$i][0]);
			}
		}
	}

	if (!empty($sch_c_name_get)) {
		$add_where .= " AND dp.c_name like '%" . $sch_c_name_get . "%'";
		$smarty->assign("sch_c_name", $sch_c_name_get);
	}

	if (!empty($sch_subject_get)) {
		$add_where .= " AND dp.dp_subject like '%" . $sch_subject_get . "%'";
		$smarty->assign("sch_subject", $sch_subject_get);
	}

	if ($sch_dp_state_get == "2")
		$add_orderby = " dp.dp_date desc ";
	else {
		if (permissionNameCheck($session_permission, "마케터"))
			$add_orderby = " case when s_no = 0 then 99999999 + dp_no else dp_no end desc ";
		else
			$add_orderby = " dp.dp_no desc ";
	}

	// 페이에 따른 limit 설정
	$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num = 20;
	$offset = ($pages-1) * $num;

	// 리스트 쿼리
	$deposit_sql = "
		SELECT
			dp.dp_no,
			dp.incentive_date,
			dp.s_no,
			(SELECT s_name FROM staff s WHERE s.s_no=dp.s_no) AS s_name,
			dp.w_no,
			(SELECT title FROM product prd WHERE prd.prd_no=(SELECT w.prd_no FROM work w WHERE w.w_no = dp.w_no)) AS product_title,
			(SELECT w.prd_no FROM work w WHERE w.w_no = dp.w_no) AS prd_no,
			dp.c_no,
			dp.c_name,
			dp.dp_state,
			dp.dp_method,
			dp.dp_date,
			(SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=dp.my_c_no) AS my_c_name,
			(SELECT mc.kind_color FROM my_company mc WHERE mc.my_c_no=dp.my_c_no) AS my_c_kind_color,
			dp.dp_subject,
			dp.cost,
			dp.dp_money,
			dp.dp_money_vat,
			dp.dp_tax_state,
			dp.dp_tax_date,
			dp.dp_tax_item,
			dp.regdate,
			(SELECT s_name FROM staff s WHERE s.s_no=dp.reg_s_no) AS reg_s_name,
			(select concat(o_registration, '||', tx_company, '||', tx_company_number, '||', tx_company_ceo) from company where c_no = dp.c_no) tx_info
		FROM deposit dp
		WHERE $add_where
					AND display='1'
		ORDER BY $add_orderby
	";

	//echo $deposit_sql; //exit;

	// 전체 게시물 수
	$cnt_sql = "SELECT count(*) FROM (".$deposit_sql.") AS cnt";
	$result= mysqli_query($my_db, $cnt_sql);
	$cnt_data=mysqli_fetch_array($result);
	$total_num = $cnt_data[0];

	$smarty->assign(array(
		"total_num"=>number_format($total_num)
	));

	$smarty->assign("total_num",$total_num);
	$pagenum = ceil($total_num/$num);


	// 페이징
	if ($pages>=$pagenum){$pages=$pagenum;}
	if ($pages<=0){$pages=1;}

	// 검색 조건
	$search_url = "sch_dp_quick=".$sch_dp_quick_get.
								"&sch=".$sch_get.
								"&sch_dp_no=".$sch_dp_no_get.
								"&sch_dp_state=".$sch_dp_state_get.
								"&sch_dp_method=".$sch_dp_method_get.
								"&sch_dp_date=".$sch_dp_date_get.
								"&sch_incentive_date=".$sch_incentive_date_get.
								"&sch_s_no=".$sch_s_no_get.
								"&sch_dp_tax_state=".$sch_dp_tax_state_get.
								"&sch_dp_etc=".$sch_dp_etc_get.
								"&sch_my_c_no=".$sch_my_c_no_get.
								"&sch_prd_no=".$sch_prd_no_get.
								"&sch_c_name=".$sch_c_name_get.
								"&sch_subject=".$sch_subject_get.
								"&w_no=".$w_no_get;

	$smarty->assign("search_url",$search_url);
	$page=pagelist($pages, "find_deposit.php", $pagenum, $search_url);
	$smarty->assign("pagelist",$page);


	// 리스트 쿼리 결과(데이터)
	$deposit_sql .= "LIMIT $offset,$num";
	//echo "$deposit_sql<br>"; //exit;

	$result= mysqli_query($my_db, $deposit_sql);

	while($deposit_array = mysqli_fetch_array($result)){
		if(!empty($deposit_array['regdate'])) {
			$regdate_day_value = date("Y/m/d",strtotime($deposit_array['regdate']));
			$regdate_time_value = date("H:i",strtotime($deposit_array['regdate']));
		}else{
			$regdate_day_value = '';
			$regdate_time_value = '';
		}

		$deposit[] = array(
			"dp_no"=>$deposit_array['dp_no'],
			"incentive_date"=>$deposit_array['incentive_date'],
			"s_no"=>$deposit_array['s_no'],
			"s_name"=>$deposit_array['s_name'] ? $deposit_array['s_name'] : "<span style=\"color:red\">알수없음</span>",
			"w_no"=>$deposit_array['w_no'],
			"product_title"=>$deposit_array['product_title'],
			"prd_no"=>$deposit_array['prd_no'],
			"c_no"=>$deposit_array['c_no'],
			"c_name"=>$deposit_array['c_name'],
			"dp_state"=>$deposit_array['dp_state'],
			"dp_method"=>$deposit_array['dp_method'],
			"dp_date"=>$deposit_array['dp_date'],
			"my_c_name"=>$deposit_array['my_c_name'],
			"my_c_kind_color"=>$deposit_array['my_c_kind_color'],
			"dp_subject"=>$deposit_array['dp_subject'],
			"cost"=>number_format($deposit_array['cost']),
			"dp_money"=>number_format($deposit_array['dp_money']),
			"dp_money_vat"=>number_format($deposit_array['dp_money_vat']),
			"dp_tax_state"=>$deposit_array['dp_tax_state'],
			"dp_tax_date"=>$deposit_array['dp_tax_date'],
			"dp_tax_item"=>$deposit_array['dp_tax_item'],
			"regdate_day"=>$regdate_day_value,
			"regdate_time"=>$regdate_time_value,
			"reg_s_name"=>$deposit_array['reg_s_name']
		);
	}
	$smarty->assign("deposit", $deposit);

	$smarty->display('popup/find_deposit.html');
}
?>
