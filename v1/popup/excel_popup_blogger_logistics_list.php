<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../Classes/PHPExcel.php');
require('../inc/helper/promotion.php');
require('../inc/model/Promotion.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);


# 캠페인 정보
$p_no        = (isset($_GET['no']))?$_GET['no']:"";
$promo_model = Promotion::Factory();
$promo_item  = $promo_model->getItem($p_no);

$promotion_channel_option   = getPromotionChannelOption();
$promotion_kind_option      = getPromotionKindOption();

$promotion_title = (isset($promo_item['title']) && !empty($promo_item['title'])) ? $promo_item['title'] : $promo_item['company'];
$channel_name    = $promotion_channel_option[$promo_item['channel']];
$kind_name       = $promotion_kind_option[$promo_item['kind']];


$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "우편번호(생략가능)")
    ->setCellValue('B1', "주소지")
    ->setCellValue('C1', "주소지2 (생략 가능)")
    ->setCellValue('D1', "수령자명")
    ->setCellValue('E1', "수령자전화")
    ->setCellValue('F1', "배송메모")
;

# 신청서 쿼리
$application_sql    = "
SELECT 
   a.zipcode, a.address1, a.address2, a.username, a.hp, a.delivery_memo, 
   REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(a.blog_url,'http://',''),'https://',''),'www.',''),'instagram.com/',''),'blog.naver.com/',''),'.blog.me',''),'/','') as blog_id
FROM application a WHERE a.p_no='{$p_no}' AND a.a_state='1'
GROUP BY a.a_no
ORDER BY a.a_state ASC, a.visit_num DESC, a.real_visit_num DESC, blog_id ASC";
$application_query  = mysqli_query($my_db, $application_sql);
$idx = 2;
while($application = mysqli_fetch_array($application_query))
{
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValueExplicit("A{$idx}", $application['zipcode'], PHPExcel_Cell_DataType::TYPE_STRING)
        ->setCellValue("B{$idx}",$application['address1'])
        ->setCellValue("C{$idx}",$application['address2'])
        ->setCellValue("D{$idx}",$application['username'])
        ->setCellValue("E{$idx}",$application['hp'])
        ->setCellValue("F{$idx}",$application['delivery_memo'])
    ;

    $idx++;
}

$idx--;

$objPHPExcel->getActiveSheet()->getStyle("A1:F{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:F{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:F{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("B2:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');

$objPHPExcel->getActiveSheet()->getStyle("B2:B{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_".$promotion_title."_".$channel_name."_".$kind_name." 블로그 리스트(커머스용).xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
exit;
