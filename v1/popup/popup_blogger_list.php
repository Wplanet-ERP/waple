<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/data_state.php');

$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

$proc	= (isset($_POST['process'])) ? $_POST['process'] : "";
$idx	= (isset($_GET['no'])) ? $_GET['no'] : "";

/* 캠페인정보 가져오기 Start */
$promotion_sql = "
	SELECT 
		p.company,
		p.channel,
		p.kind,
		(SELECT s.s_name FROM staff s WHERE s.s_no=p.s_no) as staff_s_name,
		(SELECT s.hp FROM staff s WHERE s.s_no=p.s_no) as staff_hp,
		p.exp_sdate,
		p.exp_edate
	FROM promotion p where p.p_no='{$idx}'";
$promotion_result = mysqli_query($my_db, $promotion_sql);
$promotion 		  = mysqli_fetch_array($promotion_result);

# channel name
$channel_name="";
for($i=0 ; $i < sizeof($promotion_channel) ; $i++){
	if($promotion['channel'] == $promotion_channel[$i][1]){
		$channel_name = $promotion_channel[$i][0];
	}
}

# kind name
$kind_name="";
for($i=0 ; $i < sizeof($promotion_kind) ; $i++){
	if($promotion['kind'] == $promotion_kind[$i][1]){
		$kind_name = $promotion_kind[$i][0];
	}
}

$smarty->assign(
	array(
		"company"		=> $promotion['company'],
		"channel"		=> $promotion['channel'],
		"channel_name"	=> $channel_name,
		"kind"			=> $promotion['kind'],
		"kind_name"		=> $kind_name,
		"staff_s_name"	=> $promotion['staff_s_name'],
		"staff_hp"		=> $promotion['staff_hp'],
		"exp_sdate"		=> $promotion['exp_sdate'],
		"exp_edate"		=> $promotion['exp_edate']
	)
);
/* 캠페인정보 가져오기 End */

$sql	   = "SELECT count(*) FROM application where p_no='{$idx}' and a_state='1'";
$query	   = mysqli_query($my_db,$sql);
$cnt_data  = mysqli_fetch_array($query);
$total_num = $cnt_data[0];
$url 	   = "no=".$idx;
$smarty->assign("total_num",$cnt_data[0]);

// 페이징
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$totalnum 	= $total_num;
$num 		= 20;

$pagenum = ceil($totalnum/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$page=pagelist($pages, "popup_blogger_list.php", $pagenum, $url);
$smarty->assign("pagelist",$page);//
$noi = $totalnum - ($pages-1)*$num;
$offset = ($pages-1) * $num;
$smarty->assign("totalnum",$totalnum);


if($proc=="reload") {

	$qstr	= (isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno	= (isset($_POST['save_no']))?$_POST['save_no']:"";
	$sql	= "UPDATE blog SET reload='{$nowdate}' where a_no='{$mno}'";

	mysqli_query($my_db,$sql);

	exit("<script>alert('재평가를 완료하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
}
elseif($proc=="memo")
{
	$qstr	= (isset($_POST['list_query'])) ? $_POST['list_query']:"";
	$mno	= (isset($_POST['save_no'])) ? $_POST['save_no']:"";
	$mtxt	= (isset($_POST['memo'.$mno])) ? $_POST['memo'.$mno]:"";
	$sql	= "UPDATE blog SET b_memo='".addslashes($mtxt)."' WHERE a_no='{$mno}'";

	mysqli_query($my_db,$sql);

	exit("<script>alert('메모를 등록하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
}
else if($proc=="regist")
{
	$qstr	= (isset($_POST['list_query']))?$_POST['list_query']:"";
	$mno	= (isset($_POST['save_no']))?$_POST['save_no']:"";
	$mtxt	= (isset($_POST['memo'.$mno]))?$_POST['memo'.$mno]:"";
	$sql	= "SELECT blog_url, cafe_id, nick, username, hp, email FROM application WHERE a_no='{$mno}'";
	$query	= mysqli_query($my_db,$sql);
	$data	= mysqli_fetch_array($query);

	$insert = "
		INSERT INTO blog SET
			cafe_id	 = '{$data['cafe_id']}',
			nick	 = '{$data['nick']}',
			username = '{$data['username']}',
			blog_url = '{$data['blog_url']}',
			blog_md5 = '".md5($data['blog_url'])."',
			hp		 = '{$data['hp']}',
			email	 = '{$data['email']}',
			regdate	 = '{$nowdate}',
			b_memo	 = '".addslashes($mtxt)."',
			reload	 = '{$nowdate}',
			a_no	 = '{$mno}'
	";

	mysqli_query($my_db,$insert);

	exit("<script>alert('블로거를 등록 하였습니다');location.href='blogger_selection.php".((!empty($qstr))?"?".$qstr:"")."';</script>");
}


if(!is_numeric($idx))
	exit("<script>alert('잘못된 접근입니다.');history.back();</script>");


// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query", $save_query);

$a_state_text=array(1=>'선정',2=>'탈락',3=>'선정취소');
foreach($a_state_text as $key => $value) {
	$state_stmt[]=array
		(
			"no"=>$key,
			"name"=>$value
		);
	$smarty->assign("select",$state_stmt);
}

$add_orderby = "ORDER BY a.a_state ASC, a.real_visit_num DESC, blog_id ASC";
if($promotion['channel'] == '2'){
    $add_orderby = "ORDER BY a.a_state ASC, a.visit_num DESC, blog_id ASC";
}
$blogger_sql = "
	SELECT
		*,
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(a.blog_url,'http://',''),'https://',''),'www.',''),'instagram.com/',''),'blog.naver.com/',''),'.blog.me',''),'/','') as blog_id
	FROM application a
	WHERE p_no = '{$idx}' and (a.a_state='1' or a.a_state='3')
	{$add_orderby}
	LIMIT {$offset}, {$num}
";

$blogger_list = [];
$blogger_query = mysqli_query($my_db, $blogger_sql);
while($blogger = mysqli_fetch_array($blogger_query))
{
	$tmp_hp = explode("-", $blogger['hp']);
	$hp1	= $tmp_hp[0];
	$hp2	= $tmp_hp[1];
	$hp3	= $tmp_hp[2];

	// channel name
	$channel_name="";
	for($i=0 ; $i < sizeof($promotion_channel) ; $i++){
		if($blogger['channel'] == $promotion_channel[$i][1]){
			$channel_name = $promotion_channel[$i][0];
		}
	}

	// kind name
	$kind_name="";
	for($i=0 ; $i < sizeof($promotion_kind) ; $i++){
		if($blogger['kind'] == $promotion_kind[$i][1]){
			$kind_name = $promotion_kind[$i][0];
		}
	}

    $blogger_list[] = array(
		"idx_no"		=> $noi,
		"kind"			=> $blogger['kind'],
		"no"			=> $blogger['a_no'],
		"p_no"			=> $blogger['p_no'],
		"c_no"			=> $blogger['c_no'],
		"channel"		=> $blogger['channel'],
		"channel_name"	=> $channel_name,
		"kind_name"		=> $kind_name,
		"nick"			=> $blogger['nick'],
		"username"		=> $blogger['username'],
		"delivery_memo"	=> $blogger['delivery_memo'],
		"zipcode"		=> $blogger['zipcode'],
		"address1"		=> $blogger['address1'],
		"address2"		=> $blogger['address2'],
		"hp"			=> $blogger['hp'],
		"hp1"			=> $hp1,
		"hp2"			=> $hp2,
		"hp3"			=> $hp3,
		"email"			=> $blogger['email']
	);

    $noi--;
}

$smarty->assign("blogger", $blogger_list);


$smarty->display('popup/popup_blogger_list.html');
?>
