<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_date.php');
require('../inc/helper/promotion.php');
require('../inc/model/Message.php');
require('../inc/model/Promotion.php');

# 접근 권한
if (!$session_s_no){
	$smarty->display('access_error.html');
	exit;
}

if ($_POST)
{
	$message_model 	= Message::Factory();
	$send_list		= isset($_POST['send_list']) ? $_POST['send_list'] : "";
	$sms_msg		= isset($_POST['sms_msg']) ? $_POST['sms_msg'] : "";
	$send_data_list = [];

	$send_list 		= str_replace(" ", "", $send_list);
	$send_list 		= preg_replace("/\r\n|\r|\n/", "||", $send_list);
	$send_hp_list 	= explode("||", $send_list);

	$send_name 		= "와이즈플래닛";
	$send_phone		= "02-2675-6260";
	$sms_title 		= "마감예정 알림";
	$sms_msg 		= addslashes($sms_msg);
	$sms_msg_len 	= mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
	$msg_type 		= ($sms_msg_len > 90) ? "L" : "S";
	$send_name 		= "탑블로그";

	$cur_datetime	= date("YmdHis");
	$cm_id 			= 1;
	$c_info 		= "1"; # 탑블로그 마감예정 알림

	foreach ($send_hp_list as $dest_phone)
	{
		if(empty($dest_phone)){
			continue;
		}

		$msg_id = "W{$cur_datetime}".sprintf('%04d', $cm_id++);

		$send_data_list[$msg_id] = array(
			"msg_id"        => $msg_id,
			"msg_type"      => $msg_type,
			"sender"        => $send_name,
			"sender_hp"     => $send_phone,
			"receiver_hp"   => $dest_phone,
			"title"         => $sms_title,
			"content"       => $sms_msg,
			"cinfo"         => $c_info,
		);
	}

	if(!empty($send_data_list))
	{
		$result_data = $message_model->sendMessage($send_data_list);

		if ($result_data['result']) {
			exit("<script>alert('문자 메시지를 발송하였습니다.'); if (self) self.close();</script>");
		}
	}

	exit("<script>alert('문자 메세지 발송에 실패했습니다.');history.back();</script>");
}

$promo_no			= (isset($_GET['no'])) ? $_GET['no'] : "";
$promotion_model	= Promotion::Factory();
$promotion_item		= $promotion_model->getItem($promo_no);

$promotion_channel_option 	= getPromotionChannelOption();
$promotion_kind_option 	  	= getPromotionKindOption();
$date_name_option 	  		= getDateChartOption();

$sms_promo_title 	= (isset($promotion_item['title']) && !empty($promotion_item['title'])) ? $promotion_item['title'] : $promotion_item['company'];
$sms_kind_name		= $promotion_kind_option[$promotion_item['kind']];
$sms_promo_content 	= "[탑블로그] {$sms_promo_title} {$promotion_channel_option[$promotion_item['channel']]} {$sms_kind_name} 마감은 ".date("n/j(".$date_name_option[date("w",strtotime($promotion_item['exp_edate']))].")",strtotime($promotion_item['exp_edate']))."입니다. 기한내 포스팅 필수♡";
$sms_promo_title   .= " {$sms_kind_name}";

$app_end_sql = "
	SELECT 
	    a.nick, 
	    a.hp
	FROM application a
	LEFT JOIN report r on a.a_no = r.a_no
	WHERE a.p_no = '{$promo_no}' AND a.a_state='1' AND r.post_url IS NULL
	ORDER BY a.a_no DESC
";
$app_end_query = mysqli_query($my_db, $app_end_sql);
$send_list = [];
while ($app_end = mysqli_fetch_array($app_end_query)) {
	$send_list[] = $app_end['hp'];
}
$send_num = !empty($send_list) ? count($send_list) : 0;

$smarty->assign("promo_no", $promo_no);
$smarty->assign("sms_promo_title", $sms_promo_title);
$smarty->assign("sms_promo_content", $sms_promo_content);
$smarty->assign("send_num", $send_num);
$smarty->assign("send_list", $send_list);

$smarty->display('popup/dday_sms_list.html');
?>
