<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/data_state.php');
require('../Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);


$proc=(isset($_POST['process']))?$_POST['process']:"";
$idx=(isset($_GET['no']))?$_GET['no']:"";

/* 캠페인정보 가져오기 Start */
$promotion_sql="SELECT p.title, p.company, p.channel, p.kind FROM promotion p where p.p_no='".$idx."'";
$promotion_result=mysqli_query($my_db,$promotion_sql);
$promotion = mysqli_fetch_array($promotion_result);

//캠페인명
$promotion_title = (isset($promotion['title']) && !empty($promotion['title'])) ? $promotion['title'] : $promotion['company'];

// channel name
$channel_name="";
for($i=0 ; $i < sizeof($promotion_channel) ; $i++){
	if($promotion['channel'] == $promotion_channel[$i][1]){
		$channel_name = $promotion_channel[$i][0];
	}
}

// kind name
$kind_name="";
for($i=0 ; $i < sizeof($promotion_kind) ; $i++){
	if($promotion['kind'] == $promotion_kind[$i][1]){
		$kind_name = $promotion_kind[$i][0];
	}
}
/* 캠페인정보 가져오기 End */


//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A3', "Num")
->setCellValue('B3', "닉네임")
->setCellValue('C3', "포스팅제목")
->setCellValue('D3', "포스팅URL");


// 리스트 내용
$i=4;
$ttamount=0;

$add_orderby = "ORDER BY a.a_state ASC, a.real_visit_num DESC, blog_id ASC";
if($promotion['channel'] == '2'){
    $add_orderby = "ORDER BY a.a_state ASC, a.visit_num DESC, blog_id ASC";
}
$sql="
		select	*,
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(a.blog_url,'http://',''),'https://',''),'www.',''),'instagram.com/',''),'blog.naver.com/',''),'.blog.me',''),'/','') as blog_id
		from application a
		where p_no = '".$idx."' and ( a_state='1' or a_state='3' )
		{$add_orderby}
";
$idx_no=1;

$query=mysqli_query($my_db,$sql);
while($data=mysqli_fetch_array($query)) {

	$blog_sql = "select b_no,b_memo,nick,reload from blog where blog_url='".$data['blog_url']."'";
	//$sql = "select b_memo,nick,reload from blog where b_no = '".$data['b_no']."'";
	//echo "<br><br>".$blog_sql."<br><br>";
	$blog_query=mysqli_query($my_db,$blog_sql);
	$blog_info=mysqli_fetch_array($blog_query);

	$report_sql = "select * from report where p_no='".$idx."' and a_no='".$data['a_no']."'";
	//$sql = "select b_memo,nick,reload from blog where b_no = '".$data['b_no']."'";
	//echo "<br><br>".$report_sql."<br><br>";
	$report_query=mysqli_query($my_db,$report_sql);

	$lfcr = chr(10) ;//. chr(13); // 엑셀 내 줄바꿈 alt+enter
	$post_title='';
	$post_url='';
	while($report_info=mysqli_fetch_array($report_query)){

		if($post_title != ''){
			$post_title = $post_title.$lfcr.$report_info['post_title'];
		}else{
			$post_title = $report_info['post_title'];
		}

		if($post_url != ''){
			$post_url = $post_url.$lfcr.$report_info['post_url'];
		}else{
			$post_url=$report_info['post_url'];
		}

		$report_data[]=array
		(
			"r_no"=>$report_info['r_no'],
			"a_no"=>$data['a_no'],
			"post_title"=>$report_info['post_title'],
			"post_url"=>$report_info['post_url'],
			"r_memo"=>$report_info['memo'],
		);
	}

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$i, $idx_no)
	->setCellValue('B'.$i, $data['nick'])
	->setCellValue('C'.$i, $post_title)
	->setCellValue('D'.$i, $post_url);
	$i = $i+1;
		$idx_no++;
}

if($i > 1)
	$i = $i-1;

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $promotion_title." ".$channel_name." ".$kind_name." 포스팅 리스트");
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


$objPHPExcel->getActiveSheet()->getStyle('A3:D'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:D'.$i)->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:D3')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle('A4:D'.$i)->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFont()->setSize(11);

$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
															->getStartColor()->setARGB('00c4bd97');
$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:D'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$i)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$i)->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(75);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(60);
$objPHPExcel->getActiveSheet()->setTitle('포스팅 리스트');

$i2_length = $i;
$i2=1;

while($i2_length){
	if($i2 > 2)
		$objPHPExcel->getActiveSheet()->getRowDimension($i2)->setRowHeight(40);

	if($i2 > 3 && $i2 % 2 == 1)
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i2.':D'.$i2)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ebf1de');

	$i2++;
	$i2_length--;
}


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_".$promotion_title." ".$channel_name." ".$kind_name." 포스팅 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
