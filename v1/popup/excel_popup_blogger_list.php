<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/data_state.php');
require('../Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);


$proc=(isset($_POST['process']))?$_POST['process']:"";
$idx=(isset($_GET['no']))?$_GET['no']:"";
$sort_type=(isset($_GET['sort_type']))?$_GET['sort_type']:"";

$add_where = "p_no = '{$idx}'";
if($sort_type == "1") { // 선정된 블로거만 골라보기
    $add_where .= " AND a_state='1'";
}

/* 캠페인정보 가져오기 Start */
$promotion_sql="
    SELECT 
        p.title,
        p.company,
        p.channel,
        p.kind,
        (SELECT s.s_name FROM staff s WHERE s.s_no=p.s_no) as staff_s_name,
        (SELECT s.hp FROM staff s WHERE s.s_no=p.s_no) as staff_hp,
        p.exp_sdate,
        p.exp_edate
    FROM promotion p where p.p_no='".$idx."'";
$promotion_result=mysqli_query($my_db,$promotion_sql);
$promotion = mysqli_fetch_array($promotion_result);

//캠페인명
$promotion_title = (isset($promotion['title']) && !empty($promotion['title'])) ? $promotion['title'] : $promotion['company'];

// channel name
$channel_name="";
for($i=0 ; $i < sizeof($promotion_channel) ; $i++){
	if($promotion['channel'] == $promotion_channel[$i][1]){
		$channel_name = $promotion_channel[$i][0];
	}
}

// kind name
$kind_name="";
for($i=0 ; $i < sizeof($promotion_kind) ; $i++){
	if($promotion['kind'] == $promotion_kind[$i][1]){
		$kind_name = $promotion_kind[$i][0];
	}
}
/* 캠페인정보 가져오기 End */


//상단타이틀
if($promotion['kind'] == '3' || $promotion['kind'] == '5'){
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "Num")
	->setCellValue('B3', "닉네임")
	->setCellValue('C3', "이름")
	->setCellValue('D3', "휴대폰")
	->setCellValue('E3', "체험상품")
	->setCellValue('F3', "우편번호")
	->setCellValue('G3', "배송주소")
	->setCellValue('H3', "배송메모");
}else{
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "Num")
	->setCellValue('B3', "닉네임")
	->setCellValue('C3', "이름")
	->setCellValue('D3', "휴대폰")
	->setCellValue('E3', "체험상품")
	->setCellValue('F3', "예약일시")
	->setCellValue('G3', "방문확인");
}
// 리스트 내용
$i=4;
$ttamount=0;

$add_orderby = "ORDER BY a.a_state ASC, a.real_visit_num DESC, blog_id ASC";
if($promotion['channel'] == '2'){
    $add_orderby = "ORDER BY a.a_state ASC, a.visit_num DESC, blog_id ASC";
}
$sql="
		select	*,
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(a.blog_url,'http://',''),'https://',''),'www.',''),'instagram.com/',''),'blog.naver.com/',''),'.blog.me',''),'/','') as blog_id,
		(select product from promotion p where p.p_no=a.p_no) as product_name
		from application a
		where {$add_where}
		{$add_orderby}
	";
//echo "<br><br><br>".$sql;
$idx_no=1;

$query=mysqli_query($my_db,$sql);
while($data=mysqli_fetch_array($query)) {

	$tmp_hp = explode("-",$data['hp']);
	$hp1=$tmp_hp[0];
	$hp2=$tmp_hp[1];
	$hp3=$tmp_hp[2];
	$stmt[]=array
		(
			"idx_no"=>$idx_no,
			"kind"=>$data['kind'],
			"no"=>$data['a_no'],
			"p_no"=>$data['p_no'],
			"c_no"=>$data['c_no'],
			"nick"=>$data['nick'],
			"username"=>$data['username'],
			"zipcode"=>$data['zipcode'],
			"address1"=>$data['address1'],
			"address2"=>$data['address2'],
			"hp"=>$data['hp'],
			"hp1"=>$hp1,
			"hp2"=>$hp2,
			"hp3"=>$hp3,
			"delivery_memo"=>$data['delivery_memo'],
			"product"=>$data['product_name']
		);
	// Add some data

	if ($promotion['kind'] == '3' || $promotion['kind'] == '5') // 배송체험의 경우 연락처 노출
		$hp_num = $data['hp'];
	else // 배송체험 외 연락처 비공개
		$hp_num = $hp1."-****-".$hp3;

	if ($data['product']==''){ // 신청 내역에 체험상품이 비어 있을 경우 캠페인의 체험상품 표시
		$product = $data['product_name'];

		if(substr($data['product_name'],0,1) == '=')
			$product = "'".$product;
	}else
		$product  = $data['product'];

	if($promotion['kind'] == '3' || $promotion['kind'] == '5'){
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $idx_no)
		->setCellValue('B'.$i, $data['nick'])
		->setCellValue('C'.$i, $data['username'])
		->setCellValue('D'.$i, $hp_num)
		->setCellValue('E'.$i, $product)
		->setCellValue('F'.$i, $data['zipcode'])
		->setCellValue('G'.$i, $data['address1'].' '.$data['address2'])
		->setCellValue('H'.$i, $data['delivery_memo']);
	}else{
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $idx_no)
		->setCellValue('B'.$i, $data['nick'])
		->setCellValue('C'.$i, $data['username'])
		->setCellValue('D'.$i, $hp_num)
		->setCellValue('E'.$i, $product)
		->setCellValue('F'.$i, '')
		->setCellValue('G'.$i, '');
	}

	$i = $i+1;
	$idx_no++;
}


if($i > 1)
	$i = $i-1;

if($promotion['kind'] == '3' || $promotion['kind'] == '5'){
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $promotion_title." ".$kind_name." 블로거 배송정보 리스트");
	$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setSize(20);
	$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('A3:H'.$i)->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->getStyle('A1:H'.$i)->getFont()->setName('맑은 고딕');
	$objPHPExcel->getActiveSheet()->getStyle('A1:H3')->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->getStyle('A4:H'.$i)->getFont()->setSize(10);
	$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFont()->setSize(11);

	$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
																->getStartColor()->setARGB('00c4bd97');
	$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('A3:H'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

	$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getAlignment()->setWrapText(true);


	// Rename worksheet  set the width autosize
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
	$objPHPExcel->getActiveSheet()->setTitle('배송체험 블로거 리스트');

	$i2_length = $i;
	$i2=1;

	while($i2_length){
		if($i2 > 2)
			$objPHPExcel->getActiveSheet()->getRowDimension($i2)->setRowHeight(40);

		if($i2 > 3 && $i2 % 2 == 1)
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i2.':H'.$i2)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ebf1de');

		$i2++;
		$i2_length--;
	}
}else{
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $promotion_title." ".$channel_name." ".$kind_name." 선정 리스트");
	$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setSize(20);
	$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('A3:G'.$i)->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->getStyle('A1:G'.$i)->getFont()->setName('맑은 고딕');
	$objPHPExcel->getActiveSheet()->getStyle('A1:G3')->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->getStyle('A4:G'.$i)->getFont()->setSize(10);
	$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFont()->setSize(11);

	$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
																->getStartColor()->setARGB('00c4bd97');
	$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('A3:G'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getAlignment()->setWrapText(true);


	// Rename worksheet  set the width autosize
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(45);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
	$objPHPExcel->getActiveSheet()->setTitle(' 블로거 리스트');

	$i2_length = $i;
	$i2=1;

	while($i2_length){
		if($i2 > 2)
			$objPHPExcel->getActiveSheet()->getRowDimension($i2)->setRowHeight(40);

		if($i2 > 3 && $i2 % 2 == 1)
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i2.':G'.$i2)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ebf1de');

		$i2++;
		$i2_length--;
	}

	$i++;

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':A'.(3+$i))->getFont()->setName('맑은 고딕');
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':A'.(3+$i))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':A'.(3+$i))->getFont()->setSize(13);
	//$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(15);

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$i++, '* 체험기간 : '.$promotion['exp_sdate'].' ~ '.$promotion['exp_edate'])
	->setCellValue('A'.$i++, '* 체험단 방문시 [이름]과 [닉네임], [핸드폰 전화 뒷자리]를 확인해 주세요.')
	->setCellValue('A'.$i++, '* 체험단 관련 문의 사항은 [담당자 : '.$promotion['staff_s_name'].' '.$promotion['staff_hp'].']로 연락 바랍니다.');
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_".$promotion_title."_".$channel_name."_".$kind_name." 선정 리스트.xls");


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

/*

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="popup_blogger_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
*/
