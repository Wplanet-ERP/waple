<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
),
),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "닉네임")
->setCellValue('B1', "이름")
->setCellValue('C1', "블로그URL")
->setCellValue('D1', "휴대폰")
->setCellValue('E1', "이메일")
->setCellValue('F1', "신청사연")
->setCellValue('G1', "메모")
->setCellValue('H1', "선택옵션")
->setCellValue('I1', "선정여부")
->setCellValue('J1', "우편번호")
->setCellValue('K1', "주소")
->setCellValue('L1', "배송메세지");


// 리스트 내용
$i=2;
$ttamount=0;

$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);


$proc=(isset($_POST['process']))?$_POST['process']:"";
$idx=(isset($_GET['no']))?$_GET['no']:"";
$sort_type=(isset($_GET['sort_type']))?$_GET['sort_type']:"";

$smarty->assign("p_no",$idx);
// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query",$save_query);

$a_state_text=array(1=>'선정',2=>'탈락',3=>'선정취소');
foreach($a_state_text as $key => $value) {
	$state_stmt[]=array
		(
			"no"=>$key,
			"name"=>$value
		);
	$smarty->assign("select",$state_stmt);
}

$promotion_sql   = "SELECT channel FROM promotion WHERE p_no='{$idx}'";
$promotion_query = mysqli_query($my_db, $promotion_sql);
$promotion       = mysqli_fetch_assoc($promotion_query);


// 선정된 블로거만 골라보기 김윤영 Start
$add_order_by = " a.a_state ASC, a.real_visit_num DESC, blog_id ASC";
if($sort_type == "1") { // 선정된 블로거만 골라보기
    $add_where      = " and (a.a_state='1' or a.a_state='3')";
    if($promotion['channel'] == '2'){
        $add_order_by   = " a.a_state ASC, a.visit_num DESC, blog_id ASC";
    }
}elseif($sort_type=="2") { // 평가일 오래된순 정렬(지수체크용)
    $add_where      = "";
    $add_order_by   = " b_reload ASC";
}else{
    $add_where      = "";
    if($promotion['channel'] == '2'){
        $add_order_by   = " a.a_state ASC, a.visit_num DESC, blog_id ASC";
    }
}

$smarty->assign("choice",$choice);
// 선정된 블로거만 골라보기 김윤영 End



$sql="
		select
			a.a_no,
			a.p_no,
			a.c_no,
			a.a_state,
			a.kind,
			a.nick,
			a.username,
			a.blog_url,
			REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(a.blog_url,'http://',''),'https://',''),'www.',''),'instagram.com/',''),'blog.naver.com/',''),'.blog.me',''),'/','') as blog_id,
			(select count(b_no) from blog b where b.blog_url=a.blog_url) as blog_check,
			(select b_memo from blog b where b.blog_url=a.blog_url ORDER BY b.b_no DESC LIMIT 1) as b_memo,
			b.reload as b_reload,
			a.hp,
			a.email,
			a.memo,
			a.opt_a_answer,
			a.opt_b_answer,
			a.opt_c_answer,
			a.zipcode,
			a.address1,
			a.address2,
			a.delivery_memo,
			r.r_no
		from
			application a
		left join
			blog b
			ON a.blog_url=b.blog_url
		LEFT JOIN
			report r
			ON a.a_no = r.a_no
		where
			a.p_no='".$idx."'
			$add_where
		group by a.a_no
		order by
			$add_order_by
	";

// a.a_state asc, 를 추가하여 선택, 선정, 탈락, 선정취소 순으로 기본 정렬함 20160307 김윤영

//echo "<br><br><br><br><br><br><br><br>".$sql."<br><br><br>";
$query=mysqli_query($my_db,$sql);
//echo "<br>111<br><br><br><br>";
while($data=mysqli_fetch_array($query)) {

	$zipcode=explode('-',$data['zipcode']);

	/* 계좌번호 0으로 시작하는 문자열 설정:: ' 제거 20160405 김윤영 Start
	if($data['bk_num']{0} == "`"){
		$bk_num_value = substr($data['bk_num'], 1);
	}else{
		$bk_num_value = $data['bk_num'];
	}
	// 계좌번호 0으로 시작하는 문자열 설정:: ' 제거 20160405 김윤영 End*/

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$i, stripslashes($data['nick']))
	->setCellValue('B'.$i, stripslashes($data['username']))
	->setCellValue('C'.$i, stripslashes($data['blog_url']))
	->setCellValue('D'.$i, $data['hp'])
	->setCellValue('E'.$i, stripslashes($data['email']))
	->setCellValue('F'.$i, stripslashes($data['memo']))
	->setCellValue('G'.$i, stripslashes($data['b_memo']))
	->setCellValue('H'.$i, "선택옵션 A : ".stripslashes($data['opt_a_answer'])."\n"."선택옵션 B : ".stripslashes($data['opt_b_answer'])."\n"."선택옵션 C : ".stripslashes($data['opt_c_answer']))
	->setCellValue('I'.$i, $a_state_text[$data['a_state']])
	->setCellValue('J'.$i, $data['zipcode'])
	->setCellValue('K'.$i, stripslashes($data['address1'])." ".stripslashes($data['address2']))
	->setCellValue('L'.$i, stripslashes($data['delivery_memo']));

	$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getAlignment()->setWrapText(true)
																	->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
	$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->getAlignment()->setWrapText(true);

	$i = $i+1;
}

$objPHPExcel->getActiveSheet()->getStyle('A2:L'.$i)->getFont()->setSize(10);

if($i > 1)
	$i = $i-1;

//echo "<br><br><br><br><br><br><br>";
//print_r($stmt);

$objPHPExcel->getActiveSheet()->getStyle('A1:L'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('00F3F3F6');
//$objPHPExcel->getActiveSheet()->getStyle('A1:AC'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:L'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->setTitle('블로거 선정 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="excel_blogger_selection_'.$today.'.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');
exit;
