<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/logistics.php');
require('inc/helper/work.php');
require('inc/model/Logistics.php');
require('inc/model/Work.php');
require('inc/model/Company.php');

# 프로세스 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "save_logistics")
{
    $logistics_model    = Logistics::Factory();
    $work_model         = Work::Factory();

    $lm_no              = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";
    $prev_type          = isset($_POST['prev_type']) ? $_POST['prev_type'] : "";
    $w_no               = isset($_POST['w_no']) ? $_POST['w_no'] : '';
    $stock_type         = isset($_POST['stock_type']) ? $_POST['stock_type'] : "";
    $prd_type           = isset($_POST['prd_type']) ? $_POST['prd_type'] : "1";
    $delivery_type      = isset($_POST['delivery_type']) ? $_POST['delivery_type'] : "";
    $doc_type           = isset($_POST['doc_type']) ? $_POST['doc_type'] : 0;
    $work_state         = isset($_POST['work_state']) ? $_POST['work_state'] : "3";
    $run_date           = isset($_POST['run_date']) ? $_POST['run_date'] : "";
    $subject            = isset($_POST['subject']) ? $_POST['subject'] : "";
    $sender_type        = isset($_POST['sender_type']) ? $_POST['sender_type'] : "";
    $sender_type_name   = isset($_POST['sender_type_name']) ? $_POST['sender_type_name'] : "";
    $receiver_type      = isset($_POST['receiver_type']) ? $_POST['receiver_type'] : "";
    $receiver_type_name = isset($_POST['receiver_type_name']) ? $_POST['receiver_type_name'] : "";
    $addr_company_option= getAddrCompanyOption();

    $logistics_data = array(
        "lm_no"                 => $lm_no,
        "doc_no"                => isset($_POST['doc_no']) ? $_POST['doc_no'] : $logistics_model->getNewDocNo(),
        "doc_type"              => $doc_type,
        "work_state"            => $work_state,
        "w_no"                  => $w_no,
        "stock_type"            => $stock_type,
        "prd_type"              => $prd_type,
        "delivery_type"         => $delivery_type,
        "subject"               => ($stock_type != '2') ? "NULL" : $subject,
        "sender_type"           => $sender_type,
        "sender_type_name"      => ($sender_type == '99999') ? (!empty($sender_type_name) ? $sender_type_name : "기타") : $addr_company_option[$sender_type],
        "sender_zipcode"        => isset($_POST['sender_zipcode']) ? $_POST['sender_zipcode'] : "",
        "sender_addr"           => isset($_POST['sender_addr']) ? addslashes(trim($_POST['sender_addr'])) : "",
        "sender_addr_detail"    => isset($_POST['sender_addr_detail']) ? addslashes(trim($_POST['sender_addr_detail'])) : "",
        "sender"                => isset($_POST['sender']) ? addslashes(trim($_POST['sender'])) : "",
        "sender_hp"             => isset($_POST['sender_hp']) ? addslashes(trim($_POST['sender_hp'])) : "",
        "receiver_type"         => $receiver_type,
        "receiver_type_name"    => ($receiver_type == '99999') ? (!empty($receiver_type_name) ? $receiver_type_name : "기타") : $addr_company_option[$receiver_type],
        "reason"                => isset($_POST['reason']) ? addslashes(trim($_POST['reason'])) : "",
        "run_ord_no"            => "NULL",
        "run_c_no"              => "0",
        "quick_req_date"        => "NULL",
        "is_quick_memo"         => "2",
        "quick_memo"            => "NULL",
        "quick_req_memo"        => "NULL",
    );

    $work_data = array(
        "w_no"       => $w_no,
        "work_state" => $work_state
    );

    if($stock_type == '2') {
        if($subject == '12'){
            $logistics_data['run_c_no'] = isset($_POST['sales_c_no']) ? $_POST['sales_c_no'] : "";
            $logistics_data['run_ord_no'] = isset($_POST['run_ord_no']) ? $_POST['run_ord_no'] : "";
        }elseif($subject == '13'){
            $logistics_data['run_c_no'] = isset($_POST['etc_sales_c_no']) ? $_POST['etc_sales_c_no'] : "";
        }
    }
    elseif($stock_type == '3') {
        $logistics_data['run_c_no'] = isset($_POST['transfer_c_no']) ? $_POST['transfer_c_no'] : "";
    }

    if($delivery_type == '2') {
        $logistics_data['quick_req_date']   = isset($_POST['quick_req_date']) ? $_POST['quick_req_date'] : "";
        $logistics_data['is_quick_memo']    = isset($_POST['is_quick_memo']) ? $_POST['is_quick_memo'] : "2";
        $logistics_data['quick_memo']       = isset($_POST['quick_memo']) ? addslashes(trim($_POST['quick_memo'])) : "";
        $logistics_data['quick_req_memo']   = isset($_POST['quick_req_memo']) ? addslashes(trim($_POST['quick_req_memo'])) : "";
    }

    if(!empty($_POST['req_s_no']) && !empty($_POST['req_team'])){
        $logistics_data['req_s_no'] = $_POST['req_s_no'];
        $logistics_data['req_team'] = $_POST['req_team'];
        $work_data['task_req_s_no'] = $_POST['req_s_no'];
        $work_data['task_req_team'] = $_POST['req_team'];
    }

    if($work_state == '6')
    {
        $prev_logistics_item = $logistics_model->getItem($lm_no);

        if($prev_logistics_item['work_state'] != $work_state) {
            $logistics_data['run_s_no'] = $session_s_no;
            $logistics_data['run_team'] = $session_team;
            $work_data['task_run_s_no'] = $session_s_no;
            $work_data['task_run_team'] = $session_team;
        }

        if(empty($run_date)){
            $logistics_data['run_date']     = date('Y-m-d');
            $work_data['task_run_regdate']  = date('Y-m-d H:i:s');
        }
    }

    $roc_file = $_FILES['roc_file'];
    if(isset($roc_file['name']) && !empty($roc_file['name'])) {
        $roc_file_path = add_store_file($roc_file, "logistics");
        $logistics_data['roc_file_path'] = $roc_file_path;
    }

    $zet_file = $_FILES['zet_file'];
    if(isset($zet_file['name']) && !empty($zet_file['name'])) {
        $zet_file_path = add_store_file($zet_file, "logistics");
        $logistics_data['zet_file_path'] = $zet_file_path;
    }

    $qx_file = $_FILES['qx_file'];
    if(isset($qx_file['name']) && !empty($qx_file['name'])) {
        $qx_file_path = add_store_file($qx_file, "logistics");
        $logistics_data['qx_file_path'] = $qx_file_path;
    }

    if($logistics_model->update($logistics_data))
    {
        $logistics_prd_data = [];
        $org_prd_list       = [];

        $new_prd_type   = ($prd_type == '1') ? "prd" : "unit";
        $new_lp_no      = isset($_POST["new_lp_no_{$new_prd_type}"]) ? $_POST["new_lp_no_{$new_prd_type}"] : "";
        $new_log_c_no   = isset($_POST["new_log_c_no_{$new_prd_type}"]) ? $_POST["new_log_c_no_{$new_prd_type}"] : "";
        $new_price      = isset($_POST["new_price_{$new_prd_type}"]) ? $_POST["new_price_{$new_prd_type}"] : "";
        $new_quantity   = isset($_POST["new_quantity_{$new_prd_type}"]) ? $_POST["new_quantity_{$new_prd_type}"] : "";
        $new_no         = isset($_POST["new_no_{$new_prd_type}"]) ? $_POST["new_no_{$new_prd_type}"] : "";
        $org_prd_list   = isset($_POST["org_prd_list"]) ? explode(",", $_POST["org_prd_list"]) : "";

        if($logistics_data['subject'] != '12'){
            $new_price = "";
        }

        if(!empty($new_no))
        {
            $prd_idx = 0;
            foreach($new_no as $prd_no)
            {
                $lp_no      = isset($new_lp_no[$prd_idx]) ? $new_lp_no[$prd_idx] : 0;
                $log_c_no   = isset($new_log_c_no[$prd_idx]) ? $new_log_c_no[$prd_idx] : 2809;
                $price      = isset($new_price[$prd_idx]) ? str_replace(',','', $new_price[$prd_idx]) : 0;
                $quantity   = isset($new_quantity[$prd_idx]) ? $new_quantity[$prd_idx] : 1;
                $logistics_prd_data[] = array(
                    'lp_no'     => $lp_no,
                    'lm_no'     => $lm_no,
                    'log_c_no'  => $log_c_no,
                    'prd_type'  => $prd_type,
                    'prd_no'    => $prd_no,
                    'price'     => $price,
                    'quantity'  => $quantity,
                );
                $prd_idx++;
            }
        }

        $logistics_model->saveProduct($lm_no, $prd_type, $org_prd_list, $logistics_prd_data);

        if(!empty($w_no)){
            $work_model->update($work_data);
        }

        $logistics_address_data = [];
        $org_addr_list          = isset($_POST["org_addr_list"]) ? explode(",", $_POST["org_addr_list"]) : "";

        $new_la_no          = isset($_POST["new_la_no"]) ? $_POST["new_la_no"] : "";
        $new_zipcode        = isset($_POST["new_zipcode"]) ? $_POST["new_zipcode"] : "";
        $new_address        = isset($_POST["new_address"]) ? $_POST["new_address"] : "";
        $new_address_detail = isset($_POST["new_address_detail"]) ? $_POST["new_address_detail"] : "";
        $new_receiver       = isset($_POST["new_receiver"]) ? $_POST["new_receiver"] : "";
        $new_hp             = isset($_POST["new_hp"]) ? $_POST["new_hp"] : "";
        $new_delivery_memo  = isset($_POST["new_delivery_memo"]) ? $_POST["new_delivery_memo"] : "";

        if(!empty($new_la_no))
        {
            $addr_idx = 0;
            foreach($new_la_no as $la_no)
            {
                $logistics_address_data[] = array(
                    "la_no"             => $la_no,
                    "lm_no"             => $lm_no,
                    "zipcode"           => isset($new_zipcode[$addr_idx]) ? $new_zipcode[$addr_idx] : "",
                    "address"           => isset($new_address[$addr_idx]) ? addslashes($new_address[$addr_idx]) : "",
                    "address_detail"    => isset($new_address_detail[$addr_idx]) ? addslashes($new_address_detail[$addr_idx]) : "",
                    "name"              => isset($new_receiver[$addr_idx]) ? addslashes($new_receiver[$addr_idx]) : "",
                    "hp"                => isset($new_hp[$addr_idx]) ? $new_hp[$addr_idx] : "",
                    "delivery_memo"     => isset($new_delivery_memo[$addr_idx]) ? addslashes($new_delivery_memo[$addr_idx]) : "",
                );

                $addr_idx++;
            }
        }

        if(!empty($org_addr_list) || !empty($logistics_address_data)){
            $logistics_model->saveAddress($lm_no, $org_addr_list, $logistics_address_data);
        }

        if($prev_type == 'work' && $subject == '12'){
            exit("<script>if(confirm('입고/반출 요청서가 저장되었습니다.\\n이어서 입금리스트(계산서발행) 요청하시겠습니까?')){location.href='deposit_regist.php';}else{location.href='logistics_management.php';}</script>");
        }else{
            exit("<script>alert('입고/반출 요청서가 저장되었습니다.');location.href='logistics_management.php';</script>");
        }
    }
    else
    {
        exit("<script>alert('입고/반출 요청서가 저장에 실패했습니다.');location.href='logistics_management.php?lm_no={$lm_no}'</script>");
    }
}
elseif($process == "duplicate_logistics")
{
    $logistics_model= Logistics::Factory();
    $work_model     = Work::Factory();
    $lm_no          = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";
    $regdate        = date("Y-m-d H:i:s");

    $logistic_item  = $logistics_model->getItem($lm_no);
    $work_item      = $work_model->getItem($logistic_item['w_no']);
    $new_log_data   = $logistic_item;

    $new_log_data['regdate']        = $regdate;
    $new_log_data['quick_req_date'] = "NULL";
    $new_log_data['doc_no']         = $logistics_model->getNewDocNo($logistic_item['prd_no']);
    $new_log_data['work_state']     = 3;
    $new_log_data['order_number']   = "NULL";
    $new_log_data['reason']         = "문서번호 : {$logistic_item['doc_no']} 복제";
    unset($new_log_data['lm_no']);
    unset($new_log_data['w_no']);

    $new_work_data  = $work_item;
    $new_work_data['work_state'] = 3;
    $new_work_data['regdate']    = $regdate;
    unset($new_work_data['w_no']);
    unset($new_work_data['task_req_dday']);
    unset($new_work_data['task_run_dday']);
    unset($new_work_data['task_run_regdate']);
    unset($new_work_data['linked_no']);

    if($work_model->insert($new_work_data))
    {
        $new_w_no               = $work_model->getInsertId();
        $new_log_data["w_no"]   = $new_w_no;

        if($logistics_model->insert($new_log_data))
        {
            $new_lm_no = $logistics_model->getInsertId();
            $work_model->update(array("w_no" => $new_w_no, "linked_no" => $new_lm_no));

            $new_addr_sql   = "SELECT * FROM logistics_address WHERE lm_no='{$logistic_item['lm_no']}'";
            $new_addr_query = mysqli_query($my_db, $new_addr_sql);
            $new_addr_data  = [];
            while($new_addr = mysqli_fetch_assoc($new_addr_query)){
                $new_addr_data[] = array(
                    "lm_no"             => $new_lm_no,
                    "zipcode"           => $new_addr["zipcode"],
                    "address"           => $new_addr["address"],
                    "address_detail"    => $new_addr["address_detail"],
                    "name"              => $new_addr["name"],
                    "hp"                => $new_addr["hp"],
                    "delivery_memo"     => $new_addr["delivery_memo"],
                );
            }

            $new_prd_sql    = "SELECT * FROM logistics_product WHERE lm_no='{$logistic_item['lm_no']}'";
            $new_prd_query  = mysqli_query($my_db, $new_prd_sql);
            $new_prd_data   = [];
            while($new_prd = mysqli_fetch_assoc($new_prd_query)){
                $new_prd_data[] = array(
                    'lm_no'     => $new_lm_no,
                    'log_c_no'  => $new_prd['log_c_no'],
                    'prd_type'  => $new_prd['prd_type'],
                    'prd_no'    => $new_prd['prd_no'],
                    'price'     => $new_prd['price'],
                    'quantity'  => -$new_prd['quantity'],
                );
            }

            if(!empty($new_addr_data)) {
                $logistics_addr_model   = Logistics::Factory();
                $logistics_addr_model->setMainInit("logistics_address", "la_no");
                $logistics_addr_model->multiInsert($new_addr_data);
            }

            if(!empty($new_prd_data)) {
                $logistics_prd_model    = Logistics::Factory();
                $logistics_prd_model->setMainInit("logistics_product", "lp_no");
                $logistics_prd_model->multiInsert($new_prd_data);
            }

            exit("<script>alert('복제했습니다');location.href='logistics_request.php?lm_no={$new_lm_no}';</script>");
        }else{
            $work_model->delete($new_w_no);
            exit("<script>alert('복제 처리의 실패했습니다');location.href='logistics_request.php?lm_no={$lm_no}';</script>");
        }

    }else{
        exit("<script>alert('복제 처리의 실패했습니다');location.href='logistics_request.php?lm_no={$lm_no}';</script>");
    }
}

# 입고/반출 요청서 보기
$lm_no       = isset($_GET['lm_no']) ? $_GET['lm_no'] : "";
$is_editable = false;
if($session_s_no == '17' || permissionNameCheck($session_permission, "물류관리자") || $session_s_no == '102'){
    $is_editable = true;
}
$smarty->assign("is_editable", $is_editable);

$logistics_request   = [];
$log_doc_type_option = getLogisticsDocType();

if(!empty($lm_no))
{
    $logistics_sql = "
        SELECT 
            *,
            (SELECT s.s_name FROM staff s WHERE s.s_no=lm.req_s_no) as req_s_name,
            (SELECT t.team_name FROM team t WHERE t.team_code=lm.req_team) as req_t_name,
            (SELECT s.s_name FROM staff s WHERE s.s_no=lm.run_s_no) as run_s_name,
            (SELECT t.team_name FROM team t WHERE t.team_code=lm.run_team) as run_t_name
        FROM logistics_management as lm
        WHERE lm_no = '{$lm_no}'
    ";
    $logistics_query    = mysqli_query($my_db, $logistics_sql);

    if(!$logistics_query)
    {
        exit("<script>alert('해당 요청서가 없습니다.');history.back()</script>");
    }

    $logistics_result = mysqli_fetch_assoc($logistics_query);

    $logistics_result['doc_type_name'] = "";
    if($logistics_result['doc_type'] > 0){
        $logistics_result['doc_type_name'] = $log_doc_type_option[$logistics_result['doc_type']];
    }

    $prd_type       = $logistics_result['prd_type'];
    $prd_type_name  = ($prd_type == '2') ? "unit" : "prd";
    $prd_list = [];
    $prd_sql  = "";
    $prd_key  = 0;
    $unit_key = 0;
    if($prd_type == "1"){
        $prd_sql  = "
            SELECT 
               *, 
               (SELECT p.title FROM product_cms p WHERE p.prd_no=lp.prd_no) as prd_name
            FROM logistics_product as lp WHERE lm_no='{$lm_no}' AND prd_type='{$prd_type}'
        ";
    }elseif($prd_type == "2"){
        $prd_sql  = "
            SELECT 
                *, 
               (SELECT p.option_name FROM product_cms_unit p WHERE p.`no`=lp.prd_no) as prd_name
            FROM logistics_product as lp WHERE lm_no='{$lm_no}' AND prd_type='{$prd_type}'";
    }

    if(!empty($prd_sql))
    {
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $prd_list[$prd['lp_no']] = $prd;
        }
    }

    if(!empty($prd_list)){
        $logistics_result['prd_list']     = $prd_list;
        $logistics_result['org_prd_list'] = implode(",", array_keys($prd_list));

        if($prd_type == "1"){
            $prd_key  = count($prd_list);
        }elseif($prd_type == "2"){
            $unit_key = count($prd_list);
        }
    }

    $logistics_result['prd_type_name']  = $prd_type_name;
    $logistics_result['prd_key']        = $prd_key;
    $logistics_result['unit_key']       = $unit_key;

    $addr_sql           = "SELECT * FROM logistics_address WHERE lm_no='{$logistics_result['lm_no']}'";
    $addr_query         = mysqli_query($my_db, $addr_sql);
    $receiver_addr_list = [];
    while($addr_result = mysqli_fetch_assoc($addr_query))
    {
        $receiver_addr_list[$addr_result['la_no']] = $addr_result;
    }

    $logistics_result['receiver_addr_list'] = $receiver_addr_list;
    $logistics_result['org_addr_list']      = implode(",", array_keys($receiver_addr_list));
    $logistics_result['receiver_key']       = !empty($receiver_addr_list) ? count($receiver_addr_list) : 1;

    if(empty($logistics_result['is_quick_memo'])){
        $logistics_result['is_quick_memo'] = 1;
    }

    if(empty($logistics_result['quick_memo'])){
        $logistics_result['quick_memo'] = "\$문서번호={$logistics_result['doc_no']}\$";
    }

    if(!empty($logistics_result['roc_file_path'])){
        $roc_file_paths = explode(".", $logistics_result['roc_file_path']);
        $logistics_result['roc_file_path_ext'] = end($roc_file_paths);
    }

    if(!empty($logistics_result['zet_file_path'])){
        $zet_file_paths = explode(".", $logistics_result['zet_file_path']);
        $logistics_result['zet_file_path_ext'] = end($zet_file_paths);
    }

    if(!empty($logistics_result['qx_file_path'])){
        $qx_file_paths = explode(".", $logistics_result['qx_file_path']);
        $logistics_result['qx_file_path_ext'] = end($qx_file_paths);
    }

    $quick_req_file_list = [];
    if(!empty($logistics_result['quick_req_file_path'])){
        $quick_req_file_list = explode(",", $logistics_result['quick_req_file_path']);
    }
    $logistics_result['quick_req_file_count'] = count($quick_req_file_list);

    $logistics_result['prev_type'] = isset($_GET['prev_type']) ? $_GET['prev_type'] : "";
    $logistics_request = $logistics_result;
}
else
{
    exit("<script>alert('해당 요청서가 없습니다.');history.back()</script>");
}

$company_model = Company::Factory();

$smarty->assign("work_state_option", getWorkStateOption());
$smarty->assign("work_state_color_option", getWorkStateOptionColor());
$smarty->assign("log_doc_type_option", $log_doc_type_option);
$smarty->assign("subject_option", getSubjectOption());
$smarty->assign("product_type_option", getProductTypeOption());
$smarty->assign("stock_type_option", getStockTypeDetailOption());
$smarty->assign("delivery_type_option", getDeliveryTypeOption());
$smarty->assign("addr_company_option", getAddrCompanyOption());
$smarty->assign("move_company_option", getMoveCompanyOption());
$smarty->assign("sales_company_option", getSalesCompanyOption());
$smarty->assign("etc_sales_company_option", getEtcSalesCompanyOption());
$smarty->assign("log_company_option", $company_model->getLogisticsList());
$smarty->assign($logistics_request);

$smarty->display('logistics_request.html');
?>