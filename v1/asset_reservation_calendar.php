<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');

# Process 처리
$process   = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'cancel_asset_reservation')
{
    $as_r_no    = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $sch_as_no  = isset($_POST['sch_as_no']) ? $_POST['sch_as_no'] : "";
    $as_no      = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $cancel_sql = "UPDATE `asset_reservation` SET work_state='4' WHERE as_r_no='{$as_r_no}'";

    if(mysqli_query($my_db, $cancel_sql)){
        exit("<script>alert('예약취소 했습니다');location.href='asset_reservation_calendar.php?sch_as_no={$sch_as_no}';</script>");
    }else {
        exit("<script>alert('예약취소에 실패했습니다');location.href='asset_reservation_calendar.php?sch_as_no={$sch_as_no}';</script>");
    }
}
elseif($process == 'modify_asset_reservation')
{
    $as_r_no    = isset($_POST['as_r_no']) ? $_POST['as_r_no'] : "";
    $sch_as_no  = isset($_POST['sch_as_no']) ? $_POST['sch_as_no'] : "";
    $as_no      = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $r_s_day    = isset($_POST['new_r_s_day']) ? $_POST['new_r_s_day'] : "";
    $r_s_hour   = isset($_POST['new_r_s_hour']) ? $_POST['new_r_s_hour'] : "";
    $r_s_min    = isset($_POST['new_r_s_min']) ? $_POST['new_r_s_min'] : "";
    $r_e_day    = isset($_POST['new_r_e_day']) ? $_POST['new_r_e_day'] : "";
    $r_e_hour   = isset($_POST['new_r_e_hour']) ? $_POST['new_r_e_hour'] : "";
    $r_e_min    = isset($_POST['new_r_e_min']) ? $_POST['new_r_e_min'] : "";
    $task_req   = isset($_POST['new_task_req']) ? addslashes(trim($_POST['new_task_req'])) : "";

    $r_s_date = $r_s_day." ".$r_s_hour.":".$r_s_min.":00";
    $r_e_date = $r_e_day." ".$r_e_hour.":".$r_e_min.":00";

    $update_sql = "UPDATE `asset_reservation` SET `task_req`='{$task_req}', r_s_date='{$r_s_date}', r_e_date='{$r_e_date}' WHERE as_r_no='{$as_r_no}'";

    if(mysqli_query($my_db, $update_sql)){
        exit("<script>alert('예약변경 했습니다');location.href='asset_reservation_calendar.php?sch_as_no={$sch_as_no}';</script>");
    }else {
        exit("<script>alert('예약변경에 실패했습니다');location.href='asset_reservation_calendar.php?sch_as_no={$sch_as_no}';</script>");
    }
}
elseif($process == 'new_asset_reservation')
{
    $sch_as_no      = isset($_POST['sch_as_no']) ? $_POST['sch_as_no'] : "";
    $new_req_no     = isset($_POST['new_req_no']) ? $_POST['new_req_no'] : "";
    $new_req_name   = isset($_POST['new_req_name']) ? $_POST['new_req_name'] : "";
    $new_as_no      = isset($_POST['new_as_no']) ? $_POST['new_as_no'] : "";
    $new_as_name    = isset($_POST['new_as_name']) ? $_POST['new_as_name'] : "";
    $new_work_state = isset($_POST['new_work_state']) ? $_POST['new_work_state'] : "";
    $new_management = isset($_POST['new_management']) ? $_POST['new_management'] : "";
    $new_manager    = isset($_POST['new_manager']) ? $_POST['new_manager'] : "";
    $new_sub_manager= isset($_POST['new_sub_manager']) ? $_POST['new_sub_manager'] : "";
    $share_type     = '3';
    $new_r_s_day    = isset($_POST['new_r_s_day']) ? $_POST['new_r_s_day'] : "";
    $new_r_s_hour   = isset($_POST['new_r_s_hour']) ? $_POST['new_r_s_hour'] : "";
    $new_r_s_min    = isset($_POST['new_r_s_min']) ? $_POST['new_r_s_min'] : "";
    $new_r_e_day    = isset($_POST['new_r_e_day']) ? $_POST['new_r_e_day'] : "";
    $new_r_e_hour   = isset($_POST['new_r_e_hour']) ? $_POST['new_r_e_hour'] : "";
    $new_r_e_min    = isset($_POST['new_r_e_min']) ? $_POST['new_r_e_min'] : "";
    $new_task_req   = isset($_POST['new_task_req']) ? addslashes(trim($_POST['new_task_req'])) : "";
    $new_regdate    = date('Y-m-d H:i:s');

    if($new_req_no == '284'){
        $new_req_name = '외부사용자';
    }

    $ins_sql = "INSERT INTO `asset_reservation` SET
        `work_state`  ='{$new_work_state}',
        `as_no`       ='{$new_as_no}',
        `management`  ='{$new_management}',
        `manager`     ='{$new_manager}',
        `sub_manager` ='{$new_sub_manager}',
        `task_req`    ='{$new_task_req}',
        `regdate`     ='{$new_regdate}',
        `req_no`      ='{$new_req_no}',
        `req_name`    ='{$new_req_name}',
        `req_date`    ='{$new_regdate}'
     ";

    $r_s_date = $new_r_s_day." ".$new_r_s_hour.":".$new_r_s_min.":00";
    $ins_sql .= ",r_s_date='{$r_s_date}'";

    $r_e_date = $new_r_e_day." ".$new_r_e_hour.":".$new_r_e_min.":00";
    $ins_sql .= ",r_e_date='{$r_e_date}'";

    if($new_work_state == '2')
    {
        $ins_sql .= ",run_no='{$session_s_no}', run_name='{$session_name}', run_date='{$new_regdate}'";
    }

    if(mysqli_query($my_db, $ins_sql))
    {
        if ($new_work_state == '1' && !!$new_manager)
        {
            $new_as_r_no = mysqli_insert_id($my_db);

            if(!!$new_as_r_no)
            {
                $return 	    = "https://work.wplanet.co.kr/v1/asset_reservation.php?sch=Y&sch_work_state=&sch_req_name=&sch_as_r_no={$new_as_r_no}";
                $message 	    = "자산 사용 요청 되었습니다.\r\n[자산 사용 내역] {$new_as_name} (요청자 : {$new_req_name})\r\n{$return}";
                $asset_chk_msg  = addslashes($message);
                $chat_ins_sql   = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$new_manager}', content='{$asset_chk_msg}', alert_type='51', alert_check='ASSET_REQUEST_{$new_as_no}_{$new_as_r_no}', regdate=now()";
                mysqli_query($my_db, $chat_ins_sql);
            }
        }

        exit("<script>alert('예약성공 했습니다');location.href='asset_reservation_calendar.php?sch_as_no={$sch_as_no}';</script>");
    }else {
        exit("<script>alert('오류가 발생했습니다 다시 시도해 주세요.');location.href='asset_reservation_calendar.php?sch_as_no={$sch_as_no}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "73";
$nav_title   = "공용설비 예약시스템";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

//공용시스템 상품 목록
$asset_share_sql   = "SELECT as_no, `name` FROM asset WHERE share_type='3' AND asset_state='1' AND as_no IS NOT NULL ORDER BY priority ASC";
$asset_share_query = mysqli_query($my_db, $asset_share_sql);
$asset_share_list  = $asset_share_as_no_list = $asset_share_reservation_list  = [];
$first_share       = "";
$asset_share_color_list = array('1' => '#000080', '2' => '#5e7e9b', '220' => '#009900', '221' => '#660099', '222' => '#800000', '223' => '#000000', '224' => '#FF0000', '225' => '#bfff00', '488' => '#FF7F00');
$idx = 0;
while($asset_share_result = mysqli_fetch_assoc($asset_share_query))
{
    if($idx == 0){
        $first_share = $asset_share_result['as_no'];
    }
    $asset_share_as_no_list[] = $asset_share_result['as_no'];
    $asset_share_list[$asset_share_result['as_no']] = $asset_share_result['name'];
    $idx++;
}

if(!!$asset_share_list)
{
    $asset_share_as_no = implode(',', $asset_share_as_no_list);

    $add_where = "`asr`.as_no IN({$asset_share_as_no}) AND `asr`.work_state IN('1','2','10')";
    $add_order = "r_s_date ASC";

    $sch_as_no   = isset($_GET['sch_as_no']) ? $_GET['sch_as_no'] : "";
    $sch_as_name = isset($asset_share_list[$sch_as_no]) ? $asset_share_list[$sch_as_no] : "";

    if(!empty($sch_as_no)){
        $add_where .= " AND as_no = '{$sch_as_no}'";
    }

    $smarty->assign('sch_as_no', $sch_as_no);
    $smarty->assign('sch_as_name', $sch_as_name);

    $asset_share_reservation_sql = "
        SELECT
            `asr`.as_r_no,
            `asr`.as_no,
            (SELECT `as`.`name` FROM asset `as` WHERE `as`.as_no = `asr`.as_no LIMIT 1) as as_name,
            `asr`.r_s_date,
            `asr`.r_e_date,
            DATE_FORMAT(`asr`.r_s_date, '%Y-%m-%d') as r_s_day,
            DATE_FORMAT(`asr`.r_s_date, '%H:%i') as r_s_time,
            DATE_FORMAT(`asr`.r_e_date, '%Y-%m-%d') as r_e_day,
            DATE_FORMAT(`asr`.r_e_date, '%d') as r_e_day_name,
            DATE_FORMAT(`asr`.r_e_date, '%H:%i') as r_e_time,
           `asr`.req_name as s_name,
           `asr`.req_no,
           `asr`.task_req
       FROM asset_reservation `asr`
       WHERE {$add_where}
       ORDER BY {$add_order}
    ";

    $asset_share_reservation_query = mysqli_query($my_db, $asset_share_reservation_sql);
    $title = "";
    $colors = getColors();
    while($asset_share_reservation = mysqli_fetch_assoc($asset_share_reservation_query))
    {
        if(!empty($sch_as_no)){
            if($asset_share_reservation['r_s_day'] != $asset_share_reservation['r_e_day']){
                $title = "{$asset_share_reservation['r_s_time']}-{$asset_share_reservation['r_e_day_name']}일 {$asset_share_reservation['r_e_time']} {$asset_share_reservation['s_name']}";
            }else{
                $title = "{$asset_share_reservation['r_s_time']}-{$asset_share_reservation['r_e_time']} {$asset_share_reservation['s_name']}";
            }
        }else{
            $title = "[{$asset_share_list[$asset_share_reservation['as_no']]}]";
        }

        if(!empty($sch_as_no)){
            $color = $colors[$asset_share_reservation['req_no']];
        }else{
            $color = $asset_share_color_list[$asset_share_reservation['as_no']];
        }
        $asset_share_reservation['title'] = $title;
        $asset_share_reservation['color'] = $color;
        $asset_share_reservation_list[] = $asset_share_reservation;
    }
}

$smarty->assign('asset_share_list', $asset_share_list);
$smarty->assign('asset_share_reservation_list', json_encode($asset_share_reservation_list));
$smarty->assign('cur_date', date('Y-m-d'));

$smarty->display('asset_reservation_calendar.html');

?>
