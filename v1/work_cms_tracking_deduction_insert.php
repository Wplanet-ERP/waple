<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');


$file_name   = $_FILES["deduction_file"]["tmp_name"];
$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$upd_sql_list = [];
for ($i = 3; $i <= $totalRow; $i++)
{
    $delivery_no = $deducated_reason = "";
    $deducated_fee = 0;


    # 주문데이터 처리용
    $order_number = $prd_no = $c_name = "";
    $qty = 0;

    $delivery_no        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    //운송장번호
    $deducated_fee      = (int)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));       //차감금액
    $deducated_reason   = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));    //차감사유

    // 운송장 번호가 없는 경우
    if(!$delivery_no){
        echo "ROW : {$i}<br/>";
        echo "배송비 차감내역 반영에 실패하였습니다.<br>운송장번호가 없습니다.<br>
            운송장번호  : {$delivery_no}<br>";
        exit;
    }

    $delivery_chk_sql    = "SELECT count(t_no) as cnt FROM work_cms_tracking WHERE delivery_no='{$delivery_no}'";
    $delivery_chk_query  = mysqli_query($my_db, $delivery_chk_sql);
    $delivery_chk_result = mysqli_fetch_assoc($delivery_chk_query);

    if($delivery_chk_result['cnt'] < 1)
    {
        echo "{$i}번째, {$delivery_no} 등록된 배송비 내역리스트가 없습니다.";
        exit;
    }

    $upd_sql_list[] = "UPDATE work_cms_tracking SET `deducted_fee`='{$deducated_fee}', `deducted_reason`='{$deducated_reason}' WHERE delivery_no='{$delivery_no}'";
}

if(!empty($upd_sql_list)){
    foreach($upd_sql_list as $upd_sql){
        mysqli_query($my_db, $upd_sql);
    }

    exit("<script>alert('배송비 차감내역 반영 되었습니다.');location.href='work_cms_tracking_list.php';</script>");
}else{
    exit("<script>alert('배송비 차감내역 반영할 데이터가 없습니다.');location.href='work_cms_tracking_list.php';</script>");
}

?>
