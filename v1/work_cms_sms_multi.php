<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_message.php');
require('inc/model/Message.php');
require('Classes/PHPExcel.php');

# 프로세스 처리
$process 			= isset($_POST['process']) ? $_POST['process'] : "";
$message_model 		= Message::Factory();
$send_phone_list    = getWapleSendPhoneList();
$send_phone 		= "1668-3620";

if ($process == "new_sms_send")
{
	$sms_msg 		= isset($_POST['temp_content']) ? addslashes($_POST['temp_content']) : "";
	$send_phone		= isset($_POST['send_phone']) ? trim($_POST['send_phone']) : "";
	$send_name		= $send_phone_list[$send_phone];
	$file_name      = $_FILES["sms_file"]["tmp_name"];

	# 엑셀처리
	$excelReader 	= PHPExcel_IOFactory::createReaderForFile($file_name);
	$excelReader->setReadDataOnly(true);
	$excel = $excelReader->load($file_name);
	$excel->setActiveSheetIndex(0);
	$objWorksheet   = $excel->getActiveSheet();
	$totalRow       = $objWorksheet->getHighestRow();
	$receiver_list	= [];

	for ($i = 2; $i <= $totalRow; $i++)
	{
		$name   = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 고객명
		$hp		= (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 고객전화번호

		if(empty($name) || empty($hp)){
			continue;
		}

		$receiver_list[] = array("name" => $name, "hp" => $hp);
	}

	if (!$send_phone) {
		$send_phone = "1668-3620";
	}

	# 문자 번호 처리
	$sms_title		= "CS 단체문자 발송";

	$cur_datetime	= date("YmdHis");
	$cm_id 			= 1;
	$c_info 		= "100"; # CS 고객전달
	$send_data_list = [];

	if(!empty($receiver_list))
	{
		foreach ($receiver_list as $receiver_data)
		{
			if(empty($receiver_data)){
				continue;
			}

			$receiver  		= $receiver_data['name'];
			$receiver_hp 	= $receiver_data['hp'];
			$chk_hp 	 	= str_replace("-","", $receiver_hp);
			$msg_id 	 	= "W{$cur_datetime}".sprintf('%04d', $cm_id++);
			$msg_content   	= str_replace("#{고객명}", $receiver, $sms_msg);
			$msg_content_len= mb_strlen(iconv('utf-8', 'euc-kr', $msg_content), '8bit');
			$msg_type 		= ($msg_content_len > 90) ? "L" : "S";

			$send_data_list[$msg_id] = array(
				"msg_id"        => $msg_id,
				"msg_type"      => $msg_type,
				"sender"        => $send_name,
				"sender_hp"     => $send_phone,
				"receiver"      => $receiver,
				"receiver_hp"   => $receiver_hp,
				"title"         => $sms_title,
				"content"       => $msg_content,
				"cinfo"         => $c_info,
			);
		}
	}


	if(!empty($send_data_list)){
		$result_data = $message_model->sendMessage($send_data_list);

		if ($result_data['result']) {
			exit("<script>alert('메시지를 발송하였습니다.');location.href='work_cms_sms_multi.php';</script>");
		}
	}

	exit("<script>alert('메세지 발송에 실패했습니다.');history.back();</script>");
}

$smarty->assign("send_phone", $send_phone);
$smarty->assign("send_phone_list", $send_phone_list);
$smarty->display('work_cms_sms_multi.html');
?>
