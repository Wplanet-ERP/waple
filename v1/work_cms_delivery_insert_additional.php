<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');
ini_set('display_errors', '-1');

require('inc/common.php');
require('ckadmin.php');
require('inc/model/WorkCms.php');
require('Classes/PHPExcel.php');

$search_url         = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
$additional_type    = (isset($_POST['additional_type'])) ? $_POST['additional_type'] : "1";
$file_name          = ($additional_type == "1") ? $_FILES["additional_file"]["tmp_name"] : $_FILES["additional_file_b"]["tmp_name"];

$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel          = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet   = $excel->getActiveSheet();
$totalRow       = $objWorksheet->getHighestRow();

$ins_deli_list           = [];
$delivery_chk_list       = [];
$delivery_excel_chk_list = [];

$ins_sql    = "INSERT INTO work_cms_delivery(`order_number`, `prd_no`, `delivery_type`, `delivery_no`) VALUES";
$comma      = "";
$ins_idx    = 0;
$total_idx  = 0;

if($additional_type == 1)
{
    for ($i = 4; $i < $totalRow; $i++)
    {
        $row_check      = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));

        if(empty($row_check)){
            continue;
        }

        $total_idx++;
        $order_number   = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 주문번호
        $prd_name       = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 상품명
        $delivery_type  = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 운송장타입
        $delivery_no    = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 운송장번호

        if(!$order_number) {
            echo "ROW : {$i}<br/>";
            echo "운송장번호 반영에 실패하였습니다.<br/>주문번호가 없습니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>";
            exit;
        }

        if(!$delivery_no){
            echo "ROW : {$i}<br/>";
            echo "운송장번호 반영에 실패하였습니다.<br>운송장번호가 없습니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>";
            exit;
        }

        $ord_sql    = "SELECT count(w_no) as cnt FROM work_cms WHERE order_number ='{$order_number}'";
        $ord_query  = mysqli_query($my_db, $ord_sql);
        $ord_result = mysqli_fetch_assoc($ord_query);

        if(!$ord_result || (isset($ord_result['cnt']) && $ord_result['cnt'] == '0'))
        {
            echo "ROW : {$i}<br/>";
            echo "운송장번호 반영에 실패하였습니다.<br>매칭되는 주문번호가 없습니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>";
            exit;
        }

        # 요기부터 변경
        $deli_sql    = "SELECT count(`no`) as cnt FROM work_cms_delivery WHERE order_number !='{$order_number}' AND delivery_no = '{$delivery_no}'";
        $deli_query  = mysqli_query($my_db, $deli_sql);
        $deli_result = mysqli_fetch_assoc($deli_query);

        if(isset($deli_result['cnt']) && $deli_result['cnt'] > 0)
        {
            echo "ROW : {$i}<br/>";
            echo "운송장번호 반영에 실패하였습니다.<br>운송장번호가 이미 존재합니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>";
            exit;
        }

        if(!$prd_name){
            echo "ROW : {$i}<br/>";
            echo "운송장번호 반영에 실패하였습니다.<br>상품명이 없습니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>
            상품명      : {$prd_name}<br>";
            exit;
        }

        $prd_sql    = "SELECT prd_no FROM product_cms WHERE title ='{$prd_name}' ORDER BY display ASC LIMIT 1";
        $prd_query  = mysqli_query($my_db, $prd_sql);
        $prd_result = mysqli_fetch_assoc($prd_query);
        $prd_no     = (isset($prd_result['prd_no']) && !empty($prd_result['prd_no'])) ? $prd_result['prd_no'] : "";

        if(!$prd_no)
        {
            echo "ROW : {$i}<br/>";
            echo "운송장번호 반영에 실패하였습니다.<br>상품이 없습니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>
            상품명      : {$prd_name}<br>";
            exit;
        }

        $chk_deli_sql    = "SELECT COUNT(`no`) as cnt FROM work_cms_delivery WHERE order_number='{$order_number}' AND `prd_no`='{$prd_no}' AND delivery_no='{$delivery_no}'";
        $chk_deli_query  = mysqli_query($my_db, $chk_deli_sql);
        $chk_deli_result = mysqli_fetch_assoc($chk_deli_query);

        if($chk_deli_result['cnt'] > 0 || isset($delivery_chk_list[$order_number][$prd_no][$delivery_no])){
            continue;
        }

        if(!isset($delivery_excel_chk_list[$delivery_no])){
            $delivery_excel_chk_list[$delivery_no] = $order_number;
        }

        if($delivery_excel_chk_list[$delivery_no] != $order_number)
        {
            echo "ROW : {$i}<br/>";
            echo "운송장번호 반영에 실패하였습니다.<br>엑셀내 동일한 운송장번호가 다른 주문번호에 있습니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>";
            exit;
        }

        $delivery_chk_list[$order_number][$prd_no][$delivery_no] = 1;
        $ins_sql   .= $comma."('{$order_number}', '{$prd_no}', '{$delivery_type}', '{$delivery_no}')";
        $comma      = " , ";
        $ins_idx++;
    }
}
elseif($additional_type == "2")
{
    for ($i = 3; $i < $totalRow; $i++)
    {
        $total_idx++;

        $delivery_no            = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 운송장번호
        $delivery_type          = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 운송장타입
        $org_delivery_no_val    = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 운송장번호
        $org_delivery_no        = str_replace("추가송장 : [", "", $org_delivery_no_val);
        $org_delivery_no        = str_replace("]", "", $org_delivery_no);

        # 기존 운송장 체크
        $chk_delivery_sql   = "
            SELECT 
                *
            FROM work_cms_delivery wcd
            WHERE wcd.delivery_no='{$org_delivery_no}'
            LIMIT 1
        ";
        $chk_delivery_query = mysqli_query($my_db, $chk_delivery_sql);
        $chk_delivery_count = mysqli_num_rows($chk_delivery_query);

        if($chk_delivery_count < 1)
        {
            echo "ROW : {$i}<br/>";
            echo "존재하지 않은 운송장이 있어 업로드에 실패하였습니다.<br/>확인 후 다시 시도해 주세요.<br/>{$i}열 운송장번호 {$org_delivery_no}<br>";
            exit;
        }

        $chk_delivery_result = mysqli_fetch_assoc($chk_delivery_query);

        # 새 운송장 체크
        $deli_sql    = "SELECT count(`no`) as cnt FROM work_cms_delivery WHERE delivery_no = '{$delivery_no}'";
        $deli_query  = mysqli_query($my_db, $deli_sql);
        $deli_result = mysqli_fetch_assoc($deli_query);

        if(isset($deli_result['cnt']) && $deli_result['cnt'] > 0)
        {
            echo "ROW : {$i}<br/>";
            echo "운송장번호 반영에 실패하였습니다.<br>운송장번호가 이미 존재합니다.<br>
            운송장번호  : {$delivery_no}<br>";
            exit;
        }

        $ins_sql   .= $comma."('{$chk_delivery_result['order_number']}', '{$chk_delivery_result['prd_no']}', '{$delivery_type}', '{$delivery_no}')";
        $comma      = " , ";
        $ins_idx++;
    }
}

if($ins_idx > 0)
{
    if (!mysqli_query($my_db, $ins_sql)){
        echo "운송장번호 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
        echo "SQL : ".$ins_sql."<br/>";
    }else{
        exit("<script>alert('총 {$total_idx}건 중 {$ins_idx}건의 운송장이 추가 적용되었습니다.');location.href='work_list_cms.php?{$search_url}';</script>");
    }
}else{
    exit("<script>alert('적용할 추가 운송장이 없습니다');location.href='work_list_cms.php?{$search_url}';</script>");
}

exit("<script>alert('송장번호가 반영 되었습니다.');location.href='work_list_cms.php?{$search_url}';</script>");

?>
