<?php
require('inc/common.php');

if (trim($_SESSION["ss_s_no"])) {
	if($_SESSION['ss_partner'] == 'nes'){
		goto_url("work_cms_visit_list.php");
	}elseif($_SESSION['ss_partner'] == 'csis'){
		goto_url("/v1/board/board_normal_list.php?b_id=mc_event");
	}elseif($_SESSION['ss_partner'] == 'seongill' || $_SESSION['ss_partner'] == 'dewbell' || $_SESSION['ss_partner'] == 'pico' || $_SESSION['ss_partner'] == 'paino'){
		goto_url("/v1/out_return_qc_list.php");
	}elseif($_SESSION['ss_partner'] == 'wp_guest'){
		goto_url("/v1/asset_reservation_calendar.php");
	}else {
		goto_url("media_commerce_utm_management.php");
	}
}

if ($user_id && $pw)
{
	sleep(1);

	$getpwd 		= substr(md5($pw), 8, 16);
	$login_sql 		= "SELECT s_no, id, s_name, team, `position`, pos_permission, permission, staff_state, my_c_type, my_c_no, partner_c_no FROM staff WHERE id = '{$user_id}' AND `pass`='{$getpwd}' AND partner_c_no IN('2304','2305','2472','2772','3438','4738','4874','5115') AND staff_state IN ('1','2') AND my_c_type='2'";
	$login_query 	= mysqli_query($my_db, $login_sql);
	$staff_info 	= mysqli_fetch_array($login_query);

	if($staff_info['id'])
	{
		$_SESSION["ss_my_c_no"]  		= $staff_info['my_c_no'];;
		$_SESSION["ss_s_no"] 	 		= $staff_info['s_no'];
		$_SESSION["ss_id"] 	 	 		= $staff_info['id'];
		$_SESSION["ss_name"] 	 		= $staff_info['s_name'];
		$_SESSION["ss_team"] 	 		= $staff_info['team'];
		$_SESSION["ss_position"] 		= $staff_info['position'];
		$_SESSION["ss_pos_permission"] 	= $staff_info['pos_permission'];
		$_SESSION["ss_permission"] 		= $staff_info['permission'];
		$_SESSION["ss_staff_state"] 	= $staff_info['staff_state'];
        $_SESSION["ss_my_c_type"] 		= $staff_info['my_c_type'];

        $partner 	 = "";
        $partner_url = "media_commerce_utm_management.php";
        switch($staff_info['partner_c_no']){
			case '3438':
                $partner 	 = "dansaek";
                $partner_url = "media_commerce_utm_management.php";
                break;
            case '4738':
                $partner 	 = "nes";
				$partner_url = "work_cms_visit_list.php";
                break;
			case '4874':
				$partner 	 = "csis";
				$partner_url = "/v1/board/board_normal_list.php?b_id=mc_event";
				break;
			case '2304':
				$partner 	 = "seongill";
				$partner_url = "/v1/out_return_qc_list.php";
				break;
			case '2305':
				$partner 	 = "dewbell";
				$partner_url = "/v1/out_return_qc_list.php";
				break;
			case '2772':
				$partner 	 = "pico";
				$partner_url = "/v1/out_return_qc_list.php";
				break;
			case '2472':
				$partner 	 = "paino";
				$partner_url = "/v1/out_return_qc_list.php";
				break;
			case '5115':
				$partner 	 = "wp_guest";
				$partner_url = "/v1/asset_reservation_calendar.php";
				break;
		}

        $_SESSION["ss_partner"] = $partner;

        goto_url($partner_url);
	}else{
		alert("정확한 정보를 입력하여주세요!","partners_login.php");
	}
}

$smarty->assign("nobg","style='background:none'");
$smarty->display('partners_login.html');
?>
