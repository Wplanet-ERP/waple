<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/product_cms.php');
require('inc/helper/product_cms_stock.php');
require('inc/model/ProductCmsStock.php');

$process 	= (isset($_POST['process'])) ? $_POST['process'] : "";

if($process == "f_warehouse")
{
	$no		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	$sql 	= "UPDATE product_cms_unit_management pcum SET pcum.warehouse = '{$value}' WHERE pcum.no = '{$no}'";

	if (mysqli_query($my_db, $sql)){
		echo "당기출고 창고가 저장 되었습니다.";
	}else{
		echo "당기출고 창고 저장에 실패 하였습니다.";
	}
	exit;
}



// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where = " 1=1 ";

$sch_no 			= isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_sup_c_name 	= isset($_GET['sch_sup_c_name']) ? $_GET['sch_sup_c_name'] : "";
$sch_ord_s_name 	= isset($_GET['sch_ord_s_name']) ? $_GET['sch_ord_s_name'] : "";
$sch_display		= isset($_GET['sch_display']) ? $_GET['sch_display']:"1";
$sch_sku			= isset($_GET['sch_sku']) ? $_GET['sch_sku']:"";
$sch_option_name	= isset($_GET['sch_option_name']) ? $_GET['sch_option_name']:"";
$sch_description 	= isset($_GET['sch_description']) ? $_GET['sch_description'] : "";
$sch_is_load 		= isset($_GET['sch_is_load']) ? $_GET['sch_is_load'] : "";


if (!empty($sch_no)) {
	$add_where .= " AND pcu.no='" . $sch_no . "'";
	$smarty->assign("sch_no", $sch_no);
}

if (!empty($sch_sup_c_name)) {
	$add_where .= " AND pcu.sup_c_no IN (SELECT c.c_no FROM company c WHERE c.c_name LIKE '%{$sch_sup_c_name}%' AND c.corp_kind='2')";
	$smarty->assign("sch_sup_c_name", $sch_sup_c_name);
}

if (!empty($sch_display)) {
	if($sch_display != "all"){
		$add_where .= " AND pcu.display = '{$sch_display}'";
	}
	$smarty->assign("sch_display", $sch_display);
}

if (!empty($sch_option_name)) {
	$add_where .= " AND pcu.option_name LIKE '%{$sch_option_name}%'";
	$smarty->assign("sch_option_name", $sch_option_name);
}

if (!empty($sch_description)) {
	$add_where .= " AND pcu.description LIKE '%{$sch_description}%'";
	$smarty->assign("sch_description", $sch_description);
}

if (!empty($sch_ord_s_name)) {
	$add_where .= " AND (SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_s_no) like '%{$sch_ord_s_name}%'";
	$smarty->assign("sch_ord_s_name", $sch_ord_s_name);
}

if (!empty($sch_sku)) {
	$add_where .= " AND pcu.`no` IN(SELECT pcum.prd_unit FROM product_cms_unit_management pcum WHERE pcum.sku LIKE '%{$sch_sku}%' AND pcum.log_c_no='2890')";
	$smarty->assign("sch_sku", $sch_sku);
}

if (!empty($sch_is_load)) {
	if($sch_is_load == '1'){
		$add_where .= " AND (SELECT sub.pl_no FROM product_cms_load as sub WHERE sub.pl_no IN(SELECT DISTINCT plu.pl_no FROM product_cms_load_unit as plu WHERE plu.unit_no=pcu.`no`) AND sub.display='1' LIMIT 1) != ''";
	}elseif($sch_is_load == '2'){
		$add_where .= " AND (SELECT sub.pl_no FROM product_cms_load as sub WHERE sub.pl_no IN(SELECT DISTINCT plu.pl_no FROM product_cms_load_unit as plu WHERE plu.unit_no=pcu.`no`) AND sub.display='1' LIMIT 1) IS NULL";
	}

	$smarty->assign("sch_is_load", $sch_is_load);
}


// 정렬순서 토글 & 필드 지정
$add_orderby = "pcu.no DESC";
$order=isset($_GET['od'])?$_GET['od']:"";
$order_type=isset($_GET['by'])?$_GET['by']:"";

if($order_type == '2'){
	$toggle = "DESC";
}else{
	$toggle = "ASC";
}

$order_field=array('','priority','option_name','sup_c_name');

if($order && $order<4) {
	$add_orderby = " ISNULL($order_field[$order]) ASC, $order_field[$order] $toggle";
	$smarty->assign("order",$order);
	$smarty->assign("order_type",$order_type);
}


# 전체 게시물 수
$product_unit_total_sql 	= "SELECT count(*) AS total_count FROM product_cms_unit pcu WHERE {$add_where}";
$product_unit_total_query	= mysqli_query($my_db, $product_unit_total_sql);
$product_unit_total_result	= mysqli_fetch_array($product_unit_total_query);
$product_unit_total 		= $product_unit_total_result['total_count'];

# 페이징 처리
$pages 		 = isset($_GET['page']) ?intval($_GET['page']) : 1;
$od_list_num = isset($_GET['od_list_num']) ?intval($_GET['od_list_num']) : 20;

$num	 	= $od_list_num;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($product_unit_total/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$search_url = getenv("QUERY_STRING");
$pagelist	= pagelist($pages, "with_product_cms_unit.php", $pagenum, $search_url);

$smarty->assign("total_num", $product_unit_total);
$smarty->assign("od_list_num",$od_list_num);
$smarty->assign("page", $pages);
$smarty->assign("search_url", $search_url);
$smarty->assign("pagelist", $pagelist);

# 리스트 쿼리
$product_cms_unit_sql = "
	SELECT
		*,
		(SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcu.sup_c_no) as sup_c_name,
	   	(SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcu.brand) as brand_name,
		(SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_s_no) as ord_s_name,
	    (SELECT sub.q_no FROM quick_search as sub WHERE sub.`type`='cms_unit' AND sub.s_no='{$session_s_no}' AND sub.prd_no=pcu.`no`) AS quick_prd,
	    (SELECT sub.pl_no FROM product_cms_load as sub WHERE sub.pl_no IN(SELECT DISTINCT plu.pl_no FROM product_cms_load_unit as plu WHERE plu.unit_no=pcu.`no`) AND sub.display='1' LIMIT 1) as pl_no
	FROM product_cms_unit pcu
	WHERE {$add_where}
	ORDER BY {$add_orderby}
	LIMIT {$offset}, {$num}
";
$product_cms_unit_query = mysqli_query($my_db, $product_cms_unit_sql);
$product_cms_unit_list 	= [];
$stock_model			= ProductCmsStock::Factory();
$log_warehouse_list		= $stock_model->getWarehouseBaseList();
$type_option			= getUnitTypeOption();
while($product_cms_unit = mysqli_fetch_array($product_cms_unit_query))
{
	$product_cms_unit["type_name"] 		= isset($type_option[$product_cms_unit['type']]) ? $type_option[$product_cms_unit['type']] : "";
	$product_cms_unit["sup_price"] 		= number_format($product_cms_unit['sup_price']);
	$product_cms_unit["sup_price_vat"] 	= number_format($product_cms_unit['sup_price_vat']);

	$manage_list = [];
	$unit_manage_sql 	= "
			SELECT 
			    *
			FROM product_cms_unit_management pcum 
			WHERE prd_unit = '{$product_cms_unit['no']}' ANd log_c_no='2809'
		";
	$unit_manage_query 	= mysqli_query($my_db, $unit_manage_sql);
	while($unit_manage = mysqli_fetch_assoc($unit_manage_query))
	{
		$unit_manage['warehouse_option'] = isset($log_warehouse_list[$unit_manage['log_c_no']]) ? $log_warehouse_list[$unit_manage['log_c_no']] : [];
		$manage_list[] = $unit_manage;
	}
	$product_cms_unit['manage_list'] = $manage_list;

	$product_cms_unit_list[] = $product_cms_unit;
}

$smarty->assign("is_product_load_option", getIsProductLoadOption());
$smarty->assign("product_cms_unit_list", $product_cms_unit_list);

$smarty->display('with_product_cms_unit.html');
?>
