<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:G1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A2:A3");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("B2:B3");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("C2:C3");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("D2:D3");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("E2:E3");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("F2:F3");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("G2:G3");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("H2:I2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("J2:K2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("L2:M2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("N2:O2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("P2:Q2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("R2:S2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("T2:U2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("V2:W2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("X2:Y2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("Z2:AA2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("AB2:AC2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("AD2:AE2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("AF2:AF3");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("AG2:AG3");

$lfcr = chr(10) ;

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', "업체명/브랜드명")
    ->setCellValue('B2', "와이즈 구성품명")
    ->setCellValue('C2', "물류업체")
    ->setCellValue('D2', "창고")
    ->setCellValue('E2', "재고관리코드(SKU)")
    ->setCellValue('F2', "확정 원가{$lfcr}(VAT별도)")
    ->setCellValue('G2', "확정 원가{$lfcr}(VAT별도){$lfcr}입력현황")
    ->setCellValue('H2', "기초재고")
    ->setCellValue('H3', "수량")
    ->setCellValue('I3', "금액")
    ->setCellValue('J2', "구매입고")
    ->setCellValue('J3', "수량")
    ->setCellValue('K3', "금액")
    ->setCellValue('L2', "이동입고")
    ->setCellValue('L3', "수량")
    ->setCellValue('M3', "금액")
    ->setCellValue('N2', "반품입고")
    ->setCellValue('N3', "수량")
    ->setCellValue('O3', "금액")
    ->setCellValue('P2', "기타입고")
    ->setCellValue('P3', "수량")
    ->setCellValue('Q3', "금액")
    ->setCellValue('R2', "입고합계")
    ->setCellValue('R3', "수량")
    ->setCellValue('S3', "금액")
    ->setCellValue('T2', "판매반출")
    ->setCellValue('T3', "수량")
    ->setCellValue('U3', "금액")
    ->setCellValue('V2', "이동반출")
    ->setCellValue('V3', "수량")
    ->setCellValue('W3', "금액")
    ->setCellValue('X2', "타계정반출")
    ->setCellValue('X3', "수량")
    ->setCellValue('Y3', "금액")
    ->setCellValue('Z2', "기타반출")
    ->setCellValue('Z3', "수량")
    ->setCellValue('AA3', "금액")
    ->setCellValue('AB2', "반출합계")
    ->setCellValue('AB3', "수량")
    ->setCellValue('AC3', "금액")
    ->setCellValue('AD2', "기말재고")
    ->setCellValue('AD3', "수량")
    ->setCellValue('AE3', "금액")
    ->setCellValue('AF2', "NUM")
    ->setCellValue('AG2', "평균원가")
;

$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFF2CC');
$objPHPExcel->getActiveSheet()->getStyle('R1:S1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFB2DDEF');
$objPHPExcel->getActiveSheet()->getStyle('AB1:AC1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFCE4D6');
$objPHPExcel->getActiveSheet()->getStyle('AD1:AE1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFCCCCCC');
$objPHPExcel->getActiveSheet()->getStyle('A2:I3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFCCCCCC');
$objPHPExcel->getActiveSheet()->getStyle('J2:S3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF9BC2EF');
$objPHPExcel->getActiveSheet()->getStyle('T2:AC3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFF4B084');
$objPHPExcel->getActiveSheet()->getStyle('AD2:AE3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFCCCCCC');

# 검색
$add_confirm_where      = "1=1 AND `base_type`='start'";
$add_report_where       = "1=1";
$sch_cur_month          = date("Y-m");
$sch_staff_type         = isset($_GET['sch_staff_type']) ? $_GET['sch_staff_type'] : "1";
$sch_move_date          = isset($_GET['sch_move_date']) ? $_GET['sch_move_date'] : date('Y-m', strtotime("{$sch_cur_month} -1 months"));
$sch_brand              = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_option_name        = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_log_c_no           = isset($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "";
$sch_warehouse          = isset($_GET['sch_warehouse']) ? $_GET['sch_warehouse'] : "";
$sch_warehouse_all      = isset($_GET['sch_warehouse_all']) ? $_GET['sch_warehouse_all'] : "";
$sch_sku                = isset($_GET['sch_sku']) ? $_GET['sch_sku'] : "";
$sch_not_empty          = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : 1;
$sch_is_commerce        = isset($_GET['sch_is_commerce']) ? $_GET['sch_is_commerce'] : "";
$sch_only_qty           = isset($_GET['sch_only_qty']) ? $_GET['sch_only_qty'] : "";
$sch_not_empty_except   = isset($_GET['sch_not_empty_except']) ? $_GET['sch_not_empty_except'] : "";
$search_url             = getenv("QUERY_STRING");
$with_log_c_no          = "2809";

if(empty($search_url))
{
    if($sch_staff_type == 1){
        $sch_only_qty       = 1;
    } elseif($sch_staff_type == 2){

    } elseif($sch_staff_type == 3){
        $sch_warehouse_all  = 1;
        $sch_log_c_no       = $with_log_c_no;
    }
}

if(!empty($sch_brand)){
    $add_confirm_where  .= " AND rs.brand = '{$sch_brand}'";
}

if(!empty($sch_option_name)){
    $add_confirm_where  .= " AND rs.option_name LIKE '%{$sch_option_name}%'";
}

if(!empty($sch_log_c_no)){
    $add_confirm_where  .= " AND rs.log_c_no = '{$sch_log_c_no}'";
    $add_report_where   .= " AND pcsr.log_c_no = '{$sch_log_c_no}'";
}

if(!empty($sch_warehouse)){
    $add_confirm_where  .= " AND rs.warehouse LIKE '%{$sch_warehouse}%'";
}

if(!empty($sch_sku)){
    $add_confirm_where  .= " AND rs.sku = '{$sch_sku}'";
}

if(!empty($sch_move_date)){
    $add_confirm_where  .=  " AND rs.base_mon='{$sch_move_date}'";
}

if(!empty($sch_is_commerce)){
    if($sch_is_commerce == '1'){
        $add_confirm_where  .=  " AND rs.prd_unit IN(SELECT DISTINCT co.option_no FROM commerce_order co LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no` WHERE cos.display='1' AND DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1')";
    }else{
        $add_confirm_where  .=  " AND rs.prd_unit NOT IN(SELECT DISTINCT co.option_no FROM commerce_order co LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no` WHERE cos.display='1' AND DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1')";
    }
}

# 재고자산수불부 쿼리
$total_in_base_qty = $total_in_base_price = $total_out_base_qty = $total_out_base_price = 0;
$total_in_point_qty = $total_in_point_price = $total_in_move_qty = $total_in_move_price = $total_in_return_qty = $total_in_return_price = $total_in_etc_qty = $total_in_etc_price = $total_in_qty = $total_in_price = 0;
$total_out_point_qty = $total_out_point_price = $total_out_move_qty = $total_out_move_price = $total_out_return_qty = $total_out_return_price = $total_out_etc_qty = $total_out_etc_price = $total_out_qty = $total_out_price = 0;

# 회수리스트 처리
$return_list      = [];
$return_prd_sql   = "SELECT `option`, SUM(quantity) as qty FROM work_cms_return_unit WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_return wcr WHERE wcr.return_state='6' AND DATE_FORMAT(wcr.return_date,'%Y-%m')='{$sch_move_date}') AND `type` IN(1,2,3,4) GROUP BY `option`";
$return_prd_query = mysqli_query($my_db, $return_prd_sql);
while($return_prd = mysqli_fetch_assoc($return_prd_query))
{
    $return_list[$return_prd['option']] += $return_prd['qty'];
}

# 쿼리용 날짜
$sch_move_s_date    = $sch_move_date."-01";
$end_day            = date('t', strtotime($sch_move_date));
$sch_move_e_date    = $sch_move_date."-".$end_day;

# 재고이동 리스트
$stock_move_list   = [];
$stock_move_sql     = "SELECT prd_unit, in_warehouse, out_warehouse, in_qty, out_qty FROM product_cms_stock_transfer pcst WHERE (pcst.move_date BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}')";
$stock_move_query   = mysqli_query($my_db, $stock_move_sql);
while($stock_move = mysqli_fetch_assoc($stock_move_query))
{
    $stock_move_list[$stock_move['prd_unit']][$stock_move['in_warehouse']]["in"]   += $stock_move['in_qty'];
    $stock_move_list[$stock_move['prd_unit']][$stock_move['out_warehouse']]["out"] += $stock_move['out_qty'];
}

# 입고/반출 리스트
$stock_list         = [];
$stock_report_sql   = "SELECT * FROM product_cms_stock_report pcsr WHERE {$add_report_where} AND (pcsr.regdate BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}') AND pcsr.confirm_state > 0";
$stock_report_query = mysqli_query($my_db, $stock_report_sql);
while($stock_report = mysqli_fetch_assoc($stock_report_query))
{
    $stock_kind     = "";
    $stock_dev_kind = "";
    switch($stock_report['confirm_state']){
        case "1":
        case "2":
            $stock_kind = "in_point";
            break;
        case "3":
            if($stock_report['state'] == "기간이동입고"){
                $stock_dev_kind = "in_move";
            }else{
                $stock_kind = "in_move";
            }
            break;
        case "4":
            $stock_kind = "in_etc";
            break;
        case "5":
            if($stock_report['state'] == "기간반품입고"){
                $stock_dev_kind = "in_return";
            }else{
                $stock_kind = "in_return";
            }
            break;
        case "6":
            if($stock_report['state'] == "기간이동반출"){
                $stock_dev_kind = "out_move";
            }else{
                $stock_kind = "out_move";
            }
            break;
        case "8":
            $stock_kind = "out_etc";
            break;
        case "9":
            if($stock_report['state'] == "기간판매반출"){
                $stock_dev_kind = "out_point";
            }else{
                $stock_kind = "out_point";
            }
            break;
        case "10":
            $stock_kind = "out_return";
            break;
        case "11":
            $stock_dev_kind = "in_base";
            break;
        case "12":
            $stock_dev_kind = "out_base";
            break;
    }

    if(!empty($stock_kind))
    {
        if(!isset($stock_list[$stock_report['prd_unit']][$stock_report['stock_type']])){
            $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']] = array(
                "in_point"      => 0,
                "in_move"       => 0,
                "in_etc"        => 0,
                "in_return"     => 0,
                "out_point"     => 0,
                "out_move"      => 0,
                "out_return"    => 0,
                "out_etc"       => 0,
            );
        }

        $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']][$stock_kind] += $stock_report['stock_qty'];
    }
}

# 실시간 확정원가
$commerce_order_list        = [];
$commerce_order_tmp_list    = [];
$commerce_order_sql         = "
    SELECT 
        co.set_no,
        co.option_no,
        co.unit_price, 
        co.quantity,
        co.supply_price,
        co.total_price,
        co.vat,
        co.group,
        (SELECT SUM(sub.supply_price) FROM commerce_order sub WHERE sub.set_no=co.set_no AND sub.`group`=co.`group`) as sum_sup_price,
        (SELECT SUM(sub.price) FROM commerce_order_etc sub WHERE sub.set_no=co.set_no AND sub.group_no=co.`group`) as sum_etc_price,
        (SELECT g.usd_rate FROM commerce_order_group g WHERE g.set_no=co.set_no AND g.group_no=co.`group`) as group_usd_rate,
        `cos`.sup_loc_type
    FROM commerce_order `co`
    LEFT JOIN commerce_order_set `cos` ON `cos`.no=`co`.set_no
    WHERE DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1' AND (SELECT pcsr.confirm_state FROM product_cms_stock_report pcsr WHERE pcsr.ord_no=co.`no`) IN(1,2)
";
$commerce_order_query = mysqli_query($my_db, $commerce_order_sql);
while($commerce_order = mysqli_fetch_assoc($commerce_order_query))
{
    $total_price    = $commerce_order['total_price'];
    $quantity       = $commerce_order['quantity'];

    if(isset($commerce_order_tmp_list[$commerce_order['option_no']])){
        $commerce_order_tmp_list[$commerce_order['option_no']]['total_price'] += $total_price;
        $commerce_order_tmp_list[$commerce_order['option_no']]['total_qty']   += $quantity;
    }else{
        $commerce_order_tmp_list[$commerce_order['option_no']] = array(
            "total_price"   => $total_price,
            "total_qty"     => $quantity
        );
    }
}

if(!empty($commerce_order_tmp_list))
{
    foreach($commerce_order_tmp_list as $key => $comm_tmp)
    {
        $ord_qty = $comm_tmp['total_qty'] > 0 ? $comm_tmp['total_qty'] : 1;
        $commerce_order_list[$key] = round($comm_tmp['total_price']/$ord_qty, 1);
    }
}

# 재고자산수불부 쿼리
$product_receipt_sql = "
    SELECT
        *
    FROM
    (
        SELECT
            *,
            (SELECT pcum.is_except_stock FROM product_cms_unit_management pcum WHERE pcum.prd_unit=rs.prd_unit AND pcum.log_c_no=rs.log_c_no) as log_except_stock
        FROM product_cms_stock_confirm as rs 
        WHERE {$add_confirm_where}
    ) as rs
    WHERE log_except_stock != '1' 
    ORDER BY rs.option_name ASC, rs.warehouse ASC
";
$product_receipt_query  = mysqli_query($my_db, $product_receipt_sql);
$product_receipt_list   = [];
while($product_receipt = mysqli_fetch_assoc($product_receipt_query))
{
    $stock_list_data                        = isset($stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];
    $move_list_data                         = isset($stock_move_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_move_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];
    $live_org_price                         = isset($commerce_order_list[$product_receipt['prd_unit']]) ? $commerce_order_list[$product_receipt['prd_unit']] : 0;

    $product_receipt['in_base_qty']         = $product_receipt['qty'];
    $product_receipt['in_point_qty']        = !empty($stock_list_data) ? $stock_list_data['in_point'] : 0;
    $product_receipt['in_move_sub_qty']     = !empty($stock_list_data) ? $stock_list_data['in_move'] : 0;
    $product_receipt['in_org_move_qty']     = !empty($move_list_data) && isset($move_list_data["in"]) ? $move_list_data["in"] : 0;
    $product_receipt['in_move_qty']         = $product_receipt['in_org_move_qty'] + $product_receipt['in_move_sub_qty'];
    $product_receipt['in_etc_qty']          = !empty($stock_list_data) ? $stock_list_data['in_etc'] : 0;
    $product_receipt['in_return_sub_qty']   = ($product_receipt['warehouse'] == "2-3 검수대기창고(반품건)" && isset($return_list[$product_receipt['prd_unit']])) ? $return_list[$product_receipt['prd_unit']] : 0;
    $product_receipt['in_return_org_qty']   = !empty($stock_list_data) ? $stock_list_data['in_return'] : 0;
    $product_receipt['in_return_qty']       = $product_receipt['in_return_org_qty'] + $product_receipt['in_return_sub_qty'];

    $product_receipt['live_org_price']      = $live_org_price;
    $product_receipt['in_base_price']       = $product_receipt['base_price'] * $product_receipt['in_base_qty'];
    $product_receipt['in_point_price']      = $product_receipt['org_price'] * $product_receipt['in_point_qty'];
    $product_receipt['in_move_price']       = $product_receipt['avg_price'] * $product_receipt['in_move_qty'];
    $product_receipt['in_return_price']     = $product_receipt['org_price'] * $product_receipt['in_return_qty'];
    $product_receipt['in_etc_price']        = $product_receipt['org_price'] * $product_receipt['in_etc_qty'];
    $product_receipt['in_qty']              = $product_receipt['in_point_qty'] + $product_receipt['in_move_qty'] + $product_receipt['in_return_qty'] + $product_receipt['in_etc_qty'];
    $product_receipt['in_price']            = $product_receipt['in_point_price'] + $product_receipt['in_move_price'] + $product_receipt['in_return_price'] + $product_receipt['in_etc_price'];

    $product_receipt['out_point_qty']       = !empty($stock_list_data) ? $stock_list_data['out_point']*-1 : 0;
    $product_receipt['out_org_move_qty']    = !empty($move_list_data) && isset($move_list_data["out"]) ? $move_list_data["out"] : 0;
    $product_receipt['out_move_sub_qty']    = !empty($stock_list_data) ? $stock_list_data['out_move']*-1 : 0;
    $product_receipt['out_move_qty']        = $product_receipt['out_org_move_qty'] + $product_receipt['out_move_sub_qty'];
    $product_receipt['out_return_qty']      = !empty($stock_list_data) ? $stock_list_data['out_return']*-1 : 0;
    $product_receipt['out_etc_qty']         = !empty($stock_list_data) ? $stock_list_data['out_etc']*-1 : 0;
    $product_receipt['out_qty']             = $product_receipt['out_point_qty'] + $product_receipt['out_move_qty'] + $product_receipt['out_return_qty'] + $product_receipt['out_etc_qty'];
    $product_receipt['out_base_qty']        = $product_receipt['in_base_qty'] + $product_receipt['in_qty'] - $product_receipt['out_qty'];

    # 판매반출 금액 계산
    $sales_price                            = ($product_receipt['in_base_qty']+$product_receipt['in_qty'] > 0) ? (($product_receipt['in_base_price']+$product_receipt['in_price']) / ($product_receipt['in_base_qty']+$product_receipt['in_qty'])) : 0;
    $product_receipt['out_point_price']     = $sales_price * $product_receipt['out_point_qty'];
    $product_receipt['out_move_price']      = $product_receipt['avg_price'] * $product_receipt['out_move_qty'];
    $product_receipt['out_return_price']    = $sales_price * $product_receipt['out_return_qty'];
    $product_receipt['out_etc_price']       = $sales_price * $product_receipt['out_etc_qty'];
    $product_receipt['out_price']           = $product_receipt['out_point_price'] + $product_receipt['out_move_price'] + $product_receipt['out_return_price'] + $product_receipt['out_etc_price'];
    $product_receipt['out_base_price']      = $product_receipt['in_base_price'] + $product_receipt['in_price'] - $product_receipt['out_price'];

    if($sch_not_empty_except == '1' && ($product_receipt['in_qty'] == 0 && $product_receipt['out_qty'] == 0)){
        continue;
    }
    elseif($sch_not_empty == '1' && (
            ($product_receipt['in_base_price'] == 0 && $product_receipt['in_base_qty'] == 0)
            && ($product_receipt['in_point_price'] == 0 && $product_receipt['in_point_qty'] == 0)
            && ($product_receipt['in_move_price'] == 0 && $product_receipt['in_move_qty'] == 0 && $product_receipt['in_move_qty_dev'] == 0)
            && ($product_receipt['in_return_price'] == 0 && $product_receipt['in_return_qty'] == 0)
            && ($product_receipt['in_etc_price'] == 0 && $product_receipt['in_etc_qty'] == 0)
            && ($product_receipt['in_price'] == 0 && $product_receipt['in_qty'] == 0)
            && ($product_receipt['out_point_price'] == 0 && $product_receipt['out_point_qty'] == 0)
            && ($product_receipt['out_move_price'] == 0 && $product_receipt['out_move_qty'] == 0 && $product_receipt['out_move_sub_qty'] == 0)
            && ($product_receipt['out_return_price'] == 0 && $product_receipt['out_return_qty'] == 0)
            && ($product_receipt['out_etc_price'] == 0 && $product_receipt['out_etc_qty'] == 0)
            && ($product_receipt['out_price'] == 0 && $product_receipt['out_qty']  == 0)
            && ($product_receipt['out_base_price'] == 0 && $product_receipt['out_base_qty']  == 0)
        )
    ){
        continue;
    }

    $product_receipt_list[] = $product_receipt;
    $total_in_base_qty      += $product_receipt['in_base_qty'];
    $total_in_base_price    += $product_receipt['in_base_price'];
    $total_in_point_qty     += $product_receipt['in_point_qty'];
    $total_in_point_price   += $product_receipt['in_point_price'];
    $total_in_move_qty      += $product_receipt['in_move_qty'];
    $total_in_move_price    += $product_receipt['in_move_price'];
    $total_in_return_qty    += $product_receipt['in_return_qty'];
    $total_in_return_price  += $product_receipt['in_return_price'];
    $total_in_etc_qty       += $product_receipt['in_etc_qty'];
    $total_in_etc_price     += $product_receipt['in_etc_price'];
    $total_in_qty           += $product_receipt['in_qty'];
    $total_in_price         += $product_receipt['in_price'];
    $total_out_point_qty    += $product_receipt['out_point_qty'];
    $total_out_point_price  += $product_receipt['out_point_price'];
    $total_out_move_qty     += $product_receipt['out_move_qty'];
    $total_out_move_price   += $product_receipt['out_move_price'];
    $total_out_return_qty   += $product_receipt['out_return_qty'];
    $total_out_return_price += $product_receipt['out_return_price'];
    $total_out_etc_qty      += $product_receipt['out_etc_qty'];
    $total_out_etc_price    += $product_receipt['out_etc_price'];
    $total_out_qty          += $product_receipt['out_qty'];
    $total_out_price        += $product_receipt['out_price'];

}
$total_out_base_qty     = $total_in_base_qty+$total_in_qty-$total_out_qty;
$total_out_base_price   = $total_in_base_price+$total_in_price-$total_out_price;

# 합계
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue("A1", "합계")
    ->setCellValue("H1", number_format($total_in_base_qty))
    ->setCellValue("I1", "₩".number_format($total_in_base_price))
    ->setCellValue("J1", number_format($total_in_point_qty))
    ->setCellValue("K1", "₩".number_format($total_in_point_price))
    ->setCellValue("L1", number_format($total_in_move_qty))
    ->setCellValue("M1", "₩".number_format($total_in_move_price))
    ->setCellValue("N1", number_format($total_in_return_qty))
    ->setCellValue("O1", "₩".number_format($total_in_return_price))
    ->setCellValue("P1", number_format($total_in_etc_qty))
    ->setCellValue("Q1", "₩".number_format($total_in_etc_price))
    ->setCellValue("R1", number_format($total_in_qty))
    ->setCellValue("S1", "₩".number_format($total_in_price))
    ->setCellValue("T1", number_format($total_out_point_qty))
    ->setCellValue("U1", "₩".number_format($total_out_point_price))
    ->setCellValue("V1", number_format($total_out_move_qty))
    ->setCellValue("W1", "₩".number_format($total_out_move_price))
    ->setCellValue("X1", number_format($total_out_return_qty))
    ->setCellValue("Y1", "₩".number_format($total_out_return_price))
    ->setCellValue("Z1", number_format($total_out_etc_qty))
    ->setCellValue("AA1", "₩".number_format($total_out_etc_price))
    ->setCellValue("AB1", number_format($total_out_qty))
    ->setCellValue("AC1", "₩".number_format($total_out_price))
    ->setCellValue("AD1", number_format($total_out_base_qty))
    ->setCellValue("AE1", "₩".number_format($total_out_base_price))
;

$idx = 4;
if($product_receipt_list){
    foreach($product_receipt_list as $product_receipt)
    {
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $product_receipt["brand_name"])
            ->setCellValue("B{$idx}", $product_receipt["option_name"])
            ->setCellValue("C{$idx}", $product_receipt["log_company"])
            ->setCellValue("D{$idx}", $product_receipt["warehouse"])
            ->setCellValue("E{$idx}", $product_receipt["sku"])
            ->setCellValue("F{$idx}", "₩".number_format($product_receipt["live_org_price"]))
            ->setCellValue("G{$idx}", "₩".number_format($product_receipt["org_price"]))
            ->setCellValue("H{$idx}", number_format($product_receipt["in_base_qty"]))
            ->setCellValue("I{$idx}", "₩".number_format($product_receipt["in_base_price"]))
            ->setCellValue("J{$idx}", number_format($product_receipt["in_point_qty"]))
            ->setCellValue("K{$idx}", "₩".number_format($product_receipt["in_point_price"]))
            ->setCellValue("L{$idx}", number_format($product_receipt["in_move_qty"]))
            ->setCellValue("M{$idx}", "₩".number_format($product_receipt["in_move_price"]))
            ->setCellValue("N{$idx}", number_format($product_receipt["in_return_qty"]))
            ->setCellValue("O{$idx}", "₩".number_format($product_receipt["in_return_price"]))
            ->setCellValue("P{$idx}", number_format($product_receipt["in_etc_qty"]))
            ->setCellValue("Q{$idx}", "₩".number_format($product_receipt["in_etc_price"]))
            ->setCellValue("R{$idx}", number_format($product_receipt["in_qty"]))
            ->setCellValue("S{$idx}", "₩".number_format($product_receipt["in_price"]))
            ->setCellValue("T{$idx}", number_format($product_receipt["out_point_qty"]))
            ->setCellValue("U{$idx}", "₩".number_format($product_receipt["out_point_price"]))
            ->setCellValue("V{$idx}", number_format($product_receipt["out_move_qty"]))
            ->setCellValue("W{$idx}", "₩".number_format($product_receipt["out_move_price"]))
            ->setCellValue("X{$idx}", number_format($product_receipt["out_return_qty"]))
            ->setCellValue("Y{$idx}", "₩".number_format($product_receipt["out_return_price"]))
            ->setCellValue("Z{$idx}", number_format($product_receipt["out_etc_qty"]))
            ->setCellValue("AA{$idx}", "₩".number_format($product_receipt["out_etc_price"]))
            ->setCellValue("AB{$idx}", number_format($product_receipt["out_qty"]))
            ->setCellValue("AC{$idx}", "₩".number_format($product_receipt["out_price"]))
            ->setCellValue("AD{$idx}", number_format($product_receipt["out_base_qty"]))
            ->setCellValue("AE{$idx}", "₩".number_format($product_receipt["out_base_price"]))
            ->setCellValue("AF{$idx}", $product_receipt["prd_key"])
            ->setCellValue("AG{$idx}", $product_receipt["avg_price"])
        ;
        
        $idx++;
    }
    $idx--;

    $objPHPExcel->getActiveSheet()->getStyle("B4:B{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFB2DDEF');
    $objPHPExcel->getActiveSheet()->getStyle("F4:G{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFF2CC');
    $objPHPExcel->getActiveSheet()->getStyle("H4:I{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFDDDDDD');
    $objPHPExcel->getActiveSheet()->getStyle("R4:S{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFB2DDEF');
    $objPHPExcel->getActiveSheet()->getStyle("AB4:AC{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFCE4D6');
    $objPHPExcel->getActiveSheet()->getStyle("AD4:AE{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFDDDDDD');
}


$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:AG{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:AG3")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:AG{$idx}")->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A1:AG{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:AG{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A2:AG3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("C4:C{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A4:A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("B4:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("D4:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("E4:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("F2:F3")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("G2:G3")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("A2:A{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("B2:B{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(8);

$excel_filename = $sch_move_date." 재고자산 수불부";
$objPHPExcel->getActiveSheet()->setTitle($excel_filename);


$objPHPExcel->setActiveSheetIndex(0);
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename.".xlsx");

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save('php://output');
?>
