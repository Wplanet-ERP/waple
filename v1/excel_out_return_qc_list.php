<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');
require('inc/helper/work_return.php');
require('inc/helper/manufacturer.php');

if($session_partner == "dewbell"){
    $add_c_no   = "2305";
    $add_c_name = "듀벨";
}elseif($session_partner == "seongill"){
    $add_c_no   = "2304";
    $add_c_name = "(주)성일화학";
}elseif($session_partner == "pico"){
    $add_c_no   = "2772";
    $add_c_name = "주식회사 피코그램";
}elseif($session_partner == "paino"){
    $add_c_no   = "2472";
    $add_c_name = "(주)파이노";
}else{
    exit("<script>alert('접근할 수 없는 URL 입니다.');location.href='out_return_qc_list.php';</script>");
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:A2");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("B1:C1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("D1:E1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("F1:G1");
$objPHPExcel->setActiveSheetIndex(0)->mergeCells("H1:I1");

$lfcr = chr(10);
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "r_no")
    ->setCellValue('B1', "이전주문정보")
    ->setCellValue('B2', "이전 주문번호")
    ->setCellValue('C2', "발송처리일")
    ->setCellValue('D1', "고객요청 회수정보")
    ->setCellValue('D2', "회수자명{$lfcr}회수자 전화")
    ->setCellValue('E2', "우편번호{$lfcr}회수지")
    ->setCellValue('F1', "회수요청")
    ->setCellValue('F2', "구분")
    ->setCellValue('G2', "요청메세지")
    ->setCellValue('H1', "회수요청")
    ->setCellValue('H2', "회수 택배사{$lfcr}회수 운송장번호")
    ->setCellValue('I2', "[공급업체] 재고관리코드(SKU)::수량")
;

# 검색조건
$add_where     = "1=1 AND wcr.return_type='2' AND wcr.return_state='6'  AND wcr.order_number IN(SELECT DISTINCT order_number FROM work_cms_return_unit wcru WHERE wcru.`option` IN(SELECT pcu.`no` FROM product_cms_unit pcu WHERE pcu.sup_c_no='{$add_c_no}') AND wcru.quantity > 0)";
$chk_r_no_list = isset($_POST['chk_r_no_list']) ? $_POST['chk_r_no_list'] : "";

if(!empty($chk_r_no_list)){
    $add_where .= " AND r_no IN({$chk_r_no_list})";
}else{
    exit("<script>alert('선택한 주문이 없습니다.');location.href='out_return_qc_list.php';</script>");
}

# 리스트 쿼리
$return_qc_sql = "
    SELECT
        wcr.r_no,
        wcr.order_number,
        wcr.parent_order_number,
        (SELECT w.stock_date FROM work_cms w WHERE w.order_number=wcr.parent_order_number AND stock_date != '' LIMIT 1) as stock_date,
        wcr.recipient,
        wcr.recipient_hp,
        IF(wcr.zip_code, CONCAT('[',wcr.zip_code,']'), '') as postcode,
        wcr.recipient_addr,
        wcr.return_type,
        wcr.req_memo,
        wcr.return_delivery_type,
        wcr.return_delivery_no,
        wcr.return_delivery_no2,
        (SELECT mi.mi_no FROM manufacturer_inspection mi WHERE mi.order_number=wcr.order_number AND mi.add_c_no={$add_c_no}) as mi_no,
        (SELECT mi.work_state FROM manufacturer_inspection mi WHERE mi.order_number=wcr.order_number AND mi.add_c_no={$add_c_no}) as mi_state
    FROM work_cms_return wcr
    WHERE {$add_where}
    GROUP BY order_number
    ORDER BY r_no DESC
";
$return_qc_query    = mysqli_query($my_db, $return_qc_sql);
$return_type_option = getReturnTypeOption();
$mi_state_option    = getWorkStateOption();
$idx                = 3;
while($return_qc = mysqli_fetch_assoc($return_qc_query))
{
    $recipient_sc_hp = "";
    if (isset($return_qc['recipient_hp']) && !empty($return_qc['recipient_hp'])) {
        $f_hp = substr($return_qc['recipient_hp'], 0, 4);
        $e_hp = substr($return_qc['recipient_hp'], 7, 15);
        $recipient_sc_hp = $f_hp . "***" . $e_hp;
    }

    $return_type_name  = isset($return_type_option[$return_qc['return_type']]) ? $return_type_option[$return_qc['return_type']] : "";
    $mi_state_name     = isset($mi_state_option[$return_qc['mi_state']]) ? $mi_state_option[$return_qc['mi_state']] : "";

    $return_delivery_info = $return_qc['return_delivery_type'].$lfcr.$return_qc['return_delivery_no'];

    if(!empty($return_qc['return_delivery_no2'])){
        $return_delivery_info .= $lfcr.$return_qc['return_delivery_no2'];
    }

    $return_unit_sql    = "
        SELECT
            wcr.type,
            wcr.option,
            wcr.sku,
            (SELECT c.c_name FROM company c WHERE c.c_no=pcu.sup_c_no) as sup_c_name,
            wcr.quantity
        FROM work_cms_return_unit wcr
        LEFT JOIN product_cms_unit pcu ON pcu.`no`=wcr.option
        WHERE wcr.order_number = '{$return_qc['order_number']}' AND wcr.quantity > 0 AND pcu.sup_c_no='{$add_c_no}'
    ";
    $return_unit_query  = mysqli_query($my_db, $return_unit_sql);
    $return_unit_list   = "";
    while($return_unit  = mysqli_fetch_assoc($return_unit_query))
    {
        $unit_info = "[{$return_unit['sup_c_name']}] {$return_unit['sku']}::{$return_unit['quantity']}";
        $return_unit_list .= !empty($return_unit_list) ?  $lfcr.$unit_info : $unit_info;
    }

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$idx}", $return_qc['r_no'].$lfcr.$return_qc['reg_day'])
        ->setCellValueExplicit("B{$idx}", $return_qc['parent_order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
        ->setCellValue("C{$idx}", $return_qc['stock_date'])
        ->setCellValue("D{$idx}", $return_qc['recipient'].$lfcr.$recipient_sc_hp)
        ->setCellValue("E{$idx}", $return_qc['postcode'].$lfcr.$return_qc['recipient_addr'])
        ->setCellValue("F{$idx}", $return_type_name)
        ->setCellValue("G{$idx}", $return_qc['req_memo'])
        ->setCellValueExplicit("H{$idx}", $return_delivery_info, PHPExcel_Cell_DataType::TYPE_STRING)
        ->setCellValue("I{$idx}", $return_unit_list)
    ;

    $idx++;
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');

$objPHPExcel->getActiveSheet()->getStyle('A1:G2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00A6A6A6');
$objPHPExcel->getActiveSheet()->getStyle('H1:I2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00E75353');
$objPHPExcel->getActiveSheet()->getStyle('A1:I2')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:I2")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:I2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:I{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("A3:I{$idx}")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("B3:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("D3:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("E3:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("G3:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("H3:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("I3:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("D1:D2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("E1:E2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("H1:H2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("A3:A{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("D3:D{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("E3:E{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("H3:H{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("G3:G{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("I3:I{$idx}")->getAlignment()->setWrapText(true);

// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(50);


$excel_title = "QC 리스트";
$objPHPExcel->getActiveSheet()->setTitle($excel_title);
$objPHPExcel->getActiveSheet()->getStyle("A1:I{$idx}")->applyFromArray($styleArray);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$excel_title}.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
