<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/advertising.php');
require('inc/model/Team.php');
require('inc/model/Advertising.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "184";
$nav_title   = "NOSP/GFA 광고관리(달력)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

$cur_date           = date("Y-m-d");
$sch_adv_s_date     = isset($_GET['sch_adv_s_date']) ? $_GET['sch_adv_s_date'] : $cur_date;
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_media          = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_product        = isset($_GET['sch_product']) ? $_GET['sch_product'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : 3;
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_application    = isset($_GET['sch_application']) ? $_GET['sch_application'] : $session_name;
$sch_application_my = isset($_GET['sch_application_my']) ? $_GET['sch_application_my'] : "";
$sch_is_application = isset($_GET['sch_is_application']) ? $_GET['sch_is_application'] : "";
$sch_notice         = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$quick_btn          = isset($_GET['quick_btn']) ? $_GET['quick_btn'] : 5;
$cur_mon            = date('Y-m', strtotime("{$sch_adv_s_date}"));
$prev_mon           = date('Y-m', strtotime("{$cur_mon} -1 months"));
$next_mon           = date('Y-m', strtotime("{$cur_mon} +1 months"));
$next_week          = date('Y-m-d', strtotime("{$cur_date} +1 weeks"));
$next_mon_end       = date('t', strtotime($next_mon));
$cal_s_date         = $prev_mon."-01 00:00:00";
$cal_e_date         = $next_mon."-".$next_mon_end." 23:59:59";
$search_url         = getenv("QUERY_STRING");

$smarty->assign('cur_mon', $cur_mon);
$smarty->assign('cur_date', $next_week);
$smarty->assign('search_url', $search_url);
$smarty->assign('quick_btn', $quick_btn);

$add_where          = "(adv_s_date >= '{$cal_s_date}' OR adv_e_date >= '{$cal_s_date}') AND adv_s_date <= '{$cal_e_date}' AND state NOT IN(4,6)";

if(!empty($sch_no)){
    $add_where .= " AND am.am_no = '{$sch_no}'";
    $smarty->assign("sch_no", $sch_no);
}

if(!empty($sch_media)){
    $add_where .= " AND am.media = '{$sch_media}'";
    $smarty->assign("sch_media", $sch_media);
}

if(!empty($sch_product)){
    $add_where .= " AND am.product = '{$sch_product}'";
    $smarty->assign("sch_product", $sch_product);
}

if(!empty($sch_state)){
    $add_where .= " AND am.state = '{$sch_state}'";
    $smarty->assign("sch_state", $sch_state);
}

if(!empty($sch_agency)){
    $add_where .= " AND am.agency = '{$sch_agency}'";
    $smarty->assign("sch_agency", $sch_agency);
}

if($sch_application_my == 1){
    $sch_application = $session_name;
    $smarty->assign("sch_application_my", $sch_application_my);
}

if(!empty($sch_application)){
    $add_where .= " AND am.am_no IN (SELECT `aa`.am_no FROM advertising_application `aa` WHERE `aa`.s_name LIKE '%{$sch_application}%')";
    $smarty->assign("sch_application", $sch_application);
}

if(!empty($sch_is_application)){
    if($sch_is_application == '1'){
        $add_where .= " AND (SELECT COUNT(`aa`.am_no) FROM advertising_application `aa` WHERE `aa`.am_no=`am`.am_no) > 0";
    }elseif($sch_is_application == '2'){
        $add_where .= " AND (SELECT COUNT(`aa`.am_no) FROM advertising_application `aa` WHERE `aa`.am_no=`am`.am_no) = 0";
    }
    $smarty->assign("sch_is_application", $sch_is_application);
}

if(!empty($sch_notice)){
    $add_where .= " AND am.notice LIKE '%{$sch_notice}%'";
    $smarty->assign("sch_notice", $sch_notice);
}

# 캘린더 일정
$application_management_sql     = "
    SELECT
        *,
        (SELECT COUNT(*) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no) AS app_total_cnt,
        (SELECT COUNT(*) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no AND `aa`.s_no='{$session_s_no}') AS app_cnt
    FROM advertising_management am
    WHERE {$add_where}
    ORDER BY adv_s_date
";
$application_management_query   = mysqli_query($my_db, $application_management_sql);
$adv_product_option             = getAdvertisingProductOption();
$adv_state_option               = getAdvertisingStateOption();
$adv_media_option               = getAdvertisingMediaOption();
$application_management_list    = [];
while($application_management = mysqli_fetch_assoc($application_management_query))
{
    $title_s_date = date('H:i', strtotime($application_management['adv_s_date']));
    $title_e_date = date('H:i', strtotime($application_management['adv_e_date']));

    if($application_management['product'] == '1'){
        $application_management['color'] = ($application_management['app_cnt'] > 0) ? "#196ABA" : "#3788D8";
    }elseif($application_management['product'] == '2'){
        $application_management['color'] = ($application_management['app_cnt'] > 0) ? "#046d04" : "#228B22";
    }

    $state_title    = $application_management['app_total_cnt'] > 0 ? "[{$adv_state_option[$application_management['state']]} {$application_management['app_total_cnt']}명]" : "[{$adv_state_option[$application_management['state']]}]";
    $adv_title      = "{$title_s_date}~{$title_e_date} {$state_title} {$adv_product_option[$application_management['product']]}".number_format($application_management['price']);
    if(!empty($application_management['notice'])){
        $adv_title .= " {$application_management['notice']}";
    }
    $application_management['adv_title'] = $adv_title;

    $application_management_list[] = $application_management;
}

$adv_check_total_sql        = "
    SELECT 
        `am`.*,
        (SELECT COUNT(*) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no) AS app_total_cnt,
        (SELECT COUNT(*) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no AND `aa`.s_no='{$session_s_no}') AS app_cnt
    FROM advertising_management AS `am` 
    WHERE `am`.state IN(1,3) AND (adv_s_date >= '{$cal_s_date}' OR adv_e_date >= '{$cal_s_date}') AND adv_s_date <= '{$cal_e_date}' 
    ORDER BY adv_s_date DESC
";
$adv_check_total_query      = mysqli_query($my_db, $adv_check_total_sql);
$adv_check_total_list       = array("1" => 0, "2" => 0, "3" => 0, "4" => 0, '5' => 0, '6' => 0);
while($adv_check_total = mysqli_fetch_assoc($adv_check_total_query))
{
    if($adv_check_total['state'] == '1'){
        $adv_check_total_list["1"]++;
        if($adv_check_total['app_total_cnt'] > 0){
            $adv_check_total_list["2"]++;
        }else{
            $adv_check_total_list["6"]++;
        }

        if($adv_check_total['app_cnt'] > 0){
            $adv_check_total_list["4"]++;
        }
    }

    if($adv_check_total['state'] == '3'){
        $adv_check_total_list["3"]++;

        if($adv_check_total['app_cnt'] > 0){
            $adv_check_total_list["5"]++;
        }
    }
}

$team_model             = Team::Factory();
$team_full_name_list    = $team_model->getTeamFullNameList();

$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("hour_option", getHourOption());
$smarty->assign("min_option", getMinOption());
$smarty->assign("adv_media_option", $adv_media_option);
$smarty->assign("adv_product_option", $adv_product_option);
$smarty->assign("adv_state_option", $adv_state_option);
$smarty->assign("adv_type_option", getAdvertisingTypeOption());
$smarty->assign("adv_agency_option", getAdvertisingAgencyOption());
$smarty->assign("adv_quick_search_option", getQuickSearchOption());
$smarty->assign('adv_check_total_list', $adv_check_total_list);
$smarty->assign('application_management_list', json_encode($application_management_list));

$smarty->display('advertising_calendar.html');
?>
