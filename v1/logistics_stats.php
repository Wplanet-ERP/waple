<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/logistics.php');
require('inc/model/MyQuick.php');
require('inc/model/ProductCmsUnit.php');


# Navigation & My Quick
$nav_prd_no  = "157";
$nav_title   = "물류관리(타계정) 통계";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$add_where      = "1=1 AND lm.work_state='6'";
$main_option    = getStatsMainOption();

# 필터검색
$sch_stock_type     = isset($_GET['sch_stock_type']) ? $_GET['sch_stock_type'] : "all";
$sch_addr_filter    = isset($_GET['sch_addr_filter']) ? $_GET['sch_addr_filter'] : "";
$sch_addr_type      = isset($_GET['sch_addr_type']) ? $_GET['sch_addr_type'] : "";

if(!empty($sch_stock_type) && $sch_stock_type != "all")
{
    $add_where .= " AND lm.stock_type='{$sch_stock_type}'";
}

if(!empty($sch_addr_filter))
{
    if(!empty($sch_addr_type))
    {
        if($sch_addr_type == '1'){
            $add_where .= " AND lm.{$sch_addr_filter}_type='2809'";
        }elseif($sch_addr_type == '2'){
            $add_where .= " AND lm.{$sch_addr_filter}_type='4848'";
        }elseif($sch_addr_type == '3'){
            $add_where .= " AND lm.{$sch_addr_filter}_type NOT IN(2809,4848)";
        }
    }
}else{
    $sch_addr_type = "";
}

$smarty->assign("sch_stock_type", $sch_stock_type);
$smarty->assign("sch_addr_filter", $sch_addr_filter);
$smarty->assign("sch_addr_type", $sch_addr_type);

# 조회기간
$sch_date_type  = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "set";
$sch_s_date     = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d', strtotime("-1 month"));
$sch_e_date     = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');

$one_week_day  = date('Y-m-d', strtotime("-1 week"));
$two_week_day  = date('Y-m-d', strtotime("-2 week"));
$one_month_day = date('Y-m-d', strtotime("-1 month"));

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);
$smarty->assign('one_week_day', $one_week_day);
$smarty->assign('two_week_day', $two_week_day);
$smarty->assign('one_month_day', $one_month_day);
$smarty->assign('main_option', $main_option);

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

if($sch_date_type == 'set'){
    $add_where .= " AND (lm.run_date BETWEEN '{$sch_s_date}' AND '{$sch_e_date}')";
}

# X축(day: 날짜, subject: 계정과목, unit: 구성품)
$sch_x_type         = isset($_GET['sch_x_type']) ? $_GET['sch_x_type'] : "day";
$sch_x_type_main    = isset($_GET['sch_x_type_main']) ? $_GET['sch_x_type_main'] : "";

if($sch_x_type == 'day' && $sch_x_type_main == ''){
    $sch_x_type_main = '1';
}

$type_day           = getDayChartOption();
$type_subject       = getSubjectNameOption();
$type_company       = getAddrCompanyOption();
$product_model      = ProductCmsUnit::Factory();
$type_unit          = $product_model->getLogisticsUnitList();

$sch_x_type_list    = getSchType($sch_x_type, $sch_x_type_main, $type_day, $type_subject, $type_unit, $type_company);
$x_type_title       = $main_option[$sch_x_type];
$x_type_main_list   = $sch_x_type_list['type_main_list'];
$add_x_type_where   = $sch_x_type_list['add_type_where'];
$x_type_column      = $sch_x_type_list['add_type_column'];
$x_type_name_column = $sch_x_type_list['add_type_column_name'];

$add_x_type_column  = "{$x_type_column} as x_type";
if(!empty($x_type_name_column)){
    $add_x_type_name = "DISTINCT {$x_type_column} as x_type, {$x_type_name_column} as x_type_name";
}else{
    $add_x_type_name = "DISTINCT {$x_type_column} as x_type";
}
$smarty->assign('x_type_main_list', $x_type_main_list);
$smarty->assign('sch_x_type', $sch_x_type);
$smarty->assign('sch_x_type_main', $sch_x_type_main);


# Y축(day: 날짜, subject: 계정과목, unit: 구성품)
$sch_y_type         = isset($_GET['sch_y_type']) ? $_GET['sch_y_type'] : "subject";
$sch_y_type_main    = isset($_GET['sch_y_type_main']) ? $_GET['sch_y_type_main'] : "";

$sch_y_type_list    = getSchType($sch_y_type, $sch_y_type_main, $type_day, $type_subject, $type_unit, $type_run_company);
$y_type_title       = $main_option[$sch_y_type];
$y_type_main_list   = $sch_y_type_list['type_main_list'];
$add_y_type_where   = $sch_y_type_list['add_type_where'];
$y_type_column      = $sch_y_type_list['add_type_column'];
$y_type_name_column = $sch_y_type_list['add_type_column_name'];

$add_y_type_column  = "{$y_type_column} as y_type";
if(!empty($y_type_name_column)){
    $add_y_type_name = "DISTINCT {$y_type_column} as y_type, {$y_type_name_column} as y_type_name";
}else{
    $add_y_type_name = "DISTINCT {$y_type_column} as y_type";
}
$smarty->assign('y_type_main_list', $y_type_main_list);
$smarty->assign('sch_y_type', $sch_y_type);
$smarty->assign('sch_y_type_main', $sch_y_type_main);


# Commerce List 초기화
$commerce_list      = [];
$commerce_sum_list  = [];

# X축 Name List
$x_name_list        = [];
$x_name_legend_list = [];
$commerce_x_sql     = "
    SELECT
        {$add_x_type_name}
    FROM logistics_management lm
    LEFT JOIN (
        (SELECT lp.lp_no,lp.lm_no,pcr.option_no AS prd_no,lp.quantity*pcr.quantity AS quantity FROM logistics_product lp LEFT JOIN product_cms_relation pcr ON pcr.prd_no=lp.prd_no WHERE lp.prd_type = '1' AND pcr.display='1')
	    UNION
	    (SELECT lp.lp_no,lp.lm_no,lp.prd_no,lp.quantity FROM logistics_product lp WHERE lp.prd_type='2')
    ) lp ON lp.lm_no=lm.lm_no
    WHERE {$add_where}
        {$add_x_type_where}
        {$add_y_type_where}
    ORDER BY x_type ASC
";
$commerce_x_query = mysqli_query($my_db, $commerce_x_sql);
while($commerce_x = mysqli_fetch_assoc($commerce_x_query))
{
    $commerce_sum_list[$commerce_x['x_type']] = 0;
    if(!empty($x_type_name_column)){
        $x_name_list[$commerce_x['x_type']] = $commerce_x['x_type_name'];
    }else{
        $x_name_list[$commerce_x['x_type']] = $x_type_main_list[$commerce_x['x_type']];
    }
}

# Y축 Name List
$y_name_list    = [];
$y_legend_list  = [];
$commerce_y_sql = "
    SELECT
        {$add_y_type_name}
    FROM logistics_management lm
    LEFT JOIN (
        (SELECT lp.lp_no,lp.lm_no,pcr.option_no AS prd_no,lp.quantity*pcr.quantity AS quantity FROM logistics_product lp LEFT JOIN product_cms_relation pcr ON pcr.prd_no=lp.prd_no WHERE lp.prd_type = '1' AND pcr.display='1')
	    UNION
	    (SELECT lp.lp_no,lp.lm_no,lp.prd_no,lp.quantity FROM logistics_product lp WHERE lp.prd_type='2')
    ) lp ON lp.lm_no=lm.lm_no
    WHERE {$add_where}
        {$add_x_type_where}
        {$add_y_type_where}
    GROUP BY y_type
";

$commerce_y_query = mysqli_query($my_db, $commerce_y_sql);
while($commerce_y = mysqli_fetch_assoc($commerce_y_query))
{
    foreach ($x_name_list as $x_type => $x_type_name) {
        $commerce_list[$commerce_y['y_type']][$x_type] = 0;
    }

    if(!empty($y_type_name_column)){
        $y_name_list[$commerce_y['y_type']] = $commerce_y['y_type_name'];
        $y_legend_list[] = array('type' => $commerce_y['y_type'], 'name' => $commerce_y['y_type_name']);
    }else{
        $y_name_list[$commerce_y['y_type']] = $y_type_main_list[$commerce_y['y_type']];
        $y_legend_list[] = array('type' => $commerce_y['y_type'], 'name' => $y_type_main_list[$commerce_y['y_type']]);
    }
}

$commerce_sql = "
    SELECT
        {$add_x_type_column},
        {$add_y_type_column},
	    lp.quantity as data_sum
    FROM logistics_management lm
    LEFT JOIN (
        (SELECT lp.lp_no,lp.lm_no,pcr.option_no AS prd_no,lp.quantity*pcr.quantity AS quantity FROM logistics_product lp LEFT JOIN product_cms_relation pcr ON pcr.prd_no=lp.prd_no WHERE lp.prd_type = '1' AND pcr.display='1')
	    UNION
	    (SELECT lp.lp_no,lp.lm_no,lp.prd_no,lp.quantity FROM logistics_product lp WHERE lp.prd_type='2')
    ) lp ON lp.lm_no=lm.lm_no
    WHERE {$add_where}
        {$add_x_type_where}
        {$add_y_type_where}
    ORDER BY x_type ASC, lm.lm_no ASC
";
$commerce_query = mysqli_query($my_db, $commerce_sql);
while($commerce = mysqli_fetch_assoc($commerce_query))
{
    $data_sum = $commerce['data_sum'];
    $commerce_list[$commerce['y_type']][$commerce['x_type']] += $data_sum;
    $commerce_sum_list[$commerce['x_type']] += $data_sum;
}

$x_idx = 0;
$max   = 0;
$commerce_compare_chart_list = [];

if($x_name_list)
{
    foreach($x_name_list as $x_type => $x_item)
    {
        $y_idx = 0;
        foreach($y_legend_list as $y_item)
        {
            $y_type = $y_item['type'];
            $value  = $commerce_list[$y_type][$x_type];
            if($max < $value){
                $max = $value;
            }
            $commerce_compare_chart_list[] = array($x_idx, $y_idx, $value);
            $y_idx++;
        }
        $x_idx++;

        $x_legend_list[] = $x_item;
    }
}
$commerce_total = empty($commerce_list) ? 0 : count($commerce_list);

$smarty->assign('x_name_list', $x_name_list);
$smarty->assign('y_name_list', $y_name_list);
$smarty->assign('x_legend_list', json_encode($x_legend_list));
$smarty->assign('y_legend_list', json_encode($y_legend_list));
$smarty->assign('commerce_total', $commerce_total);
$smarty->assign('commerce_list', $commerce_list);
$smarty->assign('commerce_sum_list', $commerce_sum_list);
$smarty->assign('commerce_compare_chart_list', json_encode($commerce_compare_chart_list));
$smarty->assign('max_value', $max);
$smarty->assign('x_type_title', $x_type_title);
$smarty->assign('y_type_title', $y_type_title);
$smarty->assign('curdate', date('Y-m-d'));
$smarty->assign("stock_type_option", getStockTypeOption());
$smarty->assign("addr_filter_option", getAddrFilterOption());
$smarty->assign("addr_type_option", getStockAddrOption());

$smarty->display('logistics_stats.html');
?>
