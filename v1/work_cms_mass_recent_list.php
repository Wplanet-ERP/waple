<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');

# Navigation & My Quick
$nav_prd_no  = "57";
$nav_title   = "재구매 조회";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
// [상품(업무) 종류]
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_get	= isset($_GET['sch'])?$_GET['sch']:"Y";

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);

if(!empty($sch_get)) {
    $smarty->assign("sch", $sch_get);
}

$kind_model     = Kind::Factory();
$product_model  = ProductCms::Factory();
$cms_code       = "product_cms";
$cms_group_list = $kind_model->getKindGroupList($cms_code);
$prd_total_list = $product_model->getPrdGroupData();
$prd_g1_list = $prd_g2_list = $prd_g3_list = [];

foreach($cms_group_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}

$prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
$sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);

# 주문날짜
$today_val    = date('Y-m-d');
$months_val3  = date('Y-m-d', strtotime('-3 months'));
$months_val6  = date('Y-m-d', strtotime('-6 months'));
$year_val     = date('Y-m-d', strtotime('-1 years'));

$smarty->assign("today_val", $today_val);
$smarty->assign("3months_val", $months_val3);
$smarty->assign("6months_val", $months_val6);
$smarty->assign("year_val", $year_val);


$add_where      = "1=1";
$add_sub_where  = "1=1";
$add_group      = "";
$add_having     = "";
$add_group_list     = [];
$btn_url_type_list  = [];

# 상품(업무) 조건
if (!empty($sch_prd) && $sch_prd != "0") { // 상품
    $add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

$sch_ord_s_date         = isset($_GET['sch_ord_s_date']) ? $_GET['sch_ord_s_date'] : $months_val3;
$sch_ord_e_date         = isset($_GET['sch_ord_e_date']) ? $_GET['sch_ord_e_date'] : $today_val;
$sch_ord_date_type      = isset($_GET['sch_ord_date_type']) ? $_GET['sch_ord_date_type'] : "3months";
$sch_recipient          = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp       = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_zipcode            = isset($_GET['sch_zipcode']) ? $_GET['sch_zipcode'] : "";
$sch_recipient_addr     = isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
$sch_same_quantity_type = isset($_GET['sch_same_quantity_type']) ? $_GET['sch_same_quantity_type'] : "1";
$sch_same_quantity_1    = isset($_GET['sch_same_quantity_1']) ? $_GET['sch_same_quantity_1'] : "3";
$sch_same_quantity      = 3;
$sch_not_sel_same       = "";
$ori_ord_type           = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "sum_price";
$ord_type 		        = isset($_GET['ord_type']) ? $_GET['ord_type'] : "sum_price";

if(empty($sch_recipient) && empty($sch_recipient_hp) && empty($sch_zipcode) && empty($sch_recipient_addr))
{
    $sch_not_sel_same   = '1';
    $sch_recipient      = '1';
    $sch_recipient_hp   = '1';
}

if($sch_ord_date_type != 'total'){
    if(!empty($sch_ord_s_date) && !empty($sch_ord_e_date)){
        $add_where      .= " AND (w.order_date BETWEEN '{$sch_ord_s_date}' AND '{$sch_ord_e_date}')";
    }
}

if(!empty($sch_recipient))
{
    $btn_url_type_list[] = "recipient";
    $add_group_list[]    = "recipient";
    $add_where      .= " AND (w.recipient IS NOT NULL AND w.recipient != '')";
    $add_sub_where  .= " AND sub.recipient=w.recipient";
}

if(!empty($sch_recipient_hp))
{
    $btn_url_type_list[] = "recipient_hp";
    $add_group_list[]    = "recipient_hp";
    $add_where      .= " AND (w.recipient_hp IS NOT NULL AND w.recipient_hp != '')";
    $add_sub_where  .= " AND sub.recipient_hp=w.recipient_hp";
}

if(!empty($sch_zipcode))
{
    $btn_url_type_list[] = "zip_code";
    $add_group_list[]    = "zip_code";
    $add_where      .= " AND (w.zip_code IS NOT NULL AND w.zip_code != '')";
    $add_sub_where  .= " AND sub.zip_code=w.zip_code";
}

if(!empty($sch_recipient_addr))
{
    $btn_url_type_list[] = "recipient_addr";
    $add_group_list[]    = "recipient_addr";
    $add_where      .= " AND (w.recipient_addr IS NOT NULL AND w.recipient_addr != '')";
    $add_sub_where  .= " AND sub.recipient_addr=w.recipient_addr";
}

if(!empty($add_group_list))
{
    $add_group  = implode(', ', $add_group_list);
}

switch($sch_same_quantity_type){
    case '1':
        $sch_same_quantity = $sch_same_quantity_1;
        break;
}

if(!!$add_group)
{
    $add_having  .= "ord_cnt >= {$sch_same_quantity}";
}

$sch_same_quantity_type_sub = isset($_GET['sch_same_quantity_type_sub']) ? $_GET['sch_same_quantity_type_sub'] : "";
$sch_same_quantity_sub      = isset($_GET['sch_same_quantity_sub']) ? $_GET['sch_same_quantity_sub'] : "";
if(!empty($sch_same_quantity_type_sub) && !empty($sch_same_quantity_sub))
{
    $sub_chk_sql = "
        SELECT
            (SELECT sub.w_no FROM work_cms_recent sub WHERE {$add_sub_where} ORDER BY sub.w_no DESC LIMIT 1) as w_no,
            w.recipient, w.recipient_hp, w.zip_code, w.recipient_addr, SUM(w.quantity) as prd_cnt, SUM(w.dp_price_vat) as prd_sum
        FROM work_cms_recent w
        WHERE {$add_where} 
        GROUP BY {$add_group}, prd_no
        HAVING prd_cnt > '{$sch_same_quantity_sub}' AND prd_sum > 0
    ";

    $sub_chk_query = mysqli_query($my_db, $sub_chk_sql);
    $sub_chk_list  = [];
    $ord_chk_list  = [];
    while($sub_chk_result = mysqli_fetch_assoc($sub_chk_query))
    {
        $add_recent_where = $add_where;

        if(!empty($sch_recipient))
        {
            $add_recent_where .= " AND w.recipient='{$sub_chk_result['recipient']}'";
        }

        if(!empty($sch_recipient_hp))
        {
            $add_recent_where .= " AND w.recipient_hp='{$sub_chk_result['recipient_hp']}'";
        }

        if(!empty($sch_zipcode))
        {
            $add_recent_where .= " AND w.zip_code='{$sub_chk_result['zip_code']}'";
        }

        if(!empty($sch_recipient_addr))
        {
            $add_recent_where .= " AND w.recipient_addr='{$sub_chk_result['recipient_addr']}'";
        }

        if(!isset($sub_chk_list[$sub_chk_result['w_no']]))
        {
            $sub_chk_list[$sub_chk_result['w_no']] = 1;
            $ord_chk_sql    = "SELECT DISTINCT order_number FROM work_cms_recent w WHERE {$add_recent_where}";
            $ord_chk_query  = mysqli_query($my_db, $ord_chk_sql);
            while($ord_chk_result = mysqli_fetch_assoc($ord_chk_query)){
                $ord_chk_list[] = "'".$ord_chk_result['order_number']."'";
            }
        }
    }

    if(!empty($ord_chk_list)){
        $ord_chk = implode(',', $ord_chk_list);
        $add_where .= " AND w.order_number NOT IN({$ord_chk})";
    }

    $smarty->assign("sch_same_quantity_type_sub", $sch_same_quantity_type_sub);
    $smarty->assign("sch_same_quantity_sub", $sch_same_quantity_sub);
}


$add_orderby = "";
$ord_type_by = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";
    $orderby_val = "DESC";
    if(!empty($ord_type_by))
    {
        if($ori_ord_type == $ord_type){
            if($ord_type_by == '1'){
                $orderby_val = "ASC";
            }elseif($ord_type_by == '2'){
                $orderby_val = "DESC";
            }
        }else{
            $ord_type_by = '2';
            $orderby_val = "DESC";
        }
    }

    $add_orderby = "{$ord_type} {$orderby_val}";
}

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);

$mass_cms_sql = "
    SELECT
        w.recipient,
        w.recipient_hp,
        w.zip_code,
        w.recipient_addr,
        COUNT(DISTINCT w.order_number) as ord_cnt,
        SUM(w.dp_price_vat) as sum_price
    FROM  work_cms_recent w
    WHERE {$add_where}
    GROUP BY {$add_group}
    HAVING {$add_having}
    ORDER BY {$add_orderby}
";

// 전체 게시물 수
$mass_cms_total_sql     = "SELECT count(*) as cnt FROM ({$mass_cms_sql}) as search";
$mass_cms_total_query	= mysqli_query($my_db, $mass_cms_total_sql);
$mass_cms_total         = 0;
if(!!$mass_cms_total_query){
    $mass_cms_total_result = mysqli_fetch_array($mass_cms_total_query);
    $mass_cms_total = $mass_cms_total_result['cnt'];
}


$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($mass_cms_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "work_cms_mass_recent_list.php", $pagenum, $search_url);
$smarty->assign("total_num", number_format($mass_cms_total));
$smarty->assign("search_url", $search_url);
$smarty->assign("pagelist", $page);
$smarty->assign("ord_page_type", $page_type);

$mass_cms_sql .= "LIMIT {$offset}, {$num}";

$mass_cms_list  = [];
$mass_cms_query = mysqli_query($my_db, $mass_cms_sql);
$mass_s_date    = $sch_ord_s_date;
$mass_e_date    = $sch_ord_e_date;
if($sch_ord_date_type == 'total'){
    $mass_s_date    = "2022-01-01";
    $mass_e_date    = "2022-11-10";
}
while($mass_cms = mysqli_fetch_assoc($mass_cms_query))
{

    if(!empty($btn_url_type_list))
    {
        $btn_url = "https://work.wplanet.co.kr/v1/work_cms_mass_recent_window.php?ord_s_date={$mass_s_date}&ord_e_date={$mass_e_date}";
        foreach($btn_url_type_list as $btn_url_type)
        {
            $btn_url .= "&{$btn_url_type}={$mass_cms[$btn_url_type]}";
        }
        $mass_cms['btn_url'] = $btn_url;
    }

    $mass_cms_list[] = $mass_cms;
}

$smarty->assign("page_type_option", getPageTypeOption(3));
$smarty->assign('sch_ord_s_date', $sch_ord_s_date);
$smarty->assign('sch_ord_e_date', $sch_ord_e_date);
$smarty->assign('sch_ord_date_type', $sch_ord_date_type);
$smarty->assign("sch_recipient", $sch_recipient);
$smarty->assign("sch_recipient_hp", $sch_recipient_hp);
$smarty->assign("sch_zipcode", $sch_zipcode);
$smarty->assign("sch_recipient_addr", $sch_recipient_addr);
$smarty->assign("sch_same_quantity_type", $sch_same_quantity_type);
$smarty->assign("sch_same_quantity_1", $sch_same_quantity_1);
$smarty->assign("mass_cms_list", $mass_cms_list);

$smarty->display('work_cms_mass_recent_list.html');

?>
