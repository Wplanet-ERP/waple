<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/withdraw.php');
require('inc/helper/deposit.php');
require('Classes/PHPExcel.php');


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);


// 상단 타이틀 & idx 초기화(아임웹)
$title = "CS 발송 검수";

$objPHPExcel->setActiveSheetIndex(0)->getRowDimension(1)->setRowHeight(20);
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "w_no")
    ->setCellValue('B1', "작성일")
    ->setCellValue('C1', "발송 진행상태")
    ->setCellValue('D1', "원주문번호")
    ->setCellValue('E1', "CS담당자")
    ->setCellValue('F1', "구매처")
    ->setCellValue('G1', "커머스 상품명")
    ->setCellValue('H1', "수량")
    ->setCellValue('I1', "수령자명")
    ->setCellValue('J1', "수령자전화")
    ->setCellValue('K1', "수령지")
    ->setCellValue('L1', "특이사항")
    ->setCellValue('M1', "관리자메모")
    ->setCellValue('N1', "메모")
    ->setCellValue('O1', "입금확인")
;

$cur_date       = date("Y-m-d");
$prev_date      = date("Y-m-d", strtotime("-1 weeks"));
$sch_step_data  = isset($_GET['sch_step_data']) ? $_GET['sch_step_data'] : "";

$add_where = "1=1";
if($sch_step_data == 'true'){
    $add_where  .= " AND w.delivery_state='10' AND (stock_date BETWEEN '{$prev_date}' AND '{$cur_date}') AND ((SELECT COUNT(dp_no) AS dp_count FROM `work` as sub WHERE sub.prd_no='260' AND sub.work_state='6' AND sub.linked_table='work_cms' AND sub.linked_shop_no=w.order_number AND (SELECT dp.dp_state FROM deposit dp WHERE dp.dp_no=sub.dp_no)='2') > 0 OR (SELECT count(w_no) FROM `work` as sub WHERE sub.prd_no='260' AND sub.work_state='6' AND sub.linked_table='work_cms' AND sub.linked_shop_no=w.order_number) = 0)";
}else{
    $add_where  .= " AND w.delivery_state='10' AND (stock_date BETWEEN '{$prev_date}' AND '{$cur_date}')";
}

$ord_sql    = "
    SELECT
        w.w_no,
        DATE_FORMAT(w.regdate, '%Y-%m-%d') as reg_date,
        w.order_number,
        w.parent_order_number,
        (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
        w.quantity,
        w.recipient,
		w.recipient_hp,
		w.recipient_addr,
        w.notice,
        w.manager_memo,
        w.shop_ord_no
    FROM work_cms w
    WHERE {$add_where}
    ORDER BY w_no
";
$ord_query          = mysqli_query($my_db, $ord_sql);
$ord_list           = [];
$ord_origin_list    = [];
$dp_state_option    = getDpStateOption();
$wd_state_option    = getWdStateOption();
$lfcr               = chr(10);
while($ord_result = mysqli_fetch_assoc($ord_query))
{
    if(!isset($ord_list[$ord_result['order_number']]))
    {
        $ord_sub_sql    = "SELECT sub.order_number, (SELECT s.s_name FROM staff s WHERE s.s_no=sub.task_run_s_no) as run_s_name, sub.dp_c_name FROM work_cms sub WHERE sub.order_number='{$ord_result['parent_order_number']}' LIMIT 1";
        $ord_sub_query  = mysqli_query($my_db, $ord_sub_sql);
        $ord_sub_result = mysqli_fetch_assoc($ord_sub_query);

        $comment_sql    = "SELECT s_name, regdate, `comment` FROM work_cms_comment WHERE ord_no='{$ord_result['parent_order_number']}' ORDER BY regdate ASC";
        $comment_query  = mysqli_query($my_db, $comment_sql);
        $comment_list   = [];
        while($comment_result = mysqli_fetch_assoc($comment_query)){
            $comment_list[] = $comment_result;
        }
        $ord_sub_result['comment_list'] = $comment_list;

        $deposit_text   = "";
        $deposit_sql    = "SELECT w_no, dp_no, dp_price_vat, (SELECT dp.dp_state FROM deposit dp WHERE dp.dp_no=w.dp_no) as dp_state FROM work w WHERE prd_no='260' AND work_state='6' AND linked_table='work_cms' AND linked_shop_no='{$ord_result['order_number']}'";
        $deposit_query  = mysqli_query($my_db, $deposit_sql);
        while($deposit = mysqli_fetch_assoc($deposit_query))
        {
            $price = isset($deposit['dp_price_vat']) && !empty($deposit['dp_price_vat']) ? number_format($deposit['dp_price_vat'],0)  : 0;
            $state = isset($deposit['dp_state']) && !empty($deposit['dp_state']) ? "[".$dp_state_option[$deposit['dp_state']]."]" : "[대기]";
            $deposit_text .= !empty($deposit_text) ? $lfcr."+{$price}{$state}" : "+{$price}{$state}";
        }
        $ord_result['deposit_text'] = $deposit_text;

        $ord_origin_list[$ord_result['order_number']] = $ord_sub_result;
    }

    $ord_list[$ord_result['order_number']][] = $ord_result;
}

$idx = 2;
if(!!$ord_list)
{
    foreach($ord_list as $ord_no => $order_data)
    {
        $row_idx    = count($order_data);
        $merge_idx  = $idx+$row_idx-1;
        $ord_idx    = 1;

        foreach($order_data as $order)
        {
            if($ord_idx == '1')
            {
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("D{$idx}:D{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("E{$idx}:E{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("F{$idx}:F{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("I{$idx}:I{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("J{$idx}:J{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("K{$idx}:K{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("N{$idx}:N{$merge_idx}");
                $objPHPExcel->setActiveSheetIndex(0)->mergeCells("O{$idx}:O{$merge_idx}");

                $origin_order = $ord_origin_list[$ord_no];
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicit("D{$idx}", $origin_order['order_number'], PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("E{$idx}", $origin_order['run_s_name']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("F{$idx}", $origin_order['dp_c_name']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("I{$idx}", $order['recipient']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("J{$idx}", $order['recipient_hp']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("K{$idx}", $order['recipient_addr']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("O{$idx}", $order['deposit_text']);

                $comment_text = "";
                if(!empty($origin_order['comment_list']))
                {
                    foreach($origin_order['comment_list'] as $comment){
                        if(!empty($comment_text)){
                            $comment_text .= $lfcr.$lfcr;
                        }

                        $comment_val    = str_replace('<br />',$lfcr,$comment['comment']);
                        $comment_text   .= "{$comment['s_name']} {$comment['regdate']}".$lfcr;
                        $comment_text   .= "{$comment_val}";
                    }
                }

                $objPHPExcel->setActiveSheetIndex(0)->setCellValue("N{$idx}", $comment_text);
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit("A{$idx}", $order['w_no'], PHPExcel_Cell_DataType::TYPE_STRING)
                ->setCellValue("B{$idx}", $order['reg_date'])
                ->setCellValue("C{$idx}", "작성완료(검수요청)")
                ->setCellValue("G{$idx}", $order['prd_name'])
                ->setCellValue("H{$idx}", $order['quantity'])
                ->setCellValue("L{$idx}", $order['notice'])
                ->setCellValue("M{$idx}", $order['manager_memo'])
            ;

            $objPHPExcel->setActiveSheetIndex(0)->getRowDimension($idx)->setRowHeight(40);
            $ord_idx++;
            $idx++;
        }
    }
}
$idx--;

// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:O1")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:O1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A2:O{$idx}")->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
$objPHPExcel->getActiveSheet()->getStyle("D2:F{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00D9D9D9');
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00D9D9D9');

$objPHPExcel->getActiveSheet()->getStyle("A1:O{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:O{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("M2:M{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("M2:M{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(9);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(9);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(9);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(32);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(9);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(38);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(55);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);



$objPHPExcel->getActiveSheet()->setTitle($title);
$objPHPExcel->getActiveSheet()->getStyle("A1:O{$idx}")->applyFromArray($styleArray);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$title}.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
