<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.7, 2012-05-19
 */

/** Error reporting */
ini_set('error_reporting',E_ALL & ~E_NOTICE | E_STRICT);
date_Default_TimeZone_set("Asia/Seoul");	// 시간설정
define('ROOTPATH', dirname(__FILE__));


/** Include PHPExcel */
require_once './Classes/PHPExcel.php';
require_once 'inc/common.php';
require_once 'ckadmin.php';
require_once 'inc/helper/promotion.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
),
),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
),
);


// 단순히 상태값을 텍스트 표현하기 위해 배열에 담은 정보
$state_name=array(1=>'접수대기', 2=>'접수완료',3=>'모집중',4=>'진행중',5=>'마감');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where = "";

$sch_my_c_no_get     = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_kind_get        = isset($_GET['sch_kind']) ? $_GET['sch_kind'] : "";
$sch_state_get       = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_code_get        = isset($_GET['sch_code']) ? $_GET['sch_code'] : "";
$sch_company_get     = isset($_GET['sch_company']) ? $_GET['sch_company'] : "";
$sch_reward_date_get = isset($_GET['sch_reward_date']) ? $_GET['sch_reward_date'] : "";
$sch_wd_due_date_get = isset($_GET['sch_wd_due_date']) ? $_GET['sch_wd_due_date'] : "";
$sch_tax_set_get     = isset($_GET['sch_tax_set']) ? $_GET['sch_tax_set'] : "";
$sch_wd_due_date_null_get = isset($_GET['sch_wd_due_date_null']) ? $_GET['sch_wd_due_date_null'] : "";
$sch_refund_get      = isset($_GET['sch_refund']) ? $_GET['sch_refund'] : "n";
$sch_nick_get        = isset($_GET['sch_nick']) ? $_GET['sch_nick'] : "";
$sch_username_get    = isset($_GET['sch_username']) ? $_GET['sch_username'] : "";
$sch_blog_url_get    = isset($_GET['sch_blog_url']) ? $_GET['sch_blog_url'] : "";

if(!empty($sch_my_c_no_get)) {
    $add_where.=" AND p.c_no IN(SELECT c.c_no FROM company c WHERE c.my_c_no='{$sch_my_c_no_get}')";
}

if(!empty($sch_kind_get)) {
    $add_where.=" AND p.kind = '{$sch_kind_get}'";
}

if(!empty($sch_state_get)) {
	$add_where.=" AND p.p_state = '{$sch_state_get}'";
}

if(!empty($sch_code_get)) {
	$add_where.=" AND p.promotion_code like '%{$sch_code_get}%'";
}

if(!empty($sch_company_get)) {
	$add_where.=" AND p.company like '%{$sch_company_get}%'";
}

if(!empty($sch_reward_date_get)) {
	$add_where.=" AND p.pres_reward = '{$sch_reward_date_get}'";
}

if(!empty($sch_wd_due_date_get)) {
	$add_where.=" AND a.wd_due_date = '{$sch_wd_due_date_get}'";
}

if($sch_wd_due_date_null_get == 'null') { // wd_due_date is null
	$add_where.=" AND (a.wd_due_date is null OR a.wd_due_date = '')";
}

if(!empty($sch_tax_set_get)) {
	if($sch_tax_set_get == "y") { // 소득세 저장완료
		$add_where.=" AND ((a.reward_cost IS NOT NULL AND a.reward_cost<>'0') AND a.biz_tax IS NOT NULL AND a.local_tax IS NOT NULL AND (a.reward_cost_result IS NOT NULL AND a.reward_cost_result<>'0'))";
	}elseif($sch_tax_set_get == "n"){ // 소득세 저장 미완료
		$add_where.=" AND ((a.reward_cost IS NULL OR a.reward_cost='0') OR a.biz_tax IS NULL OR a.local_tax IS NULL OR (a.reward_cost_result IS NULL OR a.reward_cost_result='0'))";
	}
}

if($sch_refund_get == "y") {
	$add_where.=" AND a.refund_count >= 1";
}else if($sch_refund_get == "n") {
	$add_where.=" AND (a.refund_count is NULL or a.refund_count = 0)";
}
if(!empty($sch_nick_get)) {
	$add_where.=" AND a.nick like '%{$sch_nick_get}%'";
}
if(!empty($sch_username_get)) {
	$add_where.=" AND a.username like '%{$sch_username_get}%'";
}
if(!empty($sch_blog_url_get)) {
	$add_where.=" AND a.blog_url like '%{$sch_blog_url_get}%'";
}


//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "no.")
    ->setCellValue('B1', "광고주")
    ->setCellValue('C1', "담당자")
    ->setCellValue('D1', "마케팅채널")
    ->setCellValue('E1', "캠페인종류")
    ->setCellValue('F1', "p_no")
    ->setCellValue('G1', "건수")
    ->setCellValue('H1', "지급액")
    ->setCellValue('I1', "사업소득세")
    ->setCellValue('J1', "지방소득세")
    ->setCellValue('K1', "실지급액");

# 정렬순서 토글 & 필드 지정
$add_orderby = " a.p_no DESC, a.a_no ASC";

$promotion_sql = "
	SELECT p.channel AS channel,
		p.kind AS kind,
		p.p_no AS p_no,
		p.c_no AS c_no,
		(SELECT c.my_c_no FROM company c WHERE c.c_no=p.c_no) AS my_c_no,
		p.company AS company,
		p.name AS staff_name,
		a.wd_due_date,
		COUNT(a.a_no) AS reward_cnt,
		SUM(a.reward_cost) AS sum_reward_cost,
		SUM(a.biz_tax) AS sum_biz_tax,
		SUM(a.local_tax) AS sum_local_tax,
		SUM(a.reward_cost_result) AS sum_reward_cost_result
	FROM promotion p, application a
	WHERE
		p.p_no=a.p_no AND (p.kind='2' OR p.kind='4' OR p.kind='5') AND a.a_state='1' {$add_where}
	GROUP BY
		p.p_no
	ORDER BY my_c_no ASC, a.p_no DESC
";
$promotion_query = mysqli_query($my_db, $promotion_sql);

$excel_idx = 2;
$waple_idx = 2;
$no_idx    = 1;
$promotion_sum_list = [];
$promotion_sum_list['total'] = array(
    'qty'       => 0,
    'cost'      => 0,
    'biz_tax'   => 0,
    'local_tax' => 0,
    'real_cost' => 0
);

$promotion_channel  = getPromotionChannelOption();
$promotion_kind     = getPromotionKindOption();
while($promotion_result = mysqli_fetch_array($promotion_query))
{
    $company_name = ($promotion_result['my_c_no'] == '3') ? "미디어커머스 ".$promotion_result['company'] : $promotion_result['company'];

	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$excel_idx}", $no_idx)
        ->setCellValue("B{$excel_idx}", $company_name)
        ->setCellValue("C{$excel_idx}", $promotion_result['staff_name'])
        ->setCellValue("D{$excel_idx}", $promotion_channel[$promotion_result['channel']])
        ->setCellValue("E{$excel_idx}", $promotion_kind[$promotion_result['kind']])
        ->setCellValue("F{$excel_idx}", $promotion_result['p_no'])
        ->setCellValue("G{$excel_idx}", $promotion_result['reward_cnt'])
        ->setCellValue("H{$excel_idx}", $promotion_result['sum_reward_cost'])
        ->setCellValue("I{$excel_idx}", $promotion_result['sum_biz_tax'])
        ->setCellValue("J{$excel_idx}", $promotion_result['sum_local_tax'])
        ->setCellValue("K{$excel_idx}", $promotion_result['sum_reward_cost_result']);

	if(!isset($promotion_sum_list[$promotion_result['my_c_no']])){
        $promotion_sum_list[$promotion_result['my_c_no']] = array(
            'qty'       => 0,
            'cost'      => 0,
            'biz_tax'   => 0,
            'local_tax' => 0,
            'real_cost' => 0
        );
    }

    $promotion_sum_list['total']['qty']        += $promotion_result['reward_cnt'];
    $promotion_sum_list['total']['cost']       += $promotion_result['sum_reward_cost'];
    $promotion_sum_list['total']['biz_tax']    += $promotion_result['sum_biz_tax'];
    $promotion_sum_list['total']['local_tax']  += $promotion_result['sum_local_tax'];
    $promotion_sum_list['total']['real_cost']  += $promotion_result['sum_reward_cost_result'];

    $promotion_sum_list[$promotion_result['my_c_no']]['qty']        += $promotion_result['reward_cnt'];
    $promotion_sum_list[$promotion_result['my_c_no']]['cost']       += $promotion_result['sum_reward_cost'];
    $promotion_sum_list[$promotion_result['my_c_no']]['biz_tax']    += $promotion_result['sum_biz_tax'];
    $promotion_sum_list[$promotion_result['my_c_no']]['local_tax']  += $promotion_result['sum_local_tax'];
    $promotion_sum_list[$promotion_result['my_c_no']]['real_cost']  += $promotion_result['sum_reward_cost_result'];

    if($promotion_result['my_c_no'] == '1'){
        $waple_idx++;
    }

    $excel_idx++;
    $no_idx++;
}
$waple_idx--;


$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('0031A5A7');
$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A2:K{$waple_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FAFAB4');

# 와이즈플래닛컴퍼니 소계
$objPHPExcel->getActiveSheet()->mergeCells("A{$excel_idx}:F{$excel_idx}");
$objPHPExcel->getActiveSheet()->getStyle("A{$excel_idx}:K{$excel_idx}")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A{$excel_idx}:K{$excel_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('0031A5A7');
$objPHPExcel->getActiveSheet()->setCellValue("A{$excel_idx}", "[와이즈플래닛컴퍼니] 소 계");
$objPHPExcel->getActiveSheet()->setCellValue("G{$excel_idx}", $promotion_sum_list[1]['qty']);
$objPHPExcel->getActiveSheet()->setCellValue("H{$excel_idx}", $promotion_sum_list[1]['cost']);
$objPHPExcel->getActiveSheet()->setCellValue("I{$excel_idx}", $promotion_sum_list[1]['biz_tax']);
$objPHPExcel->getActiveSheet()->setCellValue("J{$excel_idx}", $promotion_sum_list[1]['local_tax']);
$objPHPExcel->getActiveSheet()->setCellValue("K{$excel_idx}", $promotion_sum_list[1]['real_cost']);

$excel_idx++;

# 미디어커머스 소계
$objPHPExcel->getActiveSheet()->mergeCells("A{$excel_idx}:F{$excel_idx}");
$objPHPExcel->getActiveSheet()->getStyle("A{$excel_idx}:K{$excel_idx}")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A{$excel_idx}:K{$excel_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('0031A5A7');
$objPHPExcel->getActiveSheet()->setCellValue("A{$excel_idx}", "[와이즈미디어커머스] 소 계");
$objPHPExcel->getActiveSheet()->setCellValue("G{$excel_idx}", $promotion_sum_list[3]['qty']);
$objPHPExcel->getActiveSheet()->setCellValue("H{$excel_idx}", $promotion_sum_list[3]['cost']);
$objPHPExcel->getActiveSheet()->setCellValue("I{$excel_idx}", $promotion_sum_list[3]['biz_tax']);
$objPHPExcel->getActiveSheet()->setCellValue("J{$excel_idx}", $promotion_sum_list[3]['local_tax']);
$objPHPExcel->getActiveSheet()->setCellValue("K{$excel_idx}", $promotion_sum_list[3]['real_cost']);
$excel_idx++;

# 총계
$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FF0000');

$objPHPExcel->getActiveSheet()->mergeCells("A{$excel_idx}:F{$excel_idx}");
$objPHPExcel->getActiveSheet()->getStyle("A{$excel_idx}:K{$excel_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('0082F9B7');
$objPHPExcel->getActiveSheet()->getStyle("A{$excel_idx}:K{$excel_idx}")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("H{$excel_idx}")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->setCellValue("A{$excel_idx}", "총 계");
$objPHPExcel->getActiveSheet()->setCellValue("G{$excel_idx}", $promotion_sum_list['total']['qty']);
$objPHPExcel->getActiveSheet()->setCellValue("H{$excel_idx}", $promotion_sum_list['total']['cost']);
$objPHPExcel->getActiveSheet()->setCellValue("I{$excel_idx}", $promotion_sum_list['total']['biz_tax']);
$objPHPExcel->getActiveSheet()->setCellValue("J{$excel_idx}", $promotion_sum_list['total']['local_tax']);
$objPHPExcel->getActiveSheet()->setCellValue("K{$excel_idx}", $promotion_sum_list['total']['real_cost']);

$objPHPExcel->getActiveSheet()->getStyle("A1:K{$excel_idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A2:K{$excel_idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A2:A{$excel_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("C2:C{$excel_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


$objPHPExcel->getActiveSheet()->getStyle("G2:K{$excel_idx}")->getNumberFormat()->setFormatCode("#,##0");


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(7);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8.5);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6.5);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(11.5);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(11.5);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(11.5);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(11.5);
$objPHPExcel->getActiveSheet()->setTitle('업체별 합산 품의');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="'.$today.'_업체별 합산 품의.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
