<?php
require('../inc/common.php');
require('../inc/data_state.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);


$res_date_get=isset($_GET['res_date']) ? $_GET['res_date'] : "";
$smarty->assign("f_res_date",$res_date_get);

if($res_date_get){
	$base_day = date('Y-m', strtotime($res_date_get))."-01";
	$date_w   = date('w', strtotime($res_date_get));
	$week_no  = date('W', strtotime($res_date_get)) - (date('W', strtotime($base_day))-1);

//	if($date_w == '3' && ($week_no == '2' || $week_no == '4')){
//		exit("<script>alert('이용하실 수 없는 날짜입니다');location.href='ssh_reservation.php?sch_date={$res_date_get}';</script>");
//	}
}

$res_time_get=isset($_GET['res_time']) ? $_GET['res_time'] : "";
$smarty->assign("f_res_time",$res_time_get);


$no=isset($_GET['no']) ? $_GET['no'] : "";
$security_pw=isset($_GET['security_pw']) ? $_GET['security_pw'] : "";
$mode=(!$no) ? "write":"modify";

// POST : form action 을 통해 넘겨받은 process 가 있는지 체크하고 있으면 proc에 저장
$proc=isset($_POST['process']) ? $_POST['process'] : "";

// 접근 권한
if(($security_pw == "PW_ACCESS" || $_SESSION['security_pw']) || $mode == "write"){
  unset($_SESSION["security_pw"]);
}else{
  exit("<script>location.href='security_pw.php?no={$no}';</script>");
}

$accept_time 	= array(10,11,12,13,14,15,16,17,18);
$smarty->assign("accept_time", $accept_time);

if($mode=="modify") {
	$ssh_reservation_sql="
		SELECT
			*,
		 	(SELECT group_concat(sub_sr.res_time) FROM ssh_reservation sub_sr WHERE sr.res_date = sub_sr.res_date AND sr.no != sub_sr.no AND display=1) as disabled_time
		FROM ssh_reservation sr
		WHERE sr.no='".$no."'
	";

	//echo "<br><br><br><br>".$sql."<br><br>";
	$ssh_reservation_result = mysqli_query($my_db, $ssh_reservation_sql);
	$ssh_reservation = mysqli_fetch_array($ssh_reservation_result);


	$emails = explode("@",$ssh_reservation['e_mail']);
	$smarty->assign(
		array(
			"f_email_0"=>$emails[0],
			"f_email_1"=>$emails[1]
		)
	);

	$res_time 		= (isset($ssh_reservation['res_time']) && $ssh_reservation['res_time'] != "") ? explode(',', $ssh_reservation['res_time']) : "";
	$disabled_time  = (isset($ssh_reservation['disabled_time']) && $ssh_reservation['disabled_time'] != "") ? explode(',', $ssh_reservation['disabled_time']) : "";

	$smarty->assign(
		array(
			"no"=>$ssh_reservation['no'],
			"f_display"=>$ssh_reservation['display'],
			"f_res_date"=>$ssh_reservation['res_date'],
			"f_res_time"=>$res_time,
			"f_disabled_time"=>$disabled_time,
			"f_type"=>$ssh_reservation['type'],
			"f_company_name"=>stripslashes($ssh_reservation['company_name']),
			"f_object"=>stripslashes($ssh_reservation['object']),
			"f_people"=>$ssh_reservation['people'],
			"f_director"=>stripslashes($ssh_reservation['director']),
			"f_director_tel"=>stripslashes($ssh_reservation['director_tel']),
			"f_director_tel_agree"=>$ssh_reservation['director_tel_agree'],
			"f_add_use"=>$ssh_reservation['add_use'],
			"f_user_type"=>$ssh_reservation['user_type'],
			"f_pw"=>substr(md5($_POST['pw']),8,16)
		)
	);

}else{
	$ssh_reservation_sql="
		SELECT
		 	(SELECT group_concat(sub_sr.res_time) FROM ssh_reservation sub_sr WHERE sr.res_date = sub_sr.res_date AND display=1) as disabled_time
		FROM ssh_reservation sr
		WHERE res_date='".$res_date_get."'
	";

	$ssh_reservation_result = mysqli_query($my_db, $ssh_reservation_sql);
	$ssh_reservation = mysqli_fetch_array($ssh_reservation_result);
	$res_time		 = array($res_time_get);
	$disabled_time 	 = (isset($ssh_reservation['disabled_time']) && $ssh_reservation['disabled_time'] != "") ? explode(',', $ssh_reservation['disabled_time']) : "";;
	$smarty->assign(array("f_res_time"=>$res_time, "f_disabled_time"=>$disabled_time));
}

if ($proc=="write") {

	$add_set = "display = '1'";

	if(!empty($_POST['f_res_date'])){
		$add_set.=", res_date = '".$_POST['f_res_date']."'";
	}
	if(!empty($_POST['f_res_time'])){
		$add_set.=", res_time = '".implode(',',$_POST['f_res_time'])."'";
	}
	if(!empty($_POST['f_type'])){
		$add_set.=", type = '".$_POST['f_type']."'";
	}
	if(!empty($_POST['f_company_name'])){
		$add_set.=", company_name = '".addslashes(trim($_POST['f_company_name']))."'";
	}
	if(!empty($_POST['f_email_0']) && !empty($_POST['f_email_1'])){
		$add_set.=", e_mail = '".$_POST['f_email_0']."@".$_POST['f_email_1']."'";
	}
	if(!empty($_POST['f_object'])){
		$add_set.=", object = '".addslashes($_POST['f_object'])."'";
	}
	if(!empty($_POST['f_people'])){
		$add_set.=", people = '".$_POST['f_people']."'";
	}
	if(!empty($_POST['f_director'])){
		$add_set.=", director = '".addslashes(trim($_POST['f_director']))."'";
	}
	if(!empty($_POST['f_director_tel'])){
		$add_set.=", director_tel = '".addslashes(trim($_POST['f_director_tel']))."', director_tel_agree = now()";
	}

	$add_use_val = "";
	for( $i = 1 ; $i <= 5 ; $i++) {
		$add_use_val .= isset($_POST['f_add_use_'.$i]) ? $_POST['f_add_use_'.$i] : "0";
	}
	$add_set.=", add_use = '".$add_use_val."'";


	if(!empty($_POST['f_user_type'])){
		$add_set.=", user_type = '".$_POST['f_user_type']."'";
	}
	if(!empty($_POST['f_pw'])){
		$add_set.=", pw = '".substr(md5($_POST['f_pw']),8,16)."'";
	}

	$sql="INSERT INTO ssh_reservation SET {$add_set}";
	//echo "<br><br><br>".$sql;exit;

	if (!mysqli_query($my_db, $sql))
		exit("<script>alert('방송국 사용 예약이 실패 하였습니다');location.href='ssh_reservation.php';</script>");
	else
		exit("<script>alert('방송국 사용 예약이 완료 되었습니다');location.href='ssh_reservation.php';</script>");


// form action 이 수정일 경우
} elseif($proc=="modify") {

	$add_set = "display = '1'";

	if(!empty($_POST['f_type'])){
		$add_set.=", type = '".$_POST['f_type']."'";
	}
	if(!empty($_POST['f_company_name'])){
		$add_set.=", company_name = '".addslashes(trim($_POST['f_company_name']))."'";
	}
	if(!empty($_POST['f_res_time'])){
		$add_set.=", res_time = '".implode(',',$_POST['f_res_time'])."'";
	}
	if(!empty($_POST['f_email_0']) && !empty($_POST['f_email_1'])){
		$add_set.=", e_mail = '".$_POST['f_email_0']."@".$_POST['f_email_1']."'";
	}
	if(!empty($_POST['f_object'])){
		$add_set.=", object = '".addslashes($_POST['f_object'])."'";
	}
	if(!empty($_POST['f_people'])){
		$add_set.=", people = '".$_POST['f_people']."'";
	}
	if(!empty($_POST['f_director'])){
		$add_set.=", director = '".addslashes(trim($_POST['f_director']))."'";
	}
	if(!empty($_POST['f_director_tel'])){
		$add_set.=", director_tel = '".addslashes(trim($_POST['f_director_tel']))."', director_tel_agree = now()";
	}

	$add_use_val = "";
	for( $i = 1 ; $i <= 5 ; $i++) {
		$add_use_val .= isset($_POST['f_add_use_'.$i]) ? $_POST['f_add_use_'.$i] : "0";
	}
	$add_set.=", add_use = '".$add_use_val."'";


	if(!empty($_POST['f_user_type'])){
		$add_set.=", user_type = '".$_POST['f_user_type']."'";
	}

	$sql = "UPDATE ssh_reservation SET {$add_set}
						WHERE
							no='".$_POST['no']."'
					";
	//echo "<br><br><br>".$sql;exit;

	if (!mysqli_query($my_db, $sql))
		exit("<script>alert('수정이 실패 하였습니다');location.href='ssh_reservation.php';</script>");
	else
		exit("<script>alert('수정이 완료 되었습니다');location.href='ssh_reservation.php';</script>");

} elseif($proc=="ssh_canceled") {
	$no=(isset($_POST['no']))?$_POST['no']:"";

	$sql = "UPDATE ssh_reservation SET display='2' WHERE no='".$no."'";
	//echo "<br><br><br>".$sql;exit;

	if (!mysqli_query($my_db, $sql))
		exit("<script>alert('예약 취소가 실패 하였습니다');history.back();</script>");
	else
		exit("<script>alert('예약이 취소 되었습니다');location.href='ssh_reservation.php';</script>");
}


// 템플릿에 해당 입력값 던져주기
$smarty->assign(
	array(
		"proc"=>$proc,
		"mode"=>$mode,
		"no"=>$no
	)
);
$smarty->display('ssh/ssh_reservation_regist.html');

?>
