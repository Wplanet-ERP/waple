<?php
require('../inc/common.php');
require('../inc/data_state.php');

$session_admin = isset($_SESSION['admin_security_pw']) ? $_SESSION['admin_security_pw'] : "";
if($session_admin != "ADMIN_PW_ACCESS") {
	$smarty->display('access_company_error.html');
	exit;
}
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.7, 2012-05-19
 */

/** Error reporting */
ini_set('error_reporting',E_ALL & ~E_NOTICE | E_STRICT);
date_Default_TimeZone_set("Asia/Seoul");	// 시간설정
define('ROOTPATH', dirname(__FILE__));


/** Include PHPExcel */
require_once '../Classes/PHPExcel.php';
require_once '../inc/common.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "no")
	->setCellValue('B3', "예약/취소")
	->setCellValue('C3', "예약일자")
	->setCellValue('D3', "예약시간")
	->setCellValue('E3', "구분")
	->setCellValue('F3', "기업명")
	->setCellValue('G3', "이메일주소")
	->setCellValue('H3', "촬영목적")
	->setCellValue('I3', "인원수")
	->setCellValue('J3', "책임자")
	->setCellValue('K3', "책임자 전화번호")
	->setCellValue('L3', "책임자 동의 날짜")
	->setCellValue('M3', "추가물품대여")
	->setCellValue('N3', "스튜디오 사용여부");


	// 리스트 내용
	$i=4;
	$ttamount=0;

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where = " 1=1";

// 리스트 쿼리
$ssh_reservation_sql = "
	SELECT
		sr.no,
		sr.display,
		sr.res_date,
		sr.res_time,
		sr.type,
		sr.company_name,
		sr.e_mail,
		sr.object,
		sr.people,
		sr.director,
		sr.director_tel,
		sr.director_tel_agree,
		sr.add_use,
		sr.user_type
	FROM ssh_reservation sr
	WHERE $add_where
	ORDER BY sr.no ASC
";


$idx_no=1;
$result= mysqli_query($my_db, $ssh_reservation_sql);
if(!!$result){
	for ($i = 4; $ssh_reservation_array = mysqli_fetch_array($result); $i++) {

		$f 			 = 0;
		$res_time 	 = $ssh_reservation_array['res_time'];
		$change_time = explode(',', $res_time);
		$time_count  = count($change_time) - 1;

		foreach($change_time as $time)
		{
			$res_time_value = $time.":00 ~ ".($time+1).":00";
			$display_value = "";
			switch ($ssh_reservation_array['display']) {
				case '1'	: $display_value = "예약"; break;
				case '2'	: $display_value = "취소"; break;
			}

			$type_value = "";
			switch ($ssh_reservation_array['type']) {
				case '1'	: $type_value = "예비창업자"; break;
				case '2'	: $type_value = "기존창업자"; break;
				case '3'	: $type_value = "해당없음"; break;
			}

			$add_use_value = "";
			for($f_i = 0 ; $f_i < 5 ; $f_i++) {
				if($ssh_reservation_array['add_use'][$f_i] == '1') {
					switch ($f_i) {
						case '0'	: $add_use_value .= "1. DSLR(Canon 80D) / "; break;
						case '1'	: $add_use_value .= "2. DSLR(Canon 80D) / "; break;
						case '2'	: $add_use_value .= "3. 포토박스(한빛 포토박스) / "; break;
						case '3'	: $add_use_value .= "4. 버티컬 삼각대, 볼헤드 / "; break;
						case '4'	: $add_use_value .= "5. 삼각대(ACE M GS 1002) / "; break;
					}
				}
			}

			switch ($ssh_reservation_array['user_type']) {
				case '1'	: $user_type_value = "신규 사용자"; break;
				case '2'	: $user_type_value = "기존 사용자"; break;
			}

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $ssh_reservation_array['no'])
				->setCellValue('B'.$i, $display_value)
				->setCellValue('C'.$i, $ssh_reservation_array['res_date'])
				->setCellValue('D'.$i, $res_time_value)
				->setCellValue('E'.$i, $type_value)
				->setCellValue('F'.$i, $ssh_reservation_array['company_name'])
				->setCellValue('G'.$i, $ssh_reservation_array['e_mail'])
				->setCellValue('H'.$i, $ssh_reservation_array['object'])
				->setCellValue('I'.$i, $ssh_reservation_array['people'])
				->setCellValue('J'.$i, $ssh_reservation_array['director'])
				->setCellValue('K'.$i, $ssh_reservation_array['director_tel'])
				->setCellValue('L'.$i, $ssh_reservation_array['director_tel_agree'])
				->setCellValue('M'.$i, $add_use_value)
				->setCellValue('N'.$i, $user_type_value);

			if($f != $time_count)
				$i++;

			$f++;
		}

	}
}

if($i > 1)
	$i = $i-1;

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "서울창업허브 방송국 예약 리스트");
$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


$objPHPExcel->getActiveSheet()->getStyle('A3:N'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:N'.$i)->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:N3')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle('A4:N'.$i)->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getFont()->setSize(11);

$objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
															->getStartColor()->setARGB('00c4bd97');
$objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:N'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('I4:I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


$i2_length = $i;
$i2=1;

while($i2_length){
	if($i2 > 2)
		$objPHPExcel->getActiveSheet()->getRowDimension($i2)->setRowHeight(20);

	if($i2 > 3 && $i2 % 2 == 1)
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i2.':N'.$i2)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ebf1de');

	$i2++;
	$i2_length--;
}

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(17);


$objPHPExcel->getActiveSheet()->setTitle('예약 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);



$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_".$session_name."_예약 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>

