<?php
require('../inc/common.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

/* Admin Check */
if(isset($_SESSION["admin_security_pw"]) && $session_admin = $_SESSION["admin_security_pw"])
{
	$admin_chk = ($session_admin == 'ADMIN_PW_ACCESS') ? true : false;
	$smarty->assign('admin_chk', $admin_chk);
}

/** 달력 만들기 START */
$today 		= date("Y-m-d");
$totime		= date("H");
$next14day 	= date("Y-m-d", strtotime("+14 day"));
$smarty->assign("totime", $totime);
$smarty->assign("next14day",$next14day);

// 달력 정보 가져오기 [START]
$sch_date_get = isset($_GET['sch_date']) ? $_GET['sch_date'] : $today; // get 값이 없는 경우 오늘을 기준으로 함
$smarty->assign("sch_date", $sch_date_get);

$day_token  = explode("-", $sch_date_get); // 들어온 날짜를 년,월,일로 분할해 변수로 저장합니다.
$base_year  = $day_token[0]; // 지정 년도
$base_month = $day_token[1]; // 지정된 월
$base_day   = $day_token[2]; // 지정된 일

$smarty->assign("base_year", $base_year);
$smarty->assign("base_month", $base_month);
$smarty->assign("base_day", $base_day);

$day_length = date("t", mktime(0,0,0, $base_month, $base_day, $base_year)); // 지정된 달은 몇일까지 있을까요?
$first_week = date("N", mktime(0,0,0, $base_month,1, $base_year)); // 지정된 달의 첫날은 무슨요일일까요? 1~7 월~일
$first_empty_week = $first_week%7; // 지정된 달 1일 앞의 공백 숫자.

$smarty->assign("day_length",$day_length);
$smarty->assign("first_week",$first_week);
$smarty->assign("first_empty_week",$first_empty_week);

$week_length = ($day_length+$first_empty_week)/7;
$week_length = ceil($week_length);
$week_length = $week_length; // 지정된 달은 총 몇주로 라인을 그어야 하나?

$smarty->assign("week_length",$week_length);

// 날짜와 요일 정보 담기
$date_i = 1;
for($i = 1 ; $i <= $first_empty_week ; $i++){
	$calendar_date[]=array(
		'day'=>'',
		'date'=>'',
		'week'=>'',
		'week_no' => ''
	);
}
while($date_i <= $day_length) {
	if($date_i < 10)
		$cal_day = '0'.$date_i;
	else {
		$cal_day = $date_i;
	}
	$calendar_date[]=array(
		'day'	  => $cal_day,
		'date'	  => date('Y-m-d', strtotime($base_year.'-'.$base_month.'-'.$date_i)),
		'week'	  => date('w', strtotime($base_year.'-'.$base_month.'-'.$date_i)),
		'week_no' => date('W', strtotime($base_year.'-'.$base_month.'-'.$date_i)) - (date('W', strtotime($base_year.'-'.$base_month.'-01'))-1)
	);
	$date_i++;
}

$smarty->assign("calendar_date", $calendar_date);

$next_day   = date("Y-m-d",mktime(0,0,0,$base_month,$base_day+1,$base_year)); // 다음날
$prev_dday  = date("Y-m-d",mktime(0,0,0,$base_month,$base_day-1,$base_year)); // 이전날
$next_month = date("Y-m-d",mktime(0,0,0,$base_month+1,$base_day,$base_year)); // 다음달
$prev_month = date("Y-m-d",mktime(0,0,0,$base_month-1,$base_day,$base_year)); // 지난달
$next_year  = date("Y-m-d",mktime(0,0,0,$base_month,$base_day,$base_year+1)); // 내년
$prev_year  = date("Y-m-d",mktime(0,0,0,$base_month,$base_day,$base_year-1)); // 작년

$smarty->assign("next_day",$next_day);
$smarty->assign("prev_dday",$prev_dday);
$smarty->assign("next_month",$next_month);
$smarty->assign("prev_month",$prev_month);
$smarty->assign("next_year",$next_year);
$smarty->assign("prev_year",$prev_year);
/* 달력 만들기 END */

/* Reservation List Start */
// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where = "1=1";

// 리스트 쿼리
$ssh_reservation_sql = "
	SELECT
		*
	FROM
		ssh_reservation s_r
	WHERE
		$add_where
		AND s_r.display = '1'
	ORDER BY
		s_r.res_date ASC, s_r.res_time ASC
";

$smarty->assign("query", $ssh_reservation_sql);

// 전체 게시물 수
$cnt_sql   = "SELECT count(*) FROM (".$ssh_reservation_sql.") AS cnt";
$cnt_query = mysqli_query($my_db, $cnt_sql);
if(!!$cnt_query)
	$cnt_data = mysqli_fetch_array($cnt_query);
$total_num = $cnt_data[0];

$smarty->assign(array("total_num"=>number_format($total_num)));
$smarty->assign("total_num",$total_num);

$accept_time = array(10,11,12,13,14,15,16,17,18);
$smarty->assign("accept_time", $accept_time);
// 리스트 쿼리 결과(데이터)
$result = mysqli_query($my_db, $ssh_reservation_sql);
if(!!$result)
{
	while ($ssh_reservation_array = mysqli_fetch_array($result))
	{
		if($res_time = $ssh_reservation_array['res_time'])
		{
			$change_time = explode(',', $res_time);
			foreach($change_time as $time)
			{
				$ssh_reservation_lists[$ssh_reservation_array['res_date']][$time] = array(
					"no" 			=> $ssh_reservation_array['no'],
					"display" 		=> $ssh_reservation_array['display'],
					"res_date" 		=> $ssh_reservation_array['res_date'],
					"res_time" 		=> $ssh_reservation_array['res_time'],
					"type" 			=> $ssh_reservation_array['type'],
					"company_name" 	=> $ssh_reservation_array['company_name'],
					"e_mail" 		=> $ssh_reservation_array['e_mail'],
					"object" 		=> $ssh_reservation_array['object'],
					"people" 		=> $ssh_reservation_array['people'],
					"director" 		=> $ssh_reservation_array['director'],
					"director_tel" 	=> $ssh_reservation_array['director_tel'],
					"add_use" 		=> $ssh_reservation_array['add_use'],
					"user_type" 	=> $ssh_reservation_array['user_type']
				);
			}
		}
	}
}

$smarty->assign(array("ssh_reservation_list" => $ssh_reservation_lists));
$smarty->display('ssh/ssh_reservation.html');

?>
