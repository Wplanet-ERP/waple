<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');

# Navigation & My Quick
$nav_prd_no  = "239";
$nav_title   = "OKR 현황";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

$team_model         = Team::Factory();
$team_active_list   = $team_model->getActiveTeamList();
$cur_year           = date('Y');
$cur_month          = date("m");
$cur_date_term      = "";
$cur_s_date         = "";
$cur_e_date         = "";

switch($cur_month){
    case "1":
    case "2":
    case "3":
    case "4":
        $cur_date_term  = "1";
        $cur_s_date     = "{$cur_year}-01-01";
        $cur_e_date     = "{$cur_year}-04-30";
        break;
    case "5":
    case "6":
    case "7":
    case "8":
        $cur_date_term  = "2";
        $cur_s_date     = "{$cur_year}-05-01";
        $cur_e_date     = "{$cur_year}-08-31";
        break;
    case "9":
    case "10":
    case "11":
    case "12":
        $cur_date_term  = "3";
        $cur_s_date     = "{$cur_year}-09-01";
        $cur_e_date     = "{$cur_year}-12-31";
        break;
}

$add_team_where     = "oo.`type`='base' AND oo.display='1' AND oo.`kind`='1' AND t.display='1' AND `team` != '00200'";
$add_staff_where    = "oo.`type`='base' AND oo.display='1' AND oo.`kind`='2' AND s.staff_state='1'";
$sch_team 	        = isset($_GET['sch_team']) ? $_GET['sch_team'] : $session_team;
$sch_date_term      = isset($_GET['sch_date_term']) && !empty($_GET['sch_date_term']) ? $_GET['sch_date_term'] : $cur_date_term;
$sch_year_term      = isset($_GET['sch_year_term']) ? $_GET['sch_year_term'] : $cur_year;
$okr_top_list       = [];
$okr_team_list      = [];
$okr_staff_list     = [];

if(!empty($sch_year_term))
{
    if($sch_team == "00236"){
        $sch_team = "00221";
    }

    $sch_s_date_term = "";
    $sch_e_date_term = "";
    switch($sch_date_term){
        case "1":
            $sch_s_date_term    = "{$sch_year_term}-01-01";
            $sch_e_date_term    = "{$sch_year_term}-04-30";
            break;
        case "2":
            $sch_s_date_term    = "{$sch_year_term}-05-01";
            $sch_e_date_term    = "{$sch_year_term}-08-31";
            break;
        case "3":
            $sch_date_term      = "3";
            $sch_s_date_term    = "{$sch_year_term}-09-01";
            $sch_e_date_term    = "{$sch_year_term}-12-31";
            break;
    }
    $add_team_where     .= " AND oo.sdate='{$sch_s_date_term}' AND oo.edate='{$sch_e_date_term}'";
    $add_staff_where    .= " AND oo.sdate='{$sch_s_date_term}' AND oo.edate='{$sch_e_date_term}'";

    $okr_top_sql    = "SELECT * FROM okr_obj AS oo WHERE oo.`type`='base' AND oo.display='1' AND oo.`kind`='1' AND oo.sdate='{$sch_s_date_term}' AND oo.edate='{$sch_e_date_term}' AND oo.`team`='00200'";
    $okr_top_query  = mysqli_query($my_db, $okr_top_sql);
    $okr_top_result = mysqli_fetch_assoc($okr_top_query);

    if(isset($okr_top_result['oo_no'])){
        $okr_top_list = array("name" => "와이즈플래닛+아이즈미디어커머스", "title" => $okr_top_result['obj_name']);
    }

    if(empty($sch_team) || $sch_team == "00200")
    {
        $okr_team_sql = "
            SELECT
                oo.oo_no,
                oo.`team`,
                t.team_name,
                oo.obj_name
            FROM okr_obj AS oo
            LEFT JOIN team AS t ON t.team_code=oo.`team`
            WHERE {$add_team_where}
            ORDER BY t.priority
        ";
        $okr_team_query = mysqli_query($my_db, $okr_team_sql);
        while($okr_team = mysqli_fetch_assoc($okr_team_query))
        {
            $avg_complete_rate 	= 0;

            #OKR 목표점수 및 완료점수 계산
            $okr_kr_sql = "SELECT importance, difficulty, challenge, complete_rate FROM okr_kr ok where ok.oo_no='{$okr_team['oo_no']}'";
            $okr_kr_query = mysqli_query($my_db, $okr_kr_sql);
            $purpose_quality  = 0;
            $complete_quality = 0;
            while($okr_kr_result = mysqli_fetch_assoc($okr_kr_query))
            {
                $importance_val 	= $okr_kr_result['importance'];
                $difficulty_val 	= $okr_kr_result['difficulty'];
                $challenge_val  	= $okr_kr_result['challenge'];
                $complete_rate_val  = $okr_kr_result['complete_rate'];

                $purpose_quality += round((($importance_val*0.3) + ($difficulty_val*0.3) + ($challenge_val*0.4)), 1);

                if($complete_rate_val > 0){
                    $complete_rate = $complete_rate_val/100;
                    $complete_quality += round((($importance_val*0.3*$complete_rate) + ($difficulty_val*0.3*$complete_rate) + ($challenge_val*0.4*$complete_rate)), 1);
                }
            }

            $avg_complete_rate = ($complete_quality > 0 && $purpose_quality > 0) ? round(($complete_quality/$purpose_quality)*100, 2) : 0;

            $okr_team_list[] = array(
                "oo_no"         => $okr_team['oo_no'],
                "name"          => $okr_team['team_name'],
                "title"         => $okr_team['obj_name'],
                "comp_quality"  => $complete_quality,
                "pur_quality"   => $purpose_quality,
                "avg_rate"      => $avg_complete_rate,
                "link_url"      => "https://work.wplanet.co.kr/v1/okr_list.php?sch_s_date_term={$sch_s_date_term}&sch_e_date_term={$sch_e_date_term}&sch_kind=1&sch_active=1&sch_team={$okr_team['team']}&sch_s_no=all"
            );
        }
    }
    elseif(!empty($sch_team))
    {
        $add_team_where .= " AND oo.team='{$sch_team}'";
        $okr_team_sql    = "
            SELECT
                oo.oo_no,
                oo.`team`,
                t.team_name,
                oo.obj_name
            FROM okr_obj AS oo
            LEFT JOIN team AS t ON t.team_code=oo.`team`
            WHERE {$add_team_where}
            ORDER BY t.priority
        ";
        $okr_team_query = mysqli_query($my_db, $okr_team_sql);
        while($okr_team = mysqli_fetch_assoc($okr_team_query))
        {
            $avg_complete_rate 	= 0;

            #OKR 목표점수 및 완료점수 계산
            $okr_kr_sql = "SELECT importance, difficulty, challenge, complete_rate FROM okr_kr ok where ok.oo_no='{$okr_team['oo_no']}'";
            $okr_kr_query = mysqli_query($my_db, $okr_kr_sql);
            $purpose_quality  = 0;
            $complete_quality = 0;
            while($okr_kr_result = mysqli_fetch_assoc($okr_kr_query))
            {
                $importance_val 	= $okr_kr_result['importance'];
                $difficulty_val 	= $okr_kr_result['difficulty'];
                $challenge_val  	= $okr_kr_result['challenge'];
                $complete_rate_val  = $okr_kr_result['complete_rate'];

                $purpose_quality += round((($importance_val*0.3) + ($difficulty_val*0.3) + ($challenge_val*0.4)), 1);

                if($complete_rate_val > 0){
                    $complete_rate = $complete_rate_val/100;
                    $complete_quality += round((($importance_val*0.3*$complete_rate) + ($difficulty_val*0.3*$complete_rate) + ($challenge_val*0.4*$complete_rate)), 1);
                }
            }

            $avg_complete_rate = ($complete_quality > 0 && $purpose_quality > 0) ? round(($complete_quality/$purpose_quality)*100, 2) : 0;

            $okr_team_list[] = array(
                "oo_no"         => $okr_team['oo_no'],
                "name"          => $okr_team['team_name'],
                "title"         => $okr_team['obj_name'],
                "comp_quality"  => $complete_quality,
                "pur_quality"   => $purpose_quality,
                "avg_rate"      => $avg_complete_rate,
                "link_url"      => "okr_regist.php?oo_no={$okr_team['oo_no']}"
            );
        }

        $sel_team_text = getTeamWhere($my_db, $sch_team);
        if($sch_team == "00221"){
            $sel_team_text .= ",00236";
        }
        $add_staff_where .= " AND oo.`team` IN({$sel_team_text})";

        $okr_staff_sql = "
            SELECT
                oo.oo_no,
                oo.`s_no`,
                s.s_name,
                oo.obj_name
            FROM okr_obj AS oo
            LEFT JOIN staff AS s ON s.s_no=oo.s_no
            WHERE {$add_staff_where}
            ORDER BY s.s_no ASC
        ";
        $okr_staff_query = mysqli_query($my_db, $okr_staff_sql);
        while($okr_staff = mysqli_fetch_assoc($okr_staff_query))
        {
            $avg_complete_rate 	= 0;
            $okr_kr_list        = [];

            #OKR 목표점수 및 완료점수 계산
            $okr_kr_sql         = "SELECT kr_name, importance, difficulty, challenge, complete_rate FROM okr_kr ok where ok.oo_no='{$okr_staff['oo_no']}'";
            $okr_kr_query       = mysqli_query($my_db, $okr_kr_sql);
            $purpose_quality    = 0;
            $complete_quality   = 0;
            while($okr_kr_result = mysqli_fetch_assoc($okr_kr_query))
            {
                $importance_val 	= $okr_kr_result['importance'];
                $difficulty_val 	= $okr_kr_result['difficulty'];
                $challenge_val  	= $okr_kr_result['challenge'];
                $complete_rate_val  = $okr_kr_result['complete_rate'];

                $purpose_quality   += round((($importance_val*0.3) + ($difficulty_val*0.3) + ($challenge_val*0.4)), 1);

                if($complete_rate_val > 0){
                    $complete_rate      = $complete_rate_val/100;
                    $complete_quality  += round((($importance_val*0.3*$complete_rate) + ($difficulty_val*0.3*$complete_rate) + ($challenge_val*0.4*$complete_rate)), 1);
                }

                $okr_kr_list[] = $okr_kr_result;
            }

            $avg_complete_rate  = ($complete_quality > 0 && $purpose_quality > 0) ? round(($complete_quality/$purpose_quality)*100, 2) : 0;
            $okr_staff_list[]   = array(
                "oo_no"         => $okr_staff['oo_no'],
                "name"          => $okr_staff['s_name'],
                "title"         => $okr_staff['obj_name'],
                "comp_quality"  => $complete_quality,
                "pur_quality"   => $purpose_quality,
                "avg_rate"      => $avg_complete_rate,
                "okr_kr_list"   => $okr_kr_list,
                "link_url"      => "okr_regist.php?oo_no={$okr_staff['oo_no']}"
            );
        }
    }
}

$date_term_list  = array(
    '1' => "1/3 분기",
    '2' => "2/3 분기",
    '3' => "3/3 분기",
);

$smarty->assign("date_term_list", $date_term_list);
$smarty->assign("team_active_list", $team_active_list);
$smarty->assign("sch_team", $sch_team);
$smarty->assign("sch_date_term", $sch_date_term);
$smarty->assign("sch_year_term", $sch_year_term);
$smarty->assign("okr_top_list", $okr_top_list);
$smarty->assign("okr_team_list", $okr_team_list);
$smarty->assign("okr_staff_list", $okr_staff_list);

$smarty->display('okr_state.html');
?>
