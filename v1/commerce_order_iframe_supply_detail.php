<?php
require('inc/common.php');
require('ckadmin.php');

# 발주서 날짜별 입고상세내역
$no = isset($_GET['no']) ? $_GET['no'] : "";

# 발주서 입고현황
$com_rep_sql 	= "SELECT co.option_no, (SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_name, SUM(co.quantity) as qty FROM commerce_order co WHERE co.set_no='{$no}' AND co.type='request' GROUP BY co.option_no";
$com_rep_query 	= mysqli_query($my_db, $com_rep_sql);
$com_rep_list 	= [];
while($com_rep_result = mysqli_fetch_array($com_rep_query))
{
    $com_rep_list[$com_rep_result['option_no']] = array(
        'option_name' => $com_rep_result['option_name'],
        'sup_qty' 	  => 0,
        'req_qty' 	  => $com_rep_result['qty'],
        'dem_qty' 	  => $com_rep_result['qty'],
        'option_per'  => 0,
    );
}

# 발주서 입고상세내역
$commerce_order_sql = "
    SELECT
        co.no,
        co.option_no,
        (SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_name,
        (SELECT pcu.priority FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_priority,
        co.quantity,
        co.sup_date
    FROM commerce_order co
    WHERE co.set_no='{$no}' AND co.type='supply'
    ORDER BY sup_date ASC, option_priority ASC, no ASC
";
$commerce_order_query = mysqli_query($my_db, $commerce_order_sql);
$comm_date_list = [];
while($commerce_order_result = mysqli_fetch_array($commerce_order_query))
{
    if(isset($com_rep_list[$commerce_order_result['option_no']])){
        $com_rep_list[$commerce_order_result['option_no']]['sup_qty'] += $commerce_order_result['quantity'];
        $com_rep_list[$commerce_order_result['option_no']]['dem_qty'] -= $commerce_order_result['quantity'];
    }

    $comm_date_list[] = $commerce_order_result['sup_date'];
}

$com_rep_detail_list = [];
$com_rep_total = [];
if(!empty($com_rep_list)){
    foreach($com_rep_list as $key => $com_rep)
    {
        $com_rep_detail_list[$key] = array(
            'option_name' 	 => $com_rep['option_name'],
            'req_qty' 	  	 => $com_rep['req_qty'],
            'sup_qty' 	  	 => $com_rep['sup_qty'],
            'sub_qty' 	  	 => ($com_rep['req_qty']-$com_rep['sup_qty']),
            'date_data_list' => []
        );

        $com_rep_total['req_total'] += $com_rep['req_qty'];
        $com_rep_total['sup_total'] += $com_rep['sup_qty'];
        $com_rep_total['sub_total'] += ($com_rep['req_qty']-$com_rep['sup_qty']);
    }
}

# 입고상세내역 계산
$date_count = 1;
$comm_date_total = [];
if(!empty($com_rep_detail_list))
{
    if(!empty($comm_date_list))
    {
        $comm_date_list = array_unique($comm_date_list);

        $comm_date_sql   = "SELECT option_no, sup_date, SUM(quantity) as qty FROM commerce_order WHERE set_no='{$no}' AND `type` = 'supply' GROUP BY option_no, sup_date";
        $comm_date_query = mysqli_query($my_db, $comm_date_sql);
        $comm_date_data  = [];
        while($comm_date_result = mysqli_fetch_assoc($comm_date_query))
        {
            $comm_date_data[$comm_date_result['option_no']][$comm_date_result['sup_date']] = $comm_date_result['qty'];
        }

        foreach($comm_date_list as $comm_date)
        {
            foreach($com_rep_detail_list as $option_key => $com_rep)
            {
                $com_date_val = isset($comm_date_data[$option_key]) && isset($comm_date_data[$option_key][$comm_date]) ? $comm_date_data[$option_key][$comm_date] : 0;
                $com_rep['date_data_list'][] = $com_date_val;

                $comm_date_total[$comm_date] += $com_date_val;
                $com_rep_detail_list[$option_key] = $com_rep;
            }
        }

        $date_count = count($comm_date_list);
    }
}

$smarty->assign('date_count', $date_count);
$smarty->assign('comm_date_total', $comm_date_total);
$smarty->assign('comm_date_list', $comm_date_list);
$smarty->assign('com_rep_total', $com_rep_total);
$smarty->assign('com_rep_detail_list', $com_rep_detail_list);
$smarty->assign('set_no', $no);

$smarty->display('commerce_order_iframe_supply_detail.html');

?>
