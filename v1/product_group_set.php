<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');

// 접근 권한
if (!permissionNameCheck($session_permission,"마스터관리자") && $session_s_no != '29' && $session_s_no != '102' && $session_s_no != '171'){
	$smarty->display('access_error.html');
	exit;
}

$proc	= isset($_POST['process']) ? $_POST['process'] : "";
$target	= isset($_POST['target']) ? $_POST['target'] : "";


$kind_group_option = array(
	"product" 		=> array("code" => "01", "label" => "와이즈 업무"),
	"product_cms" 	=> array("code" => "04", "label" => "커머스 상품"),
	"asset" 		=> array("code" => "05", "label" => "자산관리"),
	"sales" 		=> array("code" => "08", "label" => "커머스 매출"),
	"cost"	 		=> array("code" => "09", "label" => "커머스 비용"),
	"utm" 			=> array("code" => "10", "label" => "커머스 UTM"),
	"guide" 		=> array("code" => "20", "label" => "와플 GUIDE"),
	"corp_card" 	=> array("code" => "30", "label" => "법인카드"),
	"approval" 		=> array("code" => "50", "label" => "결재구분"),
	"bad_reason" 	=> array("code" => "60", "label" => "불량사유"),
	"brand" 		=> array("code" => "70", "label" => "커머스 브랜드"),
	"account_code"	=> array("code" => "03", "label" => "개인경비 계정과목"),
);

if($proc=="write") // form action 이 추가일 경우
{
	$sch_k_code_post = isset($_POST['sch_k_code']) ? $_POST['sch_k_code'] : "product";
	$k_code_val	= "product";
	if(!empty($sch_k_code_post)) {
		$k_code_val = $sch_k_code_post;
	}

	// k_name_code 값 구하기 (product의 최고값)
	$sql	= "SELECT MAX(CONCAT(k_name_code)) FROM kind WHERE k_code='{$k_code_val}'";
	$result	= mysqli_query($my_db, $sql);
	$kind	= mysqli_fetch_array($result);
	$max 	= ($kind[0] == 0) ? $kind_group_option[$k_code_val]['code']."000": $kind[0];
	$k_name_code = $max+1;

	if($target == "f_prd1")
	{
		$sql = "
			INSERT INTO kind SET
				k_code = '{$k_code_val}',
				k_name_code = '{$k_name_code}',
				k_name = '".$_POST[$target.'_w_name']."',
				priority = '".$_POST[$target.'_w_priority']."'
		";

	} elseif($target == "f_prd2"){
		$sql = "
			INSERT INTO kind SET
				k_code = '{$k_code_val}',
				k_name_code = '{$k_name_code}',
				k_name = '".$_POST[$target.'_w_name']."',
				k_parent = '".$_POST['f_w_prd2_parent']."',
				priority = '".$_POST[$target.'_w_priority']."'
		";
	}

	if($my_db->query($sql)){
		alert("추가하였습니다.","product_group_set.php?sch_k_code={$sch_k_code_post}");
	}else{
		alert("추가에 실패했습니다.","product_group_set.php?sch_k_code={$sch_k_code_post}");
	}

} elseif($proc=="modify") { // form action 이 수정일 경우
	$sch_k_code_post=isset($_POST['sch_k_code']) ? $_POST['sch_k_code'] : "product";

	if(!empty($sch_k_code_post)) {
		$k_code_val = $sch_k_code_post;
	}

	if($target == "f_prd1"){
		$sql = "
			UPDATE kind SET
				k_name = '{$_POST[$target.'_name']}',
				priority = '{$_POST[$target.'_priority']}',
				display = '{$_POST[$target.'_display']}'
			WHERE
				k_name_code = '{$_POST[$target.'_code']}'
			";

		$my_db->query($sql);
		alert("수정하였습니다.","product_group_set.php?sch_k_code={$sch_k_code_post}");
	}else if($target == "f_prd2"){
		$sql = "
			UPDATE kind SET
				k_parent = '{$_POST['f_prd2_parent']}',
				k_name = '{$_POST[$target.'_name']}',
				priority = '{$_POST[$target.'_priority']}',
				display = '{$_POST[$target.'_display']}'
			WHERE
				k_name_code = '{$_POST[$target.'_code']}'
			";

		$my_db->query($sql);
		alert("수정하였습니다.","product_group_set.php?sch_k_code={$sch_k_code_post}");
	}

} else{

	# Navigation & My Quick
	$nav_prd_no  = "70";
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	$sch_k_code_get = isset($_GET['sch_k_code']) ? $_GET['sch_k_code'] : "product";

	if($session_s_no == '102'){
		if($sch_k_code_get != "bad_reason"){
			$sch_k_code_get = "bad_reason";
		}
	}
	$smarty->assign("sch_k_code",$sch_k_code_get);

	if(!empty($sch_k_code_get)) {
		$k_code_val = $sch_k_code_get;
	}

	// 상품 구분 가져오기(1차)
	$sql="SELECT k_name,k_name_code,priority,display FROM kind where k_code='{$k_code_val}' AND k_parent is null ORDER BY priority ASC";
	$result=mysqli_query($my_db,$sql);

	while($kind=mysqli_fetch_array($result)) {
	  $g_product[]=array(
	    "product_priority"=>trim($kind['priority']),
	    "product_name"=>trim($kind['k_name']),
	    "product_code"=>trim($kind['k_name_code']),
	    "display"=>trim($kind['display'])
	  );
	  $smarty->assign("g_product",$g_product);
	}

	// 2차 상품 구분 가져오기(2차)
	$sql = "SELECT k_name,k_name_code,k_parent,priority,display FROM kind WHERE k_code='{$k_code_val}' AND k_parent IS NOT NULL ORDER BY priority ASC";

	$result=mysqli_query($my_db,$sql);
	while($kind=mysqli_fetch_array($result)) {
		$edit_g_product[]=array(
	    	"product_priority"=>trim($kind['priority']),
			"product_name"=>trim($kind['k_name']),
			"product_code"=>trim($kind['k_name_code']),
			"display"=>trim($kind['display']),
		);
		$smarty->assign("edit_g_product",$edit_g_product);
	}
}
$smarty->assign("kind_group_option", $kind_group_option);
$smarty->display('product_group_set.html');

?>
