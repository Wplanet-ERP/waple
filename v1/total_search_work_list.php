<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');

# 검색 초기화 및 조건 생성
$add_where = "1=1";

# 가이드 세부 검색
$sch_keyword  = isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";
$smarty->assign('sch_keyword', $sch_keyword);

$keyword_list 		= [];
$keyword_list_sql 	= "SELECT prd_no, keyword FROM keyword_search WHERE kind='product'";
$keyword_list_query = mysqli_query($my_db, $keyword_list_sql);
while ($keyword = mysqli_fetch_array($keyword_list_query)) {
    $keyword_list[$keyword['prd_no']][] = $keyword['keyword'];
}

// [Quick Prd 확인]
$quick_prd_sql 		= "SELECT quick_prd FROM staff where id = '{$session_id}'";
$quick_prd_query 	= mysqli_query($my_db, $quick_prd_sql);
$quick_prd_list 	= mysqli_fetch_array($quick_prd_query);
$sch_quick_prd 		= explode(',', $quick_prd_list[0]);

// [Keyword List 관리]
$work_keyword_sql = "
		SELECT
			kp.k_name_code AS prd_g1_no,
			p.k_name_code AS prd_g2_no,
			p.prd_no AS prd_no,
			kp.k_name AS prd_g1,
			k.k_name AS prd_g2,
			p.title AS prd_name,
			p.description AS prd_desc,
			p.keyword AS keyword,
		    (SELECT sub.q_no FROM quick_search as sub WHERE sub.`type`='work' AND sub.s_no='{$session_s_no}' AND sub.prd_no=p.prd_no) AS quick_prd
		FROM product AS p
		LEFT JOIN kind AS k ON k.k_name_code = p.k_name_code AND k.k_code='product'
		LEFT JOIN kind AS kp ON kp.k_name_code = k.k_parent AND kp.k_code='product'
		WHERE p.keyword LIKE '%{$sch_keyword}%' AND p.display='1'
		ORDER BY p.prd_no DESC
";

# 전체 게시물 수
$work_keyword_total_sql		= "SELECT count(prd_no) FROM (SELECT `p`.prd_no FROM product as p LEFT JOIN kind AS k ON k.k_name_code = p.k_name_code AND k.k_code='product' LEFT JOIN kind AS kp ON kp.k_name_code = k.k_parent AND kp.k_code='product' WHERE p.keyword LIKE '%{$sch_keyword}%' AND p.display='1') AS cnt";
$work_keyword_total_query	= mysqli_query($my_db, $work_keyword_total_sql);
if(!!$work_keyword_total_query)
    $work_keyword_total_result = mysqli_fetch_array($work_keyword_total_query);

$work_keyword_total = $work_keyword_total_result[0];

# 페이징
$page_type  = (isset($_GET['sch_org_page']) && intval($_GET['sch_org_page']) > 0) ? intval($_GET['sch_org_page']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($work_keyword_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "total_search_work_list.php", $pagenum, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $work_keyword_total);
$smarty->assign("pagelist", $page);
$smarty->assign("ord_page_type", $page_type);

$work_keyword_sql  .= " LIMIT {$offset}, {$num}";

$sch_keyword_list 	= [];
$sch_keyword_query 	= mysqli_query($my_db, $work_keyword_sql);
while ($product_keyword = mysqli_fetch_array($sch_keyword_query)) {
    $prd_g1_url 	    = "work_list.php?sch_prd_g1=" . $product_keyword['prd_g1_no'];
    $prd_g2_url 	    = $prd_g1_url . "&sch_prd_g2=" . $product_keyword['prd_g2_no'];
    $prd_url 		    = $prd_g2_url . "&sch_prd=" . $product_keyword['prd_no'];
    $exp_prd_list		= explode(',', $product_keyword['keyword']);
    $prd_keyword_list   = "#".implode(' #', $exp_prd_list);
    $prd_keyword        = str_replace($sch_keyword,"<span class='font-red'>{$sch_keyword}</span>", $prd_keyword_list);

    $sch_keyword_list[] = array
    (
        'prd_g1' 		=> $product_keyword['prd_g1'],
        'prd_g2' 		=> $product_keyword['prd_g2'],
        'prd_no' 		=> $product_keyword['prd_no'],
        'prd_name' 		=> $product_keyword['prd_name'],
        'prd_desc' 		=> str_replace("\r\n", "<br/>", $product_keyword['prd_desc']),
        'prd_g1_url' 	=> $prd_g1_url,
        'prd_g2_url' 	=> $prd_g2_url,
        'prd_url' 		=> $prd_url,
        'keyword' 		=> $prd_keyword,
        'quick_prd' 	=> $product_keyword['quick_prd']
    );
}

$smarty->assign('sch_keyword_list', $sch_keyword_list);
$smarty->assign('sch_keyword_list_count', count($sch_keyword_list));
$smarty->assign('page_type_list', getPageTypeOption('4'));

$smarty->display('total_search_work_list.html');

?>
