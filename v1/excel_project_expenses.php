<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/project.php');
require('inc/helper/company.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");


$workSheet = $objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "내부통용사업명")
    ->setCellValue('B1', "구분")
    ->setCellValue('C1', "업체명/성명")
    ->setCellValue('D1', "청구금액")
    ->setCellValue('E1', "세액")
    ->setCellValue('F1', "지출액")
    ->setCellValue('G1', "출금통장")
    ->setCellValue('H1', "은행명")
    ->setCellValue('I1', "계좌번호")
    ->setCellValue('J1', "예금주")
    ->setCellValue('K1', "예금주정보")
    ->setCellValue('L1', "원천세 신고방식")
    ->setCellValue('M1', "비고")
    ->setCellValue('N1', "개인경비내용")
    ->setCellValue('O1', "정산방법")
    ->setCellValue('P1', "지출방식")
    ->setCellValue('Q1', "제출자")
    ->setCellValue('R1', "작성일")
    ->setCellValue('S1', "승인일")
;

$add_where = "pje.display='1'";
# 사업비 지출관리
$sch_pj_no          = isset($_GET['sch_pj_no']) ? $_GET['sch_pj_no'] : "";
$sch_pj_name        = isset($_GET['sch_pj_name']) ? $_GET['sch_pj_name'] : "";
$sch_cal_method     = isset($_GET['sch_cal_method']) ? $_GET['sch_cal_method'] : "";
$sch_spend_method   = isset($_GET['sch_spend_method']) ? $_GET['sch_spend_method'] : "1";
$sch_req_team       = isset($_GET['sch_req_team']) ? $_GET['sch_req_team'] : "";
$sch_req_name       = isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : $session_name;
$sch_req_s_date     = isset($_GET['sch_req_s_date']) ? $_GET['sch_req_s_date'] : date('Y-m-d',strtotime("-6 day"));
$sch_req_e_date     = isset($_GET['sch_req_e_date']) ? $_GET['sch_req_e_date'] : date('Y-m-d');
$sch_req_all        = isset($_GET['sch_req_all']) ? $_GET['sch_req_all'] : "";
$sch_appr_s_date    = isset($_GET['sch_appr_s_date']) ? $_GET['sch_appr_s_date'] : "";
$sch_appr_e_date    = isset($_GET['sch_appr_e_date']) ? $_GET['sch_appr_e_date'] : "";
$sch_c_name         = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_pj_kind        = isset($_GET['sch_pj_kind']) ? $_GET['sch_pj_kind'] : "";
$sch_bankbook       = isset($_GET['sch_bankbook']) ? $_GET['sch_bankbook'] : "";
$sch_bk_name        = isset($_GET['sch_bk_name']) ? $_GET['sch_bk_name'] : "";
$sch_memo           = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
$sch_payment_contents  = isset($_GET['sch_payment_contents']) ? $_GET['sch_payment_contents'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "req_date";
$ori_ord_type       = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "req_date";
$sch_exp_status     = isset($_GET['sch_exp_status']) ? $_GET['sch_exp_status'] : "";

if(!empty($sch_pj_no)) {
    $add_where .= " AND pje.pj_no = '{$sch_pj_no}'";
}

if(!empty($sch_pj_name)) {
    $add_where .= " AND pje.pj_name like '%{$sch_pj_name}%'";
}

if(!empty($sch_cal_method)) {
    $add_where .= " AND pje.cal_method = '{$sch_cal_method}'";
}

if(!empty($sch_spend_method)) {
    $add_where .= " AND pje.spend_method = '{$sch_spend_method}'";
}

if (!empty($sch_req_team))
{
    if($sch_req_team != 'all')
    {
        $sch_team_code_where = getTeamWhere($my_db, $sch_req_team);
        if($sch_team_code_where){
            $add_where .= " AND `pje`.req_team IN ({$sch_team_code_where})";
        }
    }
}else{

    $sch_team_code_where = getTeamWhere($my_db, $session_team);
    if($sch_team_code_where){
        $add_where .= " AND `pje`.req_team IN ({$sch_team_code_where})";
    }
}

if(!empty($sch_req_name)) {
    $add_where .= " AND pje.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_req_name}%')";
}

if($sch_req_all != 'all') { // sch month not alll
    if(!empty($sch_req_s_date))
    {
        $add_where .= " AND pje.req_date >= '{$sch_req_s_date}'";

    }

    if(!empty($sch_req_e_date))
    {
        $add_where .= " AND pje.req_date <= '{$sch_req_e_date}'";
    }
}

if(!empty($sch_appr_s_date))
{
    $add_where .= " AND pje.appr_date >= '{$sch_appr_s_date}'";
}

if(!empty($sch_appr_e_date))
{
    $add_where .= " AND pje.appr_date <= '{$sch_appr_e_date}'";
}

if(!empty($sch_c_name)) {
    $add_where .= " AND pje.c_name like '%{$sch_c_name}%'";
}

if(!empty($sch_pj_kind)) {
    $add_where .= " AND pje.kind IN(SELECT pek.pj_k_no FROM project_expenses_kind pek WHERE pek.k_name like '%{$sch_pj_kind}%')";
}

if(!empty($sch_bankbook)) {
    $add_where .= " AND pje.bankbook = '{$sch_bankbook}'";
}

if(!empty($sch_bk_name)) {
    $add_where .= " AND pje.bk_name like '%{$sch_bk_name}%'";
}

if(!empty($sch_memo)) {
    $add_where .= " AND pje.memo like '%{$sch_memo}%'";
}

if(!empty($sch_payment_contents)) {
    $add_where .= " AND pje.pe_no IN(SELECT pe.pe_no FROM personal_expenses pe WHERE pe.payment_contents like '%{$sch_payment_contents}%')";
}

if(!empty($sch_exp_status)) {
    $add_where .= " AND pje.expenses_status = '{$sch_exp_status}'";
}

$add_orderby = "";
$ord_type_by = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";
    if(!empty($ord_type_by))
    {
        $ord_type_list = [];
        if($ord_type == 'appr_date'){
            $ord_type_list[] = "appr_date";
        }
        elseif($ord_type == 'kind')
        {
            $ord_type_list[] = "pj_no";
            $ord_type_list[] = "kind";
        }
        elseif($ord_type == 'pj_name')
        {
            $ord_type_list[] = "pj_name";
        }
        elseif($ord_type == 'req_name')
        {
            $ord_type_list[] = "req_name";
        }
        elseif($ord_type == 'req_date')
        {
            $ord_type_list[] = "req_date";
        }

        if($ori_ord_type == $ord_type)
        {
            if($ord_type_by == '1'){
                $orderby_val = "ASC";
            }elseif($ord_type_by == '2'){
                $orderby_val = "DESC";
            }
        }else{
            $ord_type_by = '2';
            $orderby_val = "DESC";
        }

        foreach($ord_type_list as $ord_type_val){
            $add_orderby .= "{$ord_type_val} {$orderby_val}, ";
        }
    }else{
        $add_orderby .= "pj_e_no DESC, ";
    }
}

$add_orderby .= "pj_e_no DESC";

# 리스트 쿼리
$project_expenses_sql = "
    SELECT
        *,
        (SELECT s.s_name FROM staff s WHERE s.s_no=pje.req_s_no LIMIT 1) as req_name,
        (SELECT pe.fran_c_name FROM personal_expenses pe WHERE pe.pe_no=pje.pe_no LIMIT 1) as pe_c_name,
        (SELECT pe.payment_contents FROM personal_expenses pe WHERE pe.pe_no=pje.pe_no LIMIT 1) as pe_content,
        (SELECT w.c_name FROM `work` w WHERE w.w_no = (SELECT wd.w_no FROM withdraw wd WHERE wd.wd_no=pje.wd_no LIMIT 1) LIMIT 1) as wd_content
    FROM project_expenses pje
    WHERE {$add_where}
    ORDER BY {$add_orderby}
";
$idx = 2;
$project_expenses_query = mysqli_query($my_db, $project_expenses_sql);
$cal_method_option      = getCalMethodOption();
$spend_method_option    = getSpendMethodOption();
$bankbook_option        = getBankBookOption();
$vat_type_option        = getLectorVatTypeOption();
while($project_expenses = mysqli_fetch_assoc($project_expenses_query))
{
    $pj_kind_list = [];
    $pj_no = $project_expenses['pj_no'];

    $kind_name = "";
    if(!empty($pj_no))
    {
        $pj_kind_sql = "SELECT * FROM project_expenses_kind WHERE pj_no='{$pj_no}' AND pj_k_no='{$project_expenses['kind']}' ORDER BY k_name";
        $pj_kind_query = mysqli_query($my_db, $pj_kind_sql);
        while($pj_kind = mysqli_fetch_assoc($pj_kind_query))
        {
            $kind_name = $pj_kind['k_name'];
        }
    }

    $cal_method_name    = isset($cal_method_option[$project_expenses['cal_method']]) ? $cal_method_option[$project_expenses['cal_method']] : "";
    $spend_method_name  = isset($spend_method_option[$project_expenses['spend_method']]) ? $spend_method_option[$project_expenses['spend_method']] : "";
    $bankbook_name      = isset($bankbook_option[$project_expenses['bankbook']]) ? $bankbook_option[$project_expenses['bankbook']] : "";
    $pj_ext_content     = $project_expenses['wd_no'] > 0 ? $project_expenses['wd_content'] : $project_expenses['pe_content'];
    $vat_type           = !empty($project_expenses['lec_vat_type']) ? $vat_type_option[$project_expenses['lec_vat_type']]['title'] : "";
    $c_name             = ($project_expenses['spend_method'] == "2") ? $project_expenses['pe_c_name'] : $project_expenses['c_name'];

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$idx}", $project_expenses['pj_name'])
        ->setCellValue("B{$idx}", $kind_name)
        ->setCellValue("C{$idx}", $c_name)
        ->setCellValue("D{$idx}", $project_expenses['supply_price'])
        ->setCellValue("E{$idx}", $project_expenses['tax_price'])
        ->setCellValue("F{$idx}", $project_expenses['total_price'])
        ->setCellValue("G{$idx}", $bankbook_name)
        ->setCellValue("H{$idx}", $project_expenses['bk_title'])
        ->setCellValue("I{$idx}", $project_expenses['bk_num'])
        ->setCellValue("J{$idx}", $project_expenses['bk_name'])
        ->setCellValue("K{$idx}", $project_expenses['acc_info'])
        ->setCellValue("L{$idx}", $vat_type)
        ->setCellValue("M{$idx}", $project_expenses['memo'])
        ->setCellValue("N{$idx}", $pj_ext_content)
        ->setCellValue("O{$idx}", $cal_method_name)
        ->setCellValue("P{$idx}", $spend_method_name)
        ->setCellValue("Q{$idx}", $project_expenses['req_name'])
        ->setCellValue("R{$idx}", $project_expenses['req_date'])
        ->setCellValue("S{$idx}", $project_expenses['appr_date'])
    ;
    $idx++;
}


$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00999999');
$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:S{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("A2:A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("B2:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("C2:C{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("D2:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("M2:M{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("Q2:Q{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("R2:R{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("S2:S{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle("사업비 지출관리");

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_사업비 지출관리.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

?>
