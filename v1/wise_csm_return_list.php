<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/wise_csm.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');
require('inc/model/WiseCsm.php');

# Navigation & My Quick
$nav_prd_no  = "31";
$nav_title   = "반품/교환 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색쿼리
$add_where          = "1=1";
$add_where_group    = "1=1";
$company_model      = Company::Factory();
$csm_model          = WiseCsm::Factory();
$kind_model         = Kind::Factory();
$product_model 	    = ProductCms::Factory();
$cms_code		    = "product_cms";

$prd_group_list         = $kind_model->getKindGroupList($cms_code);
$prd_total_list         = $product_model->getPrdGroupData();
$dp_company_option      = $company_model->getDpList();
$return_state_option    = $csm_model->getReturnStateOption();
$company_option         = $csm_model->getReturnCompanyOption();
$return_reason_option   = $csm_model->getReturnReasonOption();
$return_type_option     = getReturnTypeOption();

# 브랜드 검색
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    
    $add_where .= " AND `cr`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where .= " AND `cr`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where .= " AND `cr`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);


# 상품검색
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_get	= isset($_GET['sch'])?$_GET['sch']:"Y";

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);

if(!empty($sch_get)) {
    $smarty->assign("sch", $sch_get);
}

$prd_g1_list = $prd_g2_list = $prd_g3_list = [];

foreach($prd_group_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}

$prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
$sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);

if (!empty($sch_prd) && $sch_prd != "0") { // 상품
    $add_where .= " AND cr.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND cr.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND cr.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

# 날짜별 검색
$today_s_w		        = date('w')-1;
$sch_date_type          = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$sch_date_column_type   = isset($_GET['sch_date_column_type']) ? $_GET['sch_date_column_type'] : "return_date";
$sch_s_year             = isset($_GET['sch_s_year']) ? $_GET['sch_s_year'] : date('Y', strtotime("-5 years"));;
$sch_e_year             = isset($_GET['sch_e_year']) ? $_GET['sch_e_year'] : date('Y');
$sch_s_month            = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("-5 months"));
$sch_e_month            = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week             = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week             = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date             = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-8 day"));
$sch_e_date             = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');
$sch_not_empty          = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : "";

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_date_column_type', $sch_date_column_type);
$smarty->assign('sch_s_year', $sch_s_year);
$smarty->assign('sch_e_year', $sch_e_year);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);
$smarty->assign('sch_not_empty', $sch_not_empty);

$chk_s_date = "";
$chk_e_date = "";
if($sch_date_type == '4') //연간
{
    $chk_s_date  = !empty($sch_s_year) ? $sch_s_year."-01-01" : "";
    $chk_e_date  = !empty($sch_e_year) ? $sch_e_year."-12-31" : "";
}
elseif($sch_date_type == '3') //월간
{
    $chk_s_date  = !empty($sch_s_month) ? $sch_s_month."-01" : "";
    $chk_e_date  = !empty($sch_e_month) ? $sch_e_month."-31" : "";

}
elseif($sch_date_type == '2') //주간
{
    $chk_s_date  = !empty($sch_s_week) ? $sch_s_week : "";
    $chk_e_date  = !empty($sch_e_week) ? $sch_e_week : "";
}
else //일간
{
    $chk_s_date  = !empty($sch_s_date) ? $sch_s_date : "";
    $chk_e_date  = !empty($sch_e_date) ? $sch_e_date : "";
}

if(!empty($chk_s_date))
{
    $sch_s_datetime  = $chk_s_date." 00:00:00";
    $add_where      .= " AND `cr`.{$sch_date_column_type} >= '{$sch_s_datetime}'";
}

if(!empty($chk_e_date))
{
    $sch_e_datetime  = $chk_e_date." 23:59:59";
    $add_where      .= " AND `cr`.{$sch_date_column_type} <= '{$sch_e_datetime}'";
}

# 세부 검색
$sch_cr_no                  = isset($_GET['cr_no']) ? $_GET['cr_no'] : "";
$sch_return_type            = isset($_GET['sch_return_type']) ? $_GET['sch_return_type'] : "";
$sch_order_number           = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_ord_bundle 	        = isset($_GET['sch_ord_bundle']) ? $_GET['sch_ord_bundle'] : "1";
$sch_return_state           = isset($_GET['sch_return_state']) ? $_GET['sch_return_state'] : "";
$sch_shop_ord_no            = isset($_GET['sch_shop_ord_no']) ? $_GET['sch_shop_ord_no'] : "";
$order_number               = isset($_GET['order_number']) ? $_GET['order_number'] : "";
$sch_dp_c_no                = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_return_reason          = isset($_GET['sch_return_reason']) ? $_GET['sch_return_reason'] : "";
$sch_customer_memo          = isset($_GET['sch_customer_memo']) ? $_GET['sch_customer_memo'] : "";
$sch_customer_memo_ext      = isset($_GET['sch_customer_memo_ext']) ? $_GET['sch_customer_memo_ext'] : "";
$sch_return_delivery_type   = isset($_GET['sch_return_delivery_type']) ? $_GET['sch_return_delivery_type'] : "";
$sch_return_delivery_no     = isset($_GET['sch_return_delivery_no']) ? $_GET['sch_return_delivery_no'] : "";
$sch_file_no                = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";

if(!empty($sch_ord_bundle))
{
    $smarty->assign("sch_ord_bundle", $sch_ord_bundle);
}

if(!empty($sch_cr_no)){
    $add_where .= " AND `cr`.cr_no = '{$sch_cr_no}'";
    $smarty->assign('sch_cr_no', $sch_cr_no);
}

if(!empty($sch_return_type)){
    $add_where .= " AND `cr`.return_type = '{$sch_return_type}'";
    $smarty->assign('sch_return_type', $sch_return_type);
}

if(!empty($sch_order_number)){
    $add_where .= " AND `cr`.order_number = '{$sch_order_number}'";
    $smarty->assign('sch_order_number', $sch_order_number);
}

if(!empty($sch_return_state)){
    $add_where .= " AND `cr`.return_state = '{$sch_return_state}'";
    $smarty->assign('sch_return_state', $sch_return_state);
}

if(!empty($sch_shop_ord_no)){
    $add_where .= " AND `cr`.shop_ord_no = '{$sch_shop_ord_no}'";
    $smarty->assign('sch_shop_ord_no', $sch_shop_ord_no);
}

if($sch_dp_c_no != ''){
    $add_where .= " AND `cr`.dp_c_no = '{$sch_dp_c_no}'";
    $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
}

if(!empty($sch_return_reason)){
    $add_where .= " AND `cr`.return_reason = '{$sch_return_reason}'";
    $smarty->assign('sch_return_reason', $sch_return_reason);
}

if(!empty($sch_customer_memo_ext)){
    $add_where .= " AND `cr`.customer_memo != ''";
    $smarty->assign('sch_customer_memo_ext', $sch_customer_memo_ext);
}else{
    if(!empty($sch_customer_memo))
    {
        $add_where .= " AND `cr`.customer_memo like '%{$sch_customer_memo}%'";
        $smarty->assign('sch_customer_memo', $sch_customer_memo);
    }
}

if(!empty($sch_return_delivery_type)){
    $add_where .= " AND `cr`.return_delivery_type = '{$sch_return_delivery_type}'";
    $smarty->assign('sch_return_delivery_type', $sch_return_delivery_type);
}

if(!empty($sch_return_delivery_no)){
    $add_where .= " AND `cr`.return_delivery_no = '{$sch_return_delivery_no}'";
    $smarty->assign('sch_return_delivery_no', $sch_return_delivery_no);
}

if(!empty($sch_file_no)){
    $add_where .= " AND `cr`.file_no = '{$sch_file_no}'";
    $smarty->assign('sch_file_no', $sch_file_no);
}

if($sch_ord_bundle == '1') {
    $csm_return_total_sql = "SELECT count(*) FROM (SELECT DISTINCT cr.order_number FROM csm_return cr WHERE {$add_where}) AS cnt";
}else{
    $csm_return_total_sql = "SELECT count(*) FROM (SELECT cr.cr_no FROM csm_return cr WHERE {$add_where}) AS cnt";
}

// 전체 게시물 수
$csm_return_total_query	= mysqli_query($my_db, $csm_return_total_sql);
if(!!$csm_return_total_query)
    $csm_return_total_result = mysqli_fetch_array($csm_return_total_query);

$csm_return_total = $csm_return_total_result[0];

//페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($csm_return_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "wise_csm_return_list.php", $pagenum, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $csm_return_total);
$smarty->assign("pagelist", $page);
$smarty->assign("ord_page_type", $page_type);


if($sch_ord_bundle == '1')
{
    $group_cr_ord_no_sql     = "SELECT DISTINCT order_number FROM csm_return as cr WHERE {$add_where} ORDER BY cr.cr_no DESC LIMIT {$offset},{$num}";
    $group_cr_ord_no_query   = mysqli_query($my_db, $group_cr_ord_no_sql);
    $group_cr_ord_no_list    = [];
    while($group_cr_ord_no = mysqli_fetch_assoc($group_cr_ord_no_query))
    {
        $group_cr_ord_no_list[] = "'{$group_cr_ord_no['order_number']}'";
    }

    $order_numbers     = implode(',', $group_cr_ord_no_list);
    if($order_numbers){
        $add_where_group .= " AND cr.order_number IN({$order_numbers})";
    }else{
        $add_where_group .= " AND cr.order_number IN('')";
    }

    // 리스트 쿼리
    $csm_return_sql = "
        SELECT
            *,
            cr.c_name as brand_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=(SELECT `c`.brand FROM company `c` WHERE c.c_no=cr.c_no))) AS brand_g1_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT `c`.brand FROM company `c` WHERE c.c_no=cr.c_no)) AS brand_g2_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=(SELECT prd_cms.k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=cr.prd_no))) AS k_prd1_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT prd_cms.k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=cr.prd_no)) AS k_prd2_name,
            (SELECT prd_cms.title from product_cms prd_cms where prd_cms.prd_no=cr.prd_no) as prd_name,
            DATE_FORMAT(cr.regdate, '%Y-%m-%d') as reg_day,
            DATE_FORMAT(cr.regdate, '%H:%i') as reg_time,
            DATE_FORMAT(cr.order_date, '%Y-%m-%d') as ord_day,
            DATE_FORMAT(cr.order_date, '%H:%i') as ord_time,
            DATE_FORMAT(cr.return_date, '%Y-%m-%d') as return_day,
            DATE_FORMAT(cr.return_date, '%H:%i') as return_time
        FROM csm_return cr
        WHERE {$add_where_group}
        ORDER BY cr.cr_no DESC
    ";
}else{
    // 리스트 쿼리
    $csm_return_sql = "
        SELECT
            *,
            cr.c_name as brand_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=(SELECT `c`.brand FROM company `c` WHERE c.c_no=cr.c_no))) AS brand_g1_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT `c`.brand FROM company `c` WHERE c.c_no=cr.c_no)) AS brand_g2_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=(SELECT prd_cms.k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=cr.prd_no))) AS k_prd1_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT prd_cms.k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=cr.prd_no)) AS k_prd2_name,
            (SELECT prd_cms.title from product_cms prd_cms where prd_cms.prd_no=cr.prd_no) as prd_name,
            DATE_FORMAT(cr.regdate, '%Y-%m-%d') as reg_day,
            DATE_FORMAT(cr.regdate, '%H:%i') as reg_time,
            DATE_FORMAT(cr.order_date, '%Y-%m-%d') as ord_day,
            DATE_FORMAT(cr.order_date, '%H:%i') as ord_time,
            DATE_FORMAT(cr.return_date, '%Y-%m-%d') as return_day,
            DATE_FORMAT(cr.return_date, '%H:%i') as return_time
        FROM csm_return cr
        WHERE {$add_where}
        ORDER BY cr.cr_no DESC
        LIMIT {$offset},{$num}
    ";
}
$csm_return_query = mysqli_query($my_db, $csm_return_sql);
$csm_return_list  = [];
while($csm_return = mysqli_fetch_assoc($csm_return_query))
{
    if(!empty($csm_return['ord_day']) && $csm_return['ord_day'] != '0000-00-00'){
        $csm_return['ord_reg_s_day'] = date('Y-m-d', strtotime("{$csm_return['ord_day']} -7 days"));
        $csm_return['ord_reg_e_day'] = date('Y-m-d', strtotime("{$csm_return['ord_day']} +7 days"));
    }else{
        $csm_return['ord_reg_s_day'] = '';
        $csm_return['ord_reg_e_day'] = '';
    }

    if(!empty($csm_return['return_delivery_no']) && !empty($csm_return['return_delivery_type']))
    {
        $return_delivery_url = ($csm_return['return_delivery_type'] == 'CJ대한통운') ? "https://trace.cjlogistics.com/next/tracking.html?wblNo=" : "https://www.hanjin.co.kr/kor/CMS/DeliveryMgr/WaybillResult.do?mCode=MN038&schLang=KR&wblnumText2=";
        $csm_return['return_delivery_url'] = $return_delivery_url.$csm_return['return_delivery_no'];
    }

    $csm_return['return_type_name'] = isset($return_type_option[$csm_return['return_type']]) ? $return_type_option[$csm_return['return_type']] : "";
    $csm_return['dp_c_name']        = isset($dp_company_option[$csm_return['dp_c_no']]) ? $dp_company_option[$csm_return['dp_c_no']] : "";
    $csm_return_list[$csm_return['order_number']][] = $csm_return;
}

$dp_company_option[0] = "구매처(확인불가)";

$smarty->assign("date_type_option", getCsmDateTypeOption());
$smarty->assign("return_type_option", $return_type_option);
$smarty->assign("return_state_option", $return_state_option);
$smarty->assign("company_option", $company_option);
$smarty->assign("dp_company_option", $dp_company_option);
$smarty->assign("return_reason_option", $return_reason_option);
$smarty->assign("delivery_type_option", getDeliveryTypeOption());
$smarty->assign("page_type_option", getPageTypeOption('3'));
$smarty->assign("csm_return_list", $csm_return_list);

$smarty->display('wise_csm_return_list.html');
?>
