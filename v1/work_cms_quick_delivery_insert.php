<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

include_once('inc/phpExcelReader/reader.php');
include_once('inc/common.php');
require('ckadmin.php');


$data = new Spreadsheet_Excel_Reader();
// Set output Encoding.
$data->setOutputEncoding('UTF-8');

$data->read($_FILES["delivery_file"]["tmp_name"]);
error_reporting(E_ALL ^ E_NOTICE);

$total_time = 0;

$sent_date   = (isset($_POST['step_02_date'])) ? $_POST['step_02_date'] : "";
$search_url  = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

$upd_sql_list            = [];
$ins_deli_list           = [];
$delivery_chk_list       = [];
$delivery_excel_chk_list = [];

for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++)
{
    $order_number  = (string)trim(addslashes($data->sheets[0]['cells'][$i][1]));   //주문번호
    $delivery_type = (string)trim(addslashes($data->sheets[0]['cells'][$i][9]));   //운송장타입
    $delivery_no   = (string)trim(addslashes($data->sheets[0]['cells'][$i][10]));  //운송장번호

    if(!$order_number)
    {
        echo "ROW : {$i}<br/>";
        echo "운송장번호 반영에 실패하였습니다.<br/>주문번호가 없습니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>";
        exit;
    }

    if(!$delivery_no){
        echo "ROW : {$i}<br/>";
        echo "운송장번호 반영에 실패하였습니다.<br>운송장번호가 없습니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>";
        exit;
    }

    $ord_sql    = "SELECT count(w_no) as cnt FROM work_cms_quick WHERE order_number ='{$order_number}'";
    $ord_query  = mysqli_query($my_db, $ord_sql);
    $ord_result = mysqli_fetch_assoc($ord_query);

    if(!$ord_result || (isset($ord_result['cnt']) && $ord_result['cnt'] == '0'))
    {
        echo "ROW : {$i}<br/>";
        echo "운송장번호 반영에 실패하였습니다.<br>매칭되는 주문번호가 없습니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>";
        exit;
    }

    # 요기부터 변경
    $deli_sql    = "SELECT count(`no`) as cnt FROM work_cms_quick_delivery WHERE order_number !='{$order_number}' AND delivery_no = '{$delivery_no}'";
    $deli_query  = mysqli_query($my_db, $deli_sql);
    $deli_result = mysqli_fetch_assoc($deli_query);

    if(isset($deli_result['cnt']) && $deli_result['cnt'] > 0)
    {
        echo "ROW : {$i}<br/>";
        echo "운송장번호 반영에 실패하였습니다.<br>운송장번호가 이미 존재합니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>";
        exit;
    }


    $chk_deli_sql    = "SELECT COUNT(`no`) as cnt FROM work_cms_quick_delivery WHERE order_number='{$order_number}' AND delivery_no='{$delivery_no}'";
    $chk_deli_query  = mysqli_query($my_db, $chk_deli_sql);
    $chk_deli_result = mysqli_fetch_assoc($chk_deli_query);

    if($chk_deli_result['cnt'] > 0){
        continue;
    }

    if(!isset($delivery_excel_chk_list[$delivery_no])){
        $delivery_excel_chk_list[$delivery_no] = $order_number;
    }

    if($delivery_excel_chk_list[$delivery_no] != $order_number)
    {
        echo "ROW : {$i}<br/>";
        echo "운송장번호 반영에 실패하였습니다.<br>엑셀내 동일한 운송장번호가 다른 주문번호에 있습니다.<br>
            주문번호    : {$order_number}<br>
            운송장번호  : {$delivery_no}<br>";
        exit;
    }

    $ins_deli_sql   = "INSERT INTO work_cms_quick_delivery SET order_number='{$order_number}', delivery_type='{$delivery_type}', delivery_no='{$delivery_no}'";

    if (!mysqli_query($my_db, $ins_deli_sql)){
        echo "운송장번호 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
        echo "{$i}번째";
        echo "SQL : ".$ins_deli_sql."<br/>";
    }
}

if($data->sheets[0]['numRows'] > 2) {
    exit("<script>alert('송장번호가 반영 되었습니다.');location.href='work_cms_quick_list.php?{$search_url}';</script>");
}else{
    exit("<script>alert('엑셀파일 데이터가 없습니다');location.href='work_cms_quick_list.php?{$search_url}';</script>");
}



?>
