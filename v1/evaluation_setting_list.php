<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/evaluation.php');
require('inc/model/MyQuick.php');
require('inc/model/Staff.php');
require('inc/model/Team.php');
require('inc/model/Evaluation.php');

$ev_model 			= Evaluation::Factory();
$ev_system_model 	= Evaluation::Factory();
$staff_model		= Staff::Factory();
$team_model			= Team::Factory();
$ev_model->setMainInit("evaluation_relation", "ev_r_no");

$is_super_admin = false;
if($session_s_no == '1' || $session_s_no == '28' || $session_s_no == '204'){
	$is_super_admin 	= true;
}
$smarty->assign("is_super_admin", $is_super_admin);


#프로세스 처리
$process 	= (isset($_POST['process'])) ? $_POST['process'] : "";

if($process == "new_receiver")
{
	$ev_no      	= isset($_POST['chk_ev_no']) ? $_POST['chk_ev_no'] : "";
	$manager    	= isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
	$search_url		= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$ev_system 		= $ev_system_model->getItem($ev_no);
	$ins_data   	= array("ev_no" => $ev_no, "manager" => $manager, "ev_u_set_no" => $ev_system['ev_u_set_no'], "rec_is_review" => 1, "eval_is_review" => 1, "reg_s_no" => $session_s_no);

	if($is_super_admin || (($ev_system['ev_state'] == '1' || $ev_system['ev_state'] == '5') && ($ev_system['admin_s_no'] == $session_s_no)))
	{
		if (!$ev_model->insert($ins_data))
			exit("<script>alert('추가에 실패했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
		else
			exit("<script>location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}else{
		exit("<script>alert('설정 관리자 추가할 수 없습니다.');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
}
elseif($process == "new_evaluator")
{
	$ev_no      	= isset($_POST['chk_ev_no']) ? $_POST['chk_ev_no'] : "";
	$manager    	= isset($_POST['chk_manager']) ? $_POST['chk_manager'] : "";
	$receiver    	= isset($_POST['chk_value']) ? $_POST['chk_value'] : "";
	$search_url		= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$ev_system 		= $ev_system_model->getItem($ev_no);
	$receiver_staff	= $staff_model->getItem($receiver);
	$ins_data   	= array("ev_no" => $ev_no, "manager" => $manager, "receiver_s_no" => $receiver, "receiver_team" => $receiver_staff['team'], "ev_u_set_no" => $ev_system['ev_u_set_no']);

	if($ev_system['ev_u_set_no'] != '9'){
		$ins_data["rec_is_review"]  = 1;
		$ins_data["eval_is_review"] = 1;
	}

	if($is_super_admin || (($ev_system['ev_state'] == '1' || $ev_system['ev_state'] == '5') && ($ev_system['admin_s_no'] == $session_s_no || $manager == $session_s_no)))
	{
		if (!$ev_model->insert($ins_data))
			exit("<script>alert('추가에 실패했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
		else
			exit("<script>location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}else{
		exit("<script>alert('평가자를 추가할 수 없습니다.');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
}
elseif ($process == "change_manager")
{
	$ev_no			= (isset($_POST['chk_ev_no'])) ? $_POST['chk_ev_no'] : "";
	$prev_manager	= (isset($_POST['chk_prev_value'])) ? $_POST['chk_prev_value'] : "";
	$manager		= (isset($_POST['chk_value'])) ? $_POST['chk_value'] : "";
	$search_url		= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$ev_system 		= $ev_system_model->getItem($ev_no);

	if($is_super_admin || (($ev_system['ev_state'] == '1' || $ev_system['ev_state'] == '5') && ($ev_system['admin_s_no'] == $session_s_no)))
	{
		if (!$ev_model->updateToArray(array("manager" => $manager), array("ev_no" => $ev_no, "manager" => $prev_manager)))
			exit("<script>alert('평가자 설정 관리자 변경에 실패했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
		else
			exit("<script>alert('평가자 설정 관리자를 변경했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}else{
		exit("<script>alert('설정 관리자를 변경할 수 없습니다.');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
}
elseif ($process == "change_receiver")
{
	$ev_no			= (isset($_POST['chk_ev_no'])) ? $_POST['chk_ev_no'] : "";
	$chk_ev_r_no	= (isset($_POST['chk_ev_r_no'])) ? $_POST['chk_ev_r_no'] : "";
	$chk_manager	= (isset($_POST['chk_manager'])) ? $_POST['chk_manager'] : "";
	$prev_receiver	= (isset($_POST['chk_prev_value'])) ? $_POST['chk_prev_value'] : "";
	$new_receiver	= (isset($_POST['chk_value'])) ? $_POST['chk_value'] : "";
	$search_url		= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$receiver_staff	= $staff_model->getStaff($new_receiver);
	$ev_system 		= $ev_system_model->getItem($ev_no);

	if($is_super_admin || (($ev_system['ev_state'] == '1' || $ev_system['ev_state'] == '5') && ($ev_system['admin_s_no'] == $session_s_no || $chk_manager == $session_s_no)))
	{
		if (!$ev_model->updateReceiver($ev_no, $chk_ev_r_no, $prev_receiver, $new_receiver, $receiver_staff['team']))
			exit("<script>alert('평가받는 사람 변경에 실패했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
		else
			exit("<script>location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
	else
	{
		exit("<script>alert('평가받는 사람을 변경할 수 없습니다.');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
}
elseif ($process == "change_evaluator")
{
	$ev_no			= (isset($_POST['chk_ev_no'])) ? $_POST['chk_ev_no'] : "";
	$chk_ev_r_no	= (isset($_POST['chk_ev_r_no'])) ? $_POST['chk_ev_r_no'] : "";
	$chk_manager	= (isset($_POST['chk_manager'])) ? $_POST['chk_manager'] : "";
	$new_evaluator	= (isset($_POST['chk_value'])) ? $_POST['chk_value'] : "";
	$search_url		= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$ev_system		= $ev_system_model->getItem($ev_no);
	$evaluator		= $staff_model->getItem($new_evaluator);

	if($is_super_admin || (($ev_system['ev_state'] == '1' || $ev_system['ev_state'] == '5') && ($ev_system['admin_s_no'] == $session_s_no || $chk_manager == $session_s_no)))
	{
		if($new_evaluator != '1' && $ev_system['ev_u_set_no'] == '9')
		{
			if(!$ev_model->checkEvaluator($ev_no, $new_evaluator)){
				exit("<script>alert('{$evaluator['s_name']} 님은 평가대상이 아닙니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
			}
		}

		if (!$ev_model->updateEvaluator($ev_no, $chk_ev_r_no, $new_evaluator, $session_s_no))
			exit("<script>alert('평가자 변경에 실패했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
		else
			exit("<script>location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
	else
	{
		exit("<script>alert('평가받는 사람을 변경할 수 없습니다.');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
}
elseif ($process == "change_group_rate")
{
	$ev_no			= (isset($_POST['chk_ev_no'])) ? $_POST['chk_ev_no'] : "";
	$ev_r_no		= (isset($_POST['chk_ev_r_no'])) ? $_POST['chk_ev_r_no'] : "";
	$chk_receiver	= (isset($_POST['chk_manager'])) ? $_POST['chk_manager'] : "";
	$new_group_rate	= (isset($_POST['chk_value'])) ? $_POST['chk_value'] : "";
	$search_url		= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$ev_system 		= $ev_system_model->getItem($ev_no);

	if($ev_system['ev_state'] == '7' && ($is_super_admin || $ev_system['admin_s_no'] == $session_s_no))
	{
		if (!$ev_model->updateGroupRate($ev_no, $ev_r_no, $chk_receiver, $new_group_rate))
			exit("<script>alert('그룹 평가비율 변경에 실패했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
		else
			exit("<script>alert('그룹 평가비율을 변경했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
	else
	{
		exit("<script>alert('평가받는 사람을 변경할 수 없습니다.');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
}
elseif ($process == "add_prev_evaluator")
{
	$prev_ev_no 	= (isset($_POST['chk_prev_value'])) ? $_POST['chk_prev_value'] : "";
	$ev_no 			= (isset($_POST['chk_value'])) ? $_POST['chk_value'] : "";
	$search_url		= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$ev_system 		= $ev_system_model->getItem($ev_no);

	if(($ev_system['ev_state'] == '1' || $ev_system['ev_state'] == '5') && ($is_super_admin || $ev_system['admin_s_no'] == $session_s_no))
	{
		if (!$ev_model->addPrevEvaluationRelation($ev_no, $prev_ev_no))
			exit("<script>alert('지난 평가자 불러오기에 실패했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
		else
			exit("<script>alert('지난 평가자를 추가했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
	else
	{
		exit("<script>alert('지난 평가자 불러오기를 사용할 수 없습니다.');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}

}
elseif ($process == "del_ev_relation")
{
	$chk_ev_r_no_list 	= isset($_POST['chk_ev_r_no_list']) ? explode(",",$_POST['chk_ev_r_no_list']) : "";
	$search_url     	= isset($_POST['search_url']) ? $_POST['search_url'] : "";
	$del_list			= [];

	foreach($chk_ev_r_no_list as $ev_r_no)
	{
		$ev_sql 	= "SELECT ev_r_no, manager, (SELECT es.admin_s_no FROM evaluation_system es WHERE es.ev_no=er.ev_no) AS admin_s_no, (SELECT es.ev_state FROM evaluation_system es WHERE es.ev_no=er.ev_no) AS ev_state FROM evaluation_relation er WHERE ev_r_no='{$ev_r_no}'";
		$ev_query 	= mysqli_query($my_db, $ev_sql);
		$ev_result	= mysqli_fetch_assoc($ev_query);

		if(isset($ev_result['ev_r_no'])){
			if($is_super_admin || (($ev_result['ev_state'] == '1' || $ev_result['ev_state'] == '5') && ($ev_result['admin_s_no'] == $session_s_no || $ev_result['manager'] == $session_s_no))){
				$del_list[] = $ev_result['ev_r_no'];
			}
		}
	}

	if(!empty($del_list))
	{
		if($ev_model->deleteEvaluationRelation($del_list))
			exit("<script>alert('삭제했습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
		else
			exit("<script>alert('삭제에 실패 하였습니다');location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}else{
		exit("<script>alert('삭제할 데이터가 없습니다다';location.href='evaluation_setting_list.php?{$search_url}';</script>");
	}
}
elseif ($process == "f_rec_is_review")
{
	$ev_r_no 	= isset($_POST['ev_r_no']) ? $_POST['ev_r_no'] : "";
	$val 	 	= isset($_POST['val']) ? $_POST['val'] : "";
	$ev_item 	= $ev_model->getItem($ev_r_no);
	$upd_data	= [];

	$chk_sql 	= "SELECT ev_r_no FROM evaluation_relation WHERE ev_no='{$ev_item['ev_no']}' AND receiver_s_no='{$ev_item['evaluator_s_no']}' AND evaluator_s_no='{$ev_item['receiver_s_no']}'";
	$chk_query	= mysqli_query($my_db, $chk_sql);
	$chk_result = mysqli_fetch_assoc($chk_query);

	if(isset($chk_result['ev_r_no'])){
		$upd_data[] = array("ev_r_no" => $ev_r_no, "rec_is_review" => $val);
		$upd_data[] = array("ev_r_no" => $chk_result['ev_r_no'], "eval_is_review" => $val);
	}else{
		$upd_data[] = array("ev_r_no" => $ev_r_no, "rec_is_review" => $val, "eval_is_review" => $val);
	}

	if($ev_model->multiUpdate($upd_data))
		echo "변경되었습니다.";
	else
		echo "변경에 실패했습니다.";
	exit;
}
elseif ($process == "f_rec_review")
{
	$ev_r_no 	= isset($_POST['ev_r_no']) ? $_POST['ev_r_no'] : "";
	$val 	 	= isset($_POST['val']) ? addslashes(trim($_POST['val'])) : "";
	$ev_item 	= $ev_model->getItem($ev_r_no);
	$upd_data	= [];

	$chk_sql 	= "SELECT ev_r_no FROM evaluation_relation WHERE ev_no='{$ev_item['ev_no']}' AND receiver_s_no='{$ev_item['evaluator_s_no']}' AND evaluator_s_no='{$ev_item['receiver_s_no']}'";
	$chk_query	= mysqli_query($my_db, $chk_sql);
	$chk_result = mysqli_fetch_assoc($chk_query);

	$upd_data[] = array("ev_r_no" => $ev_r_no, "rec_review" => $val);
	if(isset($chk_result['ev_r_no'])){
		$upd_data[] = array("ev_r_no" => $chk_result['ev_r_no'], "eval_review" => $val);
	}

	if($ev_model->multiUpdate($upd_data))
		echo "변경되었습니다.";
	else
		echo "변경에 실패했습니다.";
	exit;
}
elseif ($process == "f_rate")
{
	$ev_r_no 	= isset($_POST['ev_r_no']) ? $_POST['ev_r_no'] : "";
	$val 	 	= isset($_POST['val']) ? addslashes(trim($_POST['val'])) : "";
	$ev_item 	= $ev_model->getItem($ev_r_no);
	$ev_sum 	= $ev_model->getEvSumRate($ev_item['ev_no'], $ev_item['receiver_s_no']);
	$ev_sum_val = $ev_sum-$ev_item['rate'];

	if($ev_model->update(array("ev_r_no" => $ev_r_no, "rate" => $val))){
		echo json_encode(array("result" => true, "result_msg" => "변경되었습니다.", "sum_rate" => $ev_sum_val+$val));
	}else{
		echo json_encode(array("result" => true, "result_msg" => "변경에 실패했습니다.", "sum_rate" => $ev_sum));
	}
	exit;
}
else
{
	# Navigation & My Quick
	$nav_prd_no  = "171";
	$nav_title   = "평가자 설정 리스트";
	$quick_model = MyQuick::Factory();
	$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_title", $nav_title);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	# 검색조건
	$add_where          	= "1=1";
	$sch_ev_no              = isset($_GET['sch_ev_no']) ? $_GET['sch_ev_no'] : "";
	$sch_ev_state          	= isset($_GET['sch_ev_state']) ? $_GET['sch_ev_state'] : "";
	$sch_manager        	= isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
	$sch_receiver_team  	= isset($_GET['sch_receiver_team']) ? $_GET['sch_receiver_team'] : "";
	$sch_receiver_staff 	= isset($_GET['sch_receiver_staff']) ? $_GET['sch_receiver_staff'] : "";
	$sch_relation_state 	= isset($_GET['sch_relation_state']) ? $_GET['sch_relation_state'] : "";
	$sch_quick_search 		= isset($_GET['sch_quick_search']) ? $_GET['sch_quick_search'] : "";
	$ev_state_option		= getEvStateColorOption();
	$evaluation_system_list = $ev_model->getEvaluationSystemListByStaff($session_s_no);
	$team_full_name_list    = $team_model->getTeamFullNameList();
	$team_all_list          = $team_model->getTeamAllList();
	$all_staff_list    		= $team_all_list['staff_list'];
	$all_staff_team_list    = $team_all_list['staff_team_list'];
	$rec_staff_list			= $all_staff_list['all'];
	$eval_staff_list    	= $all_staff_team_list['all'];
	$rec_manager_list		= [];
	$staff_manager_list     = [];
	$add_rec_staff_list		= [];
	$sch_staff_list         = [];

	$search_url = getenv("QUERY_STRING");
	if(empty($search_url))
	{
		if($is_super_admin){
			$pro_system_sql 	= "SELECT ev_no FROM evaluation_system ORDER BY ev_no DESC LIMIT 1";
			$pro_system_query 	= mysqli_query($my_db, $pro_system_sql);
			$pro_system_result 	= mysqli_fetch_assoc($pro_system_query);
		}else{
			$pro_system_sql 	= "SELECT ev_no FROM evaluation_system WHERE ev_state IN('5','6') ORDER BY ev_no DESC LIMIT 1";
			$pro_system_query 	= mysqli_query($my_db, $pro_system_sql);
			$pro_system_result 	= mysqli_fetch_assoc($pro_system_query);
		}


		if(isset($pro_system_result['ev_no']))
		{
			if($session_s_no != '1' && $session_s_no != '28' && $session_s_no != '204')
			{
				$chk_pro_sql 	= "SELECT COUNT(ev_r_no) as cnt FROM evaluation_relation WHERE ev_no='{$pro_system_result['ev_no']}' AND manager='{$session_s_no}'";
				$chk_pro_query 	= mysqli_query($my_db, $chk_pro_sql);
				$chk_pro_result = mysqli_fetch_assoc($chk_pro_query);

				if($chk_pro_result['cnt'] > 0){
					$sch_ev_no  = $pro_system_result['ev_no'];
					$search_url = "sch_ev_no={$sch_ev_no}";
				}
			}
			else {
				$sch_ev_no  = $pro_system_result['ev_no'];
				$search_url = "sch_ev_no={$sch_ev_no}";
			}
		}
	}
	$smarty->assign("search_url", $search_url);

	if(!empty($sch_ev_no)) {
		$add_where .= " AND es.ev_no='{$sch_ev_no}'";
		$smarty->assign("sch_ev_no", $sch_ev_no);
	}

	$ev_relation_manager_list  	= [];
	$ev_relation_rec_list 		= [];
	$ev_relation_eval_list 		= [];
	$evaluation_system			= [];
	$ev_rec_prev_list			= [];
	$ev_rec_rate_sum_list		= [];
	$ev_date_notice				= "";
	$chk_init_editable			= false;
	$is_admin_editable 			= false;
	$is_manager_editable 		= false;
	$is_review_step 			= false;
	$is_review_editable 		= false;
	$is_multi_delete			= false;

	if(!empty($sch_ev_no))
	{
		$evaluation_system_sql 		= "SELECT * FROM evaluation_system WHERE ev_no='{$sch_ev_no}'";
		$evaluation_system_query 	= mysqli_query($my_db, $evaluation_system_sql);
		$evaluation_system 			= mysqli_fetch_assoc($evaluation_system_query);
		if(empty($evaluation_system)) {
			exit("<script type='text/javascript'>alert('없는 평가지 번호입니다.');location.href='evaluation_setting_list.php';</script>");
		}

		$sch_ev_state			= $evaluation_system['ev_state'];
		$ev_system_manager_list = $ev_model->getEvManagerList($sch_ev_no);

		# 평가지별 권한 설정
		if(isset($ev_system_manager_list[$session_s_no])){
			$is_manager_editable = true;
			$staff_manager_list  = array($session_s_no => $ev_system_manager_list[$session_s_no]);
		}

		if($evaluation_system['admin_s_no'] == $session_s_no){
			$is_admin_editable 	= true;
			$staff_manager_list = $ev_system_manager_list;
		}

		if($is_super_admin){
			if(empty($ev_system_manager_list)){
				$staff_manager_list = $staff_model->getManagerList();
			}
		}elseif($is_admin_editable){

		}elseif($is_manager_editable){
			if($sch_ev_state != '5' && $sch_ev_state != 6){
				exit("<script type='text/javascript'>alert('아직 관리자가 이용할 수 없는 평가지입니다.');location.href='evaluation_setting_list.php';</script>");
			}
			$sch_manager = $session_s_no;
		}else{
			if($sch_ev_state != 6){
				exit("<script type='text/javascript'>alert('아직 평가자가 이용 수 없는 평가지입니다.');location.href='evaluation_setting_list.php';</script>");
			}

			$sch_relation_sql 		= "SELECT manager, receiver_team, receiver_s_no FROM evaluation_relation WHERE ev_no='{$sch_ev_no}' AND receiver_s_no='{$session_s_no}' GROUP BY receiver_s_no";
			$sch_relation_query 	= mysqli_query($my_db, $sch_relation_sql);
			$sch_relation_result 	= mysqli_fetch_assoc($sch_relation_query);
			if(empty($sch_relation_result)){
				exit("<script type='text/javascript'>alert('평가 대상자가 아닙니다.');location.href='evaluation_setting_list.php';</script>");
			}
			$staff_manager_list = array($sch_relation_result['manager'] => $ev_system_manager_list[$sch_relation_result['manager']]);
			$sch_manager 		= $sch_relation_result['manager'];
			$sch_receiver_team 	= $sch_relation_result['receiver_team'];
			$sch_receiver_staff = $sch_relation_result['receiver_s_no'];
		}

		if($sch_ev_state == '6'){
			$is_review_editable = true;
		}

		if($sch_ev_state != '1' && $sch_ev_state != '5'){
			$is_review_step = true;
		}

		if($sch_ev_state == '5' && $evaluation_system['set_s_date'] && $evaluation_system['set_e_date']) {
			$cur_date 		= date('Y-m-d');
			$set_e_date 	= new DateTime($evaluation_system['set_e_date']);
			$cur_datetime 	= new DateTime($cur_date);
			$left_day 		= $set_e_date->diff($cur_datetime);

			if ($evaluation_system['set_e_date'] >= $cur_date && $left_day->days >= 0) {
				$ev_date_notice = "평가지 설정 기간 {$evaluation_system['set_s_date']} ~ {$evaluation_system['set_e_date']}까지 (D-{$left_day->days})";
			}elseif($left_day->days >= 0){
				$ev_date_notice = "평가지 설정 기간 {$evaluation_system['set_s_date']} ~ {$evaluation_system['set_e_date']}까지 (D+{$left_day->days})";
			}
		}

		$rec_manager_list	= $ev_system_model->getRecManagerList($evaluation_system['ev_no']);
		$add_rec_staff_list = $ev_system_model->getRecStaffList($evaluation_system['ev_no']);

		$chk_init_sql  		= "SELECT COUNT(ev_r_no) AS cnt FROM evaluation_relation WHERE ev_no='{$sch_ev_no}' AND receiver_s_no != evaluator_s_no";
		$chk_init_query		= mysqli_query($my_db, $chk_init_sql);
		$chk_init_result 	= mysqli_fetch_assoc($chk_init_query);

		if($chk_init_result['cnt'] == 0){
			$chk_init_editable = true;
		}

		if(!empty($sch_ev_state)) {
			$add_where .= " AND es.ev_state='{$sch_ev_state}'";
			$smarty->assign("sch_ev_state", $sch_ev_state);
		}

		if(!empty($sch_quick_search))
		{
			if($sch_quick_search == '1')
			{
				$chk_quick_sql 		= "SELECT receiver_s_no, COUNT(ev_r_no) as rec_cnt FROM evaluation_relation WHERE ev_no='{$sch_ev_no}' GROUP BY receiver_s_no HAVING rec_cnt < 2";
				$chk_quick_query 	= mysqli_query($my_db, $chk_quick_sql);
				$chk_quick_list  	= [];
				while($chk_quick = mysqli_fetch_assoc($chk_quick_query)){
					$chk_quick_list[] = $chk_quick['receiver_s_no'];
				}

				if(!empty($chk_quick_list)){
					$chk_quick_text = implode(",", $chk_quick_list);
					$add_where .= " AND er.receiver_s_no IN({$chk_quick_text})";
				}else{
					$add_where .= " AND er.receiver_s_no='0'";
				}
			}
			elseif($sch_quick_search == '2')
			{
				$chk_quick_sql 		= "SELECT ev_r_no FROM evaluation_relation WHERE ev_no='{$sch_ev_no}' AND evaluator_s_no = 0";
				$chk_quick_query 	= mysqli_query($my_db, $chk_quick_sql);
				$chk_quick_list  	= [];
				while($chk_quick = mysqli_fetch_assoc($chk_quick_query)){
					$chk_quick_list[] = $chk_quick['ev_r_no'];
				}

				if(!empty($chk_quick_list)){
					$chk_quick_text = implode(",", $chk_quick_list);
					$add_where .= " AND er.ev_r_no IN({$chk_quick_text})";
				}else{
					$add_where .= " AND er.evaluator_s_no='0'";
				}
			}

			$smarty->assign("sch_quick_search", $sch_quick_search);
		}

		if(!empty($sch_manager)) {
			$add_where .= " AND er.manager='{$sch_manager}'";
			$smarty->assign("sch_manager", $sch_manager);
		}

		if(!empty($sch_receiver_team))
		{
			if($sch_receiver_team != 'all')
			{
				$sch_staff_list = $all_staff_list[$sch_receiver_team];
				$sch_team_code_where = getTeamWhere($my_db, $sch_receiver_team);
				if($sch_team_code_where){
					$add_where .= " AND er.receiver_team IN ({$sch_team_code_where})";
				}
			}
			$smarty->assign("sch_receiver_team", $sch_receiver_team);
		}

		if(!empty($sch_receiver_staff)) {
			$add_where .= " AND er.receiver_s_no='{$sch_receiver_staff}'";
			$smarty->assign("sch_receiver_staff", $sch_receiver_staff);
		}

		if(!empty($sch_relation_state))
		{
			switch($sch_relation_state)
			{
				case '1':
					$add_where .= " AND (er.rec_is_review='1' AND er.eval_is_review='1') AND (er.receiver_s_no!=er.evaluator_s_no)";
					break;
				case '2':
					$add_where .= " AND (er.rec_is_review='2' AND er.eval_is_review='2')";
					break;
				case '3':
					$add_where .= " AND ((er.rec_is_review!='0' AND er.eval_is_review!='0') AND er.rec_is_review!=er.eval_is_review)";
					break;
				case '4':
					$add_where .= " AND (er.rec_is_review='0' OR er.eval_is_review='0')";
					break;
				case '5':
					$add_where .= " AND er.receiver_s_no=er.evaluator_s_no";
					break;
			}

			$smarty->assign("sch_relation_state", $sch_relation_state);
		}

		# 평가자수 체크
		$ev_relation_eval_sql 		= "SELECT receiver_s_no, COUNT(evaluator_s_no) as cnt FROM evaluation_relation as er WHERE er.ev_no='{$sch_ev_no}' GROUP BY receiver_s_no";
		$ev_relation_eval_query		= mysqli_query($my_db, $ev_relation_eval_sql);
		$ev_relation_eval_cnt_list	= [];
		while($ev_relation_eval_result 	= mysqli_fetch_assoc($ev_relation_eval_query))
		{
			$ev_relation_eval_cnt_list[$ev_relation_eval_result['receiver_s_no']] = $ev_relation_eval_result['cnt'];
		}

		$ev_relation_sql    = "
			SELECT
				*,
				(SELECT s.s_name FROM staff s WHERE s.s_no=er.manager) as manager_name,
				(SELECT s.s_name FROM staff s WHERE s.s_no=er.reg_s_no) as reg_name,
				(SELECT s.s_name FROM staff s WHERE s.s_no=er.receiver_s_no) as rec_name,
				(SELECT t.team_name FROM team t WHERE t.team_code=er.receiver_team) as rec_team_name,
				(SELECT t.priority FROM team t WHERE t.team_code=er.receiver_team) as rec_t_priority,
				(SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as eval_name,
				(SELECT t.team_name FROM team t WHERE t.team_code=er.evaluator_team) as eval_team_name,
				(SELECT t.priority FROM team t WHERE t.team_code=er.evaluator_team) as eval_t_priority,
				(SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as eval_name,
				IF(er.receiver_s_no!=0,'1', '2') as rec_chk,
				IF(er.evaluator_s_no!=0, '1', '2') as eval_chk,
				IF(er.receiver_s_no=er.evaluator_s_no, '1', '2') as self_chk,
			   	IF(er.evaluator_s_no='1', '1', '2') as leader_chk,
				IF(er.receiver_team=er.evaluator_team, '1', '2') as team_chk,
				IF(er.rec_is_review=1 AND er.eval_is_review=1, '1', '2') as is_eval
			FROM evaluation_relation as er
			LEFT JOIN evaluation_system es ON es.ev_no=er.ev_no
			WHERE {$add_where}
			ORDER BY manager, rec_chk, rec_t_priority, receiver_s_no, self_chk, is_eval, leader_chk, team_chk, eval_t_priority, evaluator_s_no
		";
		$ev_relation_query = mysqli_query($my_db, $ev_relation_sql);
		while($ev_relation = mysqli_fetch_assoc($ev_relation_query))
		{
			if(!isset($ev_relation_manager_list[$ev_relation['manager']])){
				$ev_relation_manager_list[$ev_relation['manager']] = array(
					"ev_no" => $ev_relation['ev_no'],
					"name" 	=> $ev_relation['manager_name'],
					"cnt"	=> 0
				);
			}
			$ev_relation_manager_list[$ev_relation['manager']]['cnt']++;

			if(!isset($ev_relation_rec_list[$ev_relation['manager']][$ev_relation['receiver_s_no']])){
				$ev_relation_rec_list[$ev_relation['manager']][$ev_relation['receiver_s_no']] = 0;
			}
			$ev_relation_rec_list[$ev_relation['manager']][$ev_relation['receiver_s_no']]++;

			$review_status = $review_status_color = "";
			if($ev_relation['receiver_s_no'] > 0 && $ev_relation['receiver_s_no'] == $ev_relation['evaluator_s_no']){
				$review_status_color 	= "lightgray";
				$review_status 		 	= "자기평가";
			}
			elseif($is_review_step && ($ev_relation['rec_is_review'] == 0 || $ev_relation['eval_is_review'] == 0)){
				$review_status_color 	= "green";
				$review_status 		 	= "(확인중)";
			}elseif($is_review_step && ($ev_relation['rec_is_review'] != $ev_relation['eval_is_review'])){
				$review_status_color 	= "red";
				$review_status 		 	= "(불일치)";
			}elseif($is_review_step && ($ev_relation['rec_is_review'] == 2 || $ev_relation['eval_is_review'] == 2)){
				$review_status 			= "대상아님";
				$review_status_color 	= "gray";
			}elseif($is_review_step && ($ev_relation['rec_is_review'] == 1 || $ev_relation['eval_is_review'] == 1)){
				$review_status 			= "평가";
				$review_status_color 	= "black";
			}

			$eval_manager = $rec_manager_list[$ev_relation['evaluator_s_no']];

			$ev_relation_eval_list[$ev_relation['manager']][] = array(
				"ev_r_no"       	=> $ev_relation['ev_r_no'],
				"manager"			=> $ev_relation['manager'],
				"is_editable"		=> ($session_s_no == $ev_relation['manager']) ? true : false,
				"chk_reg_s_no"		=> ($ev_relation['reg_s_no'] != $ev_relation['manager'] && $ev_relation['reg_s_no'] != '28') ? true : false,
				"reg_s_name"		=> $ev_relation['reg_name'],
				"receiver_s_no"		=> $ev_relation['receiver_s_no'],
				"receiver_name"		=> $ev_relation['rec_name'],
				"rec_team_name"		=> $ev_relation['rec_team_name'],
				"rec_staff_list"	=> $add_rec_staff_list,
				"evaluator_manager"	=> $rec_staff_list[$eval_manager],
				"evaluator_s_no"	=> $ev_relation['evaluator_s_no'],
				"evaluator_name"	=> $ev_relation['evaluator_name'],
				"eval_team_name"	=> $ev_relation['eval_team_name'],
				"eval_s_name"		=> $ev_relation['eval_name'],
				"rec_is_review"		=> $ev_relation['rec_is_review'],
				"rec_review"		=> $ev_relation['rec_review'],
				"review_status"		=> $review_status,
				"review_color"		=> $review_status_color,
				"eval_is_review"	=> $ev_relation['eval_is_review'],
				"eval_review"		=> $ev_relation['eval_review'],
				"group_rate"		=> $ev_relation['group_rate'],
				"rate"				=> $ev_relation['rate'],
			);

			if(!isset($ev_rec_rate_sum_list[$ev_relation['receiver_s_no']])){
				$ev_rec_rate_sum_list[$ev_relation['receiver_s_no']] = 0;
			}
			$ev_rec_rate_sum_list[$ev_relation['receiver_s_no']] += $ev_relation['rate'];
		}

		$rec_prev_sql 		= "
			SELECT
				es.ev_no,
				es.subject
			FROM evaluation_system es
			WHERE es.ev_no != '{$sch_ev_no}' AND es.ev_u_set_no='{$evaluation_system['ev_u_set_no']}'
			AND (es.subject NOT LIKE '%테스트%' AND es.subject NOT LIKE '%복제%' AND es.subject NOT LIKE '%TEST%' AND es.subject NOT LIKE '%검토%' AND es.subject NOT LIKE '%전환%' AND es.subject NOT LIKE '%재평가%')
			AND es.ev_state IN('3','4')
			ORDER BY es.ev_no DESC
		";
		$rec_prev_query = mysqli_query($my_db, $rec_prev_sql);
		while($rec_prev = mysqli_fetch_assoc($rec_prev_query))
		{
			if(!empty($rec_prev['subject'])){
				$ev_rec_prev_list[$rec_prev['ev_no']] = $rec_prev['subject'];
			}
		}
	}

	$smarty->assign("is_admin_editable", $is_admin_editable);
	$smarty->assign("is_manager_editable", $is_manager_editable);
	$smarty->assign("is_review_step", $is_review_step);
	$smarty->assign("is_review_editable", $is_review_editable);
	$smarty->assign("ev_date_notice", $ev_date_notice);
	$smarty->assign("chk_init_editable", $chk_init_editable);
	$smarty->assign("ev_state_option", $ev_state_option);
	$smarty->assign("ev_relation_state_option", getEvRelationStateOption());
	$smarty->assign("ev_review_option", getEvReviewOption());
	$smarty->assign("evaluation_system_list", $evaluation_system_list);
	$smarty->assign("staff_manager_list", $staff_manager_list);
	$smarty->assign("sch_team_list", $team_full_name_list);
	$smarty->assign("sch_staff_list", $sch_staff_list);
	$smarty->assign("rec_staff_list", $rec_staff_list);
	$smarty->assign("eval_staff_list", $eval_staff_list);

	$smarty->assign("ev_system", $evaluation_system);
	$smarty->assign("ev_relation_manager_list", $ev_relation_manager_list);
	$smarty->assign("ev_relation_rec_list", $ev_relation_rec_list);
	$smarty->assign("ev_relation_eval_list", $ev_relation_eval_list);
	$smarty->assign("ev_rec_prev_list", $ev_rec_prev_list);
	$smarty->assign("ev_rec_rate_sum_list", $ev_rec_rate_sum_list);
	$smarty->assign("ev_relation_eval_cnt_list", $ev_relation_eval_cnt_list);

	$smarty->display('evaluation_setting_list.html');
}
?>
