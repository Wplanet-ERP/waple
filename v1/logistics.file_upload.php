<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');


$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'add_file') # 생성
{
    $lm_no_val = isset($_POST['lm_no']) ? $_POST['lm_no'] : "";

    # 업무번호 및 타입 확인
    if(empty($lm_no_val)){
        $smarty->display('access_company_error.html');
        exit;
    }

    # Dropbox 이미지 확인 및 저장
    $file_origin_read = isset($_POST['file_origin_read']) ? $_POST['file_origin_read'] : "";
    $file_origin_name = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_path        = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_name        = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk         = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    $file_origin = "";
    $file_read   = "";
    if($file_chk)
    {
        if(!empty($file_path) && !empty($file_name))
        {
            $file_reads  = move_store_files($file_path, "dropzone_tmp", "logistics");
            $file_origin = implode(',', $file_name);
            $file_read   = implode(',', $file_reads);

            if(count($file_reads) != count($file_name))
            {
                exit("<script>alert('파일 업로드에 실패했습니다');location.href='logistics.file_upload.php?lm_no={$lm_no_val}';</script>");
            }

            if(!empty($file_origin_read) && !empty($file_origin_name))
            {
                if(!empty($file_path) && !empty($file_name))
                {
                    $del_images = array_diff(explode(',', $file_origin_read), $file_path);
                }else{
                    $del_images = explode(',', $file_origin_read);
                }

                del_files($del_images);
            }
        }
    }

    # 파일 Path 및 이름 저장
    $upd_sql = "UPDATE `logistics_management` SET quick_req_file_name='{$file_origin}', quick_req_file_path='{$file_read}' WHERE lm_no = '{$lm_no_val}'";

    if(mysqli_query($my_db, $upd_sql)) {
        exit("<script>alert('파일 업로드에 성공했습니다');self.close();</script>");
    }else{
        exit("<script>alert('파일 업로드에 실패했습니다');location.href='logistics.file_upload.php?lm_no={$lm_no_val}';</script>");
    }
}

# 페이지 로드
$lm_no   = isset($_GET['lm_no']) ? $_GET['lm_no'] : "";;

if(empty($lm_no)){
    $smarty->display('access_company_error.html');
    exit;
}

$logistics_sql 	    = "SELECT quick_req_file_name as file_name, quick_req_file_path as file_path FROM `logistics_management` WHERE lm_no = {$lm_no} LIMIT 1";
$logistics_query    = mysqli_query($my_db, $logistics_sql);
$logistics 		    = mysqli_fetch_assoc($logistics_query);
$file_name          = $logistics['file_name'];
$file_path          = $logistics['file_path'];

if(!empty($file_name) && !empty($file_path))
{
    $file_names = explode(',', $file_name);
    $file_paths = explode(',', $file_path);
}else{
    $file_names = [];
    $file_paths = [];
}
$total_count = empty($file_paths) ? 0 : count($file_paths);

$smarty->assign("file_count", $total_count);
$smarty->assign("lm_no", $lm_no);
$smarty->assign("file_names", $file_names);
$smarty->assign("file_paths", $file_paths);

$smarty->display('logistics.file_upload.html');

?>
