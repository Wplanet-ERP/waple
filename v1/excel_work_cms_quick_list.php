<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_cms.php');
require('inc/helper/logistics.php');
require('inc/model/Company.php');
require('Classes/PHPExcel.php');

$is_freelancer  = false;
if($session_staff_state == '2'){ // 프리랜서의 경우 기본 접근제한으로 설정
    $is_freelancer = true;
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$nowdate = date("Y-m-d H:i:s");
$lfcr   = chr(10) ;

//상단타이틀
if($is_freelancer) {
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A2', "w_no{$lfcr}작성일")
        ->setCellValue('B2', "발송진행상태")
        ->setCellValue('C2', "발송지")
        ->setCellValue('D2', "주문번호")
        ->setCellValue('E2', "수령자명{$lfcr}수령자전화{$lfcr}[우편번호]{$lfcr}수령지")
        ->setCellValue("F2", "업체명/브랜드명")
        ->setCellValue("G2", "커머스 상품명")
        ->setCellValue("H2", "수량")
        ->setCellValue("I2", "총수량")
        ->setCellValue("J2", "구성품목(SKU) * 수량{$lfcr}(수량 = 구성품수량 x 상품수량)")
        ->setCellValue("K2", "출고희망일")
        ->setCellValue("L2", "요청사항")
        ->setCellValue("M2", "출고요청자")
        ->setCellValue("N2", "출고완료일")
        ->setCellValue("O2", "배차정보")
        ->setCellValue("P2", "이슈사항")
        ->setCellValue("Q2", "납품처리자")
        ->setCellValue("R2", "납품업체");
}else{
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A2', "w_no{$lfcr}작성일")
        ->setCellValue('B2', "발송진행상태")
        ->setCellValue('C2', "발송지")
        ->setCellValue('D2', "주문번호")
        ->setCellValue('E2', "수령자명{$lfcr}수령자전화{$lfcr}[우편번호]{$lfcr}수령지")
        ->setCellValue("F2", "업체명/브랜드명")
        ->setCellValue("G2", "커머스 상품명")
        ->setCellValue("H2", "수량")
        ->setCellValue("I2", "총수량")
        ->setCellValue("J2", "구성품목(SKU) * 수량{$lfcr}(수량 = 구성품수량 x 상품수량)")
        ->setCellValue("K2", "상품별 결제금액")
        ->setCellValue("L2", "실 결제금액{$lfcr}(상품합계)")
        ->setCellValue("M2", "출고희망일")
        ->setCellValue("N2", "요청사항")
        ->setCellValue("O2", "출고요청자")
        ->setCellValue("P2", "출고완료일")
        ->setCellValue("Q2", "배차정보")
        ->setCellValue("R2", "이슈사항")
        ->setCellValue("S2", "납품처리자")
        ->setCellValue("T2", "납품업체")
        ->setCellValue("U2", "관리자 메모");
}

# 검색조건
$add_where  = "1=1";
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

if (!empty($sch_prd) && $sch_prd != "0") { // 상품
    $add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

$today_val      = date('Y-m-d');
$week_val       = date('Y-m-d', strtotime('-1 weeks'));
$month_val      = date('Y-m-d', strtotime('-1 months'));
$months_val     = date('Y-m-d', strtotime('-3 months'));
$year_val       = date('Y-m-d', strtotime('-1 years'));
$years_val      = date('Y-m-d', strtotime('-2 years'));

# 검색 처리
if($is_freelancer)
{
    $sch_free_quick_type = isset($_GET['sch_free_quick_type']) ? $_GET['sch_free_quick_type'] : "";

    if($sch_free_quick_type == '1'){
        $sch_reg_s_date         = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : "";
        $sch_reg_e_date         = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : "";
        $sch_reg_date_type      = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "";
        $sch_sender_type        = '2809';
    }
    elseif($sch_free_quick_type == '2')
    {
        $sch_reg_s_date         = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : "";
        $sch_reg_e_date         = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : "";
        $sch_reg_date_type      = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "";
        $sch_receiver_type      = '2809';
    }

}else{
    $sch_reg_s_date         = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : $week_val;
    $sch_reg_e_date         = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : $today_val;
    $sch_reg_date_type      = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "week";
    $sch_sender_type 	    = isset($_GET['sch_sender_type']) ? $_GET['sch_sender_type'] : "";
    $sch_receiver_type 	    = isset($_GET['sch_receiver_type']) ? $_GET['sch_receiver_type'] : "";
}

$sch_w_no 			    = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
$sch_quick_state        = isset($_GET['sch_quick_state']) ? $_GET['sch_quick_state'] : "";
$sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_req_memo 	        = isset($_GET['sch_req_memo']) ? $_GET['sch_req_memo'] : "";
$sch_req_s_name	        = isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : "";
$sch_run_s_date 	    = isset($_GET['sch_run_s_date']) ? $_GET['sch_run_s_date'] : "";
$sch_run_e_date 	    = isset($_GET['sch_run_e_date']) ? $_GET['sch_run_e_date'] : "";
$sch_req_date 		    = isset($_GET['sch_req_date']) ? $_GET['sch_req_date'] : "";
$sch_delivery_method    = isset($_GET['sch_delivery_method']) ? $_GET['sch_delivery_method'] : "";
$sch_delivery_no        = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
$sch_is_quick_issue	    = isset($_GET['sch_is_quick_issue']) ? $_GET['sch_is_quick_issue'] : "";
$sch_quick_issue 		= isset($_GET['sch_quick_issue']) ? $_GET['sch_quick_issue'] : "";
$sch_run_c_no 		    = isset($_GET['sch_run_c_no']) ? $_GET['sch_run_c_no'] : "";
$sch_c_no 		        = isset($_GET['sch_c_no']) ? $_GET['sch_c_no'] : "";
$sch_prd_name 		    = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_logistics          = isset($_GET['sch_logistics']) ? $_GET['sch_logistics'] : "";
$sch_receiver_name      = isset($_GET['sch_receiver_name']) ? $_GET['sch_receiver_name'] : "";
$sch_quick_search 	    = isset($_GET['sch_quick_search']) ? $_GET['sch_quick_search'] : "";
$sch_is_coupang 	    = isset($_GET['sch_is_coupang']) ? $_GET['sch_is_coupang'] : "";
$sch_is_price 	        = isset($_GET['sch_is_price']) ? $_GET['sch_is_price'] : "";
$sch_loc_subject        = isset($_GET['sch_loc_subject']) ? $_GET['sch_loc_subject'] : "";

if(!empty($sch_w_no)){
    $add_where .= " AND w.w_no='{$sch_w_no}'";
}

if(!empty($sch_reg_s_date) || !empty($sch_reg_e_date))
{
    if(!empty($sch_reg_s_date)){
        $sch_reg_s_datetime = "{$sch_reg_s_date} 00:00:00";
        $add_where      .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
    }

    if(!empty($sch_reg_e_date)){
        $sch_reg_e_datetime = "{$sch_reg_e_date} 23:59:59";
        $add_where      .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
    }
}

if(!empty($sch_quick_state)){
    $add_where .= " AND w.quick_state='{$sch_quick_state}'";
}

if(!empty($sch_sender_type)){
    $add_where .= " AND w.sender_type='{$sch_sender_type}'";
}

if(!empty($sch_receiver_type)){
    $add_where .= " AND w.receiver_type='{$sch_receiver_type}'";
}

if(!empty($sch_order_number)){
    $add_where .= " AND w.order_number='{$sch_order_number}'";
}

if(!empty($sch_req_memo)){
    $add_where .= " AND w.req_memo LIKE '%{$sch_req_memo}%'";
}

if(!empty($sch_req_s_name)){
    $add_where .= " AND w.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_req_s_name}%')";
}

if(!empty($sch_run_s_date)){
    $add_where .= " AND w.run_date >= '{$sch_run_s_date}'";
}

if(!empty($sch_run_e_date)){
    $add_where .= " AND w.run_date <= '{$sch_run_e_date}'";
}

if(!empty($sch_req_date)){
    $add_where .= " AND w.req_date='{$sch_req_date}'";
}

if(!empty($sch_delivery_method)){
    $add_where .= " AND w.delivery_method = '{$sch_delivery_method}'";
}

if(!empty($sch_delivery_no)){
    $add_where .= " AND w.order_number IN(SELECT DISTINCT order_number FROM work_cms_quick_delivery WHERE delivery_no = '{$sch_delivery_no}')";
}

if(!empty($sch_is_quick_issue)){
    $add_where .= " AND w.is_quick_issue = 1";
}

if(!empty($sch_quick_issue)){
    $add_where .= " AND w.quick_issue LIKE '%{$sch_quick_issue}%'";
}

if(!empty($sch_run_c_no)){
    $add_where .= " AND w.run_c_no='{$sch_run_c_no}'";
}

if(!empty($sch_c_no)){
    $add_where .= " AND w.c_no='{$sch_c_no}'";
}

if(!empty($sch_prd_name)){
    $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.title LIKE '%{$sch_prd_name}%')";
}

if(!empty($sch_receiver_name)){
    $add_where .= " AND w.receiver='{$sch_receiver_name}'";
}

if(!empty($sch_logistics)){
    $logistics_sql    = "SELECT order_number FROM logistics_management WHERE lm_no='{$sch_logistics}'";
    $logistics_query  = mysqli_query($my_db, $logistics_sql);
    $logistics_result = mysqli_fetch_assoc($logistics_query);
    $logistics_list   = [];

    if(isset($logistics_result['order_number']) && !empty($logistics_result['order_number']))
    {
        $logistics_tmp = explode(",", $logistics_result['order_number']);
        foreach($logistics_tmp as $log_ord){
            $logistics_list[] = "'{$log_ord}'";
        }
    }

    if(!empty($logistics_list))
    {
        $logistics_where = implode(",", $logistics_list);
        $add_where .= " AND w.order_number IN ({$logistics_where})";
    }
}

if(!empty($sch_quick_search)){
    if($sch_quick_search == '1'){
        $add_where .= " AND w.run_c_no NOT IN(5468, 5469)";
    }
}

if(!empty($sch_is_coupang)){
    if($sch_is_coupang == '1'){
        $add_where .= " AND w.run_c_no NOT IN(5468, 5469)";
    }
}

if(!empty($sch_is_price)){
    if($sch_is_price == '1'){
        $add_where .= " AND w.unit_price > 0";
    }elseif($sch_is_price == '2'){
        $add_where .= " AND w.unit_price = 0";
    }
}

if(!empty($sch_loc_subject)){
    $add_where .= " AND lm.subject='{$sch_loc_subject}'";
}

# 정렬기능
$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";
$add_orderby    = "w.w_no DESC, w.prd_no ASC";
if(!empty($ord_type))
{
    if($ord_type_by == '1'){
        $orderby_val = "ASC";
    }else{
        $orderby_val = "DESC";
    }

    $add_orderby = "w.{$ord_type} {$orderby_val}, w.prd_no ASC";
}

$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "";

$add_orderby    = "w.w_no DESC, w.prd_no ASC";
if(!empty($ord_type))
{
    if($ord_type_by == '1'){
        $orderby_val = "ASC";
    }else{
        $orderby_val = "DESC";
    }

    $add_orderby = "w.{$ord_type} {$orderby_val}, w.prd_no ASC";
}

$add_where_group    = "1=1";
$cms_ord_sql        = "SELECT DISTINCT w.order_number FROM work_cms_quick w LEFT JOIN logistics_management AS lm ON lm.doc_no=w.doc_no WHERE {$add_where} AND w.order_number is not null ORDER BY {$add_orderby}";
$cms_ord_query      = mysqli_query($my_db, $cms_ord_sql);
$order_number_list  = [];
while($cms_ord_result = mysqli_fetch_assoc($cms_ord_query)){
    $order_number_list[] =  "'".$cms_ord_result['order_number']."'";
}
$order_numbers   = implode(',', $order_number_list);

if(!empty($order_numbers)){
    $add_where_group .= " AND w.order_number IN({$order_numbers})";
}else{
    $add_where_group .= " AND w.order_number IS NULL";
}

# 리스트 쿼리
$cms_quick_sql = "
    SELECT
        *,
        DATE_FORMAT(w.regdate, '%Y-%m-%d') as reg_date,
        DATE_FORMAT(w.regdate, '%H:%i') as reg_time,
        (SELECT s.s_name FROM staff s WHERE s.s_no=w.req_s_no) as req_s_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no=w.run_s_no) as run_s_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
        (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name
    FROM
    (
        SELECT
            w.*,
            IF(w.zip_code, CONCAT('[',w.zip_code,']'), '') as postcode
        FROM work_cms_quick w
        WHERE {$add_where_group}
    ) as w
    ORDER BY {$add_orderby}
";
$cms_quick_query    = mysqli_query($my_db, $cms_quick_sql);
$cms_quick_list     = [];
$final_price_list   = [];
$total_qty_list     = [];
$idx                = 3;

$company_model              = Company::Factory();
$run_company_option         = getAddrCompanyOption();
$move_company_option        = getMoveCompanyOption();
$quick_state_option         = getQuickStateOption();
$quick_state_color_option   = getQuickStateColorOption();
$with_log_c_no              = '2809';
while ($cms_quick = mysqli_fetch_array($cms_quick_query))
{
    $cms_quick['quick_state_name']  = isset($quick_state_color_option[$cms_quick['quick_state']]) ? $quick_state_color_option[$cms_quick['quick_state']]['label'] : "";;
    $cms_quick['sender_type_name']  = isset($move_company_option[$cms_quick['sender_type']]) ? $move_company_option[$cms_quick['sender_type']] : "";
    $cms_quick['req_date']          = ($cms_quick['req_date'] == '0000-00-00') ? "" : $cms_quick['req_date'];

    if(!isset($final_price_list[$cms_quick['order_number']])){
        $final_price_list[$cms_quick['order_number']] = $cms_quick['final_price'];
    }elseif($final_price_list[$cms_quick['order_number']] <= 0 && $cms_quick['final_price'] > 0){
        $final_price_list[$cms_quick['order_number']] = $cms_quick['final_price'];
    }

    $unit_sql   = "SELECT (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='{$with_log_c_no}') as sku, pcr.quantity as qty FROM product_cms_relation as pcr WHERE pcr.prd_no ='{$cms_quick['prd_no']}' AND pcr.display='1'";
    $unit_query = mysqli_query($my_db, $unit_sql);
    $unit_list  = [];
    while($unit_result = mysqli_fetch_assoc($unit_query)){
        $unit_result['qty'] = $cms_quick['quantity']*$unit_result['qty'];
        $unit_list[] = $unit_result;
    }
    $cms_quick["unit_list"] = $unit_list;

    if(isset($cms_quick['receiver_hp']) && !empty($cms_quick['receiver_hp'])){
        $f_hp  = substr($cms_quick['receiver_hp'],0,4);
        $e_hp  = substr($cms_quick['receiver_hp'],7,15);
        $cms_quick['receiver_sc_hp'] = $f_hp."***".$e_hp;
    }

    $total_qty_list[$cms_quick['order_number']] += $cms_quick['quantity'];
    $cms_quick_list[$cms_quick['order_number']][] = $cms_quick;
}

if(!empty($cms_quick_list))
{
    $work_sheet = $objPHPExcel->setActiveSheetIndex(0);
    foreach ($cms_quick_list as $ord_no => $cms_quick_data)
    {
        $row_idx  = 0;
        $prd_count = count($cms_quick_data);
        foreach($cms_quick_data as $cms_quick)
        {
            if($row_idx == 0)
            {
                $row_prd_idx = $idx+$prd_count-1;
                if($is_freelancer) {
                    $work_sheet->mergeCells("B{$idx}:B{$row_prd_idx}");
                    $work_sheet->mergeCells("C{$idx}:C{$row_prd_idx}");
                    $work_sheet->mergeCells("D{$idx}:D{$row_prd_idx}");
                    $work_sheet->mergeCells("E{$idx}:E{$row_prd_idx}");
                    $work_sheet->mergeCells("I{$idx}:I{$row_prd_idx}");
                    $work_sheet->mergeCells("K{$idx}:K{$row_prd_idx}");
                    $work_sheet->mergeCells("L{$idx}:L{$row_prd_idx}");
                    $work_sheet->mergeCells("M{$idx}:M{$row_prd_idx}");
                    $work_sheet->mergeCells("N{$idx}:N{$row_prd_idx}");
                    $work_sheet->mergeCells("O{$idx}:O{$row_prd_idx}");
                    $work_sheet->mergeCells("P{$idx}:P{$row_prd_idx}");
                    $work_sheet->mergeCells("Q{$idx}:Q{$row_prd_idx}");
                    $work_sheet->mergeCells("R{$idx}:R{$row_prd_idx}");
                }
                else
                {
                    $work_sheet->mergeCells("B{$idx}:B{$row_prd_idx}");
                    $work_sheet->mergeCells("C{$idx}:C{$row_prd_idx}");
                    $work_sheet->mergeCells("D{$idx}:D{$row_prd_idx}");
                    $work_sheet->mergeCells("E{$idx}:E{$row_prd_idx}");
                    $work_sheet->mergeCells("I{$idx}:I{$row_prd_idx}");
                    $work_sheet->mergeCells("L{$idx}:L{$row_prd_idx}");
                    $work_sheet->mergeCells("M{$idx}:M{$row_prd_idx}");
                    $work_sheet->mergeCells("N{$idx}:N{$row_prd_idx}");
                    $work_sheet->mergeCells("O{$idx}:O{$row_prd_idx}");
                    $work_sheet->mergeCells("P{$idx}:P{$row_prd_idx}");
                    $work_sheet->mergeCells("Q{$idx}:Q{$row_prd_idx}");
                    $work_sheet->mergeCells("R{$idx}:R{$row_prd_idx}");
                    $work_sheet->mergeCells("S{$idx}:S{$row_prd_idx}");
                    $work_sheet->mergeCells("T{$idx}:T{$row_prd_idx}");
                    $work_sheet->mergeCells("U{$idx}:U{$row_prd_idx}");
                }
            }


            $unit_text = "";
            foreach($cms_quick['unit_list'] as $unit_data){
                $unit_text .= "{$unit_data['sku']} * {$unit_data['qty']}".$lfcr;
            }

            $req_memo = "[EMP 입고/반출 메모 삽입요청]{$lfcr}{$cms_quick['quick_memo']}{$lfcr}{$cms_quick['req_memo']}";

            if($is_freelancer) {
                $work_sheet
                    ->setCellValue("A{$idx}", $cms_quick['w_no'].$lfcr.$cms_quick['reg_date'].$lfcr.$cms_quick['reg_time'])
                    ->setCellValue("B{$idx}", $cms_quick['quick_state_name'])
                    ->setCellValue("C{$idx}", $cms_quick['sender_type_name'])
                    ->setCellValueExplicit("D{$idx}", $cms_quick['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue("E{$idx}", $cms_quick['receiver'].$lfcr.$cms_quick['receiver_hp'].$lfcr.$cms_quick['postcode'].$lfcr.$cms_quick['receiver_addr'])
                    ->setCellValue("F{$idx}", $cms_quick['c_name'])
                    ->setCellValue("G{$idx}", $cms_quick['k_prd1_name']." > ".$cms_quick['k_prd2_name'].$lfcr.$cms_quick['prd_name'])
                    ->setCellValue("H{$idx}", $cms_quick['quantity'])
                    ->setCellValue("I{$idx}", $total_qty_list[$cms_quick['order_number']])
                    ->setCellValue("J{$idx}", $unit_text)
                    ->setCellValue("K{$idx}", $cms_quick['req_date'])
                    ->setCellValue("L{$idx}", $req_memo)
                    ->setCellValue("M{$idx}", $cms_quick['req_s_name'])
                    ->setCellValue("N{$idx}", $cms_quick['run_date'])
                    ->setCellValue("O{$idx}", $cms_quick['quick_info'])
                    ->setCellValue("P{$idx}", $cms_quick['quick_issue'])
                    ->setCellValue("Q{$idx}", $cms_quick['run_s_name'])
                    ->setCellValue("R{$idx}", $cms_quick['run_c_name'])
                ;
            }else{
                $work_sheet
                    ->setCellValue("A{$idx}", $cms_quick['w_no'].$lfcr.$cms_quick['reg_date'].$lfcr.$cms_quick['reg_time'])
                    ->setCellValue("B{$idx}", $cms_quick['quick_state_name'])
                    ->setCellValue("C{$idx}", $cms_quick['sender_type_name'])
                    ->setCellValueExplicit("D{$idx}", $cms_quick['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
                    ->setCellValue("E{$idx}", $cms_quick['receiver'].$lfcr.$cms_quick['receiver_hp'].$lfcr.$cms_quick['postcode'].$lfcr.$cms_quick['receiver_addr'])
                    ->setCellValue("F{$idx}", $cms_quick['c_name'])
                    ->setCellValue("G{$idx}", $cms_quick['k_prd1_name']." > ".$cms_quick['k_prd2_name'].$lfcr.$cms_quick['prd_name'])
                    ->setCellValue("H{$idx}", $cms_quick['quantity'])
                    ->setCellValue("I{$idx}", $total_qty_list[$cms_quick['order_number']])
                    ->setCellValue("J{$idx}", $unit_text)
                    ->setCellValue("K{$idx}", number_format($cms_quick['unit_price'],0))
                    ->setCellValue("L{$idx}", number_format($final_price_list[$cms_quick['order_number']],0))
                    ->setCellValue("M{$idx}", $cms_quick['req_date'])
                    ->setCellValue("N{$idx}", $req_memo)
                    ->setCellValue("O{$idx}", $cms_quick['req_s_name'])
                    ->setCellValue("P{$idx}", $cms_quick['run_date'])
                    ->setCellValue("Q{$idx}", $cms_quick['quick_info'])
                    ->setCellValue("R{$idx}", $cms_quick['quick_issue'])
                    ->setCellValue("S{$idx}", $cms_quick['run_s_name'])
                    ->setCellValue("T{$idx}", $cms_quick['run_c_name'])
                    ->setCellValue("U{$idx}", $cms_quick['manager_memo'])
                ;
            }

            $row_idx++;
            $idx++;
        }
    }
}

$idx--;

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "입/출고 요청리스트");
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setSize(14);

if($is_freelancer)
{
    $objPHPExcel->getActiveSheet()->getStyle('A2:R2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A2:R2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A2:R2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
    $objPHPExcel->getActiveSheet()->getStyle('A2:R2')->getFont()->setSize(10);
    $objPHPExcel->getActiveSheet()->getStyle("A3:R{$idx}")->getFont()->setSize(9);
    $objPHPExcel->getActiveSheet()->getStyle("A2:R{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:R{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("E3:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("G3:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("J3:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("L3:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("O3:O{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("P3:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

    $objPHPExcel->getActiveSheet()->getStyle("A2:A{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("G3:G{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("O3:O{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("P3:P{$idx}")->getAlignment()->setWrapText(true);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(32);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(32);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(32);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(28);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);

    $objPHPExcel->getActiveSheet()->getStyle("A2:R{$idx}")->applyFromArray($styleArray);
}
else
{
    $objPHPExcel->getActiveSheet()->getStyle('A2:U2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A2:U2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A2:U2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('D9D9D9');
    $objPHPExcel->getActiveSheet()->getStyle('A2:U2')->getFont()->setSize(10);
    $objPHPExcel->getActiveSheet()->getStyle("A3:U{$idx}")->getFont()->setSize(9);
    $objPHPExcel->getActiveSheet()->getStyle("A2:U{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("A2:U{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("E3:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("G3:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("H3:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objPHPExcel->getActiveSheet()->getStyle("J3:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("K3:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objPHPExcel->getActiveSheet()->getStyle("L3:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objPHPExcel->getActiveSheet()->getStyle("N3:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("Q3:Q{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("R3:R{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $objPHPExcel->getActiveSheet()->getStyle("U3:U{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

    $objPHPExcel->getActiveSheet()->getStyle("A2:A{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("G3:G{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("N3:N{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("Q3:Q{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("R3:R{$idx}")->getAlignment()->setWrapText(true);
    $objPHPExcel->getActiveSheet()->getStyle("U3:U{$idx}")->getAlignment()->setWrapText(true);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(32);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(32);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(32);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(16);
    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(14);
    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(16);

    $objPHPExcel->getActiveSheet()->getStyle("A2:U{$idx}")->applyFromArray($styleArray);
}

$objPHPExcel->getActiveSheet()->setTitle("입출고요청 리스트");


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_입출고 요청리스트.xlsX");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save('php://output');
exit;

?>
