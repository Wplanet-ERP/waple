<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_message.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "52";
$nav_title   = "구매알림 발송(log)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);


# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$sch_dest_phone = isset($_GET['sch_dest_phone']) ? $_GET['sch_dest_phone'] : "";
$sch_month 		= isset($_GET['sch_month']) ? $_GET['sch_month'] : date("Y-m");
$sch_year_val 	= (int)date("Y", strtotime($sch_month));
$sch_month_val 	= (int)date("m", strtotime($sch_month));
$is_biz_kakao	= false;

if($sch_year_val < '2024' || ($sch_year_val == '2024' && $sch_month_val < '8')){
	$main_table		= "BIZ_KAKAO_LOG";
	$main_id		= "CMID";
	$add_where		= "1=1";
	$is_biz_kakao	= true;
}else{
	$main_table		= "EASY_SEND_LOG";
	$main_id		= "MSG_ID";
	$add_where		= "MSG_TYPE = 'AT'";
}

if(!empty($sch_dest_phone)) {
    $add_where .= " AND DEST_PHONE like '%{$sch_dest_phone}%'";
    $smarty->assign("sch_dest_phone", $sch_dest_phone);
}

if(!empty($sch_month))
{
	$sch_month_s_datetime 	= "{$sch_month}-01 00:00:00";
	$sch_month_e_day		= date("t", strtotime($sch_month));
	$sch_month_e_datetime	= "{$sch_month}-{$sch_month_e_day} 23:59:59";

	$add_where .= " AND REQUEST_TIME BETWEEN '{$sch_month_s_datetime}' AND '{$sch_month_e_datetime}'";
	$smarty->assign("sch_month", $sch_month);
}

# 전체 게시물 수
$total_sql 	  	= "SELECT COUNT({$main_id}) as total_cnt FROM {$main_table} WHERE {$add_where}";
$total_query  	= mysqli_query($my_db, $total_sql);
$total_result 	= mysqli_fetch_array($total_query);
$total_num 		= $total_result['total_cnt'];

# 페이징 처리
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 20;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($total_num/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list 	= pagelist($pages, "crm_log_list.php", $page_num, $search_url);

$smarty->assign("total_num", $total_num);
$smarty->assign("search_url", $search_url);
$smarty->assign("page_list", $page_list);

# 리스트 쿼리
$crm_log_sql = "
	SELECT
		*
	FROM {$main_table}
	WHERE {$add_where}
	ORDER BY {$main_id} DESC
	LIMIT {$offset}, {$num}
";
$crm_log_query 			= mysqli_query($my_db, $crm_log_sql);
$crm_send_status		= ($is_biz_kakao) ? getBizStatusOption() : getMessageStateOption();
$crm_send_call_status	= ($is_biz_kakao) ? getBizCallStatusOption() : getMessageCallStatusOption();
$crm_log_list = [];
while ($crm_log = mysqli_fetch_array($crm_log_query))
{
	if(!empty($sch_dest_phone)){
		$dest_phone = $crm_log['DEST_PHONE'];
	}else{
		$f_hp = substr($crm_log['DEST_PHONE'], 0, 3);
		$e_hp = substr($crm_log['DEST_PHONE'], 7, 4);
		$dest_phone = $f_hp . "****" . $e_hp;
	}
	
	$crm_log_list[] = array (
		"MSG_ID"		=> $crm_log[$main_id],
		"UMID" 			=> $crm_log['UMID'],
		"MSG_TYPE" 		=> "알림톡",
        "STATUS" 		=> $crm_send_status[$crm_log['STATUS']],
        "CALL_STATUS" 	=> $crm_send_call_status[$crm_log['CALL_STATUS']],
		"SEND_TIME" 	=> date("Y-m-d H:i", strtotime($crm_log['SEND_TIME'])),
		"REPORT_TIME" 	=> !empty($crm_log['REPORT_TIME']) ? date("Y-m-d H:i", strtotime($crm_log['REPORT_TIME'])) : "",
		"SEND_PHONE" 	=> $crm_log['SEND_PHONE'],
		"REGIST_COMPANY"=> !empty($crm_log['REGIST_ID']) ? "belabef(아임웹)" : "",
		"REGIST_ID" 	=> $crm_log['REGIST_ID'],
		"DEST_NAME" 	=> $crm_log['DEST_NAME'],
		"DEST_PHONE" 	=> $dest_phone,
		"MSG_BODY"		=> $crm_log['MSG_BODY']
	);
}

$smarty->assign("crm_log_list", $crm_log_list);

$smarty->display('crm_log_list.html');
?>
