<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');

// 접근 권한
if (!$session_s_no){
	$smarty->display('access_company_error.html');
	exit;
}

$process = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'add_comment')
{
    $ord_no  = isset($_POST['ord_no']) ? $_POST['ord_no'] : "";
    $s_no    = isset($_POST['s_no']) ? $_POST['s_no'] : "";
    $s_name  = isset($_POST['s_name']) ? $_POST['s_name'] : "";
    $comment = isset($_POST['comment']) ? nl2br(addslashes(trim($_POST['comment']))) : "";
    $regdate = date('Y-m-d H:i:s');
    $add_set = "";

    $file_origins   = isset($_POST['file_origin']) ? $_POST['file_origin'] : [];
    $file_paths     = isset($_POST['file_read']) ? $_POST['file_read'] : [];
    $file_types     = isset($_POST['file_type']) ? $_POST['file_type'] : [];

    $idx = 0;
    if (!empty($file_origins))
    {
        $file_reads  = move_store_files($file_paths, "dropzone_tmp", "cms_comment");

        foreach ($file_origins as $file_origin) {
            $file_idx = $idx + 1;
            if ($file_origin) {
                $add_set .= "file{$file_idx}_origin = '{$file_origin}', ";
            }

            if (isset($file_reads[$idx])) {
                $add_set .= "file{$file_idx}_read = '{$file_reads[$idx]}', ";
            }

            if (isset($file_types[$idx])) {
                $add_set .= "file{$file_idx}_type = '{$file_types[$idx]}', ";
            }

            $idx++;
        }
    }

    $add_comment_sql = "
       INSERT INTO `work_cms_comment` SET
          s_no = '{$s_no}',
          ord_no = '{$ord_no}',
          s_name = '{$s_name}',
          {$add_set}
          comment = '{$comment}',
          regdate = '{$regdate}'
    ";

    if(!mysqli_query($my_db, $add_comment_sql)){
        alert('답변 등록이 실패했습니다', "work_list_cs_comment_iframe.php?ord_no={$ord_no}");
    }else{
        alert('답변을 등록했습니다', "work_list_cs_comment_iframe.php?ord_no={$ord_no}");
    }
}
elseif($process == 'del_comment')
{
    $w_c_no  = isset($_POST['w_c_no']) ? $_POST['w_c_no'] : "";
    $ord_no  = isset($_POST['ord_no']) ? $_POST['ord_no'] : "";
    $del_comment_sql = "DELETE FROM `work_cms_comment` WHERE w_c_no='{$w_c_no}'";

    if(!mysqli_query($my_db, $del_comment_sql)){
        alert('삭제에 실패했습니다', "work_list_cs_comment_iframe.php?ord_no={$ord_no}");
    }else{
        alert('삭제했습니다', "work_list_cs_comment_iframe.php?ord_no={$ord_no}");
    }
}

$order_number = (isset($_GET['ord_no']))?$_GET['ord_no']:"";

if(!$order_number){
    alert('해당 주문번호가 없습니다.', 'work_list_cs_comment_iframe.php');
}

$comment_sql   = "SELECT * FROM work_cms_comment WHERE ord_no = '{$order_number}'";
$comment_query = mysqli_query($my_db, $comment_sql);
$comment_list  = [];
while($comment = mysqli_fetch_assoc($comment_query)){
    $comment_list[] = $comment;
}

$smarty->assign("order_number", $order_number);
$smarty->assign("comment_list", $comment_list);

//$smarty->display('work_list_cs_comment.html');
$smarty->display('work_list_cs_comment_iframe.html');
?>
