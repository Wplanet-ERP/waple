<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
require('inc/helper/commerce_sales.php');


function get_time() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

# 사용 리스트 데이터 Init
$comm_brand_list = getCommerceBrandList();

$alpha_list = array(
    "D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
    "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ",
    "BA","BB","BC","BD","BE","BF","BG","BH","BI","BJ","BK","BL","BM","BN","BO","BP","BQ","BR","BS","BT","BU","BV","BW","BX"
);


# 엑셀 파일 Load
$file_name = $_FILES["mc_file"]["tmp_name"];

$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow()+1;


# POST 데이터
$mc_brand   = isset($_POST['mc_brand']) ? $_POST['mc_brand'] : "";
$mc_apply   = isset($_POST['mc_apply']) ? $_POST['mc_apply'] : "";
$mc_s_date  = isset($_POST['mc_s_date']) ? $_POST['mc_s_date'] : date('Y-m-d');
$mc_e_date  = isset($_POST['mc_e_date']) ? $_POST['mc_e_date'] : date('Y-m-d');
$search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

if(empty($mc_brand) || empty($mc_s_date) || empty($mc_e_date)){
    exit ("<script>alert('오류가 발생했습니다. 다시 시도해 주세요');location.href='media_commerce_auto_report.php?{$search_url}';</script>");
}

# 브랜드 체크
$mc_brand_name = $comm_brand_list[$mc_brand];
$ex_brand_name = (string)trim(addslashes($objWorksheet->getCell("B1")->getValue()));

if($mc_brand_name != $ex_brand_name)
{
    exit ("<script>alert('엑셀파일 내 브랜드명이 일치하지 않아 UPLOAD 할 수 없습니다.\\r\\n확인 후 재시도 바라며, 문제가 지속될 경우 담당자에게 문의하세요.');location.href='media_commerce_auto_report.php?{$search_url}';</script>");
}

# 항목 체크
$alpha_data_list        = [];
$mc_data_sch_brand_list = [];
foreach ($alpha_list as $key)
{
    $k_name_val      = (string)trim(addslashes($objWorksheet->getCell("{$key}4")->getValue()));

    if(strpos($k_name_val,"OFF") === false)
    {
        if(!empty($k_name_val)){
            $kind_sch_sql    = "SELECT k_name_code, k_code FROM kind WHERE k_name='{$k_name_val}' AND k_code IN('cost','sales') LIMIT 1";
            $kind_sch_query  = mysqli_query($my_db, $kind_sch_sql);
            $kind_sch_result = mysqli_fetch_assoc($kind_sch_query);

            $k_name_code_result = str_pad($kind_sch_result['k_name_code'], 5, "0", STR_PAD_LEFT);
            $k_code_result      = $kind_sch_result['k_code'];

            if($k_name_code_result == '00000' && empty($k_code_result)){
                exit ("<script>alert('존재하지 항목({$k_name_val})이 있어 UPLOAD 할 수 없습니다.\\r\\n문제가 지속될 경우 담당자에게 문의하세요.');location.href='media_commerce_auto_report.php?{$search_url}';</script>");
            }

            $mc_data_sch_brand_list[] = $k_name_code_result;
            $alpha_data_list[$key] = array('k_name_code' => $k_name_code_result, 'type' => $k_code_result);
        }
    }
}

# 항목별 데이터 유무 확인
$mc_data_check = ($mc_apply == '1') ? true : false;

# 항목별 데이터 정리
$multi_sql_list = [];
$mc_data_upd_result = false;
if(!empty($alpha_data_list))
{
    for ($i = 5; $i <= $totalRow; $i++)
    {
        $sales_date_val = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));
        $sales_date     = date('Y-m-d', strtotime(jdtogregorian((int)$sales_date_val + 2415019)));

        if(empty($sales_date_val)){
            break;
        }

        if($sales_date >= $mc_s_date && $sales_date <= $mc_e_date)
        {
            foreach($alpha_data_list as $alpha => $k_data)
            {
                $type         = $k_data['type'];
                $k_name_code  = $k_data['k_name_code'];
                $row_data_val = (string)trim(addslashes($objWorksheet->getCell("{$alpha}{$i}")->getCalculatedValue()));

                if($k_name_code != '00000' && !empty($type))
                {
                    if (!empty($row_data_val) && $row_data_val > 0)
                    {
                        $mc_data_row_check = "";
                        if ($mc_data_check) {
                            $mc_data_row_check_sql    = "SELECT cr_no FROM commerce_report WHERE brand='{$mc_brand}' AND k_name_code ='{$k_name_code}' AND sales_date='{$sales_date}'";
                            $mc_data_row_check_query  = mysqli_query($my_db, $mc_data_row_check_sql);
                            $mc_data_row_check_result = mysqli_fetch_assoc($mc_data_row_check_query);

                            $mc_data_row_check = isset($mc_data_row_check_result['cr_no']) ? $mc_data_row_check_result['cr_no'] : "";
                        }

                        if(!empty($mc_data_row_check)){
                            $mc_data_upd_sql = "UPDATE commerce_report SET price='{$row_data_val}' WHERE cr_no='{$mc_data_row_check}'";
                            mysqli_query($my_db, $mc_data_upd_sql);
                            $mc_data_upd_result = true;
                        }else{
                            $multi_sql_list[] = "('{$mc_brand}','{$type}','{$k_name_code}','{$sales_date}','{$row_data_val}')";
                        }
                    }
                }
            }
        }
    }
}else{
    exit ("<script>alert('데이터가 없습니다');location.href='media_commerce_auto_report.php?{$search_url}';</script>");
}

# 항목별 데이터 삽입
if(!empty($multi_sql_list))
{
    $comma     = "";
    $multi_sql = "INSERT INTO commerce_report (brand, `type`, k_name_code, sales_date, price) VALUES ";

    foreach($multi_sql_list as $sql){
        $multi_sql .= $comma.$sql;
        $comma      = " , ";
    }

    if(mysqli_query($my_db, $multi_sql)){
        exit ("<script>alert('등록되었습니다.');location.href='media_commerce_auto_report.php?{$search_url}';</script>");
    }else{
        exit ("<script>alert('실패했습니다.\\r\\n확인 후 재시도 바라며, 문제가 지속될 경우 담당자에게 문의하세요.');location.href='media_commerce_auto_report.php?{$search_url}';</script>");
    }
}elseif($mc_data_upd_result){
    exit ("<script>alert('수정되었습니다.');location.href='media_commerce_auto_report.php?{$search_url}';</script>");
}else{
    exit ("<script>alert('데이터가 없습니다');location.href='media_commerce_auto_report.php?{$search_url}';</script>");
}

?>
