<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/broadcast.php');
require('inc/model/BroadCast.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "17";
$nav_title   = "방송광고 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# BroadCast Model
$broadcastModel = BroadCast::Factory();

# Process
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'multi_del_broadcast')
{
    $ba_no_list     = (isset($_POST['chk_ba_no_list'])) ? $_POST['chk_ba_no_list'] : "";
    $search_url_val = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $ba_no_tok = explode("||", $ba_no_list);
    $ba_no_del = implode(",", $ba_no_tok);

    $db_msg    = $broadcastModel->multiDeleteAdvertisement($ba_no_del);

    exit ("<script>alert('{$db_msg}');location.href='broadcast_ad_list.php?{$search_url_val}';</script>");
}

# 방송광고 리스트
$ba_advertiser  = isset($_GET['ba_advertiser']) ? $_GET['ba_advertiser'] : "닥터피엘";
$ba_adv_no      = isset($_GET['ba_adv_no']) ? $_GET['ba_adv_no'] : "1314";

if(!empty($ba_advertiser))
{
    $smarty->assign('ba_advertiser', $ba_advertiser);
}

if(!empty($ba_adv_no))
{
    $smarty->assign('ba_adv_no', $ba_adv_no);
}

# 검색조건
$sch_media      = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_advertiser = isset($_GET['sch_advertiser']) ? $_GET['sch_advertiser'] : "";
$sch_state      = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_title      = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_sub_s_date = isset($_GET['sch_sub_s_date']) ? $_GET['sch_sub_s_date'] : date('Y-m')."-01";
$sch_sub_e_date = isset($_GET['sch_sub_e_date']) ? $_GET['sch_sub_e_date'] : date('Y-m')."-31";
$sch_type       = isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_notice     = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$sch_detail_media   = isset($_GET['sch_detail_media']) ? $_GET['sch_detail_media'] : "";
$sch_subs_gubun     = isset($_GET['sch_subs_gubun']) ? $_GET['sch_subs_gubun'] : "";
$sch_delivery_gubun = isset($_GET['sch_delivery_gubun']) ? $_GET['sch_delivery_gubun'] : "";

$add_where = "1=1";
if(!empty($sch_media))
{
    $add_where .= " AND `ba`.media = '{$sch_media}'";
    $smarty->assign('sch_media', $sch_media);
}

if(!empty($sch_advertiser))
{
    $add_where .= " AND `ba`.advertiser like '%{$sch_advertiser}%'";
    $smarty->assign('sch_advertiser', $sch_advertiser);
}

if(!empty($sch_state))
{
    $add_where .= " AND `ba`.state = '{$sch_state}'";
    $smarty->assign('sch_state', $sch_state);
}

if(!empty($sch_title))
{
    $add_where .= " AND `ba`.program_title LIKE '%{$sch_title}%'";
    $smarty->assign('sch_title', $sch_title);
}

if(!empty($sch_sub_s_date) || !empty($sch_sub_e_date))
{
    if(empty($sch_sub_s_date)){
        $add_where .= " AND `ba`.sub_s_date <= '{$sch_sub_e_date}'";
    }elseif(empty($sch_sub_e_date)){
        $add_where .= " AND `ba`.sub_e_date >= '{$sch_sub_s_date}'";
    }else{
        $add_where .= " AND (`ba`.sub_s_date BETWEEN '{$sch_sub_s_date}' AND '{$sch_sub_e_date}' OR `ba`.sub_s_date <= '{$sch_sub_s_date}' AND `ba`.sub_e_date >= '{$sch_sub_e_date}')";
    }

    $smarty->assign('sch_sub_s_date', $sch_sub_s_date);
    $smarty->assign('sch_sub_e_date', $sch_sub_e_date);
}

if(!empty($sch_type))
{
    $add_where .= " AND `ba`.`type` = '{$sch_type}'";
    $smarty->assign('sch_type', $sch_type);
}

if(!empty($sch_notice))
{
    $add_where .= " AND `ba`.notice LIKE '%{$sch_notice}%'";
    $smarty->assign('sch_notice', $sch_notice);
}

if(!empty($sch_detail_media))
{
    $add_where .= " AND `ba`.detail_media = '{$sch_detail_media}'";
    $smarty->assign('sch_detail_media', $sch_detail_media);
}

if(!empty($sch_subs_gubun))
{
    $add_where .= " AND `ba`.subs_gubun = '{$sch_subs_gubun}'";
    $smarty->assign('sch_subs_gubun', $sch_subs_gubun);
}

if(!empty($sch_delivery_gubun))
{
    $add_where .= " AND `ba`.delivery_gubun = '{$sch_delivery_gubun}'";
    $smarty->assign('sch_delivery_gubun', $sch_delivery_gubun);
}

# Model & Helper 처리
$ba_state_option = getBaStateOption();
$ba_media_option = getBaMediaOption();
$ba_excel_option = getBaExcelOption();


$broadcastModel->setBaAllTypeOption();
$ba_type_option             = $broadcastModel->getBaTypeOption();
$ba_detail_media_option     = $broadcastModel->getBaDetailMediaOption();
$ba_subs_gubun_option       = $broadcastModel->getBaSubsOption();
$ba_delivery_gubun_option   = $broadcastModel->getBaDeliveryOption();
$broadcast_total            = $broadcastModel->getAdvertisementTotal($add_where);

# 페이징 처리
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 10;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($broadcast_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "broadcast_ad_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $broadcast_total);
$smarty->assign("page_list", $page_list);

$broadcast_sql  = "
    SELECT *,
      IF(`ba`.s_time='00:00:00', '', DATE_FORMAT(`ba`.s_time, '%H:%i')) as s_time,
      IF(`ba`.e_time='00:00:00', '', DATE_FORMAT(`ba`.e_time, '%H:%i')) as e_time,
      DATE_FORMAT(`ba`.sub_s_date, '%m/%d') as sub_s_date,
      DATE_FORMAT(`ba`.sub_e_date, '%m/%d') as sub_e_date
    FROM broadcast_advertisement `ba`
    WHERE {$add_where}
    ORDER BY `ba`.ba_no DESC
    LIMIT {$offset}, {$num}
";
$broadcast_query = mysqli_query($my_db, $broadcast_sql);
$broadcast_list  = [];
while($broadcast = mysqli_fetch_assoc($broadcast_query))
{
    $media  = isset($broadcast['media']) && !empty($broadcast['media']) ? $broadcast['media'] : "";
    $state  = isset($broadcast['state']) && !empty($broadcast['state']) ? $broadcast['state'] : "";
    $application_rate  = isset($broadcast['application_rate']) && !empty($broadcast['application_rate']) ? $broadcast['application_rate'] : "";
    $sub_e_date  = isset($broadcast['sub_e_date']) && !empty($broadcast['sub_e_date']) ? $broadcast['sub_e_date'] : "";

    $broadcast['media_name']        = !empty($media) ? $ba_media_option[$media] : "";
    $broadcast['state_name']        = !empty($state) ? $ba_state_option[$state] : "";
    $broadcast['application_rate']  = $application_rate > 0 ? $application_rate : "";
    $broadcast['sub_e_date']        = ($sub_e_date == '00/00') ? "" : $sub_e_date;
    $broadcast_list[] = $broadcast;
}

$smarty->assign('ba_media_option', $ba_media_option);
$smarty->assign('ba_state_option', $ba_state_option);
$smarty->assign('ba_type_option', $ba_type_option);
$smarty->assign('ba_detail_media_option', $ba_detail_media_option);
$smarty->assign('ba_subs_gubun_option', $ba_subs_gubun_option);
$smarty->assign('ba_delivery_gubun_option', $ba_delivery_gubun_option);
$smarty->assign("ba_excel_option", $ba_excel_option);
$smarty->assign('broadcast_list', $broadcast_list);

$smarty->display('broadcast_ad_list.html');
?>
