<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/commerce_sales.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/CommerceSales.php');

# 프로세스 처리 & Model
$comm_sales_model = CommerceSales::Factory();

# Navigation & My Quick
$nav_prd_no  = "206";
$nav_title   = "커머스 자사몰 객단가";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

/** 검색조건 START */
$cms_report_self_list   = getSelfDpCompanyList();
$cms_report_self_text   = implode(",", $cms_report_self_list);
$add_cms_where          =  "delivery_state='4' AND dp_c_no IN({$cms_report_self_text}) AND unit_price > 0";

# Kind(cost,sales), Brand Group 검색
$comm_chart_total_init      = $comm_sales_model->getChartAutoReportData();
$comm_g1_title_list         = $comm_chart_total_init['comm_g1_title'];
$comm_g2_title_list         = $comm_chart_total_init['comm_g2_title'];

$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$sch_brand_name             = "전체";

# 브랜드 검색 설정 및 수정여부
$sch_toggle         = isset($_GET['sch_toggle']) ? $_GET['sch_toggle'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$kind_column_list   = [];
$brand_column_list  = [];
$brand_company_list = [];

$smarty->assign("sch_toggle", $sch_toggle);

if(!isset($_GET['sch_brand_g1']))
{
    switch($session_s_no){
        case '4':
            $sch_brand_g1   = '70003';
            break;
        case '6':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70008';
            break;
        case '7':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70014';
            $sch_brand      = '4446';
            break;
        case '10':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5368';
            break;
        case '15':
            $sch_brand_g1   = '70002';
            break;
        case '17':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '1314';
            break;
        case '42':
        case '177':
        case '265':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            break;
        case '59':
            $sch_brand_g1   = '70005';
            $sch_brand_g2   = '70020';
            $sch_brand      = '2402';
            break;
        case '67':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5434';
            break;
        case '145':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70010';
            break;
        case '147':
            $sch_brand_g1   = '70004';
            $sch_brand_g2   = '70019';
            break;
        case '43':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5812';
            break;
    }
}

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand))
{
    $sch_brand_g2_list              = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list                 = $brand_list[$sch_brand_g2];
    $brand_column_list[$sch_brand]  = $brand_list[$sch_brand_g2][$sch_brand];
    $brand_company_list[$sch_brand] = $sch_brand;
    $sch_brand_name                 = $brand_list[$sch_brand_g2][$sch_brand];

    $add_cms_where .= " AND `w`.c_no = '{$sch_brand}'";

}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $brand_column_list  = $sch_brand_list;
    $sch_brand_name     = $brand_company_g2_list[$sch_brand_g1][$sch_brand_g2];

    foreach($sch_brand_list as $c_no => $c_name) {
        $brand_company_list[$c_no] = $c_no;
    }

    $add_cms_where .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $brand_column_list  = $sch_brand_g2_list;
    $sch_brand_name     = $brand_company_g1_list[$sch_brand_g1];

    foreach($sch_brand_g2_list as $brand_g2 => $brand_g2_name)
    {
        $init_company_list = $brand_list[$brand_g2];
        if(!empty($init_company_list))
        {
            foreach($init_company_list as $c_no => $c_name) {
                $brand_company_list[$c_no] = $c_no;
            }
        }
    }

    $add_cms_where .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
else
{
    $brand_column_list  = $brand_company_g1_list;

    foreach($brand_company_g1_list as $brand_g1 => $brand_g1_name)
    {
        $init_g2_list = $brand_company_g2_list[$brand_g1];
        if(!empty($init_g2_list))
        {
            foreach($init_g2_list as $brand_g2 => $brand_g2_name)
            {
                $init_company_list = $brand_list[$brand_g2];
                if(!empty($init_company_list))
                {
                    foreach($init_company_list as $c_no => $c_name) {
                        $brand_company_list[$c_no] = $c_no;
                    }
                }
            }
        }
    }
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_name", $sch_brand_name);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 날짜별 검색
$today_s_w		 = date('w')-1;
$sch_s_year      = isset($_GET['sch_s_year']) ? $_GET['sch_s_year'] : date('Y', strtotime("-5 years"));;
$sch_e_year      = isset($_GET['sch_e_year']) ? $_GET['sch_e_year'] : date('Y');
$sch_s_month     = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("-5 months"));
$sch_e_month     = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week      = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week      = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date      = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-8 day"));
$sch_e_date      = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');
$sch_not_empty   = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : "";

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_s_year', $sch_s_year);
$smarty->assign('sch_e_year', $sch_e_year);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);
$smarty->assign('sch_not_empty', $sch_not_empty);

//전체 기간 조회 및 누적데이터 조회
$all_date_where   = "";
$all_date_key	  = "";
$add_date_where   = "";
$add_date_column  = "";
$add_cms_column   = "";
$sch_net_date     = "";

if($sch_date_type == '4') //연간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y')";

    $add_date_where  = " AND DATE_FORMAT(sales_date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}' ";
    $add_date_column = "DATE_FORMAT(sales_date, '%Y')";

    $sch_s_datetime  = $sch_s_year."-01-01 00:00:00";
    $sch_e_datetime  = $sch_e_year."-12-31 23:59:59";

    $add_cms_where  .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column  = "DATE_FORMAT(order_date, '%Y')";

    $sch_net_date    = $sch_s_year."-01-01";
}
elseif($sch_date_type == '3') //월간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where  = " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_date_column = "DATE_FORMAT(sales_date, '%Y%m')";

    $sch_s_datetime  = $sch_s_month."-01 00:00:00";
    $sch_e_datetime  = $sch_e_month."-31 23:59:59";

    $add_cms_where  .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column  = "DATE_FORMAT(order_date, '%Y%m')";

    $sch_net_date    = $sch_s_month."-01";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where  = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_date_column = "DATE_FORMAT(DATE_SUB(sales_date, INTERVAL(IF(DAYOFWEEK(sales_date)=1,8,DAYOFWEEK(sales_date))-2) DAY), '%Y%m%d')";

    $sch_s_datetime  = $sch_s_week." 00:00:00";
    $sch_e_datetime  = $sch_e_week." 23:59:59";
    $add_cms_where  .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column  = "DATE_FORMAT(DATE_SUB(order_date, INTERVAL(IF(DAYOFWEEK(order_date)=1,8,DAYOFWEEK(order_date))-2) DAY), '%Y%m%d')";

    $sch_net_date    = $sch_s_week;
}
else //일간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_date_where  = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_date_column = "DATE_FORMAT(sales_date, '%Y%m%d')";

    $sch_s_datetime  = $sch_s_date." 00:00:00";
    $sch_e_datetime  = $sch_e_date." 23:59:59";
    $add_cms_where  .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column  = "DATE_FORMAT(order_date, '%Y%m%d')";

    $sch_net_date    = $sch_s_date;
}

# 배송리스트 구매처
$cms_report_title_list          = getSelfDpCompanyNameList();

$commerce_date_list             = [];
$cms_report_list                = [];
$cms_report_avg_price_list      = [];
$cms_report_total_sum_list      = [];
$cms_report_total_avg_list      = [];

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
if($all_date_where != '')
{
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        foreach($cms_report_title_list as $cms_key => $cms_title)
        {
            $cms_report_list[$cms_key][$date['chart_key']]['count']         = 0;
            $cms_report_list[$cms_key][$date['chart_key']]['total_price']   = 0;
            $cms_report_avg_price_list[$cms_key][$date['chart_key']]        = 0;
        }

        if(!isset($commerce_date_list[$date['chart_key']]))
        {
            $chart_s_date = "";
            $chart_e_date = "";
            $chart_mon    = "";

            if($sch_date_type == '4'){
                $chart_s_date = $chart_title."-01-01";
                $chart_e_date = $chart_title."-12-31";
                $chart_mon    = date("Y-m", strtotime($chart_s_date));
            }elseif($sch_date_type == '3'){
                $chart_s_date_val   = $date['chart_key']."01";
                $chart_s_date       = date("Y-m-d", strtotime("{$chart_s_date_val}"));
                $chart_e_day        = DATE('t', strtotime($chart_s_date));
                $chart_e_date_val   = $date['chart_key'].$chart_e_day;
                $chart_e_date       = date("Y-m-d", strtotime("{$chart_e_date_val}"));
                $chart_mon          = date("Y-m", strtotime($chart_s_date));
            }elseif($sch_date_type == '2'){
                $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_e_date = date("Y-m-d", strtotime("{$chart_s_date} +6 days"));
                $chart_mon    = date("Y-m", strtotime($chart_s_date));
            }elseif($sch_date_type == '1'){
                $chart_s_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_e_date = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_mon    = date("Y-m", strtotime($chart_s_date));
            }
            $commerce_date_list[$date['chart_key']] = array('title' => $chart_title, 's_date' => $chart_s_date, 'e_date' => $chart_e_date, 'mon_date' => $chart_mon);
        }

        $cms_report_total_sum_list[$date['chart_key']]['total_count'] = 0;
        $cms_report_total_sum_list[$date['chart_key']]['total_price'] = 0;
        $cms_report_total_avg_list[$date['chart_key']]                = 0;
    }
}

# 배송리스트 매출관련
$cms_report_sql      = "
    SELECT 
        dp_c_no,
        dp_c_name,
        order_number,
        DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
        {$add_cms_column} as key_date,
        SUM(unit_price) as total_price
    FROM work_cms as w
    WHERE {$add_cms_where}
    GROUP BY order_number
";
$cms_report_query = mysqli_query($my_db, $cms_report_sql);
while($cms_report_result = mysqli_fetch_assoc($cms_report_query))
{
    $cms_report_list[$cms_report_result['dp_c_no']][$cms_report_result['key_date']]['count']++;
    $cms_report_list[$cms_report_result['dp_c_no']][$cms_report_result['key_date']]['total_price'] += $cms_report_result['total_price'];
    $cms_report_total_sum_list[$cms_report_result['key_date']]['total_count']++;
    $cms_report_total_sum_list[$cms_report_result['key_date']]['total_price'] += $cms_report_result['total_price'];
}

foreach($cms_report_list as $dp_c_no => $dp_ord_data){
    foreach($dp_ord_data as $key_date => $key_ord_data){
        $cms_report_avg_price_list[$dp_c_no][$key_date] = ($key_ord_data['count'] > 0) ? (int)$key_ord_data['total_price']/$key_ord_data['count'] : 0;
    }
}

foreach($cms_report_total_sum_list as $key_date => $key_total_data){
    $cms_report_total_avg_list[$key_date] = ($key_total_data['total_count'] > 0) ? (int)$key_total_data['total_price']/$key_total_data['total_count'] : 0;
}

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);
$smarty->assign('commerce_date_list', $commerce_date_list);
$smarty->assign('commerce_date_count', count($commerce_date_list));
$smarty->assign('cms_report_list', $cms_report_list);
$smarty->assign('cms_report_title_list', $cms_report_title_list);
$smarty->assign('cms_report_avg_price_list', $cms_report_avg_price_list);
$smarty->assign('cms_report_total_avg_list', $cms_report_total_avg_list);

$smarty->display('media_commerce_average_report.html');
?>
