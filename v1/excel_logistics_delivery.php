<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/agency.php');
require('inc/helper/work.php');
require('Classes/PHPExcel.php');

# 검색조건
$sch_lm_no = isset($_GET['sch_lm_no']) ? $_GET['sch_lm_no'] : "";

if(empty($sch_lm_no)){
    exit("<script>alert('해당 물류가 존재하지 않습니다.');location.href='logistics_management.php';</script>");
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");


$work_sheet = $objPHPExcel->setActiveSheetIndex(0);
$work_sheet
    ->setCellValue('A1', "주문일시")
    ->setCellValue('B1', "주문번호")
    ->setCellValue('C1', "수령자명")
    ->setCellValue('D1', "수령자전화")
    ->setCellValue('E1', "수령지(주소)")
    ->setCellValue('F1', "발송처리일")
    ->setCellValue('G1', "커머스 상품명")
    ->setCellValue('H1', "수량")
    ->setCellValue('I1', "택배사 내역")
;

$logistics_sql    = "SELECT order_number FROM logistics_management WHERE lm_no='{$sch_lm_no}'";
$logistics_query  = mysqli_query($my_db, $logistics_sql);
$logistics_result = mysqli_fetch_assoc($logistics_query);
$logistics_list   = [];

if(isset($logistics_result['order_number']) && !empty($logistics_result['order_number']))
{
    $logistics_tmp = explode(",", $logistics_result['order_number']);
    foreach($logistics_tmp as $log_ord){
        $logistics_list[] = "'{$log_ord}'";
    }
}

if(empty($logistics_list)){
    exit("<script>alert('해당 물류 주문이 존재하지 않습니다.');location.href='logistics_management.php';</script>");
}

$logistics_where = implode(",", $logistics_list);

# 리스트 데이터
$logistics_delivery_sql = "
    SELECT 
        w.order_date,
        w.order_number,
        w.recipient,
        w.recipient_hp,
        w.recipient_addr,
        w.stock_date,
        (SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no) as prd_name,
        w.quantity,
        (SELECT COUNT(sub.w_no) as ord_cnt FROM work_cms sub WHERE sub.order_number=w.order_number) as ord_cnt
    FROM work_cms `w`
    WHERE order_number IN({$logistics_where})
    ORDER BY order_number
";
$idx  = 2;
$lfcr = chr(10);
$chk_order_list = [];
$work_sheet = $objPHPExcel->setActiveSheetIndex(0);
$logistics_delivery_query = mysqli_query($my_db, $logistics_delivery_sql);
while($logistics_delivery = mysqli_fetch_assoc($logistics_delivery_query))
{

    if(!isset($chk_order_list[$logistics_delivery['order_number']]))
    {
        $ord_idx = $idx+$logistics_delivery['ord_cnt']-1;
        $work_sheet->mergeCells("A{$idx}:A{$ord_idx}");
        $work_sheet->mergeCells("B{$idx}:B{$ord_idx}");
        $work_sheet->mergeCells("C{$idx}:C{$ord_idx}");
        $work_sheet->mergeCells("D{$idx}:D{$ord_idx}");
        $work_sheet->mergeCells("E{$idx}:E{$ord_idx}");
        $work_sheet->mergeCells("F{$idx}:F{$ord_idx}");
        $work_sheet->mergeCells("I{$idx}:I{$ord_idx}");
        $delivery_text  = "";

        $delivery_sql   = "SELECT delivery_no, delivery_type FROM work_cms_delivery WHERE order_number='{$logistics_delivery['order_number']}' GROUP BY delivery_no";
        $delivery_query = mysqli_query($my_db, $delivery_sql);
        while($delivery = mysqli_fetch_assoc($delivery_query))
        {
            $delivery_text .= empty($delivery_text) ? "{$delivery["delivery_type"]}: {$delivery['delivery_no']}" : $lfcr."{$delivery["delivery_type"]}: {$delivery['delivery_no']}";
        }

        $work_sheet
            ->setCellValue("A{$idx}", $logistics_delivery['order_date'])
            ->setCellValueExplicit("B{$idx}", $logistics_delivery['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("C{$idx}", $logistics_delivery['recipient'])
            ->setCellValue("D{$idx}", $logistics_delivery['recipient_hp'])
            ->setCellValue("E{$idx}", $logistics_delivery['recipient_addr'])
            ->setCellValue("F{$idx}", $logistics_delivery['stock_date'])
            ->setCellValueExplicit("I{$idx}", $delivery_text, PHPExcel_Cell_DataType::TYPE_STRING);

        $chk_order_list[$logistics_delivery['order_number']] = 1;
    }

    $work_sheet
        ->setCellValue("G{$idx}", $logistics_delivery['prd_name'])
        ->setCellValue("H{$idx}", $logistics_delivery['quantity'])
    ;

    $idx++;
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC0C0C0');

$objPHPExcel->getActiveSheet()->getStyle("A1:I{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:I{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("B2:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("A2:A{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);

$objPHPExcel->getActiveSheet()->setTitle("물류관리시스템 운송정보");


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_물류관리시스템 운송정보.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
