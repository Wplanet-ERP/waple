<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/approval.php');
require('inc/model/MyQuick.php');
require('inc/model/Approval.php');
require('inc/model/Kind.php');
require('inc/model/Team.php');
require('inc/model/Calendar.php');
require('inc/model/Leave.php');

# 프로세스 처리
$process            = isset($_POST['process']) ? $_POST['process'] : "";
$form_model         = Approval::Factory();
$report_model       = Approval::Factory();
$report_model->setReportTable();
$leave_model        = Leave::Factory();
$calendar_model     = Leave::Factory();
$calendar_model->setCalendarTable();

if($process == 'add_approval_report')
{
    $k_name_code = isset($_POST['k_name_code']) ? $_POST['k_name_code'] : "";
    $af_no       = isset($_POST['af_no']) ? $_POST['af_no'] : "";
    $is_approve  = isset($_POST['is_approve']) ? $_POST['is_approve'] : "";
    $ar_title    = isset($_POST['ar_title']) ? trim(addslashes($_POST['ar_title'])) : "";
    $ar_content  = isset($_POST['ar_content']) ? trim(addslashes($_POST['ar_content'])) : "";
    $regdatetime = date("Y-m-d H:i:s");
    $regdate     = date("Y-m-d");

    # 필수정보 체크 및 저장
    if(empty($k_name_code) || empty($af_no) || empty($ar_content))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');history.back();</script>");
    }

    # 출금 데이터 지정
    $approval_item      = $form_model->getItem($af_no);
    $is_withdraw        = $approval_item['is_withdraw'];
    $is_calendar        = $approval_item['is_calendar'];
    $wd_no              = isset($_POST['wd_no']) ? $_POST['wd_no'] : "";
    $wd_c_no            = isset($_POST['wd_c_no']) ? $_POST['wd_c_no'] : "";
    $wd_c_name          = isset($_POST['wd_c_name']) ? $_POST['wd_c_name'] : "";
    $wd_manager         = isset($_POST['wd_manager']) ? $_POST['wd_manager'] : "";
    $wd_team            = isset($_POST['wd_team']) ? $_POST['wd_team'] : "";
    $wd_title           = isset($_POST['wd_title']) ? $_POST['wd_title'] : "";
    $wd_account         = isset($_POST['wd_account']) ? $_POST['wd_account'] : "";
    $wd_num             = isset($_POST['wd_num']) ? $_POST['wd_num'] : "";

    $wd_vat_tax_choice  = isset($_POST['wd_vat_tax_choice']) ? $_POST['wd_vat_tax_choice'] : "";
    $wd_cost            = isset($_POST['wd_cost']) ? str_replace(",","", trim($_POST['wd_cost'])) : "";
    $wd_supply_cost     = isset($_POST['wd_supply_cost']) ? str_replace(",","", trim($_POST['wd_supply_cost'])) : "";
    $wd_supply_cost_vat = isset($_POST['wd_supply_cost_vat']) ? str_replace(",","", trim($_POST['wd_supply_cost_vat'])) : "";
    $wd_biz_tax         = isset($_POST['wd_biz_tax']) ? str_replace(",","", trim($_POST['wd_biz_tax'])) : "";
    $wd_local_tax       = isset($_POST['wd_local_tax']) ? str_replace(",","", trim($_POST['wd_local_tax'])) : "";
    $is_wd_file         = isset($_POST['is_wd_file']) ? $_POST['is_wd_file'] : 2;

    #결재문서 ID 생성
    $doc_date   = date('Ymd');
    $doc_sql    = "SELECT REPLACE(doc_no, '{$doc_date}_A', '') as doc_idx FROM approval_report WHERE doc_no LIKE '{$doc_date}_A%' ORDER BY doc_no DESC LIMIT 1";
    $doc_query  = mysqli_query($my_db, $doc_sql);
    $doc_result = mysqli_fetch_assoc($doc_query);

    $last_doc_idx = (isset($doc_result['doc_idx']) && !empty($doc_result['doc_idx'])) ? (int)$doc_result['doc_idx'] : 0;
    $doc_no       = $doc_date."_A".sprintf('%04d', $last_doc_idx+1);

    $ins_approval_data     = array(
        "af_no"         => $af_no,
        "doc_no"        => $doc_no,
        "k_name_code"   => $k_name_code,
        "req_team"      => $session_team,
        "req_s_no"      => $session_s_no,
        "ar_title"      => $ar_title,
        "ar_content"    => $ar_content,
        "is_wd_file"    => $is_wd_file,
        "regdate"       => $regdatetime,
    );

    # linked 관련 데이터
    $linked_no    = isset($_POST['linked_no']) ? $_POST['linked_no'] : 0;
    $linked_table = isset($_POST['linked_table']) ? $_POST['linked_table'] : "";

    if(!empty($wd_no) || !empty($linked_no) || !empty($linked_table)){
        $ins_approval_data['wd_no']         = $wd_no;
        $ins_approval_data['linked_no']     = $linked_no;
        $ins_approval_data['linked_table']  = $linked_table;
    }

    # 파일첨부
    $file_name = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_path = isset($_POST['file_path']) ? $_POST['file_path'] : "";

    $add_file_column = "";
    if(!empty($file_path))
    {
        $file_reads  = move_store_files($file_path, "dropzone_tmp","approval_report");
        $file_names  = implode(',', $file_name);
        $file_paths  = implode(',', $file_reads);

        $ins_approval_data['file_path'] = $file_paths;
        $ins_approval_data['file_name'] = $file_names;
    }

    # 결재선 저장
    $add_permission_list             = [];
    $approval_permission_type_list   = isset($_POST['approval_permission_type']) ? $_POST['approval_permission_type'] : "";
    $approval_priority_list          = isset($_POST['approval_priority']) ? $_POST['approval_priority'] : "";
    $approval_permission_team_list   = isset($_POST['approval_permission_team']) ? $_POST['approval_permission_team'] : "";
    $approval_permission_staff_list  = isset($_POST['approval_permission_staff']) ? $_POST['approval_permission_staff'] : "";

    # 상신시 데이터 처리
    $all_confirm        = false;
    $calendar_lc_state  = 1;
    if($is_approve == '1')
    {
        $ins_approval_data['ar_state'] = 2;
        $ins_approval_data['req_date'] = $regdatetime;

        if(!empty($linked_no) && $linked_table == 'work')
        {
            $all_confirm = $report_model->approvalAllConfirm($linked_no, $wd_cost);
            if($all_confirm) {
                $ins_approval_data['ar_state'] = "3";
                $ins_approval_data['req_date'] = $regdatetime;
                $ins_approval_data['run_date'] = $regdatetime;
            }
        }
        elseif(empty($approval_permission_type_list)){
            $ins_approval_data['ar_state']  = "3";
            $ins_approval_data['req_date']  = $regdatetime;
            $ins_approval_data['run_date']  = $regdatetime;
            $calendar_lc_state              = "2";
        }
    }

    $approval_state     = ($all_confirm) ? 2 : 1;
    if(!empty($approval_permission_type_list))
    {
        foreach($approval_permission_type_list as $key => $approval_permission_type)
        {
            $appr_team  = isset($approval_permission_team_list[$key]) ? $approval_permission_team_list[$key] : "";
            $appr_staff = isset($approval_permission_staff_list[$key]) ? $approval_permission_staff_list[$key] : "";

            if(!empty($appr_team) && !empty($appr_staff))
            {
                $add_permission_list[] = array(
                    'type'      => $approval_permission_type,
                    'priority'  => isset($approval_priority_list[$key]) ? $approval_priority_list[$key] : 2,
                    'team'      => $appr_team,
                    'staff'     => $appr_staff,
                    'state'     => $approval_state,
                );
            }
        }
    }

    $read_permission_team_list   = isset($_POST['reading_permission_team']) ? $_POST['reading_permission_team'] : "";
    $read_permission_staff_list  = isset($_POST['reading_permission_staff']) ? $_POST['reading_permission_staff'] : "";
    if(!empty($read_permission_team_list))
    {
        foreach($read_permission_team_list as $key => $read_permission_team)
        {
            $read_team  = !empty($read_permission_team) ? $read_permission_team : 0;
            $read_staff = isset($read_permission_staff_list[$key]) && !empty($read_permission_staff_list[$key]) ? $read_permission_staff_list[$key] : 0;
            $add_permission_list[] = array(
                'type'      => "reading",
                'priority'  => ($key+1),
                'team'      => $read_team,
                'staff'     => $read_staff,
                'state'     => 3
            );
        }
    }

    $ref_permission_team_list   = isset($_POST['reference_permission_team']) ? $_POST['reference_permission_team'] : "";
    $ref_permission_staff_list  = isset($_POST['reference_permission_staff']) ? $_POST['reference_permission_staff'] : "";
    if(!empty($ref_permission_team_list))
    {
        foreach($ref_permission_team_list as $key => $ref_team)
        {
            if(!empty($ref_team))
            {
                $ref_staff = isset($ref_permission_staff_list[$key]) && !empty($ref_permission_staff_list[$key]) ? $ref_permission_staff_list[$key] : 0;
                $add_permission_list[] = array(
                    'type'      => "reference",
                    'priority'  => ($key+1),
                    'team'      => $ref_team,
                    'staff'     => $ref_staff,
                    'state'     => 3
                );
            }
        }
    }

    if($is_approve == '1') {
        $msg = "결재보고서 상신했습니다.";
        $error_msg = "결재보고서 상신에 실패했습니다.";
    }else{
        $msg = "결재상태 = 작성중으로 저장 되었습니다.";
        $error_msg = "결재보고서 저장에 실패했습니다.";
    }

    if($report_model->insert($ins_approval_data))
    {
        $new_ar_no   = $report_model->getInsertId();
        if($is_approve == '1'){
            $ins_me_sql = "INSERT INTO approval_report_permission(`ar_no`, `arp_type`, `arp_team`, `arp_s_no`, `arp_state`, `arp_date`, `priority`) VALUES ('{$new_ar_no}', 'approval', '{$session_team}', '{$session_s_no}', '0', now(), '1')";
        }else{
            $ins_me_sql = "INSERT INTO approval_report_permission(`ar_no`, `arp_type`, `arp_team`, `arp_s_no`, `arp_state`, `priority`) VALUES ('{$new_ar_no}', 'approval', '{$session_team}', '{$session_s_no}', '0', '1')";
        }
        mysqli_query($my_db, $ins_me_sql);

        if (!empty($add_permission_list))
        {
            if($all_confirm){
                $ins_per_sql = "INSERT INTO approval_report_permission(`ar_no`, `arp_type`, `arp_team`, `arp_s_no`, `arp_state`, `arp_date`, `priority`) VALUES";
                $comma       = "";
                foreach($add_permission_list as $add_permission)
                {
                    $ins_per_sql .= $comma."('{$new_ar_no}', '{$add_permission['type']}', '{$add_permission['team']}', '{$add_permission['staff']}', '{$add_permission['state']}', now(), '{$add_permission['priority']}')";
                    $comma = " , ";
                }
            }else{
                $ins_per_sql = "INSERT INTO approval_report_permission(`ar_no`, `arp_type`, `arp_team`, `arp_s_no`, `arp_state`, `priority`) VALUES";
                $comma       = "";
                foreach($add_permission_list as $add_permission)
                {
                    $ins_per_sql .= $comma."('{$new_ar_no}', '{$add_permission['type']}', '{$add_permission['team']}', '{$add_permission['staff']}', '{$add_permission['state']}', '{$add_permission['priority']}')";
                    $comma = " , ";
                }
            }

            mysqli_query($my_db, $ins_per_sql);
        }

        # 휴가 저장
        if($is_calendar == "1")
        {
            $holiday_list       = $leave_model->getHolidayList();
            $leave_type_list	= getLeaveTypeList();
            $leave_no           = isset($_POST['leave_no']) ? $_POST['leave_no'] : "";
            $leave_type         = isset($_POST['leave_type']) ? $_POST['leave_type'] : "";
            $leave_type_name    = $leave_type_list[$leave_type]['name'];
            $leave_s_date       = isset($_POST['leave_s_date']) ? $_POST['leave_s_date'] : "";
            $leave_e_date       = isset($_POST['leave_e_date']) ? $_POST['leave_e_date'] : "";
            $leave_s_day        = date("m/d", strtotime($leave_s_date));
            $leave_e_day        = date("m/d", strtotime($leave_e_date));
            $leave_value        = 0.5;
            $cal_ar_title       = "{$session_name} ";

            if($leave_type == "2" || $leave_type == "4"){
                $leave_s_datetime   = $leave_s_date." 00:00:00";
                $leave_e_datetime   = $leave_s_date." 11:59:59";
                $cal_ar_title      .= "({$leave_s_day}";
            }
            elseif($leave_type == "3" || $leave_type == "5"){
                $leave_s_datetime   = $leave_s_date." 12:00:00";
                $leave_e_datetime   = $leave_s_date." 23:59:59";
                $cal_ar_title      .= "({$leave_s_day}";
            }
            else{
                $leave_s_datetime   = $leave_s_date." 00:00:00";
                $leave_e_datetime   = $leave_e_date." 23:59:59";
                $chk_s_datetime     = new DateTime($leave_s_date);
                $chk_e_datetime     = new DateTime($leave_e_date);
                $leave_diff         = $chk_s_datetime->diff($chk_e_datetime);
                $leave_diff_day     = $leave_diff->format("%a");
                $leave_day          = (int)$leave_diff_day+1;
                $leave_value        = 0;
                $cal_ar_title      .= ($leave_s_day == $leave_e_day) ? "({$leave_s_day}" : "({$leave_s_day}~{$leave_e_day}";

                for($idx=0; $idx < $leave_day; $idx++){
                    $chk_date       = date("Ymd", strtotime("{$leave_s_date} +{$idx} days"));
                    $chk_day_type   = date("w", strtotime($chk_date));

                    if(isset($holiday_list[$chk_date]) || ($chk_day_type == 0 || $chk_day_type == 6)){
                        continue;
                    }

                    $leave_value++;
                }
            }
            $cal_ar_title .= " ,{$leave_type_name}, {$leave_value}일)";
            $report_model->update(array("ar_no" => $new_ar_no, "ar_title" => $cal_ar_title));

            $ins_leave_sql = "
                INSERT INTO approval_report_leave SET 
                    `ar_no`         = '{$new_ar_no}',
                    `s_no`          = '{$session_s_no}',
                    `leave_no`      = '{$leave_no}',
                    `leave_type`    = '{$leave_type}',
                    `leave_s_date`  = '{$leave_s_datetime}',
                    `leave_e_date`  = '{$leave_e_datetime}',
                    `leave_value`   = '{$leave_value}'
            ";
            mysqli_query($my_db, $ins_leave_sql);

            if($is_approve == '1')
            {
                $leave_item         = $leave_model->getItem($leave_no);
                $leave_base_item    = $leave_model->getDefaultLeaveData($leave_no, $session_s_no);
                $lsm_no             = !empty($leave_item) ? $leave_base_item['lsm_no'] : 0;

                $calendar_data      = array(
                    "ar_no"         => $new_ar_no,
                    "lsm_no"        => $lsm_no,
                    "lc_state"      => $calendar_lc_state,
                    "lc_title"      => $cal_ar_title,
                    "lc_s_no"       => $session_s_no,
                    "leave_type"    => $leave_type,
                    "leave_value"   => $leave_value,
                    "leave_s_date"  => $leave_s_datetime,
                    "leave_e_date"  => $leave_e_datetime,
                    "regdate"       => $regdatetime
                );
                $calendar_model->insert($calendar_data);

                $new_lc_no = $calendar_model->getInsertId();
                $report_model->update(array("ar_no" => $new_ar_no, "lc_no" => $new_lc_no));

                if($lsm_no > 0){
                    $leave_model->useLeaveDay($lsm_no, $leave_value);
                }
            }
        }

        # 출금 저장
        if($is_withdraw == "1")
        {
            if(!empty($wd_no))
            {
                $wd_data_sql = "
                    UPDATE withdraw SET
                        c_no        = '{$wd_c_no}', 
                        c_name      = '{$wd_c_name}', 
                        s_no        = '{$wd_manager}', 
                        team        = '{$wd_team}',
                        wd_subject  = '{$ar_title}',
                        bk_title    = '{$wd_title}',
                        bk_name     = '{$wd_account}',
                        bk_num      = '{$wd_num}',
                        vat_choice  = '{$wd_vat_tax_choice}',
                        cost             = '{$wd_cost}',
                        supply_cost      = '{$wd_supply_cost}',
                        supply_cost_vat  = '{$wd_supply_cost_vat}',
                        biz_tax          = '{$wd_biz_tax}',
                        local_tax        = '{$wd_local_tax}',
                        is_wd_file       = '{$is_wd_file}'
                    WHERE wd_no='{$wd_no}'
                ";
            }
            elseif($is_approve == '1' && empty($wd_no))
            {
                $wd_data_sql = "
                    INSERT INTO `withdraw` SET 
                        my_c_no     = '1',
                        c_no        = '{$wd_c_no}', 
                        c_name      = '{$wd_c_name}', 
                        s_no        = '{$wd_manager}', 
                        team        = '{$wd_team}',
                        w_no        = '{$linked_no}',
                        wd_subject  = '{$ar_title}',
                        wd_state    = '2',
                        bk_title    = '{$wd_title}',
                        bk_name     = '{$wd_account}',
                        bk_num      = '{$wd_num}',
                        vat_choice  = '{$wd_vat_tax_choice}',
                        cost             = '{$wd_cost}',
                        supply_cost      = '{$wd_supply_cost}',
                        supply_cost_vat  = '{$wd_supply_cost_vat}',
                        biz_tax          = '{$wd_biz_tax}',
                        local_tax        = '{$wd_local_tax}',
                        reg_s_no         = '{$session_s_no}',
                        reg_team         = '{$session_team}',
                        is_wd_file       = '{$is_wd_file}',
                        regdate          = now()
                ";
            }
            else
            {
                $wd_data_sql = "
                    INSERT INTO `approval_report_withdraw` SET 
                        ar_no       = '{$new_ar_no}',
                        wd_c_no     = '{$wd_c_no}', 
                        wd_c_name   = '{$wd_c_name}', 
                        wd_manager  = '{$wd_manager}', 
                        wd_team     = '{$wd_team}',
                        wd_title    = '{$wd_title}',
                        wd_account  = '{$wd_account}',
                        wd_num      = '{$wd_num}',
                        wd_vat_tax_choice   = '{$wd_vat_tax_choice}',
                        wd_cost             = '{$wd_cost}',
                        wd_supply_cost      = '{$wd_supply_cost}',
                        wd_supply_cost_vat  = '{$wd_supply_cost_vat}',
                        wd_biz_tax          = '{$wd_biz_tax}',
                        wd_local_tax        = '{$wd_local_tax}'
                ";
            }

            mysqli_query($my_db, $wd_data_sql);

            if($is_approve == '1')
            {
                $wd_no_val = $wd_no;
                if(empty($wd_no))
                {
                    $new_wd_no  = mysqli_insert_id($my_db);
                    $wd_no_val  = $new_wd_no;
                    $upd_wd_sql = "UPDATE `approval_report` SET wd_no='{$new_wd_no}' WHERE ar_no='{$new_ar_no}'";
                    mysqli_query($my_db, $upd_wd_sql);
                }

                if($linked_table == 'work' && $linked_no > 0){
                    $upd_work_sql = "UPDATE `work` SET wd_no='{$wd_no_val}' WHERE w_no='{$linked_no}'";
                    mysqli_query($my_db, $upd_work_sql);
                }

                exit("<script>alert('{$msg}');location.href='approval_report_view.php?ar_no={$new_ar_no}'</script>");
            }
        }

        exit("<script>alert('{$msg}');location.href='approval_report_list.php'</script>");
    }else{

        exit("<script>alert('{$error_msg}');history.back();</script>");
    }
}
elseif($process == 'upd_approval_report')
{
    $ar_no       = isset($_POST['ar_no']) ? $_POST['ar_no'] : "";
    $k_name_code = isset($_POST['k_name_code']) ? $_POST['k_name_code'] : "";
    $af_no       = isset($_POST['af_no']) ? $_POST['af_no'] : "";
    $is_approve  = isset($_POST['is_approve']) ? $_POST['is_approve'] : "";
    $ar_title    = isset($_POST['ar_title']) ? trim(addslashes($_POST['ar_title'])) : "";
    $ar_content  = isset($_POST['ar_content']) ? trim(addslashes($_POST['ar_content'])) : "";
    $regdatetime = date("Y-m-d H:i:s");
    $regdate     = date("Y-m-d");

    # 필수정보 체크 및 저장
    if(empty($ar_no) || empty($k_name_code) || empty($af_no) || empty($ar_title) || empty($ar_content))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');history.back();</script>");
    }

    # 출금 데이터 지정
    $approval_item      = $form_model->getItem($af_no);
    $is_withdraw        = $approval_item['is_withdraw'];
    $is_calendar        = $approval_item['is_calendar'];
    $arw_no             = isset($_POST['arw_no']) ? $_POST['arw_no'] : "";
    $wd_c_no            = isset($_POST['wd_c_no']) ? $_POST['wd_c_no'] : "";
    $wd_c_name          = isset($_POST['wd_c_name']) ? $_POST['wd_c_name'] : "";
    $wd_manager         = isset($_POST['wd_manager']) ? $_POST['wd_manager'] : "";
    $wd_team            = isset($_POST['wd_team']) ? $_POST['wd_team'] : "";
    $wd_title           = isset($_POST['wd_title']) ? $_POST['wd_title'] : "";
    $wd_account         = isset($_POST['wd_account']) ? $_POST['wd_account'] : "";
    $wd_num             = isset($_POST['wd_num']) ? $_POST['wd_num'] : "";
    $is_wd_file         = isset($_POST['is_wd_file']) ? $_POST['is_wd_file'] : 2;

    $wd_vat_tax_choice  = isset($_POST['wd_vat_tax_choice']) ? $_POST['wd_vat_tax_choice'] : "";
    $wd_cost            = isset($_POST['wd_cost']) ? str_replace(",","", trim($_POST['wd_cost'])) : "";
    $wd_supply_cost     = isset($_POST['wd_supply_cost']) ? str_replace(",","", trim($_POST['wd_supply_cost'])) : "";
    $wd_supply_cost_vat = isset($_POST['wd_supply_cost_vat']) ? str_replace(",","", trim($_POST['wd_supply_cost_vat'])) : "";
    $wd_biz_tax         = isset($_POST['wd_biz_tax']) ? str_replace(",","", trim($_POST['wd_biz_tax'])) : "";
    $wd_local_tax       = isset($_POST['wd_local_tax']) ? str_replace(",","", trim($_POST['wd_local_tax'])) : "";

    $upd_approval_data  = array(
        "ar_no"         => $ar_no,
        "k_name_code"   => $k_name_code,
        "ar_title"      => $ar_title,
        "ar_content"    => $ar_content,
        "is_wd_file"    => $is_wd_file,
    );

    # linked 관련 데이터
    $linked_no    = isset($_POST['linked_no']) ? $_POST['linked_no'] : "";
    $linked_table = isset($_POST['linked_table']) ? $_POST['linked_table'] : "";

    if(!empty($wd_no) || !empty($linked_no) || !empty($linked_table)){
        $upd_approval_data['wd_no']         = $wd_no;
        $upd_approval_data['linked_no']     = $linked_no;
        $upd_approval_data['linked_table']  = $linked_table;
    }

    # 파일첨부 처리
    $file_origin_read = isset($_POST['file_origin_read']) ? $_POST['file_origin_read'] : "";
    $file_origin_name = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_path        = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_name        = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk         = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    $upd_file_column = "";
    if(!empty($file_path))
    {
        $file_reads  = move_store_files($file_path, "dropzone_tmp","approval_report");
        $file_names  = implode(',', $file_name);
        $file_paths  = implode(',', $file_reads);

        $upd_approval_data['file_path'] = $file_paths;
        $upd_approval_data['file_name'] = $file_names;
    }

    if(!empty($file_origin_read) && !empty($file_origin_name))
    {
        if (!empty($file_path) && !empty($file_name)) {
            $del_images = array_diff(explode(',', $file_origin_read), $file_path);
        } else {
            $del_images = explode(',', $file_origin_read);
        }
    }

    # 권한 처리
    $add_permission_list = [];
    $upd_permission_list = [];
    $del_permission_list = [];
    $upd_apply_list      = [];

    $origin_approval_arp_no          = isset($_POST['origin_approval_arp_no']) && !empty($_POST['origin_approval_arp_no']) ? explode(",", $_POST['origin_approval_arp_no']) : [];
    $approval_permission_arp_no_list = isset($_POST['approval_arp_no']) ? $_POST['approval_arp_no'] : "";
    $approval_permission_type_list   = isset($_POST['approval_permission_type']) ? $_POST['approval_permission_type'] : "";
    $approval_priority_list          = isset($_POST['approval_priority']) ? $_POST['approval_priority'] : "";
    $approval_permission_team_list   = isset($_POST['approval_permission_team']) ? $_POST['approval_permission_team'] : "";
    $approval_permission_staff_list  = isset($_POST['approval_permission_staff']) ? $_POST['approval_permission_staff'] : "";

    # 상신시 데이터 처리
    $upd_appr_column    = "";
    $all_confirm        = false;
    $calendar_lc_state  = "1";
    if($is_approve == '1')
    {
        $upd_approval_data['ar_state'] = 2;
        $upd_approval_data['req_date'] = $regdatetime;

        if(!empty($linked_no) && $linked_table == 'work')
        {
            $all_confirm = $report_model->approvalAllConfirm($linked_no, $wd_cost);
            if($all_confirm) {
                $upd_approval_data['ar_state'] = 2;
                $upd_approval_data['req_date'] = $regdatetime;
                $upd_approval_data['run_date'] = $regdatetime;
            }
        }
        elseif(empty($approval_permission_type_list)){
            $upd_approval_data['ar_state'] = "3";
            $upd_approval_data['req_date'] = $regdatetime;
            $upd_approval_data['run_date'] = $regdatetime;
            $calendar_lc_state             = "2";
        }
    }

    $approval_state = ($all_confirm) ? 2 : 1;
    if(!empty($approval_permission_type_list))
    {
        foreach($approval_permission_type_list as $key => $appr_type)
        {
            $appr_no        = isset($approval_permission_arp_no_list[$key]) ? $approval_permission_arp_no_list[$key] : "";
            $appr_team      = isset($approval_permission_team_list[$key]) ? $approval_permission_team_list[$key] : "";
            $appr_staff     = isset($approval_permission_staff_list[$key]) ? $approval_permission_staff_list[$key] : "";
            $appr_priority  = isset($approval_priority_list[$key]) ? $approval_priority_list[$key] : 2;

            if(!empty($appr_team) && !empty($appr_staff))
            {
                if(!empty($appr_no))
                {
                    $upd_apply_list[] = $appr_no;
                    $upd_permission_list[] = array(
                        'arp_no'    => $appr_no,
                        'type'      => $appr_type,
                        'priority'  => $appr_priority,
                        'team'      => $appr_team,
                        'staff'     => $appr_staff,
                        'state'     => $approval_state,
                    );
                }else{
                    $add_permission_list[] = array(
                        'type'      => $appr_type,
                        'priority'  => $appr_priority,
                        'team'      => $appr_team,
                        'staff'     => $appr_staff,
                        'state'     => $approval_state,
                    );
                }
            }
        }
    }

    if(!empty($origin_approval_arp_no))
    {
        foreach($origin_approval_arp_no as $approval_arp_no)
        {
            if(in_array($approval_arp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('arp_no' => $approval_arp_no);
        }
    }

    # 열람자 처리
    $upd_apply_list              = [];
    $origin_reading_arp_no       = isset($_POST['origin_reading_arp_no']) && !empty($_POST['origin_reading_arp_no']) ? explode(",", $_POST['origin_reading_arp_no']) : [];
    $read_permission_arp_no_list = isset($_POST['reading_arp_no']) ? $_POST['reading_arp_no'] : "";
    $read_permission_team_list   = isset($_POST['reading_permission_team']) ? $_POST['reading_permission_team'] : "";
    $read_permission_staff_list  = isset($_POST['reading_permission_staff']) ? $_POST['reading_permission_staff'] : "";
    if(!empty($read_permission_team_list))
    {
        foreach($read_permission_team_list as $key => $read_permission_team)
        {
            $read_no    = isset($read_permission_arp_no_list[$key]) ? $read_permission_arp_no_list[$key] : "";
            $read_team  = !empty($read_permission_team) ? $read_permission_team : 0;
            $read_staff = isset($read_permission_staff_list[$key]) && !empty($read_permission_staff_list[$key]) ? $read_permission_staff_list[$key] : 0;

            if(!empty($read_no))
            {
                $upd_apply_list[] = $read_no;
                $upd_permission_list[] = array(
                    'arp_no'    => $read_no,
                    'type'      => "reading",
                    'priority'  => ($key+1),
                    'team'      => $read_team,
                    'staff'     => $read_staff,
                    'state'     => 3
                );
            }else{
                $add_permission_list[] = array(
                    'type'      => "reading",
                    'priority'  => ($key+1),
                    'team'      => $read_team,
                    'staff'     => $read_staff,
                    'state'     => 3
                );
            }
        }
    }

    if(!empty($origin_reading_arp_no))
    {
        foreach($origin_reading_arp_no as $reading_arp_no)
        {
            if(in_array($reading_arp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('arp_no' => $reading_arp_no);
        }
    }

    # 참조자 처리
    $upd_apply_list              = [];
    $origin_reference_arp_no    = isset($_POST['origin_reference_arp_no']) && !empty($_POST['origin_reference_arp_no']) ? explode(",", $_POST['origin_reference_arp_no']) : [];
    $ref_permission_arp_no_list = isset($_POST['reference_arp_no']) ? $_POST['reference_arp_no'] : "";
    $ref_permission_team_list   = isset($_POST['reference_permission_team']) ? $_POST['reference_permission_team'] : "";
    $ref_permission_staff_list  = isset($_POST['reference_permission_staff']) ? $_POST['reference_permission_staff'] : "";
    if(!empty($ref_permission_team_list))
    {
        foreach($ref_permission_team_list as $key => $ref_team)
        {
            if(!empty($ref_team))
            {
                $ref_no    = isset($ref_permission_arp_no_list[$key]) ? $ref_permission_arp_no_list[$key] : "";
                $ref_staff = isset($ref_permission_staff_list[$key]) && !empty($ref_permission_staff_list[$key]) ? $ref_permission_staff_list[$key] : 0;

                if(!empty($ref_no))
                {
                    $upd_apply_list[] = $ref_no;
                    $upd_permission_list[] = array(
                        'arp_no'    => $ref_no,
                        'type'      => "reference",
                        'priority'  => ($key+1),
                        'team'      => $ref_team,
                        'staff'     => $ref_staff,
                        'state'     => 3
                    );
                }else{
                    $add_permission_list[] = array(
                        'type'      => "reference",
                        'priority'  => ($key+1),
                        'team'      => $ref_team,
                        'staff'     => $ref_staff,
                        'state'     => 3
                    );
                }

            }
        }
    }

    if(!empty($origin_reference_arp_no))
    {
        foreach($origin_reference_arp_no as $reference_arp_no)
        {
            if(in_array($reference_arp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('arp_no' => $reference_arp_no);
        }
    }

    if($is_approve == '1') {
        $msg       = "결재보고서 상신했습니다.";
        $error_msg = "결재보고서 상신에 실패했습니다.";
        $move_url  = "approval_report_view.php?ar_no={$ar_no}";
    }else{
        $msg       = "결재상태 = 작성중으로 수정 되었습니다.";
        $error_msg = "결재보고서 수정에 실패했습니다.";
        $move_url  = "approval_report_list.php";
    }

    if($report_model->update($upd_approval_data))
    {
        # 권한 저장
        if(!empty($add_permission_list))
        {
            if($all_confirm) {
                $ins_per_sql = "INSERT INTO approval_report_permission(`ar_no`, `arp_type`, `arp_team`, `arp_s_no`, `arp_state`, `arp_date`, `priority`) VALUES";
                $comma = "";
                foreach($add_permission_list as $add_permission)
                {
                    $ins_per_sql .= $comma."('{$ar_no}', '{$add_permission['type']}', '{$add_permission['team']}', '{$add_permission['staff']}', '{$add_permission['state']}', now(), '{$add_permission['priority']}')";
                    $comma = " , ";
                }
            }else{
                $ins_per_sql = "INSERT INTO approval_report_permission(`ar_no`, `arp_type`, `arp_team`, `arp_s_no`, `arp_state`, `priority`) VALUES";
                $comma = "";
                foreach($add_permission_list as $add_permission)
                {
                    $ins_per_sql .= $comma."('{$ar_no}', '{$add_permission['type']}', '{$add_permission['team']}', '{$add_permission['staff']}', '{$add_permission['state']}', '{$add_permission['priority']}')";
                    $comma = " , ";
                }
            }

            mysqli_query($my_db, $ins_per_sql);
        }

        if(!empty($upd_permission_list))
        {
            foreach($upd_permission_list as $upd_permission)
            {
                $upd_per_sql = "UPDATE approval_report_permission SET arp_team = '{$upd_permission['team']}', arp_type='{$upd_permission['type']}', arp_s_no = '{$upd_permission['staff']}', arp_state='{$upd_permission['state']}', priority='{$upd_permission['priority']}' WHERE arp_no='{$upd_permission['arp_no']}'";
                if($all_confirm) {
                    $upd_per_sql = "UPDATE approval_report_permission SET arp_team = '{$upd_permission['team']}', arp_type='{$upd_permission['type']}', arp_s_no = '{$upd_permission['staff']}', arp_state='{$upd_permission['state']}', arp_date=now(), priority='{$upd_permission['priority']}' WHERE arp_no='{$upd_permission['arp_no']}'";
                }
                mysqli_query($my_db, $upd_per_sql);
            }
        }

        if(!empty($del_permission_list))
        {
            foreach($del_permission_list as $del_permission)
            {
                $del_per_sql = "DELETE FROM approval_report_permission WHERE arp_no='{$del_permission['arp_no']}'";
                mysqli_query($my_db, $del_per_sql);
            }
        }

        if($is_approve == '1')
        {
            $upd_me_sql = "UPDATE approval_report_permission SET arp_date=now() WHERE ar_no='{$ar_no}' AND `arp_state`='0' AND arp_s_no='{$session_s_no}' AND arp_type='approval'";
            mysqli_query($my_db, $upd_me_sql);
        }

        # 휴가 저장
        if($is_calendar == "1")
        {
            $holiday_list       = $leave_model->getHolidayList();
            $leave_type_list	= getLeaveTypeList();
            $regdate            = date("Y-m-d H:i:s");
            $arl_no             = isset($_POST['arl_no']) ? $_POST['arl_no'] : "";
            $leave_no           = isset($_POST['leave_no']) ? $_POST['leave_no'] : "";
            $leave_type         = isset($_POST['leave_type']) ? $_POST['leave_type'] : "";
            $leave_type_name    = $leave_type_list[$leave_type]['name'];
            $leave_s_date       = isset($_POST['leave_s_date']) ? $_POST['leave_s_date'] : "";
            $leave_e_date       = isset($_POST['leave_e_date']) ? $_POST['leave_e_date'] : "";
            $leave_s_day        = date("m/d", strtotime($leave_s_date));
            $leave_e_day        = date("m/d", strtotime($leave_e_date));
            $leave_value        = 0.5;
            $cal_ar_title       = "{$session_name} ";

            if($leave_type == "2" || $leave_type == "4"){
                $leave_s_datetime   = $leave_s_date." 00:00:00";
                $leave_e_datetime   = $leave_s_date." 11:59:59";
                $cal_ar_title      .= "({$leave_s_day}";
            }
            elseif($leave_type == "3" || $leave_type == "5"){
                $leave_s_datetime   = $leave_s_date." 12:00:00";
                $leave_e_datetime   = $leave_s_date." 23:59:59";
                $cal_ar_title      .= "({$leave_s_day}";
            }
            else{
                $leave_s_datetime   = $leave_s_date." 00:00:00";
                $leave_e_datetime   = $leave_e_date." 23:59:59";
                $chk_s_datetime     = new DateTime($leave_s_date);
                $chk_e_datetime     = new DateTime($leave_e_date);
                $leave_diff         = $chk_s_datetime->diff($chk_e_datetime);
                $leave_diff_day     = $leave_diff->format("%a");
                $leave_day          = (int)$leave_diff_day+1;
                $leave_value        = 0;
                $cal_ar_title      .= ($leave_s_day == $leave_e_day) ? "({$leave_s_day}" : "({$leave_s_day}~{$leave_e_day}";

                for($idx=0; $idx < $leave_day; $idx++){
                    $chk_date       = date("Ymd", strtotime("{$leave_s_date} +{$idx} days"));
                    $chk_day_type   = date("w", strtotime($chk_date));

                    if(isset($holiday_list[$chk_date]) || ($chk_day_type == 0 || $chk_day_type == 6)){
                        continue;
                    }

                    $leave_value++;
                }
            }
            $cal_ar_title .= " ,{$leave_type_name}, {$leave_value}일)";
            $report_model->update(array("ar_no" => $ar_no, "ar_title" => $cal_ar_title));

            $upd_leave_sql = "
                UPDATE approval_report_leave SET 
                    `leave_no`      = '{$leave_no}',
                    `leave_type`    = '{$leave_type}',
                    `leave_s_date`  = '{$leave_s_datetime}',
                    `leave_e_date`  = '{$leave_e_datetime}',
                    `leave_value`   = '{$leave_value}'
                WHERE `arl_no` = '{$arl_no}'
            ";
            mysqli_query($my_db, $upd_leave_sql);

            if($is_approve == '1')
            {
                $leave_item         = $leave_model->getItem($leave_no);
                $leave_base_item    = $leave_model->getDefaultLeaveData($leave_no, $session_s_no);
                $lsm_no             = !empty($leave_item) ? $leave_base_item['lsm_no'] : 0;

                $calendar_data      = array(
                    "ar_no"         => $ar_no,
                    "lsm_no"        => $lsm_no,
                    "lc_state"      => $calendar_lc_state,
                    "lc_title"      => $cal_ar_title,
                    "lc_s_no"       => $session_s_no,
                    "leave_type"    => $leave_type,
                    "leave_value"   => $leave_value,
                    "leave_s_date"  => $leave_s_datetime,
                    "leave_e_date"  => $leave_e_datetime,
                    "regdate"       => $regdatetime
                );
                $calendar_model->insert($calendar_data);

                $new_lc_no = $calendar_model->getInsertId();
                $report_model->update(array("ar_no" => $ar_no, "lc_no" => $new_lc_no));

                if($lsm_no > 0){
                    $leave_model->useLeaveDay($lsm_no, $leave_value);
                }
            }
        }

        # 출금 저장
        if($is_withdraw == "1")
        {
            if(!empty($wd_no))
            {
                $wd_data_sql = "
                UPDATE withdraw SET
                    c_no        = '{$wd_c_no}', 
                    c_name      = '{$wd_c_name}', 
                    s_no        = '{$wd_manager}', 
                    team        = '{$wd_team}',
                    wd_subject  = '{$ar_title}',
                    bk_title    = '{$wd_title}',
                    bk_name     = '{$wd_account}',
                    bk_num      = '{$wd_num}',
                    vat_choice  = '{$wd_vat_tax_choice}',
                    cost             = '{$wd_cost}',
                    supply_cost      = '{$wd_supply_cost}',
                    supply_cost_vat  = '{$wd_supply_cost_vat}',
                    biz_tax          = '{$wd_biz_tax}',
                    local_tax        = '{$wd_local_tax}',
                    is_wd_file       = '{$is_wd_file}'
                WHERE wd_no='{$wd_no}'
            ";
            }
            elseif($is_approve == '1' && empty($wd_no))
            {
                $wd_data_sql = "
                    INSERT INTO `withdraw` SET 
                        my_c_no     = '1',
                        c_no        = '{$wd_c_no}', 
                        c_name      = '{$wd_c_name}', 
                        s_no        = '{$wd_manager}', 
                        team        = '{$wd_team}',
                        w_no        = '{$linked_no}',
                        wd_subject  = '{$ar_title}',
                        wd_state    = '2',
                        bk_title    = '{$wd_title}',
                        bk_name     = '{$wd_account}',
                        bk_num      = '{$wd_num}',
                        vat_choice  = '{$wd_vat_tax_choice}',
                        cost             = '{$wd_cost}',
                        supply_cost      = '{$wd_supply_cost}',
                        supply_cost_vat  = '{$wd_supply_cost_vat}',
                        biz_tax          = '{$wd_biz_tax}',
                        local_tax        = '{$wd_local_tax}',
                        reg_s_no         = '{$session_s_no}',
                        reg_team         = '{$session_team}',
                        is_wd_file       = '{$is_wd_file}',
                        regdate          = now()
                ";
            }
            elseif(!empty($arw_no))
            {
                $wd_data_sql = "
                    UPDATE `approval_report_withdraw` SET 
                        wd_c_no     = '{$wd_c_no}', 
                        wd_c_name   = '{$wd_c_name}', 
                        wd_manager  = '{$wd_manager}', 
                        wd_team     = '{$wd_team}',
                        wd_title    = '{$wd_title}',
                        wd_account  = '{$wd_account}',
                        wd_num      = '{$wd_num}',
                        wd_vat_tax_choice   = '{$wd_vat_tax_choice}',
                        wd_cost             = '{$wd_cost}',
                        wd_supply_cost      = '{$wd_supply_cost}',
                        wd_supply_cost_vat  = '{$wd_supply_cost_vat}',
                        wd_biz_tax          = '{$wd_biz_tax}',
                        wd_local_tax        = '{$wd_local_tax}'
                    WHERE arw_no='{$arw_no}'
                ";
            }else{
                $wd_data_sql = "
                    INSERT INTO `approval_report_withdraw` SET 
                        ar_no       = '{$ar_no}',
                        wd_c_no     = '{$wd_c_no}', 
                        wd_c_name   = '{$wd_c_name}', 
                        wd_manager  = '{$wd_manager}', 
                        wd_team     = '{$wd_team}',
                        wd_title    = '{$wd_title}',
                        wd_account  = '{$wd_account}',
                        wd_num      = '{$wd_num}',
                        wd_vat_tax_choice   = '{$wd_vat_tax_choice}',
                        wd_cost             = '{$wd_cost}',
                        wd_supply_cost      = '{$wd_supply_cost}',
                        wd_supply_cost_vat  = '{$wd_supply_cost_vat}',
                        wd_biz_tax          = '{$wd_biz_tax}',
                        wd_local_tax        = '{$wd_local_tax}'
                ";
            }

            mysqli_query($my_db, $wd_data_sql);

            if($is_approve == '1')
            {
                $wd_no_val  = $wd_no;

                if(empty($wd_no))
                {
                    $new_wd_no  = mysqli_insert_id($my_db);
                    $wd_no_val  = $new_wd_no;
                    $upd_wd_sql = "UPDATE `approval_report` SET wd_no='{$new_wd_no}' WHERE ar_no='{$ar_no}'";
                    mysqli_query($my_db, $upd_wd_sql);
                }

                if($linked_table == 'work' && $linked_no > 0){
                    $upd_work_sql = "UPDATE `work` SET wd_no='{$wd_no_val}' WHERE w_no='{$linked_no}'";
                    mysqli_query($my_db, $upd_work_sql);
                }
            }
        }

        if(!empty($del_images)) {
            del_image($del_images);
        }

        exit("<script>alert('{$msg}');location.href='{$move_url}'</script>");
    }else{
        exit("<script>alert('{$msg}');history.back();</script>");
    }
}
elseif($process == 'del_approval_report')
{
    $ar_no = isset($_POST['ar_no']) ? $_POST['ar_no'] : "";
    $approval_sql    = "SELECT * FROM approval_report as ar WHERE ar.ar_no='{$ar_no}'";
    $approval_query  = mysqli_query($my_db, $approval_sql);
    $approval_result = mysqli_fetch_assoc($approval_query);

    $delete_sql = "DELETE FROM approval_report WHERE ar_no='{$ar_no}'";
    if(mysqli_query($my_db, $delete_sql))
    {
        if(isset($approval_result['wd_no']) && $approval_result['wd_no'] > 0)
        {
            $delete_withdraw_sql = "DELETE FROM withdraw WHERE wd_no='{$approval_result['wd_no']}'";
            mysqli_query($my_db, $delete_withdraw_sql);
        }

        $delete_permission_sql  = "DELETE FROM approval_report_permission WHERE ar_no='{$ar_no}'";
        mysqli_query($my_db, $delete_permission_sql);
        $delete_withdraw_sql    = "DELETE FROM approval_report_withdraw WHERE ar_no='{$ar_no}'";
        mysqli_query($my_db, $delete_withdraw_sql);
        $delete_leave_sql       = "DELETE FROM approval_report_leave WHERE ar_no='{$ar_no}'";
        mysqli_query($my_db, $delete_leave_sql);

        exit("<script>alert('삭제했습니다.');location.href='approval_report_list.php'</script>");
    }else{
        exit("<script>alert('삭제에 실패했습니다.');history.back();</script>");
    }
}

# 변수 리스트
$team_model             = Team::Factory();
$kind_model             = Kind::Factory();
$team_all_list	        = $team_model->getTeamAllList();
$team_name_list	        = $team_all_list["team_name_list"];
$staff_all_list	        = $team_all_list["staff_list"];
$sch_staff_list         = $staff_all_list['all'];
$approval_group_list    = $kind_model->getKindGroupList("approval");
$leave_type_list	    = getLeaveTypeList();
$approval_g1_list       = [];
$approval_g2_list       = [];
$approval_g2_total_list = [];

foreach($approval_group_list as $key => $appr_data)
{
    if(!$key){
        $approval_g1_list = $appr_data;
    }else{
        $approval_g2_list[$key] = $appr_data;
        $approval_g2_total_list = array_merge($approval_g2_total_list, $appr_data);
    }
}
$appr_form_g1_list = $approval_g1_list;
$appr_form_g2_list = $approval_g2_total_list;

# 기본 사용 변수
$btn_title          = "저장";
$editor_path        = "approval_report";
$approval_report    = $approval_permission_me = $approval_withdraw = [];
$file_origin_read   = $file_origin_name = "";

$ar_no              = isset($_GET['ar_no']) ? $_GET['ar_no'] : "";
$re_ar_no           = isset($_GET['re_ar_no']) ? $_GET['re_ar_no'] : "";
$linked_no          = isset($_GET['linked_no']) ? $_GET['linked_no'] : 0;
$linked_table       = isset($_GET['linked_table']) ? $_GET['linked_table'] : "";
$approval_permission_me = [];

$smarty->assign("linked_no", $linked_no);
$smarty->assign("linked_table", $linked_table);

if(!empty($re_ar_no))
{
    $approval_report['is_re_approval'] = true;
    $approval_report_sql    = "
        SELECT 
            *, 
            (SELECT k_parent FROM kind k WHERE k.k_name_code=ar.k_name_code) as appr_g1
        FROM approval_report as ar 
        WHERE ar.ar_no='{$re_ar_no}'
    ";
    $approval_report_query  = mysqli_query($my_db, $approval_report_sql);
    $approval_report_result = mysqli_fetch_assoc($approval_report_query);

    if(!isset($approval_report_result['ar_no']) || empty($approval_report_result['ar_no'])) {
        exit("<script>alert('존재하지 않는 결재입니다.');location.href='approval_report_list.php';</script>");
    }else if($approval_report_result['req_s_no'] != $session_s_no){
        exit("<script>alert('재상신 권한이 없는 결재입니다.');location.href='approval_report_list.php';</script>");
    }

    $form_item = $form_model->getItem($approval_report_result['af_no']);

    $approval_report_result['is_mod_permission'] = $form_item['is_modify'];
    $approval_report_result['is_withdraw']       = $form_item['is_withdraw'];
    $approval_report_result['is_calendar']       = $form_item['is_calendar'];
    $approval_report_result['title']             = $form_item['title'];
    $approval_report_result['ar_no']             = "";
    $approval_report_result['ar_state']          = "1";

    $approval_report = $approval_report_result;
    $appr_form_g2_list = $approval_g2_list[$approval_report_result['appr_g1']];

    # Permission 가져오기
    $appr_search_idx  = 2;
    $appr_reading_idx = $appr_reference_idx = 2;
    $approval_permission_list  = $reading_permission_list = $reference_permission_list  = [];
    $origin_approval_arp_no    = $origin_reading_arp_no = $origin_reference_arp_no = [];

    $approval_permission_sql   = "SELECT * FROM approval_report_permission WHERE ar_no='{$re_ar_no}' ORDER BY priority";
    $approval_permission_query = mysqli_query($my_db, $approval_permission_sql);
    while($approval_permission_result = mysqli_fetch_assoc($approval_permission_query))
    {
        $approval_permission_result['arp_no'] = "";
        if($approval_permission_result['arp_type'] == 'approval' || $approval_permission_result['arp_type'] == 'conference')
        {
            if($approval_permission_result['arp_type'] == 'approval' && $approval_permission_result['arp_state'] == '0')
            {
                $approval_permission_me = array(
                    'arp_s_no' => $approval_permission_result['arp_s_no'],
                    'arp_team' => $approval_permission_result['arp_team'],
                    'arp_date' => date('m/d')
                );
            }
            else
            {
                $approval_permission_list[] = $approval_permission_result;
                $appr_search_idx = $approval_permission_result['priority']+1;
            }
        }elseif($approval_permission_result['arp_type'] == 'reading'){
            $reading_permission_list[] = $approval_permission_result;
            $appr_reading_idx = $approval_permission_result['priority']+1;
        }elseif($approval_permission_result['arp_type'] == 'reference'){
            $reference_permission_list[] = $approval_permission_result;
            $appr_reference_idx = $approval_permission_result['priority']+1;
        }
    }

    $approval_report['appr_search_idx']    = $appr_search_idx;
    $approval_report['appr_reading_idx']   = $appr_reading_idx;
    $approval_report['appr_reference_idx'] = $appr_reference_idx;

    $approval_report['approval_permission_list']  = $approval_permission_list;
    $approval_report['reading_permission_list']   = $reading_permission_list;
    $approval_report['reference_permission_list'] = $reference_permission_list;

    if(!empty($approval_report['wd_no']))
    {
        $withdraw_sql    = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=wd.s_no) as s_name, (SELECT t.team_name FROM team t WHERE t.team_code=wd.team) as t_name FROM withdraw wd WHERE wd_no='{$approval_report['wd_no']}'";
        $withdraw_query  = mysqli_query($my_db, $withdraw_sql);
        $withdraw_result = mysqli_fetch_assoc($withdraw_query);

        $approval_report['wd_no'] = "";
        if(isset($withdraw_result['wd_no']))
        {
            $approval_withdraw = array(
                'wd_c_no'               => $withdraw_result['c_no'],
                'wd_c_name'             => $withdraw_result['c_name'],
                'wd_manager'            => $withdraw_result['s_no'],
                'wd_team'               => $withdraw_result['team'],
                'wd_manager_name'       => "{$withdraw_result['s_name']} ({$withdraw_result['t_name']})",
                'wd_title'              => $withdraw_result['bk_title'],
                'wd_account'            => $withdraw_result['bk_name'],
                'wd_num'                => $withdraw_result['bk_num'],
                'wd_vat_tax_choice'     => $withdraw_result['vat_choice'],
                'wd_cost'               => $withdraw_result['cost'],
                'wd_supply_cost'        => $withdraw_result['supply_cost'],
                'wd_supply_cost_vat'    => $withdraw_result['supply_cost_vat'],
                'wd_biz_tax'            => $withdraw_result['biz_tax'],
                'wd_local_tax'          => $withdraw_result['local_tax'],
            );
        }
    }
    else
    {
        $withdraw_sql      = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=arw.wd_manager) as s_name, (SELECT t.team_name FROM team t WHERE t.team_code=arw.wd_team) as t_name FROM approval_report_withdraw arw WHERE ar_no='{$ar_no}'";
        $withdraw_query    = mysqli_query($my_db, $withdraw_sql);
        $withdraw_result   = mysqli_fetch_assoc($withdraw_query);
        if(isset($withdraw_result['arw_no']))
        {
            $withdraw_result['wd_manager_name'] = "{$withdraw_result['s_name']} ({$withdraw_result['t_name']})";
            $approval_withdraw = $withdraw_result;
        }
    }
}
elseif(!empty($ar_no))
{
    $btn_title  = "수정";

    $approval_report_sql    = "
        SELECT 
            *,
            (SELECT k_parent FROM kind k WHERE k.k_name_code=ar.k_name_code) as appr_g1
        FROM approval_report as ar 
        WHERE ar.ar_no='{$ar_no}' AND ar.ar_state='1'
    ";
    $approval_report_query  = mysqli_query($my_db, $approval_report_sql);
    $approval_report_result = mysqli_fetch_assoc($approval_report_query);

    if(!isset($approval_report_result['ar_no']) || empty($approval_report_result['ar_no']))
    {
        exit("<script>alert('존재하지 않는 결재입니다.');location.href='approval_report_list.php';</script>");
    }
    else if($approval_report_result['req_s_no'] != $session_s_no)
    {
        exit("<script>alert('해당 결재 권한이 없습니다.');location.href='approval_report_list.php';</script>");
    }

    $form_item = $form_model->getItem($approval_report_result['af_no']);

    $approval_report_result['is_mod_permission'] = $form_item['is_modify'];
    $approval_report_result['is_withdraw']       = $form_item['is_withdraw'];
    $approval_report_result['is_calendar']       = $form_item['is_calendar'];
    $approval_report_result['title']             = $form_item['title'];

    # 휴가처리
    if($approval_report_result['is_calendar'] == "1")
    {
        $approval_calendar_sql      = "SELECT * FROM approval_report_leave WHERE ar_no='{$ar_no}'";
        $approval_calendar_query    = mysqli_query($my_db, $approval_calendar_sql);
        $approval_calendar_result   = mysqli_fetch_assoc($approval_calendar_query);

        if(!empty($approval_calendar_result)){
            $approval_report_result['arl_no']       = $approval_calendar_result['arl_no'];
            $approval_report_result['leave_no']     = $approval_calendar_result['leave_no'];
            $approval_report_result['leave_type']   = $approval_calendar_result['leave_type'];
            $approval_report_result['leave_s_date'] = date("Y-m-d", strtotime($approval_calendar_result['leave_s_date']));
            $approval_report_result['leave_e_date'] = date("Y-m-d", strtotime($approval_calendar_result['leave_e_date']));
        }
    }

    $approval_report    = $approval_report_result;
    $appr_form_g2_list  = $approval_g2_list[$approval_report_result['appr_g1']];

    # Permission 가져오기
    $appr_search_idx  = 2;
    $appr_reading_idx = $appr_reference_idx = 1;
    $approval_permission_list  = $reading_permission_list = $reference_permission_list  = [];
    $origin_approval_arp_no    = $origin_reading_arp_no = $origin_reference_arp_no = [];

    $approval_permission_sql   = "SELECT * FROM approval_report_permission WHERE ar_no='{$ar_no}' ORDER BY priority";
    $approval_permission_query = mysqli_query($my_db, $approval_permission_sql);
    while($approval_permission_result = mysqli_fetch_assoc($approval_permission_query))
    {
        if($approval_permission_result['arp_type'] == 'approval' || $approval_permission_result['arp_type'] == 'conference')
        {
            if($approval_permission_result['arp_type'] == 'approval' && $approval_permission_result['arp_state'] == '0')
            {
                $approval_permission_me = array(
                    'arp_s_no' => $approval_permission_result['arp_s_no'],
                    'arp_team' => $approval_permission_result['arp_team'],
                    'arp_date' => !empty($approval_permission_result['arp_date']) ? date('m/d', strtotime($approval_permission_result['arp_date'])) : date('m/d')
                );
            }
            else
            {
                $approval_permission_list[] = $approval_permission_result;
                $appr_search_idx = $approval_permission_result['priority']+1;
                $origin_approval_arp_no[] = $approval_permission_result['arp_no'];
            }
        }elseif($approval_permission_result['arp_type'] == 'reading'){
            $reading_permission_list[] = $approval_permission_result;
            $appr_reading_idx = $approval_permission_result['priority']+1;
            $origin_reading_arp_no[] = $approval_permission_result['arp_no'];
        }elseif($approval_permission_result['arp_type'] == 'reference'){
            $reference_permission_list[] = $approval_permission_result;
            $appr_reference_idx = $approval_permission_result['priority']+1;
            $origin_reference_arp_no[] = $approval_permission_result['arp_no'];
        }
    }

    $approval_report['appr_search_idx']    = $appr_search_idx;
    $approval_report['appr_reading_idx']   = $appr_reading_idx;
    $approval_report['appr_reference_idx'] = $appr_reference_idx;

    $approval_report['approval_permission_list']  = $approval_permission_list;
    $approval_report['reading_permission_list']   = $reading_permission_list;
    $approval_report['reference_permission_list'] = $reference_permission_list;

    $approval_report['origin_approval_arp_no']  = !empty($approval_permission_list) ? implode(",", $origin_approval_arp_no) : "";
    $approval_report['origin_reading_arp_no']   = !empty($reading_permission_list) ? implode(",", $origin_reading_arp_no) : "";
    $approval_report['origin_reference_arp_no'] = !empty($reference_permission_list) ? implode(",", $origin_reference_arp_no) : "";

    # 파일처리
    $file_paths = $approval_report['file_path'];
    $file_names = $approval_report['file_name'];
    if(!empty($file_paths) && !empty($file_names))
    {
        $approval_report['file_paths'] = explode(',', $file_paths);
        $approval_report['file_names'] = explode(',', $file_names);

        $file_origin_read = $approval_report['file_paths'];
        $file_origin_name = $approval_report['file_names'];
    }

    # 출금처리
    if(!empty($approval_report['wd_no']))
    {
        $withdraw_sql    = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=wd.s_no) as s_name, (SELECT t.team_name FROM team t WHERE t.team_code=wd.team) as t_name FROM withdraw wd WHERE wd_no='{$approval_report['wd_no']}'";
        $withdraw_query  = mysqli_query($my_db, $withdraw_sql);
        $withdraw_result = mysqli_fetch_assoc($withdraw_query);

        if(isset($withdraw_result['wd_no']))
        {
            $approval_withdraw = array(
                'wd_no'                 => $withdraw_result['wd_no'],
                'wd_c_no'               => $withdraw_result['c_no'],
                'wd_c_name'             => $withdraw_result['c_name'],
                'wd_manager'            => $withdraw_result['s_no'],
                'wd_team'               => $withdraw_result['team'],
                'wd_manager_name'       => "{$withdraw_result['s_name']} ({$withdraw_result['t_name']})",
                'wd_title'              => $withdraw_result['bk_title'],
                'wd_account'            => $withdraw_result['bk_name'],
                'wd_num'                => $withdraw_result['bk_num'],
                'wd_vat_tax_choice'     => $withdraw_result['vat_choice'],
                'wd_cost'               => $withdraw_result['cost'],
                'wd_supply_cost'        => $withdraw_result['supply_cost'],
                'wd_supply_cost_vat'    => $withdraw_result['supply_cost_vat'],
                'wd_biz_tax'            => $withdraw_result['biz_tax'],
                'wd_local_tax'          => $withdraw_result['local_tax'],
            );
        }
    }
    else
    {
        $withdraw_sql      = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=arw.wd_manager) as s_name, (SELECT t.team_name FROM team t WHERE t.team_code=arw.wd_team) as t_name FROM approval_report_withdraw arw WHERE ar_no='{$ar_no}'";
        $withdraw_query    = mysqli_query($my_db, $withdraw_sql);
        $withdraw_result   = mysqli_fetch_assoc($withdraw_query);
        if(isset($withdraw_result['arw_no']))
        {
            $withdraw_result['wd_manager_name'] = "{$withdraw_result['s_name']} ({$withdraw_result['t_name']})";
            $approval_withdraw = $withdraw_result;
        }
    }

}else{

    $approval_report = array(
        "appr_search_idx"      => "2",
        "appr_reading_idx"     => "1",
        "appr_reference_idx"   => "1"
    );

    $approval_permission_me = array(
        'arp_s_no' => $session_s_no,
        'arp_team' => $session_team,
        'arp_date' => date('m/d')
    );

    if($linked_table == 'work' && $linked_no)
    {
        $check_ar_sql = "SELECT count(ar_no) as cnt FROM approval_report WHERE linked_no='{$linked_no}' AND linked_table='{$linked_table}'";
        $check_ar_query = mysqli_query($my_db, $check_ar_sql);
        $check_ar_result = mysqli_fetch_assoc($check_ar_query);

        if($check_ar_result['cnt'] > 0){
            exit("<script>alert('이미 결재 상신중입니다.');location.href='approval_report_list.php';</script>");
        }

        $work_sql    = "
            SELECT w_no, wd_no, wd_c_no, wd_price, wd_price_vat, wd_c_name, t_keyword, 
                (SELECT (SELECT s.s_name FROM staff s WHERE s.s_no=c.s_no LIMIT 1) as s_name FROM company c WHERE c.c_no = w.wd_c_no LIMIT 1) as wd_s_name, 
                (SELECT p.af_no FROM product p WHERE p.prd_no=w.prd_no) as af_no, (SELECT p.af_format FROM product p WHERE p.prd_no=w.prd_no) as af_format,
                (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1,
                (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2,
                (SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no) as prd_name
            FROM `work` as w WHERE w.w_no='{$linked_no}'";
        $work_query  = mysqli_query($my_db, $work_sql);
        $work_result = mysqli_fetch_assoc($work_query);

        if(isset($work_result['af_no']) && !empty($work_result['af_no']))
        {
            $work_content = $work_result['af_format'];
            $work_content = str_replace("#{업체명}", $work_result['wd_c_name'], $work_content);
            $work_content = str_replace("#{업체담당자명}", $work_result['wd_s_name'], $work_content);
            $work_content = str_replace("#{타겟키워드}", $work_result['t_keyword'], $work_content);
            $work_content = str_replace("#{지급액VAT포함}", number_format($work_result['wd_price_vat'],0), $work_content);
            $work_content = str_replace("#{지급액VAT별도}", number_format($work_result['wd_price'],0), $work_content);

            $approval_report['af_no']      = $work_result['af_no'];
            $approval_report['ar_content'] = $work_content;

            $approval_report['work_name']  = $work_result['k_prd1']." > ".$work_result['k_prd2']." > ".$work_result['prd_name'];
            $approval_report['work_link']  = "https://work.wplanet.co.kr/v1/work_list.php?sch_w_no={$work_result['w_no']}";

            if($work_result['wd_no'] > 0)
            {
                $withdraw_sql    = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=wd.s_no) as s_name, (SELECT t.team_name FROM team t WHERE t.team_code=wd.team) as t_name FROM withdraw wd WHERE wd_no='{$work_result['wd_no']}'";
                $withdraw_query  = mysqli_query($my_db, $withdraw_sql);
                $withdraw_result = mysqli_fetch_assoc($withdraw_query);

                if(isset($withdraw_result['wd_no']))
                {
                    $approval_withdraw = array(
                        'wd_no'                 => $withdraw_result['wd_no'],
                        'wd_c_no'               => $withdraw_result['c_no'],
                        'wd_c_name'             => $withdraw_result['c_name'],
                        'wd_manager'            => $withdraw_result['s_no'],
                        'wd_team'               => $withdraw_result['team'],
                        'wd_manager_name'       => "{$withdraw_result['s_name']} ({$withdraw_result['t_name']})",
                        'wd_title'              => $withdraw_result['bk_title'],
                        'wd_account'            => $withdraw_result['bk_name'],
                        'wd_num'                => $withdraw_result['bk_num'],
                        'wd_vat_tax_choice'     => $withdraw_result['vat_choice'],
                        'wd_cost'               => $withdraw_result['cost'],
                        'wd_supply_cost'        => $withdraw_result['supply_cost'],
                        'wd_supply_cost_vat'    => $withdraw_result['supply_cost_vat'],
                        'wd_biz_tax'            => $withdraw_result['biz_tax'],
                        'wd_local_tax'          => $withdraw_result['local_tax'],
                    );
                }
            }
            elseif($work_result['wd_c_no'] > 0)
            {

                $wd_company_sql    = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=c.s_no) as s_name, (SELECT s.team FROM staff s WHERE s.s_no=c.s_no) as team, (SELECT t.team_name FROM team t WHERE t.team_code=(SELECT s.team FROM staff s WHERE s.s_no=c.s_no)) as t_name FROM company c WHERE c.c_no='{$work_result['wd_c_no']}'";
                $wd_company_query  = mysqli_query($my_db, $wd_company_sql);
                $wd_company_result = mysqli_fetch_assoc($wd_company_query);

                $approval_withdraw = array(
                    'wd_c_no'               => $wd_company_result['c_no'],
                    'wd_c_name'             => $wd_company_result['c_name'],
                    'wd_manager'            => $wd_company_result['s_no'],
                    'wd_team'               => $wd_company_result['team'],
                    'wd_manager_name'       => "{$wd_company_result['s_name']} ({$wd_company_result['t_name']})",
                    'wd_title'              => $wd_company_result['bk_title'],
                    'wd_account'            => $wd_company_result['bk_name'],
                    'wd_num'                => $wd_company_result['bk_num'],
                    'wd_vat_tax_choice'     => '1',
                    'wd_cost'               => $work_result['wd_price_vat'],
                    'wd_supply_cost'        => $work_result['wd_price'],
                    'wd_supply_cost_vat'    => $work_result['wd_price_vat']-$work_result['wd_price']
                );
            }
        }
    }
    elseif($linked_table == 'commerce_order' && $linked_no)
    {
        $check_ar_sql = "SELECT count(ar_no) as cnt FROM approval_report WHERE linked_no='{$linked_no}' AND linked_table='{$linked_table}'";
        $check_ar_query = mysqli_query($my_db, $check_ar_sql);
        $check_ar_result = mysqli_fetch_assoc($check_ar_query);

        if($check_ar_result['cnt'] > 0){
            exit("<script>alert('이미 결재 상신중입니다.');location.href='approval_report_list.php';</script>");
        }

        $commerce_order_sql     = "SELECT *, (SELECT cos.c_no FROM commerce_order_set cos WHERE cos.`no`=co.set_no) as c_no, (SELECT cos.c_name FROM commerce_order_set cos WHERE cos.`no`=co.set_no) as c_name, (SELECT cos.order_count FROM commerce_order_set cos WHERE cos.`no`=co.set_no) as ord_cnt FROM commerce_order co WHERE `no` ='{$linked_no}' AND `type`='withdraw'";
        $commerce_order_query   = mysqli_query($my_db, $commerce_order_sql);
        $commerce_order_result  = mysqli_fetch_assoc($commerce_order_query);

        $approval_report['af_no']    = 3;
        $approval_report['ar_title'] = $commerce_order_result['c_name']." [".$commerce_order_result['ord_cnt']."차]";

        $approval_report['work_name'] = "발주관리";
        $approval_report['work_link'] = "http://work.wplanet.co.kr/v1/commerce_order_regist.php?no={$commerce_order_result['set_no']}";

        $withdraw_sql    = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=wd.s_no) as s_name, (SELECT t.team_name FROM team t WHERE t.team_code=wd.team) as t_name FROM withdraw wd WHERE wd_no='{$commerce_order_result['wd_no']}'";
        $withdraw_query  = mysqli_query($my_db, $withdraw_sql);
        $withdraw_result = mysqli_fetch_assoc($withdraw_query);

        if(isset($withdraw_result['wd_no']))
        {
            $approval_report['ar_title'] = $withdraw_result['wd_subject'];
            $approval_withdraw = array(
                'wd_no'                 => $withdraw_result['wd_no'],
                'wd_c_no'               => $withdraw_result['c_no'],
                'wd_c_name'             => $withdraw_result['c_name'],
                'wd_manager'            => $withdraw_result['s_no'],
                'wd_team'               => $withdraw_result['team'],
                'wd_manager_name'       => "{$withdraw_result['s_name']} ({$withdraw_result['t_name']})",
                'wd_title'              => $withdraw_result['bk_title'],
                'wd_account'            => $withdraw_result['bk_name'],
                'wd_num'                => $withdraw_result['bk_num'],
                'wd_vat_tax_choice'     => $withdraw_result['vat_choice'],
                'wd_cost'               => $withdraw_result['cost'],
                'wd_supply_cost'        => $withdraw_result['supply_cost'],
                'wd_supply_cost_vat'    => $withdraw_result['supply_cost_vat'],
                'wd_biz_tax'            => $withdraw_result['biz_tax'],
                'wd_local_tax'          => $withdraw_result['local_tax'],
            );
        }
    }
}

if(!isset($approval_report['file_paths']) || !isset($approval_report['file_names']))
{
    $approval_report['file_paths'] = "";
    $approval_report['file_names'] = "";
}

$file_count = !empty($approval_report['file_paths']) ? count($approval_report['file_paths']) : 0;
$smarty->assign("file_count", $file_count);

$smarty->assign($approval_report);
$smarty->assign("approval_permission_me", $approval_permission_me);
$smarty->assign($approval_withdraw);
$smarty->assign("btn_title", $btn_title);
$smarty->assign("file_origin_read", $file_origin_read);
$smarty->assign("file_origin_name", $file_origin_name);
$smarty->assign("approval_form_list", $report_model->getApprovalFormList());
$smarty->assign("permission_option", getPermissionOption());
$smarty->assign("appr_form_g1_list", $appr_form_g1_list);
$smarty->assign("appr_form_g2_list", $appr_form_g2_list);
$smarty->assign('appr_form_team_list', $team_name_list);
$smarty->assign('appr_form_staff_list', $staff_all_list);
$smarty->assign('leave_type_list', $leave_type_list);

$smarty->display("approval_report.html");
?>