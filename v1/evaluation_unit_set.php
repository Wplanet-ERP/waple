<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');

$ev_no=isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$smarty->assign("ev_no",$ev_no);
$ev_u_set_no=isset($_GET['ev_u_set_no']) ? $_GET['ev_u_set_no'] : "";
$smarty->assign("ev_u_set_no",$ev_u_set_no);
$ev_r_no=isset($_GET['ev_r_no']) ? $_GET['ev_r_no'] : "";
$smarty->assign("ev_r_no",$ev_r_no);


// POST : form action 을 통해 넘겨받은 process 가 있는지 체크하고 있으면 proc에 저장
$proc=isset($_POST['process']) ? $_POST['process'] : "";


if($proc == "write")
{
    $ev_no=isset($_POST['ev_no']) ? $_POST['ev_no'] : "";
    $ev_r_no=isset($_POST['ev_r_no']) ? $_POST['ev_r_no'] : "";
    $ev_u_set_no=isset($_POST['ev_u_set_no']) ? $_POST['ev_u_set_no'] : "";

    $system_add_set = "";
    $evaluation_value = "";

    // 평가 시스템 관계 쿼리
    $evaluation_relation_sql="SELECT
															ev_relation.ev_r_no,
															ev_relation.receiver_s_no,
															ev_relation.evaluator_s_no,
															ev_relation.rate
														FROM evaluation_relation ev_relation
														WHERE ev_relation.ev_r_no='{$ev_r_no}'
														";
    $evaluation_relation_result=mysqli_query($my_db,$evaluation_relation_sql);
    while($relation_result=mysqli_fetch_array($evaluation_relation_result)){

        if($relation_result['rate']){
            $system_add_set.="rate = '".$relation_result['rate']."',";
        }
        $system_add_set.="receiver_s_no = '".$relation_result['receiver_s_no']."',";
        $system_add_set.="evaluator_s_no = '".$relation_result['evaluator_s_no']."'";

    }

    // 평가지 쿼리
    $evaluation_unit_sql="SELECT
													ev_u.ev_u_no,
													ev_u.order,
													ev_u.kind,
													ev_u.question,
													ev_u.description,
													ev_u.evaluation_state,
													ev_u.choice_items,
													ev_u.essential
												FROM evaluation_unit ev_u
												LEFT JOIN evaluation_unit_set ev_u_set ON ev_u_set.ev_u_set_no = ev_u.ev_u_set_no
												WHERE ev_u.ev_u_set_no='{$ev_u_set_no}' AND ev_u.evaluation_state < 99 AND ev_u.active='1'
												ORDER BY ev_u.order ASC
												";

    $evaluation_unit_result=mysqli_query($my_db,$evaluation_unit_sql);
    while($unit_array=mysqli_fetch_array($evaluation_unit_result)) {
        $evaluation_value = "";
        $add_set = "";

        if($unit_array['evaluation_state'] == '9'){ // 객관형(복수)
            foreach ($_POST['f_evaluation_value_'.$unit_array['ev_u_no']] as $e_value) {
                if(next($_POST['f_evaluation_value_'.$unit_array['ev_u_no']]))
                    $evaluation_value .= $e_value."||";
                else
                    $evaluation_value .= $e_value;
            }
        }else{
            $evaluation_value = addslashes($_POST['f_evaluation_value_'.$unit_array['ev_u_no']]);
        }

        $add_set.="ev_no = '".$ev_no."',";
        $add_set.="ev_r_no = '".$ev_r_no."',";
        $add_set.="ev_u_no = '".$unit_array['ev_u_no']."',";
        $add_set.="ev_u_set_no = '".$ev_u_set_no."',";
        $add_set.="`order` = '".$unit_array['order']."',";
        $add_set.="kind = '".$unit_array['kind']."',";
        $add_set.="question = '".$unit_array['question']."',";
        $add_set.="description = '".$unit_array['description']."',";
        $add_set.="evaluation_state = '".$unit_array['evaluation_state']."',";
        $add_set.="evaluation_value = '".$evaluation_value."',";

        if($unit_array['evaluation_state'] == '1')
            $add_set.="hundred_points = '".($evaluation_value*20)."',";
        elseif($unit_array['evaluation_state'] == '2')
            $add_set.="hundred_points = '".($evaluation_value*10)."',";
        elseif($unit_array['evaluation_state'] == '3')
            $add_set.="hundred_points = '".$evaluation_value."',";

        $add_set.="choice_items = '".$unit_array['choice_items']."',";
        $add_set.="essential = '".$unit_array['essential']."',";
        $add_set.="regdate = now(),";

        $add_set.=$system_add_set;
        $evaluation_system_result_sql="INSERT INTO evaluation_system_result SET
															".$add_set."
															";

        if (!mysqli_query($my_db, $evaluation_system_result_sql)){
            exit("<script>alert('등록에 실패 하였습니다');location.href='evaluation_unit_set.php?ev_no=$ev_no&ev_r_no=$ev_r_no'';</script>");
        }
    }

    if($ev_no == '53'){
        exit("<script>alert('등록에 성공했습니다');location.href='evaluation_unit_set.php?ev_no=$ev_no&ev_r_no=$ev_r_no';window.open('popup/naver_survey.php','2020 인사평가 설문조사', 'location=yes,links=no,toolbar=no,top=10,left=10,width=1200,height=800,resizable=yes,scrollbars=no,status=no');</script>");
    }else{
        exit("<script>location.href='evaluation_unit_set.php?ev_no=$ev_no&ev_r_no=$ev_r_no';</script>");
    }
}elseif($proc == "modify"){
    $ev_no=isset($_POST['ev_no']) ? $_POST['ev_no'] : "";
    $ev_r_no=isset($_POST['ev_r_no']) ? $_POST['ev_r_no'] : "";
    $ev_u_set_no=isset($_POST['ev_u_set_no']) ? $_POST['ev_u_set_no'] : "";

    $ev_result_no_value = "";

    // 평가지 쿼리
    $evaluation_unit_sql="SELECT
													ev_u.ev_u_no,
													ev_u.order,
													ev_u.kind,
													ev_u.question,
													ev_u.description,
													ev_u.evaluation_state,
													ev_u.choice_items,
													ev_u.essential
												FROM evaluation_unit ev_u
												LEFT JOIN evaluation_unit_set ev_u_set ON ev_u_set.ev_u_set_no = ev_u.ev_u_set_no
												WHERE ev_u.ev_u_set_no='{$ev_u_set_no}' AND ev_u.evaluation_state < 99 AND ev_u.active='1'
												ORDER BY ev_u.order ASC
												";

    $evaluation_unit_result=mysqli_query($my_db,$evaluation_unit_sql);
    while($unit_array=mysqli_fetch_array($evaluation_unit_result)) {
        $evaluation_value = "";
        $add_set = "";

        $ev_result_no_value = $_POST['f_ev_result_no_'.$unit_array['ev_u_no']];

        if($unit_array['evaluation_state'] == '9'){ // 객관형(복수)
            foreach ($_POST['f_evaluation_value_'.$unit_array['ev_u_no']] as $e_value) {
                if(next($_POST['f_evaluation_value_'.$unit_array['ev_u_no']]))
                    $evaluation_value .= $e_value."||";
                else
                    $evaluation_value .= $e_value;
            }
        }else{
            $evaluation_value = addslashes($_POST['f_evaluation_value_'.$unit_array['ev_u_no']]);
        }

        $add_set.="`order` = '".$unit_array['order']."',";
        $add_set.="kind = '".$unit_array['kind']."',";
        $add_set.="question = '".$unit_array['question']."',";
        $add_set.="description = '".$unit_array['description']."',";
        $add_set.="evaluation_state = '".$unit_array['evaluation_state']."',";
        $add_set.="evaluation_value = '".$evaluation_value."',";

        if($unit_array['evaluation_state'] == '1')
            $add_set.="hundred_points = '".($evaluation_value*20)."',";
        elseif($unit_array['evaluation_state'] == '2')
            $add_set.="hundred_points = '".($evaluation_value*10)."',";
        elseif($unit_array['evaluation_state'] == '3')
            $add_set.="hundred_points = '".$evaluation_value."',";

        $add_set.="choice_items = '".$unit_array['choice_items']."',";
        $add_set.="essential = '".$unit_array['essential']."',";
        $add_set.="regdate = now()";

        if($ev_result_no_value){
            $evaluation_system_result_sql = "UPDATE evaluation_system_result SET	{$add_set} WHERE ev_result_no = {$ev_result_no_value}";
        }else{
            $add_set.=",ev_no = '".$ev_no."',";
            $add_set.="ev_r_no = '".$ev_r_no."',";
            $add_set.="ev_u_no = '".$unit_array['ev_u_no']."',";
            $add_set.="ev_u_set_no = '".$ev_u_set_no."'";

            // 평가 시스템 관계 쿼리
            $evaluation_relation_sql="
					SELECT
						ev_relation.ev_r_no,
						ev_relation.receiver_s_no,
						ev_relation.evaluator_s_no,
						ev_relation.rate
					FROM evaluation_relation ev_relation
					WHERE ev_relation.ev_r_no='{$ev_r_no}'
				";
            $evaluation_relation_result=mysqli_query($my_db,$evaluation_relation_sql);
            while($relation_result=mysqli_fetch_array($evaluation_relation_result)){

                if($relation_result['rate']){
                    $add_set.=",rate = '".$relation_result['rate']."'";
                }
                $add_set.=",receiver_s_no = '".$relation_result['receiver_s_no']."',";
                $add_set.="evaluator_s_no = '".$relation_result['evaluator_s_no']."'";
            }

            $evaluation_system_result_sql = "INSERT INTO evaluation_system_result SET {$add_set}";
        }

        if (!mysqli_query($my_db, $evaluation_system_result_sql)){
            exit("<script>alert('수정에 실패 하였습니다');location.href='evaluation_unit_set.php?ev_no=$ev_no&ev_r_no=$ev_r_no';</script>");
        }
    }
    exit("<script>location.href='evaluation_unit_set.php?ev_no=$ev_no&ev_r_no=$ev_r_no';</script>");
}

if($ev_no){ // 평가하기
    // 직원 가져오기
    $staff_sql="SELECT s_no,team,s_name,position FROM staff WHERE staff_state < '2' ORDER BY team ASC, s_name ASC";
    $staff_query=mysqli_query($my_db,$staff_sql);

    while($staff_data=mysqli_fetch_array($staff_query)) {
        $staff[]=array(
            'no'=>$staff_data['s_no'],
            'team'=>$staff_data['team'],
            'name'=>$staff_data['s_name'],
            'position'=>$staff_data['position']
        );
        $smarty->assign("staff",$staff);
    }

    // 평가 시스템 쿼리
    $evaluation_system_sql="
		SELECT
			ev_system.subject,
			ev_system.ev_state,
			ev_system.ev_s_date,
			ev_system.ev_e_date,
			ev_system.admin_s_no
		FROM evaluation_system ev_system
		WHERE ev_system.ev_no='{$ev_no}'
	";
    $evaluation_system_result=mysqli_query($my_db,$evaluation_system_sql);
    $system_result = mysqli_fetch_array($evaluation_system_result);

    // 평가기간 체크
    if(!($system_result['admin_s_no'] == $session_s_no || permissionNameCheck($session_permission, "대표") || permissionNameCheck($session_permission, "재무관리자")))
    {
        if($system_result['ev_state'] == '1'){
            echo "아직 평가일이 아닙니다.";
            exit;
        }elseif($system_result['ev_state'] > 2){
            echo "평가기간이 종료되었습니다.";
            exit;
        }

        if(strtotime($system_result['ev_s_date']) > strtotime($today)){
            echo "아직 평가일이 아닙니다.";
            exit;
        }elseif(strtotime($system_result['ev_e_date']) < strtotime($today)){
            echo "평가기간이 종료되었습니다.";
            exit;
        }
    }

    $smarty->assign(
        array(
            "f_subject"		=> trim($system_result['subject']),
            "f_ev_s_date"	=> trim($system_result['ev_s_date']),
            "f_ev_e_date"	=> trim($system_result['ev_e_date']),
            "f_admin_s_no"	=> trim($system_result['admin_s_no'])
        )
    );

    // 평가 시스템 관계 쿼리
    $evaluation_relation_sql="
		SELECT
			ev_relation.ev_r_no,
			ev_relation.receiver_s_no,
			(SELECT s_name FROM staff s where s.s_no=ev_relation.receiver_s_no) as receiver_s_name,
			ev_relation.evaluator_s_no,
			(SELECT s_name FROM staff s where s.s_no=ev_relation.evaluator_s_no) as evaluator_s_name,
			(SELECT ev_u_set_no FROM evaluation_system where ev_no='{$ev_no}') as ev_u_set_no,
			(SELECT COUNT(essential) FROM evaluation_system_result system_result WHERE system_result.receiver_s_no = ev_relation.receiver_s_no AND system_result.evaluator_s_no = ev_relation.evaluator_s_no AND system_result.ev_u_set_no = (SELECT ev_u_set_no FROM evaluation_system where ev_no='{$ev_no}') AND system_result.ev_r_no = ev_relation.ev_r_no) AS result_essential,
			(SELECT COUNT(ev_u.essential) FROM evaluation_unit ev_u WHERE ev_u.ev_u_set_no=(SELECT ev_u_set_no FROM evaluation_system where ev_no='{$ev_no}') AND ev_u.essential='1' AND ev_u.active='1') AS total_essential,
			(SELECT subject FROM evaluation_unit_set ev_u_set WHERE ev_u_set.ev_u_set_no = (SELECT ev_u_set_no FROM evaluation_system where ev_no='{$ev_no}')) AS set_subject,
			(SELECT description FROM evaluation_unit_set ev_u_set WHERE ev_u_set.ev_u_set_no = (SELECT ev_u_set_no FROM evaluation_system where ev_no='{$ev_no}')) AS set_description,
			ev_relation.rate
		FROM evaluation_relation ev_relation
		WHERE ev_relation.ev_no='{$ev_no}' AND ev_relation.evaluator_s_no = '{$session_s_no}'
	";


    $current_receiver_name = $self_ev_satisfaction = $self_ev_education = $self_ev_communication = '';
    $evaluation_relation_result=mysqli_query($my_db,$evaluation_relation_sql);
    $is_self_ev = false;
    while($relation_result=mysqli_fetch_array($evaluation_relation_result)){

        if(!$relation_result['ev_u_set_no']){
            echo "평가할 평가지가 존재하지 않습니다.";
            exit;
        }

        if(empty($ev_r_no)){ // 평가받는 사람 첫번째 기본 선택
            $ev_r_no = $relation_result['ev_r_no'];
            $smarty->assign("ev_r_no",$ev_r_no);
        }

        if($ev_r_no == $relation_result['ev_r_no']){ // 평가받는 사람이 선택되어 있는 경우 이름 찾기
            $current_receiver_name = $relation_result['receiver_s_name'];
            $self_ev_sql 	= "SELECT * FROM evaluation_reciever_self_evaluation WHERE ev_no = '{$ev_no}' AND receiver_s_no = '{$relation_result['receiver_s_no']}' ORDER BY ev_r_s_no LIMIT 1";
            $self_ev_query 	= mysqli_query($my_db, $self_ev_sql);
            $self_ev_count  = mysqli_num_rows($self_ev_query);
            if($self_ev_count > 0)
            {
                while ($self_ev_result = mysqli_fetch_array($self_ev_query))
                {
                    $self_ev_satisfaction 	= $self_ev_result['satisfaction'];
                    $self_ev_education 		= $self_ev_result['education'];
                    $self_ev_communication 	= $self_ev_result['communication'];
                }
                $is_self_ev = true;
            }
        }

        $result_state="평가전";
        if($relation_result['result_essential'] == 0 || $relation_result['result_essential'] == ""){ // 총 필수입력 평가 수가 0인 경우 "평가상태 = 평가전"
            $result_state="평가전";
        }else if($relation_result['result_essential'] >= $relation_result['total_essential']){ // 총 필수입력 평가 수가 평가 항목의 총 필수입력 수 이상일 경우 "평가상태 = 평가완료"
            $result_state="평가완료";
        }elseif($relation_result['result_essential'] < $relation_result['total_essential']){  // 총 필수입력 평가 수가 평가 항목의 총 필수입력 수 보다 적을 경우 "평가상태 = 평가중"
            $result_state="평가중";
        }


        $evaluation_relation_list[]=array(
            "ev_r_no"			=> $relation_result['ev_r_no'],
            "receiver_s_no"		=> $relation_result['receiver_s_no'],
            "receiver_s_name"	=> $relation_result['receiver_s_name'],
            "evaluator_s_no"	=> $relation_result['evaluator_s_no'],
            "evaluator_s_name"	=> $relation_result['evaluator_s_name'],
            "ev_u_set_no"		=> $relation_result['ev_u_set_no'],
            "result_state"		=> $result_state,
            "set_subject"		=> nl2br($relation_result['set_subject']),
            "set_description"	=> nl2br($relation_result['set_description'])
        );
    }
    $smarty->assign("current_receiver_name", $current_receiver_name);
    $smarty->assign("is_self_ev", $is_self_ev);
    $smarty->assign("self_ev_satisfaction", $self_ev_satisfaction);
    $smarty->assign("self_ev_education", $self_ev_education);
    $smarty->assign("self_ev_communication", $self_ev_communication);

    // 평가 시스템에 설정된 평가지 종류
    $evaluation_relation_sql="SELECT
															ev_u_set_no
														FROM evaluation_system
														WHERE ev_no='{$ev_no}'
														";

    $evaluation_relation_result=mysqli_query($my_db,$evaluation_relation_sql);
    while($relation_result=mysqli_fetch_array($evaluation_relation_result)){
        // 평가지 쿼리
        $evaluation_unit_sql="SELECT
									ev_u.ev_u_no,
									ev_u.ev_u_set_no,
									ev_u.kind,
									ev_u.question,
									ev_u.description,
									ev_u.evaluation_state,
									ev_u.choice_items,
									ev_u.essential
								FROM evaluation_unit ev_u
								LEFT JOIN evaluation_unit_set ev_u_set ON ev_u_set.ev_u_set_no = ev_u.ev_u_set_no
								WHERE ev_u.ev_u_set_no='{$relation_result['ev_u_set_no']}' AND ev_u.active='1'
								ORDER BY ev_u.order ASC
								";

        $evaluation_unit_result=mysqli_query($my_db,$evaluation_unit_sql);
        while($unit_array=mysqli_fetch_array($evaluation_unit_result)) {
            $evaluation_unit_list[]=array(
                "ev_u_no"=>trim($unit_array['ev_u_no']),
                "ev_u_set_no"=>trim($unit_array['ev_u_set_no']),
                "kind"=>trim($unit_array['kind']),
                "question"=>trim($unit_array['question']),
                "description"=>nl2br($unit_array['description']),
                "evaluation_state_name"=>$evaluation_state[searchArray('1', $unit_array['evaluation_state'], $evaluation_state)][0],
                "evaluation_state"=>trim($unit_array['evaluation_state']),
                "choice_items"=>trim($unit_array['choice_items']),
                "choice_items_explode"=>explode('||', $unit_array['choice_items']),
                "essential"=>trim($unit_array['essential'])
            );
        }
    }

    $smarty->assign("ev_relation_list",$evaluation_relation_list);
    $smarty->assign("ev_unit_list",$evaluation_unit_list);

    $evaluation_result_list = [];
    // 평가 시스템 결과 쿼리 - 이미 평가한 경우 평가 값을 불러옴
    $evaluation_system_result_sql="SELECT
															ev_result.ev_result_no,
	 														ev_result.ev_u_no,
															ev_result.evaluation_state,
															ev_result.evaluation_value
														FROM evaluation_system_result ev_result
														WHERE ev_result.ev_r_no='{$ev_r_no}'
														";
    $evaluation_system_result=mysqli_query($my_db,$evaluation_system_result_sql);
    while($system_result = mysqli_fetch_array($evaluation_system_result)){
        $e_value = "";
        if($system_result['evaluation_state'] == '9'){ // 객관형(복수)
            $e_value = explode('||', $system_result['evaluation_value']);
        }else{
            $e_value = $system_result['evaluation_value'];
        }

        $evaluation_result_list[$system_result['ev_u_no']]=array( //ev_u_no를 array index 로 함
            "ev_result_no"=>$system_result['ev_result_no'],
            "ev_u_no"=>$system_result['ev_u_no'],
            "evaluation_value"=>$e_value
        );
    }

    $smarty->assign("ev_result_list",$evaluation_result_list);

    //$evaluation_result_list의 값이 없으면 등록(write)으로, 없으면 수정(modify)으로 mode에 저장
    $mode=(!$evaluation_result_list) ? "write":"modify";
    $smarty->assign("mode",$mode);

}elseif($ev_u_set_no){ // 평가지 보기
    // 평가지 세트 쿼리
    $evaluation_unit_set_sql="SELECT
	 														ev_u_set.subject,
															ev_u_set.description
														FROM evaluation_unit_set ev_u_set
														WHERE ev_u_set.ev_u_set_no='{$ev_u_set_no}'
														";
    $evaluation_unit_set_result=mysqli_query($my_db,$evaluation_unit_set_sql);
    $unit_set_result = mysqli_fetch_array($evaluation_unit_set_result);

    $smarty->assign("set_subject",$unit_set_result['subject']);
    $smarty->assign("set_description",nl2br($unit_set_result['description']));

    // 평가지 쿼리
    $evaluation_unit_sql="SELECT
													ev_u.ev_u_no,
													ev_u.ev_u_set_no,
													ev_u.kind,
													ev_u.question,
													ev_u.description,
													ev_u.evaluation_state,
													ev_u.choice_items,
													ev_u.essential
												FROM evaluation_unit ev_u
												WHERE ev_u.ev_u_set_no='{$ev_u_set_no}' AND ev_u.active='1'
												ORDER BY ev_u.order ASC
												";

    $evaluation_unit_result=mysqli_query($my_db,$evaluation_unit_sql);
    while($unit_array=mysqli_fetch_array($evaluation_unit_result)) {
        $evaluation_unit_list[]=array(
            "ev_u_no"=>trim($unit_array['ev_u_no']),
            "ev_u_set_no"=>trim($unit_array['ev_u_set_no']),
            "kind"=>trim($unit_array['kind']),
            "question"=>trim($unit_array['question']),
            "description"=>nl2br($unit_array['description']),
            "evaluation_state_name"=>$evaluation_state[searchArray('1', $unit_array['evaluation_state'], $evaluation_state)][0],
            "evaluation_state"=>trim($unit_array['evaluation_state']),
            "choice_items"=>trim($unit_array['choice_items']),
            "choice_items_explode"=>explode('||', $unit_array['choice_items']),
            "essential"=>trim($unit_array['essential'])
        );
    }
    $smarty->assign("ev_unit_list",$evaluation_unit_list);
}

$smarty->display('evaluation_unit_set.html');

?>
