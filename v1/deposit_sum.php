<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "58";
$nav_title   = "매출 현황 (미디어커머스 매출 외)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where = " dp.display = '1'";

$sch_incentive_type = isset($_GET['sch_incentive_type']) ? $_GET['sch_incentive_type'] : "1";
$sch_incentive_mon 	= isset($_GET['sch_incentive_mon']) ? $_GET['sch_incentive_mon'] : date("Y-m");
$sch_incentive_year = isset($_GET['sch_incentive_year']) ? $_GET['sch_incentive_year'] : date("Y");

$smarty->assign("sch_incentive_type", $sch_incentive_type);
$smarty->assign("sch_incentive_year", $sch_incentive_year);
$smarty->assign("sch_incentive_mon", $sch_incentive_mon);

$smarty->display('deposit_sum.html');
?>
