<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');
require('inc/model/MyCompany.php');
require('inc/model/Team.php');

$chk_month_sql      = "SELECT DATE_FORMAT(cs_s_date, '%Y') as cs_month FROM calendar_schedule WHERE cal_id='hr_boost' AND cal_no='7' GROUP BY cs_month ORDER BY cs_month";
$chk_month_query    = mysqli_query($my_db, $chk_month_sql);
$chk_month_list     = [];
$chk_max_year       = date("Y");
while($chk_month = mysqli_fetch_assoc($chk_month_query)){
    $chk_month_list[$chk_month['cs_month']] = $chk_month['cs_month']."년";
    $chk_max_year = $chk_month['cs_month'];
}

$is_work_option = array("1" => "O", "2" => "X");

# 검색 처리
$add_where          = "1=1 AND cal_id='hr_boost' AND cal_no='7'";
$sch_year           = isset($_GET['sch_year']) ? $_GET['sch_year'] : $chk_max_year;
$sch_is_work        = isset($_GET['sch_is_work']) ? $_GET['sch_is_work'] : "";
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_team           = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no           = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";

if(!empty($sch_year)){
    $add_where .= " AND DATE_FORMAT(`cs`.cs_s_date,'%Y')='{$sch_year}'";
    $smarty->assign("sch_year", $sch_year);
}

if(!empty($sch_is_work)){
    if($sch_is_work == '1'){
        $add_where .= " AND (SELECT COUNT(*) FROM calendar_schedule_comment `css` WHERE `css`.cs_no=`cs`.cs_no) > 0";
    }elseif($sch_is_work == '2'){
        $add_where .= " AND (SELECT COUNT(*) FROM calendar_schedule_comment `css` WHERE `css`.cs_no=`cs`.cs_no) = 0";
    }
    $smarty->assign("sch_is_work", $sch_is_work);
}

if(!empty($sch_my_c_no)){
    $add_where   .= " AND `s`.my_c_no = '{$sch_my_c_no}'";
    $smarty->assign('sch_my_c_no', $sch_my_c_no);
}

$team_model         = Team::Factory();
$team_all_list      = $team_model->getTeamAllList();
$staff_team_list    = $team_all_list['staff_list'];
$team_full_name_list= $team_model->getTeamFullNameList();
$sch_staff_list      = [];

if (!empty($sch_team))
{
    if ($sch_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        $add_where 		 .= " AND `s`.team IN({$sch_team_code_where})";
        $sch_staff_list   = $staff_team_list[$sch_team];
    }
    $smarty->assign("sch_team", $sch_team);
}

if (!empty($sch_s_no))
{
    if ($sch_s_no != "all") {
        $add_where .= " AND `cs`.cs_s_no='{$sch_s_no}'";
    }
    $smarty->assign("sch_s_no", $sch_s_no);
}

# 리스트 쿼리
$waple_holiday_sql = "
    SELECT 
        `cs`.cs_s_no, 
        `s`.my_c_no,
        `s`.team,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`cs`.cs_s_no) AS cs_s_name, 
        DATE_FORMAT(`cs`.cs_s_date, '%Y%m%d') as cs_chk_date, 
        (SELECT COUNT(*) FROM calendar_schedule_comment `css` WHERE `css`.cs_no=`cs`.cs_no) AS css_cnt
    FROM calendar_schedule `cs`
    LEFT JOIN staff s ON s.s_no=`cs`.cs_s_no
    WHERE {$add_where}
    ORDER BY `cs`.cs_s_date
";
$waple_holiday_query    = mysqli_query($my_db, $waple_holiday_sql);
$waple_holiday_list     = [];
$cur_date               = date("Ymd");
while($waple_holiday = mysqli_fetch_assoc($waple_holiday_query))
{
    $waple_holiday['is_agree']  = "O";
    $waple_holiday['cs_date']   = date("Y-m-d", strtotime($waple_holiday['cs_chk_date']));
    $waple_holiday['is_work']   = "";

    if($waple_holiday['cs_chk_date'] <= $cur_date){
        $waple_holiday['is_work'] = $waple_holiday['css_cnt'] > 0 ? "O" : "X";
    }

    $waple_holiday_list[] = $waple_holiday;
}

$my_company_model = MyCompany::Factory();
$my_company_list  = $my_company_model->getNameList();

$smarty->assign("chk_month_list", $chk_month_list);
$smarty->assign("is_work_option", $is_work_option);
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("my_company_list", $my_company_list);
$smarty->assign("waple_holiday_list", $waple_holiday_list);

$smarty->display('waple_holiday_management.html');
?>
