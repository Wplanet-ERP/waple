<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

require('Classes/PHPExcel.php');
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/model/WorkCms.php');
require('inc/model/Custom.php');

# 기본 엑셀 변수 설정
$order_model    = WorkCms::Factory();
$regdate        = date('Y-m-d H:i:s');
$upload_type    = isset($_POST['new_excel_type']) ? $_POST['new_excel_type'] : "";
$coupon_file    = $_FILES["coupon_file"];

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($coupon_file['name']) && !empty($coupon_file['name'])){
    $upload_file_path = add_store_file($coupon_file, "upload_management");
    $upload_file_name = $coupon_file['name'];
}

$upload_data  = array(
    "upload_type"   => $upload_type,
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $session_s_no,
    "regdate"       => $regdate,
);
$upload_model->insert($upload_data);

$file_no         = $upload_model->getInsertId();
$excel_file_path = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($excel_file_path);
$excel->setActiveSheetIndex(0);

$objWorksheet   = $excel->getActiveSheet();
$totalRow       = $objWorksheet->getHighestRow();
$ins_data_list  = [];

# DB 데이터 시작
for ($i = 2; $i <= $totalRow; $i++)
{
    $order_number   = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //주문번호
    $shop_ord_no    = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //상품주문번호
    $coupon_price   = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //할인액
    $coupon_type    = empty($shop_ord_no) ? 2 : 1;


    if($coupon_type == '1')
    {
        $chk_ord_sql    = "SELECT COUNT(*) as cnt FROM work_cms_coupon WHERE order_number='{$order_number}' AND shop_ord_no='{$shop_ord_no}' AND `coupon_type`='1'";
        $chk_ord_query  = mysqli_query($my_db, $chk_ord_sql);
        $chk_ord_result = mysqli_fetch_assoc($chk_ord_query);

        if($chk_ord_result['cnt'] == 0)
        {
            $ins_data_list[] = array(
                "order_number"  => $order_number,
                "shop_ord_no"   => $shop_ord_no,
                "coupon_type"   => $coupon_type,
                "coupon_price"  => $coupon_price,
                "file_no"       => $file_no
            );
        }
    }
    elseif($coupon_type == '2')
    {
        $ord_sql    = "SELECT shop_ord_no, prd_no FROM work_cms WHERE order_number='{$order_number}' AND unit_price > 0 AND (shop_ord_no != '' AND shop_ord_no IS NOT NULL)";
        $ord_query  = mysqli_query($my_db, $ord_sql);
        $ord_list   = [];
        while($ord_result = mysqli_fetch_assoc($ord_query))
        {
            $chk_ord_sql    = "SELECT COUNT(*) as cnt FROM work_cms_coupon WHERE order_number='{$order_number}' AND shop_ord_no='{$ord_result['shop_ord_no']}' AND `coupon_type`='2'";
            $chk_ord_query  = mysqli_query($my_db, $chk_ord_sql);
            $chk_ord_result = mysqli_fetch_assoc($chk_ord_query);

            if($chk_ord_result['cnt'] == 0)
            {
                $ord_list[$ord_result['shop_ord_no']] = $ord_result['shop_ord_no'];
            }
        }

        if(!empty($ord_list))
        {
            $ord_count          = count($ord_list);
            $org_coupon_price   = $coupon_price;
            $unit_coupon_price  = (int)($coupon_price/$ord_count);

            $ord_idx = 1;
            foreach($ord_list as $shop_ord)
            {
                $org_coupon_price  -= $unit_coupon_price;
                $cal_coupon_price   = $unit_coupon_price;

                if($ord_idx == $ord_count){
                    $cal_coupon_price = $unit_coupon_price+$org_coupon_price;
                }

                $ins_data_list[] = array(
                    "order_number"  => $order_number,
                    "shop_ord_no"   => $shop_ord,
                    "coupon_type"   => $coupon_type,
                    "coupon_price"  => $cal_coupon_price,
                    "file_no"       => $file_no
                );
            }
        }
    }
}

if(!empty($ins_data_list))
{
    $order_model->setMainInit('work_cms_coupon', 'order_number');
    if($order_model->multiInsert($ins_data_list)) {
        exit("<script>alert('택배리스트에 반영 되었습니다.');location.href='work_list_cms.php?sch_ord_bundle=1&sch_reg_s_date=&sch_reg_e_date=&sch_file_no={$file_no}';</script>");
    }else{
        if(!empty($upload_file_path)) {
            $upload_model->delete($file_no);
            del_file($upload_file_path);
        }
        exit("<script>alert('택배리스트 반영에 실패했습니다.');location.href='waple_upload_management.php';</script>");
    }
}
else{
    if(!empty($upload_file_path)) {
        $upload_model->delete($file_no);
        del_file($upload_file_path);
    }
    exit("<script>alert('택배리스트 반영할 데이터가 없습니다.');location.href='waple_upload_management.php';</script>");
}
?>
