<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/promotion.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Staff.php');
require('inc/model/MyCompany.php');
require('inc/model/Work.php');
require('inc/model/Withdraw.php');
require('inc/model/Promotion.php');

$is_editable = false;
if(permissionNameCheck($session_permission, "마스터관리자") || $session_s_no == '61'){
    $is_editable = true;
}
$smarty->assign("is_editable", $is_editable);

# 프로세스 처리
$process 		 	= (isset($_POST['process'])) ? $_POST['process'] : "";
$application_model 	= Promotion::Factory();
$company_model 		= Company::Factory();
$work_model			= Work::Factory();
$application_model->setMainInit("application", "a_no");

if ($process == "wd_due_date")
{
	$a_no 		= (isset($_POST['a_no'])) ? $_POST['a_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	$upd_data 	= array("a_no" => $a_no, "wd_due_date" => empty($value) ? "NULL" : $value);

	if (!$application_model->update($upd_data))
		echo "지급요청일 저장에 실패 하였습니다.";
	else
		echo "지급요청일이 저장 되었습니다.";
	exit;
}
elseif($process == "save_bk")
{
	$a_no 		= (isset($_POST['a_no'])) ? $_POST['a_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$zipcode 	= $_POST['mb_zip1']."-".$_POST['mb_zip2'];
	$bk_jumin   = (isset($_POST['bk_jumin'])) ? addslashes(trim($_POST['bk_jumin'])) : "";
	$bk_title   = (isset($_POST['bk_title'])) ? addslashes(trim($_POST['bk_title'])) : "";
	$bk_num   	= (isset($_POST['bk_num'])) ? addslashes(trim($_POST['bk_num'])) : "";
	$bk_name   	= (isset($_POST['bk_name'])) ? addslashes(trim($_POST['bk_name'])) : "";

	if (empty($a_no)) {
		exit("<script>alert('보상 정보가 업습니다. 입금정보를 저장에 실패 하였습니다');location.href='reward_list.php?{$search_url}';</script>");
	}

	$upd_data   = array("a_no" => $a_no, "bk_jumin" => $bk_jumin, "bk_title" => $bk_title, "bk_num" => $bk_num, "bk_name" => $bk_name);

	if(!$application_model->update($upd_data)){
		exit ("<script>alert('주민번호,은행명,계좌번호,예금주등 입금정보를 저장에 실패 하였습니다');location.href='reward_list.php?{$search_url}';</script>");
	}else{
		exit ("<script>alert('주민번호,은행명,계좌번호,예금주등 입금정보를 저장 하였습니다');location.href='reward_list.php?{$search_url}';</script>");
	}
}
elseif($process == "refund")
{
	$a_no		= (isset($_POST['application_no'])) ? $_POST['application_no'] : "";
	$search_url	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$upd_data 	= array(
		"a_no"			=> $a_no,
		"refund_count" 	=> $_POST['selection_refund']
	);

	if (!$application_model->update($upd_data)){
		exit ("<script>alert('보상 정보를 저장에 실패 하였습니다');location.href='reward_list.php?{$search_url}';</script>");
	}else{
		exit ("<script>location.href='reward_list.php?{$search_url}';</script>");
	}
}
elseif($process == "save_tax")
{
	$a_no 				= (isset($_POST['a_no'])) ? $_POST['a_no'] : "";
	$search_url			= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$reward_cost_value 	= str_replace(",","", trim($_POST['f_reward_cost'])); // 보상지급액(소득세 별도) 컴마 제거하기
	$biz_tax_value 		= str_replace(",","", trim($_POST['f_biz_tax'])); // 사업소득세 컴마 제거하기
	$local_tax_value 	= str_replace(",","", trim($_POST['f_local_tax'])); // 지방소득세 컴마 제거하기
	$reward_cost_result_value = str_replace(",","", trim($_POST['f_reward_cost_result'])); // 보상지급액(소득세 포함) 컴마 제거하기

	$upd_data 			= array("a_no" => $a_no, "reward_cost" => $reward_cost_value, "biz_tax" => $biz_tax_value, "local_tax" => $local_tax_value, "reward_cost_result" => $reward_cost_result_value);

	if (!$application_model->update($upd_data)){
		exit ("<script>alert('소득세 저장에 실패 하였습니다');location.href='reward_list.php?{$search_url}';</script>");
	}else{
		exit ("<script>location.href='reward_list.php?{$search_url}';</script>");
	}
}
elseif($process == "save_tax_select")
{
	$search_url			= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$a_no_list 			= (isset($_POST['select_a_no_list'])) ? $_POST['select_a_no_list'] : "";
	$reward_cost_list 	= (isset($_POST['select_reward_cost_list'])) ? $_POST['select_reward_cost_list'] : "";
	$biz_tax_list 		= (isset($_POST['select_biz_tax_list'])) ? $_POST['select_biz_tax_list'] : "";
	$local_tax_list 	= (isset($_POST['select_local_tax_list'])) ? $_POST['select_local_tax_list'] : "";
	$reward_cost_result_list = (isset($_POST['select_reward_cost_result_list'])) ? $_POST['select_reward_cost_result_list'] : "";

	$a_no_tok 			= explode("||", $a_no_list);
	$reward_cost_tok 	= explode("||", str_replace(",","",trim($reward_cost_list)));
	$biz_tax_tok 		= explode("||", str_replace(",","",trim($biz_tax_list)));
	$local_tax_tok 		= explode("||", str_replace(",","",trim($local_tax_list)));
	$reward_cost_result_tok = explode("||", str_replace(",","",trim($reward_cost_result_list)));

	$add_set_case = "";

	// reward_cost
	$add_set_case .= " reward_cost=	CASE a_no ";
	for($tok_i = 0 ; $tok_i < count($reward_cost_tok) ; $tok_i++){
		if(!!$reward_cost_tok[$tok_i] || $reward_cost_tok[$tok_i] == '0'){
			$add_set_case .= " WHEN '{$a_no_tok[$tok_i]}' THEN '{$reward_cost_tok[$tok_i]}' ";
		}else{
			$add_set_case .= " WHEN '{$a_no_tok[$tok_i]}' THEN reward_cost ";
		}
	}
	$add_set_case .= " END, ";

	// biz_tax
	$add_set_case .= " biz_tax=	CASE a_no ";
	for($tok_i = 0 ; $tok_i < count($biz_tax_tok) ; $tok_i++){
		if(!!$biz_tax_tok[$tok_i] || $biz_tax_tok[$tok_i] == '0'){
			$add_set_case .= " WHEN '{$a_no_tok[$tok_i]}' THEN '{$biz_tax_tok[$tok_i]}' ";
		}else{
			$add_set_case .= " WHEN '{$a_no_tok[$tok_i]}' THEN biz_tax ";
		}
	}
	$add_set_case .= " END, ";

	// local_tax
	$add_set_case .= " local_tax=	CASE a_no ";
	for($tok_i = 0 ; $tok_i < count($local_tax_tok) ; $tok_i++){
		if(!!$local_tax_tok[$tok_i] || $local_tax_tok[$tok_i] == '0'){
			$add_set_case .= " WHEN '{$a_no_tok[$tok_i]}' THEN '{$local_tax_tok[$tok_i]}' ";
		}else{
			$add_set_case .= " WHEN '{$a_no_tok[$tok_i]}' THEN local_tax ";
		}
	}
	$add_set_case .= "END,";

	// reward_cost_result
	$add_set_case .= " reward_cost_result=	CASE a_no ";
	for($tok_i = 0 ; $tok_i < count($reward_cost_result_tok) ; $tok_i++){
		if(!!$reward_cost_result_tok[$tok_i] || $reward_cost_result_tok[$tok_i] == '0'){
			$add_set_case .= " WHEN '{$a_no_tok[$tok_i]}' THEN '{$reward_cost_result_tok[$tok_i]}' ";
		}else{
			$add_set_case .= " WHEN '{$a_no_tok[$tok_i]}' THEN reward_cost_result ";
		}
	}
	$add_set_case .= " END ";

	// where in a_no
	$add_set_case .= " WHERE a_no IN ( ";
	for($tok_i = 0 ; $tok_i < count($a_no_tok) ; $tok_i++){

		if(!!$a_no_tok[$tok_i]){
			if($tok_i > 0)
				$add_set_case .= " , '{$a_no_tok[$tok_i]}' ";
			else
				$add_set_case .= " '{$a_no_tok[$tok_i]}' ";
		}
	}
	$add_set_case .= " ) ";

	$sql = "UPDATE application SET {$add_set_case}";

	if (!mysqli_query($my_db, $sql))
		exit ("<script>alert('저장에 실패 하였습니다.\\n담당자에게 문의해 주세요.');history.back();</script>");
	else
		echo ("<script>alert('모두 저장 하였습니다.');</script>");

	exit ("<script>location.href='reward_list.php?{$search_url}';</script>");
}
elseif($process == "del_personal_info")  // 보상정보 삭제
{
	$search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$prev_date  = date("Y-m-d", strtotime("-1 years"))." 00:00:00";
	$affect_cnt	= 0;

	$del_rep_sql = "UPDATE report SET bk_title=NULL, bk_num=NULL, bk_name=NULL, bk_jumin=NULL WHERE a_no IN(SELECT a.a_no FROM application a WHERE a.regdate < '{$prev_date}')";
	$del_app_sql = "UPDATE application SET cafe_id=NULL, hp='', email='', bk_title=NULL, bk_num=NULL, bk_name=NULL, bk_jumin=NULL, zipcode=NULL, address1=NULL, address2=NULL WHERE regdate < '{$prev_date}'";

	if(mysqli_query($my_db, $del_rep_sql)){
		$affect_cnt += mysqli_affected_rows($my_db);
		if(mysqli_query($my_db, $del_app_sql)){
			$affect_cnt += mysqli_affected_rows($my_db);
		}

		$wplanet_c_no  	= 1580;
		$company_item  	= $company_model->getItem($wplanet_c_no);
		$task_run 		= "[탑블로그 보상 개인정보 삭제]\r\n처리내용 : 등록일로 부터 1년 이후의 주민번호,은행,계좌,예금주,휴대폰번호,이메일,주소 정보 삭제\r\n처리건 : ".number_format($affect_cnt)." 건 삭제완료";
		$regdate 		= date("Y-m-d H:i:s");
		$work_data 		= array(
			"work_state" 		=> "6",
			"k_name_code"		=> "02435",
			"c_no"				=> $company_item['c_no'],
			"c_name"            => $company_item["c_name"],
			"s_no"				=> $company_item['s_no'],
			"team"				=> $company_item['team'],
			"prd_no"            => "287",
			"task_req_s_no"     => $session_s_no,
			"task_req_team"     => $session_team,
			"quantity"			=> 1,
			"regdate"           => $regdate,
			"task_run"          => addslashes(trim($task_run)),
			"task_run_s_no"     => $session_s_no,
			"task_run_team"     => $session_team,
			"task_run_regdate"	=> $regdate,
		);
		$work_model->insert($work_data);

		exit ("<script>alert('기간내 개인정보를 삭제 하였습니다');location.href='reward_list.php?{$search_url}';</script>");
	}else{
		exit ("<script>alert('기간내 개인정보를 삭제에 실패하였습니다');location.href='reward_list.php?{$search_url}';</script>");
	}
    exit ("<script>location.href='reward_list.php?{$search_url}';</script>");

}
elseif($process == "del_bk_info")  # 보상정보 삭제(개별)
{
    $search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
    $a_no 		= (isset($_POST['a_no'])) ? $_POST['a_no'] : "";
	$del_data   = array("a_no" => $a_no, "bk_title" => "NULL", "bk_name" => "NULL", "bk_jumin" => "NULL");

    if (empty($a_no)) {
		exit ("<script>alert('보상 정보가 업습니다. 입금정보 삭제에 없습니다');location.href='reward_list.php?{$search_url}';</script>");
    }

	if(!$application_model->update($del_data)){
		exit ("<script>alert('주민번호,은행명,계좌번호,예금주등 입금정보를 삭제에 실패하였습니다');location.href='reward_list.php?{$search_url}';</script>");
	}else{
		exit ("<script>alert('주민번호,은행명,계좌번호,예금주등 입금정보를 삭제 하였습니다');location.href='reward_list.php?{$search_url}';</script>");
	}
}
elseif($process == "save_due_date_select")
{
	$search_url 	 = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$a_no_list 		 = (isset($_POST['select_a_no_list'])) ? $_POST['select_a_no_list'] : [];
	$select_due_date = (isset($_POST['select_due_date'])) ? $_POST['select_due_date'] : "";

	$due_result 	 = true;
	if(!empty($a_no_list) && !empty($select_due_date))
	{
		$a_no_tok = explode("||", $a_no_list);
		if(!empty($a_no_tok)){
			foreach($a_no_tok as $a_no_data)
			{
                $upd_chk	   	= false;
				$upd_chk_sql 	= "SELECT bk_jumin, bk_title, bk_num, bk_name FROM application WHERE a_no='{$a_no_data}' LIMIT 1";
				$upd_chk_query 	= mysqli_query($my_db, $upd_chk_sql);
				$upd_chk_result = mysqli_fetch_assoc($upd_chk_query);

				if(!empty($upd_chk_result['bk_jumin']) &&!empty($upd_chk_result['bk_title']) && !empty($upd_chk_result['bk_num']) && !empty($upd_chk_result['bk_name'])){
                    $upd_chk = true;
				}

				if($upd_chk)
				{
                    $upd_sql = "UPDATE application a SET a.wd_due_date = '{$select_due_date}' WHERE a.a_no='{$a_no_data}'";
                    if(!mysqli_query($my_db, $upd_sql)){
                        $due_result = false;
                    }
				}
			}
		}
	}else{
		exit ("<script>alert('선택된 데이터 정보가 없습니다. 다시 시도해 주세요');location.href='reward_list.php?{$search_url}';</script>");
	}

	if(!$due_result){
		exit ("<script>alert('지급요청일 선택변경에 실패했습니다');location.href='reward_list.php?{$search_url}';</script>");
	}else{
		exit ("<script>alert('지급요청일 선택변경에 성공했습니다');location.href='reward_list.php?{$search_url}';</script>");
	}
}
elseif($process == "save_reward_work")
{
	$select_p_no_list 	= (isset($_POST['select_p_no_list'])) ? $_POST['select_p_no_list'] : "";
	$search_url			= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	# 검색적용
	$add_reward_where	= "p.p_no=a.p_no AND (p.kind='2' OR p.kind='4' OR p.kind='5') AND a.a_state='1' AND (SELECT COUNT(*) FROM report r WHERE r.p_no = a.p_no AND r.a_no = a.a_no) > 0";
	$select_date 		= isset($_POST['select_date']) ? $_POST['select_date'] : "";
	$select_refund      = isset($_POST['select_refund']) ? $_POST['select_refund'] : "n";
	$select_tax_set     = isset($_POST['select_tax_set']) ? $_POST['select_tax_set'] : "";

	if(!empty($select_date)){
		$add_reward_where .= " AND a.wd_due_date='{$select_date}'";
	}

	if(!empty($select_tax_set)) {
		if($select_tax_set == "y") { // 소득세 저장완료
			$add_reward_where .= " AND ((a.reward_cost IS NOT NULL AND a.reward_cost<>'0') AND a.biz_tax IS NOT NULL AND a.local_tax IS NOT NULL AND (a.reward_cost_result IS NOT NULL AND a.reward_cost_result<>'0'))";
		}elseif($select_tax_set == "n"){ // 소득세 저장 미완료
			$add_reward_where .= " AND ((a.reward_cost IS NULL OR a.reward_cost='0') OR a.biz_tax IS NULL OR a.local_tax IS NULL OR (a.reward_cost_result IS NULL OR a.reward_cost_result='0'))";
		}
	}

	if(!empty($select_refund))
	{
		if($select_refund == "y") {
			$add_reward_where .= " AND a.refund_count >= 1";
		}else if($select_refund == "n") {
			$add_reward_where .= " AND (a.refund_count is NULL or a.refund_count = 0)";
		}
	}

	$complete_p_no  	= "";
	$complete_cnt  		= 0;
	if(!empty($select_p_no_list))
	{
		$withdraw_model	= Withdraw::Factory();
		$sel_p_no 		= str_replace("||", ",", $select_p_no_list);

		$reward_c_no 	= 5306;
		$reward_company = $company_model->getItem($reward_c_no);

		$staff_model 	= Staff::Factory();
		$prd_no			= 264;
		$run_staff		= $staff_model->getItem(61);
		$run_s_no		= $run_staff['s_no'];
		$run_team		= $run_staff['team'];
		$cur_date		= date("Y-m-d H:i:s");

		$channel_option 		= getPromotionChannelOption();
		$channel_work_option 	= getPromoChannelWorkOption();
		$kind_option 			= getPromotionKindOption();

		$reward_sql = "
			SELECT 
				p.channel AS channel,
				p.kind AS kind,
				p.p_no AS p_no,
			   	(SELECT c.my_c_no FROM company c WHERE c.c_no=p.c_no) AS my_c_no,
				p.c_no AS c_no,
				p.company AS c_name,
				p.s_no AS s_no,
				p.team AS team,
				a.wd_due_date,
				COUNT(a.a_no) AS reward_cnt,
				SUM(a.reward_cost) AS sum_reward_cost,
				SUM(a.biz_tax) AS sum_biz_tax,
				SUM(a.local_tax) AS sum_local_tax,
				SUM(a.reward_cost_result) AS sum_reward_cost_result
			FROM promotion p, application a
			WHERE {$add_reward_where} AND p.p_no IN({$sel_p_no}) 
			GROUP BY p.p_no
		";
		$reward_query = mysqli_query($my_db, $reward_sql);
		while($reward_result = mysqli_fetch_assoc($reward_query))
		{
			$channel_name   = $channel_option[$reward_result['channel']];
			$channel  		= (string)$channel_work_option[$reward_result['channel']];
			$campaign_name	= $kind_option[$reward_result['kind']];
			$task_run  		= "마케팅채널 = {$channel_name}\r\n캠페인 종류 = {$campaign_name}\r\np_no = {$reward_result['p_no']}";
			
			$work_data = array(
				"work_state" 		=> "6",
				"k_name_code" 		=> $channel,
				"c_no"				=> $reward_result['c_no'],
				"c_name"            => $reward_result["c_name"],
				"s_no"				=> $reward_result['s_no'],
				"team"				=> $reward_result['team'],
				"prd_no"            => $prd_no,
				"task_req_s_no"     => $reward_result["s_no"],
				"task_req_team"     => $reward_result['team'],
				"quantity"			=> $reward_result['reward_cnt'],
				"wd_price"			=> $reward_result['sum_reward_cost'],
				"wd_price_vat"		=> $reward_result['sum_reward_cost_result'],
				"wd_c_no"			=> $reward_company['c_no'],
				"wd_c_name"			=> $reward_company['c_name'],
				"regdate"           => $cur_date,
				"task_run"          => addslashes(trim($task_run)),
				"task_run_s_no"     => $run_s_no,
				"task_run_team"     => $run_team,
				"task_run_regdate"	=> $reward_result['wd_due_date'],
			);

			if($work_model->insert($work_data))
			{
				$new_w_no = $work_model->getInsertId();
				$withdraw_data = array(
					"my_c_no"           => $reward_company["my_c_no"],
					"wd_subject"        => "탑블로그 보상 지급",
					"wd_state"          => "3",
					"wd_method"         => "4",
					"wd_account"        => 3,
					"w_no"              => $new_w_no,
					"c_no"              => $reward_company["c_no"],
					"c_name"            => $reward_company["c_name"],
					"s_no"              => $reward_result["s_no"],
					"team"              => $reward_result["team"],
					"supply_cost"       => $reward_result["sum_reward_cost"],
					"vat_choice"        => "2",
					"biz_tax"   		=> -$reward_result["sum_biz_tax"],
					"local_tax"   		=> -$reward_result["sum_local_tax"],
					"cost"              => $reward_result["sum_reward_cost_result"],
					"wd_money"          => $reward_result["sum_reward_cost_result"],
					"wd_tax_state"      => "4",
					"regdate"           => $cur_date,
					"wd_date"           => $reward_result["wd_due_date"],
					"reg_s_no"          => $run_s_no,
					"reg_team"          => $run_team,
				);

				if($withdraw_model->insert($withdraw_data))
				{
					$last_wd_no 	= $withdraw_model->getInsertId();
					$upd_work_data  = array("w_no" => $new_w_no, "wd_no" => $last_wd_no);
					$work_model->update($upd_work_data);

					$complete_p_no .= !empty($complete_p_no) ? ", {$reward_result['p_no']}" : $reward_result['p_no'];
					$complete_cnt++;
				}
			}
		}
	}
	else {
		exit ("<script>alert('선택된 데이터 정보가 없습니다. 다시 시도해 주세요');location.href='reward_list.php?{$search_url}';</script>");
	}

	exit ("<script>alert('P_NO ({$complete_p_no}) 총 {$complete_cnt}건 인센티브정산 처리했습니다.');location.href='reward_list.php?{$search_url}';</script>");
}

# Navigation & My Quick
$nav_prd_no  = "113";
$nav_title   = "보상 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색
$add_where				= "p.p_no=a.p_no AND (p.kind='2' OR p.kind='4' OR p.kind='5') AND a.a_state='1' AND (SELECT COUNT(*) FROM report r WHERE r.p_no = a.p_no AND r.a_no = a.a_no) > 0";
$sch_my_c_no 			= isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_kind	 			= isset($_GET['sch_kind']) ? $_GET['sch_kind'] : "";
$sch_state				= isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_code				= isset($_GET['sch_code']) ? $_GET['sch_code'] : "";
$sch_company			= isset($_GET['sch_company']) ? $_GET['sch_company'] : "";
$sch_reward_date		= isset($_GET['sch_reward_date']) ? $_GET['sch_reward_date'] : "";
$sch_bk_name			= isset($_GET['sch_bk_name']) ? $_GET['sch_bk_name'] : "";
$sch_bk_num_detail		= isset($_GET['sch_bk_num_detail']) ? $_GET['sch_bk_num_detail'] : "";
$sch_bk_num				= isset($_GET['sch_bk_num']) ? $_GET['sch_bk_num'] : "";
$sch_wd_due_date		= isset($_GET['sch_wd_due_date']) ? $_GET['sch_wd_due_date'] : "";
$sch_wd_due_date_null	= isset($_GET['sch_wd_due_date_null']) ? $_GET['sch_wd_due_date_null'] : "";
$sch_tax_set			= isset($_GET['sch_tax_set']) ? $_GET['sch_tax_set'] : "";
$sch_refund				= isset($_GET['sch_refund']) ? $_GET['sch_refund'] : "n";
$sch_nick				= isset($_GET['sch_nick']) ? $_GET['sch_nick'] : "";
$sch_username			= isset($_GET['sch_username']) ? $_GET['sch_username'] : "";
$sch_blog_url			= isset($_GET['sch_blog_url']) ? $_GET['sch_blog_url'] : "";

if(!empty($sch_my_c_no)) {
    $add_where .= " AND p.c_no IN(SELECT c.c_no FROM company c WHERE c.my_c_no='{$sch_my_c_no}')";
    $smarty->assign("sch_my_c_no", $sch_my_c_no);
}

if(!empty($sch_kind)) {
	$add_where .= " AND p.kind = '{$sch_kind}'";
	$smarty->assign("sch_kind", $sch_kind);
}

if(!empty($sch_state)) {
	$add_where .= " AND p.p_state = '{$sch_state}'";
	$smarty->assign("sch_state", $sch_state);
}

if(!empty($sch_code)) {
	$add_where .= " AND p.promotion_code like '%{$sch_code}%'";
	$smarty->assign("sch_code", $sch_code);
}

if(!empty($sch_company)) {
	$add_where .= " AND p.company like '%{$sch_company}%'";
	$smarty->assign("sch_company", $sch_company);
}

if(!empty($sch_reward_date)) {
	$add_where .= " AND p.pres_reward = '{$sch_reward_date}'";
	$smarty->assign("sch_reward_date", $sch_reward_date);
}

if(!empty($sch_bk_name)) {
	$add_where .= " AND a.bk_name like '%{$sch_bk_name}%'";
	$smarty->assign("sch_bk_name", $sch_bk_name);
}

if(!empty($sch_bk_num_detail)) {
	$add_where .= " AND a.bk_num = '{$sch_bk_num_detail}'";
	$smarty->assign("sch_bk_num_detail", $sch_bk_num_detail);
}

if(!empty($sch_bk_num)) {
	if($sch_bk_num == 'n'){
		$add_where .= " AND (a.bk_num is null OR a.bk_num = '')";
	}elseif($sch_bk_num == 'y'){
		$add_where .= " AND (a.bk_num is not null AND a.bk_num <> '')";
	}
	$smarty->assign("sch_bk_num", $sch_bk_num);
}

if(!empty($sch_wd_due_date)) {
	$add_where .= " AND a.wd_due_date = '{$sch_wd_due_date}'";
	$smarty->assign("sch_wd_due_date", $sch_wd_due_date);
}

if($sch_wd_due_date_null == 'null') { // wd_due_date is null
	$add_where .= " AND (a.wd_due_date is null OR a.wd_due_date = '')";
	$smarty->assign("sch_wd_due_date_null", $sch_wd_due_date_null);
}

if(!empty($sch_tax_set)) {
	if($sch_tax_set == "y") { // 소득세 저장완료
		$add_where .= " AND ((a.reward_cost IS NOT NULL AND a.reward_cost<>'0') AND a.biz_tax IS NOT NULL AND a.local_tax IS NOT NULL AND (a.reward_cost_result IS NOT NULL AND a.reward_cost_result<>'0'))";
		$smarty->assign("sch_tax_set", $sch_tax_set);
	}elseif($sch_tax_set == "n"){ // 소득세 저장 미완료
		$add_where .= " AND ((a.reward_cost IS NULL OR a.reward_cost='0') OR a.biz_tax IS NULL OR a.local_tax IS NULL OR (a.reward_cost_result IS NULL OR a.reward_cost_result='0'))";
		$smarty->assign("sch_tax_set", $sch_tax_set);
	}
}

if($sch_refund == "y") {
	$add_where .= " AND a.refund_count >= 1";
	$smarty->assign("sch_refund", $sch_refund);
}else if($sch_refund == "n") {
	$add_where .= " AND (a.refund_count is NULL or a.refund_count = 0)";
	$smarty->assign("sch_refund", $sch_refund);
}

if(!empty($sch_nick)) {
	$add_where .= " AND a.nick like '%{$sch_nick}%'";
	$smarty->assign("sch_nick", $sch_nick);
}

if(!empty($sch_username)) {
	$add_where .= " AND a.username like '%{$sch_username}%'";
	$smarty->assign("sch_username", $sch_username);
}

if(!empty($sch_blog_url)) {
	$add_where .= " AND a.blog_url like '%{$sch_blog_url}%'";
	$smarty->assign("sch_blog_url", $sch_blog_url);
}

# 정렬순서
$add_orderby = "";
$order		 = isset($_GET['od']) ? $_GET['od'] : "";
$order_type	 = isset($_GET['by']) ? $_GET['by'] : "";
$toggle		 = $order_type ? "asc" : "desc";
$order_field = array('', 'bk_jumin');

if($order && $order<6) {
	$add_orderby .= "$order_field[$order] $toggle,";
	$smarty->assign("order",$order);
	$smarty->assign("order_type",$order_type);
}
$add_orderby .= " a.p_no DESC, a.a_no ASC";


# 전체 게시물 수 & 페이징처리
$reward_total_sql 		= "SELECT count(*) as cnt FROM promotion p, application a WHERE {$add_where}";
$reward_total_query 	= mysqli_query($my_db, $reward_total_sql);
$reward_total_result 	= mysqli_fetch_array($reward_total_query);
$reward_total 			= $reward_total_result['cnt'];

$search_url 	= getenv("QUERY_STRING");
$page 			= isset($_GET['page']) ? intval($_GET['page']) : 1;
$ord_page_type 	= isset($_GET['ord_page_type']) ? intval($_GET['ord_page_type']) : 10;
$num 			= $ord_page_type;
$offset 		= ($page-1) * $num;
$pagenum 		= ceil($reward_total/$num);

if ($page>=$pagenum){$page=$pagenum;}
if ($page<=0){$page=1;}

$pagelist		= pagelist($page, "reward_list.php", $pagenum, $search_url);

$smarty->assign("total_num", $reward_total);
$smarty->assign("search_url", $search_url);
$smarty->assign("page", $page);
$smarty->assign("ord_page_type", $ord_page_type);
$smarty->assign("pagelist", $pagelist);

# 리스트 쿼리
$reward_sql = "
	SELECT 
		p.p_state AS p_state, 
	   	p.kind AS kind, 
	    p.promotion_code AS promotion_code, 
	    p.c_no AS c_no, 
	    (SELECT c.c_name FROM company AS c WHERE c.c_no=p.c_no) AS c_name, 
	    p.pres_reward AS pres_reward,
		a.a_no,
		a.p_no,
		a.nick,
		a.username,
		a.blog_url,
		a.email,
		a.bk_jumin AS hex_bk_jumin,
		(SELECT AES_DECRYPT(UNHEX(a.bk_jumin), '{$aeskey}')) AS bk_jumin,
		a.bk_title,
		a.bk_num,
		a.bk_name,
		p.pres_money AS pres_money,
		p.posting_num,
		p.reg_num,
		a.refund_count,
		a.wd_due_date,
		a.reward_cost,
		a.biz_tax,
		a.local_tax,
		a.reward_cost_result,
		a.wd_no
	FROM promotion p, application a
	WHERE {$add_where}
	ORDER BY {$add_orderby}
	LIMIT {$offset}, {$num}
";
$reward_query 			= mysqli_query($my_db, $reward_sql);
$tax_extra_list 		= [];
$reward_list 			= [];
$report_list			= [];
$state_name_option 		= getTotalStateOption();
$date_short_option		= getDayShortOption();
$promotion_kind_option	= getPromotionKindOption();
$today 					= date("Y-m-d");

while($reward_result = mysqli_fetch_array($reward_query))
{
	$report_sql 	= "select post_title, post_url from report where p_no='{$reward_result['p_no']}' and a_no='{$reward_result['a_no']}'";
	$report_query	= mysqli_query($my_db, $report_sql);
	while($report_info = mysqli_fetch_array($report_query))
	{
		$report_list[$reward_result['a_no']][] = array
		(
			"post_title"	=> $report_info['post_title'],
			"post_url"		=> $report_info['post_url'],
		);
	}

	$pres_money_total = $reward_result['pres_money'];
	if(($reward_result['posting_num']/$reward_result['reg_num']) >= 2)
		$pres_money_total = $reward_result['pres_money']*($reward_result['posting_num']/$reward_result['reg_num']);

	$tax_apply 			 = FALSE; // 세금적용여부
	$p_no_pres_money_sum = 0;
    if(!isset($tax_extra_list[$reward_result['wd_due_date']][$reward_result['hex_bk_jumin']]))
    {
        $p_biz_tax_extra			 = 0;
        $p_data_tax_extra 			 = 0;
        $p_data_pres_money_total_sum = 0;

        $p_no_total_sql 	= "SELECT p.p_no, a.a_no, ROUND(p.pres_money * (p.posting_num/p.reg_num)) as pres_total FROM promotion p LEFT JOIN application a ON a.p_no = p.p_no WHERE a.wd_due_date='{$reward_result['wd_due_date']}' AND a.bk_jumin='{$reward_result['hex_bk_jumin']}' AND a.a_state='1' ORDER BY p.p_no";
        $p_no_total_result  = mysqli_query($my_db, $p_no_total_sql);
        $p_no_data_list		= [];

        while ($p_data = mysqli_fetch_array($p_no_total_result))
        {
            $p_data_pres_money_total 		= $p_data['pres_total'];
            $p_biz_tax_value				= $p_data_pres_money_total * 0.03;
            $p_biz_tax_floor_value			= floor($p_biz_tax_value / 10) * 10;
            $p_data_tax_value 	 	 		= $p_data_pres_money_total * 0.003;
            $p_data_tax_floor_value 		= floor($p_data_tax_value / 10) * 10;
            $p_data_pres_money_total_sum 	+= $p_data_pres_money_total;
            $p_biz_tax_extra 				+= ($p_biz_tax_value - $p_biz_tax_floor_value);
            $p_data_tax_extra 				+= ($p_data_tax_value - $p_data_tax_floor_value);

            $local_biz_extra_chk_on  		= round((($p_data_pres_money_total*0.03))/10)*10;
            $local_biz_extra_chk_off  		= floor((($p_data_pres_money_total*0.03))/10)*10;
            $local_tax_extra_chk_on  		= round((($p_data_pres_money_total*0.003))/10)*10;
            $local_tax_extra_chk_off 		= floor(($p_data_pres_money_total*0.003)/10)*10;

            $p_no_data_list[$p_data['a_no']] = array(
                "biz_on" 	=>	$local_biz_extra_chk_on,
                "biz_off" 	=>	$local_biz_extra_chk_off,
                "chk_on" 	=>	$local_tax_extra_chk_on,
                "chk_off" 	=>	$local_tax_extra_chk_off
            );
        }

		$bix_extra  	= floor($p_biz_tax_extra / 10) * 10;
		$bix_extra_chk 	= ($p_biz_tax_extra >= 10) ? true : false;
		$extra 			= floor($p_data_tax_extra / 10) * 10;
		$extra_chk 		= ($p_data_tax_extra >= 10) ? true : false;

		foreach($p_no_data_list as $key => $p_no_data)
		{
			$tax_extra_list[$reward_result['wd_due_date']][$reward_result['hex_bk_jumin']]['biz'][$key] = ($bix_extra_chk ? $p_no_data['biz_on'] : $p_no_data['biz_off']);
			$tax_extra_list[$reward_result['wd_due_date']][$reward_result['hex_bk_jumin']]['tax'][$key] = ($extra_chk ? $p_no_data['chk_on'] : $p_no_data['chk_off']);
			$bix_extra  -= ($p_no_data['biz_on'] - $p_no_data['biz_off']);
			$extra 		-= ($p_no_data['chk_on'] - $p_no_data['chk_off']);

			if($bix_extra_chk == true && $bix_extra <= 0)
			{
				$bix_extra_chk = false;
			}

			if($extra_chk == true && $extra <= 0)
			{
				$extra_chk = false;
			}
		}

        $tax_extra_list[$reward_result['wd_due_date']][$reward_result['hex_bk_jumin']]['pres_money_sum'] = $p_data_pres_money_total_sum;
    }

    $p_no_pres_money_sum = $tax_extra_list[$reward_result['wd_due_date']][$reward_result['hex_bk_jumin']]['pres_money_sum'];
    $local_biz_tax_extra = $tax_extra_list[$reward_result['wd_due_date']][$reward_result['hex_bk_jumin']]['biz'][$reward_result['a_no']];
    $local_tax_extra 	 = $tax_extra_list[$reward_result['wd_due_date']][$reward_result['hex_bk_jumin']]['tax'][$reward_result['a_no']];

    $biz_tax_value 		 = 0; // 사업소득세 (3%)
    $local_tax_value 	 = 0; // 지방소득세 (0.3%)

    if(($reward_result['posting_num']/$reward_result['reg_num']) >= 2)
        $reward_cost_result_value = $reward_result['pres_money']*($reward_result['posting_num']/$reward_result['reg_num']); // 보상지급액(소득세 포함)
    else
        $reward_cost_result_value = $reward_result['pres_money']; // 보상지급액(소득세 포함)

    // 세전 보상금 총합이 33,334원 부터 세금 적용
	$tax_apply 		 = TRUE;
	$biz_tax_value 	 = $local_biz_tax_extra;
	$local_tax_value = $local_tax_extra;

	$reward_cost_result_value -= ($biz_tax_value + $local_tax_value);

    $is_reward_view = false;

    if(!empty($reward_result['bk_jumin']) && !empty($reward_result['bk_title']) && !empty($reward_result['bk_num']) && !empty($reward_result['bk_name']) && !empty($reward_result['wd_due_date']))
    {
        $is_reward_view = true;
	}

	$reward_list[] = array(
		"a_no"		 			=> $reward_result['a_no'],
		"p_no" 		 			=> $reward_result['p_no'],
		"state_name" 			=> $state_name_option[$reward_result['p_state']],
		"kind"		 			=> $reward_result['kind'],
		"kind_name"	 			=> $promotion_kind_option[$reward_result['kind']],
		"promotion_code" 		=> $reward_result['promotion_code'],
		"company"		 		=> $reward_result['c_name'],
		"pres_reward"	 		=> isset($reward_result['pres_reward']) ? $reward_result['pres_reward'] : "",
		"pres_reward_intval" 	=> intval((strtotime($today)-strtotime($reward_result['pres_reward'])) / 86400),
		"pres_reward_week"		=> isset($reward_result['pres_reward']) ? date("m/d(".$date_short_option[date("w",strtotime($reward_result['pres_reward']))].")",strtotime($reward_result['pres_reward'])) : "",
		"pres_reward_next"		=> isset($reward_result['pres_reward']) ? date("Y-m-d",strtotime("+7 days", strtotime($reward_result['pres_reward']))) : "",
		"pres_reward_next_week"	=> isset($reward_result['pres_reward']) ? date("m/d(".$date_short_option[date("w",strtotime("+7 days", strtotime($reward_result['pres_reward'])))].")",strtotime("+7 days", strtotime($reward_result['pres_reward']))) : "",
		"nick"					=> $reward_result['nick'],
		"username"				=> $reward_result['username'],
		"blog_url"				=> $reward_result['blog_url'],
		"bk_jumin"				=> $reward_result['bk_jumin'],
		"bk_title"				=> $reward_result['bk_title'],
		"bk_num"				=> $reward_result['bk_num'],
		"bk_name"				=> $reward_result['bk_name'],
		"pres_money"			=> number_format($reward_result['pres_money']),
		"refund_count"			=> $reward_result['refund_count'],
		"wd_due_date"			=> isset($reward_result['wd_due_date']) ? date("m/d(".$date_short_option[date("w",strtotime($reward_result['wd_due_date']))].")",strtotime($reward_result['wd_due_date'])) : "",
		"wd_due_date_val"		=> isset($reward_result['wd_due_date']) ? date("Y-m-d",strtotime($reward_result['wd_due_date'])) : "",
		"pres_num"				=> $reward_result['posting_num']/$reward_result['reg_num'],
		"wd_no"					=> $reward_result['wd_no'],
		"reward_cost"			=> isset($reward_result['reward_cost']) ? number_format($reward_result['reward_cost']) : "",
		"input_reward_cost"		=> number_format($pres_money_total),
		"biz_tax"				=> isset($reward_result['biz_tax']) ? number_format($reward_result['biz_tax']) : "",
		"local_tax"				=> isset($reward_result['local_tax']) ? number_format($reward_result['local_tax']) : "",
		"reward_cost_result"	=> isset($reward_result['reward_cost_result']) ? number_format($reward_result['reward_cost_result']) : "",
		"input_biz_tax"			=> number_format($biz_tax_value),
		"input_local_tax"		=> number_format($local_tax_value),
		"input_reward_cost_result"	=> number_format($reward_cost_result_value),
		"is_reward_view" 		=> $is_reward_view
	);
}

# 마지막 개인정보 삭제날
$work_last_sql 		= "SELECT DATE_FORMAT(MAX(task_run_regdate), '%Y-%m-%d') as last_date FROM `work` WHERE prd_no='287' AND work_state='6' AND k_name_code='02435'";
$work_last_query 	= mysqli_query($my_db, $work_last_sql);
$work_last_result	= mysqli_fetch_assoc($work_last_query);
$work_last_date		= isset($work_last_result['last_date']) ? $work_last_result['last_date'] : "";

$my_company_model 		= MyCompany::Factory();
$my_company_name_list  	= $my_company_model->getNameList();

$smarty->assign("work_last_date", $work_last_date);
$smarty->assign("my_company_name_list", $my_company_name_list);
$smarty->assign("promotion_kind_option", $promotion_kind_option);
$smarty->assign("reward_state_option", getRewardStateOption());
$smarty->assign("reward_account_option", getRewardAccountOption());
$smarty->assign("reward_tax_option", getRewardTaxOption());
$smarty->assign("reward_refund_option", getRewardRefundOption());
$smarty->assign("reward_refund_cnt_option", getRewardRefundCntOption());
$smarty->assign("promotion_bank_option", getPromotionBankOption());
$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("report_list", $report_list);
$smarty->assign("reward_list", $reward_list);

$smarty->display('reward_list.html');
?>
