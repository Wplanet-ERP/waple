<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_cms.php');
require('inc/helper/work_return.php');
require('inc/helper/deposit.php');
require('inc/helper/withdraw.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');

# Navigation & My Quick
$nav_prd_no  = "167";
$nav_title   = "커머스 배송 리스트(정산 검토)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# Option 설정
$company_model          = Company::Factory();
$kind_model             = Kind::Factory();
$product_model 	        = ProductCms::Factory();
$kind_code		        = "product_cms";

$kind_chart_data_list 	= $kind_model->getKindChartData($kind_code);
$prd_group_list 		= $kind_chart_data_list['kind_group_list'];
$prd_group_name_list	= $kind_chart_data_list['kind_group_name'];
$prd_group_code_list	= $kind_chart_data_list['kind_group_code'];
$prd_group_code 		= implode(",", $prd_group_code_list);
$prd_data_list			= $product_model->getPrdGroupChartData($prd_group_code);
$prd_total_list			= $prd_data_list['prd_total'];
$prd_g1_list = $prd_g2_list = $prd_g3_list = [];

foreach($prd_group_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}

# 검색쿼리 시작
$sch_prd_g1	    = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	    = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	    = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_prd_name   = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";

$prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
$sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

$add_where      = "1=1";
$add_date_where = "1=1";
if (!empty($sch_prd) && $sch_prd != "0") {
    $add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);
$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);

if(!empty($sch_prd_name)){
    $add_date_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no from product_cms prd_cms where prd_cms.title like '%{$sch_prd_name}%')";
    $smarty->assign('sch_prd_name', $sch_prd_name);
}

# 검색 처리
$base_mon           = date("Y-m", strtotime("-1 months"));
$base_s_date        = $base_mon."-01";
$base_e_day         = date("t", strtotime($base_s_date));
$base_e_date        = $base_mon."-".$base_e_day;
$sch_w_no 			= isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
$sch_order_s_date   = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : $base_s_date;
$sch_order_e_date 	= isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : $base_e_date;
$sch_order_number 	= isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_is_refund      = isset($_GET['sch_is_refund']) ? $_GET['sch_is_refund'] : "";
$sch_is_deposit     = isset($_GET['sch_is_deposit']) ? $_GET['sch_is_deposit'] : "";
$sch_c_no 		    = isset($_GET['sch_c_no']) ? $_GET['sch_c_no'] : "";
$sch_dp_c_no 		= isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_is_vat         = isset($_GET['sch_is_vat']) ? $_GET['sch_is_vat'] : "";
$sch_is_settle      = isset($_GET['sch_is_settle']) ? $_GET['sch_is_settle'] : "";
$sch_is_return      = isset($_GET['sch_is_return']) ? $_GET['sch_is_return'] : "";
$sch_is_return_fee  = isset($_GET['sch_is_return_fee']) ? $_GET['sch_is_return_fee'] : "";


if(!empty($sch_w_no)){
    $add_where .= " AND w.w_no='{$sch_w_no}'";
    $smarty->assign('sch_w_no', $sch_w_no);
}

if(!empty($sch_order_s_date) && !empty($sch_order_e_date))
{
    $sch_ord_s_datetime = $sch_order_s_date." 00:00:00";
    $sch_ord_e_datetime = $sch_order_e_date." 23:59:59";

    $add_date_where .= " AND (w.order_date BETWEEN '{$sch_ord_s_datetime}' AND '{$sch_ord_e_datetime}')";

    $smarty->assign('sch_order_s_date', $sch_order_s_date);
    $smarty->assign('sch_order_e_date', $sch_order_e_date);
}

if(!empty($sch_order_number)){
    $add_date_where .= " AND w.order_number = '{$sch_order_number}'";
    $smarty->assign('sch_order_number', $sch_order_number);
}

if(!empty($sch_is_refund)){
    if($sch_is_refund == '1'){
        $add_where .= " AND refund_cnt > 0";
    }else{
        $add_where .= " AND refund_cnt = 0";
    }
    $smarty->assign('sch_is_refund', $sch_is_refund);
}

if(!empty($sch_is_deposit)){
    if($sch_is_deposit == '1'){
        $add_where .= " AND deposit_cnt > 0";
    }else{
        $add_where .= " AND deposit_cnt = 0";
    }
    $smarty->assign('sch_is_deposit', $sch_is_deposit);
}

if(!empty($sch_c_no)){
    $add_date_where .= " AND w.c_no='{$sch_c_no}'";
    $smarty->assign('sch_c_no', $sch_c_no);
}

if(!empty($sch_dp_c_no)){
    $add_date_where .= " AND w.dp_c_no='{$sch_dp_c_no}'";
    $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
}

if(!empty($sch_is_vat)){
    if($sch_is_vat == '1'){
        $add_where .= " AND vat_cnt > 0";
    }else{
        $add_where .= " AND(ord_no IS NOT NULL) AND vat_cnt = 0";
    }
    $smarty->assign('sch_is_vat', $sch_is_vat);
}

if(!empty($sch_is_settle)){
    if($sch_is_settle == '1'){
        $add_where .= " AND settle_cnt > 0";
    }else{
        $add_where .= " AND (ord_no IS NOT NULL) AND settle_cnt = 0";
    }
    $smarty->assign('sch_is_settle', $sch_is_settle);
}

if(!empty($sch_is_return)){
    if($sch_is_return == '1'){
        $add_where .= " AND return_cnt > 0";
    }else{
        $add_where .= " AND return_cnt = 0";
    }
    $smarty->assign('sch_is_return', $sch_is_return);
}

if(!empty($sch_is_return_fee)){
    if($sch_is_return_fee == '1'){
        $add_where .= " AND return_fee > 0";
    }else{
        $add_where .= " AND (return_fee = 0 OR return_fee IS NULL)";
    }
    $smarty->assign('sch_is_return_fee', $sch_is_return_fee);
}

$add_orderby    = "w.w_no DESC";

# 전체 게시물 수
$cms_ord_total_sql = "
    SELECT 
        * 
    FROM 
    (
        SELECT
            *,
            (SELECT COUNT(wk.w_no) FROM `work` wk WHERE wk.prd_no='229' AND wk.linked_table='work_cms' AND wk.linked_shop_no=w.shop_ord_no) as refund_cnt,
            (SELECT COUNT(wk.w_no) FROM `work` wk WHERE wk.prd_no='260' AND wk.work_state='6' AND wk.linked_table='work_cms' AND wk.linked_shop_no=w.order_number) as deposit_cnt,
            (SELECT COUNT(wcv.no) FROM work_cms_vat wcv WHERE wcv.order_number=w.ord_no) as vat_cnt,
            (SELECT GROUP_CONCAT(wcv.notice SEPARATOR ' ') FROM work_cms_vat wcv WHERE wcv.order_number=w.ord_no GROUP BY ord_no) as vat_notice,             
            (SELECT COUNT(wcs.no) FROM work_cms_settlement wcs WHERE wcs.order_number=w.ord_no) as settle_cnt,
            (SELECT GROUP_CONCAT(wcs.notice SEPARATOR ' ') FROM work_cms_settlement wcs WHERE wcs.order_number=w.ord_no GROUP BY ord_no) as settle_notice,
            (SELECT COUNT(wcr.r_no) FROM work_cms_return wcr WHERE wcr.parent_order_number=w.order_number) as return_cnt,
            (SELECT SUM(wcr.return_delivery_fee) FROM work_cms_return wcr WHERE wcr.parent_order_number=w.order_number) as return_fee
        FROM 
        (
            SELECT
                w.w_no,
                w.delivery_state,
                w.order_number,
                w.origin_ord_no,
                IF(w.origin_ord_no='', w.order_number, w.origin_ord_no) as ord_no,
                w.recipient,
                w.c_no,
                w.c_name,
                w.order_date,
                w.prd_no,
                w.quantity,
                w.dp_c_no,
                w.dp_c_name,
                w.shop_ord_no,
                SUM(w.unit_price) as ord_price,
                SUM(w.unit_delivery_price) as ord_delivery_price
            FROM work_cms w
            WHERE {$add_date_where}
            GROUP BY ord_no
        ) as w
    ) AS w
    WHERE {$add_where} AND w.order_number is not null
";
$total_price_list       = array('total_price' => 0, 'total_delivery' => 0, 'total_deposit' => 0, 'total_refund' => 0, 'total_vat' => 0, 'total_settle' => 0, 'total_return' => 0);
$total_chk_ord_list     = [];
$total_chk_deposit_list = [];
$total_chk_refund_list  = [];
$total_chk_return_list  = [];
$total_chk_vat_list     = [];
$total_chk_settle_list  = [];
$work_cms_total_query = mysqli_query($my_db, $cms_ord_total_sql);
while($work_cms_total_result  = mysqli_fetch_array($work_cms_total_query))
{
    $total_price_list['total_price']    += $work_cms_total_result['ord_price'];
    $total_price_list['total_delivery'] += $work_cms_total_result['ord_delivery_price'];

    if(!isset($total_chk_ord_list[$work_cms_total_result['order_number']])){
        $total_chk_ord_list[$work_cms_total_result['order_number']] = $total_chk_ord_list['order_number'];
    }

    if(!isset($total_chk_deposit_list[$work_cms_total_result['order_number']]) && $work_cms_total_result['deposit_cnt'] > 0){
        $total_chk_deposit_list[$work_cms_total_result['order_number']] = "'{$work_cms_total_result['order_number']}'";
    }

    if(!isset($total_chk_refund_list[$work_cms_total_result['shop_ord_no']]) && $work_cms_total_result['refund_cnt'] > 0){
        $total_chk_refund_list[$work_cms_total_result['shop_ord_no']] = "'{$work_cms_total_result['shop_ord_no']}'";
    }

    if(!isset($total_chk_return_list[$work_cms_total_result['order_number']]) && $work_cms_total_result['return_fee'] > 0){
        $total_chk_return_list[$work_cms_total_result['order_number']] = "'{$work_cms_total_result['order_number']}'";
    }

    if(!isset($total_chk_vat_list[$work_cms_total_result['ord_no']]) && $work_cms_total_result['vat_cnt'] > 0){
        $total_chk_vat_list[$work_cms_total_result['ord_no']] = "'{$work_cms_total_result['ord_no']}'";
    }

    if(!isset($total_chk_settle_list[$work_cms_total_result['ord_no']]) && $work_cms_total_result['settle_cnt'] > 0){
        $total_chk_settle_list[$work_cms_total_result['ord_no']] = "'{$work_cms_total_result['ord_no']}'";
    }
}

if(!empty($total_chk_deposit_list))
{
    $total_deposit_ord_nos  = implode(",", $total_chk_deposit_list);
    $deposit_total_sql      = "SELECT SUM(dp_price_vat) as total_deposit_price FROM `work` WHERE prd_no='260' AND work_state='6' AND linked_table='work_cms' AND linked_shop_no IN({$total_deposit_ord_nos})";
    $deposit_total_query    = mysqli_query($my_db, $deposit_total_sql);
    $deposit_total_result   = mysqli_fetch_assoc($deposit_total_query);

    $total_price_list['total_deposit']  = isset($deposit_total_result['total_deposit_price']) && !empty($deposit_total_result['total_deposit_price']) ? $deposit_total_result['total_deposit_price'] : 0;
}

if(!empty($total_chk_refund_list))
{
    $total_refund_ord_nos   = implode(",", $total_chk_refund_list);
    $refund_total_sql       = "SELECT SUM(wd_price_vat) as total_refund_price FROM `work` WHERE prd_no='229' AND linked_table='work_cms' AND linked_shop_no IN({$total_refund_ord_nos})";
    $refund_total_query     = mysqli_query($my_db, $refund_total_sql);
    $refund_total_result    = mysqli_fetch_assoc($refund_total_query);

    $total_price_list['total_refund']  = isset($refund_total_result['total_refund_price']) && !empty($refund_total_result['total_refund_price']) ? $refund_total_result['total_refund_price'] : 0;
}

if(!empty($total_chk_return_list))
{
    $total_return_ord_nos       = implode(",", $total_chk_return_list);
    $return_delivery_fee_sql    = "SELECT SUM(return_delivery_fee) as total_return_fee FROM (SELECT order_number, return_delivery_fee FROM work_cms_return WHERE return_delivery_fee > 0 AND parent_order_number IN({$total_return_ord_nos}) GROUP BY order_number) AS wcr";
    $return_delivery_fee_query  = mysqli_query($my_db, $return_delivery_fee_sql);
    $return_delivery_fee_result = mysqli_fetch_assoc($return_delivery_fee_query);

    $total_price_list['total_return']   = isset($return_delivery_fee_result['total_return_fee']) && !empty($return_delivery_fee_result['total_return_fee']) ? $return_delivery_fee_result['total_return_fee'] : 0;
}

if(!empty($total_chk_vat_list))
{
    $total_vat_ord_nos      = implode(",", $total_chk_vat_list);
    $vat_total_sql          = "SELECT SUM(cash_price+card_price+etc_price+tax_price) as total_vat_price FROM work_cms_vat WHERE order_number IN({$total_vat_ord_nos})";
    $vat_total_query        = mysqli_query($my_db, $vat_total_sql);
    $vat_total_result       = mysqli_fetch_assoc($vat_total_query);

    $total_price_list['total_vat'] = isset($vat_total_result['total_vat_price']) && !empty($vat_total_result['total_vat_price']) ? $vat_total_result['total_vat_price'] : 0;
}

if(!empty($total_chk_settle_list))
{
    $total_settle_ord_nos   = implode(",", $total_chk_settle_list);

    $settle_total_sql       = "SELECT SUM(settle_price) as total_settle_price FROM work_cms_settlement WHERE order_number IN({$total_settle_ord_nos})";
    $settle_total_query     = mysqli_query($my_db, $settle_total_sql);
    $settle_total_result    = mysqli_fetch_assoc($settle_total_query);

    $total_price_list['total_settle']   = isset($settle_total_result['total_settle_price']) && !empty($settle_total_result['total_settle_price']) ? $settle_total_result['total_settle_price'] : 0;
}


# 페이징처리
$work_cms_total = count($total_chk_ord_list);
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num 	= ceil($work_cms_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "work_cms_sales_vat_settle_commerce.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $work_cms_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 주문번호 뽑아내기
$cms_ord_total_sql  .="GROUP BY order_number ORDER BY {$add_orderby} LIMIT {$offset},{$num}";
$cms_ord_total_query = mysqli_query($my_db, $cms_ord_total_sql);
$order_number_list   = [];
while($order_number = mysqli_fetch_assoc($cms_ord_total_query)){
    $order_number_list[] =  "'".$order_number['order_number']."'";
}
$order_numbers   = implode(',', $order_number_list);
$add_where_group = !empty($order_numbers) ? "w.order_number IN({$order_numbers})" : "w.order_number=''";

# 배송리스트 쿼리
$work_cms_sql = "
    SELECT
        *,
        DATE_FORMAT(w.order_date, '%Y-%m-%d') as ord_date,
        DATE_FORMAT(w.order_date, '%H:%i') as ord_time,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
        (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name
    FROM
    (
        SELECT
            w.w_no,
            w.delivery_state,
            w.order_number,
            w.origin_ord_no,
            w.recipient,
            w.c_no,
            w.c_name,
            w.order_date,
            w.prd_no,
            w.quantity,
            w.dp_c_no,
            w.dp_c_name,
            w.shop_ord_no,
            w.unit_price,
            w.unit_delivery_price
        FROM work_cms w
        WHERE {$add_where_group}
    ) as w
    ORDER BY {$add_orderby}
";
$work_cms_query = mysqli_query($my_db, $work_cms_sql);
$work_cms_list  = $deposit_list = $delivery_list = $trade_list = $return_list = $vat_list = $vat_chk_list = $settle_list = $settle_chk_list = [];
$dp_state_option        = getDpStateOption();
$wd_state_option        = getWdStateOption();
$delivery_state_option  = getDeliveryStateOption();
$return_state_option    = getReturnStateOption();
while ($work_array = mysqli_fetch_array($work_cms_query))
{
    $work_array['delivery_state_name'] = $delivery_state_option[$work_array['delivery_state']];

    $refund_list = [];
    if(!empty($work_array['shop_ord_no']))
    {
        $refund_sql    = "SELECT w_no, wd_no, wd_price_vat, (SELECT wd.wd_state FROM withdraw wd WHERE wd.wd_no=w.wd_no) as wd_state FROM work w WHERE prd_no='229' AND linked_table='work_cms' AND linked_shop_no='{$work_array['shop_ord_no']}'";
        $refund_query  = mysqli_query($my_db, $refund_sql);
        while($refund = mysqli_fetch_assoc($refund_query))
        {
            $price = isset($refund['wd_price_vat']) && !empty($refund['wd_price_vat']) ? $refund['wd_price_vat']  : 0;
            $state = isset($refund['wd_state']) && !empty($refund['wd_state']) ? "[".$wd_state_option[$refund['wd_state']]."]" : "[대기]";
            $refund_list[$refund['w_no']] = array('price' => $price, 'state' => $state);
        }
    }
    $work_array['refund_list'] = $refund_list;

    if(!isset($work_cms_list[$work_array['order_number']]))
    {
        # 입금확인
        $deposit_sql   = "SELECT w_no, dp_no, dp_price_vat, (SELECT dp.dp_state FROM deposit dp WHERE dp.dp_no=w.dp_no) as dp_state FROM work w WHERE prd_no='260' AND work_state='6' AND linked_table='work_cms' AND linked_shop_no='{$work_array['order_number']}'";
        $deposit_query = mysqli_query($my_db, $deposit_sql);
        while($deposit = mysqli_fetch_assoc($deposit_query))
        {
            $dp_price = isset($deposit['dp_price_vat']) && !empty($deposit['dp_price_vat']) ? $deposit['dp_price_vat']  : 0;
            $dp_state = isset($deposit['dp_state']) && !empty($deposit['dp_state']) ? "[".$dp_state_option[$deposit['dp_state']]."]" : "[대기]";
            $deposit_list[$work_array['order_number']][$deposit['dp_no']] = array('price' => $dp_price, 'state' => $dp_state);
        }

        # 운송장 리스트
        $delivery_sql    = "SELECT `no`, delivery_no, delivery_type FROM work_cms_delivery WHERE order_number='{$work_array['order_number']}' GROUP BY delivery_no";
        $delivery_query  = mysqli_query($my_db, $delivery_sql);
        while ($delivery = mysqli_fetch_assoc($delivery_query)) {
            $delivery_list[$work_array['order_number']][$delivery['no']] = array('delivery_no' => $delivery['delivery_no'], 'delivery_type' => $delivery['delivery_type']);
        }

        # 교환 주문번호 체크(CMS)
        $trade_sql    = "SELECT order_number, delivery_state FROM work_cms WHERE parent_order_number = '{$work_array['order_number']}' GROUP BY order_number ORDER BY w_no";
        $trade_query  = mysqli_query($my_db, $trade_sql);
        while($trade = mysqli_fetch_assoc($trade_query))
        {
            $trade_list[$work_array['order_number']][] = array('ord_no' => $trade['order_number'], 'delivery_state' => "[".$delivery_state_option[$trade['delivery_state']]."]");
        }

        # 회수 리스트
        $return_sql    = "SELECT order_number, return_state, return_delivery_fee FROM work_cms_return WHERE parent_order_number = '{$work_array['order_number']}' GROUP BY order_number ORDER BY r_no";
        $return_query  = mysqli_query($my_db, $return_sql);
        while($return = mysqli_fetch_assoc($return_query))
        {
            $return_list[$work_array['order_number']][] = array("ord_no" => $return['order_number'], "return_state" => "[".$return_state_option[$return['return_state']]."]", "return_fee" => $return['return_delivery_fee']);
        }
    }

    # 부가세 리스트
    if(!isset($vat_chk_list[$work_array['origin_ord_no']]) && !empty($work_array['origin_ord_no']))
    {
        $vat_chk_list[$work_array['origin_ord_no']] = 1;
        $vat_sql    = "SELECT vat_date, (card_price+cash_price+etc_price+tax_price) as vat_price, notice FROM work_cms_vat WHERE order_number = '{$work_array['origin_ord_no']}' ORDER BY vat_price DESC";
        $vat_query  = mysqli_query($my_db, $vat_sql);
        while($vat = mysqli_fetch_assoc($vat_query))
        {
            $vat_list[$work_array['order_number']][] = array("vat_date" => $vat['vat_date'], "vat_price" => $vat['vat_price'], "notice" => $vat['notice']);
        }
    }

    # 정산 리스트
    if(!isset($settle_chk_list[$work_array['origin_ord_no']]) && !empty($work_array['origin_ord_no']))
    {
        $settle_chk_list[$work_array['origin_ord_no']] = 1;
        $settle_sql    = "SELECT settle_date, settle_price, notice FROM work_cms_settlement WHERE order_number = '{$work_array['origin_ord_no']}' ORDER BY settle_price DESC";
        $settle_query  = mysqli_query($my_db, $settle_sql);
        while($settle = mysqli_fetch_assoc($settle_query))
        {
            $settle_list[$work_array['order_number']][] = array("settle_date" => $settle['settle_date'], "settle_price" => $settle['settle_price'], "notice" => $settle['notice']);
        }
    }

    $work_cms_list[$work_array['order_number']][] = $work_array;
}

$smarty->assign('is_exist_option', getIsExistOption());
$smarty->assign('is_confirm_option', getIsConfirmOption());
$smarty->assign('dp_company_option', $company_model->getDpDisplayList());
$smarty->assign('commerce_brand_option', $product_model->getPrdBrandOption());
$smarty->assign("delivery_list", $delivery_list);
$smarty->assign("deposit_list", $deposit_list);
$smarty->assign("trade_list", $trade_list);
$smarty->assign("return_list", $return_list);
$smarty->assign("vat_list", $vat_list);
$smarty->assign("settle_list", $settle_list);
$smarty->assign("total_price_list", $total_price_list);
$smarty->assign("work_cms_list", $work_cms_list);

$smarty->display('work_cms_sales_vat_settle_commerce.html');

?>
