<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_upload.php');
require('inc/helper/chat.php');
require('inc/model/MyQuick.php');
require('inc/model/Staff.php');
require('inc/model/WapleChat.php');

$process    = isset($_POST['process']) ? $_POST['process'] : "";
$chat_model = WapleChat::Factory();

if($process == 'add_chat')
{
    $wc_no      = isset($_POST['wc_no']) ? $_POST['wc_no'] : "";
    $content    = isset($_POST['chat_content']) ? addslashes(trim($_POST['chat_content'])) : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $chat_model->setMainInit("waple_chat_content", "wcc_no");
    $chat_data  = array(
        "wc_no"     => $wc_no,
        "s_no"      => $session_s_no,
        "content"   => $content,
        "regdate"   => date("Y-m-d H:i:s")
    );

    # 파일첨부
    $file_names = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_paths = isset($_POST['file_path']) ? $_POST['file_path'] : "";

    $move_file_name = "";
    $move_file_path = "";
    if(!empty($file_names) && !empty($file_paths))
    {
        $move_file_paths = move_store_files($file_paths, "dropzone_tmp", "chat");
        $move_file_name  = implode(',', $file_names);
        $move_file_path  = implode(',', $move_file_paths);
    }

    if(!empty($move_file_path) && !empty($move_file_name)){

        $chat_data['file_name'] = $move_file_name;
        $chat_data['file_path'] = $move_file_path;
    }

    if($chat_model->insert($chat_data)){
        exit("<script>alert('알림톡 등록에 성공했습니다');parent.location.href='waple_chat.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('알림톡 등록에 실패했습니다');parent.location.href='waple_chat.php?{$search_url}';</script>");
    }
}
elseif($process == 'add_person_chat')
{
    $wc_no      = isset($_POST['wc_no']) ? $_POST['wc_no'] : "";
    $content    = isset($_POST['chat_content']) ? addslashes(trim($_POST['chat_content'])) : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(empty($content)){
        exit("<script>alert('내용을 입력해주세요');location.href='waple_chat.php?{$search_url}&cur_wc_no={$wc_no}';</script>");
    }

    $chat_model->setMainInit("waple_chat_content", "wcc_no");
    $chat_data  = array(
        "wc_no"         => $wc_no,
        "s_no"          => $session_s_no,
        "alert_type"    => 100,
        "content"       => $content,
        "regdate"       => date("Y-m-d H:i:s")
    );

    # 파일첨부
    $file_names = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_paths = isset($_POST['file_path']) ? $_POST['file_path'] : "";

    $move_file_name = "";
    $move_file_path = "";
    if(!empty($file_names) && !empty($file_paths))
    {
        $move_file_paths = move_store_files($file_paths, "dropzone_tmp", "chat");
        $move_file_name  = implode(',', $file_names);
        $move_file_path  = implode(',', $move_file_paths);
    }

    if(!empty($move_file_path) && !empty($move_file_name)){

        $chat_data['file_name'] = $move_file_name;
        $chat_data['file_path'] = $move_file_path;
    }

    if($chat_model->insert($chat_data)){
        exit("<script>alert('알림톡 등록에 성공했습니다');location.href='waple_chat.php?{$search_url}&cur_wc_no={$wc_no}';</script>");
    }else{
        exit("<script>alert('알림톡 등록에 실패했습니다');location.href='waple_chat.php?{$search_url}&cur_wc_no={$wc_no}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "175";
$nav_title   = "와플 알림톡";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 와플톡 리스트
$chat_title_list    = [];
$chat_title_list[0] = array("title" => "전체", "new_cnt" => 0);
$chat_type_list     = getChatTypeOption();
foreach($chat_type_list as $chat_type => $chat_data){
    $chat_title_list[$chat_type] = $chat_data;
}

$editor_folder_name = folderCheck('chat_content');
$smarty->assign("editor_path", "chat_content/{$editor_folder_name}");

$sch_chat_type      = isset($_GET['sch_chat_type']) ? $_GET['sch_chat_type'] : "";
$cur_wc_no          = isset($_GET['cur_wc_no']) ? $_GET['cur_wc_no'] : "";
$add_where          = "wc.display='1' AND wcc.display='1'";

if(!empty($sch_chat_type)){
    $add_where .= " AND wc.`type`='{$sch_chat_type}'";
    $smarty->assign('sch_chat_type', $sch_chat_type);
}
$smarty->assign('cur_wc_no', $cur_wc_no);

$search_url = getenv("QUERY_STRING");
if(strpos($search_url, "&cur_wc_no") !== false){
    $search_url_tmp = explode("&cur_wc_no", $search_url);
    $search_url = isset($search_url_tmp[0]) ? $search_url_tmp[0] : "";
}

# 와플 알림톡 쿼리
$waple_chat_sql = "
    SELECT
        wcc.wcc_no,
        wc.wc_no,
        wc.title,
        wcc.file_path,
        wcc.file_name,
        wcc.content,
        wc.`type`,
        DATE_FORMAT(wcc.regdate, '%Y-%m-%d %H:%i') AS regdate,
        IF(wc.`type`='2','WAPLE',(SELECT s.s_name FROM staff AS s WHERE s.s_no=wcc.s_no)) AS s_name,
        IF(wc.`type`='2','images/chat_waple_profile.png', (SELECT s.profile_img FROM staff AS s WHERE s.s_no=wcc.s_no)) AS profile,
        (SELECT COUNT(wcr.wcr_no) FROM waple_chat_read wcr WHERE wcr.wcc_no=wcc.wcc_no AND wcr.read_s_no='{$session_s_no}') AS read_cnt
    FROM waple_chat_content wcc
    LEFT JOIN waple_chat as wc ON wc.wc_no=wcc.wc_no
    WHERE {$add_where} AND 
        (
            (wc.`type`='1') OR (wc.`type`='2' AND wcc.s_no='{$session_s_no}')              
            OR (wc.`type`='3' AND wcc.wc_no IN(SELECT wcu.wc_no FROM waple_chat_user as wcu WHERE wcu.user='{$session_s_no}') AND wcc.s_no != '{$session_s_no}')
            OR (wc.`type`='4' AND wcc.s_no='{$session_s_no}') 
        )
    ORDER BY wcc.regdate ASC, wcc.wcc_no ASC
";
$waple_chat_query       = mysqli_query($my_db, $waple_chat_sql);
$waple_chat_list        = [];
$waple_chat_first_new   = false;
while($waple_chat = mysqli_fetch_assoc($waple_chat_query))
{
    $file_list = [];
    if(!empty($waple_chat['file_path']))
    {
        $chat_file_paths = explode(",", $waple_chat['file_path']);
        $chat_file_names = explode(",", $waple_chat['file_name']);
        foreach($chat_file_paths as $key => $file_path)
        {
            $file_name   = $chat_file_names[$key];
            $file_list[] = array("path" => $file_path, "name" => $file_name);
        }
    }

    if(!empty($waple_chat['profile']) ) {
        $waple_chat['profile'] = ($waple_chat['type'] != 2) ? "uploads/{$waple_chat['profile']}" : $waple_chat['profile'];
    }else{
        $waple_chat['profile'] = "images/staff_profile_base.png";
    }

    $waple_chat['content'] = str_replace("\r\n", "<br/>", $waple_chat['content']);
    if(strpos($waple_chat['content'], "http") !== false)
    {
        $content_val  = explode("<br/>", $waple_chat['content']);
        $content_list = [];

        foreach($content_val as $content_tmp){
            if(strpos($content_tmp, "http") !== false){
                $content_list[] = "<a href='{$content_tmp}' target='_blank' style='font-size: 10pt;' class='chat-content-link'>{$content_tmp}</a>";
            }else{
                $content_list[] = $content_tmp;
            }
        }

        $waple_chat['content'] = implode("<br/>", $content_list);
    }

    $waple_chat['file_list'] = $file_list;
    $waple_chat['is_new']    = !$waple_chat_first_new && ($waple_chat['read_cnt'] == 0) ? true : false;
    $waple_chat_list[]       = $waple_chat;

    if($waple_chat['read_cnt'] == 0){
        $chat_title_list[0]['new_cnt']++;
        $chat_title_list[$waple_chat['type']]['new_cnt']++;
        $waple_chat_first_new = true;

        if($waple_chat['type'] != '3'){
            $chat_model->readWapleChat($waple_chat['wcc_no'], $session_s_no);
        }
    }
}

$waple_chat_personal_list       = [];
$waple_chat_personal_sort_list  = [];

if($sch_chat_type == '3')
{
    $chat_per_sql = "
        SELECT 
            *,
            (SELECT s.staff_state FROM staff s WHERE s.s_no=wcu.user) as staff_state,
            (SELECT wcc.regdate FROM waple_chat_content wcc WHERE wcc.wc_no=wcu.wc_no AND wcc.s_no=wcu.user AND wcc.display='1' ORDER BY regdate DESC LIMIT 1) as last_date
        FROM waple_chat_user wcu
        LEFT JOIN waple_chat as wc ON wc.wc_no=wcu.wc_no
        WHERE wc.display='1' AND wc.`type`='3' AND wcu.`wc_no` IN(SELECT sub.wc_no FROM waple_chat_user as sub WHERE sub.user='{$session_s_no}') AND wcu.user != '{$session_s_no}'
        ORDER BY staff_state ASC, last_date DESC
    ";
    $chat_per_query = mysqli_query($my_db, $chat_per_sql);
    $staff_model    = Staff::Factory();
    while($chat_per = mysqli_fetch_assoc($chat_per_query))
    {
        $partner_no     = $chat_per['user'];
        $partner        = $staff_model->getStaff($partner_no);
        $chk_read_sql   = "
            SELECT 
                COUNT(wcc_no) as total_cnt,
	            SUM(read_cnt) as read_cnt
            FROM  
            (
                SELECT 
	                wcc.wcc_no,
	                (SELECT COUNT(wcr.wcr_no) FROM waple_chat_read wcr WHERE wcr.wcc_no=wcc.wcc_no AND wcr.read_s_no='{$session_s_no}') AS read_cnt
	             FROM waple_chat_content wcc
                WHERE wcc.display='1' AND wcc.wc_no='{$chat_per['wc_no']}' AND s_no != '{$session_s_no}'
            ) AS result_search
        ";
        $chk_read_query     = mysqli_query($my_db, $chk_read_sql);
        $chk_read_result    = mysqli_fetch_assoc($chk_read_query);

        $waple_chat_personal_sort_list[$chat_per['wc_no']] = $chk_read_result['total_cnt']-$chk_read_result['read_cnt'];

        $waple_chat_personal_list[$chat_per['wc_no']] = array(
            "chat_no"       => $chat_per['wc_no'],
            "profile"       => "uploads/".$partner['profile_img'],
            "partner"       => $partner['s_no'],
            "partner_name"  => $partner['s_name'],
            "display"       => $partner['staff_state'],
            "new_cnt"       => $chk_read_result['total_cnt']-$chk_read_result['read_cnt']
        );
    }
}
arsort($waple_chat_personal_sort_list);

$smarty->assign("search_url", $search_url);
$smarty->assign("waple_chat_title_list", $chat_title_list);
$smarty->assign("waple_chat_list", $waple_chat_list);
$smarty->assign("waple_chat_personal_sort_list", $waple_chat_personal_sort_list);
$smarty->assign("waple_chat_personal_list", $waple_chat_personal_list);

$smarty->display('waple_chat.html');
?>
