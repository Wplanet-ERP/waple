<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/work_cms.php');
require('inc/helper/wise_bm.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Company.php');
require('inc/model/ProductCms.php');

# Navigation & My Quick
$nav_prd_no  = "222";
$nav_title   = "요일/시간별 판매실적 비교(구매처)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수
$company_model              = Company::Factory();
$product_model              = ProductCms::Factory();
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_list'];
$sch_brand_total_list       = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_dp_company_list        = $company_model->getDpEtcList();
$sch_dp_company_keys        = array_keys($sch_dp_company_list);
$sch_dp_company_text        = implode(",", $sch_dp_company_keys);
$global_brand_list          = getGlobalBrandList();
$global_brand_option        = array_keys($global_brand_list);
$global_dp_all_list         = getGlobalDpCompanyList();
$global_dp_ilenol_list      = $global_dp_all_list['ilenol'];
$doc_brand_list             = getTotalDocBrandList();
$doc_brand_text             = implode(",", $doc_brand_list);
$ilenol_brand_list          = getIlenolBrandList();
$ilenol_brand_text          = implode(",", $ilenol_brand_list);
$ilenol_global_dp_text      = implode(",", $global_dp_ilenol_list);
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$sch_url_list               = [];

# 검색조건
$add_cms_where      = "w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN({$sch_dp_company_text})";
$sch_base_s_date    = isset($_GET['sch_base_s_date']) ? $_GET['sch_base_s_date'] : "";
$sch_base_e_date    = isset($_GET['sch_base_e_date']) ? $_GET['sch_base_e_date'] : "";
$sch_comp_s_date    = isset($_GET['sch_comp_s_date']) ? $_GET['sch_comp_s_date'] : "";
$sch_comp_e_date    = isset($_GET['sch_comp_e_date']) ? $_GET['sch_comp_e_date'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_dp_company     = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_dp_company)) {
    $add_cms_where  .= " AND `w`.dp_c_no='{$sch_dp_company}'";
    $smarty->assign("sch_dp_company", $sch_dp_company);
}

# 브랜드 옵션
if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    if($sch_brand == "5513"){
        $add_cms_where  .= " AND `w`.c_no IN({$doc_brand_text}) AND `w`.log_c_no = '5956'";
    }
    elseif($sch_brand == "5514"){
        $add_cms_where  .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.log_c_no = '5659' AND `w`.dp_c_no NOT IN({$ilenol_global_dp_text})";
    }
    elseif($sch_brand == "5979"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.c_no != '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
    }
    elseif($sch_brand == "6044"){
        $add_cms_where      .= " AND `w`.c_no = '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
    }
    else{
        $add_cms_where  .= " AND `w`.c_no = '{$sch_brand}'";
    }
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_cms_where     .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_cms_where     .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 변수
$hour_option            = getHourOption();
$date_w_option          = getDateChartOption();
$term_hour_name_list    = [];
$term_date_name_list    = [];
$sales_term_hour_list   = [];
$sales_term_date_list   = [];
$sales_term_total_list  = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0, "hour_base_max" => 0, "hour_comp_max" => 0, "date_base_max" => 0, "date_comp_max" => 0, "date_base_min" => 0, "date_comp_min" => 0);

foreach($hour_option as $hour){
    $hour_key = (int)$hour;
    $term_hour_name_list[$hour_key] = "{$hour}시";
    $sales_term_hour_list[$hour_key]= array("base_price" => 0,"comp_price" => 0, "comp_per" => 0);
}

foreach($date_w_option as $date_w => $date_label){
    $term_date_name_list[$date_w]   = $date_label;
    $sales_term_date_list[$date_w]  = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0);
}

$sales_term_hour_total_list    = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0);

if(!empty($sch_base_s_date) && !empty($sch_base_e_date) && !empty($sch_comp_s_date) && !empty($sch_comp_e_date))
{
    $sch_base_s_datetime = $sch_base_s_date." 00:00:00";
    $sch_base_e_datetime = $sch_base_e_date." 23:59:59";
    $sch_comp_s_datetime = $sch_comp_s_date." 00:00:00";
    $sch_comp_e_datetime = $sch_comp_e_date." 23:59:59";

    $smarty->assign("sch_base_s_date", $sch_base_s_date);
    $smarty->assign("sch_base_e_date", $sch_base_e_date);
    $smarty->assign("sch_comp_s_date", $sch_comp_s_date);
    $smarty->assign("sch_comp_e_date", $sch_comp_e_date);

    # 리스트 쿼리
    $term_sales_sql    = "

        SELECT    
            DATE_FORMAT(w.order_date, '%w') as date_key,
            DATE_FORMAT(w.order_date, '%H') as hour_key,
            SUM(
                IF(order_date BETWEEN '{$sch_base_s_datetime}' AND '{$sch_base_e_datetime}',unit_price, 0)
            ) AS base_price,
            SUM(
                IF(order_date BETWEEN '{$sch_comp_s_datetime}' AND '{$sch_comp_e_datetime}',unit_price, 0)
            ) AS comp_price
        FROM work_cms w 
        WHERE {$add_cms_where} AND
        (order_date BETWEEN '{$sch_base_s_datetime}' AND '{$sch_base_e_datetime}' OR order_date BETWEEN '{$sch_comp_s_datetime}' AND '{$sch_comp_e_datetime}')
        GROUP BY date_key, hour_key
        ORDER BY order_date
    ";
    $term_sales_query = mysqli_query($my_db, $term_sales_sql);
    while($term_sales = mysqli_fetch_assoc($term_sales_query))
    {
        $hour_key   = (int)$term_sales['hour_key'];
        $date_key   = (int)$term_sales['date_key'];
        $base_price = $term_sales['base_price'];
        $comp_price = $term_sales['comp_price'];

        $sales_term_hour_list[$hour_key]["base_price"] += $base_price;
        $sales_term_hour_list[$hour_key]["comp_price"] += $comp_price;
        $sales_term_date_list[$date_key]["base_price"] += $base_price;
        $sales_term_date_list[$date_key]["comp_price"] += $comp_price;

        $sales_term_total_list["base_price"] += $base_price;
        $sales_term_total_list["comp_price"] += $comp_price;
    }

    if(!empty($sales_term_total_list)){
        $total_comp_per = ($sales_term_total_list["comp_price"] == 0) ? 0 : (($sales_term_total_list["base_price"] == 0) ? -100 : ROUND( ((($sales_term_total_list["base_price"]-$sales_term_total_list["comp_price"])/$sales_term_total_list["comp_price"]) *100), 1));
        $sales_term_total_list["comp_per"] = $total_comp_per;
    }

    if(!empty($sales_term_hour_list))
    {
        foreach($sales_term_hour_list as $hour_key => $hour_data)
        {
            $hour_base_price    = $hour_data["base_price"];
            $hour_comp_price    = $hour_data["comp_price"];
            $hour_base_max      = $sales_term_total_list["hour_base_max"];
            $hour_comp_max      = $sales_term_total_list["hour_comp_max"];

            $hour_comp_per = ($hour_comp_price == 0) ? 0 : (($hour_base_price == 0) ? -100 : ROUND( ((($hour_base_price-$hour_comp_price)/$hour_comp_price) *100), 1));
            $sales_term_hour_list[$hour_key]["comp_per"] = $hour_comp_per;

            if($hour_base_max < $hour_base_price){
                $sales_term_total_list["hour_base_max"] = $hour_base_price;
            }

            if($hour_comp_max < $hour_comp_price){
                $sales_term_total_list["hour_comp_max"] = $hour_comp_price;
            }
        }
    }

    if(!empty($sales_term_date_list))
    {
        $date_idx = 0;
        foreach($sales_term_date_list as $date_key => $date_data)
        {
            $date_base_price    = $date_data["base_price"];
            $date_comp_price    = $date_data["comp_price"];
            $date_base_max      = $sales_term_total_list["date_base_max"];
            $date_base_min      = $sales_term_total_list["date_base_min"];
            $date_comp_max      = $sales_term_total_list["date_comp_max"];
            $date_comp_min      = $sales_term_total_list["date_comp_min"];

            if($date_idx == 0){
                $sales_term_total_list["date_base_min"] = $date_base_price;
                $sales_term_total_list["date_comp_min"] = $date_comp_price;
            }

            $date_comp_per = ($date_comp_price == 0) ? 0 : (($date_base_price == 0) ? -100 : ROUND( ((($date_base_price-$date_comp_price)/$date_comp_price) *100), 1));
            $sales_term_date_list[$date_key]["comp_per"] = $date_comp_per;
            
            if($date_base_max < $date_base_price){
                $sales_term_total_list["date_base_max"] = $date_base_price;
            }

            if($date_base_min > $date_base_price){
                $sales_term_total_list["date_base_min"] = $date_base_price;
            }

            if($date_comp_max < $date_comp_price){
                $sales_term_total_list["date_comp_max"] = $date_comp_price;
            }

            if($date_comp_min > $date_comp_price){
                $sales_term_total_list["date_comp_min"] = $date_comp_price;
            }

            $date_idx++;
        }
    }
}

$smarty->assign("sch_dp_company_list", $sch_dp_company_list);
$smarty->assign("term_hour_name_list", $term_hour_name_list);
$smarty->assign("term_date_name_list", $term_date_name_list);
$smarty->assign("sales_term_hour_list", $sales_term_hour_list);
$smarty->assign("sales_term_date_list", $sales_term_date_list);
$smarty->assign("sales_term_total_list", $sales_term_total_list);

$smarty->display('work_cms_stats_sales_term_etc.html');
?>
