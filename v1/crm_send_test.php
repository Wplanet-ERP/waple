<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_message.php');
require('inc/model/Message.php');

# Process
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$message_model  = Message::Factory();
$message_model->setCharset("utf8mb4");
$my_db->set_charset("utf8mb4");

if($process == 'new_crm_send')
{
    $t_no       = (isset($_POST['t_no'])) ? $_POST['t_no'] : "";
    $temp_no    = (isset($_POST['temp_no'])) ? $_POST['temp_no'] : "";
    $dest_name  = (isset($_POST['dest_name'])) ? $_POST['dest_name'] : "";
    $dest_phone = (isset($_POST['dest_phone'])) ? $_POST['dest_phone'] : "";
    $result     = false;
    $send_list  = [];

    if(empty($temp_no)){
        $temp_no = $t_no;
    }

    if(!empty($temp_no))
    {
        $temp_data_sql    = "SELECT * FROM crm_template WHERE t_no='{$temp_no}'";
        $temp_data_query  = mysqli_query($my_db, $temp_data_sql);
        $temp_data_result = mysqli_fetch_assoc($temp_data_query);

        $temp_type      = isset($temp_data_result['temp_type']) ? $temp_data_result['temp_type'] : "";
        $title          = isset($temp_data_result['title']) ? $temp_data_result['title'] : "";
        $sender_key     = isset($temp_data_result['send_key']) ? $temp_data_result['send_key'] : "";
        $temp_key       = isset($temp_data_result['temp_key']) ? $temp_data_result['temp_key'] : "";
        $btn_key        = isset($temp_data_result['btn_key']) ? $temp_data_result['btn_key'] : "";
        $btn_content    = isset($temp_data_result['btn_content']) ? $temp_data_result['btn_content'] : "";
        $send_name      = isset($temp_data_result['send_name']) ? $temp_data_result['send_name'] : "";
        $send_phone     = isset($temp_data_result['send_phone']) ? $temp_data_result['send_phone'] : "";
        $refuse_phone   = isset($temp_data_result['refuse_phone']) ? $temp_data_result['refuse_phone'] : "";
        $subject        = isset($temp_data_result['subject']) ? $temp_data_result['subject'] : "";
        $content        = isset($temp_data_result['content']) ? $temp_data_result['content'] : "";
        $cm_idx         = 1;
        $c_info         = "88";
        $cur_datetime   = date("YmdHis");
        $msg_id 	    = "W{$cur_datetime}".sprintf('%04d', $cm_idx++);

        if($temp_type == 'AT')
        {
            $msg_body   = str_replace("#{고객성명}", $dest_name, $content);
            $msg_body   = str_replace("#{고객명}", $dest_name, $msg_body);
            $msg_body   = str_replace("#{고객님}", $dest_name, $msg_body);

            $send_list[$msg_id] = array(
                "msg_id"        => $msg_id,
                "msg_type"      => $temp_type,
                "sender"        => $send_name,
                "sender_hp"     => $send_phone,
                "receiver"      => $dest_name,
                "receiver_hp"   => $dest_phone,
                "title"         => $title,
                "message"       => $msg_body,
                "temp_key"      => $temp_key,
                "btn_key"       => $btn_key,
                "btn_content"   => $btn_content,
                "cinfo"         => $c_info,
            );

            if($message_model->sendKakaoMessage($send_list)){
                $result = true;
            }
        }
        elseif($temp_type == 'SMS')
        {
            $content   .= "\r\n\r\n무료수신거부\r\n{$refuse_phone}";
            $msg_body   = str_replace("#{고객성명}", $dest_name, $content);
            $msg_body   = str_replace("#{고객명}", $dest_name, $msg_body);
            $msg_body   = str_replace("#{고객님}", $dest_name, $msg_body);

            if($dest_phone){
                $dest_phone_val = str_replace("-", "", $dest_phone);
                $chk_sql        = "SELECT is_agree FROM `account_refuse` WHERE hp='{$dest_phone_val}' ORDER BY regdate DESC LIMIT 1";
                $chk_query      = mysqli_query($my_db, $chk_sql);
                $chk_result     = mysqli_fetch_assoc($chk_query);

                if(isset($chk_result['is_agree']) && ($chk_result['is_agree'] != "Y"))
                {
                    exit ("<script>alert('문자 전송에 실패했습니다. 수신차단된 번호입니다.'); location.href='crm_send_test.php?crm_no={$t_no}';</script>");
                }
            }

            $sms_msg        = $msg_body;
            $sms_msg_len 	= mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
            $msg_type 		= ($sms_msg_len > 90) ? "L" : "S";

            $send_list[$msg_id] = array(
                "msg_id"        => $msg_id,
                "msg_type"      => $msg_type,
                "sender"        => $send_name,
                "sender_hp"     => $send_phone,
                "receiver"      => $dest_name,
                "receiver_hp"   => $dest_phone,
                "title"         => $title,
                "content"       => $sms_msg,
                "cinfo"         => $c_info,
            );

            if($message_model->sendMessage($send_list)){
                $result = true;
            }
        }
    }

    if($result){
        exit ("<script>alert('메세지를 전송했습니다'); location.href='crm_send_test.php?crm_no={$temp_no}';</script>");
    }else{
        exit ("<script>alert('알림톡 전송에 실패했습니다'); location.href='crm_send_test.php?crm_no={$t_no}';</script>");
    }
}

#템플릿 값
$crm_temp_no    = isset($_GET['crm_no']) ? $_GET['crm_no'] : "";
$crm_temp_list  = [];
if(!empty($crm_temp_no))
{
    $crm_temp_sql       = "SELECT * FROM crm_template WHERE t_no='{$crm_temp_no}' ORDER BY t_no";
    $crm_temp_query     = mysqli_query($my_db, $crm_temp_sql);
    $crm_temp_result    = mysqli_fetch_assoc($crm_temp_query);

    if($crm_temp_result['temp_type'] == 'SMS'){
        $crm_temp_result['content'] .= "\r\n\r\n무료수신거부\r\n{$crm_temp_result['refuse_phone']}";
    }

    $crm_temp_list[$crm_temp_result['t_no']] = $crm_temp_result['title'];
    $smarty->assign($crm_temp_result);
}
else
{
    $crm_temp_sql = "SELECT t_no, title FROM crm_template ORDER BY t_no";
    $crm_temp_query = mysqli_query($my_db, $crm_temp_sql);
    $crm_temp_list = [];
    while($crm_temp = mysqli_fetch_assoc($crm_temp_query))
    {
        $crm_temp_list[$crm_temp['t_no']] = $crm_temp['title'];
    }
}

$smarty->assign('crm_temp_type_option', getCrmTempTypeOption());
$smarty->assign('crm_temp_list', $crm_temp_list);

$smarty->display('crm_send_test.html');
?>
