<?php
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_return.php');
require('inc/model/Company.php');
require('inc/model/WorkCms.php');
require('inc/model/ProductCms.php');
require('Classes/PHPExcel.php');

# Model Init
$company_model = Company::Factory();
$dp_company_commerce_list = $company_model->getDpTotalList();
$return_reason_option     = getReturnReasonOption();

$comma          = '';
$file_name      = $_FILES["return_file"]["tmp_name"];
$dp_c_name_val  = (isset($_POST['choice_c_name']))?$_POST['choice_c_name']:"";
$return_type    = (isset($_POST['return_type']))?$_POST['return_type']:"";
$total_cnt      = 0;
$member_count   = 0;
$order_model    = WorkCms::Factory();
$return_model   = WorkCms::Factory();
$product_model  = ProductCms::Factory();
$return_model->setMainInit("work_cms_return_temporary", "r_no");

# 날짜 설정
$return_req_date    = date('Y-m-d');
$regdate            = date('Y-m-d H:i:s');
$req_hour           = date('Hi');
$req_day_val        = date('w');

# 엑셀 파일 읽기
$excelReader =   PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);

$excel              = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet       = $excel->getActiveSheet();
$totalRow           = $objWorksheet->getHighestRow();

if($req_hour >= 1630){
    $return_req_date = ($req_day_val == 5) ? date('Y-m-d', strtotime("+3 days")) : date('Y-m-d', strtotime("+1 days"));
}

$ins_data_list = [];
for ($i = 2; $i <= $totalRow; $i++)
{
    //변수 초기화
    $return_state    = "1";
    $return_reason   = 0;


    $c_no = $c_name = $s_no = $team = $dp_c_no = $prd_no = $quantity = $parent_order_number = $sku = "";
    $recipient = $recipient_hp = $recipient_addr = $zip_code = $return_cus_date = $return_purpose = $return_reason_name = $cus_memo = "";
    $parent_shop_ord_no = $member_code = "";

    //엑셀 값
    if($return_type == 'trade')   //교환
    {
        $parent_order_number = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //이전 주문번호
        $parent_shop_ord_no  = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //이전 품목주문번호
        $member_code         = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   //계정
        $return_cus_date     = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));   //고객 요청일시
        $zip_code            = (string)trim(addslashes($objWorksheet->getCell("BJ{$i}")->getValue()));  //우편번호
        $recipient           = (string)trim(addslashes($objWorksheet->getCell("BF{$i}")->getValue()));  //회수자명
        $recipient_hp        = (string)trim(addslashes($objWorksheet->getCell("BG{$i}")->getValue()));  //회수자전화
        $recipient_addr      = (string)trim(addslashes($objWorksheet->getCell("BK{$i}")->getValue()));  //회수지
        $sku                 = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));   //상품코드
        $quantity            = (string)trim(addslashes($objWorksheet->getCell("AC{$i}")->getValue()));  //수량
        $reason_and_memo     = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));   //회수사유&고객메시지
        $reason_and_memo_list = explode("/", $reason_and_memo);
        $return_purpose      = "1";
        $return_reason_name  = trim(addslashes($reason_and_memo_list[0]));
        $cus_memo            = trim(addslashes($reason_and_memo_list[1]));
    }
    elseif($return_type == 'return')   //반품
    {
        $parent_order_number = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //이전 주문번호
        $parent_shop_ord_no  = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //이전 품목주문번호
        $member_code         = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   //계정
        $return_cus_date     = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));   //고객 요청일시
        $zip_code            = (string)trim(addslashes($objWorksheet->getCell("BF{$i}")->getValue()));  //우편번호
        $recipient           = (string)trim(addslashes($objWorksheet->getCell("BB{$i}")->getValue()));  //회수자명
        $recipient_hp        = (string)trim(addslashes($objWorksheet->getCell("BC{$i}")->getValue()));  //회수자전화
        $recipient_addr      = (string)trim(addslashes($objWorksheet->getCell("BG{$i}")->getValue()));  //회수지
        $sku                 = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));   //상품코드
        $quantity            = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue()));  //수량
        $reason_and_memo     = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));   //회수사유&고객메시지
        $reason_and_memo_list = explode("/", $reason_and_memo);
        $return_purpose      = "2";
        $return_reason_name  = trim(addslashes($reason_and_memo_list[0]));
        $cus_memo            = trim(addslashes($reason_and_memo_list[1]));
    }

    if(empty($parent_order_number)){
        break;
    }

    // 상품코드가 없는 경우
    if(!$sku){
        echo "ROW : {$i}<br/>";
        echo "회수리스트 반영에 실패하였습니다.<br>상품코드가 없습니다.<br>
            상품번호  : {$sku}<br>";
        exit;
    }

    $order_item     = $order_model->getOrderItem($parent_order_number);
    $product_list   = array($sku);
    if(strpos($member_code, "****") === false)
    {
        if(strpos($sku, ",") !== false) {
            $product_list = explode(",", $sku);
        }

        foreach($product_list as $prd_sku)
        {
            $prd_sku        = trim($prd_sku);
            $product_item   = $product_model->getWorkCmsItem($prd_sku, "prd_code");
            $s_no           = (isset($product_item['manager']) && !empty($product_item['manager'])) ? $product_item['manager'] : "";
            $team           = (isset($product_item['team']) && !empty($product_item['team'])) ? $product_item['team'] : "";
            $prd_no         = (isset($product_item['prd_no']) && !empty($product_item['prd_no'])) ? $product_item['prd_no'] : "";
            $c_no           = (isset($product_item['c_no']) && !empty($product_item['c_no'])) ? $product_item['c_no'] : "";
            $c_name         = (isset($product_item['c_name']) && !empty($product_item['c_name'])) ? $product_item['c_name'] : "";

            if(empty($prd_no) || empty($c_no)){
                echo "ROW : {$i}<br/>";
                echo "회수리스트 반영에 실패하였습니다.<br>상품을 찾을 수 없습니다.<br>상품번호  : {$sku}<br>";
                exit;
            }else{

                $dp_c_no = $order_item['dp_c_no'];

                foreach($return_reason_option as $key => $label)
                {
                    if($label == $return_reason_name){
                        $return_reason = $key;
                        break;
                    }
                }

                $ins_data = array(
                    "return_state"          => $return_state,
                    "c_no"                  => $c_no,
                    "c_name"                => $c_name,
                    "s_no"                  => $s_no,
                    "team"                  => $team,
                    "dp_c_no"               => $dp_c_no,
                    "req_s_no"              => $session_s_no,
                    "req_team"              => $session_team,
                    "prd_no"                => $prd_no,
                    "quantity"              => $quantity,
                    "parent_order_number"   => $parent_order_number,
                    "parent_shop_ord_no"    => $parent_shop_ord_no,
                    "zip_code"              => $zip_code,
                    "recipient"             => $recipient,
                    "recipient_hp"          => $recipient_hp,
                    "recipient_addr1"       => $recipient_addr,
                    "regdate"               => $regdate,
                    "return_req_date"       => $return_req_date,
                    "return_cus_date"       => $return_cus_date,
                    "cus_memo"              => addslashes(trim($cus_memo)),
                    "return_purpose"        => $return_purpose,
                    "return_reason"         => $return_reason,
                );

                if($return_type == 'trade')
                {
                    $ins_data["deli_zip_code"]      = $zip_code;
                    $ins_data["deli_recipient"]         = $recipient;
                    $ins_data["deli_recipient_hp"]      = $recipient_hp;
                    $ins_data["deli_recipient_addr1"]   = $recipient_addr;
                }

                $ins_data_list[] = $ins_data;
            }
        }

        $member_count++;
    }

    $total_cnt++;
}

$empty_count = $total_cnt-$member_count;

if (!$return_model->multiInsert($ins_data_list)){
    echo "회수리스트 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 임태형<br/>";
    exit;
}else{
    exit("<script>alert('주문번호 {$total_cnt}건 중 비회원 {$empty_count}건을 제외한 회원코드(아임웹) {$member_count}건 등록이 완료되었습니다.');location.href='work_cms_return_temporary.php';</script>");
}

?>
