<?php
ini_set('memory_limit', '1024M');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
),
),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "모집상태")
->setCellValue('B1', "중단여부")
->setCellValue('C1', "구분")
->setCellValue('D1', "업체명")
->setCellValue('E1', "신청/모집")
->setCellValue('F1', "포스팅수")
->setCellValue('G1', "보상지급완료")
->setCellValue('H1', "리스트자료")
->setCellValue('I1', "담당자")
->setCellValue('J1', "모집시작")
->setCellValue('K1', "모집마감")
->setCellValue('L1', "발표")
->setCellValue('M1', "리뷰마감")
->setCellValue('N1', "보상")
->setCellValue('O1', "메모")
->setCellValue('P1', "신청일");

// 리스트 내용
$i=2;
$ttamount=0;

$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

$proc=(isset($_POST['process']))?$_POST['process']:"";
$code=(isset($_GET['code']))?$_GET['code']:"";
$smarty->assign("category",$code);

//echo $code;
//exit;

if(!in_array($code,array(1,2,3)))
	$page_code="all";
else
	$page_code=$code;

$smarty->assign("page_code",$page_code);

// 요일배열
$datey=array('일','월','화','수','목','금','토');

// 직원가져오기
$staff_sql="select s_no,s_name from staff where staff_state < '3'";
$staff_query=mysqli_query($my_db,$staff_sql);

while($staff_data=mysqli_fetch_array($staff_query)) {
	$staffs[]=array(
			'no'=>$staff_data['s_no'],
			'name'=>$staff_data['s_name']
	);
	$smarty->assign("staff",$staffs);
}


// 단순히 상태값을 텍스트 표현하기 위해 배열에 담은 정보
$state=array(
				1=>'접수대기',
				2=>'접수완료',
				3=>'모집중',
				4=>'진행중',
				5=>'마감'
			);
$state1=array(
				0=>'',
				1=>'진행중단',
			);

// 목록에서 kind 값을 통해, 구분명과 수정페이지 url 앞단어를 가져옴
$kind=array(
				'name'=>array(
					'',
					'체험단',
					'기자단',
					'배송체험'
				),
				'url'=>array(
					'',
					'consumer',
					'press',
					'delivery'
				)
			);







// 전체 게시물 수
if ($code !="" && $code !="all")
	$cnt_query = "promotion where kind=".$code;
else
	$cnt_query = "promotion";
$total_num = getcnt($cnt_query);
$totalnum = $total_num;
$smarty->assign("total_num",$total_num);

// 페이징
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;

$num = 20;

$pagenum = ceil($totalnum/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$url = "code=".$code;
//$page=pagelist($pages, "main.php", $pagenum, $url);
$smarty->assign("pagelist",$page);//
$noi = $totalnum - ($pages-1)*$num;
$offset = ($pages-1) * $num;
$smarty->assign("totalnum",$totalnum);




// all 이 아니면? kind 포함시키기 : 기본검색으로 지정하지 않았을 경우 where 절을 출력하기 위해서 사용.
if($page_code!="all") {
	$add_where=" where kind=".$page_code." ";
	$_GET['skind']=$page_code;
}


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";
$field=array();
$keyword=array();
$where=array();
$search_state=isset($_GET['sstate'])?$_GET['sstate']:"";
$search_s_name=(isset($_GET['s_name']) && $_GET['s_name'] !='®_s_name=')?$_GET['s_name']:"";
$search_team= isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ?$_GET['sch_team']:"";
$search_reg_s_name=isset($_GET['reg_s_name'])?$_GET['reg_s_name']:"";
$search_kind=isset($_GET['skind'])?$_GET['skind']:"";
$sch_channel_get=isset($_GET['schannel'])?$_GET['schannel']:"";
$f_location1_get=isset($_GET['f_location1'])?$_GET['f_location1']:"";
$f_location2_get=isset($_GET['f_location2'])?$_GET['f_location2']:"";
$search_company=isset($_GET['scompany'])?$_GET['scompany']:"";

if(!empty($search_state)) {
    $field[]="sstate";
    $keyword[]=$search_state;

    if($search_state == '6'){
        $where[]="p_state1='1'";
    }else{
        $where[]="p_state='".$search_state."' and p_state1!='1'";
    }
}

if(!empty($search_s_name)) {
    $field[]="s_name";
    $keyword[]=$search_s_name;
    $where[]="(name like '%".$search_s_name."%')";
}

if(!empty($search_team)) {
    $field[]	= "sch_team";
    $keyword[]	= $search_team;

    $sch_team_code_where = getTeamWhere($my_db, $search_team);
    if($sch_team_code_where){
        $where[] = " team IN ({$sch_team_code_where})";
    }
}

if(!empty($search_reg_s_name)) {
    $field[]="reg_s_name";
    $keyword[]=$search_reg_s_name;
    $where[]="(reg_s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name like '%{$search_reg_s_name}%'))";
}

if(!empty($search_kind)) {
    $field[]="skind";
    $keyword[]=$search_kind;
    $where[]="kind='".$search_kind."'";
}

if(!empty($sch_channel_get)) { // 마케팅 채널
    $field[]="schannel";
    $keyword[]=$sch_channel_get;
    $where[]="channel='".$sch_channel_get."'";
}


if(!empty($f_location1_get)) {
    $field[]="f_location1";
    $keyword[]=$f_location1_get;
    $where[]="c_no in (select c_no from company where location1='".$f_location1_get."')";
}

if(!empty($f_location2_get)) {
    $field[]="f_location2";
    $keyword[]=$f_location2_get;
    $where[]="c_no in (select c_no from company where location2='".$f_location2_get."')";
}

if(!empty($search_company)) {
    $field[]="scompany";
    $keyword[]=$search_company;
    $where[]="company like '%".$search_company."%'";
}

if(sizeof($where)>0) {
    $add_where=" where ";
    for($i=0;$i<sizeof($where);$i++) {
        $add_where.=$where[$i].(($i<(sizeof($where)-1))?" and ":"");
        $smarty->assign($field[$i],$keyword[$i]);
    }

}

// 정렬순서 토글 & 필드 지정
$add_orderby="";
$order=isset($_GET['od'])?$_GET['od']:"";
$order_type=isset($_GET['by'])?$_GET['by']:"";
$toggle=$order_type?"asc":"desc";
$order_field=array('','reg_sdate','reg_edate','awd_date','exp_edate','pres_reward');
if($order && $order<6) {
	$add_orderby.="$order_field[$order] $toggle,";
	$smarty->assign("order",$order);
	$smarty->assign("order_type",$order_type);
}
$add_orderby.=" p_no desc";

// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query",$save_query);

$promotion_sql="
				select
					a.p_no,
					a.s_no,
					a.kind,
					a.company,
					a.reg_num,
					a.p_state,
					a.p_state1,
					a.pres_reward,
					(select count(b.a_no) from application b where b.p_no=a.p_no) as app_num,
					(select count(b.a_no) from application b where b.p_no=a.p_no and b.a_state=1) as accept_num,
					(select count(r.r_no) from report r where r.p_no=a.p_no) as report_num,
					a.posting_num,a.name,a.reg_sdate,a.reg_edate,a.awd_date,a.exp_edate,a.p_memo,a.p_regdate,a.keyword_subject
				from
					promotion a
				$add_where
				order by $add_orderby

				";

$promotion_query= mysqli_query($my_db, $promotion_sql);
while($promotion_array = mysqli_fetch_array($promotion_query)){
	// 포스팅 건수가 2건 이상인 경우는, 접수인원 x 포스팅 건수로 해주어야 한다.
	// 50명이 접수하고, 한 사람당 2건씩 써야 하면 50 x 2 = 100 으로 화면에 표시
	if($promotion_array['posting_num']!=0)
		$posting_num = $promotion_array['posting_num'];
		//$posting_num = $promotion_array['posting_num'];
		if ($promotion_array['pres_reward'] != NULL)
			$pres_reward = date("n/d(".$datey[date("w",strtotime($promotion_array['pres_reward']))].")",strtotime($promotion_array['pres_reward']));
		else
			$pres_reward = "-";
	$promotions[] = array(
		"no"=>$promotion_array['p_no'],
		"kind"=>$promotion_array['kind'],
		"kind_name"=>$kind['name'][$promotion_array['kind']],
		"kind_url"=>$kind['url'][$promotion_array['kind']],
		"date_ymd"=>date("Y.m.d",strtotime($promotion_array['p_regdate'])),
		"date_hi"=>date("H:i",strtotime($promotion_array['p_regdate'])),
		"name"=>$promotion_array['name'],
		"company"=>$promotion_array['company'],
		"reg_start"=>date("n/d(".$datey[date("w",strtotime($promotion_array['reg_sdate']))].")",strtotime($promotion_array['reg_sdate'])),
		"reg_end"=>date("n/d(".$datey[date("w",strtotime($promotion_array['reg_edate']))].")",strtotime($promotion_array['reg_edate'])),
		"exp_edate"=>date("n/d(".$datey[date("w",strtotime($promotion_array['exp_edate']))].")",strtotime($promotion_array['exp_edate'])),
		"awd_date"=>$promotion_array['awd_date'],
		"award"=>date("n/d(".$datey[date("w",strtotime($promotion_array['awd_date']))].")",strtotime($promotion_array['awd_date'])),
		"pres_reward"=>$pres_reward,
		"awd_complete"=>(($promotion_array['p_state']>3)?"10/10":"-"),
		"reg_num"=>$promotion_array['reg_num'],
		"app_num"=>$promotion_array['app_num'],
		"accept_num"=>$promotion_array['accept_num'],
		"p_memo"=>$promotion_array['p_memo'],
		"posting"=>$posting_num,
		"report_num"=>$promotion_array['report_num'],
		"p_state"=>$promotion_array['p_state'],
		"p_state1"=>$promotion_array['p_state1'],
		"state_code"=>$state[$promotion_array['p_state']],
		"state_code1"=>$state1[$promotion_array['p_state1']],
		"keyword_subject"=>$promotion_array['keyword_subject']
	);
		if($promotion_array['p_state'] =='3')
			$state_1="블로거리스트\n포스팅리스트";
		else
			$state_1="선정";

		if($promotion_array['keyword_subject'] =='')
			$keyword=$promotion_array['reg_edate']."\n키워드 없음";
		else
			$keyword=$promotion_array['reg_edate'];
	//print_r($promotion_array);
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$i, $state[$promotion_array['p_state']])
	->setCellValue('B'.$i, $state1[$promotion_array['p_state1']])
	->setCellValue('C'.$i, $kind['name'][$promotion_array['kind']])
	->setCellValue('D'.$i, $promotion_array['company'])
	->setCellValue('E'.$i, $promotion_array['app_num']."/".$promotion_array['reg_num'])
	->setCellValue('F'.$i, $promotion_array['report_num']."/".$posting_num)
	->setCellValue('G'.$i, $promotion_array['app_num']."/".$promotion_array['reg_num'])
	->setCellValue('H'.$i, $state_1)
	->setCellValue('I'.$i, $promotion_array['name'])
	->setCellValue('J'.$i, $promotion_array['reg_sdate'])
	->setCellValue('K'.$i, $keyword)
	->setCellValue('l'.$i, $promotion_array['awd_date'])
	->setCellValue('M'.$i, $promotion_array['exp_edate'])
	->setCellValue('N'.$i, $promotion_array['pres_reward'])
	->setCellValue('O'.$i, $promotion_array['p_memo'])
	->setCellValue('P'.$i, $promotion_array['p_regdate']);
	$objPHPExcel->setActiveSheetIndex(0)->getStyle('H'.$i)->getAlignment()->setWrapText(true);
	$objPHPExcel->setActiveSheetIndex(0)->getStyle('K'.$i)->getAlignment()->setWrapText(true);
	$i = $i+1;
}

$objPHPExcel->getActiveSheet()->getStyle('A1:P'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFill()
->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
->getStartColor()->setARGB('00F3F3F6');
$objPHPExcel->getActiveSheet()->getStyle('A1:P'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:P'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setTitle('리뷰 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="excel_main.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
