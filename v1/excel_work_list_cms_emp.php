<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '4G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$nowdate = date("Y-m-d H:i:s");

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "주문번호")
	->setCellValue('B1', "상품명")
	->setCellValue('C1', "수량")
	->setCellValue('D1', "수령자명")
	->setCellValue('E1', "수령자전화")
	->setCellValue('F1', "수령자핸드폰")
	->setCellValue('G1', "수령지")
	->setCellValue('H1', "배송메모")
	->setCellValue('I1', "특이사항")
	->setCellValue('J1', "참고사항2")
	->setCellValue('K1', "구분")
;

# 검색 조건
$add_where  = "1=1 AND log_c_no='2809'";
$type       = isset($_GET['type']) ? $_GET['type'] : "";

# 상품 검색
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

if (!empty($sch_prd) && $sch_prd != "0") {
    $add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){
        $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){
        $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

# 브랜드 검색
$sch_brand_g1           = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2           = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand              = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

if(!empty($sch_brand)) {
    $add_where      .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

# 날짜 기본값
$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));

# 검색 처리
$sch_w_no 			    = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
$sch_reg_s_date         = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : $week_val;
$sch_reg_e_date         = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : $today_val;
$sch_reg_date_type      = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "week";
$sch_stock_date 	    = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : "";
$sch_delivery_state     = isset($_GET['sch_delivery_state']) ? $_GET['sch_delivery_state'] : "";
$sch_order_s_date 	    = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : "";
$sch_order_e_date 	    = isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : "";
$sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_recipient 		    = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp 	    = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_recipient_addr 	= isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
$sch_delivery_no 	    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
$sch_delivery_no_null   = isset($_GET['sch_delivery_no_null']) ? $_GET['sch_delivery_no_null'] : "";
$sch_dp_c_no 		    = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_prd_name 		    = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_notice             = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$sch_task_req           = isset($_GET['sch_task_req']) ? $_GET['sch_task_req'] : "";
$sch_ord_bundle 	    = isset($_GET['sch_ord_bundle']) ? $_GET['sch_ord_bundle'] : "";
$sch_unit_price         = isset($_GET['sch_unit_price']) ? $_GET['sch_unit_price'] : "";
$sch_delivery_price     = isset($_GET['sch_delivery_price']) ? $_GET['sch_delivery_price'] : "";
$sch_coupon_price       = isset($_GET['sch_coupon_price']) ? $_GET['sch_coupon_price'] : "";
$sch_coupon_type        = isset($_GET['sch_coupon_type']) ? $_GET['sch_coupon_type'] : "";
$sch_subscription       = isset($_GET['sch_subscription']) ? $_GET['sch_subscription'] : "";
$sch_order_type	        = isset($_GET['sch_order_type']) ? $_GET['sch_order_type'] : "";
$sch_log_c_no	        = isset($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "";
$sch_run_s_name	        = isset($_GET['sch_run_s_name']) ? $_GET['sch_run_s_name'] : "";
$sch_file_no	        = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";
$excel_title 	        = "EMP 발주등록 양식";

if(!empty($type) && $type == 'wise'){
    $add_where .= " AND w.dp_c_no IN(2280, 5468, 5469, 5475, 5476, 5477, 5478 ,5479, 5480, 5481, 5482, 5939, 6002)";
    $excel_title = "EMP 발주등록 양식(WISE)";
}elseif(!empty($type) && $type == 'nuzam'){
    $add_where .= " AND w.prd_no IN(SELECT prd_no FROM product_cms WHERE k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='04022')) AND w.dp_c_no NOT IN(2280, 5468, 5469, 5475, 5476, 5477, 5478 ,5479, 5480, 5481, 5482, 5939)";
    $excel_title = "EMP 발주등록 양식(누잠)";
}else{
    $add_where .= " AND w.c_no NOT IN(SELECT DISTINCT c_no FROM product_cms WHERE k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='04022')) AND w.dp_c_no NOT IN(2280, 5468, 5469, 5475, 5476, 5477, 5478 ,5479, 5480, 5481, 5482, 5939)";
}

if(!empty($sch_w_no)){
    $add_where .= " AND w.w_no='{$sch_w_no}'";
}

if(!empty($sch_reg_s_date) || !empty($sch_reg_e_date))
{
    if(!empty($sch_reg_s_date)){
        $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
        $add_where .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
    }

    if(!empty($sch_reg_e_date)){
        $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
        $add_where .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
    }
}

if(!empty($sch_stock_date)){
    $add_where .= " AND w.stock_date = '{$sch_stock_date}'";
}

if(!empty($sch_delivery_state)){
    $add_where .= " AND w.delivery_state = '{$sch_delivery_state}'";
}

if(!empty($sch_order_s_date) || !empty($sch_order_e_date))
{
    if(!empty($sch_order_s_date)){
        $sch_ord_s_datetime = $sch_order_s_date." 00:00:00";
        $add_where .= " AND w.order_date >= '{$sch_ord_s_datetime}'";
    }

    if(!empty($sch_order_e_date)){
        $sch_ord_e_datetime = $sch_order_e_date." 23:59:59";
        $add_where .= " AND w.order_date <= '{$sch_ord_e_datetime}'";
    }
}

if(!empty($sch_order_number)){
    $add_where .= " AND w.order_number = '{$sch_order_number}'";
}

if(!empty($sch_recipient)){
    $add_where .= " AND w.recipient like '%{$sch_recipient}%'";
}

if(!empty($sch_recipient_hp)){
    $add_where .= " AND w.recipient_hp like '%{$sch_recipient_hp}%'";
}

if(!empty($sch_recipient_addr)){
    $add_where .= " AND w.recipient_addr like '%{$sch_recipient_addr}%'";
}

if(!empty($sch_delivery_no_null))
{
    $add_where .= " AND (SELECT COUNT(DISTINCT delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) = 0";
}else{
    if(!empty($sch_delivery_no)){
        $add_where .= " AND w.order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery wcd WHERE wcd.delivery_no='{$sch_delivery_no}')";
    }
}

if(!empty($sch_dp_c_no)){
    $add_where .= " AND w.dp_c_no='{$sch_dp_c_no}'";
}

if(!empty($sch_prd_name)){
    $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no from product_cms prd_cms where prd_cms.title like '%{$sch_prd_name}%')";
}

if(!empty($sch_notice)){
    $add_where .= " AND w.notice like '%{$sch_notice}%'";
}

if(!empty($sch_task_req)){
    $add_where .= " AND w.task_req like '%{$sch_task_req}%'";
}

if(!empty($sch_delivery_price))
{
    if($sch_delivery_price == '1'){
        $add_where .= " AND w.unit_delivery_price > 0";
    }else{
        $add_where .= " AND w.unit_delivery_price = 0";
    }
}

if(!empty($sch_unit_price))
{
    if($sch_unit_price == '1'){
        $add_where .= " AND w.unit_price > 0";
    }else{
        $add_where .= " AND w.unit_price = 0";
    }
}

if(!empty($sch_coupon_price))
{
    if($sch_coupon_price == '1'){
        $add_where .= " AND (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) > 0";
    }else{
        $add_where .= " AND (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) IS NULL";
    }
}

if(!empty($sch_coupon_type))
{
    if($sch_coupon_type == '1') {
        $add_where .= " AND w.dp_c_no = '1372'";
    }elseif($sch_coupon_type == '2') {
        $add_where .= " AND w.dp_c_no IN(3295,4629,5427,5588)";
    }elseif($sch_coupon_type == '3') {
        $add_where .= " AND w.dp_c_no = '5800'";
    }elseif($sch_coupon_type == '4'){
        $add_where .= " AND w.dp_c_no = '5958'";
    }elseif($sch_coupon_type == '5'){
        $add_where .= " AND w.dp_c_no = '6012'";
    }
}

if(!empty($sch_subscription)){
    if($sch_subscription == '1'){
        $add_where .= " AND (w.subs_application_times > 0 OR w.subs_progression_times > 0)";
    }else{
        $add_where .= " AND ((w.subs_application_times = 0 OR w.subs_application_times IS NULL) AND (w.subs_progression_times = 0 OR w.subs_progression_times IS NULL))";
    }
}

if(!empty($sch_order_type)){
    $add_where .= " AND w.order_type = '{$sch_order_type}'";
}

if(!empty($sch_log_c_no)){
    $add_where .= " AND w.log_c_no='{$sch_log_c_no}'";
}

if(!empty($sch_run_s_name)){
    $add_where .= " AND w.task_run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_run_s_name}%')";
}

if(!empty($sch_file_no))
{
    $chk_file_sql   = "SELECT DISTINCT wcc.shop_ord_no FROM work_cms_coupon wcc WHERE wcc.file_no='{$sch_file_no}'";
    $chk_file_query = mysqli_query($my_db, $chk_file_sql);
    $chk_file_list  = [];
    while($chk_file_result = mysqli_fetch_assoc($chk_file_query)){
        $chk_file_list[] = "'{$chk_file_result['shop_ord_no']}'";
    }

    if(!empty($chk_file_list)){
        $chk_file_text = implode(",", $chk_file_list);
        $add_where .= " AND (w.file_no='{$sch_file_no}' OR w.shop_ord_no IN($chk_file_text))";
    }else{
        $add_where .= " AND w.file_no='{$sch_file_no}'";
    }
}

$cms_ord_sql    = "SELECT DISTINCT w.order_number FROM work_cms w WHERE {$add_where} AND w.order_number is not null ORDER BY w.w_no DESC LIMIT 10000";
$cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);
$order_number_list  = [];
while($order_number = mysqli_fetch_assoc($cms_ord_query)){
	$order_number_list[] =  "'".$order_number['order_number']."'";
}

$order_numbers = implode(',', $order_number_list);

// 리스트 쿼리
$with_sql = "
	SELECT
		w.order_number,
		(SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
		w.quantity,
		w.recipient,
		w.recipient_hp,
		w.recipient_hp2,
		w.recipient_addr,
		w.delivery_memo,
		w.notice,
		w.dp_c_name
	FROM
		work_cms w
	WHERE {$add_where} AND w.order_number IN({$order_numbers})
	ORDER BY w.order_number, w.prd_no ASC
";
$result	= mysqli_query($my_db, $with_sql);
$idx = 2;
if(!!$result)
{
    while($work_cms = mysqli_fetch_array($result))
    {
        $recipient_addr_val = trim($work_cms['recipient_addr']);
        $recipient_addr = preg_replace('/\r\n|\r|\n/','',$recipient_addr_val);

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit("A{$idx}", $work_cms['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("B{$idx}", $work_cms['prd_name'])
            ->setCellValue("C{$idx}", $work_cms['quantity'])
            ->setCellValue("D{$idx}", $work_cms['recipient'])
            ->setCellValueExplicit("E{$idx}", $work_cms['recipient_hp'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValueExplicit("F{$idx}", $work_cms['recipient_hp'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("G{$idx}", $recipient_addr)
            ->setCellValueExplicit("H{$idx}", $work_cms['delivery_memo'],PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue("I{$idx}", $work_cms['notice'])
            ->setCellValue("J{$idx}", '')
            ->setCellValue("K{$idx}", $work_cms['dp_c_name'])
        ;
        $idx++;
    }
}
$idx--;
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Tahoma');

$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
$objPHPExcel->getActiveSheet()->getStyle('I1:K1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FDE9D9');
$objPHPExcel->getActiveSheet()->getStyle("A1:K1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:K{$idx}")->getFont()->setSize(9);;
$objPHPExcel->getActiveSheet()->getStyle("A2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("A2:K{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setWrapText(true);


// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(9);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(70);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);

$objPHPExcel->getActiveSheet()->getStyle("C2:C{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->setTitle($excel_title);
$objPHPExcel->getActiveSheet()->getStyle('A1:K'.$idx)->applyFromArray($styleArray);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$excel_title}.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
