<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/approval.php');
require('inc/model/MyQuick.php');
require('inc/model/Approval.php');
require('inc/model/Kind.php');
require('inc/model/Team.php');
require('inc/model/Calendar.php');

if(!permissionNameCheck($session_permission, '재무관리자') && !permissionNameCheck($session_permission, '마스터관리자'))
{
    $smarty->display('access_error.html');
    exit;
}

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$form_model     = Approval::Factory();
$form_per_model = Approval::Factory();
$form_per_model->setMainInit("approval_form_permission", "afp_no");

if($process == 'add_approval_form')
{
    $display        = isset($_POST['display']) ? $_POST['display'] : 1;
    $k_name_code    = isset($_POST['k_name_code']) ? $_POST['k_name_code'] : "";
    $title          = isset($_POST['title']) ? trim(addslashes($_POST['title'])) : "";
    $is_withdraw    = isset($_POST['is_withdraw']) ? $_POST['is_withdraw'] : 2;
    $is_calendar    = isset($_POST['is_calendar']) ? $_POST['is_calendar'] : 2;
    $set_calendar   = isset($_POST['set_calendar']) && ($is_calendar != 2) ? $_POST['set_calendar'] : 0;
    $manager        = isset($_POST['manager']) ? $_POST['manager'] : "";
    $team           = isset($_POST['team']) ? $_POST['team'] : "";
    $explain        = isset($_POST['explain']) ? trim(addslashes($_POST['explain'])) : "";
    $content        = isset($_POST['content']) ? trim(addslashes($_POST['content'])) : "";
    $is_modify      = isset($_POST['is_modify']) ? $_POST['is_modify'] : 2;

    # 필수정보 체크 및 저장
    if(empty($display) || empty($k_name_code) || empty($title) || empty($manager) || empty($team))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');history.back();</script>");
    }

    $ins_data = array(
        "k_name_code"   => $k_name_code,
        "title"         => $title,
        "team"          => $team,
        "manager"       => $manager,
        "explain"       => $explain,
        "content"       => $content,
        "is_withdraw"   => $is_withdraw,
        "is_calendar"   => $is_calendar,
        "set_calendar"  => $set_calendar,
        "is_modify"     => $is_modify,
        "regdate"       => date("Y-m-d H:i:s"),
        "display"       => $display,
    );

    # 결재선, 열람자, 참조자 처리
    $add_permission_list         = [];
    $approval_permission_type_list   = isset($_POST['approval_permission_type']) ? $_POST['approval_permission_type'] : "";
    $approval_priority_list          = isset($_POST['approval_priority']) ? $_POST['approval_priority'] : "";
    $approval_permission_team_list   = isset($_POST['approval_permission_team']) ? $_POST['approval_permission_team'] : "";
    $approval_permission_staff_list  = isset($_POST['approval_permission_staff']) ? $_POST['approval_permission_staff'] : "";

    if(!empty($approval_permission_type_list))
    {
        foreach($approval_permission_type_list as $key => $approval_permission_type)
        {
            $appr_team  = isset($approval_permission_team_list[$key]) ? $approval_permission_team_list[$key] : "";
            $appr_staff = isset($approval_permission_staff_list[$key]) ? $approval_permission_staff_list[$key] : "";

            if(!empty($appr_team) && !empty($appr_staff))
            {
                $add_permission_list[] = array(
                    'type'      => $approval_permission_type,
                    'priority'  => isset($approval_priority_list[$key]) ? $approval_priority_list[$key] : 2,
                    'team'      => $appr_team,
                    'staff'     => $appr_staff
                );
            }
        }
    }

    $read_permission_team_list   = isset($_POST['reading_permission_team']) ? $_POST['reading_permission_team'] : "";
    $read_permission_staff_list  = isset($_POST['reading_permission_staff']) ? $_POST['reading_permission_staff'] : "";
    if(!empty($read_permission_team_list))
    {
        foreach($read_permission_team_list as $key => $read_permission_team)
        {
            $read_team  = !empty($read_permission_team) ? $read_permission_team : 0;
            $read_staff = isset($read_permission_staff_list[$key]) && !empty($read_permission_staff_list[$key]) ? $read_permission_staff_list[$key] : 0;
            $add_permission_list[] = array(
                'type'      => "reading",
                'priority'  => ($key+1),
                'team'      => $read_team,
                'staff'     => $read_staff
            );
        }
    }

    $ref_permission_team_list   = isset($_POST['reference_permission_team']) ? $_POST['reference_permission_team'] : "";
    $ref_permission_staff_list  = isset($_POST['reference_permission_staff']) ? $_POST['reference_permission_staff'] : "";
    if(!empty($ref_permission_team_list))
    {
        foreach($ref_permission_team_list as $key => $ref_team)
        {
            if(!empty($ref_team))
            {
                $ref_staff = isset($ref_permission_staff_list[$key]) && !empty($ref_permission_staff_list[$key]) ? $ref_permission_staff_list[$key] : 0;
                $add_permission_list[] = array(
                    'type'      => "reference",
                    'priority'  => ($key+1),
                    'team'      => $ref_team,
                    'staff'     => $ref_staff
                );
            }
        }
    }

    if($form_model->insert($ins_data))
    {
        if (!empty($add_permission_list))
        {
            $new_af_no      = $form_model->getInsertId();
            $ins_per_data   = [];
            foreach($add_permission_list as $add_permission)
            {
                $ins_per_data[] = array(
                    "af_no"     => $new_af_no,
                    "afp_type"  => $add_permission['type'],
                    "afp_team"  => $add_permission['team'],
                    "afp_s_no"  => $add_permission['staff'],
                    "priority"  => $add_permission['priority'],
                );
            }
            $form_per_model->multiInsert($ins_per_data);
        }

        exit("<script>alert('결재양식 등록에 성공했습니다');location.href='approval_form_list.php'</script>");
    }else{
        exit("<script>alert('결재양식 등록에 실패했습니다');history.back();</script>");
    }
}
elseif($process == 'upd_approval_form')
{
    $af_no          = isset($_POST['af_no']) ? $_POST['af_no'] : "";
    $display        = isset($_POST['display']) ? $_POST['display'] : 1;
    $k_name_code    = isset($_POST['k_name_code']) ? $_POST['k_name_code'] : "";
    $title          = isset($_POST['title']) ? trim(addslashes($_POST['title'])) : "";
    $is_withdraw    = isset($_POST['is_withdraw']) ? $_POST['is_withdraw'] : 2;
    $is_calendar    = isset($_POST['is_calendar']) ? $_POST['is_calendar'] : 2;
    $set_calendar   = isset($_POST['set_calendar']) && ($is_calendar != 2) ? $_POST['set_calendar'] : 0;
    $manager        = isset($_POST['manager']) ? $_POST['manager'] : "";
    $team           = isset($_POST['team']) ? $_POST['team'] : "";
    $explain        = isset($_POST['explain']) ? trim(addslashes($_POST['explain'])) : "";
    $content        = isset($_POST['content']) ? trim(addslashes($_POST['content'])) : "";
    $is_modify      = isset($_POST['is_modify']) ? $_POST['is_modify'] : 2;

    # 필수정보 체크 및 저장
    if(empty($af_no) || empty($display) || empty($k_name_code) || empty($title) || empty($manager) || empty($team)) {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');history.back();</script>");
    }

    $upd_data = array(
        "af_no"         => $af_no,
        "k_name_code"   => $k_name_code,
        "title"         => $title,
        "team"          => $team,
        "manager"       => $manager,
        "explain"       => $explain,
        "content"       => $content,
        "is_withdraw"   => $is_withdraw,
        "is_calendar"   => $is_calendar,
        "set_calendar"  => $set_calendar,
        "is_modify"     => $is_modify,
        "display"       => $display,
    );

    $add_permission_list = [];
    $upd_permission_list = [];
    $del_permission_list = [];
    $upd_apply_list      = [];

    # 결재자, 합의자 처리
    $origin_approval_afp_no          = isset($_POST['origin_approval_afp_no']) && !empty($_POST['origin_approval_afp_no']) ? explode(",", $_POST['origin_approval_afp_no']) : [];
    $approval_permission_afp_no_list = isset($_POST['approval_afp_no']) ? $_POST['approval_afp_no'] : "";
    $approval_permission_type_list   = isset($_POST['approval_permission_type']) ? $_POST['approval_permission_type'] : "";
    $approval_priority_list          = isset($_POST['approval_priority']) ? $_POST['approval_priority'] : "";
    $approval_permission_team_list   = isset($_POST['approval_permission_team']) ? $_POST['approval_permission_team'] : "";
    $approval_permission_staff_list  = isset($_POST['approval_permission_staff']) ? $_POST['approval_permission_staff'] : "";

    if(!empty($approval_permission_type_list))
    {
        foreach($approval_permission_type_list as $key => $approval_permission_type)
        {
            $appr_no        = isset($approval_permission_afp_no_list[$key]) ? $approval_permission_afp_no_list[$key] : "";
            $appr_team      = isset($approval_permission_team_list[$key]) ? $approval_permission_team_list[$key] : "";
            $appr_staff     = isset($approval_permission_staff_list[$key]) ? $approval_permission_staff_list[$key] : "";
            $appr_priority  = isset($approval_priority_list[$key]) ? $approval_priority_list[$key] : 2;

            if(!empty($appr_team) && !empty($appr_staff))
            {
                if(!empty($appr_no))
                {
                    $upd_apply_list[] = $appr_no;
                    $upd_permission_list[] = array(
                        'afp_no'    => $appr_no,
                        'type'      => $approval_permission_type,
                        'priority'  => $appr_priority,
                        'team'      => $appr_team,
                        'staff'     => $appr_staff
                    );
                }else{
                    $add_permission_list[] = array(
                        'type'      => $approval_permission_type,
                        'priority'  => $appr_priority,
                        'team'      => $appr_team,
                        'staff'     => $appr_staff
                    );
                }
            }
        }
    }

    if(!empty($origin_approval_afp_no))
    {
        foreach($origin_approval_afp_no as $approval_afp_no)
        {
            if(in_array($approval_afp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('afp_no' => $approval_afp_no);
        }
    }

    # 열람자 처리
    $upd_apply_list              = [];
    $origin_reading_afp_no       = isset($_POST['origin_reading_afp_no']) && !empty($_POST['origin_reading_afp_no']) ? explode(",", $_POST['origin_reading_afp_no']) : [];
    $read_permission_afp_no_list = isset($_POST['reading_afp_no']) ? $_POST['reading_afp_no'] : "";
    $read_permission_team_list   = isset($_POST['reading_permission_team']) ? $_POST['reading_permission_team'] : "";
    $read_permission_staff_list  = isset($_POST['reading_permission_staff']) ? $_POST['reading_permission_staff'] : "";
    if(!empty($read_permission_team_list))
    {
        foreach($read_permission_team_list as $key => $read_permission_team)
        {
            $read_no    = isset($read_permission_afp_no_list[$key]) ? $read_permission_afp_no_list[$key] : "";
            $read_team  = !empty($read_permission_team) ? $read_permission_team : 0;
            $read_staff = isset($read_permission_staff_list[$key]) && !empty($read_permission_staff_list[$key]) ? $read_permission_staff_list[$key] : 0;

            if(!empty($read_no))
            {
                $upd_apply_list[] = $read_no;
                $upd_permission_list[] = array(
                    'afp_no'    => $read_no,
                    'type'      => "reading",
                    'priority'  => ($key+1),
                    'team'      => $read_team,
                    'staff'     => $read_staff
                );
            }else{
                $add_permission_list[] = array(
                    'type'      => "reading",
                    'priority'  => ($key+1),
                    'team'      => $read_team,
                    'staff'     => $read_staff
                );
            }
        }
    }

    if(!empty($origin_reading_afp_no))
    {
        foreach($origin_reading_afp_no as $reading_afp_no)
        {
            if(in_array($reading_afp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('afp_no' => $reading_afp_no);
        }
    }

    # 참조자 처리
    $upd_apply_list             = [];
    $origin_reference_afp_no    = isset($_POST['origin_reference_afp_no']) && !empty($_POST['origin_reference_afp_no']) ? explode(",", $_POST['origin_reference_afp_no']) : [];
    $ref_permission_afp_no_list = isset($_POST['reference_afp_no']) ? $_POST['reference_afp_no'] : "";
    $ref_permission_team_list   = isset($_POST['reference_permission_team']) ? $_POST['reference_permission_team'] : "";
    $ref_permission_staff_list  = isset($_POST['reference_permission_staff']) ? $_POST['reference_permission_staff'] : "";
    if(!empty($ref_permission_team_list))
    {
        foreach($ref_permission_team_list as $key => $ref_team)
        {
            if(!empty($ref_team))
            {
                $ref_no    = isset($ref_permission_afp_no_list[$key]) ? $ref_permission_afp_no_list[$key] : "";
                $ref_staff = isset($ref_permission_staff_list[$key]) && !empty($ref_permission_staff_list[$key]) ? $ref_permission_staff_list[$key] : 0;

                if(!empty($ref_no))
                {
                    $upd_apply_list[] = $ref_no;
                    $upd_permission_list[] = array(
                        'afp_no'    => $ref_no,
                        'type'      => "reference",
                        'priority'  => ($key+1),
                        'team'      => $ref_team,
                        'staff'     => $ref_staff
                    );
                }else{
                    $add_permission_list[] = array(
                        'type'      => "reference",
                        'priority'  => ($key+1),
                        'team'      => $ref_team,
                        'staff'     => $ref_staff
                    );
                }

            }
        }
    }

    if(!empty($origin_reference_afp_no))
    {
        foreach($origin_reference_afp_no as $reference_afp_no)
        {
            if(in_array($reference_afp_no, $upd_apply_list)){
                continue;
            }

            $del_permission_list[] = array('afp_no' => $reference_afp_no);
        }
    }

    if($form_model->update($upd_data))
    {
        if(!empty($add_permission_list))
        {
            $ins_per_data = [];
            foreach($add_permission_list as $add_permission)
            {
                $ins_per_data[] = array(
                    "af_no"     => $af_no,
                    "afp_type"  => $add_permission['type'],
                    "afp_team"  => $add_permission['team'],
                    "afp_s_no"  => $add_permission['staff'],
                    "priority"  => $add_permission['priority'],
                );
            }

            $form_per_model->multiInsert($ins_per_data);
        }

        if(!empty($upd_permission_list))
        {
            foreach($upd_permission_list as $upd_permission)
            {
                $upd_per_data = array(
                    "afp_no"    => $upd_permission['afp_no'],
                    "afp_type"  => $upd_permission['type'],
                    "afp_team"  => $upd_permission['team'],
                    "afp_s_no"  => $upd_permission['staff'],
                    "priority"  => $upd_permission['priority'],
                );
                $form_per_model->update($upd_per_data);
            }
        }

        if(!empty($del_permission_list))
        {
            foreach($del_permission_list as $del_permission)
            {
                $form_per_model->delete($del_permission['afp_no']);
            }
        }

        exit("<script>alert('결재양식 수정에 성공했습니다');location.href='approval_form_list.php'</script>");
    }else{
        exit("<script>alert('결재양식 수정에 실패했습니다');history.back();</script>");
    }
}

# 변수 리스트
$team_model             = Team::Factory();
$kind_model             = Kind::Factory();
$cal_model              = Calendar::Factory();
$approval_group_list    = $kind_model->getKindGroupList("approval");
$team_all_list	        = $team_model->getTeamAllList();
$team_name_list         = $team_all_list['team_name_list'];
$staff_all_list	        = $team_all_list["staff_list"];
$sch_staff_list         = $staff_all_list['all'];
$calendar_list          = $cal_model->getActiveList();

$editor_path                = "approval";
$btn_title                  = "저장하기";
$approval_form              = [];
$approval_g1_list           = [];
$approval_g2_list           = [];
$appr_form_g1_list          = [];
$appr_form_g2_list          = [];

$staff_all_list["99998"]    = array("99998" => "부서장");
$staff_all_list["99999"]    = array("99999" => "부서장");
$appr_form_team_total_list  = $team_name_list;
$appr_form_team_total_list["99998"] = "기안부서";
$appr_form_team_total_list["99999"] = "상위부서";

foreach($approval_group_list as $key => $appr_data)
{
    if(!$key){
        $approval_g1_list = $appr_data;
    }else{
        $approval_g2_list[$key] = $appr_data;
    }
}
$appr_form_g1_list = $approval_g1_list;

# 양식 번호
$af_no      = isset($_GET['af_no']) ? $_GET['af_no'] : "";

if(!empty($af_no))
{
    $btn_title      = "수정하기";
    $approval_sql   = "
        SELECT 
            *, 
            (SELECT k_parent FROM kind k WHERE k.k_name_code=af.k_name_code) as appr_g1,
            (SELECT team_name FROM team t WHERE t.team_code=af.team) as team_name,
            (SELECT s_name FROM staff s WHERE s.s_no=af.manager) as s_name
        FROM approval_form as af 
        WHERE af.af_no='{$af_no}'
    ";
    $approval_query  = mysqli_query($my_db, $approval_sql);
    $approval_result = mysqli_fetch_assoc($approval_query);

    if(!isset($approval_result['af_no']) || empty($approval_result['af_no']))
    {
        exit("<script>alert('존재하지 않는 결재양식입니다.');location.href='approval_form_list.php';</script>");
    }

    $approval_result['manager_name'] = $approval_result['s_name']."(".$approval_result['team_name'].")";
    $approval_form      = $approval_result;
    $appr_form_g2_list  = $approval_g2_list[$approval_result['appr_g1']];

    # Permission 가져오기
    $appr_search_idx            = 2;
    $appr_reading_idx           = $appr_reference_idx = 2;
    $approval_permission_list   = $reading_permission_list = $reference_permission_list  = [];
    $origin_approval_afp_no     = $origin_reading_afp_no = $origin_reference_afp_no = [];

    $approval_permission_sql    = "SELECT * FROM approval_form_permission WHERE af_no='{$af_no}' ORDER BY priority";
    $approval_permission_query  = mysqli_query($my_db, $approval_permission_sql);
    while($approval_permission_result = mysqli_fetch_assoc($approval_permission_query))
    {
        if($approval_permission_result['afp_type'] == 'approval' || $approval_permission_result['afp_type'] == 'conference') {
            $approval_permission_list[] = $approval_permission_result;
            $appr_search_idx            = $approval_permission_result['priority']+1;
            $origin_approval_afp_no[]   = $approval_permission_result['afp_no'];
        }
        elseif($approval_permission_result['afp_type'] == 'reading') {
            $reading_permission_list[]  = $approval_permission_result;
            $appr_reading_idx           = $approval_permission_result['priority']+1;
            $origin_reading_afp_no[]    = $approval_permission_result['afp_no'];
        }
        elseif($approval_permission_result['afp_type'] == 'reference') {
            $reference_permission_list[]    = $approval_permission_result;
            $appr_reference_idx             = $approval_permission_result['priority']+1;
            $origin_reference_afp_no[]      = $approval_permission_result['afp_no'];
        }
    }

    $approval_form['appr_search_idx']    = $appr_search_idx;
    $approval_form['appr_reading_idx']   = $appr_reading_idx;
    $approval_form['appr_reference_idx'] = $appr_reference_idx;

    $approval_form['approval_permission_list']  = $approval_permission_list;
    $approval_form['reading_permission_list']   = $reading_permission_list;
    $approval_form['reference_permission_list'] = $reference_permission_list;

    $approval_form['origin_approval_afp_no']  = !empty($approval_permission_list) ? implode(",", $origin_approval_afp_no) : "";
    $approval_form['origin_reading_afp_no']   = !empty($reading_permission_list) ? implode(",", $origin_reading_afp_no) : "";
    $approval_form['origin_reference_afp_no'] = !empty($reference_permission_list) ? implode(",", $origin_reference_afp_no) : "";

}else{
    $approval_form = array(
        "display"               => "1",
        "manager"               => $session_s_no,
        "team"                  => $session_team,
        "manager_name"          => $session_s_team_name,
        "is_modify"             => "2",
        "appr_search_idx"       => "2",
        "appr_reading_idx"      => "1",
        "appr_reference_idx"    => "1"
    );
}

$smarty->assign($approval_form);
$smarty->assign("btn_title", $btn_title);
$smarty->assign("approval_option", $form_model->getApprovalFormList());
$smarty->assign("display_option", getDisplayOption());
$smarty->assign("permission_option", getPermissionOption());
$smarty->assign("approval_type_option", getApprovalTypeOption());
$smarty->assign("appr_form_g1_list", $appr_form_g1_list);
$smarty->assign("appr_form_g2_list", $appr_form_g2_list);
$smarty->assign('appr_form_team_list', $team_name_list);
$smarty->assign('appr_form_team_total_list', $appr_form_team_total_list);
$smarty->assign('appr_form_staff_list', $staff_all_list);
$smarty->assign('calendar_list', $calendar_list);

$smarty->display('approval_form_regist.html');
?>
