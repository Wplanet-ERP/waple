<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_upload.php');
require('inc/helper/deposit.php');
require ('api/SlackHook.php');
require('inc/model/MyCompany.php');
require('inc/model/Company.php');
require('inc/model/Corporation.php');
require('inc/model/Custom.php');
require('inc/model/Deposit.php');

#  접근 권한
if (!(permissionNameCheck($session_permission,"마케터") || permissionNameCheck($session_permission,"서비스운영") || permissionNameCheck($session_permission,"재무관리자") || permissionNameCheck($session_permission,"대표") || permissionNameCheck($session_permission,"마스터관리자") || $session_team == '00244')){
	$smarty->display('access_error.html');
	exit;
}

# 리스트 페이지 검색조건
$sch_page       = isset($_GET['page']) ? $_GET['page'] : "1";
$search_url     = "page={$sch_page}";
$sch_array_key  = array("sch_dp_quick","sch","sch_dp_no","sch_dp_state","sch_dp_method","sch_s_dp_date","sch_e_dp_date","sch_team","sch_s_no","sch_dp_tax_state","sch_dp_etc","sch_c_name","sch_prd_no","sch_subject");
foreach($sch_array_key as $sch_key)
{
    $sch_val   = isset($_GET[$sch_key]) && !empty($_GET[$sch_key]) ? $_GET[$sch_key]: "";
    if(!!$sch_val)
    {
        $search_url .= "&{$sch_key}={$sch_val}";
    }
}
$smarty->assign("search_url", $search_url);

# 사용 모델
$deposit_model 	= Deposit::Factory();
$company_model 	= Company::Factory();
$comment_model 	= Custom::Factory();
$comment_model->setMainInit("linked_comment", "no");

# 프로세스 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";
if($process == "tx_info")
{
	$c_no 			= isset($_POST['c_no']) ? $_POST['c_no'] : "";
	$company_item  	= $company_model->getItem($c_no);
	$tx_emails 		= explode("@", $company_item['tx_email']);
	$tx_email 		= $company_item['tx_email'] != "@" ? $company_item['tx_email'] : "";

	$company_array = array(
		"f_s_no"				=> $company_item['s_no'],
		"f_tx_company"			=> $company_item['tx_company'],
		"f_tx_company_number"	=> stripslashes($company_item['tx_company_number']),
		"f_tx_company_ceo"		=> stripslashes($company_item['tx_company_ceo']),
		"f_bk_manager"			=> $company_item['bk_manager'],
		"f_tx_manager"			=> $company_item['tx_manager'],
		"f_tx_call1"			=> $company_item['tx_call1'],
		"f_tx_call2"			=> $company_item['tx_call2'],
		"tx_email"				=> $tx_email,
		"f_o_registdoc" 		=> stripslashes($company_item['o_registration']),
		"f_r_registdoc" 		=> $company_item['r_registration'],
		"f_o_accdoc" 			=> stripslashes($company_item['o_accdoc']),
		"f_r_accdoc" 			=> $company_item['r_accdoc'],
		"f_o_etc" 				=> stripslashes($company_item['o_etc']),
		"f_r_etc" 				=> $company_item['r_etc'],
		"f_tx_email" 			=> $tx_email,
		"tx_email_0" 			=> $tx_emails[0],
		"tx_email_1" 			=> $tx_emails[1]
	);

	exit(json_encode($company_array));
}
elseif($process == "down_file")
{
	$c_no 		=(isset($_POST['f_c_no']))?$_POST['f_c_no']:"";
	$dp_no 		=(isset($_POST['dp_no']))?$_POST['dp_no']:"";
	$kind 		= (isset($_POST['kind']))?$_POST['kind']:"";
	$file_name 	= "";
	$file_path 	= "";

	if ($kind =='registdoc'){
		$company_item 	= $company_model->getItem($c_no);
		$file_name 		= stripslashes($company_item['o_registration']);
		$file_path 		= $company_item['r_registration'];
	}
	elseif ($kind =='accdoc'){
		$company_item 	= $company_model->getItem($c_no);
		$file_name 		= stripslashes($company_item['o_accdoc']);
		$file_path 		= $company_item['r_accdoc'];
	}
	elseif($kind =='etc'){
		$company_item 	= $company_model->getItem($c_no);
		$file_name 		= stripslashes($company_item['o_etc']);
		$file_path 		= $company_item['r_etc'];
	}
	elseif($kind =='mod_file'){
		$deposit_item 	= $deposit_model->getItem($dp_no);
		$file_name 		= stripslashes($deposit_item['mod_file_name']);
		$file_path 		= $deposit_item['mod_file_path'];
	}

	echo "File UP Name : {$file_path}<br>";
	echo "File Downloading...<br>File DN Name : {$file_name}<br>";
	exit("<script>location.href='popup/file_download.php?file_dn_name=".urlencode($file_name)."&file_up_name={$file_path}';</script>");
}
elseif($process == "del_file")
{
	$dp_no 			= (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$kind 			= (isset($_POST['kind'])) ? $_POST['kind'] : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$deposit_item 	= $deposit_model->getItem($dp_no);
	$file_path		= "";
	$del_data		= [];

	if($kind == "mod_file"){
		$file_path		= $deposit_item['mod_file_path'];
		$del_data		= array("dp_no" => $dp_no, "mod_file_path" => "NULL", "mod_file_name" => "NULL");
	}

	if(!$deposit_model->update($del_data)){
		exit ("<script>alert('삭제에 실패 하였습니다');location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");
	}else{
		del_file($file_path);
		exit ("<script>alert('삭제하였습니다');location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");
	}
}
elseif($process == "write")
{
	$search_url 			= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$supply_cost_value 		= str_replace(",","",trim($_POST['f_supply_cost'])); // 컴마 제거하기
	$supply_cost_vat_value 	= str_replace(",","",trim($_POST['f_supply_cost_vat'])); // 컴마 제거하기
	$biz_tax_value 			= str_replace(",","",trim($_POST['f_biz_tax'])); // 컴마 제거하기
	$local_tax_value 		= str_replace(",","",trim($_POST['f_local_tax'])); // 컴마 제거하기
	$cost_value 			= str_replace(",","",trim($_POST['f_cost'])); // 컴마 제거하기
	$dp_money_value 		= str_replace(",","",trim($_POST['f_dp_money'])); // 컴마 제거하기
	$dp_money_vat_value 	= str_replace(",","",trim($_POST['f_dp_money_vat'])); // 컴마 제거하기

	$deposit_ins_data = array(
		"my_c_no" 			=> $_POST['f_my_c_no'],
		"dp_subject" 		=> addslashes($_POST['f_dp_subject']),
		"dp_state" 			=> $_POST['f_dp_state'],
		"dp_method" 		=> $_POST['f_dp_method'],
		"c_no" 				=> $_POST['f_c_no'],
		"c_name" 			=> addslashes($_POST['f_c_name']),
		"s_no" 				=> $_POST['s_no'],
		"team" 				=> $_POST['team'],
		"supply_cost" 		=> $supply_cost_value,
		"vat_choice" 		=> $_POST['f_vat_tax_choice'],
		"supply_cost_vat" 	=> $supply_cost_vat_value,
		"biz_tax" 			=> $biz_tax_value,
		"local_tax" 		=> $local_tax_value,
		"cost" 				=> $cost_value,
		"dp_money" 			=> $dp_money_value,
		"dp_money_vat" 		=> $dp_money_vat_value,
		"dp_tax_state" 		=> $_POST['f_dp_tax_state'],
		"regdate" 			=> date("Y-m-d H:i:s"),
		"reg_s_no" 			=> $_POST['reg_s_no'],
		"reg_team" 			=> $_POST['reg_team'],
		"bk_name" 			=> $_POST['f_bk_name']
	);

	if(!empty($_POST['f_run_s_no'])) {
		$deposit_ins_data['run_s_no'] = $_POST['f_run_s_no'];
		$deposit_ins_data['run_team'] = $_POST['f_run_team'];
	}

	$f_dp_account = $_POST['f_dp_account'];
	if(empty($f_dp_account)) {
		if($_POST['f_my_c_no'] == '1') {
			$f_dp_account = '4';
		}elseif($_POST['f_my_c_no'] == '3') {
			$f_dp_account = '1';
		}
	}
	$deposit_ins_data["dp_account"]	 = $f_dp_account;
	$deposit_ins_data['dp_tax_date'] = !empty($_POST['f_dp_tax_date']) ? $_POST['f_dp_tax_date'] : "NULL";

	if(!empty($_POST['f_cost_info'])) {
		$deposit_ins_data["cost_info"] = addslashes($_POST['f_cost_info']);
	}

	if(!empty($_POST['f_dp_tax_item'])) {
		$deposit_ins_data["dp_tax_item"] = addslashes($_POST['f_dp_tax_item']);
	}

	# 첨부파일 체크 및 저장
	$file_names = isset($_POST['file_name']) ? $_POST['file_name'] : "";
	$file_paths = isset($_POST['file_path']) ? $_POST['file_path'] : "";

	$move_file_name 	= "";
	$move_file_path 	= "";
	if(!empty($file_names) && !empty($file_paths))
	{
		$move_file_paths 	= move_store_files($file_paths, "dropzone_tmp", "deposit");
		$move_file_name  	= implode(',', $file_names);
		$move_file_path  	= implode(',', $move_file_paths);
	}

	if(!empty($move_file_path) && !empty($move_file_name)){
		$move_file_name_conv	= str_replace("'","", $move_file_name);
		$move_file_name_conv	= str_replace('"',"", $move_file_name_conv);

		$deposit_ins_data['file_path'] 	= $move_file_path;
		$deposit_ins_data['file_name']	= addslashes($move_file_name_conv);
	}

	if(!empty($_POST['mod_dp_no'])) {
		$deposit_ins_data["mod_dp_no"] = $_POST['mod_dp_no'];
	}

	if(!empty($_POST['mod_type'])) {
		$deposit_ins_data["mod_type"] = $_POST['mod_type'];
	}

	if(!empty($_POST['mod_content'])) {
		$deposit_ins_data["mod_content"] = addslashes($_POST['mod_content']);
	}

	$mod_file = $_FILES["mod_file"];
	if($mod_file)
	{
		$mod_save_path 	= add_store_file($mod_file, "deposit");
		$mod_save_name	= str_replace("'","", $mod_file["name"]);
		$mod_save_name	= str_replace('"',"", $mod_save_name);

		if(!empty($mod_save_path)) {
			$deposit_ins_data["mod_file_path"] = $mod_save_path;
			$deposit_ins_data["mod_file_name"] = addslashes($mod_save_name);
		}
	}

	$deposit_ins_data['dp_date'] = !empty($_POST['f_dp_date']) ? $_POST['f_dp_date'] : "NULL";
	if($_POST['f_dp_state'] == '2')
	{
		if(empty($_POST['f_run_s_no'])) {
			$deposit_ins_data['run_s_no'] = $session_s_no;
			$deposit_ins_data['run_team'] = $session_team;
		}

		if(empty($_POST['f_dp_date'])) {
			$deposit_ins_data['dp_date'] 		 = date("Y-m-d");
			$deposit_ins_data['incentive_month'] = date("Y-m");
		}

		if(empty($_POST['f_dp_money_vat'])) {
			$deposit_ins_data['dp_money'] 	 	= $supply_cost_value;
			$deposit_ins_data['dp_money_vat'] 	= $cost_value;
		}
	}

	if($deposit_model->insert($deposit_ins_data)){
		exit ("<script>alert('등록했습니다');location.href='deposit_list.php?{$search_url}';</script>");
	}else{
		exit ("<script>alert('등록에 실패했습니다');location.href='deposit_list.php?{$search_url}';</script>");
	}
}
elseif($process == "modify")
{
	$search_url 			= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$supply_cost_value 		= str_replace(",","",trim($_POST['f_supply_cost'])); // 컴마 제거하기
	$supply_cost_vat_value 	= str_replace(",","",trim($_POST['f_supply_cost_vat'])); // 컴마 제거하기
	$biz_tax_value 			= str_replace(",","",trim($_POST['f_biz_tax'])); // 컴마 제거하기
	$local_tax_value 		= str_replace(",","",trim($_POST['f_local_tax'])); // 컴마 제거하기
	$cost_value 			= str_replace(",","",trim($_POST['f_cost'])); // 컴마 제거하기
	$dp_money_value 		= str_replace(",","",trim($_POST['f_dp_money'])); // 컴마 제거하기
	$dp_money_vat_value 	= str_replace(",","",trim($_POST['f_dp_money_vat'])); // 컴마 제거하기

	$deposit_upd_data = array(
		"dp_no" 			=> $_POST['dp_no'],
		"my_c_no" 			=> $_POST['f_my_c_no'],
		"dp_subject" 		=> addslashes($_POST['f_dp_subject']),
		"dp_state" 			=> $_POST['f_dp_state'],
		"dp_method" 		=> $_POST['f_dp_method'],
		"c_no" 				=> $_POST['f_c_no'],
		"c_name" 			=> addslashes($_POST['f_c_name']),
		"s_no" 				=> $_POST['s_no'],
		"team" 				=> $_POST['team'],
		"supply_cost" 		=> $supply_cost_value,
		"vat_choice" 		=> $_POST['f_vat_tax_choice'],
		"supply_cost_vat" 	=> $supply_cost_vat_value,
		"biz_tax" 			=> $biz_tax_value,
		"local_tax" 		=> $local_tax_value,
		"cost" 				=> $cost_value,
		"dp_money" 			=> $dp_money_value,
		"dp_money_vat" 		=> $dp_money_vat_value,
		"dp_tax_state" 		=> $_POST['f_dp_tax_state'],
		"reg_s_no" 			=> $_POST['reg_s_no'],
		"reg_team" 			=> $_POST['reg_team'],
		"bk_name" 			=> $_POST['f_bk_name']
	);

	if(!empty($_POST['f_run_s_no'])) {
		$deposit_upd_data['run_s_no'] = $_POST['f_run_s_no'];
		$deposit_upd_data['run_team'] = $_POST['f_run_team'];
	}

	$f_dp_account = $_POST['f_dp_account'];
	if(empty($f_dp_account)){
		if($_POST['f_my_c_no'] == '1'){
			$f_dp_account = '4';
		}elseif($_POST['f_my_c_no'] == '3'){
			$f_dp_account = '1';
		}
	}

	$deposit_upd_data["dp_account"]	 = $f_dp_account;
	$deposit_upd_data['dp_tax_date'] = !empty($_POST['f_dp_tax_date']) ? $_POST['f_dp_tax_date'] : "NULL";

	if(!empty($_POST['f_cost_info'])) {
		$deposit_upd_data["cost_info"] = addslashes($_POST['f_cost_info']);
	}

	if(!empty($_POST['f_dp_tax_item'])) {
		$deposit_upd_data["dp_tax_item"] = addslashes($_POST['f_dp_tax_item']);
	}

	$file_origin_path   = isset($_POST['file_origin_path']) ? $_POST['file_origin_path'] : "";
	$file_origin_name   = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
	$file_names         = isset($_POST['file_name']) ? $_POST['file_name'] : "";
	$file_paths         = isset($_POST['file_path']) ? $_POST['file_path'] : "";
	$file_chk           = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

	$move_file_path = "";
	$move_file_name = "";
	if($file_chk)
	{
		if(!empty($file_paths) && !empty($file_names))
		{
			$move_file_paths = move_store_files($file_paths, "dropzone_tmp", "asset");
			$move_file_name  = implode(',', $file_names);
			$move_file_path  = implode(',', $move_file_paths);
		}

		if(!empty($file_origin_path) && !empty($file_origin_name))
		{
			if(!empty($file_paths) && !empty($file_names)){
				$del_files = array_diff(explode(',', $file_origin_path), $file_paths);
			}else{
				$del_files = explode(',', $file_origin_path);
			}

			if(!empty($del_files))
			{
				del_files($del_files);
			}
		}

		if(!empty($move_file_path) && !empty($move_file_name)){
			$move_file_name_conv			= str_replace("'","", $move_file_name);
			$move_file_name_conv			= str_replace('"',"", $move_file_name_conv);
			$deposit_upd_data['file_path'] 	= $move_file_path;
			$deposit_upd_data['file_name'] 	= addslashes($move_file_name_conv);
		}else{
			$deposit_upd_data['file_path'] = "NULL";
			$deposit_upd_data['file_name'] = "NULL";
		}
	}

	if(!empty($_POST['mod_dp_no'])) {
		$deposit_upd_data["mod_dp_no"] = $_POST['mod_dp_no'];
	}

	if(!empty($_POST['mod_type'])) {
		$deposit_upd_data["mod_type"] = $_POST['mod_type'];
	}

	if(!empty($_POST['mod_content'])) {
		$deposit_upd_data["mod_content"] = addslashes($_POST['mod_content']);
	}

	$mod_file = $_FILES["mod_file"];
	if($mod_file)
	{
		$mod_save_path = add_store_file($mod_file, "deposit");

		if(!empty($mod_save_path)) {
			$mod_save_name	= str_replace("'","", $mod_file["name"]);
			$mod_save_name	= str_replace('"',"", $mod_save_name);
			$deposit_upd_data["mod_file_path"] = $mod_save_path;
			$deposit_upd_data["mod_file_name"] = addslashes($mod_save_name);
		}
	}

	$deposit_upd_data['dp_date'] = !empty($_POST['f_dp_date']) ? $_POST['f_dp_date'] : "NULL";
	if($_POST['f_dp_state'] == '2' && $_POST['f_dp_state'] != $_POST['f_dp_state_org'])
	{
		if(empty($_POST['f_run_s_no'])) {
			$deposit_upd_data['run_s_no'] = $session_s_no;
			$deposit_upd_data['run_team'] = $session_team;
		}

		if(empty($_POST['f_dp_date'])) {
			$deposit_upd_data['dp_date'] 		 = date("Y-m-d");
			$deposit_upd_data['incentive_month'] = date("Y-m");
		}

		if(empty($_POST['f_dp_money_vat'])) {
			$deposit_upd_data['dp_money'] 	 	= $supply_cost_value;
			$deposit_upd_data['dp_money_vat'] 	= $cost_value;
		}
	}

	if($deposit_model->update($deposit_upd_data)){
		exit ("<script>alert('수정하였습니다');location.href='deposit_list.php?{$search_url}';</script>");
	}else{
		exit ("<script>alert('수정에 실패했습니다');location.href='deposit_list.php?{$search_url}';</script>");
	}
}
elseif($process == "save_company")
{
	$dp_no 		= (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$search_url = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
	$upd_data 	= array("dp_no" => $dp_no, "c_no" => $_POST['f_c_no'], "c_name" => addslashes($_POST['f_c_name']), "s_no" => $_POST['s_no'], "team" => $_POST['team']);

	if(!$deposit_model->update($upd_data)){
		exit("<script>alert('업체정보와 담당자정보 저장에 실패 하였습니다');location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");
	}else{
		exit("<script>alert('업체정보와 담당자정보를 저장하였습니다');location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");
	}
}
elseif($process == "delete")
{
	$dp_no 		= (isset($_POST['dp_no']))?$_POST['dp_no']:"";
	$search_url = (isset($_POST['search_url']))?$_POST['search_url']:"";

	if (!permissionNameCheck($session_permission, "재무관리자")){
		$smarty->display('access_error.html');
		exit;
	}else{
		$work_chk_sql  	= "SELECT w.w_no FROM work w WHERE w.dp_no = '{$dp_no}'";
		$w_no_list  	= [];
		$work_chk_query = mysqli_query($my_db, $work_chk_sql);
		while($work_data = mysqli_fetch_array($work_chk_query)) {
			if(!!$w_no_list)
				$w_no_list .= ", '{$work_data['w_no']}'";
			else
				$w_no_list .= "'{$work_data['w_no']}'";
		}

		$sql = "UPDATE `deposit` SET display='2' WHERE dp_no = '{$dp_no}'";

		if(!mysqli_query($my_db,$sql)){
			echo ("<script>alert('입금 내역 삭제에 실패 하였습니다');</script>");
		}else{
			if(!empty($w_no_list))
			{
				$work_sql = "UPDATE `work` w SET w.dp_no = NULL WHERE w.w_no IN ({$w_no_list});";
				mysqli_query($my_db, $work_sql);
			}
			echo ("<script>alert('입금 내역을 삭제하였습니다');</script>");
		}

		exit ("<script>location.href='deposit_list.php?{$search_url}';</script>");
	}

// form action 이 복제일 경우
} elseif($process == "duplicate") {
	$dp_no =(isset($_POST['dp_no']))?$_POST['dp_no']:"";

	if (!permissionNameCheck($session_permission, "재무관리자")){
		$smarty->display('access_error.html');
		exit;
	}else{

		$duplicate_sql="INSERT IGNORE INTO deposit
							(my_c_no, dp_account, dp_subject, dp_state, dp_method, c_no, c_name, s_no, team, supply_cost, vat_choice, supply_cost_vat, biz_tax, local_tax, cost, cost_info, dp_money, dp_money_vat, dp_tax_state, dp_tax_date, dp_tax_item, regdate, reg_s_no, reg_team, display)
							SELECT my_c_no, dp_account, CONCAT(dp_subject, '_복제'), dp_state, dp_method, c_no, c_name, s_no, (SELECT s.team FROM staff s WHERE s_no = (SELECT dp.s_no FROM deposit dp WHERE dp.dp_no = '".$dp_no."')),supply_cost, vat_choice, supply_cost_vat, biz_tax, local_tax, cost, cost_info, dp_money, dp_money_vat, dp_tax_state, dp_tax_date, dp_tax_item, NOW(), '{$session_s_no}','{$session_team}','1'
					FROM deposit
					WHERE dp_no = '".$dp_no."'
					";
		//echo $duplicate_sql; exit;
		if(!mysqli_query($my_db,$duplicate_sql)){
			echo ("<script>alert('입금 내역 복제에 실패 하였습니다');</script>");
		}else{
			echo ("<script>alert('입금 내역을 복제하였습니다.');</script>");
		}

		$search_url = (isset($_POST['search_url']))?$_POST['search_url']:"";
		exit ("<script>location.href='deposit_list.php?{$search_url}';</script>");
	}

// 수정/환불 요청
} elseif($process == "mod_duplicate") {
	$dp_no = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$deposit_sql = "
		SELECT
			dp.*,
			(SELECT s_name FROM staff s WHERE s.s_no=dp.s_no) AS s_name,
			(SELECT depth FROM team t WHERE t.team_code=dp.team) AS t_depth,
			(SELECT s_name FROM staff s WHERE s.s_no={$session_s_no}) AS reg_s_name,
			(SELECT depth FROM team t WHERE t.team_code={$session_team}) AS reg_t_depth,
			(SELECT email FROM staff s WHERE s.s_no=dp.s_no) AS s_email,
			(SELECT s_name FROM staff s WHERE s.s_no=dp.run_s_no) AS run_name,
			(SELECT team_name FROM team t WHERE t.team_code=dp.run_team) AS run_t_name,
			c.tx_company,
			c.tx_company_number,
			c.tx_company_ceo,
			c.tx_manager,
			c.tx_call1,
			c.tx_call2,
			c.tx_email,
			c.o_registration,
			c.r_registration,
			c.o_accdoc,
			c.r_accdoc,
			c.o_etc,
			c.r_etc
		FROM deposit dp
		LEFT OUTER JOIN company c on dp.c_no = c.c_no
		WHERE dp.dp_no='{$dp_no}'
	";

	$deposit_result = mysqli_query($my_db, $deposit_sql);
	$deposit = mysqli_fetch_array($deposit_result);

	$deposit['tx_email'] = $deposit['tx_email'] != "@" ? $deposit['tx_email'] : "";

	if(!empty($deposit['regdate'])) {
		$regdate_day_value = date("Y/m/d",strtotime($deposit['regdate']));
		$regdate_time_value = date("H:i",strtotime($deposit['regdate']));
	}else{
		$regdate_day_value = '';
		$regdate_time_value = '';
	}

	$t_label 	 = getTeamFullName($my_db, $deposit['t_depth'], $deposit['team']);
	$reg_t_label = getTeamFullName($my_db, $deposit['reg_t_depth'], $session_team);

	$s_label 	 = ($t_label) ? "{$deposit['s_name']} ({$t_label})" : "";
	$reg_s_label = ($reg_t_label) ? "{$deposit['reg_s_name']} ({$reg_t_label})" : "";

	$smarty->assign(
		array(
			"f_my_c_no"		=> $deposit['my_c_no'],
			"f_dp_account"	=> $deposit['dp_account'],
			"f_dp_subject"	=> stripslashes($deposit['dp_subject']),
			"f_c_no"		=> $deposit['c_no'],
			"f_c_name"		=> stripslashes($deposit['c_name']),
			"s_no"			=> $deposit['s_no'],
			"s_name"		=> $s_label,
			"team"			=> $deposit['team'],
			"f_tx_company"		  => $deposit['tx_company'],
			"f_tx_company_number" => stripslashes($deposit['tx_company_number']),
			"f_tx_company_ceo"	  => stripslashes($deposit['tx_company_ceo']),
			"f_bk_manager"	=> $deposit['bk_manager'],
			"f_tx_manager"	=> $deposit['tx_manager'],
			"f_tx_call1"	=> $deposit['tx_call1'],
			"f_tx_call2"	=> $deposit['tx_call2'],
			"f_tx_email"	=> $deposit['tx_email'],
			"f_o_registdoc" => stripslashes($deposit['o_registration']),
			"f_r_registdoc" => $deposit['r_registration'],
			"f_o_accdoc" 	=> stripslashes($deposit['o_accdoc']),
			"f_r_accdoc" 	=> $deposit['r_accdoc'],
			"f_o_etc" 		=> stripslashes($deposit['o_etc']),
			"f_r_etc" 		=> $deposit['r_etc'],
			"reg_s_name"	=> $reg_s_label,
			"reg_s_no"		=> $session_s_no,
			"reg_team"		=> $session_team,
			"s_email"		=> $deposit['s_email'],
			"mod_dp_no" 	=> $dp_no,
			"mod_type" 		=> "",
			"mod_file_path" => "",
			"mod_file_name" => "",
			"mod_content" 	=> "수정/환불 사유:\r\n상세 수정 내용:\r\n기타:",
			"f_bk_name"		=> $deposit['bk_name'],
			"f_run_s_no"	=> $deposit['run_s_no'],
			"f_run_team"	=> $deposit['run_team'],
			"f_run_name"	=> !empty($deposit['run_s_no']) ? $deposit['run_name']."({$deposit['run_t_name']})" : "",
		)
	);

	$tx_emails = explode("@", $deposit['tx_email']);
	$smarty->assign(
		array(
			"tx_email_0"=>$tx_emails[0],
			"tx_email_1"=>$tx_emails[1]
		)
	);

	$check_msg = "";
	$check = array();

	if (!$deposit['o_registration'])
		$check[] = "사업자등록증(파일)";
	if (!$deposit['tx_company'])
		$check[] = "사업자등록 업체명";
	if (!$deposit['tx_company_number'])
		$check[] = "사업자등록 번호";
	if (!$deposit['tx_company_ceo'])
		$check[] = "대표자명";

	if (count($check) > 0) {
		$msg = implode(", ", $check);
		$check_msg = "자료없음 (" . $msg . ")";
	}

	$smarty->assign("check_msg", $check_msg);


// 입금명에 대한 마케터 수정일 경우
} elseif($process == "f_dp_subject") {
	$dp_no = (isset($_POST['dp_no']))?$_POST['dp_no']:"";
	$dp_subject_value = (isset($_POST['f_dp_subject']))?$_POST['f_dp_subject']:"";

	$sql = "UPDATE `deposit` SET dp_subject='$dp_subject_value' WHERE dp_no = '$dp_no'";
	//echo $sql;exit;

	if(!mysqli_query($my_db,$sql)){
		echo ("<script>alert('입금명 수정에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('입금명을 수정하였습니다');</script>");
	}

	$search_url = (isset($_POST['search_url']))?$_POST['search_url']:"";
	exit ("<script>location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");

// 예정 입금액에 대한 마케터 수정일 경우
} elseif($process == "f_supply_cost") {
	$dp_no = (isset($_POST['dp_no']))?$_POST['dp_no']:"";
	$search_url = (isset($_POST['search_url']))?$_POST['search_url']:"";

	$vat_choice_value = str_replace(",","",trim($_POST['f_vat_tax_choice'])); // 컴마 제거하기
	$supply_cost_value = str_replace(",","",trim($_POST['f_supply_cost'])); // 컴마 제거하기
	$supply_cost_vat_value = str_replace(",","",trim($_POST['f_supply_cost_vat'])); // 컴마 제거하기
	$biz_tax_value = str_replace(",","",trim($_POST['f_biz_tax'])); // 컴마 제거하기
	$local_tax_value = str_replace(",","",trim($_POST['f_local_tax'])); // 컴마 제거하기
	$cost_value = str_replace(",","",trim($_POST['f_cost'])); // 컴마 제거하기

	$sql = "UPDATE `deposit`
					SET supply_cost='$supply_cost_value',
				 		vat_choice='$vat_choice_value',
						supply_cost_vat='$supply_cost_vat_value',
						biz_tax='$biz_tax_value',
						local_tax='$local_tax_value',
						cost='$cost_value'
					WHERE dp_no = '$dp_no'";
	//echo $sql;exit;

	if(!mysqli_query($my_db,$sql)){
		echo ("<script>alert('예정 입금액 수정에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('예정 입금액을 수정하였습니다');</script>");
	}

	$search_url = (isset($_POST['search_url']))?$_POST['search_url']:"";
	exit ("<script>location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");

// 상품내역 대한 마케터 수정일 경우
} elseif($process == "f_cost_info") {
	$dp_no = (isset($_POST['dp_no']))?$_POST['dp_no']:"";
	$f_cost_info_value = (isset($_POST['f_cost_info']))?$_POST['f_cost_info']:"";

	$sql = "UPDATE `deposit` SET cost_info='$f_cost_info_value' WHERE dp_no = '$dp_no'";
	//echo $sql;exit;

	if(!mysqli_query($my_db,$sql)){
		echo ("<script>alert('상품내역 수정에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('상품내역을 수정하였습니다');</script>");
	}

	$search_url = (isset($_POST['search_url']))?$_POST['search_url']:"";
	exit ("<script>location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");


}  elseif($process == "f_dp_tax_state") {
	$dp_no = (isset($_POST['dp_no']))?$_POST['dp_no']:"";
	$f_dp_tax_state_value = (isset($_POST['f_dp_tax_state']))?$_POST['f_dp_tax_state']:"";

	$sql = "UPDATE `deposit` SET dp_tax_state='{$f_dp_tax_state_value}' WHERE dp_no = '{$dp_no}'";
	//echo $sql;exit;

	if(!mysqli_query($my_db,$sql)){
		echo ("<script>alert('계산서 발행상태 수정에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('계산서 발행상태를 수정하였습니다');</script>");
	}

	$search_url = (isset($_POST['search_url']))?$_POST['search_url']:"";
	exit ("<script>location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");

// 계산서 발행항목 대한 마케터 수정일 경우
}elseif($process == "f_dp_tax_item") {
	$dp_no = (isset($_POST['dp_no']))?$_POST['dp_no']:"";
	$f_dp_tax_item_value = (isset($_POST['f_dp_tax_item']))?$_POST['f_dp_tax_item']:"";

	$sql = "UPDATE `deposit` SET dp_tax_item='$f_dp_tax_item_value' WHERE dp_no = '$dp_no'";
	//echo $sql;exit;

	if(!mysqli_query($my_db,$sql)){
		echo ("<script>alert('계산서 발행항목 수정에 실패 하였습니다');</script>");
	}else{
		echo ("<script>alert('계산서 발행항목을 수정하였습니다');</script>");
	}

	$search_url = (isset($_POST['search_url']))?$_POST['search_url']:"";
	exit ("<script>location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");

}
elseif ($process == "add_comment")
{
	$dp_no 		 	= (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$comment_new 	= (isset($_POST['f_comment_new'])) ? addslashes($_POST['f_comment_new']) : "";
	$regdate     	= date("Y-m-d H:i:s");

	$ins_sql		= "
		INSERT INTO linked_comment SET 
			linked_table= 'deposit', 
			linked_no	= '{$dp_no}', 
			`comment`	= '{$comment_new}', 
			team		= '{$session_team}', 
			s_no		= '{$session_s_no}',
		    regdate		= '{$regdate}'                       
   	";

	$comment_file = $_FILES["f_comm_file"];
	if($comment_file['tmp_name'])
	{
		$file_path = add_store_file($comment_file, "linked_comment");

		if(!empty($file_path)) {
			$ins_sql .= " ,file_path = '{$file_path}'";
		}

		if(!empty($comment_file["name"])) {
			$ins_sql .= " ,file_name = '{$comment_file["name"]}'";
		}
	}

	if(mysqli_query($my_db, $ins_sql)){
		exit ("<script>alert('코멘트 등록에 성공했습니다.'); location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");
	}else{
		exit ("<script>alert('코멘트 등록에 실패했습니다.'); location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");
	}
}
elseif ($process == "del_comment")
{
	$dp_no 		= (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";
	$comment_no = (isset($_POST['comment_no'])) ? $_POST['comment_no'] : "";

	$upd_sql 	= "UPDATE linked_comment SET display='2' WHERE linked_table='deposit' AND linked_no='{$dp_no}' AND `no`='{$comment_no}'";

	if(mysqli_query($my_db, $upd_sql)){
		exit ("<script>alert('코멘트 삭제에 성공했습니다.'); location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");
	}else{
		exit ("<script>alert('코멘트 삭제에 실패했습니다.'); location.href='deposit_regist.php?dp_no={$dp_no}&{$search_url}';</script>");
	}
}

# 등록/수정 사용변수 & 옵션
$dp_no 			= isset($_GET['dp_no']) ? $_GET['dp_no'] : "";
$mode 			= (!$dp_no) ? "write" : "modify";
$my_c_no 		= isset($_GET['my_c_no']) && !empty($_GET['my_c_no']) ? $_GET['my_c_no'] : '1';
$super_admin	= (permissionNameCheck($session_permission,"재무관리자")) ? true : false;

$corp_model 		 		= Corporation::Factory();
$corp_account_option 		= $corp_model->getAccountList('1');
$corp_account_option[1][5] 	= "기업-지출";
$cur_date 					= date("Ymd");
$modal_date_chk 			= ($cur_date < 20230916) ? true : false;
$my_company_model 	 		= MyCompany::Factory();
$my_company_list	 		= $my_company_model->getNameList();

if($mode == "modify")
{
	$deposit_sql = "
		SELECT
			dp.*,
			(SELECT s_name FROM staff s WHERE s.s_no=dp.s_no) AS s_name,
			(SELECT depth FROM team t WHERE t.team_code=dp.team) AS t_depth,
			(SELECT s_name FROM staff s WHERE s.s_no=dp.reg_s_no) AS reg_s_name,
			(SELECT depth FROM team t WHERE t.team_code=dp.reg_team) AS reg_t_depth,
			(SELECT email FROM staff s WHERE s.s_no=dp.s_no) AS s_email,
			(SELECT s_name FROM staff s WHERE s.s_no=dp.run_s_no) AS run_name,
			(SELECT team_name FROM team t WHERE t.team_code=dp.run_team) AS run_t_name,
			c.tx_company,
			c.tx_company_number,
			c.tx_company_ceo,
			c.tx_manager,
			c.tx_call1,
			c.tx_call2,
			c.tx_email,
			c.o_registration,
			c.r_registration,
			c.o_accdoc,
			c.r_accdoc,
			c.o_etc,
			c.r_etc
		FROM deposit dp
		LEFT OUTER JOIN company c on dp.c_no = c.c_no
		WHERE dp.dp_no='{$dp_no}'
	";
	$deposit_query 	= mysqli_query($my_db, $deposit_sql);
	$deposit 		= mysqli_fetch_array($deposit_query);

	$deposit['tx_email'] = $deposit['tx_email'] != "@" ? $deposit['tx_email'] : "";

	if(!empty($deposit['regdate'])) {
		$regdate_day_value = date("Y/m/d",strtotime($deposit['regdate']));
		$regdate_time_value = date("H:i",strtotime($deposit['regdate']));
	}else{
		$regdate_day_value = '';
		$regdate_time_value = '';
	}

	$t_label 	 = getTeamFullName($my_db, $deposit['t_depth'], $deposit['team']);
    $reg_t_label = getTeamFullName($my_db, $deposit['reg_t_depth'], $deposit['reg_team']);

    $s_label 	 = ($t_label) ? "{$deposit['s_name']} ({$t_label})" : "";
    $reg_s_label = ($reg_t_label) ? "{$deposit['reg_s_name']} ({$reg_t_label})" : "";

	$file_paths = $deposit['file_path'];
	$file_names = $deposit['file_name'];
	$file_count = 0;
	if(!empty($file_paths) && !empty($file_names))
	{
		$file_path_list = explode(',', $file_paths);
		$file_name_list = explode(',', $file_names);
		$file_count		= count($file_path_list);

		$smarty->assign("file_paths", $file_path_list);
		$smarty->assign("file_names", $file_name_list);
		$smarty->assign("file_origin_path", $file_paths);
		$smarty->assign("file_origin_name", $file_names);
	}
	$smarty->assign("file_count", $file_count);

	$smarty->assign(
		array(
			"dp_no"				=> $deposit['dp_no'],
			"f_my_c_no"			=> $deposit['my_c_no'],
			"f_dp_account"		=> $deposit['dp_account'],
			"f_dp_subject"		=> stripslashes($deposit['dp_subject']),
			"f_dp_state"		=> $deposit['dp_state'],
			"f_dp_date"			=> $deposit['dp_date'],
			"f_dp_method"		=> $deposit['dp_method'],
			"f_c_no"			=> $deposit['c_no'],
			"f_c_name"			=> stripslashes($deposit['c_name']),
			"s_no"				=> $deposit['s_no'],
			"s_name"			=> $s_label,
			"team"				=> $deposit['team'],
			"f_supply_cost"		=> number_format($deposit['supply_cost']),
			"f_vat_tax_choice"	=> $deposit['vat_choice'],
			"f_supply_cost_vat"	=> number_format($deposit['supply_cost_vat']),
			"f_biz_tax"			=> number_format($deposit['biz_tax']),
			"f_local_tax"		=> number_format($deposit['local_tax']),
			"f_cost"			=> number_format($deposit['cost']),
			"f_cost_info"		=> stripslashes($deposit['cost_info']),
			"f_dp_money"		=> number_format($deposit['dp_money']),
			"f_dp_money_vat"	=> number_format($deposit['dp_money_vat']),
			"f_dp_tax_state" 	=> $deposit['dp_tax_state'],
			"f_dp_tax_date"	 	=> $deposit['dp_tax_date'],
			"f_dp_tax_item"		=> stripslashes($deposit['dp_tax_item']),
			"f_tx_company"			=> $deposit['tx_company'],
			"f_tx_company_number"	=> stripslashes($deposit['tx_company_number']),
			"f_tx_company_ceo"		=> stripslashes($deposit['tx_company_ceo']),
			"f_bk_manager"		=> $deposit['bk_manager'],
			"f_tx_manager"		=> $deposit['tx_manager'],
			"f_tx_call1"		=> $deposit['tx_call1'],
			"f_tx_call2"		=> $deposit['tx_call2'],
			"f_tx_email"		=> $deposit['tx_email'],
			"f_o_registdoc" 	=> stripslashes($deposit['o_registration']),
			"f_r_registdoc" 	=> $deposit['r_registration'],
			"f_o_accdoc" 		=> stripslashes($deposit['o_accdoc']),
			"f_r_accdoc"		=> $deposit['r_accdoc'],
			"f_o_etc" 			=> stripslashes($deposit['o_etc']),
			"f_r_etc" 			=> $deposit['r_etc'],
			"regdate_day"		=> $regdate_day_value,
			"regdate_time"		=> $regdate_time_value,
			"reg_s_name"		=> $reg_s_label,
			"reg_s_no"			=> $deposit['reg_s_no'],
			"reg_team"			=> $deposit['reg_team'],
			"s_email"			=> $deposit['s_email'],
			"mod_dp_no" 		=> $deposit["mod_dp_no"],
			"mod_type" 			=> $deposit["mod_type"],
			"mod_content" 		=> $deposit["mod_content"],
			"mod_file_path" 	=> $deposit["mod_file_path"],
			"mod_file_name" 	=> $deposit["mod_file_name"],
			"f_bk_name"			=> $deposit['bk_name'],
			"f_run_s_no"		=> $deposit['run_s_no'],
			"f_run_team"		=> $deposit['run_team'],
			"f_run_name"		=> !empty($deposit['run_s_no']) ? $deposit['run_name']."({$deposit['run_t_name']})" : "",
		)
	);

	$tx_emails = explode("@", $deposit['tx_email']);
	$smarty->assign(
		array(
			"tx_email_0"=>$tx_emails[0],
			"tx_email_1"=>$tx_emails[1]
		)
	);

	$check_msg = "";
	$check = array();

	if (!$deposit['o_registration'])
		$check[] = "사업자등록증(파일)";
	if (!$deposit['tx_company'])
		$check[] = "사업자등록 업체명";
	if (!$deposit['tx_company_number'])
		$check[] = "사업자등록 번호";
	if (!$deposit['tx_company_ceo'])
		$check[] = "대표자명";

	if (count($check) > 0) {
		$msg = implode(", ", $check);
		$check_msg = "자료없음 (" . $msg . ")";
	}

	$smarty->assign("check_msg", $check_msg);

	# Comment
	$linked_comment_sql 	= "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=lc.s_no) as s_name, (SELECT t.team_name FROM team t WHERE t.team_code=lc.team) as t_name  FROM linked_comment lc WHERE lc.linked_table='deposit' AND lc.linked_no='{$dp_no}' AND lc.display=1";
	$linked_comment_query	= mysqli_query($my_db, $linked_comment_sql);
	$linked_comment_list 	= [];
	while($linked_comment = mysqli_fetch_assoc($linked_comment_query))
	{
		$linked_comment['comment'] = str_replace("\r\n","<br />", htmlspecialchars($linked_comment['comment']));
		$linked_comment_list[] = $linked_comment;
	}
	$smarty->assign("linked_comment_list", $linked_comment_list);
}
elseif($mode == 'write')
{
    $reg_t_label = getTeamFullName($my_db, 0, $session_team);
	$reg_s_label = "{$session_name} ({$reg_t_label})";

	$smarty->assign("reg_s_no", $session_s_no);
	$smarty->assign("reg_team", $session_team);
	$smarty->assign("reg_s_name", $reg_s_label);
}

$smarty->assign("my_c_no", $my_c_no);
$smarty->assign("mode", $mode);
$smarty->assign("super_admin", $super_admin);
$smarty->assign("my_company_list", $my_company_list);
$smarty->assign("corp_account_option", $corp_account_option);
$smarty->assign("deposit_method_option", getDpMethodOption());
$smarty->assign("mod_type_list", getDpModTypeOption());
$smarty->assign("dp_state_option", getDpStateOption());
$smarty->assign("dp_tax_option", getDpTaxOption());
$smarty->assign("modal_date_chk", $modal_date_chk);

$smarty->display('deposit_regist.html');
?>
