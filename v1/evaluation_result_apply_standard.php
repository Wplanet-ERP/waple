<?php
require('inc/common.php');
require('ckadmin.php');

ini_set('max_execution_time', 3000);

$ev_no      = isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$type       = isset($_GET['type']) ? $_GET['type'] : "list";
$move_url   = ($type == 'regist') ? "evaluation_regist.php?ev_no={$ev_no}" : "evaluation_system.php";

if($type == "setting"){
    $move_url = "evaluation_setting_list.php";
}

if(!empty($ev_no))
{
    $result_list    = [];
    $sqrt_list      = [];

    # 평가자
    $evaluator_sql = "
        SELECT 
            DISTINCT evaluator_s_no
        FROM evaluation_system_result
        WHERE ev_no='{$ev_no}'
        ORDER BY evaluator_s_no ASC
    ";
    $evaluator_result = mysqli_query($my_db, $evaluator_sql);
    while($evaluator_array = mysqli_fetch_array($evaluator_result))
    {
        $unit_sql = "
            SELECT 
                DISTINCT ev_u_no
            FROM evaluation_system_result
            WHERE ev_no='{$ev_no}' AND evaluator_s_no='{$evaluator_array['evaluator_s_no']}' AND (evaluation_state='1' OR evaluation_state='2' OR evaluation_state='3')
            ORDER BY ev_u_no ASC
        ";
        $unit_result = mysqli_query($my_db, $unit_sql);
        while($unit_array = mysqli_fetch_array($unit_result))
        {
            #평가자의 평가지 항목별 점수(자기평가 제외)
            $unit_value_sql = "
                SELECT
                    ev_r_no,
                    rate,
                    evaluation_value
                FROM evaluation_system_result
                WHERE ev_no='{$ev_no}' AND evaluator_s_no='{$evaluator_array['evaluator_s_no']}' AND ev_u_no='{$unit_array['ev_u_no']}' AND evaluator_s_no <> receiver_s_no
                ORDER BY ev_r_no ASC
            ";
            $unit_value_result = mysqli_query($my_db,$unit_value_sql);
            $sqrt_array = [];
            while($unit_value_array=mysqli_fetch_array($unit_value_result))
            {
                $result_list[]=array(
                    "ev_u_no"           => $unit_array['ev_u_no'],
                    "evaluator_s_no"    => $evaluator_array['evaluator_s_no'],
                    "ev_r_no"           => $unit_value_array['ev_r_no'],
                    "rate"              => $unit_value_array['rate'],
                    "evaluation_value"  => $unit_value_array['evaluation_value']
                );

                $sqrt_array[] = $unit_value_array['evaluation_value'];
            }

            if(!empty($sqrt_array))
            {
                $sqrt_average = array_sum(array_filter($sqrt_array))/count($sqrt_array);
                for ($i=0 , $s=0 ; $i<count($sqrt_array) ; $i++) {
                    $s += pow($sqrt_array[$i] - $sqrt_average, 2);
                }

                $sqrt_list[]=array(
                    "ev_u_no"           => $unit_array['ev_u_no'],
                    "evaluator_s_no"    => $evaluator_array['evaluator_s_no'],
                    "average"           => $sqrt_average,
                    "sqrt"              => round(SQRT($s/count($sqrt_array)), 2)
                );
            }
        }
    }

    for ($idx_1=0 ; $idx_1<count($result_list) ; $idx_1++){
        for ($idx_2=0 ; $idx_2<count($sqrt_list) ; $idx_2++){
            if($result_list[$idx_1]['ev_u_no'] == $sqrt_list[$idx_2]['ev_u_no'] && $result_list[$idx_1]['evaluator_s_no'] == $sqrt_list[$idx_2]['evaluator_s_no']){
                if($sqrt_list[$idx_2]['sqrt'] > 0){
                    $sqrt_value = round((($result_list[$idx_1]['evaluation_value'] - $sqrt_list[$idx_2]['average']) / $sqrt_list[$idx_2]['sqrt']) * 10 + 70, 2);
                }else{ // 표준편차가 0일 경우(피평가자의 항목이 한가지 일때)
                    $sqrt_value = 70;
                }
                $result_list[$idx_1]['sqrt_value'] = $sqrt_value;
            }
        }
    }

    foreach($result_list as $result)
    {
        $upd_sql = "UPDATE evaluation_system_result SET sqrt='{$result['sqrt_value']}' WHERE ev_no='{$ev_no}' AND ev_r_no='{$result['ev_r_no']}' AND evaluator_s_no='{$result['evaluator_s_no']}' AND ev_u_no='{$result['ev_u_no']}'";
        mysqli_query($my_db, $upd_sql);
    }

    exit("<script>alert('표준편차 데이터가 저장되었습니다.');location.href='{$move_url}';</script>");
}
else
{
    exit("<script>alert('ev_no 가 없습니다. 다시 시도해 주세요.');location.href='evaluation_system.php';</script>");
}

?>
