<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/logistics.php');
require('inc/helper/work.php');
require('Classes/PHPExcel.php');

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);


$lfcr = chr(10) ;

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', "no")
    ->setCellValue('B1', "작성일")
    ->setCellValue('C1', "진행상태")
    ->setCellValue('D1', "업무완료일")
    ->setCellValue('E1', "문서번호")
    ->setCellValue('F1', "w_no")
    ->setCellValue('G1', "업무요청자")
    ->setCellValue('H1', "입고/반출")
    ->setCellValue('I1', "반출::계정과목")
    ->setCellValue('J1', "납품업체")
    ->setCellValue('K1', "처리형태")
    ->setCellValue('L1', "출고희망일")
    ->setCellValue('M1', "요청사항")
    ->setCellValue('N1', "발송지")
    ->setCellValue('O1', "수령지")
    ->setCellValue('P1', "사유")
    ->setCellValue('Q1', "커머스 상품")
    ->setCellValue('R1', "수량")
    ->setCellValue('S1', "창고")
    ->setCellValue('T1', "구성품목(SKU)")
    ->setCellValue('U1', "수량")
    ->setCellValue('V1', "주문번호")
    ->setCellValue('W1', "업무처리자")
    ->setCellValue('X1', "업무진행")
    ->setCellValue('Y1', "관리자메모")
;

# 검색조건
$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));
$years_val  = date('Y-m-d', strtotime('-2 years'));

$add_where          = "1=1";
$sch_lm_no          = isset($_GET['sch_lm_no']) ? $_GET['sch_lm_no'] : "";
$sch_reg_s_date     = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : $month_val;
$sch_reg_e_date     = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : $today_val;
$sch_reg_date_type  = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "month";
$sch_work_state     = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_run_s_date     = isset($_GET['sch_run_s_date']) ? $_GET['sch_run_s_date'] : "";
$sch_run_e_date     = isset($_GET['sch_run_e_date']) ? $_GET['sch_run_e_date'] : "";
$sch_doc_no         = isset($_GET['sch_doc_no']) ? $_GET['sch_doc_no'] : "";
$sch_req_name       = isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : "";
$sch_req_my         = isset($_GET['sch_req_my']) ? $_GET['sch_req_my'] : "";
$sch_stock_type     = isset($_GET['sch_stock_type']) ? $_GET['sch_stock_type'] : "";
$sch_subject        = isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
$sch_delivery_type  = isset($_GET['sch_delivery_type']) ? $_GET['sch_delivery_type'] : "";
$sch_warehouse      = isset($_GET['sch_warehouse']) ? $_GET['sch_warehouse'] : "";
$sch_sender_type    = isset($_GET['sch_sender_type']) ? $_GET['sch_sender_type'] : "";
$sch_receiver_type  = isset($_GET['sch_receiver_type']) ? $_GET['sch_receiver_type'] : "";
$sch_is_order       = isset($_GET['sch_is_order']) ? $_GET['sch_is_order'] : "";
$sch_run_name       = isset($_GET['sch_run_name']) ? $_GET['sch_run_name'] : "";
$sch_run_my         = isset($_GET['sch_run_my']) ? $_GET['sch_run_my'] : "";
$sch_task_run       = isset($_GET['sch_task_run']) ? $_GET['sch_task_run'] : "";
$sch_quick_type     = isset($_GET['sch_quick_type']) ? $_GET['sch_quick_type'] : "";
$sch_warehouse      = isset($_GET['sch_warehouse']) ? $_GET['sch_warehouse'] : "";
$sch_reason         = isset($_GET['sch_reason']) ? $_GET['sch_reason'] : "";
$sch_order_number   = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_quick_req_date = isset($_GET['sch_quick_req_date']) ? $_GET['sch_quick_req_date'] : "";
$sch_quick_req_memo = isset($_GET['sch_quick_req_memo']) ? $_GET['sch_quick_req_memo'] : "";
$sch_prd_sku        = isset($_GET['sch_prd_sku']) ? $_GET['sch_prd_sku'] : "";

if(!empty($sch_quick_type))
{
    switch($sch_quick_type){
        case '1':
            $sch_work_state = 3;
            $add_where  .= " AND run_c_no='5468'";
            break;
        case '2':
            $sch_work_state = 3;
            $add_where  .= " AND run_c_no='5469'";
            break;
        case '3':
            $sch_work_state = 3;
            $add_where  .= " AND (run_c_no NOT IN('5468','5469') AND run_c_no > 0)";
            break;
    }
}

if(!empty($sch_lm_no)) {
    $add_where  .= " AND lm.lm_no='{$sch_lm_no}'";
}

if(!empty($sch_reg_s_date) || !empty($sch_reg_e_date))
{
    if(!empty($sch_reg_s_date) && empty($sch_reg_e_date)){
        $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
        $add_where .= " AND lm.regdate >= '{$sch_reg_s_datetime}'";
    }elseif(!empty($sch_reg_e_date) && empty($sch_reg_s_date)){
        $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
        $add_where .= " AND lm.regdate <= '{$sch_reg_e_datetime}'";
    }else{
        $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
        $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";

        $add_where .= " AND (lm.regdate BETWEEN '{$sch_reg_s_datetime}' AND '{$sch_reg_e_datetime}')";
    }
}

if(!empty($sch_work_state)) {
    $add_where  .= " AND lm.work_state='{$sch_work_state}'";
}

if(!empty($sch_run_s_date) || !empty($sch_run_e_date))
{
    if(!empty($sch_run_s_date) && empty($sch_run_e_date)){
        $sch_run_s_datetime = $sch_run_s_date;
        $add_where .= " AND lm.run_date >= '{$sch_run_s_datetime}'";
    }elseif(!empty($sch_run_e_date) && empty($sch_run_s_date)){
        $sch_run_e_datetime = $sch_run_e_date;
        $add_where .= " AND lm.run_date <= '{$sch_run_e_datetime}'";
    }else{
        $sch_run_s_datetime = $sch_run_s_date;
        $sch_run_e_datetime = $sch_run_e_date;

        $add_where .= " AND (lm.run_date BETWEEN '{$sch_run_s_datetime}' AND '{$sch_run_e_datetime}')";
    }
}

if(!empty($sch_doc_no)) {
    $add_where  .= " AND lm.doc_no LIKE '%{$sch_doc_no}%'";
}

if(!empty($sch_req_my)){
    $add_where  .= " AND lm.req_s_no ='{$session_s_no}'";
}else{
    if(!empty($sch_req_name)) {
        $add_where  .= " AND lm.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_req_name}%')";
    }
}

if(!empty($sch_stock_type)) {
    $add_where  .= " AND lm.stock_type='{$sch_stock_type}'";
}

if(!empty($sch_subject)) {
    $add_where  .= " AND lm.subject='{$sch_subject}'";
}

if(!empty($sch_delivery_type)) {
    if($sch_delivery_type == "5"){
        $add_where  .= " AND lm.delivery_type != '4'";
    }else{
        $add_where  .= " AND lm.delivery_type='{$sch_delivery_type}'";
    }
}

if(!empty($sch_warehouse)) {
    $add_where  .= " AND lm.lm_no IN(SELECT DISTINCT lm_no FROM logistics_product sub LEFT JOIN product_cms_relation pcr ON pcr.prd_no=sub.prd_no LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcr.option_no WHERE prd_type='1' AND pcr.display='1' AND pcu.warehouse='{$sch_warehouse}')";
}

if(!empty($sch_sender_type)) {
    $add_where  .= " AND lm.sender_type='{$sch_sender_type}'";
}

if(!empty($sch_receiver_type)) {
    $add_where  .= " AND lm.receiver_type='{$sch_receiver_type}'";
}

if(!empty($sch_is_order)) {
    if($sch_is_order == '1'){
        $add_where  .= " AND lm.order_number != ''";
    }else{
        $add_where  .= " AND (lm.order_number IS NULL OR lm.order_number = '')";
    }
}

if(!empty($sch_run_my)){
    $add_where  .= " AND lm.run_s_no ='{$session_s_no}'";
}else{
    if(!empty($sch_run_name)) {
        $add_where  .= " AND lm.run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_run_name}%')";
    }
}

if(!empty($sch_task_run)) {
    $add_where  .= " AND lm.task_run LIKE '%{$sch_task_run}%'";
}

if(!empty($sch_reason)) {
    $add_where  .= " AND lm.reason LIKE '%{$sch_reason}%'";
}

if(!empty($sch_order_number)){
    $add_where  .= " AND FIND_IN_SET('{$sch_order_number}',lm.order_number) > 0";
}

if(!empty($sch_quick_req_date)){
    $add_where  .= " AND quick_req_date='{$sch_quick_req_date}'";
}

if(!empty($sch_quick_req_memo)){
    $add_where  .= " AND quick_req_memo LIKE '%{$sch_quick_req_memo}%'";
}

if(!empty($sch_prd_sku)){
    $add_where  .= " AND 
    (
        lm_no IN(SELECT lp.lm_no FROM logistics_product lp WHERE lp.prd_type='1' AND lp.prd_no IN(SELECT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.option_no IN(SELECT pcm.prd_unit FROM product_cms_unit_management pcm WHERE pcm.sku LIKE '%{$sch_prd_sku}%') AND pcr.display))
        OR
        lm_no IN(SELECT lp.lm_no FROM logistics_product lp WHERE lp.prd_type='2' AND lp.prd_no IN(SELECT pcm.prd_unit FROM product_cms_unit_management pcm WHERE pcm.sku LIKE '%{$sch_prd_sku}%'))
    )    
    ";
}

# 증명서 발급 리스트
$logistics_sql = "
    SELECT
        *,
        DATE_FORMAT(lm.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(lm.regdate, '%H:%i') as reg_time,
        (SELECT s.s_name FROM staff s WHERE s.s_no=lm.req_s_no) as req_s_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=lm.req_team) as req_t_name,
        (SELECT c.c_name FROM company c WHERE c.c_no=lm.run_c_no) as run_c_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no=lm.run_s_no) as run_s_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=lm.run_team) as run_t_name,
        (SELECT COUNT(pcsr.`no`) FROM product_cms_stock_report pcsr WHERE pcsr.log_doc_no=lm.doc_no AND pcsr.type='1') as in_stock_count,
        (SELECT COUNT(pcsr.`no`) FROM product_cms_stock_report pcsr WHERE pcsr.log_doc_no=lm.doc_no AND pcsr.type='2') as out_stock_count
    FROM logistics_management as lm
    WHERE {$add_where}
    ORDER BY lm_no DESC
";
$logistics_query        = mysqli_query($my_db, $logistics_sql);
$work_state_option      = getWorkStateOption();
$subject_option         = getSubjectOption();
$subject_name_option    = getSubjectNameOption();
$stock_option           = getStockTypeOption();
$delivery_option        = getDeliveryTypeOption();
$address_option         = getAddrCompanyOption();
$tmp_log_c_no           = '2809';

$work_sheet = $objPHPExcel->setActiveSheetIndex(0);
$idx        = 2;
while($logistics_result = mysqli_fetch_assoc($logistics_query))
{
    $logistics_result['work_state_name']    = isset($work_state_option[$logistics_result['work_state']]) ? $work_state_option[$logistics_result['work_state']] : "";
    $logistics_result['subject_name']       = isset($subject_name_option[$logistics_result['subject']]) ? $subject_name_option[$logistics_result['subject']] : "";
    $logistics_result['stock_type_name']    = isset($stock_option[$logistics_result['stock_type']]) ? $stock_option[$logistics_result['stock_type']] : "";
    $logistics_result['delivery_type_name'] = isset($delivery_option[$logistics_result['delivery_type']]) ? $delivery_option[$logistics_result['delivery_type']] : "";
    $logistics_result['run_c_name']         = isset($address_option[$logistics_result['run_c_no']]) ? $address_option[$logistics_result['run_c_no']] : $logistics_result['run_c_name'];
    $logistics_result['order_number_list']  = !empty($logistics_result['order_number']) ? str_replace(",", $lfcr, $logistics_result['order_number']) : "";

    $prd_type = $logistics_result['prd_type'];
    $prd_list = [];
    $row_idx  = 0;
    if($prd_type == "1")
    {
        $prd_sql   = "SELECT lp.log_c_no, lp.prd_no, (SELECT p.title FROM product_cms p WHERE p.prd_no=lp.prd_no) as prd_name, lp.quantity FROM logistics_product as lp WHERE lm_no='{$logistics_result['lm_no']}' AND prd_type='{$prd_type}'";
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $unit_sql   = "SELECT pcum.prd_unit as prd_no, pcum.sku, pcr.quantity, pcum.warehouse FROM product_cms_relation pcr LEFT JOIN product_cms_unit_management as pcum ON pcum.prd_unit=pcr.option_no AND pcum.log_c_no='{$prd['log_c_no']}' WHERE pcr.prd_no='{$prd['prd_no']}' AND pcr.display='1' AND pcum.log_c_no='{$prd['log_c_no']}'";
            $unit_query = mysqli_query($my_db, $unit_sql);
            $unit_list  = [];
            while($unit = mysqli_fetch_assoc($unit_query))
            {
                $unit['qty'] = $prd['quantity']*$unit['quantity'];
                $unit_list[] = $unit;
                $row_idx++;
            }
            $prd['unit_list'] = $unit_list;
            $prd['unit_cnt']  = count($unit_list);
            $prd_list[] = $prd;
        }
    }elseif($prd_type == "2"){
        $prd_sql   = "SELECT lp.prd_no, pcum.sku as prd_name, lp.quantity, pcum.warehouse FROM logistics_product as lp LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=lp.prd_no AND pcum.log_c_no=lp.log_c_no WHERE lm_no='{$logistics_result['lm_no']}' AND prd_type='{$prd_type}'";
        $prd_query = mysqli_query($my_db, $prd_sql);
        while($prd = mysqli_fetch_assoc($prd_query))
        {
            $prd_list[] = $prd;
            $row_idx++;
        }
    }

    if($row_idx == 0){
        $row_idx = 1;
    }

    $next_idx       = $idx+$row_idx;
    $total_row_idx  = $next_idx-1;

    $work_sheet->mergeCells("A{$idx}:A{$total_row_idx}");
    $work_sheet->mergeCells("B{$idx}:B{$total_row_idx}");
    $work_sheet->mergeCells("C{$idx}:C{$total_row_idx}");
    $work_sheet->mergeCells("E{$idx}:E{$total_row_idx}");
    $work_sheet->mergeCells("F{$idx}:F{$total_row_idx}");
    $work_sheet->mergeCells("G{$idx}:G{$total_row_idx}");
    $work_sheet->mergeCells("M{$idx}:M{$total_row_idx}");
    $work_sheet->mergeCells("V{$idx}:V{$total_row_idx}");
    $work_sheet->mergeCells("W{$idx}:W{$total_row_idx}");
    $work_sheet->mergeCells("X{$idx}:X{$total_row_idx}");
    $work_sheet->mergeCells("Y{$idx}:Y{$total_row_idx}");

    if($logistics_result['work_state'] == 8){
        $objPHPExcel->getActiveSheet()->getStyle("A{$idx}:Y{$total_row_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF999999');
    }else{
        $objPHPExcel->getActiveSheet()->getStyle("B{$idx}:D{$total_row_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFD7E4BD');
        $objPHPExcel->getActiveSheet()->getStyle("F{$idx}:G{$total_row_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFD7E4BD');
        $objPHPExcel->getActiveSheet()->getStyle("W{$idx}:Y{$total_row_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFD7E4BD');
    }

    $quick_req_memo = ($logistics_result['is_quick_memo']) ? $logistics_result['quick_memo'].$lfcr.$logistics_result['quick_req_memo'] : $logistics_result['quick_req_memo'];
    $work_sheet
        ->setCellValue("A{$idx}", $logistics_result['lm_no'])
        ->setCellValue("B{$idx}", $logistics_result['reg_day'].$lfcr.$logistics_result['reg_time'])
        ->setCellValue("C{$idx}", $logistics_result['work_state_name'])
        ->setCellValue("E{$idx}", $logistics_result['doc_no'])
        ->setCellValue("F{$idx}", $logistics_result['w_no'])
        ->setCellValue("G{$idx}", $logistics_result['req_s_name'].$lfcr."({$logistics_result['req_t_name']})")
        ->setCellValue("M{$idx}", $quick_req_memo)
        ->setCellValue("V{$idx}", $logistics_result['order_number_list'])
        ->setCellValue("W{$idx}", $logistics_result['run_s_name'].$lfcr."({$logistics_result['run_t_name']})")
        ->setCellValue("X{$idx}", $logistics_result['task_run'])
        ->setCellValue("Y{$idx}", $logistics_result['manager_memo'])
    ;

    $prd_idx = $idx;
    foreach($prd_list as $prd_data)
    {
        if($prd_type == "1")
        {
            $prd_row_idx = ($prd_data['unit_cnt'] == 0) ? $prd_idx+$prd_data['unit_cnt'] : $prd_idx+$prd_data['unit_cnt']-1;

            $work_sheet->mergeCells("Q{$prd_idx}:Q{$prd_row_idx}");
            $work_sheet->mergeCells("R{$prd_idx}:R{$prd_row_idx}");
            $work_sheet
                ->setCellValue("Q{$prd_idx}", $prd_data['prd_name'])
                ->setCellValue("R{$prd_idx}", $prd_data['quantity'])
            ;

            foreach($prd_data['unit_list'] as $unit_data)
            {
                $work_sheet
                    ->setCellValue("D{$prd_idx}", $logistics_result['run_date'])
                    ->setCellValue("H{$prd_idx}", $logistics_result['stock_type_name'])
                    ->setCellValue("I{$prd_idx}", $logistics_result['subject_name'])
                    ->setCellValue("J{$prd_idx}", $logistics_result['run_c_name'])
                    ->setCellValue("K{$prd_idx}", $logistics_result['delivery_type_name'])
                    ->setCellValue("L{$prd_idx}", $logistics_result['req_date'])
                    ->setCellValue("N{$prd_idx}", $logistics_result['sender_type_name'])
                    ->setCellValue("O{$prd_idx}", $logistics_result['receiver_type_name'])
                    ->setCellValue("P{$prd_idx}", $logistics_result['reason'])
                    ->setCellValue("S{$prd_idx}", $unit_data['warehouse'])
                    ->setCellValue("T{$prd_idx}", $unit_data['sku'])
                    ->setCellValue("U{$prd_idx}", $unit_data['qty'])
                ;
                $prd_idx++;
            }
        }
        else{
            $work_sheet
                ->setCellValue("D{$prd_idx}", $logistics_result['run_date'])
                ->setCellValue("H{$prd_idx}", $logistics_result['stock_type_name'])
                ->setCellValue("I{$prd_idx}", $logistics_result['subject_name'])
                ->setCellValue("J{$prd_idx}", $logistics_result['run_c_name'])
                ->setCellValue("K{$prd_idx}", $logistics_result['delivery_type_name'])
                ->setCellValue("L{$prd_idx}", $logistics_result['req_date'])
                ->setCellValue("N{$prd_idx}", $logistics_result['sender_type_name'])
                ->setCellValue("O{$prd_idx}", $logistics_result['receiver_type_name'])
                ->setCellValue("P{$prd_idx}", $logistics_result['reason'])
                ->setCellValue("Q{$prd_idx}", "")
                ->setCellValue("R{$prd_idx}", "")
                ->setCellValue("S{$prd_idx}", $prd_data['warehouse'])
                ->setCellValue("T{$prd_idx}", $prd_data['prd_name'])
                ->setCellValue("U{$prd_idx}", $prd_data['quantity'])
            ;
            $prd_idx++;
        }
    }

    $idx = $next_idx;
}
$idx--;


$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:Y1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF343A40');
$objPHPExcel->getActiveSheet()->getStyle("A1:Y1")->getFont()->setColor($fontColor);

$objPHPExcel->getActiveSheet()->getStyle("A1:Y{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:Y1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:Y{$idx}")->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A1:Y1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:Y{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A2:Y{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("M2:M{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("Q2:Q{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("T2:T{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("V2:V{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("X2:X{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("Y2:Y{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("B2:B{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("M2:M{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("Q2:Q{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("T2:T{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("V2:V{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("W2:W{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("X2:X{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("Y2:Y{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);


$excel_filename = "물류관리시스템";
$objPHPExcel->getActiveSheet()->setTitle($excel_filename);
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.date("Ymd")."_{$excel_filename}.xlsx");

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save('php://output');
?>
