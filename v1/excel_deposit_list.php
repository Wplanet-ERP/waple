<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/deposit.php');
require('Classes/PHPExcel.php');

# Create new PHPExcel object & SET document
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("WISE PLANET")
	->setLastModifiedBy("WISE PLANET")
	->setTitle("");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

# 상단타이틀 설정
$header_title_list  = array("dp_no", "입금상태", "입금방식", "입금완료일", "인센티브 정산 년/월", "입금 받은 업체명", "입금업체명", "관련상품", "입금명", "마케팅 담당자", "입금 완료액(VAT/소득세 별도)", "입금 완료액(VAT/소득세 포함)", "계산서 발행상태", "계산서 발행일자", "계산서 발행항목");
$head_idx           = 0;
foreach(range('A', 'O') as $columnID){
    $objPHPExcel->getActiveSheet()->setCellValue("{$columnID}1", $header_title_list[$head_idx]);
    $head_idx++;
}

# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where 			= " dp.display = '1'";
$sch				= isset($_GET['sch']) ? $_GET['sch']:"Y";
$sch_dp_quick 		= isset($_GET['sch_dp_quick']) ? $_GET['sch_dp_quick'] : "";
$sch_dp_state 		= isset($_GET['sch_dp_state']) ? $_GET['sch_dp_state'] : "";
$sch_dp_method 		= isset($_GET['sch_dp_method']) ? $_GET['sch_dp_method'] : "";
$sch_dp_no 			= isset($_GET['sch_dp_no']) ? $_GET['sch_dp_no'] : "";
$sch_s_dp_date 		= isset($_GET['sch_s_dp_date']) ? $_GET['sch_s_dp_date'] : "";
$sch_e_dp_date 		= isset($_GET['sch_e_dp_date']) ? $_GET['sch_e_dp_date'] : "";
$sch_dp_tax_state 	= isset($_GET['sch_dp_tax_state']) ? $_GET['sch_dp_tax_state'] : "";
$sch_team 			= isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$sch_s_no 			= isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$sch_dp_etc 		= isset($_GET['sch_dp_etc']) ? $_GET['sch_dp_etc'] : "";
$sch_my_c_no 		= isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_dp_account		= isset($_GET['sch_dp_account']) ? $_GET['sch_dp_account'] : "";
$sch_prd_no			= isset($_GET['sch_prd_no']) ? $_GET['sch_prd_no'] : "";
$sch_c_name 		= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_c_no 			= isset($_GET['sch_c_no'])?$_GET['sch_c_no']:"";
$sch_d_my_c_no 		= isset($_GET['sch_d_my_c_no']) ? $_GET['sch_d_my_c_no'] : "";
$sch_d_c_name		= isset($_GET['sch_d_c_name']) ? $_GET['sch_d_c_name'] : "";
$sch_d_c_equal		= isset($_GET['sch_d_c_equal']) ? $_GET['sch_d_c_equal'] : "";
$sch_subject 		= isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
$sch_bk_name 		= isset($_GET['sch_bk_name']) ? $_GET['sch_bk_name'] : "";
$sch_mod_type 		= isset($_GET['sch_mod_type']) ? $_GET['sch_mod_type'] : "";

if($sch_dp_quick == '8'){
    $sch_dp_account = '1';
}

if (!empty($sch_dp_no)) {
    $add_where .= " AND dp.dp_no = '{$sch_dp_no}'";
}

if (!empty($sch_dp_state)) {
    $add_where .= " AND dp.dp_state = '{$sch_dp_state}'";
}

if (!empty($sch_dp_method)) {
    $add_where .= " AND dp.dp_method = '{$sch_dp_method}'";
}

if (!empty($sch_s_dp_date)) {
    $add_where .= " AND dp.dp_date >= '{$sch_s_dp_date}'";
}

if (!empty($sch_e_dp_date)) {
    $add_where .= " AND dp.dp_date <= '{$sch_e_dp_date}'";
}

if (!empty($sch_dp_tax_state)) {
    $add_where .= " AND dp.dp_tax_state = '{$sch_dp_tax_state}'";
}

if (!empty($sch_team))
{
    $sch_team_code_where = getTeamWhere($my_db, $sch_team);
    if($sch_team_code_where){
        $add_where .= " AND team IN ({$sch_team_code_where})";
    }
}

if (!empty($sch_s_no)) {
    if($sch_s_no == "알수없음"){
        $add_where .= " AND (s_no = '{$sch_s_no}' or s_no = 0) ";
    }else{
        $add_where .= " AND s_no IN (SELECT sub.s_no FROM staff sub WHERE sub.s_name like '%{$sch_s_no}%')";
    }
}

if (!empty($sch_dp_etc)) {
    $add_where .= " AND dp.c_no in (SELECT c_no FROM company WHERE o_registration = '' OR o_registration IS NULL OR tx_company = '' OR tx_company IS NULL OR tx_company_number <= '' OR tx_company_number IS NULL OR tx_company_ceo <= '' OR tx_company_ceo IS NULL) ";
}

if (!empty($sch_my_c_no)) {
    $add_where .= " AND dp.my_c_no = '{$sch_my_c_no}'";
}

if (!empty($sch_dp_account)) {
    $add_where .= " AND dp.dp_account = '{$sch_dp_account}'";
}

if(!empty($sch_prd_no)) { // 상품
    $add_where .= " AND (SELECT w.prd_no FROM work w WHERE w.w_no=dp.w_no)='{$sch_prd_no}'";
}

if (!empty($sch_c_name)) {
    $add_where .= " AND dp.c_name like '%{$sch_c_name}%'";
}

if($sch_c_no == 'null') { // c_no is null
    $add_where.=" AND dp.c_no ='0'";
}

if (!empty($sch_subject)) {
    $add_where .= " AND dp.dp_subject like '%{$sch_subject}%'";
}

if (!empty($sch_bk_name)) {
    $add_where .= " AND dp.bk_name like '%{$sch_bk_name}%'";
}

if (!empty($sch_mod_type)) {
    $add_where .= " AND dp.mod_type = '{$sch_mod_type}'";
}

if(!empty($sch_d_my_c_no))
{
    $add_where .= " AND (SELECT (SELECT c.my_c_no FROM company c WHERE c.c_no=w.c_no) FROM work w WHERE w.w_no = dp.w_no) = '{$sch_d_my_c_no}'";
}

if (!empty($sch_d_c_name))
{
    if(!empty($sch_d_c_equal)){
        $add_where .= " AND (SELECT w.c_name FROM work w WHERE w.w_no = dp.w_no) = '{$sch_d_c_name}'";
    }else{
        $add_where .= " AND (SELECT w.c_name FROM work w WHERE w.w_no = dp.w_no) like '%{$sch_d_c_name}%'";
    }
}

if ($sch_dp_state == "2")
    $add_orderby = " dp.dp_date desc ";
else {
    if (permissionNameCheck($session_permission, "마케터"))
        $add_orderby = " case when s_no = 0 then 99999999 + dp_no else dp_no end desc ";
    else
        $add_orderby = " dp.dp_no desc ";
}

// 리스트 쿼리
$deposit_sql = "
		SELECT
		*,
		(SELECT s.s_name FROM staff s WHERE s.s_no=dp.s_no) AS s_name,
		(SELECT t.team_name FROM team t WHERE t.team_code=dp.team) AS t_name,
		(SELECT title FROM product prd WHERE prd.prd_no=(SELECT w.prd_no FROM work w WHERE w.w_no = dp.w_no)) AS product_title,
		(SELECT w.prd_no FROM work w WHERE w.w_no = dp.w_no) AS prd_no,
		(SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=dp.my_c_no) AS my_c_name,
		(SELECT mc.kind_color FROM my_company mc WHERE mc.my_c_no=dp.my_c_no) AS my_c_kind_color,
		(SELECT s_name FROM staff s WHERE s.s_no=dp.reg_s_no) AS reg_s_name,
		(SELECT s_name FROM staff s WHERE s.s_no=dp.run_s_no) AS run_s_name,
		(SELECT concat(o_registration, '||', tx_company, '||', tx_company_number, '||', tx_company_ceo) FROM company WHERE c_no = dp.c_no) tx_info
	FROM deposit dp
	WHERE {$add_where}
	ORDER BY {$add_orderby}
    LIMIT 300
";
$deposit_query          = mysqli_query($my_db, $deposit_sql);
$dp_state_option        = getDpStateOption();
$dp_method_option       = getDpMethodOption();
$dp_tax_state_option    = getDpTaxOption();
$idx                    = 2;
while ($deposit_array = mysqli_fetch_array($deposit_query))
{
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue("A{$idx}", $deposit_array['dp_no'])
		->setCellValue("B{$idx}", intval($deposit_array['dp_state']) ? $dp_state_option[$deposit_array['dp_state']] : "")
		->setCellValue("C{$idx}", intval($deposit_array['dp_method']) ? $dp_method_option[$deposit_array['dp_method']] : "")
		->setCellValue("D{$idx}", $deposit_array['dp_date'])
		->setCellValue("E{$idx}", $deposit_array['incentive_date'])
		->setCellValue("F{$idx}", $deposit_array['my_c_name'])
		->setCellValue("G{$idx}", $deposit_array['c_name'])
		->setCellValue("H{$idx}", $deposit_array['product_title'])
		->setCellValue("I{$idx}", $deposit_array['dp_subject'])
		->setCellValue("J{$idx}", $deposit_array['s_name'])
		->setCellValue("K{$idx}", $deposit_array['dp_money'])
		->setCellValue("L{$idx}", $deposit_array['dp_money_vat'])
		->setCellValue("M{$idx}", intval($deposit_array['dp_tax_state']) ? $dp_tax_state_option[$deposit_array['dp_tax_state']] : "")
		->setCellValue("N{$idx}", $deposit_array['dp_tax_date'])
		->setCellValue("O{$idx}", $deposit_array['dp_tax_item'])
    ;

    $idx++;
}
$idx--;

$objPHPExcel->getActiveSheet()->getStyle("A1:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getNumberFormat()->setFormatCode('#,##0');
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getNumberFormat()->setFormatCode('#,##0');

$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("A1:O{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:O1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00F3F3F6');

foreach(range('A', 'O') as $columnID){
	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->setTitle('입금 리스트');
$objPHPExcel->setActiveSheetIndex(0);
$filename = "deposit_list_" . date("Ymd") . ".xls";

header('Content-Type: text/html; charset=UTF-8');
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
