<?php
require('inc/common.php');
require('ckadmin.php');

$dir_location = "./";

# 초기 add_where 설정
$add_where 			= " dp.display = '1'";
$sch_incentive_type = isset($_GET['sch_incentive_type']) ? $_GET['sch_incentive_type'] : "1";
$sch_incentive_mon 	= isset($_GET['sch_incentive_mon']) ? $_GET['sch_incentive_mon'] : date("Y-m");
$sch_incentive_year = isset($_GET['sch_incentive_year']) ? $_GET['sch_incentive_year'] : date("Y");

if ($sch_incentive_type == '2') {
    $sch_incentive_s_date = $sch_incentive_year."-01-01";
    $sch_incentive_e_date = $sch_incentive_year."-12-31";
    $add_where .= " AND dp_date BETWEEN '{$sch_incentive_s_date}' AND '{$sch_incentive_e_date}'";
}else{
	$sch_incentive_s_date = $sch_incentive_mon."-01";
	$end_day          	  = date('t', strtotime($sch_incentive_mon));
	$sch_incentive_e_date = $sch_incentive_mon."-".$end_day;
    $add_where .= " AND dp_date BETWEEN '{$sch_incentive_s_date}' AND '{$sch_incentive_e_date}'";
}

# 미디어 커머스 제외
$add_where .= " AND dp.my_c_no <> '3' ";

# 입금 상태 조건
$add_where .= " AND ((dp.dp_method = '2' AND (dp.dp_state = '2' OR dp.dp_state = '3' OR dp.dp_state = '4')) "; // 현금일 경우 입금완료 or 부분입금
$add_where .= " OR (dp.dp_method = '1' AND (dp.dp_state = '1' OR dp.dp_state = '2' OR dp.dp_state = '4')) "; // 카드일 경우 입금대기 or 입금완료
$add_where .= " OR (dp.dp_method = '3' AND dp.dp_state = '2')) "; // 월정산 경우 입금완료

# 마케터 검색조건
$add_where_permission = str_replace("0", "_", $permission_name['마케터']);

$staff_sql    		= "select s_no, s_name from staff where staff_state != '2' AND permission like '{$add_where_permission}'";
$staff_query 		= mysqli_query($my_db, $staff_sql);
$marketer_deposit 	= [];
while($marketer_array = mysqli_fetch_array($staff_query))
{
	$marketer_add_where = "";
	if(!empty($marketer_array['s_no'])) {
		$marketer_add_where.=" AND dp.s_no='".$marketer_array['s_no']."'";
	}

	if($marketer_array['s_no'] == '22') // 소연님 예외처리
		continue;

	# 마케팅 데이터 처리
	$marketer_deposit_sql = "
		SELECT
			(select s_name from staff where s_no = dp.s_no) as s_name,
		    SUM(dp_money) as sum_dp_money
		FROM deposit dp
		WHERE {$add_where}
			{$marketer_add_where}
  	";

	$marketer_deposit_query = mysqli_query($my_db, $marketer_deposit_sql);
	while ($marketer_deposit_array = mysqli_fetch_array($marketer_deposit_query)) {
		$marketer_deposit[] = array(
			"s_name" 	 	=> $marketer_array['s_name'],
			"sum_dp_money" 	=> $marketer_deposit_array['sum_dp_money'] / 10000 //만원 단위로 절삭
		);
	}

	# 입금액 설정
	foreach ($marketer_deposit as $key => $row) {
		$sum_dp_money[$key]  = $row['sum_dp_money'];
	}
	array_multisort($sum_dp_money, SORT_DESC, $marketer_deposit);
}

$result_graph = [];
$x_name_list  = [];
for($cnt_i = 0 ; $cnt_i < sizeof($marketer_deposit) ; $cnt_i++){
	if($marketer_deposit[$cnt_i]['sum_dp_money'] > 0){
		$result_graph[$cnt_i] = $marketer_deposit[$cnt_i]['sum_dp_money'];
		$x_name_list[$cnt_i] = $marketer_deposit[$cnt_i]['s_name'];
	}
}

$smarty->assign('result_graph', $result_graph);
$smarty->assign('x_name_title', $x_name_list);
$smarty->assign('x_name_list', "'".implode("','", $x_name_list)."'");

$smarty->display('deposit_sum_chart.html');


?>
