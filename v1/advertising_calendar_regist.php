<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/advertising.php');
require('inc/model/Staff.php');
require('inc/model/Advertising.php');

# Model Init
$staff_model        = Staff::Factory();
$advertise_model    = Advertising::Factory();
$application_model  = Advertising::Factory();
$application_model->setMainInit("advertising_application", "aa_no");

# 프로세스 처리
$process     = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'add_advertising')
{
    $sel_day        = isset($_POST['sel_day']) ? $_POST['sel_day'] : "";
    $search_url     = "sel_day={$sel_day}";
    $new_media      = isset($_POST['new_media']) ? $_POST['new_media'] : "";
    $new_product    = isset($_POST['new_product']) ? $_POST['new_product'] : "";
    $new_state      = isset($_POST['new_state']) ? $_POST['new_state'] : "1";
    $new_agency     = isset($_POST['new_agency']) ? $_POST['new_agency'] : "";
    $new_fee_fer    = isset($_POST['new_fee_per']) ? $_POST['new_fee_per'] : "";
    $new_adv_s_day  = isset($_POST['new_adv_s_day']) ? $_POST['new_adv_s_day'] : "";
    $new_adv_s_hour = isset($_POST['new_adv_s_hour']) ? $_POST['new_adv_s_hour'] : "";
    $new_adv_s_min  = isset($_POST['new_adv_s_min']) ? $_POST['new_adv_s_min'] : "";
    $new_adv_e_day  = isset($_POST['new_adv_e_day']) ? $_POST['new_adv_e_day'] : "";
    $new_adv_e_hour = isset($_POST['new_adv_e_hour']) ? $_POST['new_adv_e_hour'] : "";
    $new_adv_e_min  = isset($_POST['new_adv_e_min']) ? $_POST['new_adv_e_min'] : "";
    $new_price      = isset($_POST['new_price']) ? str_replace(",", "", $_POST['new_price']) : "";
    $new_adv_s_date = "{$new_adv_s_day} {$new_adv_s_hour}:{$new_adv_s_min}:00";
    $new_adv_e_date = "{$new_adv_e_day} {$new_adv_e_hour}:{$new_adv_e_min}:00";
    $regdate        = date("Y-m-d H:i:s");

    $insert_data = array(
        "media"         => $new_media,
        "product"       => $new_product,
        "agency"        => $new_agency,
        "fee_per"       => $new_fee_fer,
        "price"         => $new_price,
        "adv_s_date"    => $new_adv_s_date,
        "adv_e_date"    => $new_adv_e_date,
        "reg_s_no"      => $session_s_no,
        "regdate"       => $regdate,
    );

    if($advertise_model->insert($insert_data)) {
        $search_url .= "&am_no=".$advertise_model->getInsertId();
        exit("<script>alert('광고등록에 성공했습니다');location.href='advertising_calendar_regist.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('광고 등록에 실패했습니다');location.href='advertising_calendar_regist.php?{$search_url}';</script>");
    }
}
elseif($process == 'upd_advertising')
{
    $sel_day        = isset($_POST['sel_day']) ? $_POST['sel_day'] : "";
    $chk_am_no      = isset($_POST['am_no']) ? $_POST['am_no'] : "";
    $search_url     = "sel_day={$sel_day}&am_no={$chk_am_no}";
    $new_product    = isset($_POST['new_product']) ? $_POST['new_product'] : "";
    $new_state      = isset($_POST['new_state']) ? $_POST['new_state'] : "1";
    $new_agency     = isset($_POST['new_agency']) ? $_POST['new_agency'] : "";
    $new_fee_fer    = isset($_POST['new_fee_per']) ? $_POST['new_fee_per'] : "";
    $new_adv_s_day  = isset($_POST['new_adv_s_day']) ? $_POST['new_adv_s_day'] : "";
    $new_adv_s_hour = isset($_POST['new_adv_s_hour']) ? $_POST['new_adv_s_hour'] : "";
    $new_adv_s_min  = isset($_POST['new_adv_s_min']) ? $_POST['new_adv_s_min'] : "";
    $new_adv_e_day  = isset($_POST['new_adv_e_day']) ? $_POST['new_adv_e_day'] : "";
    $new_adv_e_hour = isset($_POST['new_adv_e_hour']) ? $_POST['new_adv_e_hour'] : "";
    $new_adv_e_min  = isset($_POST['new_adv_e_min']) ? $_POST['new_adv_e_min'] : "";
    $new_price      = isset($_POST['new_price']) ? str_replace(",", "", $_POST['new_price']) : "";
    $new_adv_s_date = "{$new_adv_s_day} {$new_adv_s_hour}:{$new_adv_s_min}:00";
    $new_adv_e_date = "{$new_adv_e_day} {$new_adv_e_hour}:{$new_adv_e_min}:00";

    $update_data = array(
        "am_no"         => $chk_am_no,
        "product"       => $new_product,
        "agency"        => $new_agency,
        "fee_per"       => $new_fee_fer,
        "price"         => $new_price,
        "adv_s_date"    => $new_adv_s_date,
        "adv_e_date"    => $new_adv_e_date,
    );

    if($advertise_model->update($update_data)) {
        exit("<script>alert('광고 수정에 성공했습니다');location.href='advertising_calendar_regist.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('광고 수정에 실패했습니다');location.href='advertising_calendar_regist.php?{$search_url}';</script>");
    }
}
elseif($process == 'add_application')
{
    $sel_day     = isset($_POST['sel_day']) ? $_POST['sel_day'] : "";
    $chk_am_no   = isset($_POST['am_no']) ? $_POST['am_no'] : "";
    $search_url  = "sel_day={$sel_day}&am_no={$chk_am_no}";

    $insert_data    = array(
        "am_no"     => $chk_am_no,
        "s_no"      => $session_s_no,
        "s_name"    => $session_name,
        "regdate"   => date("Y-m-d H:i:s")
    );

    if($application_model->checkApplication($chk_am_no, $session_s_no)){
        exit("<script>alert('이미 신청한 신청자입니다. 추가에 실패했습니다');location.href='advertising_calendar_regist.php?{$search_url}';</script>");
    }

    if($application_model->insert($insert_data)) {
        exit("<script>alert('신청자 추가에 성공했습니다');parent.location.reload();</script>");
    }else{
        exit("<script>alert('신청자 추가에 실패했습니다');location.href='advertising_calendar_regist.php?{$search_url}';</script>");
    }
}
elseif($process == 'add_custom_application')
{
    $sel_day     = isset($_POST['sel_day']) ? $_POST['sel_day'] : "";
    $chk_am_no   = isset($_POST['am_no']) ? $_POST['am_no'] : "";
    $new_staff   = isset($_POST['new_staff']) ? $_POST['new_staff'] : "";
    $search_url  = "sel_day={$sel_day}&am_no={$chk_am_no}";
    $staff_item  = $staff_model->getStaff($new_staff);

    $insert_data    = array(
        "am_no"     => $chk_am_no,
        "s_no"      => $new_staff,
        "s_name"    => $staff_item['s_name'],
        "regdate"   => date("Y-m-d H:i:s")
    );

    if($application_model->checkApplication($chk_am_no, $new_staff)){
        exit("<script>alert('이미 신청한 신청자입니다. 추가에 실패했습니다');location.href='advertising_calendar_regist.php?{$search_url}';</script>");
    }

    if($application_model->insert($insert_data)) {
        exit("<script>alert('신청자 추가에 성공했습니다');parent.location.reload();</script>");
    }else{
        exit("<script>alert('신청자 추가에 실패했습니다');location.href='advertising_calendar_regist.php?{$search_url}';</script>");
    }
}
elseif($process == 'del_application')
{
    $sel_day     = isset($_POST['sel_day']) ? $_POST['sel_day'] : "";
    $chk_am_no   = isset($_POST['am_no']) ? $_POST['am_no'] : "";
    $chk_aa_no   = isset($_POST['chk_aa_no']) ? $_POST['chk_aa_no'] : "";
    $search_url  = "sel_day={$sel_day}&am_no={$chk_am_no}";

    if($application_model->delete($chk_aa_no)) {
        exit("<script>alert('신청취소에 성공했습니다');location.href='advertising_calendar_regist.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('신청취소에 실패했습니다');location.href='advertising_calendar_regist.php?{$search_url}';</script>");
    }
}

$am_no       = isset($_GET['am_no']) ? $_GET['am_no'] : "";
$sel_day     = isset($_GET['sel_day']) ? $_GET['sel_day'] : "";
$btn_title   = "추가하기";
$advertising = [];

if(strpos($sel_day, "T") !== false){
    $sel_day_tmp = explode("T", $sel_day);
    $sel_day     = date("Y-m-d", strtotime($sel_day_tmp[0]));
}

if(!empty($am_no))
{
    $advertising_sql = "
        SELECT
            *,
            DATE_FORMAT(`am`.adv_s_date, '%Y-%m-%d') as adv_s_day,
            DATE_FORMAT(`am`.adv_s_date, '%H') as adv_s_hour,
            DATE_FORMAT(`am`.adv_s_date, '%i') as adv_s_min,
            DATE_FORMAT(`am`.adv_e_date, '%Y-%m-%d') as adv_e_day,
            DATE_FORMAT(`am`.adv_e_date, '%H') as adv_e_hour,
            DATE_FORMAT(`am`.adv_e_date, '%i') as adv_e_min,
            (SELECT s.s_name FROM staff s WHERE s.s_no=`am`.reg_s_no) AS reg_s_name
        FROM advertising_management `am`
        WHERE am_no = '{$am_no}'
    ";
    $advertising_query = mysqli_query($my_db, $advertising_sql);
    $advertising = mysqli_fetch_assoc($advertising_query);

    $application_list       = [];
    $adv_application_sql    = "SELECT aa_no, s_no, s_name FROM advertising_application WHERE am_no='{$advertising['am_no']}'";
    $adv_application_query  = mysqli_query($my_db, $adv_application_sql);
    while($adv_application = mysqli_fetch_assoc($adv_application_query)){
        if($adv_application['s_no'] == $session_s_no){
            $advertising['in_application'] = true;
        }
        $application_list[] = $adv_application;
    }

    $advertising['application_list'] = $application_list;
}
else
{
    $advertising['adv_s_day']   = $sel_day;
    $advertising['adv_e_day']   = $sel_day;
    $advertising['reg_s_no']    = $session_s_no;
    $advertising['reg_s_name']  = $session_name;
}
$staff_name_list    = $staff_model->getStaffNameList("s_name", "1");

$smarty->assign("am_no", $am_no);
$smarty->assign("sel_day", $sel_day);
$smarty->assign("advertising", $advertising);
$smarty->assign("hour_option", getHourOption());
$smarty->assign("min_option", getMinOption());
$smarty->assign("adv_media_option", getAdvertisingMediaOption());
$smarty->assign("adv_product_option", getAdvertisingProductOption());
$smarty->assign("adv_state_option", getAdvertisingStateOption());
$smarty->assign("adv_type_option", getAdvertisingTypeOption());
$smarty->assign("adv_state_color_option", getAdvertisingStateColorOption());
$smarty->assign("adv_agency_option", getAdvertisingAgencyOption());
$smarty->assign("adv_quick_search_option", getQuickSearchOption());
$smarty->assign("staff_name_list", $staff_name_list);

$smarty->display('advertising_calendar_regist.html');
?>