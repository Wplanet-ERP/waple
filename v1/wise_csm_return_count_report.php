<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/wise_csm.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/WiseCsm.php');

# 프로세스 처리 & Model
$csm_model      = WiseCsm::Factory();
$kind_model     = Kind::Factory();
$cms_code		= "product_cms";

# Navigation & My Quick
$nav_prd_no  = "183";
$nav_title   = "커머스 반품/교환수";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

/** 검색조건 START */
$add_where                  = "1=1 AND return_type IN(1,2)";

# 브랜드 검색
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    
    $add_where .= " AND `cr`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where .= " AND `cr`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where .= " AND `cr`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 날짜별 검색
$today_s_w		        = date('w')-1;
$sch_date_type          = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$sch_s_year             = isset($_GET['sch_s_year']) ? $_GET['sch_s_year'] : date('Y', strtotime("-5 years"));;
$sch_e_year             = isset($_GET['sch_e_year']) ? $_GET['sch_e_year'] : date('Y');
$sch_s_month            = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("-5 months"));
$sch_e_month            = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week             = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week             = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date             = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-8 day"));
$sch_e_date             = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');
$sch_not_empty          = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : "";

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_s_year', $sch_s_year);
$smarty->assign('sch_e_year', $sch_e_year);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);
$smarty->assign('sch_not_empty', $sch_not_empty);

# 전체 기간 조회 및 누적데이터 조회
$all_date_where   = "";
$all_date_key	  = "";
$add_date_column  = "";

if($sch_date_type == '4') //연간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y')";

    $sch_s_datetime  = $sch_s_year."-01-01 00:00:00";
    $sch_e_datetime  = $sch_e_year."-12-31 23:59:59";
    $add_date_column = "DATE_FORMAT(return_date, '%Y')";
    $add_where      .= " AND (`cr`.return_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}')";
}
elseif($sch_date_type == '3') //월간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $sch_s_datetime  = $sch_s_month."-01 00:00:00";
    $sch_e_datetime  = $sch_e_month."-31 23:59:59";
    $add_date_column = "DATE_FORMAT(return_date, '%Y%m')";
    $add_where      .= " AND (`cr`.return_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}')";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $sch_s_datetime  = $sch_s_week." 00:00:00";
    $sch_e_datetime  = $sch_e_week." 23:59:59";
    $add_date_column = "DATE_FORMAT(DATE_SUB(return_date, INTERVAL(IF(DAYOFWEEK(return_date)=1,8,DAYOFWEEK(return_date))-2) DAY), '%Y%m%d')";
    $add_where      .= " AND (`cr`.return_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}')";
}
else //일간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $sch_s_datetime  = $sch_s_date." 00:00:00";
    $sch_e_datetime  = $sch_e_date." 23:59:59";
    $add_date_column = "DATE_FORMAT(return_date, '%Y%m%d')";
    $add_where      .= " AND (`cr`.return_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}')";
}

# 배송리스트 구매처
$csm_title_sql      = "SELECT DISTINCT dp_c_no, (SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=`cr`.dp_c_no) as dp_c_name, (SELECT `c`.priority FROM company `c` WHERE `c`.c_no=`cr`.dp_c_no) as priority FROM csm_return as `cr` WHERE {$add_where} AND dp_c_no > 0 ORDER BY priority ASC, dp_c_name ASC";
$csm_title_query    = mysqli_query($my_db, $csm_title_sql);
$csm_title_list     = [];
while($csm_title = mysqli_fetch_assoc($csm_title_query)) {
    $csm_title_list[$csm_title['dp_c_no']] = array("title" => $csm_title['dp_c_name'], "trade" => 0, "return" => 0);
}
$csm_title_list[0] = array("title" => "구매처(확인불가)", "trade" => 0, "return" => 0);

$search_url = getenv("QUERY_STRING");
$search_url = !empty($search_url) ? $search_url."&sch_date_column_type=return_date" : "sch_date_column_type=return_date";
$smarty->assign("search_url", $search_url);

# 요일별 계산
$csm_date_list          = [];
$csm_return_list        = [];
$csm_return_total_list  = [];

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
if($all_date_where != '')
{
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        foreach($csm_title_list as $csm_key => $csm_data)
        {
            $csm_return_list[$csm_key][$date['chart_key']] = array("trade" => 0, "return" => 0);
        }
        $csm_return_total_list[$date['chart_key']] = array("trade" => 0, "return" => 0);

        if(!isset($csm_date_list[$date['chart_key']]))
        {
            $chart_s_date       = "";
            $chart_e_date       = "";
            $chart_s_date_type  = "";
            $chart_e_date_type  = "";

            if($sch_date_type == '4'){
                $chart_s_date       = date("Y", strtotime("{$date['date_key']}"));
                $chart_e_date       = date("Y", strtotime("{$date['date_key']}"));
                $chart_s_date_type  = "sch_s_year";
                $chart_e_date_type  = "sch_e_year";
            }elseif($sch_date_type == '3'){
                $chart_s_date       = date("Y-m", strtotime("{$date['date_key']}"));
                $chart_e_date       = date("Y-m", strtotime("{$date['date_key']}"));
                $chart_s_date_type  = "sch_s_month";
                $chart_e_date_type  = "sch_e_month";
            }elseif($sch_date_type == '2'){
                $chart_s_date       = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_e_date       = date("Y-m-d", strtotime("{$chart_s_date} +6 days"));
                $chart_s_date_type  = "sch_s_week";
                $chart_e_date_type  = "sch_e_week";
            }elseif($sch_date_type == '1'){
                $chart_s_date       = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_e_date       = date("Y-m-d", strtotime("{$date['date_key']}"));
                $chart_s_date_type  = "sch_s_date";
                $chart_e_date_type  = "sch_e_date";
            }
            $csm_date_list[$date['chart_key']] = array('title' => $chart_title, 's_date_type' => $chart_s_date_type, 's_date' => $chart_s_date, 'e_date' => $chart_e_date, 'e_date_type' => $chart_e_date_type,);
        }
    }
}
$csm_date_list["total"] = array("title" => "합계");


$csm_return_sql = "
    SELECT
        {$add_date_column} as key_date,
        dp_c_no,
        return_type,
        COUNT(DISTINCT order_number) as ord_cnt
    FROM csm_return cr
    WHERE {$add_where}
    GROUP BY key_date, dp_c_no, return_type
";
$csm_return_query = mysqli_query($my_db, $csm_return_sql);
while($csm_return = mysqli_fetch_assoc($csm_return_query))
{
    $return_type = ($csm_return['return_type'] == '1') ? "return" : "trade";

    $csm_return_total_list[$csm_return['key_date']][$return_type] += $csm_return['ord_cnt'];
    $csm_return_total_list['total'][$return_type] += $csm_return['ord_cnt'];

    $csm_return_list[$csm_return['dp_c_no']][$csm_return['key_date']][$return_type] += $csm_return['ord_cnt'];
    $csm_title_list[$csm_return['dp_c_no']][$return_type] += $csm_return['ord_cnt'];
}

# 반품 리스트
$last_return_sql = "
    SELECT
	    upload_kind,
	    DATE_FORMAT(last_regdate, '%m/%d') AS upload_date
    FROM 
    (
        SELECT
           upload_kind,
           MAX(regdate) as last_regdate
        FROM upload_management 
        WHERE upload_type='10'
        GROUP BY upload_kind
        ORDER BY upload_date DESC
    ) AS rs
";
$last_return_query = mysqli_query($my_db, $last_return_sql);
$last_return_list  = [];
while($last_return = mysqli_fetch_assoc($last_return_query)){
    $last_return_list[$last_return['upload_kind']] = $last_return['upload_date'];
}

$smarty->assign("page_type_option", getPageTypeOption('3'));
$smarty->assign("csm_date_list", $csm_date_list);
$smarty->assign("csm_title_list", $csm_title_list);
$smarty->assign("csm_return_total_list", $csm_return_total_list);
$smarty->assign("csm_return_list", $csm_return_list);
$smarty->assign("last_return_list", $last_return_list);

$smarty->display('wise_csm_return_count_report.html');
?>
