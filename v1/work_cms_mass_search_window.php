<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기

$sch_prd_no	        = isset($_GET['prd_no']) ? $_GET['prd_no'] : "";
$sch_order_number   = isset($_GET['order_number']) ? $_GET['order_number'] : "";
$sch_recipient      = isset($_GET['recipient']) ? $_GET['recipient'] : "";
$sch_recipient_hp   = isset($_GET['recipient_hp']) ? $_GET['recipient_hp'] : "";
$sch_zip_code       = isset($_GET['zip_code']) ? $_GET['zip_code'] : "";
$sch_recipient_addr = isset($_GET['recipient_addr']) ? $_GET['recipient_addr'] : "";

# 조건(where)
$add_where = "1=1 AND w.delivery_state='4'";

if(!empty($sch_prd_no))
{
    $add_where .= " AND w.prd_no='{$sch_prd_no}'";
    $smarty->assign("sch_prd_no", $sch_prd_no);
}

if(!empty($sch_order_number))
{
    $add_where .= " AND w.order_number='{$sch_order_number}'";
    $smarty->assign("sch_order_number", $sch_order_number);
}

if(!empty($sch_recipient))
{
    $add_where .= " AND w.recipient = '{$sch_recipient}'";
    $smarty->assign("sch_recipient", $sch_recipient);
}

if(!empty($sch_recipient_hp))
{
    $sch_recipient_hp = str_replace('-','',$sch_recipient_hp);
    $add_where .= " AND REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(w.recipient_hp,'-',''),'.',''),' ',''),'/',''),'+',''),':',''),'?','')='{$sch_recipient_hp}'";
    $smarty->assign("sch_recipient_hp", $sch_recipient_hp);
}

if(!empty($sch_zip_code))
{
    $add_where .= " AND REPLACE(w.zip_code,'-','')='{$sch_zip_code}'";
    $smarty->assign("sch_zip_code", $sch_zip_code);
}

if(!empty($sch_recipient_addr))
{
    $add_where .= " AND w.recipient_addr='{$sch_recipient_addr}'";
    $smarty->assign("sch_recipient_addr", $sch_recipient_addr);
}

$detail_cms_sql = "
    SELECT 
        w.w_no,
        DATE_FORMAT(w.order_date, '%Y-%m-%d') as ord_date,
        DATE_FORMAT(w.order_date, '%H:%i') as ord_time,
        w.order_number,
        w.stock_date,
        w.dp_c_name,
        w.recipient,
        w.recipient_hp,
        w.zip_code,
        w.recipient_addr,
        w.c_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
        (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
        w.quantity,
        w.dp_price_vat
    FROM work_cms w
    WHERE {$add_where}
";

// 전체 게시물 수
$detail_cms_total_sql	= "SELECT count(w_no) as cnt FROM (SELECT w.w_no FROM work_cms w WHERE {$add_where}) as total";
$detail_cms_total_query	= mysqli_query($my_db, $detail_cms_total_sql);
if(!!$detail_cms_total_query)
    $detail_cms_total_result = mysqli_fetch_array($detail_cms_total_query);

$detail_cms_total = $detail_cms_total_result[0];

//페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "100";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($detail_cms_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "work_cms_mass_search_window.php", $pagenum, $search_url);
$smarty->assign("total_num", $detail_cms_total);
$smarty->assign("search_url", $search_url);
$smarty->assign("pagelist", $page);
$smarty->assign("ord_page_type", $page_type);

$detail_cms_sql .= "LIMIT {$offset}, {$num}";

$detail_cms_list  = [];
$detail_cms_query = mysqli_query($my_db, $detail_cms_sql);

while($detail_cms = mysqli_fetch_assoc($detail_cms_query))
{
    $detail_cms_list[] = $detail_cms;
}

$smarty->assign("page_type_option", getPageTypeOption(3));
$smarty->assign("detail_cms_list", $detail_cms_list);

$smarty->display('work_cms_mass_search_window.html');

?>
