<?php
ini_set('display_errors', -1);

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/model/Kind.php');

$kind_model = Kind::Factory();
$s_date     = isset($_GET['s_date']) ? $_GET['s_date'] : "";
$e_date     = isset($_GET['e_date']) ? $_GET['e_date'] : "";
$csm_g1     = isset($_GET['csm_g1']) ? $_GET['csm_g1'] : "";
$csm_g2     = isset($_GET['csm_g2']) ? $_GET['csm_g2'] : "";

if(empty($s_date) || empty($e_date) || empty($csm_g1) || empty($csm_g2)){
    echo "필요한 데이터가 없습니다.";
    exit;
}

$return_sql = "
    SELECT 
       ord_no, 
       file1_origin, 
       file1_read, 
       file2_origin, 
       file2_read, 
       file3_origin, 
       file3_read 
    FROM work_cms_comment 
    WHERE ord_no IN(SELECT DISTINCT parent_order_number FROM `work_cms_return` as r WHERE (r.return_req_date BETWEEN '{$s_date}' AND '{$e_date}') AND r.bad_reason IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$csm_g1}' AND k.display='1') AND r.bad_reason = '{$csm_g2}') 
    AND (
            (file1_origin IS NOT NULL AND file1_read != '') OR
            (file2_origin IS NOT NULL AND file2_read != '') OR
            (file3_origin IS NOT NULL AND file3_read != '')
        )
";
$return_query       = mysqli_query($my_db, $return_sql);
$order_list         = [];
while($return_result = mysqli_fetch_assoc($return_query))
{
    if(!empty($return_result['file1_read'])){
        $order_list[$return_result['ord_no']][] = $return_result['file1_read'];
    }

    if(!empty($return_result['file2_read'])){
        $order_list[$return_result['ord_no']][] = $return_result['file2_read'];
    }

    if(!empty($return_result['file3_read'])){
        $order_list[$return_result['ord_no']][] = $return_result['file3_read'];
    }
}

$zip_file_name  = "";
$zip_file_path  = "";
$bad_file_list  = [];
if(!empty($order_list))
{
    $csm_g1_name    = $kind_model->getKindName($csm_g1);
    $csm_g2_name    = str_replace("/","", $kind_model->getKindName($csm_g2));
    $zip_file_name  = "(".date("Ymd", strtotime($s_date))."~".date("Ymd", strtotime($e_date)).")_(".$csm_g1_name."_".$csm_g2_name.").zip";
    $zip_file_path   = "zip_tmp/{$zip_file_name}";
    $zip_file_origin = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$zip_file_path}";

    if(file_exists($zip_file_origin)){
        del_file($zip_file_path);
    }

    $zip = new ZipArchive;
    $res = $zip->open($zip_file_origin, ZipArchive::CREATE);

    foreach($order_list as $ord_no => $file_data)
    {
        $file_idx = 1;
        foreach($file_data as $idx => $file_path)
        {
            $file_origin    = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$file_path}";
            $file_ext       = "";
            $tmp_filename   = explode(".", $file_path);
            $file_ext       = end($tmp_filename);
            $file_size      = 0;
            $file_name      = "{$ord_no}_{$file_idx}.{$file_ext}";
            if(file_exists($file_origin)){
                $file_size = filesize($file_origin)/1024;
                $file_size = floor($file_size);

                if ($res === TRUE) {
                    $zip->addFile($file_origin, $file_name);
                }

            }else{
                continue;
            }

            $bad_file_list[] = array(
                "file_path" => $file_path,
                "file_name" => $file_name,
                "file_size" => $file_size." KB"
            );

            $file_idx++;
        }
    }

    if ($res === TRUE) {
        $zip->close();
    }

}else{
    $file_names = [];
    $file_paths = [];
}

$total_count = empty($bad_file_list) ? 0 : count($bad_file_list);

$smarty->assign("file_count", $total_count);
$smarty->assign("bad_file_list", $bad_file_list);
$smarty->assign("zip_file_name", $zip_file_name);
$smarty->assign("zip_file_path", $zip_file_path);

$smarty->display('wise_csm_bad_report.file_download.html');
?>
