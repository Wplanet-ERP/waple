<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_return.php');

$process = (isset($_POST['process']))?$_POST['process']:"";

/////////////////////////* 자동 저장 처리 부분 *//////////////////////
if($process == "return_state")
{
    $r_no 		= (isset($_POST['r_no'])) ? $_POST['r_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return_temporary SET `return_state` = '{$value}' WHERE r_no = '{$r_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "회수 진행상태 변경에 실패 하였습니다.";
    else
        echo "회수 진행상태가 변경 되었습니다.";
    exit;
}
elseif($process == "return_purpose")
{
    $r_no 		= (isset($_POST['r_no'])) ? $_POST['r_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return_temporary SET `return_purpose` = '{$value}' WHERE r_no = '{$r_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "회수목적 변경에 실패 하였습니다.";
    else
        echo "회수목적 값이 변경 되었습니다.";
    exit;
}
elseif($process == "return_reason")
{
    $r_no 		= (isset($_POST['r_no'])) ? $_POST['r_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return_temporary SET `return_reason` = '{$value}' WHERE r_no = '{$r_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "사유구분 변경에 실패 하였습니다.";
    else
        echo "사유구분 값이 변경 되었습니다.";
    exit;
}
elseif($process == "return_type")
{
    $r_no 		= (isset($_POST['r_no'])) ? $_POST['r_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return_temporary SET `return_type` = '{$value}' WHERE r_no = '{$r_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "구분 변경에 실패 하였습니다.";
    else
        echo "구분 값이 변경 되었습니다.";
    exit;
}
elseif($process == "cus_memo"){
    $r_no 		= (isset($_POST['r_no'])) ? $_POST['r_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return_temporary SET `cus_memo` = '" . addslashes($value) . "' WHERE r_no = '{$r_no}'";

    if (!mysqli_query($my_db, $sql))
        echo "고객메시지 저장에 실패 하였습니다.";
    else
        echo "고객메시지가 저장 되었습니다.";
    exit;

}
elseif($process == "req_memo")
{
    $r_no 		= (isset($_POST['r_no'])) ? $_POST['r_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "UPDATE work_cms_return_temporary SET req_memo = '" . addslashes($value) . "' WHERE r_no = '{$r_no }'";

    if (!mysqli_query($my_db, $sql))
        echo "와이즈 메시지 저장에 실패 하였습니다.";
    else
        echo "와이즈 메시지가 저장 되었습니다.";
    exit;
}
elseif($process == "del_return")
{
    $r_no 		= (isset($_POST['r_no'])) ? $_POST['r_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    $sql = "DELETE FROM work_cms_return_temporary WHERE r_no = '{$r_no}'";

    if (!mysqli_query($my_db, $sql))
        exit("<script>alert('삭제에 실패했습니다.');location.href='work_cms_return_temporary.php';</script>");
    else
        exit("<script>alert('삭제했습니다');location.href='work_cms_return_temporary.php';</script>");
    exit;

}
elseif($process == "all_delete")
{
    $del_sql = "DELETE FROM work_cms_return_temporary";

    if(!mysqli_query($my_db, $del_sql)){
        exit("<script>alert('삭제에 실패했습니다');location.href='work_cms_return_temporary.php';</script>");
    }else{
        exit("<script>alert('모두 삭제했습니다');location.href='work_cms_return_temporary.php';</script>");
    }
}
elseif($process == "return_all_delete")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $del_sql    = "DELETE FROM work_cms_return_temporary";

    if(!mysqli_query($my_db, $del_sql)){
        exit("<script>alert('삭제에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('모두 삭제했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
    }
}
elseif($process == "return_apply")
{
    ## 발송상품으로 변경
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $with_log_c_no  = "2809";

    $return_temporary_sql = "SELECT *, (SELECT `c`.c_name FROM company `c` WHERE `c`.c_no = wcrd.dp_c_no LIMIT 1) as dp_c_name FROM work_cms_return_temporary wcrd";
    $return_temporary_query = mysqli_query($my_db, $return_temporary_sql);
    $return_temporary_list = [];
    while($return_temporary = mysqli_fetch_assoc($return_temporary_query))
    {
        $return_temporary_list[$return_temporary['parent_order_number']][] = $return_temporary;
    }

    if(!empty($return_temporary_list))
    {
        $r_comma    = "";
        $u_comma    = "";
        $d_comma    = "";
        $rma_date   = date('Ymd');
        $last_rma_ord_no_sql    = "SELECT REPLACE(order_number, '{$rma_date}_RMA_', '') as ord_no FROM work_cms_return WHERE order_number LIKE '%{$rma_date}_RMA_%' ORDER BY order_number DESC LIMIT 1";
        $last_rma_ord_no_query  = mysqli_query($my_db, $last_rma_ord_no_sql);
        $last_rma_ord_no_result = mysqli_fetch_assoc($last_rma_ord_no_query);

        $last_rma_ord_no = (isset($last_rma_ord_no_result['ord_no']) && !empty($last_rma_ord_no_result['ord_no'])) ? (int)$last_rma_ord_no_result['ord_no']+1 : 1;

        $ins_return_sql = "INSERT INTO `work_cms_return`(`order_number`,`return_state`,`c_no`,`c_name`,`s_no`,`team`,`dp_c_no`,`req_s_no`,`req_team`,`req_memo`,`prd_no`,`quantity`,`parent_order_number`,`parent_shop_ord_no`,`recipient`,`recipient_addr`,`recipient_hp`,`zip_code`,`return_purpose`,`return_reason`,`return_type`,`cus_memo`,`regdate`,`return_cus_date`,`return_req_date`) VALUES";
        $ins_return_unit_sql   = "INSERT INTO `work_cms_return_unit`(`order_number`,`option`,`sku`,`quantity`,`regdate`) VALUES";
        $ins_deli_sql   = "INSERT INTO `work_cms`(`order_number`,`order_date`,`delivery_state`,`c_no`,`c_name`,`s_no`,`team`,`dp_c_no`, dp_c_name,`task_req_s_no`,`task_req_team`,`task_run_s_no`,`task_run_regdate`,`task_run_team`,`prd_no`,`quantity`,`parent_order_number`,`shop_ord_no`,`parent_shop_ord_no`,`recipient`,`recipient_addr`,`recipient_hp`,`zip_code`,`delivery_memo`,`stock_date`,`regdate`) VALUES";
        
        foreach($return_temporary_list as $return_list)
        {
            $ord_no   = $rma_date."_RMA_".sprintf('%04d', $last_rma_ord_no);
            foreach($return_list as $return)
            {
                $return_addr     = addslashes($return['recipient_addr1']." ".$return['recipient_addr2']);
                $ins_return_sql .= $r_comma."('{$ord_no}','{$return['return_state']}','{$return['c_no']}','{$return['c_name']}','{$return['s_no']}','{$return['team']}','{$return['dp_c_no']}', '{$return['req_s_no']}','{$return['req_team']}','{$return['req_memo']}','{$return['prd_no']}','{$return['quantity']}','{$return['parent_order_number']}','{$return['parent_shop_ord_no']}','{$return['recipient']}','{$return_addr}','{$return['recipient_hp']}','{$return['zip_code']}','{$return['return_purpose']}','{$return['return_reason']}','{$return['return_type']}','{$return['cus_memo']}','{$return['regdate']}','{$return['return_cus_date']}','{$return['return_req_date']}')";
                $r_comma = ",";

                $unit_sql    = "SELECT pcr.option_no, pcr.quantity, (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='{$with_log_c_no}') as sku FROM product_cms_relation pcr WHERE pcr.prd_no='{$return['prd_no']}' AND pcr.display='1'";
                $unit_query  = mysqli_query($my_db, $unit_sql);
                while($unit_result = mysqli_fetch_assoc($unit_query))
                {
                    $pcu_quantity    = $return['quantity']*$unit_result['quantity'];
                    $ins_return_unit_sql .= $u_comma."('{$ord_no}','{$unit_result['option_no']}','{$unit_result['sku']}','{$pcu_quantity}','{$return['regdate']}')";
                    $u_comma = ",";
                }

                if($return['return_purpose'] == '1')
                {
                    if(!empty($return['send_prd_list']) && !empty($return['send_prd_qty']))
                    {
                        $send_prd_list = explode(",", $return['send_prd_list']);
                        $send_prd_qty  = explode(",", $return['send_prd_qty']);

                        $send_prd_idx = 0;
                        foreach($send_prd_list as $send_prd)
                        {
                            $send_qty  = $send_prd_qty[$send_prd_idx];
                            $deli_addr = $return['deli_recipient_addr1']." ".$return['deli_recipient_addr2'];
                            $deli_shop_ord_no = "W".$ord_no."_".$send_prd;
                            $ins_deli_sql    .= $d_comma."('{$ord_no}','{$return['regdate']}','10','{$return['c_no']}','{$return['c_name']}','{$return['s_no']}','{$return['team']}','2008','교환','{$return['req_s_no']}','{$session_s_no}','{$session_team}',now(),'{$session_team}','{$send_prd}','{$send_qty}','{$return['parent_order_number']}','{$deli_shop_ord_no}','{$return['parent_shop_ord_no']}','{$return['deli_recipient']}','{$deli_addr}','{$return['deli_recipient_hp']}','{$return['deli_zip_code']}','{$return['deli_memo']}', now(), now())";
                            $d_comma = ",";

                            $send_prd_idx++;
                        }
                    }
                }
            }
            $last_rma_ord_no++;
        }

        if(!mysqli_query($my_db, $ins_return_sql) && !mysqli_query($my_db, $ins_deli_sql)){
            exit("<script>alert('등록에 실패했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
        }else{
            $del_sql = "DELETE FROM work_cms_return_temporary";
            mysqli_query($my_db, $del_sql);
            mysqli_query($my_db, $ins_return_unit_sql);
            exit("<script>alert('등록에 성공했습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
        }
    }

    exit("<script>alert('데이터가 없습니다');location.href='work_cms_return_list.php?{$search_url}';</script>");
}
elseif($process == "save_send_product")
{
    $r_no_val    = isset($_POST['r_no']) ? $_POST['r_no'] : "";
    $send_prd_no = isset($_POST['prd_no']) ? $_POST['prd_no'] : "";
    $send_qty    = isset($_POST['qty']) ? $_POST['qty'] : "";

    $upd_sql = "UPDATE work_cms_return_temporary SET send_prd_list='{$send_prd_no}', send_prd_qty='{$send_qty}' WHERE r_no='{$r_no_val}'";
    mysqli_query($my_db, $upd_sql);

    exit;

}
elseif($process == "modify_total_address")
{
    ## 발송상품으로 변경
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $r_no           = isset($_POST['r_no']) ? $_POST['r_no'] : "";
    $return_purpose = isset($_POST['return_purpose']) ? $_POST['return_purpose'] : "";

    if(!empty($r_no))
    {
        $recipient       = isset($_POST['recipient']) ? $_POST['recipient'] : "";
        $recipient_hp    = isset($_POST['recipient_hp']) ? $_POST['recipient_hp'] : "";
        $zipcode         = isset($_POST['zipcode']) ? $_POST['zipcode'] : "";
        $recipient_addr1 = isset($_POST['recipient_addr1']) ? addslashes($_POST['recipient_addr1']) : "";
        $recipient_addr2 = isset($_POST['recipient_addr2']) ? addslashes($_POST['recipient_addr2']) : "";

        $deli_column = "";

        if($return_purpose == '1')
        {
            $deli_recipient         = isset($_POST['deli_recipient']) ? $_POST['deli_recipient'] : "";
            $deli_recipient_hp      = isset($_POST['deli_recipient_hp']) ? $_POST['deli_recipient_hp'] : "";
            $deli_zipcode           = isset($_POST['deli_zipcode']) ? $_POST['deli_zipcode'] : "";
            $deli_recipient_addr1   = isset($_POST['deli_recipient_addr1']) ? addslashes($_POST['deli_recipient_addr1']) : "";
            $deli_recipient_addr2   = isset($_POST['deli_recipient_addr2']) ? addslashes($_POST['deli_recipient_addr2']) : "";
            $deli_memo              = isset($_POST['deli_memo']) ? addslashes($_POST['deli_memo']) : "";

            $deli_column = ",deli_recipient='{$deli_recipient}', deli_recipient_hp='{$deli_recipient_hp}', deli_zip_code='{$deli_zipcode}', deli_recipient_addr1='{$deli_recipient_addr1}', deli_recipient_addr2='{$deli_recipient_addr2}', deli_memo='{$deli_memo}'";
        }

        $upd_sql = "
            UPDATE work_cms_return_temporary SET
                recipient       = '{$recipient}',
                recipient_hp    = '{$recipient_hp}',
                zip_code        = '{$zipcode}',
                recipient_addr1 = '{$recipient_addr1}',
                recipient_addr2 = '{$recipient_addr2}'
                {$deli_column}
            WHERE r_no='{$r_no}'
        ";

        if(!mysqli_query($my_db, $upd_sql)){
            exit("<script>alert('저장에 실패했습니다');location.href='work_cms_return_temporary.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('저장에 성공했습니다');location.href='work_cms_return_temporary.php?{$search_url}';</script>");
        }

    }else {
        exit("<script>alert('저장에 실패했습니다. r_no 값이 없습니다');location.href='work_cms_return_temporary.php?{$search_url}';</script>");
    }
}
else
{
    $return_req_s_date = date("Y-m-d", strtotime("-7 days"));
    $return_req_e_date = date("Y-m-d");

    //검색 처리
    $add_where="1=1";
    $sch_return_purpose = isset($_GET['sch_return_purpose']) ? $_GET['sch_return_purpose'] : "";
    $sch_duplicate      = isset($_GET['sch_duplicate']) ? $_GET['sch_duplicate'] : "";

    if(!empty($sch_return_purpose)){
        $add_where .= " AND wcrd.return_purpose = '{$sch_return_purpose}'";
        $smarty->assign('sch_return_purpose', $sch_return_purpose);
    }

    if(!empty($sch_duplicate)){
        $add_where .= " AND (SELECT count(wcr.r_no) FROM work_cms_return wcr WHERE wcr.parent_order_number=wcrd.parent_order_number AND (wcr.return_req_date BETWEEN '{$return_req_s_date}' AND '{$return_req_e_date}')) > 0";
    }

	// 리스트 쿼리
	$return_sql = "
		SELECT
		    *,
			DATE_FORMAT(wcrd.regdate, '%Y-%m-%d') as reg_date,
		 	DATE_FORMAT(wcrd.return_cus_date, '%Y-%m-%d %H:%i') as cus_date,
			wcrd.zip_code,
			(SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=wcrd.dp_c_no) as dp_c_name,
			(SELECT s.s_name FROM staff s WHERE s.s_no=wcrd.req_s_no) as req_s_name,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wcrd.prd_no))) AS k_prd1_name,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=wcrd.prd_no)) AS k_prd2_name,
			(SELECT title from product_cms prd_cms where prd_cms.prd_no=wcrd.prd_no) as prd_name,
			(SELECT count(wcr.r_no) FROM work_cms_return wcr WHERE wcr.parent_order_number=wcrd.parent_order_number AND (wcr.return_req_date BETWEEN '{$return_req_s_date}' AND '{$return_req_e_date}')) as req_cnt
		FROM
			work_cms_return_temporary wcrd
		WHERE
			{$add_where}
		ORDER BY wcrd.r_no DESC
	";

    $return_total_sql		= "SELECT count(*) FROM (SELECT wcrd.r_no FROM work_cms_return_temporary wcrd WHERE {$add_where}) AS cnt";
    $return_total_query	= mysqli_query($my_db, $return_total_sql);
    if(!!$return_total_query)
        $return_total_result = mysqli_fetch_array($return_total_query);

    $return_total = isset($return_total_result[0]) ? $return_total_result[0] : 0;

    //페이징
    $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= 10;
    $offset 	= ($pages-1) * $num;
    $pagenum 	= ceil($return_total/$num);

    if ($pages >= $pagenum){$pages = $pagenum;}
    if ($pages <= 0){$pages = 1;}

    $search_url = getenv("QUERY_STRING");
    $page		= pagelist($pages, "work_cms_return_temporary.php", $pagenum, $search_url);
    $smarty->assign("add_where", $add_where);
    $smarty->assign("total_num", $return_total);
    $smarty->assign("pagelist", $page);


    // 리스트 쿼리 결과(데이터)
    $return_sql .= "LIMIT $offset,$num";

    $return_list  = [];
    $result		= mysqli_query($my_db, $return_sql);
    $total_req_cnt = 0;
    if(!!$result)
    {
        while($return_data = mysqli_fetch_array($result))
        {
            $return_send_prd_data_list  = [];
            $return_send_prd_no_list    = isset($return_data['send_prd_list']) && !empty($return_data['send_prd_list']) ? explode(',', $return_data['send_prd_list']) : [];
            $return_send_prd_qty_list   = isset($return_data['send_prd_qty']) && !empty($return_data['send_prd_qty']) ? explode(',', $return_data['send_prd_qty']) : [];


            if(!empty($return_send_prd_no_list) && !empty($return_send_prd_qty_list))
            {
                $send_prd_idx = 0;
                $prd_key      = 1;
                foreach($return_send_prd_no_list as $return_send_prd_no)
                {
                    $send_prd_name_sql    = "SELECT prd.title FROM product_cms prd WHERE prd.prd_no='{$return_send_prd_no}'";
                    $send_prd_name_query  = mysqli_query($my_db, $send_prd_name_sql);
                    $send_prd_name_result = mysqli_fetch_assoc($send_prd_name_query);

                    $return_send_prd_data_list[] = array(
                        "prd_no"    => $return_send_prd_no,
                        "prd_name"  => isset($send_prd_name_result['title']) ? $send_prd_name_result['title'] : "",
                        "prd_key"   => $return_data['r_no']."_".$prd_key,
                        "qty"       => $return_send_prd_qty_list[$send_prd_idx],
                    );

                    $prd_key++;
                    $send_prd_idx++;
                }
            }

            if($return_data['req_cnt'] > 0){
                $total_req_cnt++;
            }

            $return_data['send_prd_data_list'] = $return_send_prd_data_list;
            $return_data['send_prd_cnt'] = !empty($return_send_prd_data_list) ? count($return_send_prd_data_list) : 1;

            # 주소지 동일처리
            if($return_data['return_purpose'] == '1')
            {
                $return_data['deli_recipient']       = !empty($return_data['deli_recipient']) ? $return_data['deli_recipient'] : $return_data['recipient'];
                $return_data['deli_recipient_hp']    = !empty($return_data['deli_recipient_hp']) ? $return_data['deli_recipient_hp'] : $return_data['recipient_hp'];
                $return_data['deli_zip_code']        = !empty($return_data['deli_zip_code']) ? $return_data['deli_zip_code'] : $return_data['zip_code'];
                $return_data['deli_recipient_addr1'] = !empty($return_data['deli_recipient_addr1']) ? $return_data['deli_recipient_addr1'] : $return_data['recipient_addr1'];
                $return_data['deli_recipient_addr2'] = !empty($return_data['deli_recipient_addr2']) ? $return_data['deli_recipient_addr2'] : $return_data['recipient_addr2'];
                $return_data['deli_zip_code'] = !empty($return_data['deli_zip_code']) ? $return_data['deli_zip_code'] : $return_data['zip_code'];
            }

            $return_list[] = $return_data;
        }
    }

    $smarty->assign("return_state_option", getReturnStateOption());
    $smarty->assign("return_purpose_option", getReturnPurposeOption());
    $smarty->assign("return_reason_option", getReturnReasonOption());
    $smarty->assign("return_type_option", getReturnTypeOption());
    $smarty->assign('total_req_cnt', $total_req_cnt);
    $smarty->assign('return_list', $return_list);

    $smarty->display('work_cms_return_temporary.html');
}
?>
