<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');

/* 게시판 리스트[table_name, b_id, board_name]  START */
$board_manager_sql = "
        SELECT
            brd_mng.table_name,
            brd_mng.b_id,
            brd_mng.board_name,
            brd_mng.contents_format,
            brd_mng.comment_format
        FROM board_manager brd_mng
        WHERE brd_mng.display='1'
        ORDER BY brd_mng.priority ASC
    ";
$board_manager_query = mysqli_query($my_db,$board_manager_sql);

while($board_manager_data = mysqli_fetch_array($board_manager_query)){
    $board_manager_list[]=array(
        "table_name"=>$board_manager_data['table_name'],
        "b_id"=>$board_manager_data['b_id'],
        "board_name"=>$board_manager_data['board_name'],
        "contents_format"=>$board_manager_data['contents_format'],
        "comment_format"=>$board_manager_data['comment_format']
    );
}

$reg_year_list = array('2019' => '2019년', '2020' => '2020년');

$smarty->assign("board_manager_list", $board_manager_list);
$smarty->assign("reg_year_list", $reg_year_list);

$sch_corp_code  = isset($_GET['sch_corp_code']) ? $_GET['sch_corp_code'] : "";
$sch_corp_name  = isset($_GET['sch_corp_name']) ? $_GET['sch_corp_name'] : "";
$sch_stock_code = isset($_GET['sch_stock_code']) ? $_GET['sch_stock_code'] : "";
$sch_reg_year   = isset($_GET['sch_reg_year']) ? $_GET['sch_reg_year'] : "";

$add_where = "1=1";
if(!empty($sch_corp_code))
{
    $add_where .= " AND `corp`.corp_code = '{$sch_corp_code}'";
    $smarty->assign('sch_corp_code', $sch_corp_code);
}

if(!empty($sch_corp_name))
{
    $add_where .= " AND `corp`.corp_name like '%{$sch_corp_name}%'";
    $smarty->assign('sch_corp_name', $sch_corp_name);
}

if(!empty($sch_stock_code))
{
    $add_where .= " AND `corp`.stock_code = '{$sch_stock_code}'";
    $smarty->assign('sch_stock_code', $sch_stock_code);
}

if(!empty($sch_reg_year))
{
    $add_where .= " AND `corp`.reg_year = '{$sch_reg_year}'";
    $smarty->assign('sch_reg_year', $sch_reg_year);
}

#전체 게시물 수
$corp_total_sql = "SELECT count(corp_no) FROM (SELECT `corp`.corp_no FROM corp_info corp WHERE {$add_where}) AS cnt";
$corp_total_query	= mysqli_query($my_db, $corp_total_sql);
if(!!$corp_total_query)
    $corp_total_result = mysqli_fetch_array($corp_total_query);

$corp_total = $corp_total_result[0];

#페이징
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 20;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($corp_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "corp_info_list.php", $pagenum, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $corp_total);
$smarty->assign("pagelist", $page);

# 리스트 쿼리
$corp_info_sql   = "SELECT * FROM corp_info corp WHERE {$add_where} LIMIT {$offset}, {$num}";
$corp_info_query = mysqli_query($my_db, $corp_info_sql);
$corp_info_list  = [];
while($corp_info = mysqli_fetch_assoc($corp_info_query))
{
    $corp_info_list[] = $corp_info;
}

$smarty->assign("corp_info_list", $corp_info_list);
$smarty->display('corp_info_list.html');

?>
