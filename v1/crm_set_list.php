<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/_message.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/Company.php');

# Process
$process = isset($_POST['process']) ? $_POST['process'] : "";
$my_db->set_charset("utf8mb4");

if($process == 'new_crm_set')
{
    $search_url_val   = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    $new_crm_state    = (isset($_POST['new_crm_state'])) ? $_POST['new_crm_state'] : "";
    $new_title        = (isset($_POST['new_title'])) ? $_POST['new_title'] : "";
    $new_s_no         = (isset($_POST['new_s_no'])) ? $_POST['new_s_no'] : "";
    $new_team         = (isset($_POST['new_team'])) ? $_POST['new_team'] : "";
    $new_income_type  = (isset($_POST['new_income_type'])) ? $_POST['new_income_type'] : "";
    $new_send_type    = (isset($_POST['new_send_type'])) ? $_POST['new_send_type'] : "";
    $new_send_time    = (isset($_POST['new_send_time'])) ? $_POST['new_send_time'] : "";
    $new_send_method  = (isset($_POST['new_send_method'])) ? $_POST['new_send_method'] : "";
    $new_temp_no      = (isset($_POST['new_temp_no'])) ? $_POST['new_temp_no'] : "";
    $new_temp_type    = (isset($_POST['new_temp_type'])) ? $_POST['new_temp_type'] : "";
    $new_dp_company   = (isset($_POST['new_dp_company'])) ? $_POST['new_dp_company'] : "";
    $new_income_value = "";
    $new_send_value   = "";
    $new_send_e_date  = "";

    if(empty($new_crm_state) || empty($new_title) || empty($new_s_no) || empty($new_team) || empty($new_income_type) || empty($new_send_type) || empty($new_send_method) || empty($new_temp_no))
    {
        exit ("<script>alert('데이터를 다시 입력해 주세요.'); location.href='crm_set_list.php?$search_url_val';</script>");
    }

    if($new_income_type == '1'){
        $new_income_value = (isset($_POST['new_prd_no'])) ? implode(",", $_POST['new_prd_no']) : "";
    }elseif($new_income_type == '2'){
        $new_income_value = (isset($_POST['new_unit'])) ? implode(",", $_POST['new_unit']) : "";
    }

    if($new_send_type == '1'){
        $new_send_value = (isset($_POST['new_send_value_one'])) ? $_POST['new_send_value_one'] : "";
    }elseif($new_send_type == '2'){
        $new_send_value  = (isset($_POST['new_send_value_repeat'])) ? $_POST['new_send_value_repeat'] : "";
        $new_send_e_date = (isset($_POST['new_send_e_date'])) ? $_POST['new_send_e_date'] : "";
    }

    if(($new_income_type != '3' && empty($new_income_value)) || ($new_send_method == '1' && empty($new_send_value)))
    {
        exit ("<script>alert('데이터를 다시 입력해 주세요.'); location.href='crm_set_list.php?$search_url_val';</script>");
    }

    $ins_sql = "
          INSERT INTO `crm_set` SET 
              crm_state = '{$new_crm_state}', 
              s_no      = '{$new_s_no}', 
              team      = '{$new_team}', 
              title     = '{$new_title}', 
              t_no      = '{$new_temp_no}', 
              temp_type = '{$new_temp_type}', 
              income_type  = '{$new_income_type}', 
              income_value = '{$new_income_value}',
              send_type   = '{$new_send_type}', 
              send_time   = '{$new_send_time}', 
              send_value  = '{$new_send_value}',      
              send_method = '{$new_send_method}',      
              regdate     = now()
    ";

    if(!empty($new_dp_company)){
        $new_dp_company_list = array_filter($new_dp_company);
        $new_dp_company_text = implode(",", $new_dp_company_list);
        $ins_sql .= ", dp_company='{$new_dp_company_text}'";
    }else{
        $ins_sql .= ", dp_company=NULL";
    }

    if(!empty($new_send_e_date)){
        $ins_sql .= ", send_e_date = '{$new_send_e_date}'";
    }

    $new_exp_hour = (isset($_POST['new_exp_hour'])) ? $_POST['new_exp_hour'] : "";
    $new_exp_min  = (isset($_POST['new_exp_min'])) ? $_POST['new_exp_min'] : "";
    if(!empty($new_exp_hour)){
        $ins_sql .= ", exp_hour = '{$new_exp_hour}'";
    }

    if(!empty($new_exp_min)){
        $ins_sql .= ", exp_min = '{$new_exp_min}'";
    }

    if($new_send_method == '2'){
        $new_send_date = (isset($_POST['new_send_date'])) ? $_POST['new_send_date'] : "";
        $new_stock_s_date = (isset($_POST['new_stock_s_date'])) ? $_POST['new_stock_s_date'] : "";
        $new_stock_e_date = (isset($_POST['new_stock_e_date'])) ? $_POST['new_stock_e_date'] : "";
        $ins_sql .= ", send_date='{$new_send_date}', stock_s_date = '{$new_stock_s_date}' , stock_e_date = '{$new_stock_e_date}'";
    }

    if (!mysqli_query($my_db, $ins_sql)){
        echo ("<script>alert('알림톡 설정 저장에 실패하였습니다');</script>");
    }else{
        echo ("<script>alert('알림톡 설정 저장되었습니다');</script>");
    }

    exit ("<script>location.href='crm_set_list.php?$search_url_val';</script>");
}
elseif($process == 'modify_crm_state')
{
    $crm_no         = (isset($_POST['crm_no'])) ? $_POST['crm_no'] : "";
    $crm_state      = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($crm_no)){
        echo "crm_no 값이 없습니다.";
        exit;
    }

    $sql = "UPDATE crm_set SET crm_state='{$crm_state}' WHERE crm_no='{$crm_no}'" ;

    if (!mysqli_query($my_db, $sql))
        echo "진행상태 변경에 실패하였습니다.";
    else
        echo "진행상태가 변경되었습니다.";
    exit;
}
elseif($process == 'modify_title')
{
    $crm_no = (isset($_POST['crm_no'])) ? $_POST['crm_no'] : "";
    $title  = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($crm_no)){
        echo "crm_no 값이 없습니다.";
        exit;
    }

    $sql = "UPDATE crm_set SET title='{$title}' WHERE crm_no='{$crm_no}'" ;

    if (!mysqli_query($my_db, $sql))
        echo "알림톡 설정명 변경에 실패하였습니다.";
    else
        echo "알림톡 설정명이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_send_e_date')
{
    $crm_no      = (isset($_POST['crm_no'])) ? $_POST['crm_no'] : "";
    $send_e_date = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($crm_no)){
        echo "crm_no 값이 없습니다.";
        exit;
    }

    $sql = "UPDATE crm_set SET send_e_date='{$send_e_date}' WHERE crm_no='{$crm_no}'" ;

    if (!mysqli_query($my_db, $sql))
        echo "종료일 변경에 실패하였습니다.";
    else
        echo "종료일이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_send_time')
{
    $crm_no    = (isset($_POST['crm_no'])) ? $_POST['crm_no'] : "";
    $send_time = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($crm_no)){
        echo "crm_no 값이 없습니다.";
        exit;
    }

    $sql = "UPDATE crm_set SET send_time='{$send_time}' WHERE crm_no='{$crm_no}'" ;

    if (!mysqli_query($my_db, $sql))
        echo "발송시기 타입 변경에 실패하였습니다.";
    else
        echo "발송시기 타입이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_send_value')
{
    $crm_no    = (isset($_POST['crm_no'])) ? $_POST['crm_no'] : "";
    $send_value = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($crm_no)){
        echo "crm_no 값이 없습니다.";
        exit;
    }

    $sql = "UPDATE crm_set SET send_value='{$send_value}' WHERE crm_no='{$crm_no}'" ;

    if (!mysqli_query($my_db, $sql))
        echo "발송시기 값 변경에 실패하였습니다.";
    else
        echo "발송시기 값이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_send_date')
{
    $crm_no    = (isset($_POST['crm_no'])) ? $_POST['crm_no'] : "";
    $send_date = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($crm_no)){
        echo "crm_no 값이 없습니다.";
        exit;
    }

    $sql = "UPDATE crm_set SET send_date='{$send_date}' WHERE crm_no='{$crm_no}'" ;

    if (!mysqli_query($my_db, $sql))
        echo "발송처리일 값 변경에 실패하였습니다.";
    else
        echo "발송처리일 값이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_exp_hour')
{
    $crm_no    = (isset($_POST['crm_no'])) ? $_POST['crm_no'] : "";
    $exp_hour  = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($crm_no)){
        echo "crm_no 값이 없습니다.";
        exit;
    }

    $sql = "UPDATE crm_set SET exp_hour='{$exp_hour}' WHERE crm_no='{$crm_no}'" ;

    if (!mysqli_query($my_db, $sql))
        echo "전송예정 시간 변경에 실패하였습니다.";
    else
        echo "전송예정 시간이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_exp_min')
{
    $crm_no    = (isset($_POST['crm_no'])) ? $_POST['crm_no'] : "";
    $exp_min   = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($crm_no)){
        echo "crm_no 값이 없습니다.";
        exit;
    }

    $sql = "UPDATE crm_set SET exp_min='{$exp_min}' WHERE crm_no='{$crm_no}'" ;

    if (!mysqli_query($my_db, $sql))
        echo "전송예정 분 변경에 실패하였습니다.";
    else
        echo "전송예정 분이 변경되었습니다.";
    exit;
}
elseif($process == 'modify_memo')
{
    $crm_no = (isset($_POST['crm_no'])) ? $_POST['crm_no'] : "";
    $memo   = (isset($_POST['val'])) ? addslashes($_POST['val']) : "";

    if(empty($crm_no)){
        echo "crm_no 값이 없습니다.";
        exit;
    }

    $sql = "UPDATE crm_set SET memo='{$memo}' WHERE crm_no='{$crm_no}'" ;

    if (!mysqli_query($my_db, $sql))
        echo "메모 저장에 실패하였습니다.";
    else
        echo "메모가 저장되었습니다.";
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "50";
$nav_title   = "구매알림 설정";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$sch_crm_no         = isset($_GET['sch_crm_no']) ? $_GET['sch_crm_no'] : "";
$sch_active         = isset($_GET['sch_active']) ? $_GET['sch_active'] : "1";
$sch_crm_state      = isset($_GET['sch_crm_state']) ? $_GET['sch_crm_state'] : "";
$sch_title          = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_s_name         = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
$sch_send_method    = isset($_GET['sch_send_method']) ? $_GET['sch_send_method'] : "";
$sch_income_type    = isset($_GET['sch_income_type']) ? $_GET['sch_income_type'] : "";
$sch_send_type      = isset($_GET['sch_send_type']) ? $_GET['sch_send_type'] : "";
$sch_temp_type      = isset($_GET['sch_temp_type']) ? $_GET['sch_temp_type'] : "";
$sch_content        = isset($_GET['sch_content']) ? $_GET['sch_content'] : "";
$sch_memo           = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";

$add_where = "1=1";
if(!empty($sch_crm_no))
{
    $add_where .= " AND `crm`.crm_no = '{$sch_crm_no}'";
    $smarty->assign('sch_crm_no', $sch_crm_no);
}

if(!empty($sch_crm_state))
{
    $add_where .= " AND `crm`.crm_state = '{$sch_crm_state}'";
    $smarty->assign('sch_crm_state', $sch_crm_state);
}

if(!empty($sch_active))
{
    $add_where .= " AND `ct`.active = '{$sch_active}'";
    $smarty->assign('sch_active', $sch_active);
}

if(!empty($sch_title))
{
    $add_where .= " AND `crm`.title like '%{$sch_title}%'";
    $smarty->assign('sch_title', $sch_title);
}

if(!empty($sch_s_name))
{
    $add_where .= " AND `crm`.s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_s_name}%')";
    $smarty->assign('sch_s_name', $sch_s_name);
}

if(!empty($sch_send_method))
{
    $add_where .= " AND `crm`.send_method = '{$sch_send_method}'";
    $smarty->assign('sch_send_method', $sch_send_method);
}

if(!empty($sch_income_type))
{
    $add_where .= " AND `crm`.income_type = '{$sch_income_type}'";
    $smarty->assign('sch_income_type', $sch_income_type);
}

if(!empty($sch_send_type))
{
    $add_where .= " AND `crm`.send_type = '{$sch_send_type}'";
    $smarty->assign('sch_send_type', $sch_send_type);
}

if(!empty($sch_temp_type))
{
    $add_where .= " AND `ct`.temp_type = '{$sch_temp_type}'";
    $smarty->assign('sch_temp_type', $sch_temp_type);
}

if(!empty($sch_content))
{
    $add_where .= " AND ct.content like '%{$sch_content}%'";
    $smarty->assign('sch_content', $sch_content);
}

if(!empty($sch_memo))
{
    $add_where .= " AND `crm`.memo like '%{$sch_memo}%'";
    $smarty->assign('sch_memo', $sch_memo);
}

# 전체 게시물 수
$crm_set_total_sql      = "SELECT count(crm_no) FROM (SELECT `crm`.crm_no FROM crm_set `crm` LEFT JOIN crm_template ct ON ct.t_no = crm.t_no WHERE {$add_where}) AS cnt";
$crm_set_total_query	= mysqli_query($my_db, $crm_set_total_sql);
$crm_set_total_result   = mysqli_fetch_array($crm_set_total_query);
$crm_set_total          = $crm_set_total_result[0];

# 페이징 처리
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 10;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($crm_set_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "crm_set_list.php", $pagenum, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $crm_set_total);
$smarty->assign("pagelist", $page);

# CRM 설정
$crm_set_sql  = "
    SELECT 
        crm.crm_no,
        crm.crm_state,
        crm.title,
        crm.dp_company,
        crm.income_type,
        crm.income_value,
        crm.send_time,
        crm.send_type,
        crm.send_value,
        crm.send_e_date,       
        crm.send_method,       
        crm.send_date,       
        crm.stock_s_date,       
        crm.stock_e_date,       
        crm.exp_hour,       
        crm.exp_min,       
        crm.memo,
        ct.temp_type,
        ct.content,
        ct.btn_key,
        ct.btn_content,
        ct.send_name,
        ct.send_phone,
        (SELECT s.s_name FROM staff s WHERE s.s_no=crm.s_no LIMIT 1) as s_name,
        DATE_FORMAT(`crm`.regdate, '%Y-%m-%d') as reg_date,
        DATE_FORMAT(`crm`.regdate, '%H:%i') as reg_time       
    FROM crm_set `crm` 
    LEFT JOIN crm_template ct ON ct.t_no = crm.t_no
    WHERE {$add_where} 
    ORDER BY `crm`.crm_no DESC
    LIMIT {$offset}, {$num}
";
$crm_set_query          = mysqli_query($my_db, $crm_set_sql);
$crm_set_list           = [];
$crm_temp_type_option   = getCrmTempTypeOption();
while($crm_set = mysqli_fetch_assoc($crm_set_query))
{
    $prd_name_list = [];
    if($crm_set['income_type'] == '1'){
        $prd_name_sql       = "SELECT (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd_cms.k_name_code)) AS k_prd1_name, (SELECT k_name FROM kind k WHERE k.k_name_code=prd_cms.k_name_code) AS k_prd2_name, prd_cms.title FROM product_cms prd_cms WHERE prd_cms.prd_no IN({$crm_set['income_value']})";
        $prd_name_query     = mysqli_query($my_db, $prd_name_sql);
        while($prd_name_result = mysqli_fetch_assoc($prd_name_query)){
            $prd_name_list[] = $prd_name_result['k_prd1_name']." > ".$prd_name_result['k_prd2_name']." > ".$prd_name_result['title'];
        }
    }elseif($crm_set['income_type'] == '2'){
        $unit_name_sql      = "SELECT (SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcu.sup_c_no) as sup_c_name, pcu.option_name FROM product_cms_unit pcu WHERE pcu.`no` IN({$crm_set['income_value']})";
        $unit_name_query    = mysqli_query($my_db, $unit_name_sql);
        while($unit_name_result = mysqli_fetch_assoc($unit_name_query)){
            $prd_name_list[] = $unit_name_result['sup_c_name']." > ".$unit_name_result['option_name'];
        }
    }

    if(!empty($prd_name_list)){
        $crm_set['income_value_list'] = $prd_name_list;
    }

    if(!empty($crm_set["dp_company"])) {
        $dp_company_list = explode(",", $crm_set["dp_company"]);
        $crm_set["dp_company_list"] = $dp_company_list;
    }

    if($crm_set['btn_content'])
    {
        $btn_content = json_decode($crm_set['btn_content'], true);

        if(strpos($crm_set['btn_key'], "EASY") !== false){
            foreach($btn_content as $btn){
                $crm_set["btn_type_list"][]    = $btn['type'];
                $crm_set["btn_caption_list"][] = $btn['name'];
                $crm_set["btn_url_list"][]     = $btn['url_mobile'];
            }
        }else{
            foreach($btn_content as $btns){
                foreach($btns as $btn){
                    $crm_set["btn_type_list"][]    = $btn['type'];
                    $crm_set["btn_caption_list"][] = $btn['name'];
                    $crm_set["btn_url_list"][]     = $btn['url_mobile'];
                }
            }
        }
    }

    $crm_set['temp_type_name'] = $crm_temp_type_option[$crm_set['temp_type']];
    $crm_set['send_name']      = $crm_set['send_name'];

    $crm_set_list[] = $crm_set;
}

#템플릿 값
$crm_temp_sql = "SELECT t_no, title FROM crm_template ORDER BY t_no";
$crm_temp_query = mysqli_query($my_db, $crm_temp_sql);
$crm_temp_list = [];
while($crm_temp = mysqli_fetch_assoc($crm_temp_query))
{
    $crm_temp_list[$crm_temp['t_no']] = $crm_temp['title'];
}

$kind_model         = Kind::Factory();
$unit_model         = ProductCmsUnit::Factory();
$company_model      = Company::Factory();
$prd_g1_list        = $kind_model->getKindParentList("product_cms");
$unit_g1_list       = $unit_model->getDistinctUnitCompanyData('sup_c_no');
$dp_company_option  = $company_model->getDpList();

$smarty->assign('display_option', getDisplayOption());
$smarty->assign('prd_g1_list', $prd_g1_list);
$smarty->assign('unit_g1_list', $unit_g1_list);
$smarty->assign('crm_state_option', getCrmStateOption());
$smarty->assign('crm_income_type_option', getCrmIncomeTypeOption());
$smarty->assign('crm_temp_type_option', $crm_temp_type_option);
$smarty->assign('crm_send_type_option', getCrmSendTypeOption());
$smarty->assign('crm_send_time_option', getCrmSendTimeOption());
$smarty->assign('crm_hour_option', getHourOption());
$smarty->assign('crm_min_option', getMinOption());
$smarty->assign('crm_send_method_option', getCrmSendMethodOption());
$smarty->assign('crm_dp_company_option', $dp_company_option);
$smarty->assign('crm_temp_list', $crm_temp_list);
$smarty->assign('crm_set_list', $crm_set_list);

$smarty->display('crm_set_list.html');
?>
