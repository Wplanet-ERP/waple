<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/product_cms_stock.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');

# Model Init
$move_model = ProductCmsStock::Factory();
$move_model->setMainInit("product_cms_stock_transfer", "no");

# 프로세스 처리
$process = (isset($_POST['process'])) ? $_POST['process'] : "";
if ($process == "f_move_date")
{
    $ins_data = array(
        'no'        => (isset($_POST['no'])) ? $_POST['no'] : "",
        'move_date'   => (isset($_POST['val'])) ? $_POST['val'] : "",
    );

    if ($move_model->update($ins_data)){
        echo "일자가 저장 되었습니다.";
    }else{
        echo "일자 저장에 실패 하였습니다.";
    }
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "148";
$nav_title   = "재고이동 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$company_model= Company::Factory();
$unit_model   = ProductCmsUnit::Factory();
$stock_model  = ProductCmsStock::Factory();

$add_where = "1=1";
$add_order = "pcst.no DESC";

$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));
$years_val  = date('Y-m-d', strtotime('-2 years'));

$sch_no                 = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_move_s_date        = isset($_GET['sch_move_s_date']) ? $_GET['sch_move_s_date'] : $months_val;
$sch_move_e_date        = isset($_GET['sch_move_e_date']) ? $_GET['sch_move_e_date'] : $today_val;
$sch_t_type             = isset($_GET['sch_t_type']) ? $_GET['sch_t_type'] : "";
$sch_t_state            = isset($_GET['sch_t_state']) ? $_GET['sch_t_state'] : "";
$sch_sup_c_no           = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_option_name        = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_option_complete    = isset($_GET['sch_option_complete']) ? $_GET['sch_option_complete'] : "";
$sch_out_log_company    = isset($_GET['sch_out_log_company']) ? $_GET['sch_out_log_company'] : "";
$sch_out_warehouse      = isset($_GET['sch_out_warehouse']) ? $_GET['sch_out_warehouse'] : "";
$sch_out_sku            = isset($_GET['sch_out_sku']) ? $_GET['sch_out_sku'] : "";
$sch_in_log_company     = isset($_GET['sch_in_log_company']) ? $_GET['sch_in_log_company'] : "";
$sch_in_warehouse       = isset($_GET['sch_in_warehouse']) ? $_GET['sch_in_warehouse'] : "";
$sch_in_sku             = isset($_GET['sch_in_sku']) ? $_GET['sch_in_sku'] : "";
$sch_move_date_type     = isset($_GET['sch_move_date_type']) ? $_GET['sch_move_date_type'] : "months";
$sch_file_no            = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);
$smarty->assign("years_val", $years_val);
$smarty->assign("sch_move_date_type", $sch_move_date_type);

if (!empty($sch_no)) {
    $add_where .= " AND pcst.no='{$sch_no}'";
    $smarty->assign("sch_no", $sch_no);
}

if (!empty($sch_move_s_date)) {
    $add_where .= " AND pcst.move_date >= '{$sch_move_s_date}'";
    $smarty->assign("sch_move_s_date", $sch_move_s_date);
}

if (!empty($sch_move_e_date)) {
    $add_where .= " AND pcst.move_date <= '{$sch_move_e_date}'";
    $smarty->assign("sch_move_e_date", $sch_move_e_date);
}

if (!empty($sch_t_type)) {
    $add_where .= " AND pcst.t_type='{$sch_t_type}'";
    $smarty->assign("sch_t_type", $sch_t_type);
}

if (!empty($sch_t_state)) {
    $add_where .= " AND pcst.t_state='{$sch_t_state}'";
    $smarty->assign("sch_t_state", $sch_t_state);
}

if (!empty($sch_sup_c_no)) {
    $add_where .= " AND pcst.sup_c_no = '{$sch_sup_c_no}'";
    $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
}

if (!empty($sch_option_complete)) {
    $smarty->assign("sch_option_complete", $sch_option_complete);
}

if (!empty($sch_option_name)) {
    if($sch_option_complete == '1'){
        $add_where .= " AND pcst.option_name = '{$sch_option_name}'";
    }else{
        $add_where .= " AND pcst.option_name like '%{$sch_option_name}%'";
    }
    $smarty->assign("sch_option_name", $sch_option_name);
}

if (!empty($sch_out_log_company)) {
    $add_where .= " AND pcst.out_log_c_no = '{$sch_out_log_company}'";
    $smarty->assign("sch_out_log_company", $sch_out_log_company);
}

if (!empty($sch_out_warehouse)) {
    $add_where .= " AND pcst.out_warehouse like '%{$sch_out_warehouse}%'";
    $smarty->assign("sch_out_warehouse", $sch_out_warehouse);
}

if (!empty($sch_out_sku)) {
    $add_where .= " AND pcst.out_sku like '%{$sch_out_sku}%'";
    $smarty->assign("sch_out_sku", $sch_out_sku);
}

if (!empty($sch_in_log_company)) {
    $add_where .= " AND pcst.in_log_c_no = '{$sch_in_log_company}'";
    $smarty->assign("sch_in_log_company", $sch_in_log_company);
}

if (!empty($sch_in_warehouse)) {
    $add_where .= " AND pcst.in_warehouse like '%{$sch_in_warehouse}%'";
    $smarty->assign("sch_in_warehouse", $sch_in_warehouse);
}

if (!empty($sch_in_sku)) {
    $add_where .= " AND pcst.in_sku like '%{$sch_in_sku}%'";
    $smarty->assign("sch_in_sku", $sch_in_sku);
}

if (!empty($sch_file_no)) {
    $add_where .= " AND pcst.file_no = '{$sch_file_no}'";
    $smarty->assign("sch_file_no", $sch_file_no);
}

// 전체 게시물 수
$stock_transfer_total_sql   = "SELECT count(*) as total, SUM(in_qty) as sum_in_qty, SUM(out_qty) as sum_out_qty FROM (SELECT pcst.no, pcst.in_qty, pcst.out_qty FROM product_cms_stock_transfer pcst LEFT JOIN product_cms_unit as pcu ON pcu.no = pcst.prd_unit WHERE {$add_where}) AS cnt";
$stock_transfer_total_query = mysqli_query($my_db, $stock_transfer_total_sql);

if(!!$stock_transfer_total_query)
    $stock_transfer_total_result = mysqli_fetch_array($stock_transfer_total_query);

$stock_transfer_total   = $stock_transfer_total_result['total'];
$transfer_sum_in_total  = $stock_transfer_total_result['sum_in_qty'];
$transfer_sum_out_total = $stock_transfer_total_result['sum_out_qty'];

//페이징
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num        = 20;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($stock_transfer_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "product_cms_stock_transfer_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $stock_transfer_total);
$smarty->assign("transfer_sum_in_total", $transfer_sum_in_total);
$smarty->assign("transfer_sum_out_total", $transfer_sum_out_total);
$smarty->assign("pagelist", $pagelist);

$stock_transfer_sql = "
    SELECT
        *,
        (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcst.sup_c_no) as sup_c_name,
        (SELECT um.file_path FROM upload_management um WHERE um.file_no=pcst.file_no) as file_path,
        (SELECT um.file_name FROM upload_management um WHERE um.file_no=pcst.file_no) as file_name
    FROM product_cms_stock_transfer as pcst
    WHERE {$add_where}
    ORDER BY {$add_order}
    LIMIT {$offset},{$num}
";
$stock_transfer_query   = mysqli_query($my_db, $stock_transfer_sql);
$stock_transfer_list    = [];
$sup_c_list             = $unit_model->getDistinctUnitCompanyData("sup_c_no");
$transfer_state_option  = $stock_model->getTransferStateOption();
$transfer_type_option   = $stock_model->getTransferTypeOption();
$log_company_list       = $company_model->getLogisticsList();

while($stock_transfer = mysqli_fetch_assoc($stock_transfer_query))
{
    $stock_transfer['out_log_c_name']   = $log_company_list[$stock_transfer['out_log_c_no']];
    $stock_transfer['in_log_c_name']    = $log_company_list[$stock_transfer['in_log_c_no']];

    $stock_transfer_list[] = $stock_transfer;
}

$smarty->assign('sup_c_list', $sup_c_list);
$smarty->assign('sch_type_option', $transfer_type_option);
$smarty->assign('sch_state_option', $transfer_state_option);
$smarty->assign('log_company_list', $log_company_list);
$smarty->assign('stock_transfer_list', $stock_transfer_list);

$smarty->display('product_cms_stock_transfer_list.html');
?>
