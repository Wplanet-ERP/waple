<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/product_cms.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "● 커머스 상품 리스트");
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A2', "prd_no")
	->setCellValue('B2', "Display")
	->setCellValue('C2', "상품그룹1")
	->setCellValue('D2', "상품그룹2")
	->setCellValue('E2', "업체명")
	->setCellValue('F2', "구분")
	->setCellValue('G2', "상품명")
    ->setCellValue('H2', "공급업체")
    ->setCellValue('I2', "구성품명")
    ->setCellValue('J2', "수량")
    ->setCellValue('K2', "상품코드")
	->setCellValue('L2', "담당자")
	->setCellValue('M2', "권장가")
	->setCellValue('N2', "상품소개")
	->setCellValue('O2', "최근 주문일")
	->setCellValue('P2', "최근 입/출고일")
	->setCellValue('Q2', "작성일")
	->setCellValue('R2', "작성자")
;

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setSize(18);
$objPHPExcel->getActiveSheet()->getStyle('A2:R2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A2:R2')->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle('A2:R2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
$objPHPExcel->getActiveSheet()->getStyle('A2:R2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where   			= "1=1";
$sch_prd_g1     		= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2    		 	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd_name   		= isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_prd_type   		= isset($_GET['sch_prd_type']) ? $_GET['sch_prd_type'] : "";
$sch_display            = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$sch_manager            = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : $session_name;
$sch_c_name             = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_prd_code           = isset($_GET['sch_prd_code']) ? $_GET['sch_prd_code'] : "";
$sch_unit_name          = isset($_GET['sch_unit_name']) ? $_GET['sch_unit_name'] : "";
$sch_gift_date_type     = isset($_GET['sch_gift_date_type']) ? $_GET['sch_gift_date_type'] : "";
$sch_gift_date_on_off   = isset($_GET['sch_gift_date_on_off']) ? $_GET['sch_gift_date_on_off'] : "";
$sch_gift_s_date        = isset($_GET['sch_gift_s_date']) ? $_GET['sch_gift_s_date'] : "";
$sch_gift_e_date        = isset($_GET['sch_gift_e_date']) ? $_GET['sch_gift_e_date'] : "";
$sch_solo_prd           = isset($_GET['sch_solo_prd']) ? $_GET['sch_solo_prd'] : "";
$sch_combined_pack      = isset($_GET['sch_combined_pack']) ? $_GET['sch_combined_pack'] : "";
$gift_type_option       = getGiftTypeOption();

if(!empty($sch_prd_g1)) {
	$add_where  .= " AND k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}')";
}

if(!empty($sch_prd_g2)) {
	$add_where  .= " AND k_name_code = '{$sch_prd_g2}'";
}

if(!empty($sch_prd_name)) {
	$add_where  .= " AND title like '%{$sch_prd_name}%'";
}

if(!empty($sch_prd_type)) {
	$add_where  .= " AND prd_type = '{$sch_prd_type}'";
}

if(!empty($sch_display)) {
	$add_where  .= " AND display = '{$sch_display}'";
}

if(!empty($sch_manager)) {
	$add_where  .= " AND manager in (SELECT s.s_no FROM staff s where s.s_name like '%{$sch_manager}%')";
}

if(!empty($sch_c_name)) {
	$add_where  .= " AND c_no in (SELECT c.c_no FROM company c where c.c_name like '%{$sch_c_name}%')";
}

if(!empty($sch_prd_code)) {
	$add_where  .= " AND prd_code = '{$sch_prd_code}'";
}

if(!empty($sch_unit_name)) {
	$add_where  .= " AND prd_no in (SELECT sub.prd_no FROM product_cms_relation sub WHERE sub.display='1' AND sub.option_no IN(SELECT pcu.`no` FROM product_cms_unit pcu WHERE pcu.option_name like '%{$sch_unit_name}%'))";
}

if(!empty($sch_gift_date_type))
{
	$sch_gift_s_date_type = $gift_type_option[$sch_gift_date_type]['s_date'];
	$sch_gift_e_date_type = $gift_type_option[$sch_gift_date_type]['e_date'];

	if(!empty($sch_gift_date_on_off)) {
		$add_where  .= " AND `{$sch_gift_date_type}` = '{$sch_gift_date_on_off}'";
	}

	if(!empty($sch_gift_s_date)){
		$sch_gift_s_date_time = $sch_gift_s_date." 00:00:00";
		$add_where  .= " AND `{$sch_gift_s_date_type}` >= '{$sch_gift_s_date_time}'";
	}

	if(!empty($sch_gift_e_date)){
		$sch_gift_e_date_time = $sch_gift_e_date." 23:59:59";
		$add_where  .= " AND `{$sch_gift_e_date_type}` <= '{$sch_gift_e_date_time}'";
	}
}
else
{
	if(!empty($sch_gift_date_on_off)) {
		$add_where  .= " AND (gift_date_type='{$sch_gift_date_on_off}' OR sub_gift_date_type='{$sch_gift_date_on_off}')";
	}

	if(!empty($sch_gift_s_date)){
		$sch_gift_s_date_time = $sch_gift_s_date." 00:00:00";
		$add_where  .= " AND (gift_s_date >= '{$sch_gift_s_date_time}' OR sub_gift_s_date >= '{$sch_gift_s_date_time}')";
	}

	if(!empty($sch_gift_e_date)){
		$sch_gift_e_date_time = $sch_gift_e_date." 23:59:59";
		$add_where  .= " AND (gift_e_date <= '{$sch_gift_e_date_time}' OR sub_gift_e_date <= '{$sch_gift_e_date_time}')";
	}
}

if(!empty($sch_solo_prd))
{
	if($sch_solo_prd == '1'){
		$add_where  .= " AND solo_prd != ''";
	}elseif($sch_solo_prd == '2'){
		$add_where  .= " AND (solo_prd IS NULL OR solo_prd = '')";
	}
}

if(!empty($sch_combined_pack))
{
	if($sch_combined_pack == '1'){
		$add_where  .= " AND combined_pack = '2'";
	}elseif($sch_combined_pack == '2'){
		$add_where  .= " AND combined_pack = '1'";
	}
}

if(!empty($sch_is_in_out) && $sch_is_in_out == "2")
{
	$add_where .= " AND prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation AS pcr WHERE pcr.option_no IN(SELECT pcu.no FROM product_cms_unit AS pcu WHERE pcu.is_in_out='2') AND pcr.display='1')";
}

# 정렬순서 토글 & 필드 지정
$add_orderby    = "prd_no DESC";
$ord_type       = (isset($_GET['ord_type']) && !empty($_GET['ord_type'])) ? $_GET['ord_type'] : "";
$ord_type_by    = (isset($_GET['ord_type_by']) && !empty($_GET['ord_type_by'])) ? $_GET['ord_type_by'] : "";
if(!empty($ord_type))
{
	if($ord_type_by == '1'){
		$add_orderby = "{$ord_type} ASC, prd_no DESC";
	}elseif($ord_type_by == '2'){
		$add_orderby = "{$ord_type} DESC, prd_no DESC";
	}else{
		$add_orderby = "prd_no DESC";
	}
}

// 리스트 쿼리
$product_cms_sql = "
	SELECT
	   	prd_cms.*,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd_cms.k_name_code)) AS k_prd1_name,
		(SELECT k_name FROM kind k WHERE k.k_name_code=prd_cms.k_name_code) AS k_prd2_name,
		(SELECT s_name FROM staff s WHERE s.s_no=prd_cms.manager) AS manager_name,
		(SELECT team_name FROM team t WHERE t.team_code=prd_cms.manager_team) AS t_name,
		(SELECT c_name FROM company c WHERE c.c_no=prd_cms.c_no) AS c_name,
		(SELECT s_name FROM staff s WHERE s.s_no=prd_cms.writer) AS writer_name,
	    (SELECT COUNT(pcr.option_no) as opt_cnt FROM product_cms_relation pcr WHERE pcr.prd_no=prd_cms.prd_no AND pcr.display='1') as opt_cnt,
	    (SELECT MAX(w.run_date) FROM work_cms_quick w WHERE w.prd_no=prd_cms.prd_no AND w.prd_type='1' AND w.quick_state='4') as quick_date
	FROM product_cms prd_cms
    WHERE {$add_where}
    ORDER BY {$add_orderby}
";
$product_cms_query 	= mysqli_query($my_db, $product_cms_sql);
$prd_type_option	= getPrdTypeOption();
$idx = 3;
if(!!$product_cms_query)
{
    while($product_cms = mysqli_fetch_array($product_cms_query))
    {
		$display_name 	= "";
		$prd_type_name 	= isset($prd_type_option[$product_cms['prd_type']]) ? $prd_type_option[$product_cms['prd_type']] : "";
		$opt_cnt 		= ($product_cms['opt_cnt'] > 1) ? $product_cms['opt_cnt'] : 1;

		if($product_cms['display'] == '2'){
			$display_name = "OFF";
		}elseif($product_cms['display'] == '1'){
			$display_name = "ON";
		}

		if($opt_cnt > 1)
		{
			$mergeCell = $idx + $opt_cnt - 1;
            $objPHPExcel->setActiveSheetIndex(0)
                ->mergeCells("A{$idx}:A{$mergeCell}")
                ->mergeCells("B{$idx}:B{$mergeCell}")
                ->mergeCells("C{$idx}:C{$mergeCell}")
                ->mergeCells("D{$idx}:D{$mergeCell}")
                ->mergeCells("E{$idx}:E{$mergeCell}")
                ->mergeCells("F{$idx}:F{$mergeCell}")
                ->mergeCells("G{$idx}:G{$mergeCell}")
                ->mergeCells("K{$idx}:K{$mergeCell}")
                ->mergeCells("L{$idx}:L{$mergeCell}")
                ->mergeCells("M{$idx}:M{$mergeCell}")
                ->mergeCells("N{$idx}:N{$mergeCell}")
                ->mergeCells("O{$idx}:O{$mergeCell}")
                ->mergeCells("P{$idx}:P{$mergeCell}")
                ->mergeCells("Q{$idx}:Q{$mergeCell}")
                ->mergeCells("R{$idx}:R{$mergeCell}");
        }

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$idx, $product_cms['prd_no'])
            ->setCellValue('B'.$idx, $display_name)
            ->setCellValue('C'.$idx, $product_cms['k_prd1_name'])
            ->setCellValue('D'.$idx, $product_cms['k_prd2_name'])
            ->setCellValue('E'.$idx, $product_cms['c_name'])
            ->setCellValue('F'.$idx, $prd_type_name)
            ->setCellValue('G'.$idx, $product_cms['title'])
            ->setCellValue('K'.$idx, $product_cms['prd_code'])
            ->setCellValue('L'.$idx, $product_cms['manager_name'])
            ->setCellValue('M'.$idx, $product_cms['suggested_price'])
            ->setCellValue('N'.$idx, $product_cms['description'])
            ->setCellValue('O'.$idx, $product_cms['order_date'])
            ->setCellValue('P'.$idx, $product_cms['quick_date'])
            ->setCellValue('Q'.$idx, $product_cms['writer_date'])
            ->setCellValue('R'.$idx, $product_cms['writer_name'])
		;

		$pcu_sql 	= "
			SELECT
				(SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no) as sup_c_name,
				pcu.option_name,
				pcr.priority,
				pcr.quantity
			FROM product_cms_unit as pcu
			LEFT JOIN product_cms_relation as pcr ON pcr.option_no = pcu.no
			WHERE pcr.prd_no = '{$product_cms['prd_no']}'AND pcr.display='1'
			ORDER BY pcr.priority ASC
		";
		$pcu_query  = mysqli_query($my_db, $pcu_sql);
		if(!!$pcu_query) {
			$pcu_num = mysqli_num_rows($pcu_query);
			if($pcu_num > 0)
			{
                while ($pcu = mysqli_fetch_assoc($pcu_query)) {
                    $sup_c_name = isset($pcu['sup_c_name']) ? $pcu['sup_c_name'] : "";
                    $option_name = isset($pcu['option_name']) ? $pcu['option_name'] : "";
                    $priority = isset($pcu['priority']) ? $pcu['priority'] : 0;
                    $quantity = isset($pcu['quantity']) ? $pcu['quantity'] : 0;
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("H{$idx}", $sup_c_name)
                        ->setCellValue("I{$idx}", $option_name)
                        ->setCellValue("J{$idx}", $quantity);

                    $idx++;
                }
            }else{
                $objPHPExcel->setActiveSheetIndex(0)
					->setCellValue("H{$idx}", "")
					->setCellValue("I{$idx}", "")
					->setCellValue("J{$idx}", 0);

                $idx++;
			}
        }
    }
}

$idx--;

$objPHPExcel->getActiveSheet()->getStyle("A3:R{$idx}")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("A3:R{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A3:R{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G3:G'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('I3:I'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('N3:N'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('E3:K'.$idx)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00FFFFCC');

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(8);

$objPHPExcel->getActiveSheet()->getStyle("A2:R{$idx}")->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->setTitle('커머스 상품 리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_커머스 상품 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
