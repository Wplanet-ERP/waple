<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/evaluation.php');
require('inc/model/MyQuick.php');
require('inc/model/Staff.php');
require('inc/model/Team.php');
require('inc/model/Evaluation.php');

$ev_system_model 	= Evaluation::Factory();
$staff_model		= Staff::Factory();
$team_model			= Team::Factory();

$is_super_admin = false;
if($session_s_no == '1' || $session_s_no == '28'){
	$is_super_admin = true;
}
$smarty->assign("is_super_admin", $is_super_admin);

# Navigation & My Quick
$nav_prd_no  = "172";
$nav_title   = "평가 결과 리스트(통계)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$add_where          	= "1=1 AND er.is_complete='1' AND (er.rec_is_review='1' AND er.eval_is_review='1') AND (esr.evaluator_s_no != esr.receiver_s_no)";
$sch_ev_no              = isset($_GET['sch_ev_no']) ? $_GET['sch_ev_no'] : "";
$sch_result_type  		= isset($_GET['sch_result_type']) ? $_GET['sch_result_type'] : "evaluation_value";
$sch_receiver_team  	= isset($_GET['sch_receiver_team']) ? $_GET['sch_receiver_team'] : "";
$sch_receiver_staff 	= isset($_GET['sch_receiver_staff']) ? $_GET['sch_receiver_staff'] : "";
$sch_receiver_name 		= isset($_GET['sch_receiver_name']) ? $_GET['sch_receiver_name'] : "";
$sch_evaluator_team  	= isset($_GET['sch_evaluator_team']) ? $_GET['sch_evaluator_team'] : "";
$sch_evaluator_staff 	= isset($_GET['sch_evaluator_staff']) ? $_GET['sch_evaluator_staff'] : "";
$sch_evaluator_name 	= isset($_GET['sch_evaluator_name']) ? $_GET['sch_evaluator_name'] : "";
$ord_type 				= isset($_GET['ord_type']) ? $_GET['ord_type'] : "total_avg";
$ord_type_by    		= isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";
$search_url 			= getenv("QUERY_STRING");

$ev_result_type_option  = getEvResultTypeOption();
$ev_system_list			= $ev_system_model->getEvaluationSystemCompleteListByStaff($session_s_no);
$team_full_name_list    = $team_model->getTeamFullNameList();
$team_all_list          = $team_model->getTeamAllList();
$all_staff_list    		= $team_all_list['staff_list'];
$sch_rec_staff_list     = [];
$sch_eval_staff_list    = [];
$evaluation_system		= [];
$ev_result_tmp_list		= [];
$ev_result_rec_tmp_list	= [];
$ev_unit_question_list  = [];
$ev_result_list 		= [];
$ev_result_sort_chk_list= [];
$ev_result_type_name	= $ev_result_type_option[$sch_result_type];
$smarty->assign("ev_result_type_name", $ev_result_type_name);
$smarty->assign("sch_result_type", $sch_result_type);

if(empty($sch_ev_no) && $is_super_admin)
{
	$pro_system_sql 	= "SELECT ev_no FROM evaluation_system WHERE ev_state IN(3,4) ORDER BY ev_no DESC LIMIT 1";
	$pro_system_query 	= mysqli_query($my_db, $pro_system_sql);
	$pro_system_result 	= mysqli_fetch_assoc($pro_system_query);

	$sch_ev_no = $pro_system_result['ev_no'];
	if(empty($search_url)){
		$search_url = "sch_ev_no={$sch_ev_no}";
	}
}

if(!empty($sch_ev_no))
{
	# 해당 평가지 체크
	$evaluation_system 	= $ev_system_model->getItem($sch_ev_no);

	if(empty($evaluation_system['ev_no'])) {
		exit("<script>alert('없는 평가지 입니다.');location.href='evaluation_result_stats.php';</script>");
	}

	if(!$is_super_admin && $evaluation_system['admin_s_no'] != $session_s_no) {
		exit("<script>alert('평가 결과 확인 권한이 없습니다.');location.href='evaluation_result_stats.php';</script>");
	}

	if(!$is_super_admin && $evaluation_system['ev_state'] != '3' && $evaluation_system['ev_state'] != '4') {
		exit("<script>alert('아직 종료된 평가가 아닙니다.');location.href='evaluation_result_stats.php';</script>");
	}

	$add_where 			.= " AND esr.ev_no='{$sch_ev_no}'";
	$smarty->assign("sch_ev_no", $sch_ev_no);

	if($evaluation_system['ev_state'] == '2' || $evaluation_system['ev_state'] == '3')
	{
		$add_where .= " AND esr.receiver_s_no !='{$session_s_no}'";
	}

	if(!empty($sch_evaluator_team))
	{
		if($sch_evaluator_team != 'all')
		{
			$sch_eval_staff_list = $all_staff_list[$sch_evaluator_team];
			$sch_team_code_where = getTeamWhere($my_db, $sch_evaluator_team);
			if($sch_team_code_where){
				$add_where .= " AND er.evaluator_team IN ({$sch_team_code_where})";
			}
		}
		$smarty->assign("sch_evaluator_team", $sch_evaluator_team);
	}

	if(!empty($sch_evaluator_staff)) {
		$add_where .= " AND esr.evaluator_s_no='{$sch_evaluator_staff}'";
		$smarty->assign("sch_evaluator_staff", $sch_evaluator_staff);
	}

	if(!empty($sch_evaluator_name)) {
		$add_where .= " AND esr.evaluator_s_no=(SELECT s.s_no FROM staff s WHERE s.s_name='{$sch_evaluator_name}')";
		$smarty->assign("sch_evaluator_name", $sch_evaluator_name);
	}

	if(!empty($sch_receiver_team))
	{
		if($sch_receiver_team != 'all')
		{
			$sch_rec_staff_list = $all_staff_list[$sch_receiver_team];
			$sch_team_code_where = getTeamWhere($my_db, $sch_receiver_team);
			if($sch_team_code_where){
				$add_where .= " AND er.receiver_team IN ({$sch_team_code_where})";
			}
		}
		$smarty->assign("sch_receiver_team", $sch_receiver_team);
	}

	if(!empty($sch_receiver_staff)) {
		$add_where .= " AND esr.receiver_s_no='{$sch_receiver_staff}'";
		$smarty->assign("sch_receiver_staff", $sch_receiver_staff);
	}

	if(!empty($sch_receiver_name)) {
		$add_where .= " AND esr.receiver_s_no=(SELECT s.s_no FROM staff s WHERE s.s_name='{$sch_receiver_name}')";
		$smarty->assign("sch_receiver_name", $sch_receiver_name);
	}

	$evaluation_unit_sql    = "SELECT * FROM evaluation_unit WHERE ev_u_set_no='{$evaluation_system['ev_u_set_no']}' AND active='1' AND evaluation_state NOT IN(99,100) ORDER BY `order`, page ASC";
	$evaluation_unit_query  = mysqli_query($my_db, $evaluation_unit_sql);
	$page_idx               = 0;
	$page_chk               = 1;
	while($evaluation_unit = mysqli_fetch_array($evaluation_unit_query))
	{
		if($page_chk == $evaluation_unit['page']){
			$page_idx++;
		}else{
			$page_chk = $evaluation_unit['page'];
			$page_idx = 1;
		}

		$ev_unit_question_list[$evaluation_unit['ev_u_no']] = array(
			'subject'   => $evaluation_unit['page']."_".$page_idx,
			"type"      => $evaluation_unit['evaluation_state'],
			"question"  => $evaluation_unit['question']
		);
	}

	if(!empty($ev_unit_question_list)){
		$ev_unit_question_list["avg"] = array(
			"subject"	=> "평균",
			"type"      => "",
			"question"  => ""
		);
	}

	$ev_result_sql = "
		SELECT 
			esr.ev_result_no,
			esr.ev_r_no,
			esr.ev_u_no,
			esr.evaluation_state,
			esr.evaluation_value,
			esr.sqrt,
		    esr.receiver_s_no,   
		    er.receiver_team,   
		    esr.evaluator_s_no,
		    esr.rate,
			(SELECT s.s_name FROM staff s WHERE s.s_no=esr.receiver_s_no) as rec_name,
			(SELECT t.team_name FROM team t WHERE t.team_code=er.receiver_team) as rec_team,
			(SELECT t.priority FROM team t WHERE t.team_code=er.receiver_team) as rec_t_priority,
			(SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as eval_name,
			(SELECT t.team_name FROM team t WHERE t.team_code=er.evaluator_team) as eval_team,   
			(SELECT t.priority FROM team t WHERE t.team_code=er.evaluator_team) as eval_t_priority,
			(SELECT s.s_name FROM staff s WHERE s.s_no=er.evaluator_s_no) as eval_name
		FROM evaluation_system_result as esr
		LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
		WHERE {$add_where}
		ORDER BY rec_t_priority, er.receiver_s_no ASC, rate DESC, evaluator_s_no, ev_u_no ASC
	";
	$ev_result_query = mysqli_query($my_db, $ev_result_sql);
	while($ev_result = mysqli_fetch_assoc($ev_result_query))
	{
		if(!isset($ev_result_rec_tmp_list[$ev_result['receiver_s_no']]))
		{
			$rec_tmp_data = array(
				'eval_cnt' 	=> 0,
			);

			$rec_data = array(
				'rec_team' 	=> $ev_result['receiver_team'],
				'rec_t_name'=> $ev_result['rec_team'],
				'rec_name' 	=> $ev_result['rec_name'],
				'eval_cnt' 	=> 0,
				'avg' 		=> 0,
				'avg_total' => 0,
				'per_sqrt' 	=> 0
			);

			foreach($ev_unit_question_list as $ev_u_no => $unit_data)
			{
				if($unit_data['type'] == '1' || $unit_data['type'] == '2' || $unit_data['type'] == '3'){
					$rec_tmp_data[$ev_u_no] = 0;
					$rec_data[$ev_u_no] 	= 0;
				}elseif($unit_data['type'] == '4' || $unit_data['type'] == '5'){
					$rec_data[$ev_u_no] 	= [];
				}else{
					$rec_data[$ev_u_no] 	= "";
				}
			}

			$ev_result_rec_tmp_list[$ev_result['receiver_s_no']] = $rec_tmp_data;
			$ev_result_list[$ev_result['receiver_s_no']] 		 = $rec_data;
		}

		if(!isset($ev_result_tmp_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']]))
		{
			$ev_result_rec_tmp_list[$ev_result['receiver_s_no']]['eval_cnt']++;

			$result_data = array(
				'rate' 		=> $ev_result['rate'],
				'num_cnt' 	=> 0,
				'total' 	=> 0
			);

			foreach($ev_unit_question_list as $ev_u_no => $unit_data)
			{
				$result_data[$ev_u_no] = ($unit_data['type'] == '1' || $unit_data['type'] == '2' || $unit_data['type'] == '3') ? 0 : "";
			}

			$ev_result_tmp_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']] = $result_data;
		}

		if($ev_result['evaluation_state'] == 1 || $ev_result['evaluation_state'] == 2 || $ev_result['evaluation_state'] == 3)
		{
			$ev_value = ($sch_result_type == 'sqrt') ? $ev_result['sqrt'] : $ev_result['evaluation_value'];
			$ev_result_tmp_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']][$ev_result['ev_u_no']] = $ev_value;
			$ev_result_tmp_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']]['total'] += $ev_value;
			$ev_result_tmp_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']]['num_cnt']++;

			$ev_result_rec_tmp_list[$ev_result['receiver_s_no']][$ev_result['ev_u_no']] += $ev_value;
		}
		elseif($ev_result['evaluation_state'] == 4 || $ev_result['evaluation_state'] == 5)
		{
			$ev_result_list[$ev_result['receiver_s_no']][$ev_result['ev_u_no']][] = $ev_result['evaluation_value'];
		}
		else{
			$ev_result_tmp_list[$ev_result['receiver_s_no']][$ev_result['evaluator_s_no']][$ev_result['ev_u_no']] = $ev_result['evaluation_value'];
		}
	}
}

foreach($ev_result_tmp_list as $rec_s_no => $rec_data)
{
	foreach($rec_data as $eval_s_no => $eval_data)
	{
		if($eval_data['num_cnt'] > 0)
		{
			$cal_avg = round($eval_data['total']/$eval_data['num_cnt'], 2);
			$per_avg = round($cal_avg*($eval_data['rate']/100),2);

			$ev_result_list[$rec_s_no]['avg_total'] += $cal_avg;
			$ev_result_list[$rec_s_no]['per_sqrt']	+= $per_avg;
		}
	}

	if($ev_result_rec_tmp_list[$rec_s_no])
	{
		$eval_cnt = $ev_result_rec_tmp_list[$rec_s_no]['eval_cnt'];
		$ev_result_list[$rec_s_no]['avg']		= round($ev_result_list[$rec_s_no]['avg_total']/$eval_cnt, 2);
		$ev_result_list[$rec_s_no]['per_sqrt']	= round($ev_result_list[$rec_s_no]['per_sqrt'], 2);
		foreach($ev_result_rec_tmp_list[$rec_s_no] as $key => $tmp_value)
		{
			if($key != 'eval_cnt'){
				$ev_result_list[$rec_s_no][$key] = round($tmp_value/$eval_cnt, 2);
			}else{
				$ev_result_list[$rec_s_no][$key] = $tmp_value;
			}
		}
	}

	$sort_value = ($ord_type == 'eval_cnt') ? $ev_result_rec_tmp_list[$rec_s_no]['eval_cnt'] : $ev_result_list[$rec_s_no]['per_sqrt'];

	$ev_result_sort_chk_list[$rec_s_no] = $sort_value;
}

if($ord_type_by == '2'){
	arsort($ev_result_sort_chk_list);
}else{
	asort($ev_result_sort_chk_list);
}

$smarty->assign("ord_type", $ord_type);
$smarty->assign("ord_type_by", $ord_type_by);


$ev_unit_question_cnt 	= ($sch_result_type == 'sqrt') ? count($ev_unit_question_list)+1 : count($ev_unit_question_list);
$ev_unit_total_cnt 		= (empty($sch_ev_no) || $sch_result_type == 'sqrt') ? $ev_unit_question_cnt+5 : $ev_unit_question_cnt+4;

$smarty->assign("search_url", $search_url);
$smarty->assign("ev_system_list", $ev_system_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("sch_eval_staff_list", $sch_eval_staff_list);
$smarty->assign("sch_rec_staff_list", $sch_rec_staff_list);
$smarty->assign("ev_result_type_option", $ev_result_type_option);
$smarty->assign("ev_unit_total_cnt", $ev_unit_total_cnt);
$smarty->assign("ev_unit_question_cnt", $ev_unit_question_cnt);
$smarty->assign("ev_unit_question_list", $ev_unit_question_list);
$smarty->assign("evaluation_system", $evaluation_system);
$smarty->assign("ev_result_list", $ev_result_list);
$smarty->assign("ev_result_sort_chk_list", $ev_result_sort_chk_list);

$smarty->display('evaluation_result_stats.html');

?>
