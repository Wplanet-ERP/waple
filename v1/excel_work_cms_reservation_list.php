<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_cms.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$excel_title    = "예약판매 리스트";
$nowdate        = date("Y-m-d H:i:s");

# 상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "주문일시")
	->setCellValue('B1', "주문번호")
	->setCellValue('C1', "예약상태")
	->setCellValue('D1', "수령자명")
	->setCellValue('E1', "수령자전화")
	->setCellValue('F1', "우편번호")
	->setCellValue('G1', "수령지")
	->setCellValue('H1', "배송메모")
	->setCellValue('I1', "업체명/브랜드명")
	->setCellValue('J1', "커머스 상품명")
	->setCellValue('K1', "수량")
	->setCellValue('L1', "주문요약정보")
	->setCellValue('M1', "결제금액")
	->setCellValue('N1', "결제일시")
	->setCellValue('O1', "구매처")
	->setCellValue('P1', "구성품")
;

# 상품 검색
$add_where      = "1=1";
$sch_prd_g1	    = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	    = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	    = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

if (!empty($sch_prd) && $sch_prd != "0") {
    $add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

# 브랜드 검색
$sch_brand_g1           = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2           = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand              = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

if(!empty($sch_brand)) {
    $add_where      .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$sch_order_s_date 	    = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : "";
$sch_order_e_date 	    = isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : "";
$sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_recipient 		    = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp 	    = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_recipient_addr 	= isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
$sch_dp_c_no 		    = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_prd_name 		    = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_option	            = isset($_GET['sch_option']) ? $_GET['sch_option'] : "";

if(!empty($sch_order_s_date) || !empty($sch_order_e_date))
{
    if(!empty($sch_order_s_date)){
        $sch_ord_s_datetime = $sch_order_s_date." 00:00:00";
        $add_where .= " AND w.order_date >= '{$sch_ord_s_datetime}'";
    }

    if(!empty($sch_order_e_date)){
        $sch_ord_e_datetime = $sch_order_e_date." 23:59:59";
        $add_where .= " AND w.order_date <= '{$sch_ord_e_datetime}'";
    }
}

if(!empty($sch_order_number)){
    $add_where .= " AND w.order_number = '{$sch_order_number}'";
}

if(!empty($sch_delivery_state)){
    $add_where .= " AND w.delivery_state = '{$sch_delivery_state}'";
}else{
    $add_where .= " AND w.delivery_state != '4'";
}

if(!empty($sch_recipient)){
    $add_where .= " AND w.recipient like '%{$sch_recipient}%'";
}

if(!empty($sch_recipient_hp)){
    $add_where .= " AND w.recipient_hp like '%{$sch_recipient_hp}%'";
}

if(!empty($sch_recipient_addr)){
    $add_where .= " AND w.recipient_addr like '%{$sch_recipient_addr}%'";
}

if(!empty($sch_dp_c_no)){
    $add_where .= " AND w.dp_c_no = '{$sch_dp_c_no}'";
}

if(!empty($sch_prd_name)){
    $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no from product_cms prd_cms where prd_cms.title like '%{$sch_prd_name}%')";
}

if(!empty($sch_option))
{
    $chk_option_where = "";
    $chk_option_list  = [];

    if($sch_option == "240"){
        $chk_option_where = "option_no IN(240,241)";
    }elseif($sch_option == "238"){
        $chk_option_where = "option_no IN(238,239)";
    }elseif($sch_option == "475"){
        $chk_option_where = "option_no IN(473,475)";
    }elseif($sch_option == "275"){
        $chk_option_where = "option_no IN(275,276,277)";
    }elseif($sch_option == "100000"){
        $chk_option_list = array(1732,1733,1735,1736,1737,1738,1740,1742,1745,1734,1739,1741,1743,1744);
    }elseif($sch_option == "100001"){
        $chk_option_list = array(1733,1735,1738,1742,1745);
    }elseif($sch_option == "100002"){
        $chk_option_list = array(1732,1736,1737,1734,1739,1740,1741,1743,1744);
    }else{
        $chk_option_where = "option_no = '{$sch_option}'";
    }

    if(!empty($chk_option_where))
    {
        $chk_option_sql   = "SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE {$chk_option_where} AND pcr.display='1'";
        $chk_option_query = mysqli_query($my_db, $chk_option_sql);
        while($chk_option_result = mysqli_fetch_assoc($chk_option_query)){
            $chk_option_list[] = "'{$chk_option_result['prd_no']}'";
        }
    }

    if(!empty($chk_option_list)){
        $chk_option_text = implode(",", $chk_option_list);
        $add_where .= " AND w.prd_no IN({$chk_option_text})";
    }
}

# 정렬기능
$add_orderby    = "w.order_date ASC, w.order_number ASC, w.prd_no ASC";

# 주문번호 뽑아내기
$cms_ord_sql    = "SELECT DISTINCT w.order_number FROM work_cms_reservation w WHERE {$add_where} AND w.order_number is not null ORDER BY {$add_orderby}";
$cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);
$order_number_list  = [];
while($order_number = mysqli_fetch_assoc($cms_ord_query)){
    $order_number_list[] =  "'".$order_number['order_number']."'";
}
$order_numbers   = implode(',', $order_number_list);
$add_where_group = !empty($order_numbers) ? "w.order_number IN({$order_numbers})" : "1!=1";

$cms_reservation_sql = "
    SELECT
        *,
        DATE_FORMAT(w.regdate, '%Y-%m-%d') as reg_date,
        DATE_FORMAT(w.regdate, '%H:%i') as reg_time,
        DATE_FORMAT(w.order_date, '%Y-%m-%d') as ord_date,
        DATE_FORMAT(w.order_date, '%H:%i') as ord_time,
        (SELECT s.s_name FROM staff s WHERE s.s_no=w.s_no) as s_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
        (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no=w.task_req_s_no) as task_req_s_name,
        (SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name
    FROM
    (
        SELECT
            w.w_no,
            w.delivery_state,
            w.regdate,
            w.stock_date,
            w.order_type,
            w.order_date,
            w.order_number,
            w.parent_order_number,
            w.recipient,
            w.recipient_hp,
            w.recipient_hp2,
            w.recipient_addr,
            IF(w.zip_code, CONCAT('[',w.zip_code,']'), '') as postcode,
            w.delivery_memo,
            w.s_no,
            w.log_c_no,
            w.c_name,
            w.prd_no,
            w.task_req,
            w.task_req_s_no,
            w.task_run_s_no,
            w.quantity,
            w.unit_price,
            w.dp_c_no,
            w.dp_c_name,
            w.manager_memo,
            w.notice,
            w.shop_ord_no,
            w.unit_delivery_price,
            w.final_price
        FROM work_cms_reservation w
        WHERE {$add_where_group}
    ) as w
    ORDER BY {$add_orderby}
";
$cms_reservation_query  = mysqli_query($my_db, $cms_reservation_sql);
$delivery_state_list    = getReservationStateOption();
$idx = 2;
$lfcr = chr(10) ;
if(!!$cms_reservation_query)
{
    while($cms_reservation = mysqli_fetch_array($cms_reservation_query))
    {
        $dp_price_vat   = number_format($cms_reservation['dp_price_vat']);
        $delivery_state = $cms_reservation['delivery_state'] ? $delivery_state_list[$cms_reservation['delivery_state']] : "";

        $unit_sql   = "SELECT (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='2809') as sku, pcr.quantity as qty FROM product_cms_relation as pcr WHERE pcr.prd_no ='{$cms_reservation['prd_no']}' AND pcr.display='1'";
        $unit_query = mysqli_query($my_db, $unit_sql);
        $unit_list  = [];
        while($unit_result = mysqli_fetch_assoc($unit_query)){
            $unit_result['qty'] = $cms_reservation['quantity']*$unit_result['qty'];
            $unit_list[] = "{$unit_result['sku']} * {$unit_result['qty']}";
        }
        $unit_text = !empty($unit_list) ? implode($lfcr, $unit_list) : "";

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$idx, $cms_reservation['order_date'])
            ->setCellValueExplicit('B'.$idx, $cms_reservation['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('C'.$idx, $delivery_state)
            ->setCellValue('D'.$idx, $cms_reservation['recipient'])
            ->setCellValue('E'.$idx, $cms_reservation['recipient_hp'])
            ->setCellValueExplicit('F'.$idx, $cms_reservation['postcode'],PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('G'.$idx, $cms_reservation['recipient_addr'])
            ->setCellValueExplicit('H'.$idx, $cms_reservation['delivery_memo'],PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('I'.$idx, $cms_reservation['c_name'])
            ->setCellValue('J'.$idx, $cms_reservation['prd_name'])
            ->setCellValue('K'.$idx, $cms_reservation['quantity'])
            ->setCellValue('L'.$idx, $cms_reservation['task_req'])
            ->setCellValue('M'.$idx, $dp_price_vat)
            ->setCellValue('N'.$idx, $cms_reservation['payment_date'])
            ->setCellValue('O'.$idx, $cms_reservation['dp_c_name'])
            ->setCellValue('P'.$idx, $unit_text)
        ;

        $idx++;
    }
}

$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC0C0C0');
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("A2:P{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:P{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A2:P'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('I2:I'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('J2:J'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('G2:G'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('H2:H'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('L2:L'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('P2:P'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('G2:G'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('H2:H'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('L2:L'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('P2:P'.$idx)->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(40);
$objPHPExcel->getActiveSheet()->getStyle("A1:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->setTitle($excel_title);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_{$excel_title}.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
