<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_upload.php');
require('inc/helper/asset.php');
require('inc/model/Asset.php');
require('inc/model/Kind.php');
require('inc/model/MyCompany.php');
require('inc/model/Staff.php');
include('libs/qrcode/qrlib.php');

# Search URL 정리
$sch_page       = isset($_GET['page']) ? $_GET['page'] : "1";
$sch_page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
$search_url     = "page={$sch_page}&ord_page_type={$sch_page_type}";
$sch_array_key  = array("sch_asset_g1","sch_asset_g2","sch_asset","sch","sch_asset_state","sch_my_c_name","sch_keyword","sch_as_no","sch_management","sch_description","sch_use_type","sch_object_type","sch_share_type","sch_dp_c_name","sch_manufacturer","sch_get_s_date","sch_get_e_date","sch_expire_s_date","sch_expire_e_date","sch_notice","sch_memo");
foreach($sch_array_key as $sch_key)
{
    $sch_val   = isset($_GET[$sch_key]) && !empty($_GET[$sch_key]) ? $_GET[$sch_key]: "";
    if(!!$sch_val)
    {
        $search_url .= "&{$sch_key}={$sch_val}";
    }
}
$smarty->assign("search_url", $search_url);

# Model 처리
$kind_model         = Kind::Factory();
$asset_model        = Asset::Factory();
$my_comp_model      = MyCompany::Factory();
$my_company_list    = $my_comp_model->getList();
$staff_model        = Staff::Factory();
$staff_name_list    = $staff_model->getStaffNameList("s_name", "1");

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$regist_url     = "asset_regist.php";
$list_url       = "asset_management.php";
$asset	        = [];
$asset_g1       = "";
$title		    = "등록하기";
$is_button      = true;

if($process == 'new_asset') # 생성
{
    // 필수정보
    $search_url   = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $asset_state  = isset($_POST['asset_state']) ? $_POST['asset_state'] : "";
    $my_c_no      = isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "";
    $k_name_code  = isset($_POST['asset_g2']) ? sprintf('%05d', $_POST['asset_g2']) : "";
    $asset_name   = isset($_POST['asset_name']) ? addslashes($_POST['asset_name']) : "";
    $use_type     = isset($_POST['use_type']) ? $_POST['use_type'] : "";
    $object_type  = isset($_POST['object_type']) ? $_POST['object_type'] : "";
    $share_type   = isset($_POST['share_type']) ? $_POST['share_type'] : "";
    $keyword      = isset($_POST['keyword']) && !empty($_POST['keyword']) ? implode(',', $_POST['keyword']) : "";
    $reg_date     = date("Y-m-d H:i:s");
    $management   = "";
    $manager        = isset($_POST['manager']) ? addslashes($_POST['manager']) : "";
    $manager_staff  = $staff_model->getStaff($manager);
    $manager_name   = $manager_staff['s_name'];

    if(!!$my_c_no){
        $my_c_ini = isset($my_company_list[$my_c_no]) ? $my_company_list[$my_c_no]['initial']."_0" : "";

        if(!!$my_c_ini){
            $management_sql    = "SELECT REPLACE(management, '{$my_c_ini}', '') as manage_no FROM asset WHERE management like '{$my_c_ini}%' ORDER BY management DESC LIMIT 1";
            $management_query  = mysqli_query($my_db, $management_sql);
            $management_result = mysqli_fetch_assoc($management_query);

            $last_manage_no    = (isset($management_result['manage_no']) && !empty($management_result['manage_no'])) ? (int)$management_result['manage_no'] : 0;
            $management        = $my_c_ini.sprintf('%04d', $last_manage_no+1);
        }
    }

    # 필수정보 체크 및 저장
    if(empty($asset_state) || empty($my_c_no) || empty($k_name_code) || empty($asset_name) || empty($manager) || empty($use_type) || empty($object_type) || empty($share_type) || empty($management))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');location.href='{$regist_url}?{$search_url}';</script>");
    }

    $sub_manager        = isset($_POST['sub_manager']) ? addslashes($_POST['sub_manager']) : "";
    $add_manager_column = "";

    if(!empty($sub_manager)){
        $sub_manager_staff  = $staff_model->getStaff($sub_manager);
        $sub_manager_name   = $sub_manager_staff['s_name'];
        $add_manager_column = ", sub_manager='{$sub_manager}', sub_manager_name='{$sub_manager_name}'";
    }else{
        $add_manager_column = ", sub_manager=NULL, sub_manager_name=NULL";
    }

    $ins_sql = "
        INSERT INTO asset SET
            `asset_state`   = '{$asset_state}',
            `my_c_no`       = '{$my_c_no}',
            `k_name_code`   = '{$k_name_code}',
            `name`          = '{$asset_name}',
            `manager`       = '{$manager}',
            `manager_name`  = '{$manager_name}',
            `use_type`      = '{$use_type}',
            `object_type`   = '{$object_type}',
            `share_type`    = '{$share_type}',
            `management`    = '{$management}',
            `keyword`       = '{$keyword}',
            `regdate`       = '{$reg_date}'
             {$add_manager_column}
    ";

    # 이미지 체크 및 저장
    $img_names = isset($_POST['img_name']) ? $_POST['img_name'] : "";
    $img_paths = isset($_POST['img_path']) ? $_POST['img_path'] : "";

    $move_img_name = "";
    $move_img_path = "";
    if(!empty($img_names) && !empty($img_paths))
    {
        $move_img_paths = move_store_files($img_paths, "dropzone_tmp", "asset");
        $move_img_name  = implode(',', $img_names);
        $move_img_path  = implode(',', $move_img_paths);
    }

    if(!empty($move_img_path) && !empty($move_img_name)){
        $ins_sql .= ", img_path='{$move_img_path}', img_name='{$move_img_name}'";
    }

    # 첨부파일 체크 및 저장
    $file_names = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_paths = isset($_POST['file_path']) ? $_POST['file_path'] : "";

    $move_file_name = "";
    $move_file_path = "";
    if(!empty($file_names) && !empty($file_paths))
    {
        $move_file_paths = move_store_files($file_paths, "dropzone_tmp", "asset");
        $move_file_name  = implode(',', $file_names);
        $move_file_path  = implode(',', $move_file_paths);
    }

    if(!empty($move_file_path) && !empty($move_file_name)){
        $ins_sql .= ", file_path='{$move_file_path}', file_name='{$move_file_name}'";
    }

    # 세부정보 저장
    $description  = isset($_POST['description']) ? addslashes($_POST['description']) : "";
    $form         = isset($_POST['form']) ? addslashes($_POST['form']) : "";
    $non_description = isset($_POST['non_description']) ? addslashes($_POST['non_description']) : "";
    $object_site = isset($_POST['object_site']) ? addslashes($_POST['object_site']) : "";
    $object_url  = isset($_POST['object_url']) ? addslashes($_POST['object_url']) : "";
    $object_id   = isset($_POST['object_id']) ? addslashes($_POST['object_id']) : "";
    $object_pw   = isset($_POST['object_pw']) ? addslashes($_POST['object_pw']) : "";
    $dp_c_name   = isset($_POST['dp_c_name']) ? addslashes($_POST['dp_c_name']) : "";
    $manufacturer= isset($_POST['manufacturer']) ? addslashes($_POST['manufacturer']) : "";
    $get_date    = isset($_POST['get_date']) ? $_POST['get_date'] : "";
    $expire_date = isset($_POST['expire_date']) ? $_POST['expire_date'] : "";
    $cost        = isset($_POST['cost']) ? $_POST['cost'] : "";
    $notice      = isset($_POST['notice']) ? addslashes($_POST['notice']) : "";
    $asset_loc   = isset($_POST['asset_loc']) ? addslashes($_POST['asset_loc']) : "";

    if(!empty($description)){
        $ins_sql .= ", description='{$description}'";
    }

    if(!empty($form)){
        $ins_sql .= ", form='{$form}'";
    }

    if(!empty($non_description)){
        $ins_sql .= ", non_description='{$non_description}'";
    }

    if(!empty($object_site)){
        $ins_sql .= ", object_site='{$object_site}'";
    }

    if(!empty($object_url)){
        $ins_sql .= ", object_url='{$object_url}'";
    }

    if(!empty($object_id)){
        $ins_sql .= ", object_id='{$object_id}'";
    }

    if(!empty($object_pw)){
        $ins_sql .= ", object_pw='{$object_pw}'";
    }

    if(!empty($dp_c_name)){
        $ins_sql .= ", dp_c_name='{$dp_c_name}'";
    }

    if(!empty($manufacturer)){
        $ins_sql .= ", manufacturer='{$manufacturer}'";
    }

    if(!empty($get_date)){
        $ins_sql .= ", get_date='{$get_date}'";
    }

    if(!empty($expire_date)){
        $ins_sql .= ", expire_date='{$expire_date}'";
    }

    if(!empty($cost)){
        $cost     = str_replace(',','',$cost);
        $ins_sql .= ", cost='{$cost}'";
    }

    if(!empty($notice)){
        $ins_sql .= ", notice='{$notice}'";
    }

    if(!empty($asset_loc)){
        $ins_sql .= ", asset_loc='{$asset_loc}'";
    }

    if(isset($_POST['last_pw_date']))
    {
        if(!empty($_POST['last_pw_date'])){
            $ins_sql .= ", last_pw_date='{$_POST['last_pw_date']}'";
        }else{
            $ins_sql .= ", last_pw_date=NULL";
        }
    }

    if(isset($_POST['pw_date']))
    {
        if(!empty($_POST['pw_date'])){
            $ins_sql .= ", pw_date='{$_POST['pw_date']}', pw_term='{$_POST['pw_term']}'";
        }else{
            $ins_sql .= ", pw_date=NULL, pw_term=NULL";
        }
    }

    if(mysqli_query($my_db, $ins_sql))
    {
        $as_no = mysqli_insert_id($my_db);

        if($as_no)
        {
            $asset_g1_val   = isset($_POST['asset_g1']) ? sprintf('%05d', $_POST['asset_g1']) : "";
            $qr_content     = "https://work.wplanet.co.kr/v1/asset_reservation.php?sch_management={$management}&sch_qrcode=Y";
            $fileName       = "asset_qr_code_{$management}.png";
            $absolute_path  = "uploads/qr_code/{$fileName}";

            QRcode::png($qr_content, $absolute_path,'L', 10, 2);

            $upd_qrcode_sql = "UPDATE asset SET qr_code='{$absolute_path}' WHERE as_no='{$as_no}'";
            mysqli_query($my_db, $upd_qrcode_sql);
        }

        exit("<script>alert('자산관리 등록에 성공했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }else{
        exit("<script>alert('자산관리 등록에 실패했습니다');location.href='{$regist_url}?{$search_url}';</script>");
    }
}
elseif($process == 'modify_asset') # 수정
{
    $modify_key = array('search_url', 'as_no', 'asset_state', 'my_c_no', 'asset_g2', 'asset_name', 'manager', 'description', 'use_type', 'object_type', 'share_type', 'asset_loc', 'non_description', 'object_site', 'object_url', 'object_id', 'object_pw', 'dp_c_name', 'manufacturer', 'get_date', 'expire_date', 'cost', 'notice','use_person','old_use_person');
    $modify_var = array_fill_keys($modify_key, "");
    foreach ($modify_var as $key => $val) {
        $modify_var[$key] = $_POST[$key];
    }
    extract($modify_var);

    # 필수정보 체크 및 저장
    if(empty($as_no) || empty($asset_state) || empty($my_c_no) || empty($asset_g2) || empty($asset_name) || empty($manager) || empty($use_type) || empty($object_type) || empty($share_type))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 수정해 주세요.');location.href='{$regist_url}?as_no={$as_no}&{$search_url}';</script>");
    }

    # 매니저 체크크
    $manager_staff      = $staff_model->getStaff($manager);
    $manager_name       = $manager_staff['s_name'];

    $sub_manager        = isset($_POST['sub_manager']) ? addslashes($_POST['sub_manager']) : "";
    $add_manager_column = "";
    if(!empty($sub_manager)){
        $sub_manager_staff  = $staff_model->getStaff($sub_manager);
        $sub_manager_name   = $sub_manager_staff['s_name'];
        $add_manager_column = ", sub_manager='{$sub_manager}', sub_manager_name='{$sub_manager_name}'";
    }else{
        $add_manager_column = ", sub_manager=NULL, sub_manager_name=NULL";
    }

    $keyword = isset($_POST['keyword']) && !empty($_POST['keyword']) ? implode(',', $_POST['keyword']) : "";

    $upd_sql = "
        UPDATE asset SET
            `asset_state`   = '{$asset_state}',
            `my_c_no`       = '{$my_c_no}',
            `k_name_code`   = '{$asset_g2}',
            `name`          = '{$asset_name}',
            `manager`       = '{$manager}',
            `manager_name`  = '{$manager_name}',
            `use_type`      = '{$use_type}',
            `object_type`   = '{$object_type}',
            `keyword`       = '{$keyword}',
            `share_type`    = '{$share_type}'
            {$add_manager_column}
    ";

    # 이미지 체크 및 저장
    $img_origin_path = isset($_POST['img_origin_path']) ? $_POST['img_origin_path'] : "";
    $img_origin_name = isset($_POST['img_origin_name']) ? $_POST['img_origin_name'] : "";
    $img_paths       = isset($_POST['img_path']) ? $_POST['img_path'] : "";
    $img_names       = isset($_POST['img_name']) ? $_POST['img_name'] : "";
    $img_chk         = isset($_POST['img_chk']) ? $_POST['img_chk'] : "";

    $move_img_path = "";
    $move_img_name = "";
    if($img_chk)
    {
        if(!empty($img_paths) && !empty($img_names))
        {
            $move_img_paths = move_store_files($img_paths, "dropzone_tmp", "asset");
            $move_img_name  = implode(',', $img_names);
            $move_img_path  = implode(',', $move_img_paths);
        }

        if(!empty($img_origin_path) && !empty($img_origin_name))
        {
            if(!empty($img_paths) && !empty($img_names)){
                $del_images = array_diff(explode(',', $img_origin_path), $img_paths);
            }else{
                $del_images = explode(',', $img_origin_path);
            }

            if(!empty($del_images))
            {
                del_files($del_images);
            }
        }

        if(!empty($move_img_path) && !empty($move_img_name)){
            $upd_sql .= ", img_path='{$move_img_path}', img_name='{$move_img_name}'";
        }else{
            $upd_sql .= ", img_path='', img_name=''";
        }
    }

    # 이미지 체크 및 저장
    $file_origin_path   = isset($_POST['file_origin_path']) ? $_POST['file_origin_path'] : "";
    $file_origin_name   = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_names         = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_paths         = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_chk           = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    $move_file_path = "";
    $move_file_name = "";
    if($file_chk)
    {
        if(!empty($file_paths) && !empty($file_names))
        {
            $move_file_paths = move_store_files($file_paths, "dropzone_tmp", "asset");
            $move_file_name  = implode(',', $file_names);
            $move_file_path  = implode(',', $move_file_paths);
        }

        if(!empty($file_origin_path) && !empty($file_origin_name))
        {
            if(!empty($file_paths) && !empty($file_names)){
                $del_files = array_diff(explode(',', $file_origin_path), $file_paths);
            }else{
                $del_files = explode(',', $file_origin_path);
            }

            if(!empty($del_files))
            {
                del_files($del_files);
            }
        }

        if(!empty($move_file_path) && !empty($move_file_name)){
            $upd_sql .= ", file_path='{$move_file_path}', file_name='{$move_file_name}'";
        }else{
            $upd_sql .= ", file_path='', file_name=''";
        }
    }

    # 세부정보 저장
    if(!empty($description)){
        $upd_sql .= ", description='{$description}'";
    }

    if(!empty($form)){
        $upd_sql .= ", form='{$form}'";
    }

    if(!empty($non_description)){
        $upd_sql .= ", non_description='{$non_description}'";
    }

    if(!empty($object_site)){
        $upd_sql .= ", object_site='{$object_site}'";
    }else{
        $upd_sql .= ", object_site=NULL";
    }

    if(!empty($object_url)){
        $upd_sql .= ", object_url='{$object_url}'";
    }else{
        $upd_sql .= ", object_url=NULL";
    }

    $is_editable = isset($_POST['is_editable']) ? $_POST['is_editable'] : "";
    if($is_editable == '1')
    {
        if(!empty($object_id)){
            $upd_sql .= ", object_id='{$object_id}'";
        }else{
            $upd_sql .= ", object_id=NULL";
        }

        if(!empty($object_pw)){
            $upd_sql .= ", object_pw='{$object_pw}'";
        }else{
            $upd_sql .= ", object_pw=NULL";
        }
    }

    if(!empty($get_date)){
        $upd_sql .= ", get_date='{$get_date}'";
    }else{
        $upd_sql .= ", get_date=NULL";
    }

    if(!empty($expire_date)){
        $upd_sql .= ", expire_date='{$expire_date}'";
    }else{
        $upd_sql .= ", expire_date=NULL";
    }

    $upd_sql .= ", dp_c_name='{$dp_c_name}'";
    $upd_sql .= ", manufacturer='{$manufacturer}'";
    $cost     = str_replace(',','',$cost);
    $upd_sql .= ", cost='{$cost}'";
    $upd_sql .= ", notice='{$notice}'";
    $upd_sql .= ", asset_loc='{$asset_loc}'";

    if(isset($_POST['pw_date']))
    {
        if(!empty($_POST['pw_date'])){
            $upd_sql .= ", pw_date='{$_POST['pw_date']}', pw_term='{$_POST['pw_term']}'";
        }else{
            $upd_sql .= ", pw_date=NULL, pw_term=NULL";
        }
    }

    if(isset($_POST['last_pw_date']))
    {
        if(!empty($_POST['last_pw_date'])){
            $upd_sql .= ", last_pw_date='{$_POST['last_pw_date']}'";
        }else{
            $upd_sql .= ", last_pw_date=NULL";
        }
    }

    $upd_sql .= " WHERE as_no = '{$as_no}'";
    
    if(mysqli_query($my_db, $upd_sql))
    {
        $old_person      = $_POST['old_use_person'];
        $use_person_list = isset($_POST['use_person']) && !empty($_POST['use_person']) ? $_POST['use_person'] : [];

        if(!empty($old_person) || !empty($use_person_list))
        {
            $old_person_list = !empty($old_person) ? explode(',', $old_person) : [];
            $add_person_list = array_diff($use_person_list, $old_person_list);
            $del_person_list = array_diff($old_person_list, $use_person_list);

            if($share_type == '1' && count($use_person_list) > 1){
                exit("<script>alert('자산관리 수정에 성공했습니다. 개별타입 사용자 등록은 한명만 적용시켜주세요'); location.href='{$regist_url}?as_no={$as_no}&{$search_url}';</script>");
            }

            if(!empty($del_person_list))
            {
                foreach($del_person_list as $del_person)
                {
                    $del_person_no      = "";
                    $del_staff_sql      = "SELECT s_no, s_name FROM staff WHERE s_name='{$del_person}' limit 1";
                    $del_staff_query    = mysqli_query($my_db, $del_staff_sql);
                    $del_staff_result   = mysqli_fetch_assoc($del_staff_query);

                    if(empty($del_staff_result)){
                        continue;
                    }else{
                        $del_person_no = $del_staff_result['s_no'];
                    }

                    if(!empty($del_person_no))
                    {
                        $cur_date = date('Y-m-d H:i:s');
                        $dup_sql = "INSERT INTO asset_reservation(`work_state`, `as_no`, `management`, `manager`, `sub_manager`, `regdate`, `req_no`, `req_name`, `req_date`, `run_no`, `run_name`, `run_date`) (SELECT '9', '{$as_no}', management, manager, `sub_manager`, '{$cur_date}', '{$del_person_no}', '{$del_person}', '{$cur_date}', '{$session_s_no}', '{$session_name}', '{$cur_date}' FROM asset_reservation WHERE as_no='{$as_no}' AND work_state='2' AND req_no='{$del_person_no}' AND req_name='{$del_person}')";
                        mysqli_query($my_db, $dup_sql);

                        $dup_upd_sql = "UPDATE `asset_reservation` SET work_state='10' WHERE as_no='{$as_no}' AND work_state='2' AND req_no='{$del_person_no}' AND req_name='{$del_person}'";
                        mysqli_query($my_db, $dup_upd_sql);
                    }
                }
            }

            if(!empty($add_person_list))
            {
                foreach($add_person_list as $add_person)
                {
                    $add_person_no      = "";
                    $add_staff_sql      = "SELECT s_no, s_name FROM staff WHERE s_name='{$add_person}' limit 1";
                    $add_staff_query    = mysqli_query($my_db, $add_staff_sql);
                    $add_staff_result   = mysqli_fetch_assoc($add_staff_query);

                    if(empty($add_staff_result)){
                        continue;
                    }else{
                        $add_person_no = $add_staff_result['s_no'];
                    }

                    if(!empty($add_person_no))
                    {
                        $ins_sql = "INSERT INTO `asset_reservation` SET as_no='{$as_no}', work_state='2', management='{$management}', manager='{$manager}', sub_manager='{$sub_manager}', regdate=now(), req_no='{$add_person_no}', req_name='{$add_person}', req_date=now(), run_date=now(), run_no='{$manager}', run_name='{$manager_name}'";
                        if(mysqli_query($my_db, $ins_sql) && $share_type == '1'){
                            $upd_state_sql = "UPDATE asset SET asset_state='5' WHERE as_no='{$as_no}'";
                            mysqli_query($my_db, $upd_state_sql);
                        }
                    }
                }
            }
        }

        exit("<script>alert('자산관리 수정에 성공했습니다');location.href='{$regist_url}?as_no={$as_no}&{$search_url}';</script>");
    }else{
        exit("<script>alert('자산관리 수정에 실패했습니다');location.href='{$regist_url}?as_no={$as_no}&{$search_url}';</script>");
    }
}
elseif($process == 'duplicate_asset') # 복제
{
    $as_no      = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $my_c_no    = isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(!$as_no) {
        exit("<script>alert('자산관리 복제에 실패했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }

    $dup_management = "";
    $dup_asset_sql  = "SELECT *, (SELECT k.k_parent FROM kind k WHERE k.k_name_code = `as`.k_name_code) as asset_g1 FROM asset `as` WHERE `as`.as_no = {$as_no} LIMIT 1";
    $dup_asset_query= mysqli_query($my_db, $dup_asset_sql);
    $asset 		    = mysqli_fetch_assoc($dup_asset_query);
    $asset['cost']          = !empty($asset['cost']) ? number_format($asset['cost']) : "";
    $asset['management']    = $asset['notice'] = $asset['memo'] = $asset['as_no'] = $asset['qr_code'] = "";
    $asset['manager']       = $session_s_no;
    $asset['manager_name']  = $session_name;
    $asset_g1               = isset($asset['asset_g1']) ? sprintf('%05d', $asset['asset_g1']) : "";
}
elseif($process == 'asset_qr_code') # QR 코드 생성
{
    $as_no_val      = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $management_val = isset($_POST['management']) ? $_POST['management'] : "";
    $asset_g1_val   = isset($_POST['asset_g1']) ? sprintf('%05d', $_POST['asset_g1']) : "";
    $asset_g2_val   = isset($_POST['asset_g2']) ? sprintf('%05d', $_POST['asset_g2']) : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if($as_no_val)
    {
        $qr_content     = "https://work.wplanet.co.kr/v1/asset_reservation.php?sch_management={$management_val}&sch_qrcode=Y";
        $fileName       = "asset_qr_code_{$management_val}.png";
        $absolute_path  = "uploads/qr_code/{$fileName}";

        QRcode::png($qr_content, $absolute_path,'L', 10, 2);

        $upd_qrcode_sql = "UPDATE asset SET qr_code='{$absolute_path}' WHERE as_no='{$as_no_val}'";
        if(mysqli_query($my_db, $upd_qrcode_sql))
        {
            exit("<script>alert('QR코드 생성에 성공했습니다');location.href='{$regist_url}?as_no={$as_no_val}&{$search_url}';</script>");
        }else{
            exit("<script>alert('QR코드 생성에 실패했습니다');location.href='{$regist_url}?as_no={$as_no_val}&{$search_url}';</script>");
        }

    }
}
elseif($process == 'asset_memo') # 관리자 메모
{
    $as_no      = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $memo       = isset($_POST['memo']) ? $_POST['memo'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_sql = "UPDATE asset SET `memo`='{$memo}' WHERE as_no='{$as_no}'";

    if(mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('관리자메모 수정에 성공했습니다');location.href='{$regist_url}?as_no={$as_no}&{$search_url}';</script>");
    }else{
        exit("<script>alert('관리자메모 수정에 실패했습니다');location.href='{$regist_url}?as_no={$as_no}&{$search_url}';</script>");
    }
}
elseif($process == 'asset_pw_msg') # 비밀번호 변경 알림
{
    $as_no      = isset($_POST['as_no']) ? $_POST['as_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $upd_sql    = "UPDATE asset SET `last_pw_date`=now() WHERE as_no='{$as_no}'";

    if(mysqli_query($my_db, $upd_sql))
    {
        $asset_item         = $asset_model->getItem($as_no);
        $reservation_sql    = "SELECT DISTINCT req_no FROM asset_reservation WHERE as_no='{$as_no}' AND work_state='2'";
        $reservation_query  = mysqli_query($my_db, $reservation_sql);
        $pw_url 	        = "https://work.wplanet.co.kr/v1/asset_regist.php?as_no={$as_no}";
        $message 	        = "[비밀번호 변경] `{$asset_item['name']}`의 비밀번호가 변경되었습니다.\r\n{$pw_url}";
        $asset_pw_msg       = addslashes($message);
        while($reservation = mysqli_fetch_assoc($reservation_query))
        {
            $chat_ins_sql   = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$reservation['req_no']}', content='{$asset_pw_msg}', alert_type='56', alert_check='ASSET_PW_{$as_no}', regdate=now()";
            mysqli_query($my_db, $chat_ins_sql);
        }

        exit("<script>alert('비밀번호 변경 알림톡을 보냈습니다.');location.href='{$regist_url}?as_no={$as_no}&{$search_url}';</script>");
    }else{
        exit("<script>alert('비밀번호 변경 알림톡 발송 실패했습니다');location.href='{$regist_url}?as_no={$as_no}&{$search_url}';</script>");
    }
}

# 등록/수정
$as_no      = isset($_GET['as_no']) ? $_GET['as_no'] : "";
$is_button  = true;

if($as_no)
{
    $asset_sql 	 = "SELECT *, (SELECT k.k_parent FROM kind k WHERE k.k_name_code = `as`.k_name_code) as asset_g1 FROM asset `as` WHERE `as`.as_no = {$as_no} LIMIT 1";
    $asset_query = mysqli_query($my_db, $asset_sql);
    $asset 		 = mysqli_fetch_assoc($asset_query);
    $asset_g1    = isset($asset['asset_g1']) ? sprintf('%05d', $asset['asset_g1']) : "";
    $asset['cost'] = !empty($asset['cost']) ? number_format($asset['cost']) : "";
    $img_paths = $asset['img_path'];
    $img_names = $asset['img_name'];
    $old_use_person = "";

    if(!empty($img_paths) && !empty($img_names))
    {
        $asset['img_paths'] = explode(',', $img_paths);
        $asset['img_names'] = explode(',', $img_names);
        $asset['img_origin_path'] = $img_paths;
        $asset['img_origin_name'] = $img_names;
    }

    $file_paths = $asset['file_path'];
    $file_names = $asset['file_name'];
    if(!empty($file_paths) && !empty($file_names))
    {
        $asset['file_paths'] = explode(',', $file_paths);
        $asset['file_names'] = explode(',', $file_names);
        $asset['file_origin_path'] = $file_paths;
        $asset['file_origin_name'] = $file_names;
    }

    $title = "수정하기";

    $asset_use_person = $asset_use_person_list = [];
    $asset_use_person_sql = "SELECT DISTINCT req_name FROM asset_reservation WHERE as_no='{$as_no}' AND work_state='2'";
    $asset_use_person_query = mysqli_query($my_db, $asset_use_person_sql);
    while($asset_use = mysqli_fetch_assoc($asset_use_person_query))
    {
        $asset_use_person_list[] = $asset_use['req_name'];
    }

    if(!empty($asset_use_person_list)){
        $old_use_person = implode(',', $asset_use_person_list);
    }

    $asset['show_permission'] = '1';
    if($asset['manager'] == $session_s_no || $asset['sub_manager'] == $session_s_no) {
        $asset['is_editable']       = 1;
    }
    else
    {
        if(($asset['use_type'] == '1' && $asset['object_type'] == '3') && !in_array($session_name, $asset_use_person_list)){
             $asset['show_permission'] = '2';
        }
    }

    $smarty->assign('old_use_person', $old_use_person);
    $smarty->assign('asset_use_person_list', $asset_use_person_list);

    $is_button = (permissionNameCheck($session_permission, "재무관리자") || permissionNameCheck($session_permission, "마스터관리자") || $session_s_no == $asset['manager'] || $session_s_no == $asset['sub_manager']) ? true : false;
}else{
    $asset['is_editable'] = 1;
}

# 자산 그룹리스트 처리
$kind_model       = Kind::Factory();
$asset_group_list = $kind_model->getKindGroupList("asset");
$asset_g1_list = $asset_g2_list = [];

foreach($asset_group_list as $key => $asset_data)
{
    if(!$key){
        $asset_g1_list = $asset_data;
    }else{
        $asset_g2_list[$key] = $asset_data;
    }
}
$asset_g2_list = isset($asset_g2_list[$asset_g1]) ? $asset_g2_list[$asset_g1] : [];

$smarty->assign("asset_g1_list", $asset_g1_list);
$smarty->assign("asset_g2_list", $asset_g2_list);

if(!isset($asset['asset_state'])){
    $asset['asset_state'] = '1';
}

if(!isset($asset['my_c_no'])){
    $asset['my_c_no'] = '1';
}

if(!isset($asset['manager'])){
    $asset['manager'] = $session_s_no;
}

if(!isset($asset['manager_name'])){
    $asset['manager_name'] = $session_name;
}

if(!isset($asset['img_paths']) || !isset($asset['img_paths']))
{
    $asset['img_paths'] = "";
    $asset['img_names'] = "";
}
$img_count = !empty($asset['img_paths']) ? count($asset['img_paths']) : 0;
$smarty->assign("img_count", $img_count);

if(!isset($asset['file_paths']) || !isset($asset['file_paths']))
{
    $asset['file_paths'] = "";
    $asset['file_names'] = "";
}
$file_count = !empty($asset['file_paths']) ? count($asset['file_paths']) : 0;
$smarty->assign("file_count", $file_count);

$smarty->assign('my_company_list', $my_company_list);
$smarty->assign('staff_name_list', $staff_name_list);
$smarty->assign('asset_state_option', getAssetStateOption());
$smarty->assign('asset_use_type_option', getAssetUseTypeOption());
$smarty->assign('asset_share_type_option', getAssetShareTypeOption());
$smarty->assign('asset_object_type_option', getAssetObjectTypeOption());
$smarty->assign('term_option', getTermOption());
$smarty->assign("title", $title);
$smarty->assign("is_button", $is_button);
$smarty->assign($asset);

$smarty->display('asset_regist.html');
?>
