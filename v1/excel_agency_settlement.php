<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/agency.php');
require('inc/helper/work.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

$lfcr = chr(10);
$work_sheet = $objPHPExcel->setActiveSheetIndex(0);

$work_sheet->mergeCells("A1:A2");
$work_sheet->mergeCells("B1:B2");
$work_sheet->mergeCells("C1:C2");
$work_sheet->mergeCells("D1:D2");
$work_sheet->mergeCells("E1:E2");
$work_sheet->mergeCells("F1:F2");
$work_sheet->mergeCells("G1:P1");
$work_sheet->mergeCells("Q1:Q2");
$work_sheet->mergeCells("R1:R2");
$work_sheet->mergeCells("S1:S2");
$work_sheet->mergeCells("T1:T2");
$work_sheet->mergeCells("U1:U2");
$work_sheet->mergeCells("V1:V2");
$work_sheet->mergeCells("W1:W2");
$work_sheet->mergeCells("X1:X2");
$work_sheet->mergeCells("Y1:Y2");


//상단타이틀
$work_sheet
	->setCellValue('A1', "No")
	->setCellValue('B1', "작성월")
	->setCellValue('C1', "작성일")
	->setCellValue('D1', "진행상태")
	->setCellValue('E1', "업무완료일")
	->setCellValue('F1', "w_no")
	->setCellValue('G1', "매체 대행사 수수료 정산내역")
	->setCellValue('G2', "매체대행사")
	->setCellValue('H2', "업체")
	->setCellValue('I2', "광고매체")
	->setCellValue('J2', "상품명")
	->setCellValue('K2', "상품상세")
	->setCellValue('L2', "광고주명")
	->setCellValue('M2', "광고주Key")
	->setCellValue('N2', "매체(공급가액){$lfcr}VAT별도")
	->setCellValue('O2', "수수료(%)")
	->setCellValue('P2', "수수료(공급가액){$lfcr}VAT별도")
	->setCellValue('Q1', "사업자")
	->setCellValue('R1', "업체명")
	->setCellValue('S1', "부서")
	->setCellValue('T1', "담당자")
	->setCellValue('U1', "출금액 VAT별도{$lfcr}(광고비)")
	->setCellValue('V1', "입금액 VAT별도{$lfcr}(대행수수료)")
	->setCellValue('W1', "업무진행")
	->setCellValue('X1', "업무처리자")
;

# 기본 변수 값 설정
$prev_settle_month  = date('Y-m', strtotime("-1 months"));

# 검색조건
$add_where          = "1=1";
$sch_settle_month   = isset($_GET['sch_settle_month']) ? $_GET['sch_settle_month'] : $prev_settle_month;
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_status         = isset($_GET['sch_status']) ? $_GET['sch_status'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_media          = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_is_partner     = isset($_GET['sch_is_partner']) ? $_GET['sch_is_partner'] : "";
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_partner_name   = isset($_GET['sch_partner_name']) ? $_GET['sch_partner_name'] : "";
$sch_manager_team   = isset($_GET['sch_manager_team']) ? $_GET['sch_manager_team'] : $session_team;
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_run_name       = isset($_GET['sch_run_name']) ? $_GET['sch_run_name'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ori_ord_type       = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "";

if(!empty($sch_settle_month))
{
    $add_where .= " AND `as`.settle_month = '{$sch_settle_month}'";
}

if(!empty($sch_no))
{
    $add_where .= " AND `as`.as_no = '{$sch_no}'";
}

if(!empty($sch_status)){
    $add_where   .= " AND `as`.status = '{$sch_status}'";
}

if(!empty($sch_agency)){
    $add_where   .= " AND `as`.agency = '{$sch_agency}'";
}

if(!empty($sch_media)){
    $add_where   .= " AND `as`.media = '{$sch_media}'";
}

if(!empty($sch_is_partner))
{
    if($sch_is_partner == '1'){
        $add_where   .= " AND `as`.partner > 0";
    }elseif($sch_is_partner == '2'){
        $add_where   .= " AND (`as`.partner is null OR `as`.partner < 1)";
    }
}

if(!empty($sch_my_c_no)){
    $add_where   .= " AND `as`.my_c_no = '{$sch_my_c_no}'";
}

if(!empty($sch_partner_name)){
    $add_where   .= " AND `as`.partner_name LIKE '%{$sch_partner_name}%'";
}

if (!empty($sch_manager_team))
{
    if ($sch_manager_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_manager_team);
        $add_where 		 .= " AND `as`.manager_team IN({$sch_team_code_where})";
    }
}

if (!empty($sch_manager))
{
    if ($sch_manager != "all") {
        $add_where 		 .= " AND `as`.manager='{$sch_manager}'";
    }
}

if(!empty($sch_run_name)){
    $add_where   .= " AND `as`.run_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_run_name}%')";
}

# 브랜드 검색
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

if(!empty($sch_brand)) {
    $add_where      .= " AND `as`.partner = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $add_where      .= " AND `as`.partner IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $add_where      .= " AND `as`.partner IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$add_order_by = "";
$ord_type_by  = "";
$order_by_val = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

    if(!empty($ord_type_by))
    {
        $ord_type_list = [];
        if($ord_type == 'team_manager'){
            $ord_type_list[] = "team_manager";
        }

        if($ori_ord_type == $ord_type)
        {
            if($ord_type_by == '1'){
                $order_by_val = "ASC";
            }elseif($ord_type_by == '2'){
                $order_by_val = "DESC";
            }
        }else{
            $ord_type_by  = '2';
            $order_by_val = "DESC";
        }

        foreach($ord_type_list as $ord_type_val){

            if($ord_type_val == 'team_manager'){
                $add_order_by .= "team_priority {$order_by_val}, manager_name ASC, ";
            }else{
                $add_order_by .= "{$ord_type_val} {$order_by_val}, ";
            }
        }
    }
}
$add_order_by .= "group_no DESC";

# 대행수수료 그룹번호
$agency_group_sql   = "SELECT DISTINCT group_no, (SELECT t.priority FROM team t WHERE t.team_code=`as`.manager_team) AS team_priority, (SELECT s_name FROM staff s WHERE s.s_no=`as`.manager) AS manager_name FROM agency_settlement `as` WHERE {$add_where} ORDER BY {$add_order_by}";
$agency_group_query = mysqli_query($my_db, $agency_group_sql);
$add_group_where_list = [];
while($add_group = mysqli_fetch_assoc($agency_group_query))
{
    $add_group_where_list[] = "'{$add_group['group_no']}'";
}
$add_group_where     = implode(",", $add_group_where_list);

# 리스트 데이터
$agency_settle_sql = "
    SELECT 
        *,
        DATE_FORMAT(`as`.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(`as`.regdate, '%H:%i') as reg_hour,
        (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=`as`.my_c_no) AS my_c_name,
        (SELECT t.priority FROM team t WHERE t.team_code=`as`.manager_team) AS team_priority,
        (SELECT t.team_name FROM team t WHERE t.team_code=`as`.manager_team) AS manager_team_name,
        (SELECT s_name FROM staff s WHERE s.s_no=`as`.manager) AS manager_name,
        (SELECT s_name FROM staff s WHERE s.s_no=`as`.run_s_no) AS run_name
    FROM agency_settlement `as`
    WHERE group_no IN({$add_group_where})
    ORDER BY {$add_order_by} 
";

$idx = 3;
$agency_settle_query = mysqli_query($my_db, $agency_settle_sql);
$status_option       = getWorkStateOption();
$agency_option       = getSettleAgencyOption();
$media_option        = getAdvertiseMediaOption();
while($agency_settle = mysqli_fetch_assoc($agency_settle_query))
{
    $status_name = $status_option[$agency_settle['status']];
    $agency_settle['agency_name'] = $agency_option[$agency_settle['agency']];
    $agency_settle['media_name']  = $media_option[$agency_settle['media']];

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$idx}", $agency_settle['as_no'])
        ->setCellValue("B{$idx}", $agency_settle['settle_month'])
        ->setCellValue("C{$idx}", $agency_settle['reg_day'].$lfcr.$agency_settle['reg_hour'])
        ->setCellValue("D{$idx}", $status_name)
        ->setCellValue("E{$idx}", $agency_settle['run_date'])
        ->setCellValue("F{$idx}", $agency_settle['w_no'])
        ->setCellValue("G{$idx}", $agency_settle['agency_name'])
        ->setCellValue("H{$idx}", $agency_settle['company'])
        ->setCellValue("I{$idx}", $agency_settle['media_name'])
        ->setCellValue("J{$idx}", $agency_settle['prd_name'])
        ->setCellValue("K{$idx}", $agency_settle['prd_detail'])
        ->setCellValue("L{$idx}", $agency_settle['advertiser_name'])
        ->setCellValue("M{$idx}", $agency_settle['advertiser_id'])
        ->setCellValue("N{$idx}", number_format($agency_settle['supply_price'], 0))
        ->setCellValue("O{$idx}", number_format($agency_settle['supply_fee_per'], 2))
        ->setCellValue("P{$idx}", number_format($agency_settle['supply_fee'], 0))
        ->setCellValue("Q{$idx}", $agency_settle['my_c_name'])
        ->setCellValue("R{$idx}", $agency_settle['partner_name'])
        ->setCellValue("S{$idx}", $agency_settle['manager_team_name'])
        ->setCellValue("T{$idx}", $agency_settle['manager_name'])
        ->setCellValue("U{$idx}", number_format($agency_settle['wd_price'],0))
        ->setCellValue("V{$idx}", number_format($agency_settle['dp_price'],0))
        ->setCellValue("W{$idx}", $agency_settle['run_memo'])
        ->setCellValue("X{$idx}", $agency_settle['run_name'])
    ;

    $idx++;
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getStyle("A1:X{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:X2')->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle('A1:X2')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle('A1:X2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:X2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:X{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:X2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFC0C0C0');

$objPHPExcel->getActiveSheet()->getStyle("A1:X{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:X{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("J3:M{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("N3:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("R3:R{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("U3:V{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("W3:W{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


$objPHPExcel->getActiveSheet()->getStyle("N2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("P2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("U1:U2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("V1:V2")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("C3:C{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("I3:I{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("J3:J{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("K3:K{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("L3:L{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("R3:R{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("W3:W{$idx}")->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(10);

$objPHPExcel->getActiveSheet()->setTitle("대행수수료 정산관리");


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_대행수수료 정산관리 리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
