<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/withdraw.php');
require('inc/model/MyQuick.php');
require('inc/model/MyCompany.php');
require('inc/model/Corporation.php');
require('inc/model/Company.php');
require('inc/model/Product.php');
require('inc/model/Work.php');
require('inc/model/Withdraw.php');
require('inc/model/Team.php');
require('inc/model/Kind.php');

// 접근 권한
if (!(permissionNameCheck($session_permission, "마케터") || permissionNameCheck($session_permission, "대표") || permissionNameCheck($session_permission, "재무관리자") || permissionNameCheck($session_permission, "외주관리자")  || $session_team == '00244')){
	$smarty->display('access_error.html');
	exit;
}

# 프로세스 처리
$process 		= (isset($_POST['process'])) ? $_POST['process'] : "";
$withdraw_model = Withdraw::Factory();

if ($process == "f_wd_state")
{
	$wd_no	= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value	= (isset($_POST['val'])) ? $_POST['val'] : "";

	$withdraw_item 		= $withdraw_model->getItem($wd_no);
	$withdraw_method 	= $withdraw_item['wd_method'];
	$withdraw_data		= array("wd_no" => $wd_no, "wd_state" => $value);

	if($value == '3')
	{
		$withdraw_data['wd_date'] 	= date("Y-m-d");
		$withdraw_data['run_s_no'] 	= $session_s_no;
		$withdraw_data['run_team'] 	= $session_team;

		if($withdraw_method != '3'){
			$withdraw_data['wd_money'] = $withdraw_item['cost'];
		}
	}

	if (!$withdraw_model->update($withdraw_data))
		echo "출금상태 저장에 실패 하였습니다.";
	else
		echo "출금상태가 저장 되었습니다.";
	exit;
}
elseif ($process == "f_wd_method")
{
	$wd_no		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data 	= array("wd_no" => $wd_no, "wd_method" => $value);

	if (!$withdraw_model->update($upd_data))
		echo "출금방식 저장에 실패 하였습니다.";
	else
		echo "출금방식이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_wd_date")
{
	$wd_no		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data 	= array("wd_no" => $wd_no, "wd_date" => empty($value) ? "NULL" : $value);

	if (!$withdraw_model->update($upd_data))
		echo "출금완료일 저장에 실패 하였습니다.";
	else
		echo "출금완료일이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_wd_money")
{
	$wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$value 		= str_replace(",","",trim($value)); // 컴마 제거하기
	$upd_data 	= array("wd_no" => $wd_no, "wd_money" => $value);

	if (!$withdraw_model->update($upd_data))
		echo "현재 출금액 저장에 실패 하였습니다.";
	else
		echo "현재 출금액이 저장 되었습니다.";
	exit;
}
elseif ($process == "f_wd_tax_state")
{
	$wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data 	= array("wd_no" => $wd_no, "wd_tax_state" => $value);

	if (!$withdraw_model->update($upd_data))
		echo "계산서 발행상태 저장에 실패 하였습니다.";
	else
		echo "계산서 발행상태가 저장 되었습니다.";
	exit;
}
elseif ($process == "f_wd_tax_date")
{
	$wd_no		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data 	= array("wd_no" => $wd_no, "wd_tax_date" => empty($value) ? "NULL" : $value);

	if (!$withdraw_model->update($upd_data))
		echo "계산서 발행일자 저장에 실패 하였습니다.";
	else
		echo "계산서 발행일자가 저장 되었습니다.";
	exit;
}
elseif ($process == "f_wd_tax_item")
{
	$wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data 	= array("wd_no" => $wd_no, "wd_tax_item" => $value);

	if (!$withdraw_model->update($upd_data))
		echo "계산서 발행항목 저장에 실패 하였습니다.";
	else
		echo "계산서 발행항목이 저장 되었습니다.";
	exit;
}
elseif ($process == "wd_check_complete")
{
	$wd_no 			= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$withdraw_item 	= $withdraw_model->getItem($wd_no);
	$withdraw_data  = array("wd_no" => $wd_no, "wd_state" => "3", "wd_date" => date("Y-m-d"), "run_s_no" => $session_s_no, "run_team" => $session_team);
	if($withdraw_item['wd_method'] != "3"){
		$withdraw_data['wd_money'] = $withdraw_item['cost'];
	}

	if (!$withdraw_model->update($withdraw_data)){
		echo ("<script>alert('{$wd_no} 출금 완료처리에 실패 하였습니다');</script>");
	}
	exit ("<script>location.href='withdraw_list.php?{$search_url}';</script>");
}
elseif ($process == "del_manager_record")
{
	$wd_no 	  		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	$work_chk_sql  	= "SELECT w.w_no FROM work w WHERE w.wd_no = '{$wd_no}'";
	$w_no_list  	= [];
	$work_chk_query = mysqli_query($my_db, $work_chk_sql);
	while($work_data = mysqli_fetch_array($work_chk_query)) {
		if(!!$w_no_list)
			$w_no_list .= ",'{$work_data['w_no']}'";
		else
			$w_no_list .= "'{$work_data['w_no']}'";
	}

	if (!$withdraw_model->update(array('wd_no' => $wd_no, "display" => "2"))){
		echo ("<script>alert('출금내역을 DISPLAY OFF 처리에 실패 하였습니다');</script>");
	}else{
		if(!empty($w_no_list))
		{
			$work_sql = "UPDATE `work` w SET w.wd_no = NULL WHERE `w`.w_no IN ({$w_no_list})";
			mysqli_query($my_db, $work_sql);
		}
	}
	exit ("<script>location.href='withdraw_list.php?{$search_url}';</script>");
}
elseif ($process == "f_manager_memo")
{
	$wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value 		= (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
	$upd_data 	= array("wd_no" => $wd_no, "manager_memo" => $value, "manager_memo_regdate" => date("Y-m-d H:i:s"), "manager_memo_s_no" => $session_s_no);

	if (!$withdraw_model->update($upd_data))
		echo "관리자 메모 저장에 실패 하였습니다.";
	else
		echo "관리자 메모가 저장 되었습니다.";
	exit;
}
elseif ($process == "save_select_withdraw")
{
	$wd_list 		= (isset($_POST['select_wd_no_list'])) ? $_POST['select_wd_no_list'] : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	if($wd_list)
	{
		$chk_wd_sql 	= "SELECT * FROM withdraw WHERE wd_no IN({$wd_list})";
		$chk_wd_query	= mysqli_query($my_db, $chk_wd_sql);
		$upd_wd_data 	= [];
		while($chk_wd = mysqli_fetch_array($chk_wd_query))
		{
			$upd_wd_tmp_data = array("wd_no" => $chk_wd['wd_no'], "wd_state" => 3, "wd_date" => date('Y-m-d'), "run_s_no" => $session_s_no, "run_team" => $session_team);
			if($chk_wd['wd_method'] != '3'){
				$upd_wd_tmp_data["wd_money"] = $chk_wd['cost'];
			}

			$upd_wd_data[] = $upd_wd_tmp_data;
		}

		if(!empty($upd_wd_data)){
			if($withdraw_model->multiUpdate($upd_wd_data)){
				exit ("<script>alert('출금완료 처리했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
			}else{
				exit ("<script>alert('출금완료 처리에 실패했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
			}
		}
	}
	exit ("<script>alert('출금완료 처리에 실패했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
}
elseif ($process == "modify_select_state")
{
	$wd_list 		= (isset($_POST['select_wd_no_list'])) ? $_POST['select_wd_no_list'] : "";
	$select_state 	= (isset($_POST['select_state'])) ? $_POST['select_state'] : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	if($wd_list)
	{
		$chk_wd_sql 	= "SELECT * FROM withdraw WHERE wd_no IN({$wd_list})";
		$chk_wd_query	= mysqli_query($my_db, $chk_wd_sql);
		$upd_wd_data 	= [];
		while($chk_wd = mysqli_fetch_array($chk_wd_query))
		{
			$upd_tmp_data = array("wd_no" => $chk_wd['wd_no'], "wd_state" => $select_state);
			if($select_state == '3')
			{
				$upd_tmp_data['wd_date']  = date("Y-m-d");
				$upd_tmp_data['run_team'] = $session_team;
				$upd_tmp_data['run_s_no'] = $session_s_no;

				if($chk_wd['wd_method'] != '3'){
					$upd_tmp_data['wd_money'] = $chk_wd['cost'];
				}
			}
			$upd_wd_data[] = $upd_tmp_data;
		}

		if(!empty($upd_wd_data)){
			if($withdraw_model->multiUpdate($upd_wd_data)){
				exit ("<script>alert('입금상태 변경을 완료했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
			}else{
				exit ("<script>alert('입금상태 변경에 실패했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
			}
		}
	}
	exit ("<script>alert('출금완료일 변경에 실패했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
}
elseif ($process == "modify_select_date")
{
	$wd_list 		= (isset($_POST['select_wd_no_list'])) ? $_POST['select_wd_no_list'] : "";
	$select_date 	= (isset($_POST['select_date'])) ? $_POST['select_date'] : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	if($wd_list)
	{
		$chk_wd_sql 	= "SELECT * FROM withdraw WHERE wd_no IN({$wd_list}) AND wd_state='3'";
		$chk_wd_query	= mysqli_query($my_db, $chk_wd_sql);
		$upd_wd_data 	= [];
		while($chk_wd = mysqli_fetch_array($chk_wd_query))
		{
			$upd_wd_data[] = array("wd_no" => $chk_wd['wd_no'], "wd_date" => $select_date);
		}

		if(!empty($upd_wd_data)){
			if($withdraw_model->multiUpdate($upd_wd_data)){
				exit ("<script>alert('출금완료일 변경을 완료했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
			}else{
				exit ("<script>alert('출금완료일 변경에 실패했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
			}
		}
	}
	exit ("<script>alert('출금완료일 변경에 실패했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
}
elseif ($process == "save_select_inspection")
{
	$wd_list 		= (isset($_POST['select_wd_no_list'])) ? $_POST['select_wd_no_list'] : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	if($wd_list)
	{
		$chk_wd_sql 	= "SELECT * FROM withdraw WHERE wd_no IN({$wd_list})";
		$chk_wd_query	= mysqli_query($my_db, $chk_wd_sql);
		$upd_wd_data 	= [];
		while($chk_wd = mysqli_fetch_array($chk_wd_query))
		{
			$upd_wd_data[] = array("wd_no" => $chk_wd['wd_no'], "is_inspection" => 1, "ins_date" => date('Y-m-d'), "ins_s_no" => $session_s_no);
		}

		if(!empty($upd_wd_data)){
			if($withdraw_model->multiUpdate($upd_wd_data)){
				exit ("<script>alert('검수완료 처리했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
			}else{
				exit ("<script>alert('검수완료 처리에 실패했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
			}
		}
	}
	exit ("<script>alert('검수완료 처리에 실패했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
}
elseif ($process == "f_is_inspection")
{
	$wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	if($value == 'false'){
		$upd_data 	= array("wd_no" => $wd_no, "is_inspection" => 2, "ins_s_no" => "NULL", "ins_date" => "NULL");
	}else{
		$upd_data 	= array("wd_no" => $wd_no, "is_inspection" => 1, "ins_s_no" => $session_s_no, "ins_date" => date("Y-m-d H:i:s"));
	}

	if (!$withdraw_model->update($upd_data))
		echo "검수완료 처리에 실패했습니다.";
	else
		echo "검수완료 처리했습니다.";
	exit;
}
elseif ($process == "save_select_approve")
{
	$wd_list 		= (isset($_POST['select_wd_no_list'])) ? $_POST['select_wd_no_list'] : "";
	$search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

	if($wd_list)
	{
		$add_where = "";
		if($session_s_no == "18"){
			$add_where = " AND reg_s_no IN('170','171','61')";
		}elseif($session_s_no == "102" || $session_s_no == "274"){
			$add_where = " AND reg_s_no='146'";
		}

		if(!empty($add_where))
		{
			$chk_wd_sql 	= "SELECT * FROM withdraw WHERE wd_no IN({$wd_list}) {$add_where}";
			$chk_wd_query	= mysqli_query($my_db, $chk_wd_sql);
			$upd_wd_data 	= [];
			while($chk_wd = mysqli_fetch_array($chk_wd_query))
			{
				$upd_wd_data[] = array("wd_no" => $chk_wd['wd_no'], "is_approve" => 1, "appr_date" => date('Y-m-d'), "appr_s_no" => $session_s_no);
			}

			if(!empty($upd_wd_data)){
				if($withdraw_model->multiUpdate($upd_wd_data)){
					exit ("<script>alert('승인완료 처리했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
				}else{
					exit ("<script>alert('승인완료 처리에 실패했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
				}
			}
		}
	}
	exit ("<script>alert('승인완료 처리에 실패했습니다.');location.href='withdraw_list.php?{$search_url}';</script>");
}
elseif ($process == "f_is_approve")
{
	$wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

	if($value == 'false'){
		$upd_data 	= array("wd_no" => $wd_no, "is_approve" => 2, "appr_s_no" => "NULL", "appr_date" => "NULL");
	}else{
		$upd_data 	= array("wd_no" => $wd_no, "is_approve" => 1, "appr_s_no" => $session_s_no, "appr_date" => date("Y-m-d H:i:s"));
	}

	if (!$withdraw_model->update($upd_data))
		echo "승인완료 처리에 실패했습니다.";
	else
		echo "승인완료 처리했습니다.";
	exit;
}
elseif ($process == "f_bk_title")
{
    $wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data 	= array("wd_no" => $wd_no, "bk_title" => empty($value) ? "NULL" : $value);

	if (!$withdraw_model->update($upd_data))
        echo "은행명 저장에 실패 하였습니다.";
    else
        echo "은행명이 저장 되었습니다.";
    exit;
}
elseif ($process == "f_bk_name")
{
    $wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data 	= array("wd_no" => $wd_no, "bk_name" => empty($value) ? "NULL" : $value);

	if (!$withdraw_model->update($upd_data))
        echo "예금주 저장에 실패 하였습니다.";
    else
        echo "예금주 저장 되었습니다.";
    exit;
}
elseif ($process == "f_bk_num")
{
    $wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data 	= array("wd_no" => $wd_no, "bk_num" => empty($value) ? "NULL" : $value);

	if (!$withdraw_model->update($upd_data))
        echo "계산서 발행일자 저장에 실패 하였습니다.";
    else
        echo "계산서 발행일자가 저장 되었습니다.";
    exit;
}
elseif ($process == "f_wd_account")
{
	$wd_no 		= (isset($_POST['wd_no'])) ? $_POST['wd_no'] : "";
	$value 		= (isset($_POST['val'])) ? $_POST['val'] : "";
	$upd_data 	= array("wd_no" => $wd_no, "wd_account" => $value);

	if (!$withdraw_model->update($upd_data))
		echo "출금하는계좌 변경에 실패 하였습니다.";
	else
		echo "출금하는계좌 변경에 성공했습니다.";
	exit;
}
elseif ($process == "f_w_c_no")
{
	$w_no 			= (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
	$value 			= (isset($_POST['val'])) ? $_POST['val'] : "";

	$work_model 	= Work::Factory();
	$company_model 	= Company::Factory();
	$company_item 	= $company_model->getItem($value);

	$upd_data 		= array("w_no" => $w_no, "c_no" => $company_item["c_no"], "c_name" => $company_item["c_name"], "s_no" => $company_item["s_no"], "team" => $company_item["team"]);

	if (!$work_model->update($upd_data))
		echo "업무 업체명 변경에 실패 하였습니다.";
	else
		echo "업무 업체명 변경에 성공했습니다.";
	exit;
}

# Navigation & My Quick
$nav_prd_no  = "63";
$nav_title   = "출금리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

$is_search  = false;
if(permissionNameCheck($session_permission, "재무관리자") || permissionNameCheck($session_permission, "마스터관리자")
	|| ($session_team == '00222' || $session_team == '00244' || $session_team == '00211' || $session_team == '00221' || $session_s_no == '62'))
{
	$is_search = true;
}
$smarty->assign("is_search", $is_search);

$is_approve_view = false;
$is_approve_edit = false;
if(($session_s_no == "18" || $session_s_no == "102" || $session_s_no == "274" || $session_s_no == "61" || $session_s_no == "146" || $session_s_no == "170" || $session_s_no == "171") || $session_team == '00211'){
	$is_approve_view = true;
	if($session_s_no == "18" || $session_s_no == "102" || $session_s_no == "274"){
		$is_approve_edit = true;
	}
}
$smarty->assign("is_approve_view", $is_approve_view);
$smarty->assign("is_approve_edit", $is_approve_edit);

# Option Model 설정
$corp_model 		= Corporation::Factory();
$mycomp_model 		= MyCompany::Factory();
$product_model  	= Product::Factory();
$out_product_list 	= $product_model->getWithdrawWorkProduct();

$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

$base_wd_method = 1;
if(permissionNameCheck($session_permission, "외주관리자")){
	$base_wd_method = "";
}

# 검색 쿼리
$add_where 			= "display = 1";
$sch_wd_quick 		= isset($_GET['sch_wd_quick']) ? $_GET['sch_wd_quick'] : "";
$sch_wd_no 			= isset($_GET['sch_wd_no']) ? $_GET['sch_wd_no'] : "";
$sch_w_req_date 	= isset($_GET['sch_w_req_date']) ? $_GET['sch_w_req_date'] : "";
$sch_wd_state 		= isset($_GET['sch_wd_state']) ? $_GET['sch_wd_state'] : "";
$sch_wd_method 		= isset($_GET['sch_wd_method']) ? $_GET['sch_wd_method'] : $base_wd_method;
$sch_s_wd_date 		= isset($_GET['sch_s_wd_date']) ? $_GET['sch_s_wd_date'] : "";
$sch_e_wd_date 		= isset($_GET['sch_e_wd_date']) ? $_GET['sch_e_wd_date'] : "";
$sch_wd_tax_state 	= isset($_GET['sch_wd_tax_state']) ? $_GET['sch_wd_tax_state'] : "";
$sch_reg_s_no 		= isset($_GET['sch_reg_s_no']) ? $_GET['sch_reg_s_no'] : "";
$sch_wd_etc 		= isset($_GET['sch_wd_etc']) ? $_GET['sch_wd_etc'] : "";
$sch_my_c_no 		= isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_wd_account		= isset($_GET['sch_wd_account']) ? $_GET['sch_wd_account'] : "";
$sch_prd_no			= isset($_GET['sch_prd_no'])?$_GET['sch_prd_no']:"";
$sch_c_name 		= isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_c_no 			= isset($_GET['sch_c_no'])?$_GET['sch_c_no']:"";
$sch_subject 		= isset($_GET['sch_subject']) ? $_GET['sch_subject'] : "";
$sch_w_my_c_no 		= isset($_GET['sch_w_my_c_no']) ? $_GET['sch_w_my_c_no'] : "";
$sch_w_c_no 		= isset($_GET['sch_w_c_no'])?$_GET['sch_w_c_no']:"";
$sch_w_c_name 		= isset($_GET['sch_w_c_name']) ? $_GET['sch_w_c_name'] : "";
$sch_w_c_equal 		= isset($_GET['sch_w_c_equal']) ? $_GET['sch_w_c_equal'] : "";
$sch_w_team 		= isset($_GET['sch_w_team']) ? $_GET['sch_w_team'] : "";
$sch_w_s_name 		= isset($_GET['sch_w_s_name']) ? $_GET['sch_w_s_name'] : "";
$sch_is_inspection 	= isset($_GET['sch_is_inspection']) ? $_GET['sch_is_inspection'] : "";
$sch_is_approve 	= isset($_GET['sch_is_approve']) ? $_GET['sch_is_approve'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

if(!empty($sch_brand)) {
	$sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
	$sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
	$sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand)) {
	$sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
	$sch_brand_list     = $brand_list[$sch_brand_g2];

	$add_where .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
	$sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
	$sch_brand_list     = $brand_list[$sch_brand_g2];

	$add_where .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
	$sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

	$add_where .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);
$smarty->assign("sch_wd_quick", $sch_wd_quick);

if(!$is_search && !isset($_GET['sch_w_s_name'])){
	$sch_w_s_name = $session_name;
}

if (!empty($sch_wd_no)) {
	$add_where .= " AND wd.wd_no='{$sch_wd_no}'";
	$smarty->assign("sch_wd_no", $sch_wd_no);
}

if (!empty($sch_w_req_date)) {
	$add_where .= " AND wd.regdate like '%{$sch_w_req_date}%'";
	$smarty->assign("sch_w_req_date", $sch_w_req_date);
}

if (!empty($sch_wd_state)) {
	$add_where .= " AND wd.wd_state='{$sch_wd_state}'";
	$smarty->assign("sch_wd_state", $sch_wd_state);
}

if (!empty($sch_wd_method)) {
	$add_where .= " AND wd.wd_method='{$sch_wd_method}'";
	$smarty->assign("sch_wd_method", $sch_wd_method);
}

if (!empty($sch_s_wd_date)) {
	$add_where .= " AND wd.wd_date >= '{$sch_s_wd_date}'";
	$smarty->assign("sch_s_wd_date", $sch_s_wd_date);
}

if (!empty($sch_e_wd_date)) {
	$add_where .= " AND wd.wd_date <= '{$sch_e_wd_date}'";
	$smarty->assign("sch_e_wd_date", $sch_e_wd_date);
}

if (!empty($sch_wd_tax_state)) {
	$add_where .= " AND wd.wd_tax_state='{$sch_wd_tax_state}'";
	$smarty->assign("sch_wd_tax_state", $sch_wd_tax_state);
}

if (!empty($sch_reg_s_no)) {
	$add_where .= " AND wd.reg_s_no IN (SELECT sub.s_no FROM staff sub WHERE sub.s_name like '%{$sch_reg_s_no}%')";
	$smarty->assign("sch_reg_s_no", $sch_reg_s_no);
}

if (!empty($sch_wd_etc)) {
	if ($sch_wd_etc == "1") {
		$add_where .= " AND  ";
	} elseif ($sch_wd_etc == "2") {
		$add_where .= " AND  ";
	}
	$smarty->assign("sch_wd_etc", $sch_wd_etc);
}

if (!empty($sch_my_c_no)) {
	$add_where .= " AND wd.my_c_no = '{$sch_my_c_no}'";
	$smarty->assign("sch_my_c_no", $sch_my_c_no);
}

if (!empty($sch_wd_account)) {
	$add_where .= " AND wd.wd_account = '{$sch_wd_account}'";
	$smarty->assign("sch_wd_account", $sch_wd_account);
}

if(!empty($sch_prd_no)) { // 상품
	$add_where .= " AND `w`.prd_no='{$sch_prd_no}'";
	$smarty->assign("sch_prd_no", $sch_prd_no);
	$smarty->assign("sch_prd_no_value", $out_product_list[$sch_prd_no]);
}

if($sch_c_no == 'null') { // c_no is null
	$add_where .= " AND wd.c_no ='0'";
	$smarty->assign("sch_c_no", $sch_c_no);
}

if (!empty($sch_c_name))
{
	$add_where .= " AND wd.c_name like '%{$sch_c_name}%'";
	$smarty->assign("sch_c_name", $sch_c_name);
}

if (!empty($sch_subject)) {
	$add_where .= " AND wd.wd_subject like '%{$sch_subject}%'";
	$smarty->assign("sch_subject", $sch_subject);
}

if($sch_w_c_no)
{
	$add_where .= " AND `w`.c_no = '0' AND `w`.task_run_regdate >= '2020-01-01 00:00:00'";
	$smarty->assign("sch_w_c_no", $sch_w_c_no);
}
else
{
	if(!empty($sch_w_my_c_no))
	{
		$add_where .= " AND (SELECT cp.my_c_no FROM company cp WHERE cp.c_no =`w`.c_no) = '{$sch_w_my_c_no}'";
		$smarty->assign("sch_w_my_c_no", $sch_w_my_c_no);
	}

	if (!empty($sch_w_c_name))
	{
		if(!empty($sch_w_c_equal)){
			$add_where .= " AND `w`.c_name = '{$sch_w_c_name}'";
			$smarty->assign("sch_w_c_equal", $sch_w_c_equal);
		}else{
			$add_where .= " AND `w`.c_name LIKE '%{$sch_w_c_name}%'";
		}

		$smarty->assign("sch_w_c_name", $sch_w_c_name);
	}
}

if(!$is_search){
	$sch_w_team = $session_team;
}

if(!empty($sch_w_team))
{
	if ($sch_w_team != "all")
	{
		if($sch_w_team == "00257"){
			$team_where_1 	= getTeamWhere($my_db, $sch_w_team);
			$team_where_2 	= getTeamWhere($my_db, "00252");
			$team_where 	= $team_where_1.",".$team_where_2;
		}else{
			$team_where = getTeamWhere($my_db, $sch_w_team);
		}

		$add_where .= " AND wd.team IN({$team_where})";
		$smarty->assign("sch_w_team", $sch_w_team);
	}
}

if(!empty($sch_w_s_name))
{
	$add_where .= " AND wd.s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_w_s_name}%')";
	$smarty->assign("sch_w_s_name", $sch_w_s_name);
}

if (!empty($sch_is_inspection))
{
	$add_where .= " AND wd.is_inspection='{$sch_is_inspection}'";
	$smarty->assign("sch_is_inspection", $sch_is_inspection);
}

if (!empty($sch_is_approve))
{
	$add_where .= " AND wd.is_approve='{$sch_is_approve}'";
	$smarty->assign("sch_is_approve", $sch_is_approve);
}

if ($sch_wd_state == "2" || $sch_wd_state == "3")
	$add_orderby = " wd.wd_date DESC ";
else {
	if (permissionNameCheck($session_permission, "마케터"))
		$add_orderby = " case when wd.s_no = 0 then 99999999 + wd.wd_no else wd.wd_no end DESC ";
	else
		$add_orderby = " wd.wd_no DESC ";
}

# 전체 게시물 수
$withdraw_total_sql 	= "
	SELECT
		COUNT(wd.wd_no) as total_cnt,
		SUM(wd.supply_cost) AS total_supply_cost,
		SUM(wd.cost) AS total_cost,
		SUM(wd.wd_money) AS total_wd_money
	FROM withdraw wd
	LEFT JOIN `work` as `w` ON `w`.wd_no=wd.wd_no
	WHERE {$add_where}
";
$withdraw_total_query	= mysqli_query($my_db, $withdraw_total_sql);
$withdraw_total_result	= mysqli_fetch_array($withdraw_total_query);
$withdraw_total 		= $withdraw_total_result['total_cnt'];

# 페이징 처리
$page 	 	= isset($_GET['page']) ? intval($_GET['page']) : 1;
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$num 	 	= $page_type;
$offset  	= ($page-1) * $num;
$page_num 	= ceil($withdraw_total/$num);

if ($page >= $page_num){$pages=$page_num;}
if ($page <= 0){$page=1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($page, "withdraw_list.php", $page_num, $search_url);

$smarty->assign("total_num", $withdraw_total);
$smarty->assign("total_supply_cost", $withdraw_total_result['total_supply_cost']);
$smarty->assign("total_cost", $withdraw_total_result['total_cost']);
$smarty->assign("total_wd_money", $withdraw_total_result['total_wd_money']);
$smarty->assign("search_url",$search_url);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$withdraw_sql = "
	SELECT
		wd.*,
		(SELECT s_name FROM staff s WHERE s.s_no=wd.s_no) AS s_name,
		(SELECT team_name FROM team t WHERE t.team_code=wd.team) AS t_name,
		(SELECT title FROM product prd WHERE prd.prd_no=`w`.prd_no) AS product_title,
		w.prd_no AS prd_no,
		(SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=wd.my_c_no) AS my_c_name,
		(SELECT mc.kind_color FROM my_company mc WHERE mc.my_c_no=wd.my_c_no) AS my_c_kind_color,
		(SELECT s_name FROM staff s WHERE s.s_no=wd.reg_s_no) AS reg_s_name,
		(SELECT s_name FROM staff s WHERE s.s_no=wd.manager_memo_s_no) AS manager_memo_s_name,
		(select CONCAT(o_registration, '||', o_accdoc, '||', bk_title, '||', bk_num, '||', bk_name) from company where c_no = wd.c_no) tx_info,
		(SELECT cp.my_c_no FROM company cp WHERE cp.c_no = `w`.c_no) as w_my_c_no,
		w.c_no as w_c_no,
		w.c_name as w_c_name,
		w.task_req as work_task,
		(SELECT s_name FROM staff s WHERE s.s_no=wd.run_s_no) as run_s_name,
		(SELECT COUNT(*) FROM linked_comment lc WHERE lc.linked_table='withdraw' AND lc.linked_no=wd.wd_no) as comment_cnt
	FROM withdraw wd
	LEFT JOIN `work` `w` ON `w`.wd_no=wd.wd_no
	WHERE {$add_where}
	ORDER BY {$add_orderby}
	LIMIT {$offset}, {$num}
";
$withdraw_query 	= mysqli_query($my_db, $withdraw_sql);
$withdraw_list  	= [];
$wd_method_option 	= getWdMethodOption();
while($withdraw_array = mysqli_fetch_array($withdraw_query))
{
	$withdraw_array['wd_method_name'] = $wd_method_option[$withdraw_array['wd_method']];
	$withdraw_array['s_name'] 		  = !empty($withdraw_array['s_name']) ? $withdraw_array['s_name'] : "<span style=\"color:red\">알수없음</span>";
	$withdraw_array['work_task']	  = str_replace("\r\n","<br />",$withdraw_array['work_task']);

	if(!empty($withdraw_array['regdate'])) {
		$withdraw_array['regdate_day']  = date("Y/m/d",strtotime($withdraw_array['regdate']));
		$withdraw_array['regdate_time'] = date("H:i",strtotime($withdraw_array['regdate']));
	}else{
		$withdraw_array['regdate_day']  = '';
		$withdraw_array['regdate_time'] = '';
	}

	if(!empty($withdraw_array['manager_memo_regdate'])) {
		$withdraw_array['manager_memo_regdate_day']  = date("Y/m/d",strtotime($withdraw_array['manager_memo_regdate']));
		$withdraw_array['manager_memo_regdate_time'] = date("H:i",strtotime($withdraw_array['manager_memo_regdate']));
	}else{
		$withdraw_array['manager_memo_regdate_day']  = '';
		$withdraw_array['manager_memo_regdate_time'] = '';
	}

	$withdraw_array['is_approve_view'] = false;
	$withdraw_array['is_approve_edit'] = false;
	if($withdraw_array['reg_s_no'] == '146' || $withdraw_array['reg_s_no'] == '170' || $withdraw_array['reg_s_no'] == "171" ||  $withdraw_array['reg_s_no'] == '61')
	{
		if(($session_s_no == "18" || $session_s_no == "102" || $session_s_no == "274" || $session_s_no == "61" || $session_s_no == "146" || $session_s_no == "170" || $session_s_no == "171") || $session_team == '00211'){
			$withdraw_array['is_approve_view'] = true;
		}

		if($session_s_no == "18" && ($withdraw_array['reg_s_no'] == '170' || $withdraw_array['reg_s_no'] == "171" || $withdraw_array['reg_s_no'] == '61')){
			$withdraw_array['is_approve_edit'] = true;
		}

		if(($session_s_no == "102" || $session_s_no == "274") && $withdraw_array['reg_s_no'] == '146'){
			$withdraw_array['is_approve_edit'] = true;
		}
	}

	$withdraw_list[] = $withdraw_array;
}

# 월정산 데이터
$withdraw_mon_total_sql 	= "SELECT c_no, c_name, SUM(cost) as total FROM withdraw wd WHERE wd_method='4' AND wd_state='2' AND display='1' GROUP BY c_no ORDER BY c_name ASC";
$withdraw_mon_total_query 	= mysqli_query($my_db, $withdraw_mon_total_sql);
$withdraw_mon_total_list 	= [];
while($withdraw_mon_total = mysqli_fetch_array($withdraw_mon_total_query)){
	$withdraw_mon_total_list[$withdraw_mon_total['c_no']] = $withdraw_mon_total;
}

$team_model         = Team::Factory();
$sch_team_full_list = $team_model->getTeamFullNameList();
$sch_team_name_list = $team_model->getTeamNameList();

$smarty->assign("my_company_list", $mycomp_model->getList());
$smarty->assign("sch_my_company_option", $mycomp_model->getNameList());
$smarty->assign("sch_team_full_list", $sch_team_full_list);
$smarty->assign("sch_team_name_list", $sch_team_name_list);
$smarty->assign("wd_state_option", getWdStateOption());
$smarty->assign("wd_method_option", $wd_method_option);
$smarty->assign("wd_tax_state_option",getWdTaxStateOption());
$smarty->assign("wd_is_inspection_option", getWdIsInspectionOption());
$smarty->assign("sch_corp_account_option", $corp_model->getAccountList('2'));
$smarty->assign("page_type_option", getPageTypeOption(3));
$smarty->assign("out_product_list", $out_product_list);
$smarty->assign("withdraw_list", $withdraw_list);
$smarty->assign("withdraw_mon_total_list", $withdraw_mon_total_list);

$smarty->display('withdraw_list.html');
?>
