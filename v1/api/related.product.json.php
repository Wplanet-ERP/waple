<?php
require('../inc/common.php');

$prd_no = isset($_POST['prd_no'])?$_POST['prd_no']:"";
$w_no   = isset($_POST['w_no'])?$_POST['w_no']:"";
$result = false;

if(!!$prd_no){
    // 리스트 쿼리
    $related_product_sql = "SELECT prd.prd_no, prd.title as prd_name, (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code)) AS prd_g1_name, (SELECT k.k_name FROM kind k WHERE k.k_name_code=prd.k_name_code) AS prd_g2_name FROM product_relation pr LEFT JOIN product prd ON prd.prd_no = pr.related_prd WHERE pr.prd_no='{$prd_no}' ORDER BY pr.priority ASC, pr.no ASC";

    $query = mysqli_query($my_db,  $related_product_sql);
    $html  = "";

    while ($related_product = mysqli_fetch_assoc($query))
    {
        $new_prd_g1_txt = $related_product['prd_g1_name'];
        $new_prd_g2_txt = $related_product['prd_g2_name'];
        $new_prd_txt    = $related_product['prd_name'];
        $new_prd        = $related_product['prd_no'];
        $new_class_name = "related-prd-{$new_prd}";

        $html .= "
                <tr class='related-prd-wrap'><td>{$new_prd_g1_txt}</td><td>{$new_prd_g2_txt}</td><td>{$new_prd_txt}</td>
                    <td>
                        <button type='button' class='related-move-btn' onclick='moveRelatedWork(\"exist\", \"{$new_prd}\")'>업무요청</button>
                    </td>
                </tr>";
    }

    $data = !empty($html) ? array("result" => true, "html" => $html) : array("result" => false, "html" => $html);
}else{
    $data = array("result" => false, "html" => "");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
