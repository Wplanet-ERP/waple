<?php
require('../inc/common.php');

$s_name_get     = isset($_GET['s_name'])?$_GET['s_name']:"";
$s_type_get     = isset($_GET['type'])?$_GET['type']:"";

if($s_type_get == '1'){
    $staff_sql  = "SELECT s.s_no, s.s_name, s.team_list FROM staff s WHERE s.s_name like '%{$s_name_get}%' AND s.staff_state='1' AND permission like '__1%'";
}else{
    $staff_sql  = "SELECT s.s_no, s.s_name, s.team_list FROM staff s WHERE s.s_name like '%{$s_name_get}%' AND s.staff_state='1'";
}
$staff_query    = mysqli_query($my_db, $staff_sql);

while ($staff_array = mysqli_fetch_assoc($staff_query)) {

    $team_list = explode(",", $staff_array['team_list']);

    foreach($team_list as $team_code)
    {
        $team_sql = "SELECT depth, team_name FROM team WHERE team_code='{$team_code}'";
        $team_query = mysqli_query($my_db, $team_sql);
        $team_result = mysqli_fetch_assoc($team_query);

        $s_label = "{$staff_array['s_name']}({$team_result['team_name']})";
        $arr[] = array(
            "s_no"      => $staff_array['s_no'],
            "s_name"    => $staff_array['s_name'],
            "s_label"   => $s_label,
            "team"      => $team_code
        );
    }
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
