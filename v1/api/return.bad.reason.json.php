<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "") {

    $parent_data_get = $_GET[$_GET['parent_data']];

    if ($_GET['parent_data'] == "new_bad_reason_g1" || $_GET['parent_data'] == "bad_reason_g1")
    {
        $sql    = "SELECT k_name_code, k_name FROM kind WHERE display='1' AND k_code='bad_reason' AND k_parent='{$parent_data_get}' ORDER BY priority ASC";
        $query  = mysqli_query($my_db, $sql);
        $count  = mysqli_num_rows($query);

        echo "[" . PHP_EOL;
        if ($count > 0) {
            while ($result = mysqli_fetch_assoc($query)):
                echo "{\"" . $result['k_name_code'] . "\":\"" . $result['k_name'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;
    }
}
?>
