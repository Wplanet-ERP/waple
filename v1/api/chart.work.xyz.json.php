<?php
require('../inc/common.php');
require('../inc/helper/stats.php');

if(($parent_type = $_GET['parent_data']) && !empty($_GET[$_GET['parent_data']]))
{
    $type = "";
    $data = "";

    if($parent_type == 'sch_x_type' || $parent_type == 'sch_y_type') {
        $type  = $_GET[$parent_type];
        $depth = 1;
    }elseif($parent_type == 'sch_x_type_main' || $parent_type == 'sch_y_type_main'){
        $type  = $_GET['type'];
        $data  = $_GET[$parent_type];
        $depth = 2;
    }elseif($parent_type == 'sch_x_type_sub' || $parent_type == 'sch_y_type_sub'){
        $type  = $_GET['type'];
        $data  = $_GET[$parent_type];
        $depth = 3;
    }

    switch($type)
    {
        case 'day':
            if($depth == '1'){
                echo "[" . PHP_EOL;
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
                foreach($type_day as $key => $label){
                    echo ",{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
                }
                echo "]" . PHP_EOL;
            }
            break;
        case 'work':
            if($depth == '1' || $depth == '2'){
                echo "[" . PHP_EOL;
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
                foreach($work_list as $key => $prd_data)
                {
                    if(!$key && $depth == '1'){
                        foreach($prd_data as $prd){
                            echo ",{\"" . $prd['k_name_code'] . "\":\"" . $prd['k_name'] . "\"}" . PHP_EOL;
                        }
                    }elseif($key == $data && $depth == '2'){
                        foreach($prd_data as $prd){
                            echo ",{\"" . $prd['k_name_code'] . "\":\"" . $prd['k_name'] . "\"}" . PHP_EOL;
                        }
                    }
                }
                echo "]" . PHP_EOL;
            }elseif($depth == '3'){
                echo "[" . PHP_EOL;
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
                foreach($work_prd_list[$data] as $prd){
                    echo ",{\"" . $prd['prd_no'] . "\":\"" . $prd['title'] . "\"}" . PHP_EOL;
                }
                echo "]" . PHP_EOL;
            }
            break;
        case 'staff':
        case 'task_req':
        case 'task_run':
            if($depth == '1'){
                echo "[" . PHP_EOL;
                foreach($sch_team_name_list as $team_code => $team_name)
                {
                    if($team_code == 'all'){
                        echo "{\"" . $team_code . "\":\"" . $team_name . "\"}" . PHP_EOL;
                    }else{
                        echo ",{\"" . $team_code . "\":\"" . $team_name . "\"}" . PHP_EOL;
                    }
                }
                echo "]" . PHP_EOL;
            }elseif($depth == '2'){
                $staff_list = [];
                $sch_team_code_where = getTeamWhere($my_db, $data);
                $sch_team_code_list_val = explode(",", $sch_team_code_where);

                $staff_team_where_list = [];
                foreach($sch_team_code_list_val as $sch_team_code){
                    $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
                }

                $staff_team_where = implode(" OR ", $staff_team_where_list);
                $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
                $staff_result=mysqli_query($my_db,$staff_sql);
                while($staff=mysqli_fetch_array($staff_result)) {
                    $staff_list[]=array(
                        "s_no"=>trim($staff['s_no']),
                        "s_name"=>trim($staff['s_name'])
                    );
                }

                echo "[" . PHP_EOL;
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
                if($staff_list) {
                    foreach ($staff_list as $staff) {
                        echo ",{\"" . $staff['s_no'] . "\":\"" . $staff['s_name'] . "\"}" . PHP_EOL;
                    }
                }
                echo "]" . PHP_EOL;
            }
            break;
        case 'dp_comp':
            if($depth == '1'){
                echo "[" . PHP_EOL;
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
                foreach($type_dp_comp as $key => $label){
                    echo ",{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
                }
                echo "]" . PHP_EOL;
            }
            break;
        case 'wd_comp':
            if($depth == '1'){
                echo "[" . PHP_EOL;
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
                foreach($type_wd_comp as $key => $label){
                    echo ",{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
                }
                echo "]" . PHP_EOL;
            }
            break;
        case 'task_req_eval':
        case 'task_run_eval':
            if($depth == '1'){
                echo "[" . PHP_EOL;
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
                foreach($type_task_eval as $key => $label){
                    echo ",{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
                }
                echo "]" . PHP_EOL;
            }
            break;
    }
}

?>
