<?php
require('../inc/common.php');
require('../inc/helper/_common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$s_name_get = isset($_GET['s_name'])?$_GET['s_name']:"";

$aes_unhex        = "(SELECT AES_DECRYPT(UNHEX(s.cert_no), '{$aeskey}')) AS cert_no";
// 리스트 쿼리
$staff_sql   = "
SELECT 
       s.s_no, 
       s.s_name, 
       s.team_list, 
       s.staff_state,
       {$aes_unhex}, 
       s.position, 
       s.staff_state,
       s.employment_date,
       (SELECT my.c_name FROM my_company my WHERE my.my_c_no=s.my_c_no) as my_company_name, 
       (SELECT CONCAT(my.address1,' ',my.address2) FROM my_company my WHERE my.my_c_no=s.my_c_no) as my_company_addr 
FROM staff s 
WHERE s.s_name like '%{$s_name_get}%'";
$staff_query = mysqli_query($my_db, $staff_sql);

while ($staff_array = mysqli_fetch_assoc($staff_query))
{
    $cert_no_1 = $cert_no_2 = $cert_no_3 = "";
    if(!empty($staff_array['cert_no'])){
        $cert_nos = explode('-', $staff_array['cert_no']);
        $cert_no_1 = !empty($cert_nos) && isset($cert_nos[0]) ? $cert_nos[0] : "";
        $cert_no_2 = !empty($cert_nos) && isset($cert_nos[1]) ? substr($cert_nos[1], 0, 1) : "";
        $cert_no_3 = !empty($cert_nos) && isset($cert_nos[1]) ? substr($cert_nos[1], 1) : "";
    }

    $team_list = explode(",", $staff_array['team_list']);
    $s_state   = ($staff_array['staff_state'] == '3') ? " :: 퇴사자" : "";

    foreach($team_list as $team_code)
    {
        $team_sql    = "SELECT team_name FROM team WHERE team_code='{$team_code}' LIMIT 1";
        $team_query  = mysqli_query($my_db, $team_sql);
        $team_result = mysqli_fetch_assoc($team_query);

        $s_label = "{$staff_array['s_name']} ({$team_result['team_name']})".$s_state;

        $arr[] = array(
            "s_no"      => $staff_array['s_no'],
            "s_name"    => $staff_array['s_name'],
            "s_label"   => $s_label,
            "team"      => $team_code,
            "team_name" => $team_result['team_name'],
            "cert_no_1" => $cert_no_1,
            "cert_no_2" => $cert_no_2,
            "cert_no_3" => $cert_no_3,
            "position"  => $staff_array['position'],
            "my_company_name"   => $staff_array['my_company_name'],
            "my_company_ceo"    => "대 표 이 사 주경민",
            "my_company_addr"   => $staff_array['my_company_addr'],
            "work_s_date"  => $staff_array['employment_date'],
            "staff_state"  => $staff_array['staff_state'],
        );
    }
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
