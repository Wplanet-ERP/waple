<?php
require('../inc/common.php');
require('../ckadmin.php');

$type       = isset($_POST['type']) ? $_POST['type'] : "";
$blog_url   = isset($_POST['url']) ? $_POST['url'] : "";
$result     = false;

if($blog_url)
{
    $page_result = @file_get_contents($blog_url);
    $result 	 = ($page_result === false) ? false : true;
}

$data = array("result" => $result, "blog_url" => $blog_url);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>