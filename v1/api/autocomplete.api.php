<?php
require('../inc/common.php');
header("Content-Type: text/html; charset=UTF-8");

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="1=1";
$c_name_get=isset($_GET['c_name'])?$_GET['c_name']:"";
$s_no_get=isset($_GET['s_no'])?$_GET['s_no']:"";

if(!empty($c_name_get)) {
	$add_where.=" AND c_name like '%".$c_name_get."%'";
}
if(!empty($s_no_get)) {
	$add_where.=" AND s_no='".$s_no_get."'";
}

// 리스트 쿼리
$company_sql="
				SELECT 
					*,
					(SELECT CONCAT (c_name, ' :: ', (SELECT s.s_name FROM staff s WHERE s.s_no=c.s_no))) AS sch_c_name,
					(SELECT s.team FROM staff s WHERE s.s_no=c.s_no) AS team,
					(SELECT t.depth FROM team t WHERE t.team_code =(SELECT s.team FROM staff s WHERE s.s_no=c.s_no)) AS t_depth,
					(SELECT s.s_name FROM staff s WHERE s.s_no=c.s_no) AS s_name
				FROM 
					company c
				WHERE 
					$add_where 
				ORDER BY c_name ASC  
			";
//echo $company_sql;exit;
$result = mysqli_query($my_db,$company_sql);
while ($company_array = mysqli_fetch_assoc($result)):
	$zip=explode("-",$company_array['zipcode']);
	$tel=explode("-",$company_array['tel']);

    $t_label_list = [];
    $t_depth = isset($company_array['t_depth']) ? $company_array['t_depth'] : 1;
    $t_code  = $company_array['team'];

    for($i=0; $i<$t_depth; $i++)
    {
        if(empty($t_code)){
            break;
        }
        $t_label_sql    = "SELECT t.team_name, t.team_code_parent FROM team t WHERE t.team_code='{$t_code}'";
        $t_label_query  = mysqli_query($my_db, $t_label_sql);
        $t_label_result = mysqli_fetch_assoc($t_label_query);

        $t_code = $t_label_result['team_code_parent'];
        $t_label_list[$i] = $t_label_result['team_name'];
    }

    krsort($t_label_list);
    $t_label = implode(" > ", $t_label_list);

    $s_label = "{$company_array['s_name']} ({$t_label})";

	$arr[] = array(
                "c_no"=>$company_array['c_no'],
                "c_name"=>$company_array['c_name'],
                "c_about"=>$company_array['about'],
                "cate1"=>$company_array['cate1'],
                "cate2"=>$company_array['cate2'],
                "loca1"=>$company_array['location1'],
                "loca2"=>$company_array['location2'],
                "zip1"=>$zip[0],
                "zip2"=>$zip[1],
                "address1"=>$company_array['address1'],
                "address2"=>$company_array['address2'],
                "tel1"=>$tel[0],
                "tel2"=>$tel[1],
                "tel3"=>$tel[2],
                "parking"=>$company_array['parking'],
                "site"=>$company_array['site'],
                "s_no"=>$company_array['s_no'],
                "s_name"=>$company_array['s_name'],
                "team"=>$company_array['team'],
                "s_label"=>$s_label,
                "label"=>$company_array['sch_c_name']
			);
endwhile;

echo json_encode($arr);

?>