<?php
require('../inc/common.php');
require('../ckadmin.php');

$af_no      = isset($_POST['af_no']) ? $_POST['af_no'] : "";
$result     = false;
$content    = "";

if(!empty($af_no))
{
    $approval_form_sql   = "SELECT af_no, content FROM approval_form WHERE af_no='{$af_no}' LIMIT 1";
    $approval_form_query = mysqli_query($my_db, $approval_form_sql);
    $approval_form       = mysqli_fetch_assoc($approval_form_query);

    if(isset($approval_form['af_no']))
    {
        $content = $approval_form['content'];
        $result  = true;
    }

}

$data = array("result" => $result, "content" => $content);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>