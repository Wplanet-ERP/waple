<?php
require('../inc/common.php');
require('../ckadmin.php');

$utm_no     = isset($_POST['utm_no']) ? $_POST['utm_no'] : "";
$result     = false;
$utm_data   = [];
if(!empty($utm_no))
{
   $sql      = "SELECT *, (SELECT s.s_name FROM staff s WHERE s.s_no=utm.s_no) as s_name FROM commerce_utm as utm WHERE utm_no='{$utm_no}'";
   $query    = mysqli_query($my_db, $sql);
   $utm_data = mysqli_fetch_assoc($query);

   if($utm_data['utm_no'])
   {
       $result = true;
   }

}

$data = array("result" => $result, "utm_data" => $utm_data);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>