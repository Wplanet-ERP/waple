<?php
require('../inc/common.php');

$unit_name  = isset($_GET['unit_name'])?$_GET['unit_name']:"";
$req_staff  = isset($_GET['req_staff'])?$_GET['req_staff']:"";
$result_arr = [];

$comm_unit_sql  = "
    SELECT
	    *,
        (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcu.`no` AND pcum.log_c_no='2809') as sku
    FROM product_cms_unit pcu
    WHERE `no` IN(SELECT option_no FROM commerce_order WHERE set_no IN(SELECT `no` FROM commerce_order_set WHERE state='2' AND display='1' AND req_s_no='{$req_staff}') AND `type`='request' GROUP BY option_no)
    AND option_name LIKE '%{$unit_name}%'
";
$comm_unit_query = mysqli_query($my_db, $comm_unit_sql);
while ($comm_unit = mysqli_fetch_assoc($comm_unit_query))
{
    $result_arr[] = array(
        "option_no"     => $comm_unit['no'],
        "option_name"   => $comm_unit['option_name'],
        "sku"           => $comm_unit['sku'],
    );
}

echo json_encode($result_arr, JSON_UNESCAPED_UNICODE);

?>
