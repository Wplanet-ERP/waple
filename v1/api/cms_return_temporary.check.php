<?php
require('../inc/common.php');

$return_temporary_sql    = "SELECT r_no FROM work_cms_return_temporary WHERE (send_prd_list = '' OR send_prd_list IS NULL) AND return_purpose='1' LIMIT 1";
$return_temporary_query  = mysqli_query($my_db, $return_temporary_sql);
$return_temporary_result = mysqli_fetch_assoc($return_temporary_query);

$r_no = isset($return_temporary_result['r_no']) ? $return_temporary_result['r_no'] : "0";

if($r_no > 0){
    $data_result = array("result" => false, "r_no" => $r_no);
}else{
    $data_result = array("result" => true, "r_no" => $r_no);
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($data_result, JSON_UNESCAPED_UNICODE);

?>
