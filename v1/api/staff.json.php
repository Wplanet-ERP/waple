<?php
require('../inc/common.php');

if($_GET['parent_data'] != "")
{
    $team_code = $_GET[$_GET['parent_data']];

    if($_GET['parent_data'] == "f_team" || $_GET['parent_data'] == "sch_team" || $_GET['parent_data'] == "sch_writer_team")
    {
        if(empty($team_code) || $team_code == "all") {
            $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE s.staff_state='1' ORDER BY s_name ASC";
        }
        else
        {
            $sch_team_code_list 	= getTeamWhere($my_db, $team_code);
            $staff_team_where 		= "";
            $staff_team_where_list 	= [];

            if($sch_team_code_list){
                $sch_team_code_list_val = explode(",", $sch_team_code_list);

                foreach($sch_team_code_list_val as $sch_team_code){
                    $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
                }
            }

            if($staff_team_where_list){
                $staff_team_where = implode(" OR ", $staff_team_where_list);
            }

            $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
        }
        $staff_query = mysqli_query($my_db, $staff_sql);
        $staff_count = mysqli_num_rows($staff_query);

        echo "[".PHP_EOL;
        if($staff_count>0) {
    			if($_GET['parent_data'] == "sch_team" || $_GET['parent_data'] == "sch_writer_team"){
    				echo "{\"\":\"::전체::\"}".(($staff_count>0)?",":"").PHP_EOL;
    			}
	        while($staff = mysqli_fetch_assoc($staff_query)):
		        echo "{\"".$staff['s_no']."\":\"".$staff['s_name']."\"},".PHP_EOL;
	        endwhile;
	        echo "{\"selected\":\"\"}".PHP_EOL;
        } else {
	        echo "{\"\":\"::전체::\"}".PHP_EOL;
        }
        echo "]".PHP_EOL;
    }
}
?>
