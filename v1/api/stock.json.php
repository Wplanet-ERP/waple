<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "") {

    $parent_data_get = $_GET[$_GET['parent_data']];


    if($_GET['parent_data'] == "sch_team" || $_GET['parent_data'] == "sch_manager_team")
    {
        $team_code              = $_GET[$_GET['parent_data']];
        $sch_team_code_list 	= getTeamWhere($my_db, $team_code);
        $staff_team_where 		= "";
        $staff_team_where_list 	= [];

        if($team_code == 'all')
        {
            $sql="SELECT s.s_no, s.s_name FROM staff s WHERE s.staff_state ='1' ORDER BY s_name ASC";

        }else{
            if($sch_team_code_list){
                $sch_team_code_list_val = explode(",", $sch_team_code_list);

                foreach($sch_team_code_list_val as $sch_team_code){
                    $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
                }
            }

            if($staff_team_where_list){
                $staff_team_where = implode(" OR ", $staff_team_where_list);
            }

            $sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
        }


        $query=mysqli_query($my_db,$sql);
        $count=mysqli_num_rows($query);

        echo "[".PHP_EOL;
        if($count>0) {
            if($_GET['parent_data'] == "sch_team" || $_GET['parent_data'] == "sch_manager_team"){
                echo "{\"all\":\"::전체::\"}".(($count>0)?",":"").PHP_EOL;
            }
            while($result=mysqli_fetch_assoc($query)):
                echo "{\"".$result['s_no']."\":\"".$result['s_name']."\"},".PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}".PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}".PHP_EOL;
        }
        echo "]".PHP_EOL;
    }
    elseif ($_GET['parent_data'] == "sch_ord_s_no") # 발주담당자 선택시 공급업체
    {
        if(empty($parent_data_get)){
            $sql = "SELECT DISTINCT pcu.sup_c_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no), '(주)','') as c_name FROM product_cms_unit as pcu WHERE display='1' ORDER BY c_name ASC";
        }else{
            $sql = "SELECT DISTINCT pcu.sup_c_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no), '(주)','') as c_name FROM product_cms_unit as pcu WHERE display='1' AND (ord_s_no='{$parent_data_get}' OR ord_sub_s_no='{$parent_data_get}' OR ord_third_s_no='{$parent_data_get}') ORDER BY c_name ASC";
        }

        $query = mysqli_query($my_db, $sql);
        $count = mysqli_num_rows($query);

        echo "[" . PHP_EOL;
        if ($count > 0) {
            if ($_GET['parent_data'] == "sch_ord_s_no") {
                echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
            }
            while ($result = mysqli_fetch_assoc($query)):
                echo "{\"" . $result['sup_c_no'] . "\":\"" . $result['c_name'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;

    }
    else if ($_GET['parent_data'] == "sch_sup_c_no") #공급업체 선택시 구성품목명
    { // 상품 목록의 경우
        if(empty($parent_data_get)){
            $sql = "SELECT pcu.`no`, pcu.option_name FROM product_cms_unit as pcu WHERE pcu.display=1 ORDER BY option_name ASC";
        }else{
            $sql = "SELECT pcu.`no`, pcu.option_name FROM product_cms_unit as pcu WHERE pcu.display=1 AND pcu.sup_c_no='{$parent_data_get}' ORDER BY option_name ASC";
        }

        $query = mysqli_query($my_db, $sql);
        $count = mysqli_num_rows($query);

        echo "[" . PHP_EOL;
        if ($count > 0) {
            if ($_GET['parent_data'] == "sch_sup_c_no") {
                echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
            }

            while ($result = mysqli_fetch_assoc($query)):
                echo "{\"" . $result['no'] . "\":\"" . $result['option_name'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;

    }
    elseif ($_GET['parent_data'] == "sch_brand") # 브랜드 선택시 공급업체
    {
        if(empty($parent_data_get)){
            $sql   = "SELECT DISTINCT pcu.sup_c_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no), '(주)','') as c_name FROM product_cms_unit as pcu WHERE display='1' ORDER BY c_name ASC";
            $query = mysqli_query($my_db, $sql);
            $count = mysqli_num_rows($query);

            echo "[" . PHP_EOL;
            if ($count > 0) {
                if ($_GET['parent_data'] == "sch_brand") {
                    echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
                }
                while ($result = mysqli_fetch_assoc($query)):
                    echo "{\"" . $result['sup_c_no'] . "\":\"" . $result['c_name'] . "\"}," . PHP_EOL;
                endwhile;
                echo "{\"selected\":\"\"}" . PHP_EOL;
            } else {
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
            }
            echo "]" . PHP_EOL;

        }else{
            $brand = $parent_data_get;
            
            if($brand == '1314'){
                $brand_sup_sql    = "SELECT DISTINCT pcu.sup_c_no, (SELECT REPLACE(c.c_name, '(주)', '') FROM company as `c` WHERE c.c_no=pcu.sup_c_no) as c_name FROM product_cms_unit pcu WHERE pcu.brand IN(1314,3303) AND pcu.display='1' ORDER BY c_name";
            }else{
                $brand_sup_sql    = "SELECT DISTINCT pcu.sup_c_no, (SELECT REPLACE(c.c_name, '(주)', '') FROM company as `c` WHERE c.c_no=pcu.sup_c_no) as c_name FROM product_cms_unit pcu WHERE pcu.brand='{$brand}' AND pcu.display='1' ORDER BY c_name";
            }

            $brand_sup_query  = mysqli_query($my_db, $brand_sup_sql);
            while($brand_sup_result = mysqli_fetch_assoc($brand_sup_query))
            {
                $brand_sup_option[$brand][$brand_sup_result['sup_c_no']] = $brand_sup_result['c_name'];
            }

            $count = isset($brand_sup_option[$parent_data_get]) ? count($brand_sup_option[$parent_data_get]) : 0;

            echo "[" . PHP_EOL;
            if ($count > 0) {
                if ($_GET['parent_data'] == "sch_brand") {
                    echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
                }
                foreach($brand_sup_option[$parent_data_get] as $sup_c_no => $sup_name) {
                    echo "{\"" . $sup_c_no . "\":\"" . $sup_name . "\"}," . PHP_EOL;
                }
                echo "{\"selected\":\"\"}" . PHP_EOL;
            } else {
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
            }
            echo "]" . PHP_EOL;
        }
    }
    elseif ($_GET['parent_data'] == "sch_s_no")
    {
        if(empty($parent_data_get)){
            $sql = "SELECT DISTINCT pcu.sup_c_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no), '(주)','') as c_name FROM product_cms_unit as pcu WHERE display='1' ORDER BY c_name ASC";
        }else{
            $sql = "SELECT DISTINCT pcu.sup_c_no, REPLACE((SELECT c.c_name FROM company c WHERE c.c_no = pcu.sup_c_no), '(주)','') as c_name FROM product_cms_unit as pcu WHERE display='1' AND (ord_s_no='{$parent_data_get}' OR ord_sub_s_no='{$parent_data_get}' OR ord_third_s_no='{$parent_data_get}') ORDER BY c_name ASC";
        }

        $query = mysqli_query($my_db, $sql);
        $count = mysqli_num_rows($query);

        echo "[" . PHP_EOL;
        if ($count > 0) {
            if ($_GET['parent_data'] == "sch_s_no") {
                echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
            }
            while ($result = mysqli_fetch_assoc($query)):
                echo "{\"" . $result['sup_c_no'] . "\":\"" . $result['c_name'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;

    }
}
?>
