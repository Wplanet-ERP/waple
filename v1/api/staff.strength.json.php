<?php
require('../inc/common.php');

$s_no   = isset($_POST['s_no'])?$_POST['s_no']:"";
$result = false;

if(!!$s_no){
    // 리스트 쿼리
    $staff_strength_sql = "SELECT (SELECT s.s_name FROM staff s WHERE s.s_no = ss.s_no LIMIT 1) as s_name, ss.keyword, (SELECT ssk.strength FROM staff_strength_keyword ssk WHERE ssk.k_no = ss.keyword_no LIMIT 1) AS strength FROM staff_strength ss WHERE ss.s_no = '{$s_no}' ORDER BY ss.priority";

    $query = mysqli_query($my_db, $staff_strength_sql);
    $html  = "";
    $idx   = 0;

    while ($staff_strength = mysqli_fetch_assoc($query))
    {
        $s_name   = $staff_strength['s_name'];
        $keyword  = $staff_strength['keyword'];
        $strength = $staff_strength['strength'];

        if($idx == 0){
            $html .= "<h2>{$s_name}님의 강점</h2>";
        }

        $html .= "<h4>{$keyword}</h4><p>{$strength}</p>";
        $idx++;
    }

    $data = !empty($html) ? array("result" => true, "html" => $html) : array("result" => false, "html" => $html);
}else{
    $data = array("result" => false, "html" => "");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
