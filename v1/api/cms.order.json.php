<?php
require('../inc/common.php');

$w_no   = isset($_POST['w_no'])?$_POST['w_no']:"";
$result = false;
$order  = "";

if(!!$w_no){
    // 리스트 쿼리
    $ord_sql = "SELECT w_no, order_type, order_number, log_c_no, recipient, recipient_hp, zip_code, recipient_addr, delivery_memo, dp_c_no, order_date, payment_date, stock_date, prd_no, quantity, (SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no) as prd_name, notice, manager_memo FROM work_cms w WHERE w_no = '{$w_no}' LIMIT 1";

    $query = mysqli_query($my_db, $ord_sql);

    while ($ord_data = mysqli_fetch_assoc($query))
    {
        $order = $ord_data;
    }

    $data = !empty($order) ? array("result" => true, "order" => $order) : array("result" => false, "order" => "");
}else{
    $data = array("result" => false, "order" => "");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
