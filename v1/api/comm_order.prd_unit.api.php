<?php
require('../inc/common.php');

$prd_name   = isset($_GET['prd_name'])?$_GET['prd_name']:"";
$sup_c_no   = isset($_GET['sup_c_no'])?$_GET['sup_c_no']:"";

$product_cms_unit_sql="
		SELECT
			pcu.no,
			pcu.option_name,
			pcu.sup_price,
			pcu.sup_price_vat
		FROM product_cms_unit pcu
		WHERE pcu.sup_c_no='{$sup_c_no}' AND pcu.option_name like '%{$prd_name}%' AND pcu.display='1'
		ORDER BY pcu.priority ASC
";
$product_cms_unit_query    = mysqli_query($my_db, $product_cms_unit_sql);

while ($product_cms_unit = mysqli_fetch_assoc($product_cms_unit_query)) {
    $arr[] = array(
        "no"          => $product_cms_unit['no'],
        "label"       => $product_cms_unit['option_name'],
        "option_name" => $product_cms_unit['option_name'],
    );
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
