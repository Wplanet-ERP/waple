<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "") {

    $parent_data_get = $_GET[$_GET['parent_data']];

    $sql = "SELECT team_code, team_name from team WHERE team_code_parent = '{$parent_data_get}' ORDER BY priority ASC";

    $query = mysqli_query($my_db, $sql);
    $count = mysqli_num_rows($query);

    echo "[" . PHP_EOL;
    if ($count > 0) {
        if ($_GET['parent_data'] == "sch_department") {
            echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
        }
        while ($result = mysqli_fetch_assoc($query)):
            echo "{\"" . $result['team_code'] . "\":\"" . $result['team_name'] . "\"}," . PHP_EOL;
        endwhile;
        echo "{\"selected\":\"\"}" . PHP_EOL;
    } else {
        echo "{\"\":\"::전체::\"}" . PHP_EOL;
    }
    echo "]" . PHP_EOL;
}
?>
