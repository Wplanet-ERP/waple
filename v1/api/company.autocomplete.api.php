<?php
require('../inc/common.php');

# 검색 변수
$c_name         = isset($_GET['c_name']) ? $_GET['c_name'] : "";
$corp_kind      = isset($_GET['corp_kind']) ? $_GET['corp_kind'] : "";
$display        = isset($_GET['display']) ? $_GET['display'] : "1";
$add_where      = "1=1";

if(!empty($c_name)){
    $add_where .= " AND c_name LIKE '%{$c_name}%'";
}

if(!empty($corp_kind)){
    $add_where .= " AND corp_kind = '{$corp_kind}'";
}

if(!empty($display)){
    $add_where .= " AND display = '{$display}'";
}

# 검색 쿼리
$company_sql    = "SELECT c_no, c_name, s_no FROM company WHERE {$add_where} ";
$company_query  = mysqli_query($my_db, $company_sql);
$company_list   = [];
while ($company_result = mysqli_fetch_assoc($company_query))
{
    $company_list[] = $company_result;
}

# Return
echo json_encode($company_list, JSON_UNESCAPED_UNICODE);
?>
