<?php
require('../inc/common.php');
header("Content-Type: text/html; charset=UTF-8");

$blog_url  = isset($_POST['blog_url'])?$_POST['blog_url']:"";
$p_no      = isset($_POST['p_no'])?$_POST['p_no']:"";
$msg       = "URL 값이 없습니다";
$result    = false;

if(!empty($blog_url) && !empty($p_no))
{
    // 리스트 쿼리
    $blog_chk_url_sql    = "SELECT count(a.a_no) as cnt FROM application `a` WHERE a.blog_url='{$blog_url}' AND a.p_no = '{$p_no}' AND a.a_state='1'";
    $blog_chk_url_query  = mysqli_query($my_db, $blog_chk_url_sql);
    $blog_chk_url_result = mysqli_fetch_assoc($blog_chk_url_query);

    if($blog_chk_url_result['cnt'] > 0)
    {
        $result = true;
        $msg    = "확인 되었습니다";
    }else{
        $result = false;
        $msg    = "선정된 URL이 아닙니다";
    }
}

$data = array("result" => $result, "msg" => $msg);
echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>