<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "") {

    $parent_data_get = $_GET[$_GET['parent_data']];

    if ($_GET['parent_data'] == "write_permission_team_val" || $_GET['parent_data'] == "read_permission_team_val" || $_GET['parent_data'] == "manager_permission_team_val" || $_GET['parent_data'] == "new_permission_team" || $_GET['parent_data'] == "view_schedule_permission_team_val" || $_GET['parent_data'] == "calendar_permission_team_val")
    {
        if($parent_data_get) {
            $team_where = getTeamWhere($my_db, $parent_data_get);

            $sql = "SELECT s_no, s_name from staff WHERE staff_state='1' AND team IN({$team_where})";

        }else{
            $sql="SELECT s.s_no, s.s_name FROM staff s WHERE s.staff_state ='1' ORDER BY s_name ASC";
        }

        $query = mysqli_query($my_db, $sql);
        $count = mysqli_num_rows($query);

        if ($parent_data_get == '99999') {
            $count = 1;
        }

        echo "[" . PHP_EOL;
        if ($count > 0) {
            if ($_GET['parent_data'] == "write_permission_team_val" || $_GET['parent_data'] == "read_permission_team_val" || $_GET['parent_data'] == "manager_permission_team_val" || $_GET['parent_data'] == "new_permission_team" || $_GET['parent_data'] == "view_schedule_permission_team_val" || $_GET['parent_data'] == "calendar_permission_team_val") {
                echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
            }

            if ($parent_data_get == '99999') {
                echo "{\"179\":\"위드플레이스\"}," . PHP_EOL;
                echo "{\"249\":\"위드3\"}," . PHP_EOL;
            }else{
                while ($result = mysqli_fetch_assoc($query)) {
                    echo "{\"" . $result['s_no'] . "\":\"" . $result['s_name'] . "\"}," . PHP_EOL;
                }
            }

            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;

    }
}
?>
