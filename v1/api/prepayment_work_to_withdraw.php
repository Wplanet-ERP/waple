<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
<body>
<table border="1" cellpadding="0" cellspacing="0" style="text-align: center;" style="margin-top: 20px; border-color: #a7a7a7;">
    <colgroup>
        <col width="50px"; />
        <col width="100px"; />
        <col width="600px"; />
        <col width="50px"; />
        <col width="50px"; />
        <col width="600px"; />
        <col width="50px"; />
    </colgroup>
    <tr>
        <th>w_no</th>
        <th>c_name</th>
        <th>Insert Query</th>
        <th>Insert Result</th>
        <th>wd_no</th>
        <th>Update Query</th>
        <th>Update Result</th>
    </tr>
<?php
require('../inc/common.php');
$prepayment_sql     = "SELECT *, (SELECT price_apply FROM product prd WHERE prd.prd_no=w.prd_no) AS price_apply, (SELECT k_name FROM kind WHERE k_name_code=w.k_name_code) AS k_name, (SELECT regdate FROM withdraw wd where wd.wd_no=w.wd_no) as wd_regdate, (SELECT wd_date FROM withdraw wd where wd.wd_no=w.wd_no) as wd_date, (SELECT regdate FROM deposit dp where dp.dp_no=w.dp_no) as dp_regdate, (SELECT dp_date FROM deposit dp where dp.dp_no=w.dp_no) as dp_date, (SELECT s_name FROM staff s where s.s_no=w.task_req_s_no) as task_req_s_name, (SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name, (SELECT staff_state FROM staff s where s.s_no=w.task_run_s_no) as task_run_staff_state, (SELECT s_name from staff s where s.s_no=w.s_no) as s_no_name, (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1, (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1_name, (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2, (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2_name, (SELECT title from product prd where prd.prd_no=w.prd_no) as prd_no_name, (SELECT prd.wd_dp_state FROM product prd WHERE prd.prd_no = w.prd_no) as wd_dp_state, (SELECT prd.work_time_method FROM product prd WHERE prd.prd_no = w.prd_no) as work_time_method FROM work w WHERE 1=1 AND w.work_state = 6 AND w.prd_no in ('10','53') AND w.wd_c_no is not null AND w.wd_c_no != '2180' AND (w.task_run_regdate is not null OR w.task_run_regdate != '') AND (DATE_FORMAT(w.task_run_regdate,'%Y-%m-%d') BETWEEN '2019-01-01' AND '2020-01-20') AND w.wd_no is null ORDER BY w.w_no ASC";
$prepayment_query   = mysqli_query($my_db, $prepayment_sql);
$total_data         = [];
$total_data['367']  = array('title' => '오른웍스','count' => 0);
$total_data['383']  = array('title' => '(주)한국미디어리서치','count' => 0);
$total_data['527']  = array('title' => 'JB스퀘어', 'count' => 0);

while($work = mysqli_fetch_array($prepayment_query))
{
    if(isset($work['w_no']) && !empty($work['w_no']))
    {
        $w_no           = $work['w_no'];
        $s_no           = $work['s_no'];
        $c_name         = $work['c_name'];
        $wd_c_no        = $work['wd_c_no'];
        $wd_c_name      = $work['wd_c_name'];
        $wd_price       = $work['wd_price'];
        $wd_price_vat   = $work['wd_price_vat'];
        $wd_subject     = (isset($work['t_keyword']) && !empty($work['t_keyword'])) ? $c_name."::".$work['t_keyword'] : $c_name;


        //부가세와 부가세액 자동계산 처리
        $wd_price_vat_value = 0;
        $supply_cost_vat_value = 0;
        if($work['wd_price'] < $work['wd_price_vat']){ // 부가세가 크면 부가세 설정
            $wd_price_vat_value = 1;
            $supply_cost_vat_value = $work['wd_price_vat'] - $work['wd_price'];
        }elseif($work['wd_price'] > $work['wd_price_vat']){ // 부가세가 적은 건은 소득세처리
            $wd_price_vat_value = 2;
            $supply_cost_vat_value = $work['wd_price'] - $work['wd_price_vat'];
        }elseif($work['wd_price'] == $work['wd_price_vat']){ // 부가세가 동일하면 동일처리
            $wd_price_vat_value = 3;
            $supply_cost_vat_value = 0;
        }

        $insert_sql = "
            INSERT INTO withdraw SET
                wd_subject = '{$wd_subject}',
                w_no = '{$w_no}',
                s_no = '{$s_no}',
                my_c_no = '1',
                c_no = '{$wd_c_no}',
                c_name = '{$wd_c_name}',
                team = (SELECT s.team	FROM staff s WHERE s_no = '{$s_no}'),
                cost = '{$wd_price_vat}',
                supply_cost = '{$wd_price}',
                vat_choice = '{$wd_price_vat_value}',
                supply_cost_vat = '{$supply_cost_vat_value}',
                wd_state = '6',
                wd_method = '5',
                wd_tax_state = '1',
                regdate = now(),
                reg_s_no = '18'
            ";

        echo "<tr><td>{$w_no}</td><td>{$c_name}</td>";

        if (!mysqli_query($my_db, $insert_sql))
            echo "<td>{$insert_sql}</td><td colspan='4'>실패</td>";
        else{
            echo "<td>{$insert_sql}</td><td>성공</td>";

            $withdraw_query  = mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
            $withdraw_result = mysqli_fetch_array($withdraw_query);
            $last_wd_no      = $withdraw_result[0];

            $wd_no_update_sql = "UPDATE work w SET w.wd_no = '" . $last_wd_no . "' WHERE w.w_no = '" . $w_no . "'";

            if (!mysqli_query($my_db, $wd_no_update_sql))
                echo "<td>{$last_wd_no}</td><td>{$wd_no_update_sql}</td><td>실패</td>";
            else
                echo "<td>{$last_wd_no}</td><td>{$wd_no_update_sql}</td><td>성공</td>";
        }
        $i++;

        if(isset($total_data[$wd_c_no]))
            $total_data[$wd_c_no]['count']++;

        echo "</tr>";
    }
}

echo "<tr><td colspan='7' style='padding: 50px; font-size:15px; font-weight: bold;'>전체 선결제 수 : {$i} , {$total_data['367']['title']}: {$total_data['367']['count']}, {$total_data['383']['title']}: {$total_data['383']['count']}, {$total_data['527']['title']}: {$total_data['527']['count']}</td></tr>";
?>
</table>
</body>
</html>
