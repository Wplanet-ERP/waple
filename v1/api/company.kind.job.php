<?php
require('../inc/common.php');

$f_corp_kind = (isset($_GET['f_corp_kind'])) ? $_GET['f_corp_kind'] : "";

if($f_corp_kind == '1')
{
    $sql="select * from kind where k_code='job' and k_parent is null";
    $query=mysqli_query($my_db,$sql);
    $count=mysqli_num_rows($query);
}
else
{
    $sql="select * from kind where k_code='company_job' and k_parent is null";
    $query=mysqli_query($my_db,$sql);
    $count=mysqli_num_rows($query);
}

$html = "<option value=''>::전체::</option>";

while($result=mysqli_fetch_assoc($query))
{
    $html .= "<option value='{$result['k_name_code']}'>{$result['k_name']}</option>";
}

$data = array("html" => $html);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>