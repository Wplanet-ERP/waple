<?php
require('../inc/common.php');
require('../ckadmin.php');

header("Content-Type: text/html; charset=UTF-8");

$page        = isset($_POST['page'])?$_POST['page']:1;
$asset_list  = [];
$result      = false;

if(!empty($page))
{
    $staff_sql = "SELECT quick_asset FROM staff WHERE s_no='{$session_s_no}' LIMIT 1";
    $staff_query = mysqli_query($my_db, $staff_sql);
    $staff_result = mysqli_fetch_assoc($staff_query);

    if(isset($staff_result['quick_asset']) && !empty($staff_result['quick_asset']))
    {
        $quick_asset = $staff_result['quick_asset'];
        $offset      = $page*10;
        $page_num    = 10;

        if(!empty($quick_asset))
        {
            // 리스트 쿼리
            $asset_sql = "SELECT `a`.as_no, IFNULL(`a`.object_url,'') as object_url, IFNULL(`a`.object_site, '') as object_site, IFNULL(`a`.object_id, '') as object_id, IFNULL(`a`.object_pw,'') as object_pw, IFNULL(`a`.notice, '') as notice  FROM asset `a` WHERE as_no IN({$quick_asset}) AND display='1' LIMIT {$offset}, {$page_num}";
            $asset_query = mysqli_query($my_db, $asset_sql);
            while($asset = mysqli_fetch_assoc($asset_query))
            {
                $asset_list[] = $asset;
            }
        }

        if(!empty($asset_list)){
            $result = true;
        }

    }
}

$data = array('result' => $result, 'asset_list' => $asset_list);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>