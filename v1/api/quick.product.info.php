<?php
require('../inc/common.php');
require('../ckadmin.php');

$prd_no         = isset($_POST['prd_no']) ? $_POST['prd_no'] : "";
$prd_type       = isset($_POST['prd_type']) ? $_POST['prd_type'] : "";
$prd_title      = "";
$option_html    = "";
$with_log_c_no  = '2809';

if(!empty($prd_no))
{
    if($prd_type == 'cms_with'){
        $prd_chk_sql    = "SELECT (SELECT p.description FROM product_cms p WHERE p.prd_no='{$prd_no}') as description, pcum.sku as option_name, pr.quantity FROM product_cms_unit_management pcum LEFT JOIN product_cms_relation pr ON pr.option_no=pcum.prd_unit WHERE pr.prd_no='{$prd_no}' AND pr.display='1' AND pcum.log_c_no='{$with_log_c_no}'";
    }else{
        $prd_chk_sql    = "SELECT (SELECT p.description FROM product_cms p WHERE p.prd_no='{$prd_no}') as description, pcu.option_name, pr.quantity FROM product_cms_unit pcu LEFT JOIN product_cms_relation pr ON pr.option_no=pcu.`no` WHERE pr.prd_no='{$prd_no}' AND pr.display='1'";
    }

    $prd_chk_query  = mysqli_query($my_db, $prd_chk_sql);
    while($prd_chk_result = mysqli_fetch_assoc($prd_chk_query))
    {
        $prd_title   = "[상품소개] {$prd_chk_result['description']}";
        $option_html .= !empty($option_html) ? "<br/><span>[구성품] {$prd_chk_result['option_name']} * {$prd_chk_result['quantity']}</span>" : "<span>[구성품] {$prd_chk_result['option_name']} * {$prd_chk_result['quantity']}</span>";
    }
}

$quick_data = array("prd_title" => $prd_title, "option_html" => $option_html);

echo json_encode($quick_data, JSON_UNESCAPED_UNICODE);

?>
