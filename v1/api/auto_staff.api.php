<?php
require('../inc/common.php');
require('../ckadmin.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$s_name  = isset($_GET['s_name'])?$_GET['s_name']:"";
$add_where = "1=1 ";

if(!empty($s_name)) {
	$add_where.=" AND s_name like '%".$s_name."%' AND staff_state='1'";
}

// 정렬순서 토글 & 필드 지정
$add_orderby.=" s_name ASC";

// 리스트 쿼리
$staff_sql="
		SELECT
			s_no,
			s_name,
			email,
			my_c_no
		FROM
			staff
		WHERE
			$add_where
		ORDER BY
			$add_orderby
	";


$result = mysqli_query($my_db, $staff_sql);

while ($staff = mysqli_fetch_assoc($result)) {
    $arr[] = array(
        "s_no" 		=> $staff['s_no'],
        "s_name" 	=> $staff['s_name'],
        "email"	 	=> $staff['email'],
        "my_c_no"	=> $staff['my_c_no']
    );
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
