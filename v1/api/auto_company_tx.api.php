<?php
require('../inc/common.php');
require('../ckadmin.php');


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="1=1";
$tx_company_get=isset($_GET['tx_company'])?$_GET['tx_company']:"";
$bk_manager_get=isset($_GET['bk_manager'])?$_GET['bk_manager']:"";
$tx_company_ceo_get=isset($_GET['tx_company_ceo'])?$_GET['tx_company_ceo']:"";

$s_no_get=isset($_GET['s_no'])?$_GET['s_no']:"";

if(!empty($tx_company_get)) {
	$add_where.=" AND tx_company like '%".$tx_company_get."%'";
}
if(!empty($bk_manager_get)) {
	$add_where.=" AND bk_manager like '%".$bk_manager_get."%'";
}
if(!empty($tx_company_ceo_get)) {
	$add_where.=" AND tx_company_ceo like '%".$tx_company_ceo_get."%'";
}


// 인큐베이팅본부의 경우 담당자가 입금 단위로 변경 가능하므로 타 담당자 조회가능
// 재무관리자, 외주관리자, 마스터관리자 또한 타담장자 조회가능
if( empty($s_no_get) || $s_no_get == '8' || $s_no_get == '11' || $s_no_get == '9' || $s_no_get == '10' || $s_no_get == '38' || $s_no_get == '39'
 	|| permissionNameCheck($session_permission, "재무관리자") || permissionNameCheck($session_permission, "외주관리자") || permissionNameCheck($session_permission, "마스터관리자") || permissionNameCheck($session_permission, "서비스운영")
	){
}else{
	$add_where.=" AND s_no='".$s_no_get."'";
}


// 정렬순서 토글 & 필드 지정
if(!empty($tx_company_get))
	$add_orderby.=" tx_company ASC";
elseif(!empty($bk_manager_get))
	$add_orderby.=" bk_manager ASC";
elseif(!empty($tx_company_ceo_get))
		$add_orderby.=" tx_company_ceo ASC";

// 리스트 쿼리
$company_sql="
				SELECT
					c_no, c_name, tx_company, bk_manager, tx_company_ceo, s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=c.s_no) AS s_name, (SELECT s.team FROM staff s WHERE s.s_no=c.s_no) AS team, (SELECT t.depth FROM team t WHERE t.team_code =(SELECT s.team FROM staff s WHERE s.s_no=c.s_no)) AS t_depth
				FROM
					company c
				WHERE
					$add_where
					AND c.display = '1'
				ORDER BY
					$add_orderby
			";

$result = mysqli_query($my_db,$company_sql);
while ($company_array = mysqli_fetch_assoc($result)) {

    $t_label_list = [];
    $t_depth = isset($company_array['t_depth']) ? $company_array['t_depth'] : 1;
    $t_code  = $company_array['team'];

    for($i=0; $i<$t_depth; $i++)
    {
        if(empty($t_code)){
            break;
        }
        $t_label_sql    = "SELECT t.team_name, t.team_code_parent FROM team t WHERE t.team_code='{$t_code}'";
        $t_label_query  = mysqli_query($my_db, $t_label_sql);
        $t_label_result = mysqli_fetch_assoc($t_label_query);

        $t_code = $t_label_result['team_code_parent'];
        $t_label_list[$i] = $t_label_result['team_name'];
    }

    krsort($t_label_list);
    $t_label = implode(" > ", $t_label_list);
    $s_label = "{$company_array['s_name']} ({$t_label})";

	$arr[] = array(
		"c_no"				=> $company_array['c_no'],
		"c_name"			=> $company_array['c_name'],
		"s_no"				=> $company_array['s_no'],
		"s_name"			=> $company_array['s_name'],
		"s_label"			=> $s_label,
		"team"				=> $company_array['team'],
		"tx_company"		=> $company_array['tx_company'],
		"bk_manager"		=> $company_array['bk_manager'],
		"tx_company_ceo"	=> $company_array['tx_company_ceo']
	);
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
