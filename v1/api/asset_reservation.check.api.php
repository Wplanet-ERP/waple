<?php
require('../inc/common.php');
header("Content-Type: text/html; charset=UTF-8");

$as_no       = isset($_POST['as_no'])?$_POST['as_no']:"";
$as_r_no     = isset($_POST['as_r_no'])?$_POST['as_r_no']:"";
$asset_name  = isset($_POST['asset_name'])?$_POST['asset_name']:"";
$s_date      = isset($_POST['r_s_date'])?$_POST['r_s_date']:"";
$e_date      = isset($_POST['r_e_date'])?$_POST['r_e_date']:"";

$cur_time_msg = "";
if($as_no == '1062' & !empty($s_date) && !empty($e_date)){ // 강의장의 경우
  $s_date = date("Y-m-d H:i:s", strtotime($s_date.'-1 hours'));
  $e_date = date("Y-m-d H:i:s", strtotime($e_date.'+1 hours'));
  $cur_time_msg = "<div style='color:red;'>※{$asset_name}은 1시간 전/후 예약이 불가능 합니다.</div>";
}

$html        = "<h2 style='color: red;'>이미 예약된 시간이 있습니다</h2>{$cur_time_msg}";

if(!empty($as_r_no) && !empty($as_no))
{
    $asset     = "";

    $add_where = "`asr`.as_r_no != '{$as_r_no}' AND `asr`.as_no = '{$as_no}' AND `asr`.work_state IN('1','2') AND ((`asr`.r_s_date >= '{$s_date}' AND `asr`.r_s_date < '{$e_date}') OR (`asr`.r_e_date > '{$s_date}' AND `asr`.r_e_date <= '{$e_date}') OR (`asr`.r_s_date < '{$s_date}' AND `asr`.r_e_date >= '{$e_date}'))";

    // 리스트 쿼리
    $asset_sql = "
      SELECT
       `asr`.r_s_date,
       `asr`.r_e_date
      FROM asset_reservation `asr` WHERE {$add_where} ORDER BY `asr`.r_s_date
    ";

    $asset_query = mysqli_query($my_db, $asset_sql);
    while ($asset_result = mysqli_fetch_assoc($asset_query))
    {
        $asset = $asset_result;
        $html .= "<p>{$asset_result['r_s_date']} ~ {$asset_result['r_e_date']} : 예약됨</p>";
    }

    $data = !empty($asset) ? array("result" => false, "html" => $html) : array("result" => true, "html" => "");
}
elseif(!empty($as_no))
{

    $asset     = "";
    $add_where = "`asr`.as_no = '{$as_no}' AND `asr`.work_state IN('1','2') AND ((`asr`.r_s_date >= '{$s_date}' AND `asr`.r_s_date < '{$e_date}') OR (`asr`.r_e_date > '{$s_date}' AND `asr`.r_e_date <= '{$e_date}') OR (`asr`.r_s_date < '{$s_date}' AND `asr`.r_e_date >= '{$e_date}'))";

    // 리스트 쿼리
    $asset_sql = "
      SELECT
       `asr`.r_s_date,
       `asr`.r_e_date
      FROM asset_reservation `asr` WHERE {$add_where} ORDER BY `asr`.r_s_date
    ";

    $asset_query = mysqli_query($my_db, $asset_sql);
    while ($asset_result = mysqli_fetch_assoc($asset_query))
    {
        $asset = $asset_result;
        $html .= "<p>{$asset_result['r_s_date']} ~ {$asset_result['r_e_date']} : 예약됨</p>";
    }

    $data = !empty($asset) ? array("result" => false, "html" => $html) : array("result" => true, "html" => "");
}else{
    $data = array("result" => false, "html" => "오류발생 다시 시도해 주세요");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
