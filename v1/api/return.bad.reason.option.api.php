<?php
require('../inc/common.php');
require('../inc/model/Kind.php');

$kind_model         = Kind::Factory();
$bad_reason_g1_list = $kind_model->getKindParentList("bad_reason");

$html           = "";
$prd_no         = isset($_POST['prd_no'])?$_POST['prd_no']:"";
$unit_bad_sql   = "SELECT DISTINCT bad_reason FROM product_cms_unit as pcu WHERE pcu.`no` IN(SELECT pr.option_no FROM product_cms_relation pr WHERE pr.prd_no='{$prd_no}' AND pr.display='1') ORDER BY bad_reason ASC";
$unit_bad_query = mysqli_query($my_db, $unit_bad_sql);
$unit_bad_list  = [];
while($unit_bad_result = mysqli_fetch_assoc($unit_bad_query))
{
    if(isset($bad_reason_g1_list[$unit_bad_result['bad_reason']]))
    {
        $unit_bad_list[$unit_bad_result['bad_reason']] = $bad_reason_g1_list[$unit_bad_result['bad_reason']];
    }
}

$bad_g1_list = !empty($unit_bad_list) ? $unit_bad_list : $bad_reason_g1_list;

if(!empty($bad_g1_list)){
    foreach($bad_g1_list as $key => $label){
        $html .= "<option value='{$key}'>{$label}</option>>";
    }
}

$arr = array(
    'result'    => true,
    'html'      => $html
);

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
