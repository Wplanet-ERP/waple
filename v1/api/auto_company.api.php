<?php
require('../inc/common.php');
require('../ckadmin.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where 	="1=1";
$c_name_get	= isset($_GET['c_name']) ? $_GET['c_name'] : "";
$s_no_get	= isset($_GET['s_no']) ? $_GET['s_no'] : "";
$f_all_get	= isset($_GET['f_all']) ? $_GET['f_all'] : "";
$is_dp_get	= isset($_GET['is_dp']) ? $_GET['is_dp'] : "";

if(!empty($c_name_get)) {
	$add_where .= " AND c_name like '%{$c_name_get}%'";
}

if(empty($s_no_get)){

}elseif($f_all_get == 'true'){

}else{
	$add_where .= " AND s_no='{$s_no_get}'";
}

if(!empty($is_dp_get)) {
	$add_where .= " AND dp_e_type='1'";
}

// 리스트 쿼리
$company_sql = "
	SELECT
		my_c_no,
		(SELECT m.c_name FROM my_company as m WHERE m.my_c_no=c.my_c_no) as my_c_name,
		(SELECT m.c_name FROM my_company as m WHERE m.my_c_no=c.my_c_no) as my_c_name,
		c_no,
		(SELECT CONCAT (c_name, ' :: ', (SELECT s.s_name FROM staff s WHERE s.s_no=c.s_no))) AS sch_c_name,
		c_name,
		tx_company,
		bk_manager,
		tx_company_ceo,
		s_no,
		(SELECT s.team FROM staff s WHERE s.s_no=c.s_no) AS team,
		(SELECT t.team_name FROM team t WHERE t.team_code =(SELECT s.team FROM staff s WHERE s.s_no=c.s_no)) AS team_name,
		(SELECT t.depth FROM team t WHERE t.team_code =(SELECT s.team FROM staff s WHERE s.s_no=c.s_no)) AS t_depth,
		(SELECT s.s_name FROM staff s WHERE s.s_no=c.s_no) AS s_name,
		bk_title,
		bk_name,
		bk_num,
		lector_vat_type
	FROM company c
	WHERE {$add_where} AND c.display = '1'
	ORDER BY c_name ASC
";

$result = mysqli_query($my_db,$company_sql);
while ($company_array = mysqli_fetch_assoc($result)) {

    $t_label_list = [];
	$t_depth = isset($company_array['t_depth']) ? $company_array['t_depth'] : 1;
    $t_code  = $company_array['team'];

    for($i=0; $i<$t_depth; $i++)
    {
        if(empty($t_code)){
            break;
        }
        $t_label_sql    = "SELECT t.team_name, t.team_code_parent FROM team t WHERE t.team_code='{$t_code}'";
        $t_label_query  = mysqli_query($my_db, $t_label_sql);
        $t_label_result = mysqli_fetch_assoc($t_label_query);

        $t_code = $t_label_result['team_code_parent'];
        $t_label_list[$i] = $t_label_result['team_name'];
    }
    
    $ss_label = "{$company_array['s_name']} ({$t_label_list[0]})";
    krsort($t_label_list);
    $t_label = implode(" > ", $t_label_list);

    $s_label = "{$company_array['s_name']} ({$t_label})";
	$arr[] = array(
		"my_c_no"		=> $company_array['my_c_no'],
		"my_c_name"		=> $company_array['my_c_name'],
		"c_no"			=> $company_array['c_no'],
		"sch_c_name"	=> $company_array['sch_c_name'],
		"c_name"		=> $company_array['c_name'],
		"tx_company"	=> $company_array['tx_company'],
		"bk_manager"	=> $company_array['bk_manager'],
		"tx_company_ceo"=> $company_array['tx_company_ceo'],
		"s_no"   		=> $company_array['s_no'],
		"s_name" 		=> $company_array['s_name'],
		"s_label" 		=> $s_label,
		"ss_label" 		=> $ss_label,
		"team"   		=> $company_array['team'],
		"team_name" 	=> $company_array['team_name'],
		"bk_title"   	=> $company_array['bk_title'],
		"bk_name"   	=> $company_array['bk_name'],
		"bk_num"   		=> $company_array['bk_num'],
		"lec_vat_type"	=> $company_array['lector_vat_type'],
	);
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
