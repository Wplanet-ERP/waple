<?php
require('../inc/common.php');
require('../ckadmin.php');
header("Content-Type: text/html; charset=UTF-8");

$t_no       = isset($_POST['t_no']) ? $_POST['t_no'] : "";
$temp_type  = "";
$subject    = "";
$send_name  = "";
$send_phone = "";
$content    = "";

if(!empty($t_no))
{
    $temp_sql   = "SELECT * FROM crm_template WHERE t_no='{$t_no}' LIMIT 1";
    $temp_query = mysqli_query($my_db, $temp_sql);
    $temp_result = mysqli_fetch_assoc($temp_query);
    $temp_type  = isset($temp_result['temp_type']) ? $temp_result['temp_type'] : "";
    $send_name  = isset($temp_result['send_name']) ? $temp_result['send_name'] : "";
    $send_phone = isset($temp_result['send_phone']) ? $temp_result['send_phone'] : "";
    $subject    = isset($temp_result['subject']) ? $temp_result['subject'] : "";
    $content    = isset($temp_result['content']) ? $temp_result['content'] : "";
}

$data = array('temp_type' => $temp_type, 'subject' => $subject, 'send_name' => $send_name, 'send_phone' => $send_phone, 'content' => $content);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>