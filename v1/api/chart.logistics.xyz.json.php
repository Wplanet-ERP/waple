<?php
require('../inc/common.php');
require('../inc/helper/_date.php');
require('../inc/helper/logistics.php');
require('../inc/model/ProductCmsUnit.php');

if(($parent_type = $_GET['parent_data']) && !empty($_GET[$_GET['parent_data']]))
{
    $type           = $_GET[$parent_type];
    $type_day       = getDayChartOption();
    $type_subject   = getSubjectNameOption();
    $product_model  = ProductCmsUnit::Factory();
    $type_unit      = $product_model->getLogisticsUnitList();

    switch($type)
    {
        case 'day':
            echo "[" . PHP_EOL;
            foreach($type_day as $key => $label){
                if($key == 3){
                    echo "{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
                }else{
                    echo "{\"" . $key . "\":\"" . $label . "\"}," . PHP_EOL;
                }
            }
            echo "]" . PHP_EOL;
            break;
        case 'subject':
            echo "[" . PHP_EOL;
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
            foreach($type_subject as $key => $label){
                echo ",{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
            }
            echo "]" . PHP_EOL;
            break;
        case 'unit':
            echo "[" . PHP_EOL;
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
            foreach($type_unit as $key => $label){
                echo ",{\"" . $key . "\":\"" . $label. "\"}" . PHP_EOL;
            }
            echo "]" . PHP_EOL;
            break;
    }
}

?>
