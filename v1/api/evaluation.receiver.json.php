<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/model/Team.php');

if ($_GET['parent_data'] != "")
{
    $team_model             = Team::Factory();
    $team_all_list          = $team_model->getTeamAllList();
    $team_all_staff_list    = $team_all_list['staff_list'];

    $team_code              = $_GET[$_GET['parent_data']];
    $sch_staff_list         = $team_all_staff_list[$team_code];

    $count                  = count($sch_staff_list);

    echo "[" . PHP_EOL;
    if ($team_code && $count > 0) {
        if ($_GET['parent_data'] == "sch_receiver_team" || $_GET['parent_data'] == "sch_evaluator_team" ) {
            echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
        }
        foreach ($sch_staff_list as $key => $label):
            echo "{\"" . $key . "\":\"" . $label . "\"}," . PHP_EOL;
        endforeach;
        echo "{\"selected\":\"\"}" . PHP_EOL;
    } else {
        echo "{\"\":\"::전체::\"}" . PHP_EOL;
    }
    echo "]" . PHP_EOL;
}
?>
