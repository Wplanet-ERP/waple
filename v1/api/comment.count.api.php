<?php
require('../inc/common.php');
require('../ckadmin.php');
header("Content-Type: text/html; charset=UTF-8");

$b_id       = isset($_POST['b_id']) ? $_POST['b_id'] : "";
$result     = false;
$msg        = "";
$regdate    = date("Y-m-d H:i:s");

if(!empty($session_s_no) && $b_id == 'event')
{
    $chk_date  = date('mdHi',strtotime($regdate));

    # 댓글 체크
    if($chk_date >= 11052330 && $chk_date < 11060000)
    {
        $chk_sql    = "SELECT count(*) as cnt FROM `comment` WHERE `table_name`='board_normal' AND b_id='event' AND s_no='{$session_s_no}' AND (regdate >= '2020-11-05 23:30:00' AND regdate < '2020-11-06 00:00:00') AND display='1'";
        $chk_query  = mysqli_query($my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);
        $chk_count  = isset($chk_result['cnt']) ? $chk_result['cnt'] : 0;

        if($chk_count > 4){
            $result = true;
            $msg = "5개이상 등록할 수 없습니다";
        }
    }elseif($chk_date >= 11060000) #11/6일부터 막음
    {
        if($session_s_no != '18') {
            $result = true;
            $msg = "자산경매 이벤트가 종료되었습니다.";
        }
    }

}else{
    $msg = "오류가 발생했습니다";
}

$data = array("result" => $result, "msg" => $msg);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>