<?php
require('../inc/common.php');
require('../ckadmin.php');

$parent_data_get = $_GET[$_GET['parent_data']];

if ($_GET['parent_data'] == "f_sup_c_no_new")
{
    $chk_sql    = "SELECT DISTINCT log_c_no, (SELECT c.c_name FROM company c WHERE c.c_no=pcum.log_c_no) as log_c_name FROM product_cms_unit_management pcum WHERE pcum.prd_unit IN(SELECT pcu.`no` FROM product_cms_unit pcu WHERE pcu.sup_c_no='{$parent_data_get}' AND (pcu.ord_s_no='{$session_s_no}' OR pcu.ord_sub_s_no='{$session_s_no}' OR pcu.ord_third_s_no='{$session_s_no}'))";
    $chk_query  = mysqli_query($my_db, $chk_sql);
    $chk_count  = mysqli_num_rows($chk_query);

    echo "[" . PHP_EOL;
    if ($chk_count > 0) {
        if ($_GET['parent_data'] == "f_sup_c_no_new") {
            echo "{\"\":\"::전체::\"}" . (($chk_count > 0) ? "," : "") . PHP_EOL;
        }
        while ($result = mysqli_fetch_assoc($chk_query)):
            echo "{\"" . $result['log_c_no'] . "\":\"" . $result['log_c_name'] . "\"}," . PHP_EOL;
        endwhile;
        echo "{\"selected\":\"\"}" . PHP_EOL;
    } else {
        echo "{\"\":\"::전체::\"}" . PHP_EOL;
    }
    echo "]" . PHP_EOL;
}
?>