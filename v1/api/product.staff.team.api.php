<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$name_get = isset($_GET['name'])?$_GET['name']:"";

$staff_sql = "SELECT s_no, s_name, team_list FROM staff WHERE staff_state='1' AND s_name like '%{$name_get}%'";
$staff_query = mysqli_query($my_db, $staff_sql);
while($staff = mysqli_fetch_assoc($staff_query))
{
    $s_no       = $staff['s_no'];
    $s_name     = $staff['s_name'];
    $team_list  = $staff['team_list'];

    if($team_list)
    {
        $team_list = explode(',', $team_list);
        foreach($team_list as $team_code)
        {
            $t_depth_sql    = "SELECT t.depth FROM team t WHERE t.team_code='{$team_code}' LIMIT 1";
            $t_depth_query  = mysqli_query($my_db, $t_depth_sql);
            $t_depth_result = mysqli_fetch_assoc($t_depth_query);
            $t_depth        = $t_depth_result['depth'];

            $t_code       = $team_code;
            $t_label_list = [];
            for($i=0; $i<$t_depth; $i++)
            {
                if(empty($t_code)){
                    break;
                }
                $t_label_sql    = "SELECT t.team_name, t.team_code_parent FROM team t WHERE t.team_code='{$t_code}'";
                $t_label_query  = mysqli_query($my_db, $t_label_sql);
                $t_label_result = mysqli_fetch_assoc($t_label_query);

                $t_code = $t_label_result['team_code_parent'];
                $t_label_list[$i] = $t_label_result['team_name'];
            }

            if($t_label_list)
            {
                krsort($t_label_list);
                $t_code  = $s_no."_".$team_code;
                $t_label = $s_name." (".implode(" > ",$t_label_list).")";

                $arr[] = array(
                    "code"  => $t_code,
                    "label"   => $t_label,
                );
            }
        }
    }
}
// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
