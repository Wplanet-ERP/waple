<?php
require('../inc/common.php');
require('../ckadmin.php');

$b_no       = isset($_POST['b_no'])?$_POST['b_no']:"";
$result     = false;
$guide_html = "";

if(!empty($b_no))
{
    $modal_guide_sql    = "
        SELECT 
           *, 
           (SELECT s.s_name FROM staff s WHERE s.s_no=`bg`.manager) AS manager_name, 
           (SELECT count(bgr.r_no) FROM board_guide_read bgr WHERE bgr.read_s_no='{$session_s_no}' AND bgr.b_no = bg.b_no) as read_cnt 
        FROM board_guide bg WHERE bg.b_no='{$b_no}' AND bg.display = '1' 
        ORDER BY bg.b_no DESC
    ";
    $modal_guide_query  = mysqli_query($my_db, $modal_guide_sql);
    $modal_guide        = mysqli_fetch_assoc($modal_guide_query);

    if($modal_guide['b_no'])
    {
        $modal_file_names  = isset($modal_guide['file_name']) && !empty($modal_guide['file_name']) ? explode(',', $modal_guide['file_name']) : [];
        $modal_file_paths  = isset($modal_guide['file_path']) && !empty($modal_guide['file_path']) ? explode(',', $modal_guide['file_path']) : [];
        $modal_file_count  = !empty($modal_file_paths) && !empty($modal_file_paths) ? count($modal_file_paths) : 0;

        $hit            = (int)$modal_guide['hit']+1;
        $hit_upd_sql    = "UPDATE board_guide SET hit={$hit} WHERE b_no='{$b_no}'";
        mysqli_query($my_db, $hit_upd_sql);

        if($modal_guide['read_cnt'] == '0'){
            $read_upd_sql = "INSERT INTO board_guide_read SET b_no='{$b_no}', read_s_no='{$session_s_no}', read_date=now()";
            mysqli_query($my_db, $read_upd_sql);
        }

        $guide_html = "
        <h2 style='margin-bottom: 10px;'>{$modal_guide['question']}</h2>
        <div style='clear: both; overflow: hidden;'>
            <span style='display: inline-block; float: left; width: 60px;'>담당자 :</span>
            <span style='display: inline-block; float: left; margin-left: 7px;'>{$modal_guide['manager_name']}</span>
        </div>
        <div style='clear: both;overflow: hidden;'>
            <div style='float: left; height: auto; width: 60px;'>첨부파일 :</div>
            <div style='float: left; margin-left: 7px;'>
    ";

        if($modal_file_count > 0)
        {
            foreach($modal_file_paths as $key => $file_path)
            {
                $file_name = $modal_file_names[$key];

                $guide_html .= "<a type='application/octet-stream' href='/v1/uploads/{$file_path}' download='{$file_name}>' title='<{$file_name}>'>{$file_name}</a><br/>";
            }
            $guide_html .= "</div></div>";
        }else{
            $guide_html .= "</div></div>";
        }

        $guide_html .= "<div style='margin: 15px 0 10px; border: 1px solid #ccc; padding: 12px; max-height: 600px; min-height: 400px; overflow-y: scroll;'>".$modal_guide['contents']."</div>";
        $guide_html .= "<div>
                        <button type='button' onclick='closeGuideModalContent()' class='btn btn-dark font-weight-bold' style='float: left;font-size: 12px;background: #333;color: white;padding: 3px 7px;border-radius: 3px;font-weight: bold;'><span>뒤로가기</span></button>
                        <button type='button' onclick='moveGuideContent({$b_no})' class='btn btn-dark font-weight-bold' style='float: right;font-size: 12px;background: #333;color: white;padding: 3px 7px;border-radius: 3px;font-weight: bold;'><span>와플 GUIDE 상세페이지 보기</span></button>
                    </div>";
        $guide_html .= "</div>";

        $result = true;
    }
}

$data = array(
    'result'    => $result,
    'html'      => $guide_html
);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
