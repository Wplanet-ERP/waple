<?php
require('../inc/common.php');
require('../ckadmin.php');

$add_where      = "`cu`.display='1'";
$sch_type       = isset($_POST['sch_type']) ? $_POST['sch_type'] : "";
$sch_brand_g1   = isset($_POST['sch_brand_g1']) ? $_POST['sch_brand_g1'] : "";
$sch_brand_g2   = isset($_POST['sch_brand_g2']) ? $_POST['sch_brand_g2'] : "";
$sch_brand      = isset($_POST['sch_brand']) ? $_POST['sch_brand'] : "";
$sch_dp_c_no    = isset($_POST['sch_dp_c_no']) ? $_POST['sch_dp_c_no'] : "";
$sch_url_list   = [];

if(!empty($sch_type)){
    $add_where  .= " AND `cu`.url LIKE 'idx=%'";
}

if(!empty($sch_brand)){
    $add_where  .= " AND `cu`.brand='{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)){
    $add_where  .= " AND `cu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)){
    $add_where  .= " AND `cu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

if(!empty($sch_dp_c_no)) {
    $add_where  .= " AND `cu`.dp_c_no='{$sch_dp_c_no}'";
}

# URL 리스트
$sch_url_sql    = "SELECT url_no, page_title, url, REPLACE(url,'idx=','') as chk_url FROM commerce_url as `cu` WHERE {$add_where} ORDER BY LENGTH(chk_url) ASC, chk_url";
$sch_url_query  = mysqli_query($my_db, $sch_url_sql);
while($sch_url_result = mysqli_fetch_assoc($sch_url_query))
{
    $sch_url_list[$sch_url_result['url_no']] = $sch_url_result['page_title']." :: ".$sch_url_result['url'];
}

$select_option_html = "<option value=''>::전체::</option>";
if (!empty($sch_url_list))
{
    foreach ($sch_url_list as $key => $label) {
        $select_option_html .= "<option value='{$key}'>{$label}</option>";
    }
}

echo json_encode(array("select_option_html" => $select_option_html));
?>
