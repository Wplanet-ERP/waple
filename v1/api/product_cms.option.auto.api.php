<?php
require('../inc/common.php');

$tmp_log_c_no   = '2809';
$option_name    = isset($_GET['option_name']) ? $_GET['option_name'] : "";
$sup_c_no       = isset($_GET['sup_c_no']) ? $_GET['sup_c_no'] : "";
$option_type    = isset($_GET['option_type']) ? $_GET['option_type'] : "option_name";
$display        = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$join_type      = isset($_GET['join_type']) ? $_GET['join_type'] : "";

if($option_type == 'sku'){
    $unit_where = "pcum.{$option_type} LIKE '%{$option_name}%'";
}else {
    $unit_where = "pcu.{$option_type} LIKE '%{$option_name}%'";
}

if(!empty($display)){
    $unit_where .= " AND pcu.display='{$display}'";
}

if(!empty($sup_c_no)){
    $unit_where  .= " AND pcu.sup_c_no='{$sup_c_no}'";
}

$join_where = "";
if($join_type != 'all'){
    $join_where .= " AND pcum.log_c_no='{$tmp_log_c_no}'";
}

$unit_sql    = "
    SELECT 
        pcu.`no`, 
        pcu.option_name, 
        pcu.sup_c_no,  
        pcu.notice,
        pcum.sku,
        pcum.log_c_no,
        (SELECT sub.c_name FROM company sub WHERE sub.c_no=pcu.sup_c_no) as sup_c_name,
        (SELECT sub.c_name FROM company sub WHERE sub.c_no=pcum.log_c_no) as log_c_name
    FROM product_cms_unit as pcu
    LEFT JOIN product_cms_unit_management pcum ON pcum.prd_unit=pcu.no {$join_where}
    WHERE {$unit_where}
    ORDER BY pcu.priority ASC
";
$unit_query  = mysqli_query($my_db, $unit_sql);
while ($product_array = mysqli_fetch_assoc($unit_query))
{
    $label      = $product_array['sup_c_name']." :: ".$product_array['option_name'];
    $ware_label = $product_array['log_c_name']." :: ".$product_array['sku'];
    if(!empty($product_array['notice'])){
        $label      .= " :: 「주의 : {$product_array['notice']}」";
        $ware_label .= " :: 「주의 : {$product_array['notice']}」";
    }

    $arr[] = array(
        "label"         => $label,
        "ware_label"    => $ware_label,
        "option_no"     => $product_array['no'],
        "option_name"   => $product_array[$option_type],
        "sku"           => $product_array['sku'],
        "sup_c_no"      => $product_array['sup_c_no'],
        "sup_c_name"    => $product_array['sup_c_name'],
        "log_c_no"      => $product_array['log_c_no'],
        "log_c_name"    => $product_array['log_c_name'],
    );
}

echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
