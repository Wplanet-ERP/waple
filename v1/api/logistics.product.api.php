<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$prd_out_code   = isset($_POST['out_code']) ? $_POST['out_code'] : "";
$file_type      = isset($_POST['file_type']) ? $_POST['file_type'] : "";
$product        = [];
$result         = false;

if(!empty($prd_out_code))
{
    if($file_type == "qx"){
        $prd_out_sql    = "SELECT * FROM product_cms WHERE prd_code='{$prd_out_code}' LIMIT 1";
        $prd_out_query  = mysqli_query($my_db, $prd_out_sql);
        $prd_out_result = mysqli_fetch_assoc($prd_out_query);
    }else{
        $prd_out_sql    = "SELECT * FROM product_cms WHERE prd_no IN(SELECT prd_no FROM product_cms_outsourcing WHERE `value`='{$prd_out_code}') LIMIT 1";
        $prd_out_query  = mysqli_query($my_db, $prd_out_sql);
        $prd_out_result = mysqli_fetch_assoc($prd_out_query);
    }

    if(!empty($prd_out_result))
    {
        $result  = true;
        $product = $prd_out_result;
    }
}

echo json_encode(array("result" => $result, "product" => $product), JSON_UNESCAPED_UNICODE);
?>
