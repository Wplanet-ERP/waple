<?php
require('../inc/common.php');

$s_no   = isset($_POST['s_no'])?$_POST['s_no']:"";
$result = false;

if(!!$s_no){
    // 리스트 쿼리
    $staff_result_sql = "SELECT s.mbti, s.s_name, (SELECT sm.title FROM staff_mbti sm WHERE sm.mbti=s.mbti) as title, (SELECT sm.features FROM staff_mbti sm WHERE sm.mbti=s.mbti) as features FROM staff s WHERE s.s_no='{$s_no}'";

    $query = mysqli_query($my_db, $staff_result_sql);
    $html  = "";
    $idx   = 0;

    while ($staff_result = mysqli_fetch_assoc($query))
    {
        $s_name   = $staff_result['s_name'];
        $title    = $staff_result['title'];
        $features = $staff_result['features'];

        if($idx == 0){
            $html .= "<h2>{$s_name}님의 MBTI</h2>";
        }

        $html .= "<h4>{$title}</h4><p>{$features}</p>";
        $idx++;
    }

    $data = !empty($html) ? array("result" => true, "html" => $html) : array("result" => false, "html" => $html);
}else{
    $data = array("result" => false, "html" => "");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
