<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$mbti = isset($_GET['mbti'])?$_GET['mbti']:"";


// 리스트 쿼리
$sql    = "SELECT mbti, title FROM staff_mbti WHERE mbti like '%{$mbti}%'";
$query  = mysqli_query($my_db, $sql);

while ($staff_mbti = mysqli_fetch_assoc($query)) {
    $arr[] = array(
        "mbti"    => $staff_mbti['mbti'],
        "label"   => $staff_mbti['mbti']." :: ".$staff_mbti['title']
    );
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
