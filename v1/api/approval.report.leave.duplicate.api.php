<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/approval.php');
require('../inc/model/Leave.php');

$leave_model        = Leave::Factory();
$ar_no              = isset($_POST['ar_no']) ? $_POST['ar_no'] : "";
$leave_no           = isset($_POST['leave_no']) ? $_POST['leave_no'] : "";
$leave_type         = isset($_POST['leave_type']) ? $_POST['leave_type'] : "";
$leave_s_date       = isset($_POST['leave_s_date']) ? $_POST['leave_s_date'] : "";
$leave_e_date       = isset($_POST['leave_e_date']) ? $_POST['leave_e_date'] : "";
$leave_type_data    = $leave_model->getItem($leave_no);
$holiday_list       = $leave_model->getHolidayList();
$staff_base_leave   = $leave_model->getDefaultLeaveData($leave_no, $session_s_no);
$staff_leave_day    = $staff_base_leave['create_day']-$staff_base_leave['use_day'];

$result         = false;
$msg            = "오류가 발생했습니다";
$leave_s_year   = date("Y", strtotime($leave_s_date));
$leave_e_year   = date("Y", strtotime($leave_e_date));

if($leave_s_year != $leave_e_year){
    $result = false;
    $msg    = "연도가 다릅니다";
}
elseif($leave_s_date > $leave_e_date){
    $result = false;
    $msg    = "휴가일을 변경해주세요";
}
else{
    if($leave_type == "2" || $leave_type == "4"){
        $chk_date           = date("Ymd", strtotime($leave_s_date));
        $chk_day_type       = date("w", strtotime($chk_date));
        $leave_s_datetime   = $leave_s_date." 00:00:00";
        $leave_e_datetime   = $leave_s_date." 11:59:59";

        if(isset($holiday_list[$chk_date]) || ($chk_day_type == 0 || $chk_day_type == 6)){
            $result = false;
            $msg    = "휴일 입니다.";
        }else{
            if($leave_model->getCheckLeaveDay($ar_no, $session_s_no, $leave_s_datetime, $leave_e_datetime)){
                $result = false;
                $msg    = "중복되는 휴가일이 있습니다.";
            }else{
                $result = true;
                $msg    = "";
            }
        }
    }
    elseif($leave_type == "3" || $leave_type == "5"){
        $chk_date           = date("Ymd", strtotime($leave_s_date));
        $chk_day_type       = date("w", strtotime($chk_date));
        $leave_s_datetime   = $leave_s_date." 12:00:00";
        $leave_e_datetime   = $leave_s_date." 23:59:59";

        if(isset($holiday_list[$chk_date]) || ($chk_day_type == 0 || $chk_day_type == 6)){
            $result = false;
            $msg    = "휴일 입니다.";
        }else{
            if($leave_model->getCheckLeaveDay($ar_no, $session_s_no, $leave_s_datetime, $leave_e_datetime)){
                $result = false;
                $msg    = "중복되는 휴가일이 있습니다.";
            }else{
                $result = true;
                $msg    = "";
            }
        }
    }
    else{
        $chk_s_datetime     = new DateTime($leave_s_date);
        $chk_e_datetime     = new DateTime($leave_e_date);
        $leave_diff         = $chk_s_datetime->diff($chk_e_datetime);
        $leave_diff_day     = $leave_diff->format("%a");
        $leave_day          = (int)$leave_diff_day+1;
        $real_leave_day     = 0;
        $leave_s_datetime   = $leave_s_date." 00:00:00";
        $leave_e_datetime   = $leave_e_date." 23:59:59";

        for($idx=0; $idx < $leave_day; $idx++){
            $chk_date       = date("Ymd", strtotime("{$leave_s_date} +{$idx} days"));
            $chk_day_type   = date("w", strtotime($chk_date));

            if(isset($holiday_list[$chk_date]) || ($chk_day_type == 0 || $chk_day_type == 6)){
                continue;
            }

            $real_leave_day++;
        }

        if($real_leave_day == 0){
            $result = false;
            $msg  = "휴일 입니다.";
        }
        else{
            if($leave_model->getCheckLeaveDay($ar_no, $session_s_no, $leave_s_datetime, $leave_e_datetime)){
                $result = false;
                $msg    = "중복되는 휴가일이 있습니다.";
            }else{
                $result = true;
                $msg    = "";
            }
        }
    }
}

$data = array(
    "result"        => $result,
    "msg"           => $msg
);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
