<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_date.php');
require('../inc/helper/asset.php');
header("Content-Type: text/html; charset=UTF-8");

$sch_as_no  = isset($_POST['sch_as_no'])?$_POST['sch_as_no']:"";
$as_no      = isset($_POST['as_no'])?$_POST['as_no']:"";
$as_r_no    = isset($_POST['as_r_no'])?$_POST['as_r_no']:"";

$cur_time_msg = "";
if($as_no == '1062'){ // 강의장의 경우
  $cur_time_msg = "<div style='color:red;'>※1시간 전/후 예약불가</div>";
}

$asset_hour_option  = getHourOption();
$asset_min_option   = getMinOption();
if(!empty($as_r_no))
{
    $cur_date_cal = date('YmdHis');
    // 리스트 쿼리
    $asset_reservation_sql = "
      SELECT
        `asr`.as_r_no,
        `asr`.as_no,
        (SELECT `as`.`name` FROM asset `as` WHERE `as`.as_no = `asr`.as_no) as as_name,
        DATE_FORMAT(`asr`.r_s_date, '%Y/%m/%d') as r_s_day,
        DATE_FORMAT(`asr`.r_s_date, '%H') as r_s_hour,
        DATE_FORMAT(`asr`.r_s_date, '%i') as r_s_min,
        DATE_FORMAT(`asr`.r_e_date, '%Y/%m/%d') as r_e_day,
        DATE_FORMAT(`asr`.r_e_date, '%H') as r_e_hour,
        DATE_FORMAT(`asr`.r_e_date, '%i') as r_e_min,
        DATE_FORMAT(`asr`.r_e_date, '%Y%m%d%H%i%s') as r_e_day_cal,
        `asr`.req_name,
        `asr`.req_no,
        `asr`.task_req
      FROM asset_reservation `asr` WHERE `asr`.as_r_no ='{$as_r_no}' LIMIT 1
    ";

    $asset_reservation_query = mysqli_query($my_db, $asset_reservation_sql);
    while ($asset_result = mysqli_fetch_assoc($asset_reservation_query))
    {
        if($session_s_no == $asset_result['req_no'])
        {
            $html = "<form action='asset_reservation_calendar.php' name='asset_reserv_frm' method='post' >
                 <input type='hidden' name='sch_as_no' value='{$sch_as_no}'>
                <input type='hidden' name='as_r_no' value='{$as_r_no}'>
                <input type='hidden' name='as_no' value='{$asset_result['as_no']}'>
                <input type='hidden' name='process' value='cancel_asset_reservation'>
                <table class='table table-striped table-bordered'>
                    <thead class='thead-dark text-center'>
                        <tr>";

            $html .=       "<th class='text-center'>자산명</th>
                            <th class='text-center' scope='col'>예약일시</th>
                            <th class='text-center' scope='col'>요청내용</th>
                            <th class='text-center' scope='col'>요청자</th>
                            <th class='text-center' scope='col'></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>";

            $html .=        "<td style='font-weight: bold; vertical-align: middle;'>{$asset_result['as_name']}{$cur_time_msg}</td>
                            <td>
                                <div class='new_r_s_date_wrap'>
                                    <input type='text' id='new_r_s_day' name='new_r_s_day' class='new_r_date form-control date-picker' placeholder='시작일자' value='{$asset_result['r_s_day']}' style='font-size: 12px; float: left; width: 94px;'>
                                    <select id='new_r_s_hour' name='new_r_s_hour' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

            foreach($asset_hour_option as $hour){
                if($hour == $asset_result['r_s_hour']){
                    $html .= "<option value='{$hour}' selected>$hour</option>";
                }else{
                    $html .= "<option value='{$hour}'>$hour</option>";
                }
            }
            $html .=           "</select>
                                    <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>시</span>
                                    <select id='new_r_s_min' name='new_r_s_min' class='new_r_date form-control date-picker' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

            foreach($asset_min_option as $min){
                if($min == $asset_result['r_s_min']){
                    $html .= "<option value='{$min}' selected>$min</option>";
                }else{
                    $html .= "<option value='{$min}'>$min</option>";
                }
            }

            $html .=           "</select>
                                    <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>분</span>
                                </div>
                                <div class='new_r_e_date_wrap'>
                                    <input type='text' id='new_r_e_day' name='new_r_e_day' class='new_r_date form-control date-picker' placeholder='마감일자' value='{$asset_result['r_e_day']}' style='font-size: 12px; float: left; width: 94px;'>
                                    <select id='new_r_e_hour' name='new_r_e_hour' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";
            foreach($asset_hour_option as $hour){
                if($hour == $asset_result['r_e_hour']){
                    $html .= "<option value='{$hour}' selected>$hour</option>";
                }else{
                    $html .= "<option value='{$hour}'>$hour</option>";
                }
            }
            $html .=           "</select>
                                    <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>시</span>
                                    <select id='new_r_e_min' name='new_r_e_min' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

            foreach($asset_min_option as $min){
                if($min == $asset_result['r_e_min']){
                    $html .= "<option value='{$min}' selected>$min</option>";
                }else{
                    $html .= "<option value='{$min}'>$min</option>";
                }
            }

            $html .=           "</select>
                                    <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>분</span>
                                </div>
                            </td>
                            <td>
                                <textarea name='new_task_req' id='new_task_req' style='width: 145px; height: 70px;' class='textarea_work_message'>{$asset_result['task_req']}</textarea>
                            </td>
                            <td style='line-height: 70px;'>{$asset_result['req_name']}</td>
                            <td>";

            if($cur_date_cal < $asset_result['r_e_day_cal']){
                $html .= "<button type='button' class='btn btn-success font-weight-bold' onclick='modifyReservation();' style='height: 32px; line-height: 18px;'>예약변경</button><br/>";
                $html .= "<button type='button' class='btn btn-warning font-weight-bold' onclick='cancelReservation();' style='height: 32px; line-height: 18px;'>예약취소</button>";
            }

            $html .=    "</td>
                        </tr>
                    </tbody>
                </table>
            </form>";
        }else{
            $html = "<form action='asset_reservation_calendar.php' name='asset_reserv_frm' method='post' >
                <input type='hidden' name='sch_as_no' value='{$sch_as_no}'>
                <input type='hidden' name='as_r_no' value='{$as_r_no}'>
                <input type='hidden' name='as_no' value='{$as_no}'>
                <input type='hidden' name='process' value='cancel_asset_reservation'>
                <table class='table table-striped table-bordered'>
                    <thead class='thead-dark text-center'>
                        <tr>";

            $html .=        "<th class='text-center'>자산명</th>
                            <th class='text-center' scope='col'>예약일시</th>
                            <th class='text-center' scope='col'>요청내용</th>
                            <th class='text-center' scope='col'>요청자</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>";

            $html .=      "<td style='font-weight: bold; vertical-align: middle;'>{$asset_result['as_name']}{$cur_time_msg}</td>
                          <td>
                                <div class='new_r_s_date_wrap'>
                                    <input type='text' id='new_r_s_day' name='new_r_s_day' class='new_r_date form-control date-picker' placeholder='시작일자' value='{$asset_result['r_s_day']}' style='font-size: 12px; float: left; width: 94px;' disabled>
                                    <select id='new_r_s_hour' name='new_r_s_hour' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;' disabled>";

            foreach($asset_hour_option as $hour){
                if($hour == $asset_result['r_s_hour']){
                    $html .= "<option value='{$hour}' selected disabled>$hour</option>";
                }else{
                    $html .= "<option value='{$hour}' disabled>$hour</option>";
                }
            }
            $html .=           "</select>
                                    <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>시</span>
                                    <select id='new_r_s_min' name='new_r_s_min' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;' disabled>";

            foreach($asset_min_option as $min){
                if($min == $asset_result['r_s_min']){
                    $html .= "<option value='{$min}' selected disabled>$min</option>";
                }else{
                    $html .= "<option value='{$min}' disabled>$min</option>";
                }
            }

            $html .=           "</select>
                                    <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>분</span>
                                </div>
                                <div class='new_r_e_date_wrap'>
                                    <input type='text' id='new_r_e_day' name='new_r_e_day' class='new_r_date form-control date-picker' placeholder='마감일자' value='{$asset_result['r_e_day']}' style='font-size: 12px; float: left; width: 94px;' disabled>
                                    <select id='new_r_e_hour' name='new_r_e_hour' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;' disabled>";
            foreach($asset_hour_option as $hour){
                if($hour == $asset_result['r_e_hour']){
                    $html .= "<option value='{$hour}' selected disabled>$hour</option>";
                }else{
                    $html .= "<option value='{$hour}' disabled>$hour</option>";
                }
            }
            $html .=           "</select>
                                    <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>시</span>
                                    <select id='new_r_e_min' name='new_r_e_min' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;' disabled>";

            foreach($asset_min_option as $min){
                if($min == $asset_result['r_e_min']){
                    $html .= "<option value='{$min}' selected disabled>$min</option>";
                }else{
                    $html .= "<option value='{$min}' disabled>$min</option>";
                }
            }

            $html .=           "</select>
                                    <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>분</span>
                                </div>
                            </td>
                            <td>
                                <textarea name='new_task_req' id='new_task_req' style='width: 280px; height: 70px;' class='textarea_work_message' disabled>{$asset_result['task_req']}</textarea>
                            </td>
                            <td style='line-height: 70px;'>{$asset_result['req_name']}</td>
                        </tr>
                    </tbody>
                </table>
            </form>";
        }
    }

    $data = array("html" => $html);
}
elseif(!empty($as_no) && $as_no == 'all')
{
    $cur_date    = isset($_POST['r_date']) ? $_POST['r_date'] : date('Y-m-d');
    $cur_hour    = date('H');
    $cur_min_cal = ceil(date('i')/10)*10;
    $cur_min     = ($cur_min_cal == '60') ? '00' : $cur_min_cal;
    $cur_min     = sprintf('%02d', $cur_min);

    $asset_share_sql   = "SELECT as_no, `name` FROM asset WHERE share_type='3' AND asset_state='1' AND as_no IS NOT NULL ORDER BY as_no ASC";
    $asset_share_query = mysqli_query($my_db, $asset_share_sql);
    $asset_share_list  = [];

    while($asset_share_result = mysqli_fetch_assoc($asset_share_query))
    {
        $asset_share_list[$asset_share_result['as_no']] = $asset_share_result['name'];
    }

    $html = "<form action='asset_reservation_calendar.php' name='new_asset_frm' id='new_asset_frm' method='post' >
            <input type='hidden' name='sch_as_no' value='{$sch_as_no}'>
            <input type='hidden' name='new_req_no' value='{$session_s_no}'>
            <input type='hidden' name='new_req_name' value='{$session_name}'>
            <input type='hidden' name='new_as_no' id='new_as_no' value=''>
            <input type='hidden' name='new_as_name' id='new_as_name' value=''>
            <input type='hidden' name='new_management' id='new_management' value=''>
            <input type='hidden' name='new_manager' id='new_manager' value=''>
            <input type='hidden' name='new_sub_manager' id='new_sub_manager' value=''>
            <input type='hidden' name='new_work_state' id='new_work_state' value=''>
            <input type='hidden' name='process' value='new_asset_reservation'>
            <table class='table table-striped table-bordered'>
                <thead class='thead-dark text-center'>
                    <tr>
                        <th class='text-center' scope='col'>자산명</th>
                        <th class='text-center' scope='col'>예약일시</th>
                        <th class='text-center' scope='col'>요청내용</th>
                        <th class='text-center' scope='col'>요청자</th>
                        <th class='text-center' scope='col'></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select id='new_sel_as_no' name='new_sel_as_no' class='new_sel_as_no form-control' style='width:120px; font-size: 12px; padding: 0; text-align: center;'>
                            <option value=''>::전체::</option>
                            ";

    foreach($asset_share_list as $key => $asset_share){
        $html .= "<option value='{$key}'>{$asset_share}</option>";
    }

    $html .= "          </select>
                        </td>
                        <td>
                            <div class='new_r_s_date_wrap'>
                                <input type='text' id='new_r_s_day' name='new_r_s_day' class='new_r_date form-control date-picker' placeholder='시작일자' value='{$cur_date}' style='font-size: 12px; float: left; width: 94px;'>
                                <select id='new_r_s_hour' name='new_r_s_hour' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

    foreach($asset_hour_option as $hour){
        if($hour == $cur_hour){
            $html .= "<option value='{$hour}' selected>{$hour}</option>";
        }else{
            $html .= "<option value='{$hour}'>{$hour}</option>";
        }
    }
    $html .=       "</select>
                                <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>시</span>
                                <select id='new_r_s_min' name='new_r_s_min' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

    foreach($asset_min_option as $min){
        if($min == $cur_min){
            $html .= "<option value='{$min}' selected>$min</option>";
        }else{
            $html .= "<option value='{$min}'>$min</option>";
        }
    }
    $html .=       "</select>
                                <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>분</span>
                            </div>
                            <div class='new_r_e_date_wrap'>
                                <input type='text' id='new_r_e_day' name='new_r_e_day' class='new_r_date form-control date-picker' placeholder='마감일자' value='{$cur_date}' style='font-size: 12px; float: left; width: 94px;'>
                                <select id='new_r_e_hour' name='new_r_e_hour' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

    foreach($asset_hour_option as $hour){
        if($hour == $cur_hour){
            $html .= "<option value='{$hour}' selected>$hour</option>";
        }else{
            $html .= "<option value='{$hour}'>$hour</option>";
        }
    }
    $html .=       "</select>
                                <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>시</span>
                                <select id='new_r_e_min' name='new_r_e_min' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

    foreach($asset_min_option as $min){
        if($min == $cur_min){
            $html .= "<option value='{$min}' selected>$min</option>";
        }else{
            $html .= "<option value='{$min}'>$min</option>";
        }
    }
    $html .=       "</select>
                                <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>분</span>
                            </div>
                        </td>
                        <td>
                            <textarea name='new_task_req' id='new_task_req' style='width: 145px; height: 70px;' class='textarea_work_message'></textarea>
                        </td>
                        <td style='line-height: 70px;'>{$session_name}</td>
                        <td><button type='button' class='btn btn-dark font-weight-bold' onclick='newEmptyCheck();' style='height: 70px;'>예약하기</button></td>
                    </tr>
                </tbody>
            </table>
        </form>";

    $data = array("html" => $html);
}
elseif(!empty($as_no) && $as_no != 'all')
{
    $cur_date    = isset($_POST['r_date']) ? $_POST['r_date'] : date('Y-m-d');
    $cur_hour    = date('H');
    $cur_min_cal = ceil(date('i')/10)*10;
    $cur_min     = ($cur_min_cal == '60') ? '00' : $cur_min_cal;
    $cur_min     = sprintf('%02d', $cur_min);

    $asset_sql    = "SELECT use_type, management, form, manager, sub_manager, `name` FROM asset WHERE as_no='{$as_no}' LIMIT 1";
    $asset_query  = mysqli_query($my_db, $asset_sql);
    $asset        = mysqli_fetch_assoc($asset_query);
    $use_type       = $asset['use_type'];
    $as_name        = $asset['name'];
    $as_management  = $asset['management'];
    $as_manager     = $asset['manager'];
    $as_sub_manager = $asset['sub_manager'];
    $as_form        = $asset['form'];
    $work_state     = ($use_type == '1') ? '1' :(($use_type == '2') ? "2" : "");

    $html = "<form action='asset_reservation_calendar.php' name='new_asset_frm' id='new_asset_frm' method='post' >
            <input type='hidden' name='sch_as_no' value='{$sch_as_no}'>
            <input type='hidden' name='new_req_no' value='{$session_s_no}'>
            <input type='hidden' name='new_req_name' value='{$session_name}'>
            <input type='hidden' name='new_as_no' value='{$as_no}'>
            <input type='hidden' name='new_as_name' value='{$as_name}'>
            <input type='hidden' name='new_management' value='{$as_management}'>
            <input type='hidden' name='new_manager' value='{$as_manager}'>
            <input type='hidden' name='new_sub_manager' value='{$as_sub_manager}'>
            <input type='hidden' name='new_work_state' value='{$work_state}'>
            <input type='hidden' name='process' value='new_asset_reservation'>
            <table class='table table-striped table-bordered'>
                <thead class='thead-dark text-center'>
                    <tr>
                      <th class='text-center' scope='col'>자산명</th>
                        <th class='text-center' scope='col'>예약일시</th>
                        <th class='text-center' scope='col'>요청내용</th>
                        <th class='text-center' scope='col'>요청자</th>
                        <th class='text-center' scope='col'></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                      <td style='font-weight: bold; vertical-align: middle;'>{$as_name}{$cur_time_msg}</td>
                        <td>
                            <div class='new_r_s_date_wrap'>
                                <input type='text' id='new_r_s_day' name='new_r_s_day' class='new_r_date form-control date-picker' placeholder='시작일자' value='{$cur_date}' style='font-size: 12px; float: left; width: 94px;'>
                                <select id='new_r_s_hour' name='new_r_s_hour' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

    foreach($asset_hour_option as $hour){
        if($hour == $cur_hour){
            $html .= "<option value='{$hour}' selected>{$hour}</option>";
        }else{
            $html .= "<option value='{$hour}'>{$hour}</option>";
        }
    }
    $html .=       "</select>
                                <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>시</span>
                                <select id='new_r_s_min' name='new_r_s_min' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

    foreach($asset_min_option as $min){
        if($min == $cur_min){
            $html .= "<option value='{$min}' selected>$min</option>";
        }else{
            $html .= "<option value='{$min}'>$min</option>";
        }
    }
    $html .=       "</select>
                                <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>분</span>
                            </div>
                            <div class='new_r_e_date_wrap'>
                                <input type='text' id='new_r_e_day' name='new_r_e_day' class='new_r_date form-control date-picker' placeholder='마감일자' value='{$cur_date}' style='font-size: 12px; float: left; width: 94px;'>
                                <select id='new_r_e_hour' name='new_r_e_hour' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

    foreach($asset_hour_option as $hour){
        if($hour == $cur_hour){
            $html .= "<option value='{$hour}' selected>$hour</option>";
        }else{
            $html .= "<option value='{$hour}'>$hour</option>";
        }
    }
    $html .=       "</select>
                                <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>시</span>
                                <select id='new_r_e_min' name='new_r_e_min' class='new_r_date form-control' style='width:50px; float: left;font-size: 12px; padding: 0; text-align: center;'>";

    foreach($asset_min_option as $min){
        if($min == $cur_min){
            $html .= "<option value='{$min}' selected>$min</option>";
        }else{
            $html .= "<option value='{$min}'>$min</option>";
        }
    }
    $html .=       "</select>
                                <span style='float: left; display: inline-block; line-height: 35px; margin: 0 2px;'>분</span>
                            </div>
                        </td>
                        <td>
                            <textarea name='new_task_req' id='new_task_req' style='width: 145px; height: 70px;' class='textarea_work_message'>{$as_form}</textarea>
                        </td>
                        <td style='line-height: 70px;'>{$session_name}</td>
                        <td><button type='button' class='btn btn-dark font-weight-bold' onclick='newEmptyCheck();' style='height: 70px;'>예약하기</button></td>
                    </tr>
                </tbody>
            </table>
        </form>";

    $data = array("html" => $html);
}else{
    $data = array("html" => "오류발생 다시 시도해 주세요");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
