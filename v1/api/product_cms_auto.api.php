<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$prd_name_get = isset($_GET['prd_name'])?$_GET['prd_name']:"";


// 리스트 쿼리
$product_sql = "
    SELECT 
       (SELECT sub.k_name FROM kind sub WHERE sub.k_name_code=k.k_parent) AS k_parent_name, 
       k.k_name, 
       p.prd_no,
       p.title,
       p.notice, 
       p.log_c_no,
       (SELECT c.c_name FROM company `c` WHERE `c`.c_no=p.log_c_no) as log_c_name
    FROM product_cms p 
    LEFT JOIN kind k ON k.k_name_code=p.k_name_code
    WHERE p.title like '%{$prd_name_get}%' AND p.display=1 ORDER BY k.priority ASC, p.priority ASC";

$query = mysqli_query($my_db, $product_sql);

while ($product_array = mysqli_fetch_assoc($query))
{
    $prd_label = $product_array['k_parent_name']." > ".$product_array['k_name']." > ".$product_array['title'];
    if($product_array['notice']){
        $prd_label .= " :: 「주의 : {$product_array['notice']}」";
    }

    $chk_unit_sql   = "SELECT pcr.option_no, pcu.option_name, pcu.is_in_out, pcu.in_out_msg FROM product_cms_relation pcr LEFT JOIN product_cms_unit pcu ON pcu.no=pcr.option_no WHERE pcr.prd_no='{$product_array['prd_no']}' AND pcr.display='1' AND pcu.is_in_out='2'";
    $chk_unit_query = mysqli_query($my_db, $chk_unit_sql);
    $chk_unit_text  = "";
    while($chk_unit = mysqli_fetch_assoc($chk_unit_query)){
        $chk_unit_text .= empty($chk_unit_text) ? "※주의 : 입/출고 정지 [{$chk_unit['in_out_msg']}]" : ", [{$chk_unit['in_out_msg']}]";
    }

    $arr[] = array(
        "prd_no"        => $product_array['prd_no'],
        "prd_name"      => $product_array['title'],
        "log_c_no"      => $product_array['log_c_no'],
        "log_c_name"    => $product_array['log_c_name'],
        "unit_msg"      => $chk_unit_text,
        "prd_label"     => $prd_label
    );
}

echo json_encode($arr, JSON_UNESCAPED_UNICODE);
?>
