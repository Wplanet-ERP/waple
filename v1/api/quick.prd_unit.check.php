<?php
require('../inc/common.php');

$prd_no         = isset($_POST['prd_no']) ? $_POST['prd_no'] : "";
$chk_unit_sql   = "SELECT pcr.option_no, pcu.is_in_out, pcu.in_out_msg FROM product_cms_relation pcr LEFT JOIN product_cms_unit pcu ON pcu.no=pcr.option_no WHERE pcr.prd_no='{$prd_no}' AND pcr.display='1' AND pcu.is_in_out='2'";
$chk_unit_query = mysqli_query($my_db, $chk_unit_sql);
$chk_unit_text  = "";
while($chk_unit = mysqli_fetch_assoc($chk_unit_query)){
    $chk_unit_text .= empty($chk_unit_text) ? "※주의 : 입/출고 정지 [{$chk_unit['in_out_msg']}]" : ", [{$chk_unit['in_out_msg']}]";
}

echo json_encode(array("unit_msg" => $chk_unit_text), JSON_UNESCAPED_UNICODE);
?>
