<?php
require('../inc/common.php');
require('../ckadmin.php');

$mc_brand  = isset($_POST['mc_brand'])?$_POST['mc_brand']:"";
$mc_s_date = isset($_POST['mc_s_date'])?$_POST['mc_s_date']:"";
$mc_e_date = isset($_POST['mc_e_date'])?$_POST['mc_e_date']:"";

$mc_result = 2;
$error_result = 2;
$msg = "";
if(!empty($mc_brand) && !empty($mc_s_date) && !empty($mc_e_date)) {
    $mc_data_check_sql = "SELECT DISTINCT sales_date FROM commerce_report WHERE brand='{$mc_brand}' AND (sales_date >= '{$mc_s_date}' AND sales_date <= '{$mc_e_date}') ORDER BY sales_date ASC";
    $mc_data_check_query  = mysqli_query($my_db, $mc_data_check_sql);
    $mc_data_check_list = [];
    while($mc_data_check_result = mysqli_fetch_assoc($mc_data_check_query)){
        $mc_data_check_list[] = $mc_data_check_result['sales_date'];
    }

    $mc_data_check_cnt = !empty($mc_data_check_list) ? count($mc_data_check_list) : 0;

    if($mc_data_check_cnt > 0){
        $mc_data_sales_date = $mc_data_check_list[0];
        $mc_data_check_cnt_exc = $mc_data_check_cnt-1;
        if($mc_data_check_cnt == 1){
            $msg = "이미 저장된 {$mc_data_sales_date}의 매출 및 비용 정보가 있습니다.\r\n엑셀파일의 내용으로 새로 저장 시 [확인]을\r\n취소하려면 [취소]를 선택하세요.";
        }else{
            $msg = "이미 저장된 {$mc_data_sales_date}(외 {$mc_data_check_cnt_exc}일)의 매출 및 비용 정보가 있습니다.\r\n엑셀파일의 내용으로 새로 저장 시 [확인]을\r\n취소하려면 [취소]를 선택하세요.";
        }

        $mc_result = 1;
    }
}else{
    $error_result = 1;
}

$data = array("mc_result" => $mc_result, "error_result" => $error_result, "msg" => $msg);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>