<?php
require('../inc/common.php');
require('../inc/model/ProductCmsUnit.php');

$prd_unit       = isset($_POST['prd_unit']) ? $_POST['prd_unit'] : "";
$chk_unit_val   = isset($_POST['chk_unit']) ? $_POST['chk_unit'] : "";
$result         = true;
$msg            = "";

if(!empty($prd_unit) && !empty($chk_unit_val))
{
    $chk_unit_list  = explode(",", $chk_unit_val);
    foreach($chk_unit_list as $chk_unit)
    {
        $chk_sql    = "SELECT sub_unit_name, COUNT(*) as cnt FROM product_cms_unit_subcontract WHERE parent_unit='{$prd_unit}' AND sub_unit='{$chk_unit}'";
        $chk_query  = mysqli_query($my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        if($chk_result['cnt'] > 0) {
            $result = false;
            $msg .= " {$chk_result['sub_unit_name']}은 이미 등록된 사급품목입니다.";
        }
    }
}

$data = array("result" => $result, "msg" => $msg);
echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>