<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/company.php');
require('../inc/helper/project.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$company_bank_option    = getBkTitleOption();
$prj_bank_option        = getBankOption();
$total_bank_option      = array_merge($prj_bank_option, $company_bank_option);

$bk_name   	 = isset($_POST['bk_name']) ? $_POST['bk_name'] : "";
$bk_chk 	 = in_array($bk_name, $total_bank_option) ? true : false;
$data_result = array("result" => $bk_chk);

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($data_result, JSON_UNESCAPED_UNICODE);

?>
