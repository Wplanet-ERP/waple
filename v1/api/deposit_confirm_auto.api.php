<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$keyword = isset($_GET['keyword'])?$_GET['keyword']:"";

// 리스트 쿼리
$confirm_sql    = "SELECT dpc_no, dpc_company, dp_date, bk_title, bk_name, price FROM deposit_confirm WHERE work_state='3' AND (price = '{$keyword}' OR bk_name LIKE '%{$keyword}%')";
$confirm_query  = mysqli_query($my_db, $confirm_sql);
$confirm        = [];
while ($confirm_result = mysqli_fetch_assoc($confirm_query)) {
    $confirm[] = array(
        "dpc_no"    => $confirm_result['dpc_no'],
        "label"     => "진행요청 / {$confirm_result['dpc_company']} / {$confirm_result['dp_date']} / {$confirm_result['bk_title']} / {$confirm_result['bk_name']} / {$confirm_result['price']}"
    );
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($confirm, JSON_UNESCAPED_UNICODE);

?>
