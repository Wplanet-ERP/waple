<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$c_name_get = isset($_GET['c_name'])?$_GET['c_name']:"";

$company_sql = "SELECT `c`.c_no, `c`.c_name, (SELECT s.s_name FROM staff s WHERE s.s_no=`c`.s_no) as s_name FROM company c WHERE `c`.display='1' AND `c`.c_name like '%{$c_name_get}%'";
$company_query = mysqli_query($my_db, $company_sql);
while($company = mysqli_fetch_assoc($company_query))
{
    $c_no  = $company['c_no'];
    $label = $company['c_name']." :: ".$company['s_name'];


    $arr[] = array(
        "c_no"  => $c_no,
        "label" => $label,
    );
}
// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
