<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "")
{
    $parent_data    = $_GET[$_GET['parent_data']];
    $sch_type       = isset($_GET['sch_type']) ? $_GET['sch_type'] : "brand";
    $sch_group      = isset($_GET['sch_group']) ? $_GET['sch_group'] : "sch_kind_g1";
    $sch_group_type = isset($_GET['sch_group_type']) ? $_GET['sch_group_type'] : "";
    $sch_display    = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";

    if ($sch_group == "sch_kind_g1" || $sch_group == "sel_kind_g1")
    {
        $kind_g1_sql    = "SELECT k_name_code,k_name FROM kind WHERE k_code='{$sch_type}' AND k_parent='{$parent_data}' AND display='{$sch_display}' ORDER BY priority ASC";
        $kind_g1_query  = mysqli_query($my_db, $kind_g1_sql);
        $kind_g1_count  = mysqli_num_rows($kind_g1_query);
        $base_option    = ($sch_group == "sch_kind_g1") ? "::전체::" : "::선택::";

        echo "[" . PHP_EOL;
        if ($kind_g1_count > 0) {
            echo "{\"\":\"{$base_option}\"}" . (($kind_g1_count > 0) ? "," : "") . PHP_EOL;
            while ($kind_g1 = mysqli_fetch_assoc($kind_g1_query)):
                echo "{\"" . $kind_g1['k_name_code'] . "\":\"" . $kind_g1['k_name'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"{$base_option}\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;
    }
    elseif ($sch_group == "sch_kind_g2" || $sch_group == "sel_kind_g2")
    {
        $kind_g2_sql = "";
        if($sch_type == "brand"){
            $add_brand_where = "1!=1";
            if(!empty($parent_data)){
                $add_brand_where  = "brand='{$parent_data}' AND display='{$sch_display}'";

                if($sch_group_type == "domestic"){
                    $add_brand_where .= " AND (c_name NOT LIKE '%해외%' AND c_name NOT LIKE '%일본%')";
                }
            }
            $kind_g2_sql   = "SELECT c.c_no as key_value, c.c_name as label_value FROM company c WHERE {$add_brand_where} ORDER BY priority ASC";
        }
        elseif($sch_type == "product_cms"){
            $kind_g2_sql   = "SELECT p.prd_no as key_value, p.title as label_value FROM product_cms p WHERE k_name_code='{$parent_data}' AND display='{$sch_display}'";

            if($sch_group_type == "product"){
                $kind_g2_sql   .= " AND prd_type != '2'";
            }
        }
        $kind_g2_query  = mysqli_query($my_db, $kind_g2_sql);
        $kind_g2_count  = mysqli_num_rows($kind_g2_query);
        $base_option    = ($sch_group == "sch_kind_g2") ? "::전체::" : "::선택::";

        echo "[" . PHP_EOL;
        if ($kind_g2_count > 0) {
            echo "{\"\":\"{$base_option}\"}" . (($kind_g2_count > 0) ? "," : "") . PHP_EOL;
            while ($kind_g2 = mysqli_fetch_assoc($kind_g2_query)):
                echo "{\"" . $kind_g2['key_value'] . "\":\"" . $kind_g2['label_value'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"{$base_option}\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;
    }
}
?>
