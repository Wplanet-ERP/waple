<?php
require('../inc/common.php');
require('../ckadmin.php');

$new_dp_company = $_POST['new_dp_company'];
$new_url        = $_POST['new_url'];
$result         = false;

if(!empty($new_dp_company) && !empty($new_url))
{
    $chk_url_sql    = "SELECT COUNT(*) as cnt FROM commerce_url WHERE url='{$new_url}' AND dp_c_no='{$new_dp_company}'";
    $chk_url_query  = mysqli_query($my_db, $chk_url_sql);
    $chk_url_result = mysqli_fetch_assoc($chk_url_query);
    $chk_url_cnt    = isset($chk_url_result['cnt']) && !empty($chk_url_result['cnt']) ? $chk_url_result['cnt'] : 0;

    if($chk_url_cnt == 0){
        $result  = true;
    }
}

echo json_encode(array("result" => $result), JSON_UNESCAPED_UNICODE);
?>
