<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$t_name_get = isset($_GET['t_name'])?$_GET['t_name']:"";
$t_my_c_no_get = isset($_GET['t_my_c_no'])?$_GET['t_my_c_no']:"";

// 리스트 쿼리
if($t_my_c_no_get){
    $team_sql = "SELECT t.team_code, t.team_name, t.team_group, t.depth FROM team t WHERE t.team_name like '%{$t_name_get}%' AND t.display='1' AND t.my_c_no='{$t_my_c_no_get}' ORDER BY team_name";
}else{
    $team_sql = "SELECT t.team_code, t.team_name, t.team_group, t.depth FROM team t WHERE t.team_name like '%{$t_name_get}%' AND t.display='1' ORDER BY team_name";
}

$query = mysqli_query($my_db, $team_sql);

while ($team_array = mysqli_fetch_assoc($query))
{
    $t_label_list = [];
    $t_sch_depth = $team_array['depth'];

    $t_sch_code = $team_array['team_code'];
    for($i=0; $i<$t_sch_depth; $i++)
    {
        if(empty($t_sch_code)){
            break;
        }
        $t_label_sql    = "SELECT t.team_name, t.team_code_parent FROM team t WHERE t.team_code='{$t_sch_code}'";
        $t_label_query  = mysqli_query($my_db, $t_label_sql);
        $t_label_result = mysqli_fetch_assoc($t_label_query);

        $t_sch_code = $t_label_result['team_code_parent'];
        $t_label_list[$i] = $t_label_result['team_name'];
    }

    krsort($t_label_list);
    $t_label = implode(" > ", $t_label_list);

    $arr[] = array(
        "t_code"  => $team_array['team_code'],
        "t_name"  => $team_array['team_name'],
        "t_group" => $team_array['team_group'],
        "label"   => $t_label,
    );
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
