<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$c_name_get = isset($_GET['c_name'])?$_GET['c_name']:"";


// 리스트 쿼리
$company_sql = "SELECT c.c_no, c.c_name, (SELECT s.s_name FROM staff s WHERE s.s_no=c.s_no) AS s_name FROM company c WHERE c.c_name like '%{$c_name_get}%' AND c.display=1 AND c.corp_kind=2";

$query = mysqli_query($my_db,$company_sql);

while ($company_array = mysqli_fetch_assoc($query)) {
    $arr[] = array(
        "c_no"      => $company_array['c_no'],
        "c_name"    => $company_array['c_name'],
        "s_name"=>$company_array['s_name']
    );
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
