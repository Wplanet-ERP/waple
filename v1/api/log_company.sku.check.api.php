<?php
require('../inc/common.php');
require('../inc/model/ProductCmsUnit.php');
header("Content-Type: text/html; charset=UTF-8");

$sku_val        = isset($_POST['sku']) ? $_POST['sku'] : "";
$log_company    = isset($_POST['log_company']) ? $_POST['log_company'] : "";
$result         = true;
$msg            = "";

$unit_manage_model 	= ProductCmsUnit::Factory();
$unit_manage_model->setMainInit("product_cms_unit_management", "no");

if(!empty($log_company) && !empty($sku_val))
{
    $sku_list       = explode(",", $sku_val);
    $log_comp_list  = explode(",", $log_company);
    foreach($sku_list as $key => $chk_sku)
    {
        $chk_log_c_no = $log_comp_list[$key];
        if(!$unit_manage_model->checkLogCompanySku($chk_log_c_no, $chk_sku)){
            $result = false;
            $msg .= "이미 존재하는 SKU({$chk_sku}) 입니다.";
        }
    }
}

$data = array("result" => $result, "msg" => $msg);
echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>