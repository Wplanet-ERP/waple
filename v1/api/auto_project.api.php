<?php
require('../inc/common.php');
require('../ckadmin.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$pj_name_get = isset($_GET['pj_name'])?$_GET['pj_name']:"";

// 리스트 쿼리
$project_sql = "
    SELECT
        pj_no,
        pj_name
    FROM
        project pj
    WHERE
        pj.pj_name like '%{$pj_name_get}%' AND pj.work_state='5'
    ORDER BY
        pj_name ASC
";

$arr = [];
$project_query = mysqli_query($my_db, $project_sql);
while ($project_array = mysqli_fetch_assoc($project_query))
{
    $pj_kind_list = [];
    if(!empty($project_array['pj_no']))
    {
        $pj_kind_sql = "SELECT * FROM project_expenses_kind WHERE pj_no='{$project_array['pj_no']}' ORDER BY k_priority ASC";
        $pj_kind_query = mysqli_query($my_db, $pj_kind_sql);
        while($pj_kind = mysqli_fetch_assoc($pj_kind_query))
        {
            $pj_kind_list[$pj_kind['pj_k_no']] = $pj_kind['k_name'];
        }
    }

	$arr[] = array(
		"pj_no"			=> $project_array['pj_no'],
		"pj_name"		=> $project_array['pj_name'],
		"pj_kind_list"	=> $pj_kind_list
	);
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
