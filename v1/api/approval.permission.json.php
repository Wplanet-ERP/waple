<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "") {

    $parent_data_get = $_GET[$_GET['parent_data']];

    if ($_GET['parent_data'] == "approval_permission_team_val" || $_GET['parent_data'] == "reading_permission_team_val" || $_GET['parent_data'] == "reference_permission_team_val")
    {
        if($parent_data_get == "99999" || $parent_data_get == "99998")
        {
            echo "[" . PHP_EOL;
            echo "{\"" . $parent_data_get . "\":\"" . "부서장" . "\"}," . PHP_EOL;
            echo "{\"selected\":\"\"}" . PHP_EOL;
            echo "]" . PHP_EOL;
        }
        else
        {
            if($parent_data_get)
            {
                $team_where = getTeamWhere($my_db, $parent_data_get);

                $sql = "SELECT s_no, s_name from staff WHERE staff_state='1' AND team IN({$team_where})";

            }else{
                $sql="SELECT s.s_no, s.s_name FROM staff s WHERE s.staff_state ='1' ORDER BY s_name ASC";
            }

            $query = mysqli_query($my_db, $sql);
            $count = mysqli_num_rows($query);

            echo "[" . PHP_EOL;
            if ($count > 0) {
                if ($_GET['parent_data'] == "approval_permission_team_val" || $_GET['parent_data'] == "reading_permission_team_val" || $_GET['parent_data'] == "reference_permission_team_val") {
                    echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
                }
                while ($result = mysqli_fetch_assoc($query)):
                    echo "{\"" . $result['s_no'] . "\":\"" . $result['s_name'] . "\"}," . PHP_EOL;
                endwhile;
                echo "{\"selected\":\"\"}" . PHP_EOL;
            } else {
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
            }
            echo "]" . PHP_EOL;
        }
    }
}
?>
