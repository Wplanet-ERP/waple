<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$t_my_c_no_get = isset($_POST['t_my_c_no'])?$_POST['t_my_c_no']:"";
$t_name_get = isset($_POST['t_name'])?$_POST['t_name']:"";
$t_code_get = isset($_POST['t_code'])?$_POST['t_code']:"";
$t_parent_code_get = isset($_POST['t_parent_code'])?$_POST['t_parent_code']:"";

$add_where = "";

if($t_code_get){
    $add_where .= "AND t.team_code!='{$t_code_get}'";
}

if($t_parent_code_get){
    $add_where .= "AND t.team_code_parent='{$t_parent_code_get}'";
}

$team_name_sql = "SELECT count(t.id) as cnt FROM team t WHERE t.team_name='{$t_name_get}' AND t.my_c_no='{$t_my_c_no_get}' AND t.display='1' {$add_where}";

$result = true;
$team_name_query = mysqli_query($my_db, $team_name_sql);
$team_name_result = mysqli_fetch_assoc($team_name_query);
$team_name_cnt = isset($team_name_result['cnt']) ? $team_name_result['cnt'] : 0;
if($team_name_cnt > 0){
    $result = false;
}

$arr = array("result" => $result);

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
