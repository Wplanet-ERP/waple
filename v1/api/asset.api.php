<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/asset.php');
header("Content-Type: text/html; charset=UTF-8");

$as_no  = isset($_POST['as_no'])?$_POST['as_no']:"";
$asset  = "";

if(!empty($as_no))
{
    // 리스트 쿼리
    $asset_sql = "
      SELECT 
        `as`.as_no,
        `as`.k_name_code,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=`as`.k_name_code)) AS k_asset1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=`as`.k_name_code) AS k_asset2_name,
        `as`.`name`,
        `as`.management,
        `as`.manager,
        `as`.manager_name,
        `as`.sub_manager,
        `as`.sub_manager_name,
        `as`.asset_state,
        `as`.use_type,
        `as`.share_type,
        `as`.object_type,
        `as`.asset_loc,
        IFNULL(`as`.object_url, '') as object_url,
        IFNULL(`as`.object_site, '') as object_site,
        IFNULL(`as`.object_id, '') as object_id,
        IFNULL(`as`.object_pw, '') as object_pw,
        `as`.non_description,
        `as`.form,
        `as`.qr_code,
        `as`.img_path,
        `as`.img_name
      FROM asset `as` WHERE `as`.as_no = '{$as_no}' LIMIT 1
    ";
    $asset_query = mysqli_query($my_db, $asset_sql);
    $cur_min_cal    = ceil(date('i')/10)*10;
    $cur_min        = ($cur_min_cal == '60') ? '00' : $cur_min_cal;
    $cur_min        = sprintf('%02d', $cur_min);
    $asset_object_type_option = getAssetObjectTypeOption();
    while ($asset_result = mysqli_fetch_assoc($asset_query))
    {
        $use_type = $asset_result['use_type'];
        $asset_result['work_state'] = ($use_type == '1') ? '1' :(($use_type == '2') ? "2" : "");
        $asset_result['work_state_name'] = ($use_type == '1') ? "사용요청중" :(($use_type == '2') ? "사용승인" : "");
        $asset_result['r_s_day']  = "";
        $asset_result['r_s_hour'] = "";
        $asset_result['r_s_min']  = "";

        if($asset_result['share_type'] == '3'){
            $cur_min_cal = ceil(date('i')/10)*10;
            $cur_min     = ($cur_min_cal == '60') ? '00' : $cur_min_cal;
            $cur_min     = sprintf('%02d', $cur_min);

            $asset_result['r_s_day']  = date('Y-m-d');
            $asset_result['r_s_hour'] = date('H');
            $asset_result['r_s_min']  = $cur_min;
        }

        # 이미지 & QR 코드
        $img_paths = $asset_result['img_path'];
        $img_names = $asset_result['img_name'];

        if(!empty($img_paths) && !empty($img_names))
        {
            $img_paths_arr = explode(',', $img_paths);
            $img_names_arr = explode(',', $img_names);
            $asset_result['first_img_path'] = $img_paths_arr[0];
            $asset_result['first_img_name'] = $img_names_arr[0];
        }else{
            $asset_result['first_img_path'] = "";
            $asset_result['first_img_name'] = "";
        }

        $asset_result['qr_code'] = (isset($asset_result['qr_code']) && !empty($asset_result['qr_code'])) ? $asset_result['qr_code'] : "";
        $asset_result['object_type_name'] = (isset($asset_object_type_option[$asset_result['object_type']]) && !empty($asset_object_type_option[$asset_result['object_type']])) ? $asset_object_type_option[$asset_result['object_type']]: "";
        $asset = $asset_result;
    }

    $data = !empty($asset) ? array("result" => true, "asset" => $asset) : array("result" => false, "asset" => "");
}else{
    $data = array("result" => false, "asset" => "");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>