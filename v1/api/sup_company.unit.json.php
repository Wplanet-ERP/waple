<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "") {

    $parent_data = $_GET[$_GET['parent_data']];

    $add_where = "`type`='2'";
    if(is_array($parent_data)){
        $parent_data = $parent_data[0];
    }
    $add_where .= " AND sup_c_no='{$parent_data}'";

    $product_unit_sql   = "SELECT `no`, option_name FROM product_cms_unit WHERE {$add_where} ORDER BY `no` ASC";
    $product_unit_query = mysqli_query($my_db, $product_unit_sql);
    $product_unit_count = mysqli_num_rows($product_unit_query);

    echo "[" . PHP_EOL;
    if ($product_unit_count > 0) {
        echo "{\"\":\"::선택::\"}" . (($product_unit_count > 0) ? "," : "") . PHP_EOL;
        while ($product_unit_result = mysqli_fetch_assoc($product_unit_query)):
            echo "{\"" . $product_unit_result['no'] . "\":\"" . $product_unit_result['option_name'] . "\"}," . PHP_EOL;
        endwhile;
        echo "{\"selected\":\"\"}" . PHP_EOL;
    } else {
        echo "{\"\":\"::선택::\"}" . PHP_EOL;
    }
    echo "]" . PHP_EOL;
}
?>
