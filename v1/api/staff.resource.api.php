<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$s_name_get = isset($_GET['s_name'])?$_GET['s_name']:"";


$t_depth_sql = "SELECT MAX(depth) as t_max FROM team WHERE display='1'";
$t_depth_query = mysqli_query($my_db, $t_depth_sql);
$t_depth_result = mysqli_fetch_assoc($t_depth_query);
$t_depth = isset($t_depth_result['t_max']) ? $t_depth_result['t_max'] : "1";

// 리스트 쿼리
$staff_sql = "SELECT s.s_no, s.s_name, s.team_list, s.addr, s.my_c_no, s.position FROM staff s WHERE s.s_name like '%{$s_name_get}%' AND s.staff_state='1'";

$query = mysqli_query($my_db,$staff_sql);

while ($staff_array = mysqli_fetch_assoc($query)) {

    $team_list = explode(",", $staff_array['team_list']);

    foreach($team_list as $team_code)
    {
        $team_sql = "SELECT depth, team_name FROM team WHERE team_code='{$team_code}'";
        $team_query = mysqli_query($my_db, $team_sql);
        $team_result = mysqli_fetch_assoc($team_query);

        $t_depth = isset($team_result['depth']) ? $team_result['depth'] : 0;
        $t_label = getTeamFullName($my_db, $t_depth, $team_code);

        $s_label  = "{$staff_array['s_name']} ({$t_label})";
        $ss_label = "{$staff_array['s_name']} ({$team_result['team_name']})";
        $arr[] = array(
            "s_no"      => $staff_array['s_no'],
            "s_name"    => $staff_array['s_name'],
            "s_label"   => $s_label,
            "ss_label"  => $ss_label,
            "team"      => $team_code,
            "team_name" => $team_result['team_name'],
            "my_c_no"   => $staff_array['my_c_no'],
            "addr"      => $staff_array['addr'],
            "position"  => $staff_array['position'],
        );
    }
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
