<?php
require('../inc/common.php');

$guide_no = isset($_POST['guide_no'])?$_POST['guide_no']:"";
$result   = false;

if(!!$guide_no)
{
    // 리스트 쿼리
    $modal_guide_sql    = "SELECT *, (SELECT k.k_name FROM kind k WHERE k.k_name_code =(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code = `bg`.k_name_code LIMIT 1)) as guide_g1_name, (SELECT s.s_name FROM staff s WHERE s.s_no=`bg`.manager) AS manager_name FROM board_guide bg WHERE bg.b_no ='{$guide_no}'";
    $modal_guide_query  = mysqli_query($my_db, $modal_guide_sql);

    $html  = "";

    $modal_guide = mysqli_fetch_assoc($modal_guide_query);

    if(isset($modal_guide['b_no']))
    {
        $modal_file_name   = isset($modal_guide['file_name']) && !empty($modal_guide['file_name']) ? explode(',', $modal_guide['file_name']) : [];
        $modal_file_path   = isset($modal_guide['file_path']) && !empty($modal_guide['file_path']) ? explode(',', $modal_guide['file_path']) : [];
        $modal_file_count  = !empty($modal_file_path) && !empty($modal_file_path) ? count($modal_file_path) : 0;

        $html .= "<h2 style='margin-bottom: 10px;'>{$modal_guide['question']}</h2>";
        $html .= "<div style='clear: both; overflow: hidden;'>
                <span style='display: inline-block; float: left; width: 60px;'>담당자 :</span>
                <span style='display: inline-block; float: left; margin-left: 7px;'>{$modal_guide['manager_name']}</span>
              </div>"
        ;

        $html .= "<div style='clear: both;overflow: hidden;'>
                <div style='float: left; height: auto; width: 60px;'>첨부파일 :</div>
                <div style='float: left; margin-left: 7px;'>";

        if($modal_file_count > 0 && $modal_file_path)
        {
            foreach($modal_file_path as $idx => $g_path)
            {
                $html .= "<a type='application/octet-stream' href='/v1/uploads/{$g_path}' download='{$modal_file_name[$idx]}' title='{$modal_file_name[$idx]}'>{$modal_file_name[$idx]}</a><br/>";
            }
        }

        $html .= "  </div>";
        $html .= "</div>";
        $html .= "<div style='margin: 15px 0 10px; border: 1px solid #ccc; padding: 12px; max-height: 600px; min-height: 400px; overflow-y: scroll;'>{$modal_guide['contents']}</div>";

        $result = true;
    }

    $data = !empty($html) ? array("result" => true, "html" => $html) : array("result" => false, "html" => $html);
}else{
    $data = array("result" => false, "html" => "");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
