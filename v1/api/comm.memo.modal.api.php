<?php
require('../inc/common.php');
require('../ckadmin.php');

$cm_no       = isset($_POST['cm_no'])?$_POST['cm_no']:"";
$brand       = isset($_POST['brand'])?$_POST['brand']:"";
$k_name_code = isset($_POST['k_name_code'])?$_POST['k_name_code']:"";
$sales_date  = isset($_POST['sales_date'])?$_POST['sales_date']:"";
$type        = isset($_POST['type'])?$_POST['type']:"";
$memo        = "";
$parent_height = 70;

if(!empty($cm_no)) {
    $comm_memo_sql   = "SELECT * FROM commerce_report_memo WHERE cm_no='{$cm_no}'";
    $comm_memo_query = mysqli_query($my_db, $comm_memo_sql);
    $comm_memo       = mysqli_fetch_assoc($comm_memo_query);

    if (isset($comm_memo['cm_no']) && !empty($comm_memo['cm_no'])) {
        $brand       = $comm_memo['brand'];
        $k_name_code = $comm_memo['k_name_code'];
        $sales_date  = $comm_memo['sales_date'];
        $memo        = $comm_memo['memo'];
    }
}elseif($type == 'parent'){
    $comm_memo_sql   = "SELECT * FROM commerce_report_memo WHERE brand='{$brand}' AND k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$k_name_code}') AND sales_date='{$sales_date}'";
    $comm_memo_query = mysqli_query($my_db, $comm_memo_sql);
    $memo            = "";
    $idx             = 1;
    while($comm_memo = mysqli_fetch_assoc($comm_memo_query))
    {
        $memo .= "{$comm_memo['memo']}\r\n\r\n";
        $idx++;
    }
    $parent_height *= $idx;
}

$html = "
    <form action='media_commerce_report.php' name='comm_memo_frm' method='post' >
        <input type='hidden' name='new_cm_no' value='{$cm_no}'>
        <input type='hidden' name='new_brand' value='{$brand}'>
        <input type='hidden' name='new_k_name_code' value='{$k_name_code}'>
        <input type='hidden' name='new_sales_date' value='{$sales_date}'>
        <input type='hidden' name='process' value='add_commerce_memo'>
        <table class='table table-striped table-bordered'>
            <thead class='thead-dark text-center'>
                <tr>
                    <th class='text-center' scope='col'>메모</th>
                    <th class='text-center' scope='col'></th>
                </tr>
            </thead>
            <tbody>
                <tr>";
if($type == 'editor') {
    $html .= "
            <td>
                <textarea name='new_memo' id='new_memo' style='width: 275px; height: 70px; padding: 5px;' class='textarea_work_message'>{$memo}</textarea>
            </td>
            <td style='vertical-align: middle; text-align: center;'>
                <button type='button' class='btn btn-dark font-weight-bold' onclick='modal_save_el(this);' style='height: 40px; font-size:12px;'>저장하기</button>";
}elseif($type == 'parent'){
    $html .= "
            <td colspan='2'>
                <textarea name='new_memo' id='new_memo' style='width: 370px; height: {$parent_height}px; padding: 5px; max-height: 500px;' class='textarea_work_message' readonly>{$memo}</textarea>
            </td>";
}else{
    $html .= "
            <td>
                <textarea name='new_memo' id='new_memo' style='width: 275px; height: 70px; padding: 5px;' class='textarea_work_message' readonly>{$memo}</textarea>
            </td>
            <td style='vertical-align: middle; text-align: center;'>
                <button type='button' class='btn btn-dark font-weight-bold' onclick='modal_save_el(this);' style='height: 40px; font-size:12px;' disabled>저장하기</button>";
}

$html .= "          </td>
                </tr>
            </tbody>
        </table>
    </form>";

$data = array("html" => $html);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>