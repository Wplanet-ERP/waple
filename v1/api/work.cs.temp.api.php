<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_message.php');

$temp_no    = isset($_POST['temp_no']) ? $_POST['temp_no'] : "";
$content    = "";

if($temp_no == "1"){
    $content = "안녕하세요 #{고객명} 고객님 닥터피엘 입니다 : )\r\n주방용 교환 요청 내역이 확인 됩니다.\r\n제품 교환 시에는 교환비 6,000원이 발생 됩니다.\r\n기업은행 / 541-049509-04-031(와이즈미디어커머스) 으로 `#{고객명}`님 성함으로 교환배송비 입금 부탁드리며 고객님 성함으로 입금 확인 후 교환 제품 출고 됩니다.\r\n감사합니다.\r\n\r\n-닥터피엘 드림 -";
}
elseif($temp_no == "2"){
    $content = "안녕하세요 #{고객명} 고객님 아이레놀 입니다 : )\r\n아이레놀 제품 교환 요청 내역이 확인 됩니다.\r\n제품 교환시에는 교환 배송비 6,000원이 발생 됩니다.\r\n기업은행 / 541-049509-04-031(와이즈미디어커머스) 으로 `#{고객명}`님 성함으로 교환배송비 입금 부탁드리며 고객님 성함으로 입금 확인 되어야 교환 진행 됩니다.\r\n감사합니다 : )\r\n\r\n-아이레놀 드림-";
}
elseif($temp_no == "3"){
    $content = "안녕하세요 #{고객명} 고객님 닥터피엘 입니다 : )\r\n고객님 교환 요청 하신 제품 정확한 확인 및 안내가 필요한 부분으로\r\n\r\nhttps://2nev7.channel.io/workflows/683811\r\n\r\n위 실시간 채팅 상담 링크로 교환 문자 받으신 내용과 함께 보내주시면 확인 후 안내 도와드리겠습니다. 감사합니다 : )\r\n\r\n-닥터피엘 드림-";
}
elseif($temp_no == "4"){
    $content = "안녕하세요 #{고객명} 고객님 누잠 입니다 : )\r\n고객님 교환 요청 하신 제품 정확한 확인 및 안내가 필요한 부분으로\r\n\r\nhttps://belabefftalk.channel.io/workflows/108104\r\n\r\n위 실시간 채팅 상담 링크로 교환 문자 받으신 내용과 함께 보내주시면 확인 후 안내 도와드리겠습니다. 감사합니다.\r\n\r\n-누잠 드림-";
}
elseif($temp_no == "5"){
    $content = "안녕하세요 #{고객명} 고객님 베라베프 입니다 : )\r\n고객님 교환 요청 하신 제품 정확한 확인 및 안내가 필요한 부분으로\r\nhttps://belabefftalk.channel.io/workflows/108104\r\n\r\n위 실시간 채팅 상담 링크로 교환 문자 받으신 내용과 함께 보내주시면 확인 후 안내 도와드리겠습니다. 감사합니다.\r\n\r\n-베라베프 드림-";
}
elseif($temp_no == "6"){
    $content = "";
}

$data = array('content' => $content);
echo json_encode($data, JSON_UNESCAPED_UNICODE);
?>