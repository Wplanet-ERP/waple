<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$prd_no = isset($_POST['prd_no'])?$_POST['prd_no']:"";
$title  = isset($_POST['title'])?$_POST['title']:"";

$product_cms_sql = "";
$result = false;
if($prd_no){
    $product_cms_sql = "SELECT count(prd_no) as cnt FROM product_cms WHERE prd_no != '{$prd_no}' AND title='{$title}'";
}else{
    $product_cms_sql = "SELECT count(prd_no) as cnt FROM product_cms WHERE title='{$title}'";
}
$product_cms_query = mysqli_query($my_db, $product_cms_sql);
$product_cms_result = mysqli_fetch_assoc($product_cms_query);

if(isset($product_cms_result['cnt'])) {
    if($product_cms_result['cnt'] == '0'){
        $result = true;
    }
}else{
    $result = true;
}

$arr = array("result" => $result);

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
