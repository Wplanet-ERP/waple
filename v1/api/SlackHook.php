<?php
/**
 * Created by PhpStorm.
 * User: TH
 * Date: 2020-03-31
 * Time: 오후 2:38
 */

class SlackHook
{
    private $request_token      = "xoxb-1039437758967-1078553797124-z0suBjvYD2ZsNNIBpDeeS8Sv";
    private $request_username   = "waple";
    private $completed_token    = "xoxb-1039437758967-1165535406294-vzB5qkvXEhQBttlSrIdRysKw";
    private $completed_username = "workcompleted";
    private $notice_token       = "xoxb-1039437758967-1172495158275-PTfiMVCViu34QkK4XbmfrSlF";
    private $notice_username    = "notice";
    private $connection_token    = "xoxb-1039437758967-1218769883351-pfve6rnXPyTieiUV6MLjuUkn";
    private $connection_username = "connection";
    private $deposit_token       = "xoxb-1039437758967-1625465854242-JMJ59Uk2OdggJhMuJQHQdXhB";
    private $deposit_username    = "deposit";

    private $token    = "";
    private $username = "";
    private $channel;
    private $message;
    private $title;

    public function setTokenUsername($type='request')
    {
        if($type == 'completed'){
            $this->token    = $this->completed_token;
            $this->username = $this->completed_username;
        }elseif($type == 'notice'){
            $this->token    = $this->notice_token;
            $this->username = $this->notice_username;
        }elseif($type == 'connection'){
            $this->token    = $this->connection_token;
            $this->username = $this->connection_username;
        }elseif($type == 'deposit'){
            $this->token    = $this->deposit_token;
            $this->username = $this->deposit_username;
        }else{
            $this->token    = $this->request_token;
            $this->username = $this->request_username;
        }
    }

    public function setChannel($channel) {
        $this->channel = $channel;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function send()
    {
        $url = "https://slack.com/api/chat.postMessage";

        $postData = array(
            'token' => $this->token,
            'channel' => $this->channel,
            'username' => $this->username,
            'text' => $this->message,
            'as_user' => true
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_exec($ch);
        curl_close($ch);
    }

    public function sendBlock()
    {
        $url = "https://slack.com/api/chat.postMessage";

        $blockData = array(
            array(
                "type" => "section",
                "text" => array(
                    "type" => "mrkdwn",
                    "text" => "*[{$this->title}]*\r\n{$this->message}"
                )
            )
        );

        $postData = array(
            'token' => $this->token,
            'channel' => $this->channel,
            'username' => $this->username,
            'blocks' => json_encode($blockData),
            'as_user' => true
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_exec($ch);
        curl_close($ch);
    }
}