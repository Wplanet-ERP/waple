<?php
require('../inc/common.php');
header("Content-Type: text/html; charset=UTF-8");

$as_no_list  = isset($_POST['as_no_list'])?$_POST['as_no_list']:"";
$s_date      = isset($_POST['r_s_date'])?$_POST['r_s_date']:"";
$e_date      = isset($_POST['r_e_date'])?$_POST['r_e_date']:"";

$msg        = "이미 예약된 시간이 있습니다";

if(!empty($as_no_list))
{
    $asset     = "";
    $add_where = "`asr`.as_no IN({$as_no_list}) AND `asr`.work_state IN('1','2') AND ((`asr`.r_s_date >= '{$s_date}' AND `asr`.r_s_date < '{$e_date}') OR (`asr`.r_e_date > '{$s_date}' AND `asr`.r_e_date <= '{$e_date}') OR (`asr`.r_s_date < '{$s_date}' AND `asr`.r_e_date >= '{$e_date}'))";

    // 리스트 쿼리
    $asset_sql = "
      SELECT
       `asr`.r_s_date,
       `asr`.r_e_date
      FROM asset_reservation `asr` WHERE {$add_where} ORDER BY `asr`.r_s_date
    ";

    $asset_query = mysqli_query($my_db, $asset_sql);
    while ($asset_result = mysqli_fetch_assoc($asset_query))
    {
        $asset = $asset_result;
    }

    $data = !empty($asset) ? array("result" => false, "msg" => $msg) : array("result" => true, "msg" => "");
}else{
    $data = array("result" => false, "msg" => "오류발생 다시 시도해 주세요");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
