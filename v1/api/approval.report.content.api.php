<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/approval.php');
require('../inc/model/Team.php');

$af_no                  = isset($_POST['af_no'])?$_POST['af_no']:"";
$result                 = false;
$msg                    = "오류가 발생했습니다";
$team_model             = Team::Factory();
$team_all_list	        = $team_model->getTeamAllList();
$team_name_list	        = $team_all_list["team_name_list"];
$staff_all_list	        = $team_all_list["staff_list"];

$appr_g1 = $appr_g2 = $title = $content = $approval_html = $read_html = $ref_html = "";
$is_withdraw = 2;
$is_modify   = 2;
$appr_idx    = 2;
$read_idx    = 1;
$ref_idx     = 1;

if(!empty($af_no))
{
    $approval_form_sql = "
        SELECT
            af.*, (SELECT k_parent FROM kind k WHERE k.k_name_code=af.k_name_code) as appr_g1
        FROM approval_form af
        WHERE af.af_no='{$af_no}' LIMIT 1
    ";

    $approval_form_query = mysqli_query($my_db, $approval_form_sql);
    $approval_form = mysqli_fetch_assoc($approval_form_query);

    if(isset($approval_form['af_no']) && !empty($approval_form['af_no']))
    {
        $approval_permission_sql    = "SELECT * FROM approval_form_permission WHERE af_no='{$af_no}' ORDER BY priority";
        $approval_permission_query  = mysqli_query($my_db, $approval_permission_sql);
        $approval_permission_list   = [];
        $reading_permission_list    = [];
        $reference_permission_list  = [];
        while($approval_permission = mysqli_fetch_assoc($approval_permission_query))
        {
            if($approval_permission['afp_type'] == 'approval' || $approval_permission['afp_type'] == 'conference'){
                $approval_permission_list[] = $approval_permission;
                $appr_idx = $approval_permission['priority']+1;
            }elseif($approval_permission['afp_type'] == 'reading'){
                $reading_permission_list[] = $approval_permission;
                $read_idx = $approval_permission['priority']+1;
            }elseif($approval_permission['afp_type'] == 'reference'){
                $reference_permission_list[] = $approval_permission;
                $ref_idx = $approval_permission['priority']+1;
            }
        }

        $result             = true;
        $appr_g1            = $approval_form['appr_g1'];
        $appr_g2            = $approval_form['k_name_code'];
        $title              = $approval_form['title'];
        $content            = $approval_form['content'];
        $is_withdraw        = $approval_form['is_withdraw'];
        $is_calendar        = $approval_form['is_calendar'];
        $is_modify          = $approval_form['is_modify'];
        $chk_t_parent_s_no  = "";
        $chk_team_s_no      = "";

        if(!empty($approval_permission_list))
        {
            foreach($approval_permission_list as $approval_permission)
            {
                $appr_team      = $approval_permission['afp_team'];
                $appr_s_no      = $approval_permission['afp_s_no'];
                $appr_priority  = $approval_permission['priority'];
                $appr_type      = $approval_permission['afp_type'];

                if($appr_s_no == "99998"){
                    $chk_team_sql       = "SELECT *, (SELECT s.team FROM staff s WHERE s.s_no=t.team_leader) AS s_team FROM team AS t WHERE team_code='{$session_team}'";
                    $chk_team_query     = mysqli_query($my_db, $chk_team_sql);
                    $chk_team_result    = mysqli_fetch_assoc($chk_team_query);

                    if(!empty($chk_team_result) && (isset($chk_team_result['team_leader']) && !empty($chk_team_result['team_leader']))){
                        $appr_team      = $chk_team_result['s_team'];
                        $appr_s_no      = $chk_team_result['team_leader'];
                        $chk_team_s_no  = $chk_team_result['team_leader'];
                    }else{
                        continue;
                    }
                }

                if($appr_s_no == "99999"){
                    $chk_team_sql       = "SELECT *, (SELECT s.team FROM staff s WHERE s.s_no=t.team_leader) AS s_team FROM team AS t WHERE team_code=(SELECT sub.team_code_parent FROM team AS sub WHERE sub.team_code='{$session_team}')";
                    $chk_team_query     = mysqli_query($my_db, $chk_team_sql);
                    $chk_team_result    = mysqli_fetch_assoc($chk_team_query);

                    if(!empty($chk_team_result) && (isset($chk_team_result['team_leader']) && !empty($chk_team_result['team_leader']))){
                        $appr_team          = $chk_team_result['s_team'];
                        $appr_s_no          = $chk_team_result['team_leader'];
                        $chk_t_parent_s_no  = $chk_team_result['team_leader'];
                    }else{
                        continue;
                    }
                }

                if($session_s_no == $appr_s_no){
                    continue;
                }

                if(!empty($chk_team_s_no) && !empty($chk_t_parent_s_no) && $chk_team_s_no == $chk_t_parent_s_no){
                    continue;
                }

                $approval_html .= "<div class='approval_permission_content clear_wrapper' id='approval_permission_content_{$appr_team}_{$appr_s_no}' attr-idx='{$appr_priority}'>
									    <input type='hidden' name='approval_arp_no[]' value=''>
									    <input type='hidden' name='approval_permission_type[]' id='approval_permission_type{$appr_priority}' value='{$appr_type}'>
									    <input type='text' name='approval_priority_val' class='form-control approval_priority' value='{$appr_priority}' attr-idx='{$appr_priority}'>
									    <input type='hidden' name='approval_permission_team[]' id='approval_permission_team{$appr_priority}' value='{$appr_team}'>
									    <input type='hidden' name='approval_permission_staff[]' id='approval_permission_staff{$appr_priority}' value='{$appr_s_no}'>
									    <input type='hidden' name='approval_priority[]' id='approval_priority{$appr_priority}' value='{$appr_priority}'>
									    <select name='approval_permission_team_val' class='form-control appr_team approval_permission_team_val approval_permission_team{$appr_priority}' attr-idx='{$appr_priority}' required>
										    <option value=''>::전체::</option>
                ";

                foreach ($team_name_list as $key => $label)
                {
                    $appr_selected = "";
                    if($appr_team == $key){
                        $appr_selected = "selected";
                    }
                    $approval_html .= "    <option value='{$key}' {$appr_selected}>{$label}</option>";
                }
                $approval_html .= "    </select>";

                $appr_staff_list = [];
                if(isset($staff_all_list[$appr_team])) {
                    $appr_staff_list = $staff_all_list[$appr_team];
                }else{
                    $appr_staff_list = $staff_all_list['all'];
                }
                $approval_html .= "    <select name='approval_permission_staff_val' class='form-control appr_staff approval_permission_staff approval_permission_staff{$appr_priority}' attr-idx='{$appr_priority}' required>
                                            <option value=''>::전체::</option>
                ";
                foreach ($appr_staff_list as $key => $label)
                {
                    $appr_selected = "";
                    if($appr_s_no == $key){
                        $appr_selected = "selected";
                    }
                    $approval_html .= "    <option value='{$key}' {$appr_selected}>{$label}</option>";
                }
                $approval_html .= "    </select>";
                $approval_html .= "    <button type='button' onclick='delPermission(this)' class='del_appr_btn'>X</button>";

                $appr_chk = "";
                $conf_chk = "";
                if($appr_type == 'conference'){
                    $conf_chk = "checked";
                }else{
                    $appr_chk = "checked";
                }
                $approval_html .= "    <div class='appr_type_wrapper'>
                                            <input type='radio' name='approval_permission_type{$appr_priority}' value='approval' id='appr_type_approval_{$appr_priority}' class='appr_type_radio approval_permission_type' attr-idx='{$appr_priority}' {$appr_chk}>
                                            <label for='appr_type_approval_{$appr_priority}'  class='appr_type_radio_label appr_type_approval_label'>결재</label>
                                            <input type='radio' name='approval_permission_type{$appr_priority}' value='conference' id='appr_type_conference_{$appr_priority}' class='appr_type_radio approval_permission_type' attr-idx='{$appr_priority}' {$conf_chk}>
                                            <label for='appr_type_conference_{$appr_priority}' class='appr_type_radio_label appr_type_conference_label'>순차합의</label>
									   </div>
								    </div>
                ";
            }
        }

        if(!empty($reading_permission_list))
        {
            foreach($reading_permission_list as $reading_permission)
            {
                $read_team      = $reading_permission['afp_team'];
                $read_s_no      = $reading_permission['afp_s_no'];
                $read_priority  = $reading_permission['priority'];

                $read_html .= "<div class='reading_permission_content clear_wrapper'>
                                    <input type='hidden' name='reading_arp_no[]' value=''>
                                    <input type='hidden' name='reading_permission_team[]' id='reading_permission_team{$read_priority}' value='{$read_team}'>
                                    <input type='hidden' name='reading_permission_staff[]' id='reading_permission_staff{$read_priority}' value='{$read_s_no}'>
                                    <select name='reading_permission_team_val' class='form-control appr_team reading_permission_team_val reading_permission_team{$read_priority}' attr-idx='{$read_priority}' required>
                                        <option value=''>::전체::</option>
                ";

                foreach ($team_name_list as $key => $label)
                {
                    $read_selected = "";
                    if($read_team == $key){
                        $read_selected = "selected";
                    }
                    $read_html .= "       <option value='{$key}' {$read_selected}>{$label}</option>";
                }
                $read_html .= "     </select>";


                $read_staff_list = [];
                if(isset($staff_all_list[$read_team])) {
                    $read_staff_list = $staff_all_list[$read_team];
                }else{
                    $read_staff_list = $staff_all_list['all'];
                }
                $read_html .= "     <select name='reading_permission_staff_val' class='form-control appr_staff reading_permission_staff reading_permission_staff{$read_priority}' attr-idx='{$read_priority}'>
									    <option value=''>::전체::</option>
                ";
                foreach ($read_staff_list as $key => $label)
                {
                    $read_selected = "";
                    if($read_s_no == $key){
                        $read_selected = "selected";
                    }
                    $read_html .= "    <option value='{$key}' {$read_selected}>{$label}</option>";
                }
                $read_html .= "     </select>";
                $read_html .= "		<button type='button' onclick='delPermission(this)' class='del_appr_btn'>X</button>
		                       </div>
		        ";
            }
        }

        if(!empty($reference_permission_list))
        {
            foreach($reference_permission_list as $reference_permission)
            {
                $ref_team      = $reference_permission['afp_team'];
                $ref_s_no      = $reference_permission['afp_s_no'];
                $ref_priority  = $reference_permission['priority'];

                $ref_html .= "<div class='reference_permission_content clear_wrapper'>
                                    <input type='hidden' name='reference_arp_no[]' value=''>
                                    <input type='hidden' name='reference_permission_team[]' id='reference_permission_team{$ref_priority}' value='{$ref_team}'>
                                    <input type='hidden' name='reference_permission_staff[]' id='reference_permission_staff{$ref_priority}' value='{$ref_s_no}'>
                                    <select name='reference_permission_team_val' class='form-control appr_team reference_permission_team_val reference_permission_team{$ref_priority}' attr-idx='{$ref_priority}' required>
                                        <option value=''>::전체::</option>
                ";

                foreach ($team_name_list as $key => $label)
                {
                    $ref_selected = "";
                    if($ref_team == $key){
                        $ref_selected = "selected";
                    }
                    $ref_html .= "       <option value='{$key}' {$ref_selected}>{$label}</option>";
                }
                $ref_html .= "     </select>";

                $ref_staff_list = [];
                if(isset($staff_all_list[$ref_team])) {
                    $ref_staff_list = $staff_all_list[$ref_team];
                }else{
                    $ref_staff_list = $staff_all_list['all'];
                }
                $ref_html .= "     <select name='reference_permission_staff_val' class='form-control appr_staff reference_permission_staff reference_permission_staff{$ref_priority}' attr-idx='{$ref_priority}'>
									    <option value=''>::전체::</option>
                ";
                foreach ($ref_staff_list as $key => $label)
                {
                    $ref_selected = "";
                    if($ref_s_no == $key){
                        $ref_selected = "selected";
                    }
                    $ref_html .= "    <option value='{$key}' {$ref_selected}>{$label}</option>";
                }
                $ref_html .= "     </select>";
                $ref_html .= "		<button type='button' onclick='delPermission(this)' class='del_appr_btn'>X</button>
		                       </div>
		        ";
            }
        }
    }
}

$data = array(
    'result'        => $result,
    'msg'           => $msg,
    'title'         => $title,
    'appr_g1'       => $appr_g1,
    'appr_g2'       => $appr_g2,
    'content'       => $content,
    'approval'      => $approval_html,
    'appr_idx'      => $appr_idx,
    'reading'       => $read_html,
    'read_idx'      => $read_idx,
    'reference'     => $ref_html,
    'ref_idx'       => $ref_idx,
    'is_withdraw'   => $is_withdraw,
    'is_calendar'   => $is_calendar,
    'is_modify'     => $is_modify
);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
