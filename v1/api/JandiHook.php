<?php
/**
 * Created by PhpStorm.
 * User: TH
 * Date: 2020-03-31
 * Time: 오후 2:38
 */

class JandiHook
{
    private $jandi_hook_url = "https://wh.jandi.com/connect-api/webhook/";
    private $user_token     = "";

    public function setUserToken($token){
        $this->user_token = $token;
    }

    public function send($data)
    {
        $data = json_encode($data);

        $headers[] = "Accept: application/vnd.tosslab.jandi-v2+json"; // 신규 API 키
        $headers[] = "Content-type: Application/json";

        $jandi_url = $this->jandi_hook_url.$this->user_token; // API URL

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $jandi_url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_VERBOSE, true);

        $response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($response, true);

        return $response;
    }
}