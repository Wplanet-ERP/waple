<?php
require('../inc/common.php');
require('../inc/model/ProductCms.php');

$pl_no      = isset($_POST['pl_no']) ? $_POST['pl_no'] : "";
$unit_list  = isset($_POST['units']) ? $_POST['units'] : "";
$cms_model  = ProductCms::Factory();

if($cms_model->checkLoadUnit($pl_no, $unit_list)){
    $data_result = array("result" => true, "units" => $unit_list);
}else{
    $data_result = array("result" => false, "units" => $unit_list);
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($data_result, JSON_UNESCAPED_UNICODE);

?>
