<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/advertising.php');

$agency     = $_POST['agency'];
$product    = $_POST['product'];
$fee_per    = 0;
$result     = false;

if(!empty($agency) && !empty($product))
{
    $auto_fee_option = getAutoAdvertisingFee();
    $fee_per = isset($auto_fee_option[$agency][$product]) ? $auto_fee_option[$agency][$product] : 0;
    $result  = true;
}

echo json_encode(array("result" => $result, "fee_per" => $fee_per), JSON_UNESCAPED_UNICODE);
?>
