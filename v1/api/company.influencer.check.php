<?php
require('../inc/common.php');

$value      = isset($_POST['val']) ? $_POST['val'] : "";
$html       = "";
if(!empty($value) )
{

    $check_sql      = "SELECT COUNT(ci_no) as cnt FROM company_influencer WHERE url LIKE '%{$value}%'";
    $check_query    = mysqli_query($my_db, $check_sql);
    $check_result   = mysqli_fetch_assoc($check_query);
    $check_cnt      = $check_result['cnt'];

    $url  = "company_influencer_management.php?sch_url={$value}";

    if($check_cnt > 0){
        $html .=  "<span class='font-red'>※주의 : 유사한 URL이 ({$check_cnt})건 확인 되었습니다.</span><a href='{$url}' target='_blank' style='background: #999; color: white; padding: 3px 5px; margin-left: 5px; cursor: pointer; font-size: 10px;'>새 창 보기</a>";
    }else{
        $html .=  "<span style='color: black;'>중복되는 ID가 없습니다.</span>";
    }
}

$arr = array(
    "result" => true,
    "html"   => $html
);

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
