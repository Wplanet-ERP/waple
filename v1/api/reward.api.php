<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/promotion.php');
require('../inc/model/MyCompany.php');
header("Content-Type: text/html; charset=UTF-8");


$add_where				= "p.p_no=a.p_no AND (p.kind='2' OR p.kind='4' OR p.kind='5') AND a.a_state='1' AND (SELECT COUNT(*) FROM report r WHERE r.p_no = a.p_no AND r.a_no = a.a_no) > 0";
$sch_wd_due_date        = isset($_POST['sch_wd_due_date']) ? $_POST['sch_wd_due_date'] : "";
$sch_refund             = isset($_POST['sch_refund']) ? $_POST['sch_refund'] : "n";
$sch_tax_set            = isset($_POST['sch_tax_set']) ? $_POST['sch_tax_set'] : "";

if(!empty($sch_wd_due_date)) {
    $add_where .= " AND a.wd_due_date = '{$sch_wd_due_date}'";
}

if(!empty($sch_tax_set)) {
    if($sch_tax_set == "y") { // 소득세 저장완료
        $add_where .= " AND ((a.reward_cost IS NOT NULL AND a.reward_cost<>'0') AND a.biz_tax IS NOT NULL AND a.local_tax IS NOT NULL AND (a.reward_cost_result IS NOT NULL AND a.reward_cost_result<>'0'))";
    }elseif($sch_tax_set == "n"){ // 소득세 저장 미완료
        $add_where .= " AND ((a.reward_cost IS NULL OR a.reward_cost='0') OR a.biz_tax IS NULL OR a.local_tax IS NULL OR (a.reward_cost_result IS NULL OR a.reward_cost_result='0'))";
    }
}

if(!empty($sch_refund))
{
    if($sch_refund == "y") {
        $add_where .= " AND a.refund_count >= 1";
    }else if($sch_refund == "n") {
        $add_where .= " AND (a.refund_count is NULL or a.refund_count = 0)";
    }
}

$reward_html  = "
    <button type='button' style='font-size: 14px; font-weight: bold; background: #ff5722; color: white; padding: 5px 10px; margin-bottom: 10px; border-radius: 3px;' onclick='saveRewardWork()'>인센티브정산 반영</button>
    <table class='table table-bordered text-bold'>
        <colgroup>
            <col width='40px;'>
            <col width='140px;'>
            <col width='70px;'>
            <col width='120px;'>
            <col width='40px;'>
            <col width='200px;'>
            <col width='60px;'>
            <col width='40px;'>
            <col width='80px;'>
            <col width='80px;'>
            <col width='80px;'>
            <col width='80px;'>
            <col width='80px;'>
        </colgroup>
        <thead class='thead-dark text-center'>
            <tr>
                <th scope='col' rowspan='2'>
                    <input type='checkbox' name='check_all' value='all' onchange='modal_select_check_all(this);'>
                </th>
                <th scope='col' rowspan='2'>계열사</th>
                <th scope='col' rowspan='2'>채널</th>
                <th scope='col' rowspan='2'>캠페인종류</th>
                <th scope='col' rowspan='2'>p_no</th>
                <th scope='col' rowspan='2'>업체명</th>
                <th scope='col' rowspan='2'>담당자</th>
                <th scope='col' rowspan='2'>건수</th>
                <th scope='col' rowspan='2'>보상지급일</th>
                <th scope='col' rowspan='2'>보상지급액</th>
                <th scope='col' rowspan='2'>사업소득세</th>
                <th scope='col' rowspan='2'>지방소득세</th>
                <th scope='col' rowspan='2'>실지급액</th>
            </tr>
        </thead>
        <tbody>
";

$reward_sql = "
    SELECT 
        p.channel AS channel,
        p.kind AS kind,
        p.p_no AS p_no,
        p.c_no AS c_no,
        (SELECT c.my_c_no FROM company c WHERE c.c_no=p.c_no) AS my_c_no,
        p.company AS company,
        p.name AS staff_name,
        a.wd_due_date,
        COUNT(a.a_no) AS reward_cnt,
        SUM(a.reward_cost) AS sum_reward_cost,
        SUM(a.biz_tax) AS sum_biz_tax,
        SUM(a.local_tax) AS sum_local_tax,
        SUM(a.reward_cost_result) AS sum_reward_cost_result 
    FROM promotion p, application a
    WHERE {$add_where}
    GROUP BY p.p_no
    ORDER BY my_c_no ASC, a.p_no DESC, a.a_no ASC
";
$reward_query = mysqli_query($my_db, $reward_sql);
$promotion_channel_option = getPromotionChannelOption();
$promotion_kind_option    = getPromotionKindOption();
$my_company_model         = MyCompany::Factory();
$my_company_model->setList();
$my_company_list          = $my_company_model->getList();
while($reward = mysqli_fetch_assoc($reward_query))
{
    $my_company_name  = $my_company_list[$reward['my_c_no']]['c_name'];
    $my_company_color = $my_company_list[$reward['my_c_no']]['color'];
    $sum_reward_cost = number_format($reward['sum_reward_cost'],0);
    $sum_biz_tax     = number_format($reward['sum_biz_tax'],0);
    $sum_local_tax   = number_format($reward['sum_local_tax'],0);
    $sum_reward_cost_result = number_format($reward['sum_reward_cost_result'],0);

    $reward_html .= "
        <tr>
            <td class='text-center' style='height: 40px;'>
                <input type='checkbox' name='modal_select_check[]' class='modal_select_check' value='{$reward['p_no']}'>
            </td>
            <td class='text-left' style='color: {$my_company_color};'>{$my_company_name}</td>
            <td class='text-left'>{$promotion_channel_option[$reward['channel']]}</td>
            <td class='text-left'>{$promotion_kind_option[$reward['kind']]}</td>
            <td class='text-center'>{$reward['p_no']}</td>
            <td class='text-left'>{$reward['company']}</td>
            <td class='text-center'>{$reward['staff_name']}</td>
            <td class='text-center'>{$reward['reward_cnt']}</td>
            <td class='text-center'>{$reward['wd_due_date']}</td>
            <td class='text-right'>{$sum_reward_cost}</td>
            <td class='text-right'>{$sum_biz_tax}</td>
            <td class='text-right'>{$sum_local_tax}</td>
            <td class='text-right'>{$sum_reward_cost_result}</td>
        </tr>
    ";
}

$reward_html .= "
        </tbody>
    </table>
";

echo json_encode(array("reward_html" => $reward_html), JSON_UNESCAPED_UNICODE);

?>