<?php
require('../inc/common.php');
require('../ckadmin.php');

$type       = isset($_POST['type'])?$_POST['type']:"";
$ev_no      = isset($_POST['ev_no'])?$_POST['ev_no']:"";
$staff      = isset($_POST['staff'])?$_POST['staff']:"";
$new_staff  = isset($_POST['new_staff'])?$_POST['new_staff']:"";
$result     = false;

if(!empty($type) && !empty($ev_no) && !empty($new_staff))
{
    $ev_relation_chk_sql = "";
    if($type == 'receiver'){
        $ev_relation_chk_sql = "SELECT COUNT(ev_r_no) as cnt FROM evaluation_relation WHERE ev_no='{$ev_no}' AND receiver_s_no='{$new_staff}'";
    }elseif($type == 'evaluator'){
        $ev_relation_chk_sql = "SELECT COUNT(ev_r_no) as cnt FROM evaluation_relation WHERE ev_no='{$ev_no}' AND receiver_s_no='{$staff}' AND evaluator_s_no='{$new_staff}'";
    }

    if(!empty($ev_relation_chk_sql))
    {
        $ev_relation_chk_query  = mysqli_query($my_db, $ev_relation_chk_sql);
        $ev_relation_chk_result = mysqli_fetch_assoc($ev_relation_chk_query);

        if($ev_relation_chk_result['cnt'] == 0){
            $result = true;
        }
    }
}

echo json_encode(array("result" => $result));
?>
