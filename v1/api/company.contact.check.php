<?php
require('../inc/common.php');

$type       = isset($_POST['type']) ? $_POST['type'] : "";
$fir_val    = isset($_POST['fir_val']) ? $_POST['fir_val'] : "";
$sec_val    = isset($_POST['sec_val']) ? $_POST['sec_val'] : "";

$add_check               = "1=1";
$complete_check          = "";
$company_check           = "";
$company_complete_check  = "";
$partner_type            = ($type == 'hp') ? "tel" : "email";

if(!empty($fir_val) && !empty($sec_val))
{

    if($type == 'hp'){
        $add_check             .= " AND (REPLACE(`{$type}`,'-','') LIKE '%{$fir_val}%' OR REPLACE(`{$type}`,'-','') LIKE '%{$sec_val}%')";
        $company_check          = "(REPLACE(`{$partner_type}`,'-','') LIKE '%{$fir_val}%' OR REPLACE(`{$partner_type}`,'-','') LIKE '%{$sec_val}%')";
        $complete_check         = "REPLACE(`{$type}`,'-','') LIKE '%{$fir_val}{$sec_val}'";
        $company_complete_check = "REPLACE(`{$partner_type}`,'-','') LIKE '%{$fir_val}{$sec_val}'";
    }elseif($type == 'email'){
        $add_check             .= " AND (REPLACE(`{$type}`,'-','') LIKE '%{$fir_val}@{$sec_val}%')";
        $company_check          = "(REPLACE(`{$partner_type}`,'-','') LIKE '%{$fir_val}@{$sec_val}%')";
        $complete_check         = "`{$type}` = '{$fir_val}@{$sec_val}'";
        $company_complete_check = "`{$partner_type}` = '{$fir_val}@{$sec_val}'";
    }
}
elseif(!empty($fir_val))
{
    $add_check      .= " AND REPLACE(`{$type}`,'-','') LIKE '%{$fir_val}%'";
    $company_check   = "(REPLACE(`{$partner_type}`,'-','') LIKE '%{$fir_val}%')";

}
elseif(!empty($sec_val))
{
    if($type == 'hp'){
        $add_check      .= " AND REPLACE(`{$type}`,'-','') LIKE '%{$sec_val}%'";
        $company_check   = "(REPLACE(`{$partner_type}`,'-','') LIKE '%{$sec_val}%')";
    }
}

$complete_check_cnt = 0;
if(!empty($complete_check))
{
    $complete_check_sql     = "SELECT count(`{$type}`) as cnt FROM company_contact WHERE {$complete_check}";
    $complete_check_query   = mysqli_query($my_db, $complete_check_sql);
    $complete_check_result  = mysqli_fetch_assoc($complete_check_query);
    $complete_check_cnt     = $complete_check_result['cnt'];
}

$check_sql      = "SELECT `{$type}` FROM company_contact WHERE {$add_check}";
$check_query    = mysqli_query($my_db, $check_sql);
$check_list     = [];
while($check_result = mysqli_fetch_assoc($check_query))
{
    $check_list[] = $check_result[$type];
}

$html = "";
if(!empty($check_list))
{
    $type_name = ($type == 'hp') ? "전화번호" : "이메일";
    $count = count($check_list);

    if($complete_check_cnt > 0){
        $count = $count-$complete_check_cnt;
    }

    $url  = ($type == 'hp') ? "company_contact_management.php?sch_hp1={$fir_val}&sch_hp2={$sec_val}" : "company_contact_management.php?sch_email={$fir_val}";

    if($complete_check_cnt > 0){
        $html .= "<span class='font-red'>※주의 : [파트너 컨택] {$type_name} 일치 ({$complete_check_cnt})건";
    }

    if($count > 0){
        $html .= !empty($html) ? ", {$type_name} 유사 ({$count})건</span><a href='{$url}' target='_blank' style='background: #999; color: white; padding: 3px 5px; margin-left: 5px; cursor: pointer; font-size: 10px;'>새 창 보기</a>" : "<span class='font-red'>※주의 : [파트너 컨택] {$type_name} 유사 ({$count})건</span><a href='{$url}' target='_blank' style='background: #999; color: white; padding: 3px 5px; margin-left: 5px; cursor: pointer; font-size: 10px;'>새 창 보기</a>";
    }else{
        $html .= !empty($html) ? "</span><a href='{$url}' target='_blank' style='background: #999; color: white; padding: 3px 5px; margin-left: 5px; cursor: pointer; font-size: 10px;'>새 창 보기</a>" : "";
    }
}


$company_check_cnt = 0;
if(!empty($company_check))
{
    $company_check_sql      = "SELECT count(`{$partner_type}`) as cnt FROM company WHERE {$company_check}";
    $company_check_query    = mysqli_query($my_db, $company_check_sql);
    $company_check_result   = mysqli_fetch_assoc($company_check_query);
    $company_check_cnt      = $company_check_result['cnt'];
}

$company_complete_cnt = 0;
if(!empty($company_complete_check))
{
    $company_complete_sql       = "SELECT count(`{$partner_type}`) as cnt FROM company WHERE {$company_complete_check}";
    $company_complete_query     = mysqli_query($my_db, $company_complete_sql);
    $company_complete_result    = mysqli_fetch_assoc($company_complete_query);
    $company_complete_cnt       = $company_complete_result['cnt'];
}

if($company_check_cnt > 0 || $company_complete_cnt > 0)
{
    $partner_type_name = ($type == 'hp') ? "전화번호" : "이메일";
    if($company_complete_cnt > 0){
        $html .= !empty($html) ? "<br/>" : "";
        $html .= "<span class='font-red'>※주의 : [파트너사] {$partner_type_name} 일치 ({$company_complete_cnt})건";
    }

    $company_cnt = $company_check_cnt-$company_complete_cnt;

    if($company_cnt > 0){
        if($company_complete_cnt > 0){
            $html .= !empty($html) ? ", {$partner_type_name} 유사 ({$company_cnt})건</span>" : "<span class='font-red'>※주의 : [파트너사] {$partner_type_name} 유사 ({$company_cnt})건</span>";
        }else{
            $html .= !empty($html) ? "<br/><span class='font-red'>※주의 : [파트너사] {$partner_type_name} 유사 ({$company_cnt})건</span>" : "<span class='font-red'>※주의 : [파트너사] {$partner_type_name} 유사 ({$company_cnt})건</span>";
        }
    }else{
        $html .= !empty($html) ? "</span>" : "";
    }
}



$arr = array(
    "result" => true,
    "html"   => $html
);

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
