<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/commerce_utm.php');
require('../inc/model/Kind.php');

$type   = isset($_POST['type']) ? $_POST['type'] : "";
$val    = isset($_POST['val']) ? $_POST['val'] : "";
$result = false;
$value  = "";
$text   = "";

$kind_model             = Kind::Factory();
$comm_utm_camp_option   = getUtmCampaignOption();
if(!empty($type) && !empty($val))
{
    switch($type){
        case 'new_utm_source':
            $kind_item = $kind_model->getKindParent($val);
            $text  = !empty($kind_item) ? $kind_item['k_parent_name'] : "";
            break;
        case 'new_utm_campaign':
            $text  = isset($comm_utm_camp_option[$val]) ? $comm_utm_camp_option[$val] : "";
            break;
    }

    if($text){
        $result = true;
    }

}else{
    $text = "오류가 발생했습니다";
}

$data = array("result" => $result, "text" => $text);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>