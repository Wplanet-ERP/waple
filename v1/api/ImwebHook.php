<?php
/**
 * Created by PhpStorm.
 * User: TH
 * Date: 2020-03-31
 * Time: 오후 2:38
 */
ini_set('display_errors', -1);
class ImwebHook
{
    private $_api_key       = "0e0f269f75c7588fc8647d97cf77b74fd40c5f426b";
    private $_secret_key    = "71a904358778f84c4d8f0d";
    private $_access_token  = "f13e6a95f96e87c7fe1bd05339c1541b";

    private function setAccessToken($token)
    {
        $this->_access_token = $token;
    }

    public function getAccessToken(){
        return $this->_access_token;
    }

    public function initAccessToken()
    {
        $url = "https://api.imweb.me/v2/auth";

        $postData = array(
            "key"       => $this->_api_key,
            "secret"    => $this->_secret_key
        );

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_result = curl_exec($curl);
        curl_close($curl);

        $json_data  = json_decode($curl_result, true);
        $result     = isset($json_data['msg']) ? $json_data['msg'] : "Fail";

        if($result == 'SUCCESS') {
            $this->setAccessToken($json_data['access_token']);
        }
    }

    public function getTotalPage()
    {
        $url = "https://api.imweb.me/v2/member/members";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1) ;
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["access-token: {$this->_access_token}"]);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(['version' => 'latest', 'limit' => '100', 'orderBy' => 'jointime']));

        $curl_result = curl_exec($curl);
        curl_close($curl);

        $json_data  = json_decode($curl_result, true);

        return $json_data['data']['pagenation']['total_page'];
    }

    public function getMemberData($offset)
    {
        $url = "https://api.imweb.me/v2/member/members";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1) ;
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["access-token: {$this->_access_token}"]);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(['version' => 'latest', 'offset' => $offset, 'limit' => '100', 'orderBy' => 'jointime']));

        $curl_result = curl_exec($curl);
        curl_close($curl);

        $json_data   = json_decode($curl_result, true);
        $return_data = isset($json_data['data']) ? $json_data['data']['list'] : [];

        return $return_data;
    }

}