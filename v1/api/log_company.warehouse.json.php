<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "") {

    $parent_data    = $_GET[$_GET['parent_data']];
    $warehouse_type = $_GET['warehouse_type'];

    $add_where = "1=1";
    if(is_array($parent_data)){
        $parent_data = $parent_data[0];
    }

    if(!empty($warehouse_type)){
        $add_where .= " AND `type`='{$warehouse_type}'";
    }
    $add_where .= " AND log_c_no='{$parent_data}'";

    $warehouse_sql = "SELECT warehouse FROM product_cms_stock_warehouse WHERE {$add_where} ORDER BY warehouse ASC";

    $query = mysqli_query($my_db, $warehouse_sql);
    $count = mysqli_num_rows($query);

    echo "[" . PHP_EOL;
    if ($count > 0) {
        echo "{\"\":\"::선택::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
        while ($result = mysqli_fetch_assoc($query)):
            echo "{\"" . $result['warehouse'] . "\":\"" . $result['warehouse'] . "\"}," . PHP_EOL;
        endwhile;
        echo "{\"selected\":\"\"}" . PHP_EOL;
    } else {
        echo "{\"\":\"::선택::\"}" . PHP_EOL;
    }
    echo "]" . PHP_EOL;
}
?>
