<?php
require('../inc/common.php');
require('../ckadmin.php');

$unit_no    = isset($_POST['unit_no']) ? $_POST['unit_no'] : "";
$result     = false;
$unit_data  = [];

if(!empty($unit_no))
{
    $unit_sql       = "
        SELECT
            pcu.no,
            pcu.brand,
            pcu.type,
            (SELECT sub_c.c_name FROM company as sub_c WHERE sub_c.c_no = pcu.brand) as brand_name,
            pcu.sup_c_no,
            c.c_name as sup_c_name,
            pcu.ord_s_no,
            pcu.ord_sub_s_no,
            pcu.ord_third_s_no,
            (SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_s_no) as ord_s_name,
            (SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_sub_s_no) as ord_sub_s_name,
            (SELECT s.s_name FROM staff s WHERE s.s_no = pcu.ord_third_s_no) as ord_third_s_name,
            pcu.option_name,
            pcu.sup_price,
            pcu.sup_price_vat,			
            pcu.priority,
            pcu.description,
            pcu.display,
            pcu.is_control,
            c.license_type
        FROM product_cms_unit pcu
        LEFT JOIN company as c ON c.c_no = pcu.sup_c_no
        WHERE pcu.`no`='{$unit_no}'
    ";
    $unit_query     = mysqli_query($my_db, $unit_sql);
    $unit_data      = mysqli_fetch_assoc($unit_query);
    $chk_unit_ware_list  = [];
   if($unit_data['no'])
   {
       $result = true;

       $chk_unit_ware_sql   = "SELECT pcum.prd_unit, pcum.log_c_no, pcum.warehouse, pcum.sku FROm product_cms_unit_management AS pcum WHERE pcum.prd_unit='{$unit_no}' LIMIT 1";
       $chk_unit_ware_query = mysqli_query($my_db, $chk_unit_ware_sql);
       while($chk_unit_ware = mysqli_fetch_assoc($chk_unit_ware_query))
       {
           $chk_unit_ware_list[] = $chk_unit_ware;
       }
   }

    $unit_data['ware_list'] = $chk_unit_ware_list;
}

$data = array("result" => $result, "unit_data" => $unit_data);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>