<?php
require('../inc/common.php');
require('../inc/helper/product.php');

$staff_list = [];
if($data = $_POST['task_team'])
{
    $sql    = "SELECT s.s_no, s.s_name FROM staff s WHERE s.staff_state<>'3' AND s.team IN(".implode(',',$_POST['task_team']).")";
    $query  = mysqli_query($my_db,$sql);
    $count  = mysqli_num_rows($query);

    if($t_req_accepter = $_POST['req_staff']){
        foreach($t_req_accepter as $t_req){
            $staff_list[] = array('value' => $t_req, 'title' => $staff_name_list[$t_req]['s_name']);
        }
    }

    if($t_run_staff = $_POST['run_staff']){
        foreach($t_run_staff as $t_run){
            $staff_list[] = array('value' => $t_run, 'title' => $staff_name_list[$t_run]['s_name']);
        }
    }

    if($t_run_d_staff = $_POST['def_staff']){
        $staff_list[] = array('value' => $t_run_d_staff, 'title' => $staff_name_list[$t_run_d_staff]['s_name']);
    }

    if($count > 0)
    {
        while($result = mysqli_fetch_assoc($query))
        {
            $staff_list[] = array(
                'value' => $result['s_no'],
                'title' => $result['s_name']
            );
        }
    }

    $staff_list = array_map("unserialize", array_unique(array_map("serialize", $staff_list)));;
    sort($staff_list);
}
echo json_encode($staff_list);

?>
