<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_message.php');

$temp_no   = isset($_POST['temp_no']) ? $_POST['temp_no'] : "";
$team_data = [];

if(!empty($temp_no))
{
    $temp_sql       = "SELECT * FROM crm_template `ct` WHERE ct.t_no='{$temp_no}' LIMIT 1";
    $temp_query     = mysqli_query($my_db, $temp_sql);
    $temp_result    = mysqli_fetch_assoc($temp_query);
    $crm_temp_type_option = getCrmTempTypeOption();

   $team_data = array(
       'new_temp_type'      => $temp_result['temp_type'],
       'new_temp_type_name' => $crm_temp_type_option[$temp_result['temp_type']],
       'new_send_name'      => $temp_result['send_name']
   );

}

$data = array('result' => $team_data);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>