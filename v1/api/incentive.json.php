<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/withdraw.php');
require('../inc/helper/deposit.php');

if ($_GET['parent_data'] != "") {

    $parent_data_get = $_GET[$_GET['parent_data']];

    if($_GET['parent_data'] == "sch_manager_team")
    {
        $team_code              = $_GET[$_GET['parent_data']];
        $sch_team_code_list 	= getTeamWhere($my_db, $team_code);
        $staff_team_where 		= "";
        $staff_team_where_list 	= [];

        if($team_code == 'all')
        {
            $sql="SELECT s.s_no, s.s_name FROM staff s WHERE s.staff_state ='1' ORDER BY s_name ASC";

        }else{
            if($sch_team_code_list){
                $sch_team_code_list_val = explode(",", $sch_team_code_list);

                foreach($sch_team_code_list_val as $sch_team_code){
                    $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
                }
            }

            if($staff_team_where_list){
                $staff_team_where = implode(" OR ", $staff_team_where_list);
            }

            $sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
        }


        $query=mysqli_query($my_db,$sql);
        $count=mysqli_num_rows($query);

        echo "[".PHP_EOL;
        if($count>0) {
            if($_GET['parent_data'] == "sch_manager_team"){
                echo "{\"all\":\"::전체::\"}".(($count>0)?",":"").PHP_EOL;
            }
            while($result=mysqli_fetch_assoc($query)):
                echo "{\"".$result['s_no']."\":\"".$result['s_name']."\"},".PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}".PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}".PHP_EOL;
        }
        echo "]".PHP_EOL;
    }
    elseif ($_GET['parent_data'] == "sch_incentive_type") # 발주담당자 선택시 공급업체
    {
        $deposit_method_option  = getDpMethodOption();
        $withdraw_method_option = getWdMethodOption();
        $incentive_type_option  = ($_GET[$_GET['parent_data']] == 'deposit') ? $deposit_method_option : $withdraw_method_option;

        echo "[" . PHP_EOL;
        echo "{\"\":\"::전체::\"},". PHP_EOL;

        foreach ($incentive_type_option as $key => $label):
            echo "{\"" . $key . "\":\"" . $label . "\"}," . PHP_EOL;
        endforeach;
        echo "{\"selected\":\"\"}" . PHP_EOL;
        echo "]" . PHP_EOL;

    }
}
?>
