<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "")
{
    $quick_prd      = $_GET[$_GET['parent_data']];
    $quick_type     = isset($_GET['quick_type']) ? $_GET['quick_type'] : "work";
    $with_log_c_no  = '2809';

    $sql = "";
    if ($_GET['parent_data'] == "f_prd_g1")
    {
        switch($quick_type){
            case 'work':
                $sql = "SELECT k_name_code, k_name from kind WHERE display='1' AND k_code='product' AND k_parent='{$quick_prd}' ORDER BY priority ASC";
                break;
            case 'cms':
            case 'cms_with':
                $sql = "SELECT k_name_code, k_name from kind WHERE display='1' AND k_code='product_cms' AND k_parent='{$quick_prd}' ORDER BY priority ASC";
                break;
            case 'cms_unit':
                $sql = "SELECT `no` as k_name_code, option_name as k_name from product_cms_unit WHERE display='1' AND brand='{$quick_prd}' ORDER BY priority ASC";
                break;
            case 'cms_unit_with':
                $sql = "SELECT `no` as k_name_code, (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcu.`no` AND pcum.log_c_no='{$with_log_c_no}') as k_name from product_cms_unit pcu WHERE display='1' AND brand='{$quick_prd}' ORDER BY priority ASC";
                break;
            case 'navigation':
                $sql = "SELECT `no` as k_name_code, nav_name as k_name from navigation WHERE display='1' AND nav_kind='waple' AND parent_nav_code = (SELECT nav_code FROM navigation sub WHERE sub.`no`='{$quick_prd}') ORDER BY priority ASC";
                break;
            case 'staff':
                $add_team_where = getTeamWhere($my_db, $quick_prd);
                $sql = "SELECT s_no as k_name_code, s_name as k_name from staff WHERE staff_state='1' AND team IN($add_team_where) ORDER BY s_no ASC";
                break;
        }
        $query = mysqli_query($my_db, $sql);
        $count = mysqli_num_rows($query);

        echo "[" . PHP_EOL;
        if ($count > 0) {
            if ($_GET['parent_data'] == "f_prd_g1") {
                echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
            }
            while ($result = mysqli_fetch_assoc($query)):
                echo "{\"" . $result['k_name_code'] . "\":\"" . $result['k_name'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;

    }
    else if ($_GET['parent_data'] == "f_prd_g2")
    {
        if($quick_type == 'work'){
            $sql = "SELECT prd_no, title FROM product WHERE display='1' AND k_name_code='{$quick_prd}' ORDER BY priority ASC";
        } elseif($quick_type == 'cms' || $quick_type == 'cms_with') {
            $sql = "SELECT prd_no, title FROM product_cms WHERE display='1' AND k_name_code='{$quick_prd}' ORDER BY priority ASC";
        }elseif($quick_type == 'navigation') {
            $sql = "SELECT `no` as prd_no, nav_name as title FROM navigation WHERE display='1' AND nav_kind='waple' AND parent_nav_code = (SELECT nav_code FROM navigation sub WHERE sub.`no`='{$quick_prd}') ORDER BY priority ASC";
        }

        $query = mysqli_query($my_db, $sql);
        $count = mysqli_num_rows($query);

        echo "[" . PHP_EOL;
        if ($count > 0) {
            if ($_GET['parent_data'] == "f_prd_g2") {
                echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
            }

            while ($result = mysqli_fetch_assoc($query)):
                echo "{\"" . $result['prd_no'] . "\":\"" . $result['title'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;

    }
}
?>
