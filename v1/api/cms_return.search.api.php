<?php
require('../inc/common.php');

$delivery_no    = isset($_POST['delivery_no'])?$_POST['delivery_no']:"";
$parent_ord_no  = isset($_POST['parent_ord_no'])?$_POST['parent_ord_no']:"";
$return_deli_no = isset($_POST['return_deli_no'])?$_POST['return_deli_no']:"";
$recipient      = isset($_POST['recipient'])?$_POST['recipient']:"";
$recipient_hp   = isset($_POST['recipient_hp'])?$_POST['recipient_hp']:"";
$recipient_addr = isset($_POST['recipient_addr'])?$_POST['recipient_addr']:"";
$zip_code       = isset($_POST['zip_code'])?$_POST['zip_code']:"";

$add_where = "1=1";

if(!empty($delivery_no)){
    $add_where .= " AND r.parent_order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery WHERE delivery_no='{$delivery_no}')";
}

if(!empty($parent_ord_no)){
    $add_where .= " AND r.parent_order_number = '{$parent_ord_no}'";
}

if(!empty($return_deli_no)){
    $add_where .= " AND (r.return_delivery_no='{$return_deli_no}' OR r.return_delivery_no2='{$return_deli_no}')";
}

if(!empty($recipient)){
    $add_where .= " AND r.recipient LIKE '%{$recipient}%'";
}

if(!empty($recipient_hp)){
    $add_where .= " AND r.recipient_hp LIKE '%{$recipient_hp}%'";
}

if(!empty($recipient_addr)){
    $add_where .= " AND r.recipient_addr LIKE '%{$recipient_addr}%'";
}

if(!empty($zip_code)){
    $add_where .= " AND r.zip_code='{$zip_code}'";
}

$cms_return_total_sql    = "SELECT count(DISTINCT order_number) AS cnt FROM (SELECT r.order_number FROM work_cms_return r WHERE {$add_where}) AS cnt";
$cms_return_total_query  = mysqli_query($my_db, $cms_return_total_sql);
$cms_return_total_result = mysqli_fetch_array($cms_return_total_query);
$cms_return_result       = ($cms_return_total_result['cnt'] > 0) ? true : false;

$data = array("result" => $cms_return_result);
echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>