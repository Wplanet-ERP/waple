<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$team_code = isset($_POST['team_code'])?$_POST['team_code']:"";

// 리스트 쿼리
$team_sql = "SELECT (SELECT sub_t.team_name FROM team sub_t WHERE sub_t.team_code=t.team_code_parent LIMIT 1) as team_parent_name, team_code_parent, team_group, team_name, priority, display FROM team t WHERE t.team_code='{$team_code}' LIMIT 1";

$result = false;
$team_query = mysqli_query($my_db, $team_sql);
$team_result = mysqli_fetch_assoc($team_query);

$team = "";
if(isset($team_result['team_name'])){
    $result = true;
    $team = $team_result;
}

$arr = array("result" => $result, "team" => $team);

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
