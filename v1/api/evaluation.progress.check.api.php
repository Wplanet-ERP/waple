<?php
require('../inc/common.php');
require('../ckadmin.php');

$ev_no      = isset($_POST['ev_no'])?$_POST['ev_no']:"";
$ev_r_no    = isset($_POST['ev_r_no'])?$_POST['ev_r_no']:"";
$result_msg = "오류가 발생했습니다.";
$result     = false;
$page       = 1;

if(!empty($ev_no) && !empty($ev_r_no))
{
    $ev_relation_chk_sql    = "SELECT esr.ev_result_no, (SELECT eu.page FROM evaluation_unit eu WHERE eu.ev_u_no=esr.ev_u_no) as page FROM evaluation_system_result esr WHERE ev_no='{$ev_no}' AND ev_r_no='{$ev_r_no}' AND (evaluation_value = '' OR evaluation_value IS NULL OR evaluation_value = '0') AND essential = '1' ORDER BY `order` LIMIT 1";
    $ev_relation_chk_query  = mysqli_query($my_db, $ev_relation_chk_sql);
    $ev_relation_chk_result = mysqli_fetch_assoc($ev_relation_chk_query);

    if(!isset($ev_relation_chk_result['ev_result_no'])){
        $result     = true;
        $result_msg = "완료되었습니다.";
    }else{
        $result_msg = "필수항목이 입력 되지 않았습니다.";
        $page       = $ev_relation_chk_result['page'];
    }
}

echo json_encode(array("result" => $result, "result_msg" => $result_msg, "page" => $page));
?>
