<?php
require('../inc/common.php');
require('../ckadmin.php');

$brand  = isset($_POST['brand']) ? $_POST['brand'] : "";
$result = false;
$brand_data  = [];

if(!empty($brand))
{
    $brand_sql    = "SELECT c_no, (SELECT my.c_name FROM my_company my WHERE my.my_c_no=c.my_c_no) as my_c_name, c.s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=c.s_no) as s_name, (SELECT s.team FROM staff s WHERE s.s_no=c.s_no) as team, (SELECT t.team_name FROM team t WHERE t.team_code=(SELECT s.team FROM staff s WHERE s.s_no=c.s_no)) as team_name FROM company as c WHERE c_no='{$brand}'";
    $brand_query  = mysqli_query($my_db, $brand_sql);
    $brand_result = mysqli_fetch_assoc($brand_query);
    if(isset($brand_result['c_no']) && !empty($brand_result['c_no']))
    {
        $brand_data['s_no']      = $brand_result['s_no'];
        $brand_data['team']      = $brand_result['team'];
        $brand_data['my_c_name'] = $brand_result['my_c_name'];
        $brand_data['s_name']    = $brand_result['s_name'];
        $brand_data['t_name']    = $brand_result['team_name'];
    }

    $result=true;
}

$data = array("result" => $result, "brand" => $brand_data);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>