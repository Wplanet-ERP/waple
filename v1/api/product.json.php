<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "") {

    $parent_data_get = $_GET[$_GET['parent_data']];
    $k_code_get = isset($_GET['sch_k_code']) ? $_GET['sch_k_code'] : "product";

    if ($_GET['parent_data'] == "f_prd_g1" || $_GET['parent_data'] == "new_f_prd_g1" || $_GET['parent_data'] == "sch_prd_g1" || $_GET['parent_data'] == "mod_prd_g1")
    { // 상품그룹2의 경우
        $sql = "SELECT k_name_code,k_name from kind WHERE display='1' AND k_code='{$k_code_get}' AND k_parent='" . $parent_data_get . "' ORDER BY priority ASC";

        $query = mysqli_query($my_db, $sql);
        $count = mysqli_num_rows($query);

        echo "[" . PHP_EOL;
        if ($count > 0) {
            if ($_GET['parent_data'] == "sch_prd_g1") {
                echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
            }
            while ($result = mysqli_fetch_assoc($query)):
                echo "{\"" . $result['k_name_code'] . "\":\"" . $result['k_name'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;

    }
    else if ($_GET['parent_data'] == "f_prd_g2" || $_GET['parent_data'] == "new_f_prd_g2" || $_GET['parent_data'] == "sch_prd_g2" || $_GET['parent_data'] == "mod_prd_g2")
    { // 상품 목록의 경우
        $sql = "SELECT prd_no, title FROM {$k_code_get} WHERE display='1' AND k_name_code='" . $parent_data_get . "' ORDER BY priority ASC";

        $query = mysqli_query($my_db, $sql);
        $count = mysqli_num_rows($query);

        echo "[" . PHP_EOL;
        if ($count > 0) {
            if ($_GET['parent_data'] == "sch_prd_g2") {
                echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
            }

            while ($result = mysqli_fetch_assoc($query)):
                if($result['prd_no'] == '179' && $session_team != "00211"){
                    continue;
                }
                echo "{\"" . $result['prd_no'] . "\":\"" . $result['title'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;

    }
    else if ($_GET['parent_data'] == "f_prd")
    { // 상품 소개의 경우
        $k_parent 	 = sprintf('%05d', $parent_data_get);
        $sql 	     = "SELECT k_name_code, k_name FROM kind WHERE k_code='work_task_run' AND k_parent='{$k_parent}' AND display='1' ORDER BY priority";

        $query = mysqli_query($my_db, $sql);
        $count = mysqli_num_rows($query);

        echo "[" . PHP_EOL;
        if ($count > 0) {
            if ($_GET['parent_data'] == "f_prd") {
                echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
            }

            while ($result = mysqli_fetch_assoc($query)):
                echo "{\"" . $result['k_name_code'] . "\":\"" . $result['k_name'] . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;
    }
}
?>
