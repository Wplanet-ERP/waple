<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "") {

    $parent_data_get = $_GET[$_GET['parent_data']];

    $sql = "SELECT s_no, s_name from staff WHERE team='{$parent_data_get}' AND staff_state='1' ORDER BY s_no ASC";

    $query = mysqli_query($my_db, $sql);
    $count = mysqli_num_rows($query);

    echo "[" . PHP_EOL;
    if ($count > 0) {
        if ($_GET['parent_data'] == "sch_team" ) {
            echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
        }
        while ($result = mysqli_fetch_assoc($query)):
            echo "{\"" . $result['s_no'] . "\":\"" . $result['s_name'] . "\"}," . PHP_EOL;
        endwhile;
        echo "{\"selected\":\"\"}" . PHP_EOL;
    } else {
        echo "{\"\":\"::전체::\"}" . PHP_EOL;
    }
    echo "]" . PHP_EOL;
}
?>
