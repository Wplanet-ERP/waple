<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$keyword = isset($_GET['keyword'])?$_GET['keyword']:"";


// 리스트 쿼리
$sql    = "SELECT keyword FROM staff_strength_keyword WHERE keyword like '%{$keyword}%'";
$query  = mysqli_query($my_db, $sql);

while ($strength_keyword = mysqli_fetch_assoc($query)) {
    $arr[] = array(
        "keyword"    => $strength_keyword['keyword']
    );
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
