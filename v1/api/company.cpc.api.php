<?php
require('../inc/common.php');

$c_no   = isset($_POST['c_no'])?$_POST['c_no']:"";
$type   = isset($_POST['type'])?$_POST['type']:"";
$result = false;

if(!!$c_no)
{

    $cpc_type_sql    = "SELECT kakao, naver FROM company WHERE c_no='{$c_no}'";
    $cpc_type_query  = mysqli_query($my_db,  $cpc_type_sql);
    $cpc_type_result = mysqli_fetch_assoc($cpc_type_query);

    $cpc_id  = "";
    if($type == '814'){
        $cpc_id  = $cpc_type_result['naver'];
    }elseif($type == '1079'){
        $cpc_id  = $cpc_type_result['kakao'];
    }

    $data = !empty($cpc_id) ? array("result" => true, "cpc_id" => $cpc_id) : array("result" => true, "cpc_id" => "");
}else{
    $data = array("result" => false, "cpc_id" => "");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
