<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "") {

    $parent_data    = $_GET[$_GET['parent_data']];
    $state_type     = isset($_GET['state_type']) ? $_GET['state_type'] : "";

    if ($_GET['parent_data'])
    {
        $corp_sql   = "SELECT co_no, `name` FROM corp_account WHERE display='1' AND my_c_no='{$parent_data}' AND `type`='{$state_type}'";
        $corp_query = mysqli_query($my_db, $corp_sql);
        $corp_count = mysqli_num_rows($corp_query);

        echo "[" . PHP_EOL;
        if ($corp_count > 0) {
            if ($_GET['parent_data']) {
                echo "{\"\":\"::전체::\"}" . (($corp_count > 0) ? "," : "") . PHP_EOL;
            }
            while ($corp_result = mysqli_fetch_assoc($corp_query)):
                echo "{\"" . $corp_result['co_no'] . "\":\"" . $corp_result['name'] . "\"}," . PHP_EOL;
            endwhile;

            if($state_type == '1' && $_GET['parent_data'] == 'f_my_c_no' && $parent_data == '1'){
                echo "{\"5\":\"기업-지출\"}," . PHP_EOL;
            }

            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;

    }
}
?>
