<?php
require('../inc/common.php');
require('../ckadmin.php');

if ($_GET['parent_data'] != "")
{
    $parent_data_get        = $_GET[$_GET['parent_data']];
    if ($_GET['parent_data'] == "sch_comm_g1")
    { // 상품그룹2의 경우
        $sql = "SELECT k_code, k_name_code, k_name, display from kind WHERE k_parent='{$parent_data_get}' ORDER BY priority ASC";

        $query = mysqli_query($my_db, $sql);
        $count = mysqli_num_rows($query);

        echo "[" . PHP_EOL;
        if ($count > 0)
        {
            if ($_GET['parent_data'] == "sch_comm_g1") {
                echo "{\"\":\"::전체::\"}" . (($count > 0) ? "," : "") . PHP_EOL;
            }
            while ($result = mysqli_fetch_assoc($query)):
                $k_name = ($result['display'] != '1') ? $result['k_name']." [OFF]" : $result['k_name'];
                echo "{\"" . $result['k_name_code'] . "\":\"" . $k_name . "\"}," . PHP_EOL;
            endwhile;
            echo "{\"selected\":\"\"}" . PHP_EOL;
        } else {
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
        }
        echo "]" . PHP_EOL;

    }
}
?>
