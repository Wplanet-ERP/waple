<?php
require('../inc/common.php');
header("Content-Type: text/html; charset=UTF-8");

$com_no   = isset($_POST['com_no'])?$_POST['com_no']:"";
$b_no     = isset($_POST['b_no'])?$_POST['b_no']:"";
$b_id     = isset($_POST['b_id'])?$_POST['b_id']:"";
$s_no     = isset($_POST['s_no'])?$_POST['s_no']:"";

$result         = false;
$like_result    = false;
$like_cnt       = 0;
$msg            = "";

if(!empty($com_no))
{
    $board_com_like_cnt_sql    = "SELECT count(l_no) as cnt FROM comment_like WHERE b_no='{$b_no}' AND b_id='{$b_id}' AND s_no='{$s_no}' AND `like`='1'";
    $board_com_like_cnt_query  = mysqli_query($my_db, $board_com_like_cnt_sql);
    $board_com_like_cnt_result = mysqli_fetch_assoc($board_com_like_cnt_query);

    $board_com_like_s_count = isset($board_com_like_cnt_result['cnt']) ? $board_com_like_cnt_result['cnt'] : 0;

    // 리스트 쿼리
    $like_chk_sql    = "SELECT `like` FROM `comment_like` WHERE com_no='{$com_no}' AND s_no='{$s_no}'";
    $like_chk_query  = mysqli_query($my_db, $like_chk_sql);
    $like_chk_result = mysqli_fetch_assoc($like_chk_query);

    $like = '1';
    if(!empty($like_chk_result) && isset($like_chk_result['like'])){
        $like         = ($like_chk_result['like'] != '1') ? "1" : "2";
        $com_like_sql = "UPDATE comment_like SET `like`='{$like}' WHERE com_no='{$com_no}' AND s_no='{$s_no}'";
    }else{
        $com_like_sql = "INSERT INTO comment_like SET `s_no`='{$s_no}', `b_id`='{$b_id}', `b_no`='{$b_no}', `com_no`='{$com_no}', `like`='{$like}'";
    }

    if($like == '1'){
        $board_com_like_s_count++;
    }else{
        $board_com_like_s_count--;
    }

    $result = true;
    if(mysqli_query($my_db, $com_like_sql)){
        $like_cnt_sql    = "SELECT count(l_no) as cnt FROM comment_like WHERE com_no='{$com_no}' AND `like`='1'";
        $like_cnt_query  = mysqli_query($my_db, $like_cnt_sql);
        $like_cnt_result = mysqli_fetch_assoc($like_cnt_query);
        $like_cnt         = isset($like_cnt_result['cnt']) ? $like_cnt_result['cnt'] : 0;

        $like_result = true;
    }else{
        $msg = "오류가 발생했습니다";
    }

    /**
    if($board_com_like_s_count > 3)
    {
        $result = true;
        $msg    = "3개까지만 좋아요를 누를 수 있습니다";
    }
    else
    {
        $result = true;
        if(mysqli_query($my_db, $com_like_sql)){
            $like_cnt_sql    = "SELECT count(l_no) as cnt FROM comment_like WHERE com_no='{$com_no}' AND `like`='1'";
            $like_cnt_query  = mysqli_query($my_db, $like_cnt_sql);
            $like_cnt_result = mysqli_fetch_assoc($like_cnt_query);
            $like_cnt         = isset($like_cnt_result['cnt']) ? $like_cnt_result['cnt'] : 0;

            $like_result = true;
        }else{
            $msg = "오류가 발생했습니다";
        }
    }
     */
}else{
    $msg = "오류가 발생했습니다";
}

$data = array("result" => $result, "like_result" => $like_result, "like_cnt" => $like_cnt, "msg" => $msg);

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>