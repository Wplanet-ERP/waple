<?php
require('../inc/common.php');

$ev_no      = isset($_POST['ev_no'])?$_POST['ev_no']:"";
$ev_u_no    = isset($_POST['ev_u_no'])?$_POST['ev_u_no']:"";
$s_no       = isset($_POST['s_no'])?$_POST['s_no']:"";
$result     = false;

if(!empty($ev_no) && !empty($ev_u_no) && !empty($s_no)){
    // 리스트 쿼리
    $ev_result_sql = "SELECT evaluation_value, evaluator_s_no FROM evaluation_system_result WHERE ev_no='{$ev_no}' AND ev_u_no='{$ev_u_no}' AND receiver_s_no='{$s_no}' ORDER BY ev_result_no ASC";

    $query = mysqli_query($my_db, $ev_result_sql);
    $html  = "";

    while ($ev_result = mysqli_fetch_assoc($query))
    {
        $value = $ev_result['evaluation_value'];
        $chk_s_no = $ev_result['evaluator_s_no'];
        
        if($chk_s_no == $s_no){
            $html .= "<p><b>○ 자기평가 : {$value}</b></p>";
        }else{
            $html .= "<p>○ {$value}</p>";
        }
    }

    $data = !empty($html) ? array("result" => true, "html" => $html) : array("result" => false, "html" => $html);
}else{
    $data = array("result" => false, "html" => "");
}

echo json_encode($data, JSON_UNESCAPED_UNICODE);

?>
