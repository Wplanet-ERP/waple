<?php
require('../inc/common.php');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$s_name_get = isset($_GET['s_name'])?$_GET['s_name']:"";


// 리스트 쿼리
$staff_sql = "SELECT s.s_no, s.s_name FROM staff s WHERE s.s_name like '%{$s_name_get}%' AND s.staff_state=1";

$query = mysqli_query($my_db,$staff_sql);

while ($staff_array = mysqli_fetch_assoc($query)) {
    $arr[] = array(
        "s_no"      => $staff_array['s_no'],
        "s_name"    => $staff_array['s_name']
    );
}

// JSON_UNESCAPED_UNICODE 옵션은 한글그대로 표기하도록
echo json_encode($arr, JSON_UNESCAPED_UNICODE);

?>
