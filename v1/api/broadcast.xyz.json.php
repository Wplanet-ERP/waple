<?php
require('../inc/common.php');
require('../inc/helper/_date.php');
require('../inc/helper/broadcast.php');
require('../inc/model/BroadCast.php');

if(($parent_type = $_GET['parent_data']) && !empty($_GET[$_GET['parent_data']]))
{
    $type = "";
    $data = "";

    if($parent_type == 'sch_x_type' || $parent_type == 'sch_y_type') {
        $type  = $_GET[$parent_type];
    }

    $type_day_option    = getDayChartOption();
    $type_media_option  = getBaMediaOption();
    $type_time_option   = getTimeChartOption();
    $type_date_option   = getDateChartOption();
    $broadcastModel     = BroadCast::Factory();
    $type_advertiser_option = $broadcastModel->getAdvertiseOption();

    switch($type)
    {
        case 'day':
            echo "[" . PHP_EOL;
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
            foreach($type_day_option as $key => $label){
                echo ",{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
            }
            echo "]" . PHP_EOL;
            break;

        case 'media':
                echo "[" . PHP_EOL;
                echo "{\"\":\"::전체::\"}" . PHP_EOL;
            foreach($type_media_option as $key => $label){
                echo ",{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
            }
                echo "]" . PHP_EOL;
            break;

        case 'time':
            echo "[" . PHP_EOL;
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
            foreach($type_time_option as $key => $label){
                echo ",{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
            }
            echo "]" . PHP_EOL;
            break;

        case 'advertiser':
            echo "[" . PHP_EOL;
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
            foreach($type_advertiser_option as $key => $label){
                echo ",{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
            }
            echo "]" . PHP_EOL;
            break;

        case 'date':
            echo "[" . PHP_EOL;
            echo "{\"\":\"::전체::\"}" . PHP_EOL;
            foreach($type_date_option as $key => $label){
                echo ",{\"" . $key . "\":\"" . $label . "\"}" . PHP_EOL;
            }
            echo "]" . PHP_EOL;
            break;
    }
}

?>
