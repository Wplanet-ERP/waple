<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/project.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

$pj_s_no        = isset($_GET['pj_s_no']) ? $_GET['pj_s_no'] : "";
$project_list   = [];
if(!empty($pj_s_no))
{
    $add_where = "`p`.display='1' AND `p`.pj_no IN(SELECT pj_no FROM project_input_report WHERE pj_s_no='{$pj_s_no}' AND active='1')";

    // 전체 게시물 수
    $pj_total_sql	= "SELECT count(pj_no) FROM (SELECT `p`.pj_no FROM project `p` WHERE {$add_where}) AS cnt";
    $pj_total_query	= mysqli_query($my_db, $pj_total_sql);
    if(!!$pj_total_query)
        $pj_total_result = mysqli_fetch_array($pj_total_query);

    $pj_staff_total = $pj_total_result[0];

    $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= 10;
    $offset 	= ($pages-1) * $num;
    $pagenum 	= ceil($pj_staff_total/$num);

    if ($pages >= $pagenum){$pages = $pagenum;}
    if ($pages <= 0){$pages = 1;}

    $search_url = getenv("QUERY_STRING");
    $pagelist	= pagelist($pages, "project_list_iframe.php?pj_s_no={$pj_s_no}", $pagenum, $search_url);
    $smarty->assign("total_num", $pj_staff_total);
    $smarty->assign("pagelist", $pagelist);

    $project_sql = "
        SELECT 
            *, 
            (SELECT t.depth FROM team t WHERE t.team_code = p.team) as t_depth,
            (SELECT ps.s_name FROM project_staff ps WHERE ps.pj_s_no = '{$pj_s_no}') as pj_s_name,
            (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=p.my_c_no) as my_c_name,
            (SELECT s.s_name FROM staff s WHERE s.s_no = p.req_s_no) as req_s_name,
            (SELECT COUNT(pir.pj_r_no) FROM project_input_report as pir WHERE pir.pj_no = p.pj_no AND pir.active='1') as total_pj_staff,
            (SELECT SUM(pir.resource) FROM project_input_report as pir WHERE pir.pj_no = p.pj_no AND pir.active='1') as total_pj_resource,
            (SELECT pir.pir_s_date FROM project_input_report as pir WHERE pir.pj_no = p.pj_no AND pir.pj_s_no='{$pj_s_no}' AND pir.active='1') as pir_s_date,
            (SELECT pir.pir_e_date FROM project_input_report as pir WHERE pir.pj_no = p.pj_no AND pir.pj_s_no='{$pj_s_no}' AND pir.active='1') as pir_e_date,
            (SELECT pir.resource FROM project_input_report as pir WHERE pir.pj_no = p.pj_no AND pir.pj_s_no='{$pj_s_no}' AND pir.active='1') as pj_resource
        FROM project `p` 
        WHERE {$add_where}
        ORDER BY p.pj_no DESC
        LIMIT {$offset}, {$num}
     ";

    $project_query   = mysqli_query($my_db, $project_sql);
    $idx             = 0;
    $state_option    = getProjectStateOption();
    $cal_state_option= getCalStateOption();
    while($project = mysqli_fetch_assoc($project_query))
    {
        $project['team_name']           = getTeamFullName($my_db, $project['t_depth'], $project['team']);
        $project['state_name']          = isset($state_option[$project['state']]) ? $state_option[$project['state']] : "";
        $project['state_color']         = $project['state'] == '2' ? 'black' : 'red';
        $project['cal_state_color']     = $project['cal_state'] == '2' ? 'black' : 'red';
        $project['cal_state_name']      = isset($cal_state_option[$project['cal_state']]) ? $cal_state_option[$project['cal_state']] : "";
        $project['total_pj_staff']      = isset($project['total_pj_staff']) && !empty($project['total_pj_staff']) ? $project['total_pj_staff'] : '0';
        $project['total_pj_resource']   = isset($project['total_pj_resource']) && !empty($project['total_pj_resource']) ? $project['total_pj_resource'] : '0';

        if($idx == 0) {
            $smarty->assign('pj_s_name', $project['pj_s_name']);
        }

        $project_list[] = $project;
        $idx++;
    }
}

$smarty->assign("project_list", $project_list);

$smarty->display('project_list_iframe.html');
?>
