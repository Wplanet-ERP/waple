<?php
require('inc/common.php');
require('ckadmin.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

# 기본 변수 설정
$pj_no    = isset($_GET['pj_no']) ? $_GET['pj_no'] : "";
$list_url = "project_expenses_input_iframe.php";
$editable = false;
$project  = [];

$pj_exp_kind_list   = [];
$empty_inner_price  = 0;
$empty_comp_price   = 0;
$total_price        = 0;

# 프로세스 처리
$process  = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'add_pr_exp_kind')
{
    $pj_no      = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $max_sql    = "SELECT MAX(k_priority) as priority FROM project_expenses_kind WHERE pj_no='{$pj_no}'";
    $max_query  = mysqli_query($my_db, $max_sql);
    $max_result = mysqli_fetch_assoc($max_query);
    $priority   = isset($max_result['priority']) && !empty($max_result['priority']) ? $max_result['priority']+1 : 1;
    $kind_sql   = "INSERT INTO `project_expenses_kind` SET pj_no='{$pj_no}', `k_name`='', `k_price`=0, `k_priority`='{$priority}'";

    if(!mysqli_query($my_db, $kind_sql)){
        alert("구분 추가에 실패했습니다", "{$list_url}?pj_no={$pj_no}");
    }else{
        exit("<script>location.href='{$list_url}?pj_no={$pj_no}';</script>");
    }
}
elseif($process == 'modify_k_price')
{
    $pj_no    = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_k_no  = isset($_POST['pj_k_no']) ? $_POST['pj_k_no'] : "";
    $k_price  = isset($_POST['k_price']) ? str_replace(',', '', $_POST['k_price']) : "";
    $upd_sql  = "UPDATE `project_expenses_kind` SET k_price='{$k_price}' WHERE pj_k_no='{$pj_k_no}' AND pj_no='{$pj_no}'";

    if(!mysqli_query($my_db, $upd_sql)){
        alert("산출내역서 금액 변경에 실패했습니다", "{$list_url}?pj_no={$pj_no}");
    }else{
        exit("<script>location.href='{$list_url}?pj_no={$pj_no}';</script>");
    }
}
elseif($process == 'modify_k_priority')
{
    $pj_no    = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_k_no  = isset($_POST['pj_k_no']) ? $_POST['pj_k_no'] : "";
    $priority = isset($_POST['k_priority']) ? $_POST['k_priority'] : "";

    $priority_sql   = "SELECT pj_k_no, k_priority FROM project_expenses_kind WHERE pj_k_no!='{$pj_k_no}' AND pj_no='{$pj_no}' AND k_priority >= '{$priority}' ORDER BY k_priority, k_name";
    $priority_query = mysqli_query($my_db, $priority_sql);
    $upd_priority   = $priority;
    while($priority_val = mysqli_fetch_assoc($priority_query))
    {
        if($priority_val['pj_k_no'])
        {
            $upd_priority++;
            $init_upd_sql = "UPDATE `project_expenses_kind` SET k_priority='{$upd_priority}' WHERE pj_k_no='{$priority_val['pj_k_no']}' AND pj_no='{$pj_no}'";
            mysqli_query($my_db, $init_upd_sql);
        }
    }

    $upd_sql  = "UPDATE `project_expenses_kind` SET k_priority='{$priority}' WHERE pj_k_no='{$pj_k_no}' AND pj_no='{$pj_no}'";

    if(!mysqli_query($my_db, $upd_sql)){
        alert("순번 변경에 실패했습니다", "{$list_url}?pj_no={$pj_no}");
    }else{
        exit("<script>location.href='{$list_url}?pj_no={$pj_no}';</script>");
    }
}
elseif($process == 'modify_k_name')
{
    $pj_no    = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_k_no  = isset($_POST['pj_k_no']) ? $_POST['pj_k_no'] : "";
    $value    = isset($_POST['value']) ? $_POST['value'] : "";
    $upd_sql  = "UPDATE `project_expenses_kind` SET k_name='{$value}' WHERE pj_k_no='{$pj_k_no}' AND pj_no='{$pj_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "구분명 변경에 실패했습니다.";
    else
        echo "구분명이 변경 되었습니다.";
    exit;
}
elseif($process == 'del_expenses')
{
    $pj_no    = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_k_no  = isset($_POST['pj_k_no']) ? $_POST['pj_k_no'] : "";

    if(!empty($pj_no) && !empty($pj_k_no))
    {
        $upd_sql  = "DELETE FROM `project_expenses_kind` WHERE pj_k_no='{$pj_k_no}' AND pj_no='{$pj_no}'";
        if(!mysqli_query($my_db, $upd_sql)){
            alert("구분 삭제에 실패했습니다", "{$list_url}?pj_no={$pj_no}");
        }else{
            exit("<script>location.href='{$list_url}?pj_no={$pj_no}';</script>");
        }
    }
}
elseif($process == 'total_save')
{
    $pj_no             = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_k_no_list      = isset($_POST['pj_k_no']) ? $_POST['pj_k_no'] : [];
    $k_priority_list   = isset($_POST['k_priority']) ? $_POST['k_priority'] : [];
    $k_name_list       = isset($_POST['k_name']) ? $_POST['k_name'] : [];
    $k_price_list      = isset($_POST['k_price']) ? $_POST['k_price'] : [];

    if(!empty($pj_no) && !empty($pj_k_no_list))
    {
        $idx = 0;
        foreach($pj_k_no_list as $pj_k_no)
        {
            $k_priority = isset($k_priority_list[$idx]) ?  $k_priority_list[$idx] : 0;
            $k_name     = isset($k_name_list[$idx]) ?  $k_name_list[$idx] : "";
            $k_price    = isset($k_price_list[$idx]) ? str_replace(',', '', $k_price_list[$idx]) : 0;
            if(!empty($pj_k_no)){
                $total_sql = "UPDATE `project_expenses_kind` SET k_priority='{$k_priority}', k_name='{$k_name}', k_price='{$k_price}' WHERE pj_k_no='{$pj_k_no}' AND pj_no='{$pj_no}'";
            }else{
                $k_priority = ($k_priority == 0) ? $idx+1 : $k_priority;
                $total_sql = "INSERT INTO project_expenses_kind SET pj_no='{$pj_no}', k_priority='{$k_priority}', k_name='{$k_name}', k_price='{$k_price}'";
            }

            mysqli_query($my_db, $total_sql);

            $idx++;
        }
    }

    goto_url("{$list_url}?pj_no={$pj_no}");
}
else
{
    $project_sql         = "SELECT * FROM project WHERE pj_no='{$pj_no}'";
    $project_query       = mysqli_query($my_db, $project_sql);
    $project             = mysqli_fetch_assoc($project_query);

    if($project['team'] == $session_team || $session_s_no == '62'){
        $editable = true;
    }

    $pj_exp_kind_sql     = "SELECT *, (SELECT COUNT(pe.pj_e_no) FROM project_expenses pe WHERE pe.kind=pek.pj_k_no AND pe.pj_no='{$pj_no}' AND pe.display='1') as active_cnt FROM `project_expenses_kind` as pek WHERE pek.pj_no='{$pj_no}' ORDER BY k_priority ASC";
    $pj_exp_kind_query   = mysqli_query($my_db, $pj_exp_kind_sql);
    while($pj_exp_kind = mysqli_fetch_assoc($pj_exp_kind_query))
    {
        $k_price        = $pj_exp_kind['k_price'];
        $comp_price     = 0;
        $inner_price    = 0;
        $pj_kind_price_sql   = "SELECT pe.supply_price, pe.cal_method FROM project_expenses pe WHERE pe.kind='{$pj_exp_kind['pj_k_no']}' AND pe.pj_no='{$pj_no}' AND display='1'";
        $pj_kind_price_query = mysqli_query($my_db, $pj_kind_price_sql);
        while($pj_kind_price = mysqli_fetch_assoc($pj_kind_price_query))
        {
            if($pj_kind_price['cal_method'] == '1'){
                $inner_price += $pj_kind_price['supply_price'];
            }elseif($pj_kind_price['cal_method'] == '2'){
                $comp_price += $pj_kind_price['supply_price'];
            }
        }

        $pj_exp_kind['comp_spend']      = $comp_price;
        $pj_exp_kind['inner_spend']     = $inner_price;
        $pj_exp_kind['balance']         = $k_price-$comp_price;
        $pj_exp_kind['total_balance']   = $k_price-$comp_price-$inner_price;
        $pj_exp_kind_list[] = $pj_exp_kind;
    }

    $pj_empty_kind_sql    = "SELECT pe.supply_price, pe.cal_method FROM project_expenses pe WHERE (pe.kind='0' OR pe.kind is NULL) AND pe.pj_no='{$pj_no}' AND display='1'";
    $pj_empty_kind_query  = mysqli_query($my_db, $pj_empty_kind_sql);
    while($pj_empty_kind = mysqli_fetch_assoc($pj_empty_kind_query))
    {
        if($pj_empty_kind['cal_method'] == '1'){
            $empty_inner_price += $pj_empty_kind['supply_price'];
        }elseif($pj_empty_kind['cal_method'] == '2'){
            $empty_comp_price += $pj_empty_kind['supply_price'];
        }
    }

    $total_price_sql    = "SELECT SUM(pe.supply_price) as total_price FROM project_expenses pe WHERE pe.pj_no='{$pj_no}' AND pe.display='1'";
    $total_price_query  = mysqli_query($my_db, $total_price_sql);
    $total_price_result = mysqli_fetch_assoc($total_price_query);
    $total_price        = isset($total_price_result['total_price']) ? $total_price_result['total_price'] : 0;
}

$smarty->assign("pj_no", $pj_no);
$smarty->assign("editable", $editable);
$smarty->assign("project", $project);
$smarty->assign("pj_exp_kind_list", $pj_exp_kind_list);
$smarty->assign("empty_inner_price", $empty_inner_price);
$smarty->assign("empty_comp_price", $empty_comp_price);
$smarty->assign("total_price", $total_price);

$smarty->display('project_expenses_input_iframe.html');
?>
