<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/wise_bm.php');
require('inc/helper/commerce_sales.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Company.php');

# Navigation & My Quick
$nav_prd_no  = "235";
$nav_title   = "커머스 매출 및 비용관리(비교)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수
$company_model              = Company::Factory();
$kind_model                 = Kind::Factory();
$dp_company_list            = $company_model->getDpList();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$sch_brand_total_list       = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$dp_except_list             = getNotApplyDpList();
$dp_except_text             = implode(",", $dp_except_list);
$dp_self_imweb_list         = getSelfDpImwebCompanyList();
$global_dp_all_list         = getGlobalDpCompanyList();
$global_dp_total_list       = $global_dp_all_list['total'];
$global_dp_ilenol_list      = $global_dp_all_list['ilenol'];
$global_brand_list          = getGlobalBrandList();
$global_brand_option        = array_keys($global_brand_list);
$doc_brand_list             = getTotalDocBrandList();
$doc_brand_text             = implode(",", $doc_brand_list);
$ilenol_brand_list          = getIlenolBrandList();
$ilenol_brand_text          = implode(",", $ilenol_brand_list);
$ilenol_global_dp_text      = implode(",", $global_dp_ilenol_list);
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$base_type                  = "base";
$comp_type                  = "comp";

# 검색조건
$add_where                  = "display='1'";
$add_cms_where              = "w.delivery_state='4' AND dp_c_no NOT IN({$dp_except_text})";
$add_quick_where            = "w.prd_type='1' AND w.unit_price > 0 AND w.quick_state='4'";
$add_kind_where             = "";
$add_result_where           = "`am`.state IN(3,5) AND `am`.product IN(1,2) AND `am`.media='265'";

$sch_brand_g1               = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2               = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand                  = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_dp_company             = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";
$kind_column_list           = [];
$brand_column_list          = [];
$brand_company_list         = [];
$company_chk_list           = [];
$search_report_url          = "";

if(!empty($sch_brand)) {
    $sch_brand_g1       = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2       = $brand_parent_list[$sch_brand]["brand_g2"];
    $search_report_url  = "sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}&sch_brand={$sch_brand}";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1       = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
    $search_report_url  = "sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}";
}
elseif(!empty($sch_brand_g1)){
    $search_report_url  = "sch_brand_g1={$sch_brand_g1}";
}

# 브랜드 옵션
if(!empty($sch_brand))
{
    $sch_brand_g2_list              = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list                 = $brand_list[$sch_brand_g2];
    $brand_column_list[$sch_brand]  = $brand_list[$sch_brand_g2][$sch_brand];
    $brand_company_list[$sch_brand] = $sch_brand;
    $sch_brand_name                 = $brand_list[$sch_brand_g2][$sch_brand];

    if(in_array($sch_brand, $global_brand_option)) {
        if($sch_brand == '5979' || $sch_brand == '6044'){
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','09056'))";
            $add_kind_where .= " AND k_parent IN('08027','09056')";
        }elseif($sch_brand == '5514'){
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08033','08034','08035','08036','08042','08046','09060','09061','09062','09063','09081','09092'))";
            $add_kind_where .= " AND k_parent IN('08033','08034','08035','08036','08042','08046','09060','09061','09062','09063','09081','09092')";
        }else{
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092'))";
            $add_kind_where .= " AND k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092')";
        }
    } else {
        $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code NOT IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092'))";
        $add_kind_where .= " AND k_parent NOT IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092')";
    }

    if(in_array($sch_brand, $doc_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5956' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif(in_array($sch_brand, $ilenol_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5659' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "5513"){
        $add_cms_where      .= " AND `w`.c_no IN({$doc_brand_text}) AND (`w`.log_c_no = '5956' OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$doc_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5514"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND ((`w`.log_c_no = '5659' AND `w`.dp_c_no NOT IN({$ilenol_global_dp_text})) OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5979"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.c_no != '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "6044"){
        $add_cms_where      .= " AND `w`.c_no = '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    else{
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}'";
    }
    $add_result_where   .= " AND `ar`.brand = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $brand_column_list  = $sch_brand_list;
    $sch_brand_name     = $brand_company_g2_list[$sch_brand_g1][$sch_brand_g2];

    foreach($sch_brand_list as $c_no => $c_name) {
        $brand_company_list[$c_no] = $c_no;
    }

    $add_where          .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_cms_where      .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_quick_where    .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_result_where   .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $brand_column_list  = $sch_brand_g2_list;
    $sch_brand_name     = $brand_company_g1_list[$sch_brand_g1];

    foreach($sch_brand_g2_list as $brand_g2 => $brand_g2_name)
    {
        $init_company_list = $brand_list[$brand_g2];
        if(!empty($init_company_list))
        {
            foreach($init_company_list as $c_no => $c_name) {
                $brand_company_list[$c_no] = $c_no;
            }
        }
    }

    $add_where          .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_cms_where      .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_quick_where    .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_result_where   .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
else
{
    $brand_column_list  = $brand_company_g1_list;

    foreach($brand_company_g1_list as $brand_g1 => $brand_g1_name)
    {
        $init_g2_list = $brand_company_g2_list[$brand_g1];
        if(!empty($init_g2_list))
        {
            foreach($init_g2_list as $brand_g2 => $brand_g2_name)
            {
                $init_company_list = $brand_list[$brand_g2];
                if(!empty($init_company_list))
                {
                    foreach($init_company_list as $c_no => $c_name) {
                        $brand_company_list[$c_no] = $c_no;
                    }
                }
            }
        }
    }
}

foreach($brand_column_list as $kind_key => $kind_title)
{
    foreach($brand_total_list[$kind_key] as $brand){
        $company_chk_list[$brand] = $kind_key;
    }
}

if(!empty($sch_dp_company)) {
    $add_cms_where      .= " AND `w`.dp_c_no='{$sch_dp_company}'";
    $add_quick_where    .= " AND `w`.run_c_no='{$sch_dp_company}'";
    $smarty->assign("sch_dp_company", $sch_dp_company);
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 날짜 처리
$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$sch_base_s_date    = isset($_GET['sch_base_s_date']) ? $_GET['sch_base_s_date'] : "";
$sch_base_e_date    = isset($_GET['sch_base_e_date']) ? $_GET['sch_base_e_date'] : "";
$sch_comp_s_date    = isset($_GET['sch_comp_s_date']) ? $_GET['sch_comp_s_date'] : "";
$sch_comp_e_date    = isset($_GET['sch_comp_e_date']) ? $_GET['sch_comp_e_date'] : "";
$search_report_url .= "&sch_date_type={$sch_date_type}";

if($sch_date_type == "1"){
    $search_report_url .= "&sch_s_date={$sch_base_s_date}&sch_e_date={$sch_base_e_date}";
}elseif($sch_date_type == "2"){
    $search_report_url .= "&sch_s_week={$sch_base_s_date}&sch_e_week={$sch_base_e_date}";
}elseif($sch_date_type == "3"){
    $sch_base_s_month = date("Y-m", strtotime($sch_base_s_date));
    $sch_base_e_month = date("Y-m", strtotime($sch_base_e_date));
    $search_report_url .= "&sch_s_month={$sch_base_s_month}&sch_e_month={$sch_base_e_month}";
}else{
    $sch_base_s_year    = date("Y-m", strtotime($sch_base_s_date));
    $sch_base_e_year    = date("Y-m", strtotime($sch_base_e_date));
    $search_report_url .= "&sch_s_year={$sch_base_s_year}&sch_e_year={$sch_base_e_year}";
}
$smarty->assign("sch_date_type", $sch_date_type);
$smarty->assign("sch_base_s_date", $sch_base_s_date);
$smarty->assign("sch_base_e_date", $sch_base_e_date);
$smarty->assign("sch_comp_s_date", $sch_comp_s_date);
$smarty->assign("sch_comp_e_date", $sch_comp_e_date);
$smarty->assign("search_report_url", $search_report_url);

$total_all_data_list  = array("chk_empty" => 0,
    "auto_base_cost" => 0, "auto_comp_cost" => 0, "auto_cost_per" => 0, "self_base_cost" => 0, "self_comp_cost" => 0, "self_cost_per" => 0,
    "auto_base_sales" => 0, "auto_comp_sales" => 0, "auto_sales_per" => 0, "self_base_sales" => 0, "self_comp_sales" => 0, "self_sales_per" => 0,
    "net_base_cost" => 0, "net_comp_cost" => 0, "net_cost_per" => 0, "net_base_sales" => 0, "net_comp_sales" => 0, "net_sales_per" => 0,
    "net_base_price" => 0, "net_comp_price" => 0, "net_price_per" => 0, "net_base_profit" => 0, "net_comp_profit" => 0, "net_profit_per" => 0,
    "net_base_expected" => 0, "net_comp_expected" => 0, "net_expect_per" => 0, "net_base_roas" => 0, "net_comp_roas" => 0, "net_roas_per" => 0,
    "net_base_profit_per" => 0, "net_sum_profit_per" => 0, "net_comp_profit_per" => 0,
);

if(!empty($sch_base_s_date) && !empty($sch_base_e_date) && !empty($sch_comp_s_date) && !empty($sch_comp_e_date))
{
    $total_all_data_list['chk_empty']++;

    $sch_base_s_datetime    = $sch_base_s_date." 00:00:00";
    $sch_base_e_datetime    = $sch_base_e_date." 23:59:59";
    $sch_comp_s_datetime    = $sch_comp_s_date." 00:00:00";
    $sch_comp_e_datetime    = $sch_comp_e_date." 23:59:59";
    $net_base_date          = $sch_base_s_date;
    $net_comp_date          = $sch_comp_s_date;

    $all_base_where         = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_base_s_date}' AND '{$sch_base_e_date}'";
    $all_comp_where         = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_comp_s_date}' AND '{$sch_comp_e_date}'";
    $add_cms_date_where     = $add_cms_where;
    $add_quick_date_where   = $add_quick_where;

    if($sch_base_s_date < $sch_comp_s_date){
        $add_cms_date_where     .= " AND order_date BETWEEN '{$sch_base_s_datetime}'  AND '{$sch_comp_e_datetime}'";
        $add_quick_date_where   .= " AND run_date BETWEEN '{$sch_base_s_datetime}'  AND '{$sch_comp_e_datetime}'";
    }else{
        $add_cms_date_where     .= " AND order_date BETWEEN '{$sch_comp_s_datetime}'  AND '{$sch_base_e_datetime}'";
        $add_quick_date_where   .= " AND run_date BETWEEN '{$sch_comp_s_datetime}'  AND '{$sch_base_e_datetime}'";
    }

    $base_where         = $add_where." AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_base_s_date}' AND '{$sch_base_e_date}'";
    $base_quick_where   = $add_quick_where." AND run_date BETWEEN '{$sch_base_s_datetime}' AND '{$sch_base_e_datetime}'";
    $base_result_where  = $add_result_where." AND adv_s_date BETWEEN '{$sch_base_s_datetime}' AND '{$sch_base_e_datetime}'";
    $comp_where         = $add_where." AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_comp_s_date}' AND '{$sch_comp_e_date}'";
    $comp_quick_where   = $add_quick_where." AND run_date BETWEEN '{$sch_comp_s_datetime}' AND '{$sch_comp_e_datetime}'";
    $comp_result_where  = $add_result_where." AND adv_s_date BETWEEN '{$sch_comp_s_datetime}' AND '{$sch_comp_e_datetime}'";
    $base_date_where    = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_base_s_date}' AND '{$sch_base_e_date}' ";;
    $comp_date_where    = " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_comp_s_date}' AND '{$sch_comp_e_date}' ";;

    # 비용/매출 수기 리스트
    $kind_display_list  = [];
    $kind_column_list   = [];
    $kind_column_sql    = "
        SELECT 
            k_parent, 
            (SELECT sub_k.k_name FROM kind sub_k WHERE sub_k.k_name_code=k.k_parent) as k_parent_name, 
            (SELECT sub_k.priority FROM kind sub_k WHERE sub_k.k_name_code=k.k_parent) as k_parent_priority, 
            k_code, 
            k_name_code, 
            k_name,
            display
        FROM kind k 
        WHERE (k_code='cost' OR k_code='sales')
          AND k_parent is not null {$add_kind_where} 
        ORDER BY k_parent_priority ASC, priority ASC
    ";
    $kind_column_query = mysqli_query($my_db, $kind_column_sql);
    while($kind_column_data = mysqli_fetch_assoc($kind_column_query))
    {
        $kind_display_list[$kind_column_data['k_name_code']] = $kind_column_data['display'];
        $kind_column_list[$kind_column_data['k_parent']][$kind_column_data['k_name_code']] = array(
            'k_name'        => $kind_column_data['k_name'],
            'k_name_code'   => $kind_column_data['k_name_code'],
            'type'          => $kind_column_data['k_code'],
            'k_parent'      => $kind_column_data['k_parent'],
            'k_parent_name' => $kind_column_data['k_parent_name'],
            'price'         => 0,
        );
    }

    # 배송리스트 구매처
    $cms_title_sql              = "SELECT DISTINCT dp_c_no, dp_c_name, (SELECT c.priority FROM company c WHERE c.c_no=w.dp_c_no) as priority FROM work_cms as w WHERE {$add_cms_date_where} ORDER BY priority ASC, dp_c_name ASC";
    $cms_title_query            = mysqli_query($my_db, $cms_title_sql);
    $cms_report_title_list      = [];
    $cms_report_delivery_list   = [];
    $cms_delivery_free_chk_list = [];
    while($cms_title = mysqli_fetch_assoc($cms_title_query)) {
        $cms_report_title_list[$cms_title['dp_c_no']]               = $cms_title['dp_c_name'];
        $cms_report_delivery_list[$base_type][$cms_title['dp_c_no']]    = 0;
        $cms_report_delivery_list[$comp_type][$cms_title['dp_c_no']]    = 0;
    }

    # 입/출고요청 구매처
    $quick_title_sql            = "SELECT DISTINCT run_c_no, run_c_name FROM work_cms_quick as w WHERE {$add_quick_where} ORDER BY run_c_name ASC";
    $quick_title_query          = mysqli_query($my_db, $quick_title_sql);
    $quick_report_title_list    = [];
    while($quick_title = mysqli_fetch_assoc($quick_title_query)) {
        $quick_report_title_list[$quick_title['run_c_no']] = $quick_title['run_c_name'];
    }

    # 사용 array
    $total_all_date_list            = [];
    $total_all_base_date_list       = [];
    $total_all_comp_date_list       = [];
    $cms_report_list                = [];
    $sales_report_list              = [];
    $cost_report_list               = [];
    $coupon_imweb_report_total_list = [];
    $coupon_smart_report_total_list = [];
    $cms_delivery_free_list         = [];
    $cms_delivery_free_total_list   = [];
    $nosp_result_special_list       = [];
    $nosp_result_time_list          = [];
    $quick_report_list              = [];
    $sales_tmp_total_list           = [];
    $cost_tmp_total_list            = [];
    $net_report_date_list           = [];
    $net_parent_percent_list        = [];

    # 기본일 날짜
    $date_name_option   = getDateChartOption();
    $all_base_date_sql  = "
        SELECT
            DATE_FORMAT(`allday`.Date, '%m/%d_%w') as chart_title,
            DATE_FORMAT(`allday`.Date, '%Y%m%d') as chart_key
        FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
        as allday
        WHERE {$all_base_where}
        ORDER BY chart_key
    ";
    $all_base_date_query = mysqli_query($my_db, $all_base_date_sql);
    while($all_base_date = mysqli_fetch_assoc($all_base_date_query))
    {
        $total_all_base_date_list[$all_base_date['chart_key']] = array("s_date" => date("Y-m-d", strtotime($all_base_date['chart_key']))." 00:00:00", "e_date" => date("Y-m-d", strtotime($all_base_date['chart_key']))." 23:59:59");
        $total_all_date_list[$base_type][$all_base_date['chart_key']] = $all_base_date['chart_title'];

        foreach($kind_column_list as $comm_g1 => $kind_columns)
        {
            foreach($kind_columns as $comm_g2 => $kind_column){
                if($kind_column['type'] == 'sales'){
                    $sales_report_list[$base_type][$comm_g1][$comm_g2][$all_base_date['chart_key']] = $kind_column;
                }elseif($kind_column['type'] == 'cost'){
                    $cost_report_list[$base_type][$comm_g1][$comm_g2][$all_base_date['chart_key']]  = $kind_column;
                }
            }
        }

        foreach($cms_report_title_list as $cms_key => $cms_title)
        {
            $cms_report_list[$base_type][$cms_key][$all_base_date['chart_key']]['subtotal'] = 0;
            $cms_report_list[$base_type][$cms_key][$all_base_date['chart_key']]['delivery'] = 0;

            $cms_delivery_free_list[$base_type][$cms_key][$all_base_date['chart_key']] = 0;
        }

        foreach($quick_report_title_list as $quick_key => $quick_title)
        {
            $quick_report_list[$base_type][$quick_key][$all_base_date['chart_key']] = 0;
        }

        $coupon_imweb_report_total_list[$base_type][$all_base_date['chart_key']] = array("total" => 0, "belabef" => 0, "doc" => 0, "ilenol" => 0, "nuzam" => 0);
        $coupon_smart_report_total_list[$base_type][$all_base_date['chart_key']] = 0;
        $cms_delivery_free_total_list[$base_type][$all_base_date['chart_key']]   = 0;
        $nosp_result_special_list[$base_type][$all_base_date['chart_key']]       = 0;
        $nosp_result_time_list[$base_type][$all_base_date['chart_key']]          = 0;

        foreach($brand_company_list as $c_no)
        {
            $cost_tmp_total_list[$base_type][$c_no][$all_base_date['chart_key']]  = 0;
            $sales_tmp_total_list[$base_type][$c_no][$all_base_date['chart_key']] = 0;
            $net_report_date_list[$base_type][$c_no][$all_base_date['chart_key']] = 0;
            $net_parent_percent_list[$base_type][$c_no] = 0;
        }
    }

    # 비교일 날짜
    $all_comp_date_sql = "
        SELECT
             DATE_FORMAT(`allday`.Date, '%m/%d_%w') as chart_title,
            DATE_FORMAT(`allday`.Date, '%Y%m%d') as chart_key
        FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
        as allday
        WHERE {$all_comp_where}
        ORDER BY chart_key
    ";
    $all_comp_date_query = mysqli_query($my_db, $all_comp_date_sql);
    while($all_comp_date = mysqli_fetch_assoc($all_comp_date_query))
    {
        $total_all_comp_date_list[$all_comp_date['chart_key']] = array("s_date" => date("Y-m-d", strtotime($all_comp_date['chart_key']))." 00:00:00", "e_date" => date("Y-m-d", strtotime($all_comp_date['chart_key']))." 23:59:59");
        $total_all_date_list[$comp_type][$all_comp_date['chart_key']] = $all_comp_date['chart_title'];

        foreach($kind_column_list as $comm_g1 => $kind_columns)
        {
            foreach($kind_columns as $comm_g2 => $kind_column){
                if($kind_column['type'] == 'sales'){
                    $sales_report_list[$comp_type][$comm_g1][$comm_g2][$all_comp_date['chart_key']] = $kind_column;
                }elseif($kind_column['type'] == 'cost'){
                    $cost_report_list[$comp_type][$comm_g1][$comm_g2][$all_comp_date['chart_key']]  = $kind_column;
                }
            }
        }

        foreach($cms_report_title_list as $cms_key => $cms_title)
        {
            $cms_report_list[$comp_type][$cms_key][$all_comp_date['chart_key']]['subtotal'] = 0;
            $cms_report_list[$comp_type][$cms_key][$all_comp_date['chart_key']]['delivery'] = 0;

            $cms_delivery_free_list[$comp_type][$cms_key][$all_comp_date['chart_key']] = 0;
        }

        foreach($quick_report_title_list as $quick_key => $quick_title)
        {
            $quick_report_list[$comp_type][$quick_key][$all_comp_date['chart_key']] = 0;
        }

        $coupon_imweb_report_total_list[$comp_type][$all_comp_date['chart_key']] = array("total" => 0, "belabef" => 0, "doc" => 0, "ilenol" => 0, "nuzam" => 0);
        $coupon_smart_report_total_list[$comp_type][$all_comp_date['chart_key']] = 0;
        $cms_delivery_free_total_list[$comp_type][$all_comp_date['chart_key']]   = 0;
        $nosp_result_special_list[$comp_type][$all_comp_date['chart_key']]       = 0;
        $nosp_result_time_list[$comp_type][$all_comp_date['chart_key']]          = 0;

        foreach($brand_company_list as $c_no)
        {
            $cost_tmp_total_list[$comp_type][$c_no][$all_comp_date['chart_key']]  = 0;
            $sales_tmp_total_list[$comp_type][$c_no][$all_comp_date['chart_key']] = 0;
            $net_report_date_list[$comp_type][$c_no][$all_comp_date['chart_key']] = 0;
            $net_parent_percent_list[$comp_type][$c_no] = 0;
        }
    }

    # 수기비용,매출 처리
    $cost_report_total_list         = [];
    $cost_report_total_sum_list     = [];
    $sales_report_total_list        = [];
    $sales_report_total_sum_list    = [];

    #기준일 수기비용&매출
    $commerce_base_sql  = "
        SELECT
            cr.`type`,
            cr.price as price,
            cr.brand,
            cr.k_name_code as comm_g2,
            DATE_FORMAT(sales_date, '%Y%m%d') as key_date,
            (SELECT k.k_code FROM kind k WHERE k.k_name_code=`cr`.k_name_code) as comm_type,
            (SELECT k.k_name_code FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cr`.k_name_code)) as comm_g1,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cr`.k_name_code)) as comm_g1_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=`cr`.k_name_code) as comm_g2_name
        FROM commerce_report `cr`
        WHERE {$base_where}
        ORDER BY sales_date ASC
    ";
    $commerce_base_query = mysqli_query($my_db, $commerce_base_sql);
    while($commerce_base = mysqli_fetch_assoc($commerce_base_query))
    {
        $comm_g1 = str_pad($commerce_base['comm_g1'], 5, "0", STR_PAD_LEFT);
        if($commerce_base['type'] == 'sales'){
            $sales_report_list[$base_type][$comm_g1][$commerce_base['comm_g2']][$commerce_base['key_date']]['price'] += $commerce_base['price'];
            $sales_tmp_total_list[$base_type][$commerce_base['brand']][$commerce_base['key_date']] += $commerce_base['price'];
        }elseif($commerce_base['type'] == 'cost'){
            $cost_report_list[$base_type][$comm_g1][$commerce_base['comm_g2']][$commerce_base['key_date']]['price'] += $commerce_base['price'];
            $cost_tmp_total_list[$base_type][$commerce_base['brand']][$commerce_base['key_date']]  += $commerce_base['price'];
        }
    }

    #비교일 수기비용&매출
    $commerce_comp_sql  = "
        SELECT
            cr.`type`,
            cr.price as price,
            cr.brand,
            cr.k_name_code as comm_g2,
            DATE_FORMAT(sales_date, '%Y%m%d') as key_date,
            (SELECT k.k_code FROM kind k WHERE k.k_name_code=`cr`.k_name_code) as comm_type,
            (SELECT k.k_name_code FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cr`.k_name_code)) as comm_g1,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cr`.k_name_code)) as comm_g1_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=`cr`.k_name_code) as comm_g2_name
        FROM commerce_report `cr`
        WHERE {$comp_where}
        ORDER BY sales_date ASC
    ";
    $commerce_comp_query = mysqli_query($my_db, $commerce_comp_sql);
    while($commerce_comp = mysqli_fetch_assoc($commerce_comp_query))
    {
        $comm_g1 = str_pad($commerce_comp['comm_g1'], 5, "0", STR_PAD_LEFT);
        if($commerce_comp['type'] == 'sales'){
            $sales_report_list[$comp_type][$comm_g1][$commerce_comp['comm_g2']][$commerce_comp['key_date']]['price'] += $commerce_comp['price'];
            $sales_tmp_total_list[$comp_type][$commerce_comp['brand']][$commerce_comp['key_date']] += $commerce_comp['price'];
        }elseif($commerce_comp['type'] == 'cost'){
            $cost_report_list[$comp_type][$comm_g1][$commerce_comp['comm_g2']][$commerce_comp['key_date']]['price'] += $commerce_comp['price'];
            $cost_tmp_total_list[$comp_type][$commerce_comp['brand']][$commerce_comp['key_date']]  += $commerce_comp['price'];
        }
    }

    # 지출 Total 값 구하기
    if(!empty($cost_report_list))
    {
        foreach($cost_report_list as $type => $cost_type_report)
        {
            $cost_type = "self_{$type}_cost";
            foreach($cost_type_report as $comm_g1 => $cost_parent_report)
            {
                foreach($cost_parent_report as $comm_g2 => $cost_report)
                {
                    foreach($cost_report as $key_date => $cost_data)
                    {
                        if(!isset($cost_report_total_list[$type][$comm_g1])){
                            $cost_report_total_list[$type][$comm_g1][$key_date] = 0;
                        }
                        $cost_report_total_list[$type][$comm_g1][$key_date] += $cost_data['price'];

                        if(!isset($cost_report_total_sum_list[$type][$key_date])){
                            $cost_report_total_sum_list[$type][$key_date] = 0;
                        }
                        $cost_report_total_sum_list[$type][$key_date] += $cost_data['price'];

                        $total_all_data_list[$cost_type] += $cost_data['price'];
                    }
                }
            }
        }

        $total_all_data_list['self_cost_per'] = ($total_all_data_list["self_comp_cost"] == 0) ? 0 : (($total_all_data_list["self_base_cost"] == 0) ? -100 : ROUND(((($total_all_data_list["self_base_cost"]-$total_all_data_list["self_comp_cost"])/$total_all_data_list["self_comp_cost"]) *100), 1));
    }

    # 매출 Total 값 구하기
    if(!empty($sales_report_list))
    {
        foreach($sales_report_list as $type => $sales_type_report)
        {
            $sales_type = "self_{$type}_sales";
            foreach($sales_type_report as $comm_g1 => $sales_parent_report)
            {
                foreach($sales_parent_report as $comm_g2 => $sales_report)
                {
                    foreach($sales_report as $key_date => $sales_data)
                    {
                        if(!isset($sales_report_total_list[$type][$comm_g1])){
                            $sales_report_total_list[$type][$comm_g1][$key_date] = 0;
                        }
                        $sales_report_total_list[$type][$comm_g1][$key_date] += $sales_data['price'];

                        if(!isset($sales_report_total_sum_list[$type][$key_date])){
                            $sales_report_total_sum_list[$type][$key_date] = 0;
                        }
                        $sales_report_total_sum_list[$type][$key_date] += $sales_data['price'];

                        $total_all_data_list[$sales_type] += $sales_data['price'];
                    }
                }
            }
        }

        $total_all_data_list['self_sales_per'] = ($total_all_data_list["self_comp_sales"] == 0) ? 0 : (($total_all_data_list["self_base_sales"] == 0) ? -100 : ROUND(((($total_all_data_list["self_base_sales"]-$total_all_data_list["self_comp_sales"])/$total_all_data_list["self_comp_sales"]) *100), 1));
    }

    # 배송리스트 매출관련
    $commerce_fee_option            = getCommerceFeeOption();
    $cms_delivery_chk_fee_list      = [];
    $cms_delivery_chk_nuzam_list    = [];
    $cms_delivery_chk_ord_list      = [];
    $cms_delivery_chk_list          = [];
    $commerce_fee_list              = [];
    $cms_report_total_sum_list      = [];
    $cms_report_subtotal_sum_list   = [];
    $cms_report_delivery_sum_list   = [];

    foreach($total_all_base_date_list as $base_date_data)
    {
        $cms_base_report_sql      = "
            SELECT 
                c_no,
                dp_c_no,
                dp_c_name,
                order_number,
                DATE_FORMAT(order_date, '%Y%m%d') as key_date,
                unit_price,
                (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
                unit_delivery_price,
                (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
            FROM work_cms as w
            WHERE {$add_cms_where} AND (order_date BETWEEN '{$base_date_data['s_date']}' AND '{$base_date_data['e_date']}')
        ";
        $cms_base_report_query = mysqli_query($my_db, $cms_base_report_sql);
        while($cms_base_report = mysqli_fetch_assoc($cms_base_report_query))
        {
            $commerce_fee_total_list    = isset($commerce_fee_option[$cms_base_report['c_no']]) ? $commerce_fee_option[$cms_base_report['c_no']] : [];
            $commerce_fee_per_list      = !empty($commerce_fee_total_list) && isset($commerce_fee_total_list[$cms_base_report['dp_c_no']]) ? $commerce_fee_total_list[$cms_base_report['dp_c_no']] : [];
            $commerce_fee_per           = 0;

            if(!empty($commerce_fee_per_list))
            {
                foreach($commerce_fee_per_list as $fee_date => $fee_val){
                    if($cms_base_report['key_date'] >= $fee_date){
                        $commerce_fee_per = $fee_val;
                        $commerce_fee_list[$cms_base_report['dp_c_no']] = $fee_val;
                    }
                }
            }
            $cms_report_fee      = $cms_base_report['unit_price']*($commerce_fee_per/100);
            $total_price         = $cms_base_report['unit_price'] - $cms_report_fee;

            $cms_report_list[$base_type][$cms_base_report['dp_c_no']][$cms_base_report['key_date']]['subtotal'] += $total_price;
            $cms_report_list[$base_type][$cms_base_report['dp_c_no']][$cms_base_report['key_date']]['delivery'] += $cms_base_report['unit_delivery_price'];
            $cms_report_delivery_list[$base_type][$cms_base_report['dp_c_no']] += $cms_base_report['unit_delivery_price'];

            if(in_array($sch_brand, $global_brand_option)){
                $cms_base_report['c_no'] = $sch_brand;
            }

            $sales_tmp_total_list[$base_type][$cms_base_report['c_no']][$cms_base_report['key_date']] += $total_price;

            if(in_array($cms_base_report['dp_c_no'], $dp_self_imweb_list))
            {
                if($cms_base_report['dp_c_no'] == '1372'){
                    $coupon_imweb_report_total_list[$base_type][$cms_base_report['key_date']]['belabef']+= $cms_base_report['coupon_price'];
                }elseif($cms_base_report['dp_c_no'] == '5800'){
                    $coupon_imweb_report_total_list[$base_type][$cms_base_report['key_date']]['doc']    += $cms_base_report['coupon_price'];
                }elseif($cms_base_report['dp_c_no'] == '5958'){
                    $coupon_imweb_report_total_list[$base_type][$cms_base_report['key_date']]['ilenol'] += $cms_base_report['coupon_price'];
                }elseif($cms_base_report['dp_c_no'] == '6012'){
                    $coupon_imweb_report_total_list[$base_type][$cms_base_report['key_date']]['nuzam']  += $cms_base_report['coupon_price'];
                }

                $coupon_imweb_report_total_list[$base_type][$cms_base_report['key_date']]['total']       += $cms_base_report['coupon_price'];
                $cost_tmp_total_list[$base_type][$cms_base_report['c_no']][$cms_base_report['key_date']] += $cms_base_report['coupon_price'];
                $total_all_data_list['auto_base_cost'] += $cms_base_report['coupon_price'];
            }

            if($cms_base_report['dp_c_no'] == '3295' || $cms_base_report['dp_c_no'] == '4629' || $cms_base_report['dp_c_no'] == '5427' || $cms_base_report['dp_c_no'] == '5588'){
                $coupon_smart_report_total_list[$base_type][$cms_base_report['key_date']] += $cms_base_report['coupon_price'];
                $total_all_data_list['auto_base_cost'] += $cms_base_report['coupon_price'];
            }

            if($cms_base_report['key_date'] >= 20240101)
            {
                if($cms_base_report['unit_delivery_price'] > 0) {
                    $cms_delivery_chk_fee_list[$base_type][$cms_base_report['order_number']] = 1;
                }
                elseif($cms_base_report['unit_delivery_price'] == 0 && $cms_base_report['unit_price'] > 0)
                {
                    $cms_delivery_chk_list[$base_type][$cms_base_report['order_number']][$cms_base_report['dp_c_no']][$cms_base_report['key_date']] = $cms_base_report['deli_cnt'];
                    $cms_delivery_chk_ord_list[$base_type][$cms_base_report['order_number']][$cms_base_report['c_no']] = $cms_base_report['c_no'];

                    if($cms_base_report['c_no'] == '2827' || $cms_base_report['c_no'] == '4878'){
                        $cms_delivery_chk_nuzam_list[$base_type][$cms_base_report['order_number']] = 1;
                    }
                }
            }
        }
    }

    foreach($total_all_comp_date_list as $comp_date_data)
    {
        $cms_comp_report_sql      = "
            SELECT 
                c_no,
                dp_c_no,
                dp_c_name,
                order_number,
                DATE_FORMAT(order_date, '%Y%m%d') as key_date,
                unit_price,
                (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
                unit_delivery_price,
                (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
            FROM work_cms as w
            WHERE {$add_cms_where} AND (order_date BETWEEN '{$comp_date_data['s_date']}' AND '{$comp_date_data['e_date']}')
        ";
        $cms_comp_report_query = mysqli_query($my_db, $cms_comp_report_sql);
        while($cms_comp_report = mysqli_fetch_assoc($cms_comp_report_query))
        {
            $commerce_fee_total_list    = isset($commerce_fee_option[$cms_comp_report['c_no']]) ? $commerce_fee_option[$cms_comp_report['c_no']] : [];
            $commerce_fee_per_list      = !empty($commerce_fee_total_list) && isset($commerce_fee_total_list[$cms_comp_report['dp_c_no']]) ? $commerce_fee_total_list[$cms_comp_report['dp_c_no']] : [];
            $commerce_fee_per           = 0;

            if(!empty($commerce_fee_per_list))
            {
                foreach($commerce_fee_per_list as $fee_date => $fee_val){
                    if($cms_comp_report['key_date'] >= $fee_date){
                        $commerce_fee_per = $fee_val;
                        $commerce_fee_list[$cms_comp_report['dp_c_no']] = $fee_val;
                    }
                }
            }
            $cms_report_fee      = $cms_comp_report['unit_price']*($commerce_fee_per/100);
            $total_price         = $cms_comp_report['unit_price'] - $cms_report_fee;

            $cms_report_list[$comp_type][$cms_comp_report['dp_c_no']][$cms_comp_report['key_date']]['subtotal'] += $total_price;
            $cms_report_list[$comp_type][$cms_comp_report['dp_c_no']][$cms_comp_report['key_date']]['delivery'] += $cms_comp_report['unit_delivery_price'];
            $cms_report_delivery_list[$comp_type][$cms_comp_report['dp_c_no']] += $cms_comp_report['unit_delivery_price'];

            if(in_array($sch_brand, $global_brand_option)){
                $cms_comp_report['c_no'] = $sch_brand;
            }

            $sales_tmp_total_list[$comp_type][$cms_comp_report['c_no']][$cms_comp_report['key_date']] += $total_price;

            if(in_array($cms_comp_report['dp_c_no'], $dp_self_imweb_list))
            {
                if($cms_comp_report['dp_c_no'] == '1372'){
                    $coupon_imweb_report_total_list[$comp_type][$cms_comp_report['key_date']]['belabef']+= $cms_comp_report['coupon_price'];
                }elseif($cms_comp_report['dp_c_no'] == '5800'){
                    $coupon_imweb_report_total_list[$comp_type][$cms_comp_report['key_date']]['doc']    += $cms_comp_report['coupon_price'];
                }elseif($cms_comp_report['dp_c_no'] == '5958'){
                    $coupon_imweb_report_total_list[$comp_type][$cms_comp_report['key_date']]['ilenol'] += $cms_comp_report['coupon_price'];
                }elseif($cms_comp_report['dp_c_no'] == '6012'){
                    $coupon_imweb_report_total_list[$comp_type][$cms_comp_report['key_date']]['nuzam']  += $cms_comp_report['coupon_price'];
                }

                $coupon_imweb_report_total_list[$comp_type][$cms_comp_report['key_date']]['total']       += $cms_comp_report['coupon_price'];
                $cost_tmp_total_list[$comp_type][$cms_comp_report['c_no']][$cms_comp_report['key_date']] += $cms_comp_report['coupon_price'];
                $total_all_data_list['auto_comp_cost'] += $cms_comp_report['coupon_price'];
            }

            if($cms_comp_report['dp_c_no'] == '3295' || $cms_comp_report['dp_c_no'] == '4629' || $cms_comp_report['dp_c_no'] == '5427' || $cms_comp_report['dp_c_no'] == '5588'){
                $coupon_smart_report_total_list[$comp_type][$cms_comp_report['key_date']] += $cms_comp_report['coupon_price'];
                $total_all_data_list['auto_comp_cost'] += $cms_comp_report['coupon_price'];
            }

            if($cms_comp_report['key_date'] >= 20240101)
            {
                if($cms_comp_report['unit_delivery_price'] > 0) {
                    $cms_delivery_chk_fee_list[$comp_type][$cms_comp_report['order_number']] = 1;
                }
                elseif($cms_comp_report['unit_delivery_price'] == 0 && $cms_comp_report['unit_price'] > 0)
                {
                    $cms_delivery_chk_list[$comp_type][$cms_comp_report['order_number']][$cms_comp_report['dp_c_no']][$cms_comp_report['key_date']] = $cms_comp_report['deli_cnt'];
                    $cms_delivery_chk_ord_list[$comp_type][$cms_comp_report['order_number']][$cms_comp_report['c_no']] = $cms_comp_report['c_no'];

                    if($cms_comp_report['c_no'] == '2827' || $cms_comp_report['c_no'] == '4878'){
                        $cms_delivery_chk_nuzam_list[$comp_type][$cms_comp_report['order_number']] = 1;
                    }
                }
            }
        }
    }

    if(!empty($cms_report_list))
    {
        foreach($cms_report_list as $cms_type => $cms_type_report)
        {
            $data_type = "auto_{$cms_type}_sales";
            foreach($cms_type_report as $dp_c_no => $cms_parent_report)
            {
                foreach($cms_parent_report as $key_date => $cms_report)
                {
                    if(!isset($cms_report_total_sum_list[$cms_type][$key_date])){
                        $cms_report_total_sum_list[$cms_type][$key_date] = 0;
                    }

                    if(!isset($cms_report_subtotal_sum_list[$cms_type][$key_date])){
                        $cms_report_subtotal_sum_list[$cms_type][$key_date] = 0;
                    }

                    if(!isset($cms_report_delivery_sum_list[$cms_type][$key_date])){
                        $cms_report_delivery_sum_list[$cms_type][$key_date] = 0;
                    }

                    $cms_total_price = $cms_report['subtotal']+$cms_report['delivery'];
                    $cms_report_total_sum_list[$cms_type][$key_date]     += $cms_total_price;
                    $cms_report_subtotal_sum_list[$cms_type][$key_date]  += $cms_report['subtotal'];
                    $cms_report_delivery_sum_list[$cms_type][$key_date]  += $cms_report['delivery'];

                    $total_all_data_list[$data_type] += $cms_total_price;
                }
            }
        }
    }

    # 무료 배송비 계산
    foreach($cms_delivery_chk_list as $chk_type => $chk_type_data)
    {
        $data_type = "auto_{$chk_type}_cost";
        foreach($chk_type_data as $chk_ord => $chk_ord_data)
        {
            if(isset($cms_delivery_chk_fee_list[$chk_type][$chk_ord])){
                continue;
            }

            $chk_delivery_price = isset($cms_delivery_chk_nuzam_list[$chk_type][$chk_ord]) ? 5000 : 3000;

            foreach ($chk_ord_data as $chk_dp_c_no => $chk_dp_data)
            {
                if(in_array($chk_dp_c_no, $global_dp_total_list)){
                    $chk_delivery_price = 6000;
                }

                foreach ($chk_dp_data as $chk_key_date => $deli_cnt)
                {
                    $cal_delivery_price = $chk_delivery_price * $deli_cnt;

                    $cms_delivery_free_list[$chk_type][$chk_dp_c_no][$chk_key_date] += $cal_delivery_price;
                    $cms_delivery_free_total_list[$chk_type][$chk_key_date]         += $cal_delivery_price;
                    $cms_delivery_free_chk_list[$chk_type][$chk_dp_c_no]            += $deli_cnt;
                    $total_all_data_list[$data_type] += $cal_delivery_price;

                    $chk_brand_deli_price   = $cal_delivery_price;
                    $chk_brand_cnt          = count($cms_delivery_chk_ord_list[$chk_type][$chk_ord]);
                    $cal_brand_deli_price   = round($cal_delivery_price/$chk_brand_cnt);
                    $cal_brand_idx          = 1;
                    foreach($cms_delivery_chk_ord_list[$chk_type][$chk_ord] as $chk_c_no)
                    {
                        $cal_one_delivery_price  = $cal_brand_deli_price;
                        $chk_brand_deli_price   -= $cal_one_delivery_price;

                        if($cal_brand_idx == $chk_brand_cnt){
                            $cal_one_delivery_price += $chk_brand_deli_price;
                        }

                        $cost_tmp_total_list[$chk_type][$chk_c_no][$chk_key_date] += $cal_one_delivery_price;

                        $cal_brand_idx++;
                    }
                }
            }
        }
    }

    # 입/출고요청 리스트
    $quick_base_report_sql      = "
        SELECT 
            c_no,
            run_c_no,
            run_c_name,
            order_number,
            DATE_FORMAT(run_date, '%Y%m%d') as key_date, 
            unit_price
        FROM work_cms_quick as w
        WHERE {$base_quick_where}
    ";
    $quick_base_report_query = mysqli_query($my_db, $quick_base_report_sql);
    while($quick_base_report = mysqli_fetch_assoc($quick_base_report_query))
    {
        if(in_array($sch_brand, $global_brand_option)){
            $quick_base_report['c_no'] = $sch_brand;
        }

        $quick_report_list[$base_type][$quick_base_report['run_c_no']][$quick_base_report['key_date']] += $quick_base_report['unit_price'];
        $sales_tmp_total_list[$base_type][$quick_base_report['c_no']][$quick_base_report['key_date']]  += $quick_base_report['unit_price'];
        $cms_report_subtotal_sum_list[$base_type][$quick_base_report['key_date']] += $quick_base_report['unit_price'];
        $cms_report_total_sum_list[$base_type][$quick_base_report['key_date']]    += $quick_base_report['unit_price'];

        $total_all_data_list["auto_base_sales"] += $quick_base_report['unit_price'];
    }

    $quick_comp_report_sql      = "
        SELECT 
            c_no,
            run_c_no,
            run_c_name,
            order_number,
            DATE_FORMAT(run_date, '%Y%m%d') as key_date, 
            unit_price
        FROM work_cms_quick as w
        WHERE {$comp_quick_where}
    ";
    $quick_comp_report_query = mysqli_query($my_db, $quick_comp_report_sql);
    while($quick_comp_report = mysqli_fetch_assoc($quick_comp_report_query))
    {
        if(in_array($sch_brand, $global_brand_option)){
            $quick_comp_report['c_no'] = $sch_brand;
        }

        $quick_report_list[$comp_type][$quick_comp_report['run_c_no']][$quick_comp_report['key_date']] += $quick_comp_report['unit_price'];
        $sales_tmp_total_list[$comp_type][$quick_comp_report['c_no']][$quick_comp_report['key_date']]  += $quick_comp_report['unit_price'];
        $cms_report_subtotal_sum_list[$comp_type][$quick_comp_report['key_date']] += $quick_comp_report['unit_price'];
        $cms_report_total_sum_list[$comp_type][$quick_comp_report['key_date']]    += $quick_comp_report['unit_price'];

        $total_all_data_list["auto_comp_sales"] += $quick_comp_report['unit_price'];
    }

    # NOSP 계산
    $nosp_result_tmp_list   = [];
    $nosp_base_result_sql   = "
            SELECT
            `ar`.am_no,
            `ar`.brand,
            `am`.product,
            `am`.fee_per,
            `am`.price,
            `ar`.impressions,
            `ar`.click_cnt,
            `ar`.adv_price,
            DATE_FORMAT(am.adv_s_date, '%Y%m%d') as key_date
        FROM advertising_result `ar`
        LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
        WHERE {$base_result_where}
        ORDER BY `am`.adv_s_date ASC
    ";
    $nosp_base_result_query = mysqli_query($my_db, $nosp_base_result_sql);
    while($nosp_base_result = mysqli_fetch_assoc($nosp_base_result_query))
    {
        $nosp_result_tmp_list[$base_type][$nosp_base_result['am_no']][$nosp_base_result['brand']]["product"]          = $nosp_base_result['product'];
        $nosp_result_tmp_list[$base_type][$nosp_base_result['am_no']][$nosp_base_result['brand']]["key_date"]         = $nosp_base_result['key_date'];
        $nosp_result_tmp_list[$base_type][$nosp_base_result['am_no']][$nosp_base_result['brand']]["fee_per"]          = $nosp_base_result['fee_per'];
        $nosp_result_tmp_list[$base_type][$nosp_base_result['am_no']][$nosp_base_result['brand']]["total_adv_price"] += $nosp_base_result['adv_price'];
    }

    $nosp_comp_result_sql   = "
            SELECT
            `ar`.am_no,
            `ar`.brand,
            `am`.product,
            `am`.fee_per,
            `am`.price,
            `ar`.impressions,
            `ar`.click_cnt,
            `ar`.adv_price,
            DATE_FORMAT(am.adv_s_date, '%Y%m%d') as key_date
        FROM advertising_result `ar`
        LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
        WHERE {$comp_result_where}
        ORDER BY `am`.adv_s_date ASC
    ";
    $nosp_comp_result_query = mysqli_query($my_db, $nosp_comp_result_sql);
    while($nosp_comp_result = mysqli_fetch_assoc($nosp_comp_result_query))
    {
        $nosp_result_tmp_list[$comp_type][$nosp_comp_result['am_no']][$nosp_comp_result['brand']]["product"]          = $nosp_comp_result['product'];
        $nosp_result_tmp_list[$comp_type][$nosp_comp_result['am_no']][$nosp_comp_result['brand']]["key_date"]         = $nosp_comp_result['key_date'];
        $nosp_result_tmp_list[$comp_type][$nosp_comp_result['am_no']][$nosp_comp_result['brand']]["fee_per"]          = $nosp_comp_result['fee_per'];
        $nosp_result_tmp_list[$comp_type][$nosp_comp_result['am_no']][$nosp_comp_result['brand']]["total_adv_price"] += $nosp_comp_result['adv_price'];
    }

    foreach($nosp_result_tmp_list as $nosp_type => $nosp_type_result_tmp)
    {
        $nosp_data_type = "auto_{$nosp_type}_cost";
        foreach($nosp_type_result_tmp as $am_no => $nosp_result_tmp)
        {
            foreach($nosp_result_tmp as $brand => $nosp_brand_data)
            {
                $fee_price      = $nosp_brand_data['total_adv_price']*($nosp_brand_data['fee_per']/100);
                $cal_price      = $nosp_brand_data["total_adv_price"]-$fee_price;
                $cal_price_vat  = ROUND($cal_price*1.1);

                if($nosp_brand_data['product'] == "1"){
                    $nosp_result_time_list[$nosp_type][$nosp_brand_data['key_date']]    += $cal_price_vat;
                }elseif($nosp_brand_data['product'] == "2"){
                    $nosp_result_special_list[$nosp_type][$nosp_brand_data['key_date']] += $cal_price_vat;
                }
                $cost_tmp_total_list[$nosp_type][$brand][$nosp_brand_data['key_date']]  += $cal_price_vat;

                $total_all_data_list[$nosp_data_type] += $cal_price_vat;
            }
        }
    }

    $total_all_data_list['auto_cost_per']  = ($total_all_data_list["auto_comp_cost"] == 0) ? 0 : (($total_all_data_list["auto_base_cost"] == 0) ? -100 : ROUND(((($total_all_data_list["auto_base_cost"]-$total_all_data_list["auto_comp_cost"])/$total_all_data_list["auto_comp_cost"]) *100), 1));
    $total_all_data_list['auto_sales_per'] = ($total_all_data_list["auto_comp_sales"] == 0) ? 0 : (($total_all_data_list["auto_base_sales"] == 0) ? -100 : ROUND(((($total_all_data_list["auto_base_sales"]-$total_all_data_list["auto_comp_sales"])/$total_all_data_list["auto_comp_sales"]) *100), 1));

    # 예산지출비율 및 ROAS 계산
    foreach($total_all_date_list as $key_type => $key_type_data)
    {
        $net_cost_type  = "net_{$key_type}_cost";
        $net_sales_type = "net_{$key_type}_sales";

        foreach($key_type_data as $key_date => $key_title)
        {
            $sales_total        = $sales_report_total_sum_list[$key_type][$key_date]+$cms_report_subtotal_sum_list[$key_type][$key_date];
            $cost_total         = $cost_report_total_sum_list[$key_type][$key_date]+$coupon_imweb_report_total_list[$key_type][$key_date]["total"]+$cms_delivery_free_total_list[$key_type][$key_date]+$nosp_result_special_list[$key_type][$key_date]+$nosp_result_time_list[$key_type][$key_date];

            $total_all_data_list[$net_cost_type]    += $cost_total;
            $total_all_data_list[$net_sales_type]   += $sales_total;
        }
    }
    $total_all_data_list['net_base_expected']   = ($total_all_data_list['net_base_sales'] > 0) ? ROUND(($total_all_data_list['net_base_cost']/$total_all_data_list['net_base_sales']) * 100, 1) : 0;
    $total_all_data_list['net_comp_expected']   = ($total_all_data_list['net_comp_sales'] > 0) ? ROUND(($total_all_data_list['net_comp_cost']/$total_all_data_list['net_comp_sales']) * 100, 1) : 0;
    $total_all_data_list['net_base_roas']       = ($total_all_data_list['net_base_cost'] > 0) ? ROUND(($total_all_data_list['net_base_sales']/$total_all_data_list['net_base_cost']) * 100, 1) : 0;
    $total_all_data_list['net_comp_roas']       = ($total_all_data_list['net_comp_cost'] > 0) ? ROUND(($total_all_data_list['net_comp_sales']/$total_all_data_list['net_comp_cost']) * 100, 1) : 0;
    $total_all_data_list['net_cost_per']        = ($total_all_data_list["net_comp_cost"] == 0) ? 0 : (($total_all_data_list["net_base_cost"] == 0) ? -100 : ROUND(((($total_all_data_list["net_base_cost"]-$total_all_data_list["net_comp_cost"])/$total_all_data_list["net_comp_cost"]) *100), 1));
    $total_all_data_list['net_sales_per']       = ($total_all_data_list["net_comp_sales"] == 0) ? 0 : (($total_all_data_list["net_base_sales"] == 0) ? -100 : ROUND(((($total_all_data_list["net_base_sales"]-$total_all_data_list["net_comp_sales"])/$total_all_data_list["net_comp_sales"]) *100), 1));
    $total_all_data_list['net_expect_per']      = $total_all_data_list["net_base_expected"]-$total_all_data_list["net_comp_expected"];
    $total_all_data_list['net_roas_per']        = $total_all_data_list["net_base_roas"]-$total_all_data_list["net_comp_roas"];

    # NET 매출 관련 작업
    $net_report_list            = [];
    $net_report_cal_list        = [];
    $net_report_profit_list     = [];
    $net_report_profit_per_list = [];
    foreach($brand_company_list as $c_no)
    {
        $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as key_date FROM commerce_report_net WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$net_base_date}' ORDER BY key_date DESC LIMIT 1;";
        $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
        $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
        $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
        $net_parent             = isset($net_pre_percent_result['key_date']) ? $net_pre_percent_result['key_date'] : 0;

        $net_parent_percent_list[$base_type][$c_no] = $net_pre_percent;

        $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as key_date FROM commerce_report_net WHERE brand='{$c_no}' {$base_date_where} ORDER BY key_date ASC";
        $net_report_query       = mysqli_query($my_db, $net_report_sql);
        while($net_report = mysqli_fetch_assoc($net_report_query))
        {
            $net_report_date_list[$base_type][$c_no][$net_report['key_date']] = $net_report['percent'];
        }
    }

    foreach($brand_company_list as $c_no)
    {
        $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as key_date FROM commerce_report_net WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$net_comp_date}' ORDER BY key_date DESC LIMIT 1;";
        $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
        $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
        $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
        $net_parent             = isset($net_pre_percent_result['key_date']) ? $net_pre_percent_result['key_date'] : 0;

        $net_parent_percent_list[$comp_type][$c_no] = $net_pre_percent;

        $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as key_date FROM commerce_report_net WHERE brand='{$c_no}' {$comp_date_where} ORDER BY key_date ASC";
        $net_report_query       = mysqli_query($my_db, $net_report_sql);
        while($net_report = mysqli_fetch_assoc($net_report_query))
        {
            $net_report_date_list[$comp_type][$c_no][$net_report['key_date']] = $net_report['percent'];
        }
    }

    foreach($net_report_date_list as $net_type => $net_type_data)
    {
        $net_price_type     = "net_{$net_type}_price";
        $net_profit_type    = "net_{$net_type}_profit";

        foreach($net_type_data as $c_no => $net_company_data)
        {
            $net_pre_percent = $net_parent_percent_list[$net_type][$c_no];
            $parent_key      = $company_chk_list[$c_no];

            foreach($net_company_data as $key_date => $net_percent_val)
            {
                if($net_percent_val > 0 ){
                    $net_pre_percent = $net_percent_val;
                }

                $net_percent = $net_pre_percent/100;
                $sales_total = round($sales_tmp_total_list[$net_type][$c_no][$key_date],0);
                $cost_total  = $cost_tmp_total_list[$net_type][$c_no][$key_date];
                $net_price   = $sales_total > 0 ? (int)($sales_total*$net_percent) : 0;
                $profit      = $cost_total > 0 ? ($net_price-$cost_total) : $net_price;

                $total_all_data_list[$net_price_type]   += $net_price;
                $total_all_data_list[$net_profit_type]  += $profit;
            }
        }
    }

    $total_all_data_list['net_price_per']       = ($total_all_data_list["net_comp_price"] == 0) ? 0 : (($total_all_data_list["net_base_price"] == 0) ? -100 : ROUND(((($total_all_data_list["net_base_price"]-$total_all_data_list["net_comp_price"])/$total_all_data_list["net_comp_price"]) *100), 1));
    $total_all_data_list['net_profit_per']      = ($total_all_data_list["net_comp_profit"] == 0) ? 0 : (($total_all_data_list["net_base_profit"] == 0) ? -100 : ROUND(((($total_all_data_list["net_base_profit"]-$total_all_data_list["net_comp_profit"])/$total_all_data_list["net_comp_profit"]) *100), 1));
    $total_all_data_list["net_base_profit_per"] = round($total_all_data_list['net_base_profit']/$total_all_data_list['net_base_sales'] * 100, 1);
    $total_all_data_list["net_comp_profit_per"] = round($total_all_data_list['net_comp_profit']/$total_all_data_list['net_comp_sales'] * 100, 1);
    $total_all_data_list["net_sum_profit_per"]  = $total_all_data_list["net_base_profit_per"]-$total_all_data_list["net_comp_profit_per"];
}

$smarty->assign("dp_company_list", $dp_company_list);
$smarty->assign("total_all_data_list", $total_all_data_list);

$smarty->display('media_commerce_stats_report_term.html');
?>
