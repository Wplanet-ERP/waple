<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_upload.php');
require('inc/helper/project.php');
require('inc/model/MyCompany.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

# 변수 설정
$pj_s_no        = isset($_GET['pj_s_no']) ? $_GET['pj_s_no'] : "";
$project_staff  = [];
$title		    = "등록하기";
$regist_url     = "project_staff_regist.php";
$list_url       = "project_staff_list.php";

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
if($process == 'new_project_staff') # 생성
{
    // 필수정보
    $active       = isset($_POST['active']) ? $_POST['active'] : "";
    $my_c_no      = isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "";
    $department   = isset($_POST['department']) ? sprintf('%05d', $_POST['department']) : "";
    $s_no         = isset($_POST['s_no']) ? $_POST['s_no'] : "";
    $s_name       = isset($_POST['s_name']) ? addslashes($_POST['s_name']) : "";
    $info         = isset($_POST['info']) ? addslashes($_POST['info']) : "";
    $reg_date     = date("Y-m-d H:i:s");

    # 필수정보 체크 및 저장
    if(empty($my_c_no) || empty($s_no) || empty($s_name) || empty($active))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');location.href='{$regist_url}';</script>");
    }

    $ins_sql = "
        INSERT INTO `project_staff` SET
            `active`  = '{$active}',
            `my_c_no` = '{$my_c_no}',
            `s_no`    = '{$s_no}',
            `s_name`  = '{$s_name}',
            `info`    = '{$info}',
            `regdate` = '{$reg_date}'
    ";

    //파일 업로드
    $file1 = $_FILES['file1'];
    if(isset($file1['name']) && !empty($file1['name'])){
        $file_path = add_store_file($file1, "project_staff");
        $file_name = $file1['name'];
        $ins_sql  .= ", `file1_path`='{$file_path}', `file1_name`='{$file_name}'";
    }

    $file2 = $_FILES['file2'];
    if(isset($file2['name']) && !empty($file2['name'])){
        $file_path = add_store_file($file2, "project_staff");
        $file_name = $file2['name'];
        $ins_sql  .= ", `file2_path`='{$file_path}', `file2_name`='{$file_name}'";
    }

    $file3 = $_FILES['file3'];
    if(isset($file3['name']) && !empty($file3['name'])){
        $file_path = add_store_file($file3, "project_staff");
        $file_name = $file3['name'];
        $ins_sql  .= ", `file3_path`='{$file_path}', `file3_name`='{$file_name}'";
    }

    $file4 = $_FILES['file4'];
    if(isset($file4['name']) && !empty($file4['name'])){
        $file_path = add_store_file($file4, "project_staff");
        $file_name = $file4['name'];
        $ins_sql  .= ", `file4_path`='{$file_path}', `file4_name`='{$file_name}'";
    }

    $file5 = $_FILES['file5'];
    if(isset($file5['name']) && !empty($file5['name'])){
        $file_path = add_store_file($file5, "project_staff");
        $file_name = $file5['name'];
        $ins_sql  .= ", `file5_path`='{$file_path}', `file5_name`='{$file_name}'";
    }

    if(mysqli_query($my_db, $ins_sql)){
        exit("<script>alert('인력 등록에 성공했습니다');location.href='{$list_url}';</script>");
    }else{
        exit("<script>alert('인력 등록에 실패했습니다');location.href='{$regist_url}';</script>");
    }
}
elseif($process == 'modify_project_staff') # 수정
{
    // 필수정보
    $pj_s_no      = isset($_POST['pj_s_no']) ? $_POST['pj_s_no'] : "";
    $active       = isset($_POST['active']) ? $_POST['active'] : "";
    $my_c_no      = isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "";
    $s_no         = isset($_POST['s_no']) ? $_POST['s_no'] : "";
    $s_name       = isset($_POST['s_name']) ? addslashes($_POST['s_name']) : "";
    $info         = isset($_POST['info']) ? addslashes($_POST['info']) : "";

    # 필수정보 체크 및 저장
    if(empty($my_c_no) || empty($s_no) || empty($s_name) || empty($active))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');location.href='{$regist_url}';</script>");
    }

    $upd_sql = "
        UPDATE `project_staff` SET
            `active`  = '{$active}',
            `my_c_no` = '{$my_c_no}',
            `s_no`    = '{$s_no}',
            `s_name`  = '{$s_name}',
            `info`    = '{$info}'
    ";

    //파일 업로드
    $file1 = $_FILES['file1'];
    if(isset($file1['name']) && !empty($file1['name'])){
        $file_path = add_store_file($file1, "project_staff");
        $file_name = $file1['name'];
        $upd_sql .= ", `file1_path`='{$file_path}', `file1_name`='{$file_name}'";
    }

    $file2 = $_FILES['file2'];
    if(isset($file2['name']) && !empty($file2['name'])){
        $file_path = add_store_file($file2, "project_staff");
        $file_name = $file2['name'];
        $upd_sql .= ", `file2_path`='{$file_path}', `file2_name`='{$file_name}'";
    }

    $file3 = $_FILES['file3'];
    if(isset($file3['name']) && !empty($file3['name'])){
        $file_path = add_store_file($file3, "project_staff");
        $file_name = $file3['name'];
        $upd_sql .= ", `file3_path`='{$file_path}', `file3_name`='{$file_name}'";
    }

    $file4 = $_FILES['file4'];
    if(isset($file4['name']) && !empty($file4['name'])){
        $file_path = add_store_file($file4, "project_staff");
        $file_name = $file4['name'];
        $upd_sql .= ", `file4_path`='{$file_path}', `file4_name`='{$file_name}'";
    }

    $file5 = $_FILES['file5'];
    if(isset($file5['name']) && !empty($file5['name'])){
        $file_path = add_store_file($file5, "project_staff");
        $file_name = $file5['name'];
        $upd_sql .= ", `file5_path`='{$file_path}', `file5_name`='{$file_name}'";
    }
    $upd_sql .= " WHERE pj_s_no='{$pj_s_no}'";

    if(mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('인력 수정에 성공했습니다');location.href='{$list_url}';</script>");
    }else{
        exit("<script>alert('인력 수정에 실패했습니다');location.href='{$regist_url}?pj_s_no={$pj_s_no}';</script>");
    }
}
elseif($process == 'del_file')
{
    $folder     = isset($_POST['folder']) ? $_POST['folder'] : "";
    $file_path  = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $pj_s_no    = isset($_POST['pj_s_no']) ? $_POST['pj_s_no'] : "";

    $del_sql = "UPDATE `project_staff` SET `{$folder}_name`='', `{$folder}_path`='' WHERE pj_s_no = '{$pj_s_no}'";

    if(del_file($file_path) && mysqli_query($my_db, $del_sql))
    {
        exit("<script>alert('해당 파일을 삭제 하였습니다.');location.href='{$regist_url}?pj_s_no={$pj_s_no}';</script>");
    }
}
elseif($pj_s_no)
{
    $project_staff_sql 	 = "SELECT * FROM project_staff `ps` WHERE `ps`.pj_s_no = {$pj_s_no} LIMIT 1";
    $project_staff_query = mysqli_query($my_db, $project_staff_sql);
    $project_staff 		 = mysqli_fetch_assoc($project_staff_query);

    if(isset($project_staff['pj_s_no']))
    {
        $title = "수정하기";
    }
}

$my_company_model = MyCompany::Factory();
$smarty->assign("title", $title);
$smarty->assign($project_staff);
$smarty->assign('my_company_list', $my_company_model->getList());
$smarty->assign('active_option', getActiveOption());

$smarty->display('project_staff_regist.html');

?>
