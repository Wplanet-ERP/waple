<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');
require('inc/helper/work.php');
require('inc/helper/work_extra.php');
require('inc/helper/deposit.php');
require('inc/helper/withdraw.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Staff.php');
require('inc/model/Team.php');
require('inc/model/Product.php');
require('inc/model/Work.php');
require('inc/model/WorkCertificate.php');
require('inc/model/Logistics.php');
require('inc/model/Corporation.php');
require('inc/model/Company.php');

// SLACK
require ('api/SlackHook.php');

// NAVER API [START]
ini_set("default_socket_timeout", 30);
require('inc/restapi.php');

$config = parse_ini_file("inc/n_account.ini");

$api = new RestApi($config['BASE_URL'], $config['API_KEY'], $config['SECRET_KEY'], $config['CUSTOMER_ID']);
// NAVER API [END]

// 접근 권한 [START]
if (!$session_s_no){ // 비로그인 접속 불가
	$smarty->display('access_company_error.html');
	exit;
}

$reject = false;
if($session_staff_state == '2'){ // 프리랜서의 경우 기본 접근제한으로 설정
	$reject = true;
}

for($i=0 ; $i < sizeof($access_accept_list) ; $i++){ // 접근허용 리스트
	if($session_s_no == $access_accept_list[$i][1]){
		if($_GET['sch_prd'] == $access_accept_list[$i][2] || $access_accept_list[$i][2] == 'all'){
			$reject = false;
		}
	}
}

if($reject){
	for($i=0 ; $i < sizeof($access_reject_list) ; $i++){ // 접근제한 리스트
		if($session_s_no == $access_reject_list[$i][1]){
			if($_GET['sch_prd'] == $access_reject_list[$i][2] || $access_reject_list[$i][2] == 'all'){
				$reject = true;
			}
		}
	}
}
if($reject && !!$_GET['sch_prd']){
	$smarty->display('access_error.html');
	exit;
}

//개인경비 정리작업 접근권한(경영지원팀만 가능)
if((isset($_GET['sch_prd']) && !empty($_GET['sch_prd'])) && ($_GET['sch_prd'] =='179' && $session_team != "00211"))
{
    $smarty->display('access_error.html');
    exit;
}
// 접근 권한 [END]

$proc   = (isset($_POST['process']))?$_POST['process']:"";

$sch_keyword 	  = isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";
$sch_keyword_mode = isset($_GET['sch_keyword_mode']) ? $_GET['sch_keyword_mode'] : "";
$smarty->assign("sch_keyword", $sch_keyword);


if ($sch_keyword && $sch_keyword_mode == 'search')
{
    // [상품(업무) 종류]
    $sch_prd_g1 = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
    $sch_prd_g2 = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
    $sch_prd 	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

    $smarty->assign("sch_prd_g1", $sch_prd_g1);
    $smarty->assign("sch_prd_g2", $sch_prd_g2);
    $smarty->assign("sch_prd", $sch_prd);

    $prd_g1_list = $prd_g2_list = $prd_g3_list = [];

    foreach ($work_list as $key => $prd_data) {
        if (!$key) {
            $prd_g1_list = $prd_data;
        } else {
            $prd_g2_list[$key] = $prd_data;
        }
    }

    $prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
    $sch_prd_list = isset($work_prd_list[$sch_prd_g2]) ? $work_prd_list[$sch_prd_g2] : [];

    $smarty->assign("prd_g1_list", $prd_g1_list);
    $smarty->assign("prd_g2_list", $prd_g2_list);
    $smarty->assign("sch_prd_list", $sch_prd_list);


    $keyword_list 		= [];
    $keyword_list_sql 	= "SELECT prd_no, keyword FROM keyword_search WHERE kind='product'";
    $keyword_list_query = mysqli_query($my_db, $keyword_list_sql);
    while ($keyword = mysqli_fetch_array($keyword_list_query)) {
        $keyword_list[$keyword['prd_no']][] = $keyword['keyword'];
    }

    // [Quick Prd 확인]
    $quick_prd_sql 		= "SELECT quick_prd FROM staff where id = '{$session_id}'";
    $quick_prd_query 	= mysqli_query($my_db, $quick_prd_sql);
    $quick_prd_list 	= mysqli_fetch_array($quick_prd_query);
    $sch_quick_prd 		= explode(',', $quick_prd_list[0]);

    // [Keyword List 관리]
	$sch_keyword_sql = "
		SELECT
			kp.k_name_code AS prd_g1_no,
			p.k_name_code AS prd_g2_no,
			p.prd_no AS prd_no,
			kp.k_name AS prd_g1,
			k.k_name AS prd_g2,
			p.title AS prd_name,
			p.description AS prd_desc,
			p.keyword AS keyword,
			(SELECT sub.q_no FROM quick_search as sub WHERE sub.`type`='work' AND sub.s_no='{$session_s_no}' AND sub.prd_no=p.prd_no) AS quick_prd
		FROM product AS p
		LEFT JOIN kind AS k ON k.k_name_code = p.k_name_code AND k.k_code='product'
		LEFT JOIN kind AS kp ON kp.k_name_code = k.k_parent AND kp.k_code='product'
		WHERE p.keyword LIKE '%{$sch_keyword}%' AND p.display='1'
		ORDER BY p.prd_no
	";

	$sch_keyword_list 	= [];
	$sch_keyword_query 	= mysqli_query($my_db, $sch_keyword_sql);
	while ($product_keyword = mysqli_fetch_array($sch_keyword_query)) {
		$prd_g1_url 	    = "work_list.php?sch_prd_g1=" . $product_keyword['prd_g1_no'];
		$prd_g2_url 	    = $prd_g1_url . "&sch_prd_g2=" . $product_keyword['prd_g2_no'];
		$prd_url 		    = $prd_g2_url . "&sch_prd=" . $product_keyword['prd_no'];
		$quick_prd_chk 	    = in_array($product_keyword['prd_no'], $sch_quick_prd) ? "1" : "";
		$exp_prd_list		= explode(',', $product_keyword['keyword']);
		$prd_keyword_list   = "#".implode(' #', $exp_prd_list);
		$prd_keyword        = str_replace($sch_keyword,"<span class='font-red'>{$sch_keyword}</span>", $prd_keyword_list);

		$sch_keyword_list[] = array
		(
			'prd_g1' 		=> $product_keyword['prd_g1'],
			'prd_g2' 		=> $product_keyword['prd_g2'],
			'prd_no' 		=> $product_keyword['prd_no'],
			'prd_name' 		=> $product_keyword['prd_name'],
			'prd_desc' 		=> nl2br($product_keyword['prd_desc']),
			'prd_g1_url' 	=> $prd_g1_url,
			'prd_g2_url' 	=> $prd_g2_url,
			'prd_url' 		=> $prd_url,
			'keyword' 		=> $prd_keyword,
            'quick_prd' 	=> $product_keyword['quick_prd']
		);
	}

    $smarty->assign('sch_keyword_list', $sch_keyword_list);
    $smarty->assign('sch_keyword_list_count', count($sch_keyword_list));
    $smarty->display('work_keyword_list.html');
}
else
{
    # Model Init
    $work_model     = Work::Factory();
    $company_model  = Company::Factory();
    $product_model  = Product::Factory();

    # 프로세스 처리
    if ($proc == "c_memo") # 출금업체 메모 저장
    {
        $c_no       = (isset($_POST['c_no'])) ? $_POST['c_no'] : "";
        $value      = (isset($_POST['val']) && !empty($_POST['val'])) ? $_POST['val'] : "NULL";
        $upd_data   = array("c_no" => $c_no, "c_memo" => addslashes($value));

        if (!$company_model->update($upd_data))
            echo "출금업체 메모 저장에 실패 하였습니다.";
        else
            echo "출금업체 메모가 저장되었습니다.";
        exit;
    }
    elseif ($proc == "f_task_run_regdate") # 업무완료일 저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val']) && !empty($_POST['val'])) ? $_POST['val'] : "NULL";
        $upd_data   = array("w_no" => $w_no, "task_run_regdate" => $value);

        if (!$work_model->update($upd_data))
            echo "업무완료일 저장에 실패 하였습니다.";
        else
            echo "업무완료일이 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_work_state") # 진행상태 자동저장
    {
        $w_no   = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value  = (isset($_POST['val'])) ? $_POST['val'] : "";

        $upd_result     = false;
        $work_upd_data  = array("w_no" => $w_no, "work_state" => $value);
        $work_data      = $work_model->getExtraItem($w_no);

        $is_work_extra  = ($work_data['prd_work_type'] == '2' && !empty($work_data['prd_work_extra'])) ? true : false;
        $linked_no      = (isset($work_data['linked_no']) && !empty($work_data['linked_no'])) ? $work_data['linked_no'] : "";
        $linked_table   = (isset($work_data['linked_table']) && !empty($work_data['linked_table'])) ? $work_data['linked_table'] : "";

        if ($value == '6')
        {
            $task_run_date  = isset($work_data['task_run_regdate']) && !empty($work_data['task_run_regdate']) ? $work_data['task_run_regdate'] : date("Y-m-d H:i:s");

            $work_upd_data['task_run_regdate'] = $task_run_date;
            $upd_result = $work_model->update($work_upd_data);

            if (!empty($w_no))
            {
                if(!empty($linked_no) && !empty($linked_table))
                {
                    switch ($linked_table)
                    {
                        case 'work_certificate':
                            if ($is_work_extra) {
                                $extra_model = WorkCertificate::Factory();
                                $extra_model->update(array("wc_no" => $linked_no, "work_state" => '6', "run_date" => date("Y-m-d H:i:s")));
                            }
                            break;
                        case 'logistics_management':
                            if ($is_work_extra) {
                                $extra_model    = Logistics::Factory();
                                $extra_upd_data = array("lm_no" => $linked_no, "work_state" => '6', "run_date" => date("Y-m-d H:i:s"));

                                if(!empty($work_data['task_run_s_no'])){
                                    $extra_upd_data['run_s_no'] = $work_data['task_run_s_no'];
                                    $extra_upd_data['run_team'] = $work_data['task_run_team'];
                                }
                                $extra_model->update($extra_upd_data);
                            }
                            break;
                    }
                }

                # SLACK API
                if ($work_data && $work_data['task_run_s_no'])
                {
                    $company_name   = (isset($work_data['t_keyword']) && !empty($work_data['t_keyword'])) ? "{$work_data['c_name']} {$work_data['t_keyword']}" : $work_data['c_name'];
                    $prd_no         = $work_data['prd_no'];
                    $prd_name       = $work_data['prd_name'];
                    $s_name         = isset($staff_all_list[$work_data['task_run_s_no']]) ? $staff_all_list[$work_data['task_run_s_no']]['s_name'] : "없음";
                    $return         = "https://work.wplanet.co.kr/v1/work_list.php?sch=Y&sch_w_no={$w_no}&sch_prd={$prd_no}";
                    $title          = "{$prd_name}";
                    $message        = "{$company_name} :: {$s_name} (업무완료)\r\n{$return}";
                    $channel        = $slack_all_list[$work_data['task_req_s_no']];
                    $slack_type     = 'completed';

                    if (empty($vacation_staff_list) || (!empty($vacation_staff_list) && !in_array($work_data['task_req_s_no'], $vacation_staff_list))) {
                        sendSlackMessageBlock($channel, $title, $message, $slack_type);
                    }
                }
            }
        }
        elseif ($value == '3')
        {
            $price_apply_val    = $work_data['price_apply'];
            $service_apply_val  = $work_data['service_apply'];
            $selling_price_val  = $work_data['selling_price'];
            $service_memo_val   = $work_data['service_memo'];

            if ($price_apply_val == '2') {
                if ($service_apply_val == '1' && (!$selling_price_val || $selling_price_val == "0")) { // Service No & 판매가 미입력의 경우
                    echo "진행요청 불가!!! 판매를 입력하세요.";
                    exit;
                } else if ($service_apply_val == '2' && !$service_memo_val) { // Service Yes & 서비스사유 미입력의 경우
                    echo "진행요청 불가!!! 서비스 사유를 입력하세요.";
                    exit;
                }
            } else {
                $upd_result = $work_model->update($work_upd_data);
                if(!empty($linked_no) && !empty($linked_table))
                {
                    switch ($linked_table)
                    {
                        case 'work_certificate':
                            if($is_work_extra){
                                $extra_model = WorkCertificate::Factory();
                                $extra_model->update(array("wc_no" => $linked_no, "work_state" => $value));
                            }
                            break;
                        case 'logistics_management':
                            if($is_work_extra){
                                $extra_model = Logistics::Factory();
                                $extra_model->update(array("lm_no" => $linked_no, "work_state" => $value));
                            }
                            break;
                    }
                }
            }
        }
        else
        {
            $upd_result = $work_model->update($work_upd_data);
            $slack_list = [];
            $prd_no     = "";

            if(!empty($linked_no) && !empty($linked_table))
            {
                switch($linked_table)
                {
                    case 'work_certificate':
                        if($is_work_extra){
                            $extra_model = WorkCertificate::Factory();
                            $extra_model->update(array("wc_no" => $linked_no, "work_state" => $value));
                        }
                        break;
                    case 'logistics_management':
                        if($is_work_extra){
                            $extra_model = Logistics::Factory();
                            $extra_model->update(array("lm_no" => $linked_no, "work_state" => $value));
                        }
                        break;
                }

                # 취소일때 Slack
                if($value == '8')
                {
                    # SLACK API
                    if (isset($work_data['task_run_s_no']) && !empty($work_data['task_run_s_no'])) {
                        $slack_list[$work_data['task_run_s_no']] = $slack_all_list[$work_data['task_run_s_no']];
                    } else {
                        if ((isset($work_data['prd_no']) && !empty($work_data['prd_no']))
                            && (isset($task_req_accepter_slack_list[$work_data['prd_no']]) && !empty($task_req_accepter_slack_list[$work_data['prd_no']]))
                        ) {
                            $task_req_accepter = $task_req_accepter_slack_list[$work_data['prd_no']];
                            $prd_no = $work_data['prd_no'];

                            foreach ($task_req_accepter as $task_req_s_no) {
                                $slack_list[$task_req_s_no] = $slack_all_list[$task_req_s_no];
                            }
                        }
                    }

                    $company_name = $work_data['c_name'];
                    if (isset($work_data['t_keyword']) && !empty($work_data['t_keyword'])) {
                        $company_name .= " {$work_data['t_keyword']}";
                    }
                    $prd_name = $work_data['prd_name'];
                    $return = "https://work.wplanet.co.kr/v1/work_list.php?sch=Y&sch_w_no={$w_no}&sch_prd={$prd_no}";
                    $title = "{$prd_name}";
                    $message = "{$company_name} (업무취소)\r\n{$return}";
                    $slack_type = 'request';

                    if (!empty($slack_list))
                    {
                        foreach ($slack_list as $key => $slack_id) {
                            if (!!$slack_id) {
                                if (empty($vacation_staff_list) || (!empty($vacation_staff_list) && !in_array($key, $vacation_staff_list))) {
                                    sendSlackMessageBlock($slack_id, $title, $message, $slack_type);
                                }
                            }
                        }
                    }
                    /* Slack API END */
                }
            }
        }

        if (!$upd_result)
            echo "진행상태 저장에 실패 하였습니다.";
        else {
            echo "진행상태가 저장 되었습니다.";
        }
        exit;
    }
    elseif ($proc == "f_t_keyword") # 타겟키워드 자동저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "t_keyword" => $value);

        if (!$work_model->update($upd_data))
            echo "타겟키워드 저장에 실패 하였습니다.";
        else
            echo "타겟키워드가 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_r_keyword") # 노출키워드 자동저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "r_keyword" => $value);

        if (!$work_model->update($upd_data))
            echo "노출키워드 저장에 실패 하였습니다.";
        else
            echo "노출키워드가 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_k_name_code") # 업무진행구분 자동저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val']) && !empty($_POST['val'])) ? addslashes(trim($_POST['val'])) : "NULL";
        $upd_data   = array("w_no" => $w_no, "k_name_code" => $value);

        if (!$work_model->update($upd_data))
            echo "업무진행구분 저장에 실패 하였습니다.";
        else
            echo "업무진행구분이 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_quantity") # 수량 자동저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "quantity" => $value);

        if (!$work_model->update($upd_data))
            echo "수량 저장에 실패 하였습니다.";
        else
            echo "수량이 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_set_title") # 세트명 자동저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "title" => $value);

        $work_model->setMainInit("work_set_info", "w_no");
        if (!$work_model->update($upd_data))
            echo "세트명 저장에 실패 하였습니다.";
        else
            echo "세트명이 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_task_req") # 업무요청 자동저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "task_req" => $value);

        $work_model->setCharset("utf8mb4");
        if (!$work_model->update($upd_data))
            echo "업무요청 저장에 실패 하였습니다.";
        else
            echo "업무요청이 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_task_run_staff" || $proc == "f_task_run_staff_name") { //업무처리 담당자 자동저장
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $task_run_s_no = (isset($_POST['run_s_no'])) ? $_POST['run_s_no'] : "";
        $task_run_team = (isset($_POST['run_team'])) ? $_POST['run_team'] : "";

        $sql = "UPDATE work w SET task_run_s_no='{$task_run_s_no}', task_run_team ='{$task_run_team}' WHERE w.w_no = '{$w_no}'";

        if (!mysqli_query($my_db, $sql))
            echo "업무처리 담당자 저장에 실패 하였습니다.";
        else {
            echo "업무처리 담당자가 저장 되었습니다.";

            /* Slack API START */
            $channel = isset($slack_all_list[$task_run_s_no]) ? $slack_all_list[$task_run_s_no] : "";

            if (!!$channel) {
                $work_data_sql = "SELECT w.work_state, w.c_name, w.t_keyword, w.prd_no, (SELECT p.title FROM product p WHERE p.prd_no = w.prd_no) as prd_name, task_req_s_no, w.work_state FROM work w WHERE w.w_no = '{$w_no}' AND work_state = '3' LIMIT 1";
                $work_data_query = mysqli_query($my_db, $work_data_sql);
                $work_data = mysqli_fetch_assoc($work_data_query);

                if ($work_data && (isset($work_data['work_state']) && $work_data['work_state'] == '3')) {
                    $company_name = $work_data['c_name'];
                    if (isset($work_data['t_keyword']) && !empty($work_data['t_keyword'])) {
                        $company_name .= " {$work_data['t_keyword']}";
                    }
                    $prd_no = $work_data['prd_no'];
                    $prd_name = $work_data['prd_name'];
                    $s_name = isset($staff_all_list[$work_data['task_req_s_no']]) ? $staff_all_list[$work_data['task_req_s_no']]['s_name'] : "없음";
                    $return = "https://work.wplanet.co.kr/v1/work_list.php?sch=Y&sch_w_no={$w_no}&sch_prd={$prd_no}";
                    $title = "{$prd_name}";
                    $message = "{$company_name} :: {$s_name} (업무요청)\r\n{$return}";
                    $slack_type = 'request';

                    if (empty($vacation_staff_list) || (!empty($vacation_staff_list) && !in_array($task_run_s_no, $vacation_staff_list))) {
                        sendSlackMessageBlock($channel, $title, $message, $slack_type);
                    }
                }
            }
            /* Slack API END */
        }

        exit;

    }
    elseif ($proc == "f_work_time") # work_time 자동저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "work_time" => $value);

        if (!$work_model->update($upd_data))
            echo "work_time 저장에 실패 하였습니다.";
        else
            echo "work_time이 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_task_run") # 업무진행 자동저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "task_run" => $value);

        $work_model->setCharset("utf8mb4");
        if (!$work_model->update($upd_data))
            echo "업무진행 저장에 실패 하였습니다.";
        else
            echo "업무진행이 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_dp_price") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value = (isset($_POST['val'])) ? $_POST['val'] : "";
        $value = str_replace(",", "", trim($value)); // 컴마 제거하기
        $sub_value = (isset($_POST['sub_val'])) ? $_POST['sub_val'] : "";

        if (!empty($sub_value)) {
            $upd_sql = "UPDATE work w SET w.dp_price = '{$value}' WHERE w.w_no = '{$w_no}'";
        } else {
            $company_sql = "SELECT c.license_type, c.c_no, (SELECT prd.equally_vat FROM product prd WHERE prd.prd_no=(SELECT w.prd_no FROM work w WHERE w.w_no='{$w_no}')) AS equally_vat FROM company c WHERE c.c_no=(SELECT w.dp_c_no FROM work w WHERE w.w_no='{$w_no}')";
            $company_query = mysqli_query($my_db, $company_sql);
            $company_result = mysqli_fetch_array($company_query);
            $license_type = $company_result['license_type'];
            $dp_c_no = $company_result['c_no'];
            $equally_vat = $company_result['equally_vat'];

            $value_vat = ""; // VAT 포함

            # 부가세 계산
            if ($dp_c_no == '5') { // c_no = 5 와이즈플래닛
                $value_vat = 0;
            } elseif ($equally_vat == '1') { //상품속성에 VAT동일 시
                $value_vat = $value;
            } else {
                if ($license_type == '1' || $license_type == '2') { // 법인회사 or 개인회사 (부가세처리)
                    $value_vat = $value + ($value * 0.1);
                } elseif ($license_type == '3') { // 개인
                    //$value_vat = $value * 0.967;
                    $value_tmp1 = $value * 0.03;
                    $value_tmp1 = floor($value_tmp1 / 10) * 10;
                    $value_tmp2 = floor(($value_tmp1 * 0.1) / 10) * 10;
                    $value_vat = $value - $value_tmp1 - $value_tmp2;
                }
            }

            $upd_sql = "UPDATE work w SET w.dp_price = '{$value}', w.dp_price_vat= '{$value_vat}' WHERE w.w_no = '{$w_no}'";
        }

        if (!mysqli_query($my_db, $upd_sql))
            echo "입금비 단가 저장에 실패 하였습니다.";
        else {
            echo "입금비 단가가 저장 되었습니다.";
        }

        exit;

    } elseif ($proc == "f_dp_price_vat") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value = (isset($_POST['val'])) ? $_POST['val'] : "";
        $value = str_replace(",", "", trim($value)); // 컴마 제거하기
        $sub_value = (isset($_POST['sub_val'])) ? $_POST['sub_val'] : "";

        if (!empty($sub_value)) {
            $upd_sql = "UPDATE work w SET dp_price_vat='{$value}' WHERE w.w_no = '{$w_no}'";
        } else {
            $company_sql = "SELECT c.license_type, c.c_no, (SELECT prd.equally_vat FROM product prd WHERE prd.prd_no=(SELECT w.prd_no FROM work w WHERE w.w_no='{$w_no}')) AS equally_vat FROM company c WHERE c.c_no=(SELECT w.dp_c_no FROM work w WHERE w.w_no='{$w_no}')";
            $company_query = mysqli_query($my_db, $company_sql);
            $company_result = mysqli_fetch_array($company_query);
            $license_type = $company_result['license_type'];
            $dp_c_no = $company_result['c_no'];
            $equally_vat = $company_result['equally_vat'];

            if ($dp_c_no == '5') { // c_no = 5 와이즈플래닛
                $value_vat = 0;
            } elseif ($equally_vat == '1') { //상품속성에 VAT동일 시
                $value_vat = $value;
            } else {
                $value_vat = $value;
                if ($license_type == '1' || $license_type == '2') { // 법인회사 or 개인회사 (부가세처리)

                    $tax_ratio = 10;
                    $amount_tax = round($value / ((100 + $tax_ratio) / 10));
                    $value = (int)$value_vat - (int)$amount_tax;

                } elseif ($license_type == '3') { // 개인
                    $tax_ratio = -3.3;
                    $amount_total = floor(($value / ((100 + $tax_ratio) / 100)) / 10) * 10;
                    $value = (int)$amount_total;
                }
            }

            $upd_sql = "UPDATE work w SET dp_price='{$value}', dp_price_vat = '{$value_vat}' WHERE w.w_no = '{$w_no}'";
        }


        if (!mysqli_query($my_db, $upd_sql))
            echo "입금비 VAT 저장에 실패 하였습니다.";
        else {
            echo "입금비 VAT 저장 되었습니다.";
        }

    } elseif ($proc == "f_dp_c_no" || $proc == "f_dp_c_name") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value = (isset($_POST['val'])) ? $_POST['val'] : "";

        if (empty($value)) { // dp_c_no = 0 ::미선택:: 으로 설정 시
            $sql = "UPDATE work w SET w.dp_c_no = NULL , w.dp_c_name = NULL WHERE w.w_no = '" . $w_no . "'";
        } else {
            $sql = "UPDATE work w SET w.dp_c_no = '" . $value . "' , w.dp_c_name = (SELECT c.c_name FROM company c WHERE c_no='" . $value . "')  WHERE w.w_no = '" . $w_no . "'";
        }


        if (!mysqli_query($my_db, $sql))
            echo "입금업체 저장에 실패 하였습니다.";
        else {
            // 상품의 VAT동일여부 및 법인회사 개인회사 구분하여 부가세 자동계산 Start
            $company_sql = "SELECT c.license_type,
		 										c.c_no,
												(SELECT prd.equally_vat FROM product prd WHERE prd.prd_no=(SELECT w.prd_no FROM work w WHERE w.w_no='" . $w_no . "')) AS equally_vat
								FROM company c
								WHERE c.c_no=(SELECT w.dp_c_no FROM work w WHERE w.w_no='" . $w_no . "')
								";
            $company_result = mysqli_query($my_db, $company_sql);
            $result = mysqli_fetch_array($company_result);
            $license_type = $result['license_type'];
            $dp_c_no = $result['c_no'];
            $equally_vat = $result['equally_vat'];

            $value_vat = ""; // VAT 포함

            //내부 작업건의 경우 입금요청액(VAT 포함)을 0으로 처리
            if ($dp_c_no == '5') { // c_no = 5 와이즈플래닛
                $sql = "UPDATE work w SET w.dp_price_vat = '0' WHERE w.w_no = '" . $w_no . "'";
            } elseif ($equally_vat == '1') { //상품속성에 VAT동일 시
                $sql = "UPDATE work w SET w.dp_price_vat = w.dp_price WHERE w.w_no = '" . $w_no . "'";
            } else {
                if ($license_type == '1' || $license_type == '2') { // 법인회사 or 개인회사 (부가세처리)
                    $sql = "UPDATE work w SET w.dp_price_vat = (w.dp_price*1.1) WHERE w.w_no = '" . $w_no . "'";
                } elseif ($license_type == '3') { // 개인
                    $sql = "UPDATE work w SET w.dp_price_vat = (w.dp_price - TRUNCATE(w.dp_price*0.03, -1) - TRUNCATE((TRUNCATE(w.dp_price*0.03, -1) * 0.1), -1)) WHERE w.w_no = '" . $w_no . "'";
                }
            }
            // 상품의 VAT동일여부 및 법인회사 개인회사 구분하여 부가세 자동계산 End

            if (!mysqli_query($my_db, $sql))
                echo "입금업체 저장에 실패 하였습니다.";
            else {
                echo "입금업체가 저장 되었습니다.";
            }
        }

        exit;

    } elseif ($proc == "f_wd_price") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value = (isset($_POST['val'])) ? $_POST['val'] : "";
        $value = str_replace(",", "", trim($value)); // 컴마 제거하기

        $sub_value = (isset($_POST['sub_val'])) ? $_POST['sub_val'] : "";

        if (!empty($sub_value)) {
            $upd_sql = "UPDATE work w SET w.wd_price='{$value}' WHERE w.w_no = '{$w_no}'";
        } else {
            $company_sql = "SELECT c.license_type, c.c_no, (SELECT prd.equally_vat FROM product prd WHERE prd.prd_no=(SELECT w.prd_no FROM work w WHERE w.w_no='{$w_no}')) AS equally_vat FROM company c WHERE c.c_no=(SELECT w.wd_c_no FROM work w WHERE w.w_no='{$w_no}')";
            $company_query = mysqli_query($my_db, $company_sql);
            $company_result = mysqli_fetch_array($company_query);
            $license_type = $company_result['license_type'];
            $wd_c_no = $company_result['c_no'];
            $equally_vat = $company_result['equally_vat'];

            $value_vat = ""; // VAT 포함

            # 부가세 계산
            if ($wd_c_no == '5') { // c_no = 5 와이즈플래닛
                $value_vat = 0;
            } elseif ($equally_vat == '1') { //상품속성에 VAT동일 시
                $value_vat = $value;
            } else {
                if ($license_type == '1' || $license_type == '2') { // 법인회사 or 개인회사 (부가세처리)
                    $value_vat = $value + ($value * 0.1);
                } elseif ($license_type == '3') { // 개인
                    //$value_vat = $value * 0.967;
                    $value_tmp1 = $value * 0.03;
                    $value_tmp1 = floor($value_tmp1 / 10) * 10;
                    $value_tmp2 = floor(($value_tmp1 * 0.1) / 10) * 10;
                    $value_vat = $value - $value_tmp1 - $value_tmp2;
                }
            }

            $upd_sql = "UPDATE work w SET w.wd_price='{$value}', wd_price_vat='{$value_vat}' WHERE w.w_no = '{$w_no}'";
        }


        if (!mysqli_query($my_db, $upd_sql))
            echo "출금비 단가 저장에 실패 하였습니다.";
        else {
            echo "출금비 단가가 저장 되었습니다.";
        }

        exit;

    } elseif ($proc == "f_wd_price_vat") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value = (isset($_POST['val'])) ? $_POST['val'] : "";
        $value = str_replace(",", "", trim($value)); // 컴마 제거하기

        $sub_value = (isset($_POST['sub_val'])) ? $_POST['sub_val'] : "";

        if (!empty($sub_value)) {
            $upd_sql = "UPDATE work w SET wd_price_vat = '{$value}' WHERE w.w_no = '{$w_no}'";
        } else {
            $company_sql = "SELECT c.license_type, c.c_no, (SELECT prd.equally_vat FROM product prd WHERE prd.prd_no=(SELECT w.prd_no FROM work w WHERE w.w_no='{$w_no}')) AS equally_vat FROM company c WHERE c.c_no=(SELECT w.wd_c_no FROM work w WHERE w.w_no='{$w_no}')";
            $company_query = mysqli_query($my_db, $company_sql);
            $company_result = mysqli_fetch_array($company_query);
            $license_type = $company_result['license_type'];
            $wd_c_no = $company_result['c_no'];
            $equally_vat = $company_result['equally_vat'];

            if ($wd_c_no == '5') { // c_no = 5 와이즈플래닛
                $value_vat = 0;
            } elseif ($equally_vat == '1') { //상품속성에 VAT동일 시
                $value_vat = $value;
            } else {
                $value_vat = $value;
                if ($license_type == '1' || $license_type == '2') { // 법인회사 or 개인회사 (부가세처리)

                    $tax_ratio = 10;
                    $amount_tax = round($value / ((100 + $tax_ratio) / 10));
                    $value = (int)$value_vat - (int)$amount_tax;

                } elseif ($license_type == '3') { // 개인
                    $tax_ratio = -3.3;
                    $amount_total = floor(($value / ((100 + $tax_ratio) / 100)) / 10) * 10;
                    $value = (int)$amount_total;
                }
            }

            $upd_sql = "UPDATE work w SET wd_price='{$value}', wd_price_vat = '{$value_vat}' WHERE w.w_no = '{$w_no}'";
        }

        if (!mysqli_query($my_db, $upd_sql))
            echo "출금비 VAT 저장에 실패 하였습니다.";
        else {
            echo "출금비 VAT 저장 되었습니다.";
        }

    } elseif ($proc == "f_wd_c_no" || $proc == "f_wd_c_name") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value = (isset($_POST['val'])) ? $_POST['val'] : "";

        if (empty($value)) { // wd_c_no = 0 ::미선택:: 으로 설정 시
            $sql = "UPDATE work w SET w.wd_c_no = NULL , w.wd_c_name = NULL WHERE w.w_no = '" . $w_no . "'";
        } else {
            $sql = "UPDATE work w SET w.wd_c_no = '" . $value . "' , w.wd_c_name = (SELECT c.c_name FROM company c WHERE c_no='" . $value . "')  WHERE w.w_no = '" . $w_no . "'";
        }


        if (!mysqli_query($my_db, $sql))
            echo "출금업체 저장에 실패 하였습니다.";
        else {
            // 상품의 VAT동일여부 및 법인회사 개인회사 구분하여 부가세 자동계산 Start
            $company_sql = "SELECT c.license_type,
		 										c.c_no,
												(SELECT prd.equally_vat FROM product prd WHERE prd.prd_no=(SELECT w.prd_no FROM work w WHERE w.w_no='" . $w_no . "')) AS equally_vat
								FROM company c
								WHERE c.c_no=(SELECT w.wd_c_no FROM work w WHERE w.w_no='" . $w_no . "')
								";
            $company_result = mysqli_query($my_db, $company_sql);
            $result = mysqli_fetch_array($company_result);
            $license_type = $result['license_type'];
            $wd_c_no = $result['c_no'];
            $equally_vat = $result['equally_vat'];

            $value_vat = ""; // VAT 포함

            //내부 작업건의 경우 출금요청액(VAT 포함)을 0으로 처리
            if ($wd_c_no == '5') { // c_no = 5 와이즈플래닛
                $sql = "UPDATE work w SET w.wd_price_vat = '0' WHERE w.w_no = '" . $w_no . "'";
            } elseif ($equally_vat == '1') { //상품속성에 VAT동일 시
                $sql = "UPDATE work w SET w.wd_price_vat = w.wd_price WHERE w.w_no = '" . $w_no . "'";
            } else {
                if ($license_type == '1' || $license_type == '2') { // 법인회사 or 개인회사 (부가세처리)
                    $sql = "UPDATE work w SET w.wd_price_vat = (w.wd_price*1.1) WHERE w.w_no = '" . $w_no . "'";
                } elseif ($license_type == '3') { // 개인
                    $sql = "UPDATE work w SET w.wd_price_vat = (w.wd_price - TRUNCATE(w.wd_price*0.03, -1) - TRUNCATE((TRUNCATE(w.wd_price*0.03, -1) * 0.1), -1)) WHERE w.w_no = '" . $w_no . "'";
                }
            }
            // 상품의 VAT동일여부 및 법인회사 개인회사 구분하여 부가세 자동계산 End

            if (!mysqli_query($my_db, $sql))
                echo "출금업체 저장에 실패 하였습니다.";
            else {
                echo "출금업체가 저장 되었습니다.";
            }
        }

        exit;
    } elseif ($proc == "f_extension_date") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value = (isset($_POST['val'])) ? $_POST['val'] : "";

        if (empty($value)) {
            $sql = "UPDATE work w SET w.extension_date = NULL WHERE w.w_no = '" . $w_no . "'";
        } else {
            $sql = "UPDATE work w SET w.extension_date = '" . $value . "' WHERE w.w_no = '" . $w_no . "'";
        }

        if (!mysqli_query($my_db, $sql))
            echo "연장여부 마감일 저장에 실패 하였습니다.";
        else
            echo "연장여부 마감일이 저장 되었습니다.";

        exit;

    } elseif ($proc == "f_task_req_dday") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value = (isset($_POST['val'])) ? $_POST['val'] : "";
        $prd_no = (isset($_POST['prd_no'])) ? $_POST['prd_no'] : "";

        if (empty($value)) {
            $sql = "UPDATE work w SET w.task_req_dday = NULL WHERE w.w_no = '" . $w_no . "'";
        } else {
            $sql = "UPDATE work w SET w.task_req_dday = '" . $value . "' WHERE w.w_no = '" . $w_no . "'";

            $task_req_dday_cnt = task_req_dday_count($prd_no, $value); // 상품별 희망완료일 최대수 점검
            task_req_dday_count_alert($task_req_dday_cnt, $prd_no, $value); // 상품별 희망완료일 최대수를 넘어설 경우 alert 처리
        }

        if (!mysqli_query($my_db, $sql))
            echo "희망 완료일 저장에 실패 하였습니다.";
        else
            echo "희망 완료일이 저장 되었습니다.";

        exit;

    } elseif ($proc == "f_task_run_dday") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value = (isset($_POST['val'])) ? $_POST['val'] : "";

        if (empty($value)) {
            $sql = "UPDATE work w SET w.task_run_dday = NULL WHERE w.w_no = '" . $w_no . "'";
        } else {
            $sql = "UPDATE work w SET w.task_run_dday = '" . $value . "' WHERE w.w_no = '" . $w_no . "'";
        }

        if (!mysqli_query($my_db, $sql))
            echo "완료 예정일 저장에 실패 하였습니다.";
        else
            echo "완료 예정일이 저장 되었습니다.";

        exit;

    }
    elseif ($proc == "f_extension") # 업무 연장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? $_POST['val'] : "";
        $prd_no     = (isset($_POST['f_prd_no'])) ? $_POST['f_prd_no'] : "";

        $upd_data   = array("w_no" => $w_no, "extension" => $value);

        if (!$work_model->update($upd_data))
        {
            echo "연장여부 저장에 실패 하였습니다.";
        }
        else
        {
            if ($value == '1')
            {
                $work_item      = $work_model->getItem($w_no);
                $company_item   = $company_model->getItem($work_item['c_no']);
                $product_item   = $product_model->getItem($work_item['prd_no']);

                $cur_date               = date("Y-m-d H:i:s");
                $task_req_s_no_value    = $session_s_no;
                $task_req_s_team_value  = $session_team;
                $work_state_value       = '3';
                if (permissionNameCheck($session_permission, "외주관리자")) { // 외주관리자가 연장시 업무요청자는 업체 담당자로 하고 접수완료로 처리함
                    $task_req_s_no_value    = $company_item["s_no"];
                    $task_req_s_team_value  = $company_item["team"];
                    $work_state_value       = '4';
                }

                $work_insert_data = array(
                    "regdate"           => $cur_date,
                    "work_state"        => $work_state_value,
                    "c_no"              => $work_item['c_no'],
                    "c_name"            => $company_item['c_name'],
                    "s_no"              => $company_item['s_no'],
                    "team"              => $company_item['team'],
                    "prd_no"            => $work_item['prd_no'],
                    "t_keyword"         => addslashes($work_item['t_keyword']),
                    "r_keyword"         => addslashes($work_item['r_keyword']),
                    "wd_price"          => $work_item['wd_price'],
                    "wd_price_vat"      => $work_item['wd_price_vat'],
                    "wd_c_name"         => $work_item['wd_c_name'],
                    "wd_c_no"           => $work_item['wd_c_no'],
                    "extension_date"    => date("Y-m-d", strtotime("{$work_item['extension_date']} +1 months")),
                    "task_req"          => addslashes("연장 ({$w_no})"),
                    "task_req_s_no"     => $task_req_s_no_value,
                    "task_req_team"     => $task_req_s_team_value,
                    "work_time"         => $product_item['work_time_default'],
                );

                $task_run_staff_list     = explode(",", $product_item['task_run_staff']);
                $task_run_staff_list_cnt = count($task_run_staff_list);

                if ($task_run_staff_list_cnt == '1') {
                    $work_insert_data['task_run_s_no'] = $work_item['task_run_s_no'];
                    $work_insert_data['task_run_team'] = $work_item['task_run_team'];
                }

                if (!$work_model->insert($work_insert_data))
                    echo "동일한 내용으로 새로 요청에 실패 하였습니다.";
                else
                    echo "동일한 내용으로 새로 요청 후 연장여부가 저장 되었습니다.";
                echo "<script>setTimeout(function() {location.reload();}, 1200);</script>";
            } else {
                echo "연장여부가 저장 되었습니다.";
            }
        }
        exit;
    }
    elseif ($proc == "f_manager_memo") # 관리자메모 저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "manager_memo" => $value);

        if (!$work_model->update($upd_data))
            echo "관리자 메모 저장에 실패 하였습니다.";
        else
            echo "관리자 메모가 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_selling_price") # 판매가 저장
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? trim($_POST['val']) : "";
        $value      = str_replace(",", "", $value);
        $upd_data   = array("w_no" => $w_no, "selling_price" => $value);

        if (!$work_model->update($upd_data))
            echo "판매가 저장에 실패 하였습니다.";
        else
            echo "팬매가가 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_service_apply") # 서비스 적용여부
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "service_apply" => $value);

        if ($value == '2') {
            echo("<script>alert('하단에 서비스 사유를 꼭 작성해 주세요.');</script>");
        }

        if (!$work_model->update($upd_data))
            echo "서비스적용 저장에 실패 하였습니다.";
        else
            echo "서비스적용이 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_service_memo") # 서비스 사유
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "service_memo" => $value);

        if (!$work_model->update($upd_data))
            echo "서비스사유 저장에 실패 하였습니다.";
        else
            echo "서비스사유가 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_req_evaluation") # 업무요청자 평가
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "req_evaluation" => $value);

        if (!$work_model->update($upd_data))
            echo "업무요청자에 대한 업무평가 저장에 실패 하였습니다.";
        else
            echo "업무요청자에 대한 업무평가가 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_req_evaluation_memo") # 업무요청자 평가메모
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "req_evaluation_memo" => $value);

        if (!$work_model->update($upd_data))
            echo "업무요청자에 대한 업무평가 메모 저장에 실패 하였습니다.";
        else
            echo "업무요청자에 대한 업무평가 메모가 저장 되었습니다.";

        exit;
    }
    elseif ($proc == "f_evaluation") # 업무처리자 평가
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "evaluation" => $value);

        if (!$work_model->update($upd_data))
            echo "업무평가 저장에 실패 하였습니다.";
        else
            echo "업무평가가 저장 되었습니다.";
        exit;
    }
    elseif ($proc == "f_evaluation_memo") # 업무처리자 평가메모
    {
        $w_no       = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value      = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
        $upd_data   = array("w_no" => $w_no, "evaluation_memo" => $value);

        if (!$work_model->update($upd_data))
            echo "업무평가 메모 저장에 실패 하였습니다.";
        else
            echo "업무평가 메모가 저장 되었습니다.";
        exit;

    } elseif ($proc == "f_c_name") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $c_no = (isset($_POST['c_no'])) ? $_POST['c_no'] : "";
        $s_no = (isset($_POST['s_no'])) ? $_POST['s_no'] : "";
        $team = (isset($_POST['team'])) ? $_POST['team'] : "";
        $c_name = (isset($_POST['c_name'])) ? $_POST['c_name'] : "";

        $sql = "UPDATE work w SET w.c_no = '{$c_no}', w.c_name='{$c_name}', w.s_no='{$s_no}', w.team='{$team}' WHERE w.w_no = '{$w_no}'";

        if (!mysqli_query($my_db, $sql))
            echo "업체명 저장에 실패 하였습니다.";
        else
            echo "업체명이 저장 되었습니다.";

        exit;

    } elseif ($proc == "f_s_no") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $s_no = (isset($_POST['s_no'])) ? $_POST['s_no'] : "";
        $team = (isset($_POST['team'])) ? $_POST['team'] : "";
        "";

        $sql = "UPDATE work w SET w.s_no='{$s_no}', w.team='{$team}' WHERE w.w_no = '{$w_no}'";

        if (!mysqli_query($my_db, $sql))
            echo "담당자 저장에 실패 하였습니다.";
        else
            echo "담당자가 저장 되었습니다.";

        exit;

    } elseif ($proc == "task_req_s_no") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $task_req_s_no = (isset($_POST['req_s_no'])) ? $_POST['req_s_no'] : "";
        $task_req_team = (isset($_POST['req_team'])) ? $_POST['req_team'] : "";

        if (empty($task_req_s_no) || empty($task_req_team)) {
            echo "요청자를 다시 입력해주세요";
        } else {
            $sql = "UPDATE work w SET w.task_req_s_no='{$task_req_s_no}', w.task_req_team='{$task_req_team}' WHERE w.w_no = '{$w_no}'";

            if (!mysqli_query($my_db, $sql))
                echo "요청자 저장에 실패 하였습니다.";
            else
                echo "요청자가 저장 되었습니다.";
        }

        exit;

    } else if ($proc == "f_priority") { // 순번 자동저장
        $w_no   = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value  = (isset($_POST['val'])) ? $_POST['val'] : "";
        $set_no = (isset($_POST['set_no'])) ? $_POST['set_no'] : "";

        if (empty($value)) {
            $sql = "UPDATE  `work` SET priority = NULL WHERE w_no = '" . $w_no . "'";
        } else {
            $sql = "UPDATE  `work` SET priority = '" . $value . "' WHERE w_no = '" . $w_no . "'";
        }

        $f_priority_new     = !(empty($value)) ? $value : 0;
        $set_w_no           = $work_val_result['set_w_no'];
        $chk_priority_list  = [];

        if($set_no)
        {
            $chk_priority_sql 	= "SELECT w.w_no FROM `work` w WHERE w.set_tag like '%#{$set_no}%' AND w_no != '{$w_no}' AND priority >= {$f_priority_new} ORDER BY priority ASC, w_no ASC";
            $chk_priority_query = mysqli_query($my_db, $chk_priority_sql);

            while($chk_priority = mysqli_fetch_assoc($chk_priority_query))
            {
                $f_priority_new++;
                $chk_priority_list[$chk_priority['w_no']] = $f_priority_new;
            }
        }

        if (!mysqli_query($my_db, $sql))
            echo "순번 변경에 실패 하였습니다.";
        else {
            if(!empty($chk_priority_list)){
                foreach($chk_priority_list as $w_no_val => $priority){
                    $upd_priority_sql = "UPDATE `work` SET priority='{$priority}' WHERE w_no='{$w_no_val}'";
                    mysqli_query($my_db, $upd_priority_sql);
                }
            }
            echo "순번이 변경 되었습니다.";
            echo "<script>setTimeout(function() {location.reload();}, 1200);</script>";
        }

        exit;

    } elseif ($proc == "f_work_value") { //work value
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value = (isset($_POST['val'])) ? $_POST['val'] : "";
        $value = str_replace(",", "", trim($value)); // 컴마 제거하기

        $sql = "UPDATE work w SET w.work_value = '{$value}' WHERE w.w_no = '{$w_no}'";

        if (!mysqli_query($my_db, $sql))
            echo "Work value 저장에 실패 하였습니다.";
        else
            echo "Work value 저장 되었습니다.";

        exit;


/////////////////////////* fom action 처리 부분 *//////////////////////

    }
    elseif ($proc == "proc_quick_prd")
    {
        $quick_prd_sql   = "SELECT *, (SELECT p.`title` FROM product p WHERE p.prd_no=qs.prd_no) as prd_name FROM quick_search qs WHERE `type`='work' AND s_no='{$session_s_no}' ORDER BY priority ASC";
        $quick_prd_query = mysqli_query($my_db, $quick_prd_sql);
        $quick_prd_list  = [];
        while($quick_prd = mysqli_fetch_assoc($quick_prd_query))
        {
            $quick_prd_list[$quick_prd['prd_no']] = $quick_prd['prd_name'];
        }
        $sch_quick_chk = isset($_POST['sch_quick_chk']) ? $_POST['sch_quick_chk'] : "";

        echo "|";
        echo "<table cellpadding='0' cellspacing='0' style='min-width: 440px;'><tr><td style='width: 120px;text-align:center;padding: 5px;clear: both;overflow: hidden;'>";
        echo "<span style='margin-top:10px;font-size:14px; font-weight: bold;'>MY QUICK SET</span><br/>";
        echo "<span style='font-size: 11px;font-weight: normal;display: block;margin-bottom: 5px;'>와이즈 업무";
        echo "<a href='popup/quick_prd_set.php' onclick=\"window.open(this.href,'MY QUICK SET','width=710, height=540, toolbar=no, menubar=no, scrollbars=yes, resizable=yes');return false;\">";
        echo "<img src='images/icon/setting_lightgray.png' style='margin-left: 4px;margin-top: -3px;vertical-align: middle;width: 16px;filter: opacity(0.5) drop-shadow(0 0 0 #333333);'>";
        echo "</a>";
        echo "</span>";
        if ($sch_quick_chk == '1') {
            echo "<input type='checkbox' id='quick_search_chk' name='quick_search_chk' value='1' checked>";
        } else {
            echo "<input type='checkbox' id='quick_search_chk' name='quick_search_chk' value='1'>";
        }
        echo "<label for='quick_search_chk' style='font-size: 11px; cursor: pointer;'>검색조건유지</label>";
        echo "</td><td>";

        $quick_cnt  = 0;
        $sch_prd    = isset($_POST['sch_prd']) ? $_POST['sch_prd'] : "";
        $sch_prd_g1 = isset($_POST['sch_prd_g1']) ? $_POST['sch_prd_g1'] : "";
        $sch_prd_g2 = isset($_POST['sch_prd_g2']) ? $_POST['sch_prd_g2'] : "";

        # 퀵버튼 이동
        foreach ($quick_prd_list as $key => $label)
        {
            if ($quick_cnt == '0')
            {
                echo "<span>";
                if (empty($sch_prd) && (empty($sch_prd_g1) && empty($sch_prd_g2))) {
                    echo "<button type='button' class='btn_small btn_quick_add' attr-url='' onclick='moveQuickUrl(this)' style='padding: 4px 6px;margin-left: 0px; background-color: #c73333;'>::전체::</button>";
                } else {
                    echo "<button type='button' class='btn_small btn_quick_add' attr-url='' onclick='moveQuickUrl(this)' style='padding: 4px 6px;margin-left: 0px;'>::전체::</button>";
                }
                echo "</span> ";
            }

            echo "<span>";
            if ($sch_prd != $key)
                echo "<button type='button' class='btn_small btn_quick_add' attr-url='sch_prd_g1={$sch_prd_g1}&sch_prd_g2={$sch_prd_g2}&sch_prd={$key}' onclick='moveQuickUrl(this)' style='padding: 4px 6px;margin-left: 0px;'>{$label}</button>";
            else
                echo "<button type='button' class='btn_small btn_quick_add' attr-url='sch_prd_g1={$sch_prd_g1}&sch_prd_g2={$sch_prd_g2}&sch_prd={$key}' onclick='moveQuickUrl(this)' style='padding: 4px 6px;margin-left: 0px; background-color: #c73333;'>{$label}</button>";
            echo "</span> ";

            $quick_cnt++;
        }
        echo "</td></tr></table>";
        exit;

    } elseif ($proc == "check_f_t_keyword_new") { // 타겟 키워드 중복체크
        $f_t_keyword_new = str_replace(" ", "", $_POST['f_t_keyword_new']);
        $f_prd_no_new = (isset($_POST['f_prd_no_new'])) ? $_POST['f_prd_no_new'] : "";

        $sql = "
		SELECT COUNT(w_no) AS cnt
			FROM work
		WHERE prd_no = '{$f_prd_no_new}'
			AND extension_date >= date(now())
			AND REPLACE(t_keyword, ' ', '') = '{$f_t_keyword_new}'
			AND work_state < 7
			AND NOT(extension ='1' OR extension = '3')
	";

        $result = mysqli_query($my_db, $sql);
        $row = mysqli_fetch_array($result);
        $cnt = $row['cnt'];
        echo $cnt;

        exit;
    } elseif ($proc == "monthly_total_cnt") { // 월간 검색수
        $f_t_keyword_new = str_replace(" ", "", $_POST['f_t_keyword_new']);
        $f_prd_no_new = (isset($_POST['f_prd_no_new'])) ? $_POST['f_prd_no_new'] : "";

        $monthly_total_cnt = 0;

        $keyword_list = array(
            "hintKeywords" => $f_t_keyword_new
        );

        $response = $api->GET('/keywordstool', str_replace(' ', '', $keyword_list));

        for ($arr_i = 0; $arr_i < count($response["keywordList"]); $arr_i++) {
            $f_t_keyword_new = strtoupper($f_t_keyword_new); // 대문자로 변경

            if ($response["keywordList"][$arr_i]["relKeyword"] == $f_t_keyword_new) {
                $monthly_total_cnt = $response["keywordList"][$arr_i]["monthlyPcQcCnt"] + $response["keywordList"][$arr_i]["monthlyMobileQcCnt"];
                break;
            }
        }
        echo $monthly_total_cnt;
        exit;
    } elseif ($proc == "last_task_run_regdate") { // 동일 타겟 마지막 진행일
        $f_t_keyword_new = str_replace(" ", "", $_POST['f_t_keyword_new']);
        $f_prd_no_new = (isset($_POST['f_prd_no_new'])) ? $_POST['f_prd_no_new'] : "";

        $sql = "
		SELECT DATE_FORMAT(w.task_run_regdate, '%Y/%m/%d') AS task_run_regdate
			FROM work w
		WHERE w.prd_no = '{$f_prd_no_new}'
			AND REPLACE(w.t_keyword, ' ', '') = '{$f_t_keyword_new}'
			AND w.work_state = '6'
		ORDER BY w.task_run_regdate DESC
		LIMIT 1
	";

        $result = mysqli_query($my_db, $sql);
        $row = mysqli_fetch_array($result);
        $date = $row['task_run_regdate'];

        if ($date) {
            $d_day = (strtotime(date("Y/m/d", time())) - strtotime($date)) / 86400;
            echo $date . " (" . $d_day . "일 지남)";
        } else {
            echo "진행사례 없음";
        }

        exit;

    }
    elseif ($proc == "del_manager_record")
    {
        $w_no           = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
        $work_data      = $work_model->getExtraItem($w_no);

        $is_work_extra  = ($work_data['prd_work_type'] == '2' && !empty($work_data['prd_work_extra'])) ? true : false;
        $linked_no      = (isset($work_data['linked_no']) && !empty($work_data['linked_no'])) ? $work_data['linked_no'] : "";
        $linked_table   = (isset($work_data['linked_table']) && !empty($work_data['linked_table'])) ? $work_data['linked_table'] : "";

        if($work_data['work_state'] > 3){
            exit("<script>alert('w_no = $w_no 업무 레코드를 삭제하는데 실패 하였습니다. 접수완료전 데이터만 삭제가능합니다.');location.href='work_list.php?{$search_url}';</script>");
        }

        if (!$work_model->delete($w_no)) {
            exit("<script>alert('w_no = $w_no 업무 레코드를 삭제하는데 실패 하였습니다');location.href='work_list.php?{$search_url}';</script>");
        }
        else # 연관업무 및 extra 데이터 삭제
        {
            if ($w_no)
            {
                # 연관 아래 Depth 리스트
                $wr_chk_sql     = "SELECT w_no FROM work_relation WHERE w_parent='{$w_no}'";
                $wr_chk_query   = mysqli_query($my_db, $wr_chk_sql);
                $wr_chk_list    = [];
                while ($wr_chk_result = mysqli_fetch_assoc($wr_chk_query)) {
                    $wr_chk_list[] = $wr_chk_result['w_no'];
                }

                # 연관 윗 Depth 찾기
                $wr_self_sql = "SELECT w_parent FROM work_relation WHERE w_no='{$w_no}' LIMIT 1";
                $wr_self_query = mysqli_query($my_db, $wr_self_sql);
                $wr_self_result = mysqli_fetch_assoc($wr_self_query);
                $wr_chk_parent = isset($wr_self_result['w_parent']) ? $wr_self_result['w_parent'] : "";

                # 해당업무 삭제 및 parent 초기화
                $wr_upd_sql = "UPDATE work_relation SET w_parent=null WHERE w_parent='{$w_no}'";
                mysqli_query($my_db, $wr_upd_sql);
                $wr_del_sql = "DELETE FROM work_relation WHERE w_no='{$w_no}'";
                mysqli_query($my_db, $wr_del_sql);

                # 연관 아래 Depth 제거
                if ($wr_chk_list) {
                    foreach ($wr_chk_list as $wr_no) {
                        if ($wr_no) {
                            $wr_child_sql = "SELECT count(w_no) as cnt FROM work_relation WHERE w_parent='{$wr_no}'";
                            $wr_child_query = mysqli_query($my_db, $wr_child_sql);
                            $wr_child_result = mysqli_fetch_assoc($wr_child_query);

                            if (isset($wr_child_result['cnt']) && $wr_child_result['cnt'] == 0) {
                                $wr_child_del_sql = "DELETE FROM work_relation WHERE w_no='{$wr_no}'";
                                mysqli_query($my_db, $wr_child_del_sql);
                            }
                        }
                    }
                }

                # 연관 윗 Depth 제거
                if ($wr_chk_parent) {
                    $wr_parent_sql = "SELECT count(w_no) as cnt FROM work_relation WHERE w_parent = '{$wr_chk_parent}'";
                    $wr_parent_query = mysqli_query($my_db, $wr_parent_sql);
                    $wr_parent_result = mysqli_fetch_assoc($wr_parent_query);

                    #parent가 null인지 확인
                    $wr_parent_null_sql = "SELECT count(w_no) as cnt FROM work_relation WHERE w_no='{$wr_chk_parent}' AND w_parent IS NOT NULL LIMIT 1";
                    $wr_parent_null_query = mysqli_query($my_db, $wr_parent_null_sql);
                    $wr_parent_null_result = mysqli_fetch_assoc($wr_parent_null_query);

                    if ($wr_parent_null_result['cnt'] == 0 && $wr_parent_result['cnt'] == 0) {
                        $wr_parent_del_sql = "DELETE FROM work_relation WHERE w_no='{$wr_chk_parent}'";
                        mysqli_query($my_db, $wr_parent_del_sql);
                    }
                }

                if($is_work_extra && (!empty($linked_no) && !empty($linked_table)))
                {
                    switch ($linked_table) {
                        case "work_certificate":
                            $extra_model = WorkCertificate::Factory();
                            $extra_model->delete($linked_no);
                            break;
                        case "logistics_management":
                            $extra_model = Logistics::Factory();
                            $extra_model->delete($linked_no);
                            break;
                    }
                }
            }

        }
        exit("<script>alert('w_no = $w_no 업무 레코드를 삭제하였습니다.');location.href='work_list.php?{$search_url}';</script>");
        
    }
    elseif ($proc == "duplicate_manager_record") # 복제
    {
        $w_no           = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $search_url     = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";
        $prd_no         = (isset($_POST['f_prd_no'])) ? $_POST['f_prd_no'] : "";
        $set_w_no       = (isset($_POST['sch_set_w_no'])) ? $_POST['sch_set_w_no'] : "";
        $set_tag        = "#{$set_w_no}";
        $cur_date       = date("Y-m-d H:i:s");

        $work_item      = $work_model->getItem($w_no);
        $company_item   = $company_model->getItem($work_item['c_no']);
        $product_item   = $product_model->getItem($work_item['prd_no']);

        $task_dday_prd_list      = $product_model->getDdayList();
        $contents_prd_option     = getContentsWorkOption();
        $task_run_staff_list     = explode(",", $product_item['task_run_staff']);
        $task_run_staff_list_cnt = count($task_run_staff_list);

        $work_insert_data = array(
            "work_state"        => "3",
            "c_no"              => $work_item['c_no'],
            "c_name"            => $company_item['c_name'],
            "s_no"              => $company_item['s_no'],
            "team"              => $company_item['team'],
            "prd_no"            => $work_item['prd_no'],
            "t_keyword"         => addslashes($work_item['t_keyword']),
            "r_keyword"         => addslashes($work_item['r_keyword']),
            "selling_price"     => $work_item['selling_price'],
            "regdate"           => $cur_date,
            "task_req"          => addslashes($work_item['task_req']),
            "task_req_s_no"     => $session_s_no,
            "task_req_team"     => $session_team,
            "work_time"         => $product_item['work_time_default'],
            "set_tag"           => $set_tag
        );

        # dday 설정 상품들
        $task_req_dday = date("Y-m-d", strtotime("+7 day"));
        if (in_array($prd_no, $task_dday_prd_list) && in_array($prd_no, $contents_prd_option)){
            $work_insert_data["task_req_dday"] = $task_req_dday;
        }

        if ($task_run_staff_list_cnt == '1') {
            $work_insert_data['task_run_s_no'] = $work_item['task_run_s_no'];
            $work_insert_data['task_run_team'] = $work_item['task_run_team'];
        }

        if (!$work_model->insert($work_insert_data)) {
            exit("<script>alert('w_no = {$w_no} 업무 레코드를 복제에 실패하였습니다.\\n개발 담당자에게 문의해 주세요.');location.href='work_list.php?{$search_url}';</script>");
        } else {
            # 업무태그
            if(!empty($set_w_no))
            {
                $last_w_no = $work_model->getInsertId();
                if(!empty($last_w_no) && $last_w_no != $w_no)
                {
                    $priority   = $work_model->getSetMaxPriority($set_tag);
                    $work_model->update(array("w_no" => $last_w_no, "priority" => $priority));
                }
            }

            if (in_array($prd_no, $task_dday_prd_list) && in_array($prd_no, $contents_prd_option))
            {
                echo "<script>alert('희망 완료일이 7일후인 {$task_req_dday} 로 업무가 복제되었습니다.\\n원하는 완료일로 꼭 설정 바랍니다.');</script>";
                $task_req_dday_cnt = $work_model->getTaskReqDdayCount($prd_no, $task_req_dday);
                task_req_dday_count_alert($task_req_dday_cnt, $prd_no, $task_req_dday); // 상품별 희망완료일 최대수를 넘어설 경우 alert 처리
            }
        }
        exit("<script>location.href='work_list.php?{$search_url}';</script>");

    }
    elseif ($proc == "canceled_set_work") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $sch_set_w_no = (isset($_POST['sch_set_w_no'])) ? $_POST['sch_set_w_no'] : "";
        $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        $cur_priority_sql    = "SELECT w.priority FROM `work` as w WHERE w.w_no='{$w_no}' LIMIT 1";
        $cur_priority_query  = mysqli_query($my_db, $cur_priority_sql);
        $cur_priority_result = mysqli_fetch_assoc($cur_priority_query);
        $f_priority_new	     = isset($cur_priority_result['priority']) ? $cur_priority_result['priority'] : 1;

        $chk_priority_sql    = "SELECT w.w_no FROM `work` as w WHERE w.w_no != '{$w_no}' AND w.set_tag like '%#{$sch_set_w_no}%' AND w.priority > {$f_priority_new} ORDER BY w.priority ASC, w.w_no ASC";
        $chk_priority_query  = mysqli_query($my_db, $chk_priority_sql);
        $chk_priority_list   = [];
        while($chk_priority = mysqli_fetch_assoc($chk_priority_query))
        {
            $chk_priority_list[$chk_priority['w_no']] = $f_priority_new;
            $f_priority_new++;
        }

        $sql = "UPDATE `work` w SET w.set_tag = REPLACE(w.set_tag,'#{$sch_set_w_no}',''), w.priority=NULL WHERE w.w_no='{$w_no}'";

        if (!mysqli_query($my_db, $sql)) {
            exit("<script>alert('업무를 세트구성에서 제외하는데 실패 하였습니다');location.href='work_list.php?{$search_url}';</script>");
        }else{
            if(!empty($chk_priority_list)){
                foreach($chk_priority_list as $w_no_val => $priority){
                    $upd_priority_sql = "UPDATE `work` SET priority='{$priority}' WHERE w_no='{$w_no_val}'";
                    mysqli_query($my_db, $upd_priority_sql);
                }
            }
        }

        exit("<script>location.href='work_list.php?{$search_url}';</script>");

    } elseif ($proc == "remove_set_work") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $sch_set_w_no = (isset($_POST['sch_set_w_no'])) ? $_POST['sch_set_w_no'] : "";
        $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        $sql = "UPDATE `work` w SET w.set_tag = REPLACE(w.set_tag,'#{$sch_set_w_no}',''), w.priority=NULL WHERE w.set_tag like '%{$w_no}%'";

        if (!mysqli_query($my_db, $sql)) {
            exit("<script>alert('업무를 세트구성에서 제외하는데 실패 하였습니다');location.href='work_list.php?{$search_url}';</script>");
        } else {
            $sql2 = "DELETE FROM `work_set_info` WHERE w_no = '{$w_no}'";
            if (!mysqli_query($my_db, $sql2)) {
                exit("<script>alert('세트 정보 삭제에 실패 하였습니다');location.href='work_list.php';</script>");
            } else {
                exit ("<script>location.href='work_list.php';</script>");
            }
        }


    } elseif ($proc == "down_file") {
        $w_no = isset($_POST['w_no']) ? $_POST['w_no'] : "";
        $folder = isset($_POST['folder']) ? $_POST['folder'] : "";

        if ($folder == "out_task_req") {
            $sql = "SELECT task_req_file_origin, task_req_file_read FROM work w WHERE w_no = '{$w_no}'";
            $result = mysqli_query($my_db, $sql);
            $file_info = mysqli_fetch_array($result);

            $filename = htmlspecialchars($file_info['task_req_file_origin']);
            $string = $file_info['task_req_file_read'];
        } else if ($folder == "out_task_run") {
            $sql = "SELECT task_run_file_origin, task_run_file_read FROM work w WHERE w_no = '{$w_no}'";
            $result = mysqli_query($my_db, $sql);
            $file_info = mysqli_fetch_array($result);

            $filename = htmlspecialchars($file_info['task_run_file_origin']);
            $string = $file_info['task_run_file_read'];
        }

        echo "File UP Name : $string<br>";
        echo "File Downloading...<br>File DN Name : $filename<br>";
        exit("<script>location.href='popup/file_download.php?file_dn_name=" . urlencode($filename) . "&file_up_name=" . $string . "';target='_blank';</script>");

    } elseif ($proc == "del_file") {
        $w_no = isset($_POST['w_no']) ? $_POST['w_no'] : "";
        $folder = isset($_POST['folder']) ? $_POST['folder'] : "";
        $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        if ($folder == "out_task_req") {
            $sql = "SELECT task_req_file_origin, task_req_file_read FROM work w WHERE w_no = '{$w_no}'";
            $result = mysqli_query($my_db, $sql);
            $file_info = mysqli_fetch_array($result);

            $string = "uploads/" . $file_info['task_req_file_read'];
            $del_sql = "UPDATE work w SET task_req_file_origin = '', task_req_file_read = '' WHERE w_no = '{$w_no}'";
        } else if ($folder == "out_task_run") {
            $sql = "SELECT task_run_file_origin, task_run_file_read FROM work w WHERE w_no = '{$w_no}'";
            $result = mysqli_query($my_db, $sql);
            $file_info = mysqli_fetch_array($result);

            $string = "uploads/" . $file_info['task_run_file_read'];
            $del_sql = "UPDATE work w SET task_run_file_origin = '', task_run_file_read = '' WHERE w_no = '{$w_no}'";
        }

        unlink($string);
        $my_db->query($del_sql);

        exit("<script>alert('해당 파일을 삭제 하였습니다.');location.href='work_list.php?{$search_url}';</script>");

    }
    elseif ($proc == "save_extension") # 연장하기
    {
        $w_no           = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
        $f_extension    = isset($_POST['f_extension']) ? $_POST['f_extension'] : "";
        $prd_no         = (isset($_POST['f_prd_no'])) ? $_POST['f_prd_no'] : "";
        $work_upd_data  = array("w_no" => $w_no, "extension" => $f_extension);

        $work_model->update($work_upd_data);

        if ($f_extension == '1') # 연장시 복제하여 신규로 요청하기
        {
            $work_item      = $work_model->getItem($w_no);
            $company_item   = $company_model->getItem($work_item['c_no']);
            $product_item   = $product_model->getItem($work_item['prd_no']);

            $cur_date               = date("Y-m-d H:i:s");
            $task_req_s_no_value    = $session_s_no;
            $task_req_s_team_value  = $session_team;
            $work_state_value       = '3';
            if (permissionNameCheck($session_permission, "외주관리자")) { // 외주관리자가 연장시 업무요청자는 업체 담당자로 하고 접수완료로 처리함
                $task_req_s_no_value    = $company_item["s_no"];
                $task_req_s_team_value  = $company_item["team"];
                $work_state_value       = '4';
            }

            $work_insert_data = array(
                "regdate"           => $cur_date,
                "work_state"        => $work_state_value,
                "c_no"              => $work_item['c_no'],
                "c_name"            => $company_item['c_name'],
                "s_no"              => $company_item['s_no'],
                "team"              => $company_item['team'],
                "prd_no"            => $work_item['prd_no'],
                "t_keyword"         => addslashes($work_item['t_keyword']),
                "r_keyword"         => addslashes($work_item['r_keyword']),
                "wd_price"          => $work_item['wd_price'],
                "wd_price_vat"      => $work_item['wd_price_vat'],
                "wd_c_name"         => $work_item['wd_c_name'],
                "wd_c_no"           => $work_item['wd_c_no'],
                "extension_date"    => date("Y-m-d", strtotime("{$work_item['extension_date']} +1 months")),
                "task_req"          => addslashes("연장 ({$w_no})"),
                "task_req_s_no"     => $task_req_s_no_value,
                "task_req_team"     => $task_req_s_team_value,
                "work_time"         => $product_item['work_time_default'],
            );

            $task_run_staff_list     = explode(",", $product_item['task_run_staff']);
            $task_run_staff_list_cnt = count($task_run_staff_list);

            if ($task_run_staff_list_cnt == '1') {
                $work_insert_data['task_run_s_no'] = $work_item['task_run_s_no'];
                $work_insert_data['task_run_team'] = $work_item['task_run_team'];
            }

            if (!$work_model->insert($work_insert_data))
                exit("<script>alert('연장 요청에 실패 하였습니다');location.href='work_list.php?{$search_url}';</script>");
            else
                exit("<script>location.href='work_list.php?{$search_url}';</script>");
        }
        elseif ($f_extension == '3')
        {
            exit("<script>alert('w_no = {$w_no} 미연장 하였습니다');location.href='work_list.php?{$search_url}';</script>");
        }
    }
    elseif ($proc == "add_req_record") # 업무 추가
    {
        $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        $add_set = "";

        if (!empty($_POST['f_k_name_code_new'])) {
            $add_set .= "k_name_code = '" . $_POST['f_k_name_code_new'] . "',";
        }

        if (!empty($_POST['f_dp_c_no_new'])) {
            $add_set .= "dp_c_name = (SELECT c_name FROM company WHERE c_no='" . $_POST['f_dp_c_no_new'] . "'),
					dp_c_no = '" . $_POST['f_dp_c_no_new'] . "',";
        }

        if (!empty($_POST['f_wd_c_no_new'])) {
            $add_set .= "wd_c_name = (SELECT c_name FROM company WHERE c_no='" . $_POST['f_wd_c_no_new'] . "'),
					wd_c_no = '" . $_POST['f_wd_c_no_new'] . "',";
        }

        if (!empty($_POST['f_task_req_dday_new'])) {
            $add_set .= "task_req_dday = '" . $_POST['f_task_req_dday_new'] . "',";
        } else {
            $add_set .= "task_req_dday = NULL,";
        }

        if (!empty($_POST['f_selling_price_new'])) {
            $f_selling_price_new = str_replace(",", "", trim($_POST['f_selling_price_new'])); // 컴마 제거하기
            $add_set .= "selling_price = '" . $f_selling_price_new . "',";
        }

        if (!empty($_POST['f_service_apply_new'])) {
            $add_set .= "service_apply = '" . $_POST['f_service_apply_new'] . "',";
        }

        if (!empty($_POST['f_service_memo_new'])) {
            $add_set .= "service_memo = '" . $_POST['f_service_memo_new'] . "',";
        }

        if (!empty($_POST['f_dp_price_new'])) {
            $add_set .= "dp_price = '" . str_replace(",", "", trim($_POST['f_dp_price_new'])) . "',";
            $add_set .= "dp_price_vat = '" . getPriceVat($my_db, $_POST['f_dp_c_no_new'], str_replace(",", "", trim($_POST['f_dp_price_new'])), $_POST['f_prd_no_new']) . "',";
        }

        if (!empty($_POST['f_wd_price_new'])) {
            $add_set .= "wd_price = '" . str_replace(",", "", trim($_POST['f_wd_price_new'])) . "',";
            $add_set .= "wd_price_vat = '" . getPriceVat($my_db, $_POST['f_wd_c_no_new'], str_replace(",", "", trim($_POST['f_wd_price_new'])), $_POST['f_prd_no_new']) . "',";
        }

        # 멀티파일첨부
        if (!empty($_POST['f_task_req_file_read_new'])) {
            $add_set .= "task_req_file_read = '" . $_POST['f_task_req_file_read_new'] . "',";
        }

        if (!empty($_POST['f_task_req_file_origin_new'])) {
            $add_set .= "task_req_file_origin='" . addslashes($_POST['f_task_req_file_origin_new']) . "',";
        }

        # 서비스운영 업무추가 변경(210126), 예정완료일, 업무진행, 시간 체크 후 삽입
        if (!empty($_POST['f_task_run_dday_new'])) {
            $add_set .= "task_run_dday='{$_POST['f_task_run_dday_new']}',";
        }

        if (!empty($_POST['f_task_run_regdate_new'])) {
            $add_set .= "task_run_regdate='{$_POST['f_task_run_regdate_new']}',";
        }

        if (!empty($_POST['f_work_time_new'])) {
            $add_set .= "work_time='{$_POST['f_work_time_new']}',";
        } else {
            if (!empty($_POST['f_work_time_default'])) {
                $add_set .= "work_time = '" . $_POST['f_work_time_default'] . "',";
            }
        }

        if (!empty($_POST['f_task_run_new'])) {
            $add_set .= "task_run='" . addslashes($_POST['f_task_run_new']) . "',";
        }

        if ($_POST['f_work_state_new'] == '6' && empty($_POST['f_task_run_regdate_new'])) {
            $add_set .= "task_run_regdate=now(),";
        }

        $task_run_team_set = "task_run_team=(SELECT s.team	FROM staff s WHERE s.s_no='{$_POST['f_task_run_staff_new']}'),";
        $f_task_run_staff_team = !empty($_POST['f_task_run_staff_new']) ? $_POST['f_task_run_team_new'] : "";

        if ($f_task_run_staff_team) {
            $task_run_team_set = "task_run_team='{$f_task_run_staff_team}',";
        }

        if (!empty($_POST['f_linked_table_new'])) {
            $add_set .= "linked_table = '" . $_POST['f_linked_table_new'] . "',";
        }

        if (!empty($_POST['f_linked_no_new'])) {
            $add_set .= "linked_no = '" . $_POST['f_linked_no_new'] . "',";
        }

        if($_POST['f_prd_no_new'] == '280' && ($_POST['f_linked_table_new'] == 'company_influencer' && $_POST['f_linked_no_new'] > 0)) {
            $add_set .= "manager_memo = '인플루언서 no : {$_POST['f_linked_no_new']}',";
        }

        $work_regdate   = date("Y-m-d H:i:s");
        $company_item   = $company_model->getItem($_POST['f_c_no_new']);

        $ins_sql        = "
            INSERT INTO `work` SET
                work_state  = '{$_POST['f_work_state_new']}',
                c_no        = '{$_POST['f_c_no_new']}',
                c_name      = '{$company_item['c_name']}',
                s_no        = '{$company_item['s_no']}',
                team        = '{$company_item['team']}',
                prd_no      = '{$_POST['f_prd_no_new']}',
                quantity    = '".trim($_POST['f_quantity_new'])."',
                t_keyword   = '".trim($_POST['f_t_keyword_new'])."',
                r_keyword   = '".trim($_POST['f_r_keyword_new'])."',
                task_req        = '".addslashes($_POST['f_task_req_new'])."',
                task_req_s_no   = '{$_POST['f_task_req_s_no_new']}',
                task_req_team   = '{$_POST['f_task_req_team_new']}',
                task_run_s_no   = '{$_POST['f_task_run_staff_new']}',
                {$task_run_team_set}
                {$add_set}
                evaluation_memo = '".addslashes($_POST['f_evaluation_memo_new'])."',
                regdate = '{$work_regdate}'
        ";

        //연관업무
        $related_w_no_val = isset($_POST['related_w_no']) && !empty($_POST['related_w_no']) ? $_POST['related_w_no'] : "";

        $my_db->set_charset("utf8mb4");
        if (mysqli_query($my_db, $ins_sql))
        {
            // 업무 추가 시 GET 값을 치환하여 업무 요청 펼치기
            $search_url = str_replace("&new=&", "&new=Y&", $search_url);
            $search_url = str_replace("&new=N&", "&new=Y&", $search_url);

            /* Slack API START */
            $sql_query = mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
            $sql_result = mysqli_fetch_array($sql_query);
            $last_w_no = $sql_result[0];

            //연관업무
            if ($related_w_no_val)
            {
                $wr_chk_sql = "SELECT w_no FROM work_relation WHERE w_no='{$related_w_no_val}'";
                $wr_chk_query = mysqli_query($my_db, $wr_chk_sql);
                $wr_chk_result = mysqli_fetch_assoc($wr_chk_query);

                if (!isset($wr_chk_result['w_no']) && empty($wr_chk_result['w_no'])) {
                    $wr_parent_sql = "INSERT INTO work_relation(`w_no`) VALUES('{$related_w_no_val}')";
                    mysqli_query($my_db, $wr_parent_sql);
                }

                $wr_ins_sql = "INSERT INTO work_relation(`w_parent`, `w_no`) VALUES('{$related_w_no_val}', '{$last_w_no}')";
                mysqli_query($my_db, $wr_ins_sql);

                # 세트업무 관련 적용
                $work_data  = $work_model->getItem($related_w_no_val);
                if($work_data['set_tag'])
                {
                    $priority   = $work_model->getSetMaxPriority($work_data['set_tag']);
                    $work_model->update(array("w_no" => $last_w_no, "priority" => $priority, "set_tag" => $work_data['set_tag']));
                }

                $search_url = "sch=Y&sch_prd={$_POST['f_prd_no_new']}";
            }

            $slack_list = [];

            if (!!$_POST['f_task_run_staff_new']) {
                $slack_list[$_POST['f_task_run_staff_new']] = $slack_all_list[$_POST['f_task_run_staff_new']];
            } else { //관리자 전체 메세지
                if ((isset($_POST['f_prd_no_new']) && !empty($_POST['f_prd_no_new']))
                    && (isset($task_req_accepter_slack_list[$_POST['f_prd_no_new']]) && !empty($task_req_accepter_slack_list[$_POST['f_prd_no_new']]))
                ) {
                    $task_req_accepter = $task_req_accepter_slack_list[$_POST['f_prd_no_new']];

                    foreach ($task_req_accepter as $task_req_s_no) {
                        $slack_list[$task_req_s_no] = $slack_all_list[$task_req_s_no];
                    }
                }
            }

            if($_POST['f_prd_no_new'] == '280' && ($_POST['f_linked_table_new'] == 'company_influencer' && $_POST['f_linked_no_new'] > 0))
            {
                $inf_model      = Company::Factory();
                $inf_model->setMainInit("company_influencer", "ci_no");
                $inf_item       = $inf_model->getInfluencerItem($_POST['f_linked_no_new']);
                $upd_w_no_list  = !empty($inf_item["w_no_list"]) ? $inf_item["w_no_list"].",{$last_w_no}" : $last_w_no;

                $inf_model->update(array("ci_no" => $inf_item['ci_no'], "w_no_list" => $upd_w_no_list));
            }

            if (!!$slack_list) {
                $company_name = $_POST['f_c_name_new'];
                if ($_POST['f_t_keyword_new']) {
                    $company_name .= " {$_POST['f_t_keyword_new']}";
                }
                $prd_no = $_POST['f_prd_no_new'];
                $prd_name = isset($work_product_all_list[$_POST['f_prd_no_new']]) ? $work_product_all_list[$_POST['f_prd_no_new']] : "없음";
                $return = "https://work.wplanet.co.kr/v1/work_list.php?sch=Y&sch_w_no={$last_w_no}&sch_prd={$prd_no}";
                $title = "{$prd_name}";
                $message = "{$company_name} :: {$session_name} (업무요청)\r\n{$return}";
                $slack_type = 'request';

                foreach ($slack_list as $key => $slack_id) {
                    if (!!$slack_id) {
                        if (empty($vacation_staff_list) || (!empty($vacation_staff_list) && !in_array($key, $vacation_staff_list))) {
                            sendSlackMessageBlock($slack_id, $title, $message, $slack_type);
                        }
                    }
                }
            }
            /* Slack API END */

            /* 재직증명서 */
            if($_POST['f_work_extra'] && $last_w_no > 0)
            {
                $extra_linked_table = !empty($_POST['f_task_req_new']) ? $_POST['f_task_req_new'] : $product_model->getPrdWorkExtra($_POST['f_prd_no_new']);

                if(!empty($extra_linked_table))
                {
                    $extra_linked_no    = "";
                    $extra_linked_url   = "";

                    switch($extra_linked_table){
                        case "work_certificate":
                            $staff_model        = Staff::Factory();
                            $req_staff_data     = $staff_model->getStaff($_POST['f_task_req_s_no_new']);

                            $extra_model        = WorkCertificate::Factory();
                            $extra_linked_doc   = $extra_model->getNewDocNo();
                            $extra_data         = array(
                                "doc_no"        => $extra_linked_doc,
                                "work_state"    => '3',
                                "req_s_no"      => $req_staff_data['s_no'],
                                "req_team"      => $req_staff_data['team'],
                                "req_name"      => $req_staff_data['s_name'],
                                "cert_no"       => $req_staff_data['cert_no'],
                                "cert_hide"     => '1',
                                "req_position"  => $req_staff_data['position'],
                                "company_addr"  => $req_staff_data['my_c_addr'],
                                "company_ceo"   => '대 표 이 사 주경민',
                                "company_name"  => $req_staff_data['my_c_name'],
                                "w_no"          => $last_w_no,
                                "regdate"       => $work_regdate,
                                "req_date"      => $work_regdate
                            );

                            if($req_staff_data['staff_state'] == '1'){
                                $extra_data['work_s_date'] = $req_staff_data['employment_date'];
                                $extra_data['work_e_date'] = date('Y-m-d');
                            }else{
                                $extra_data['work_s_date'] = $req_staff_data['employment_date'];
                            }

                            if(!empty($_POST['f_task_run_staff_new'])){
                                $extra_data['run_s_no'] = $_POST['f_task_run_staff_new'];
                                $extra_data['run_team'] = "00211";
                            }

                            $extra_model->insert($extra_data);
                            $extra_linked_no    = $extra_model->getInsertId();

                            $extra_linked_url   = "work_certificate_view.php?wc_no={$extra_linked_no}";
                            break;
                        case "logistics_management":
                            $staff_model        = Staff::Factory();
                            $req_staff_data     = $staff_model->getStaff($_POST['f_task_req_s_no_new']);

                            $extra_model        = Logistics::Factory();
                            $extra_linked_doc   = $extra_model->getNewDocNo($_POST['f_prd_no_new']);
                            $extra_data         = array(
                                "doc_no"        => $extra_linked_doc,
                                "prd_no"        => $_POST['f_prd_no_new'],
                                "work_state"    => '3',
                                "req_s_no"      => $req_staff_data['s_no'],
                                "req_team"      => $req_staff_data['team'],
                                "w_no"          => $last_w_no,
                                "regdate"       => $work_regdate,
                                "req_date"      => $work_regdate
                            );

                            if(!empty($_POST['f_task_run_staff_new'])){
                                $run_staff_data         = $staff_model->getStaff($_POST['f_task_run_staff_new']);
                                $extra_data['run_s_no'] = $run_staff_data['s_no'];
                                $extra_data['run_team'] = $run_staff_data['team'];
                            }else{
                                if($_POST['f_prd_no_new'] == '251'){
                                    $extra_data['run_s_no'] = "61";
                                    $extra_data['run_team'] = "00221";
                                }
                            }

                            if($_POST['f_prd_no_new'] == '251'){
                                $extra_data['sender_type']          = '1113';
                                $extra_data['sender_type_name']     = '와이즈미디어커머스';
                                $extra_data['sender']               = '김진주';
                                $extra_data['sender_hp']            = '010-9990-8389';
                                $extra_data['sender_zipcode']       = '08513';
                                $extra_data['sender_addr']          = '서울 금천구 벚꽃로 244 (가산동, 벽산디지털밸리5차)';
                                $extra_data['sender_addr_detail']   = '902호';
                            }

                            $extra_model->insert($extra_data);
                            $extra_linked_no    = $extra_model->getInsertId();

                            $extra_linked_url   = "logistics_request.php?lm_no={$extra_linked_no}&prev_type=work";
                            break;
                    }


                    if($extra_linked_no){
                        $work_model->update(array("linked_table" => $extra_linked_table, "linked_no" => $extra_linked_no, "w_no" => $last_w_no));
                        exit("<script>alert('업무 리스트를 추가 하였습니다');location.href='{$extra_linked_url}';</script>");
                    }
                }
            }

            exit("<script>alert('업무 리스트를 추가 하였습니다');location.href='work_list.php?{$search_url}';</script>");

        } else
            exit("<script>alert('업무 리스트를 추가에 실패 하였습니다');location.href='work_list.php?{$search_url}';</script>");

    }
    elseif ($proc == "add_set_record") # 세트 추가
    {
        $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
        $add_set    = "";

        if (!empty($_POST['f_k_name_code_new'])) {
            $add_set .= "k_name_code = '" . $_POST['f_k_name_code_new'] . "',";
        }

        if (!empty($_POST['f_wd_c_no_new'])) {
            $add_set .= "wd_c_name = (SELECT c_name FROM company WHERE c_no='" . $_POST['f_wd_c_no_new'] . "'),
					wd_c_no = '" . $_POST['f_wd_c_no_new'] . "',";
        }

        if (!empty($_POST['f_task_req_dday_new'])) {
            $add_set .= "task_req_dday = '" . $_POST['f_task_req_dday_new'] . "',";
        } else {
            $add_set .= "task_req_dday = NULL,";
        }

        if (!empty($_POST['f_work_time_default'])) {
            $add_set .= "work_time = '" . $_POST['f_work_time_default'] . "',";
        }

        if (!empty($_POST['f_dp_price_new'])) {
            $add_set .= "dp_price = '" . str_replace(",", "", trim($_POST['f_dp_price_new'])) . "',";
            $add_set .= "dp_price_vat = '" . getPriceVat($my_db, $_POST['f_dp_c_no_new'], str_replace(",", "", trim($_POST['f_dp_price_new'])), $_POST['f_prd_no_new']) . "',";
        }

        if (!empty($_POST['f_wd_price_new'])) {
            $add_set .= "wd_price = '" . str_replace(",", "", trim($_POST['f_wd_price_new'])) . "',";
            $add_set .= "wd_price_vat = '" . getPriceVat($my_db, $_POST['f_wd_c_no_new'], str_replace(",", "", trim($_POST['f_wd_price_new'])), $_POST['f_prd_no_new']) . "',";
        }

        $sql = "INSERT into work set
					work_state = '" . $_POST['f_work_state_new'] . "',
					c_no = '" . $_POST['f_c_no_new'] . "',
					c_name = '" . $_POST['f_c_name_new'] . "',
					s_no = '" . $_POST['f_s_no_new'] . "',
					team = (SELECT s.team	FROM staff s WHERE s_no = '" . $_POST['f_s_no_new'] . "'),
					prd_no = '" . $_POST['f_prd_no_new'] . "',
					t_keyword = '" . trim($_POST['f_t_keyword_new']) . "',
					r_keyword = '" . trim($_POST['f_r_keyword_new']) . "',
					task_req = '" . addslashes($_POST['f_task_req_new']) . "',
					task_req_s_no = '" . $_POST['f_task_req_s_no_new'] . "',
					task_req_team = (SELECT s.team	FROM staff s WHERE s_no = '" . $_POST['f_task_req_s_no_new'] . "'),
					task_run_s_no = '" . $_POST['f_task_run_staff_new'] . "',
					task_run_team = (SELECT s.team	FROM staff s WHERE s.s_no = '" . $_POST['f_task_run_staff_new'] . "'),
					" . $add_set . "
					evaluation_memo = '" . addslashes($_POST['f_evaluation_memo_new']) . "',
					regdate = '" . date("Y-m-d H:i:s") . "'
				";
        //echo $sql; exit;


        if (mysqli_query($my_db, $sql)) {

            // 업무 추가 시 GET 값을 치환하여 업무 요청 펼치기
            $search_url = str_replace("&new=&", "&new=Y&", $search_url);
            $search_url = str_replace("&new=N&", "&new=Y&", $search_url);

            $sql_query = mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
            $sql_result = mysqli_fetch_array($sql_query);
            $last_w_no = $sql_result[0];

            $sql = "INSERT INTO work_set_info SET
							w_no = '{$last_w_no}',
							title = '{$_POST['f_set_title_new']}',
							regdate = now()
					";
            //echo "<br>";echo $sql; //exit;
            if (!mysqli_query($my_db, $sql)) {
                exit("<script>alert('work_set_info 저장에 실패 하였습니다.');history.back();</script>");
            } else {
                $sql2 = "UPDATE work w SET w.set_tag = '#{$last_w_no}' WHERE w.w_no = '" . $last_w_no . "'";

                //echo "<br>";echo $sql2; exit;
                if (!mysqli_query($my_db, $sql2)) {
                    exit("<script>alert('업무 세트 추가에 실패 하였습니다.');history.back();</script>");
                } else {
                    exit("<script>history.back();</script>");
                }
            }
            exit;

        } else
            exit("<script>alert('업무 세트 추가에 실패 하였습니다.');history.back();</script>");

    }
    elseif ($proc == "add_req_record_set") # 선택상품 세트에 업무추가
    {
        $set_w_no       = isset($_POST['set_w_no']) ? $_POST['set_w_no'] : "";
        $add_set        = "";
        $task_req       = "";

        if (!empty($_POST['sch_prd'])) {
            $add_set        .= "prd_no = '{$_POST['sch_prd']}', ";
            $product_item    = $product_model->getItem($_POST['sch_prd']);
            $task_req        = addslashes($product_item['work_format']);
            $task_run_staffs = $product_item['task_run_staff_default'];

            if(!empty($task_run_staffs))
            {
                $task_run_staff_info = explode("_", $task_run_staffs);
                $add_set        .= "task_run_s_no = '{$task_run_staff_info[0]}', task_run_team = '{$task_run_staff_info[1]}', ";
            }
        }

        $set_tag        = "#".$set_w_no;
        $priority       = $work_model->getSetMaxPriority($set_tag);
        $work_item      = $work_model->getItem($set_w_no);
        $company_item   = $company_model->getItem($work_item['c_no']);

        $ins_sql = "
            INSERT INTO `work` SET
                work_state      = '1',
                c_no            = '{$work_item['c_no']}',
                c_name          = '{$company_item['c_name']}',
                s_no            = '{$company_item['s_no']}',
                team            = '{$company_item['team']}',
                task_req        = '{$task_req}',
                task_req_s_no   = '{$session_s_no}',
                task_req_team   = '{$session_team}',
                {$add_set}
                regdate         = now(),
                priority        = '{$priority}',
                set_tag         = '#{$set_w_no}'
        ";

        if (mysqli_query($my_db, $ins_sql)) {
            exit("<script>alert('연관업무를 추가 하였습니다');location.href='work_list.php?sch_set_w_no={$set_w_no}';</script>");
        } else
            exit("<script>alert('연관업무 추가에 실패 하였습니다');location.href='work_list.php?sch_set_w_no={$set_w_no}';</script>");

    }
    elseif ($proc == "add_related_prd_set") # 즐겨찾기 상품 세트에 업무추가
    {
        $set_w_no       = isset($_POST['set_w_no']) ? $_POST['set_w_no'] : "";
        $add_set        = "";
        $task_req       = "";

        if (!empty($_POST['set_related_prd'])) {
            $add_set        .= "prd_no = '{$_POST['set_related_prd']}', ";
            $product_item    = $product_model->getItem($_POST['set_related_prd']);
            $task_req        = addslashes($product_item['work_format']);
            $task_run_staffs = $product_item['task_run_staff_default'];

            if(!empty($task_run_staffs))
            {
                $task_run_staff_info = explode("_", $task_run_staffs);
                $add_set        .= "task_run_s_no = '{$task_run_staff_info[0]}', task_run_team = '{$task_run_staff_info[1]}', ";
            }
        }

        $set_tag        = "#".$set_w_no;
        $priority       = $work_model->getSetMaxPriority($set_tag);
        $work_item      = $work_model->getItem($set_w_no);
        $company_item   = $company_model->getItem($work_item['c_no']);

        $ins_sql = "INSERT INTO `work` SET
					work_state      = '1',
					c_no            = '{$work_item['c_no']}',
					c_name          = '{$company_item['c_name']}',
					s_no            = '{$company_item['s_no']}',
					team            = '{$company_item['team']}',
					task_req        = '{$task_req}',
					task_req_s_no   = '{$session_s_no}',
					task_req_team   = '{$session_team}',
					{$add_set}
					regdate         = now(),
					priority        = '{$priority}',
					set_tag         = '#{$set_w_no}'
        ";

        if (mysqli_query($my_db, $ins_sql)) {
            exit("<script>alert('연관업무를 추가 하였습니다');location.href='work_list.php?sch_set_w_no={$set_w_no}';</script>");
        } else
            exit("<script>alert('연관업무 추가에 실패 하였습니다');location.href='work_list.php?sch_set_w_no={$set_w_no}';</script>");

    }
    elseif ($proc == "f_work_state_save_4")
    {
        $w_no           = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        $work_model->update(array("w_no" => $w_no, "work_state" => "4"));

        # Extra 연동
        $work_data       = $work_model->getExtraItem($w_no);
        $is_work_extra   = ($work_data['prd_work_type'] == '2' && !empty($work_data['prd_work_extra'])) ? true : false;
        $linked_no       = (isset($work_data['linked_no']) && !empty($work_data['linked_no'])) ? $work_data['linked_no'] : "";
        $linked_table    = (isset($work_data['linked_table']) && !empty($work_data['linked_table'])) ? $work_data['linked_table'] : "";

        if($is_work_extra && (!empty($linked_no) && !empty($linked_table)))
        {
            switch ($linked_table) {
                case "work_certificate":
                    $extra_model = WorkCertificate::Factory();
                    $extra_model->update(array("wc_no" => $linked_no, "work_state" => "4"));
                    break;
                case "logistics_management":
                    $extra_model = Logistics::Factory();
                    $extra_model->update(array("lm_no" => $linked_no, "work_state" => "4"));
                    break;
            }
        }

        exit("<script>location.href='work_list.php?{$search_url}';</script>");

    }
    elseif ($proc == "f_work_state_save_6")
    {
        $w_no               = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $f_task_run_regdate = date("Y-m-d H:i:s");
        $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        $work_upd_data  = array("w_no" => $w_no, "work_state" => "6", "task_run_regdate" => $f_task_run_regdate);
        $work_data      = $work_model->getExtraItem($w_no);

        if ($work_model->update($work_upd_data))
        {
            if (!empty($w_no))
            {
                $is_work_extra  = ($work_data['prd_work_type'] == '2' && !empty($work_data['prd_work_extra'])) ? true : false;
                $linked_no      = (isset($work_data['linked_no']) && !empty($work_data['linked_no'])) ? $work_data['linked_no'] : "";
                $linked_table   = (isset($work_data['linked_table']) && !empty($work_data['linked_table'])) ? $work_data['linked_table'] : "";
                if(!empty($linked_no) && !empty($linked_table))
                {
                    switch($linked_table)
                    {
                        case 'work_certificate':
                            if($is_work_extra){
                                $extra_model = WorkCertificate::Factory();
                                $extra_model->update(array("wc_no" => $linked_no, "work_state" => '6', "run_date" => date("Y-m-d H:i:s")));
                            }
                            break;
                        case 'logistics_management':
                            if($is_work_extra){
                                $extra_model    = Logistics::Factory();
                                $extra_upd_data = array("lm_no" => $linked_no, "work_state" => '6', "run_date" => date("Y-m-d H:i:s"));

                                if(!empty($work_data['task_run_s_no'])){
                                    $extra_upd_data['run_s_no'] = $work_data['task_run_s_no'];
                                    $extra_upd_data['run_team'] = $work_data['task_run_team'];
                                }
                                $extra_model->update($extra_upd_data);
                            }
                            break;
                    }
                }

                #SLACK API
                if ($work_data && $work_data['task_run_s_no']) {
                    $company_name = $work_data['c_name'];
                    if (isset($work_data['t_keyword']) && !empty($work_data['t_keyword'])) {
                        $company_name .= " {$work_data['t_keyword']}";
                    }
                    $prd_no = $work_data['prd_no'];
                    $prd_name = $work_data['prd_name'];
                    $s_name = isset($staff_all_list[$work_data['task_run_s_no']]) ? $staff_all_list[$work_data['task_run_s_no']]['s_name'] : "없음";
                    $return = "https://work.wplanet.co.kr/v1/work_list.php?sch=Y&sch_w_no={$w_no}&sch_prd={$prd_no}";
                    $title = "{$prd_name}";
                    $message = "{$company_name} :: {$s_name} (업무완료)\r\n{$return}";
                    $channel = $slack_all_list[$work_data['task_req_s_no']];
                    $slack_type = 'completed';

                    if (empty($vacation_staff_list) || (!empty($vacation_staff_list) && !in_array($work_data['task_req_s_no'], $vacation_staff_list))) {
                        sendSlackMessageBlock($channel, $title, $message, $slack_type);
                    }
                }
            }
        }

        exit("<script>location.href='work_list.php?{$search_url}';</script>");

    } elseif ($proc == "f_w_set_title") { // 업무세트 제목 저장하기

        $set_w_no = (isset($_POST['set_w_no'])) ? $_POST['set_w_no'] : "";
        $value = (isset($_POST['f_w_set_title'])) ? $_POST['f_w_set_title'] : "";

        $sql = "UPDATE work_set_info w_s_i SET w_s_i.title = '" . addslashes($value) . "' WHERE w_s_i.w_no = '" . $set_w_no . "'";
        //echo $sql; exit;

        if (!mysqli_query($my_db, $sql))
            exit("<script>alert('업무세트 TITLE 저장에 실패 하였습니다');history.back();</script>");
        else
            exit("<script>alert('업무세트 TITLE이 저장 되었습니다');history.back();</script>");

    } elseif ($proc == "f_w_set_memo") { // 업무세트 메모 저장하기

        $set_w_no = (isset($_POST['set_w_no'])) ? $_POST['set_w_no'] : "";
        $value = (isset($_POST['f_w_set_memo'])) ? $_POST['f_w_set_memo'] : "";

        $sql = "UPDATE work_set_info w_s_i SET w_s_i.memo = '" . addslashes($value) . "' WHERE w_s_i.w_no = '" . $set_w_no . "'";

        if (!mysqli_query($my_db, $sql))
            exit("<script>alert('업무세트 MEMO 저장에 실패 하였습니다');history.back();</script>");
        else
            exit("<script>alert('업무세트 MEMO 저장 되었습니다');history.back();</script>");

    } elseif ($proc == "add_set_unit") { // 업무세트 구성 추가하기

        $set_w_no = (isset($_POST['set_w_no'])) ? $_POST['set_w_no'] : "";
        $wsu_no = (isset($_POST['wsu_no'])) ? $_POST['wsu_no'] : "";

        $comma = '';
        $sql = " INSERT INTO `work`
	            (work_state, c_no, c_name, s_no, team, prd_no, priority, regdate, task_req, task_req_s_no, task_req_team,  set_tag)
	          VALUES ";

        $work_set_unit_relation_sql = "SELECT
																		wsu_r.wsu_no,
																		(SELECT c_no FROM work WHERE w_no = '{$set_w_no}') AS c_no,
																		(SELECT c_name FROM work WHERE w_no = '{$set_w_no}') AS c_name,
																		(SELECT s_no FROM work WHERE w_no = '{$set_w_no}') AS s_no,
																		(SELECT team FROM work WHERE w_no = '{$set_w_no}') AS team,
																		wsu_r.prd_no,
																		wsu_r.priority,
																		(SELECT work_format FROM product WHERE prd_no = wsu_r.prd_no) AS work_format
																	FROM work_set_unit_relation wsu_r
																	WHERE wsu_r.wsu_no='{$wsu_no}'
																";
        $work_set_unit_relation_query = mysqli_query($my_db, $work_set_unit_relation_sql);
        while ($wsu_r_data = mysqli_fetch_array($work_set_unit_relation_query)) {
            $sql .= $comma . " ( '1', '{$wsu_r_data['c_no']}', '{$wsu_r_data['c_name']}', '{$wsu_r_data['s_no']}', '{$wsu_r_data['team']}', '{$wsu_r_data['prd_no']}', '{$wsu_r_data['priority']}', now(), '{$wsu_r_data['work_format']}', '{$session_s_no}', '{$session_team}', '#{$set_w_no}' ) ";
            $comma = ' , ';
        }
        //echo $sql; exit;

        if (!mysqli_query($my_db, $sql))
            exit("<script>alert('업무세트 구성 저장에 실패 하였습니다');history.back();</script>");
        else
            exit("<script>alert('업무세트 구성이 모두 저장 되었습니다');history.back();</script>");

    }
    elseif ($proc == "create_wd_no")
    {

        $w_no           = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value          = (isset($_POST['val'])) ? $_POST['val'] : "";
        $f_prd_no       = (isset($_POST['f_prd_no'])) ? $_POST['f_prd_no'] : "";
        $f_wd_account   = (isset($_POST['f_wd_account'])) ? $_POST['f_wd_account'] : "";
        $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        $work           = $work_model->getItem($w_no);

        if (!!$work['wd_no']) {
            exit("<script>alert('이미 출금등록이 되어있어 처리가 불가능합니다.\\n다시 확인 후 문제가 있을 경우 개발담당자에게 문의 바랍니다.');location.href='work_list.php?{$search_url}';</script>");
        }

        $subject_concat = "";
        $f_t_keyword_POST = (isset($_POST['f_t_keyword'])) ? $_POST['f_t_keyword'] : "";
        if (!!$f_t_keyword_POST)
            $subject_concat = $f_t_keyword_POST;

        $wd_c_no        = $work['wd_c_no'];
        $prd_no         = !empty($f_prd_no) ? $f_prd_no : $work['prd_no'];

        if(empty($f_wd_account))
        {
            $product         = $product_model->getItem($prd_no);
            $product_account = !empty($product['wd_account']) ? explode(',', $product['wd_account']) : "";
            $f_wd_account    = isset($product_account[0]) ? $product_account[0] : "";
        }

        if ($work['wd_price_vat'] == 0 && $wd_c_no != '5') {
            $work_wd_price_vat = getPriceVat($my_db, $wd_c_no, str_replace(",", "", trim($work['wd_price'])), $f_prd_no);

            $upd_wd_price_vat = "UPDATE `work` SET wd_price_vat='{$work_wd_price_vat}' WHERE w_no='{$w_no}'";
            mysqli_query($my_db, $upd_wd_price_vat);

            $work_re_sql = "SELECT w.wd_no, w.wd_c_no, w.wd_price, w.wd_price_vat FROM work w WHERE w_no = '{$w_no}'";
            $work_result = mysqli_query($my_db, $work_re_sql);
            $work = mysqli_fetch_array($work_result);

            if (!!$work['wd_no']) {
                exit("<script>alert('이미 출금등록이 되어있어 처리가 불가능합니다.\\n다시 확인 후 문제가 있을 경우 개발담당자에게 문의 바랍니다.');location.href='work_list.php?{$search_url}';</script>");
            }
        }

        if($f_prd_no == '229')
        {
            $task_req_val = !empty($work['task_req']) ? explode("\r\n", $work['task_req']) : "";
            $bk_title_val = "";
            $bk_num_val   = "";
            $bk_name_val  = "";
            $wd_price_val = 0;

            if(is_array($task_req_val) && count($task_req_val) < 2){
                $task_req_val = !empty($work['task_req']) ? explode("\n", $work['task_req']) : "";
            }

            foreach($task_req_val as $task_req_data)
            {
                if(strpos($task_req_data, "은행 :") !== false){
                    $bk_title_val = str_replace("은행 : ", "", $task_req_data);
                }elseif(strpos($task_req_data, "계좌번호 :") !== false){
                    $bk_num_val = str_replace("계좌번호 : ", "", $task_req_data);
                }elseif(strpos($task_req_data, "예금주 :") !== false){
                    $bk_name_val = str_replace("예금주 : ", "", $task_req_data);
                }elseif(strpos($task_req_data, "금액 :") !== false){
                    $wd_price_val = str_replace("금액 : ", "", $task_req_data);
                }
            }

            if(empty($task_req_val))
            {
                exit("<script>alert('출금 데이터가 없습니다.');location.href='work_list.php?{$search_url}';</script>");
            }

            if(!$wd_price_val || $wd_price_val < 1)
            {
                exit("<script>alert('출금 금액이 이상합니다.');location.href='work_list.php?{$search_url}';</script>");
            }

            $work['wd_price']       = $wd_price_val;
            $wd_price_vat_value     = 3;
            $supply_cost_vat_value  = 0;
            $staff_model            = Staff::Factory();
            $task_req_staff         = $staff_model->getStaff($work['task_req_s_no']);
            $subject_concat         = "커머스 환불 (요청자 : {$task_req_staff['s_name']})";

            $cost_insert = "cost = '{$wd_price_val}',";
            $add_set_my_company = "my_c_no = (SELECT c.my_c_no FROM company c WHERE c.c_no = (SELECT w.c_no FROM work w WHERE w.w_no = '{$w_no}')), ";

            $add_set = "wd_state = '2', wd_method = '1', bk_title='{$bk_title_val}', bk_name='{$bk_name_val}', bk_num='{$bk_num_val}', wd_account='{$f_wd_account}',";
        }
        else
        {
            //부가세와 부가세액 자동계산 처리
            $wd_price_vat_value = 0;
            $supply_cost_vat_value = 0;
            if ($work['wd_price'] < $work['wd_price_vat']) { // 부가세가 크면 부가세 설정
                $wd_price_vat_value = 1;
                $supply_cost_vat_value = $work['wd_price_vat'] - $work['wd_price'];
            } elseif ($work['wd_price'] > $work['wd_price_vat']) { // 부가세가 적은 건은 소득세처리
                $wd_price_vat_value = 2;
                $supply_cost_vat_value = $work['wd_price'] - $work['wd_price_vat'];
            } elseif ($work['wd_price'] == $work['wd_price_vat']) { // 부가세가 동일하면 동일처리
                $wd_price_vat_value = 3;
                $supply_cost_vat_value = 0;
            }

            //내부 작업건의 경우 출금요청액(VAT 포함)을 0으로 처리
            if ($wd_c_no == '5') { // c_no = 5 와이즈플래닛
                $cost_insert = "cost = '0',";
                $supply_cost_vat_value = 0;
            } else {
                $cost_insert = "cost = '{$work['wd_price_vat']}',";
            }

            $add_set_my_company = "";
            // 블로그 리뷰, 카페 리뷰, 모바일 리뷰, 포스트 리뷰, 지식인 리뷰, 언론기사 의 경우 와이즈플래닛으로 처리
            if ($f_prd_no == '4' || $f_prd_no == '5' || $f_prd_no == '6' || $f_prd_no == '8' || $f_prd_no == '9' || $f_prd_no == '10') {
                $add_set_my_company = "my_c_no = '1', ";
            } else {
                $add_set_my_company = "my_c_no = (SELECT c.my_c_no FROM company c WHERE c.c_no = (SELECT w.c_no FROM work w WHERE w.w_no = '" . $w_no . "')), ";
            }

            $base_wd_method  = isset($total_wd_list[$f_prd_no][$wd_c_no]) ? $total_wd_list[$f_prd_no][$wd_c_no] : '1';
            $f_wd_method     = (isset($_POST['f_wd_method'])) ? $_POST['f_wd_method'] : "";
            $f_wd_method_val = !empty($f_wd_method) ? $f_wd_method : $base_wd_method;

            $add_set  = "";
            if($f_wd_method_val == "3") {
                $add_set  = "wd_state='3', wd_method='{$f_wd_method_val}', wd_date=now(),";
            }elseif($f_wd_method_val == "4") {
                $add_set  = "wd_state='3', wd_method='{$f_wd_method_val}', wd_date= now(), run_s_no='{$work['task_req_s_no']}', run_team='{$work['task_req_team']}', ";
            }else{
                $add_set  = "wd_state='2', wd_method='{$f_wd_method_val}', wd_account='{$f_wd_account}',";
            }

            $wd_c_no_sql   = "SELECT c_no, c_name, bk_title, bk_name, bk_num FROM company c WHERE c.c_no=(SELECT w.wd_c_no FROM work w WHERE w.w_no = '{$w_no}' LIMIT 1)";
            $wd_c_no_query = mysqli_query($my_db, $wd_c_no_sql);
            $wd_c_no_result = mysqli_fetch_assoc($wd_c_no_query);

            if(isset($wd_c_no_result['bk_title']) && !empty($wd_c_no_result['bk_title']))
            {
                $add_set .= "bk_title='{$wd_c_no_result['bk_title']}',";
            }

            if(isset($wd_c_no_result['bk_name']) && !empty($wd_c_no_result['bk_name']))
            {
                $add_set .= "bk_name='{$wd_c_no_result['bk_name']}',";
            }

            if(isset($wd_c_no_result['bk_num']) && !empty($wd_c_no_result['bk_num']))
            {
                $add_set .= "bk_num='{$wd_c_no_result['bk_num']}',";
            }
        }


        $sql = "
            INSERT INTO withdraw SET
                {$add_set_my_company}
                {$add_set}
                wd_subject  = '{$subject_concat}',
                w_no        = '{$w_no}',
                c_no        = (SELECT w.wd_c_no FROM work w WHERE w.w_no = '{$w_no}'),
                c_name      = (SELECT w.wd_c_name FROM work w WHERE w.w_no = '{$w_no}'),
                s_no        = (SELECT w.s_no FROM work w WHERE w.w_no = '{$w_no}'),
                team        = (SELECT w.team FROM work w WHERE w.w_no = '{$w_no}'),
                supply_cost     = '{$work['wd_price']}',
                vat_choice      = '{$wd_price_vat_value}',
                supply_cost_vat = '{$supply_cost_vat_value}',
                {$cost_insert}
                wd_tax_state    = '1',
                regdate     = now(),
                reg_s_no    = '{$session_s_no}',
                reg_team    = '{$session_team}'
		";

        if (!mysqli_query($my_db, $sql))
            exit("<script>alert('출금등록에 실패하였습니다.');location.href='work_list.php?{$search_url}';</script>");
        else {
            $withdraw_query     = mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
            $withdraw_result    = mysqli_fetch_array($withdraw_query);
            $last_wd_no         = $withdraw_result[0];
            $upd_work_data      = array("w_no" => $w_no, "wd_no" => $last_wd_no);

            if (!$work_model->update($upd_work_data))
                exit("<script>alert('출금번호 저장에 실패하였습니다.');location.href='work_list.php?{$search_url}';</script>");
            else
                exit("<script>location.href='work_list.php?{$search_url}';</script>");
        }
    }
    elseif ($proc == "create_mon_wd_no")  #월정산지급 추가
    {
        $w_no           = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value          = (isset($_POST['val'])) ? $_POST['val'] : "";
        $f_prd_no       = (isset($_POST['f_prd_no'])) ? $_POST['f_prd_no'] : "";
        $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        $work           = $work_model->getItem($w_no);

        if (!!$work['wd_no']) {
            exit("<script>alert('이미 출금등록이 되어있어 처리가 불가능합니다.\\n다시 확인 후 문제가 있을 경우 개발담당자에게 문의 바랍니다.');location.href='work_list.php?{$search_url}';</script>");
        }

        $subject_concat = "";
        $f_t_keyword_POST = (isset($_POST['f_t_keyword'])) ? $_POST['f_t_keyword'] : "";
        if (!!$f_t_keyword_POST)
            $subject_concat = $f_t_keyword_POST;

        $wd_c_no = $work['wd_c_no'];
        $prd_no  = !empty($f_prd_no) ? $f_prd_no : $work['prd_no'];

        //부가세와 부가세액 자동계산 처리
        $wd_price_vat_value = 0;
        $supply_cost_vat_value = 0;
        if ($work['wd_price'] < $work['wd_price_vat']) { // 부가세가 크면 부가세 설정
            $wd_price_vat_value = 1;
            $supply_cost_vat_value = $work['wd_price_vat'] - $work['wd_price'];
        } elseif ($work['wd_price'] > $work['wd_price_vat']) { // 부가세가 적은 건은 소득세처리
            $wd_price_vat_value = 2;
            $supply_cost_vat_value = $work['wd_price'] - $work['wd_price_vat'];
        } elseif ($work['wd_price'] == $work['wd_price_vat']) { // 부가세가 동일하면 동일처리
            $wd_price_vat_value = 3;
            $supply_cost_vat_value = 0;
        }

        //내부 작업건의 경우 출금요청액(VAT 포함)을 0으로 처리
        if ($wd_c_no == '5') { // c_no = 5 와이즈플래닛
            $cost_insert = "cost = '0',";
            $supply_cost_vat_value = 0;
        } else {
            $cost_insert = "cost = '" . $work['wd_price_vat'] . "',";
        }

        $add_set_my_company = "my_c_no = (SELECT c.my_c_no FROM company c WHERE c.c_no = (SELECT w.c_no FROM work w WHERE w.w_no = '{$w_no}')), ";

        $add_set = "wd_state = '3', wd_method = '4',";

        $wd_c_no_sql = "SELECT c_no, c_name, bk_title, bk_name, bk_num FROM company c WHERE c.c_no=(SELECT w.wd_c_no FROM work w WHERE w.w_no = '{$w_no}' LIMIT 1)";
        $wd_c_no_query = mysqli_query($my_db, $wd_c_no_sql);
        $wd_c_no_result = mysqli_fetch_assoc($wd_c_no_query);

        if(isset($wd_c_no_result['bk_title']) && !empty($wd_c_no_result['bk_title']))
        {
            $add_set .= "bk_title='{$wd_c_no_result['bk_title']}',";
        }

        if(isset($wd_c_no_result['bk_name']) && !empty($wd_c_no_result['bk_name']))
        {
            $add_set .= "bk_name='{$wd_c_no_result['bk_name']}',";
        }

        if(isset($wd_c_no_result['bk_num']) && !empty($wd_c_no_result['bk_num']))
        {
            $add_set .= "bk_num='{$wd_c_no_result['bk_num']}',";
        }

        $sql = "
			INSERT INTO withdraw SET
				{$add_set_my_company}
				{$add_set}
				wd_subject  = '{$subject_concat}',
				w_no        = '{$w_no}',
				c_no        = (SELECT w.wd_c_no FROM work w WHERE w.w_no = '{$w_no}'),
				c_name      = (SELECT w.wd_c_name FROM work w WHERE w.w_no = '{$w_no}'),
				s_no        = (SELECT w.s_no FROM work w WHERE w.w_no = '{$w_no}'),
				team        = (SELECT w.team FROM work w WHERE w.w_no = '{$w_no}'),
				supply_cost     = '{$work['wd_price']}',
				vat_choice      = '{$wd_price_vat_value}',
				supply_cost_vat = '{$supply_cost_vat_value}',
				{$cost_insert}
				wd_tax_state    = '1',
				regdate     = now(),
				reg_s_no    = '{$session_s_no}',
				reg_team    = '{$session_team}',
                wd_date     = now(),
                run_s_no    = '{$work['task_req_s_no']}',
				run_team    = '{$work['task_req_team']}'
		";

        if (!mysqli_query($my_db, $sql)) {
            exit("<script>alert('출금등록에 실패하였습니다.');location.href='work_list.php?{$search_url}';</script>");
        } else {
            $withdraw_query  = mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
            $withdraw_result = mysqli_fetch_array($withdraw_query);
            $last_wd_no      = $withdraw_result[0];
            $upd_work_data   = array("w_no" => $w_no, "wd_no" => $last_wd_no);

            if (!$work_model->update($upd_work_data))
                exit("<script>alert('출금번호 저장에 실패하였습니다.');location.href='work_list.php?{$search_url}';</script>");
            else
                exit("<script>location.href='work_list.php?{$search_url}';</script>");
        }

    }
    elseif ($proc == "del_wd_no")
    {
        $w_no       = isset($_POST['w_no']) ? $_POST['w_no'] : "";
        $wd_no      = isset($_POST['wd_no']) ? $_POST['wd_no'] : "";
        $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        if (empty($w_no) || empty($wd_no)) {
            exit("<script>alert('출금 삭제 처리가 안됩니다.\\n다시 확인 후 문제가 있을 경우 개발담당자에게 문의 바랍니다.');location.href='work_list.php?{$search_url}';</script>");
        }

        $with_upd_sql = "UPDATE `withdraw` SET wd_state='4' WHERE wd_no='{$wd_no}' AND w_no='{$w_no}'";

        if (mysqli_query($my_db, $with_upd_sql)) {
            $work_upd_sql = "UPDATE `work` SET wd_no = NULL WHERE w_no='{$w_no}'";
            if (!mysqli_query($my_db, $work_upd_sql)) {
                exit("<script>alert('출금 삭제중 업무 처리에 실패했습니다.\\n 개발담당자에게 문의 바랍니다.');location.href='work_list.php?{$search_url}';</script>");
            } else {
                exit("<script>alert('출금 취소 처리 되었습니다');location.href='work_list.php?{$search_url}';</script>");
            }
        } else {
            exit("<script>alert('출금 삭제 처리에 실패했습니다.\\n 개발담당자에게 문의 바랍니다.');location.href='work_list.php?{$search_url}';</script>");
        }

    }
    elseif ($proc == "create_dp_no")
    {
        $w_no           = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $value          = (isset($_POST['val'])) ? $_POST['val'] : "";
        $f_dp_account   = (isset($_POST['f_dp_account'])) ? $_POST['f_dp_account'] : "";
        $f_dp_method    = (isset($_POST['f_dp_method'])) ? $_POST['f_dp_method'] : "2";
        $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        $work           = $work_model->getItem($w_no);

        if (!!$work['dp_no']) {
            exit("<script>alert('이미 입금등록이 되어있어 처리가 불가능합니다.\\n다시 확인 후 문제가 있을 경우 개발담당자에게 문의 바랍니다.');location.href='work_list.php?{$search_url}';</script>");
        }

        $dp_c_no        = $work['dp_c_no'];
        $prd_no         = $work['prd_no'];

        if(empty($f_dp_account))
        {
            $product         = $product_model->getItem($prd_no);
            $product_account = !empty($product['dp_account']) ? explode(',', $product['dp_account']) : "";
            $f_dp_account    = isset($product_account[0]) ? $product_account[0] : "";

            if(empty($f_dp_account)){
                $f_dp_account = ($work['my_c_no'] == '3') ? '4' : '1';
            }
        }

        //부가세와 부가세액 자동계산 처리
        $dp_price_vat_value = 0;
        $supply_cost_vat_value = 0;
        if ($work['dp_price'] < $work['dp_price_vat']) { // 부가세가 크면 부가세 설정
            $dp_price_vat_value = 1;
            $supply_cost_vat_value = $work['dp_price_vat'] - $work['dp_price'];
        } elseif ($work['dp_price'] > $work['dp_price_vat']) { // 부가세가 적은 건은 소득세처리
            $dp_price_vat_value = 2;
            $supply_cost_vat_value = $work['dp_price'] - $work['dp_price_vat'];
        } elseif ($work['dp_price'] == $work['dp_price_vat']) { // 부가세가 동일하면 동일처리
            $dp_price_vat_value = 3;
            $supply_cost_vat_value = 0;
        }

        $f_dp_state     = 1;
        $f_dp_tax_state = 1;
        $add_dp_month   = "";
        if($f_dp_method == '3'){
            $f_dp_state     = 2;
            $f_dp_tax_state = 4;
            $add_dp_month   = ", dp_date=now()";
        }

        $sql = "
            INSERT INTO deposit SET
                my_c_no     = (SELECT c.my_c_no FROM company c WHERE c.c_no = (SELECT w.dp_c_no FROM work w WHERE w.w_no = '{$w_no}')),
                dp_subject  = (SELECT w.c_name FROM work w WHERE w.w_no = '{$w_no}'),
                dp_state    = '{$f_dp_state}',
                dp_method   = '{$f_dp_method}',
                dp_account  = '{$f_dp_account}',
                w_no        = '{$w_no}',
                c_no        = (SELECT w.dp_c_no FROM work w WHERE w.w_no = '{$w_no}'),
                c_name      = (SELECT w.dp_c_name FROM work w WHERE w.w_no = '{$w_no}'),
                s_no        = (SELECT `c`.s_no FROM company `c` WHERE `c`.c_no = (SELECT w.dp_c_no FROM work w WHERE w.w_no = '{$w_no}')),
                team        = (SELECT s.team FROM staff s WHERE s.s_no = (SELECT `c`.s_no FROM company `c` WHERE `c`.c_no = (SELECT w.dp_c_no FROM work w WHERE w.w_no = '{$w_no}'))),
                supply_cost     = '{$work['dp_price']}',
                vat_choice      = '{$dp_price_vat_value}',
                supply_cost_vat = '{$supply_cost_vat_value}',
                cost            = '{$work['dp_price_vat']}',
                dp_tax_state    = '{$f_dp_tax_state}',
                regdate     = now(),
                reg_s_no    = '{$session_s_no}',
                reg_team    = '{$session_team}'
                {$add_dp_month}
        ";

        if (!mysqli_query($my_db, $sql)) {
            exit("<script>alert('입금등록에 실패하였습니다.');location.href='work_list.php?{$search_url}';</script>");
        } else {
            $deposit_query  = mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
            $deposit_result = mysqli_fetch_array($deposit_query);
            $last_dp_no     = $deposit_result[0];
            $upd_work_data  = array("w_no" => $w_no, "dp_no" => $last_dp_no);

            if (!$work_model->update($upd_work_data))
                exit("<script>alert('입금번호 저장에 실패하였습니다.');location.href='work_list.php?{$search_url}';</script>");
            else
                exit("<script>location.href='work_list.php?{$search_url}';</script>");
        }

    }
    elseif ($proc == "create_work_set") { // 업무 세트 만들기
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";

        $work_set_info_sql = "SELECT w_s_i.w_no
		 					FROM work_set_info w_s_i WHERE w_s_i.w_no = '" . $w_no . "'";

        $work_set_info_result = mysqli_query($my_db, $work_set_info_sql);
        $work_set_info = mysqli_fetch_array($work_set_info_result);

        if ($w_no == $work_set_info['w_no']) {
            exit("<script>alert('#{$w_no}로 업무 세트가 이미 존재하여 만들 수 없습니다.');history.back();</script>");
        } else {
            $sql = "INSERT INTO work_set_info SET
								w_no = '{$w_no}',
								title = '#{$w_no}의 업무세트',
								regdate = now()
						";
            //echo "<br>";echo $sql; //exit;
            if (!mysqli_query($my_db, $sql)) {
                exit("<script>alert('work_set_info 저장에 실패 하였습니다.');history.back();</script>");
            } else {
                $sql2 = "UPDATE work w SET w.set_tag = IF(ISNULL(w.set_tag), '#{$w_no}', CONCAT(w.set_tag,'#{$w_no}')) WHERE w.w_no = '" . $w_no . "'";

                //echo "<br>";echo $sql2; exit;
                if (!mysqli_query($my_db, $sql2)) {
                    exit("<script>alert('work table에 set_tag 저장에 실패 하였습니다.');history.back();</script>");
                } else {
                    exit("<script>location.href='work_list.php?sch_set_w_no=$w_no';</script>");
                }
            }
        }
        exit;

    } elseif ($proc == "save_dp_no") {
        $w_no = (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
        $dp_no = (isset($_POST['dp_no'])) ? $_POST['dp_no'] : "";

        $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        $sql = "UPDATE `work` w SET
							w.dp_no = '{$dp_no}'
						WHERE
							w.w_no='" . $w_no . "'
						";

        mysqli_query($my_db, $sql);

        exit("<script>location.href='work_list.php?{$search_url}';</script>");
    } elseif ($proc == "mod_work_prd") { //업무상품변경
        $mod_w_no = (isset($_POST['mod_w_no'])) ? $_POST['mod_w_no'] : "";
        $mod_prd = (isset($_POST['mod_prd'])) ? $_POST['mod_prd'] : "";
        $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

        $sql = "UPDATE `work` w SET w.prd_no='{$mod_prd}' WHERE w.w_no='{$mod_w_no}'";

        if (!mysqli_query($my_db, $sql)) {
            exit("<script>alert('업무변경에 실패했습니다.');location.href='work_list.php?{$search_url}';</script>");
        } else {
            exit("<script>alert('업무변경에 성공했습니다.');location.href='work_list.php?{$search_url}';</script>");
        }

    } else {

        // 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
        $add_where          = "1=1";
        $set_get            = isset($_GET['sch_set']) ? $_GET['sch_set'] : "";
        $sch_set_w_no_get   = isset($_GET['sch_set_w_no']) ? $_GET['sch_set_w_no'] : "";
        $q_sch_get          = isset($_GET['q_sch']) ? $_GET['q_sch'] : "";
        $sch_get            = isset($_GET['sch']) ? $_GET['sch'] : "";

        # 브랜드 검색
        $kind_model                 = Kind::Factory();
        $team_model                 = Team::Factory();
        $brand_company_total_list   = $kind_model->getBrandCompanyList();
        $brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
        $brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
        $brand_list                 = $brand_company_total_list['brand_dom_list'];
        $brand_total_list           = $brand_company_total_list['brand_total_list'];
        $brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
        $brand_parent_list          = $brand_company_total_list['brand_parent_list'];
        $sch_team_list              = $team_model->getActiveTeamList();
        $sch_brand_g2_list          = [];
        $sch_brand_list             = [];

        # 브랜드 검색
        $sch_brand_g1               = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
        $sch_brand_g2               = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
        $sch_brand                  = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

        # 브랜드 parent 매칭
        if(!empty($sch_brand)) {
            $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
            $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
        }
        elseif(!empty($sch_brand_g2)) {
            $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
        }

        if(!empty($sch_brand)) {
            $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
            $sch_brand_list     = $brand_list[$sch_brand_g2];

            $add_where      .= " AND `w`.c_no = '{$sch_brand}'";
        }
        elseif(!empty($sch_brand_g2)) {
            $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
            $sch_brand_list     = $brand_list[$sch_brand_g2];

            $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
        }
        elseif(!empty($sch_brand_g1)) {
            $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

            $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
        }

        $smarty->assign("sch_brand_g1", $sch_brand_g1);
        $smarty->assign("sch_brand_g2", $sch_brand_g2);
        $smarty->assign("sch_brand", $sch_brand);
        $smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
        $smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
        $smarty->assign("sch_brand_list", $sch_brand_list);
        $smarty->assign("sch_team_list", $sch_team_list);

        if (!$sch_set_w_no_get) // 업무 세트 리스트는 예외
            $new_get = isset($_GET['new']) ? $_GET['new'] : "";

        $out_memo_get   = isset($_GET['out_memo']) ? $_GET['out_memo'] : "";
        $sch_prd_g1_get = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
        $sch_prd_g2_get = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
        $sch_prd_get    = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

        $sch_reg_s_date                 = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : "";
        $sch_reg_e_date                 = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : "";
        $sch_task_run_s_date            = isset($_GET['sch_task_run_s_date']) ? $_GET['sch_task_run_s_date'] : "";
        $sch_task_run_e_date            = isset($_GET['sch_task_run_e_date']) ? $_GET['sch_task_run_e_date'] : "";
        $sch_work_state_get             = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
        $sch_wd_dp_no_state_get         = isset($_GET['sch_wd_dp_no_state']) ? $_GET['sch_wd_dp_no_state'] : "";
        $sch_s_name_get                 = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
        $sch_req_team                   = isset($_GET['sch_req_team']) ? $_GET['sch_req_team'] : "";
        $sch_req_s_name_get             = isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : "";
        $sch_service_apply_get          = isset($_GET['sch_service_apply']) ? $_GET['sch_service_apply'] : "";
        $sch_run_team                   = isset($_GET['sch_run_team']) ? $_GET['sch_run_team'] : "";
        $sch_run_s_name_get             = isset($_GET['sch_run_s_name']) ? $_GET['sch_run_s_name'] : "";
        $sch_run_direct_get             = isset($_GET['sch_run_direct']) ? $_GET['sch_run_direct'] : "";
        $sch_work_time_s_get            = isset($_GET['sch_work_time_s']) ? $_GET['sch_work_time_s'] : "";
        $sch_work_time_e_get            = isset($_GET['sch_work_time_e']) ? $_GET['sch_work_time_e'] : "";
        $sch_dp_c_no_get                = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
        $sch_wd_c_no_get                = isset($_GET['sch_wd_c_no']) ? $_GET['sch_wd_c_no'] : "";
        $sch_extension_get              = isset($_GET['sch_extension']) ? $_GET['sch_extension'] : "";
        $sch_extension_date_get         = isset($_GET['sch_extension_date']) ? $_GET['sch_extension_date'] : "";
        $sch_req_evaluation_get         = isset($_GET['sch_req_evaluation']) ? $_GET['sch_req_evaluation'] : "";
        $sch_req_evaluation_memo_get    = isset($_GET['sch_req_evaluation_memo']) ? $_GET['sch_req_evaluation_memo'] : "";
        $sch_evaluation_get             = isset($_GET['sch_evaluation']) ? $_GET['sch_evaluation'] : "";
        $sch_evaluation_memo_get        = isset($_GET['sch_evaluation_memo']) ? $_GET['sch_evaluation_memo'] : "";
        $sch_c_name_get                 = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
        $sch_t_keyword_get              = isset($_GET['sch_t_keyword']) ? $_GET['sch_t_keyword'] : "";
        $sch_r_keyword_get              = isset($_GET['sch_r_keyword']) ? $_GET['sch_r_keyword'] : "";
        $sch_dp_no_get                  = isset($_GET['sch_dp_no']) ? $_GET['sch_dp_no'] : "";
        $sch_wd_no_get                  = isset($_GET['sch_wd_no']) ? $_GET['sch_wd_no'] : "";
        $sch_task_req_get               = isset($_GET['sch_task_req']) ? $_GET['sch_task_req'] : "";
        $sch_task_run_get               = isset($_GET['sch_task_run']) ? $_GET['sch_task_run'] : "";
        $quick_search_chk_val           = isset($_GET['quick_search_chk']) ? $_GET['quick_search_chk'] : "";
        $sch_file_no                    = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";
        $sch_run_is_null                = isset($_GET['sch_run_is_null']) ? $_GET['sch_run_is_null'] : "";
        $sch_category                   = isset($_GET['sch_category']) ? $_GET['sch_category'] : "";

        if (!$sch_set_w_no_get) // 업무 세트 리스트는 예외
            $sch_w_no_get = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";

        if (!empty($quick_search_chk_val)) {
            $smarty->assign("sch_quick_chk", $quick_search_chk_val);
        }

        //QUICK SEARCH
        if (!empty($q_sch_get)) { // 퀵서치
            if ($q_sch_get == "1") { //요청안된건
                $add_where .= " AND (w.work_state='1' OR w.work_state='2')";
            } elseif ($q_sch_get == "2") { //요청중인건
                $add_where .= " AND w.work_state='3'";
            } elseif ($q_sch_get == "3") { //진행중인건
                $add_where .= " AND w.work_state IN(4,5)";
            } elseif ($q_sch_get == "4") { //진행완료건
                $add_where .= " AND w.work_state='6'";
            } elseif ($q_sch_get == "5") { //연장여부 확인요청건
                // D-10일 ~ +5일
                $add_where .= " AND w.work_state='6' AND w.extension_date <= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL 10 DAY), '%Y-%m-%d') AND w.extension_date >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -5 DAY), '%Y-%m-%d') AND (w.extension='2' OR w.extension='0' OR w.extension IS NULL)";
            }elseif ($q_sch_get == "6") { //미완료건
                $add_where .= " AND w.work_state IN(3,4,5)";
            }
            $smarty->assign("q_sch", $q_sch_get);
        }

        if (!empty($sch_get)) { // 상세검색 펼치기
            $smarty->assign("sch", $sch_get);
        }
        if (!empty($new_get)) { // 업무 추가 요청 펼치기
            $smarty->assign("new", $new_get);
        }
        if (!empty($out_memo_get)) { // 출금업체 메모 보기 펼치기
            $smarty->assign("out_memo", $out_memo_get);
        }

        $f_desire_date = isset($_GET['desire_date']) ? $_GET['desire_date'] : "";
        if(!empty($f_desire_date)){
            $smarty->assign("f_desire_date", $f_desire_date);
        }

        // 상품에 따른 그룹 코드 설정
        for ($arr_i = 1; $arr_i < count($product_list); $arr_i++) {
            if ($product_list[$arr_i]['prd_no'] == $sch_prd_get) {
                $sch_prd_g1_get = $product_list[$arr_i]['g1_code'];
                $sch_prd_g2_get = $product_list[$arr_i]['g2_code'];
                break;
            }
        }

        $smarty->assign("sch_prd_g1", $sch_prd_g1_get);
        $smarty->assign("sch_prd_g2", $sch_prd_g2_get);

        if (!empty($sch_s_name_get)) { // 업체 담당자
            $add_where .= " AND w.s_no IN (SELECT s_no FROM staff WHERE s_name LIKE '%{$sch_s_name_get}%')";
        }
        $smarty->assign("sch_s_name", $sch_s_name_get);

        if (!empty($sch_req_team))
        {
            $sch_req_team_where = getTeamWhere($my_db, $sch_req_team);
            if($sch_req_team == "00221"){
                $sch_req_team_where .= ",00236";
            }
            $add_where .= " AND w.task_req_team IN({$sch_req_team_where})";

            $smarty->assign("sch_req_team", $sch_req_team);
        }

        if (!empty($sch_req_s_name_get)) { // 업무 요청자
            $add_where .= " AND w.task_req_s_no IN (SELECT s_no FROM staff WHERE s_name LIKE '%{$sch_req_s_name_get}%')";
        } else {
            if (!$sch_get && permissionNameCheck($session_permission, "마케터")) {

            } elseif($session_team == '00244'){

            }elseif (!$sch_get && !permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "외주관리자") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "서비스운영")) {
                $sch_req_s_name_get = $session_name;
                $add_where .= " AND w.task_req_s_no='{$session_s_no}'";
            }
        }
        $smarty->assign("sch_req_s_name", $sch_req_s_name_get);

        if (!empty($sch_work_time_s_get) || $sch_work_time_s_get == '0') { // work time s
            if ($sch_work_time_s_get == "null") {
                $add_where .= " AND w.work_time IS NULL";
            } else {
                $add_where .= " AND w.work_time>='" . $sch_work_time_s_get . "'";
            }
            $smarty->assign("sch_work_time_s", $sch_work_time_s_get);
        }

        if (!empty($sch_work_time_e_get) || $sch_work_time_e_get == '0') { // work time e
            if ($sch_work_time_e_get == "null") {
                $add_where .= " AND w.work_time IS NULL";
            } else {
                $add_where .= " AND w.work_time<='" . $sch_work_time_e_get . "'";
            }
            $smarty->assign("sch_work_time_e", $sch_work_time_e_get);
        }

        $is_prd_staff_all = false;
        if (!empty($sch_prd_get)) { // 상품
            $add_where .= " AND w.prd_no='" . $sch_prd_get . "'";

            $is_prd_staff_chk_sql = "SELECT task_run_staff_type FROM product WHERE prd_no='{$sch_prd_get}'";
            $is_prd_staff_chk_query = mysqli_query($my_db, $is_prd_staff_chk_sql);
            $is_prd_staff_result = mysqli_fetch_assoc($is_prd_staff_chk_query);

            if(isset($is_prd_staff_result['task_run_staff_type']) && $is_prd_staff_result['task_run_staff_type'] == '2'){
                $is_prd_staff_all = true;
            }
        } else {
            if ($sch_prd_g2_get) { // 상품 선택 없이 두번째 그룹까지 설정한 경우
                $add_where .= " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.display='1' AND prd.k_name_code='{$sch_prd_g2_get}')";
            } elseif ($sch_prd_g1_get) { // 상품 선택 없이 찻번째 그룹까지 설정한 경우
                $add_where .= " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.display='1' AND prd.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1_get}'))";
            }
        }
        $smarty->assign("sch_prd", $sch_prd_get); // 전체 업무내역에 자주하는 업무가 나타게 하기위해 empty 밖에서 처리함
        $smarty->assign("is_prd_staff_all", $is_prd_staff_all);

        if (!empty($sch_reg_s_date)) { // 작성일
            $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
            $add_where .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
            $smarty->assign("sch_reg_s_date", $sch_reg_s_date);
        }

        if (!empty($sch_reg_e_date)) { // 작성일
            $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
            $add_where .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
            $smarty->assign("sch_reg_e_date", $sch_reg_e_date);
        }

        if (!empty($sch_task_run_s_date)) { // 업무완료일
            $sch_task_run_s_datetime = $sch_task_run_s_date." 00:00:00";
            $add_where .= " AND w.task_run_regdate >= '{$sch_task_run_s_date}'";
            $smarty->assign("sch_task_run_s_date", $sch_task_run_s_date);
        }

        if (!empty($sch_task_run_e_date)) { // 업무완료일
            $sch_task_run_e_datetime = $sch_task_run_e_date." 23:59:59";
            $add_where .= " AND w.task_run_regdate <= '{$sch_task_run_e_datetime}'";
            $smarty->assign("sch_task_run_e_date", $sch_task_run_e_date);
        }

        if (!empty($sch_work_state_get) && empty($q_sch_get)) { // 진행상태 [퀵서치를 안한경우]
            $add_where .= " AND w.work_state='" . $sch_work_state_get . "'";
            $smarty->assign("sch_work_state", $sch_work_state_get);
        }
        if (!empty($sch_wd_dp_no_state_get)) { // 지급/입금상태
            if ($sch_wd_dp_no_state_get == '1') {
                $add_where .= " AND w.wd_no IS NOT NULL AND ((SELECT wd_state FROM withdraw wd where wd.wd_no=w.wd_no) = '1' OR (SELECT wd_state FROM withdraw wd where wd.wd_no=w.wd_no) = '2')";
            } elseif ($sch_wd_dp_no_state_get == '2') {
                $add_where .= " AND (SELECT wd_state FROM withdraw wd where wd.wd_no=w.wd_no) = '3'";
            } elseif ($sch_wd_dp_no_state_get == '3') {
                $add_where .= " AND w.dp_no IS NOT NULL AND ((SELECT dp_state FROM deposit dp where dp.dp_no=w.dp_no) = '1' OR (SELECT dp_state FROM deposit dp where dp.dp_no=w.dp_no) = '3')";
            } elseif ($sch_wd_dp_no_state_get == '4') {
                $add_where .= " AND w.dp_no IS NOT NULL";
            }
            $smarty->assign("sch_wd_dp_no_state", $sch_wd_dp_no_state_get);
        }

        if ($sch_dp_no_get == 'null') { // dp_no is null
            $add_where .= " AND w.dp_no is null";
            $smarty->assign("sch_dp_no_get", $sch_dp_no_get);
        }

        if ($sch_wd_no_get == 'null') { // wd_no is null
            $add_where .= " AND w.wd_no is null";
            $smarty->assign("sch_wd_no", $sch_wd_no_get);
        }

        if (!empty($sch_run_direct_get)) {
            $smarty->assign("sch_run_direct", $sch_run_direct_get);
        }

				if (!empty($sch_service_apply_get)) { // Service?
            if ($sch_service_apply_get == '1') {
                $add_where .= " AND w.service_apply = '1'";
            } elseif ($sch_service_apply_get == '2') {
                $add_where .= " AND w.service_apply = '2'";
            }
            $smarty->assign("sch_service_apply", $sch_service_apply_get);
        }

        if($sch_run_is_null == "1"){
            $add_where .= " AND (w.task_run_s_no = 0 OR w.task_run_s_no IS NULL)";
        }else{

            if (!empty($sch_run_team))
            {
                $sch_run_team_where = getTeamWhere($my_db, $sch_run_team);
                if($sch_run_team == "00221"){
                    $sch_run_team_where .= ",00236";
                }
                $add_where .= " AND w.task_run_team IN({$sch_run_team_where})";

                $smarty->assign("sch_run_team", $sch_run_team);
            }

            if (!empty($sch_run_s_name_get)) { // 업무 처리자
                $add_where .= " AND w.task_run_s_no IN(SELECT s_no FROM staff WHERE s_name like '%{$sch_run_s_name_get}%')";
                $smarty->assign("sch_run_s_name", $sch_run_s_name_get);
            }
        }
        $smarty->assign("sch_run_is_null", $sch_run_is_null);

        if (!empty($sch_dp_c_no_get)) { // 입금업체(입금처)
            if ($sch_dp_c_no_get != "null") {
                $add_where .= " AND w.dp_c_no='" . $sch_dp_c_no_get . "'";
            } else {
                $add_where .= " AND (w.dp_c_no IS NULL OR w.dp_c_no ='0')";
            }

            $smarty->assign("sch_dp_c_no", $sch_dp_c_no_get);
        }

        if (!empty($sch_wd_c_no_get)) { // 출금업체(지급처)
            if ($sch_wd_c_no_get != "null") {
                $add_where .= " AND w.wd_c_no='" . $sch_wd_c_no_get . "'";
            } else {
                $add_where .= " AND (w.wd_c_no IS NULL OR w.wd_c_no ='0')";
            }

            $smarty->assign("sch_wd_c_no", $sch_wd_c_no_get);
        }
        if (!empty($sch_extension_get)) { // 연장여부
            $add_where .= " AND w.extension='" . $sch_extension_get . "'";
            $smarty->assign("sch_extension", $sch_extension_get);
        }
        if (!empty($sch_extension_date_get)) { //연장여부 마감일
            $add_where .= " AND w.extension_date='" . $sch_extension_date_get . "'";
            $smarty->assign("sch_extension_date", $sch_extension_date_get);
        }

        if (!empty($sch_req_evaluation_get)) { //업무요청자 업무평가
            if (permissionNameCheck($session_permission, "대표") || permissionNameCheck($session_permission, "마스터관리자")) {
                $add_where .= " AND w.req_evaluation='" . $sch_req_evaluation_get . "'";
            } else {
                $add_where .= " AND w.req_evaluation='" . $sch_req_evaluation_get . "' AND ((SELECT staff_state FROM staff WHERE s_no=w.task_run_s_no)='2' || (SELECT staff_state FROM staff WHERE s_no=w.task_run_s_no)='3'	)";
            }
            $smarty->assign("sch_req_evaluation", $sch_req_evaluation_get);
        }
        if (!empty($sch_req_evaluation_memo_get)) { //업무요청자 업무평가 메모
            if ($sch_req_evaluation_memo_get == '1') {
                $add_where .= " AND w.req_evaluation_memo IS NOT NULL AND w.req_evaluation_memo <> ''";
                $smarty->assign("sch_req_evaluation_memo", $sch_req_evaluation_memo_get);
            } elseif ($sch_req_evaluation_memo_get == '2') {
                $add_where .= " AND w.req_evaluation_memo = NULL OR w.req_evaluation_memo = '' ";
                $smarty->assign("sch_req_evaluation_memo", $sch_req_evaluation_memo_get);
            }
        }

        if (!empty($sch_evaluation_get)) { //업무처리자 업무평가
            if (permissionNameCheck($session_permission, "마케터") || permissionNameCheck($session_permission, "대표") || permissionNameCheck($session_permission, "마스터관리자") || $session_s_no == '19') {
                $add_where .= " AND w.evaluation='" . $sch_evaluation_get . "'";
            } else {
                $add_where .= " AND w.evaluation='" . $sch_evaluation_get . "' AND ((SELECT staff_state FROM staff WHERE s_no=w.task_run_s_no)='2' || (SELECT staff_state FROM staff WHERE s_no=w.task_run_s_no)='3'	)";
            }
            $smarty->assign("sch_evaluation", $sch_evaluation_get);
        }
        if (!empty($sch_evaluation_memo_get)) { //업무처리자 업무평가 메모
            if ($sch_evaluation_memo_get == '1') {
                $add_where .= " AND w.evaluation_memo IS NOT NULL AND w.evaluation_memo <> ''";
                $smarty->assign("sch_evaluation_memo", $sch_evaluation_memo_get);
            } elseif ($sch_evaluation_memo_get == '2') {
                $add_where .= " AND w.evaluation_memo = NULL OR w.evaluation_memo = '' ";
                $smarty->assign("sch_evaluation_memo", $sch_evaluation_memo_get);
            }
        }

        if (!empty($sch_c_name_get)) { // 업체명
            $add_where .= " AND w.c_name like '%" . $sch_c_name_get . "%'";
            $smarty->assign("sch_c_name", $sch_c_name_get);
        }
        if (!empty($sch_t_keyword_get)) { // 타겟키워드 및 타겟채널
            $add_where .= " AND w.t_keyword like '%" . $sch_t_keyword_get . "%'";
            $smarty->assign("sch_t_keyword", $sch_t_keyword_get);
        }
        if (!empty($sch_r_keyword_get)) { // 노출키워드
            $add_where .= " AND w.r_keyword like '%" . $sch_r_keyword_get . "%'";
            $smarty->assign("sch_r_keyword", $sch_r_keyword_get);
        }
        if (!empty($sch_w_no_get)) { // 업무번호
            $add_where .= " AND w.w_no ='" . $sch_w_no_get . "'";
            $smarty->assign("sch_w_no", $sch_w_no_get);
        }
        if (!empty($sch_task_req_get)) { // 업무요청
            $add_where .= " AND w.task_req like '%" . $sch_task_req_get . "%'";
            $smarty->assign("sch_task_req", $sch_task_req_get);
        }
        if (!empty($sch_task_run_get)) { // 업무진행
            $add_where .= " AND w.task_run like '%" . $sch_task_run_get . "%'";
            $smarty->assign("sch_task_run", $sch_task_run_get);
        }

        if (!empty($sch_file_no)) { // 파일번호
            $add_where .= " AND w.file_no = '{$sch_file_no}'";
            $smarty->assign("sch_file_no", $sch_file_no);
        }

        if (!empty($sch_category)) { // 세부카테고리
            $add_where .= " AND w.k_name_code IN(SELECT sub.k_name_code FROM kind AS sub WHERE sub.k_code='work_task_run' AND sub.k_name LIKE '%{$sch_category}%' AND sub.display='1')";
            $smarty->assign("sch_category", $sch_category);
        }

        if (!!$sch_set_w_no_get) { // 업무 세트의 태그 조건
            $add_where .= " AND w.set_tag IS NOT NULL AND w.set_tag LIKE '%" . $sch_set_w_no_get . "%'";
            $smarty->assign("sch_set_w_no", $sch_set_w_no_get);

            // 업무 세트 정보를 가져옴
            $work_set_info_sql = "
		    SELECT
		        w_s_i.no,
						w_s_i.w_no,
						w_s_i.title,
						w_s_i.memo,
						w_s_i.regdate
		    FROM work_set_info w_s_i
		    WHERE
		        w_s_i.w_no = {$sch_set_w_no_get}
		    ";
            //echo $work_set_info_sql; //exit;
            $work_set_info_result = mysqli_query($my_db, $work_set_info_sql);
            $work_set_info = mysqli_fetch_array($work_set_info_result);
            $smarty->assign("ws_info_no", $work_set_info['no']);
            $smarty->assign("ws_info_w_no", $work_set_info['w_no']);
            $smarty->assign("ws_info_title", $work_set_info['title']);
            $smarty->assign("ws_info_memo", $work_set_info['memo']);
            $smarty->assign("ws_info_regdate", $work_set_info['regdate']);


            // 업무세트 구성 리스트
            $work_set_unit_sql = "SELECT w_s_u.wsu_no,
															w_s_u.title
														FROM work_set_unit w_s_u
														WHERE w_s_u.display='1'
														ORDER BY w_s_u.wsu_no ASC
								";

            $work_set_unit_query = mysqli_query($my_db, $work_set_unit_sql);

            $work_set_unit[] = array('::전체::', '0', '');
            if (!!$work_set_unit_query)
                while ($work_set_unit_data = mysqli_fetch_array($work_set_unit_query)) {
                    $work_set_unit[] = array(
                        "wsu_no" => $work_set_unit_data['wsu_no'],
                        "title" => $work_set_unit_data['title']
                    );
                }
            $smarty->assign("work_set_unit", $work_set_unit);
        }

        // 상품 구분 가져오기(1차)
        $prd_g1_sql = "
	    SELECT
	        k_name,k_name_code
	    FROM kind
	    WHERE
	        k_code='product' AND (k_parent='0' OR k_parent is null) AND display='1'
	    ORDER BY
	        priority ASC
	    ";
        $prd_g1_result = mysqli_query($my_db, $prd_g1_sql);

        while ($prd_g1 = mysqli_fetch_array($prd_g1_result)) {
            $prd_g1_list[] = array(
                "k_name" => trim($prd_g1['k_name']),
                "k_name_code" => trim($prd_g1['k_name_code'])
            );
        }
        $smarty->assign("prd_g1_list", $prd_g1_list);


        // 상품 구분 가져오기(2차)
        if (empty($sch_prd_get) && $sch_prd_g2_get) { // 상품 선택 없이 상품 그룹2로 검색시 그룹2 가져오기
            $prd_g2_sql = "
		    SELECT
		        k_name,k_name_code,k_parent
		    FROM kind
		    WHERE
						display='1' AND
		        k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code='" . $sch_prd_g2_get . "')
		    ORDER BY
		        priority ASC
		    ";
        } elseif (empty($sch_prd_get) && $sch_prd_g1_get) { // 상품 선택 없이 상품 그룹1로 검색시 그룹2 가져오기
            $prd_g2_sql = "
				SELECT
						k_name,k_name_code,k_parent
				FROM kind
				WHERE
						display='1' AND
						k_code='product' AND k_parent='" . $sch_prd_g1_get . "'
				ORDER BY
						priority ASC
				";
        } else { // 그 외 상품에 따른 그룹2 가져오기
            $prd_g2_sql = "
		    SELECT
		        k_name,k_name_code,k_parent
		    FROM kind
		    WHERE
		        k_code='product' AND k_parent=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE display='1' AND prd.prd_no='" . $sch_prd_get . "')) AND display='1'
		    ORDER BY
		        priority ASC
		    ";
        }

        $prd_g2_result = mysqli_query($my_db, $prd_g2_sql);
        while ($prd_g2 = mysqli_fetch_array($prd_g2_result)) {
            $prd_g2_list[] = array(
                "k_name" => trim($prd_g2['k_name']),
                "k_name_code" => trim($prd_g2['k_name_code'])
            );
        }
        $smarty->assign("prd_g2_list", $prd_g2_list);


        // 상품 목록 가져오기
        if (empty($sch_prd_get) && $sch_prd_g2_get) { // 상품 선택 없이 상품 그룹2로 검색시 상품 가져오기
            $prd_sql = "
                SELECT
                    title,prd_no,k_name_code
                FROM product
                WHERE display='1' AND k_name_code='{$sch_prd_g2_get}'
                ORDER BY priority ASC
		    ";
        } else {
            $prd_sql = "
                SELECT
                    title,prd_no,k_name_code
                FROM product
                WHERE display='1' AND k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no='{$sch_prd_get}')
                ORDER BY priority ASC
		    ";
        }

        $prd_result = mysqli_query($my_db, $prd_sql);
        while ($prd = mysqli_fetch_array($prd_result)) {

            if ($prd['prd_no'] == '179' && $session_team != "00211") {
                continue;
            }

            $sch_prd_list[] = array(
                "w_no" => trim($work_array['w_no']),
                "title" => trim($prd['title']),
                "prd_no" => trim($prd['prd_no']),
                "k_name_code" => trim($prd['k_name_code'])
            );
        }
        $smarty->assign("sch_prd_list", $sch_prd_list);

        #업무추가 가능여부 확인
        $is_prd_add  = false;
        $is_guide_on = false;
        $is_guide_no = 0;
        $is_guide_required = false;

        if(!empty($sch_prd_get))
        {
            $sch_prd_sql    = "SELECT display, is_guide, guide_no, guide_required FROM product WHERE prd_no='{$sch_prd_get}'";
            $sch_prd_query  = mysqli_query($my_db, $sch_prd_sql);
            $sch_prd_result = mysqli_fetch_assoc($sch_prd_query);

            $is_prd_add         = (isset($sch_prd_result['display']) && !empty($sch_prd_result['display']) && $sch_prd_result['display'] == '1') ? true : false;
            $is_guide_on        = (isset($sch_prd_result['is_guide']) && $sch_prd_result['is_guide']=='1') ? true : false;
            $is_guide_no        = (isset($sch_prd_result['guide_no']) && !empty($sch_prd_result['guide_no'])) ? $sch_prd_result['guide_no'] : 0;
            $is_guide_required  = (isset($sch_prd_result['guide_required']) && $sch_prd_result['guide_required']=='1') ? true : false;
        }
        $smarty->assign('is_prd_add', $is_prd_add);
        $smarty->assign('is_guide_on', $is_guide_on);
        $smarty->assign('is_guide_no', $is_guide_no);
        $smarty->assign('is_guide_required', $is_guide_required);

        // 직원가져오기
        //$add_where_permission = str_replace("0", "_", $permission_name['마케터']);

        //$staff_sql="select s_no,s_name from staff where staff_state < '3' AND permission like '".$add_where_permission."'";
        $staff_sql = "select s_no,s_name from staff where staff_state < '3'";
        $staff_query = mysqli_query($my_db, $staff_sql);

        while ($staff_data = mysqli_fetch_array($staff_query)) {
            $staff[] = array(
                'no' => $staff_data['s_no'],
                'name' => $staff_data['s_name']
            );
            $smarty->assign("staff", $staff);
        }

        // 출금업체 가져오기
        $out_com_add_where = "(";
        for ($i = 0; $i < count($wd_company_list); $i++) {
            if ($wd_company_list[$i][2] == $sch_prd) {
                if ($out_com_add_where == "(") {
                    $out_com_add_where .= "c_no='{$wd_company_list[$i][1]}'";
                } else {
                    $out_com_add_where .= " OR c_no='{$wd_company_list[$i][1]}'";
                }
            }
        }
        $out_com_add_where .= ")";

        if ($out_com_add_where != "()") { // 출금업체 목록이 없는 경우 생략
            $wd_company_sql = "SELECT c_no, c_memo FROM company WHERE corp_kind='2' AND {$out_com_add_where}";
            $wd_company_query = mysqli_query($my_db, $wd_company_sql);

            while ($wd_company_data = mysqli_fetch_array($wd_company_query)) {
                $wd_company[] = array(
                    'c_no' => $wd_company_data['c_no'],
                    'c_memo' => htmlspecialchars($wd_company_data['c_memo'])
                );
                $smarty->assign("wd_company", $wd_company);
            }
        }

        // 상품정보 - 업무요청 내용, 입/출금 설정 가져오기 (1:미설정, 2:입금, 3:출금, 4:입금+출금) && work time mehtod 가져오기 (0:미사용, 1:사용(기본값 변경불가), 2:사용(기본값 변경가능)) && work time 기본값 가져오기
        $prd_info_sql = "SELECT
												prd.kind,
												prd.work_format,
												prd.equally_vat,
												prd.wd_dp_state,
												prd.work_time_method,
												prd.work_time_default
										FROM product prd
										WHERE display='1' AND prd.prd_no = '{$sch_prd_get}'
									";
        $prd_info_result = mysqli_query($my_db, $prd_info_sql);

        if (!!$prd_info_result)
            $product_data = mysqli_fetch_array($prd_info_result);

        $work_kind = $product_data['kind'];
        $work_format = $product_data['work_format'];
        $work_equally_vat = $product_data['equally_vat'];

        $smarty->assign("work_kind", $work_kind);
        $smarty->assign("work_format", $work_format);
        $smarty->assign("work_equally_vat", $work_equally_vat);

        $smarty->assign("wd_dp_state", $product_data['wd_dp_state']);
        $smarty->assign("work_time_method", $product_data['work_time_method']);
        $smarty->assign("work_time_default", $product_data['work_time_default']);


        // 상품에 대한 업무진행자 리스트 수 가져오기
        $task_run_staff_list_cnt = 0;
        for ($arr_i = 0; $arr_i < count($task_run_staff_list); $arr_i++) {
            if ($task_run_staff_list[$arr_i][2] == $sch_prd_get  && $sch_prd_get != "250") {
                $task_run_staff_list_cnt++;
            }
        }
        $smarty->assign("task_run_staff_list_cnt", $task_run_staff_list_cnt);

        # 사업지원팀 project 관련 처리
        $sch_linked_table = isset($_GET['sch_linked_table']) ? $_GET['sch_linked_table'] : "";
        $sch_linked_no = isset($_GET['sch_linked_no']) ? $_GET['sch_linked_no'] : "";
        if(!empty($sch_linked_table) && !empty($sch_linked_no))
        {
            $smarty->assign("sch_linked_table", $sch_linked_table);
            $smarty->assign("sch_linked_no", $sch_linked_no);
        }

        // 선결제 잔액 가져오기
        if (permissionNameCheck($session_permission, "재무관리자") || permissionNameCheck($session_permission, "외주관리자")) {
            for ($arr_i = 0; $arr_i < count($paid_list); $arr_i++) {
                if ($paid_list[$arr_i][0] == $sch_prd) {
                    $withdraw_sql = "SELECT
													wd.c_name,
													SUM(wd.wd_money) AS sum_wd_money
												FROM withdraw wd
												WHERE (w_no='0' OR w_no IS NULL) AND wd.c_no='" . $paid_list[$arr_i][1] . "'";

                    //echo $withdraw_sql; exit;

                    $withdraw_query = mysqli_query($my_db, $withdraw_sql);
                    while ($withdraw_array = mysqli_fetch_array($withdraw_query)) {
                        // 잔액 계산하기 선결재 - 업무상 출금비 지출액
                        if ($sch_prd == '10') {
                            if ($paid_list[$arr_i][1] == '367') { // 오른웍스
                                $work_sql = "SELECT
														SUM(w.wd_price_vat) AS sum_wd_price_vat
													FROM work w
													WHERE w.work_state='6' AND w.task_run_regdate IS NOT NULL AND ((SELECT wd.wd_method FROM withdraw wd WHERE wd.wd_no = w.wd_no) = '3') AND w.prd_no='{$paid_list[$arr_i][0]}' AND w.wd_c_no='" . $paid_list[$arr_i][1] . "' AND (w.wd_price_vat<>'0' OR w.wd_price_vat IS NOT NULL)";
                                $work_query = mysqli_query($my_db, $work_sql);
                                $work_data = mysqli_fetch_array($work_query);
                                $sum_used_money = $work_data[0]+280434000; // 8876번 기준 잔액 (11378번 ~ 8876번 44,000원 오차 반영)
                            } else if ($paid_list[$arr_i][1] == '383') { // 미디어리서치
                                $work_sql = "SELECT
														SUM(w.wd_price_vat) AS sum_wd_price_vat
													FROM work w
													WHERE w.work_state='6' AND w.task_run_regdate IS NOT NULL AND (w.wd_no IS NULL OR (SELECT wd.wd_method FROM withdraw wd WHERE wd.wd_no = w.wd_no) = '3') AND w.prd_no='{$paid_list[$arr_i][0]}' AND w.wd_c_no='" . $paid_list[$arr_i][1] . "' AND (w.wd_price_vat<>'0' OR w.wd_price_vat IS NOT NULL)";
                                $work_query = mysqli_query($my_db, $work_sql);
                                $work_data = mysqli_fetch_array($work_query);
                                $sum_used_money = $work_data[0];
                            } else if ($paid_list[$arr_i][1] == '2166') { // 리얼타임네트웍스
                                $work_sql = "SELECT
														SUM(w.wd_price_vat) AS sum_wd_price_vat
													FROM work w
													WHERE w.work_state='6' AND w.task_run_regdate IS NOT NULL AND (w.wd_no IS NULL OR (SELECT wd.wd_method FROM withdraw wd WHERE wd.wd_no = w.wd_no) = '3') AND w.prd_no IN('8', '10') AND w.wd_c_no='" . $paid_list[$arr_i][1] . "' AND (w.wd_price_vat<>'0' OR w.wd_price_vat IS NOT NULL)";
                                $work_query = mysqli_query($my_db, $work_sql);
                                $work_data = mysqli_fetch_array($work_query);
                                $sum_used_money = $work_data[0];
                            } else if ($paid_list[$arr_i][1] == '2803') { // 미디어마케팅그룹
                                $work_sql = "SELECT
														SUM(w.wd_price_vat) AS sum_wd_price_vat
													FROM work w
													WHERE w.work_state='6' AND w.task_run_regdate IS NOT NULL AND (w.wd_no IS NULL OR (SELECT wd.wd_method FROM withdraw wd WHERE wd.wd_no = w.wd_no) = '3') AND w.prd_no='{$paid_list[$arr_i][0]}' AND w.wd_c_no='" . $paid_list[$arr_i][1] . "' AND (w.wd_price_vat<>'0' OR w.wd_price_vat IS NOT NULL)";
                                $work_query = mysqli_query($my_db, $work_sql);
                                $work_data = mysqli_fetch_array($work_query);
                                $sum_used_money = $work_data[0];
                            }
                        } else if ($sch_prd == '53') {
                            $work_sql = "SELECT
													SUM(w.wd_price_vat) AS sum_wd_price_vat
												FROM work w
												WHERE w.work_state='6' AND w.task_run_regdate IS NOT NULL AND w.wd_c_no='" . $paid_list[$arr_i][1] . "' AND (w.wd_price_vat<>'0' OR w.wd_price_vat IS NOT NULL)";
                            $work_query = mysqli_query($my_db, $work_sql);
                            $work_data = mysqli_fetch_array($work_query);
                            $sum_used_money = $work_data[0];
                        } else if ($sch_prd == '8') {
                            if ($paid_list[$arr_i][1] == '2166') { // 리얼타임네트웍스
                                $work_sql = "SELECT
																	SUM(w.wd_price_vat) AS sum_wd_price_vat
																FROM work w
																WHERE w.work_state='6' AND w.task_run_regdate IS NOT NULL AND (w.wd_no IS NULL OR (SELECT wd.wd_method FROM withdraw wd WHERE wd.wd_no = w.wd_no) = '3') AND w.prd_no IN('8', '10') AND w.wd_c_no='" . $paid_list[$arr_i][1] . "' AND (w.wd_price_vat<>'0' OR w.wd_price_vat IS NOT NULL)";
                                $work_query = mysqli_query($my_db, $work_sql);
                                $work_data = mysqli_fetch_array($work_query);
                                $sum_used_money = $work_data[0];
                            }
                        } else if ($sch_prd == '193') {
                            if ($paid_list[$arr_i][1] == '3228') { // (주)에이엠피엠글로벌
                                $waple_gfa_wd_money = 54048547;
                                $withdraw_waple_sql = "SELECT wd.c_name, SUM(wd.wd_money) AS sum_wd_money FROM withdraw wd WHERE (w_no='0' OR w_no IS NULL) AND wd.c_no='3228' AND wd.regdate >= '2021-05-20 00:00:00' AND wd.my_c_no='1'";
                                $withdraw_waple_query = mysqli_query($my_db, $withdraw_waple_sql);
                                $withdraw_waple_array = mysqli_fetch_array($withdraw_waple_query);
                                $waple_gfa_wd_money += $withdraw_waple_array['sum_wd_money'];

                                $media_gfa_wd_money = 22000000;
                                $withdraw_media_sql = "SELECT wd.c_name, SUM(wd.wd_money) AS sum_wd_money FROM withdraw wd WHERE (w_no='0' OR w_no IS NULL) AND wd.c_no='3228' AND wd.regdate >= '2021-05-20 00:00:00' AND wd.my_c_no='3'";
                                $withdraw_media_query = mysqli_query($my_db, $withdraw_media_sql);
                                $withdraw_media_array = mysqli_fetch_array($withdraw_media_query);
                                $media_gfa_wd_money += $withdraw_media_array['sum_wd_money'];

                                $work_waple_sql = "SELECT SUM(w.wd_price_vat) AS sum_wd_price_vat FROM work w
								WHERE w.work_state='6' AND w.task_run_regdate IS NOT NULL AND (w.wd_no IS NULL OR (SELECT wd.wd_method FROM withdraw wd WHERE wd.wd_no = w.wd_no) = '3') AND w.prd_no='{$paid_list[$arr_i][0]}' AND w.wd_c_no='" . $paid_list[$arr_i][1] . "' AND (w.wd_price_vat<>'0' OR w.wd_price_vat IS NOT NULL)
								 AND w.task_run_regdate >= '2021-05-20 00:00:00' AND w.c_no != '1113'";
                                $work_waple_query = mysqli_query($my_db, $work_waple_sql);
                                $work_waple_data = mysqli_fetch_array($work_waple_query);
                                $waple_sum_used_money = $work_waple_data[0];

                                $work_media_sql = "SELECT SUM(w.wd_price_vat) AS sum_wd_price_vat FROM work w
								WHERE w.work_state='6' AND w.task_run_regdate IS NOT NULL AND (w.wd_no IS NULL OR (SELECT wd.wd_method FROM withdraw wd WHERE wd.wd_no = w.wd_no) = '3') AND w.prd_no='{$paid_list[$arr_i][0]}' AND w.wd_c_no='" . $paid_list[$arr_i][1] . "' AND (w.wd_price_vat<>'0' OR w.wd_price_vat IS NOT NULL)
								 AND w.task_run_regdate >= '2021-05-20 00:00:00' AND w.c_no = '1113'";
                                $work_media_query = mysqli_query($my_db, $work_media_sql);
                                $work_media_data = mysqli_fetch_array($work_media_query);
                                $media_sum_used_money = $work_media_data[0];

                                $paid_lists[] = array(
                                    "c_name" => "와이즈플래닛",
                                    "sum_wd_money" => number_format(($waple_gfa_wd_money - $waple_sum_used_money) / 1.1),
                                    "sum_wd_money_vat" => number_format($waple_gfa_wd_money - $waple_sum_used_money)
                                );

                                $paid_lists[] = array(
                                    "c_name" => "미디어커머스",
                                    "sum_wd_money" => number_format(($media_gfa_wd_money - $media_sum_used_money) / 1.1),
                                    "sum_wd_money_vat" => number_format($media_gfa_wd_money - $media_sum_used_money)
                                );
                            }
                        }


                        if ($sch_prd != '193') {
                            $paid_lists[] = array(
                                "c_name" => $withdraw_array['c_name'],
                                "sum_wd_money" => number_format(($withdraw_array['sum_wd_money'] - $sum_used_money) / 1.1),
                                "sum_wd_money_vat" => number_format($withdraw_array['sum_wd_money'] - $sum_used_money)
                            );
                        }

                        $smarty->assign(array(
                            "paid_lists" => $paid_lists
                        ));
                    }
                }
            }

        }

        // 정렬순서 토글 & 필드 지정
        $add_orderby = "";
        $order = isset($_GET['od']) ? $_GET['od'] : "";
        $order_type = isset($_GET['by']) ? $_GET['by'] : "1";

        if ($order_type == '2') {
            $toggle = "DESC";
        } else {
            $toggle = "ASC";
        }

        if ($order == 1 || $order == 2) {
            if ($order_type == '1') {
                $toggle = "DESC";
            } else {
                $toggle = "ASC";
            }
        }

        $order_field = array('', 'task_req_dday', 'task_run_dday', 'extension_date');
        if ($order && $order < 4) {
            $add_orderby .= " ISNULL($order_field[$order]) ASC, $order_field[$order] $toggle,";
            $add_where .= " AND ((DATE_FORMAT(NOW(), '%Y-%m-%d') <= $order_field[$order]) OR ((w.work_state = '3' OR w.work_state = '4' OR w.work_state = '5') AND $order_field[$order] IS NOT NULL))";
            $smarty->assign("order", $order);
            $smarty->assign("order_type", $order_type);
        } elseif ($order == 4) {
            $add_orderby .= " ISNULL(task_run_regdate) ASC, task_run_regdate $toggle,";
            $add_where .= " AND w.work_state = '6' AND task_run_regdate  IS NOT NULL";
            $smarty->assign("order", $order);
            $smarty->assign("order_type", $order_type);
        }

        if ($q_sch_get == "5") { // 퀵서치의 연장여부 확인 요청건은 \연장여부 마감일순으로 정렬
            if (!!$sch_set_w_no_get) { // 업무 세트의 경우 세트 태그업무는 무조건 최상단 표시
                $add_orderby .= " w.w_no = '{$sch_set_w_no_get}' DESC, extension_date asc ";
            } else {
                $add_orderby .= " extension_date asc ";
            }
        } else {
            if (!!$sch_set_w_no_get) { // 업무 세트의 경우 세트 태그업무는 무조건 최상단 표시
                $add_orderby .= " w.w_no = '{$sch_set_w_no_get}' DESC, w.priority ASC, ";
            }
            $add_orderby .= " w_no desc ";
        }

        if (!!$sch_set_w_no_get) { // 업무 세트의 경우 세트 태그업무는 무조건 포함 시키기
            $add_where .= " OR w_no = '{$sch_set_w_no_get}' ";
        }

        // 페이지에 따른 limit 설정
        $pages = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $smarty->assign("page", $pages);

        // 페이지당 리스트수
        if($sch_set_w_no_get){
            $od_list_num = isset($_GET['od_list_num']) ? intval($_GET['od_list_num']) : 50;
        }else{
            $od_list_num = isset($_GET['od_list_num']) ? intval($_GET['od_list_num']) : 10;
        }
        $smarty->assign("od_list_num", $od_list_num);
        $num = $od_list_num;
        $offset = ($pages - 1) * $num;


        // 세트 업무형의 경우 세트명과 메모, 업무 구성 수 표기
        $add_select = "";
        if ($work_kind == '2') { // 세트 업무형
            $add_select .= "(SELECT wsi.title FROM work_set_info wsi WHERE wsi.w_no=w.w_no) AS set_title,";
            $add_select .= "(SELECT COUNT(*)-1 FROM `work` w2 WHERE w2.set_tag LIKE CONCAT('%',w.w_no,'%')) AS set_tag_count,";
            $add_select .= "(SELECT wsi.memo FROM work_set_info wsi WHERE wsi.w_no=w.w_no) AS set_memo,";
        }

        $my_db->set_charset("utf8mb4");
        // 리스트 쿼리
        $work_sql = "
            SELECT
                *,
                {$add_select}
                (SELECT price_apply FROM product prd WHERE prd.prd_no=w.prd_no) AS price_apply,
                (SELECT k_name FROM kind WHERE k_name_code=w.k_name_code) AS k_name,
                (SELECT regdate FROM withdraw wd where wd.wd_no=w.wd_no) as wd_regdate,
                (SELECT wd_date FROM withdraw wd where wd.wd_no=w.wd_no) as wd_date,
                (SELECT wd_state FROM withdraw wd where wd.wd_no=w.wd_no) as wd_state,
                (SELECT wd_method FROM withdraw wd where wd.wd_no=w.wd_no) as wd_method,
                (SELECT regdate FROM deposit dp where dp.dp_no=w.dp_no) as dp_regdate,
                (SELECT dp_date FROM deposit dp where dp.dp_no=w.dp_no) as dp_date,
                (SELECT dp_state FROM deposit dp where dp.dp_no=w.dp_no) as dp_state,
                (SELECT dp_method FROM deposit dp where dp.dp_no=w.dp_no) as dp_method,
                (SELECT s_name FROM staff s where s.s_no=w.task_req_s_no) as task_req_s_name,
                (SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name,
                (SELECT staff_state FROM staff s where s.s_no=w.task_run_s_no) as task_run_staff_state,
                (SELECT s_name from staff s where s.s_no=w.s_no) as s_no_name,
                (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1,
                (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1_name,
                (SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2,
                (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2_name,
                (SELECT title from product prd where prd.prd_no=w.prd_no) as prd_no_name,
                (SELECT prd.wd_dp_state FROM product prd	WHERE prd.prd_no = w.prd_no) as wd_dp_state,
                (SELECT prd.work_time_method FROM product prd	WHERE prd.prd_no = w.prd_no) as work_time_method,
                (SELECT prd.wd_sch_type FROM product prd WHERE prd.prd_no = w.prd_no) as wd_sch_type,
                (SELECT prd.dp_sch_type FROM product prd WHERE prd.prd_no = w.prd_no) as dp_sch_type,
                (SELECT prd.task_run_staff_type FROM product prd WHERE prd.prd_no = w.prd_no) as run_staff_type,
                (SELECT t.team_leader FROM team t WHERE t.team_code=w.task_run_team) as t_leader,
                (SELECT c.kakao FROM company as c WHERE c.c_no=w.c_no) as kakao_id,
                (SELECT c.naver FROM company as c WHERE c.c_no=w.c_no) as naver_id,
                (SELECT prd.work_format_type from product prd where prd.prd_no=w.prd_no) as prd_work_type,
                (SELECT prd.work_format_extra from product prd where prd.prd_no=w.prd_no) as prd_work_extra,
                (SELECT prd.dp_account from product prd where prd.prd_no=w.prd_no) as dp_account,
                (SELECT prd.wd_account from product prd where prd.prd_no=w.prd_no) as wd_account,
                (SELECT t.team_name FROM team t WHERE t.team_code=w.team) as t_name, 
                (SELECT t.team_name FROM team t WHERE t.team_code=w.task_req_team) as req_t_name, 
                (SELECT t.team_name FROM team t WHERE t.team_code=w.task_run_team) as run_t_name 
            FROM work w
            WHERE {$add_where}
            ORDER BY {$add_orderby}
		";

        //echo $work_sql; //exit;
        $smarty->assign("query", $work_sql);

        // 전체 게시물 수
        $cnt_sql = "SELECT count(w.w_no) FROM work w WHERE {$add_where}";
        $cnt_query = mysqli_query($my_db, $cnt_sql);
        if (!!$cnt_query)
            $cnt_data = mysqli_fetch_array($cnt_query);
        $total_num = $cnt_data[0];

        $smarty->assign(array(
            "total_num" => number_format($total_num)
        ));

        $smarty->assign("total_num", $total_num);
        $pagenum = ceil($total_num / $num);


        // 페이징
        if ($pages >= $pagenum) {
            $pages = $pagenum;
        }
        if ($pages <= 0) {
            $pages = 1;
        }

        // 검색 조건
        $search_url = getenv("QUERY_STRING");

        # 퀵버튼 이동
        $quick_search_url = "";
        if (!empty($search_url)) {
            $search_url_list = explode("&", $search_url);
            $quick_search_url_list = [];
            foreach ($search_url_list as $search_key) {
                if (strpos($search_key, 'sch_prd_g1') !== false || strpos($search_key, 'sch_prd_g2') !== false
                    || strpos($search_key, 'sch_prd') !== false || strpos($search_key, 'quick_search_url') !== false || strpos($search_key, 'quick_search_chk') !== false
                ) {

                } else {
                    $quick_search_url_list[] = $search_key;
                }
            }

            if (!empty($quick_search_url_list)) {
                $quick_search_url = implode("&", $quick_search_url_list);
            }
        }

        $smarty->assign("search_url", $search_url);
        $smarty->assign("quick_search_url", $quick_search_url);
        $page = pagelist($pages, "work_list.php", $pagenum, $search_url);
        $smarty->assign("pagelist", $page);

        // 리스트 쿼리 결과(데이터)
        $work_sql .= "LIMIT $offset,$num";
        $result = mysqli_query($my_db, $work_sql);

        $t_keyword_arr = array();

        # Model Option 설정
        $corp_model          = Corporation::Factory();
        $corp_account_option = $corp_model->getTotalAccountList();

        if (!!$result)
            while ($work_array = mysqli_fetch_array($result)) {

                if (!empty($work_array['regdate'])) {
                    $regdate_day_value = date("Y/m/d", strtotime($work_array['regdate']));
                    $regdate_time_value = date("H:i", strtotime($work_array['regdate']));
                } else {
                    $regdate_day_value = "";
                    $regdate_time_value = "";
                }

                if (!empty($work_array['wd_price'])) {
                    $wd_price_value = number_format($work_array['wd_price']);
                } else {
                    $wd_price_value = "";
                }
                if (!empty($work_array['wd_price_vat'])) {
                    $wd_price_vat_value = number_format($work_array['wd_price_vat']);
                } else {
                    $wd_price_vat_value = "";
                }
                if (!empty($work_array['wd_regdate'])) {
                    $wd_regdate_day_value = date("Y/m/d", strtotime($work_array['wd_regdate']));
                    $wd_regdate_time_value = date("H:i", strtotime($work_array['wd_regdate']));
                } else {
                    $wd_regdate_day_value = "";
                    $wd_regdate_time_value = "";
                }

                if (!empty($work_array['dp_price'])) {
                    $dp_price_value = number_format($work_array['dp_price']);
                } else {
                    $dp_price_value = "";
                }
                if (!empty($work_array['dp_price_vat'])) {
                    $dp_price_vat_value = number_format($work_array['dp_price_vat']);
                } else {
                    $dp_price_vat_value = "";
                }
                if (!empty($work_array['dp_regdate'])) {
                    $dp_regdate_day_value = date("Y/m/d", strtotime($work_array['dp_regdate']));
                    $dp_regdate_time_value = date("H:i", strtotime($work_array['dp_regdate']));
                } else {
                    $dp_regdate_day_value = "";
                    $dp_regdate_time_value = "";
                }

                // 입금업체 이름 찾기
                $dp_c_name2 = "";
                for ($arr_i = 1; $arr_i < count($dp_company_list); $arr_i++) {
                    if ($work_array['dp_c_no'] == $dp_company_list[$arr_i][1] && $work_array['prd_no'] == $dp_company_list[$arr_i][2]) {
                        $dp_c_name2 = $dp_company_list[$arr_i][3];
                    }
                }

                // 출금업체 이름 찾기
                $wd_c_no_name2 = "";
                for ($arr_i = 1; $arr_i < count($wd_company_list); $arr_i++) {
                    if ($work_array['wd_c_no'] == $wd_company_list[$arr_i][1] && $work_array['prd_no'] == $wd_company_list[$arr_i][2]) {
                        $wd_c_no_name2 = $wd_company_list[$arr_i][3];
                    }
                }

                //연장 마감일 D-Day
                $extension_d_day = "";
                if ($work_array['extension_date'])
                    $extension_d_day = intval((strtotime(date("Y-m-d", time())) - strtotime($work_array['extension_date'])) / 86400);

                // ±3일 이내 남은 연장여부 없는 것들만 보여줌
                if ($work_array['extension'] != '1' && $work_array['extension'] != '3' && !empty($work_array['extension_date']) && $work_array['extension_date'] >= date('Y-m-d', strtotime('-3 day', time())) && $work_array['extension_date'] <= date('Y-m-d', strtotime('+3 day', time()))) {
                    $extension_alert = "연장기한";
                    // 3일 지난 연장여부 없는 것들만(연장기한초과)
                } elseif (empty($work_array['extension']) && !empty($work_array['extension_date']) && $work_array['extension_date'] < date('Y-m-d', strtotime('+3 day', time()))) {
                    $extension_alert = "연장기한초과";
                } else {
                    $extension_alert = "";
                }

                // 선결제 여부 확인
                $paid_list_boolean = false;
                for ($arr_i = 0; $arr_i < count($paid_list); $arr_i++) {
                    if ($paid_list[$arr_i][0] == $work_array['prd_no'] && $paid_list[$arr_i][1] == $work_array['wd_c_no']) {
                        $paid_list_boolean = true;
                    }
                }

                // 월정산 여부
                $month_paid_list_boolean = false;
                for ($arr_i = 0; $arr_i < count($month_paid_list); $arr_i++) {
                    if ($month_paid_list[$arr_i][0] == $work_array['prd_no'] && $month_paid_list[$arr_i][1] == $work_array['wd_c_no']) {
                        $month_paid_list_boolean = true;
                    }
                }

                // 업무진행자 리스트에 존재여부 확인
                $is_task_run_staff_list = false;
                for ($arr_i = 0; $arr_i < count($task_run_staff_list); $arr_i++) {
                    if ($task_run_staff_list[$arr_i][1] == $work_array['task_run_s_no']) {
                        if ($task_run_staff_list[$arr_i][2] == $work_array['prd_no']) {
                            $is_task_run_staff_list = true;
                        }
                    }
                }

                // 입금업체 리스트에 존재여부 확인
                $is_dp_company_list = false;
                for ($arr_i = 0; $arr_i < count($dp_company_list); $arr_i++) {
                    if ($dp_company_list[$arr_i][1] == $work_array['dp_c_no']) {
                        if ($dp_company_list[$arr_i][2] == $work_array['prd_no']) {
                            $is_dp_company_list = true;
                        }
                    }
                }

                // 출금업체 리스트에 존재여부 확인
                $is_wd_company_list = false;
                for ($arr_i = 0; $arr_i < count($wd_company_list); $arr_i++) {
                    if ($wd_company_list[$arr_i][1] == $work_array['wd_c_no']) {
                        if ($wd_company_list[$arr_i][2] == $work_array['prd_no']) {
                            $is_wd_company_list = true;
                        }
                    }
                }

                // set_tag를 #단위로 tokenized 하여 array로 가져옴
                $set_tag_token = array();
                $set_tag_token = stringTokenized($work_array['set_tag'], '#');


                // 인센티브 정산 || 캠프의 경우 출금액 합산 값구하기
                $wd_price_sum_value = "";
                if ($work_array['prd_no'] == '56' || $work_array['prd_no'] == '121') {
                    $wd_price_sum_sql = "SELECT SUM(w.wd_price) FROM `work` w WHERE w.set_tag LIKE '%{$work_array['w_no']}%'";
                    $wd_price_sum_query = mysqli_query($my_db, $wd_price_sum_sql);
                    $wd_price_sum_data = mysqli_fetch_array($wd_price_sum_query);
                    $wd_price_sum_value = $wd_price_sum_data[0];
                }

                // 월간 검색수를 위한 키워드 array 보관
                if (!empty($work_array['t_keyword'])) {
                    $wno_keyword = array($work_array['w_no'], $work_array['t_keyword'], 0);
                    array_push($t_keyword_arr, $wno_keyword);
                }

                //연관업무
                $related_work_list = [];
                $related_work_list_sql = "SELECT w_no, (SELECT work_state FROM work w WHERE w.w_no=wr.w_no LIMIT 1) as work_state, w_parent, (SELECT work_state FROM work w WHERE w.w_no=wr.w_parent LIMIT 1) as p_work_state FROM work_relation wr WHERE wr.w_no='{$work_array['w_no']}' OR wr.w_parent='{$work_array['w_no']}' ORDER BY w_no ASC";
                $related_work_list_query = mysqli_query($my_db, $related_work_list_sql);
                while ($related_work = mysqli_fetch_assoc($related_work_list_query)) {
                    if ($related_work['w_no'] == $work_array['w_no'] && !empty($related_work['w_parent'])) {
                        $p_work_state_name = isset($related_work['p_work_state']) && isset($work_state_name_list[$related_work['p_work_state']]) ? $work_state_name_list[$related_work['p_work_state']] : "";
                        $related_work_list[$related_work['w_parent']] = $p_work_state_name;
                    }

                    $work_state_name = isset($related_work['work_state']) && isset($work_state_name_list[$related_work['work_state']]) ? $work_state_name_list[$related_work['work_state']] : "";
                    $related_work_list[$related_work['w_no']] = $work_state_name;
                }

                if (!empty($related_work_list) && count($related_work_list) == '1') {
                    $related_work_list = [];
                }

                //탑블로그 링크
                $linked_url = "";
                $linked_url_option = getExtraUrlOption();
                if (isset($work_array['linked_no']) && !empty($work_array['linked_no']))
                {
                    if(isset($linked_url_option[$work_array['linked_table']]))
                    {
                        $linked_url = $linked_url_option[$work_array['linked_table']].$work_array['linked_no'];
                    }
                }

                if($work_array['linked_table'] == "logistics_management"){
                    $logistics_sql      = "SELECT * FROM logistics_management WHERE lm_no='{$work_array['linked_no']}'";
                    $logistics_query    = mysqli_query($my_db, $logistics_sql);
                    $logistics_result   = mysqli_fetch_assoc($logistics_query);
                    $work_array['task_req'] = $logistics_result['reason'];
                }

                # 멀티파일첨부
                $req_file_count = $run_file_count = 0;
                if (!empty($work_array['task_req_file_origin'])) {

                    $req_file_list = explode(',', $work_array['task_req_file_origin']);
                    $req_file_count = ($req_file_list == "") ? 0 : count($req_file_list);
                }

                if (!empty($work_array['task_run_file_origin'])) {
                    $run_file_list = explode(',', $work_array['task_run_file_origin']);
                    $run_file_count = ($run_file_list == "") ? 0 : count($run_file_list);
                }

                $s_no_name          = $work_array['s_no_name'] . "({$work_array['t_name']})";
                $task_req_s_name    = $work_array['task_req_s_name'] . "({$work_array['req_t_name']})";
                $task_run_s_name    = $work_array['task_run_s_name'] . "({$work_array['run_t_name']})";

                $task_run_url = "";
                if (strpos($work_array['task_run'], 'http') !== false) {
                    $work_task_run = explode('http', $work_array['task_run']);
                    $task_run_url = isset($work_task_run[1]) ? $work_task_run[1] : "";
                }

                #업무진행 옵션 선택
                $task_run_permission = false;
                if (!empty($work_array['prd_no'])) {
                    $content_work_list = array('35', '36', '38', '39', '40', '54');
                    $content_team = "00222";
                    if ($work_array['task_run_s_no'] == $session_s_no || (in_array($work_array['prd_no'], $content_work_list) && $session_team == $content_team)) {
                        $task_run_permission = true;
                    }
                }

                $prd_work_extra = ($work_array['prd_work_type'] == '2' && !empty($work_array['prd_work_extra'])) ? true : false;

                # 법인계좌 & 업무 연동
                $f_dp_account_list  = (isset($work_array['dp_account']) && !empty($work_array['dp_account'])) ? explode(',', $work_array['dp_account']) : [];
                $f_wd_account_list  = (isset($work_array['wd_account']) && !empty($work_array['wd_account'])) ? explode(',', $work_array['wd_account']) : [];
                $dp_account_option  = [];
                $wd_account_option  = [];

                if(!empty($f_dp_account_list)){
                    foreach($f_dp_account_list as $dp_co_no){
                        $dp_account_option[$dp_co_no] = $corp_account_option[$dp_co_no];
                    }
                }

                if(!empty($f_wd_account_list)){
                    foreach($f_wd_account_list as $wd_co_no){
                        $wd_account_option[$wd_co_no] = $corp_account_option[$wd_co_no];
                    }
                }

                # 입금, 출금 기본방식
                $base_dp_method  = isset($total_dp_list[$work_array['prd_no']][$work_array['dp_c_no']]) ? $total_dp_list[$work_array['prd_no']][$work_array['dp_c_no']] : '2';
                $base_wd_method  = isset($total_wd_list[$work_array['prd_no']][$work_array['wd_c_no']]) ? $total_wd_list[$work_array['prd_no']][$work_array['wd_c_no']] : '1';

                $work_lists[] = array(
                    "set_tag" => $set_tag_token,
                    "w_no" => $work_array['w_no'],
                    "regdate" => $work_array['regdate'],
                    "task_run_regdate" => isset($work_array['task_run_regdate']) ? date("Y-m-d", strtotime($work_array['task_run_regdate'])) : "",
                    "work_state" => $work_array['work_state'],
                    "extension_alert" => $extension_alert,
                    "extension_d_day" => $extension_d_day,
                    "wd_regdate_day" => $wd_regdate_day_value,
                    "wd_regdate_time" => $wd_regdate_time_value,
                    "dp_regdate_day" => $dp_regdate_day_value,
                    "dp_regdate_time" => $dp_regdate_time_value,
                    "c_no" => $work_array['c_no'],
                    "c_name" => $work_array['c_name'],
                    "s_no" => $work_array['s_no'],
                    "s_no_name" => $s_no_name,
                    "team" => $work_array['team'],
                    "task_req_s_name" => $task_req_s_name,
                    "task_req_team" => $work_array['task_req_team'],
                    "task_run_s_name" => $task_run_s_name,
                    "task_run_team" => $work_array['task_run_team'],
                    "task_run_staff_type" => $work_array['run_staff_type'],
                    "task_run_url" => $task_run_url,
                    "k_prd1" => $work_array['k_prd1'],
                    "k_prd1_name" => $work_array['k_prd1_name'],
                    "k_prd2" => $work_array['k_prd2'],
                    "k_prd2_name" => $work_array['k_prd2_name'],
                    "prd_no" => $work_array['prd_no'],
                    "prd_no_name" => $work_array['prd_no_name'],
                    "quantity" => $work_array['quantity'],
                    "priority" => $work_array['priority'],
                    "wd_dp_state" => $work_array['wd_dp_state'],
                    "t_keyword" => $work_array['t_keyword'],
                    "price_apply" => $work_array['price_apply'],
                    "selling_price" => number_format($work_array['selling_price']),
                    "service_apply" => $work_array['service_apply'],
                    "service_memo" => htmlspecialchars($work_array['service_memo']),
                    "k_name_code" => $work_array['k_name_code'],
                    "k_name" => $work_array['k_name'],
                    "r_keyword" => $work_array['r_keyword'],
                    "set_title" => $work_array['set_title'],
                    "set_tag_count" => $work_array['set_tag_count'],
                    "set_memo" => $work_array['set_memo'],
                    "task_req" => htmlspecialchars($work_array['task_req']),
                    "task_req_s_no" => $work_array['task_req_s_no'],
                    "task_req_dday" => $work_array['task_req_dday'],
                    "req_file_count" => $req_file_count, // 멀티파일첨부
                    "req_evaluation" => $work_array['req_evaluation'],
                    "req_evaluation_memo" => htmlspecialchars($work_array['req_evaluation_memo']),
                    "work_time_method" => $work_array['work_time_method'],
                    "work_time" => $work_array['work_time'],
                    "task_run" => $work_array['task_run'],
                    "task_run_permission" => $task_run_permission,
                    "task_run_dday" => $work_array['task_run_dday'],
                    "task_run_s_no" => $work_array['task_run_s_no'],
                    "task_run_staff_state" => $work_array['task_run_staff_state'],
                    "is_task_run_staff_list" => $is_task_run_staff_list,
                    "task_run_regdate_day" => isset($work_array['task_run_regdate']) ? date("Y/m/d", strtotime($work_array['task_run_regdate'])) : "",
                    "task_run_regdate_time" => isset($work_array['task_run_regdate']) ? date("H:i", strtotime($work_array['task_run_regdate'])) : "",
                    "run_file_count" => $run_file_count, // 멀티파일첨부
                    "dp_price" => $dp_price_value,
                    "dp_price_vat" => $dp_price_vat_value,
                    "dp_c_no" => $work_array['dp_c_no'],
                    "dp_c_name" => $work_array['dp_c_name'],
                    "is_dp_company_list" => $is_dp_company_list,
                    "dp_c_name2" => $dp_c_name2,
                    "dp_no" => $work_array['dp_no'],
                    "dp_state" => $work_array['dp_state'],
                    "dp_method" => $work_array['dp_method'],
                    "wd_price_sum" => $wd_price_sum_value,
                    "wd_price" => $wd_price_value,
                    "wd_price_vat" => $wd_price_vat_value,
                    "wd_c_no" => $work_array['wd_c_no'],
                    "wd_c_name" => $work_array['wd_c_name'],
                    "is_wd_company_list" => $is_wd_company_list,
                    "wd_c_no_name2" => $wd_c_no_name2,
                    "wd_no" => $work_array['wd_no'],
                    "wd_state" => $work_array['wd_state'],
                    "wd_method" => $work_array['wd_method'],
                    "manager_memo" => htmlspecialchars($work_array['manager_memo']),
                    "wd_pay_date" => isset($work_array['wd_date']) ? date("Y-m-d", strtotime($work_array['wd_date'])) : "",
                    "dp_pay_date" => isset($work_array['dp_date']) ? date("Y-m-d", strtotime($work_array['dp_date'])) : "",
                    "extension_date" => isset($work_array['extension_date']) ? date("Y-m-d", strtotime($work_array['extension_date'])) : "",
                    "incentive_apply" => $work_array['incentive_apply'],
                    "extension" => $work_array['extension'],
                    "evaluation" => $work_array['evaluation'],
                    "evaluation_memo" => htmlspecialchars($work_array['evaluation_memo']),
                    "regdate_day" => $regdate_day_value,
                    "regdate_time" => $regdate_time_value,
                    "paid_list_boolean" => $paid_list_boolean,
                    "month_paid_list_boolean" => $month_paid_list_boolean,
                    "work_value" => (isset($work_array['work_value']) && !empty($work_array['work_value'])) ? number_format($work_array['work_value']) : "",
                    "linked_no" => $work_array['linked_no'],
                    "linked_url" => $linked_url,
                    "linked_table" => $work_array['linked_table'],
                    "related_work_list" => $related_work_list,
                    "wd_sch_type" => $work_array['wd_sch_type'],
                    "dp_sch_type" => $work_array['dp_sch_type'],
                    "t_leader"  => $work_array['t_leader'],
                    "naver_id"  => $work_array['naver_id'],
                    "kakao_id"  => $work_array['kakao_id'],
                    "prd_work_extra"    => $prd_work_extra,
                    "dp_account_option" => $dp_account_option,
                    "wd_account_option" => $wd_account_option,
                    "base_dp_method"    => $base_dp_method,
                    "base_wd_method"    => $base_wd_method,
                );
            }

        // 월간 검색수 Start [w_no, t_keyword, monthly_total]
        $hintKeywords_5 = "";
        $cnt_i = 0;
        for ($arr_5 = 0; $arr_5 < count($t_keyword_arr); $arr_5++) {
            $t_keyword_arr[$arr_5][1] = str_replace(" ", "", $t_keyword_arr[$arr_5][1]); // 공백 제거
            $t_keyword_arr[$arr_5][1] = strtoupper($t_keyword_arr[$arr_5][1]); // 공백제거 후 대문자변환


            if ($hintKeywords_5 == "") {
                $hintKeywords_5 = $t_keyword_arr[$arr_5][1];
            } else {
                $hintKeywords_5 = $hintKeywords_5 . "," . $t_keyword_arr[$arr_5][1];
            }
            $cnt_i++;

            if (($arr_5 + 1) % 5 == 0 || $arr_5 == (count($t_keyword_arr) - 1)) { // 5개 단위로 묶어서 검색량 조회
                $keyword_list = array(
                    "hintKeywords" => $hintKeywords_5
                );

                $response = $api->GET('/keywordstool', str_replace(' ', '', $keyword_list));
                $hintKeywords_5 = ""; // 초기화

                for ($arr_j = 0; $arr_j < count($t_keyword_arr); $arr_j++) {
                    if ($arr_j < ($arr_5 - ($cnt_i - 1)) || $arr_j > $arr_5)
                        continue;

                    for ($arr_i = 0; $arr_i < count($response["keywordList"]); $arr_i++) {
                        if ($response["keywordList"][$arr_i]["relKeyword"] == $t_keyword_arr[$arr_j][1]) {
                            $t_keyword_arr[$arr_j][2] = number_format((int)$response["keywordList"][$arr_i]["monthlyPcQcCnt"] + (int)$response["keywordList"][$arr_i]["monthlyMobileQcCnt"]);
                            break;
                        }
                    }
                }
                $cnt_i = 0;
                usleep(200000);
            }
        }
        // 월간 검색수 End

        //연관업무
        $related_w_no = (isset($_GET['related_w_no']) && !empty($_GET['related_w_no'])) ? $_GET['related_w_no'] : "";
        $new_s_no = $session_s_no;
        $new_s_name = $session_s_team_name;
        $new_team = $session_team;
        $new_c_no = "";
        $new_c_name = "";

        if (!!$related_w_no) {
            $related_work_sql = "SELECT c_name, c_no, s_no, (SELECT s.s_name FROM staff s where s.s_no = w.s_no) as s_name, team, (SELECT t.team_name FROM team t where t.team_code = w.team) as t_name  FROM work w WHERE w.w_no='{$related_w_no}' LIMIT 1";
            $related_work_query = mysqli_query($my_db, $related_work_sql);
            $related_work_result = mysqli_fetch_assoc($related_work_query);

            $new_s_no = $related_work_result['s_no'];
            $new_s_name = $related_work_result['s_name'] . "(" . $related_work_result['t_name'] . ")";
            $new_team = $related_work_result['team'];
            $new_c_no = $related_work_result['c_no'];
            $new_c_name = $related_work_result['c_name'];
        }

        $smarty->assign('related_w_no', $related_w_no);
        $smarty->assign('new_s_no', $new_s_no);
        $smarty->assign('new_team', $new_team);
        $smarty->assign('new_s_name', $new_s_name);
        $smarty->assign('new_c_no', $new_c_no);
        $smarty->assign('new_c_name', $new_c_name);


        $session_team_name = $team_name_list[$session_team];
        $session_full_name = $session_name."({$session_team_name})";
        $smarty->assign("session_full_name",$session_full_name);

        $smarty->assign(array(
            "work_list" => $work_lists,
            "t_keyword_arr" => $t_keyword_arr
        ));

        #월정산지급 추가
        $monthly_work_date = date('Y-m');
        $smarty->assign('wd_state_option', getWdStateOption());
        $smarty->assign('wd_method_option', getWdMethodOption());
        $smarty->assign('dp_state_option', getDpStateOption());
        $smarty->assign('dp_method_option', getDpMethodOption());

        #Topblog prd
        $topblog_prd_list = array('1', '2', '3', '170', '171');
        $smarty->assign('topblog_prd_list', $topblog_prd_list);

        #업무세트 연관업무
        $set_related_prd_list = [];
        if(!empty($sch_set_w_no_get))
        {
            $set_related_prd_sql    = "SELECT prd.prd_no, prd.title FROM product_relation pr LEFT JOIN product prd ON prd.prd_no = pr.related_prd WHERE pr.prd_no=(SELECT w.prd_no FROM `work` w WHERE w.w_no='{$sch_set_w_no_get}') ORDER BY pr.priority ASC, pr.no ASC";
            $set_related_prd_query  = mysqli_query($my_db,  $set_related_prd_sql);

            while ($set_related_prd = mysqli_fetch_assoc($set_related_prd_query)) {
                $set_related_prd_list[] = $set_related_prd;
            }
        }
        $smarty->assign('set_related_prd_list', $set_related_prd_list);


        # 외부기능 관련
        $is_work_extra       = false;
        $work_extra_type_new = "";
        if($sch_prd_get)
        {
            $work_extra_sql = "SELECT work_format_type, work_format_extra FROM product p WHERE p.prd_no='{$sch_prd_get}' LIMIT 1";
            $work_extra_query = mysqli_query($my_db, $work_extra_sql);
            $work_extra_result = mysqli_fetch_assoc($work_extra_query);

            if ($work_extra_result['work_format_type'] == 2) {
                $is_work_extra = true;
                $work_extra_type_new = $work_extra_result['work_format_extra'];
            }
        }

        $smarty->assign("is_work_extra", $is_work_extra);
        $smarty->assign("work_extra_type_new", $work_extra_type_new);
        $smarty->assign("work_format_extra_option", getWorkFormatExtra());

        # Navigation & My Quick
        $nav_prd_no  = "12";
        $nav_title   = "업무 리스트";
        $quick_model = MyQuick::Factory();
        $is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

        $smarty->assign("is_my_quick", $is_my_quick);
        $smarty->assign("nav_title", $nav_title);
        $smarty->assign("nav_prd_no", $nav_prd_no);

        $smarty->display('work_list.html');
    }
}

/* Slack API START */
function sendSlackMessageBlock($channel, $title, $message, $type)
{
    $slackHook = new SlackHook();
    $slackHook->setTokenUsername($type);
    $slackHook->setChannel($channel);
    $slackHook->setTitle($title);
    $slackHook->setMessage($message);
    $slackHook->sendBlock();
}
/* Slack API END */

function task_req_dday_count_alert($task_req_dday_cnt, $prd_no, $value){
	if(!!$value)
		for($i = 0 ; $i < count($GLOBALS['task_req_dday_count_list']) ; $i++) {
			if($GLOBALS['task_req_dday_count_list'][$i][0] == $prd_no){
				if($GLOBALS['task_req_dday_count_list'][$i][1] <= $task_req_dday_cnt){
					echo "<script>alert('요청하신 희망완료일 {$value}에 {$task_req_dday_cnt}건이 요청중에 있습니다.\\n희망완료일이 몰릴 경우 스케줄조정이 불가피하므로\\n가능하다면 다른날로 변경 부탁드립니다.');</script>";
				}
			}
		}
}

?>
