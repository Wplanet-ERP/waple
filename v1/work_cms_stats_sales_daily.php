<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Custom.php');
require('inc/model/ProductCms.php');

# Navigation & My Quick
$nav_prd_no  = "213";
$nav_title   = "요일/시간별 판매실적 현황(아임웹)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수
$product_model              = ProductCms::Factory();
$url_model                  = Custom::Factory();
$url_model->setMainInit("commerce_url", "url_no");
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$sch_brand_total_list       = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$sch_url_list               = [];
$dp_self_imweb_list         = getSelfDpImwebCompanyList();
$dp_self_imweb_text         = implode(",", $dp_self_imweb_list);

# 검색조건
$add_where          = "`cu`.display = '1' AND `cu`.url LIKE 'idx=%'";
$add_cms_where      = "w.page_idx > 0 AND w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN({$dp_self_imweb_text})";
$sch_daily_type     = isset($_GET['sch_daily_type']) ? $_GET['sch_daily_type'] : "1";
$sch_ord_s_date     = isset($_GET['sch_ord_s_date']) ? $_GET['sch_ord_s_date'] : date("Y-m-d",strtotime("-8 days"));
$sch_ord_e_date     = isset($_GET['sch_ord_e_date']) ? $_GET['sch_ord_e_date'] : date("Y-m-d",strtotime("-2 days"));
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_dp_company     = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";
$sch_url            = isset($_GET['sch_url']) ? $_GET['sch_url'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_dp_company)) {
    $add_where      .= " AND `cu`.dp_c_no='{$sch_dp_company}'";
    $add_cms_where  .= " AND `w`.dp_c_no='{$sch_dp_company}'";
    $smarty->assign("sch_dp_company", $sch_dp_company);
}

if(!empty($sch_url))
{
    $chk_url_item   = $url_model->getItem($sch_url);
    $chk_page_idx   = str_replace("idx=","", $chk_url_item['url']);
    
    $add_cms_where .= " AND `w`.page_idx='{$chk_page_idx}' AND `w`.dp_c_no = '{$chk_url_item['dp_c_no']}'";
    $smarty->assign("sch_url", $sch_url);
}

# 브랜드 옵션
if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `cu`.brand = '{$sch_brand}'";
    $add_cms_where     .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `cu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_cms_where     .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where         .= " AND `cu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_cms_where     .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

if(!empty($sch_ord_s_date) && !empty($sch_ord_e_date))
{
    $sch_ord_s_datetime = $sch_ord_s_date . " 00:00:00";
    $sch_ord_e_datetime = $sch_ord_e_date . " 23:59:59";

    $add_cms_where .= " AND w.order_date BETWEEN '{$sch_ord_s_datetime}' AND '{$sch_ord_e_datetime}'";
}
$smarty->assign("sch_ord_s_date", $sch_ord_s_date);
$smarty->assign("sch_ord_e_date", $sch_ord_e_date);

# URL 리스트
$sch_page_url_list  = [];
$sch_url_sql        = "SELECT url_no, page_title, url, REPLACE(url,'idx=','') as chk_url FROM commerce_url `cu` WHERE {$add_where} ORDER BY LENGTH(chk_url) ASC, chk_url";
$sch_url_query      = mysqli_query($my_db, $sch_url_sql);
while($sch_url_result = mysqli_fetch_assoc($sch_url_query))
{
    $sch_url_list[$sch_url_result['url_no']] = $sch_url_result['page_title']." :: ".$sch_url_result['url'];
}

# 변수
$date_name_option       = getDateChartOption();
$daily_hour_option      = getHourOption();
$daily_date_list        = [];
$daily_hour_list        = [];
$daily_sales_list       = [];
$daily_date_total_list  = [];
$daily_hour_total_list  = array("max" => 0);
$daily_sales_total_price= 0;

foreach($daily_hour_option as $hour){
    $hour_key = (int)$hour;
    $daily_hour_list[$hour_key] = $hour."시";
}

if(!empty($sch_ord_s_date) && !empty($sch_ord_e_date))
{
    foreach($date_name_option as $date_key => $date_title)
    {
        $daily_date_list[$date_key] = $date_title;

        $daily_sales_list[$date_key]['max'] = 0;
        $daily_date_total_list[$date_key]   = 0;
        foreach($daily_hour_list as $hour_key){
            $daily_sales_list[$date_key][$hour_key] = 0;
            $daily_hour_total_list[$hour_key]       = 0;
        }
    }

    # 리스트 쿼리
    $daily_sales_sql    = "
        SELECT
            DATE_FORMAT(w.order_date, '%w') as date_key,
            DATE_FORMAT(w.order_date, '%H') as hour_key,
            SUM(unit_price) as total_price
        FROM work_cms w
        WHERE {$add_cms_where}
        GROUP BY date_key, hour_key
    ";
    $daily_sales_query = mysqli_query($my_db, $daily_sales_sql);
    while($daily_sales_result = mysqli_fetch_assoc($daily_sales_query))
    {
        $daily_date_key     = $daily_sales_result['date_key'];
        $daily_hour_key     = (int)$daily_sales_result['hour_key'];
        $daily_total_price  = (int)$daily_sales_result['total_price'];
        $daily_max          = $daily_sales_list[$daily_date_key]["max"];

        $daily_date_total_list[$daily_date_key] += $daily_total_price;
        $daily_hour_total_list[$daily_hour_key] += $daily_total_price;
        $daily_sales_total_price                += $daily_total_price;

        $daily_sales_list[$daily_date_key][$daily_hour_key] += $daily_total_price;
        if($daily_max < $daily_total_price){
            $daily_sales_list[$daily_date_key]["max"] = $daily_total_price;
        }
    }

    $daily_date_total_list["min"] = min($daily_date_total_list);
    $daily_date_total_list["max"] = max($daily_date_total_list);
    $daily_hour_total_list["max"] = max($daily_hour_total_list);
}

$smarty->assign("sch_dp_company_option", getTrafficDpCompanyOption());
$smarty->assign("sch_url_list", $sch_url_list);
$smarty->assign("daily_date_list", $daily_date_list);
$smarty->assign("daily_hour_list", $daily_hour_list);
$smarty->assign("daily_sales_total_price", $daily_sales_total_price);
$smarty->assign("daily_date_total_list", $daily_date_total_list);
$smarty->assign("daily_hour_total_list", $daily_hour_total_list);
$smarty->assign("daily_sales_list", $daily_sales_list);

$smarty->display('work_cms_stats_sales_daily.html');
?>
