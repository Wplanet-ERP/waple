<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');

$w_no    = isset($_GET['w_no']) ? $_GET['w_no'] : "";
$w_type  = isset($_GET['w_type']) ? $_GET['w_type'] : "";

// 업무번호 및 타입 확인
if(empty($w_no) || empty($w_type)){
    $smarty->display('access_company_error.html');
    exit;
}

$f_name_column = $w_type."_file_origin";
$f_path_column = $w_type."_file_read";

$work_sql 	 = "SELECT {$f_name_column} as file_name, {$f_path_column} as file_path FROM `work` WHERE w_no = {$w_no} LIMIT 1";
$work_query  = mysqli_query($my_db, $work_sql);
$work 		 = mysqli_fetch_assoc($work_query);

$zip_file_name  = "";
$zip_file_path  = "";
$work_file_name = $work['file_name'];
$work_file_path = $work['file_path'];
$work_file_list = [];
if(!empty($work_file_name) && !empty($work_file_path))
{
    $file_names = explode(',', $work_file_name);
    $file_paths = explode(',', $work_file_path);

    $zip_file_name   = "work_{$w_no}_{$w_type}_file.zip";
    $zip_file_path   = "zip_tmp/{$zip_file_name}";
    $zip_file_origin = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$zip_file_path}";

    if(file_exists($zip_file_origin)){
        del_file($zip_file_path);
    }

    $zip = new ZipArchive;
    $res = $zip->open($zip_file_origin, ZipArchive::CREATE);

    foreach($file_paths as $idx => $file_path)
    {
        $file_origin = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$file_path}";
        $file_size   = 0;
        $file_name   = isset($file_names[$idx]) ? $file_names[$idx] : "";
        if(file_exists($file_origin)){
            $file_size = filesize($file_origin)/1024;
            $file_size = floor($file_size);

             if ($res === TRUE) {
                 $zip->addFile($file_origin, $file_name);
             }

        }else{
            continue;
        }

        $work_file_list[] = array(
            "file_path" => $file_path,
            "file_name" => $file_name,
            "file_size" => $file_size." KB"
        );
    }

    if ($res === TRUE) {
        $zip->close();
    }

}else{
    $file_names = [];
    $file_paths = [];
}

$total_count = empty($work_file_list) ? 0 : count($work_file_list);

$smarty->assign("file_count", $total_count);
$smarty->assign("w_no", $w_no);
$smarty->assign("w_type", $w_type);
$smarty->assign("work_file_list", $work_file_list);
$smarty->assign("zip_file_name", $zip_file_name);
$smarty->assign("zip_file_path", $zip_file_path);

$smarty->display('work_list.file_download.html');
?>
