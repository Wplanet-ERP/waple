<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/asset.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");


$lfcr = chr(10);
//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "as_no")
	->setCellValue('B1', "등록일시")
	->setCellValue('C1', "진행상태")
	->setCellValue('D1', "사용자")
	->setCellValue('E1', "소유권 업체명")
	->setCellValue('F1', "그룹1")
	->setCellValue('G1', "그룹2")
	->setCellValue('H1', "자산관리번호")
	->setCellValue('I1', "자산명")
	->setCellValue('J1', "자산관리자")
	->setCellValue('K1', "제품명 및 자산설명")
	->setCellValue('L1', "사용방식")
	->setCellValue('M1', "자산형태")
	->setCellValue('N1', "유형자산위치")
	->setCellValue('O1', "무형자산정보")
	->setCellValue('P1', "무형(계정)정보")
	->setCellValue('Q1', "공유형태")
	->setCellValue('R1', "구입업체")
	->setCellValue('S1', "제조사")
	->setCellValue('T1', "취득일")
	->setCellValue('U1', "보증(갱신)만료일")
	->setCellValue('V1', "취득원가{$lfcr}(VAT포함)")
	->setCellValue('W1', "비고")
	->setCellValue('X1', "담당자 메모")
;

# 검색조건
$add_where = "1=1";

# 자산관리 상품 처리
$sch_asset_g1   = isset($_GET['sch_asset_g1']) ? $_GET['sch_asset_g1'] : "";
$sch_asset_g2   = isset($_GET['sch_asset_g2']) ? $_GET['sch_asset_g2'] : "";
$sch_asset      = isset($_GET['sch_asset']) ? $_GET['sch_asset'] : "";

if (!empty($sch_asset)) {
    $add_where .= " AND as.name like '%{$sch_asset}%'";
}

if($sch_asset_g2){
    $add_where .= " AND as.k_name_code ='{$sch_asset_g2}'";
}elseif($sch_asset_g1){
    $add_where .= " AND (SELECT k_parent FROM kind k WHERE k.k_name_code=`as`.k_name_code) ='".$sch_asset_g1."'";
}

$sch_asset_state    = isset($_GET['sch_asset_state']) ? $_GET['sch_asset_state'] : "";
$sch_my_c_name      = isset($_GET['sch_my_c_name']) ? $_GET['sch_my_c_name'] : "";
$sch_keyword        = isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";
$sch_as_no          = isset($_GET['sch_as_no']) ? $_GET['sch_as_no'] : "";
$sch_management     = isset($_GET['sch_management']) ? $_GET['sch_management'] : "";
$sch_manager_name   = isset($_GET['sch_manager_name']) ? $_GET['sch_manager_name'] : "";
$sch_description    = isset($_GET['sch_description']) ? $_GET['sch_description'] : "";
$sch_use_type       = isset($_GET['sch_use_type']) ? $_GET['sch_use_type'] : "";
$sch_asset_loc      = isset($_GET['sch_asset_loc']) ? $_GET['sch_asset_loc'] : "";
$sch_object_type    = isset($_GET['sch_object_type']) ? $_GET['sch_object_type'] : "";
$sch_share_type     = isset($_GET['sch_share_type']) ? $_GET['sch_share_type'] : "";
$sch_dp_c_name      = isset($_GET['sch_dp_c_name']) ? $_GET['sch_dp_c_name'] : "";
$sch_manufacturer   = isset($_GET['sch_manufacturer']) ? $_GET['sch_manufacturer'] : "";
$sch_get_s_date     = isset($_GET['sch_get_s_date']) ? $_GET['sch_get_s_date'] : "";
$sch_get_e_date     = isset($_GET['sch_get_e_date']) ? $_GET['sch_get_e_date'] : "";
$sch_expire_s_date  = isset($_GET['sch_expire_s_date']) ? $_GET['sch_expire_s_date'] : "";
$sch_expire_e_date  = isset($_GET['sch_expire_e_date']) ? $_GET['sch_expire_e_date'] : "";
$sch_notice         = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$sch_memo           = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
$sch_use_pw         = isset($_GET['sch_use_pw']) ? $_GET['sch_use_pw'] : "";

if(!empty($sch_asset_state))
{
    $add_where .= " AND `as`.asset_state = '{$sch_asset_state}'";
}

if(!empty($sch_my_c_name))
{
    $add_where .= " AND `as`.my_c_no IN(SELECT mc.my_c_no FROM my_company mc WHERE mc.c_name LIKE '%{$sch_my_c_name}%')";
}

if(!empty($sch_keyword))
{
    $add_where .= " AND `as`.keyword like '%{$sch_keyword}%'";
}

if(!empty($sch_as_no))
{
    $add_where .= " AND `as`.as_no = '{$sch_as_no}'";
}

if(!empty($sch_management))
{
    $add_where .= " AND `as`.management LIKE '%{$sch_management}%'";
}

if(!empty($sch_manager_name))
{
    $add_where .= " AND `as`.manager_name LIKE '%{$sch_manager_name}%'";
}

if(!empty($sch_description))
{
    $add_where .= " AND `as`.description LIKE '%{$sch_description}%'";
}

if(!empty($sch_use_type))
{
    $add_where .= " AND `as`.use_type = '{$sch_use_type}'";
}

if(!empty($sch_asset_loc))
{
    $add_where .= " AND `as`.asset_loc LIKE '%{$sch_asset_loc}%'";
}

if(!empty($sch_object_type))
{
    $add_where .= " AND `as`.object_type = '{$sch_object_type}'";
}

if(!empty($sch_share_type))
{
    $add_where .= " AND `as`.share_type = '{$sch_share_type}'";
}

if(!empty($sch_dp_c_name))
{
    $add_where .= " AND `as`.dp_c_name LIKE '%{$sch_dp_c_name}%'";
}

if(!empty($sch_manufacturer))
{
    $add_where .= " AND `as`.manufacturer LIKE '%{$sch_manufacturer}%'";
}

if(!empty($sch_get_s_date))
{
    $add_where .= " AND `as`.get_date >= '{$sch_get_s_date}'";
}

if(!empty($sch_get_e_date))
{
    $add_where .= " AND `as`.get_date <= '{$sch_get_e_date}'";
}

if(!empty($sch_expire_s_date))
{
    $add_where .= " AND `as`.expire_date >= '{$sch_expire_s_date}'";
}

if(!empty($sch_expire_e_date))
{
    $add_where .= " AND `as`.expire_date <= '{$sch_expire_e_date}'";
}

if(!empty($sch_notice))
{
    $add_where .= " AND `as`.notice LIKE '%{$sch_notice}%'";
}

if(!empty($sch_memo))
{
    $add_where .= " AND `as`.memo LIKE '%{$sch_memo}%'";
}

if(!empty($sch_use_pw))
{
    if($sch_use_pw == '1'){
        $add_where .= " AND `as`.pw_date != ''";
    }elseif($sch_use_pw == '2'){
        $add_where .= " AND (`as`.pw_date is null OR `as`.pw_date = '')";
    }
}

# 리스트 쿼리
$asset_sql  = "
    SELECT 
        *,
        DATE_FORMAT(`as`.regdate, '%Y-%m-%d %H:%i') as reg_date,
        (SELECT mc.c_name FROM my_company as mc WHERE mc.my_c_no = `as`.my_c_no) as my_c_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=`as`.k_name_code)) AS k_asset1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=`as`.k_name_code) AS k_asset2_name,
        DATE_FORMAT(`as`.get_date, '%Y/%m/%d') as get_date,
        DATE_FORMAT(`as`.expire_date, '%Y/%m/%d') as expire_date,
        (SELECT req_name FROM asset_reservation ar WHERE ar.as_no = `as`.as_no AND ar.work_state='2' AND `as`.asset_state='5' ORDER BY ar.as_r_no DESC LIMIT 1) as use_info,
        (SELECT count(as_r_no) FROM asset_reservation ar WHERE ar.as_no = `as`.as_no AND ar.work_state='2' AND ar.req_no='{$session_s_no}') as ar_cnt
    FROM asset `as` 
    WHERE {$add_where} 
    ORDER BY `as`.as_no DESC
";
$asset_query = mysqli_query($my_db, $asset_sql);
$idx         = 2;
$asset_state_option         = getAssetStateOption();
$asset_use_type_option      = getAssetUseTypeOption();
$asset_share_type_option    = getAssetShareTypeOption();
$asset_object_type_option   = getAssetObjectTypeOption();
while($asset = mysqli_fetch_array($asset_query))
{
    $asset_state     = isset($asset['asset_state']) && !empty($asset['asset_state']) ? $asset['asset_state'] : "";
    $use_type        = isset($asset['use_type']) && !empty($asset['use_type']) ? $asset['use_type'] : "";
    $share_type      = isset($asset['share_type']) && !empty($asset['share_type']) ? $asset['share_type'] : "";
    $object_type     = isset($asset['object_type']) && !empty($asset['object_type']) ? $asset['object_type'] : "";

    $asset['asset_state_name']  = !empty($asset_state) ? $asset_state_option[$asset_state]['label'] : "";
    $asset['use_type_name']     = !empty($use_type) ? $asset_use_type_option[$use_type] : "";
    $asset['share_type_name']   = !empty($share_type) ? $asset_share_type_option[$share_type] : "";
    $asset['object_type_name']  = !empty($object_type) ? $asset_object_type_option[$object_type] : "";

    $asset['object_type_1']  = ($object_type == '1') ? $asset['asset_loc'] : "";
    $asset['object_type_2']  = ($object_type == '2') ? $asset['asset_loc'] : "";
    $asset['object_type_3']  = ($object_type == '3') ? "{$asset['object_site']}\r\n{$asset['object_url']}\r\n{$asset['object_id']}\r\r{$asset['object_pw']}" : "";

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$idx}", $asset['as_no'])
        ->setCellValue("B{$idx}", $asset['reg_date'])
        ->setCellValue("C{$idx}", $asset['asset_state_name'])
        ->setCellValue("D{$idx}", $asset['use_info'])
        ->setCellValue("E{$idx}", $asset['my_c_name'])
        ->setCellValue("F{$idx}", $asset['k_asset1_name'])
        ->setCellValue("G{$idx}", $asset['k_asset2_name'])
        ->setCellValue("H{$idx}", $asset['management'])
        ->setCellValue("I{$idx}", $asset['name'])
        ->setCellValue("J{$idx}", $asset['manager_name'])
        ->setCellValue("K{$idx}", $asset['description'])
        ->setCellValue("L{$idx}", $asset['use_type_name'])
        ->setCellValue("M{$idx}", $asset['object_type_name'])
        ->setCellValue("N{$idx}", $asset['object_type_1'])
        ->setCellValue("O{$idx}", $asset['object_type_2'])
        ->setCellValue("P{$idx}", $asset['object_type_3'])
        ->setCellValue("Q{$idx}", $asset['share_type_name'])
        ->setCellValue("R{$idx}", $asset['dp_c_name'])
        ->setCellValue("S{$idx}", $asset['manufacturer'])
        ->setCellValue("T{$idx}", $asset['get_date'])
        ->setCellValue("U{$idx}", $asset['expire_date'])
        ->setCellValue("V{$idx}", number_format($asset['cost'], 0))
        ->setCellValue("W{$idx}", $asset['notice'])
        ->setCellValue("X{$idx}", $asset['memo'])
    ;
    $idx++;
}
$idx--;

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');

$objPHPExcel->getActiveSheet()->getStyle('A1:X1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:X1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');

$objPHPExcel->getActiveSheet()->getStyle("A1:X{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:X{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:X{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("S2:S{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("V2:V{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("W2:W{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("X2:X{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("V1")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("B2:B{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("W2:W{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("X2:X{$idx}")->getAlignment()->setWrapText(true);

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);
$objPHPExcel->getActiveSheet()->getStyle("A1:X{$idx}")->applyFromArray($styleArray);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(20);

$objPHPExcel->getActiveSheet()->setTitle('자산리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR', date('Y-m-d')."_자산리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
