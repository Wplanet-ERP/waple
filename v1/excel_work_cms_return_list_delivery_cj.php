<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '3G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "예약구분")
	->setCellValue('B1', "집하예정일")
	->setCellValue('C1', "보내는분성명")
    ->setCellValue('D1', "보내는분전화번호")
    ->setCellValue('E1', "보내는분기타연락처")
	->setCellValue('F1', "보내는분우편번호")
	->setCellValue('G1', "보내는분주소(전체,분할)")
	->setCellValue('H1', "받는분성명")
	->setCellValue('I1', "받는분전화번호")
	->setCellValue('J1', "받는분기타연락처")
	->setCellValue('K1', "받는분우편번호")
	->setCellValue('L1', "받는분주소(전체,분할)")
	->setCellValue('M1', "운송장번호")
	->setCellValue('N1', "고객주문번호")
	->setCellValue('O1', "품목명")
	->setCellValue('P1', "박스수량")
	->setCellValue('Q1', "박스타입")
	->setCellValue('R1', "기본운임")
	->setCellValue('S1', "배송메세지1")
	->setCellValue('T1', "배송메세지2")
;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');

$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00808080');
$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:T1")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("A1:T1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:T1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:T1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


# 검색조건 처리
$add_where          = "1=1";
$sch_req_s_date 	= isset($_GET['sch_req_s_date']) ? $_GET['sch_req_s_date'] : "";
$sch_req_e_date 	= isset($_GET['sch_req_e_date']) ? $_GET['sch_req_e_date'] : "";
$sch_return_state   = isset($_GET['sch_return_state']) ? $_GET['sch_return_state'] : "";

if(!empty($sch_req_s_date)){
    $add_where .= " AND r.return_req_date >= '{$sch_req_s_date}'";
}

if(!empty($sch_req_e_date)){
    $add_where .= " AND r.return_req_date <= '{$sch_req_e_date}'";
}

if(!empty($sch_return_state)){
    $add_where .= " AND r.return_state = '{$sch_return_state}'";
}

$add_orderby = "r.r_no DESC, r.prd_no ASC";


$cms_ord_sql    = "SELECT DISTINCT r.order_number, count(r_no) as r_count FROM work_cms_return r WHERE {$add_where} AND r.order_number is not null GROUP BY r.order_number ORDER BY {$add_orderby} LIMIT 10000";
$cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);

$order_number_list  = [];
$order_count_list   = [];
$return_unit_list   = [];
$lfcr               = chr(10);
while($order_number = mysqli_fetch_assoc($cms_ord_query)){
	$order_number_list[] =  "'".$order_number['order_number']."'";
    $order_count_list[$order_number['order_number']] = $order_number['r_count'];
}

$order_numbers = implode(',', $order_number_list);

// 리스트 쿼리
$return_sql = "
	SELECT
	    r.return_req_date,
	    r.return_state,
		r.parent_order_number,
	    r.order_number,
		r.recipient,
		r.recipient_hp,
		r.recipient_addr,
	    (SELECT title from product_cms prd_cms where prd_cms.prd_no=r.prd_no) as prd_name,
	    r.return_type,
	    r.req_memo
	FROM
		work_cms_return r
	WHERE {$add_where} AND r.order_number IN({$order_numbers})
	GROUP BY order_number
	ORDER BY r_no ASC
";
$return_query	= mysqli_query($my_db, $return_sql);
$idx            = 2;
if(!!$return_query)
{
    while($work_cms = mysqli_fetch_array($return_query))
    {
        $recipient_addr_val = trim($work_cms['recipient_addr']);
        $recipient_addr     = preg_replace('/\r\n|\r|\n/','',$recipient_addr_val);

        $objPHPExcel->setActiveSheetIndex(0)
			->setCellValue("C{$idx}", $work_cms['recipient'])
			->setCellValue("D{$idx}", $work_cms['recipient_hp'])
            ->setCellValue("G{$idx}", $recipient_addr)
			->setCellValueExplicit("N{$idx}", $work_cms['order_number'])
			->setCellValue("O{$idx}", $work_cms['prd_name'])
        ;

        $idx++;
    }
}
$idx--;

// Work Sheet Width & alignment
$objPHPExcel->getActiveSheet()->getStyle("A2:T{$idx}")->getFont()->setSize(7);;
$objPHPExcel->getActiveSheet()->getStyle("A2:T{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A2:T{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("G2:G{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("S2:S{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("T2:T{$idx}")->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(14);

$objPHPExcel->getActiveSheet()->setTitle("회수 EMP 발주등록 양식");
$objPHPExcel->getActiveSheet()->getStyle("A1:T{$idx}")->applyFromArray($styleArray);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_택배사 회수접수(cj).xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
