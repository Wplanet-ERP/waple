<?php
require('inc/common.php');
require('ckadmin.php');

$pj_no      = isset($_GET['pj_no']) ? $_GET['pj_no'] : "";
$pj_conf_no = isset($_GET['pj_conf_no']) ? $_GET['pj_conf_no'] : "";

$project_name       = $confirm_content = "";
$confirm_name       = "확인용";
$confirm_department = "소속";
$confirm_position   = "직위";
$confirm_hp         = "010-0101-0101";
$confirm_tel        = "02-0220-0202";
$confirm_email      = "test@wplanet.co.kr";
$pj_c_name          = "확 인 용";

$cur_date = date('Y.m.d');
if(!empty($pj_no))
{
    $project_sql = "SELECT pj_name, confirm_content FROM project WHERE pj_no='{$pj_no}'";
    $project_query = mysqli_query($my_db, $project_sql);
    $project = mysqli_fetch_assoc($project_query);

    $project_name    = isset($project['pj_name']) ? $project['pj_name'] : "";
    $confirm_content = isset($project['confirm_content']) ? $project['confirm_content'] : "";
}

$confirm_data = array(
  "pj_name"             => $project_name,
  "confirm_content"     => $confirm_content,
  "confirm_name"        => $confirm_name,
  "confirm_department"  => $confirm_department,
  "confirm_position"    => $confirm_position,
  "confirm_hp"          => $confirm_hp,
  "confirm_tel"         => $confirm_tel,
  "confirm_email"       => $confirm_email,
  "cur_date"            => $cur_date,
  "pj_c_name"           => $pj_c_name,
);

$smarty->assign($confirm_data);
$smarty->display('project_regist_external_participation_confirm.html');

?>
