<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/personal_expenses.php');
require('inc/model/Work.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

$work_model     = Work::Factory();
$to_date        = date("Y-m-d");
$deadline_list  = $work_model->getDeadLine();
$deadline_date  = ""; // 제출마감일
$deadline_d_day = ""; // 제출마감일 남은 일수
$deadline_w_no  = ""; // 제출마감일 업무번호

foreach($deadline_list as $deadline_token)
{
    $deadline 		= $deadline_token['dday'];
    $deadline_w_no 	= $deadline_token['w_no'];
    $gap = (strtotime($to_date) - strtotime($deadline))/60/60/24; // 문자형 날짜를 초로 계산하여 일자로 변환

    if($gap >= -19 && $gap <= 14){ // 18일 이전 14일이 지나지 않은 날짜를 제출마감일로 정함
        $deadline_date  = $deadline;
        $deadline_d_day = $gap;
        break;
    }
}
$pre_month_deadline_date = date("Y-m",strtotime ("-1 month", strtotime($deadline_date))); // 제출마감일의 전달

# 검색쿼리
$add_where              = " pe.display = 1";
$sch_pe_no              = isset($_GET['sch_pe_no']) ? $_GET['sch_pe_no'] : "";
$sch_req_s_name         = isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : $session_name;
$sch_state              = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_team               = isset($_GET['sch_team']) ? $_GET['sch_team'] : $session_team;
$sch_share_card         = isset($_GET['sch_share_card']) ? $_GET['sch_share_card'] : "";
$sch_s_month            = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : $pre_month_deadline_date; // 월간
$sch_e_month            = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : $pre_month_deadline_date; // 월간
$sch_month_all          = isset($_GET['sch_month_all']) ? $_GET['sch_month_all'] : ""; // 월간(전체)
$sch_payment_date       = isset($_GET['sch_payment_date']) ? $_GET['sch_payment_date'] : "";
$sch_account_code       = isset($_GET['sch_account_code']) ? $_GET['sch_account_code'] : "";
$sch_wd_method          = isset($_GET['sch_wd_method']) ? $_GET['sch_wd_method'] : "";
$sch_card_num           = isset($_GET['sch_card_num']) ? $_GET['sch_card_num'] : "";
$sch_payment_contents   = isset($_GET['sch_payment_contents']) ? $_GET['sch_payment_contents'] : "";
$sch_corp_my_c_no       = isset($_GET['sch_corp_my_c_no']) ? $_GET['sch_corp_my_c_no'] : "";
$sch_fran_c_name        = isset($_GET['sch_fran_c_name']) ? $_GET['sch_fran_c_name'] : "";

if(!empty($sch_corp_my_c_no)){
    if($sch_corp_my_c_no == 'default'){
        $add_where .= " AND pe.my_c_no IN(1,2,5,7)";
    }elseif($sch_corp_my_c_no == 'commerce'){
        $add_where .= " AND pe.my_c_no = 3";
    }
}

if (!empty($sch_pe_no)) {
    $add_where .= " AND pe.pe_no='{$sch_pe_no}'";
}

if (!empty($sch_req_s_name)) {
    $add_where .= " AND pe.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_req_s_name}')";
}

if (!empty($sch_state)) {
    $add_where .= " AND pe.state='{$sch_state}'";
}

if(!empty($sch_team)) {
    if ($sch_team != "all")
    {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        $add_where          .= " AND pe.team IN ({$sch_team_code_where})";
    }
    elseif($sch_team == "all" && $session_s_no == '62')
    {
        if($sch_account_code != '03011'){
            $add_where .= " AND pe.team IN('00251','00252','00254')";
        }
    }
}

if (!empty($sch_share_card)) {
    $add_where .= " AND pe.corp_card_no IN (SELECT `cc`.cc_no FROM corp_card `cc` WHERE `cc`.share_card like '%{$sch_share_card}%')";
}

if($sch_month_all != 'all') {
    if (!empty($sch_s_month)) {
        $add_where .= " AND DATE_FORMAT(pe.payment_date, '%Y-%m') >= '{$sch_s_month}'";
    }

    if (!empty($sch_e_month)) {
        $add_where .= " AND DATE_FORMAT(pe.payment_date, '%Y-%m') <= '{$sch_e_month}' ";
    }
}

if (!empty($sch_payment_date)) {
    $add_where .= " AND pe.payment_date='{$sch_payment_date}'";
}

if (!empty($sch_account_code)) {
    $add_where .= " AND pe.account_code='{$sch_account_code}'";
}

if (!empty($sch_wd_method)) {
    $add_where .= " AND pe.wd_method='{$sch_wd_method}'";
}

if (!empty($sch_card_num)) {
    $add_where .= " AND pe.card_num LIKE '%{$sch_card_num}%'";
}

if (!empty($sch_payment_contents)) {
    $sch_payment_contents_list      = explode(" ", $sch_payment_contents);
    $sch_payment_contents_trim_list = [];

    foreach($sch_payment_contents_list as $sch_payment_contents_word){
        $sch_payment_contents_trim_list[] = trim($sch_payment_contents_word);
    }
    $sch_payment_contents_trim = implode("",$sch_payment_contents_trim_list);

    $add_where .= " AND (pe.payment_contents like '%{$sch_payment_contents}%' OR pe.payment_contents like '%{$sch_payment_contents_trim}%')";
}

if (!empty($sch_fran_c_name)) {
    $add_where .= " AND pe.fran_c_name LIKE '%{$sch_fran_c_name}%'";
}

// 정렬순서 토글 & 필드 지정
$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "priority";
$ori_ord_type   = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "";
$ord_type_by    = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "1";
$add_order_by   = "";
$order_by_val   = "";
if(!empty($ord_type))
{
    if(!empty($ord_type_by))
    {
        if($ori_ord_type == $ord_type)
        {
            if($ord_type_by == '1'){
                $order_by_val = "ASC";
            }elseif($ord_type_by == '2'){
                $order_by_val = "DESC";
            }
        }else{
            $ord_type_by  = '1';
            $order_by_val = "ASC";
        }

        $add_order_by .= "ISNULL({$ord_type}) ASC, {$ord_type} {$order_by_val}, pe_no ASC";
    }
}

$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:S1");
$objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFont()->setSize(18);
$objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getFont()->setBold(true);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A1", "개인경비 리스트");
$objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:S1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight('40');

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue("A2", "Pe_no")
->setCellValue("B2", "순서")
->setCellValue("C2", "카드번호")
->setCellValue("D2", "이름")
->setCellValue("E2", "국내외 사용구분")
->setCellValue("F2", "승인번호")
->setCellValue("G2", "승인일자")
->setCellValue("H2", "승인시간")
->setCellValue("I2", "매출종류")
->setCellValue("J2", "승인금액")
->setCellValue("K2", "거래금액")
->setCellValue("L2", "부가세")
->setCellValue("M2", "거래금액(외화)")
->setCellValue("N2", "가맹점명")
->setCellValue("O2", "가맹점 주소")
->setCellValue("P2", "가맹점사업번호")
->setCellValue("Q2", "청구금액")
->setCellValue("R2", "결제수단")
->setCellValue("S2", "계정과목1")
->setCellValue("T2", "계정과목2")
->setCellValue("U2", "내용");

$objPHPExcel->getActiveSheet()->getStyle("A2:U2")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB("00BFBFBF");

# 리스트 쿼리
$personal_expenses_sql = "
	SELECT
		pe.*,
		(SELECT s_name FROM staff s where s.s_no=pe.s_no) AS s_name,
		(SELECT k2.k_name FROM kind k2 WHERE k2.k_name_code=(SELECT k1.k_parent FROM kind k1 WHERE k1.k_name_code=pe.account_code)) AS account_code_g1_name,
		(SELECT k_name FROM kind WHERE k_name_code=pe.account_code) AS account_code_g2_name
	FROM personal_expenses pe
	WHERE {$add_where}
	ORDER BY {$add_order_by}
";
$personal_expenses_query        = mysqli_query($my_db, $personal_expenses_sql);
$personal_expenses_state_option = getPeStateOption();
$wd_method_name_option          = getPeWdMethodOption();
$idx    = 1;
$pe_idx = 3;
while($data = mysqli_fetch_array($personal_expenses_query)){
    if(!empty($data['payment_date'])) {
        $pay_day_value = date("Y-m-d",strtotime($data['payment_date']));
        $pay_time_value = date("H:i",strtotime($data['payment_date']));
    }else{
        $pay_day_value  = "";
        $pay_time_value = "";
    }

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue("A{$pe_idx}", $data['pe_no'])
	->setCellValue("B{$pe_idx}", $idx)
	->setCellValueExplicit("C{$pe_idx}", $data['card_num'], PHPExcel_Cell_DataType::TYPE_STRING)
	->setCellValue("D{$pe_idx}", $data['s_name'])
	->setCellValue("E{$pe_idx}", $data['inout_type'])
	->setCellValueExplicit("F{$pe_idx}", $data['appr_no'], PHPExcel_Cell_DataType::TYPE_STRING)
	->setCellValue("G{$pe_idx}", $pay_day_value)
	->setCellValue("H{$pe_idx}", $pay_time_value)
	->setCellValue("I{$pe_idx}", $data['sales_type'])
	->setCellValue("J{$pe_idx}", $data['appr_price'])
	->setCellValue("K{$pe_idx}", $data['pay_price'])
	->setCellValue("L{$pe_idx}", $data['tax_price'])
	->setCellValue("M{$pe_idx}", $data['pay_out_price'])
	->setCellValue("N{$pe_idx}", $data['fran_c_name'])
	->setCellValue("O{$pe_idx}", $data['fran_c_addr'])
	->setCellValue("P{$pe_idx}", $data['fran_no'])
	->setCellValue("Q{$pe_idx}", $data['money'])
	->setCellValue("R{$pe_idx}", $wd_method_name_option[$data['wd_method']])
	->setCellValue("S{$pe_idx}", $data['account_code_g1_name'])
	->setCellValue("T{$pe_idx}", $data['account_code_g2_name'])
	->setCellValue("U{$pe_idx}", $data['payment_contents']);

    $pe_idx++;
    $idx++;
}
$pe_idx--;

$objPHPExcel->getActiveSheet()->getStyle("A1:U{$pe_idx}")->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A2:U{$pe_idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A3:U{$pe_idx}")->getFont()->setSize(10);

$objPHPExcel->getActiveSheet()->getStyle("A2:A{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("B2:B{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("C2:C{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("D2:D{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("E2:E{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("F2:F{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("G2:G{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("H2:H{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("K2:K{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("L2:L{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("M2:M{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("Q2:Q{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("R2:R{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("S2:S{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("T2:T{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("U2:U{$pe_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("J")->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension("K")->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension("L")->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension("M")->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension("N")->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension("O")->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension("P")->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension("Q")->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension("R")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("S")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("T")->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension("U")->setWidth(35);
$objPHPExcel->getActiveSheet()->setTitle("개인경비 리스트");


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="'.$today.'_개인경비 리스트(제출용).xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
