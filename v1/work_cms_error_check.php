<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/work_cms.php');
require('inc/model/ProductCms.php');
require('inc/model/WorkCms.php');
require('inc/model/Custom.php');
require('Classes/PHPExcel.php');

if($session_team != "00244" && $session_s_no != 167 && $session_s_no != 9){
    echo "접근할 수 없습니다.";
    exit;
}

# 엑셀 파일 읽기
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$cms_model      = WorkCms::Factory();
$error_model    = Custom::Factory();
$error_model->setMainInit("work_cms_error", "order_number");

if($process == "self_add_error_file")
{
    $error_type     = isset($_POST['error_type']) ? $_POST['error_type'] : "1";

    $file_name      = $_FILES["error_file"]["tmp_name"];
    $excelReader    =   PHPExcel_IOFactory::createReaderForFile($file_name);
    $excelReader->setReadDataOnly(true);

    $excel              = $excelReader->load($file_name);
    $excel->setActiveSheetIndex(0);
    $objWorksheet       = $excel->getActiveSheet();
    $totalRow           = $objWorksheet->getHighestRow();
    $totalOrder         = $totalRow;
    $order_excel_list   = [];
    $continue_ord_list  = [];
    $regdate            = date("Y-m-d H:i:s");

    if($error_type == "1" || $error_type == "2")
    {
        $totalOrder    -= 1;
        $order_info     = ($error_type == "1") ? "아임웹 취소요청" : "아임웹 환불요청";
        $is_exist_order = 1;

        for ($i = 2; $i <= $totalRow; $i++)
        {
            $order_number  = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //주문번호
            $shop_ord_no   = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //품목주문번호

            if(empty($order_number)){
                continue;
            }

            $chk_ord_sql    = "SELECT * FROM work_cms WHERE order_number='{$order_number}' AND shop_ord_no ='{$shop_ord_no}' AND delivery_state IN(6,8) LIMIT 1";
            $chk_ord_query  = mysqli_query($my_db, $chk_ord_sql);
            $chk_ord_result = mysqli_fetch_assoc($chk_ord_query);
            if(!isset($chk_ord_result['order_number']) && empty($chk_ord_result['order_number']))
            {
                $chk_reservation_sql    = "SELECT * FROM work_cms_reservation WHERE order_number='{$order_number}' AND shop_ord_no ='{$shop_ord_no}' AND delivery_state != '4' LIMIT 1";
                $chk_reservation_query  = mysqli_query($my_db, $chk_reservation_sql);
                $chk_reservation_result = mysqli_fetch_assoc($chk_reservation_query);
                if(!isset($chk_reservation_result['order_number']) && empty($chk_reservation_result['order_number']))
                {
                    $chk_ord_cnt_sql    = "SELECT COUNT(*) as ord_cnt FROM work_cms WHERE order_number='{$order_number}' AND shop_ord_no ='{$shop_ord_no}'";
                    $chk_ord_cnt_query  = mysqli_query($my_db, $chk_ord_cnt_sql);
                    $chk_ord_cnt_result = mysqli_fetch_assoc($chk_ord_cnt_query);
                    if(isset($chk_ord_cnt_result['ord_cnt']) && $chk_ord_cnt_result['ord_cnt'] > 0) {
                        $continue_ord_list[] = array("ord_no" => $order_number, "shop_ord_no" => $shop_ord_no);
                        continue;
                    }else{
                        $is_exist_order = 2;
                    }
                }
            }

            $order_excel_list[$order_number] = array("order_number" => $order_number, "shop_ord_no" => $shop_ord_no , "order_info" => $order_info, "error_type" => $error_type, "is_exist_order" => $is_exist_order, "regdate" => $regdate);
        }
    }
    elseif($error_type == "4" || $error_type == "5" || $error_type == "6")
    {
        $totalOrder    -= 1;
        $order_info     = ($error_type == "4") ? "스마트스토어 취소요청" : ($error_type == "5") ? "스마트스토어 환불요청" : "스마트스토어 환불수거";
        $is_exist_order = 1;

        for ($i = 2; $i <= $totalRow; $i++)
        {
            $order_number   = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   //주문번호
            $shop_ord_no    = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //품목주문번호

            if(empty($order_number)){
                continue;
            }

            $chk_ord_sql    = "SELECT * FROM work_cms WHERE order_number='{$order_number}' AND shop_ord_no ='{$shop_ord_no}' AND delivery_state IN(6,8) LIMIT 1";
            $chk_ord_query  = mysqli_query($my_db, $chk_ord_sql);
            $chk_ord_result = mysqli_fetch_assoc($chk_ord_query);

            if(!isset($chk_ord_result['order_number']) && empty($chk_ord_result['order_number']))
            {
                $chk_reservation_sql    = "SELECT * FROM work_cms_reservation WHERE order_number='{$order_number}' AND shop_ord_no ='{$shop_ord_no}' AND delivery_state != '4' LIMIT 1";
                $chk_reservation_query  = mysqli_query($my_db, $chk_reservation_sql);
                $chk_reservation_result = mysqli_fetch_assoc($chk_reservation_query);
                if(!isset($chk_reservation_result['order_number']) && empty($chk_reservation_result['order_number']))
                {
                    $chk_ord_cnt_sql    = "SELECT COUNT(*) as ord_cnt FROM work_cms WHERE order_number='{$order_number}' AND shop_ord_no ='{$shop_ord_no}'";
                    $chk_ord_cnt_query  = mysqli_query($my_db, $chk_ord_cnt_sql);
                    $chk_ord_cnt_result = mysqli_fetch_assoc($chk_ord_cnt_query);
                    if(isset($chk_ord_cnt_result['ord_cnt']) && $chk_ord_cnt_result['ord_cnt'] > 0) {
                        $continue_ord_list[] = array("ord_no" => $order_number, "shop_ord_no" => $shop_ord_no);
                        continue;
                    }else{
                        $is_exist_order = 2;
                    }
                }
            }

            $order_excel_list[$order_number] = array("order_number" => $order_number, "shop_ord_no" => $shop_ord_no ,"order_info" => $order_info, "error_type" => $error_type, "is_exist_order" => $is_exist_order, "regdate" => $regdate);
        }
    }
    elseif($error_type == "3")
    {
        for ($i = 1; $i <= $totalRow; $i++)
        {
            $order_number_val   = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   //주문번호

            if(empty($order_number_val)){
                continue;
            }

            $order_number_val = str_replace("\xC2\xA0", ' ', $order_number_val);
            $order_number_val = preg_replace('/\xC2\xA0/', ' ', $order_number_val);
            $order_number_val = preg_replace('/\xA0/u', ' ', $order_number_val);

            $order_data         = explode(" ", $order_number_val, 2);
            $order_number       = trim($order_data[0]);
            $order_info         = trim($order_data[1]);

            $order_excel_list[] = array("order_number" => $order_number, "order_info" => $order_info, "error_type" => $error_type, "regdate" => $regdate);
        }
    }

    $not_add_cnt    = count($continue_ord_list);
    $add_cnt        = $totalOrder - $not_add_cnt;

    if($error_model->multiInsert($order_excel_list))
    {
        exit("<script>alert('총 {$totalOrder}건 중 {$add_cnt}건 등록했습니다');location.href='work_cms_error_check.php?error_type={$error_type}';</script>");
    }else{
        $error_msg = "등록 실패했습니다.";
        if($add_cnt == 0){
            $error_msg = "등록 할 건이 없습니다.";
        }
        exit("<script>alert('{$error_msg}');location.href='work_cms_error_check.php?error_type={$error_type}';</script>");
    }
}
elseif($process == "del_error_file")
{
    $error_type = isset($_POST['error_type']) ? $_POST['error_type'] : "";
    $add_where  = !empty($error_type) ? "WHERE error_type='{$error_type}'" : "";

    $del_sql = "DELETE FROM work_cms_error {$add_where}";
    mysqli_query($my_db, $del_sql);

    exit("<script>alert('삭제 했습니다');location.href='work_cms_error_check.php?error_type={$error_type};</script>");
}
elseif($process == "delivery_state")
{
    $w_no 		= (isset($_POST['w_no'])) ? $_POST['w_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$cms_model->update(array("w_no" => $w_no, "delivery_state" => $value)))
        echo "발송 진행상태 변경에 실패 했습니다.";
    else
        echo "발송 진행상태 변경에 성공 했습니다.";
    exit;
}

$view_order_list    = [];
$error_type         = isset($_GET['error_type']) ? $_GET['error_type'] : 3;
$add_where          = "1=1 AND sub.error_type='{$error_type}'";

$ord_chk_sql    = "
    SELECT 
       *,
       (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name, 
       (SELECT sub.order_info from work_cms_error sub where sub.order_number=w.order_number AND {$add_where} LIMIT 1) as ord_info,
       (SELECT sub.error_type from work_cms_error sub where sub.order_number=w.order_number AND {$add_where} LIMIT 1) as error_type
    FROM work_cms w 
    WHERE order_number IN(SELECT order_number FROM work_cms_error sub WHERE {$add_where}) ORDER BY regdate DESC
";
$ord_chk_query                  = mysqli_query($my_db, $ord_chk_sql);
$delivery_state_color_list      = getDeliveryStateColorOption();
$delivery_state_color_list[8]   = array('label' => '운송장완료', 'bg' => 'yellow', 'color' => 'blue');
while($ord_chk_result = mysqli_fetch_assoc($ord_chk_query))
{
    $delivery_state_color   = isset($delivery_state_color_list[$ord_chk_result['delivery_state']]) ? $delivery_state_color_list[$ord_chk_result['delivery_state']]['color'] : "black";
    $delivery_state_bg      = isset($delivery_state_color_list[$ord_chk_result['delivery_state']]) ? $delivery_state_color_list[$ord_chk_result['delivery_state']]['bg'] : "white";

    $ord_chk_result['delivery_state_color'] = $delivery_state_color;
    $ord_chk_result['delivery_state_bg']    = $delivery_state_bg;

    $link_url = "";
    switch($ord_chk_result["error_type"]){
        case "1":
            $link_url = "https://belabef.com/admin/shopping/cancel/?q=YjowOw%3D%3D&keyword={$ord_chk_result['order_number']}&page=1&reset_status=Y&is_all_status_search=Y";
            break;
        case "2":
            $link_url = "https://belabef.com/admin/shopping/return/?q=YjowOw%3D%3D&keyword={$ord_chk_result['order_number']}&page=1&reset_status=Y&is_all_status_search=Y";
            break;
        case "3":
            $link_url = "https://belabef.com/admin/shopping/order/?q=YjowOw%3D%3D&keyword={$ord_chk_result['order_number']}&page=1&reset_status=Y&is_all_status_search=Y";
            break;
        case "4":
            $link_url = "";
            break;
        case "5":
            $link_url = "";
            break;
        case "6":
            $link_url = "";
            break;
    }

    $ord_chk_result["link_url"] = $link_url;
    $view_order_list[$ord_chk_result['order_number']][] = $ord_chk_result;
}

$reservation_chk_sql    = "
    SELECT 
       *,
       (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
       (SELECT sub.order_info from work_cms_error sub where sub.order_number=w.order_number AND {$add_where} LIMIT 1) as ord_info,
       (SELECT sub.error_type from work_cms_error sub where sub.order_number=w.order_number AND {$add_where} LIMIT 1) as error_type
    FROM work_cms_reservation w 
    WHERE order_number IN(SELECT order_number FROM work_cms_error sub WHERE {$add_where}) AND w.delivery_state != '4' ORDER BY regdate ASC
";
$reservation_chk_query = mysqli_query($my_db, $reservation_chk_sql);
$view_reservation_list = [];
while($reservation_chk = mysqli_fetch_assoc($reservation_chk_query))
{
    $link_url = "";
    switch($reservation_chk["error_type"]){
        case "1":
            $link_url = "https://belabef.com/admin/shopping/cancel/?q=YjowOw%3D%3D&keyword={$reservation_chk['order_number']}&page=1&reset_status=Y&is_all_status_search=Y";
            break;
        case "2":
            $link_url = "https://belabef.com/admin/shopping/return/?q=YjowOw%3D%3D&keyword={$reservation_chk['order_number']}&page=1&reset_status=Y&is_all_status_search=Y";
            break;
        case "3":
            $link_url = "https://belabef.com/admin/shopping/order/?q=YjowOw%3D%3D&keyword={$reservation_chk['order_number']}&page=1&reset_status=Y&is_all_status_search=Y";
            break;
        case "4":
            $link_url = "";
            break;
        case "5":
            $link_url = "";
            break;
        case "6":
            $link_url = "";
            break;
    }

    $view_reservation_list[$reservation_chk['order_number']][] = $reservation_chk;
}

$smarty->assign("error_type", $error_type);
$smarty->assign("delivery_state_color_list", $delivery_state_color_list);
$smarty->assign("reservation_state_list", getReservationStateOption());
$smarty->assign("error_type_option", getErrorExcelTypeOption());
$smarty->assign("view_order_list", $view_order_list);
$smarty->assign("view_reservation_list", $view_reservation_list);
$smarty->display("work_cms_error_check.html");
?>