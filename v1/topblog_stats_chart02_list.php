<?php
require('inc/common.php');
require('ckadmin.php');

$search_kind=isset($_GET['skind'])?$_GET['skind']:"";
$search_start_month=isset($_GET['ssmonth'])?$_GET['ssmonth']:date("Y/m", strtotime("-12 month"));
$search_end_month=isset($_GET['semonth'])?$_GET['semonth']:date("Y/m");
$search_sno=isset($_GET['sname'])?$_GET['sname']:"";
$search_slocation1=isset($_GET['f_location1'])?$_GET['f_location1']:"";
$search_slocation2=isset($_GET['f_location2'])?$_GET['f_location2']:"";

$where = [];
if(!empty($search_sno)) {
    $field[]="s_no";
    $keyword[]=$search_sno;
    $where[]="s_no='".$search_sno."'";
}

if(!empty($search_slocation1)) {
    $field[]="location1";
    $keyword[]=$search_slocation1;
    $where[]="(SELECT c.location1 FROM company c WHERE c.c_no=p.c_no) = '".$search_slocation1."' ";
}

if(!empty($search_slocation2)) {
    $field[]="location2";
    $keyword[]=$search_slocation2;
    $where[]="(SELECT c.location2 FROM company c WHERE c.c_no=p.c_no) = '".$search_slocation2."' ";
}

if(sizeof($where)>0) {
    for($i=0;$i<sizeof($where);$i++) {
        $add_where.=$where[$i]." AND ";
        $smarty->assign($field[$i],$keyword[$i]);
    }
}


$chart_date = $search_start_month."/01";


$result_month_array = [];
$result_total_array = [];
$result_exp_array = [];
$result_rep_array = [];
$result_dlv_array = [];
$result_exp_rep_array = [];
$result_dlv_rep_array = [];

/* 날짜와 테이블 where 조건을 토대로 chart01의 쿼리값을 불러옴 */
function chart01_sql($chart_date_f, $table_name_f, $add_where_f){
    if($chart_date_f == date("2016/02/01")){
        $sql_f="
			SELECT
				result_month,
				SUM(`reg_num`) AS total_reg_num,
				SUM(`application_sum`) AS total_application_sum
			FROM
				(SELECT
					DATE_FORMAT('".$chart_date_f."', '%Y-%m') AS result_month,
					reg_num,
					(SELECT COUNT(*) FROM application_old a WHERE a.p_no= p.p_no) AS application_sum
					FROM $table_name_f p
				WHERE
				$add_where_f
				DATE_FORMAT(reg_sdate, '%Y-%m') BETWEEN DATE_FORMAT('".$chart_date_f."', '%Y-%m') AND DATE_FORMAT(DATE_ADD(DATE_ADD('".$chart_date_f."',INTERVAL 1 MONTH),INTERVAL -1 DAY), '%Y-%m') ) AS sql_result
			WHERE 1=1
		";
    }else{
        $sql_f="
			SELECT
				result_month,
				SUM(`reg_num`) AS total_reg_num,
				SUM(`application_sum`) AS total_application_sum
			FROM
				(SELECT
					DATE_FORMAT('".$chart_date_f."', '%Y-%m') AS result_month,
					reg_num,
					(SELECT COUNT(*) FROM application a WHERE a.p_no= p.p_no) AS application_sum
					FROM $table_name_f p
				WHERE
				$add_where_f
				DATE_FORMAT(reg_sdate, '%Y-%m') BETWEEN DATE_FORMAT('".$chart_date_f."', '%Y-%m') AND DATE_FORMAT(DATE_ADD(DATE_ADD('".$chart_date_f."',INTERVAL 1 MONTH),INTERVAL -1 DAY), '%Y-%m') ) AS sql_result
			WHERE 1=1
		";
    }
    return $sql_f;
}

echo "<span style='margin-left:40px;font-weight:bold;'>[년-월]&nbsp;신청자수&nbsp;/&nbsp;모집인원&nbsp;(신청율)</span><br>";

for ($i=0 ; $chart_date <= $search_end_month."/01" ; $i++){ // for roof start

    if($chart_date < date("2016/02/01")){
        $table_name = "promotion_old";
    }else{
        $table_name = "promotion";
    }


    if($search_kind == '1'){
        // 체험단 모집인원수, 신청인원수 가져오기...
        $sql = chart01_sql($chart_date, $table_name, $add_where." kind = '1' and p_state1 = '0' and");

        //echo $sql."<br>";

        $query=mysqli_query($my_db,$sql);
        $data=mysqli_fetch_array($query);

        if($data['result_month']){
            $result_exp_array[$data['result_month']] = ($data['total_reg_num'] > 0) ? ($data['total_application_sum'] / $data['total_reg_num'] * 100) : 0;
        }else{
            $result_exp_array[date("Y-m", strtotime($chart_date))] = 0;
        }

        if($chart_date == date("2016/02/01")){
            $sql = chart01_sql($chart_date, "promotion_old", $add_where." kind = '1' and p_state1 = '0' and");
            $query=mysqli_query($my_db,$sql);
            $data=mysqli_fetch_array($query);

            $result_exp_array[$data['result_month']] += ($data['total_application_sum'] / $data['total_reg_num'] * 100); // 2016년 2월 지난 체험단 모집인원수, 신청인원 평균값 더하기(promotion_old)
            $result_exp_array[$data['result_month']] = $result_exp_array[$data['result_month']] / 2; // 평균값 구하기
        }
        if($data['result_month']){
            echo "<span style='margin-left:40px;'>[".$data['result_month']."]&nbsp;&nbsp;".$data['total_application_sum']."&nbsp;/&nbsp;".$data['total_reg_num']."&nbsp;(".round($result_exp_array[$data['result_month']],2)."%)</span><br>";
        }else{
            echo "<span style='margin-left:40px;'>[".date("Y-m", strtotime($chart_date))."]&nbsp;&nbsp;0&nbsp;/&nbsp;0</span><br>";
        }
    }else if($search_kind == '2'){
        // 기자단 모집인원수, 신청인원수 가져오기...
        $sql = chart01_sql($chart_date, $table_name, $add_where." kind = '2' and p_state1 = '0' and");

        //echo $sql."<br>";

        $query=mysqli_query($my_db,$sql);
        $data=mysqli_fetch_array($query);

        if($data['result_month']){
            $result_rep_array[$data['result_month']] = ($data['total_reg_num'] > 0) ? ($data['total_application_sum'] / $data['total_reg_num'] * 100) : 0;
        }else{
            $result_rep_array[date("Y-m", strtotime($chart_date))] = 0;
        }

        if($chart_date == date("2016/02/01")){
            $sql = chart01_sql($chart_date, "promotion_old", $add_where." kind = '2' and p_state1 = '0' and");
            $query=mysqli_query($my_db,$sql);
            $data=mysqli_fetch_array($query);

            $result_rep_array[$data['result_month']] += ($data['total_application_sum'] / $data['total_reg_num'] * 100); // 2016년 2월 지난 기자단 모집인원수, 신청인원 평균값 더하기(promotion_old)
            $result_rep_array[$data['result_month']] = $result_rep_array[$data['result_month']] / 2; // 평균값 구하기
        }
        if($data['result_month']){
            echo "<span style='margin-left:40px;'>[".$data['result_month']."]&nbsp;&nbsp;".$data['total_application_sum']."&nbsp;/&nbsp;".$data['total_reg_num']."&nbsp;(".round($result_rep_array[$data['result_month']],2)."%)</span><br>";
        }else{
            echo "<span style='margin-left:40px;'>[".date("Y-m", strtotime($chart_date))."]&nbsp;&nbsp;0&nbsp;/&nbsp;0</span><br>";
        }
    }else if($search_kind == '3'){
        // 배송체험단 모집인원수, 신청인원수 가져오기...
        $sql = chart01_sql($chart_date, $table_name, $add_where." kind = '3' and p_state1 = '0' and");

        //echo $sql."<br>";

        $query=mysqli_query($my_db,$sql);
        $data=mysqli_fetch_array($query);

        if($data['result_month']){
            $result_dlv_array[$data['result_month']] = ($data['total_reg_num'] > 0) ? ($data['total_application_sum'] / $data['total_reg_num'] * 100) : 0;
        }else{
            $result_dlv_array[date("Y-m", strtotime($chart_date))] = 0;
        }

        if($chart_date == date("2016/02/01")){
            $sql = chart01_sql($chart_date, "promotion_old", $add_where." kind = '3' and p_state1 = '0' and");
            $query=mysqli_query($my_db,$sql);
            $data=mysqli_fetch_array($query);

            if($data['result_month']){
                $result_dlv_array[$data['result_month']] += ($data['total_application_sum'] / $data['total_reg_num'] * 100); // 2016년 2월 지난 배송체험단 모집인원수, 신청인원 평균값 더하기(promotion_old)
                $result_dlv_array[$data['result_month']] = $result_dlv_array[$data['result_month']] / 2; // 평균값 구하기
            }
        }
        if($data['result_month']){
            echo "<span style='margin-left:40px;'>[".$data['result_month']."]&nbsp;&nbsp;".$data['total_application_sum']."&nbsp;/&nbsp;".$data['total_reg_num']."&nbsp;(".round($result_dlv_array[$data['result_month']],2)."%)</span><br>";
        }else{
            echo "<span style='margin-left:40px;'>[".date("Y-m", strtotime($chart_date))."]&nbsp;&nbsp;0&nbsp;/&nbsp;0</span><br>";
        }
    }else if($search_kind == '4'){
        // 체험단(+보상) 모집인원수, 신청인원수 가져오기...
        $sql = chart01_sql($chart_date, $table_name, $add_where." kind = '4' and p_state1 = '0' and");

        //echo $sql."<br>";

        $query=mysqli_query($my_db,$sql);
        $data=mysqli_fetch_array($query);

        if($data['result_month']){
            $result_exp_rep_array[$data['result_month']] = ($data['total_reg_num'] > 0) ? ($data['total_application_sum'] / $data['total_reg_num'] * 100) : 0;
        }else{
            $result_exp_rep_array[date("Y-m", strtotime($chart_date))] = 0;
        }

        if($data['result_month']){
            echo "<span style='margin-left:40px;'>[".$data['result_month']."]&nbsp;&nbsp;".$data['total_application_sum']."&nbsp;/&nbsp;".$data['total_reg_num']."&nbsp;(".round($result_dlv_array[$data['result_month']],2)."%)</span><br>";
        }else{
            echo "<span style='margin-left:40px;'>[".date("Y-m", strtotime($chart_date))."]&nbsp;&nbsp;0&nbsp;/&nbsp;0</span><br>";
        }
    }else if($search_kind == '5'){
        // 배송체험단(+보상) 모집인원수, 신청인원수 가져오기...
        $sql = chart01_sql($chart_date, $table_name, $add_where." kind = '5' and p_state1 = '0' and");

        //echo $sql."<br>";

        $query=mysqli_query($my_db,$sql);
        $data=mysqli_fetch_array($query);

        if($data['result_month']){
            $result_dlv_rep_array[$data['result_month']] = ($data['total_reg_num'] > 0) ? ($data['total_application_sum'] / $data['total_reg_num'] * 100) : 0;
        }else{
            $result_dlv_rep_array[date("Y-m", strtotime($chart_date))] = 0;
        }

        if($data['result_month']){
            echo "<span style='margin-left:40px;'>[".$data['result_month']."]&nbsp;&nbsp;".$data['total_application_sum']."&nbsp;/&nbsp;".$data['total_reg_num']."&nbsp;(".round($result_dlv_array[$data['result_month']],2)."%)</span><br>";
        }else{
            echo "<span style='margin-left:40px;'>[".date("Y-m", strtotime($chart_date))."]&nbsp;&nbsp;0&nbsp;/&nbsp;0</span><br>";
        }
    }else{
        // 전체 모집인원수, 신청인원수 가져오기...
        $sql = chart01_sql($chart_date, $table_name, $add_where);

        //echo $sql."<br>";

        $query=mysqli_query($my_db,$sql);
        $data=mysqli_fetch_array($query);

        if($data['result_month']){
            $result_total_array[$data['result_month']] = ($data['total_reg_num'] > 0) ? ($data['total_application_sum'] / $data['total_reg_num'] * 100) : 0;
        }else{
            $result_total_array[date("Y-m", strtotime($chart_date))] = 0;
        }

        if($chart_date == date("2016/02/01")){
            $sql = chart01_sql($chart_date, "promotion_old", $add_where);
            $query=mysqli_query($my_db,$sql);
            $data=mysqli_fetch_array($query);

            if($data['result_month']){
                $result_total_array[$data['result_month']] += ($data['total_application_sum'] / $data['total_reg_num'] * 100); // 2016년 2월 지난 배송체험단 모집인원수, 신청인원 평균값 더하기(promotion_old)
                $result_total_array[$data['result_month']] = $result_total_array[$data['result_month']] / 2; // 평균값 구하기
            }
        }

        if($data['result_month']){
            echo "<span style='margin-left:40px;'>[".$data['result_month']."]&nbsp;&nbsp;".$data['total_application_sum']."&nbsp;/&nbsp;".$data['total_reg_num']."&nbsp;(".round($result_total_array[$data['result_month']],2)."%)</span><br>";
        }else{
            echo "<span style='margin-left:40px;'>[".date("Y-m", strtotime($chart_date))."]&nbsp;&nbsp;0&nbsp;/&nbsp;0</span><br>";
        }
    }

    $chart_date =  date("Y/m/01", strtotime("+1 month", strtotime($chart_date)));

    if($i >= 50){
        echo "조회기간이 너무 많습니다. 조회기간을 줄여주세요.";
        return;
    }
}// for roof end

?>
