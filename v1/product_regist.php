<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/product.php');
require('inc/helper/withdraw.php');
require('inc/helper/deposit.php');
require('inc/helper/work_extra.php');
require('inc/model/Kind.php');
require('inc/model/Staff.php');
require('inc/model/MyCompany.php');
require('inc/model/Company.php');
require('inc/model/Corporation.php');
require('inc/model/Product.php');

# Model 선언
$kind_model 	    = Kind::Factory();
$staff_model 	    = Staff::Factory();
$my_company_model 	= MyCompany::Factory();
$company_model 	    = Company::Factory();
$corp_model 	    = Corporation::Factory();
$product_model 	    = Product::Factory();
$relation_model 	= Product::Factory();

# 목록버튼 검색조건
$sch_page		  = isset($_GET['page'])?$_GET['page']:"1";
$sch_display_get  = isset($_GET['sch_display'])?$_GET['sch_display']:"1";
$sch_prd_g1_get	  = isset($_GET['sch_prd_g1'])?$_GET['sch_prd_g1']:"";
$sch_prd_g2_get	  = isset($_GET['sch_prd_g2'])?$_GET['sch_prd_g2']:"";
$sch_prd_name_get = isset($_GET['sch_prd_name'])?$_GET['sch_prd_name']:"";
$search_url 	  = "page={$sch_page}&sch_prd_g1={$sch_prd_g1_get}&sch_prd_g2={$sch_prd_g2_get}&sch_display={$sch_display_get}&sch_prd_name={$sch_prd_name_get}";
$smarty->assign("search_url", $search_url);


# 프로세스 처리
$proc		= isset($_POST['process']) ? $_POST['process'] : "";

if($proc == "write")
{
	$search_url				= isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $task_req_staff_type 	= $_POST['f_task_req_staff_type'] ? $_POST['f_task_req_staff_type'] : "";
    $task_req_staff 	 	= $_POST['f_task_req_staff'] ? implode(',', $_POST['f_task_req_staff']) : "";
    $task_run_staff_type 	= $_POST['f_task_run_staff_type'] ? $_POST['f_task_run_staff_type'] : "";
    $task_run_staff 	 	= $_POST['f_task_run_staff'] ? implode(',', $_POST['f_task_run_staff']) : "";
	$keyword				= $_POST['f_keyword'] ? implode(',', $_POST['f_keyword']) : "";
    $is_guide 		        = isset($_POST['f_is_guide']) ? $_POST['f_is_guide'] : "2";
    $guide_no 		        = isset($_POST['f_guide_no']) ? $_POST['f_guide_no'] : "0";
    $guide_required         = isset($_POST['f_guide_required']) ? $_POST['f_guide_required'] : "0";
    $f_dp_account           = array_filter($_POST['f_dp_account']);
    $f_wd_account           = array_filter($_POST['f_wd_account']);
    $dp_account             = !empty($f_dp_account) ? implode(',', $f_dp_account) : "";
    $wd_account             = !empty($f_wd_account) ? implode(',', $f_wd_account) : "";

	$product_insert_data    = array(
        "display"           => $_POST['f_display'],
        "k_name_code"       => $_POST['f_prd_g2'],
        "priority"          => $_POST['f_priority'],
        "title"             => addslashes($_POST['f_title']),
        "keyword"           => $keyword,
        "is_task_dday"      => $_POST['f_is_task_dday'],
        "kind"              => $_POST['f_kind'],
        "description"       => addslashes(trim($_POST['f_description'])),
        "work_format_type"  => $_POST['f_work_format_type'],
        "work_format"       => addslashes(trim($_POST['f_work_format'])),
        "work_format_extra" => $_POST['f_work_format_extra'],
        "suggested_price"   => $_POST['f_suggested_price'],
        "market_price"      => $_POST['f_market_price'],
        "min_price"         => $_POST['f_min_price'],
        "max_price"         => $_POST['f_max_price'],
        "wd_dp_state"       => $_POST['f_wd_dp_state'],
        "dp_account"        => $dp_account,
        "wd_account"        => $wd_account,
        "work_time_method"  => $_POST['f_work_time_method'],
        "work_time_default" => $_POST['f_work_time_default'],
        "out_price"         => addslashes($_POST['f_out_price']),
        "equally_vat"       => $_POST['f_equally_vat'],
        "cost_rate"         => addslashes($_POST['f_cost_rate']),
        "is_guide"          => $is_guide,
        "guide_no"          => $guide_no,
        "guide_required"    => $guide_required,
        "regdate"           => date("Y-m-d H:i:s"),
        "task_req_staff_type"   => $task_req_staff_type,
        "task_run_staff_type"   => $task_run_staff_type,
    );

    $product_insert_data['task_req_staff']          = ($task_req_staff_type == '1') ? $task_req_staff : "NULL";
    $product_insert_data['task_run_staff']          = ($task_run_staff_type == '1') ? $task_run_staff : "NULL";
    $product_insert_data['task_run_staff_default']  = ($task_run_staff_type == '1') ? $_POST['f_task_run_staff_default'] : "NULL";

    # 입금업체 관련
    if(!empty($_POST['f_deposit_comp_ids']) && !empty($_POST['f_deposit_comp_nicknames']))
    {
        $dp_comp_ids 		= implode(",", $_POST['f_deposit_comp_ids']);
        $dp_comp_nicknames  = implode(",", $_POST['f_deposit_comp_nicknames']);
        $dp_comp_methods    = implode(",", $_POST['f_deposit_comp_method']);

        $product_insert_data['dp_sch_type']     = $_POST['f_dp_sch_type'];
        $product_insert_data['dp_c_no_list']    = $dp_comp_ids;
        $product_insert_data['dp_c_label_list'] = $dp_comp_nicknames;
        $product_insert_data['dp_method_list']  = $dp_comp_methods;
    }else{
        $product_insert_data['dp_sch_type']     = $_POST['f_dp_sch_type'];
        $product_insert_data['dp_c_no_list']    = '';
        $product_insert_data['dp_c_label_list'] = '';
        $product_insert_data['dp_method_list']  = '';
    }

    # 출금업체 관련
    if(!empty($_POST['f_withdraw_comp_ids']) && !empty($_POST['f_withdraw_comp_nicknames']))
    {
        $wd_comp_ids 		= implode(",", $_POST['f_withdraw_comp_ids']);
        $wd_comp_nicknames  = implode(",", $_POST['f_withdraw_comp_nicknames']);
        $wd_comp_methods    = implode(",", $_POST['f_withdraw_comp_method']);

        $product_insert_data['wd_sch_type']     = $_POST['f_wd_sch_type'];
        $product_insert_data['wd_c_no_list']    = $wd_comp_ids;
        $product_insert_data['wd_c_label_list'] = $wd_comp_nicknames;
        $product_insert_data['wd_method_list']  = $wd_comp_methods;
    }else{
        $product_insert_data['wd_sch_type']     = $_POST['f_wd_sch_type'];
        $product_insert_data['wd_c_no_list']    = '';
        $product_insert_data['wd_c_label_list'] = '';
        $product_insert_data['wd_method_list']  = '';
    }

    if($product_model->insert($product_insert_data))
    {
        $prd_no = $product_model->getInsertId();

        if($prd_no && ($related_products = $_POST['related_prd']))
        {
            $rp_idx = 0;
            $rp_priorities = $_POST['related_priority'];
            foreach ($related_products as $related_prd)
            {
                $rp_priority 	    = isset($rp_priorities[$rp_idx]) ? $rp_priorities[$rp_idx] : 1;
                $related_ins_data   = array(
                    "prd_no"        => $prd_no,
                    "related_prd"   => $related_prd,
                    "priority"      => $rp_priority,
                );

                $product_model->insertRelation($related_ins_data);
                $rp_idx++;
            }
        }

        if($prd_no && ($new_sub_categories = $_POST['sub_category_ids']))
        {
            $new_k_parent        = sprintf('%05d', $prd_no);
            $new_sub_cate_names  = $_POST['sub_category_names'];
            $new_sub_description = "work table의 업무진행구분 k_parent = p_no";

            $new_sub_idx = 0;
            foreach ($new_sub_categories as $new_sub_cate)
            {
                $new_sub_cate_name = $new_sub_cate_names[$new_sub_idx];
                $new_sub_priority  = $new_sub_idx+1;
                $new_sub_cate_ins_data = array(
                    "k_code"        => "work_task_run",
                    "k_name"        => $new_sub_cate_name,
                    "k_name_code"   => $new_sub_cate,
                    "k_parent"      => $new_k_parent,
                    "k_discription" => $new_sub_description,
                    "priority"      => $new_sub_priority,
                    "display"       => '1'
                );

                $kind_model->insert($new_sub_cate_ins_data);
                $new_sub_idx++;
            }
        }

        exit("<script>alert('상품 등록에 성공했습니다');location.href='product_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('상품 등록에 실패했습니다');history.back();</script>");
    }
} 
elseif($proc=="modify") 
{
	$search_url			    = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $task_req_staff_type    = $_POST['f_task_req_staff_type'] ? $_POST['f_task_req_staff_type'] : "";
    $task_req_staff 	    = $_POST['f_task_req_staff'] ? implode(',', $_POST['f_task_req_staff']) : "";
    $task_run_staff_type    = $_POST['f_task_run_staff_type'] ? $_POST['f_task_run_staff_type'] : "";
    $task_run_staff 	    = $_POST['f_task_run_staff'] ? implode(',', $_POST['f_task_run_staff']) : "";
	$keyword			    = $_POST['f_keyword'] ? implode(',', $_POST['f_keyword']) : "";
    $is_guide 		        = isset($_POST['f_is_guide']) ? $_POST['f_is_guide'] : "2";
    $guide_no 		        = isset($_POST['f_guide_no']) ? $_POST['f_guide_no'] : "0";
    $guide_required         = isset($_POST['f_guide_required']) ? $_POST['f_guide_required'] : "0";
    $prd_no                 = $_POST['idx'];
    $f_dp_account           = array_filter($_POST['f_dp_account']);
    $f_wd_account           = array_filter($_POST['f_wd_account']);
    $dp_account             = !empty($f_dp_account) ? implode(',', $f_dp_account) : "";
    $wd_account             = !empty($f_wd_account) ? implode(',', $f_wd_account) : "";

    $product_update_data    = array(
        "prd_no"            => $prd_no,
        "display"           => $_POST['f_display'],
        "k_name_code"       => $_POST['f_prd_g2'],
        "priority"          => $_POST['f_priority'],
        "title"             => addslashes(trim($_POST['f_title'])),
        "keyword"           => $keyword,
        "is_task_dday"      => $_POST['f_is_task_dday'],
        "kind"              => $_POST['f_kind'],
        "description"       => addslashes(trim($_POST['f_description'])),
        "work_format_type"  => $_POST['f_work_format_type'],
        "work_format"       => addslashes(trim($_POST['f_work_format'])),
        "work_format_extra" => $_POST['f_work_format_extra'],
        "price_apply"       => $_POST['f_price_apply'],
        "suggested_price"   => $_POST['f_suggested_price'],
        "market_price"      => $_POST['f_market_price'],
        "min_price"         => $_POST['f_min_price'],
        "max_price"         => $_POST['f_max_price'],
        "wd_dp_state"       => $_POST['f_wd_dp_state'],
        "dp_account"        => $dp_account,
        "wd_account"        => $wd_account,
        "work_time_method"  => $_POST['f_work_time_method'],
        "work_time_default" => $_POST['f_work_time_default'],
        "out_price"         => addslashes($_POST['f_out_price']),
        "equally_vat"       => $_POST['f_equally_vat'],
        "cost_rate"         => addslashes($_POST['f_cost_rate']),
        "is_guide"          => $is_guide,
        "guide_no"          => $guide_no,
        "guide_required"    => $guide_required,
        "task_req_staff_type" => $task_req_staff_type,
        "task_run_staff_type" => $task_run_staff_type
    );
    
    $product_update_data['task_req_staff']          = ($task_req_staff_type == '1') ? $task_req_staff : "NULL";
    $product_update_data['task_run_staff']          = ($task_run_staff_type == '1') ? $task_run_staff : "NULL";
    $product_update_data['task_run_staff_default']  = ($task_run_staff_type == '1') ? $_POST['f_task_run_staff_default'] : "NULL";

    # 입금업체 관련
    if(!empty($_POST['f_deposit_comp_ids']) && !empty($_POST['f_deposit_comp_nicknames']))
    {
        $dp_comp_ids 		= implode(",", $_POST['f_deposit_comp_ids']);
        $dp_comp_nicknames  = implode(",", $_POST['f_deposit_comp_nicknames']);
        $dp_comp_methods    = implode(",", $_POST['f_deposit_comp_method']);

        $product_update_data['dp_sch_type']     = $_POST['f_dp_sch_type'];
        $product_update_data['dp_c_no_list']    = $dp_comp_ids;
        $product_update_data['dp_c_label_list'] = $dp_comp_nicknames;
        $product_update_data['dp_method_list']  = $dp_comp_methods;
    }else{
        $product_update_data['dp_sch_type']     = $_POST['f_dp_sch_type'];
        $product_update_data['dp_c_no_list']    = '';
        $product_update_data['dp_c_label_list'] = '';
        $product_update_data['dp_method_list']  = '';
    }

    # 출금업체 관련
    if(!empty($_POST['f_withdraw_comp_ids']) && !empty($_POST['f_withdraw_comp_nicknames']))
    {
        $wd_comp_ids 		= implode(",", $_POST['f_withdraw_comp_ids']);
        $wd_comp_nicknames  = implode(",", $_POST['f_withdraw_comp_nicknames']);
        $wd_comp_methods    = implode(",", $_POST['f_withdraw_comp_method']);

        $product_update_data['wd_sch_type']     = $_POST['f_wd_sch_type'];
        $product_update_data['wd_c_no_list']    = $wd_comp_ids;
        $product_update_data['wd_c_label_list'] = $wd_comp_nicknames;
        $product_update_data['wd_method_list']  = $wd_comp_methods;
    }else{
        $product_update_data['wd_sch_type']     = $_POST['f_wd_sch_type'];
        $product_update_data['wd_c_no_list']    = '';
        $product_update_data['wd_c_label_list'] = '';
        $product_update_data['wd_method_list']  = '';
    }

	if($product_model->update($product_update_data))
	{
        # 연관상품 관련
        $related_products = $_POST['related_prd'];
        $del_related 	  = [];
        $add_related  	  = [];
        $cur_related_prd  = $_POST['cur_related_prd'];

        if(!empty($related_products) && !empty($cur_related_prd))
        {
            $cur_related_prd = explode(',', $cur_related_prd);

            $del_related = array_diff($cur_related_prd, $related_products); //삭제하는 관련상품
            $add_related = array_diff($related_products, $cur_related_prd); //추가하는 관련상품
        }
        elseif(!empty($related_products) && empty($cur_related_prd))
        {
            $add_related = $related_products;
        }
        elseif(empty($related_products) && !empty($cur_related_prd))
        {
            $cur_related_prd = explode(',', $cur_related_prd);
            $del_related 	 = $cur_related_prd;
        }

        if(!empty($del_related))
        {
            foreach ($del_related as $related_prd)
            {
                $product_model->deleteRelation($prd_no, $related_prd);
            }
        }

        if(!empty($add_related))
        {
            $rp_priorities 		= $_POST['related_priority'];
            foreach ($add_related as $related_prd)
            {
                $rp_idx             = 0;
                $rp_priority 	    = isset($rp_priorities[$rp_idx]) ? $rp_priorities[$rp_idx] : 1;
                $related_ins_data   = array(
                    "prd_no"        => $prd_no,
                    "related_prd"   => $related_prd,
                    "priority"      => $rp_priority,
                );

                $product_model->insertRelation($related_ins_data);
                $rp_idx++;

            }
        }

        # 세부카테고리
        $new_k_parent   = sprintf('%05d', $prd_no);
        $sub_categories = $_POST['sub_category_ids'];
        $sub_cate_names = $_POST['sub_category_names'];
        $cur_sub_categories = $_POST['cur_sub_category'];
        $del_sub_cate		= [];

        if(!empty($sub_categories) && !empty($cur_sub_categories))
        {
            $cur_sub_category = explode(',', $cur_sub_categories);
            $del_sub_cate 	  = array_diff($cur_sub_category, $sub_categories); //삭제하는 관련상품
        }
        elseif(empty($sub_categories) && !empty($cur_sub_categories))
        {
            $cur_sub_category = explode(',', $cur_sub_categories);
            $del_sub_cate 	  = $cur_sub_category;
        }

        if(!empty($del_sub_cate))
        {
            foreach ($del_sub_cate as $sub_cate)
            {
                $sub_cate_upd_data = array(
                    "display" => "2"
                );

                $sub_cate_key_data = array(
                    "k_code"        => "work_task_run",
                    "k_name_code"   => $sub_cate,
                    "k_parent"      => $new_k_parent
                );

                $kind_model->updateToArray($sub_cate_upd_data, $sub_cate_key_data);
            }
        }

        if(!empty($sub_categories) && !empty($sub_cate_names))
        {
            $sub_idx = 0;
            $sub_description = "work table의 업무진행구분 k_parent = p_no";
            foreach ($sub_categories as $sub_cate)
            {
                $sub_cate_chk_sql 	= "SELECT COUNT(*) as cnt FROM `kind` WHERE k_code='work_task_run' AND k_parent='{$new_k_parent}' AND k_name_code='{$sub_cate}'";
                $sub_cate_chk_query = mysqli_query($my_db, $sub_cate_chk_sql);
                $sub_cate_result	= mysqli_fetch_assoc($sub_cate_chk_query);

                $sub_cate_name = $sub_cate_names[$sub_idx];
                $sub_priority  = $sub_idx+1;

                if($sub_cate_result['cnt'] > 0)
                {
                    $sub_cate_upd_data = array(
                        "k_name"        => $sub_cate_name,
                        "display"       => '1',
                        "priority"      => $sub_priority,
                    );

                    $sub_cate_key_data = array(
                        "k_code"        => "work_task_run",
                        "k_name_code"   => $sub_cate,
                        "k_parent"      => $new_k_parent
                    );

                    $kind_model->updateToArray($sub_cate_upd_data, $sub_cate_key_data);
                }else{
                    $sub_cate_ins_data = array(
                        "k_code"        => "work_task_run",
                        "k_name"        => $sub_cate_name,
                        "k_name_code"   => $sub_cate,
                        "k_parent"      => $new_k_parent,
                        "k_discription" => $sub_description,
                        "priority"      => $sub_priority,
                        "display"       => '1'
                    );

                    $kind_model->insert($sub_cate_ins_data);
                }

                $sub_idx++;
            }
        }

        exit("<script>alert('상품 수정에 성공했습니다');location.href='product_list.php?{$search_url}';</script>");

    }else{
        exit("<script>alert('상품 수정에 실패했습니다');history.back();</script>");
    }

}
elseif($proc == "memo")
{
	$prd_no = (isset($_POST['idx']))?$_POST['idx']:"";
	$mtxt   = (isset($_POST['f_prd_memo']))?addslashes($_POST['f_prd_memo']):"";

	$upd_data = array(
        "prd_memo"      => $mtxt,
        "prd_memo_date" => date("Y-m-d H:i:s"),
        "prd_no"        => $prd_no,
    );

	$product_model->update($upd_data);

	exit("<script>alert('메모를 등록하였습니다');location.href='product_regist.php?no={$_POST['idx']}&{$search_url}';</script>");

}
elseif($proc == 'modify_related_priority')
{
	$pr_no  = isset($_POST['pr_no']) ? $_POST['pr_no'] : "";
	$value  = isset($_POST['val']) ? $_POST['val'] : "";

	if(empty($pr_no)) {
        echo "연관 번호가 없습니다. 연관상품 우선순위 저장에 실패 하였습니다.";
        exit;
	}

    if (!$relation_model->update(array("no" => $pr_no, "priority" => $value)))
        echo "연관상품 우선순위 저장에 실패 하였습니다.";
    else
        echo "연관상품 우선순위가 저장 되었습니다.";
    exit;
}
else
{
	# 기본값 설정
    $idx		= isset($_GET['no']) ? $_GET['no'] : "";
    $mode		= (!$idx) ? "write" : "modify";
    $submit_btn = ($mode == "write") ? "등록하기" : "수정하기";
    $k_code     = "product";

    $smarty->assign("idx", $idx);
    $smarty->assign("mode", $mode);
    $smarty->assign("submit_btn", $submit_btn);

    $cur_related_prd_list  = [];
    $cur_sub_category_list = [];
    $prd_g1_list           = $kind_model->getKindParentList($k_code);
    $my_company_name_list  = $my_company_model->getNameList();
    $dp_account_option     = $corp_model->getAccountList('1');
    $wd_account_option     = $corp_model->getAccountList('2');

    if(!empty($idx))
    {
        $product_sql = "
			SELECT
				*,
				(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code)) AS k_prd1,
				(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=prd.k_name_code) AS k_prd2
			FROM product prd
			WHERE prd.prd_no='{$idx}'
		";

        $product_query 	= mysqli_query($my_db, $product_sql);
        $product 		= mysqli_fetch_array($product_query);

        $task_req_staff_list		  = [];
        $task_run_staff_list		  = [];
        $task_run_staff_default_label = "";

        if(!empty($product['task_req_staff'])){
            $task_req_staff_val_list = explode(',', $product['task_req_staff']);
            foreach($task_req_staff_val_list as $task_req_staff){
                $task_req_staff_val = explode("_", $task_req_staff);

                $t_label = getTeamFullName($my_db, 0, $task_req_staff_val[1]);
                $task_req_staff_list[$task_req_staff] = $staff_model->getStaffName($task_req_staff_val[0])." ({$t_label})";
            }
        }

        if(!empty($product['task_run_staff'])){
            $task_run_staff_val_list = explode(',', $product['task_run_staff']);

            if(!empty($product['task_run_staff_default'])){
                $task_run_key = array_search( $product['task_run_staff_default'], $task_run_staff_val_list);
                array_splice($task_run_staff_val_list, $task_run_key, 1);
            }

            foreach($task_run_staff_val_list as $task_run_staff){
                $task_run_staff_val = explode("_", $task_run_staff);

                $t_label = getTeamFullName($my_db, 0, $task_run_staff_val[1]);
                $task_run_staff_list[$task_run_staff] = $staff_model->getStaffName($task_run_staff_val[0])." ({$t_label})";
            }
        }

        if(!empty($product['task_run_staff_default'])){
            $run_staff_val = explode("_", $product['task_run_staff_default']);

            $t_label = getTeamFullName($my_db, 0, $run_staff_val[1]);
            $task_run_staff_default_label = $staff_model->getStaffName($run_staff_val[0])." ({$t_label})";
        }

        # 입금업체 리스트
        $deposit_comp_list = [];
        if(!empty($product['dp_c_no_list'])){
            $dp_c_no_list 	 = explode(",", $product['dp_c_no_list']);
            $dp_c_label_list = explode(",", $product['dp_c_label_list']);
            $dp_method_list  = explode(",", $product['dp_method_list']);

            for($dp_idx=0; $dp_idx < count($dp_c_no_list); $dp_idx++){
                $dp_c_no   = $dp_c_no_list[$dp_idx];
                $dp_c_name = $company_model->getCompanyLabel($dp_c_no);
                $deposit_comp_list[] = array(
                    "c_no" 		=> $dp_c_no,
                    "c_name" 	=> $dp_c_name,
                    "nickname" 	=> isset($dp_c_label_list[$dp_idx]) ? $dp_c_label_list[$dp_idx] : "",
                    "method" 	=> isset($dp_method_list[$dp_idx]) && !empty($dp_method_list[$dp_idx]) ? $dp_method_list[$dp_idx] : "2"
                );
            }
        }

        #출금업체 리스트
        $withdraw_comp_list = [];
        if(!empty($product['wd_c_no_list'])){
            $wd_c_no_list 	 = explode(",", $product['wd_c_no_list']);
            $wd_c_label_list = explode(",", $product['wd_c_label_list']);
            $wd_method_list = explode(",", $product['wd_method_list']);

            for($wd_idx=0; $wd_idx < count($wd_c_no_list); $wd_idx++){
                $wd_c_no 	= $wd_c_no_list[$wd_idx];
                $wd_c_name 	= $company_model->getCompanyLabel($wd_c_no);
                $withdraw_comp_list[] = array(
                    "c_no" 		=> $wd_c_no,
                    "c_name" 	=> $wd_c_name,
                    "nickname" 	=> isset($wd_c_label_list[$wd_idx]) ? $wd_c_label_list[$wd_idx] : "",
                    "method" 	=> isset($wd_method_list[$wd_idx]) ? $wd_method_list[$wd_idx] : "",
                );
            }
        }

        $dp_account_g1 = $dp_account = $wd_account_g1 = $wd_account = [];

        if(!empty($product['dp_account']))
        {
            $dp_account = explode(',', $product['dp_account']);

            foreach($dp_account as $co_no){
                $corp_account = $corp_model->getItem($co_no);
                $dp_account_g1[] = $corp_account['my_c_no'];
            }
        }

        if(!empty($product['wd_account']))
        {
            $wd_account = explode(',', $product['wd_account']);

            foreach($wd_account as $co_no){
                $corp_account = $corp_model->getItem($co_no);
                $wd_account_g1[] = $corp_account['my_c_no'];
            }
        }


        $smarty->assign(
            array(
                "idx"                   => $product['prd_no'],
                "f_display"             => $product['display'],
                "f_prd_g1"              => $product['k_prd1'],
                "f_prd_g2"              => $product['k_prd2'],
                "f_priority"            => $product['priority'],
                "f_title"               => stripslashes($product['title']),
                "f_keyword"             => $product['keyword'],
                "f_is_task_dday"        => $product['is_task_dday'],
                "f_kind"                => $product['kind'],
                "f_description"         => stripslashes($product['description']),
                "f_work_format_type"    => $product['work_format_type'],
                "f_work_format"         => stripslashes($product['work_format']),
                "f_work_format_extra"   => $product['work_format_extra'],
                "f_price_apply"         => $product['price_apply'],
                "f_suggested_price"     => $product['suggested_price'],
                "f_market_price"        => $product['market_price'],
                "f_min_price"           => $product['min_price'],
                "f_max_price"           => $product['max_price'],
                "f_wd_dp_state"         => $product['wd_dp_state'],
                "f_work_time_method"    => $product['work_time_method'],
                "f_work_time_default"   => $product['work_time_default'],
                "f_out_price"           => $product['out_price'],
                "f_equally_vat"         => $product['equally_vat'],
                "f_cost_rate"           => $product['cost_rate'],
                "f_prd_memo"            => $product['prd_memo'],
                "f_prd_memo_day"        => date("Y/m/d",strtotime($product['prd_memo_date'])),
                "f_prd_memo_time"       => date("H:i",strtotime($product['prd_memo_date'])),
                "f_task_req_staff_type"          => $product['task_req_staff_type'],
                "f_task_req_staff_list"          => $task_req_staff_list,
                "f_task_run_staff_type"          => $product['task_run_staff_type'],
                "f_task_run_staff_list"          => $task_run_staff_list,
                "f_task_run_staff_default"       => $product['task_run_staff_default'],
                "f_task_run_staff_default_label" => $task_run_staff_default_label,
                "f_dp_sch_type"         => $product['dp_sch_type'],
                "f_deposit_comp_list"   => $deposit_comp_list,
                "f_wd_sch_type"         => $product['wd_sch_type'],
                "f_withdraw_comp_list"  => $withdraw_comp_list,
                "f_is_guide"            => $product['is_guide'],
                "f_guide_no"            => $product['guide_no'],
                "f_guide_required"      => $product['guide_required'],
                "f_dp_account_g1"       => $dp_account_g1,
                "f_dp_account"          => $dp_account,
                "f_wd_account_g1"       => $wd_account_g1,
                "f_wd_account"          => $wd_account,
            )
        );

        # 해당 상품 그룹 가져오기
        $prd_g2_list        = $kind_model->getKindGroupChildList($k_code, $product['k_prd1']);

        # 해당 상품에 등록된 연관상품 가져오기
        $related_product_list = $product_model->getProductRelationList($product['prd_no']);

        if(!empty($related_product_list)){
            $cur_related_prd_list = implode(',', array_keys($related_product_list));
        }

        # 해당 상품에 세부카테고리 가져오기
        $k_parent 	        = sprintf('%05d', $product['prd_no']);
        $sub_category_list  = $kind_model->getKindGroupChildList("work_task_run", $k_parent);

        if(!empty($sub_category_list)){

            $cur_sub_category_list = implode(',', array_keys($sub_category_list));
        }

        $smarty->assign('prd_g2_list', $prd_g2_list);
        $smarty->assign('related_product_list', $related_product_list);
        $smarty->assign('sub_category_list', $sub_category_list);
	}

    $last_kind_no = $kind_model->getKindLastNo("work_task_run");

    $smarty->assign('cur_related_prd_list', $cur_related_prd_list);
    $smarty->assign('cur_sub_category_list', $cur_sub_category_list);
    $smarty->assign('last_kind_no', $last_kind_no);

    $smarty->assign('prd_g1_list', $prd_g1_list);
    $smarty->assign('my_company_name_list', $my_company_name_list);
    $smarty->assign('dp_account_option', $dp_account_option);
    $smarty->assign('wd_account_option', $wd_account_option);
    $smarty->assign('display_option', getDisplayOption());
    $smarty->assign('sch_wd_dp_state_option', getWdDpStateOption());
    $smarty->assign('sch_type_option', getSchTypeOption());
    $smarty->assign('wd_method_option', getWdMethodOption());
    $smarty->assign('dp_method_option', getDpMethodOption());
    $smarty->assign('work_format_extra_option', getWorkFormatExtra());
    $smarty->assign("product_kind_option", getProductKindOption());
}

$smarty->display('product_regist.html');

?>
