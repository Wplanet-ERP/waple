<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where          = "1=1";
$set_get            = isset($_GET['sch_set']) ? $_GET['sch_set'] : "";
$sch_set_w_no_get   = isset($_GET['sch_set_w_no']) ? $_GET['sch_set_w_no'] : "";
$q_sch_get          = isset($_GET['q_sch']) ? $_GET['q_sch'] : "";
$sch_get            = isset($_GET['sch']) ? $_GET['sch'] : "";

$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

if(!empty($sch_brand)) {
    $add_where      .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$out_memo_get   = isset($_GET['out_memo']) ? $_GET['out_memo'] : "";
$sch_prd_g1_get = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2_get = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd_get    = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

$sch_reg_s_date                 = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : "";
$sch_reg_e_date                 = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : "";
$sch_task_run_s_date            = isset($_GET['sch_task_run_s_date']) ? $_GET['sch_task_run_s_date'] : "";
$sch_task_run_e_date            = isset($_GET['sch_task_run_e_date']) ? $_GET['sch_task_run_e_date'] : "";
$sch_work_state_get             = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_wd_dp_no_state_get         = isset($_GET['sch_wd_dp_no_state']) ? $_GET['sch_wd_dp_no_state'] : "";
$sch_s_name_get                 = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
$sch_req_team                   = isset($_GET['sch_req_team']) ? $_GET['sch_req_team'] : "";
$sch_req_s_name_get             = isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : "";
$sch_service_apply_get          = isset($_GET['sch_service_apply']) ? $_GET['sch_service_apply'] : "";
$sch_run_team                   = isset($_GET['sch_run_team']) ? $_GET['sch_run_team'] : "";
$sch_run_s_name_get             = isset($_GET['sch_run_s_name']) ? $_GET['sch_run_s_name'] : "";
$sch_run_direct_get             = isset($_GET['sch_run_direct']) ? $_GET['sch_run_direct'] : "";
$sch_work_time_s_get            = isset($_GET['sch_work_time_s']) ? $_GET['sch_work_time_s'] : "";
$sch_work_time_e_get            = isset($_GET['sch_work_time_e']) ? $_GET['sch_work_time_e'] : "";
$sch_dp_c_no_get                = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_wd_c_no_get                = isset($_GET['sch_wd_c_no']) ? $_GET['sch_wd_c_no'] : "";
$sch_extension_get              = isset($_GET['sch_extension']) ? $_GET['sch_extension'] : "";
$sch_extension_date_get         = isset($_GET['sch_extension_date']) ? $_GET['sch_extension_date'] : "";
$sch_req_evaluation_get         = isset($_GET['sch_req_evaluation']) ? $_GET['sch_req_evaluation'] : "";
$sch_req_evaluation_memo_get    = isset($_GET['sch_req_evaluation_memo']) ? $_GET['sch_req_evaluation_memo'] : "";
$sch_evaluation_get             = isset($_GET['sch_evaluation']) ? $_GET['sch_evaluation'] : "";
$sch_evaluation_memo_get        = isset($_GET['sch_evaluation_memo']) ? $_GET['sch_evaluation_memo'] : "";
$sch_c_name_get                 = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_t_keyword_get              = isset($_GET['sch_t_keyword']) ? $_GET['sch_t_keyword'] : "";
$sch_r_keyword_get              = isset($_GET['sch_r_keyword']) ? $_GET['sch_r_keyword'] : "";
$sch_dp_no_get                  = isset($_GET['sch_dp_no']) ? $_GET['sch_dp_no'] : "";
$sch_wd_no_get                  = isset($_GET['sch_wd_no']) ? $_GET['sch_wd_no'] : "";
$sch_task_req_get               = isset($_GET['sch_task_req']) ? $_GET['sch_task_req'] : "";
$sch_task_run_get               = isset($_GET['sch_task_run']) ? $_GET['sch_task_run'] : "";
$quick_search_chk_val           = isset($_GET['quick_search_chk']) ? $_GET['quick_search_chk'] : "";
$sch_file_no                    = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";
$sch_run_is_null                = isset($_GET['sch_run_is_null']) ? $_GET['sch_run_is_null'] : "";
$sch_category                   = isset($_GET['sch_category']) ? $_GET['sch_category'] : "";

if (!empty($q_sch_get)) { // 퀵서치
    if ($q_sch_get == "1") { //요청안된건
        $add_where .= " AND (w.work_state='1' OR w.work_state='2')";
    } elseif ($q_sch_get == "2") { //요청중인건
        $add_where .= " AND w.work_state='3'";
    } elseif ($q_sch_get == "3") { //진행중인건
        $add_where .= " AND w.work_state IN(4,5)";
    } elseif ($q_sch_get == "4") { //진행완료건
        $add_where .= " AND w.work_state='6'";
    } elseif ($q_sch_get == "5") { //연장여부 확인요청건
        // D-10일 ~ +5일
        $add_where .= " AND w.work_state='6' AND w.extension_date <= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL 10 DAY), '%Y-%m-%d') AND w.extension_date >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -5 DAY), '%Y-%m-%d') AND (w.extension='2' OR w.extension='0' OR w.extension IS NULL)";
    }elseif ($q_sch_get == "6") { //미완료건
        $add_where .= " AND w.work_state IN(3,4,5)";
    }
    $smarty->assign("q_sch", $q_sch_get);
}

// 상품에 따른 그룹 코드 설정
for ($arr_i = 1; $arr_i < count($product_list); $arr_i++) {
    if ($product_list[$arr_i]['prd_no'] == $sch_prd_get) {
        $sch_prd_g1_get = $product_list[$arr_i]['g1_code'];
        $sch_prd_g2_get = $product_list[$arr_i]['g2_code'];
        break;
    }
}

if (!empty($sch_s_name_get)) { // 업체 담당자
    $add_where .= " AND w.s_no IN (SELECT s_no FROM staff WHERE s_name LIKE '%{$sch_s_name_get}%')";
}

if (!empty($sch_req_team))
{
    $sch_req_team_where = getTeamWhere($my_db, $sch_req_team);
    if($sch_req_team == "00221"){
        $sch_req_team_where .= ",00236";
    }
    $add_where .= " AND w.task_req_team IN({$sch_req_team_where})";
}

if (!empty($sch_req_s_name_get)) { // 업무 요청자
    $add_where .= " AND w.task_req_s_no IN (SELECT s_no FROM staff WHERE s_name LIKE '%{$sch_req_s_name_get}%')";
} else {
    if (!$sch_get && permissionNameCheck($session_permission, "마케터")) {

    } elseif($session_team == '00244'){

    }elseif (!$sch_get && !permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "외주관리자") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "서비스운영")) {
        $sch_req_s_name_get = $session_name;
        $add_where .= " AND w.task_req_s_no='{$session_s_no}'";
    }
}

if (!empty($sch_work_time_s_get) || $sch_work_time_s_get == '0') { // work time s
    if ($sch_work_time_s_get == "null") {
        $add_where .= " AND w.work_time IS NULL";
    } else {
        $add_where .= " AND w.work_time>='" . $sch_work_time_s_get . "'";
    }
}

if (!empty($sch_work_time_e_get) || $sch_work_time_e_get == '0') { // work time e
    if ($sch_work_time_e_get == "null") {
        $add_where .= " AND w.work_time IS NULL";
    } else {
        $add_where .= " AND w.work_time<='" . $sch_work_time_e_get . "'";
    }
}

if (!empty($sch_prd_get)) { // 상품
    $add_where .= " AND w.prd_no='" . $sch_prd_get . "'";
} else {
    if ($sch_prd_g2_get) { // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.display='1' AND prd.k_name_code='{$sch_prd_g2_get}')";
    } elseif ($sch_prd_g1_get) { // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_where .= " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.display='1' AND prd.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1_get}'))";
    }
}

if (!empty($sch_reg_s_date)) { // 작성일
    $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
    $add_where .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
}

if (!empty($sch_reg_e_date)) { // 작성일
    $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
    $add_where .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
}

if (!empty($sch_task_run_s_date)) { // 업무완료일
    $sch_task_run_s_datetime = $sch_task_run_s_date." 00:00:00";
    $add_where .= " AND w.task_run_regdate >= '{$sch_task_run_s_date}'";
}

if (!empty($sch_task_run_e_date)) { // 업무완료일
    $sch_task_run_e_datetime = $sch_task_run_e_date." 23:59:59";
    $add_where .= " AND w.task_run_regdate <= '{$sch_task_run_e_datetime}'";
}

if (!empty($sch_work_state_get) && empty($q_sch_get)) { // 진행상태 [퀵서치를 안한경우]
    $add_where .= " AND w.work_state='" . $sch_work_state_get . "'";
}
if (!empty($sch_wd_dp_no_state_get)) { // 지급/입금상태
    if ($sch_wd_dp_no_state_get == '1') {
        $add_where .= " AND w.wd_no IS NOT NULL AND ((SELECT wd_state FROM withdraw wd where wd.wd_no=w.wd_no) = '1' OR (SELECT wd_state FROM withdraw wd where wd.wd_no=w.wd_no) = '2')";
    } elseif ($sch_wd_dp_no_state_get == '2') {
        $add_where .= " AND (SELECT wd_state FROM withdraw wd where wd.wd_no=w.wd_no) = '3'";
    } elseif ($sch_wd_dp_no_state_get == '3') {
        $add_where .= " AND w.dp_no IS NOT NULL AND ((SELECT dp_state FROM deposit dp where dp.dp_no=w.dp_no) = '1' OR (SELECT dp_state FROM deposit dp where dp.dp_no=w.dp_no) = '3')";
    } elseif ($sch_wd_dp_no_state_get == '4') {
        $add_where .= " AND w.dp_no IS NOT NULL";
    }
}

if ($sch_dp_no_get == 'null') { // dp_no is null
    $add_where .= " AND w.dp_no is null";
}

if ($sch_wd_no_get == 'null') { // wd_no is null
    $add_where .= " AND w.wd_no is null";
}

if (!empty($sch_service_apply_get)) { // Service?
    if ($sch_service_apply_get == '1') {
        $add_where .= " AND w.service_apply = '1'";
    } elseif ($sch_service_apply_get == '2') {
        $add_where .= " AND w.service_apply = '2'";
    }
}

if($sch_run_is_null == "1"){
    $add_where .= " AND (w.task_run_s_no = 0 OR w.task_run_s_no IS NULL)";
}else{

    if (!empty($sch_run_team))
    {
        $sch_run_team_where = getTeamWhere($my_db, $sch_run_team);
        if($sch_run_team == "00221"){
            $sch_run_team_where .= ",00236";
        }
        $add_where .= " AND w.task_run_team IN({$sch_run_team_where})";
    }

    if (!empty($sch_run_s_name_get)) { // 업무 처리자
        $add_where .= " AND w.task_run_s_no IN(SELECT s_no FROM staff WHERE s_name like '%{$sch_run_s_name_get}%')";
    }
}

if (!empty($sch_dp_c_no_get)) { // 입금업체(입금처)
    if ($sch_dp_c_no_get != "null") {
        $add_where .= " AND w.dp_c_no='" . $sch_dp_c_no_get . "'";
    } else {
        $add_where .= " AND (w.dp_c_no IS NULL OR w.dp_c_no ='0')";
    }
}

if (!empty($sch_wd_c_no_get)) { // 출금업체(지급처)
    if ($sch_wd_c_no_get != "null") {
        $add_where .= " AND w.wd_c_no='" . $sch_wd_c_no_get . "'";
    } else {
        $add_where .= " AND (w.wd_c_no IS NULL OR w.wd_c_no ='0')";
    }
}
if (!empty($sch_extension_get)) { // 연장여부
    $add_where .= " AND w.extension='" . $sch_extension_get . "'";
}
if (!empty($sch_extension_date_get)) { //연장여부 마감일
    $add_where .= " AND w.extension_date='" . $sch_extension_date_get . "'";
}

if (!empty($sch_req_evaluation_get)) { //업무요청자 업무평가
    if (permissionNameCheck($session_permission, "대표") || permissionNameCheck($session_permission, "마스터관리자")) {
        $add_where .= " AND w.req_evaluation='" . $sch_req_evaluation_get . "'";
    } else {
        $add_where .= " AND w.req_evaluation='" . $sch_req_evaluation_get . "' AND ((SELECT staff_state FROM staff WHERE s_no=w.task_run_s_no)='2' || (SELECT staff_state FROM staff WHERE s_no=w.task_run_s_no)='3'	)";
    }
}
if (!empty($sch_req_evaluation_memo_get)) { //업무요청자 업무평가 메모
    if ($sch_req_evaluation_memo_get == '1') {
        $add_where .= " AND w.req_evaluation_memo IS NOT NULL AND w.req_evaluation_memo <> ''";
    } elseif ($sch_req_evaluation_memo_get == '2') {
        $add_where .= " AND w.req_evaluation_memo = NULL OR w.req_evaluation_memo = '' ";
    }
}

if (!empty($sch_evaluation_get)) { //업무처리자 업무평가
    if (permissionNameCheck($session_permission, "마케터") || permissionNameCheck($session_permission, "대표") || permissionNameCheck($session_permission, "마스터관리자") || $session_s_no == '19') {
        $add_where .= " AND w.evaluation='" . $sch_evaluation_get . "'";
    } else {
        $add_where .= " AND w.evaluation='" . $sch_evaluation_get . "' AND ((SELECT staff_state FROM staff WHERE s_no=w.task_run_s_no)='2' || (SELECT staff_state FROM staff WHERE s_no=w.task_run_s_no)='3'	)";
    }
}
if (!empty($sch_evaluation_memo_get)) { //업무처리자 업무평가 메모
    if ($sch_evaluation_memo_get == '1') {
        $add_where .= " AND w.evaluation_memo IS NOT NULL AND w.evaluation_memo <> ''";
    } elseif ($sch_evaluation_memo_get == '2') {
        $add_where .= " AND w.evaluation_memo = NULL OR w.evaluation_memo = '' ";
    }
}

if (!empty($sch_c_name_get)) { // 업체명
    $add_where .= " AND w.c_name like '%" . $sch_c_name_get . "%'";
}
if (!empty($sch_t_keyword_get)) { // 타겟키워드 및 타겟채널
    $add_where .= " AND w.t_keyword like '%" . $sch_t_keyword_get . "%'";
}
if (!empty($sch_r_keyword_get)) { // 노출키워드
    $add_where .= " AND w.r_keyword like '%" . $sch_r_keyword_get . "%'";
}
if (!empty($sch_w_no_get)) { // 업무번호
    $add_where .= " AND w.w_no ='" . $sch_w_no_get . "'";
}
if (!empty($sch_task_req_get)) { // 업무요청
    $add_where .= " AND w.task_req like '%" . $sch_task_req_get . "%'";
}
if (!empty($sch_task_run_get)) { // 업무진행
    $add_where .= " AND w.task_run like '%" . $sch_task_run_get . "%'";
}

if (!empty($sch_file_no)) { // 파일번호
    $add_where .= " AND w.file_no = '{$sch_file_no}'";
}

if (!empty($sch_category)) { // 세부카테고리
    $add_where .= " AND w.k_name_code IN(SELECT sub.k_name_code FROM kind AS sub WHERE sub.k_code='work_task_run' AND sub.k_name LIKE '%{$sch_category}%' AND sub.display='1')";
}

if (!!$sch_set_w_no_get) { // 업무 세트의 태그 조건
    $add_where .= " AND w.set_tag IS NOT NULL AND w.set_tag LIKE '%" . $sch_set_w_no_get . "%'";
}

// 정렬순서 토글 & 필드 지정
$add_orderby = "";
$order = isset($_GET['od']) ? $_GET['od'] : "";
$order_type = isset($_GET['by']) ? $_GET['by'] : "1";

if ($order_type == '2') {
    $toggle = "DESC";
} else {
    $toggle = "ASC";
}

if ($order == 1 || $order == 2) {
    if ($order_type == '1') {
        $toggle = "DESC";
    } else {
        $toggle = "ASC";
    }
}

$order_field = array('', 'task_req_dday', 'task_run_dday', 'extension_date');
if ($order && $order < 4) {
    $add_orderby .= " ISNULL($order_field[$order]) ASC, $order_field[$order] $toggle,";
    $add_where .= " AND ((DATE_FORMAT(NOW(), '%Y-%m-%d') <= $order_field[$order]) OR ((w.work_state = '3' OR w.work_state = '4' OR w.work_state = '5') AND $order_field[$order] IS NOT NULL))";
} elseif ($order == 4) {
    $add_orderby .= " ISNULL(task_run_regdate) ASC, task_run_regdate $toggle,";
    $add_where .= " AND w.work_state = '6' AND task_run_regdate  IS NOT NULL";
}

if ($q_sch_get == "5") { // 퀵서치의 연장여부 확인 요청건은 \연장여부 마감일순으로 정렬
    if (!!$sch_set_w_no_get) { // 업무 세트의 경우 세트 태그업무는 무조건 최상단 표시
        $add_orderby .= " w.w_no = '{$sch_set_w_no_get}' DESC, extension_date asc ";
    } else {
        $add_orderby .= " extension_date asc ";
    }
} else {
    if (!!$sch_set_w_no_get) { // 업무 세트의 경우 세트 태그업무는 무조건 최상단 표시
        $add_orderby .= " w.w_no = '{$sch_set_w_no_get}' DESC, w.priority ASC, ";
    }
    $add_orderby .= " w_no desc ";
}

if (!!$sch_set_w_no_get) { // 업무 세트의 경우 세트 태그업무는 무조건 포함 시키기
    $add_where .= " OR w_no = '{$sch_set_w_no_get}' ";
}

# 상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A3', "w_no")
    ->setCellValue('B3', "작성일")
    ->setCellValue('C3', "진행상태")
    ->setCellValue('D3', "희망완료일")
    ->setCellValue('E3', "예정완료일")
    ->setCellValue('F3', "업무완료일")
    ->setCellValue('G3', "업체명")
    ->setCellValue('H3', "업체담당자")
    ->setCellValue('I3', "상품명")
    ->setCellValue('J3', "업무요청자")
    ->setCellValue('K3', "타겟키워드")
    ->setCellValue('L3', "노출키워드")
    ->setCellValue('M3', "업무요청내용")
    ->setCellValue('N3', "판매가 및 기여매출액(VAT 별도)")
    ->setCellValue('O3', "Service 여부")
    ->setCellValue('P3', "사유")
    ->setCellValue('Q3', "업무처리자")
    ->setCellValue('R3', "수량")
    ->setCellValue('S3', "work_time")
    ->setCellValue('T3', "work_value")
    ->setCellValue('U3', "업무진행내용")
    ->setCellValue('V3', "연장여부 마감일")
    ->setCellValue('W3', "연장여부")
    ->setCellValue('X3', "입금액(VAT별도)")
    ->setCellValue('Y3', "입금액(VAT포함)")
    ->setCellValue('Z3', "입금업체(입금처)")
    ->setCellValue('AA3', "입금등록일")
    ->setCellValue('AB3', "입금일")
    ->setCellValue('AC3', "출금액(VAT별도)")
    ->setCellValue('AD3', "출금액(VAT포함)")
    ->setCellValue('AE3', "출금업체(지급처)")
    ->setCellValue('AF3', "출금등록일")
    ->setCellValue('AG3', "출금일")
;

# 리스트 쿼리
$work_sql = "
	SELECT
		w_no,
		w.regdate,
		w.task_req_dday,
		w.task_run_dday,
		w.task_run_regdate,
		w.work_state,
		w.c_name,
		w.t_keyword,
		w.r_keyword,
		w.dp_price,
		w.dp_price_vat,
		w.wd_price,
		w.wd_price_vat,
		w.task_req,
		w.task_run,
		w.quantity,
		w.work_time,
        w.work_value,
		w.extension_date,
		w.extension,
		(SELECT regdate FROM deposit dp where dp.dp_no=w.dp_no) as dp_regdate,
		(SELECT dp_date FROM deposit dp where dp.dp_no=w.dp_no) as dp_date,
		(SELECT regdate FROM withdraw wd where wd.wd_no=w.wd_no) as wd_regdate,
		(SELECT wd_date FROM withdraw wd where wd.wd_no=w.wd_no) as wd_date,
		(SELECT s_name FROM staff s where s.s_no=w.task_req_s_no) as task_req_s_name,
		(SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name,
		(SELECT s_name from staff s where s.s_no=w.s_no) as s_no_name,
		w.prd_no,
		p.title as prd_no_name,
		p.wd_dp_state as prd_no_wd_dp_state,
		w.dp_c_no,
		w.wd_c_no,
		(SELECT c_name from company c where c.c_no=w.dp_c_no) as dp_c_no_name,
		(SELECT c_name from company c where c.c_no=w.wd_c_no) as wd_c_no_name,
		w.selling_price,
		w.service_apply,
		w.service_memo
	FROM `work` w
	LEFT JOIN product p ON p.prd_no=w.prd_no
	WHERE {$add_where} AND p.display='1'
	ORDER BY {$add_orderby}
    LIMIT 1000
";
$work_query = mysqli_query($my_db, $work_sql);
$idx        = 4;
while($work_array = mysqli_fetch_array($work_query))
{
    foreach($work_array as $key => $value) {
        if($work_array[$key][0] === '='){
            $work_array[$key] = "'".$work_array[$key];
        }
    }

    if(!empty($work_array['regdate'])) {
        $regdate_day_value = date("Y/m/d",strtotime($work_array['regdate']));
        $regdate_time_value = date("H:i",strtotime($work_array['regdate']));
    }else{
        $regdate_day_value="";
        $regdate_time_value="";
    }

    if(!empty($work_array['dp_price'])){
        $dp_price_value=number_format($work_array['dp_price']);
    }else{
        $dp_price_value="";
    }
    if(!empty($work_array['dp_price_vat'])){
        $dp_price_vat_value=number_format($work_array['dp_price_vat']);
    }else{
        $dp_price_vat_value="";
    }
    if(!empty($work_array['dp_regdate'])) {
        $dp_regdate_day_value = date("Y/m/d",strtotime($work_array['dp_regdate']));
        $dp_regdate_time_value = date("H:i",strtotime($work_array['dp_regdate']));
    }else{
        $dp_regdate_day_value="";
        $dp_regdate_time_value="";
    }

    if(!empty($work_array['wd_price'])){
        $wd_price_value=number_format($work_array['wd_price']);
    }else{
        $wd_price_value="";
    }
    if(!empty($work_array['wd_price_vat'])){
        $wd_price_vat_value=number_format($work_array['wd_price_vat']);
    }else{
        $wd_price_vat_value="";
    }
    if(!empty($work_array['wd_regdate'])) {
        $wd_regdate_day_value = date("Y/m/d",strtotime($work_array['wd_regdate']));
        $wd_regdate_time_value = date("H:i",strtotime($work_array['wd_regdate']));
    }else{
        $wd_regdate_day_value="";
        $wd_regdate_time_value="";
    }

    if($work_array['extension'] == '0')
        $work_array['extension'] = "";

    //연장 마감일 D-Day
    $extension_d_day = "";
    if($work_array['extension_date'])
        $extension_d_day = intval((strtotime(date("Y-m-d",time()))-strtotime($work_array['extension_date'])) / 86400);

    // 2017-04-13 부터 10일 이내 남은 연장여부 없는 것들만 보여줌
    if($work_array['extension'] != '1' && $work_array['extension'] != '3' && $work_array['extension_date'] >= date('Y-m-d', strtotime('-1 day','2017-04-14')) && $work_array['extension_date'] <= date('Y-m-d', strtotime('+10 day', time()))){
        $extension_alert="연장을 확인해주세요!!";
    }else{
        $extension_alert="";
    }

    // 입금업체 이름 찾기
    $dp_c_no_name2 = "";
    for ($arr_i = 1 ; $arr_i < count($dp_company_list) ; $arr_i++ ){
        if($work_array['dp_c_no'] == $dp_company_list[$arr_i][1] && $work_array['prd_no'] == $dp_company_list[$arr_i][2]){
            if(!!$dp_company_list[$arr_i][3]){
                $dp_c_no_name2 = $dp_company_list[$arr_i][3];
            }else{
                $dp_c_no_name2 = $dp_company_list[$arr_i][0];
            }
        }
    }

    // 출금업체 이름 찾기
    $wd_c_no_name2 = "";
    for ($arr_i = 1 ; $arr_i < count($wd_company_list) ; $arr_i++ ){
        if($work_array['wd_c_no'] == $wd_company_list[$arr_i][1] && $work_array['prd_no'] == $wd_company_list[$arr_i][2]){
            if(!!$wd_company_list[$arr_i][3]){
                $wd_c_no_name2 = $wd_company_list[$arr_i][3];
            }else{
                $wd_c_no_name2 = $wd_company_list[$arr_i][0];
            }
        }
    }

    if(!empty($work_array['selling_price'])){
        $selling_price_val = number_format($work_array['selling_price']);
    }else{
        $selling_price_val = "";
    }

    if(!empty($work_array['service_apply'])){
        $service_apply_val = $work_array['service_apply'] == '1' ? "미적용" : "적용";
    }else{
        $service_apply_val = "";
    }

    if(!empty($work_array['work_value'])){
        $work_value_value=number_format($work_array['work_value']);
    }else{
        $work_value_value="";
    }

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A{$idx}", $work_array['w_no'])
        ->setCellValue("B{$idx}", $work_array['regdate'])
        ->setCellValue("C{$idx}", $work_state[$work_array['work_state']][0])
        ->setCellValue("D{$idx}", $work_array['task_req_dday'])
        ->setCellValue("E{$idx}", $work_array['task_run_dday'])
        ->setCellValue("F{$idx}", $work_array['task_run_regdate'])
        ->setCellValue("G{$idx}", $work_array['c_name'])
        ->setCellValue("H{$idx}", $work_array['s_no_name'])
        ->setCellValue("I{$idx}", $work_array['prd_no_name'])
        ->setCellValue("J{$idx}", $work_array['task_req_s_name'])
        ->setCellValue("K{$idx}", $work_array['t_keyword'])
        ->setCellValue("L{$idx}", $work_array['r_keyword'])
        ->setCellValue("M{$idx}", $work_array['task_req'])
        ->setCellValue("N{$idx}", $selling_price_val)
        ->setCellValue("O{$idx}", $service_apply_val)
        ->setCellValue("P{$idx}", $work_array['service_memo'])
        ->setCellValue("Q{$idx}", $work_array['task_run_s_name'])
        ->setCellValue("R{$idx}", $work_array['quantity'])
        ->setCellValue("S{$idx}", number_format($work_array['work_time'], 1, '.', ''))
        ->setCellValue("T{$idx}", $work_value_value)
        ->setCellValue("U{$idx}", $work_array['task_run'])
        ->setCellValue("V{$idx}", isset($work_array['extension_date'])?date("Y-m-d",strtotime($work_array['extension_date'])):"")
        ->setCellValue("W{$idx}", $work_extension[$work_array['extension']][0])
        ->setCellValue("X{$idx}", $dp_price_value)
        ->setCellValue("Y{$idx}", $dp_price_vat_value)
        ->setCellValue("Z{$idx}", $dp_c_no_name2)
        ->setCellValue("AA{$idx}", $work_array['dp_regdate'])
        ->setCellValue("AB{$idx}", $work_array['dp_date'])
        ->setCellValue("AC{$idx}", $wd_price_value)
        ->setCellValue("AD{$idx}", $wd_price_vat_value)
        ->setCellValue("AE{$idx}", $wd_c_no_name2)
        ->setCellValue("AF{$idx}", $work_array['wd_regdate'])
        ->setCellValue("AG{$idx}", $work_array['wd_date'])
    ;

    $idx++;
}
$idx--;

$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:AG1");
$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A1", "업무 리스트");
$objPHPExcel->getActiveSheet()->getStyle("A1:AG1")->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle("A1:AG1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("A3:AG{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:AG{$idx}")->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:AG3")->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle("A4:AG{$idx}")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("A3:AG3")->getFont()->setSize(11);

$objPHPExcel->getActiveSheet()->getStyle("A3:AG3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
    ->getStartColor()->setARGB('00c4bd97');
$objPHPExcel->getActiveSheet()->getStyle("A3:AG3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A3:AG{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A3:AG{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("G4:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("K4:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("L4:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("M4:M{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("N4:N{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("P4:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("T4:T{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("U4:U{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("X4:X{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("Y4:Y{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("AC4:AC{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("AD4:AD{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("G4:G{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("K4:K{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("L4:L{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("M4:M{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("S4:S{$idx}")->getNumberFormat()->setFormatCode("0.0");
$objPHPExcel->getActiveSheet()->getStyle("P4:P{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("U4:U{$idx}")->getAlignment()->setWrapText(true);

$i2_length  = $idx;
$i2         = 1;

while($i2_length){
    if($i2 > 2)
        $objPHPExcel->getActiveSheet()->getRowDimension($i2)->setRowHeight(20);

    if($i2 > 3 && $i2 % 2 == 1)
        $objPHPExcel->getActiveSheet()->getStyle("A{$i2}:AG{$i2}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ebf1de');

    $i2++;
    $i2_length--;
}

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(12);

$objPHPExcel->getActiveSheet()->setTitle('업무 리스트');
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_".$session_name."_업무 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
