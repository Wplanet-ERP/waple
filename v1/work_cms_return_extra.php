<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_return.php');

$is_with = false;
if($session_my_c_type == '3'){
    $is_with = true;
}
$smarty->assign("is_with", $is_with);

$process = (isset($_POST['process']))?$_POST['process']:"";

if($process == "new_return")
{
    $prev_ord_no    = isset($_POST['parent_order_number']) ? $_POST['parent_order_number'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $result         = false;
    $with_log_c_no  = '2809';

    # 회수 주문번호 생성
    $req_date  = date('Y-m-d');
    $rma_date  = date('Ymd');
    $last_rma_ord_no_sql    = "SELECT REPLACE(order_number, '{$rma_date}_RMA_', '') as ord_no FROM work_cms_return WHERE order_number LIKE '%{$rma_date}_RMA_%' ORDER BY order_number DESC LIMIT 1";
    $last_rma_ord_no_query  = mysqli_query($my_db, $last_rma_ord_no_sql);
    $last_rma_ord_no_result = mysqli_fetch_assoc($last_rma_ord_no_query);

    $last_rma_ord_no = (isset($last_rma_ord_no_result['ord_no']) && !empty($last_rma_ord_no_result['ord_no'])) ? (int)$last_rma_ord_no_result['ord_no'] : 0;
    $ord_no          = $rma_date."_RMA_".sprintf('%04d', $last_rma_ord_no+1);


    # 이전 주문데이터 추가
    $origin_order_sql    = "SELECT dp_c_no, recipient, recipient_addr, recipient_hp, zip_code FROM work_cms WHERE order_number='{$prev_ord_no}' LIMIT 1";
    $origin_order_query  = mysqli_query($my_db, $origin_order_sql);
    $origin_order_result = mysqli_fetch_assoc($origin_order_query);

    $parent_dp_c_no         = "";
    $parent_zip_code        = "";
    $parent_recipient       = "";
    $parent_recipient_addr  = "";
    $parent_recipient_hp    = "";

    if(isset($origin_order_result['dp_c_no'])){
        $parent_dp_c_no         = $origin_order_result['dp_c_no'];
        $parent_zip_code        = $origin_order_result['zip_code'];
        $parent_recipient       = addslashes($origin_order_result['recipient']);
        $parent_recipient_addr  = addslashes($origin_order_result['recipient_addr']);
        $parent_recipient_hp    = $origin_order_result['recipient_hp'];
    }

    $insert_sql = "INSERT INTO `work_cms_return` SET
            order_number          = '{$ord_no}',
            parent_order_number   = '{$prev_ord_no}',
            return_state     = '1',
            return_req_date  = '{$req_date}',
            recipient        = '{$parent_recipient}',
            recipient_addr   = '{$parent_recipient_addr}',
            recipient_hp     = '{$parent_recipient_hp}',
            dp_c_no          = '{$parent_dp_c_no}',
            zip_code         = '{$parent_zip_code}',
            req_s_no         = '{$session_s_no}',
            req_team         = '{$session_team}',
            regdate          = now()
    ";

    $insert_sub_sql = "
        INSERT INTO `work_cms_return_unit` SET 
            order_number = '{$ord_no}',
            regdate      = now()
    ";

    $prd_no           = isset($_POST['new_prd_no']) ? $_POST['new_prd_no'] : "";
    $prev_shop_ord_no = isset($_POST['new_shop_ord_no']) ? $_POST['new_shop_ord_no'] : "";
    $quantities       = isset($_POST['new_quantity']) ? $_POST['new_quantity'] : "";
    $return_purpose   = isset($_POST['new_return_purpose']) ? $_POST['new_return_purpose'] : "";
    $return_reason    = isset($_POST['new_return_reason']) ? $_POST['new_return_reason'] : "";
    $return_type      = isset($_POST['new_return_type']) ? $_POST['new_return_type'] : "";

    if(!empty($prd_no))
    {
        $prd_list     = [];
        $prd_val_list = [];
        for($i=0; $i<count($prd_no); $i++)
        {
            if(isset($prd_no[$i]) && !empty($prd_no[$i]))
            {
                $shop_no_val   = isset($prev_shop_ord_no[$i]) ? $prev_shop_ord_no[$i] : "";
                $quantity_val  = isset($quantities[$i]) ? $quantities[$i] : 1;
                $r_purpose_val = isset($return_purpose[$i]) ? $return_purpose[$i] : 1;
                $r_reason_val  = isset($return_reason[$i]) ? $return_reason[$i] : 1;
                $r_type_val    = isset($return_type[$i]) ? $return_type[$i] : 1;

                $prd_val_list[] = $prd_no[$i];
                $prd_list[$prd_no[$i]] = array('shop_ord_no' => $shop_no_val,'quantity' => $quantity_val, 'r_purpose' => $r_purpose_val, 'r_reason' => $r_reason_val, 'r_type' => $r_type_val);
            }
        }

        if(!empty($prd_list))
        {
            $ins_sql_list = "";
            foreach($prd_list as $p_no => $item)
            {

                $new_ins_sql     = $insert_sql;
                if($p_no)
                {
                    $c_no_sql    = "SELECT prd_cms.manager, prd_cms.manager_team, prd_cms.c_no, (SELECT c.c_name FROM company c WHERE c.c_no=prd_cms.c_no) as c_name FROM product_cms prd_cms WHERE prd_cms.prd_no = '{$p_no}' AND prd_cms.c_no != '0' ORDER BY prd_cms.c_no DESC LIMIT 1";
                    $c_no_query  = mysqli_query($my_db, $c_no_sql);
                    $c_no_result = mysqli_fetch_assoc($c_no_query);

                    $s_no        = isset($c_no_result['manager']) ? $c_no_result['manager'] : 0;
                    $team        = isset($c_no_result['manager_team']) ? $c_no_result['manager_team'] : 0;
                    $c_no        = isset($c_no_result['c_no']) ? $c_no_result['c_no'] : 0;
                    $c_name      = isset($c_no_result['c_name']) ? $c_no_result['c_name'] : "";

                    $new_ins_sql  .= ", parent_shop_ord_no='{$item['shop_ord_no']}', c_no='{$c_no}', c_name='{$c_name}', s_no='{$s_no}', team='{$team}', prd_no = '{$p_no}', quantity='{$item['quantity']}', return_purpose='{$item['r_purpose']}', return_reason='{$item['r_reason']}', return_type='{$item['r_type']}';";
                    $ins_sql_list .= $new_ins_sql;

                    $unit_sql    = "SELECT pcr.option_no, pcr.quantity, (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='{$with_log_c_no}') as sku FROM product_cms_relation pcr WHERE pcr.prd_no='{$p_no}' AND pcr.display='1'";
                    $unit_query  = mysqli_query($my_db, $unit_sql);
                    while($unit_result = mysqli_fetch_assoc($unit_query))
                    {
                        $pcu_quantity    = $item['quantity']*$unit_result['quantity'];
                        $new_ins_sub_sql = $insert_sub_sql.", `option`='{$unit_result['option_no']}', sku='{$unit_result['sku']}', quantity='{$pcu_quantity}';";
                        $ins_sql_list   .= $new_ins_sub_sql;
                    }
                }
            }
            
            if($ins_sql_list)
            {
                if(mysqli_multi_query($my_db, $ins_sql_list) == true){
                    $result = true;
                }else{
                    exit("<script>alert('회수리스트 등록에 실패했습니다.');history.back();</script>");
                }
            }
        }
    }

    if($result){
        exit("<script>if(confirm('회수 리스트에 추가되었습니다. 바로 회수 확인 페이지로 이동하시겠습니까?')){opener.parent.location.href='work_cms_return_list.php?sch_default=N&sch_detail=Y&sch_order_number={$ord_no}';window.close();}else{window.close();}</script>");
    }else{
        exit("<script>alert('회수리스트 등록에 실패했습니다.');history.back();</script>");
    }

}
else
{
    $add_where   = "1=1";
    $add_r_where = "1=1";

    $today_val  = date('Y-m-d');
    $week_val   = date('Y-m-d', strtotime('-1 weeks'));
    $month_val  = date('Y-m-d', strtotime('-1 months'));
    $months_val = date('Y-m-d', strtotime('-3 months'));
    $year_val   = date('Y-m-d', strtotime('-1 years'));
    $years_val  = date('Y-m-d', strtotime('-2 years'));

    $smarty->assign("today_val", $today_val);
    $smarty->assign("week_val", $week_val);
    $smarty->assign("month_val", $month_val);
    $smarty->assign("months_val", $months_val);
    $smarty->assign("year_val", $year_val);
    $smarty->assign("years_val", $years_val);

    $sch_reg_s_date         = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : $years_val;
    $sch_reg_e_date         = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : $today_val;
    $sch_reg_date_type      = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "years";
    $sch_delivery_no 		= isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
    $sch_parent_ord_no 		= isset($_GET['sch_parent_ord_no']) ? $_GET['sch_parent_ord_no'] : "";
    $sch_recipient 		    = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
    $sch_recipient_hp 	    = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
    $sch_recipient_addr 	= isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
    $sch_zip_code 	        = isset($_GET['sch_zip_code']) ? $_GET['sch_zip_code'] : "";

    if(!empty($sch_reg_s_date) || !empty($sch_reg_e_date))
    {
        if(!empty($sch_reg_s_date)){
            $sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
            $add_where .= " AND w.regdate >= '{$sch_reg_s_datetime}'";
            $smarty->assign('sch_reg_s_date', $sch_reg_s_date);
        }

        if(!empty($sch_reg_e_date)){
            $sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
            $add_where .= " AND w.regdate <= '{$sch_reg_e_datetime}'";
            $smarty->assign('sch_reg_e_date', $sch_reg_e_date);
        }
    }
    $smarty->assign('sch_reg_date_type', $sch_reg_date_type);

    if(!empty($sch_delivery_no)){
        $add_where   .= " AND w.order_number IN(SELECT DISTINCT wcd.order_number FROM work_cms_delivery wcd WHERE wcd.delivery_no='{$sch_delivery_no}')";
        $add_r_where .= " AND r.parent_order_number IN(SELECT DISTINCT wcd.order_number FROM work_cms_delivery wcd WHERE wcd.delivery_no='{$sch_delivery_no}')";
        $smarty->assign('sch_delivery_no', $sch_delivery_no);
    }

    if(!empty($sch_parent_ord_no)){
        $add_where   .= " AND w.order_number LIKE '{$sch_parent_ord_no}'";
        $add_r_where .= " AND r.parent_order_number LIKE '{$sch_parent_ord_no}'";
        $smarty->assign('sch_parent_ord_no', $sch_parent_ord_no);
    }

    if(!empty($sch_recipient)){
        $add_where   .= " AND w.recipient LIKE '%{$sch_recipient}%'";
        $add_r_where .= " AND r.recipient LIKE '%{$sch_recipient}%'";
        $smarty->assign('sch_recipient', $sch_recipient);
    }

    if(!empty($sch_recipient_hp)){

        $add_where   .= " AND w.recipient_hp LIKE '%{$sch_recipient_hp}%'";
        $add_r_where .= " AND r.recipient_hp LIKE '%{$sch_recipient_hp}%'";
        $smarty->assign('sch_recipient_hp', $sch_recipient_hp);
    }

    if(!empty($sch_recipient_addr)){
        $add_where   .= " AND w.recipient_addr LIKE '%{$sch_recipient_addr}%'";
        $add_r_where .= " AND r.recipient_addr LIKE '%{$sch_recipient_addr}%'";
        $smarty->assign('sch_recipient_addr', $sch_recipient_addr);
    }

    if(!empty($sch_zip_code)){
        $add_where   .= " AND w.zip_code = '{$sch_zip_code}'";
        $add_r_where .= " AND r.zip_code = '{$sch_zip_code}'";
        $smarty->assign('sch_zip_code', $sch_zip_code);
    }

    /*
    if(!empty($add_r_where)){
        $add_where .= " AND w.order_number NOT IN(SELECT DISTINCT(r.parent_order_number) FROM work_cms_return r WHERE {$add_r_where})";
    }
    */

    // 페이지에 따른 limit 설정
    $pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
    $smarty->assign("page",$pages);

    $num = 10;
    $offset = ($pages-1) * $num;

    // 전체 게시물 수
    $work_cms_total_sql   = "SELECT count(order_number) FROM (SELECT DISTINCT w.order_number FROM work_cms w WHERE {$add_where}) AS cnt";
    $work_cms_total_query = mysqli_query($my_db, $work_cms_total_sql);
    if(!!$work_cms_total_query)
        $work_cms_total_result = mysqli_fetch_array($work_cms_total_query);

    $work_cms_total = $work_cms_total_result[0];

    //페이징
    $page_type  = (isset($_GET['ord_page_type']) && !empty($_GET['ord_page_type'])) ? intval($_GET['ord_page_type']) : "10";
    $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= $page_type;
    $offset 	= ($pages-1) * $num;
    $pagenum 	= ceil($work_cms_total/$num);

    if ($pages >= $pagenum){$pages = $pagenum;}
    if ($pages <= 0){$pages = 1;}

    $search_url = getenv("QUERY_STRING");
    $pagelist	= pagelist($pages, "work_cms_return_extra.php", $pagenum, $search_url);
    $smarty->assign("search_url", $search_url);
    $smarty->assign("total_num", $work_cms_total);
    $smarty->assign("pagelist", $pagelist);
    $smarty->assign("ord_page_type", $page_type);


    //GET ORDER Number
    $work_cms_ord_sql    = "SELECT DISTINCT w.order_number FROM work_cms w WHERE {$add_where} AND w.order_number is not null ORDER BY w.w_no DESC LIMIT {$offset},{$num}";
    $work_cms_ord_query  = mysqli_query($my_db, $work_cms_ord_sql);
    $order_number_list  = [];
    while($order_number = mysqli_fetch_assoc($work_cms_ord_query)){
        $order_number_list[] =  "'".$order_number['order_number']."'";
    }
    $order_numbers = implode(',', $order_number_list);

    // 리스트 쿼리
    $work_cms_sql = "
		SELECT
		    w.w_no,
			w.order_date,
		 	DATE_FORMAT(w.order_date, '%Y-%m-%d') as ord_date,
			DATE_FORMAT(w.order_date, '%H:%i') as ord_time,
			w.order_number,
			w.recipient,
			w.recipient_hp,
			w.recipient_addr,
			IF(w.zip_code, CONCAT('[',w.zip_code,']'), '') as postcode,
			w.stock_date,
			w.delivery_memo,
		    w.prd_no,
            (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
            w.quantity,
		    w.shop_ord_no
		FROM work_cms w
		WHERE w.order_number IN({$order_numbers})
		ORDER BY w.order_number ASC, w.w_no ASC, w.prd_no ASC
	";
    $work_cms_query  = mysqli_query($my_db, $work_cms_sql);

    $work_cms_list   = [];
    $return_idx      = 1;
    if($work_cms_query)
    {
        while($work_array = mysqli_fetch_assoc($work_cms_query))
        {
            if(empty($work_array['shop_ord_no'])){
                $new_shop_no = "W".$work_array['order_number']."_".$work_array['prd_no'];
                $upd_sql = "UPDATE work_cms SET shop_ord_no='{$new_shop_no}' WHERE w_no='{$work_array['w_no']}'";
                mysqli_query($my_db, $upd_sql);

                $work_array['shop_ord_no'] = $new_shop_no;
            }

            if(!isset($work_cms_list[$work_array['order_number']]))
            {
                $return_idx = 1;
                if(isset($work_array['recipient_hp']) && !empty($work_array['recipient_hp'])) {
                    $f_hp = substr($work_array['recipient_hp'], 0, 4);
                    $e_hp = substr($work_array['recipient_hp'], 7, 15);
                    $work_array['recipient_sc_hp'] = $f_hp . "***" . $e_hp;
                }
                $work_array['first_shop_no'] = $work_array['shop_ord_no'];

                $delivery_list  = [];
                $delivery_sql   = "SELECT `no`, delivery_no, delivery_type FROM work_cms_delivery WHERE order_number='{$work_array['order_number']}' GROUP BY delivery_no";
                $delivery_query = mysqli_query($my_db, $delivery_sql);
                while($delivery = mysqli_fetch_assoc($delivery_query))
                {
                    $delivery_list[$delivery['no']] = array('delivery_no' => $delivery['delivery_no'], 'delivery_type' => $delivery['delivery_type']);
                }
                $work_array['delivery_list'] = $delivery_list;
                $work_cms_list[$work_array['order_number']] = $work_array;
            }else{
                if(empty($work_cms_list[$work_array['order_number']]['first_shop_no'])){
                    $work_cms_list[$work_array['order_number']]['first_shop_no'] = $work_array['shop_ord_no'];
                }
            }

            $product = array(
                'unit_idx'      => $work_array['order_number']."_".$return_idx,
                'prd_no'        => $work_array['prd_no'],
                'prd_name'      => $work_array['prd_name'],
                'quantity'      => $work_array['quantity'],
                'shop_ord_no'   => $work_array['shop_ord_no']
            );

            $work_cms_list[$work_array['order_number']]['prd_list'][] = $product;

            $return_idx++;
        }
    }

    $smarty->assign("return_purpose_option", getReturnPurposeOption());
    $smarty->assign("return_reason_option", getReturnReasonOption());
    $smarty->assign("return_type_option", getReturnTypeOption());
    $smarty->assign("work_cms_list", $work_cms_list);

    $smarty->display('work_cms_return_extra.html');
}
?>
