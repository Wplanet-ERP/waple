<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/wise_csm.php');
require('inc/model/Staff.php');

# 접근 권한
if (!(permissionNameCheck($session_permission, "대표") || permissionNameCheck($session_permission, "재무관리자") || permissionNameCheck($session_permission, "마스터관리자")) && $session_s_no != "102"){
    $smarty->display('access_error.html');
    exit;
}

$add_where          = "1=1";
$sch_csm_no         = isset($_GET['sch_csm_no']) ? $_GET['sch_csm_no'] : "";
$sch_regdate        = isset($_GET['sch_regdate']) ? $_GET['sch_regdate'] : "";
$sch_ev_type        = isset($_GET['sch_ev_type']) ? $_GET['sch_ev_type'] : "";
$sch_question_type  = isset($_GET['sch_question_type']) ? $_GET['sch_question_type'] : "1";
$sch_s_score        = isset($_GET['sch_s_score']) ? $_GET['sch_s_score'] : "1";
$sch_e_score        = isset($_GET['sch_e_score']) ? $_GET['sch_e_score'] : "5";
$sch_q5             = isset($_GET['sch_q5']) ? $_GET['sch_q5'] : "";
$sch_q5_not_null    = isset($_GET['sch_q5_not_null']) ? $_GET['sch_q5_not_null'] : "";
$sch_s_no           = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$staff_model        = Staff::Factory();
$cs_staff_list      = $staff_model->getActiveTeamStaff("00244");
$cs_staff_list[10000] = "100번";
$cs_staff_list[10001] = "101번";
$cs_staff_list[10002] = "102번";

if(!empty($sch_csm_no))
{
    $add_where .= " AND `ce`.csm_e_no = '{$sch_csm_no}'";
    $smarty->assign('sch_csm_no', $sch_csm_no);
}

if(!empty($sch_regdate))
{
    $add_where .= " AND `ce`.regdate = '{$sch_regdate}'";
    $smarty->assign('sch_regdate', $sch_regdate);
}

if(!empty($sch_ev_type))
{
    if($sch_ev_type == '2'){
        $add_where .= " AND `ce`.type = '1'";
    }elseif($sch_ev_type == '3'){
        $add_where .= " AND `ce`.type = '2'";
    }

    $smarty->assign('sch_ev_type', $sch_ev_type);
}

if(!empty($sch_q5))
{
    $add_where .= " AND `ce`.q5_text like '%{$sch_q5}%'";
    $smarty->assign('sch_q5', $sch_q5);
}

if(!empty($sch_q5_not_null))
{
    $add_where .= " AND `ce`.q5_text != ''";
    $smarty->assign('sch_q5_not_null', $sch_q5_not_null);
}

if(!empty($sch_s_no))
{
    $add_where .= " AND `ce`.s_no = '{$sch_s_no}'";
    $smarty->assign('sch_s_no', $sch_s_no);
}

switch($sch_question_type)
{
    case 1:
        $add_where .= " AND ((`ce`.q1_score >= {$sch_s_score} AND `ce`.q1_score <= {$sch_e_score}) 
            OR (`ce`.q2_score >= {$sch_s_score} AND `ce`.q2_score <= {$sch_e_score})
            OR (`ce`.q3_score >= {$sch_s_score} AND `ce`.q3_score <= {$sch_e_score})
            OR (`ce`.q4_score >= {$sch_s_score} AND `ce`.q4_score <= {$sch_e_score}))
        ";
        break;
    case 2:
        $add_where .= " AND (`ce`.q1_score >= {$sch_s_score} AND `ce`.q1_score <= {$sch_e_score})";
        break;
    case 3:
        $add_where .= " AND (`ce`.q2_score >= {$sch_s_score} AND `ce`.q2_score <= {$sch_e_score})";
        break;
    case 4:
        $add_where .= " AND (`ce`.q3_score >= {$sch_s_score} AND `ce`.q3_score <= {$sch_e_score})";
        break;
    case 5:
        $add_where .= " AND (`ce`.q4_score >= {$sch_s_score} AND `ce`.q4_score <= {$sch_e_score})";
        break;
}
$smarty->assign("sch_question_type", $sch_question_type);
$smarty->assign("sch_s_score", $sch_s_score);
$smarty->assign("sch_e_score", $sch_e_score);

# 전체 게시물 수
$csm_ev_total_sql       = "SELECT count(`ce`.csm_e_no) as cnt FROM csm_evaluation `ce` WHERE {$add_where}";
$csm_ev_total_query	    = mysqli_query($my_db, $csm_ev_total_sql);
$csm_ev_total_result    = mysqli_fetch_array($csm_ev_total_query);
$csm_ev_total           = $csm_ev_total_result['cnt'];

# 페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($csm_ev_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist	= pagelist($pages, "wise_csm_evaluation_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $csm_ev_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$csm_ev_sql  = "
    SELECT 
        *,
        DATE_FORMAT(`ce`.regdate, '%Y-%m-%d') as reg_date,
        DATE_FORMAT(`ce`.regdate, '%H:%i') as reg_time,
        (SELECT s.s_name FROM staff s WHERE s.s_no = ce.s_no LIMIT 1) as s_name
    FROM csm_evaluation `ce`
    WHERE {$add_where}
    ORDER BY `ce`.csm_e_no DESC
    LIMIT {$offset}, {$num}
";
$csm_ev_query           = mysqli_query($my_db, $csm_ev_sql);
$csm_ev_list            = [];
$evaluation_type_option = getEvaluationTypeOption();
while($csm_ev = mysqli_fetch_assoc($csm_ev_query))
{
    $score_avg              = round((($csm_ev['q1_score'] + $csm_ev['q2_score'] + $csm_ev['q3_score'] +$csm_ev['q4_score'])/4), 1);
    $csm_ev['score_avg']    = $score_avg;
    $csm_ev['type_name']    = $evaluation_type_option[($csm_ev['type']+1)];
    $csm_ev['s_name']       = empty($csm_ev['s_name']) ? $cs_staff_list[$csm_ev['s_no']] : $csm_ev['s_name'];

    $csm_ev_list[]          = $csm_ev;
}

$smarty->assign("cs_staff_list", $cs_staff_list);
$smarty->assign("evaluation_type_option", $evaluation_type_option);
$smarty->assign("question_type_option", getQuestionTypeOption());
$smarty->assign("score_type_option", getScoreTypeOption());
$smarty->assign("page_type_option", getPageTypeOption('4'));
$smarty->assign("csm_ev_list", $csm_ev_list);

$smarty->display('wise_csm_evaluation_list.html');
?>
