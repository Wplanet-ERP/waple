<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/project.php');
require('inc/model/MyQuick.php');
require('inc/model/MyCompany.php');
require('inc/model/Team.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$list_url       = "project_list.php";

if($process == "work_state")
{
    $pj_no      = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $work_state = isset($_POST['work_state']) ? $_POST['work_state'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_sql    = "UPDATE project SET work_state='{$work_state}' WHERE pj_no='{$pj_no}'";
    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('진행상태 변경에 실패했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }else{
        exit("<script>alert('진행상태 변경했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "19";
$nav_title   = "사업리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 박미소님 권한부여
$is_admin = false;
if($session_s_no == "62"){
    $is_admin = true;
}
$smarty->assign("is_admin", $is_admin);

# 사업리스트 검색처리
$add_where          = "`p`.display = '1'";
$sch_pj_no          = isset($_GET['sch_pj_no']) ? $_GET['sch_pj_no'] : "";
$sch_work_state     = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "5";
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_pj_name        = isset($_GET['sch_pj_name']) ? $_GET['sch_pj_name'] : "";
$sch_contract_type  = isset($_GET['sch_contract_type']) ? $_GET['sch_contract_type'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_team           = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_resource_type  = isset($_GET['sch_resource_type']) ? $_GET['sch_resource_type'] : "";
$sch_cal_state      = isset($_GET['sch_cal_state']) ? $_GET['sch_cal_state'] : "";
$sch_achievements   = isset($_GET['sch_achievements']) ? $_GET['sch_achievements'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "pj_e_date";
$ori_ord_type       = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "pj_e_date";
$ord_type_by        = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
$url_check_str  = $_SERVER['QUERY_STRING'];
$url_chk_result = false;
if(empty($url_check_str)){
    $url_check_team_where   = getTeamWhere($my_db, $session_team);
    $url_check_sql          = "SELECT count(pj_no) as cnt FROM project WHERE `team` IN ({$url_check_team_where})";
    $url_check_query  		= mysqli_query($my_db, $url_check_sql);
    $url_check_result 		= mysqli_fetch_assoc($url_check_query);

    if($url_check_result['cnt'] == 0){
        $sch_team       = "all";
        $url_chk_result = true;
    }
}

if(!empty($sch_pj_no)) {
    $add_where .= " AND p.pj_no = '{$sch_pj_no}'";
    $smarty->assign("sch_pj_no",$sch_pj_no);
}

if(!empty($sch_work_state)) {
    $add_where .= " AND p.work_state = '{$sch_work_state}'";
    $smarty->assign("sch_work_state", $sch_work_state);
}

if(!empty($sch_my_c_no)) {
    $add_where .= " AND p.my_c_no = '{$sch_my_c_no}'";
    $smarty->assign("sch_my_c_no",$sch_my_c_no);
}

if(!empty($sch_pj_name)) {
    $add_where .= " AND p.pj_name like '%{$sch_pj_name}%'";
    $smarty->assign("sch_pj_name",$sch_pj_name);
}

if(!empty($sch_contract_type)) {
    $add_where .= " AND p.contract_type = '{$sch_contract_type}'";
    $smarty->assign("sch_contract_type", $sch_contract_type);
}

if(!empty($sch_state)) {
    $add_where .= " AND p.state = '{$sch_state}'";
    $smarty->assign("sch_state",$sch_state);
}

if (!empty($sch_team))
{
    if($sch_team != 'all')
    {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        if($sch_team_code_where){
            $add_where .= " AND `p`.team IN ({$sch_team_code_where})";
        }
    }

    $smarty->assign("sch_team",$sch_team);
}else{

    $sch_team_code_where = getTeamWhere($my_db, $session_team);
    if($sch_team_code_where){
        $add_where .= " AND `p`.team IN ({$sch_team_code_where})";
    }
    $smarty->assign("sch_team",$session_team);
}

if(!empty($sch_manager))
{
    $add_where .= " AND p.manager IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_manager}%')";
    $smarty->assign("sch_manager", $sch_manager);
}

if(!empty($sch_resource_type))
{

    $resource_where_list = [];
    foreach($sch_resource_type as $sch_resource){
        $resource_where_list[] = "p.resource_type LIKE '%{$sch_resource}%'";
    }

    $resource_where = implode(" OR ", $resource_where_list);
    $add_where .= " AND ({$resource_where})";
    $smarty->assign("sch_resource_type", $sch_resource_type);
}

if(!empty($sch_cal_state))
{
    $add_where .= " AND p.cal_state = '{$sch_cal_state}'";
    $smarty->assign("sch_cal_state",$sch_cal_state);
}

if(!empty($sch_achievements))
{
    $add_where .= " AND p.achievements LIKE '%{$sch_achievements}%'";
    $smarty->assign("sch_achievements",$sch_achievements);
}

$add_orderby = "";

if($ori_ord_type != $ord_type && empty($ord_type_by)){
    $ord_type_by = "2";
}

if(!empty($ord_type_by))
{
    $ord_type_list      = [];
    $ord_type_list[]    = $ord_type;
    $orderby_val        = "";

    if($ord_type_by == '1'){
        $orderby_val = "ASC";
    }elseif($ord_type_by == '2'){
        $orderby_val = "DESC";
    }

    foreach($ord_type_list as $ord_type_val){
        $add_orderby .= "{$ord_type_val} {$orderby_val}, ";
    }
}
$add_orderby .= "pj_no DESC";

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);


# 전체 게시물 수
$pj_total_sql		= "SELECT count(pj_no) FROM (SELECT `p`.pj_no FROM project `p` WHERE {$add_where}) AS cnt";
$pj_total_query	= mysqli_query($my_db, $pj_total_sql);
if(!!$pj_total_query)
    $pj_total_result = mysqli_fetch_array($pj_total_query);

$pj_staff_total = $pj_total_result[0];

$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= 10;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($pj_staff_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

if(empty($url_check_str) && $url_chk_result) {
    $search_url = "ori_ord_type=pj_e_date&ord_type=pj_e_date&ord_type_by=2&sch_work_state=5&sch_team=all";
}else{
    $search_url = getenv("QUERY_STRING");
}

$page		= pagelist($pages, "project_list.php", $pagenum, $search_url);
$smarty->assign("total_num", $pj_staff_total);
$smarty->assign("pagelist", $page);
$smarty->assign("search_url", $search_url);

# 프로젝트 쿼리
$project_sql = "
    SELECT 
        *,
        (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=p.my_c_no) as my_c_name,
        (SELECT t.depth FROM team t WHERE t.team_code = p.team) as t_depth,
        (SELECT s.s_name FROM staff s WHERE s.s_no = p.manager) as manager_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no = p.req_s_no) as req_s_name,
        (SELECT COUNT(pir.pj_r_no) FROM project_input_report as pir WHERE pir.pj_no = p.pj_no AND pir.active='1') as total_pj_staff,
        (SELECT SUM(pir.resource) FROM project_input_report as pir WHERE pir.pj_no = p.pj_no AND pir.active='1') as total_pj_resource,
        (SELECT SUM(pe.supply_price) FROM project_expenses pe WHERE pe.pj_no=p.pj_no AND pe.display='1') as total_price
    FROM project `p` 
    WHERE {$add_where} 
    ORDER BY {$add_orderby}
    LIMIT {$offset}, {$num}
 ";
$project_query          = mysqli_query($my_db, $project_sql);
$project_list           = [];
$my_company_model       = MyCompany::Factory();
$team_model             = Team::Factory();
$my_company_list        = $my_company_model->getList();
$team_full_name_list    = $team_model->getTeamFullNameList();
$state_option           = getProjectStateOption();
$work_state_option      = getProjectWorkStateOption();
$cal_state_option       = getCalStateOption();
$resource_type_option   = getResourceTypeOption();
$contract_type_option   = getContractTypeOption();
while($project = mysqli_fetch_assoc($project_query))
{
    $project['team_name']           = getTeamFullName($my_db, $project['t_depth'], $project['team']);
    $project['state_name']          = isset($state_option[$project['state']]) ? $state_option[$project['state']] : "";
    $project['state_color']         = $project['state'] == '2' ? 'black' : 'red';
    $project['cal_state_color']     = $project['cal_state'] == '2' ? 'black' : 'red';
    $project['cal_state_name']      = isset($cal_state_option[$project['cal_state']]) ? $cal_state_option[$project['cal_state']] : "";
    $project['total_pj_staff']      = isset($project['total_pj_staff']) && !empty($project['total_pj_staff']) ? $project['total_pj_staff'] : '0';
    $project['total_pj_resource']   = isset($project['total_pj_resource']) && !empty($project['total_pj_resource']) ? $project['total_pj_resource'] : '0';
    $project['contract_type_name']  = isset($contract_type_option[$project['contract_type']]) ? $contract_type_option[$project['contract_type']] : "";
    $project['work_state_color']    = isset($work_state_option[$project['work_state']]) ? $work_state_option[$project['work_state']]['color'] : "";

    $resource_type_name = [];
    if(!empty($project['resource_type'])){
        $resource_type_val_list = explode(',', $project['resource_type']);
        foreach($resource_type_val_list as $resource_type){
            $resource_type_name[] = $resource_type_option[$resource_type];
        }
    }
    $project['resource_type_name'] = !empty($resource_type_name) ? implode('  ', $resource_type_name) : "";

    $project_list[] = $project;
}

$smarty->assign("my_company_list", $my_company_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("state_option", $state_option);
$smarty->assign("cal_state_option", $cal_state_option);
$smarty->assign("contract_type_option", $contract_type_option);
$smarty->assign("work_state_option", $work_state_option);
$smarty->assign("resource_type_option", $resource_type_option);
$smarty->assign("project_list", $project_list);

$smarty->display('project_list.html');
?>
