<?php
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
include_once('inc/model/Advertising.php');

# 파일 변수
$file_name      = $_FILES["excel_file"]["tmp_name"];
$search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";
$excel_type     = isset($_POST['excel_type']) ? $_POST['excel_type'] : "";
$time_type      = isset($_POST['time_type']) ? $_POST['time_type'] : "";
$media_type     = isset($_POST['media_type']) ? $_POST['media_type'] : "";

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$adver_model    = Advertising::Factory();
$regdate        = date('Y-m-d H:i:s');
$media          = $media_type;
$product        = $excel_type;
$ins_data       = [];
$start_idx      = ($excel_type == '2') ? 12 : 2;

for ($i = $start_idx; $i <= $totalRow; $i++)
{
    $base_price = 0;
    $new_adv_s_date = $new_adv_e_date = $notice = "";

    if($excel_type == '1')
    {
        $base_date      = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue())); // 날짜
        $base_time      = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue())); // 시간
        $base_price     = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue())); // 단가
        $notice         = ($time_type == '1') ? "#미판" : "";

        $base_date      = str_replace(" ","", str_replace(".","-", $base_date));
        $new_s_date     = date("Y-m-d", strtotime($base_date));
        $new_e_date     = date("Y-m-d", strtotime("{$new_s_date} +1 days"));
        $new_time_list  = explode("~", $base_time);
        $new_s_time     = $new_time_list[0];
        $new_e_time     = $new_time_list[1];

        $new_adv_s_date = $new_s_date." ".$new_s_time;
        $new_adv_e_date = $new_s_date." ".$new_e_time;

        if($new_e_time == '24:00'){
            $new_adv_e_date = $new_e_date." 00:00";
        }
    }
    elseif($excel_type == '2')
    {
        $check          = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue())); // 상품명 (체크용으로 사용)
        $base_date_val  = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue())); // 날짜
        $base_time      = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue())); // 시간
        $base_price     = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue())); // 단가
        $target         = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue())); // 타겟팅
        $notice         = ($time_type == '1') ? "#미판" : "";

        if(!empty($target)){
            $target  = str_replace("(","", str_replace(")","", $target));
            $notice .= !empty($notice) ? " #{$target}" : "#{$target}";
        }

        $base_date      = str_replace(" ","", str_replace(".","-", $base_date_val));
        $new_s_date     = date("Y-m-d", strtotime($base_date));
        $new_e_date     = date("Y-m-d", strtotime("{$new_s_date} +1 days"));
        $new_time_list  = explode("-", $base_time);
        $new_s_time     = $new_time_list[0];
        $new_e_time     = $new_time_list[1];

        $new_adv_s_date = $new_s_date." ".$new_s_time;
        $new_adv_e_date = $new_s_date." ".$new_e_time;
        $new_adv_e_date = date("Y-m-d H:i", strtotime("{$new_adv_e_date} +1 minutes"));

        if(empty($check)){
            break;
        }
    }

    $ins_data[] = array(
        "media"         => $media,
        "type"          => $time_type,
        "product"       => $product,
        "price"         => $base_price,
        "adv_s_date"    => $new_adv_s_date,
        "adv_e_date"    => $new_adv_e_date,
        "reg_s_no"      => $session_s_no,
        "notice"        => $notice,
        "regdate"       => $regdate,
    );
}

if (!$adver_model->multiInsert($ins_data)){
    echo "업로드 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    exit;
}else{
    exit("<script>alert('업로드 성공했습니다.');location.href='advertising_management.php?{$search_url}';</script>");
}

?>
