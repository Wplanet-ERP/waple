<?php
ini_set('max_execution_time', '3600');
ini_set('memory_limit', '5G');

require('Classes/PHPExcel.php');
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/helper/work_sales.php');
require('inc/model/Company.php');
require('inc/model/Custom.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');
require('inc/model/WorkCmsSettle.php');

# 파일 변수
$company_model      = Company::Factory();
$dp_c_list          = $company_model->getDpTotalList();
$unit_model         = ProductCmsUnit::Factory();
$settle_model       = WorkCmsSettle::Factory();

$upload_type        = isset($_POST['new_excel_type']) ? $_POST['new_excel_type'] : "";
$settle_type        = isset($_POST['settle_type']) ? $_POST['settle_type'] : "";
$upload_kind        = $settle_type;
$settle_dp_c_no     = isset($_POST['settle_dp_c_no']) ? $_POST['settle_dp_c_no'] : "";
$settle_month       = isset($_POST['settle_month']) ? $_POST['settle_month'] : date('Y-m');
$settle_org_price   = isset($_POST['settle_org_price']) ? $_POST['settle_org_price'] : "";
$settle_total_price = isset($_POST['settle_total_price']) ? $_POST['settle_total_price'] : "";
$settle_c_no        = isset($_POST['settle_c_no']) ? $_POST['settle_c_no'] : "2809";
$settle_file        = $_FILES["settle_file"];
$waple_c_no         = 1314;
$waple_c_name       = "닥터피엘";
$convert_price      = 1;

if($upload_type == '4'){
    $settle_type        = isset($_POST['amazon_settle_type']) ? $_POST['amazon_settle_type'] : "";
    $upload_kind        = $settle_type;
    $settle_dp_c_no     = "5711";
    $settle_dp_c_name   = "아마존_닥터피엘(미국)";

    switch ($settle_type){
        case "21":
            $settle_dp_c_no     = "5711";
            $settle_dp_c_name   = "아마존_닥터피엘(미국)";
            $waple_c_no         = 1314;
            $waple_c_name       = "닥터피엘";
            break;
        case "25":
            $settle_dp_c_no     = "5795";
            $settle_dp_c_name   = "아마존_아이레놀(일본)";
            $waple_c_no         = 2388;
            $waple_c_name       = "아이레놀";
            break;
        case "27":
            $settle_dp_c_no     = "5885";
            $settle_dp_c_name   = "아마존_아이레놀(미국)";
            $waple_c_no         = 2388;
            $waple_c_name       = "아이레놀";
            break;
    }
    $settle_month       = isset($_POST['amazon_settle_month']) ? $_POST['amazon_settle_month'] : date('Y-m');
    $convert_price      = $settle_total_price/$settle_org_price;
    $settle_file        = $_FILES["amazon_settle_file"];
}
elseif($upload_type == '9'){
    $settle_type    = "";
    $settle_month   = isset($_POST['log_settle_month']) ? $_POST['log_settle_month'] : date('Y-m');
    $settle_file    = $_FILES["log_settle_file"];
    $upload_kind    = "";
}

$cur_date           = date("Y-m-d H:i:s");
$reg_s_no           = $session_s_no;

if($upload_type == '3' && empty($settle_type)){
    exit("<script>alert('정산자료가 없습니다. 다시 시도해 주세요');location.href='work_cms_sales_settlement.php';</script>");
}

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($settle_file['name']) && !empty($settle_file['name'])){
    $upload_file_path = add_store_file($settle_file, "upload_management");
    $upload_file_name = $settle_file['name'];
}

$upload_data  = array(
    "upload_type"   => $upload_type,
    "upload_kind"   => $upload_kind,
    "upload_date"   => $settle_month,
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $reg_s_no,
    "regdate"       => $cur_date,
);
$upload_model->insert($upload_data);

$file_no         = $upload_model->getInsertId();
$excel_file_path = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($excel_file_path);
$excel->setActiveSheetIndex(0);

$start_idx = 2;
if($settle_type == '11'){
    $start_idx = 11;
}
elseif($settle_type == '20'){
    $start_idx    = 4;
}

$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$chk_data_list              = [];
$ins_data_list              = [];
$ins_tmp_data_list          = [];
$upd_data_list              = [];
$settle_type_match_option   = getSettleTypeMatchingCompanyList();
$settle_gubun_option        = getSettleGubunOption();
$empty_settle_brand_option  = getEmptySettleBrandOption();

if($upload_type == '9') # 물류비정산
{
    $company_item   = $company_model->getItem($settle_c_no);
    $settle_c_name  = $company_item['c_name'];

    for ($i = 2; $i <= $totalRow; $i++)
    {
        $brand_name         = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 업체명/브랜드명
        $c_no               = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 업체번호
        $settle_gubun_val   = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 구분
        $settle_cs_val      = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));    // 매출/비용
        $settle_price       = (int)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));    // price
        $settle_price_vat   = (int)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    // price
        $notice             = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));    // 비고
        $settle_cs          = ($settle_cs_val == "비용") ? 2 : 1;
        $settle_gubun       = array_search($settle_gubun_val, $settle_gubun_option);

        if(empty($brand_name)){
            break;
        }

        if($c_no){
            $brand_item = $company_model->getItem($c_no);
        }else{
            if($brand_name == '와이즈미디어커머스'){
                $brand_item = array("c_no" => 1113, "c_name" => $brand_name);
            }else{
                $brand_item = $company_model->getNameWhereItem($brand_name);
            }
        }

        if(empty($brand_item)){
            echo "없는 브랜드명입니다. 브랜드명 : {$brand_name}, Row : {$i}";
            exit;
        }

        $ins_data_list[]    = array(
            "file_no"           => $file_no,
            "settle_type"       => $settle_type,
            "c_no"              => $brand_item['c_no'],
            "c_name"            => $brand_item['c_name'],
            "settle_dp_c_no"    => "2809",
            "settle_dp_c_name"  => "위드플레이스",
            "settle_c_no"       => $settle_c_no,
            "settle_c_name"     => $settle_c_name,
            "settle_month"      => $settle_month,
            "is_return"         => 2,
            "settle_gubun"      => $settle_gubun,
            "settle_cs"         => $settle_cs,
            "settle_price"      => $settle_price,
            "settle_price_vat"  => $settle_price_vat,
            "sales_price"       => 0,
            'reg_s_no'          => $reg_s_no,
            'reg_team'          => $session_team,
            'notice'            => $notice,
            'regdate'           => $cur_date
        );
    }
}
elseif($upload_type == '4') # 아마존
{
    $amazon_c_no        = $settle_dp_c_no;
    $amazon_c_name      = $settle_dp_c_name;
    $tmp_ins_data       = [];
    for ($i = 3; $i <= $totalRow; $i++)
    {
        $tran_type      = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));    // transaction-type
        $order_id       = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    // order-id
        $sku            = (string)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue()));    // sku
        $price_type     = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));    // price-type
        $price          = (double)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));    // price
        $price_type2    = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));    // price-type2
        $price2         = (double)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue()));   // price2
        $promotion_type = (string)trim(addslashes($objWorksheet->getCell("AF{$i}")->getValue()));   // Shipping
        $etc_price      = !empty($objWorksheet->getCell("AG{$i}")->getValue()) ? (double)trim(addslashes($objWorksheet->getCell("AG{$i}")->getValue())) : (double)trim(addslashes($objWorksheet->getCell("AJ{$i}")->getValue()));   // etc_price
        $quantity       = (int)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));    // quantity

        if($tran_type == 'CouponRedemptionFee'){
            continue;
        }

        $settle_cs = $settle_gubun = "";
        switch($tran_type)
        {
            case 'Order':
                $settle_cs      = 1;
                $settle_gubun   = 1;
                break;
            case 'Refund':
                $settle_cs      = 2;
                $settle_gubun   = 3;
                break;
            case 'DisposalComplete':
                $settle_cs      = 2;
                $settle_gubun   = 8;
                break;
            case 'Subscription Fee':
                $settle_cs      = 2;
                $settle_gubun   = 4;
                break;
            case 'Storage Fee':
            case 'StorageRenewalBilling':
                $settle_cs      = 2;
                $settle_gubun   = 5;
                break;
            case 'WAREHOUSE_DAMAGE':
                $settle_cs      = 1;
                $settle_gubun   = 11;
                break;
            case 'FBA Inbound Placement Service Fee':
                $settle_cs      = 2;
                $settle_gubun   = 10;
                break;
            case 'Previous Reserve Amount Balance':
            case 'Current Reserve Amount':
                $settle_cs      = 2;
                $settle_gubun   = 12;
                break;
            case '페이먼트수수료':
                $settle_cs      = 2;
                $settle_gubun   = 14;
                break;
            case 'FREE_REPLACEMENT_REFUND_ITEMS':
                $settle_cs      = 1;
                $settle_gubun   = 11;
                break;
            case 'REVERSAL_REIMBURSEMENT':
                $settle_cs      = 1;
                $settle_gubun   = 16;
                break;
            case 'COMPENSATED_CLAWBACK':
                $settle_cs      = 2;
                $settle_gubun   = 15;
                break;
        }

        if(empty($settle_gubun)){
            echo "{$tran_type} 확인되지 않는 Transaction Type 입니다";
            exit;
        }

        if(!empty($sku))
        {
            if(!isset($tmp_ins_data[$order_id][$sku][$settle_gubun]))
            {
                $chk_unit_item = $unit_model->getSkuItem($sku, $amazon_c_no);

                if(empty($chk_unit_item)){
                    echo "{$sku} 등록되지 않는 SKU 입니다";
                    exit;
                }

                $tmp_ins_data[$order_id][$sku][$settle_gubun] = array(
                    "file_no"           => $file_no,
                    "settle_type"       => $settle_type,
                    "c_no"              => $chk_unit_item['brand'],
                    "c_name"            => $chk_unit_item['brand_name'],
                    "settle_c_no"       => $amazon_c_no,
                    "settle_c_name"     => $amazon_c_name,
                    "settle_dp_c_no"    => $amazon_c_no,
                    "settle_dp_c_name"  => $amazon_c_name,
                    "settle_month"      => $settle_month,
                    "is_return"         => 2,
                    "settle_gubun"      => $settle_gubun,
                    "settle_cs"         => $settle_cs,
                    "log_c_no"          => $amazon_c_no,
                    "sku"               => $sku,
                    "settle_price"      => 0,
                    "settle_price_vat"  => 0,
                    "sales_price"       => 0,
                    'reg_s_no'          => $reg_s_no,
                    'reg_team'          => $session_team,
                    'regdate'           => $cur_date
                );
            }

            if($tran_type == "Order")
            {
                $chk_price_type = "";
                $chk_price      = "";

                if(!empty($price_type)){
                    $chk_price_type = $price_type;
                }
                elseif(!empty($price_type2)){
                    $chk_price_type = $price_type2;
                }

                if(empty($chk_price_type))
                {
                    if(!empty($promotion_type)){
                        $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $etc_price;
                    }else{
                        continue;
                    }
                }
                else
                {
                    switch($chk_price_type){
                        case "Principal":
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['sales_price']  += $price;
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $price;
                            break;
                        case "Shipping":
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['sales_price']  += $price;
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $price;
                            break;
                        case "FBAPerUnitFulfillmentFee":
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $price2;
                            break;
                        case "Commission":
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $price2;
                            break;
                        case "ShippingChargeback":
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $price2;
                            break;
                    }
                }
            }
            elseif($tran_type == "Refund")
            {
                $chk_price_type = "";
                $chk_price      = "";

                if(!empty($price_type)){
                    $chk_price_type = $price_type;
                }
                elseif(!empty($price_type2)){
                    $chk_price_type = $price_type2;
                }

                if(empty($chk_price_type)){
                    if(!empty($promotion_type)){
                        $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $etc_price;
                    }else{
                        continue;
                    }
                }else{
                    switch($chk_price_type){
                        case "Principal":
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['sales_price']  += $price;
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $price;
                            break;
                        case "Shipping":
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['sales_price']  += $price;
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $price;
                            break;
                        case "RefundCommission":
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $price2;
                            break;
                        case "Commission":
                            $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $price2;
                            break;
                    }
                }
            }
            elseif($tran_type == "WAREHOUSE_DAMAGE") {
                $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $etc_price;
            }else{
                $tmp_ins_data[$order_id][$sku][$settle_gubun]['settle_price_vat'] += $etc_price;
            }
        }
        else
        {
            if(!isset($tmp_ins_data[$order_id]["empty"][$settle_gubun]))
            {
                $tmp_ins_data[$order_id]["empty"][$settle_gubun] = array(
                    "file_no"           => $file_no,
                    "settle_type"       => $settle_type,
                    "c_no"              => $waple_c_no,
                    "c_name"            => $waple_c_name,
                    "settle_c_no"       => $amazon_c_no,
                    "settle_c_name"     => $amazon_c_name,
                    "settle_dp_c_no"    => $amazon_c_no,
                    "settle_dp_c_name"  => $amazon_c_name,
                    "settle_month"      => $settle_month,
                    "is_return"         => 2,
                    "settle_gubun"      => $settle_gubun,
                    "settle_cs"         => $settle_cs,
                    "log_c_no"          => $amazon_c_no,
                    "sku"               => "",
                    "settle_price"      => 0,
                    "settle_price_vat"  => 0,
                    "sales_price"       => 0,
                    'reg_s_no'          => $reg_s_no,
                    'reg_team'          => $session_team,
                    'regdate'           => $cur_date
                );
            }

            if(!empty($settle_gubun)){
                $tmp_ins_data[$order_id]["empty"][$settle_gubun]['settle_price_vat'] += $etc_price;
            }
        }

    }

    if(!empty($tmp_ins_data))
    {
        foreach($tmp_ins_data as $ord_no => $ord_data)
        {
            foreach($ord_data as $ord_sku => $ord_sku_data)
            {
                foreach($ord_sku_data as $ord_gubun => $ord_gubun_data)
                {
                    if(!isset($ins_tmp_data_list[$ord_sku][$ord_gubun])){
                        $ins_tmp_data_list[$ord_sku][$ord_gubun] = $ord_gubun_data;
                    }else{
                        $ins_tmp_data_list[$ord_sku][$ord_gubun]['sales_price']  += $ord_gubun_data['sales_price'];
                        $ins_tmp_data_list[$ord_sku][$ord_gubun]['settle_price_vat'] += $ord_gubun_data['settle_price_vat'];
                    }
                }
            }
        }
    }

    if(!empty($ins_tmp_data_list))
    {
        foreach($ins_tmp_data_list as $tmp_sku => $tmp_sku_data) {
            foreach($tmp_sku_data as $tmp_gubun => $tmp_gubun_data)
            {
                $tmp_gubun_data['org_sales_price']   = $tmp_gubun_data['sales_price'];
                $tmp_gubun_data['org_settle_price']  = $tmp_gubun_data['settle_price_vat'];
                $tmp_gubun_data['sales_price']      *= $convert_price;
                $tmp_gubun_data['settle_price_vat'] *= $convert_price;
                $ins_data_list[] = $tmp_gubun_data;
            }
        }
    }
}
else
{
    for ($i = $start_idx; $i <= $totalRow; $i++)
    {
        #변수 초기화
        $settle_date = $ord_no = $ord_date = $notice = $order_prd_no = "";
        $sales_price = $settle_price_vat = 0;

        $settle_data   = array(
            "file_no"           => $file_no,
            "settle_type"       => $settle_type,
            "settle_dp_c_no"    => $settle_dp_c_no,
            "settle_dp_c_name"  => !empty($settle_dp_c_no) ? $dp_c_list[$settle_dp_c_no] : "",
            "settle_month"      => $settle_month,
            "is_return"         => 2,
            "settle_gubun"      => 1,
            "settle_cs"         => 1,
            'reg_s_no'          => $session_s_no,
            'reg_team'          => $session_team,
            'regdate'           => $cur_date
        );

        if($settle_type == '1') # 이니시스 신용카드
        {
            $check_idx          = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 지급일
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 주문번호
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));   // 거래금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue()));   // 지급액
            $status             = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));   // 상태

            if($check_idx == '합계'){
                continue;
            }

            if($status != "승인"){
                $notice = "#{$status}";
            }
        }
        elseif($settle_type == '2' || $settle_type == '3') # 이니시스 계좌이체 || 가상계좌
        {
            $check_idx          = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 지급일
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));    // 주문번호
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));    // 거래금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));    // 지급액
            $status             = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));    // 상태

            if($check_idx == '합계'){
                continue;
            }

            if($status != "승인"){
                $notice = "#{$status}";
            }
        }
        elseif($settle_type == '4' || $settle_type == '5') # 네이버페이_정산형, 스마트스토어
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    // 정산완료일
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));    // 결제금액
            $settle_price_tax1  = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));    // 주문 관리 수수료
            $settle_price_tax2  = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));    // 매출연동 수수료
            $settle_price_tax3  = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));    // 무이자할부 수수료
            $settle_price_vat   = (int)$sales_price + ((int)$settle_price_tax1 + (int)$settle_price_tax2 + (int)$settle_price_tax3);
            $gubun              = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));    // 구분1
            $gubun2             = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 구분2

            if($gubun == '정산후 취소'){
                $notice .= "#정산후 취소";
            }

            if($gubun2 == '배송비'){
                $notice .= "#배송비";
            }
        }
        elseif($settle_type == '15') # 스마트스토어_혜택정산
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    // 정산완료일
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));    // 금액
            $settle_price_vat   = $sales_price;
            $status             = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));    // 혜택구분
            $notice            .= "#{$status}";
        }
        elseif($settle_type == '14') # 네이버페이_결제형
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));    // 정산완료일
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));    // 결제금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));    // 정산금액
            $status             = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));    // 정산상태

            if($status == '정산후 취소'){
                $notice .= "#정산후 취소";
            }
        }
        elseif($settle_type == '17') # 네이버페이_정산형&스마트스토어_공제환급
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));    // 정산완료일
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));    // 금액
            $settle_price_vat   = $sales_price;
            $status             = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));    // 정산상태

            if(!empty($status)){
                $notice .= "#{$status}";
            }
        }
        elseif($settle_type == '6') # 쿠팡
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 주문번호
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));    // 매출금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));   // 정산금액
            $refund_qty         = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));    // 환불수량
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));   // 정산예정일
            if($settle_date == '-'){
                $settle_date = "";
            }

            if($refund_qty > 0){
                $notice = "#환불";
            }

            $settle_data['settle_dp_c_no']      = "1812";
            $settle_data['settle_dp_c_name']    = "쿠팡";
        }
        elseif($settle_type == '7') # 정산통합
        {
            $settle_type_name   = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 정산자료
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 정산완료일
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 주문번호
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));    // 매출금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));    // 정산금액
            $notice             = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));    // 비고

            $settle_data['settle_dp_c_no']     = array_search($settle_type_name, $dp_c_list);
            $settle_data['settle_dp_c_name']   = $settle_type_name;
        }
        elseif($settle_type == '8') # 카카오페이
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    // 정산예정일
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));    // 판매금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));    // 정산금액
            $status             = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));    // 구분

            if($status != '상품'){
                $notice = "#{$status}";
            }
        }
        elseif($settle_type == '9') # 지마켓 상품
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));    // 정산예정일
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));    // 판매금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("AD{$i}")->getValue()));   // 정산금액
            $refund_date        = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));    // 환불일

            if(!empty($refund_date)){
                $settle_date = $refund_date;
                $notice = "#환불";
            }

            $settle_data['settle_dp_c_no']   = "1834";
            $settle_data['settle_dp_c_name'] = "지마켓";
        }
        elseif($settle_type == '10') # 지마켓 배송비
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));    // 정산예정일
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));    // 판매금액(배송비)
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));    // 정산금액(배송비)
            $notice             = "#배송비";

            $settle_data['settle_dp_c_no']   = "1834";
            $settle_data['settle_dp_c_name'] = "지마켓";
        }
        elseif($settle_type == '19') # 지마켓 환급금
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));    // 정산완료일
            $settle_date        = !empty($settle_date) ? date("Y-m-d", strtotime($settle_date)) : "";
            $sales_price        = 0;
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));    // 구매자 부담 환급금

            $settle_data['settle_dp_c_no']   = "1834";
            $settle_data['settle_dp_c_name'] = "지마켓";
        }
        elseif($settle_type == '11') # 11번가
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 정산일자
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));    // 정산금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));    // 송금액

            $settle_data['settle_dp_c_no']   = "1818";
            $settle_data['settle_dp_c_name'] = "11번가";
        }
        elseif($settle_type == '12') # 오늘의집
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 정산기준일
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));    // 결제금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    // 정산금액
            $delivery_price     = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));    // 배송비
            $type_check         = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 정산구분

            if($type_check != "구매확정"){
                continue;
            }

            if($delivery_price != '0'){
                $notice = "#배송비";
            }

            $settle_data['settle_dp_c_no']   = "3337";
            $settle_data['settle_dp_c_name'] = "오늘의집";
        }
        elseif($settle_type == '13') # 지그재그
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 정산완료일
            $settle_date        = !empty($settle_date) ? date("Y-m-d", strtotime($settle_date)) : "";
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));    // 결제금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue()));    // 정산금액
            $service_type       = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));    // 서비스타입

            if(!empty($service_type)){
                continue;
            }

            $settle_data['settle_dp_c_no']   = "4737";
            $settle_data['settle_dp_c_name'] = "지그재그";
        }
        elseif($settle_type == '16') # 옥션
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 주문번호
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));   // 상품금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));   // 송금액
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 지급예정일

            $settle_data['settle_dp_c_no']     = "1863";
            $settle_data['settle_dp_c_name']   = "옥션";
        }
        elseif($settle_type == '18') # 카카오톡스토어&카카오톡선물하기
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 정산확정예정일
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("AE{$i}")->getValue()));  // 정산기준금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("AR{$i}")->getValue()));  // 판매정산금액
        }
        elseif($settle_type == '20') # 현대이지웰
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 주문번호
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));   // 상품금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));   // 송금액
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue()));  // 지급예정일

            $settle_data['settle_dp_c_no']     = "5641";
            $settle_data['settle_dp_c_name']   = "현대이지웰";
        }
        elseif($settle_type == '22') # 베네피아
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 주문번호
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));   // 매출액(배송비포함)
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));   // 매입액(배송비포함)
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 정산확정일
            $settle_date        = !empty($settle_date) ? date("Y-m-d", strtotime($settle_date)) : "";
            $type_check         = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 정산구분

            if($type_check == '정산확정 후 취소'){
                $notice = "# 정산후 취소";
            }

            $settle_data['settle_dp_c_no']     = "5157";
            $settle_data['settle_dp_c_name']   = "베네피아";
        }
        elseif($settle_type == '23') # 에이블리 구매확정
        {
            $order_prd_no       = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 상품번호
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));   // 지급예정일

            $settle_data['settle_dp_c_no']      = "5548";
            $settle_data['settle_dp_c_name']    = "에이블리";
            $settle_data['order_prd_no']        = $order_prd_no;
        }
        elseif($settle_type == '24') # 에이블리 정산관리
        {
            $order_prd_no       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 상품번호
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 결제금액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));   // 정산금

            $sales_price        = str_replace(',','', $sales_price);
            $settle_price_vat   = str_replace(',','', $settle_price_vat);
            $settle_price       = (int)($settle_price_vat/1.1);

            $chk_sql        = "SELECT `no`, order_number, file_no, COUNT(*) as cnt FROM work_cms_settlement WHERE order_prd_no = '{$order_prd_no}'";
            $chk_query      = mysqli_query($my_db, $chk_sql);
            while($chk_result= mysqli_fetch_assoc($chk_query))
            {
                $chk_ord_no     = $chk_result['order_number'];
                $chk_file_no    = $chk_result['file_no'];

                if($chk_result['cnt'] > 1)
                {
                    $order_sql          = "SELECT SUM(unit_price) as total_price FROM work_cms WHERE order_number='{$chk_ord_no}' AND unit_price > 0";
                    $order_query        = mysqli_query($my_db, $order_sql);
                    $ord_item           = mysqli_fetch_assoc($order_query);

                    $chk_ord_list       = [];
                    $ord_total_price    = $ord_item['total_price'];
                    $total_settle_price = $settle_price_vat;
                    $total_sales_price  = $sales_price;
                    $chk_settle_price   = $total_settle_price;
                    $chk_sales_price    = $total_sales_price;

                    $chk_ord_sql    = "SELECT c_no, c_name, SUM(unit_price) as unit_total, (SELECT wcs.`no` FROM work_cms_settlement wcs WHERE order_prd_no='{$order_prd_no}' AND wcs.c_no=w.c_no) as settle_no FROM work_cms w WHERE unit_price > 0 AND order_number='{$chk_ord_no}' GROUP BY c_no";
                    $chk_ord_query  = mysqli_query($my_db, $chk_ord_sql);
                    while($chk_ord = mysqli_fetch_assoc($chk_ord_query))
                    {
                        $chk_ord_list[] = $chk_ord;
                    }

                    $chk_ord_cnt = count($chk_ord_list);
                    $chk_ord_idx = 1;
                    foreach($chk_ord_list as $chk_ord)
                    {
                        $cal_ord_per        = $chk_ord['unit_total']/$ord_total_price;
                        $cal_settle_price   = round($total_settle_price*$cal_ord_per);
                        $cal_sales_price    = round($total_sales_price*$cal_ord_per);

                        $chk_settle_price -= $cal_settle_price;
                        $chk_sales_price  -= $cal_sales_price;

                        if($chk_ord_cnt == $chk_ord_idx){
                            $cal_settle_price += $chk_settle_price;
                            $cal_sales_price  += $chk_sales_price;
                        }

                        $upd_data_list[] = array(
                            "no"                => $chk_ord['settle_no'],
                            "sales_price"       => $cal_sales_price,
                            "settle_price"      => (int)($cal_settle_price/1.1),
                            "settle_price_vat"  => $cal_settle_price,
                            "file_no"           => $chk_file_no.",".$file_no
                        );

                        $chk_ord_idx++;
                    }

                }else{
                    $upd_data_list[] = array(
                        "no"                => $chk_result['no'],
                        "sales_price"       => $sales_price,
                        "settle_price"      => $settle_price,
                        "settle_price_vat"  => $settle_price_vat,
                        "file_no"           => $chk_file_no.",".$file_no
                    );
                }
            }

            continue;
        }
        elseif($settle_type == '26') # 토스페이먼츠
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 주문번호
            $sales_price        = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   // 결제&취소액
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));   // 당일 정산액
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 정산액 입금일
            $type_check         = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 정산구분

            if($type_check == '취소' || $type_check == '부분취소'){
                $notice = "# {$type_check}";
            }
        }
        elseif($settle_type == '28') # 카카오페이(지정택배 정산내역)
        {
            $ord_no             = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 주문번호
            $settle_date        = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));    // 충전금반영일
            $settle_price_vat   = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));    // 정산금액
            $notice             = "#지정택배정산";
        }

        $ord_no = str_replace("-P1", "", $ord_no);
        if($settle_data['settle_dp_c_no'] == 0 || empty($settle_data['settle_dp_c_no']))
        {
            echo "판매처를 찾을 수 없습니다.<br/>ROW: {$i}, 판매처:{$settle_data['settle_dp_c_name']}, 주문번호: {$ord_no}, 정산일: {$settle_date}, 판매금액: {$sales_price}, 정산금액: {$settle_price_vat}";
            exit;
        }

        $settle_data['settle_c_no']     = isset($settle_type_match_option[$settle_type][$settle_data['settle_dp_c_no']]) ? $settle_type_match_option[$settle_type][$settle_data['settle_dp_c_no']]['c_no'] : $settle_data['settle_dp_c_no'];
        $settle_data['settle_c_name']   = isset($settle_type_match_option[$settle_type][$settle_data['settle_dp_c_no']]) ? $settle_type_match_option[$settle_type][$settle_data['settle_dp_c_no']]['c_name'] : $settle_data['settle_dp_c_name'];

        if(!empty($settle_date))
        {
            if(strpos($settle_date, "T") !== false){
                $settle_date = str_replace("T"," ", $settle_date);
                $settle_date = date("Y-m-d", strtotime($settle_date));
            }elseif(strpos($settle_date, "-") !== false){
                $settle_date = $settle_date;
            }elseif(strpos($settle_date, ".") !== false){
                $settle_date = str_replace(".","-", $settle_date);
            }elseif(strpos($settle_date, "/") !== false){
                $settle_date = str_replace("/","-", $settle_date);
            }else{
                $settle_time_cal = ($settle_date-25569)*86400;
                $settle_date     = date("Y-m-d", $settle_time_cal);
            }

            if(!empty($settle_date) && $settle_date != '0000-00-00'){
                $settle_data['settle_date'] = $settle_date;
            }
        }

        $settle_data['sales_price']      = str_replace(',','', $sales_price);
        $settle_data['settle_price_vat'] = str_replace(',','', $settle_price_vat);
        $settle_data['settle_price']     = (int)($settle_data['settle_price_vat']/1.1);
        $settle_data['notice']           = $notice;

        # 중복 체크
        $check_sql      = "SELECT count(*) as cnt FROM work_cms_settlement WHERE order_number='{$ord_no}' AND settle_date='{$settle_date}' AND sales_price='{$sales_price}' AND settle_price_vat='{$settle_price_vat}'";
        $check_query    = mysqli_query($my_db, $check_sql);
        $check_result   = mysqli_fetch_assoc($check_query);

        if($check_result['cnt'] > 0){
            echo "[중복 확인] 동일한 내용이 이미 업로드 된것 같습니다.<br/>ROW: {$i}, 주문번호: {$ord_no}, 정산일: {$settle_date}, 판매금액: {$sales_price}, 정산금액: {$settle_price_vat}";
            exit;
        }

        if($settle_type != '23' && ($settle_data['settle_price_vat'] == 0 && $settle_data['sales_price'] == 0)){
            continue;
        }

        if(!empty($ord_no))
        {
            $settle_data['total_ord_no'] = $ord_no;
            $settle_data['order_number'] = $ord_no;

            $is_order = true;
            if($settle_data['settle_price_vat'] < 0 || $settle_data['sales_price'] < 0)
            {
                $return_sql      = "SELECT parent_order_number, c_no, c_name, COUNT(DISTINCT c_no) as c_cnt FROM work_cms_return WHERE parent_order_number='{$ord_no}'";
                $return_query    = mysqli_query($my_db, $return_sql);
                $return_item     = mysqli_fetch_assoc($return_query);

                if(!isset($return_item['c_no']))
                {
                    $return_sql      = "SELECT parent_order_number, c_no, c_name, COUNT(DISTINCT c_no) as c_cnt FROM work_cms_return WHERE parent_order_number=(SELECT order_number FROM work_cms WHERE origin_ord_no='{$ord_no}' AND unit_price > 0 LIMIT 1)";
                    $return_query    = mysqli_query($my_db, $return_sql);
                    $return_item     = mysqli_fetch_assoc($return_query);
                }

                if($return_item['c_cnt'] > 0)
                {
                    $is_order                   = false;
                    $settle_data["is_return"]   = 1;
                    $settle_data['c_no']        = ($return_item['c_cnt'] > 1) ? 9999 : $return_item['c_no'];
                    $settle_data['c_name']      = ($return_item['c_cnt'] > 1) ? "혼합" : $return_item['c_name'];
                    $settle_data['total_ord_no']= $return_item['parent_order_number'];

                    if($settle_data['c_no'] == 9999){
                        $settle_data['chk_type']    = "return";
                        $settle_data['total_price'] = $return_item['c_cnt'];
                        $chk_data_list[]            = $settle_data;
                    }else{
                        $ins_data_list[] = $settle_data;
                    }
                }
            }

            if($is_order)
            {
                $order_sql      = "SELECT order_number, c_no, c_name, COUNT(DISTINCT c_no) as c_cnt, SUM(unit_price) as total_price FROM work_cms WHERE order_number='{$ord_no}' AND unit_price > 0";
                $order_query    = mysqli_query($my_db, $order_sql);
                $ord_item       = mysqli_fetch_assoc($order_query);

                if(!isset($ord_item['c_no']))
                {
                    $order_sql      = "SELECT order_number, c_no, c_name, COUNT(DISTINCT c_no) as c_cnt, SUM(unit_price) as total_price FROM work_cms WHERE origin_ord_no='{$ord_no}' AND unit_price > 0";
                    $order_query    = mysqli_query($my_db, $order_sql);
                    $ord_item       = mysqli_fetch_assoc($order_query);
                }

                if($ord_item['c_cnt'] > 0) {
                    $settle_data['c_no']         = ($ord_item['c_cnt'] > 1) ? 9999 : $ord_item['c_no'];
                    $settle_data['c_name']       = ($ord_item['c_cnt'] > 1) ? "혼합" : $ord_item['c_name'];
                    $settle_data['total_ord_no'] = $ord_item['order_number'];
                }
                else {
                    if($settle_type == '17'){
                        $settle_data['c_no']   = isset($empty_settle_brand_option[$settle_dp_c_no]) ? $empty_settle_brand_option[$settle_dp_c_no]['c_no'] : 0;
                        $settle_data['c_name'] = isset($empty_settle_brand_option[$settle_dp_c_no]) ? $empty_settle_brand_option[$settle_dp_c_no]['c_name'] : 0;
                    }else{
                        $settle_data['c_no']   = 0;
                        $settle_data['c_name'] = "NULL";
                    }
                }

                if($settle_data['c_no'] == 9999){
                    $settle_data['chk_type']    = "order";
                    $settle_data['total_price'] = $ord_item['total_price'];
                    $chk_data_list[] = $settle_data;
                }else{
                    $ins_data_list[] = $settle_data;
                }
            }
        }
        else
        {
            $settle_data['c_no']    = 0;
            $settle_data['c_name']  = "NULL";
            $ins_data_list[]        = $settle_data;
        }
    }
}

if(!empty($upd_data_list))
{
    if (!$settle_model->multiUpdate($upd_data_list)){
        echo "정산관리 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
        exit;
    }else{
        exit("<script>alert('정산 데이터가 반영 되었습니다.');location.href='work_cms_sales_settlement.php?sch_settle_month=&sch_file_no={$file_no}&sch_settle_c_no=5548';</script>");
    }
}
else
{
    if(!empty($chk_data_list))
    {
        foreach($chk_data_list as $chk_data)
        {
            $chk_type           = $chk_data['chk_type'];
            $order_number       = $chk_data['total_ord_no'];
            $ord_total_price    = $chk_data['total_price'];
            $total_settle_price = $chk_data['settle_price_vat'];
            $total_sales_price  = $chk_data['sales_price'];
            $chk_settle_price   = $total_settle_price;
            $chk_sales_price    = $total_sales_price;
            $chk_ord_list       = [];

            unset($chk_data['total_price']);
            unset($chk_data['chk_type']);

            if($chk_type == 'order')
            {
                $chk_ord_sql    = "SELECT c_no, c_name, SUM(unit_price) as unit_total FROM work_cms WHERE unit_price > 0 AND order_number='{$order_number}' GROUP BY c_no";
                $chk_ord_query  = mysqli_query($my_db, $chk_ord_sql);
                while($chk_ord = mysqli_fetch_assoc($chk_ord_query))
                {
                    $chk_ord_list[] = $chk_ord;
                }
            }
            elseif($chk_type == 'return')
            {
                $chk_ord_sql    = "SELECT c_no, c_name FROM work_cms_return WHERE parent_order_number='{$order_number}' GROUP BY c_no";
                $chk_ord_query  = mysqli_query($my_db, $chk_ord_sql);
                while($chk_ord = mysqli_fetch_assoc($chk_ord_query))
                {
                    $chk_ord['unit_total'] = 1;
                    $chk_ord_list[] = $chk_ord;
                }
            }

            $chk_ord_cnt = count($chk_ord_list);
            $chk_ord_idx = 1;
            foreach($chk_ord_list as $chk_ord)
            {
                $chk_settle_data            = $chk_data;
                $chk_settle_data['c_no']    = $chk_ord['c_no'];
                $chk_settle_data['c_name']  = $chk_ord['c_name'];

                $cal_ord_per        = $chk_ord['unit_total']/$ord_total_price;
                $cal_settle_price   = round($total_settle_price*$cal_ord_per);
                $cal_sales_price    = round($total_sales_price*$cal_ord_per);

                $chk_settle_price -= $cal_settle_price;
                $chk_sales_price  -= $cal_sales_price;

                if($chk_ord_cnt == $chk_ord_idx){
                    $cal_settle_price += $chk_settle_price;
                    $cal_sales_price  += $chk_sales_price;
                }

                $chk_settle_data['settle_price']     = (int)($cal_settle_price/1.1);
                $chk_settle_data['settle_price_vat'] = $cal_settle_price;
                $chk_settle_data['sales_price']      = $cal_sales_price;

                $chk_ord_idx++;
                $ins_data_list[] = $chk_settle_data;
            }
        }
    }

    $settle_model = WorkCmsSettle::Factory();
    if (!$settle_model->multiInsert($ins_data_list)){
        echo "정산관리 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
        exit;
    }else{
        exit("<script>alert('정산 데이터가 반영 되었습니다.');location.href='work_cms_sales_settlement.php?sch_settle_month=&sch_file_no={$file_no}&sch_settle_type={$settle_type}';</script>");
    }
}

?>
