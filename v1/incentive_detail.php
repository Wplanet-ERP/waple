<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');

$proc=(isset($_POST['process']))?$_POST['process']:"";

if($proc=="f_incentive_date") {
	$wp_no=(isset($_POST['wp_no']))?$_POST['wp_no']:"";
	$incentive_date_post=(isset($_POST['incentive_date']))?$_POST['incentive_date']:"";
    $search_url_post=isset($_POST['search_url'])?$_POST['search_url']:"";

	$work_package_sql="UPDATE work_package wp SET 
						wp.incentive_date='".$incentive_date_post."'
						WHERE wp.wp_no='".$wp_no."'
					";
	//echo $work_package_sql;exit;
	if(mysqli_query($my_db,$work_package_sql))
		exit("<script>alert('work package의 인센티브 정산 년/월을 설정하였습니다.');location.href='incentive_detail.php?$search_url_post';</script>");
	else
		exit("<script>alert('work package의 인센티브 정산 년/월 설정에 실패 하였습니다.');location.href='incentive_detail.php?$search_url_post';</script>");

} elseif($proc=="f_i_incentive_date") {
	$wp_no=(isset($_POST['wp_no']))?$_POST['wp_no']:"";
	$i_no=(isset($_POST['i_no']))?$_POST['i_no']:"";
    $search_url_post=isset($_POST['search_url'])?$_POST['search_url']:"";

	$work_package_sql="UPDATE work_package wp SET 
						wp.i_no='".$i_no."'
						WHERE wp.wp_no='".$wp_no."'
					";
	//echo $work_package_sql;exit;
	if(mysqli_query($my_db,$work_package_sql))
		exit("<script>alert('incentive의 인센티브 정산 년/월을 설정하였습니다.');location.href='incentive_detail.php?$search_url_post';</script>");
	else
		exit("<script>alert('incentive의 인센티브 정산 년/월 설정에 실패 하였습니다.');location.href='incentive_detail.php?$search_url_post';</script>");

} else {

	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
	$add_where="1=1";
	$sch_incentive_date_get=isset($_GET['sch_incentive_date'])?$_GET['sch_incentive_date']:"";
	$sch_s_no_get=isset($_GET['sch_s_no'])?$_GET['sch_s_no']:"";
	$sch_i_no_get=isset($_GET['sch_i_no'])?$_GET['sch_i_no']:"";

	// 접근 권한
	if (!permissionNameCheck($session_permission,"대표") && !permissionNameCheck($session_permission,"재무관리자") && $sch_s_no_get != $session_s_no){
		$smarty->display('access_error.html');
		exit;
	}
    
	if(!empty($sch_incentive_date_get)) {
		$add_where.=" AND wp.incentive_date='".$sch_incentive_date_get."'";
		$smarty->assign("sch_incentive_date",$sch_incentive_date_get);
	}
	if(!empty($sch_s_no_get)) {
		$add_where.=" AND wp.s_no='".$sch_s_no_get."'";
		$smarty->assign("sch_s_no",$sch_s_no_get);
	}
	if(!empty($sch_i_no_get)) {
		$add_where.=" AND wp.i_no='".$sch_i_no_get."'";
		$smarty->assign("sch_i_no",$sch_i_no_get);
	}

	$search_url = "sch_incentive_date=".$sch_incentive_date_get."&sch_s_no=".$sch_s_no_get."&sch_i=".$sch_i_no;
    $smarty->assign("search_url",$search_url);

	// 마케터 가져오기 배열
	$add_where_permission = str_replace("0", "_", $permission_name['마케터']);
		
    $staff_sql="select s_no,s_name from staff where staff_state < '3' AND permission like '".$add_where_permission."'";
    $staff_query=mysqli_query($my_db,$staff_sql);
	
    while($staff_data=mysqli_fetch_array($staff_query)) {
	    $staffs[]=array(
			    'no'=>$staff_data['s_no'],
			    'name'=>$staff_data['s_name']
	    );
	    $smarty->assign("staff",$staffs);
    }

	// 인센티브 일정에 대한 work_package 가져오기 배열
    $work_package_sql="SELECT wp.wp_no
							FROM work_package wp 
							WHERE wp.i_no='".$sch_i_no_get."'
						UNION
						SELECT wp.wp_no 
							FROM work_package wp 
							WHERE 1=1 AND wp.incentive_date='".$sch_incentive_date_get."'
									AND wp.s_no='".$sch_s_no_get."'
									AND (wp.i_no NOT IN ('".$sch_i_no_get."') OR wp.i_no IS NULL)
							ORDER BY wp_no ASC
						";
	
	//echo "$work_package_sql<br>"; //exit;
	$work_package_result= mysqli_query($my_db, $work_package_sql);
	// 마케터 단위로 인센티브 리스트를 가져옴
    while($work_package_array = mysqli_fetch_array($work_package_result)){
		
		$add_wp_where="";
		if(!empty($work_package_array['wp_no'])) {
			$add_wp_where.=" AND wp.wp_no='".$work_package_array['wp_no']."'";
		}

		// 리스트 쿼리
		$work_pakcage_sum_sql="
			SELECT	wp.wp_no,
					wp.incentive_date,
					(SELECT incentive_date FROM incentive WHERE i_no=wp.i_no) AS i_incentive_date,
					wp.company, 
					wp.subject, 
					wp.s_no,
					(SELECT s_name FROM staff s WHERE s.s_no=wp.s_no) AS s_name,
					SUM(w.selling_price) AS sum_selling_price,
					SUM(w.cost_price) AS sum_cost_price,
					SUM(w.incentive_sales_price) AS sum_incentive_sales_price
			FROM work_package wp 
				LEFT JOIN `work` w
					ON wp.wp_no=w.wp_no
			WHERE
				(wp.incentive_date='".$sch_incentive_date_get."' OR wp.i_no='".$sch_i_no_get."' )
				AND wp.s_no='".$sch_s_no_get."'
				$add_wp_where
				";
		
		//echo "$work_pakcage_sum_sql<br>"; //exit;
		$work_pakcage_sum_sql_result= mysqli_query($my_db, $work_pakcage_sum_sql);
		$work_pakcage_sum_data=mysqli_fetch_array($work_pakcage_sum_sql_result);
		
		$work_pakcage_sum[] = array(
			"wp_no"=>$work_pakcage_sum_data['wp_no'],
			"incentive_date"=>$work_pakcage_sum_data['incentive_date'],
			"i_incentive_date"=>$work_pakcage_sum_data['i_incentive_date'],
			"company"=>$work_pakcage_sum_data['company'],
			"subject"=>$work_pakcage_sum_data['subject'],
			"s_no"=>$work_pakcage_sum_data['s_no'],
			"s_name"=>$work_pakcage_sum_data['s_name'],
			"sum_selling_price"=>number_format($work_pakcage_sum_data['sum_selling_price']),
			"sum_cost_price"=>number_format($work_pakcage_sum_data['sum_cost_price']),
			"sum_incentive_sales_price"=>number_format($work_pakcage_sum_data['sum_incentive_sales_price'])
		);
	}
    
	$smarty->assign(array(
		"work_pakcage_sum"=>$work_pakcage_sum
	));
}

// 템플릿에 해당 입력값 던져주기
$smarty->assign(
	array(
		"proc"=>$proc,
	)
);
$smarty->display('incentive_detail.html');

?>