<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/corporation.php');
require('inc/model/MyQuick.php');
require('inc/model/MyCompany.php');
require('inc/model/Corporation.php');

// 접근 권한
if (!permissionNameCheck($session_permission, "재무관리자") && !permissionNameCheck($session_permission, "대표")){
    $smarty->display('access_error.html');
    exit;
}

$process    = (isset($_POST['process'])) ? $_POST['process'] : "";
$corp_model = Corporation::Factory();
if($process == 'modify_display')
{
    $co_no      = isset($_POST['co_no']) ? $_POST['co_no'] : "";
    $type       = isset($_POST['type']) ? $_POST['type'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(empty($co_no) || empty($type))
    {
        exit("<script>alert('다시 시도해 주세요');location.href='corp_account_management.php?{$search_url}';</script>");
    }

    $upd_data = array(
        "co_no"     => $co_no,
        "display"   => $type,
    );

    if(!$corp_model->update($upd_data)) {
        exit("<script>alert('변경에 실패했습니다');location.href='corp_account_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('변경했습니다');location.href='corp_account_management.php?{$search_url}';</script>");
    }
}
elseif($process == 'modify_memo')
{
    $co_no      = isset($_POST['co_no']) ? $_POST['co_no'] : "";
    $value      = isset($_POST['value']) ? addslashes(trim($_POST['value'])) : "";

    $upd_data = array(
        "co_no" => $co_no,
        "memo"  => $value,
    );

    if(!$corp_model->update($upd_data)) {
        echo "변경에 실패했습니다";
    }else{
        echo "변경에 성공했습니다";
    }
    exit;
}


# Navigation & My Quick
$nav_prd_no  = "151";
$nav_title   = "입금/출금 계좌 관리";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색
$add_where      = "1=1";
$sch_reg_name   = isset($_GET['sch_reg_name']) ? $_GET['sch_reg_name'] : "";
$sch_type       = isset($_GET['sch_type']) ? $_GET['sch_type'] : "";
$sch_my_c_no    = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_name       = isset($_GET['sch_name']) ? $_GET['sch_name'] : "";
$sch_display    = isset($_GET['sch_display']) ? $_GET['sch_display'] : "";
$sch_memo       = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";

if(!empty($sch_reg_name))
{
    $add_where .= " AND `ca`.reg_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_reg_name}%')";
    $smarty->assign('sch_reg_name', $sch_reg_name);
}

if(!empty($sch_type))
{
    $add_where .= " AND `ca`.`type` = '{$sch_type}'";
    $smarty->assign('sch_type', $sch_type);
}

if(!empty($sch_my_c_no))
{
    $add_where .= " AND `ca`.my_c_no = '{$sch_my_c_no}'";
    $smarty->assign('sch_my_c_no', $sch_my_c_no);
}

if(!empty($sch_name))
{
    $add_where .= " AND `ca`.name LIKE '%{$sch_name}%'";
    $smarty->assign('sch_name', $sch_name);
}

if(!empty($sch_display))
{
    $add_where .= " AND `ca`.display = '{$sch_display}'";
    $smarty->assign('sch_display', $sch_display);
}

if(!empty($sch_memo))
{
    $add_where .= " AND `ca`.memo LIKE '%{$sch_memo}%'";
    $smarty->assign('sch_memo', $sch_memo);
}

# 전체 게시물 수
$corp_account_total_sql		= "SELECT count(co_no) FROM (SELECT `ca`.co_no FROM corp_account `ca` WHERE {$add_where}) AS cnt";
$corp_account_total_query	= mysqli_query($my_db, $corp_account_total_sql);
if(!!$corp_account_total_query)
    $corp_account_total_result = mysqli_fetch_array($corp_account_total_query);

$corp_account_total = $corp_account_total_result[0];

# 페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($corp_account_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($pages, "corp_account_management.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $corp_account_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

$my_company_model = MyCompany::Factory();
$my_company_model->setList();
$my_company_name_list = $my_company_model->getNameList();
$account_type_option  = getAccountTypeOption();

# 리스트 쿼리
$corp_account_sql  = "
    SELECT
        *,
        (SELECT s.s_name FROM staff s WHERE s.s_no=ca.reg_s_no) as reg_s_name,
        DATE_FORMAT(ca.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(ca.regdate, '%H:%i:%s') as reg_hour
    FROM corp_account ca
    WHERE {$add_where}
    ORDER BY co_no DESC
    LIMIT {$offset}, {$num}
";

$corp_account_query  = mysqli_query($my_db, $corp_account_sql);
$corp_account_list   = [];
while($corp_account = mysqli_fetch_assoc($corp_account_query))
{
    $corp_account['my_c_name'] = $my_company_name_list[$corp_account['my_c_no']];
    $corp_account['type_name'] = $account_type_option[$corp_account['type']];
    $corp_account_list[] = $corp_account;
}



$smarty->assign('display_option', getDisplayOption());
$smarty->assign('account_type_option', $account_type_option);
$smarty->assign('my_company_option', $my_company_name_list);
$smarty->assign('corp_account_list', $corp_account_list);

$smarty->display('corp_account_management.html');

?>
