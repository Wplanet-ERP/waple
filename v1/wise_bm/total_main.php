<?php
require('../inc/common.php');
require('../../libs/dbconfig_mil.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_date.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');
require('../inc/helper/commerce_sales.php');
require('../inc/model/MyQuick.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# Navigation & My Quick
$nav_prd_no  = "195";
$nav_title   = "WISE BM";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

# 검색조건
$sch_e_date     = date('Y-m-d', strtotime("-1 days"));
$sch_e_month    = date('Y-m', strtotime("-1 months"));
$sch_cur_month  = date('Y-m');
$sch_s_date     = date('Y-m-d', strtotime("{$sch_e_date} -7 days"));
$sch_s_month    = date('Y-m', strtotime("{$sch_e_month} -6 months"));
$sch_s_month_val= date('Ym', strtotime($sch_s_month));
$sch_e_month_val= date('Ym', strtotime($sch_e_month));
$sch_s_year     = date('Y-m', strtotime("{$sch_e_month} -1 years"));
$bm_type_option = getBMBaseTypeOption();
$sch_base_type  = isset($_GET['sch_base_type']) ? $_GET['sch_base_type'] : "doc";
$sch_date_type  = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "date";
$bm_type_name   = $bm_type_option[$sch_base_type];
$nav_title     .= " :: ".$bm_type_name;
$sch_brand_url  = "";
$dp_except_list = getNotApplyDpList();
$dp_except_text = implode(",", $dp_except_list);

$smarty->assign("sch_s_month", $sch_s_month);
$smarty->assign("sch_e_month", $sch_e_month);
$smarty->assign("sch_s_year", $sch_s_year);

if($sch_base_type == "doc"){
    $sch_brand_url = "sch_brand_g1=70001";
}elseif($sch_base_type == "nuzam"){
    $sch_brand_url = "sch_brand_g1=70003";
}

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);
$smarty->assign("sch_brand_url", $sch_brand_url);
$smarty->assign("bm_type_name", $bm_type_name);
$smarty->assign("bm_type_option", $bm_type_option);
$smarty->assign("sch_base_type", $sch_base_type);
$smarty->assign("sch_date_type", $sch_date_type);
$smarty->assign("sch_date_type_option", getBMDateTypeOption());
$smarty->assign("traffic_date_type_option", array("day" => "일별", "week" => "주별"));

# 기본 변수
$s_month_datetime       = $sch_s_month."-01 00:00:00";
$e_month_end_day        = date("t", strtotime($sch_e_month));
$e_month_datetime       = $sch_e_month."-{$e_month_end_day} 23:59:59";
$cur_month_day          = date("t", strtotime($sch_cur_month));
$s_year_datetime        = $sch_s_year."-01 00:00:00";
$s_datetime             = $sch_s_date." 00:00:00";
$e_datetime             = $sch_e_date." 23:59:59";
$global_net_date        = $sch_s_month."-01";

$mil_s_month            = date("Y-m", strtotime("-6 months"));
$mil_s_date             = $mil_s_month."-01";
$mil_s_datetime         = $mil_s_date." 00:00:00";
$mil_e_datetime         = $sch_cur_month."-{$cur_month_day} 23:59:59";;

# 브랜드 변수
$main_brand_option      = getTotalMainBrandOption();
$total_brand_option     = getTotalBrandList();
$global_brand_option    = getGlobalBrandList();
$global_brand_keys      = array_keys($global_brand_option);
$nosp_brand_option      = array("1314","2863","3303","3386","4140","4445","4446","5283","5434","5812","2827","4878","5201","5642","5415","5810","4333","5447");
$doc_brand_option       = getTotalDocBrandList();
$doc_brand_name_option  = getDocBrandNameOption();
$doc_filter_option      = getDocFilterOption();
$doc_cubing_option      = getDocCubingOption();
$nuzam_brand_option     = getNuzamBrandList();
$ilenol_brand_option    = getIlenolBrandList();
$city_brand_option      = getCityBrandList();
$mil_brand_option       = getMilBrandList();
$sel_brand_option       = [];
$global_brand_text      = implode(",", $global_brand_keys);
$total_brand_text       = implode(",", $total_brand_option);
$nosp_brand_text        = implode(",", $nosp_brand_option);
$doc_brand_text         = implode(",", $doc_brand_option);
$nuzam_brand_text       = implode(",", $nuzam_brand_option);
$mil_brand_text         = implode(",", $mil_brand_option);

# 해외 매출 기본 변수
$global_base_option = array(
    "total"     => array("sales" => 0, "cost" => 0, "profit" => 0),
    "doc"       => array("sales" => 0, "cost" => 0, "profit" => 0),
    "ilenol"    => array("sales" => 0, "cost" => 0, "profit" => 0),
);
$global_mil_option = array(
    "total"     => array("sales" => 0, "cost" => 0, "profit" => 0),
    "doc"       => array("sales" => 0, "cost" => 0, "profit" => 0),
    "ilenol"    => array("sales" => 0, "cost" => 0, "profit" => 0),
);
$global_report_list     = [];
$global_report_tmp_list = [];

# 자사몰 매출 기본 변수
$self_base_init     = array("imweb" => 0, "imweb_ilenol" => 0, "imweb_nuzam" => 0, "smart" => 0, "total" => 0);
$self_base_option   = [];
$mil_base_option    = array("total" => array("cafe" => 0, "smart" => 0, "total" => 0));
$self_report_list   = [];

# 주문수 기본 변수
$cms_count_init     = array("comb" => 0, "total" => 0);
$cms_count_option   = [];
$mil_count_option   = array("total" => array("total" => 0));
$cms_count_list     = [];

# 반품/교환수 기본 변수
$return_count_init          = array("trade" => 0, "return" => 0);
$return_count_option        = [];
$mil_return_count_option    = array("total" => array("trade" => 0, "return" => 0));
$return_count_list          = [];

$nosp_result_init           = array("adv_price" => 0, "click_cnt" => 0, "adv_cpc" => 0 , "impressions" => 0, "click_per" => 0, "cnt" => 0);
$nosp_result_option         = [];
$nosp_time_report_list      = [];
$nosp_special_report_list   = [];

# 불량회수 기본 변수
$bad_return_count_option    = [];
$bad_return_count_list      = [];

# 위드플레이스 물류비 기본 변수
$with_delivery_option       = [];
$with_delivery_list         = [];
$with_delivery_exc_list     = [];

# 밀리옹 제품원가 변수
$mil_product_cost_option    = array("total" => array("sales" => 0, "cost" => 0, "cost_per" => 0));
$mil_product_cost_list      = [];

if($sch_base_type == "all"){
    $sel_brand_option = $main_brand_option;
    $with_delivery_option["media"] = 0;
}
elseif($sch_base_type == "doc"){
    $sel_brand_option = $doc_brand_option;
}
elseif($sch_base_type == "nuzam"){
    $sel_brand_option = $nuzam_brand_option;
}

if(!empty($sel_brand_option))
{
    $self_base_option["total"]          = $self_base_init;
    $cms_count_option["total"]          = $cms_count_init;
    $return_count_option["total"]       = $return_count_init;
    $nosp_result_option["total"]        = $nosp_result_init;
    $bad_return_count_option["total"]   = 0;
    $with_delivery_option["total"]      = 0;

    foreach($sel_brand_option as $sel_brand)
    {
        if($sel_brand == "5283"){
            continue;
        }

        $self_base_option[$sel_brand]           = $self_base_init;
        $cms_count_option[$sel_brand]           = $cms_count_init;
        $return_count_option[$sel_brand]        = $return_count_init;
        $nosp_result_option[$sel_brand]         = $nosp_result_init;
        $bad_return_count_option[$sel_brand]    = 0;
        $with_delivery_option[$sel_brand]       = 0;
    }
}

foreach($mil_brand_option as $mil_brand){
    $mil_base_option[$mil_brand]            = array("cafe" => 0, "smart" => 0, "total" => 0);
    $mil_count_option[$mil_brand]           = array("total" => 0);
    $mil_return_count_option[$mil_brand]    = array("trade" => 0, "return" => 0);
    $mil_product_cost_option[$mil_brand]    = array("sales" => 0, "cost" => 0, "cost_per" => 0);
}

# 1주일 기본 리스트
$custom_date_sql = "
	SELECT
 		DATE_FORMAT(`allday`.Date, '%m/%d') as chart_day,
 		DATE_FORMAT(`allday`.Date, '%w') as chart_day_w,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as chart_key,
	    DATE_FORMAT(`allday`.Date, '%Y-%m-%d') as check_key   
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'
	ORDER BY chart_key
";
$custom_date_query      = mysqli_query($my_db, $custom_date_sql);
$date_short_option      = getDayShortOption();
$custom_date_list       = [];
while($custom_date = mysqli_fetch_assoc($custom_date_query))
{
    $custom_date_text = $custom_date['chart_day']."({$date_short_option[$custom_date['chart_day_w']]})";
    $custom_date_list[$custom_date['chart_key']]    = $custom_date_text;

    if($sch_base_type != "mil"){
        $self_report_list[$custom_date['chart_key']]    = $self_base_option;
        $cms_count_list[$custom_date['chart_key']]      = $cms_count_option;
    }
}

# 6개월 기본 리스트
$custom_month_sql = "
	SELECT
 		DATE_FORMAT(`allday`.Date, '%Y.%m') as chart_title,
 		DATE_FORMAT(`allday`.Date, '%Y%m') as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as sales_date
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_year}' AND '{$sch_cur_month}'
	ORDER BY sales_date
";
$custom_month_query     = mysqli_query($my_db, $custom_month_sql);
$custom_month_list      = [];
$custom_year_list       = [];
$mil_custom_month_list  = [];
while($custom_month = mysqli_fetch_assoc($custom_month_query))
{
    if($custom_month['chart_key'] >= $sch_s_month_val && $custom_month['chart_key'] <= $sch_e_month_val)
    {
        $custom_month_list[$custom_month['chart_key']]      = $custom_month['chart_title'];

        if($sch_base_type != "mil"){
            $return_count_list[$custom_month['chart_key']]      = $return_count_option;
            $with_delivery_list[$custom_month['chart_key']]     = $with_delivery_option;
            $with_delivery_exc_list[$custom_month['chart_key']] = $with_delivery_option;
        }

        if($sch_base_type == "all"){
            $global_report_list[$custom_month['chart_key']]     = $global_base_option;
            $global_report_tmp_list["doc"][$custom_month['chart_key']][$custom_month['sales_date']]     = array('sales' => 0, 'cost' => 0);
            $global_report_tmp_list["ilenol"][$custom_month['chart_key']][$custom_month['sales_date']]  = array('sales' => 0, 'cost' => 0);
        }
    }

    if($sch_base_type == "mil" && $custom_month['chart_key'] > $sch_s_month_val)
    {
        $mil_custom_month_list[$custom_month['chart_key']]  = $custom_month['chart_title'];
        $self_report_list[$custom_month['chart_key']]       = $mil_base_option;
        $cms_count_list[$custom_month['chart_key']]         = $mil_count_option;
        $return_count_list[$custom_month['chart_key']]      = $mil_return_count_option;
        $mil_product_cost_list[$custom_month['chart_key']]  = $mil_product_cost_option;
    }

    if($custom_month['chart_key'] <= $sch_e_month_val)
    {
        $custom_year_list[$custom_month['chart_key']]  = $custom_month['chart_title'];

        if($sch_base_type != "mil"){
            $bad_return_count_list[$custom_month['chart_key']] = $bad_return_count_option;
        }
    }
}

# 해외 매출 리스트
$global_cms_where    = "w.delivery_state='4' AND (w.order_date BETWEEN '{$s_month_datetime}' AND '{$e_month_datetime}') AND w.log_c_no IN(5659,5956)";
$global_report_where = "1=1 AND `cr`.display='1' AND (DATE_FORMAT(`cr`.sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}') AND `cr`.brand IN({$global_brand_text})";

if($sch_base_type == "all")
{
    # NET 매출 관련 작업
    $net_parent_percent_list = [];
    $net_report_date_list    = [];
    foreach($global_brand_option as $c_no => $c_type)
    {
        $net_pre_percent_sql    = "SELECT `percent` FROM commerce_report_net WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$global_net_date}' ORDER BY sales_date DESC LIMIT 1;";
        $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
        $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
        $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;

        $net_parent_percent_list[$c_type] = $net_pre_percent;

        $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$c_no}' AND (sales_date BETWEEN '{$s_month_datetime}' AND '{$e_month_datetime}') ORDER BY sales_date ASC";
        $net_report_query       = mysqli_query($my_db, $net_report_sql);
        while($net_report = mysqli_fetch_assoc($net_report_query))
        {
            $net_report_date_list[$c_type][$net_report['sales_date']] = $net_report['percent'];
        }
    }

    # 해외 매출 자동
    $global_cms_sql = "
        SELECT
            w.order_number,
            w.c_no as brand,
            w.unit_price as cms_price,
            w.unit_delivery_price,               
            DATE_FORMAT(w.order_date, '%Y%m') as key_month,
            DATE_FORMAT(w.order_date, '%Y%m%d') as sales_date,
            (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
        FROM work_cms w 
        WHERE {$global_cms_where}
        ORDER BY key_month, brand
    ";
    $global_cms_query = mysqli_query($my_db, $global_cms_sql);
    $cms_delivery_chk_list      = [];
    $cms_delivery_chk_ord_list  = [];
    $cms_delivery_chk_fee_list  = [];
    while($global_cms = mysqli_fetch_assoc($global_cms_query))
    {
        $global_cms_brand = "";
        if(in_array($global_cms['brand'], $ilenol_brand_option)){
            $global_cms_brand = "ilenol";
        }

        if(!empty($global_cms_brand))
        {
            if($global_cms['sales_date'] >= 20240101)
            {
                if ($global_cms['unit_delivery_price'] > 0) {
                    $cms_delivery_chk_fee_list[$global_cms['order_number']] = 1;
                }
                elseif($global_cms['unit_delivery_price'] == 0 && $global_cms['cms_price'] > 0)
                {
                    $cms_delivery_chk_list[$global_cms['order_number']][$global_cms['key_month']][$global_cms['sales_date']] = $global_cms['deli_cnt'];
                    $cms_delivery_chk_ord_list[$global_cms['order_number']][$global_cms_brand] = $global_cms_brand;
                }
            }

            $global_report_tmp_list[$global_cms_brand][$global_cms['key_month']][$global_cms['sales_date']]["sales"] += $global_cms["cms_price"];
        }
    }

    # 해외 지출/매출 수기입력
    $global_report_sql  = "
        SELECT 
            `cr`.`type`,
            `cr`.brand,
            `cr`.price as report_price,
            DATE_FORMAT(sales_date, '%Y%m') as key_month,
            DATE_FORMAT(sales_date, '%Y%m%d') as sales_date
        FROM commerce_report `cr`
        WHERE {$global_report_where}
        ORDER BY sales_date
    ";
    $global_report_query = mysqli_query($my_db, $global_report_sql);
    while($global_report = mysqli_fetch_assoc($global_report_query))
    {
        $global_brand = "";
        if(in_array($global_report["brand"], $global_brand_keys)){
            $global_brand = $global_brand_option[$global_report["brand"]];
        }

        if(!empty($global_brand)){
            $global_report_tmp_list[$global_brand][$global_report['key_month']][$global_report['sales_date']][$global_report['type']] += $global_report["report_price"];
        }
    }

    # 무료 배송비 계산
    foreach($cms_delivery_chk_list as $chk_ord => $chk_ord_data)
    {
        if(isset($cms_delivery_chk_fee_list[$chk_ord])){
            continue;
        }

        $chk_delivery_price = 6000;

        foreach ($chk_ord_data as $chk_key_date => $chk_key_data)
        {
            foreach ($chk_key_data as $chk_sales_date => $deli_cnt)
            {
                $cal_delivery_price     = $chk_delivery_price * $deli_cnt;

                $chk_brand_deli_price   = $cal_delivery_price;
                $chk_brand_cnt          = count($cms_delivery_chk_ord_list[$chk_ord]);
                $cal_brand_deli_price   = round($cal_delivery_price/$chk_brand_cnt);
                $cal_brand_idx          = 1;
                foreach($cms_delivery_chk_ord_list[$chk_ord] as $chk_brand)
                {
                    $cal_one_delivery_price  = $cal_brand_deli_price;
                    $chk_brand_deli_price   -= $cal_one_delivery_price;

                    if($cal_brand_idx == $chk_brand_cnt){
                        $cal_one_delivery_price += $chk_brand_deli_price;
                    }

                    $global_report_tmp_list[$chk_brand][$chk_key_date][$chk_sales_date]['cost'] += $cal_one_delivery_price;
                    $cal_brand_idx++;
                }
            }
        }
    }

    if(!empty($global_report_tmp_list))
    {
        foreach($global_report_tmp_list as $global_brand => $global_brand_data)
        {
            $net_cal_percent = $net_parent_percent_list[$global_brand];
            foreach($global_brand_data as $key_month => $global_brand_month_data)
            {
                foreach($global_brand_month_data as $sales_date => $global_brand_date_data)
                {
                    if(isset($net_report_date_list[$global_brand][$sales_date]) && $net_report_date_list[$global_brand][$sales_date] > 0){
                        $net_cal_percent = $net_report_date_list[$global_brand][$sales_date];
                    }

                    $net_percent    = $net_cal_percent/100;
                    $sales_price    = $global_brand_date_data['sales'];
                    $cost_price     = $global_brand_date_data['cost'];
                    $net_price      = $sales_price > 0 ? (int)($sales_price*$net_percent) : 0;
                    $profit_price   = $cost_price > 0 ? ($net_price-$cost_price) : $net_price;

                    $global_report_list[$key_month][$global_brand]["sales"]     += $sales_price;
                    $global_report_list[$key_month][$global_brand]["cost"]      += $cost_price;
                    $global_report_list[$key_month][$global_brand]["profit"]    += $profit_price;

                    $global_report_list[$key_month]["total"]["sales"]     += $sales_price;
                    $global_report_list[$key_month]["total"]["cost"]      += $cost_price;
                    $global_report_list[$key_month]["total"]["profit"]    += $profit_price;
                }
            }
        }
    }
}

# 요일/시간별 판매실적 현황/비교
$date_name_option           = getDateChartOption();
$daily_hour_option          = getHourOption();
$daily_s_date               = date("Y-m-d", strtotime("{$sch_e_date} -6 days"));
$daily_e_date               = $sch_e_date;
$daily_s_datetime           = "{$daily_s_date} 00:00:00";
$daily_e_datetime           = "{$daily_e_date} 23:59:59";
$daily_s_date_tmp           = date("m/d_w", strtotime($daily_s_date));
$daily_e_date_tmp           = date("m/d_w", strtotime($daily_e_date));

$daily_s_date_convert       = explode('_', $daily_s_date_tmp);
$daily_s_date_name          = $date_name_option[$daily_s_date_convert[1]];
$daily_s_date_text          = $daily_s_date_convert[0]."({$daily_s_date_name})";
$daily_e_date_convert       = explode('_', $daily_e_date_tmp);
$daily_e_date_name          = $date_name_option[$daily_e_date_convert[1]];
$daily_e_date_text          = $daily_e_date_convert[0]."({$daily_e_date_name})";

$prev_daily_s_date          = date("Y-m-d", strtotime("{$daily_s_date} -1 weeks"));
$prev_daily_e_date          = date("Y-m-d", strtotime("{$daily_e_date} -1 weeks"));
$prev_daily_s_datetime      = "{$prev_daily_s_date} 00:00:00";
$prev_daily_e_datetime      = "{$prev_daily_e_date} 23:59:59";
$prev_daily_s_date_tmp      = date("m/d_w", strtotime($prev_daily_s_date));
$prev_daily_e_date_tmp      = date("m/d_w", strtotime($prev_daily_e_date));

$prev_daily_s_date_convert  = explode('_', $prev_daily_s_date_tmp);
$prev_daily_s_date_name     = $date_name_option[$prev_daily_s_date_convert[1]];
$prev_daily_s_date_text     = $prev_daily_s_date_convert[0]."({$prev_daily_s_date_name})";
$prev_daily_e_date_convert  = explode('_', $prev_daily_e_date_tmp);
$prev_daily_e_date_name     = $date_name_option[$prev_daily_e_date_convert[1]];
$prev_daily_e_date_text     = $prev_daily_e_date_convert[0]."({$prev_daily_e_date_name})";

$daily_date_list            = [];
$daily_hour_list            = [];
$daily_sales_list           = [];
$daily_date_total_list      = [];
$daily_hour_total_list      = array("max" => 0);
$daily_sales_total_price    = 0;

$sales_term_hour_list       = [];
$sales_term_date_list       = [];
$sales_term_total_list      = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0, "hour_base_max" => 0, "hour_comp_max" => 0, "date_base_max" => 0, "date_comp_max" => 0, "date_base_min" => 0, "date_comp_min" => 0);
$sales_term_hour_total_list = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0);

foreach($daily_hour_option as $hour){
    $hour_key = (int)$hour;
    $daily_hour_list[$hour_key]     = "{$hour}시";
    $sales_term_hour_list[$hour_key]= array("base_price" => 0,"comp_price" => 0, "comp_per" => 0);
}

foreach($date_name_option as $date_key => $date_title)
{
    $daily_date_list[$date_key]         = $date_title;
    $daily_sales_list[$date_key]['max'] = 0;
    $daily_date_total_list[$date_key]   = 0;
    $sales_term_date_list[$date_key]    = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0);

    foreach($daily_hour_list as $hour_key){
        $daily_sales_list[$date_key][$hour_key] = 0;
        $daily_hour_total_list[$hour_key]       = 0;
    }
}

$add_sales_daily_where  = "w.page_idx > 0 AND w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN(1372,5800) AND (order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}')";
$add_sales_term_where   = "w.page_idx > 0 AND w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN(1372,5800) AND (order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}' OR order_date BETWEEN '{$prev_daily_s_datetime}' AND '{$prev_daily_e_datetime}')";
if($sch_base_type == "all"){
    $add_sales_daily_where  .= " AND w.c_no IN({$total_brand_text})";
    $add_sales_term_where   .= " AND w.c_no IN({$total_brand_text})";
}elseif($sch_base_type == "doc"){
    $add_sales_daily_where  .= " AND w.c_no IN({$doc_brand_text})";
    $add_sales_term_where   .= " AND w.c_no IN({$doc_brand_text})";
}elseif($sch_base_type == "nuzam"){
    $add_sales_daily_where  .= " AND w.c_no IN({$nuzam_brand_text})";
    $add_sales_term_where   .= " AND w.c_no IN({$nuzam_brand_text})";
}

if($sch_base_type != "mil")
{
    # 요일/시간별 판매실적 현황 쿼리
    $daily_sales_sql  = "
        SELECT
            DATE_FORMAT(w.order_date, '%w') as date_key,
            DATE_FORMAT(w.order_date, '%H') as hour_key,
            SUM(unit_price) as total_price
        FROM work_cms w 
        WHERE {$add_sales_daily_where}
        GROUP BY date_key, hour_key
    ";
    $daily_sales_query    = mysqli_query($my_db, $daily_sales_sql);
    while($daily_sales_result = mysqli_fetch_assoc($daily_sales_query))
    {
        $daily_date_key     = $daily_sales_result['date_key'];
        $daily_hour_key     = (int)$daily_sales_result['hour_key'];
        $daily_total_price  = (int)$daily_sales_result['total_price'];
        $daily_max          = $daily_sales_list[$daily_date_key]["max"];

        $daily_date_total_list[$daily_date_key] += $daily_total_price;
        $daily_hour_total_list[$daily_hour_key] += $daily_total_price;
        $daily_sales_total_price                += $daily_total_price;

        $daily_sales_list[$daily_date_key][$daily_hour_key] += $daily_total_price;
        if($daily_max < $daily_total_price){
            $daily_sales_list[$daily_date_key]["max"] = $daily_total_price;
        }
    }

    $daily_date_total_list["min"] = min($daily_date_total_list);
    $daily_date_total_list["max"] = max($daily_date_total_list);
    $daily_hour_total_list["max"] = max($daily_hour_total_list);

    # 요일/시간별 판매실적 비교 쿼리
    $term_sales_sql    = "

        SELECT    
            DATE_FORMAT(w.order_date, '%w') as date_key,
            DATE_FORMAT(w.order_date, '%H') as hour_key,
            SUM(
                IF(order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}',unit_price, 0)
            ) AS base_price,
            SUM(
                IF(order_date BETWEEN '{$prev_daily_s_datetime}' AND '{$prev_daily_e_datetime}',unit_price, 0)
            ) AS comp_price
        FROM work_cms w 
        WHERE {$add_sales_term_where}
        GROUP BY date_key, hour_key
        ORDER BY order_date
    ";
    $term_sales_query = mysqli_query($my_db, $term_sales_sql);
    while($term_sales = mysqli_fetch_assoc($term_sales_query))
    {
        $hour_key   = (int)$term_sales['hour_key'];
        $date_key   = (int)$term_sales['date_key'];
        $base_price = $term_sales['base_price'];
        $comp_price = $term_sales['comp_price'];

        $sales_term_hour_list[$hour_key]["base_price"] += $base_price;
        $sales_term_hour_list[$hour_key]["comp_price"] += $comp_price;
        $sales_term_date_list[$date_key]["base_price"] += $base_price;
        $sales_term_date_list[$date_key]["comp_price"] += $comp_price;

        $sales_term_total_list["base_price"] += $base_price;
        $sales_term_total_list["comp_price"] += $comp_price;
    }

    if(!empty($sales_term_total_list)){
        $total_comp_per = ($sales_term_total_list["comp_price"] == 0) ? 0 : (($sales_term_total_list["base_price"] == 0) ? -100 : ROUND( ((($sales_term_total_list["base_price"]-$sales_term_total_list["comp_price"])/$sales_term_total_list["comp_price"]) *100), 1));
        $sales_term_total_list["comp_per"] = $total_comp_per;
    }

    if(!empty($sales_term_hour_list))
    {
        foreach($sales_term_hour_list as $hour_key => $hour_data)
        {
            $hour_base_price    = $hour_data["base_price"];
            $hour_comp_price    = $hour_data["comp_price"];
            $hour_base_max      = $sales_term_total_list["hour_base_max"];
            $hour_comp_max      = $sales_term_total_list["hour_comp_max"];

            $hour_comp_per = ($hour_comp_price == 0) ? 0 : (($hour_base_price == 0) ? -100 : ROUND( ((($hour_base_price-$hour_comp_price)/$hour_comp_price) *100), 1));
            $sales_term_hour_list[$hour_key]["comp_per"] = $hour_comp_per;

            if($hour_base_max < $hour_base_price){
                $sales_term_total_list["hour_base_max"] = $hour_base_price;
            }

            if($hour_comp_max < $hour_comp_price){
                $sales_term_total_list["hour_comp_max"] = $hour_comp_price;
            }
        }
    }

    if(!empty($sales_term_date_list))
    {
        $date_idx = 0;
        foreach($sales_term_date_list as $date_key => $date_data)
        {
            $date_base_price    = $date_data["base_price"];
            $date_comp_price    = $date_data["comp_price"];
            $date_base_max      = $sales_term_total_list["date_base_max"];
            $date_base_min      = $sales_term_total_list["date_base_min"];
            $date_comp_max      = $sales_term_total_list["date_comp_max"];
            $date_comp_min      = $sales_term_total_list["date_comp_min"];

            if($date_idx == 0){
                $sales_term_total_list["date_base_min"] = $date_base_price;
                $sales_term_total_list["date_comp_min"] = $date_comp_price;
            }

            $date_comp_per = ($date_comp_price == 0) ? 0 : (($date_base_price == 0) ? -100 : ROUND( ((($date_base_price-$date_comp_price)/$date_comp_price) *100), 1));
            $sales_term_date_list[$date_key]["comp_per"] = $date_comp_per;

            if($date_base_max < $date_base_price){
                $sales_term_total_list["date_base_max"] = $date_base_price;
            }

            if($date_base_min > $date_base_price){
                $sales_term_total_list["date_base_min"] = $date_base_price;
            }

            if($date_comp_max < $date_comp_price){
                $sales_term_total_list["date_comp_max"] = $date_comp_price;
            }

            if($date_comp_min > $date_comp_price){
                $sales_term_total_list["date_comp_min"] = $date_comp_price;
            }

            $date_idx++;
        }
    }
}

$smarty->assign("daily_s_date", $daily_s_date);
$smarty->assign("daily_e_date", $daily_e_date);
$smarty->assign("daily_s_date_text", $daily_s_date_text);
$smarty->assign("daily_e_date_text", $daily_e_date_text);
$smarty->assign("prev_daily_s_date", $prev_daily_s_date);
$smarty->assign("prev_daily_e_date", $prev_daily_e_date);
$smarty->assign("prev_daily_s_date_text", $prev_daily_s_date_text);
$smarty->assign("prev_daily_e_date_text", $prev_daily_e_date_text);
$smarty->assign("daily_date_list", $daily_date_list);
$smarty->assign("daily_hour_list", $daily_hour_list);
$smarty->assign("daily_sales_total_price", $daily_sales_total_price);
$smarty->assign("daily_date_total_list", $daily_date_total_list);
$smarty->assign("daily_hour_total_list", $daily_hour_total_list);
$smarty->assign("daily_sales_list", $daily_sales_list);
$smarty->assign("sales_term_hour_list", $sales_term_hour_list);
$smarty->assign("sales_term_date_list", $sales_term_date_list);
$smarty->assign("sales_term_total_list", $sales_term_total_list);

# 자사몰 매출 리스트
$self_cms_where         = "w.delivery_state='4' AND (w.order_date BETWEEN '{$s_datetime}' AND '{$e_datetime}') AND unit_price > 0";
$self_dp_company_list   = getSelfDpCompanyList();
$self_dp_company_text   = implode(",", $self_dp_company_list);

if($sch_base_type == "all"){
    $self_cms_where .= " AND w.dp_c_no IN({$self_dp_company_text}) AND w.c_no IN({$total_brand_text})";
}elseif($sch_base_type == "doc"){
    $self_cms_where .= " AND w.dp_c_no IN(1372,5427,5800) AND w.c_no IN({$doc_brand_text})";
}elseif($sch_base_type == "nuzam"){
    $self_cms_where .= " AND w.dp_c_no IN(1372,4629,6012) AND w.c_no IN({$nuzam_brand_text})";
}elseif($sch_base_type == "mil"){
    $self_cms_where  = "w.delivery_state='4' AND (w.order_date BETWEEN '{$mil_s_datetime}' AND '{$mil_e_datetime}') AND unit_price > 0 AND w.dp_c_no IN(2,3) AND w.c_no IN({$mil_brand_text})";
}

if($sch_base_type != "mil")
{
    $self_cms_sql = "     
        SELECT 
            w.c_no as brand,
            w.dp_c_no,
            COUNT(DISTINCT order_number) as ord_cnt,
            SUM(w.unit_price) as cms_price,
            DATE_FORMAT(w.order_date, '%Y%m%d') as key_date
        FROM work_cms w 
        WHERE {$self_cms_where}
        GROUP BY key_date, brand, dp_c_no
        ORDER BY key_date, brand
    ";
    $self_cms_query = mysqli_query($my_db, $self_cms_sql);
    while($self_cms = mysqli_fetch_assoc($self_cms_query))
    {
        $self_cms_brand     = "";
        $self_cms_company   = "";
        $total_cms_company  = "";

        switch($self_cms['dp_c_no']){
            case "1372":
            case "5800":
                $self_cms_company   = "imweb";
                $total_cms_company  = $self_cms_company;
                break;
            case "5958":
                $self_cms_company   = "imweb_ilenol";
                $total_cms_company  = "imweb";
                break;
            case "6012":
                $self_cms_company   = "imweb_nuzam";
                $total_cms_company  = "imweb";
                break;
            case "3295":
            case "4629":
            case "5427":
            case "5588":
                $self_cms_company   = "smart";
                $total_cms_company  = $self_cms_company;
                break;
        }

        if($sch_base_type == "all")
        {
            if(in_array($self_cms['brand'], $doc_brand_option))
            {
                $self_cms_brand = "doc";
            }
            elseif(in_array($self_cms['brand'], $nuzam_brand_option))
            {
                $self_cms_brand = "nuzam";
            }
            elseif(in_array($self_cms['brand'], $ilenol_brand_option))
            {
                $self_cms_brand = "ilenol";
            }
            elseif(in_array($self_cms['brand'], $city_brand_option))
            {
                $self_cms_brand = "city";
            }
        }
        elseif($sch_base_type == "doc")
        {
            if(in_array($self_cms['brand'], $doc_filter_option)){
                $self_cms_brand = "1314";
            }elseif(in_array($self_cms['brand'], $doc_cubing_option)){
                $self_cms_brand = "3386";
            }else{
                $self_cms_brand = $self_cms['brand'];
            }
        }
        elseif($sch_base_type == "nuzam")
        {
            $self_cms_brand = $self_cms['brand'];
        }

        if(!empty($self_cms_brand) && !empty($self_cms_company) && isset($self_report_list[$self_cms['key_date']][$self_cms_brand]))
        {
            $self_report_list[$self_cms['key_date']][$self_cms_brand][$self_cms_company] += $self_cms['cms_price'];
            $self_report_list[$self_cms['key_date']][$self_cms_brand]["total"] += $self_cms['cms_price'];
            $self_report_list[$self_cms['key_date']]["total"][$total_cms_company] += $self_cms['cms_price'];
            $self_report_list[$self_cms['key_date']]["total"]["total"] += $self_cms['cms_price'];
        }
    }
}
else
{
    $self_cms_sql = "     
        SELECT 
            w.c_no as brand,
            w.dp_c_no,
            COUNT(DISTINCT w.order_number) as ord_cnt,
            SUM(w.unit_price*w.org_price) as cms_price,
            DATE_FORMAT(w.order_date, '%Y%m') as key_month
        FROM work_cms w 
        WHERE {$self_cms_where}
        GROUP BY key_month, brand, dp_c_no
        ORDER BY key_month, brand
    ";
    $self_cms_query = mysqli_query($mil_db, $self_cms_sql);
    while($self_cms = mysqli_fetch_assoc($self_cms_query))
    {
        $self_cms_brand     = "";
        $self_cms_company   = "";
        $total_cms_company  = "";

        switch($self_cms['dp_c_no']){
            case "2":
                $self_cms_company   = "cafe";
                $total_cms_company  = $self_cms_company;
                break;

            case "3":
                $self_cms_company   = "smart";
                $total_cms_company  = $self_cms_company;
                break;
        }

        if(in_array($self_cms['brand'], $mil_brand_option))
        {
            $self_cms_brand = $self_cms['brand'];
        }

        if(!empty($self_cms_brand) && !empty($self_cms_company) && isset($self_report_list[$self_cms['key_month']][$self_cms_brand]))
        {
            $self_report_list[$self_cms['key_month']][$self_cms_brand][$self_cms_company] += $self_cms['cms_price'];
            $self_report_list[$self_cms['key_month']][$self_cms_brand]["total"] += $self_cms['cms_price'];
            $self_report_list[$self_cms['key_month']]["total"][$total_cms_company] += $self_cms['cms_price'];
            $self_report_list[$self_cms['key_month']]["total"]["total"] += $self_cms['cms_price'];
        }
    }
}

# 커머스 주문수 리스트
$cms_count_where  = "delivery_state='4' AND (w.order_date BETWEEN '{$s_datetime}' AND '{$e_datetime}') AND dp_c_no NOT IN({$dp_except_text}) AND unit_price > 0";

if($sch_base_type == "all"){
    $cms_count_where .= " AND w.c_no IN({$total_brand_text})";
}
elseif($sch_base_type == "doc")
{
    $cms_count_where .= " AND w.c_no IN({$doc_brand_text})";
    $cms_count_chk_sql = "
        SELECT 
            order_number
        FROM work_cms as w
        WHERE {$cms_count_where}
        GROUP BY order_number
    ";
    $cms_count_chk_query = mysqli_query($my_db, $cms_count_chk_sql);
    $cms_count_chk_list  = [];
    while($cms_count_chk = mysqli_fetch_assoc($cms_count_chk_query)){
        $cms_count_chk_list[] = "'{$cms_count_chk['order_number']}'";
    }

    if(!empty($cms_count_chk_list)){
        $cms_count_chk_text = implode(",", $cms_count_chk_list);
        $cms_count_where = "w.order_number IN({$cms_count_chk_text}) AND w.delivery_state='4' AND (w.order_date BETWEEN '{$s_datetime}' AND '{$e_datetime}') AND dp_c_no NOT IN(2005,2007,2008,5127,5128,5134,5135,5475,5476,5477,5478,5479,5482,5484,5659,5469,5665,5777,5939,6002) AND unit_price > 0";
    }
}
elseif($sch_base_type == "nuzam")
{
    $cms_count_where .= " AND w.c_no IN({$nuzam_brand_text})";
    $cms_count_chk_sql = "
        SELECT 
            order_number
        FROM work_cms as w
        WHERE {$cms_count_where}
        GROUP BY order_number
    ";
    $cms_count_chk_query = mysqli_query($my_db, $cms_count_chk_sql);
    $cms_count_chk_list  = [];
    while($cms_count_chk = mysqli_fetch_assoc($cms_count_chk_query)){
        $cms_count_chk_list[] = "'{$cms_count_chk['order_number']}'";
    }

    if(!empty($cms_count_chk_list)){
        $cms_count_chk_text = implode(",", $cms_count_chk_list);
        $cms_count_where = "w.order_number IN({$cms_count_chk_text}) AND w.delivery_state='4' AND (w.order_date BETWEEN '{$s_datetime}' AND '{$e_datetime}') AND dp_c_no NOT IN(2005,2007,2008,5127,5128,5134,5135,5475,5476,5477,5478,5479,5482,5484,5659,5469,5665,5777,5939) AND unit_price > 0";
    }
}elseif($sch_base_type == "mil"){
    $cms_count_where = "delivery_state='4' AND (w.order_date BETWEEN '{$mil_s_datetime}' AND '{$mil_e_datetime}') AND unit_price > 0 AND w.c_no IN({$mil_brand_text})";
}

if($sch_base_type != "mil")
{
    $cms_count_sql = "
        SELECT 
            c_no as brand,
            order_number,
            DATE_FORMAT(order_date, '%Y%m%d') as sales_date
        FROM work_cms as w
        WHERE {$cms_count_where}
    ";
    $cms_count_query = mysqli_query($my_db, $cms_count_sql);
    $tmp_cms_count_list = [];
    while($cms_count = mysqli_fetch_assoc($cms_count_query))
    {
        $cms_count_brand = "";
        if($sch_base_type == "all")
        {
            if(in_array($cms_count['brand'], $doc_brand_option)){
                $cms_count_brand = "doc";
            }
            elseif(in_array($cms_count['brand'], $nuzam_brand_option)) {
                $cms_count_brand = "nuzam";
            }
            elseif(in_array($cms_count['brand'], $ilenol_brand_option)) {
                $cms_count_brand = "ilenol";
            }
            elseif(in_array($cms_count['brand'], $city_brand_option)) {
                $cms_count_brand = "city";
            }
            else {
                $cms_count_brand = "etc";
            }
        }
        elseif($sch_base_type == "doc")
        {
            if(in_array($cms_count['brand'], $doc_filter_option)){
                $cms_count_brand = "1314";
            }elseif(in_array($cms_count['brand'], $doc_cubing_option)){
                $cms_count_brand = "3386";
            }else{
                $cms_count_brand = $cms_count['brand'];
            }
        }
        elseif($sch_base_type == "nuzam")
        {
            $cms_count_brand = $cms_count['brand'];
        }

        $tmp_cms_count_list[$cms_count['sales_date']][$cms_count['order_number']][$cms_count['brand']] = $cms_count_brand;
    }

    $cms_count_chk_list = [];
    if(!empty($tmp_cms_count_list))
    {
        foreach($tmp_cms_count_list as $key_date => $ord_total_data)
        {
            foreach($ord_total_data as $ord_no => $ord_data)
            {
                $chk_count = count($ord_data);
                $cms_count_list[$key_date]["total"]["total"]++;

                if($chk_count > 1){
                    $cms_count_list[$key_date]["total"]["comb"]++;
                }

                foreach($ord_data as $ord_key => $ord_brand)
                {
                    if(!isset($cms_count_chk_list[$ord_brand][$ord_no]))
                    {
                        $cms_count_chk_list[$ord_brand][$ord_no] = 1;

                        $cms_count_list[$key_date][$ord_brand]['total']++;

                        if($chk_count > 1){
                            $cms_count_list[$key_date][$ord_brand]["comb"]++;
                        }
                    }
                }
            }
        }
    }
}
else
{
    $cms_count_sql = "
        SELECT 
            c_no as brand,
            order_number,
            DATE_FORMAT(order_date, '%Y%m') as key_month
        FROM work_cms as w
        WHERE {$cms_count_where} 
    ";
    $cms_count_query    = mysqli_query($mil_db, $cms_count_sql);
    $tmp_cms_count_list = [];
    while($cms_count = mysqli_fetch_assoc($cms_count_query))
    {
        $cms_count_brand = "";
        if(in_array($cms_count['brand'], $mil_brand_option)){
            $cms_count_brand = $cms_count['brand'];
        }

        if(!empty($cms_count_brand)){
            $tmp_cms_count_list[$cms_count['key_month']][$cms_count['order_number']][$cms_count['brand']] = $cms_count_brand;
        }
    }

    $cms_count_chk_list = [];
    if(!empty($tmp_cms_count_list))
    {
        foreach($tmp_cms_count_list as $key_month => $ord_total_data)
        {
            foreach($ord_total_data as $ord_no => $ord_data)
            {
                $chk_count = count($ord_data);
                $cms_count_list[$key_month]["total"]["total"]++;

                foreach($ord_data as $ord_key => $ord_brand)
                {
                    if(!isset($cms_count_chk_list[$ord_brand][$ord_no]))
                    {
                        $cms_count_chk_list[$ord_brand][$ord_no] = 1;
                        $cms_count_list[$key_month][$ord_brand]['total']++;
                    }
                }
            }
        }
    }
}

# 반품/교환수 리스트
$return_count_where = "`cr`.return_type IN(1,2) AND (`cr`.return_date BETWEEN '{$s_month_datetime}' AND '{$e_month_datetime}')";
$last_return_list   = [];

if($sch_base_type == "all"){
    $return_count_where .= " AND `cr`.c_no IN({$total_brand_text})";
}elseif($sch_base_type == "doc"){
    $return_count_where .= " AND `cr`.c_no IN({$doc_brand_text})";
}elseif($sch_base_type == "nuzam"){
    $return_count_where .= " AND `cr`.c_no IN({$nuzam_brand_text})";
}

if($sch_base_type != "mil")
{
    # 최근 반품/교환 업로드 내역
    $last_return_sql = "
        SELECT
            upload_kind,
            DATE_FORMAT(last_regdate, '%m/%d') AS upload_date
        FROM 
        (
            SELECT
               upload_kind,
               MAX(regdate) as last_regdate
            FROM upload_management 
            WHERE upload_type='10'
            GROUP BY upload_kind
            ORDER BY upload_date DESC
        ) AS rs
    ";
    $last_return_query = mysqli_query($my_db, $last_return_sql);
    while($last_return = mysqli_fetch_assoc($last_return_query)){
        $last_return_list[$last_return['upload_kind']] = $last_return['upload_date'];
    }

    $return_count_sql = "
         SELECT
            DATE_FORMAT(`cr`.return_date, '%Y%m') as key_month,
            order_number,
            c_no as brand,
            return_type
        FROM csm_return cr
        WHERE {$return_count_where}
        ORDER BY key_month
    ";
    $return_count_query = mysqli_query($my_db, $return_count_sql);
    $tmp_return_count_list = [];
    while($return_count = mysqli_fetch_assoc($return_count_query))
    {
        $return_count_brand = "";

        if($sch_base_type == "all")
        {
            if(in_array($return_count['brand'], $doc_brand_option)){
                $return_count_brand = "doc";
            }
            elseif(in_array($return_count['brand'], $nuzam_brand_option)) {
                $return_count_brand = "nuzam";
            }
            elseif(in_array($return_count['brand'], $ilenol_brand_option)) {
                $return_count_brand = "ilenol";
            }
            elseif(in_array($return_count['brand'], $city_brand_option)) {
                $return_count_brand = "city";
            }
        }
        elseif($sch_base_type == "doc")
        {
            if(in_array($return_count['brand'], $doc_filter_option)){
                $return_count_brand = "1314";
            }elseif(in_array($return_count['brand'], $doc_cubing_option)){
                $return_count_brand = "3386";
            }else{
                $return_count_brand = $return_count['brand'];
            }
        }
        elseif($sch_base_type == "nuzam")
        {
            $return_count_brand = $return_count['brand'];
        }

        if(!empty($return_count_brand))
        {
            $chk_return_type = ($return_count['return_type']=='1') ? "return" : "trade";
            $tmp_return_count_list[$return_count['key_month']][$return_count['order_number']][$chk_return_type][$return_count_brand] = $return_count_brand;
        }
    }

    if(!empty($tmp_return_count_list))
    {
        foreach($tmp_return_count_list as $key_month => $mon_data)
        {
            foreach($mon_data as $ord_no => $ord_data)
            {
                foreach($ord_data as $type => $type_data)
                {
                    $return_count_list[$key_month]["total"][$type]++;

                    foreach($type_data as $ord_key => $ord_brand)
                    {
                        $return_count_list[$key_month][$ord_key][$type]++;
                    }
                }

            }
        }
    }
}
else
{
    $last_return_sql = "
        SELECT
            return_type,
            DATE_FORMAT(regdate, '%Y-%m-%d') AS reg_date
        FROM (SELECT DISTINCT regdate, return_type FROM csm_return ORDER BY regdate DESC) AS rs
        GROUP BY return_type
    ";
    $last_return_query = mysqli_query($my_db, $last_return_sql);
    while($last_return = mysqli_fetch_assoc($last_return_query)){
        $last_return_list[$last_return['return_type']] = $last_return['reg_date'];
    }

    $return_count_where = "`cr`.return_type IN(1,2) AND (`cr`.return_date BETWEEN '{$mil_s_datetime}' AND '{$mil_e_datetime}')";
    $return_count_sql = "
         SELECT
            DATE_FORMAT(`cr`.return_date, '%Y%m') as key_month,
            order_number,
            c_no as brand,
            return_type
        FROM csm_return cr
        WHERE {$return_count_where}
        ORDER BY key_month
    ";
    $return_count_query     = mysqli_query($mil_db, $return_count_sql);
    $tmp_return_count_list  = [];
    while($return_count = mysqli_fetch_assoc($return_count_query))
    {
        $return_count_brand = "";
        if(in_array($return_count['brand'], $mil_brand_option)){
            $return_count_brand = $return_count['brand'];
        }

        if(!empty($return_count_brand)){
            $chk_return_type = ($return_count['return_type']=='1') ? "return" : "trade";
            $tmp_return_count_list[$return_count['key_month']][$return_count['order_number']][$chk_return_type][$return_count_brand] = $return_count_brand;
        }
    }

    $cms_count_chk_list = [];
    if(!empty($tmp_return_count_list))
    {
        foreach($tmp_return_count_list as $key_month => $mon_data)
        {
            foreach($mon_data as $ord_no => $ord_data)
            {
                foreach($ord_data as $type => $type_data)
                {
                    $return_count_list[$key_month]["total"][$type]++;

                    foreach($type_data as $ord_key => $ord_brand)
                    {
                        $return_count_list[$key_month][$ord_key][$type]++;
                    }
                }
            }
        }
    }
}

# NOSP & 불량회수, 위드 물류비
if($sch_base_type != "mil")
{
    # NOSP 타임보드 결과보고 내역
    $add_nosp_where = "1=1 AND `am`.state IN(3,5)";

    if($sch_base_type == "all"){
        $add_nosp_where .= " AND `ar`.brand IN({$nosp_brand_text})";
    }elseif($sch_base_type == "doc"){
        $add_nosp_where .= " AND `ar`.brand IN({$doc_brand_text})";
    }elseif($sch_base_type == "nuzam"){
        $add_nosp_where .= " AND `ar`.brand IN({$nuzam_brand_text})";
    }

    # NOSP 타임보드 날짜 가져오기 (마지막 보고날 부터 8일간)
    $add_nosp_time_where  = $add_nosp_where;
    $add_nosp_time_where .= " AND `am`.product='1'";

    $nosp_time_last_sql = "
        SELECT 
            `am`.adv_s_date
        FROM advertising_result `ar`
        LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
        WHERE {$add_nosp_time_where}
        ORDER BY `am`.adv_s_date DESC LIMIT 1
    ";
    $nosp_time_last_query   = mysqli_query($my_db, $nosp_time_last_sql);
    $nosp_time_last_result  = mysqli_fetch_assoc($nosp_time_last_query);
    $nosp_time_e_date       = isset($nosp_time_last_result['adv_s_date']) ? date("Y-m-d", strtotime($nosp_time_last_result['adv_s_date'])) : "";
    $nosp_time_s_date       = date("Y-m-d", strtotime("{$nosp_time_e_date} -7 days"));
    $nosp_time_e_datetime   = !empty($nosp_time_e_date) ? $nosp_time_e_date." 23:59:59" : $e_datetime;
    $nosp_time_s_datetime   = !empty($nosp_time_s_date) ? $nosp_time_s_date." 00:00:00" : $s_datetime;
    $nosp_time_date_list    = [];
    $add_nosp_time_where   .= " AND `am`.adv_s_date BETWEEN '{$nosp_time_s_datetime}' AND '{$nosp_time_e_datetime}'";

    if(!empty($nosp_time_e_date))
    {
        $nosp_date_sql = "
            SELECT
                DATE_FORMAT(`allday`.Date, '%m/%d') as chart_day,
                DATE_FORMAT(`allday`.Date, '%w') as chart_day_w,
                DATE_FORMAT(`allday`.Date, '%Y%m%d') as chart_key
            FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
            as allday
            WHERE DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$nosp_time_s_date}' AND '{$nosp_time_e_date}'
            ORDER BY chart_key
        ";
        $nosp_date_query        = mysqli_query($my_db, $nosp_date_sql);
        while($nosp_date = mysqli_fetch_assoc($nosp_date_query))
        {
            $nosp_date_text = $nosp_date['chart_day']."({$date_short_option[$nosp_date['chart_day_w']]})";
            $nosp_time_date_list[$nosp_date['chart_key']] = $nosp_date_text;
        }
    }else{
        $nosp_time_date_list = $custom_date_list;
    }

    foreach($nosp_time_date_list as $nosp_date => $nosp_label){
        if($sch_base_type != "all"){
            $nosp_time_report_list[$nosp_date] = $nosp_result_option;
        }
    }

    $nosp_time_report_sql = "
        SELECT
            `ar`.brand,
            DATE_FORMAT(`am`.adv_s_date, '%Y%m%d') as key_date,
            `ar`.adv_price,
            `ar`.click_cnt,
            `ar`.impressions,
            `am`.fee_per
        FROM advertising_result `ar`
        LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
        WHERE {$add_nosp_time_where}
        ORDER BY key_date
    ";
    $nosp_time_report_query = mysqli_query($my_db, $nosp_time_report_sql);
    while($nosp_time_report = mysqli_fetch_assoc($nosp_time_report_query))
    {
        $nosp_time_brand = "";

        if($sch_base_type == "all")
        {
            if(in_array($nosp_time_report['brand'], $doc_brand_option)){
                $nosp_time_brand = "doc";
            }
            elseif(in_array($nosp_time_report['brand'], $nuzam_brand_option)) {
                $nosp_time_brand = "nuzam";
            }
            elseif(in_array($nosp_time_report['brand'], $city_brand_option)) {
                $nosp_time_brand = "city";
            }
        }
        elseif($sch_base_type == "doc")
        {
            if(in_array($nosp_time_report['brand'], $doc_filter_option)){
                $nosp_time_brand = "1314";
            }elseif(in_array($nosp_time_report['brand'], $doc_cubing_option)){
                $nosp_time_brand = "3386";
            }else{
                $nosp_time_brand = $nosp_time_report['brand'];
            }
        }
        elseif($sch_base_type == "nuzam")
        {
            $nosp_time_brand = $nosp_time_report['brand'];
        }

        if(!empty($nosp_time_brand))
        {
            $cal_adv_price = $nosp_time_report['adv_price'] - ($nosp_time_report['adv_price']*($nosp_time_report['fee_per']/100));

            $nosp_time_report_list[$nosp_time_report['key_date']][$nosp_time_brand]['cnt']++;
            $nosp_time_report_list[$nosp_time_report['key_date']][$nosp_time_brand]['adv_price']   += $cal_adv_price;
            $nosp_time_report_list[$nosp_time_report['key_date']][$nosp_time_brand]['click_cnt']   += $nosp_time_report['click_cnt'];
            $nosp_time_report_list[$nosp_time_report['key_date']][$nosp_time_brand]['impressions'] += $nosp_time_report['impressions'];

            $nosp_time_report_list[$nosp_time_report['key_date']]['total']['cnt']++;
            $nosp_time_report_list[$nosp_time_report['key_date']]['total']['adv_price']   += $cal_adv_price;
            $nosp_time_report_list[$nosp_time_report['key_date']]['total']['click_cnt']   += $nosp_time_report['click_cnt'];
            $nosp_time_report_list[$nosp_time_report['key_date']]['total']['impressions'] += $nosp_time_report['impressions'];
        }
    }

    foreach($nosp_time_report_list as $key_date => $nosp_time_brand_data)
    {
        foreach($nosp_time_brand_data as $brand => $nosp_time_data)
        {
            $nosp_time_report_list[$key_date][$brand]['adv_cpc']    += !empty($nosp_time_data['click_cnt']) ? $nosp_time_data['adv_price']/$nosp_time_data['click_cnt'] : 0;
            $nosp_time_report_list[$key_date][$brand]['click_per']  += !empty($nosp_time_data['click_cnt']) ? ($nosp_time_data['click_cnt']/$nosp_time_data['impressions'])*100 : 0;
        }
    }

    # NOSP Special 결과보고 내역
    $add_nosp_special_where  = $add_nosp_where;
    $add_nosp_special_where .= " AND `am`.product='2'";

    $nosp_special_date_sql  = "
        SELECT 
           DATE_FORMAT(`am`.adv_s_date, '%Y%m%d') as adv_s_day,
           DATE_FORMAT(`am`.adv_s_date, '%Y-%m-%d') as adv_s_day_text
        FROM advertising_result `ar`
        LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
        WHERE {$add_nosp_special_where}
        GROUP BY adv_s_day
        ORDER BY adv_s_day DESC LIMIT 8
    ";
    $nosp_special_date_query    = mysqli_query($my_db, $nosp_special_date_sql);
    $nosp_special_date_list     = [];
    $nosp_special_s_date        = "";
    $nosp_special_e_date        = "";
    while($nosp_special_date  = mysqli_fetch_assoc($nosp_special_date_query)){
        $nosp_special_date_list[$nosp_special_date["adv_s_day"]] = $nosp_special_date["adv_s_day_text"];
    }
    ksort($nosp_special_date_list);

    $nosp_special_search_list = [];
    foreach($nosp_special_date_list as $nosp_date => $nosp_label)
    {
        if($sch_base_type != "mil"){
            $nosp_special_report_list[$nosp_date] = $nosp_result_option;
        }

        $nosp_special_search_list[] = "'{$nosp_date}'";
    }

    $nosp_special_search_text  = implode(",", $nosp_special_search_list);
    $add_nosp_special_where   .= " AND DATE_FORMAT(`am`.adv_s_date, '%Y%m%d') IN({$nosp_special_search_text})";

    $nosp_special_report_sql = "
        SELECT
            `ar`.brand,
            DATE_FORMAT(`am`.adv_s_date, '%Y%m%d') as key_date,
            `ar`.adv_price,
            `ar`.click_cnt,
            `ar`.impressions,
            `am`.fee_per
        FROM advertising_result `ar`
        LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
        WHERE {$add_nosp_special_where}
        ORDER BY key_date
    ";
    $nosp_special_report_query = mysqli_query($my_db, $nosp_special_report_sql);
    while($nosp_special_report = mysqli_fetch_assoc($nosp_special_report_query))
    {
        $nosp_special_brand = "";

        if($sch_base_type == "all")
        {
            if(in_array($nosp_special_report['brand'], $doc_brand_option)){
                $nosp_special_brand = "doc";
            }
            elseif(in_array($nosp_special_report['brand'], $nuzam_brand_option)) {
                $nosp_special_brand = "nuzam";
            }
            elseif(in_array($nosp_special_report['brand'], $city_brand_option)) {
                $nosp_special_brand = "city";
            }
        }
        elseif($sch_base_type == "doc")
        {
            if(in_array($nosp_special_report['brand'], $doc_filter_option)){
                $nosp_special_brand = "1314";
            }elseif(in_array($nosp_special_report['brand'], $doc_cubing_option)){
                $nosp_special_brand = "3386";
            }else{
                $nosp_special_brand = $nosp_special_report['brand'];
            }
        }
        elseif($sch_base_type == "nuzam")
        {
            $nosp_special_brand = $nosp_special_report['brand'];
        }

        if(!empty($nosp_special_brand))
        {
            $cal_adv_price = $nosp_special_report['adv_price'] - ($nosp_special_report['adv_price']*($nosp_special_report['fee_per']/100));

            $nosp_special_report_list[$nosp_special_report['key_date']][$nosp_special_brand]['cnt']++;
            $nosp_special_report_list[$nosp_special_report['key_date']][$nosp_special_brand]['adv_price']   += $cal_adv_price;
            $nosp_special_report_list[$nosp_special_report['key_date']][$nosp_special_brand]['click_cnt']   += $nosp_special_report['click_cnt'];
            $nosp_special_report_list[$nosp_special_report['key_date']][$nosp_special_brand]['impressions'] += $nosp_special_report['impressions'];

            $nosp_special_report_list[$nosp_special_report['key_date']]['total']['cnt']++;
            $nosp_special_report_list[$nosp_special_report['key_date']]['total']['adv_price']   += $cal_adv_price;
            $nosp_special_report_list[$nosp_special_report['key_date']]['total']['click_cnt']   += $nosp_special_report['click_cnt'];
            $nosp_special_report_list[$nosp_special_report['key_date']]['total']['impressions'] += $nosp_special_report['impressions'];
        }
    }

    foreach($nosp_special_report_list as $key_date => $nosp_special_brand_data)
    {
        foreach($nosp_special_brand_data as $brand => $nosp_special_data)
        {
            $nosp_special_report_list[$key_date][$brand]['adv_cpc']    += !empty($nosp_special_data['click_cnt']) ? $nosp_special_data['adv_price']/$nosp_special_data['click_cnt'] : 0;
            $nosp_special_report_list[$key_date][$brand]['click_per']  += !empty($nosp_special_data['click_cnt']) ? ($nosp_special_data['click_cnt']/$nosp_special_data['impressions'])*100 : 0;
        }
    }
    $smarty->assign("nosp_time_date_list", $nosp_time_date_list);
    $smarty->assign("nosp_special_date_list", $nosp_special_date_list);

    # 불량 회수 수량
    $bad_return_count_where = "`wcr`.bad_reason != '' AND `wcr`.return_state NOT IN('7','8') AND (`wcr`.return_req_date BETWEEN '{$s_year_datetime}' AND '{$e_month_datetime}')";

    if($sch_base_type == "all"){
        $bad_return_count_where .= " AND `wcr`.c_no IN({$total_brand_text})";
    }elseif($sch_base_type == "doc"){
        $bad_return_count_where .= " AND `wcr`.c_no IN({$doc_brand_text})";
    }elseif($sch_base_type == "nuzam"){
        $bad_return_count_where .= " AND `wcr`.c_no IN({$nuzam_brand_text})";
    }

    $bad_return_count_sql = "
        SELECT
            `wcr`.c_no as brand,
            DATE_FORMAT(`wcr`.return_req_date, '%Y%m') as key_month,
            1 as qty
        FROM work_cms_return wcr
        WHERE {$bad_return_count_where}
        GROUP BY order_number
        ORDER BY key_month ASC
    ";
    $bad_return_count_query = mysqli_query($my_db, $bad_return_count_sql);
    while($bad_return_count = mysqli_fetch_assoc($bad_return_count_query))
    {
        $bad_count_brand = "";

        if($sch_base_type == "all")
        {
            if(in_array($bad_return_count['brand'], $doc_brand_option)){
                $bad_count_brand = "doc";
            }
            elseif(in_array($bad_return_count['brand'], $nuzam_brand_option)) {
                $bad_count_brand = "nuzam";
            }
            elseif(in_array($bad_return_count['brand'], $ilenol_brand_option)) {
                $bad_count_brand = "ilenol";
            }
            elseif(in_array($bad_return_count['brand'], $city_brand_option)) {
                $bad_count_brand = "city";
            }
        }
        elseif($sch_base_type == "doc")
        {
            if(in_array($bad_return_count['brand'], $doc_filter_option)){
                $bad_count_brand = "1314";
            }elseif(in_array($bad_return_count['brand'], $doc_cubing_option)){
                $bad_count_brand = "3386";
            }else{
                $bad_count_brand = $bad_return_count['brand'];
            }
        }
        elseif($sch_base_type == "nuzam")
        {
            $bad_count_brand = $bad_return_count['brand'];
        }

        if(!empty($bad_count_brand))
        {
            $bad_return_count_list[$bad_return_count['key_month']][$bad_count_brand]++;
            $bad_return_count_list[$bad_return_count['key_month']]["total"]++;
        }
    }

    # 위드플레이스 물류비
    $with_delivery_where = "1=1 AND `w`.work_state='6' AND w.prd_no IN('281','270','271','272','273','274','275')";

    if($sch_base_type == "all"){
        $with_delivery_where .= " AND `w`.c_no IN(1113, {$total_brand_text})";
    }elseif($sch_base_type == "doc"){
        $with_delivery_where .= " AND `w`.c_no IN({$doc_brand_text})";
    }elseif($sch_base_type == "nuzam"){
        $with_delivery_where .= " AND `w`.c_no IN({$nuzam_brand_text})";
    }

    $with_delivery_sql = "
        SELECT
            w.c_no as brand,
            w.prd_no,
            DATE_FORMAT(`w`.task_run_regdate, '%Y%m') as key_month,
            w.wd_price_vat as total_value
        FROM `work` w
        WHERE {$with_delivery_where}
        ORDER BY key_month
    ";
    $with_delivery_query = mysqli_query($my_db, $with_delivery_sql);
    while($with_delivery = mysqli_fetch_assoc($with_delivery_query))
    {
        $with_delivery_brand = "";

        if($sch_base_type == "all")
        {
            if(in_array($with_delivery['brand'], $doc_brand_option)){
                $with_delivery_brand = "doc";
            }
            elseif(in_array($with_delivery['brand'], $nuzam_brand_option)) {
                $with_delivery_brand = "nuzam";
            }
            elseif(in_array($with_delivery['brand'], $ilenol_brand_option)) {
                $with_delivery_brand = "ilenol";
            }
            elseif(in_array($with_delivery['brand'], $city_brand_option)) {
                $with_delivery_brand = "city";
            }
            elseif($with_delivery['brand'] == '1113') {
                $with_delivery_brand = "media";
            }
        }
        elseif($sch_base_type == "doc")
        {
            if(in_array($with_delivery['brand'], $doc_filter_option)){
                $with_delivery_brand = "1314";
            }elseif(in_array($with_delivery['brand'], $doc_cubing_option)){
                $with_delivery_brand = "3386";
            }else{
                $with_delivery_brand = $with_delivery['brand'];
            }
        }
        elseif($sch_base_type == "nuzam")
        {
            $with_delivery_brand = $with_delivery['brand'];
        }

        if(!empty($with_delivery_brand))
        {
            if($with_delivery['prd_no'] == '270'){
                $with_delivery_list[$with_delivery['key_month']][$with_delivery_brand]   += $with_delivery['total_value'];
                $with_delivery_list[$with_delivery['key_month']]["total"]                += $with_delivery['total_value'];
            }else{
                $with_delivery_exc_list[$with_delivery['key_month']][$with_delivery_brand]   += $with_delivery['total_value'];
                $with_delivery_exc_list[$with_delivery['key_month']]["total"]                += $with_delivery['total_value'];
            }
        }
    }
}

if($sch_base_type == "mil")
{
    $mil_commerce_fee_option = getMilCommerceFeeOption();
    $cms_report_sql = "
         SELECT 
            c_no,
            prd_no,
            dp_c_no,
            order_number,
            quantity,
            DATE_FORMAT(order_date, '%Y%m') as key_month,
            (unit_price * org_price) as unit_price
        FROM work_cms as w
        WHERE delivery_state='4' AND order_date BETWEEN '{$mil_s_datetime}' AND '{$mil_e_datetime}'
        ORDER BY order_date ASC
    ";
    $cms_report_query  = mysqli_query($mil_db, $cms_report_sql);
    while($cms_report  = mysqli_fetch_assoc($cms_report_query))
    {
        $chk_c_no = $cms_report['c_no'];
        if($cms_report['dp_c_no'] == "19"){
            $cms_report['c_no'] = "23";
        }

        $mil_commerce_fee_list  = isset($mil_commerce_fee_option[$cms_report['c_no']]) ? $mil_commerce_fee_option[$cms_report['c_no']] : [];
        $cms_report_fee_per     = !empty($mil_commerce_fee_list) && isset($mil_commerce_fee_list[$cms_report['dp_c_no']]) ? $mil_commerce_fee_list[$cms_report['dp_c_no']] : 0;
        $cms_report_fee         = $cms_report['unit_price']*($cms_report_fee_per/100);
        $total_price            = $cms_report['unit_price'] - $cms_report_fee;
        
        $mil_product_cost_list[$cms_report['key_month']][$chk_c_no]["sales"] += $total_price;
        $mil_product_cost_list[$cms_report['key_month']]["total"]["sales"] += $total_price;

        $unit_sql   = "SELECT pcr.quantity as qty, (SELECT pcu.sup_price_vat FROM product_cms_unit pcu WHERE pcu.`no`=pcr.option_no) as price FROM product_cms_relation as pcr WHERE pcr.prd_no ='{$cms_report['prd_no']}' AND pcr.display='1'";
        $unit_query = mysqli_query($mil_db, $unit_sql);
        while($unit_result = mysqli_fetch_assoc($unit_query))
        {
            $unit_total_qty = $cms_report['quantity']*$unit_result['qty'];
            $unit_org_price = $unit_total_qty*$unit_result['price'];
            $mil_product_cost_list[$cms_report['key_month']][$chk_c_no]['cost'] += $unit_org_price;
            $mil_product_cost_list[$cms_report['key_month']]["total"]['cost'] += $unit_org_price;
        }
    }

    $return_sql = "
        SELECT 
            c_no,
            DATE_FORMAT(return_date, '%Y%m') as key_month,
            return_price
        FROM csm_return 
        WHERE order_number != '' AND order_number IS NOT NULL AND (return_date BETWEEN '{$mil_s_datetime}' AND '{$mil_e_datetime}') AND c_no > 0
        GROUP BY order_number, return_type, c_no
        ORDER BY return_date
    ";
    $return_query = mysqli_query($mil_db, $return_sql);
    while($return_result = mysqli_fetch_assoc($return_query))
    {
        $mil_product_cost_list[$return_result["key_month"]][$return_result["c_no"]]["sales"] -= $return_result['return_price'];
        $mil_product_cost_list[$return_result["key_month"]]["total"]["sales"] -= $return_result['return_price'];
    }

    if(!empty($mil_product_cost_list)){
        foreach($mil_product_cost_list as $key_date => $date_data){
            foreach($date_data as $c_no => $brand_data)
            {
                $cost_per = ($brand_data['sales'] > 0 && $brand_data['cost']) ? round(($brand_data['cost']/$brand_data['sales'])*100, 1) : 0;
                $mil_product_cost_list[$key_date][$c_no]['cost_per'] = $cost_per;
            }
        }
    }
}

$smarty->assign("custom_date_list", $custom_date_list);
$smarty->assign("custom_month_list", $custom_month_list);
$smarty->assign("custom_year_list", $custom_year_list);
$smarty->assign("mil_custom_month_list", $mil_custom_month_list);
$smarty->assign("global_report_list", $global_report_list);
$smarty->assign("self_report_list", $self_report_list);
$smarty->assign("cms_count_list", $cms_count_list);
$smarty->assign("last_return_list", $last_return_list);
$smarty->assign("return_count_list", $return_count_list);
$smarty->assign("nosp_time_report_list", $nosp_time_report_list);
$smarty->assign("nosp_special_report_list", $nosp_special_report_list);
$smarty->assign("bad_return_count_list", $bad_return_count_list);
$smarty->assign("with_delivery_list", $with_delivery_list);
$smarty->assign("with_delivery_exc_list", $with_delivery_exc_list);
$smarty->assign("mil_product_cost_list", $mil_product_cost_list);

if($sch_base_type == "doc"){
    $smarty->display('wise_bm/total_main_doc.html');
}elseif($sch_base_type == "nuzam"){
    $smarty->display('wise_bm/total_main_nuzam.html');
}elseif($sch_base_type == "mil"){
    $smarty->display('wise_bm/total_main_mil.html');
}else{
    $smarty->display('wise_bm/total_main.html');
}
?>
