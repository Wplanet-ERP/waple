<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 검색조건
$bm_brand_option    = getBrandDetailOption();
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sel_brand_data     = isset($bm_brand_option[$sch_brand]) ? $bm_brand_option[$sch_brand] : "";

if(empty($sel_brand_data)){
    exit("<script>alert('접근 권한이 없습니다.');location.href='/v1/main.php';</script>");
}

$main_brand_option  = $sel_brand_data['brand_list'];
$main_brand_text    = implode(",", $main_brand_option);
$dp_except_list     = getNotApplyDpList();
$dp_except_text     = implode(",", $dp_except_list);

$sch_prev_month     = date('Y-m', strtotime("-1 months"));
$sch_s_datetime     = "{$sch_prev_month}-01 00:00:00";
$sch_e_day          = date("t", strtotime($sch_prev_month));
$sch_e_datetime     = "{$sch_prev_month}-{$sch_e_day} 23:59:59";

$cms_event_sql  = "
    SELECT
        w.prd_no,
        w.task_req,
        w.quantity
    FROM work_cms w 
    WHERE w.delivery_state='4'
      AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'
      AND w.dp_c_no NOT IN({$dp_except_text}) 
      AND w.c_no IN({$main_brand_text}) AND w.log_c_no IN(1113,2809,5484) AND unit_price=0 AND prd_no IN(1027,1028,1167,1600,1625)
    ORDER BY order_date ASC
";
$cms_event_query    = mysqli_query($my_db, $cms_event_sql);
$cms_event_prd_list = array(
    "1027" => array("prd_name" => "SS 보관백", "total_qty" => 0, "main_qty" => 0, "second_qty" => 0, "third_qty" => 0, "fourth_qty" => 0),
    "1028" => array("prd_name" => "Q 보관백", "total_qty" => 0, "main_qty" => 0, "second_qty" => 0, "third_qty" => 0, "fourth_qty" => 0),
    "1167" => array("prd_name" => "세탁망", "total_qty" => 0, "main_qty" => 0, "second_qty" => 0, "third_qty" => 0, "fourth_qty" => 0),
    "1600" => array("prd_name" => "리유저블백", "total_qty" => 0, "main_qty" => 0, "second_qty" => 0, "third_qty" => 0, "fourth_qty" => 0),
    "1625" => array("prd_name" => "슬립미스트", "total_qty" => 0, "main_qty" => 0, "second_qty" => 0, "third_qty" => 0, "fourth_qty" => 0),
);
while($cms_event_prd = mysqli_fetch_assoc($cms_event_query))
{
    if($cms_event_prd['prd_no'] == 1167)
    {
        if(strpos($cms_event_prd["task_req"], "이불") !== false){
            $cms_event_prd_list[$cms_event_prd['prd_no']]['third_qty'] += $cms_event_prd['quantity'];
        }elseif(strpos($cms_event_prd["task_req"], "바디필로우") !== false){
            $cms_event_prd_list[$cms_event_prd['prd_no']]['fourth_qty'] += $cms_event_prd['quantity'];
        }elseif(strpos($cms_event_prd["task_req"], "더블업") !== false){
            $cms_event_prd_list[$cms_event_prd['prd_no']]['second_qty'] += $cms_event_prd['quantity'];
        }elseif(strpos($cms_event_prd["task_req"], "매트리스") !== false){
            $cms_event_prd_list[$cms_event_prd['prd_no']]['main_qty'] += $cms_event_prd['quantity'];
        }
    }

    $cms_event_prd_list[$cms_event_prd['prd_no']]['total_qty'] += $cms_event_prd['quantity'];
}

# 본품
$cms_base_sql  = "
    SELECT
        SUM(w.quantity) as total_qty
    FROM work_cms w 
    WHERE w.delivery_state='4'
      AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'
      AND w.dp_c_no NOT IN({$dp_except_text}) 
      AND w.c_no IN({$main_brand_text}) AND w.log_c_no IN(1113,2809,5484) AND unit_price > 0 AND prd_no IN(2033,2034,2035,2036,2037,2038,2039,2040,2041,2042,2043,2044,2100,2101,2102,2103,2104,2105,2106,2107,2108,2109,2110,2111,2112,2113,2114,2115,2116,2117,2118,2119,2120,2121,2122,2123,2124,2125,2126,2127,2128,2129,2130,2131,2132,2133,2134,2135,2167,2168,2169,2170,2171,2172,2173,2174,2175,2176,2177,2178)
";
$cms_base_query = mysqli_query($my_db, $cms_base_sql);
while($cms_base = mysqli_fetch_assoc($cms_base_query)){
    $cms_event_prd_list['1167']['main_qty']  += $cms_base['total_qty'];
    $cms_event_prd_list['1167']['total_qty'] += $cms_base['total_qty'];
}

$cms_set_sql  = "
    SELECT
        SUM(w.quantity) as total_qty
    FROM work_cms w 
    WHERE w.delivery_state='4'
      AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'
      AND w.dp_c_no NOT IN({$dp_except_text}) 
      AND w.c_no IN({$main_brand_text}) AND w.log_c_no IN(1113,2809,5484) AND unit_price > 0 AND prd_no IN(2057,2058,2059,2060,2061,2062,2063,2064,2065,2066,2067,2068,2069,2070,2071,2072,2074,2075,2076,2077,2078,2079,2080,2081,2082,2083,2084,2085,2086,2087,2088,2089,2090,2091,2092,2093,2094,2095,2096,2097,2098,2099)
";
$cms_set_query = mysqli_query($my_db, $cms_set_sql);
while($cms_set = mysqli_fetch_assoc($cms_set_query)){
    $total_qty = $cms_set['total_qty']*2;
    $cms_event_prd_list['1167']['main_qty']  += $total_qty;
    $cms_event_prd_list['1167']['total_qty'] += $total_qty;
}

$smarty->assign("cms_event_prd_list", $cms_event_prd_list);
$smarty->display('wise_bm/brand_main_nuzam_iframe_cms_event.html');
?>