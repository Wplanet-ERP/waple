<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 검색조건
$bm_brand_option    = getBrandDetailOption();
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sel_brand_data     = isset($bm_brand_option[$sch_brand]) ? $bm_brand_option[$sch_brand] : "";

if(empty($sel_brand_data)){
    exit("<script>alert('접근 권한이 없습니다.');location.href='/v1/main.php';</script>");
}

# 브랜드 변수
$main_brand_option  = $sel_brand_data['brand_list'];
$main_brand_text    = implode(",", $main_brand_option);
$dp_except_list     = getNotApplyDpList();
$dp_except_text     = implode(",", $dp_except_list);

# 상품 판매 RANK (최근 1개월)
$cms_recent_s_date      = date("Y-m-d", strtotime("-1 months"));
$cms_recent_s_datetime  = $cms_recent_s_date." 00:00:00";
$cms_recent_e_date      = date("Y-m-d");
$cms_recent_e_datetime  = date("Y-m-d")." 23:59:59";

$cms_recent_where   = "w.delivery_state='4' AND w.c_no IN({$main_brand_text}) AND (w.order_date BETWEEN '{$cms_recent_s_datetime}' AND '{$cms_recent_e_datetime}') AND w.unit_price > 0";
$cms_recent_sql     = "
    SELECT
        w.prd_no,
        (SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no) as prd_title,
        (SELECT p.combined_product FROM product_cms p WHERE p.prd_no=w.prd_no) as combine_prd_no,
        (SELECT sub.title FROM product_cms sub WHERE sub.prd_no=(SELECT p.combined_product FROM product_cms p WHERE p.prd_no=w.prd_no)) as combine_prd_title,
        SUM(w.quantity) as total_qty,
        SUM(w.unit_price) as total_price
    FROM work_cms as w 
    WHERE {$cms_recent_where}
    GROUP BY prd_no
";
$cms_recent_query       = mysqli_query($my_db, $cms_recent_sql);
$cms_recent_qty_list    = [];
$cms_recent_price_list  = [];
$cms_recent_name_list   = [];
while($cms_recent = mysqli_fetch_assoc($cms_recent_query))
{
    $chk_prd_no     = $cms_recent['prd_no'];
    $chk_prd_title  = $cms_recent['prd_title'];

    if(!empty($cms_recent['combine_prd_no'])){
        $chk_prd_no     = $cms_recent['combine_prd_no'];
        $chk_prd_title  = $cms_recent['combine_prd_title'];
    }

    $cms_recent_qty_list[$chk_prd_no]   += $cms_recent['total_qty'];
    $cms_recent_price_list[$chk_prd_no] += $cms_recent['total_price']/1000;
    $cms_recent_name_list[$chk_prd_no]   = $chk_prd_title;
}

$cms_recent_qty_best_list       = [];
$cms_recent_qty_worst_list      = [];
$cms_recent_price_best_list     = [];
$cms_recent_price_worst_list    = [];

$qty_best_idx   = 0;
$qty_worst_idx  = 0;
arsort($cms_recent_qty_list);
foreach($cms_recent_qty_list as $prd_no => $total_qty){
    if($qty_best_idx > 4){
        break;
    }

    $cms_recent_qty_best_list[] = array("title" => $cms_recent_name_list[$prd_no], "total" => $total_qty);
    $qty_best_idx++;
}

asort($cms_recent_qty_list);
foreach($cms_recent_qty_list as $prd_no => $total_qty){
    if($qty_worst_idx > 4){
        break;
    }

    $cms_recent_qty_worst_list[] = array("title" => $cms_recent_name_list[$prd_no], "total" => $total_qty);
    $qty_worst_idx++;
}

$price_best_idx  = 0;
$price_worst_idx = 0;
arsort($cms_recent_price_list);
foreach($cms_recent_price_list as $prd_no => $total_price){
    if($price_best_idx > 4){
        break;
    }

    $cms_recent_price_best_list[] = array("title" => $cms_recent_name_list[$prd_no], "total" => $total_price);
    $price_best_idx++;
}

asort($cms_recent_price_list);
foreach($cms_recent_price_list as $prd_no => $total_price){
    if($price_worst_idx > 4){
        break;
    }

    $cms_recent_price_worst_list[] = array("title" => $cms_recent_name_list[$prd_no], "total" => $total_price);
    $price_worst_idx++;
}

$smarty->assign("cms_recent_qty_best_list", $cms_recent_qty_best_list);
$smarty->assign("cms_recent_qty_worst_list", $cms_recent_qty_worst_list);
$smarty->assign("cms_recent_price_best_list", $cms_recent_price_best_list);
$smarty->assign("cms_recent_price_worst_list", $cms_recent_price_worst_list);

$smarty->display('wise_bm/brand_main_base_iframe_cms_rank.html');
?>
