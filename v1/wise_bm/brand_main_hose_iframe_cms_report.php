<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_date.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');
require('../inc/helper/commerce_sales.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 검색조건
$bm_brand_option    = getBrandDetailOption();
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sel_brand_data     = isset($bm_brand_option[$sch_brand]) ? $bm_brand_option[$sch_brand] : "";

if(empty($sel_brand_data)){
    exit("<script>alert('접근 권한이 없습니다.');location.href='/v1/main.php';</script>");
}

# 브랜드 변수
$brand_manager          = $sel_brand_data['manager'];
$brand_manager_name     = $sel_brand_data['manager_name'];
$main_brand_option      = $sel_brand_data['brand_list'];
$global_base_option     = isset($sel_brand_data['global_base']) ? $sel_brand_data['global_base'] : [];
$global_cms_option      = isset($sel_brand_data['global_cms']) ? $sel_brand_data['global_cms'] : [];
$sch_brand_url          = $sel_brand_data['brand_url'];
$main_brand_text        = implode(",", $main_brand_option);
$global_base_brand      = implode(",", $global_base_option);
$global_cms_brand       = implode(",", $global_cms_option);
$dp_except_list         = getNotApplyDpList();
$dp_except_text         = implode(",", $dp_except_list);
$dp_self_imweb_list     = getSelfDpImwebCompanyList();
$global_dp_all_list     = getGlobalDpCompanyList();
$global_dp_total_list   = $global_dp_all_list['total'];

# 날짜 체크
$today_s_w		    = date('w')-1;
$sch_e_date         = date('Y-m-d');
$sch_s_date         = date('Y-m-d', strtotime("{$sch_e_date} -14 days"));
$today_s_week       = date('Y-m-d', strtotime("-{$today_s_w} days"));
$sch_s_week         = date('Y-m-d', strtotime("{$today_s_week} -10 weeks"));
$sch_e_week         = date("Y-m-d",strtotime("{$today_s_week} +6 days"));
$sch_e_month        = date('Y-m', strtotime($sch_e_date));
$sch_s_month        = date('Y-m', strtotime("{$sch_e_month} -6 months"));

$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "date";
$sch_s_datetime     = "";
$sch_e_datetime     = "";
$sch_net_date       = "";

$add_comm_where     = "display='1' AND brand IN({$main_brand_text})";
$add_cms_where      = "w.delivery_state='4' AND w.dp_c_no NOT IN({$dp_except_text}) AND w.c_no IN({$main_brand_text}) AND w.log_c_no IN(1113,2809,5484)";
$add_quick_where    = "w.prd_type='1' AND w.unit_price > 0 AND w.quick_state='4' AND w.c_no IN({$main_brand_text})";
$add_return_where   = "order_number != '' AND order_number IS NOT NULL";
$add_result_where   = "1=1 AND `am`.state IN(3,5) AND `am`.product IN(1,2) AND `am`.media='265' AND `ar`.brand IN({$main_brand_text})";
$add_net_where      = "1=1";
$add_comm_column    = "";
$add_cms_column     = "";
$add_quick_column   = "";
$add_return_column  = "";
$add_result_column  = "";
$add_net_column     = "";

# 글로벌 검색
$add_global_comm_where = "display='1' AND brand IN({$global_base_brand})";
$add_global_cms_where  = "w.delivery_state='4' AND w.dp_c_no NOT IN({$dp_except_text}) AND w.c_no IN({$global_cms_brand}) AND w.log_c_no NOT IN(1113,2809,5484)";

# 날짜 검색
$sch_rate_date = "";
if($sch_date_type == "month")
{
    $sch_s_month            = date("Y-m", strtotime("{$sch_s_month} -1 months"));
    $sch_rate_date          = date("Ym", strtotime($sch_s_month));

    $all_date_where         = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	        = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title         = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_comm_where        .= " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_global_comm_where .= " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_comm_column        = "DATE_FORMAT(sales_date, '%Y%m')";

    $add_net_where         .= " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_net_column         = "DATE_FORMAT(sales_date, '%Y%m')";
    $add_cms_column         = "DATE_FORMAT(order_date, '%Y%m')";

    $sch_e_day              = date("t", strtotime($sch_e_month));
    $sch_s_datetime         = $sch_s_month."-01 00:00:00";
    $sch_e_datetime         = $sch_e_month."-{$sch_e_day} 23:59:59";

    $add_quick_where       .= " AND w.run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column       = "DATE_FORMAT(w.run_date, '%Y%m')";

    $add_return_where      .= " AND return_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_return_column      = "DATE_FORMAT(return_date, '%Y%m')";

    $add_result_where      .= " AND am.adv_s_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'";
    $add_result_column      = "DATE_FORMAT(am.adv_s_date, '%Y%m')";

    $sch_net_date           = $sch_s_month."-01";
}
elseif($sch_date_type == "week")
{
    $all_date_where         = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	        = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title         = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_comm_where        .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_global_comm_where .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_comm_column        = "DATE_FORMAT(DATE_SUB(sales_date, INTERVAL(IF(DAYOFWEEK(sales_date)=1,8,DAYOFWEEK(sales_date))-2) DAY), '%Y%m%d')";

    $add_net_where         .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_net_column         = "DATE_FORMAT(DATE_SUB(sales_date, INTERVAL(IF(DAYOFWEEK(sales_date)=1,8,DAYOFWEEK(sales_date))-2) DAY), '%Y%m%d')";
    $add_cms_column         = "DATE_FORMAT(DATE_SUB(order_date, INTERVAL(IF(DAYOFWEEK(order_date)=1,8,DAYOFWEEK(order_date))-2) DAY), '%Y%m%d')";

    $sch_s_datetime         = $sch_s_week." 00:00:00";
    $sch_e_datetime         = $sch_e_week." 23:59:59";

    $add_quick_where       .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column       = "DATE_FORMAT(DATE_SUB(run_date, INTERVAL(IF(DAYOFWEEK(run_date)=1,8,DAYOFWEEK(run_date))-2) DAY), '%Y%m%d')";

    $add_return_where      .= " AND return_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_return_column      = "DATE_FORMAT(DATE_SUB(return_date, INTERVAL(IF(DAYOFWEEK(return_date)=1,8,DAYOFWEEK(return_date))-2) DAY), '%Y%m%d')";

    $add_result_where      .= " AND am.adv_s_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'";
    $add_result_column      = "DATE_FORMAT(DATE_SUB(am.adv_s_date, INTERVAL(IF(DAYOFWEEK(am.adv_s_date)=1,8,DAYOFWEEK(am.adv_s_date))-2) DAY), '%Y%m%d')";

    $sch_net_date           = $sch_s_week;
}
else
{
    $all_date_where         = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	        = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title         = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_comm_where        .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_global_comm_where .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_comm_column        = "DATE_FORMAT(sales_date, '%Y%m%d')";

    $add_net_where         .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_net_column         = "DATE_FORMAT(sales_date, '%Y%m%d')";
    $add_cms_column         = "DATE_FORMAT(order_date, '%Y%m%d')";

    $sch_s_datetime         = $sch_s_date." 00:00:00";
    $sch_e_datetime         = $sch_e_date." 23:59:59";

    $add_quick_where       .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column       = "DATE_FORMAT(run_date, '%Y%m%d')";

    $add_return_where      .= " AND return_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_return_column      = "DATE_FORMAT(return_date, '%Y%m%d')";

    $add_result_where      .= " AND am.adv_s_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'";
    $add_result_column      = "DATE_FORMAT(am.adv_s_date, '%Y%m%d')";

    $sch_net_date           = $sch_s_date;
}

# 날짜 정리 및 리스트 Init
$commerce_date_list             = [];
$commerce_link_date_list        = [];
$sales_tmp_total_list           = [];
$cost_tmp_total_list            = [];
$net_report_date_list           = [];
$net_parent_percent_list        = [];
$total_all_date_list            = [];
$total_report_table_list        = [];
$self_report_table_list         = [];
$total_date_chart_list          = [];

$global_sales_tmp_total_list    = [];
$global_cost_tmp_total_list     = [];
$global_net_report_date_list    = [];
$global_net_parent_percent_list = [];
$global_report_table_list       = [];
$global_date_chart_list         = [];
$cms_report_subtotal_sum_list   = [];
$nosp_result_list               = [];
$nosp_result_total              = 0;

$date_name_option   = getDateChartOption();;
$nosp_result_init   = array("1" => array("count" => 0, "total" => 0), "2" => array("count" => 0, "total" => 0)); # 1: 타임보드, 2: 스폐셜DA

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($date = mysqli_fetch_array($all_date_query))
{
    $total_all_date_list[$date['date_key']] = array("s_date" => date("Y-m-d",strtotime($date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($date['date_key']))." 23:59:59");

    $chart_title = $date['chart_title'];
    if($sch_date_type == 'date'){
        $date_convert   = explode('_', $chart_title);
        $date_name      = isset($date_convert[1]) ? $date_name_option[$date_convert[1]] : "";
        $chart_title    = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
    }

    foreach($main_brand_option as $c_no)
    {
        $cost_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']]  = 0;
        $sales_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
        $net_report_date_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
        $net_parent_percent_list[$c_no] = 0;
    }

    if(!empty($global_base_option))
    {
        foreach($global_base_option as $global_c_no)
        {
            $global_cost_tmp_total_list[$global_c_no][$date['chart_key']][$date['date_key']]  = 0;
            $global_sales_tmp_total_list[$global_c_no][$date['chart_key']][$date['date_key']] = 0;
            $global_net_report_date_list[$global_c_no][$date['chart_key']][$date['date_key']] = 0;
            $global_net_parent_percent_list[$global_c_no] = 0;
        }
    }

    if(!isset($commerce_date_list[$date['chart_key']]))
    {
        $commerce_date_list[$date['chart_key']] = $chart_title;
        $nosp_result_list[$date['chart_key']]   = $nosp_result_init;

        $total_report_table_list[$date['chart_key']] = array(
            "sales"     => 0,
            "cost"      => 0,
            "profit"    => 0,
            "roas"      => 0,
            "profit_per"=> 0,
        );

        $self_report_table_list[$date['chart_key']] = array(
            "imweb"     => 0,
            "smart"     => 0
        );

        if(!empty($global_base_option))
        {
            $global_report_table_list[$date['chart_key']] = array(
                "sales"     => 0,
                "cost"      => 0,
                "profit"    => 0
            );
        }

        $cms_report_subtotal_sum_list[$date['chart_key']] = 0;

        if($sch_date_type == "month"){
            $link_month     = date("Y-m", strtotime($date['date_key']));
            $link_e_day     = date("t", strtotime($link_month));
            $link_s_date    = "{$link_month}-01";
            $link_e_date    = "{$link_month}-{$link_e_day}";
            $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
        }
        elseif($sch_date_type == "week"){
            $link_s_date    = date("Y-m-d", strtotime($date['date_key']));
            $link_s_w       = date('w', strtotime($link_s_date));
            $link_e_w       = 7-$link_s_w;
            $link_e_date    = date("Y-m-d", strtotime("{$link_s_date} +{$link_e_w} days"));
            $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
        }
        else{
            $link_s_date    = date("Y-m-d", strtotime($date['date_key']));
            $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_s_date);
        }
    }
}

# 수기 데이터
$commerce_sql  = "
    SELECT
        cr.`type`,
        cr.price as price,
        cr.brand,
        DATE_FORMAT(sales_date, '%Y%m%d') as sales_date,
        {$add_comm_column} as key_date
    FROM commerce_report `cr`
    WHERE {$add_comm_where}
    ORDER BY key_date ASC
";
$commerce_query = mysqli_query($my_db, $commerce_sql);
while($commerce = mysqli_fetch_assoc($commerce_query))
{
    if($commerce['type'] == 'cost'){
        $cost_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
    }elseif($commerce['type'] == 'sales'){
        $sales_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
    }
}

if(!empty($global_report_table_list))
{
    $global_commerce_sql  = "
        SELECT
            cr.`type`,
            cr.price as price,
            cr.brand,
            DATE_FORMAT(sales_date, '%Y%m%d') as sales_date,
            {$add_comm_column} as key_date
        FROM commerce_report `cr`
        WHERE {$add_global_comm_where}
        ORDER BY key_date ASC
    ";
    $global_commerce_query = mysqli_query($my_db, $global_commerce_sql);
    while($global_commerce = mysqli_fetch_assoc($global_commerce_query))
    {
        if($global_commerce['type'] == 'cost'){
            $global_cost_tmp_total_list[$global_commerce['brand']][$global_commerce['key_date']][$global_commerce['sales_date']] += $global_commerce['price'];
        }elseif($global_commerce['type'] == 'sales'){
            $global_sales_tmp_total_list[$global_commerce['brand']][$global_commerce['key_date']][$global_commerce['sales_date']] += $global_commerce['price'];
        }
    }
}

# 배송리스트 데이터
$commerce_fee_option         = getCommerceFeeOption();
$cms_delivery_chk_fee_list   = [];
$cms_delivery_chk_nuzam_list = [];
$cms_delivery_chk_ord_list   = [];
$cms_delivery_chk_list       = [];

$global_cms_delivery_chk_fee_list   = [];
$global_cms_delivery_chk_ord_list   = [];
$global_cms_delivery_chk_list       = [];

foreach($total_all_date_list as $date_data)
{
    $cms_report_sql      = "
        SELECT
            c_no,
            dp_c_no,
            order_number,
            DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
            {$add_cms_column} as key_date, 
            unit_price,
            (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
            unit_delivery_price,
            (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
        FROM work_cms as w
        WHERE {$add_cms_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}') 
    ";
    $cms_report_query = mysqli_query($my_db, $cms_report_sql);
    while($cms_report = mysqli_fetch_assoc($cms_report_query))
    {
        $commerce_fee_total_list    = isset($commerce_fee_option[$cms_report['c_no']]) ? $commerce_fee_option[$cms_report['c_no']] : [];
        $commerce_fee_per_list      = !empty($commerce_fee_total_list) && isset($commerce_fee_total_list[$cms_report['dp_c_no']]) ? $commerce_fee_total_list[$cms_report['dp_c_no']] : 0;
        $commerce_fee_per           = 0;

        if(!empty($commerce_fee_per_list))
        {
            foreach($commerce_fee_per_list as $fee_date => $fee_val){
                if($cms_report['sales_date'] >= $fee_date){
                    $commerce_fee_per = $fee_val;
                }
            }
        }

        $cms_report_fee = $cms_report['unit_price']*($commerce_fee_per/100);
        $total_price    = $cms_report['unit_price'] - $cms_report_fee;

        $sales_tmp_total_list[$cms_report['c_no']][$cms_report['key_date']][$cms_report['sales_date']] += $total_price;
        $cms_report_subtotal_sum_list[$cms_report['key_date']] += $total_price;

        if(in_array($cms_report['dp_c_no'], $dp_self_imweb_list))
        {
            $cost_tmp_total_list[$cms_report['c_no']][$cms_report['key_date']][$cms_report['sales_date']] += $cms_report['coupon_price'];
            $self_report_table_list[$cms_report['key_date']]["imweb"] += $total_price;
        }

        if($cms_report['dp_c_no'] == '5427'){
            $self_report_table_list[$cms_report['key_date']]["smart"] += $total_price;
        }

        if($cms_report['sales_date'] >= 20240101)
        {
            if ($cms_report['unit_delivery_price'] > 0) {
                $cms_delivery_chk_fee_list[$cms_report['order_number']] = 1;
            }
            elseif($cms_report['unit_delivery_price'] == 0 && $cms_report['unit_price'] > 0)
            {
                $cms_delivery_chk_list[$cms_report['order_number']][$cms_report['dp_c_no']][$cms_report['key_date']][$cms_report['sales_date']] = $cms_report['deli_cnt'];
                $cms_delivery_chk_ord_list[$cms_report['order_number']][$cms_report['c_no']] = $cms_report['c_no'];

                if ($cms_report['c_no'] == '2827' || $cms_report['c_no'] == '4878') {
                    $cms_delivery_chk_nuzam_list[$cms_report['order_number']] = 1;
                }
            }
        }
    }

    if(!empty($global_report_table_list))
    {
        $global_cms_report_sql      = "
            SELECT
                c_no,
                dp_c_no,
                order_number,
                DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
                {$add_cms_column} as key_date, 
                unit_price,
                (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
                unit_delivery_price,
                (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
            FROM work_cms as w
            WHERE {$add_global_cms_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}') 
        ";
        $global_cms_report_query = mysqli_query($my_db, $global_cms_report_sql);
        while($global_cms_report = mysqli_fetch_assoc($global_cms_report_query))
        {
            if($sch_brand == "doc"){
                $global_cms_report['c_no']  = '5513';
            }else{
                break;
            }

            $global_total_price    = $global_cms_report['unit_price'];

            if($global_cms_report['sales_date'] >= 20240101)
            {
                if ($global_cms_report['unit_delivery_price'] > 0) {
                    $global_cms_delivery_chk_fee_list[$global_cms_report['order_number']] = 1;
                }
                elseif($global_cms_report['unit_delivery_price'] == 0 && $global_cms_report['unit_price'] > 0)
                {
                    $global_cms_delivery_chk_list[$global_cms_report['order_number']][$global_cms_report['dp_c_no']][$global_cms_report['key_date']][$global_cms_report['sales_date']] = $global_cms_report['deli_cnt'];
                    $global_cms_delivery_chk_ord_list[$global_cms_report['order_number']][$global_cms_report['c_no']] = $global_cms_report['c_no'];
                }
            }

            $global_sales_tmp_total_list[$global_cms_report['c_no']][$global_cms_report['key_date']][$global_cms_report['sales_date']] += $global_total_price;
        }
    }

}

# 무료 배송비 계산
foreach($cms_delivery_chk_list as $chk_ord => $chk_ord_data)
{
    if(isset($cms_delivery_chk_fee_list[$chk_ord])){
        continue;
    }

    $chk_delivery_price = isset($cms_delivery_chk_nuzam_list[$chk_ord]) ? 5000 : 3000;

    foreach ($chk_ord_data as $chk_dp_c_no => $chk_dp_data)
    {
        if(in_array($chk_dp_c_no, $global_dp_total_list)){
            $chk_delivery_price = 6000;
        }

        foreach ($chk_dp_data as $chk_key_date => $chk_key_data)
        {
            foreach ($chk_key_data as $chk_sales_date => $deli_cnt)
            {
                $cal_delivery_price     = $chk_delivery_price * $deli_cnt;

                $chk_brand_deli_price   = $cal_delivery_price;
                $chk_brand_cnt          = count($cms_delivery_chk_ord_list[$chk_ord]);
                $cal_brand_deli_price   = round($cal_delivery_price/$chk_brand_cnt);
                $cal_brand_idx          = 1;
                foreach($cms_delivery_chk_ord_list[$chk_ord] as $chk_c_no)
                {
                    $cal_one_delivery_price  = $cal_brand_deli_price;
                    $chk_brand_deli_price   -= $cal_one_delivery_price;

                    if($cal_brand_idx == $chk_brand_cnt){
                        $cal_one_delivery_price += $chk_brand_deli_price;
                    }

                    $cost_tmp_total_list[$chk_c_no][$chk_key_date][$chk_sales_date] += $cal_one_delivery_price;

                    $cal_brand_idx++;
                }
            }
        }
    }
}

# 입/출고요청 리스트
$quick_report_sql      = "
        SELECT 
            c_no,
            run_c_no,
            run_c_name,
            order_number,
            DATE_FORMAT(run_date, '%Y%m%d') as sales_date,
            {$add_quick_column} as key_date, 
            unit_price
        FROM work_cms_quick as w
        WHERE {$add_quick_where}
    ";
$quick_report_query = mysqli_query($my_db, $quick_report_sql);
while($quick_report_result = mysqli_fetch_assoc($quick_report_query))
{
    $sales_tmp_total_list[$quick_report_result['c_no']][$quick_report_result['key_date']][$quick_report_result['sales_date']] += $quick_report_result['unit_price'];
}

# NOSP 계산
$nosp_result_sql = "
        SELECT
        `ar`.am_no,
        `ar`.brand,
        `am`.product,
        `am`.fee_per,
        `am`.price,
        `ar`.impressions,
        `ar`.click_cnt,
        `ar`.adv_price,
        DATE_FORMAT(am.adv_s_date, '%Y%m%d') as adv_s_day,
        {$add_result_column} as key_date
    FROM advertising_result `ar`
    LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
    WHERE {$add_result_where}
    ORDER BY `am`.adv_s_date ASC
";
$nosp_result_query      = mysqli_query($my_db, $nosp_result_sql);
$nosp_result_tmp_list   = [];
$nosp_result_chk_list   = [];
while($nosp_result = mysqli_fetch_assoc($nosp_result_query))
{
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["product"]          = $nosp_result['product'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["sales_date"]       = $nosp_result['adv_s_day'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["key_date"]         = $nosp_result['key_date'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["fee_per"]          = $nosp_result['fee_per'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_adv_price"]  += $nosp_result['adv_price'];
}

foreach($nosp_result_tmp_list as $am_no => $nosp_result_tmp)
{
    foreach($nosp_result_tmp as $brand => $nosp_brand_data)
    {
        $fee_price      = $nosp_brand_data['total_adv_price']*($nosp_brand_data['fee_per']/100);
        $cal_price      = $nosp_brand_data["total_adv_price"]-$fee_price;
        $cal_price_vat  = $cal_price*1.1;

        $nosp_result_list[$nosp_brand_data['key_date']][$nosp_brand_data['product']]['total'] += $cal_price_vat;

        if(!isset($nosp_result_chk_list[$am_no])){
            $nosp_result_chk_list[$am_no] = 1;
            $nosp_result_list[$nosp_brand_data['key_date']][$nosp_brand_data['product']]['count']++;
            $nosp_result_total++;
        }

        $cost_tmp_total_list[$brand][$nosp_brand_data['key_date']][$nosp_brand_data['sales_date']] += $cal_price_vat;
    }
}

foreach($main_brand_option as $c_no)
{
    # NET 매출 관련 작업
    $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$sch_net_date}' ORDER BY sales_date DESC LIMIT 1;";
    $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
    $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
    $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
    $net_parent             = isset($net_pre_percent_result['sales_date']) ? $net_pre_percent_result['sales_date'] : 0;

    $net_parent_percent_list[$c_no] = $net_pre_percent;

    $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date, {$add_net_column} as key_date FROM commerce_report_net WHERE {$add_net_where} AND brand='{$c_no}' ORDER BY sales_date ASC";
    $net_report_query       = mysqli_query($my_db, $net_report_sql);
    while($net_report = mysqli_fetch_assoc($net_report_query))
    {
        $net_report_date_list[$c_no][$net_report['key_date']][$net_report['sales_date']] = $net_report['percent'];
    }
}

foreach($net_report_date_list as $c_no => $net_company_data)
{
    $cal_net_percent = $net_parent_percent_list[$c_no];

    foreach($net_company_data as $key_date => $net_report_data)
    {
        foreach($net_report_data as $sales_date => $net_percent_val)
        {
            if($net_percent_val > 0 ){
                $cal_net_percent = $net_percent_val;
            }

            $net_percent = $cal_net_percent/100;
            $sales_total = round($sales_tmp_total_list[$c_no][$key_date][$sales_date]);
            $cost_total  = round($cost_tmp_total_list[$c_no][$key_date][$sales_date]);
            $net_price   = $sales_total > 0 ? (int)($sales_total*$net_percent) : 0;
            $profit      = $cost_total > 0 ? ($net_price-$cost_total) : $net_price;

            $total_report_table_list[$key_date]['sales']     += $sales_total;
            $total_report_table_list[$key_date]['cost']      += $cost_total;
            $total_report_table_list[$key_date]['profit']    += $profit;
        }
    }
}

$date_chart_name_list   = $commerce_date_list;
$prev_data = [];
foreach($total_report_table_list as $key_date => $date_data)
{
    # ROAS 계산
    $cms_subtotal = $cms_report_subtotal_sum_list[$key_date];
    $total_report_table_list[$key_date]['roas']         = ($date_data['cost'] > 0) ? ($date_data['sales']/$date_data['cost'])*100 : 0;
    $total_report_table_list[$key_date]['profit_rate']  = ($date_data['sales'] > 0) ? ($date_data['profit']/$date_data['sales'])*100 : 0;

    if(!empty($sch_rate_date) && $sch_rate_date == $key_date){
        $prev_data      = $total_report_table_list[$key_date];
        $prev_self_data = $self_report_table_list[$key_date];
        continue;
    }

    $total_date_chart_list[0]['title']  = "매출";
    $total_date_chart_list[0]['type']   = "bar";
    $total_date_chart_list[0]['color']  = "rgba(0,0,255,0.8)";
    $total_date_chart_list[0]['data'][] = $date_data['sales'];
    $total_date_chart_list[1]['title']  = "비용";
    $total_date_chart_list[1]['type']   = "bar";
    $total_date_chart_list[1]['color']  = "rgba(255,0,0,0.8)";
    $total_date_chart_list[1]['data'][] = $date_data['cost'];
    $total_date_chart_list[2]['title']  = "공헌이익";
    $total_date_chart_list[2]['type']   = "line";
    $total_date_chart_list[2]['color']  = "rgba(0,0,0,0.8)";
    $total_date_chart_list[2]['data'][] = $date_data['profit'];

    # 증감률 계산
    $cur_date_data      = $total_report_table_list[$key_date];
    $prev_sales_per     = ($prev_data['sales'] == 0 || $cur_date_data['sales'] == 0) ? 0 : $prev_data['sales']/100;
    $prev_cost_per      = ($prev_data['cost'] == 0 || $cur_date_data['cost'] == 0) ? 0 : $prev_data['cost']/100;
    $prev_profit_per    = ($prev_data['profit'] == 0 || $cur_date_data['profit'] == 0) ? 0 : $prev_data['profit']/100;
    $prev_roas_per      = ($prev_data['roas'] == 0 || $cur_date_data['roas'] == 0) ? 0 : $prev_data['roas'];
    $prev_profit_rate   = ($prev_data['profit_rate'] == 0 || $cur_date_data['profit_rate'] == 0) ? 0 : $prev_data['profit_rate'];
    $prev_self_imweb    = ($prev_self_data['imweb'] == 0 || $prev_self_data['imweb'] == 0) ? 0 : $prev_self_data['imweb']/100;
    $prev_self_smart    = ($prev_self_data['smart'] == 0 || $prev_self_data['smart'] == 0) ? 0 : $prev_self_data['smart']/100;

    $sales_tmp          = $cur_date_data['sales']-$prev_data['sales'];
    $sales_per          = ($prev_sales_per == 0) ? 0 : round($sales_tmp/$prev_sales_per, 1);
    $cost_tmp           = ($cur_date_data['cost']*-1)-($prev_data['cost']*-1);
    $cost_per           = ($prev_cost_per == 0) ? 0 : round($cost_tmp/$prev_cost_per, 1);
    $profit_tmp         = $cur_date_data['profit']-$prev_data['profit'];

    if($prev_data['profit'] < 0 && $cur_date_data['profit'] > 0){
        $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1)*-1;
    }elseif($prev_data['profit'] < 0 && $cur_date_data['profit'] < 0){
        $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1)*-1;
    }else{
        $profit_per = ($prev_profit_per == 0) ? 0 : round($profit_tmp/$prev_profit_per, 1);
    }

    $roas_tmp           = $cur_date_data['roas']-$prev_data['roas'];
    $roas_per           = ($prev_profit_per == 0) ? 0 : round($roas_tmp,1);
    $profit_rate_tmp    = $cur_date_data['profit_rate']-$prev_data['profit_rate'];
    $profit_rate_per    = ($prev_profit_rate == 0) ? 0 : round($profit_rate_tmp,1);

    $date_self_data     = $self_report_table_list[$key_date];
    $self_imweb_tmp     = $date_self_data['imweb']-$prev_self_data['imweb'];
    $self_imweb_per     = ($prev_self_imweb == 0) ? 0 : round($self_imweb_tmp/$prev_self_imweb, 1);
    $self_smart_tmp     = $date_self_data['smart']-$prev_self_data['smart'];
    $self_smart_per     = ($prev_self_smart == 0) ? 0 : round($self_smart_tmp/$prev_self_smart, 1);

    $total_report_table_list[$key_date]["sales_per"]        = $sales_per;
    $total_report_table_list[$key_date]["cost_per"]         = $cost_per;
    $total_report_table_list[$key_date]["profit_per"]       = $profit_per;
    $total_report_table_list[$key_date]["roas_per"]         = $roas_per;
    $total_report_table_list[$key_date]["profit_rate_per"]  = $profit_rate_per;
    $total_report_table_list[$key_date]["self_imweb_per"]   = $self_imweb_per;
    $total_report_table_list[$key_date]["self_smart_per"]   = $self_smart_per;

    $prev_data      = $total_report_table_list[$key_date];
    $prev_self_data = $self_report_table_list[$key_date];
}

if(!empty($sch_rate_date))
{
    unset($commerce_date_list[$sch_rate_date]);
    unset($date_chart_name_list[$sch_rate_date]);
    unset($total_report_table_list[$sch_rate_date]);
    unset($self_report_table_list[$sch_rate_date]);
}

# 해외 매출 계산
if(!empty($global_report_table_list))
{
    foreach($global_cms_delivery_chk_list as $global_chk_ord => $global_chk_ord_data)
    {
        if(isset($global_cms_delivery_chk_fee_list[$global_chk_ord])){
            continue;
        }

        $global_chk_delivery_price = 6000;

        foreach ($global_chk_ord_data as $global_chk_dp_c_no => $global_chk_dp_data)
        {
            foreach ($global_chk_dp_data as $global_chk_key_date => $global_chk_key_data)
            {
                foreach ($global_chk_key_data as $global_chk_sales_date => $global_deli_cnt)
                {
                    $global_cal_delivery_price     = $global_chk_delivery_price * $global_deli_cnt;

                    $global_chk_brand_deli_price   = $global_cal_delivery_price;
                    $global_chk_brand_cnt          = count($global_cms_delivery_chk_ord_list[$global_chk_ord]);
                    $global_cal_brand_deli_price   = round($global_cal_delivery_price/$global_chk_brand_cnt);
                    $global_cal_brand_idx          = 1;
                    foreach($global_cms_delivery_chk_ord_list[$global_chk_ord] as $global_chk_c_no)
                    {
                        $global_cal_one_delivery_price  = $global_cal_brand_deli_price;
                        $global_chk_brand_deli_price   -= $global_cal_one_delivery_price;

                        if($global_cal_brand_idx == $global_chk_brand_cnt){
                            $global_cal_one_delivery_price += $global_chk_brand_deli_price;
                        }

                        $global_cost_tmp_total_list[$global_chk_c_no][$global_chk_key_date][$global_chk_sales_date] += $global_cal_one_delivery_price;

                        $global_cal_brand_idx++;
                    }
                }
            }
        }
    }

    foreach($global_base_option as $global_c_no)
    {
        # NET 매출 관련 작업
        $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$global_c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$sch_net_date}' ORDER BY sales_date DESC LIMIT 1;";
        $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
        $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
        $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
        $net_parent             = isset($net_pre_percent_result['sales_date']) ? $net_pre_percent_result['sales_date'] : 0;

        $global_net_parent_percent_list[$global_c_no] = $net_pre_percent;

        $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date, {$add_net_column} as key_date FROM commerce_report_net WHERE {$add_net_where} AND brand='{$global_c_no}' ORDER BY sales_date ASC";
        $net_report_query       = mysqli_query($my_db, $net_report_sql);
        while($net_report = mysqli_fetch_assoc($net_report_query))
        {
            $global_net_report_date_list[$global_c_no][$net_report['key_date']][$net_report['sales_date']] = $net_report['percent'];
        }
    }

    foreach($global_net_report_date_list as $global_c_no => $global_net_company_data)
    {
        $global_cal_net_percent = $global_net_parent_percent_list[$global_c_no];

        foreach($global_net_company_data as $global_key_date => $global_net_report_data)
        {
            foreach($global_net_report_data as $global_sales_date => $global_net_percent_val)
            {
                if($global_net_percent_val > 0){
                    $global_cal_net_percent = $global_net_percent_val;
                }

                $global_net_percent = $global_cal_net_percent/100;
                $global_sales_total = round($global_sales_tmp_total_list[$global_c_no][$global_key_date][$global_sales_date]);
                $global_cost_total  = $global_cost_tmp_total_list[$global_c_no][$global_key_date][$global_sales_date];
                $global_net_price   = $global_sales_total > 0 ? (int)($global_sales_total*$global_net_percent) : 0;
                $global_profit      = $global_cost_total > 0 ? ($global_net_price-$global_cost_total) : $global_net_price;

                $global_report_table_list[$global_key_date]['sales']     += $global_sales_total;
                $global_report_table_list[$global_key_date]['cost']      += $global_cost_total;
                $global_report_table_list[$global_key_date]['profit']    += $global_profit;
            }
        }
    }

    foreach($global_report_table_list as $key_date => $date_data)
    {
        $global_date_chart_list[0]['title']    = "매출";
        $global_date_chart_list[0]['type']     = "bar";
        $global_date_chart_list[0]['color']    = "rgba(0,0,255,0.8)";
        $global_date_chart_list[0]['data'][]   = $date_data['sales'];
        $global_date_chart_list[1]['title']     = "비용";
        $global_date_chart_list[1]['type']      = "bar";
        $global_date_chart_list[1]['color']     = "rgba(255,0,0,0.8)";
        $global_date_chart_list[1]['data'][]    = $date_data['cost'];
        $global_date_chart_list[2]['title']   = "공헌이익";
        $global_date_chart_list[2]['type']    = "line";
        $global_date_chart_list[2]['color']   = "rgba(0,0,0,0.8)";
        $global_date_chart_list[2]['data'][]  = $date_data['profit'];
    }
}

$smarty->assign("sch_date_type", $sch_date_type);
$smarty->assign("sch_brand_url", $sch_brand_url);
$smarty->assign("global_base_option", $global_base_option);
$smarty->assign("commerce_date_list", $commerce_date_list);
$smarty->assign("commerce_link_date_list", $commerce_link_date_list);
$smarty->assign("commerce_date_cnt", count($commerce_date_list));
$smarty->assign("total_report_table_list", $total_report_table_list);
$smarty->assign("self_report_table_list", $self_report_table_list);
$smarty->assign("global_report_table_list", $global_report_table_list);
$smarty->assign("nosp_result_total", $nosp_result_total);
$smarty->assign("nosp_result_list", $nosp_result_list);
$smarty->assign("date_chart_name_list", json_encode($date_chart_name_list));
$smarty->assign("total_date_chart_list", json_encode($total_date_chart_list));
$smarty->assign("global_date_chart_list", json_encode($global_date_chart_list));

$smarty->display('wise_bm/brand_main_hose_iframe_cms_report.html');

?>