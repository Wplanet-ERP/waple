<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_date.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');
require('../inc/helper/commerce_sales.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 검색조건
$bm_brand_option    = getBrandDetailOption();
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sel_brand_data     = isset($bm_brand_option[$sch_brand]) ? $bm_brand_option[$sch_brand] : "";

if(empty($sel_brand_data)){
    exit("<script>alert('접근 권한이 없습니다.');location.href='/v1/main.php';</script>");
}

# 브랜드 변수
$bm_type_name       = $sel_brand_data['title'];
$brand_manager      = $sel_brand_data['manager'];
$brand_manager_name = $sel_brand_data['manager_name'];
$cms_brand_option   = $sel_brand_data['brand_list'];
$sch_brand_url      = $sel_brand_data['brand_url'];
$cms_brand_text     = implode(",", $cms_brand_option);
$main_log_company   = $sel_brand_data['log_company'];
$main_brand_list    = $sel_brand_data['main_brand'];
$main_brand_text    = implode(",",$sel_brand_data['main_brand']);
$main_brand         = $main_brand_list[0];
$dp_except_list     = getNotApplyDpList();
$dp_except_text     = implode(",", $dp_except_list);

# 날짜 체크
$today_s_w		    = date('w')-1;
$sch_e_date         = date('Y-m-d');
$sch_s_date         = date('Y-m-d', strtotime("{$sch_e_date} -14 days"));
$today_s_week       = date('Y-m-d', strtotime("-{$today_s_w} days"));
$sch_s_week         = date('Y-m-d', strtotime("{$today_s_week} -10 weeks"));
$sch_e_week         = date("Y-m-d",strtotime("{$today_s_week} +6 days"));
$sch_e_month        = date('Y-m', strtotime($sch_e_date));
$sch_s_month        = date('Y-m', strtotime("{$sch_e_month} -6 months"));

$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "week";
$sch_s_datetime     = "";
$sch_e_datetime     = "";
$sch_net_date       = "";

$add_comm_where     = "display='1' AND brand IN ({$main_brand_text})";
$add_cms_where      = "w.delivery_state='4' AND w.dp_c_no NOT IN({$dp_except_text}) AND w.c_no IN({$cms_brand_text}) AND w.log_c_no IN({$main_log_company})";
$add_net_where      = "1=1";
$add_comm_column    = "";
$add_cms_column     = "";
$add_net_column     = "";

# 날짜 검색
if($sch_date_type == "month")
{
    $all_date_where         = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	        = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title         = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_comm_where        .= " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_comm_column        = "DATE_FORMAT(sales_date, '%Y%m')";

    $add_net_where         .= " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_net_column         = "DATE_FORMAT(sales_date, '%Y%m')";
    $add_cms_column         = "DATE_FORMAT(order_date, '%Y%m')";

    $sch_e_day              = date("t", strtotime($sch_e_month));
    $sch_s_datetime         = $sch_s_month."-01 00:00:00";
    $sch_e_datetime         = $sch_e_month."-{$sch_e_day} 23:59:59";;

    $sch_net_date           = $sch_s_month."-01";
}
elseif($sch_date_type == "week")
{
    $all_date_where         = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	        = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title         = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_comm_where        .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_comm_column        = "DATE_FORMAT(DATE_SUB(sales_date, INTERVAL(IF(DAYOFWEEK(sales_date)=1,8,DAYOFWEEK(sales_date))-2) DAY), '%Y%m%d')";

    $add_net_where         .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_net_column         = "DATE_FORMAT(DATE_SUB(sales_date, INTERVAL(IF(DAYOFWEEK(sales_date)=1,8,DAYOFWEEK(sales_date))-2) DAY), '%Y%m%d')";
    $add_cms_column         = "DATE_FORMAT(DATE_SUB(order_date, INTERVAL(IF(DAYOFWEEK(order_date)=1,8,DAYOFWEEK(order_date))-2) DAY), '%Y%m%d')";

    $sch_s_datetime         = $sch_s_week." 00:00:00";
    $sch_e_datetime         = $sch_e_week." 23:59:59";

    $sch_net_date           = $sch_s_week;
}
else
{
    $all_date_where         = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	        = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title         = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_comm_where        .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_comm_column        = "DATE_FORMAT(sales_date, '%Y%m%d')";

    $add_net_where         .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_net_column         = "DATE_FORMAT(sales_date, '%Y%m%d')";
    $add_cms_column         = "DATE_FORMAT(order_date, '%Y%m%d')";

    $sch_s_datetime         = $sch_s_date." 00:00:00";
    $sch_e_datetime         = $sch_e_date." 23:59:59";

    $sch_net_date           = $sch_s_date;
}

# 날짜 정리 및 리스트 Init
$commerce_date_list             = [];
$commerce_link_date_list        = [];
$sales_tmp_total_list           = [];
$cost_tmp_total_list            = [];
$net_report_date_list           = [];
$net_parent_percent_list        = [];
$total_all_date_list            = [];
$total_report_table_list        = [];
$total_date_chart_list          = [];

$date_name_option   = getDateChartOption();;

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($date = mysqli_fetch_array($all_date_query))
{
    $total_all_date_list[$date['date_key']] = array("s_date" => date("Y-m-d",strtotime($date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($date['date_key']))." 23:59:59");

    $chart_title = $date['chart_title'];
    if($sch_date_type == 'date'){
        $date_convert   = explode('_', $chart_title);
        $date_name      = isset($date_convert[1]) ? $date_name_option[$date_convert[1]] : "";
        $chart_title    = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
    }

    foreach($main_brand_list as $c_no)
    {
        $cost_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']]  = 0;
        $sales_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
        $net_report_date_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
        $net_parent_percent_list[$c_no] = 0;
    }

    if(!isset($commerce_date_list[$date['chart_key']]))
    {
        $commerce_date_list[$date['chart_key']]     = $chart_title;

        $total_report_table_list[$date['chart_key']] = array(
            "sales"     => 0,
            "cost"      => 0,
            "profit"    => 0,
            "net_price" => 0,
            "roas"      => 0,
            "profit_per"=> 0,
        );

        if($sch_date_type == "month"){
            $link_month     = date("Y-m", strtotime($date['date_key']));
            $link_e_day     = date("t", strtotime($link_month));
            $link_s_date    = "{$link_month}-01";
            $link_e_date    = "{$link_month}-{$link_e_day}";
            $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
        }
        elseif($sch_date_type == "week"){
            $link_s_date    = date("Y-m-d", strtotime($date['date_key']));
            $link_s_w       = date('w', strtotime($link_s_date));
            $link_e_w       = 7-$link_s_w;
            $link_e_date    = date("Y-m-d", strtotime("{$link_s_date} +{$link_e_w} days"));
            $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
        }
        else{
            $link_s_date    = date("Y-m-d", strtotime($date['date_key']));
            $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_s_date);
        }
    }
}

# 수기 데이터
$commerce_sql  = "
    SELECT
        cr.`type`,
        cr.price as price,
        cr.brand,
        DATE_FORMAT(sales_date, '%Y%m%d') as sales_date,
        {$add_comm_column} as key_date
    FROM commerce_report `cr`
    WHERE {$add_comm_where}
    ORDER BY key_date ASC
";
$commerce_query = mysqli_query($my_db, $commerce_sql);
while($commerce = mysqli_fetch_assoc($commerce_query))
{
    if($commerce['type'] == 'cost'){
        $cost_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
    }elseif($commerce['type'] == 'sales'){
        $sales_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
    }
}

# 배송리스트 데이터
$commerce_fee_option         = getCommerceFeeOption();
$cms_delivery_chk_fee_list   = [];
$cms_delivery_chk_nuzam_list = [];
$cms_delivery_chk_ord_list   = [];
$cms_delivery_chk_list       = [];

$global_cms_delivery_chk_fee_list   = [];
$global_cms_delivery_chk_ord_list   = [];
$global_cms_delivery_chk_list       = [];

foreach($total_all_date_list as $date_data)
{
    $cms_report_sql      = "
        SELECT
            c_no,
            dp_c_no,
            order_number,
            DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
            {$add_cms_column} as key_date, 
            unit_price,
            (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
            unit_delivery_price,
            (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
        FROM work_cms as w
        WHERE {$add_cms_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}') 
    ";
    $cms_report_query = mysqli_query($my_db, $cms_report_sql);
    while($cms_report = mysqli_fetch_assoc($cms_report_query))
    {
        $cms_report['c_no'] = $main_brand;
        $total_price        = $cms_report['unit_price'];

        $sales_tmp_total_list[$cms_report['c_no']][$cms_report['key_date']][$cms_report['sales_date']] += $total_price;

        if($cms_report['sales_date'] >= 20240101)
        {
            if ($cms_report['unit_delivery_price'] > 0) {
                $cms_delivery_chk_fee_list[$cms_report['order_number']] = 1;
            }
            elseif($cms_report['unit_delivery_price'] == 0 && $cms_report['unit_price'] > 0)
            {
                $cms_delivery_chk_list[$cms_report['order_number']][$cms_report['dp_c_no']][$cms_report['key_date']][$cms_report['sales_date']] = $cms_report['deli_cnt'];
                $cms_delivery_chk_ord_list[$cms_report['order_number']][$cms_report['c_no']] = $cms_report['c_no'];
            }
        }
    }
}

# 무료 배송비 계산
foreach($cms_delivery_chk_list as $chk_ord => $chk_ord_data)
{
    if(isset($cms_delivery_chk_fee_list[$chk_ord])){
        continue;
    }

    $chk_delivery_price = 6000;

    foreach ($chk_ord_data as $chk_dp_c_no => $chk_dp_data)
    {
        foreach ($chk_dp_data as $chk_key_date => $chk_key_data)
        {
            foreach ($chk_key_data as $chk_sales_date => $deli_cnt)
            {
                $cal_delivery_price     = $chk_delivery_price * $deli_cnt;

                $chk_brand_deli_price   = $cal_delivery_price;
                $chk_brand_cnt          = count($cms_delivery_chk_ord_list[$chk_ord]);
                $cal_brand_deli_price   = round($cal_delivery_price/$chk_brand_cnt);
                $cal_brand_idx          = 1;
                foreach($cms_delivery_chk_ord_list[$chk_ord] as $chk_c_no)
                {
                    $cal_one_delivery_price  = $cal_brand_deli_price;
                    $chk_brand_deli_price   -= $cal_one_delivery_price;

                    if($cal_brand_idx == $chk_brand_cnt){
                        $cal_one_delivery_price += $chk_brand_deli_price;
                    }

                    $cost_tmp_total_list[$chk_c_no][$chk_key_date][$chk_sales_date] += $cal_one_delivery_price;

                    $cal_brand_idx++;
                }
            }
        }
    }
}

# NET 매출 관련 작업
foreach($main_brand_list as $c_no)
{
    $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$sch_net_date}' ORDER BY sales_date DESC LIMIT 1;";
    $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
    $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
    $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
    $net_parent             = isset($net_pre_percent_result['sales_date']) ? $net_pre_percent_result['sales_date'] : 0;

    $net_parent_percent_list[$c_no] = $net_pre_percent;

    $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date, {$add_net_column} as key_date FROM commerce_report_net WHERE {$add_net_where} AND brand='{$c_no}' ORDER BY sales_date ASC";
    $net_report_query       = mysqli_query($my_db, $net_report_sql);
    while($net_report = mysqli_fetch_assoc($net_report_query))
    {
        $net_report_date_list[$c_no][$net_report['key_date']][$net_report['sales_date']] = $net_report['percent'];
    }
}

foreach($net_report_date_list as $c_no => $net_company_data)
{
    $cal_net_percent = $net_parent_percent_list[$c_no];

    foreach($net_company_data as $key_date => $net_report_data)
    {
        foreach($net_report_data as $sales_date => $net_percent_val)
        {
            if($net_percent_val > 0 ){
                $cal_net_percent = $net_percent_val;
            }

            $net_percent = $cal_net_percent/100;
            $sales_total = round($sales_tmp_total_list[$c_no][$key_date][$sales_date]);
            $cost_total  = round($cost_tmp_total_list[$c_no][$key_date][$sales_date]);
            $net_price   = $sales_total > 0 ? (int)($sales_total*$net_percent) : 0;
            $profit      = $cost_total > 0 ? ($net_price-$cost_total) : $net_price;

            $total_report_table_list[$key_date]['sales']     += $sales_total;
            $total_report_table_list[$key_date]['cost']      += $cost_total;
            $total_report_table_list[$key_date]['profit']    += $profit;
            $total_report_table_list[$key_date]['net_price'] += $net_price;
        }
    }
}

$date_chart_name_list  = $commerce_date_list;
foreach($total_report_table_list as $key_date => $date_data)
{
    $total_report_table_list[$key_date]['roas']         = ($date_data['cost'] > 0) ? ($date_data['sales']/$date_data['cost'])*100 : 0;
    $total_report_table_list[$key_date]['profit_rate']  = ($date_data['sales'] > 0) ? ($date_data['profit']/$date_data['sales'])*100 : 0;

    $total_date_chart_list[0]['title']  = "매출";
    $total_date_chart_list[0]['type']   = "bar";
    $total_date_chart_list[0]['color']  = "rgba(0,0,255,0.8)";
    $total_date_chart_list[0]['data'][] = $date_data['sales'];
    $total_date_chart_list[1]['title']  = "비용";
    $total_date_chart_list[1]['type']   = "bar";
    $total_date_chart_list[1]['color']  = "rgba(255,0,0,0.8)";
    $total_date_chart_list[1]['data'][] = $date_data['cost'];
    $total_date_chart_list[2]['title']  = "공헌이익";
    $total_date_chart_list[2]['type']   = "line";
    $total_date_chart_list[2]['color']  = "rgba(0,0,0,0.8)";
    $total_date_chart_list[2]['data'][] = $date_data['profit'];
}

$smarty->assign("sch_date_type", $sch_date_type);
$smarty->assign("bm_type_name", $bm_type_name);
$smarty->assign("sch_brand_url", $sch_brand_url);
$smarty->assign("commerce_date_list", $commerce_date_list);
$smarty->assign("commerce_date_cnt", count($commerce_date_list));
$smarty->assign("commerce_link_date_list", $commerce_link_date_list);
$smarty->assign("total_report_table_list", $total_report_table_list);
$smarty->assign("date_chart_name_list", json_encode($date_chart_name_list));
$smarty->assign("total_date_chart_list", json_encode($total_date_chart_list));

$smarty->display('wise_bm/brand_main_global_iframe_cms_report.html');
?>
