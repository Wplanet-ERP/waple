<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');
require('../inc/model/Company.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 검색조건
$bm_brand_option    = getBrandDetailOption();
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sel_brand_data     = isset($bm_brand_option[$sch_brand]) ? $bm_brand_option[$sch_brand] : "";

if(empty($sel_brand_data)){
    exit("<script>alert('접근 권한이 없습니다.');location.href='/v1/main.php';</script>");
}

# 브랜드 변수
$main_brand_option  = $sel_brand_data['brand_list'];
$main_brand_text    = implode(",", $main_brand_option);
$dp_except_list     = getNotApplyDpList();
$dp_except_text     = implode(",", $dp_except_list);
$sch_brand_url      = $sel_brand_data['brand_url'];

# 커머스 단품 상품별 정산률
$company_model          = Company::Factory();
$prev_month             = date('Y-m', strtotime("-1 months"));
$prev_month_e_day       = date('t', strtotime($prev_month));
$cms_settle_s_date      = "{$prev_month}-01";
$cms_settle_s_datetime  = "{$cms_settle_s_date} 00:00:00";
$cms_settle_e_date      = "{$prev_month}-{$prev_month_e_day}";
$cms_settle_e_datetime  = "{$cms_settle_e_date} 23:59:59";

$cms_settle_dp_list     = $company_model->getBrandSettleDpCompanyList($main_brand_text, $cms_settle_s_date, $cms_settle_e_date);
$cms_settle_dp_c_no     = array_rand($cms_settle_dp_list, 1);
$cms_settle_dp_c_name   = isset($cms_settle_dp_list[$cms_settle_dp_c_no]) ? $cms_settle_dp_list[$cms_settle_dp_c_no] : "";
$cms_settle_where       = "w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no = '{$cms_settle_dp_c_no}' AND w.c_no IN({$main_brand_text}) AND order_date BETWEEN '{$cms_settle_s_datetime}' AND '{$cms_settle_e_datetime}'";
$cms_settle_group_where = "w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no = '{$cms_settle_dp_c_no}' AND order_date BETWEEN '{$cms_settle_s_datetime}' AND '{$cms_settle_e_datetime}'";
$cms_settle_prd_list    = [];

$cms_settle_prd_sql     = "
    SELECT
        rs.c_no,
        rs.prd_no,
        (SELECT p.title FROM product_cms p WHERE p.prd_no=rs.prd_no) AS prd_name,
        ROUND(AVG(rs.unit_sales_price/rs.quantity)) AS avg_sales,
        ROUND(AVG(rs.settle_price/rs.quantity)) AS avg_settle,
        (SUM(rs.settle_price)/SUM(rs.unit_sales_price)) AS avg_per,
        MIN(rs.settle_price) AS min_settle
    FROM
    (    
        SELECT
            w.c_no,
            w.prd_no,
            quantity,
            (unit_price+unit_delivery_price) AS unit_sales_price,
            (SELECT SUM(wcs.settle_price_vat) FROM work_cms_settlement wcs WHERE wcs.order_number=w.order_number) AS settle_price
        FROM work_cms w 
        WHERE {$cms_settle_where} AND w.order_number IN(
            SELECT 
                DISTINCT order_number 
            FROM work_cms w
            WHERE {$cms_settle_group_where} 
            GROUP BY order_number HAVING COUNT(*) = 1
        ) AND order_number NOT IN(SELECT DISTINCT wcs.order_number FROM work_cms_settlement wcs WHERE wcs.order_number=w.order_number AND wcs.settle_price_vat < 0)
        GROUP BY order_number
    ) AS rs
    GROUP BY prd_no
    ORDER BY min_settle ASC LIMIT 10
";
$cms_settle_prd_query = mysqli_query($my_db, $cms_settle_prd_sql);
while($cms_settle_prd = mysqli_fetch_assoc($cms_settle_prd_query))
{
    $cms_settle_prd['dp_c_name']        = $cms_settle_dp_c_name;
    $cms_settle_prd['total_avg_per']    = round($cms_settle_prd['avg_per']*100, 2);
    $cms_settle_prd_list[] = $cms_settle_prd;
}

$smarty->assign("sch_brand_url", $sch_brand_url);
$smarty->assign("cms_settle_dp_c_no", $cms_settle_dp_c_no);
$smarty->assign("cms_settle_s_date", $cms_settle_s_date);
$smarty->assign("cms_settle_e_date", $cms_settle_e_date);
$smarty->assign("cms_settle_prd_list", $cms_settle_prd_list);

$smarty->display('wise_bm/brand_main_base_iframe_cms_settle.html');
?>
