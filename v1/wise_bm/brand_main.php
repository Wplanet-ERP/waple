<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_date.php');
require('../inc/helper/advertising.php');
require('../inc/helper/product_cms.php');
require('../inc/helper/logistics.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');
require('../inc/model/MyQuick.php');
require('../inc/model/Company.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

$bm_brand_option    = getBrandDetailOption();
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sel_brand_data     = isset($bm_brand_option[$sch_brand]) ? $bm_brand_option[$sch_brand] : [];
$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "date";

if(empty($sel_brand_data)){
    exit("<script>alert('접근 권한이 없습니다.');location.href='/v1/main.php';</script>");
}

# Navigation & My Quick
$nav_prd_no     = $sel_brand_data['nav_no'];
$bm_type_name   = $sel_brand_data['title'];
$nav_title      = "WISE BM :: {$bm_type_name}";
$quick_model    = MyQuick::Factory();
$is_my_quick    = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_date_type", $sch_date_type);
$smarty->assign("bm_type_name", $bm_type_name);
$smarty->assign("bm_date_type_option", getBMDateTypeOption());
$smarty->assign("traffic_date_type_option", array("day" => "일별", "week" => "주별"));

# 브랜드 변수
$brand_manager      = $sel_brand_data['manager'];
$brand_manager_name = $sel_brand_data['manager_name'];
$main_brand_option  = $sel_brand_data['brand_list'];
$sch_brand_url      = $sel_brand_data['brand_url'];
$main_brand_text    = implode(",", $main_brand_option);
$dp_except_list     = getNotApplyDpList();
$dp_except_text     = implode(",", $dp_except_list);
$dp_self_imweb_list = getSelfDpImwebCompanyList();
$dp_self_imweb_text = implode(",", $dp_self_imweb_list);

$smarty->assign("brand_manager", $brand_manager);
$smarty->assign("brand_manager_name", $brand_manager_name);
$smarty->assign("sch_brand_url", $sch_brand_url);

# 사용 변수
$company_model      = Company::Factory();
$dp_company_list    = $company_model->getDpList();
$date_name_option   = getDateChartOption();
$daily_hour_option  = getHourOption();

# 기본 날짜 설정
$cur_date       = date("Y-m-d");
$prev_date      = date('Y-m-d', strtotime("-1 days"));
$prev_mon_date  = date('Y-m-d', strtotime("-1 months"));
$prev_month     = date('Y-m', strtotime("-1 months"));

# 요일/시간별 판매실적 현황/비교
$daily_s_date               = date("Y-m-d", strtotime("{$prev_date} -6 days"));
$daily_e_date               = $prev_date;
$daily_s_datetime           = "{$daily_s_date} 00:00:00";
$daily_e_datetime           = "{$daily_e_date} 23:59:59";
$daily_s_date_tmp           = date("m/d_w", strtotime($daily_s_date));
$daily_e_date_tmp           = date("m/d_w", strtotime($daily_e_date));

$daily_s_date_convert       = explode('_', $daily_s_date_tmp);
$daily_s_date_name          = $date_name_option[$daily_s_date_convert[1]];
$daily_s_date_text          = $daily_s_date_convert[0]."({$daily_s_date_name})";
$daily_e_date_convert       = explode('_', $daily_e_date_tmp);
$daily_e_date_name          = $date_name_option[$daily_e_date_convert[1]];
$daily_e_date_text          = $daily_e_date_convert[0]."({$daily_e_date_name})";

$prev_daily_s_date          = date("Y-m-d", strtotime("{$daily_s_date} -1 weeks"));
$prev_daily_e_date          = date("Y-m-d", strtotime("{$daily_e_date} -1 weeks"));
$prev_daily_s_datetime      = "{$prev_daily_s_date} 00:00:00";
$prev_daily_e_datetime      = "{$prev_daily_e_date} 23:59:59";
$prev_daily_s_date_tmp      = date("m/d_w", strtotime($prev_daily_s_date));
$prev_daily_e_date_tmp      = date("m/d_w", strtotime($prev_daily_e_date));

$prev_daily_s_date_convert  = explode('_', $prev_daily_s_date_tmp);
$prev_daily_s_date_name     = $date_name_option[$prev_daily_s_date_convert[1]];
$prev_daily_s_date_text     = $prev_daily_s_date_convert[0]."({$prev_daily_s_date_name})";
$prev_daily_e_date_convert  = explode('_', $prev_daily_e_date_tmp);
$prev_daily_e_date_name     = $date_name_option[$prev_daily_e_date_convert[1]];
$prev_daily_e_date_text     = $prev_daily_e_date_convert[0]."({$prev_daily_e_date_name})";

$daily_date_list            = [];
$daily_hour_list            = [];
$daily_sales_list           = [];
$daily_date_total_list      = [];
$daily_hour_total_list      = array("max" => 0);
$daily_sales_total_price    = 0;

$sales_term_hour_list       = [];
$sales_term_date_list       = [];
$sales_term_total_list      = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0, "hour_base_max" => 0, "hour_comp_max" => 0, "date_base_max" => 0, "date_comp_max" => 0, "date_base_min" => 0, "date_comp_min" => 0);
$sales_term_hour_total_list = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0);

foreach($daily_hour_option as $hour){
    $hour_key = (int)$hour;
    $daily_hour_list[$hour_key]     = "{$hour}시";
    $sales_term_hour_list[$hour_key]= array("base_price" => 0,"comp_price" => 0, "comp_per" => 0);
}

foreach($date_name_option as $date_key => $date_title)
{
    $daily_date_list[$date_key]         = $date_title;
    $daily_sales_list[$date_key]['max'] = 0;
    $daily_date_total_list[$date_key]   = 0;
    $sales_term_date_list[$date_key]    = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0);

    foreach($daily_hour_list as $hour_key){
        $daily_sales_list[$date_key][$hour_key] = 0;
        $daily_hour_total_list[$hour_key]       = 0;
    }
}

$add_sales_daily_where  = "w.page_idx > 0 AND w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN({$dp_self_imweb_text}) AND (order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}') AND w.c_no IN({$main_brand_text})";
$add_sales_term_where   = "w.page_idx > 0 AND w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN({$dp_self_imweb_text}) AND (order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}' OR order_date BETWEEN '{$prev_daily_s_datetime}' AND '{$prev_daily_e_datetime}')  AND w.c_no IN({$main_brand_text})";

# 요일/시간별 판매실적 현황 쿼리
$daily_sales_sql  = "
        SELECT
            DATE_FORMAT(w.order_date, '%w') as date_key,
            DATE_FORMAT(w.order_date, '%H') as hour_key,
            SUM(unit_price) as total_price
        FROM work_cms w 
        WHERE {$add_sales_daily_where}
        GROUP BY date_key, hour_key
    ";
$daily_sales_query    = mysqli_query($my_db, $daily_sales_sql);
while($daily_sales_result = mysqli_fetch_assoc($daily_sales_query))
{
    $daily_date_key     = $daily_sales_result['date_key'];
    $daily_hour_key     = (int)$daily_sales_result['hour_key'];
    $daily_total_price  = (int)$daily_sales_result['total_price'];
    $daily_max          = $daily_sales_list[$daily_date_key]["max"];

    $daily_date_total_list[$daily_date_key] += $daily_total_price;
    $daily_hour_total_list[$daily_hour_key] += $daily_total_price;
    $daily_sales_total_price                += $daily_total_price;

    $daily_sales_list[$daily_date_key][$daily_hour_key] += $daily_total_price;
    if($daily_max < $daily_total_price){
        $daily_sales_list[$daily_date_key]["max"] = $daily_total_price;
    }
}

$daily_date_total_list["min"] = min($daily_date_total_list);
$daily_date_total_list["max"] = max($daily_date_total_list);
$daily_hour_total_list["max"] = max($daily_hour_total_list);

# 요일/시간별 판매실적 비교 쿼리
$term_sales_sql    = "

        SELECT    
            DATE_FORMAT(w.order_date, '%w') as date_key,
            DATE_FORMAT(w.order_date, '%H') as hour_key,
            SUM(
                IF(order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}',unit_price, 0)
            ) AS base_price,
            SUM(
                IF(order_date BETWEEN '{$prev_daily_s_datetime}' AND '{$prev_daily_e_datetime}',unit_price, 0)
            ) AS comp_price
        FROM work_cms w 
        WHERE {$add_sales_term_where}
        GROUP BY date_key, hour_key
        ORDER BY order_date
    ";
$term_sales_query = mysqli_query($my_db, $term_sales_sql);
while($term_sales = mysqli_fetch_assoc($term_sales_query))
{
    $hour_key   = (int)$term_sales['hour_key'];
    $date_key   = (int)$term_sales['date_key'];
    $base_price = $term_sales['base_price'];
    $comp_price = $term_sales['comp_price'];

    $sales_term_hour_list[$hour_key]["base_price"] += $base_price;
    $sales_term_hour_list[$hour_key]["comp_price"] += $comp_price;
    $sales_term_date_list[$date_key]["base_price"] += $base_price;
    $sales_term_date_list[$date_key]["comp_price"] += $comp_price;

    $sales_term_total_list["base_price"] += $base_price;
    $sales_term_total_list["comp_price"] += $comp_price;
}

if(!empty($sales_term_total_list)){
    $total_comp_per = ($sales_term_total_list["comp_price"] == 0) ? 0 : (($sales_term_total_list["base_price"] == 0) ? -100 : ROUND( ((($sales_term_total_list["base_price"]-$sales_term_total_list["comp_price"])/$sales_term_total_list["comp_price"]) *100), 1));
    $sales_term_total_list["comp_per"] = $total_comp_per;
}

if(!empty($sales_term_hour_list))
{
    foreach($sales_term_hour_list as $hour_key => $hour_data)
    {
        $hour_base_price    = $hour_data["base_price"];
        $hour_comp_price    = $hour_data["comp_price"];
        $hour_base_max      = $sales_term_total_list["hour_base_max"];
        $hour_comp_max      = $sales_term_total_list["hour_comp_max"];

        $hour_comp_per = ($hour_comp_price == 0) ? 0 : (($hour_base_price == 0) ? -100 : ROUND( ((($hour_base_price-$hour_comp_price)/$hour_comp_price) *100), 1));
        $sales_term_hour_list[$hour_key]["comp_per"] = $hour_comp_per;

        if($hour_base_max < $hour_base_price){
            $sales_term_total_list["hour_base_max"] = $hour_base_price;
        }

        if($hour_comp_max < $hour_comp_price){
            $sales_term_total_list["hour_comp_max"] = $hour_comp_price;
        }
    }
}

if(!empty($sales_term_date_list))
{
    $date_idx = 0;
    foreach($sales_term_date_list as $date_key => $date_data)
    {
        $date_base_price    = $date_data["base_price"];
        $date_comp_price    = $date_data["comp_price"];
        $date_base_max      = $sales_term_total_list["date_base_max"];
        $date_base_min      = $sales_term_total_list["date_base_min"];
        $date_comp_max      = $sales_term_total_list["date_comp_max"];
        $date_comp_min      = $sales_term_total_list["date_comp_min"];

        if($date_idx == 0){
            $sales_term_total_list["date_base_min"] = $date_base_price;
            $sales_term_total_list["date_comp_min"] = $date_comp_price;
        }

        $date_comp_per = ($date_comp_price == 0) ? 0 : (($date_base_price == 0) ? -100 : ROUND( ((($date_base_price-$date_comp_price)/$date_comp_price) *100), 1));
        $sales_term_date_list[$date_key]["comp_per"] = $date_comp_per;

        if($date_base_max < $date_base_price){
            $sales_term_total_list["date_base_max"] = $date_base_price;
        }

        if($date_base_min > $date_base_price){
            $sales_term_total_list["date_base_min"] = $date_base_price;
        }

        if($date_comp_max < $date_comp_price){
            $sales_term_total_list["date_comp_max"] = $date_comp_price;
        }

        if($date_comp_min > $date_comp_price){
            $sales_term_total_list["date_comp_min"] = $date_comp_price;
        }

        $date_idx++;
    }
}

# NOSP 광고관리
$nosp_product_option    = getAdvertisingProductOption();
$nosp_state_option      = array("3" => array("title" => "입찰확정", "cnt" => 0), "1" => array("title" => "신청중", "cnt" => 0), "2" => array("title" => "신청가능 구좌", "cnt" => 0));
$nosp_manage_where      = "am.am_no IN (SELECT `aa`.am_no FROM advertising_application `aa` WHERE `aa`.s_no = '{$brand_manager}')";

$nosp_manage_count_sql  = "
    SELECT 
       `am`.state, 
        COUNT(`am`.am_no) as total_cnt,
        SUM((SELECT COUNT(DISTINCT s_no) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no AND `aa`.s_no='{$brand_manager}')) AS app_cnt
    FROM advertising_management `am`
    WHERE `am`.state IN(1,3)
    GROUP BY `am`.`state`
";
$nosp_manage_count_query = mysqli_query($my_db, $nosp_manage_count_sql);
while($nosp_manage_count = mysqli_fetch_assoc($nosp_manage_count_query)){

    if($nosp_manage_count['state'] == "1"){
        $nosp_state_option[$nosp_manage_count['state']]['cnt'] = $nosp_manage_count['app_cnt'];
        $nosp_state_option["2"]['cnt'] = $nosp_manage_count['total_cnt']-$nosp_manage_count['app_cnt'];
    }else{
        $nosp_state_option[$nosp_manage_count['state']]['cnt'] = $nosp_manage_count['app_cnt'];
    }
}

$nosp_manage_confirm_list   = [];
$nosp_manage_confirm_sql    = "
    SELECT
        `am`.state,
        `am`.product,
        `am`.type,
        `am`.price,
        `am`.adv_s_date,
        `am`.adv_e_date,
        DATE_FORMAT(`am`.adv_s_date, '%m/%d') as adv_s_day,
        DATE_FORMAT(`am`.adv_s_date, '%H:%i') as adv_s_time,
        DATE_FORMAT(`am`.adv_e_date, '%m/%d') as adv_e_day,
        DATE_FORMAT(`am`.adv_e_date, '%H:%i') as adv_e_time,
        (SELECT GROUP_CONCAT(`aa`.s_name) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no GROUP BY `aa`.am_no) as req_staff 
    FROM advertising_management `am`
    WHERE {$nosp_manage_where} AND `am`.state='3'
    ORDER BY adv_s_date DESC LIMIT 10
";
$nosp_manage_confirm_query = mysqli_query($my_db, $nosp_manage_confirm_sql);
while($nosp_manage_confirm = mysqli_fetch_assoc($nosp_manage_confirm_query))
{
    $nosp_manage_confirm['state_name']      = "입찰확정";
    $nosp_manage_confirm['product_name']    = $nosp_product_option[$nosp_manage_confirm['product']];
    $nosp_manage_confirm['adv_date_text']   = $nosp_manage_confirm['adv_s_day']." (".$date_name_option[date("w", strtotime($nosp_manage_confirm['adv_s_date']))].") ".$nosp_manage_confirm['adv_s_time'];

    if($nosp_manage_confirm['adv_s_day'] != $nosp_manage_confirm['adv_e_day'] && $nosp_manage_confirm['adv_e_time'] != "00:00"){
        $nosp_manage_confirm['adv_date_text'] .= " ~ ".$nosp_manage_confirm['adv_e_day']." (".$date_name_option[date("w", strtotime($nosp_manage_confirm['adv_e_date']))].") ".$nosp_manage_confirm['adv_e_time'];
    }else{
        $nosp_manage_confirm['adv_date_text'] .= " ~ ".$nosp_manage_confirm['adv_e_time'];
    }

    $nosp_manage_confirm_list[] = $nosp_manage_confirm;
}

$nosp_manage_request_list   = [];
$nosp_manage_request_sql    = "
    SELECT
        `am`.state,
        `am`.product,
        `am`.type,
        `am`.price,
        `am`.adv_s_date,
        `am`.adv_e_date,
        DATE_FORMAT(`am`.adv_s_date, '%m/%d') as adv_s_day,
        DATE_FORMAT(`am`.adv_s_date, '%H:%i') as adv_s_time,
        DATE_FORMAT(`am`.adv_e_date, '%m/%d') as adv_e_day,
        DATE_FORMAT(`am`.adv_e_date, '%H:%i') as adv_e_time,
        (SELECT GROUP_CONCAT(`aa`.s_name) FROM advertising_application AS `aa` WHERE `aa`.am_no=`am`.am_no GROUP BY `aa`.am_no) as req_staff 
    FROM advertising_management `am`
    WHERE {$nosp_manage_where} AND `am`.state='1'
    ORDER BY adv_s_date DESC LIMIT 10
";
$nosp_manage_request_query = mysqli_query($my_db, $nosp_manage_request_sql);
while($nosp_manage_request = mysqli_fetch_assoc($nosp_manage_request_query))
{
    $nosp_manage_request['state_name']      = "입찰확정";
    $nosp_manage_request['product_name']    = $nosp_product_option[$nosp_manage_request['product']];
    $nosp_manage_request['adv_date_text']   = $nosp_manage_request['adv_s_day']." (".$date_name_option[date("w", strtotime($nosp_manage_request['adv_s_date']))].") ".$nosp_manage_request['adv_s_time'];

    if($nosp_manage_request['adv_s_day'] != $nosp_manage_request['adv_e_day'] && $nosp_manage_request['adv_e_time'] != "00:00"){
        $nosp_manage_request['adv_date_text'] .= " ~ ".$nosp_manage_request['adv_e_day']." (".$date_name_option[date("w", strtotime($nosp_manage_request['adv_e_date']))].") ".$nosp_manage_request['adv_e_time'];
    }else{
        $nosp_manage_request['adv_date_text'] .= " ~ ".$nosp_manage_request['adv_e_time'];
    }

    $nosp_manage_request_list[] = $nosp_manage_request;
}

# NOSP 광고 결과보고
$nosp_result_chk_date   = $prev_mon_date;
$nosp_result_where      = "ar.brand IN({$main_brand_text}) AND `am`.state IN(3,5) AND `am`.adv_s_date >= '{$nosp_result_chk_date}'";
$nosp_result_list       = [];
$nosp_result_tmp_list   = [];

$nosp_result_sql = "
    SELECT 
        ar.*,
        am.am_no,
        `am`.adv_s_date,
        am.product,
        am.fee_per,
        am.price,
        DATE_FORMAT(`am`.adv_s_date, '%m/%d') as adv_s_day,
        DATE_FORMAT(`am`.adv_s_date, '%H:%i') as adv_s_time
    FROM advertising_result as `ar`
    LEFT JOIN advertising_management as `am` ON `am`.am_no=`ar`.am_no
    WHERE {$nosp_result_where}
    ORDER BY am.adv_s_date DESC, am.am_no ASC, ar.brand ASC
";
$nosp_result_query = mysqli_query($my_db, $nosp_result_sql);
$nosp_result_chk_count = 0;

while($nosp_result = mysqli_fetch_assoc($nosp_result_query))
{
    if(!isset($nosp_result_list[$nosp_result['am_no']]) && $nosp_result_chk_count < 10)
    {
        $nosp_result_list[$nosp_result['am_no']] = array(
            "prd_name"      => $nosp_product_option[$nosp_result['product']],
            "adv_date"      => $nosp_result['adv_s_day']." (".$date_name_option[date("w", strtotime($nosp_result['adv_s_date']))].") ".$nosp_result['adv_s_time'],
            "brand_data"    => [],
            "total_cnt"     => 0,
        );

        $nosp_result_chk_count++;
    }

    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["brand_name"]         = $nosp_result['brand_name'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["fee_per"]            = $nosp_result['fee_per'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_price"]        = $nosp_result['price'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_impressions"] += $nosp_result['impressions'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_click_cnt"]   += $nosp_result['click_cnt'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_adv_price"]   += $nosp_result['adv_price'];
}

$nosp_result_prev_idx  = 0;
foreach($nosp_result_tmp_list as $am_no => $am_data)
{
    $brand_total_cnt = 0;
    foreach($am_data as $brand => $brand_data)
    {
        $fee_price      = $brand_data['total_adv_price']*($brand_data['fee_per']/100);
        $cal_price      = $brand_data["total_adv_price"]-$fee_price;
        $cal_price_vat  = $cal_price*1.1;

        $nosp_result_list[$am_no]["brand_data"][$brand] = array(
            "brand_name"    => $brand_data['brand_name'],
            "impressions"   => $brand_data["total_impressions"],
            "click_cnt"     => $brand_data["total_click_cnt"],
            "click_per"     => $brand_data["total_click_cnt"]/$brand_data["total_impressions"]*100,
            "adv_price"     => $brand_data["total_adv_price"],
            "fee_price"     => $fee_price,
            "cal_price"     => $cal_price_vat,
        );

        $brand_total_cnt++;
    }

    $nosp_result_list[$am_no]['total_cnt'] = $brand_total_cnt;
    $nosp_result_prev_idx++;

    if($nosp_result_prev_idx == 10){
        break;
    }
}

# NOSP 월별 광고결과 분석 월별처리
$nosp_stats_s_month     = date('Y-m', strtotime("-3 months"));
$nosp_stats_e_month     = date('Y-m');
$nosp_stats_e_day       = date("t", strtotime($nosp_stats_e_month));
$nosp_stats_s_date      = $nosp_stats_s_month."-01";
$nosp_stats_e_date      = $nosp_stats_e_month."-".$nosp_stats_e_day;
$nosp_stats_s_datetime  = $nosp_stats_s_date." 00:00:00";
$nosp_stats_e_datetime  = $nosp_stats_e_date." 23:59:59";
$nosp_stats_where       = "`am`.product IN(1,2) AND ar.brand IN({$main_brand_text}) AND `am`.state IN(3,5) AND (`am`.adv_s_date >= '{$nosp_stats_s_datetime}' AND `am`.adv_s_date <= '{$nosp_stats_e_datetime}')";

$nosp_stats_date_list      = [];
$nosp_stats_time_list      = [];
$nosp_stats_special_list   = [];
$nosp_stats_chart_list     = array(
    array(
        "title" => "광고비",
        "type"  => "bar",
        "color" => "#BFBFFF",
        "data"  => array(),
    ),
    array(
        "title" => "클릭수",
        "type"  => "bar",
        "color" => "#BDBEBD",
        "data"  => array(),
    ),
    array(
        "title" => "CPC",
        "type"  => "line",
        "color" => "#FF0000",
        "data"  => array(),
    )
);
$nosp_stats_time_chart_list     = $nosp_stats_chart_list;
$nosp_stats_special_chart_list  = $nosp_stats_chart_list;

# 마케팃 비용 및 수수료 월정산 월별처리
$agency_e_month     = $prev_month;
$agency_s_month     = date('Y-m', strtotime("{$agency_e_month} -2 months"));
$agency_e_day       = date("t", strtotime($agency_e_month));
$agency_s_date      = $agency_s_month."-01";
$agency_e_date      = $agency_e_month."-".$agency_e_day;
$agency_s_datetime  = $agency_s_date." 00:00:00";
$agency_e_datetime  = $agency_e_date." 23:59:59";
$agency_month_where = "`as`.partner IN({$main_brand_text}) AND `as`.settle_month BETWEEN '{$agency_s_month}' AND '{$agency_e_month}'";

$agency_date_list       = [];
$agency_table_name_list = [];
$agency_table_list      = [];
$agency_table_init      = array("total_cnt" => 0, "total_price" => 0, "total_fee" => 0);
$agency_chart_list      = array(
    array(
        "title" => "광고비",
        "type"  => "bar",
        "color" => "#D99694",
        "data"  => array(),
    ),
    array(
        "title" => "대행수수료",
        "type"  => "bar",
        "color" => "#95B3D7",
        "data"  => array(),
    ),
    array(
        "title" => "횟수",
        "type"  => "line",
        "color" => "#333333",
        "data"  => array(),
    )
);

# 월별 불량사유 통계 월별처리
$csm_bad_s_month        = date('Y-m', strtotime("-5 months"));
$csm_bad_e_month        = $nosp_stats_e_month;
$csm_bad_s_date         = $csm_bad_s_month."-01";
$csm_bad_e_date         = $csm_bad_e_month."-{$nosp_stats_e_day}";
$csm_bad_where          = "wcr.bad_reason != '' AND wcr.return_state NOT IN('7','8') AND wcr.c_no IN({$main_brand_text}) AND wcr.return_req_date BETWEEN '{$csm_bad_s_date}' AND '{$csm_bad_e_date}'";
$csm_bad_date_list      = [];

# 월별 물류비정산 월별처리
$work_track_option      = getTrackOption();
$work_track_e_month     = $prev_month;
$work_track_s_month     = date('Y-m', strtotime("{$work_track_e_month} -3 months"));
$work_track_e_day       = date('t', strtotime("{$work_track_e_month}"));
$work_track_s_datetime  = $work_track_s_month."-01 00:00:00";
$work_track_e_datetime  = $work_track_e_month."-{$work_track_e_day} 23:59:59";
$work_track_where       = "`w`.c_no IN({$main_brand_text}) AND `w`.work_state='6' AND w.prd_no IN('281','270','271','272','273','274','275') AND (w.task_run_regdate BETWEEN '{$work_track_s_datetime}' AND '{$work_track_e_datetime}')";
$work_track_date_list   = [];
$work_track_table_list  = [];
$work_track_total_list  = [];
$work_track_name_list   = array("281" => "배송비", "270" => "보관료", "271" => "적출료", "272" => "합포장", "273" => "화물 입/출고", "274" => "폐기", "275" => "기타");
$work_track_init        = array("281" => 0, "270" => 0, "271" => 0, "272" => 0, "273" => 0, "274" => 0, "275" => 0);

# 커머스 출금 리스트 월별 처리
$withdraw_s_month           = date('Y-m', strtotime("-5 months"));
$withdraw_e_month           = $prev_month;
$withdraw_s_date            = $withdraw_s_month."-01";
$withdraw_e_day             = date("t", strtotime($withdraw_e_month));
$withdraw_e_date            = $withdraw_e_month."-{$withdraw_e_day}";
$withdraw_where             = "w.c_no IN({$main_brand_text}) AND wd.wd_date BETWEEN '{$withdraw_s_date}' AND '{$withdraw_e_date}' AND wd.wd_state='3' AND wd.w_no > 0 AND w.c_no > 0";
$withdraw_init              = [];
$withdraw_month_list        = [];
$withdraw_title_list        = [];
$withdraw_table_list        = [];
$withdraw_chart_list        = [];
$withdraw_total_list        = [];
$withdraw_month_total_list  = [];

$withdraw_init_sql          = "SELECT w.prd_no, (SELECT p.title FROM product p WHERE p.prd_no=w.prd_no) as prd_name FROM withdraw wd LEFT JOIN `work` as w ON w.w_no=wd.w_no WHERE {$withdraw_where} GROUP BY w.prd_no";
$withdraw_init_query        = mysqli_query($my_db, $withdraw_init_sql);
while($withdraw_init_result = mysqli_fetch_assoc($withdraw_init_query)){
    $withdraw_init[$withdraw_init_result['prd_no']]         = 0;
    $withdraw_title_list[$withdraw_init_result['prd_no']]   = $withdraw_init_result['prd_name'];
    $withdraw_total_list[$withdraw_init_result['prd_no']]   = 0;
}

# 월별 처리
$month_date_sql = "
	SELECT
 		DATE_FORMAT(`allday`.Date, '%m') as chart_title,
 		DATE_FORMAT(`allday`.Date, '%Y%m') as chart_key,
	    DATE_FORMAT(`allday`.Date, '%Y-%m') as chart_name,
	    DATE_FORMAT(`allday`.Date, '%Y/%m') as chart_other_name,
	    DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key,
        DATE_FORMAT(`allday`.Date, '%Y-%m-%d') as date_name
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$csm_bad_s_month}' AND '{$csm_bad_e_month}'
	ORDER BY chart_key, date_key
";
$month_date_query = mysqli_query($my_db, $month_date_sql);
while($month_date = mysqli_fetch_assoc($month_date_query))
{
    if($month_date['chart_name'] >= $nosp_stats_s_month) {
        $nosp_stats_date_list[$month_date['chart_key']]     = $month_date['chart_title']."월";
        $nosp_stats_time_list[$month_date['chart_key']]     = array("adv_price" => 0, "click_cnt" => 0, "adv_cpc" => 0, "impressions" => 0, "click_per" => 0);
        $nosp_stats_special_list[$month_date['chart_key']]  = array("adv_price" => 0, "click_cnt" => 0, "adv_cpc" => 0, "impressions" => 0, "click_per" => 0);
    }

    if($month_date['chart_name'] >= $agency_s_month && $month_date['chart_name'] <= $agency_e_month) {
        $agency_date_list[$month_date['chart_key']]        = $month_date['chart_title']."월";
        $agency_table_name_list[$month_date['chart_key']]  = $month_date['chart_name'];
        $agency_table_list[$month_date['chart_key']]       = $agency_table_init;
    }

    if($month_date['chart_name'] >= $work_track_s_month && $month_date['chart_name'] <= $work_track_e_month) {
        $work_track_date_list[$month_date['chart_key']]  = $month_date['chart_other_name'];
        $work_track_table_list[$month_date['chart_key']] = $work_track_init;
        $work_track_total_list[$month_date['chart_key']] = 0;
    }

    if($month_date['chart_name'] >= $withdraw_s_month && $month_date['chart_name'] <= $withdraw_e_month) {
        $withdraw_month_list[$month_date['chart_key']]          = $month_date['chart_other_name'];
        $withdraw_table_list[$month_date['chart_key']]          = $withdraw_init;
        $withdraw_month_total_list[$month_date['chart_key']]    = 0;
    }

    $csm_bad_date_list[$month_date['chart_key']]    = $month_date['chart_other_name'];
    $total_all_date_list[$month_date['date_key']]   = array("s_date" => date("Y-m-d",strtotime($month_date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($month_date['date_key']))." 23:59:59");
}

# NOSP 월별 광고결과 분석
$nosp_stats_sql = "
    SELECT
        `ar`.am_no,
        `ar`.brand,
        `am`.product,
        `am`.fee_per,
        `am`.price,
        `ar`.impressions,
        `ar`.click_cnt,
        `ar`.adv_price,
        DATE_FORMAT(`am`.adv_s_date, '%Y%m') as key_date
    FROM advertising_result `ar`
    LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
    WHERE {$nosp_stats_where}
    ORDER BY `am`.adv_s_date ASC
";
$nosp_stats_query       = mysqli_query($my_db, $nosp_stats_sql);
$stats_result_tmp_list  = [];
while($stats_result = mysqli_fetch_assoc($nosp_stats_query))
{
    $stats_result_tmp_list[$stats_result['am_no']][$stats_result['product']][$stats_result['brand']]["key_date"]            = $stats_result['key_date'];
    $stats_result_tmp_list[$stats_result['am_no']][$stats_result['product']][$stats_result['brand']]["fee_per"]             = $stats_result['fee_per'];
    $stats_result_tmp_list[$stats_result['am_no']][$stats_result['product']][$stats_result['brand']]["total_price"]         = $stats_result['price'];
    $stats_result_tmp_list[$stats_result['am_no']][$stats_result['product']][$stats_result['brand']]["total_impressions"]  += $stats_result['impressions'];
    $stats_result_tmp_list[$stats_result['am_no']][$stats_result['product']][$stats_result['brand']]["total_click_cnt"]    += $stats_result['click_cnt'];
    $stats_result_tmp_list[$stats_result['am_no']][$stats_result['product']][$stats_result['brand']]["total_adv_price"]    += $stats_result['adv_price'];
}

foreach($stats_result_tmp_list as $am_no => $stats_result_tmp)
{
    foreach($stats_result_tmp as $product => $stats_product_data)
    {
        foreach($stats_product_data as $brand => $stats_brand_data)
        {
            $fee_price      = $stats_brand_data['total_adv_price']*($stats_brand_data['fee_per']/100);
            $cal_price      = $stats_brand_data["total_adv_price"]-$fee_price;

            if($product == '1') {
                $nosp_stats_time_list[$stats_brand_data['key_date']]['adv_price']    += $cal_price;
                $nosp_stats_time_list[$stats_brand_data['key_date']]['click_cnt']    += $stats_brand_data["total_click_cnt"];
                $nosp_stats_time_list[$stats_brand_data['key_date']]['impressions']  += $stats_brand_data["total_impressions"];
            }
            elseif($product == '2') {
                $nosp_stats_special_list[$stats_brand_data['key_date']]['adv_price']    += $cal_price;
                $nosp_stats_special_list[$stats_brand_data['key_date']]['click_cnt']    += $stats_brand_data["total_click_cnt"];
                $nosp_stats_special_list[$stats_brand_data['key_date']]['impressions']  += $stats_brand_data["total_impressions"];
            }
        }
    }
}

foreach($nosp_stats_time_list as $key_date => $key_data){
    $nosp_stats_time_chart_list[0]['data'][] = (int)$key_data['adv_price'];
    $nosp_stats_time_chart_list[1]['data'][] = (int)$key_data['click_cnt'];
    $nosp_stats_time_chart_list[2]['data'][] = ($key_data['click_cnt'] == 0) ? 0 : round($key_data['adv_price']/$key_data['click_cnt'], 1);
}

foreach($nosp_stats_special_list as $key_date => $key_data){
    $nosp_stats_special_chart_list[0]['data'][] = (int)$key_data['adv_price'];
    $nosp_stats_special_chart_list[1]['data'][] = (int)$key_data['click_cnt'];
    $nosp_stats_special_chart_list[2]['data'][] = ($key_data['click_cnt'] == 0) ? 0 : round($key_data['adv_price']/$key_data['click_cnt'], 1);
}

# 마케팃 비용 및 수수료 월정산
$agency_data_sql    = "
    SELECT 
       REPLACE(settle_month,'-','') as key_month, 
       COUNT(as_no) as total_cnt, 
       SUM(wd_price) as total_price, 
       SUM(dp_price) as total_fee 
    FROM agency_settlement `as` 
    WHERE {$agency_month_where} 
    GROUP BY key_month 
    ORDER BY key_month
";
$agency_data_query  = mysqli_query($my_db, $agency_data_sql);
while($agency_data = mysqli_fetch_assoc($agency_data_query))
{
    $agency_table_list[$agency_data['key_month']]['total_cnt']    = (int)$agency_data['total_cnt'];
    $agency_table_list[$agency_data['key_month']]['total_price']  = (int)$agency_data['total_price'];
    $agency_table_list[$agency_data['key_month']]['total_fee']    = (int)$agency_data['total_fee'];
}

foreach($agency_table_list as $key_month => $key_data){
    $agency_chart_list[0]['data'][] = (int)$key_data['total_price'];
    $agency_chart_list[1]['data'][] = (int)$key_data['total_fee'];
    $agency_chart_list[2]['data'][] = (int)$key_data['total_cnt'];
}

# 이벤트 일정
$event_chk_date         = date('Y-m-d', strtotime("+1 days"));
$event_chk_s_datetime   = "{$event_chk_date} 00:00:00";
$event_chk_e_datetime   = date('Y-m-d')." 00:00:00";
$cms_event_where        = "ae.event_s_date < '{$event_chk_s_datetime}' AND ae.event_e_date > '{$event_chk_e_datetime}' AND ae.is_active='1' AND ae.reg_s_no='{$brand_manager}'";

$cms_event_sql      = "
    SELECT
        *
    FROM advertising_event `ae`
    WHERE {$cms_event_where}
    ORDER BY ae.event_s_date DESC LIMIT 10
";
$cms_event_query    = mysqli_query($my_db, $cms_event_sql);
$cms_event_list     = [];
while($cms_event_result = mysqli_fetch_assoc($cms_event_query))
{
    $explain_tmp    = explode("]" ,$cms_event_result['explain']);
    $dp_list_tmp    = explode(",", $cms_event_result['dp_list']);
    $event_dp_list  = [];

    if(!empty($dp_list_tmp)){
        foreach($dp_list_tmp as $event_dp_no){
            $event_dp_list[$event_dp_no] = $dp_company_list[$event_dp_no];
        }
    }

    $cms_event_list[] = array(
        "title"     => str_replace("[","", $explain_tmp[0]),
        "date"      => " ~ ".date("m/d", strtotime($cms_event_result['event_e_date']))." (".$date_name_option[date("w", strtotime($cms_event_result['event_e_date']))].") ".date("H:i", strtotime($cms_event_result['event_e_date'])),
        "dp_list"   => $event_dp_list
    );
}

# 사은품 증정(진행중) 일정
$prd_event_datetime = "{$cur_date} 00:00:00";
$prd_event_where    = "p.c_no IN({$main_brand_text}) AND p.display='1' AND (((p.gift_date_type='1' AND gift_e_date IS NULL) OR (p.gift_date_type='1' AND gift_e_date >= '{$prd_event_datetime}')) OR ((p.sub_gift_date_type='1' AND sub_gift_e_date IS NULL) OR (p.sub_gift_date_type='1' AND sub_gift_e_date >= '{$prd_event_datetime}')))";
$prd_event_sql      = "
    SELECT
        p.*,
        COUNT(p.prd_no) as total_cnt
    FROM product_cms p 
    WHERE {$prd_event_where}
    GROUP BY gift_s_date, gift_e_date, sub_gift_s_date, sub_gift_e_date
";
$prd_event_query = mysqli_query($my_db, $prd_event_sql);
$prd_event_list  = [];
while($prd_event = mysqli_fetch_assoc($prd_event_query))
{
    $prd_event['total_cnt'] -= 1;

    $prd_event_url = "sch_manager=&sch_gift_date_on_off=1&ord_page_type=100&sch_brand_search={$sch_brand}";

    $prd_event['gift_e_date_text'] = "";
    if($prd_event['gift_date_type'] == "1")
    {
        $gift_s_date    = !empty($prd_event['gift_s_date']) ? $prd_event['gift_s_date'] : "NULL";
        $gift_e_date    = !empty($prd_event['gift_e_date']) ? $prd_event['gift_e_date'] : "NULL";
        $prd_event_url .= "&brand_s_date={$gift_s_date}&brand_e_date={$gift_e_date}";

        if(!empty($prd_event['gift_e_date'])){
            $prd_event['gift_e_date_text'] = date("m/d", strtotime($prd_event['gift_e_date']))." (".$date_name_option[date("w", strtotime($prd_event['gift_e_date']))].") ".date("H:i", strtotime($prd_event['gift_e_date']));
        }
    }

    $prd_event['sub_gift_e_date_text'] = "";
    if($prd_event['sub_gift_date_type'] == "1")
    {
        $sub_gift_s_date    = !empty($prd_event['sub_gift_s_date']) ? $prd_event['sub_gift_s_date'] : "NULL";
        $sub_gift_e_date    = !empty($prd_event['sub_gift_e_date']) ? $prd_event['sub_gift_e_date'] : "NULL";
        $prd_event_url     .= "&brand_sub_s_date={$sub_gift_s_date}&brand_sub_e_date={$sub_gift_e_date}";

        if(!empty($prd_event['sub_gift_e_date'])){
            $prd_event['sub_gift_e_date_text'] = date("m/d", strtotime($prd_event['sub_gift_e_date']))." (".$date_name_option[date("w", strtotime($prd_event['sub_gift_e_date']))].") ".date("H:i", strtotime($prd_event['sub_gift_e_date']));
        }
    }

    if($prd_event['gift_date_type'] == "1" || $prd_event['sub_gift_date_type'] == "1")
    {
        $prd_event["event_url"] = $prd_event_url;
    }

    $prd_event_list[] = $prd_event;
}

# 월별 불량사유통계
$csm_bad_report_list        = [];
$csm_bad_report_name_list   = [];
$csm_bad_report_top_list    = [];
$csm_bad_report_total_list  = [];

$csm_bad_report_name_sql = "
    SELECT
        SUM(qty) as total_qty,
        csm_g2,
        csm_g1_name,
        csm_g2_name
    FROM 
    (
        SELECT 
            '1' as qty, 
            wcr.bad_reason as csm_g2,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`wcr`.bad_reason)) as csm_g1_name,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=`wcr`.bad_reason) as csm_g2_name
        FROM work_cms_return wcr 
        WHERE {$csm_bad_where} 
        GROUP BY order_number
    ) AS rs
    GROUP BY csm_g2
    ORDER BY total_qty DESC LIMIT 5
";
$csm_bad_report_name_query = mysqli_query($my_db, $csm_bad_report_name_sql);
while($csm_bad_report_name = mysqli_fetch_assoc($csm_bad_report_name_query))
{
    $csm_bad_report_name_list[$csm_bad_report_name['csm_g2']] = $csm_bad_report_name['csm_g1_name']." > ".$csm_bad_report_name['csm_g2_name'];
    $csm_bad_report_top_list[] = $csm_bad_report_name['csm_g2'];
}
$csm_bad_report_name_list["etc"] = "그 외";

foreach($csm_bad_date_list as $bad_month => $bad_month_label)
{
    foreach($csm_bad_report_name_list as $csm_g2 => $csm_g2_name){
        $csm_bad_report_list[$bad_month][$csm_g2] = 0;
    }
    $csm_bad_report_total_list[$bad_month] = 0;
}

$csm_bad_report_sql = "
    SELECT
        csm_g2,
        return_date,
        SUM(qty) AS total_qty
    FROM 
    (
        SELECT 
            '1' as qty, 
            wcr.bad_reason as csm_g2, 
            DATE_FORMAT(return_req_date, '%Y%m') as return_date
        FROM work_cms_return wcr
        WHERE {$csm_bad_where}
        GROUP BY order_number
    ) AS rs
    GROUP BY return_date, csm_g2
    ORDER BY return_date
";
$csm_bad_report_query = mysqli_query($my_db, $csm_bad_report_sql);
while($csm_bad_report = mysqli_fetch_assoc($csm_bad_report_query))
{
    $csm_g2 = "etc";
    if(in_array($csm_bad_report['csm_g2'], $csm_bad_report_top_list)){
        $csm_g2 = $csm_bad_report['csm_g2'];
    }
    $csm_bad_report_list[$csm_bad_report['return_date']][$csm_g2] += $csm_bad_report['total_qty'];
    $csm_bad_report_total_list[$csm_bad_report['return_date']] += $csm_bad_report['total_qty'];
}

# 월별 물류비 정산
$work_track_sql = "
    SELECT
        w.prd_no,
        DATE_FORMAT(`w`.task_run_regdate, '%Y%m') as key_month,
        SUM(wd_price_vat) as total_value
    FROM work w
    WHERE {$work_track_where}
    GROUP BY prd_no, key_month
    ORDER BY prd_no, key_month
";
$work_track_query = mysqli_query($my_db, $work_track_sql);
while($work_track = mysqli_fetch_assoc($work_track_query))
{
    $work_track_table_list[$work_track['key_month']][$work_track['prd_no']] += $work_track['total_value'];
    $work_track_total_list[$work_track['key_month']] += $work_track['total_value'];
}

# 재고현황 내역
$product_stock_where    = "`pcu`.brand IN({$main_brand_text}) AND pcs.stock_c_no='2809' AND (pcs.warehouse LIKE '%정상%' OR pcs.warehouse LIKE '%세이프인%')";
$with_stock_prd_list    = [];
$with_stock_unit_list   = [];
$unit_type_option       = getUnitTypeOption();

# 재고현황 최근 기준일
$last_stock_sql         = "SELECT MAX(stock_date) as last_date FROM product_cms_stock WHERE stock_c_no='2809' ORDER BY stock_date DESC";
$last_stock_query       = mysqli_query($my_db, $last_stock_sql);
$last_stock_result      = mysqli_fetch_assoc($last_stock_query);
$last_stock_date        = $last_stock_result['last_date'];
$product_stock_where   .= " AND pcs.stock_date = '{$last_stock_date}'";

$prev_stock_month       = date("Y-m", strtotime("{$last_stock_date} -1 months"));
$chk_report_s_date      = $prev_stock_month."-01";
$chk_report_e_day       = date('t', strtotime($prev_stock_month));
$chk_report_e_date      = $prev_stock_month."-".$chk_report_e_day;

$product_stock_sql     = "
    SELECT
        rs.*,
        (SELECT c.c_name FROM company c WHERE c.c_no=rs.brand) as brand_name,
        (SELECT c.c_name FROM company c WHERE c.c_no=rs.sup_c_no) as sup_c_name,
        (SELECT c.c_name FROM company c WHERE c.c_no=rs.stock_c_no) as stock_c_name,
        SUM(qty) as total_qty,
        IF(day_qty IS NULL OR day_qty=0, '1', '2') as day_null,
        (SUM(qty)/day_qty) as exp_day
    FROM
    (
        SELECT 
            pcs.no,
            pcs.brand,
            pcs.prd_unit,
            pcs.unit_type,
            pcs.stock_prd_sku as sku,
            pcs.option_name,
            pcs.sup_c_no,
            pcs.stock_c_no,
            pcs.qty,
            (SELECT SUM(co.quantity) FROM commerce_order co LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no` WHERE co.option_no=pcs.prd_unit AND `cos`.sup_c_no=pcs.sup_c_no AND `cos`.display='1' AND `cos`.state='2' AND `co`.`type` IN('request','supply') ) AS co_qty,
            pcs.report_qty,
            (pcs.report_qty*-1) as ord_report_qty,
            ((pcs.report_qty*-1)/{$chk_report_e_day}) as day_qty
        FROM (
            SELECT 
                pcs.*,
                pcu.brand,  
                pcu.`type` as unit_type,
                pcu.option_name,
                (SELECT SUM(stock_qty) FROM product_cms_stock_report pcsr WHERE pcsr.prd_unit=pcs.prd_unit AND pcsr.state='판매반출' AND pcsr.confirm_state='9' AND pcsr.log_c_no=pcs.stock_c_no AND (pcsr.regdate BETWEEN '{$chk_report_s_date}' AND '{$chk_report_e_date}')) AS report_qty
            FROM product_cms_stock pcs 
            LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcs.prd_unit
            WHERE {$product_stock_where}
        ) as pcs
    ) AS rs
    WHERE (co_qty != 0 OR qty != 0)
    GROUP BY prd_unit
    ORDER BY day_null DESC, exp_day ASC, total_qty ASC, prd_unit ASC
";
$product_stock_query = mysqli_query($my_db, $product_stock_sql);
$stock_prd_idx  = 0;
$stock_unit_idx = 0;
while($product_stock = mysqli_fetch_assoc($product_stock_query)){
    if($product_stock['unit_type'] == "1"){
        if($stock_prd_idx < 10){
            $with_stock_prd_list[] = $product_stock;
        }
        $stock_prd_idx++;
    }elseif($product_stock['unit_type'] == "2"){
        if($stock_unit_idx < 10){
            $with_stock_unit_list[] = $product_stock;
        }
        $stock_unit_idx++;
    }

    if($stock_prd_idx > 10 && $stock_unit_idx > 10){
        break;
    }
}

# 타계정반출 통계
$stock_other_tmp_list   = [];
$stock_other_total_list = [];
$stock_other_table_list = [];
$stock_other_chart_list = [];
$stock_other_title_list = getUseBmSubjectNameOption();
$stock_other_month      = $prev_month;
$stock_other_month_text = date("m", strtotime($stock_other_month));
$stock_other_e_day      = date("t", strtotime($stock_other_month));
$stock_other_s_date     = "{$stock_other_month}-01";
$stock_other_e_date     = "{$stock_other_month}-{$stock_other_e_day}";

# 타계정반출 쿼리
$stock_other_where      = "pcu.brand IN({$main_brand_text}) AND pcsr.regdate BETWEEN '{$stock_other_s_date}' AND '{$stock_other_e_date}' AND pcsr.log_subject IN(1,2,3,4,5,9,14) AND pcsr.confirm_state='10'";
$stock_other_sql        = "
    SELECT 
        pcsr.option_name,
        pcu.brand,
        pcsr.sku,
        pcsr.prd_unit,
        (SELECT org_price FROM product_cms_stock_confirm pcsc WHERE base_type='start' AND pcsc.prd_unit=pcsr.prd_unit AND base_mon='{$stock_other_month}' LIMIT 1) as unit_price,
        pcsr.log_subject,
        SUM(pcsr.stock_qty) as total_qty
    FROM product_cms_stock_report pcsr
    LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcsr.prd_unit
    WHERE {$stock_other_where}
    GROUP BY pcsr.prd_unit, pcsr.log_subject
    ORDER BY pcsr.prd_unit
";
$stock_other_query      = mysqli_query($my_db, $stock_other_sql);
while($stock_other_result = mysqli_fetch_assoc($stock_other_query))
{
    if(!isset($stock_other_tmp_list[$stock_other_result['prd_unit']]))
    {
        $stock_other_tmp_list[$stock_other_result['prd_unit']] = array(
            "name"          => $stock_other_result['option_name'],
            "sku"           => $stock_other_result['sku'],
            "prd_unit"      => $stock_other_result['prd_unit'],
            "price"         => $stock_other_result['unit_price'],
            "1_qty"         => 0,
            "1_price"       => 0,
            "2_qty"         => 0,
            "2_price"       => 0,
            "3_qty"         => 0,
            "3_price"       => 0,
            "4_qty"         => 0,
            "4_price"       => 0,
            "5_qty"         => 0,
            "5_price"       => 0,
            "9_qty"         => 0,
            "9_price"       => 0,
            "14_qty"        => 0,
            "14_price"      => 0,
            "15_qty"        => 0,
            "15_price"      => 0,
            "total_qty"     => 0,
            "total_price"   => 0
        );
    }

    $subject_qty_name   = $stock_other_result['log_subject']."_qty";
    $subject_price_name = $stock_other_result['log_subject']."_price";
    $total_qty          = $stock_other_result['total_qty']*-1;
    $unit_price         = $stock_other_result['unit_price'];
    $total_price        = $total_qty*$unit_price;

    $stock_other_tmp_list[$stock_other_result['prd_unit']][$subject_qty_name]   += $total_qty;
    $stock_other_tmp_list[$stock_other_result['prd_unit']][$subject_price_name] += $total_price;
    $stock_other_tmp_list[$stock_other_result['prd_unit']]["total_qty"]         += $total_qty;
    $stock_other_tmp_list[$stock_other_result['prd_unit']]["total_price"]       += $total_price;

    $stock_other_total_list[$stock_other_result['prd_unit']] += $total_qty;
}

if(!empty($stock_other_tmp_list))
{
    arsort($stock_other_total_list);

    foreach($stock_other_total_list as $prd_unit => $qty)
    {
        $tmp_data = $stock_other_tmp_list[$prd_unit];
        $stock_other_table_list[$prd_unit] = $tmp_data;

        $stock_other_chart_list[] = array(
            "title" => $tmp_data['name'],
            "type"  => "bar",
            "data"  => array($tmp_data["1_qty"],$tmp_data["9_qty"],$tmp_data["2_qty"],$tmp_data["3_qty"],$tmp_data["4_qty"],$tmp_data["5_qty"]),
        );
    }
}

# 출금 리스트
$withdraw_sql = "
    SELECT
        DATE_FORMAT(`wd`.wd_date, '%Y%m') as key_month,
        w.prd_no,
	    SUM(wd.wd_money) as total_money 
    FROM withdraw `wd` 
    LEFT JOIN `work` AS `w` ON `w`.w_no=`wd`.w_no
    WHERE {$withdraw_where}
    GROUP BY key_month, w.prd_no
";
$withdraw_query = mysqli_query($my_db, $withdraw_sql);
while($withdraw = mysqli_fetch_assoc($withdraw_query))
{
    $withdraw_table_list[$withdraw['key_month']][$withdraw['prd_no']] += $withdraw['total_money'];
    $withdraw_total_list[$withdraw['prd_no']]           += $withdraw['total_money'];
    $withdraw_month_total_list[$withdraw['key_month']]  += $withdraw['total_money'];
}
arsort($withdraw_total_list);

if(!empty($withdraw_total_list) && !empty($withdraw_table_list))
{
    foreach($withdraw_total_list as $prd_no => $total_value)
    {
        if($total_value == 0){
            unset($withdraw_total_list[$prd_no]);
            continue;
        }
        $withdraw_chart_init_data = array(
            "title" => $withdraw_title_list[$prd_no],
            "type"  => "bar",
            "data"  => array(),
        );

        foreach($withdraw_month_list as $wd_month => $month_label){
            $withdraw_chart_init_data["data"][] = $withdraw_table_list[$wd_month][$prd_no];
        }

        $withdraw_chart_list[] = $withdraw_chart_init_data;
    }
}

$smarty->assign("daily_s_date", $daily_s_date);
$smarty->assign("daily_e_date", $daily_e_date);
$smarty->assign("daily_s_date_text", $daily_s_date_text);
$smarty->assign("daily_e_date_text", $daily_e_date_text);
$smarty->assign("prev_daily_s_date", $prev_daily_s_date);
$smarty->assign("prev_daily_e_date", $prev_daily_e_date);
$smarty->assign("prev_daily_s_date_text", $prev_daily_s_date_text);
$smarty->assign("prev_daily_e_date_text", $prev_daily_e_date_text);
$smarty->assign("daily_date_list", $daily_date_list);
$smarty->assign("daily_hour_list", $daily_hour_list);
$smarty->assign("daily_sales_total_price", $daily_sales_total_price);
$smarty->assign("daily_date_total_list", $daily_date_total_list);
$smarty->assign("daily_hour_total_list", $daily_hour_total_list);
$smarty->assign("daily_sales_list", $daily_sales_list);
$smarty->assign("sales_term_hour_list", $sales_term_hour_list);
$smarty->assign("sales_term_date_list", $sales_term_date_list);
$smarty->assign("sales_term_total_list", $sales_term_total_list);
$smarty->assign("nosp_state_option", $nosp_state_option);
$smarty->assign("nosp_manage_confirm_list", $nosp_manage_confirm_list);
$smarty->assign("nosp_manage_request_list", $nosp_manage_request_list);
$smarty->assign("nosp_result_list", $nosp_result_list);
$smarty->assign("nosp_stats_s_date", $nosp_stats_s_date);
$smarty->assign("nosp_stats_e_date", $nosp_stats_e_date);
$smarty->assign("nosp_product_option", $nosp_product_option);
$smarty->assign("nosp_stats_date_list", json_encode($nosp_stats_date_list));
$smarty->assign("nosp_stats_time_chart_list", json_encode($nosp_stats_time_chart_list));
$smarty->assign("nosp_stats_special_chart_list", json_encode($nosp_stats_special_chart_list));
$smarty->assign("agency_s_month", $agency_s_month);
$smarty->assign("agency_e_month", $agency_e_month);
$smarty->assign("agency_date_list", $agency_date_list);
$smarty->assign("agency_table_name_list", $agency_table_name_list);
$smarty->assign("agency_table_list", $agency_table_list);
$smarty->assign("agency_chart_name_list", json_encode($agency_date_list));
$smarty->assign("agency_chart_list", json_encode($agency_chart_list));
$smarty->assign("cms_event_list", $cms_event_list);
$smarty->assign("cms_recent_s_date", $prev_mon_date);
$smarty->assign("cms_recent_e_date", $cur_date);
$smarty->assign("prd_event_list", $prd_event_list);
$smarty->assign("csm_bad_s_month", $csm_bad_s_month);
$smarty->assign("csm_bad_e_month", $csm_bad_e_month);
$smarty->assign("csm_bad_date_list", $csm_bad_date_list);
$smarty->assign("csm_bad_report_list", $csm_bad_report_list);
$smarty->assign("csm_bad_report_name_list", $csm_bad_report_name_list);
$smarty->assign("csm_bad_report_total_list", $csm_bad_report_total_list);
$smarty->assign("csm_bad_report_chart_list", json_encode($csm_bad_report_total_list));
$smarty->assign("csm_bad_report_chart_name_list", json_encode($csm_bad_date_list));
$smarty->assign("work_track_option", $work_track_option);
$smarty->assign("work_track_date_list", $work_track_date_list);
$smarty->assign("work_track_name_list", $work_track_name_list);
$smarty->assign("work_track_table_list", $work_track_table_list);
$smarty->assign("work_track_total_list", $work_track_total_list);
$smarty->assign("work_track_chart_list", json_encode($work_track_total_list));
$smarty->assign("work_track_chart_name_list", json_encode($work_track_date_list));
$smarty->assign("unit_type_option", $unit_type_option);
$smarty->assign("with_stock_prd_list", $with_stock_prd_list);
$smarty->assign("with_stock_unit_list", $with_stock_unit_list);
$smarty->assign("other_type_option", getOtherTypeOption());
$smarty->assign("stock_other_month", $stock_other_month);
$smarty->assign("stock_other_month_text", $stock_other_month_text);
$smarty->assign("stock_other_title_list", $stock_other_title_list);
$smarty->assign("stock_other_table_list", $stock_other_table_list);
$smarty->assign("stock_other_chart_list", json_encode($stock_other_chart_list));
$smarty->assign("stock_other_chart_name_list", json_encode($stock_other_title_list));
$smarty->assign("withdraw_s_date", $withdraw_s_date);
$smarty->assign("withdraw_e_date", $withdraw_e_date);
$smarty->assign("withdraw_month_list", $withdraw_month_list);
$smarty->assign("withdraw_title_list", $withdraw_title_list);
$smarty->assign("withdraw_total_list", $withdraw_total_list);
$smarty->assign("withdraw_month_total_list", $withdraw_month_total_list);
$smarty->assign("withdraw_table_list", $withdraw_table_list);
$smarty->assign("withdraw_chart_list", json_encode($withdraw_chart_list));
$smarty->assign("withdraw_chart_name_list", json_encode($withdraw_month_list));

if($sch_brand == 'ilenol' || $sch_brand == 'hose' || $sch_brand == 'pure' || $sch_brand == 'nuzam'){
    $smarty_html = "wise_bm/brand_main_{$sch_brand}.html";
}else{
    $smarty_html = "wise_bm/brand_main_base.html";
}

$smarty->display($smarty_html);
?>
