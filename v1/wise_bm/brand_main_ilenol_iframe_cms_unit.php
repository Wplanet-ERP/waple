<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');
require('../inc/model/Company.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 검색조건
$bm_brand_option    = getBrandDetailOption();
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sel_brand_data     = isset($bm_brand_option[$sch_brand]) ? $bm_brand_option[$sch_brand] : "";

if(empty($sel_brand_data)){
    exit("<script>alert('접근 권한이 없습니다.');location.href='/v1/main.php';</script>");
}

# 브랜드 변수
$main_brand_option      = $sel_brand_data['brand_list'];
$main_brand_text        = implode(",", $main_brand_option);
$dp_except_list         = getNotApplyDpList();
$dp_except_text         = implode(",", $dp_except_list);
$sch_brand_url          = $sel_brand_data['brand_url'];
$total_all_date_list    = [];

# 택배리스트(구성품) 통계 월별처리(아이레놀)
$unit_cms_s_month           = date('Y-m', strtotime("-5 months"));
$unit_cms_e_month           = date('Y-m');
$unit_cms_e_day             = date("t", strtotime($unit_cms_e_month));
$unit_cms_s_date            = $unit_cms_s_month."-01";
$unit_cms_e_date            = $unit_cms_e_month."-{$unit_cms_e_day}";
$unit_cms_where             = "w.c_no IN({$main_brand_text}) AND w.delivery_state='4'";
$unit_cms_date_list         = [];
$total_all_date_list        = [];
$unit_cms_init              = [];
$unit_cms_title_list        = [];
$unit_cms_table_list        = [];
$unit_cms_total_chk_list    = [];
$unit_cms_chart_list        = [];

$unit_cms_init_sql          = "SELECT p.`no`, p.option_name FROM product_cms_unit p WHERE p.brand IN({$main_brand_text}) AND p.display='1'";
$unit_cms_init_query        = mysqli_query($my_db, $unit_cms_init_sql);
while($unit_cms_init_result = mysqli_fetch_assoc($unit_cms_init_query)){
    $unit_cms_title_list[$unit_cms_init_result['no']]       = $unit_cms_init_result['option_name'];
    $unit_cms_init[$unit_cms_init_result['no']]             = 0;
    $unit_cms_total_chk_list[$unit_cms_init_result['no']]   = 0;
}

# 상품별 주문수 통계(아이레놀)
$prd_cms_s_date             = date("Y-m-d", strtotime("-6 days"));
$prd_cms_e_date             = date("Y-m-d");
$prd_cms_s_datetime         = $prd_cms_s_date." 00:00:00";
$prd_cms_e_datetime         = $prd_cms_e_date." 23:59:59";
$prd_cms_where              = "w.c_no IN({$main_brand_text}) AND w.delivery_state='4' AND w.unit_price > 0 AND w.order_date BETWEEN '{$prd_cms_s_datetime}' AND '{$prd_cms_e_datetime}'";
$prd_cms_color_list         = array("#5470C6","#EE6666","#3BA272","#FC8452","#9A60B4","#EA7CCC","#73C0DE","#91CC75","#FAC858","#ED9FB6","#DDDDDD");
$prd_cms_count_init         = [];
$prd_cms_title_list         = [];
$prd_cms_date_list          = [];
$prd_cms_count_list         = [];
$prd_cms_chart_tmp_list     = [];
$prd_cms_chart_list         = [];
$prd_cms_count_total_list   = [];

# 상품별 주문수 상품들(BEST 10개 나머지)
$prd_cms_title_sql      = "SELECT prd_no, (SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no) as prd_name, COUNT(DISTINCT order_number) as ord_cnt FROM work_cms as w WHERE {$prd_cms_where} GROUP BY prd_no ORDER BY ord_cnt DESC, prd_name ASC LIMIT 10";
$prd_cms_title_query    = mysqli_query($my_db, $prd_cms_title_sql);
while($prd_cms_result = mysqli_fetch_assoc($prd_cms_title_query)) {
    $prd_cms_title_list[$prd_cms_result['prd_no']] = $prd_cms_result['prd_name'];
    $prd_cms_count_init[$prd_cms_result['prd_no']] = 0;
}
$prd_cms_title_list["etc"] = "기타";
$prd_cms_count_init["etc"] = 0;

# 월별 처리
$month_date_sql = "
	SELECT
 		DATE_FORMAT(`allday`.Date, '%m') as chart_title,
 		DATE_FORMAT(`allday`.Date, '%Y%m') as chart_key,
	    DATE_FORMAT(`allday`.Date, '%Y-%m') as chart_name,
	    DATE_FORMAT(`allday`.Date, '%Y/%m') as chart_other_name,
	    DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key,
        DATE_FORMAT(`allday`.Date, '%Y-%m-%d') as date_name
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$unit_cms_s_month}' AND '{$unit_cms_e_month}'
	ORDER BY chart_key, date_key
";
$month_date_query = mysqli_query($my_db, $month_date_sql);
while($month_date = mysqli_fetch_assoc($month_date_query))
{
    if($month_date['date_name'] >= $prd_cms_s_date && $month_date['date_name'] <= $prd_cms_e_date){
        $prd_cms_date_list[$month_date['date_key']]         = $month_date['date_name'];
        $prd_cms_count_list[$month_date['date_key']]        = $prd_cms_count_init;
        $prd_cms_count_total_list[$month_date['date_key']]  = 0;
    }

    if(!isset($unit_cms_date_list[$month_date['chart_key']])){
        $unit_cms_date_list[$month_date['chart_key']]   = $month_date['chart_other_name'];
        $unit_cms_table_list[$month_date['chart_key']]  = $unit_cms_init;
    }
    
    $total_all_date_list[$month_date['date_key']]   = array("s_date" => date("Y-m-d",strtotime($month_date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($month_date['date_key']))." 23:59:59");
}

foreach($total_all_date_list as $date_data)
{
    $unit_cms_sql = "
        SELECT
            pcu.`no` AS pcu_no,
            pcu.option_name,
            DATE_FORMAT(w.order_date, '%Y%m') as key_month,
            SUM(w.quantity*pcr.quantity) as total_qty
        FROM `work_cms` w
        LEFT JOIN product_cms_relation pcr ON pcr.prd_no=w.prd_no AND pcr.display='1'
        LEFT JOIN product_cms_unit pcu ON pcr.option_no=pcu.`no`
        WHERE {$unit_cms_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}')
        GROUP BY pcu_no, key_month
    ";
    $unit_cms_query = mysqli_query($my_db, $unit_cms_sql);
    while($unit_cms = mysqli_fetch_assoc($unit_cms_query)){
        $unit_cms_table_list[$unit_cms['key_month']][$unit_cms['pcu_no']] += $unit_cms['total_qty'];
        $unit_cms_total_chk_list[$unit_cms['pcu_no']] += $unit_cms['total_qty'];
    }
}

# 판매수 0 제외
if(!empty($unit_cms_total_chk_list)){
    foreach($unit_cms_total_chk_list as $prd_unit => $unit_qty){
        if($unit_qty == 0){
            unset($unit_cms_title_list[$prd_unit]);
        }
    }
}else{
    $unit_cms_title_list = [];
}

# 차트 리스트 생성
if(!empty($unit_cms_title_list) && !empty($unit_cms_total_chk_list))
{
    foreach($unit_cms_title_list as $prd_unit => $title)
    {
        $unit_chart_init_data = array(
            "title" => $title,
            "type"  => "bar",
            "data"  => array(),
        );

        foreach($unit_cms_date_list as $cms_date => $date_label){
            $unit_chart_init_data["data"][] = $unit_cms_table_list[$cms_date][$prd_unit];
        }

        $unit_cms_chart_list[] = $unit_chart_init_data;
    }
}

# 상품별 주문수
$prd_cms_count_sql = "
    SELECT 
       prd_no, 
       DATE_FORMAT(`w`.order_date, '%Y%m%d') as key_date, 
       COUNT(DISTINCT order_number) as ord_cnt 
    FROM work_cms `w`
    WHERE {$prd_cms_where} 
    GROUP BY key_date, prd_no
";
$prd_cms_count_query = mysqli_query($my_db, $prd_cms_count_sql);
while($prd_cms_count = mysqli_fetch_assoc($prd_cms_count_query))
{
    if(isset($prd_cms_title_list[$prd_cms_count['prd_no']])){
        $prd_cms_count_list[$prd_cms_count['key_date']][$prd_cms_count['prd_no']] += $prd_cms_count['ord_cnt'];
    }else{
        $prd_cms_count_list[$prd_cms_count['key_date']]["etc"] += $prd_cms_count['ord_cnt'];
    }

    $prd_cms_count_total_list[$prd_cms_count['key_date']] += $prd_cms_count['ord_cnt'];
}

foreach($prd_cms_count_list as $key_date => $date_data)
{
    $prd_idx = 0;
    foreach($date_data as $prd_no => $prd_data)
    {
        if(!isset($prd_cms_chart_tmp_list[$prd_no]))
        {
            $prd_cms_chart_tmp_list[$prd_no] = array(
                "color" => $prd_cms_color_list[$prd_idx],
                "data"  => array(),
            );
        }

        $prd_cms_chart_tmp_list[$prd_no]["data"][] = $prd_data;
        $prd_idx++;
    }
}

if(!empty($prd_cms_chart_tmp_list))
{
    foreach($prd_cms_chart_tmp_list as $prd_no => $tmp_chart_data)
    {
        $prd_cms_chart_list[] = array(
            "title" => $prd_cms_title_list[$prd_no],
            "color" => $tmp_chart_data["color"],
            "data"  => $tmp_chart_data["data"],
        );
    }
}

$smarty->assign("unit_cms_s_month", $unit_cms_s_month);
$smarty->assign("unit_cms_e_month", $unit_cms_e_month);
$smarty->assign("unit_cms_date_list", $unit_cms_date_list);
$smarty->assign("unit_cms_title_list", $unit_cms_title_list);
$smarty->assign("unit_cms_table_list", $unit_cms_table_list);
$smarty->assign("unit_cms_chart_list", json_encode($unit_cms_chart_list));
$smarty->assign("unit_cms_chart_name_list", json_encode($unit_cms_date_list));
$smarty->assign("prd_cms_chart_list", json_encode($prd_cms_chart_list));
$smarty->assign("prd_cms_chart_total_list", json_encode($prd_cms_count_total_list));
$smarty->assign("prd_cms_chart_name_list", json_encode($prd_cms_date_list));

$smarty->display('wise_bm/brand_main_ilenol_iframe_cms_unit.html');
?>
