<?php
require('../inc/common.php');
require('../../libs/dbconfig_mil.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_date.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');
require('../inc/helper/commerce_sales.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 검색조건
$total_brand_option     = getTotalBrandList();
$doc_brand_option       = getTotalDocBrandList();
$nuzam_brand_option     = getNuzamBrandList();
$ilenol_brand_option    = getIlenolBrandList();
$city_brand_option      = getCityBrandList();
$mil_brand_option       = getMilBrandList();
$total_brand_text       = implode(",", $total_brand_option);
$doc_brand_text         = implode(",", $doc_brand_option);
$nuzam_brand_text       = implode(",", $nuzam_brand_option);
$mil_brand_text         = implode(",", $mil_brand_option);
$dp_except_list         = getNotApplyDpList();
$dp_except_text         = implode(",", $dp_except_list);
$dp_self_imweb_list     = getSelfDpImwebCompanyList();
$dp_self_imweb_text     = implode(",", $dp_self_imweb_list);
$global_dp_all_list     = getGlobalDpCompanyList();
$global_dp_total_list   = $global_dp_all_list['total'];

# 브랜드 Name
$total_brand_column     = getTotalBrandNameOption();
$doc_brand_column       = getDocBrandNameOption();
$nuzam_brand_column     = getNuazmBrandNameOption();
$mil_brand_column       = getMilBrandNameOption();
$doc_filter_option      = getDocFilterOption();
$doc_cubing_option      = getDocCubingOption();

# 날짜 체크
$today_s_w		= date('w')-1;
$sch_e_date     = date('Y-m-d', strtotime("-1 days"));
$sch_s_date     = date('Y-m-d', strtotime("{$sch_e_date} -7 days"));
$today_s_week   = date('Y-m-d', strtotime("-{$today_s_w} days"));
$sch_s_week     = date('Y-m-d', strtotime("{$today_s_week} -6 weeks"));
$sch_e_week     = date("Y-m-d",strtotime("{$today_s_week} +6 days"));
$sch_e_month    = date('Y-m', strtotime($sch_e_date));
$sch_s_month    = date('Y-m', strtotime("{$sch_e_month} -6 months"));

$sch_base_type  = isset($_GET['sch_base_type']) ? $_GET['sch_base_type'] : "all";
$sch_date_type  = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "date";
$sch_s_datetime = "";
$sch_e_datetime = "";
$sch_net_date   = "";
    
$add_comm_where     = "display='1'";
$add_cms_where      = "w.delivery_state='4' AND w.dp_c_no NOT IN({$dp_except_text})";
$add_quick_where    = "w.prd_type='1' AND w.unit_price > 0 AND w.quick_state='4'";
$add_return_where   = "order_number != '' AND order_number IS NOT NULL";
$add_work_where     = "w.work_state='6' AND w.prd_no IN(SELECT p.prd_no FROM product p WHERE p.k_name_code IN('01004','01006') AND display='1')";
$add_result_where   = "1=1 AND `am`.state IN(3,5) AND `am`.product IN(1,2) AND `am`.media='265'";
$add_net_where      = "";
$add_comm_column    = "";
$add_cms_column     = "";
$add_quick_column   = "";
$add_return_column  = "";
$add_work_column    = "";
$add_result_column  = "";
$add_net_column     = "";
$sch_brand_name     = "";
$sch_date_name      = "";
$sch_brand_url      = "";

$chk_total_brand_option  = [];
$total_brand_column_list = [];

# 기본타입 체크
if($sch_base_type == "all"){
    $add_comm_where            .= " AND brand IN({$total_brand_text})";
    $add_cms_where             .= " AND w.c_no IN({$total_brand_text})";
    $add_quick_where           .= " AND w.c_no IN({$total_brand_text})";
    $add_result_where          .= " AND ar.brand IN({$total_brand_text})";

    $total_brand_column_list    = $total_brand_column;
    $chk_total_brand_option     = $total_brand_option;

    $sch_brand_name             = "커머스 전체";
}
elseif($sch_base_type == "doc"){
    $add_comm_where            .= " AND brand IN({$doc_brand_text})";
    $add_cms_where             .= " AND w.c_no IN({$doc_brand_text})";
    $add_quick_where           .= " AND w.c_no IN({$doc_brand_text})";
    $add_result_where          .= " AND ar.brand IN({$doc_brand_text})";

    $total_brand_column_list    = $doc_brand_column;
    $chk_total_brand_option     = $doc_brand_option;

    $sch_brand_name             = "닥터피엘 전체";
    $sch_brand_url              = "sch_brand_g1=70001";
}
elseif($sch_base_type == "nuzam"){
    $add_comm_where            .= " AND brand IN({$nuzam_brand_text})";
    $add_cms_where             .= " AND w.c_no IN({$nuzam_brand_text})";
    $add_quick_where           .= " AND w.c_no IN({$nuzam_brand_text})";
    $add_result_where          .= " AND ar.brand IN({$nuzam_brand_text})";

    $total_brand_column_list    = $nuzam_brand_column;
    $chk_total_brand_option     = $nuzam_brand_option;

    $sch_brand_name             = "누잠 전체";
    $sch_brand_url              = "sch_brand_g1=70003";
}
elseif($sch_base_type == "mil"){
    $add_comm_where            .= " AND brand IN({$mil_brand_text})";
    $add_cms_where              = "w.delivery_state='4'";
    $add_work_where            .= " AND w.c_no IN({$mil_brand_text})";
    $add_return_where          .= " AND c_no IN({$mil_brand_text})";
    $sch_date_type              = "month";

    $total_brand_column_list    = $mil_brand_column;
    $chk_total_brand_option     = $mil_brand_option;

    $sch_brand_name             = "밀리옹 전체";
}

# 날짜 검색
if($sch_date_type == "month")
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_comm_where    .= " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_comm_column    = "DATE_FORMAT(sales_date, '%Y%m')";

    $add_net_where     .= " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_net_column     = "DATE_FORMAT(sales_date, '%Y%m')";
    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m')";

    $sch_e_day          = date("t", strtotime($sch_e_month));
    $sch_s_datetime     = $sch_s_month."-01 00:00:00";
    $sch_e_datetime     = $sch_e_month."-{$sch_e_day} 23:59:59";

    $add_return_where  .= " AND return_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_return_column  = "DATE_FORMAT(return_date, '%Y%m')";

    if($sch_base_type == "mil"){
        $add_cms_where .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    }

    $add_quick_where   .= " AND w.run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(w.run_date, '%Y%m')";

    $add_work_where    .= " AND w.task_run_regdate BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_work_column    = "DATE_FORMAT(w.task_run_regdate, '%Y%m')";

    $add_result_where  .= " AND am.adv_s_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y%m')";
    
    $sch_net_date       = $sch_s_month."-01";
    $sch_date_name      = $sch_brand_name." ({$sch_s_month} ~ {$sch_e_month}) 합계";
}
elseif($sch_date_type == "week")
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	    = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title     = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_comm_where    .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_comm_column    = "DATE_FORMAT(DATE_SUB(sales_date, INTERVAL(IF(DAYOFWEEK(sales_date)=1,8,DAYOFWEEK(sales_date))-2) DAY), '%Y%m%d')";

    $add_net_where     .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}' ";
    $add_net_column     = "DATE_FORMAT(DATE_SUB(sales_date, INTERVAL(IF(DAYOFWEEK(sales_date)=1,8,DAYOFWEEK(sales_date))-2) DAY), '%Y%m%d')";
    $add_cms_column     = "DATE_FORMAT(DATE_SUB(order_date, INTERVAL(IF(DAYOFWEEK(order_date)=1,8,DAYOFWEEK(order_date))-2) DAY), '%Y%m%d')";

    $sch_s_datetime     = $sch_s_week." 00:00:00";
    $sch_e_datetime     = $sch_e_week." 23:59:59";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(DATE_SUB(run_date, INTERVAL(IF(DAYOFWEEK(run_date)=1,8,DAYOFWEEK(run_date))-2) DAY), '%Y%m%d')";

    $add_return_where  .= " AND return_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_return_column  = "DATE_FORMAT(DATE_SUB(return_date, INTERVAL(IF(DAYOFWEEK(return_date)=1,8,DAYOFWEEK(return_date))-2) DAY), '%Y%m%d')";

    $add_result_where  .= " AND am.adv_s_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'";
    $add_result_column  = "DATE_FORMAT(DATE_SUB(am.adv_s_date, INTERVAL(IF(DAYOFWEEK(am.adv_s_date)=1,8,DAYOFWEEK(am.adv_s_date))-2) DAY), '%Y%m%d')";

    $sch_net_date       = $sch_s_week;
    $sch_date_name      = $sch_brand_name." ({$sch_s_week} ~ {$sch_e_week}) 합계";
}
else
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_comm_where    .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_comm_column    = "DATE_FORMAT(sales_date, '%Y%m%d')";

    $add_net_where     .= " AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_net_column     = "DATE_FORMAT(sales_date, '%Y%m%d')";
    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m%d')";

    $sch_s_datetime     = $sch_s_date." 00:00:00";
    $sch_e_datetime     = $sch_e_date." 23:59:59";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y%m%d')";

    $add_return_where  .= " AND return_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_return_column  = "DATE_FORMAT(return_date, '%Y%m%d')";

    $add_result_where  .= " AND am.adv_s_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}'";
    $add_result_column  = "DATE_FORMAT(am.adv_s_date, '%Y%m%d')";

    $sch_net_date       = $sch_s_date;
    $sch_date_name      = $sch_brand_name." ({$sch_s_date} ~ {$sch_e_date}) 합계";
}

$smarty->assign("sch_brand_name", $sch_brand_name);
$smarty->assign("sch_date_name", $sch_date_name);

# 날짜 정리 및 리스트 Init
$commerce_date_list         = [];
$sales_tmp_total_list       = [];
$cost_tmp_total_list        = [];
$net_report_date_list       = [];
$net_parent_percent_list    = [];

$total_all_date_list        = [];
$total_report_table_list    = [];
$total_report_brand_list    = [];
$total_report_date_list     = [];
$nosp_result_list           = [];
$nosp_result_total          = 0;

$date_name_option   = getDateChartOption();
$nosp_result_init   = array("1" => array("count" => 0, "df_total" => 0, "n_total" => 0, "e_total" => 0, "total" => 0), "2" => array("count" => 0, "df_total" => 0, "n_total" => 0, "e_total" => 0, "total" => 0)); # 1: 타임보드, 2: 스폐셜DA

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($date = mysqli_fetch_array($all_date_query))
{
    $total_all_date_list[$date['date_key']] = array("s_date" => date("Y-m-d",strtotime($date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($date['date_key']))." 23:59:59");

    $chart_title = $date['chart_title'];
    if($sch_date_type == 'date'){
        $date_convert   = explode('_', $chart_title);
        $date_name      = isset($date_convert[1]) ? $date_name_option[$date_convert[1]] : "";
        $chart_title    = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
    }

    foreach($total_brand_column_list as $kind_key => $kind_title)
    {
        $total_report_table_list[$kind_key]['title'] = $kind_title;
        $total_report_table_list[$kind_key][$date['chart_key']] = array(
            "sales"     => 0,
            "cost"      => 0,
            "profit"    => 0
        );

        $total_report_brand_list[$kind_key] = array(
            "sales"     => 0,
            "cost"      => 0,
            "profit"    => 0
        );
    }

    foreach($chk_total_brand_option as $c_no)
    {
        $cost_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
        $sales_tmp_total_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
        $net_report_date_list[$c_no][$date['chart_key']][$date['date_key']] = 0;
        $net_parent_percent_list[$c_no] = 0;
    }

    if(!isset($commerce_date_list[$date['chart_key']]))
    {
        $commerce_date_list[$date['chart_key']] = $chart_title;
        $nosp_result_list[$date['chart_key']]   = $nosp_result_init;

        $total_report_date_list[$date['chart_key']] = array(
            "sales"     => 0,
            "cost"      => 0,
            "profit"    => 0,
            "average"   => 0
        );

        if($sch_date_type == "month"){
            $link_month     = date("Y-m", strtotime($date['date_key']));
            $link_e_day     = date("t", strtotime($link_month));
            $link_s_date    = "{$link_month}-01";
            $link_e_date    = "{$link_month}-{$link_e_day}";
            $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
        }
        elseif($sch_date_type == "week"){
            $link_s_date    = date("Y-m-d", strtotime($date['date_key']));
            $link_s_w       = date('w', strtotime($link_s_date));
            $link_e_w       = 7-$link_s_w;
            $link_e_date    = date("Y-m-d", strtotime("{$link_s_date} +{$link_e_w} days"));
            $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
        }
        else{
            $link_s_date    = date("Y-m-d", strtotime($date['date_key']));
            $commerce_link_date_list[$date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_s_date);
        }
    }
}

if($sch_base_type != "mil")
{
    # 수기 데이터
    $commerce_sql  = "
        SELECT
            cr.`type`,
            cr.price as price,
            cr.brand,
            DATE_FORMAT(sales_date, '%Y%m%d') as sales_date,
            {$add_comm_column} as key_date
        FROM commerce_report `cr`
        WHERE {$add_comm_where}
        ORDER BY key_date ASC
    ";
    $commerce_query = mysqli_query($my_db, $commerce_sql);
    while($commerce = mysqli_fetch_assoc($commerce_query))
    {
        if($commerce['type'] == 'cost'){
            $cost_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
        }elseif($commerce['type'] == 'sales'){
            $sales_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
        }
    }

    # 배송리스트 데이터
    $commerce_fee_option         = getCommerceFeeOption();
    $cms_delivery_chk_fee_list   = [];
    $cms_delivery_chk_nuzam_list = [];
    $cms_delivery_chk_ord_list   = [];
    $cms_delivery_chk_list       = [];
    $cms_delivery_chk_avg_list   = [];
    $self_dp_company_list        = getSelfDpCompanyList();

    foreach($total_all_date_list as $date_data)
    {
        $cms_report_sql      = "
            SELECT
                c_no,
                dp_c_no,
                order_number,
                DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
                {$add_cms_column} as key_date, 
                unit_price,
                (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
                unit_delivery_price,
                (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
            FROM work_cms as w
            WHERE {$add_cms_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}') 
        ";
        $cms_report_query = mysqli_query($my_db, $cms_report_sql);

        while($cms_report = mysqli_fetch_assoc($cms_report_query))
        {
            $commerce_fee_total_list    = isset($commerce_fee_option[$cms_report['c_no']]) ? $commerce_fee_option[$cms_report['c_no']] : [];
            $commerce_fee_per_list      = !empty($commerce_fee_total_list) && isset($commerce_fee_total_list[$cms_report['dp_c_no']]) ? $commerce_fee_total_list[$cms_report['dp_c_no']] : 0;
            $commerce_fee_per           = 0;

            if(!empty($commerce_fee_per_list))
            {
                foreach($commerce_fee_per_list as $fee_date => $fee_val){
                    if($cms_report['sales_date'] >= $fee_date){
                        $commerce_fee_per = $fee_val;
                    }
                }
            }

            $cms_report_fee             = $cms_report['unit_price']*($commerce_fee_per/100);
            $total_price                = $cms_report['unit_price'] - $cms_report_fee;

            $sales_tmp_total_list[$cms_report['c_no']][$cms_report['key_date']][$cms_report['sales_date']] += $total_price;

            if(in_array($cms_report['dp_c_no'], $dp_self_imweb_list)) {
                $cost_tmp_total_list[$cms_report['c_no']][$cms_report['key_date']][$cms_report['sales_date']] += $cms_report['coupon_price'];
            }

            if(in_array($cms_report['dp_c_no'], $self_dp_company_list) && $cms_report['unit_price'] > 0)
            {
                $brand_key = "";
                if($sch_base_type == "all")
                {
                    if(in_array($cms_report["c_no"], $doc_brand_option)){
                        $brand_key = "doc";
                    }
                    elseif(in_array($cms_report["c_no"], $nuzam_brand_option)){
                        $brand_key = "nuzam";
                    }
                    elseif(in_array($cms_report["c_no"], $ilenol_brand_option)){
                        $brand_key = "ilenol";
                    }
                    elseif(in_array($cms_report["c_no"], $city_brand_option)){
                        $brand_key = "city";
                    }
                }
                elseif($sch_base_type == "doc")
                {
                    if(in_array($cms_report['brand'], $doc_filter_option)){
                        $brand_key = "1314";
                    }elseif(in_array($cms_report['brand'], $doc_cubing_option)){
                        $brand_key = "3386";
                    }else{
                        $brand_key = $cms_report['brand'];
                    }
                }
                elseif($sch_base_type == "nuzam")
                {
                    $brand_key =  $cms_report["c_no"];
                }

                if(!empty($brand_key)){
                    $cms_delivery_chk_avg_list[$brand_key][$cms_report['key_date']][$cms_report['order_number']] += $cms_report['unit_price'];
                    $cms_delivery_chk_avg_list["total"][$cms_report['key_date']][$cms_report['order_number']] += $cms_report['unit_price'];
                }
            }

            if($cms_report['sales_date'] >= 20240101)
            {
                if ($cms_report['unit_delivery_price'] > 0) {
                    $cms_delivery_chk_fee_list[$cms_report['order_number']] = 1;
                }
                elseif($cms_report['unit_delivery_price'] == 0 && $cms_report['unit_price'] > 0)
                {
                    $cms_delivery_chk_list[$cms_report['order_number']][$cms_report['dp_c_no']][$cms_report['key_date']][$cms_report['sales_date']] = $cms_report['deli_cnt'];
                    $cms_delivery_chk_ord_list[$cms_report['order_number']][$cms_report['c_no']] = $cms_report['c_no'];

                    if ($cms_report['c_no'] == '2827' || $cms_report['c_no'] == '4878') {
                        $cms_delivery_chk_nuzam_list[$cms_report['order_number']] = 1;
                    }
                }
            }
        }
    }

    foreach($cms_delivery_chk_avg_list as $brand_key => $brand_ord_data)
    {
        foreach($brand_ord_data as $key_date => $avg_ord_data)
        {
            $total_ord_price    = 0;
            $total_ord_cnt      = count($avg_ord_data);
            foreach($avg_ord_data as $order_no => $ord_price){
                $total_ord_price += $ord_price;
            }

            if($brand_key == "total"){
                $total_report_date_list[$key_date]['average'] = ($total_ord_cnt > 0) ? round($total_ord_price/$total_ord_cnt): 0;
            }else{
                $total_report_table_list[$brand_key][$key_date]['average'] = ($total_ord_cnt > 0) ? round($total_ord_price/$total_ord_cnt): 0;
            }
        }
    }

    # 무료 배송비 계산
    foreach($cms_delivery_chk_list as $chk_ord => $chk_ord_data)
    {
        if(isset($cms_delivery_chk_fee_list[$chk_ord])){
            continue;
        }

        $chk_delivery_price = isset($cms_delivery_chk_nuzam_list[$chk_ord]) ? 5000 : 3000;

        foreach ($chk_ord_data as $chk_dp_c_no => $chk_dp_data)
        {
            if(in_array($chk_dp_c_no, $global_dp_total_list)){
                $chk_delivery_price = 6000;
            }

            foreach ($chk_dp_data as $chk_key_date => $chk_key_data)
            {
                foreach ($chk_key_data as $chk_sales_date => $deli_cnt)
                {
                    $cal_delivery_price     = $chk_delivery_price * $deli_cnt;

                    $chk_brand_deli_price   = $cal_delivery_price;
                    $chk_brand_cnt          = count($cms_delivery_chk_ord_list[$chk_ord]);
                    $cal_brand_deli_price   = round($cal_delivery_price/$chk_brand_cnt);
                    $cal_brand_idx          = 1;
                    foreach($cms_delivery_chk_ord_list[$chk_ord] as $chk_c_no)
                    {
                        $cal_one_delivery_price  = $cal_brand_deli_price;
                        $chk_brand_deli_price   -= $cal_one_delivery_price;

                        if($cal_brand_idx == $chk_brand_cnt){
                            $cal_one_delivery_price += $chk_brand_deli_price;
                        }

                        $cost_tmp_total_list[$chk_c_no][$chk_key_date][$chk_sales_date] += $cal_one_delivery_price;

                        $cal_brand_idx++;
                    }
                }
            }
        }
    }

    # 입/출고요청 리스트
    $quick_report_sql      = "
        SELECT 
            c_no,
            run_c_no,
            run_c_name,
            order_number,
            DATE_FORMAT(run_date, '%Y%m%d') as sales_date,
            {$add_quick_column} as key_date, 
            unit_price
        FROM work_cms_quick as w
        WHERE {$add_quick_where}
    ";
    $quick_report_query = mysqli_query($my_db, $quick_report_sql);
    while($quick_report_result = mysqli_fetch_assoc($quick_report_query))
    {
        $sales_tmp_total_list[$quick_report_result['c_no']][$quick_report_result['key_date']][$quick_report_result['sales_date']] += $quick_report_result['unit_price'];
    }

    # NOSP 계산
    $nosp_df_option     = array(1314);
    $nosp_nuzam_option  = array(2827,4878);
    $nosp_result_sql    = "
            SELECT
            `ar`.am_no,
            `ar`.brand,
            `am`.product,
            `am`.fee_per,
            `am`.price,
            `ar`.impressions,
            `ar`.click_cnt,
            `ar`.adv_price,
            DATE_FORMAT(am.adv_s_date, '%Y%m%d') as adv_s_day,
            {$add_result_column} as key_date
        FROM advertising_result `ar`
        LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
        WHERE {$add_result_where}
        ORDER BY `am`.adv_s_date ASC
    ";
    $nosp_result_query      = mysqli_query($my_db, $nosp_result_sql);
    $nosp_result_tmp_list   = [];
    $nosp_result_chk_list   = [];
    while($nosp_result = mysqli_fetch_assoc($nosp_result_query))
    {
        $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["product"]          = $nosp_result['product'];
        $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["sales_date"]       = $nosp_result['adv_s_day'];
        $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["key_date"]         = $nosp_result['key_date'];
        $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["fee_per"]          = $nosp_result['fee_per'];
        $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_adv_price"]  += $nosp_result['adv_price'];
    }

    foreach($nosp_result_tmp_list as $am_no => $nosp_result_tmp)
    {
        foreach($nosp_result_tmp as $brand => $nosp_brand_data)
        {
            $fee_price      = $nosp_brand_data['total_adv_price']*($nosp_brand_data['fee_per']/100);
            $cal_price      = $nosp_brand_data["total_adv_price"]-$fee_price;
            $cal_price_vat  = $cal_price*1.1;
            $chk_brand      = "e_total";

            if(array_search($brand, $nosp_df_option) !== false){
                $chk_brand = "df_total";
            }elseif(array_search($brand, $nosp_nuzam_option) !== false){
                $chk_brand = "n_total";
            }

            $nosp_result_list[$nosp_brand_data['key_date']][$nosp_brand_data['product']]['total']       += $cal_price_vat;
            $nosp_result_list[$nosp_brand_data['key_date']][$nosp_brand_data['product']][$chk_brand]    += $cal_price_vat;

            if(!isset($nosp_result_chk_list[$am_no])){
                $nosp_result_chk_list[$am_no] = 1;
                $nosp_result_list[$nosp_brand_data['key_date']][$nosp_brand_data['product']]['count']++;
                $nosp_result_total++;
            }

            $cost_tmp_total_list[$brand][$nosp_brand_data['key_date']][$nosp_brand_data['sales_date']] += $cal_price_vat;
        }
    }

    foreach($chk_total_brand_option as $c_no)
    {
        # NET 매출 관련 작업
        $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_report_net WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$sch_net_date}' ORDER BY sales_date DESC LIMIT 1;";
        $net_pre_percent_query  = mysqli_query($my_db, $net_pre_percent_sql);
        $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
        $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
        $net_parent             = isset($net_pre_percent_result['sales_date']) ? $net_pre_percent_result['sales_date'] : 0;

        $net_parent_percent_list[$c_no] = $net_pre_percent;

        $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date, {$add_net_column} as key_date FROM commerce_report_net WHERE brand='{$c_no}' {$add_net_where} ORDER BY sales_date ASC";
        $net_report_query       = mysqli_query($my_db, $net_report_sql);
        while($net_report = mysqli_fetch_assoc($net_report_query))
        {
            $net_report_date_list[$c_no][$net_report['key_date']][$net_report['sales_date']] = $net_report['percent'];
        }
    }
}
else
{
    # 수기 데이터
    $commerce_sql  = "
        SELECT
            cr.`type`,
            cr.price as price,
            cr.brand,
            DATE_FORMAT(sales_date, '%Y%m%d') as sales_date,
            {$add_comm_column} as key_date
        FROM commerce_report `cr`
        WHERE {$add_comm_where}
        ORDER BY key_date ASC
    ";
    $commerce_query = mysqli_query($mil_db, $commerce_sql);
    while($commerce = mysqli_fetch_assoc($commerce_query))
    {
        if($commerce['type'] == 'cost'){
            $cost_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
        }elseif($commerce['type'] == 'sales'){
            $sales_tmp_total_list[$commerce['brand']][$commerce['key_date']][$commerce['sales_date']] += $commerce['price'];
        }
    }

    # 배송리스트 데이터
    $commerce_fee_option        = getMilCommerceFeeOption();
    $delivery_chk_option        = getMilDeliveryChkOption();
    $delivery_price_option      = getMilDeliveryPriceOption();
    $cms_delivery_chk_avg_list  = [];
    
    $cms_report_sql      = "
        SELECT
            c_no,
            dp_c_no, 
            dp_c_name, 
            prd_no,
            order_number,
            DATE_FORMAT(order_date, '%Y%m%d') as sales_date,
            {$add_cms_column} as key_date, 
            (unit_price * org_price) as unit_price,
            (coupon_price * org_price) as coupon_price,
            (point_price * org_price) as point_price,
            (unit_delivery_price * org_price) as unit_delivery_price
        FROM work_cms as w
        WHERE {$add_cms_where}
        ORDER BY key_date ASC
    ";
    $cms_report_query = mysqli_query($mil_db, $cms_report_sql);
    
    while($cms_report = mysqli_fetch_assoc($cms_report_query))
    {
        if($cms_report['dp_c_no'] == "19"){
            $cms_report['c_no'] = "23";
        }

        $commerce_fee_list   = isset($commerce_fee_option[$cms_report['c_no']]) ? $commerce_fee_option[$cms_report['c_no']] : [];
        $cms_report_fee_per  = !empty($commerce_fee_list) && isset($commerce_fee_list[$cms_report['dp_c_no']]) ? $commerce_fee_list[$cms_report['dp_c_no']] : 0;
        $cms_report_fee      = $cms_report['unit_price']*($cms_report_fee_per/100);
        $total_price         = $cms_report['unit_price'] - $cms_report_fee;

        $sales_tmp_total_list[$cms_report['c_no']][$cms_report['key_date']][$cms_report['sales_date']] += $total_price;
        $cost_tmp_total_list[$cms_report['c_no']][$cms_report['key_date']][$cms_report['sales_date']]  += ($cms_report['coupon_price']+$cms_report['point_price']);

        if(in_array($cms_report['dp_c_no'], array("2","3","19")) && $cms_report['unit_price'] > 0)
        {
            $cms_delivery_chk_avg_list[$cms_report["c_no"]][$cms_report['key_date']][$cms_report['order_number']] += $cms_report['unit_price'];
            $cms_delivery_chk_avg_list["total"][$cms_report['key_date']][$cms_report['order_number']] += $cms_report['unit_price'];
        }
    }

    foreach($cms_delivery_chk_avg_list as $brand_key => $brand_ord_data)
    {
        foreach($brand_ord_data as $key_date => $avg_ord_data)
        {
            $total_ord_price    = 0;
            $total_ord_cnt      = count($avg_ord_data);
            foreach($avg_ord_data as $order_no => $ord_price){
                $total_ord_price += $ord_price;
            }

            if($brand_key == "total"){
                $total_report_date_list[$key_date]['average'] = ($total_ord_cnt > 0) ? round($total_ord_price/$total_ord_cnt): 0;
            }else{
                $total_report_table_list[$brand_key][$key_date]['average'] = ($total_ord_cnt > 0) ? round($total_ord_price/$total_ord_cnt): 0;
            }
        }
    }

    # 반품/교환 리스트
    $return_sql = "
        SELECT 
            c_no,
            {$add_return_column} as key_date,
            DATE_FORMAT(return_date, '%Y%m%d') as return_date,
            return_price
        FROM csm_return 
        WHERE {$add_return_where}
        GROUP BY order_number, return_type
        ORDER BY return_date
    ";
    $return_query = mysqli_query($mil_db, $return_sql);
    while($return_result = mysqli_fetch_assoc($return_query))
    {
        $sales_tmp_total_list[$return_result['c_no']][$return_result['key_date']][$return_result['return_date']] -= $return_result['return_price'];
    }

    # 업무리스트 비용(자동)
    $work_sql = "
        SELECT
            w.c_no,
            w.prd_no,
            (SELECT sub.k_name_code FROM product sub WHERE sub.prd_no=w.prd_no) as prd_g1,
            {$add_work_column} as key_date, 
            DATE_FORMAT(w.task_run_regdate, '%Y%m%d') as sales_date, 
            wd_price as wd_price_total, 
            dp_price as dp_price_total
        FROM `work` w
        WHERE {$add_work_where} AND c_no > 0
        ORDER BY sales_date
    ";
    $work_query = mysqli_query($mil_db, $work_sql);
    while($work_result = mysqli_fetch_assoc($work_query))
    {
        $dp_price_total = $work_result['dp_price_total'];
        $wd_price_total = $work_result['wd_price_total'];

        if($work_result['prd_no'] == '4' && $dp_price_total > 0){
            $dp_price_total = $dp_price_total+($dp_price_total*0.035);
            $sales_tmp_total_list[$work_result['c_no']][$work_result['key_date']][$work_result['sales_date']] += $dp_price_total;
        }

        $cost_tmp_total_list[$work_result['c_no']][$work_result['key_date']][$work_result['sales_date']] += $wd_price_total;
    }

    foreach($chk_total_brand_option as $c_no)
    {
        # NET 매출 관련 작업
        $net_pre_percent_sql    = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date FROM commerce_net_percent WHERE brand='{$c_no}' AND DATE_FORMAT(sales_date, '%Y-%m-%d') < '{$sch_net_date}' ORDER BY sales_date DESC LIMIT 1;";
        $net_pre_percent_query  = mysqli_query($mil_db, $net_pre_percent_sql);
        $net_pre_percent_result = mysqli_fetch_assoc($net_pre_percent_query);
        $net_pre_percent        = isset($net_pre_percent_result['percent']) ? $net_pre_percent_result['percent'] : 0;
        $net_parent             = isset($net_pre_percent_result['sales_date']) ? $net_pre_percent_result['sales_date'] : 0;

        $net_parent_percent_list[$c_no] = $net_pre_percent;

        $net_report_sql         = "SELECT `percent`, DATE_FORMAT(sales_date, '%Y%m%d') as sales_date, {$add_net_column} as key_date FROM commerce_net_percent WHERE brand='{$c_no}' {$add_net_where} ORDER BY sales_date ASC";
        $net_report_query       = mysqli_query($mil_db, $net_report_sql);
        while($net_report = mysqli_fetch_assoc($net_report_query))
        {
            $net_report_date_list[$c_no][$net_report['key_date']][$net_report['sales_date']] = $net_report['percent'];
        }
    }
}

foreach($net_report_date_list as $c_no => $net_company_data)
{
    $cal_net_percent = $net_parent_percent_list[$c_no];
    $parent_key      = "";

    if($sch_base_type == "all")
    {
        if(in_array($c_no, $doc_brand_option)){
            $parent_key = "doc";
        }
        elseif(in_array($c_no, $nuzam_brand_option)){
            $parent_key = "nuzam";
        }
        elseif(in_array($c_no, $ilenol_brand_option)){
            $parent_key = "ilenol";
        }
        elseif(in_array($c_no, $city_brand_option)){
            $parent_key = "city";
        }
    }
    elseif($sch_base_type == "doc")
    {
        if(in_array($c_no, $doc_filter_option)){
            $parent_key = "1314";
        }elseif(in_array($c_no, $doc_cubing_option)){
            $parent_key = "3386";
        }else{
            $parent_key = $c_no;
        }
    }
    elseif($sch_base_type == "nuzam")
    {
        $parent_key = $c_no;
    }
    elseif($sch_base_type == "mil")
    {
        $parent_key = $c_no;
    }

    if(!empty($parent_key))
    {
        foreach($net_company_data as $key_date => $net_report_data)
        {
            foreach($net_report_data as $sales_date => $net_percent_val)
            {
                if($net_percent_val > 0 ){
                    $cal_net_percent = $net_percent_val;
                }

                $net_percent = $cal_net_percent/100;
                $sales_total = round($sales_tmp_total_list[$c_no][$key_date][$sales_date]);
                $cost_total  = round($cost_tmp_total_list[$c_no][$key_date][$sales_date]);
                $net_price   = $sales_total > 0 ? (int)($sales_total*$net_percent) : 0;
                $profit      = $cost_total > 0 ? ($net_price-$cost_total) : $net_price;

                $total_report_table_list[$parent_key][$key_date]['sales']     += $sales_total;
                $total_report_table_list[$parent_key][$key_date]['cost']      += $cost_total;
                $total_report_table_list[$parent_key][$key_date]['profit']    += $profit;

                $total_report_brand_list[$parent_key]['sales']  += $sales_total;
                $total_report_brand_list[$parent_key]['cost']   += $cost_total;
                $total_report_brand_list[$parent_key]['profit'] += $profit;

                $total_report_date_list[$key_date]['sales']     += $sales_total;
                $total_report_date_list[$key_date]['cost']      += $cost_total;
                $total_report_date_list[$key_date]['profit']    += $profit;
            }
        }
    }
}

$brand_chart_name_list      = array_reverse($total_brand_column_list);
$total_report_brand_list    = array_reverse($total_report_brand_list);
$brand_chart_list           = [];
foreach($total_report_brand_list as $brand_key => $brand_data) {
    $brand_chart_list['sales_data']['title']    = "매출";
    $brand_chart_list['sales_data']['color']    = "rgba(0,0,255,0.8)";
    $brand_chart_list['sales_data']['data'][]   = $brand_data['sales'];
    $brand_chart_list['cost_data']['title']     = "비용";
    $brand_chart_list['cost_data']['color']     = "rgba(255,0,0,0.8)";
    $brand_chart_list['cost_data']['data'][]    = $brand_data['cost'];
    $brand_chart_list['profit_data']['title']   = "공헌이익";
    $brand_chart_list['profit_data']['color']   = "rgba(0,0,0,0.8)";
    $brand_chart_list['profit_data']['data'][]  = $brand_data['profit'];
}

$date_chart_name_list  = $commerce_date_list;
$date_chart_list       = [];
foreach($total_report_date_list as $date_key => $date_data)
{
    $date_chart_list['sales_data']['title']    = "매출";
    $date_chart_list['sales_data']['color']    = "rgba(0,0,255,0.8)";
    $date_chart_list['sales_data']['data'][]   = $date_data['sales'];
    $date_chart_list['cost_data']['title']     = "비용";
    $date_chart_list['cost_data']['color']     = "rgba(255,0,0,0.8)";
    $date_chart_list['cost_data']['data'][]    = $date_data['cost'];
    $date_chart_list['profit_data']['title']   = "공헌이익";
    $date_chart_list['profit_data']['color']   = "rgba(0,0,0,0.8)";
    $date_chart_list['profit_data']['data'][]  = $date_data['profit'];
}

$smarty->assign("sch_brand_url", $sch_brand_url);
$smarty->assign("commerce_date_list", $commerce_date_list);
$smarty->assign("commerce_link_date_list", $commerce_link_date_list);
$smarty->assign("total_report_table_list", $total_report_table_list);
$smarty->assign("total_report_date_list", $total_report_date_list);
$smarty->assign("nosp_result_total", $nosp_result_total);
$smarty->assign("nosp_result_list", $nosp_result_list);
$smarty->assign("brand_chart_name_list", json_encode($brand_chart_name_list));
$smarty->assign("brand_chart_list", json_encode($brand_chart_list));
$smarty->assign("date_chart_name_list", json_encode($date_chart_name_list));
$smarty->assign("date_chart_list", json_encode($date_chart_list));

$smarty->display('wise_bm/total_main_base_iframe_cms_report.html');
?>
