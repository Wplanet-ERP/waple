<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_date.php');
require('../inc/helper/advertising.php');
require('../inc/helper/product_cms.php');
require('../inc/helper/logistics.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');
require('../inc/model/MyQuick.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

$bm_brand_option    = getBrandDetailOption();
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sel_brand_data     = isset($bm_brand_option[$sch_brand]) ? $bm_brand_option[$sch_brand] : [];
$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "week";

if(empty($sel_brand_data)){
    exit("<script>alert('접근 권한이 없습니다.');location.href='/v1/main.php';</script>");
}

# Navigation & My Quick
$nav_prd_no     = $sel_brand_data['nav_no'];
$bm_type_name   = $sel_brand_data['title'];
$nav_title      = "WISE BM :: {$bm_type_name}";
$quick_model    = MyQuick::Factory();
$is_my_quick    = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_date_type", $sch_date_type);
$smarty->assign("bm_type_name", $bm_type_name);
$smarty->assign("bm_date_type_option", getBMDateTypeOption());
$smarty->assign("traffic_date_type_option", array("day" => "일별", "week" => "주별"));

# 브랜드 변수
$brand_manager      = $sel_brand_data['manager'];
$brand_manager_name = $sel_brand_data['manager_name'];
$cms_brand_option   = $sel_brand_data['brand_list'];
$sch_brand_url      = $sel_brand_data['brand_url'];
$cms_brand_text     = implode(",", $cms_brand_option);
$main_log_company   = $sel_brand_data['log_company'];
$main_brand_list    = $sel_brand_data['main_brand'];
$main_brand_text    = implode(",",$sel_brand_data['main_brand']);
$main_brand         = $main_brand_list[0];
$dp_except_list     = getNotApplyDpList();
$dp_except_text     = implode(",", $dp_except_list);
$ilenol_dp_list     = array("5661", "1816");
$ilenol_dp_text     = implode(",", $ilenol_dp_list);
$ilenol_kind_list   = array('09057','09058','09076','09077','08028','08030');
$ilenol_kind_text   = implode(",", $ilenol_kind_list);

$smarty->assign("brand_manager", $brand_manager);
$smarty->assign("brand_manager_name", $brand_manager_name);
$smarty->assign("sch_brand_url", $sch_brand_url);

# 사용 변수
$date_name_option   = getDateChartOption();
$daily_hour_option  = getHourOption();
$prev_date          = date('Y-m-d', strtotime("-1 days"));
$sch_e_date         = date('Y-m-d');
$sch_s_date         = date('Y-m-d', strtotime("{$sch_e_date} -10 days"));
$sch_s_datetime     = "{$sch_s_date} 00:00:00";
$sch_e_datetime     = "{$sch_e_date} 23:59:59";

$commerce_date_list = [];
$cost_total_list    = [];
$sales_total_list   = [];
$cost_total_init    = array(
    "total"     => 0,
    "delivery"  => 0,
    "09057"     => 0,
    "09058"     => 0,
    "09076"     => 0,
    "09077"     => 0,
);

$sales_total_init   = array(
    "total"     => 0,
    "cms"       => 0,
    "08028"     => 0,
    "08030"     => 0,
);


$all_date_sql = "
	SELECT
 		DATE_FORMAT(`allday`.Date, '%m/%d_%w') as chart_title,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as chart_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'
	ORDER BY chart_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($date = mysqli_fetch_array($all_date_query))
{
    $chart_title    = $date['chart_title'];
    $date_convert   = explode('_', $chart_title);
    $date_name      = isset($date_convert[1]) ? $date_name_option[$date_convert[1]] : "";
    $chart_title    = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];

    $commerce_date_list[$date['chart_key']] = $chart_title;

    foreach($ilenol_dp_list as $ilenol_dp){
        $cost_total_list[$ilenol_dp][$date['chart_key']]    = $cost_total_init;
        $sales_total_list[$ilenol_dp][$date['chart_key']]   = $sales_total_init;
    }
}

# 수기 데이터
$commerce_sql  = "
    SELECT
        cr.`type`,
        cr.price as price,
        cr.brand,
        DATE_FORMAT(sales_date, '%Y%m%d') as key_date,
        cr.k_name_code
    FROM commerce_report `cr`
    WHERE display='1' AND brand='{$main_brand}' AND k_name_code IN({$ilenol_kind_text}) AND DATE_FORMAT(sales_date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'
    ORDER BY key_date ASC
";
$commerce_query = mysqli_query($my_db, $commerce_sql);
while($commerce = mysqli_fetch_assoc($commerce_query))
{
    if($commerce['type'] == 'cost')
    {
        $comm_dp_no = "";
        $comm_kind = $commerce['k_name_code'];

        switch($comm_kind){
            case '09057':
                $comm_dp_no = "5661";
                break;
            case '09058':
                $comm_dp_no = "1816";
                break;
            case '09076':
                $comm_dp_no = "1816";
                break;
            case '09077':
                $comm_dp_no = "1816";
                break;
        }

        if(!empty($comm_dp_no))
        {
            $cost_total_list[$comm_dp_no][$commerce["key_date"]][$comm_kind]    += $commerce['price'];
            $cost_total_list[$comm_dp_no][$commerce["key_date"]]["total"]       += $commerce['price'];
        }

    }
    elseif($commerce['type'] == 'sales')
    {
        $comm_dp_no = "";
        $comm_kind = $commerce['k_name_code'];

        switch($comm_kind){
            case '08028':
                $comm_dp_no = "5661";
                break;
            case '08030':
                $comm_dp_no = "1816";
                break;
        }

        if(!empty($comm_dp_no))
        {
            $sales_total_list[$comm_dp_no][$commerce["key_date"]][$comm_kind]   += $commerce['price'];
            $sales_total_list[$comm_dp_no][$commerce["key_date"]]["total"]      += $commerce['price'];
        }
    }
}

# 매출 자동
$cms_delivery_chk_list      = [];
$cms_delivery_chk_ord_list  = [];

$cms_report_where   = "w.delivery_state='4' AND w.dp_c_no IN({$ilenol_dp_text}) AND (w.order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}') AND w.c_no IN({$cms_brand_text})";
$cms_report_sql     = "
    SELECT
        dp_c_no,
        order_number,
        DATE_FORMAT(order_date, '%Y%m%d') as key_date,
        unit_price,
        unit_delivery_price,
        (SELECT COUNT(DISTINCT wcd.delivery_no) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) as deli_cnt
    FROM work_cms as w
    WHERE {$cms_report_where}
";
$cms_report_query = mysqli_query($my_db, $cms_report_sql);
while($cms_report = mysqli_fetch_assoc($cms_report_query))
{
    $sales_total_list[$cms_report["dp_c_no"]][$cms_report["key_date"]]["cms"]   += $cms_report['unit_price'];
    $sales_total_list[$cms_report["dp_c_no"]][$cms_report["key_date"]]["total"] += $cms_report['unit_price'];

    $cms_delivery_chk_list[$cms_report['order_number']][$cms_report['dp_c_no']][$cms_report['key_date']] = $cms_report['deli_cnt'];
    $cms_delivery_chk_ord_list[$cms_report['order_number']][$cms_report['c_no']] = $cms_report['c_no'];
}

foreach($cms_delivery_chk_list as $chk_ord => $chk_ord_data)
{
    $chk_delivery_price = 6000;

    foreach ($chk_ord_data as $chk_dp_c_no => $chk_dp_data)
    {
        foreach ($chk_dp_data as $chk_key_date => $deli_cnt)
        {
            $cal_delivery_price  = $chk_delivery_price * $deli_cnt;

            $cost_total_list[$chk_dp_c_no][$chk_key_date]['delivery'] += $cal_delivery_price;
        }
    }
}

# 요일/시간별 판매실적 현황/비교
$daily_s_date               = date("Y-m-d", strtotime("{$prev_date} -6 days"));
$daily_e_date               = $prev_date;
$daily_s_datetime           = "{$daily_s_date} 00:00:00";
$daily_e_datetime           = "{$daily_e_date} 23:59:59";
$daily_s_date_tmp           = date("m/d_w", strtotime($daily_s_date));
$daily_e_date_tmp           = date("m/d_w", strtotime($daily_e_date));

$daily_s_date_convert       = explode('_', $daily_s_date_tmp);
$daily_s_date_name          = $date_name_option[$daily_s_date_convert[1]];
$daily_s_date_text          = $daily_s_date_convert[0]."({$daily_s_date_name})";
$daily_e_date_convert       = explode('_', $daily_e_date_tmp);
$daily_e_date_name          = $date_name_option[$daily_e_date_convert[1]];
$daily_e_date_text          = $daily_e_date_convert[0]."({$daily_e_date_name})";

$prev_daily_s_date          = date("Y-m-d", strtotime("{$daily_s_date} -1 weeks"));
$prev_daily_e_date          = date("Y-m-d", strtotime("{$daily_e_date} -1 weeks"));
$prev_daily_s_datetime      = "{$prev_daily_s_date} 00:00:00";
$prev_daily_e_datetime      = "{$prev_daily_e_date} 23:59:59";
$prev_daily_s_date_tmp      = date("m/d_w", strtotime($prev_daily_s_date));
$prev_daily_e_date_tmp      = date("m/d_w", strtotime($prev_daily_e_date));

$prev_daily_s_date_convert  = explode('_', $prev_daily_s_date_tmp);
$prev_daily_s_date_name     = $date_name_option[$prev_daily_s_date_convert[1]];
$prev_daily_s_date_text     = $prev_daily_s_date_convert[0]."({$prev_daily_s_date_name})";
$prev_daily_e_date_convert  = explode('_', $prev_daily_e_date_tmp);
$prev_daily_e_date_name     = $date_name_option[$prev_daily_e_date_convert[1]];
$prev_daily_e_date_text     = $prev_daily_e_date_convert[0]."({$prev_daily_e_date_name})";

$daily_date_list            = [];
$daily_hour_list            = [];
$daily_sales_list           = [];
$daily_date_total_list      = [];
$daily_hour_total_list      = array("max" => 0);
$daily_sales_total_price    = [];

$sales_term_hour_list       = [];
$sales_term_date_list       = [];
$sales_term_total_list      = [];
$sales_term_total_init      = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0, "hour_base_max" => 0, "hour_comp_max" => 0, "date_base_max" => 0, "date_comp_max" => 0, "date_base_min" => 0, "date_comp_min" => 0);

foreach($daily_hour_option as $hour){
    $hour_key = (int)$hour;
    $daily_hour_list[$hour_key]     = "{$hour}시";
    foreach($ilenol_dp_list as $ilenol_dp)
    {
        $sales_term_hour_list[$ilenol_dp][$hour_key]= array("base_price" => 0,"comp_price" => 0, "comp_per" => 0);
    }
}

foreach($ilenol_dp_list as $ilenol_dp)
{
    foreach($date_name_option as $date_key => $date_title)
    {
        $daily_date_list[$date_key]                     = $date_title;
        $daily_sales_list[$ilenol_dp][$date_key]['max'] = 0;
        $daily_date_total_list[$ilenol_dp][$date_key]   = 0;
        $sales_term_date_list[$ilenol_dp][$date_key]    = array("base_price" => 0, "comp_price" => 0, "comp_per" => 0);

        foreach($daily_hour_list as $hour_key)
        {
            $daily_sales_list[$ilenol_dp][$date_key][$hour_key] = 0;
            $daily_hour_total_list[$ilenol_dp][$hour_key]       = 0;
        }
    }
    $daily_sales_total_price[$ilenol_dp]    = 0;
    $sales_term_total_list[$ilenol_dp]      = $sales_term_total_init;
}


$add_sales_daily_where  = "w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN({$ilenol_dp_text}) AND (order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}') AND w.c_no IN({$cms_brand_text})";
$add_sales_term_where   = "w.delivery_state='4' AND w.unit_price > 0 AND w.dp_c_no IN({$ilenol_dp_text}) AND (order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}' OR order_date BETWEEN '{$prev_daily_s_datetime}' AND '{$prev_daily_e_datetime}')  AND w.c_no IN({$cms_brand_text})";

# 요일/시간별 판매실적 현황 쿼리
$daily_sales_sql  = "
    SELECT
        w.dp_c_no,
        DATE_FORMAT(w.order_date, '%w') as date_key,
        DATE_FORMAT(w.order_date, '%H') as hour_key,
        SUM(unit_price) as total_price
    FROM work_cms w 
    WHERE {$add_sales_daily_where}
    GROUP BY date_key, hour_key, dp_c_no
";
$daily_sales_query    = mysqli_query($my_db, $daily_sales_sql);
while($daily_sales_result = mysqli_fetch_assoc($daily_sales_query))
{
    $daily_date_key     = $daily_sales_result['date_key'];
    $daily_hour_key     = (int)$daily_sales_result['hour_key'];
    $daily_total_price  = (int)$daily_sales_result['total_price'];
    $daily_max          = $daily_sales_list[$daily_sales_result['dp_c_no']][$daily_date_key]["max"];

    $daily_date_total_list[$daily_sales_result['dp_c_no']][$daily_date_key] += $daily_total_price;
    $daily_hour_total_list[$daily_sales_result['dp_c_no']][$daily_hour_key] += $daily_total_price;
    $daily_sales_total_price[$daily_sales_result['dp_c_no']]                += $daily_total_price;

    $daily_sales_list[$daily_sales_result['dp_c_no']][$daily_date_key][$daily_hour_key] += $daily_total_price;
    if($daily_max < $daily_total_price){
        $daily_sales_list[$daily_sales_result['dp_c_no']][$daily_date_key]["max"] = $daily_total_price;
    }
}

foreach($ilenol_dp_list as $ilenol_dp)
{
    $daily_date_total_list[$ilenol_dp]["min"] = min($daily_date_total_list[$ilenol_dp]);
    $daily_date_total_list[$ilenol_dp]["max"] = max($daily_date_total_list[$ilenol_dp]);
    $daily_hour_total_list[$ilenol_dp]["max"] = max($daily_hour_total_list[$ilenol_dp]);
}

# 요일/시간별 판매실적 비교 쿼리
$term_sales_sql    = "
    SELECT
        w.dp_c_no,
        DATE_FORMAT(w.order_date, '%w') as date_key,
        DATE_FORMAT(w.order_date, '%H') as hour_key,
        SUM(
            IF(order_date BETWEEN '{$daily_s_datetime}' AND '{$daily_e_datetime}',unit_price, 0)
        ) AS base_price,
        SUM(
            IF(order_date BETWEEN '{$prev_daily_s_datetime}' AND '{$prev_daily_e_datetime}',unit_price, 0)
        ) AS comp_price
    FROM work_cms w 
    WHERE {$add_sales_term_where}
    GROUP BY date_key, hour_key, dp_c_no
    ORDER BY order_date
";
$term_sales_query = mysqli_query($my_db, $term_sales_sql);
while($term_sales = mysqli_fetch_assoc($term_sales_query))
{
    $hour_key   = (int)$term_sales['hour_key'];
    $date_key   = (int)$term_sales['date_key'];
    $base_price = $term_sales['base_price'];
    $comp_price = $term_sales['comp_price'];

    $sales_term_hour_list[$term_sales['dp_c_no']][$hour_key]["base_price"] += $base_price;
    $sales_term_hour_list[$term_sales['dp_c_no']][$hour_key]["comp_price"] += $comp_price;
    $sales_term_date_list[$term_sales['dp_c_no']][$date_key]["base_price"] += $base_price;
    $sales_term_date_list[$term_sales['dp_c_no']][$date_key]["comp_price"] += $comp_price;

    $sales_term_total_list[$term_sales['dp_c_no']]["base_price"] += $base_price;
    $sales_term_total_list[$term_sales['dp_c_no']]["comp_price"] += $comp_price;
}

if(!empty($sales_term_total_list))
{
    foreach($ilenol_dp_list as $ilenol_dp)
    {
        $total_comp_per = ($sales_term_total_list[$ilenol_dp]["comp_price"] == 0) ? 0 : (($sales_term_total_list[$ilenol_dp]["base_price"] == 0) ? -100 : ROUND( ((($sales_term_total_list[$ilenol_dp]["base_price"]-$sales_term_total_list[$ilenol_dp]["comp_price"])/$sales_term_total_list[$ilenol_dp]["comp_price"]) *100), 1));
        $sales_term_total_list[$ilenol_dp]["comp_per"] = $total_comp_per;
    }

}

if(!empty($sales_term_hour_list))
{
    foreach($sales_term_hour_list as $dp_c_no => $dp_hour_data)
    {
        foreach($dp_hour_data as $hour_key => $hour_data)
        {
            $hour_base_price    = $hour_data["base_price"];
            $hour_comp_price    = $hour_data["comp_price"];
            $hour_base_max      = $sales_term_total_list[$dp_c_no]["hour_base_max"];
            $hour_comp_max      = $sales_term_total_list[$dp_c_no]["hour_comp_max"];

            $hour_comp_per = ($hour_comp_price == 0) ? 0 : (($hour_base_price == 0) ? -100 : ROUND( ((($hour_base_price-$hour_comp_price)/$hour_comp_price) *100), 1));
            $sales_term_hour_list[$dp_c_no][$hour_key]["comp_per"] = $hour_comp_per;

            if($hour_base_max < $hour_base_price){
                $sales_term_total_list[$dp_c_no]["hour_base_max"] = $hour_base_price;
            }

            if($hour_comp_max < $hour_comp_price){
                $sales_term_total_list[$dp_c_no]["hour_comp_max"] = $hour_comp_price;
            }
        }
    }
}

if(!empty($sales_term_date_list))
{
    $date_idx = 0;
    foreach($sales_term_date_list as $dp_c_no => $dp_date_data)
    {
        foreach($dp_date_data as $date_key => $date_data)
        {
            $date_base_price    = $date_data["base_price"];
            $date_comp_price    = $date_data["comp_price"];
            $date_base_max      = $sales_term_total_list[$dp_c_no]["date_base_max"];
            $date_base_min      = $sales_term_total_list[$dp_c_no]["date_base_min"];
            $date_comp_max      = $sales_term_total_list[$dp_c_no]["date_comp_max"];
            $date_comp_min      = $sales_term_total_list[$dp_c_no]["date_comp_min"];

            if($date_idx == 0){
                $sales_term_total_list[$dp_c_no]["date_base_min"] = $date_base_price;
                $sales_term_total_list[$dp_c_no]["date_comp_min"] = $date_comp_price;
            }

            $date_comp_per = ($date_comp_price == 0) ? 0 : (($date_base_price == 0) ? -100 : ROUND( ((($date_base_price-$date_comp_price)/$date_comp_price) *100), 1));
            $sales_term_date_list[$dp_c_no][$date_key]["comp_per"] = $date_comp_per;

            if($date_base_max < $date_base_price){
                $sales_term_total_list[$dp_c_no]["date_base_max"] = $date_base_price;
            }

            if($date_base_min > $date_base_price){
                $sales_term_total_list[$dp_c_no]["date_base_min"] = $date_base_price;
            }

            if($date_comp_max < $date_comp_price){
                $sales_term_total_list[$dp_c_no]["date_comp_max"] = $date_comp_price;
            }

            if($date_comp_min > $date_comp_price){
                $sales_term_total_list[$dp_c_no]["date_comp_min"] = $date_comp_price;
            }

            $date_idx++;
        }
    }
}

# 주문수 날짜 설정
$today_s_w		            = date('w')-1;
$today_s_week               = date('Y-m-d', strtotime("-{$today_s_w} days"));
$count_s_week               = date('Y-m-d', strtotime("{$today_s_week} -3 weeks"));
$count_e_week               = date("Y-m-d",strtotime("{$today_s_week} +6 days"));
$count_s_week_time          = $count_s_week." 00:00:00";
$count_e_week_time          = $count_e_week." 23:59:59";
$commerce_count_date_list   = [];
$cms_count_list             = [];
$cms_count_total_list       = [];

$count_date_sql = "
	SELECT
 		DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d') as chart_key,
 		CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d')) as chart_title
	FROM (
	    SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e
    )
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$count_s_week}' AND '{$count_e_week}'
	GROUP BY chart_key
	ORDER BY chart_key
";
$count_date_query = mysqli_query($my_db, $count_date_sql);
while($count_date = mysqli_fetch_array($count_date_query))
{
    $commerce_count_date_list[$count_date['chart_key']] = $count_date['chart_title'];
    $cms_count_list[$count_date['chart_key']]           = $ilenol_dp_list;

    foreach($ilenol_dp_list as $ilenol_dp){
        $cms_count_total_list[$ilenol_dp][$count_date['chart_key']] = 0;
    }
}

# 주문수 데이터 수집
$cms_count_where = "w.delivery_state='4' AND w.dp_c_no IN({$ilenol_dp_text}) AND (w.order_date BETWEEN '{$count_s_week_time}' AND '{$count_e_week_time}') AND w.c_no IN({$cms_brand_text}) AND unit_price > 0";
$cms_count_sql = "
    SELECT 
        dp_c_no,
        COUNT(DISTINCT order_number) as ord_cnt,
        DATE_FORMAT(DATE_SUB(order_date, INTERVAL(IF(DAYOFWEEK(order_date)=1,8,DAYOFWEEK(order_date))-2) DAY), '%Y%m%d') as key_date
    FROM work_cms as w
    WHERE {$cms_count_where}
    GROUP BY dp_c_no, key_date 
";
$cms_count_query = mysqli_query($my_db, $cms_count_sql);
while($cms_count = mysqli_fetch_assoc($cms_count_query))
{
    $cms_count_list[$cms_count['key_date']][$cms_count['dp_c_no']]          += $cms_count['ord_cnt'];
    $cms_count_total_list[$cms_count['dp_c_no']][$cms_count['key_date']]    += $cms_count['ord_cnt'];
}

$smarty->assign("cms_s_date", $sch_s_date);
$smarty->assign("cms_e_date", $sch_e_date);
$smarty->assign("commerce_date_list", $commerce_date_list);
$smarty->assign("cost_total_list", $cost_total_list);
$smarty->assign("sales_total_list", $sales_total_list);
$smarty->assign("daily_s_date", $daily_s_date);
$smarty->assign("daily_e_date", $daily_e_date);
$smarty->assign("daily_s_date_text", $daily_s_date_text);
$smarty->assign("daily_e_date_text", $daily_e_date_text);
$smarty->assign("prev_daily_s_date", $prev_daily_s_date);
$smarty->assign("prev_daily_e_date", $prev_daily_e_date);
$smarty->assign("prev_daily_s_date_text", $prev_daily_s_date_text);
$smarty->assign("prev_daily_e_date_text", $prev_daily_e_date_text);
$smarty->assign("daily_date_list", $daily_date_list);
$smarty->assign("daily_hour_list", $daily_hour_list);
$smarty->assign("daily_sales_total_price", $daily_sales_total_price);
$smarty->assign("daily_date_total_list", $daily_date_total_list);
$smarty->assign("daily_hour_total_list", $daily_hour_total_list);
$smarty->assign("daily_sales_list", $daily_sales_list);
$smarty->assign("sales_term_hour_list", $sales_term_hour_list);
$smarty->assign("sales_term_date_list", $sales_term_date_list);
$smarty->assign("sales_term_total_list", $sales_term_total_list);
$smarty->assign("count_s_week", $count_s_week);
$smarty->assign("count_e_week", $count_e_week);
$smarty->assign("commerce_count_date_list", $commerce_count_date_list);
$smarty->assign("commerce_count_date_cnt", count($commerce_count_date_list)+1);
$smarty->assign("count_chart_name_list", json_encode($commerce_count_date_list));
$smarty->assign("cms_count_list", $cms_count_list);
$smarty->assign("cms_count_total_list", json_encode($cms_count_total_list));

$smarty_html = "wise_bm/brand_main_global.html";

$smarty->display($smarty_html);
?>
