<?php
require('../inc/common.php');
require('../../libs/dbconfig_mil.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 검색조건
$main_brand_option      = getTotalMainBrandOption();
$total_brand_option     = getTotalBrandList();
$doc_brand_option       = getTotalDocBrandList();
$nuzam_brand_option     = getNuzamBrandList();
$ilenol_brand_option    = getIlenolBrandList();
$city_brand_option      = getCityBrandList();
$dp_self_imweb_list     = getSelfDpImwebCompanyList();
$total_brand_text       = implode(",", $total_brand_option);
$doc_brand_text         = implode(",", $doc_brand_option);
$nuzam_brand_text       = implode(",", $nuzam_brand_option);
$dp_self_imweb_text     = implode(",", $dp_self_imweb_list);
$doc_filter_option      = getDocFilterOption();
$doc_cubing_option      = getDocCubingOption();

# 마지막 날짜
$imweb_max_convert_date_sql     = "SELECT convert_date FROM commerce_conversion WHERE dp_c_no > 0 AND brand > 0 ORDER BY convert_date DESC LIMIT 1";
$imweb_max_convert_date_query   = mysqli_query($my_db, $imweb_max_convert_date_sql);
$imweb_max_convert_date_result  = mysqli_fetch_assoc($imweb_max_convert_date_query);
$imweb_max_convert_date         = isset($imweb_max_convert_date_result['convert_date']) ? date("Y-m-d", strtotime($imweb_max_convert_date_result['convert_date'])) : date('Y-m-d', strtotime("-3 days"));

# 날짜 체크
$add_where      = "brand > 0 AND dp_c_no > 0";
$add_cms_where  = "w.delivery_state = '4' AND w.unit_price > 0 AND w.page_idx > 0 AND dp_c_no IN({$dp_self_imweb_text})";
$add_nosp_where = "`am`.state IN(3,5) AND `am`.product IN(1,2)";
$sch_e_day      = $imweb_max_convert_date;
$sch_s_day      = date('Y-m-d', strtotime("{$sch_e_day} -14 days"));
$sch_s_week_val = date('Y-m-d', strtotime("{$sch_e_day} -12 weeks"));
$sch_s_week_w   = date('w', strtotime("{$sch_s_week_val}"))-1;
$sch_s_week     = date('Y-m-d', strtotime("{$sch_s_week_val} -{$sch_s_week_w} day"));
$sch_e_week_w   = date('w', strtotime("{$sch_e_day}"))-1;
$sch_e_week_val = date('Y-m-d', strtotime("{$sch_e_day} -{$sch_e_week_w} day"));
$sch_e_week     = date("Y-m-d", strtotime("{$sch_e_week_val} +6 day"));
$sch_base_type  = isset($_GET['sch_base_type']) ? $_GET['sch_base_type'] : "all";
$sch_date_type  = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "day";

if(empty($sch_base_type) || empty($sch_date_type)) {
    echo "기본 검색 값이 없으면 차트 생성이 안됩니다.";
    exit;
}

$total_chart_type   = getTotalBrandChartType();
$doc_chart_type     = getDocBrandChartType();
$nuzam_chart_type   = getNuzamBrandChartType();
$convert_total_chart_list       = [];
$convert_total_chart_style_list = [];
$convert_total_chart_init       = array(
    array("title" => "유입", "type" => "line", "color" => "rgba(0,176,80)", "data" => array()),
    array("title" => "전환", "type" => "line", "color" => "rgba(255,192,0)", "data" => array()),
    array("title" => "전환율", "type" => "line", "color" => "rgba(0,176,240)", "data" => array()),
    array("title" => "아임웹 매출", "type" => "line", "color" => "rgba(128,100,162)", "data" => array()),
    array("title" => "주문건당 단가", "type" => "line", "color" => "rgba(255,51,204)", "data" => array())
);

if($sch_base_type == "all"){
    $add_where      .= " AND brand IN({$total_brand_text})";
    $add_cms_where  .= " AND w.c_no IN({$total_brand_text})";
    $add_nosp_where .= " AND `ar`.brand IN({$total_brand_text})";
    $sch_brand_name  = "커머스 전체";

    foreach($main_brand_option as $brand) {
        $convert_total_chart_style_list[$brand] = $total_chart_type[$brand];
    }
}
elseif($sch_base_type == "doc"){
    $add_where      .= " AND brand IN({$doc_brand_text})";
    $add_cms_where  .= " AND w.c_no IN({$doc_brand_text})";
    $add_nosp_where .= " AND `ar`.brand IN({$doc_brand_text})";
    $sch_brand_name  = "닥터피엘 전체";

    foreach($doc_brand_option as $brand) {
        $convert_total_chart_style_list[$brand] = $doc_chart_type[$brand];
    }
}
elseif($sch_base_type == "nuzam"){
    $add_where      .= " AND brand IN({$nuzam_brand_text})";
    $add_cms_where  .= " AND w.c_no IN({$nuzam_brand_text})";
    $add_nosp_where .= " AND `ar`.brand IN({$nuzam_brand_text})";
    $sch_brand_name  = "누잠 전체";

    foreach($nuzam_brand_option as $brand) {
        $convert_total_chart_style_list[$brand] = $nuzam_chart_type[$brand];
    }
}

$sch_convert_s_date     = ($sch_date_type == "day") ? $sch_s_day  : $sch_s_week;
$sch_convert_e_date     = ($sch_date_type == "day") ? $sch_e_day : $sch_e_week;
$sch_convert_s_datetime = $sch_convert_s_date." 00:00:00";
$sch_convert_e_datetime = $sch_convert_e_date." 23:59:59";

$all_date_where         = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_convert_s_date}' AND '{$sch_convert_e_date}'";
$add_where             .= " AND convert_date BETWEEN '{$sch_convert_s_datetime}' AND '{$sch_convert_e_datetime}'";
$add_cms_where         .= " AND order_date BETWEEN '{$sch_convert_s_datetime}' AND '{$sch_convert_e_datetime}'";
$add_nosp_where        .= " AND `am`.adv_s_date BETWEEN '{$sch_convert_s_datetime}' AND '{$sch_convert_e_datetime}'";

if($sch_date_type == 'week') //주간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_convert_s_date}' AND '{$sch_convert_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_key_column  = "DATE_FORMAT(DATE_SUB(`cc`.convert_date, INTERVAL(IF(DAYOFWEEK(`cc`.convert_date)=1,8,DAYOFWEEK(`cc`.convert_date))-2) DAY), '%Y%m%d')";
    $add_cms_column  = "DATE_FORMAT(DATE_SUB(`w`.order_date, INTERVAL(IF(DAYOFWEEK(`w`.order_date)=1,8,DAYOFWEEK(`w`.order_date))-2) DAY), '%Y%m%d')";
    $add_nosp_column = "DATE_FORMAT(DATE_SUB(`am`.adv_s_date, INTERVAL(IF(DAYOFWEEK(`am`.adv_s_date)=1,8,DAYOFWEEK(`am`.adv_s_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";

    $add_key_column  = "DATE_FORMAT(`cc`.convert_date, '%Y%m%d')";
    $add_cms_column  = "DATE_FORMAT(`w`.order_date, '%Y%m%d')";
    $add_nosp_column = "DATE_FORMAT(`am`.adv_s_date, '%Y%m%d')";
}

$convert_date_list      = [];
$convert_link_date_list = [];
$convert_total_list     = [];
$nosp_result_list       = [];
$nosp_result_total_list = [];

# 자사몰 유입전환
$conversion_init        = array("traffic" => 0, "conversion" => 0, "conversion_rate" => 0,"total_price" => 0, "avg_price" => 0);
$nosp_result_init       = array("1" => array("count" => 0, "total" => 0), "2" => array("count" => 0, "total" => 0)); # 1: 타임보드, 2: 스페셜DA

# 날짜 생성
$all_date_sql = "
    SELECT
        {$all_date_title} as chart_title,
        {$all_date_key} as chart_key,
        DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
    FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
            (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
    as allday
    WHERE {$all_date_where}
    ORDER BY chart_key, date_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($all_date = mysqli_fetch_assoc($all_date_query))
{
    $convert_date_list[$all_date['chart_key']]  = $all_date['chart_title'];

    if($sch_base_type == "all")
    {
        foreach($main_brand_option as $main_brand){
            $convert_total_list[$all_date['chart_key']][$main_brand]    = $conversion_init;
            $nosp_result_list[$main_brand][$all_date['chart_key']]      = $nosp_result_init;
            $nosp_result_total_list[$main_brand] = 0;
        }
    }
    elseif($sch_base_type == "doc")
    {
        foreach($doc_brand_option as $doc_brand){
            if(in_array($doc_brand, $doc_filter_option)){
                $doc_brand_key = "1314";
            }elseif(in_array($doc_brand, $doc_cubing_option)){
                $doc_brand_key = "3386";
            }else{
                $doc_brand_key = $doc_brand;
            }

            $convert_total_list[$all_date['chart_key']][$doc_brand_key] = $conversion_init;
            $nosp_result_list[$doc_brand_key][$all_date['chart_key']]   = $nosp_result_init;
            $nosp_result_total_list[$doc_brand_key] = 0;
        }
    }
    elseif($sch_base_type == "nuzam")
    {
        foreach($nuzam_brand_option as $nuzam_brand){
            $convert_total_list[$all_date['chart_key']][$nuzam_brand]   = $conversion_init;
            $nosp_result_list[$nuzam_brand][$all_date['chart_key']]     = $nosp_result_init;
            $nosp_result_total_list[$nuzam_brand] = 0;
        }
    }

    if(!isset($convert_link_date_list[$all_date['chart_key']]))
    {
        if($sch_date_type == "week"){
            $link_s_date    = date("Y-m-d", strtotime($all_date['date_key']));
            $link_s_w       = date('w', strtotime($link_s_date));
            $link_e_w       = 7-$link_s_w;
            $link_e_date    = date("Y-m-d", strtotime("{$link_s_date} +{$link_e_w} days"));
            $convert_link_date_list[$all_date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
        }
        else{
            $link_s_date    = date("Y-m-d", strtotime($all_date['date_key']));
            $convert_link_date_list[$all_date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_s_date);
        }
    }
}

# 유입/전환
$convert_sql = "
    SELECT
        `cc`.brand,
        {$add_key_column} as key_date,
        SUM(`cc`.traffic_count) as traffic_cnt,
        SUM(`cc`.conversion_count) as convert_cnt,
        SUM(`cc`.conversion_price) as total_price
    FROM commerce_conversion as `cc`
    WHERE {$add_where}
    GROUP BY key_date, brand
    ORDER BY key_date, brand
";
$convert_query = mysqli_query($my_db, $convert_sql);
while($convert_result = mysqli_fetch_assoc($convert_query))
{
    $brand_key = "";

    if($sch_base_type == "all")
    {
        if(in_array($convert_result["brand"], $doc_brand_option)){
            $brand_key = "doc";
        }
        elseif(in_array($convert_result["brand"], $nuzam_brand_option)){
            $brand_key = "nuzam";
        }
        elseif(in_array($convert_result["brand"], $ilenol_brand_option)){
            $brand_key = "ilenol";
        }
        elseif(in_array($convert_result["brand"], $city_brand_option)){
            $brand_key = "city";
        }
    }
    elseif($sch_base_type == "doc")
    {
        if(in_array($convert_result['brand'], $doc_filter_option)){
            $brand_key = "1314";
        }elseif(in_array($convert_result['brand'], $doc_cubing_option)){
            $brand_key = "3386";
        }else{
            $brand_key = $convert_result['brand'];
        }
    }
    elseif($sch_base_type == "nuzam")
    {
        $brand_key = $convert_result["brand"];
    }

    if(!empty($brand_key)){
        $convert_total_list[$convert_result['key_date']][$brand_key]["traffic"]       += $convert_result['traffic_cnt'];
        $convert_total_list[$convert_result['key_date']][$brand_key]["conversion"]    += $convert_result['convert_cnt'];
    }
}

# 매출
$cms_sql = "
    SELECT
        `w`.c_no,
        `w`.order_number,
        {$add_cms_column} as key_date,
        `w`.unit_price
    FROM work_cms as `w`
    WHERE {$add_cms_where}
    ORDER BY key_date
";
$cms_query = mysqli_query($my_db, $cms_sql);
$chk_cms_order_list = [];
while($cms_result = mysqli_fetch_assoc($cms_query))
{
    $brand_key      = "";

    if($sch_base_type == "all")
    {
        if(in_array($cms_result["c_no"], $doc_brand_option)){
            $brand_key = "doc";
        }
        elseif(in_array($cms_result["c_no"], $nuzam_brand_option)){
            $brand_key = "nuzam";
        }
        elseif(in_array($cms_result["c_no"], $ilenol_brand_option)){
            $brand_key = "ilenol";
        }
        elseif(in_array($cms_result["c_no"], $city_brand_option)){
            $brand_key = "city";
        }
    }
    elseif($sch_base_type == "doc")
    {
        if(in_array($cms_result['c_no'], $doc_filter_option)){
            $brand_key = "1314";
        }elseif(in_array($cms_result['c_no'], $doc_cubing_option)){
            $brand_key = "3386";
        }else{
            $brand_key = $cms_result['c_no'];
        }
    }
    elseif($sch_base_type == "nuzam")
    {
        $brand_key = $cms_result["c_no"];
    }

    if(!isset($chk_cms_order_list[$brand_key][$cms_result['order_number']])){
        $chk_cms_order_list[$brand_key][$cms_result['order_number']] = 1;
        $convert_total_list[$cms_result['key_date']][$brand_key]["order_cnt"]++;
    }
    $convert_total_list[$cms_result['key_date']][$brand_key]["total_price"] += $cms_result['unit_price'];
}

foreach($convert_total_list as $key_date => $key_data)
{
    foreach($key_data as $brand => $brand_data)
    {
        $traffic_count  = $brand_data['traffic'];
        $convert_count  = $brand_data['conversion'];
        $total_price    = $brand_data['total_price'];
        $order_cnt      = $brand_data['order_cnt'];
        $convert_rate   = ($traffic_count > 0) ? round((($convert_count / $traffic_count) * 100), 2) : 0;
        $avg_price      = ($order_cnt > 0) ? round($total_price / $order_cnt) : 0;

        $convert_total_list[$key_date][$brand]["avg_price"]         = $avg_price;
        $convert_total_list[$key_date][$brand]["conversion_rate"]   = $convert_rate;

        if(!isset($convert_total_chart_list[$brand])) {
            $convert_total_chart_list[$brand] = $convert_total_chart_init;
        }

        $convert_total_chart_list[$brand][0]['data'][]  = $traffic_count;
        $convert_total_chart_list[$brand][1]['data'][]  = $convert_count;
        $convert_total_chart_list[$brand][2]['data'][]  = $convert_rate;
        $convert_total_chart_list[$brand][3]['data'][]  = $total_price;
        $convert_total_chart_list[$brand][4]['data'][]  = $avg_price;
    }
}

# NOSP 광고
$nosp_result_sql = "
    SELECT 
        {$add_nosp_column} as key_date,
        am.am_no,
        ar.brand,
        am.product,
        am.fee_per,
        ar.adv_price
    FROM advertising_result as `ar`
    LEFT JOIN advertising_management as `am` ON `am`.am_no=`ar`.am_no
    WHERE {$add_nosp_where}
    ORDER BY key_date 
";
$nosp_result_query      = mysqli_query($my_db, $nosp_result_sql);
$nosp_result_tmp_list   = [];
while($nosp_result  = mysqli_fetch_assoc($nosp_result_query))
{
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["key_date"]         = $nosp_result['key_date'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["product"]          = $nosp_result['product'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["fee_per"]          = $nosp_result['fee_per'];
    $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_adv_price"] += $nosp_result['adv_price'];
}

$nosp_result_chk_list = [];
foreach($nosp_result_tmp_list as $am_no => $nosp_result_tmp)
{
    foreach($nosp_result_tmp as $brand => $adv_brand_data)
    {
        $fee_price      = $adv_brand_data['total_adv_price']*($adv_brand_data['fee_per']/100);
        $cal_price      = $adv_brand_data["total_adv_price"]-$fee_price;
        $cal_price_vat  = $cal_price*1.1;

        $brand_key      = "";
        if($sch_base_type == "all") {
            if(in_array($brand, $doc_brand_option)){
                $brand_key = "doc";
            }
            elseif(in_array($brand, $nuzam_brand_option)){
                $brand_key = "nuzam";
            }
            elseif(in_array($brand, $ilenol_brand_option)){
                $brand_key = "ilenol";
            }
            elseif(in_array($brand, $city_brand_option)){
                $brand_key = "city";
            }
        }
        elseif($sch_base_type == "doc") {
            if(in_array($brand, $doc_filter_option)){
                $brand_key = "1314";
            }elseif(in_array($brand, $doc_cubing_option)){
                $brand_key = "3386";
            }else{
                $brand_key = $brand;
            }
        }
        elseif($sch_base_type == "nuzam") {
            $brand_key = $brand;
        }

        $nosp_result_list[$brand_key][$adv_brand_data['key_date']][$adv_brand_data['product']]['total'] += $cal_price_vat;

        if(!isset($nosp_result_chk_list[$brand_key][$am_no])){
            $nosp_result_chk_list[$brand_key][$am_no] = 1;
            $nosp_result_list[$brand_key][$adv_brand_data['key_date']][$adv_brand_data['product']]['count']++;
            $nosp_result_total_list[$brand_key]++;
        }
    }
}

$smarty->assign("sch_date_type", $sch_date_type);
$smarty->assign("convert_date_list", $convert_date_list);
$smarty->assign("convert_link_date_list", $convert_link_date_list);
$smarty->assign("convert_total_list", $convert_total_list);
$smarty->assign("nosp_result_list", $nosp_result_list);
$smarty->assign("nosp_result_total_list", $nosp_result_total_list);
$smarty->assign("convert_chart_name_list", json_encode($convert_date_list));
$smarty->assign("convert_total_chart_list", json_encode($convert_total_chart_list));
$smarty->assign("convert_total_chart_style_list", json_encode($convert_total_chart_style_list));

if($sch_base_type == "doc"){
    $smarty->display('wise_bm/total_main_doc_iframe_traffic_conversion.html');
}elseif($sch_base_type == "nuzam"){
    $smarty->display('wise_bm/total_main_nuzam_iframe_traffic_conversion.html');
}else{
    $smarty->display('wise_bm/total_main_base_iframe_traffic_conversion.html');
}
?>
