<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/wise_bm.php');
require('../inc/helper/work_cms.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 검색조건
$bm_brand_option    = getBrandDetailOption();
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sel_brand_data     = isset($bm_brand_option[$sch_brand]) ? $bm_brand_option[$sch_brand] : "";

if(empty($sel_brand_data)){
    exit("<script>alert('접근 권한이 없습니다.');location.href='/v1/main.php';</script>");
}

# 브랜드 변수
$main_brand_option  = $sel_brand_data['brand_list'];
$main_brand_text    = implode(",", $main_brand_option);
$dp_except_list     = getNotApplyDpList();
$dp_except_text     = implode(",", $dp_except_list);
$bm_type_name       = $sel_brand_data['title'];
$sch_brand_url      = $sel_brand_data['brand_url'];

# 날짜 변수
$prev_month         = date('Y-m', strtotime("-1 months"));
$today_s_w		    = date('w')-1;
$today_s_week       = date('Y-m-d', strtotime("-{$today_s_w} days"));
$count_s_week       = date('Y-m-d', strtotime("{$today_s_week} -3 weeks"));
$count_e_week       = date("Y-m-d",strtotime("{$today_s_week} +6 days"));
$count_s_week_time  = $count_s_week." 00:00:00";
$count_e_week_time  = $count_e_week." 23:59:59";

# 주문수 관련 그룹설정
$cms_count_group_title_list = [];
$cms_count_group_list       = [];
$commerce_count_date_list   = [];
$cms_count_list             = [];
$cms_count_total_list       = [];

$cms_count_where            = "w.c_no IN($main_brand_text) AND w.delivery_state='4' AND (w.order_date BETWEEN '{$count_s_week_time}' AND '{$count_e_week_time}') AND dp_c_no NOT IN({$dp_except_text}) AND unit_price > 0";
$cms_count_column           = "DATE_FORMAT(DATE_SUB(order_date, INTERVAL(IF(DAYOFWEEK(order_date)=1,8,DAYOFWEEK(order_date))-2) DAY), '%Y%m%d')";

$cms_count_group_sql    = "SELECT DISTINCT dp_c_no, dp_c_name, COUNT(DISTINCT order_number) AS total_ord FROM work_cms as w WHERE {$cms_count_where} GROUP BY dp_c_no ORDER BY total_ord DESC LIMIT 5";
$cms_count_group_query  = mysqli_query($my_db, $cms_count_group_sql);
while($cms_count_group = mysqli_fetch_assoc($cms_count_group_query)){
    $cms_count_group_list[$cms_count_group['dp_c_no']]       = 0;
    $cms_count_group_title_list[$cms_count_group['dp_c_no']] = $cms_count_group['dp_c_name'];
}
$cms_count_group_list['etc']        = 0;
$cms_count_group_title_list['etc']  = "기타";

# 주문수 날짜 설정
$count_date_sql = "
	SELECT
 		DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d') as chart_key,
 		CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d')) as chart_title
	FROM (
	    SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e
    )
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$count_s_week}' AND '{$count_e_week}'
	GROUP BY chart_key
	ORDER BY chart_key
";
$count_date_query = mysqli_query($my_db, $count_date_sql);
while($count_date = mysqli_fetch_array($count_date_query))
{
    $commerce_count_date_list[$count_date['chart_key']] = $count_date['chart_title'];
    $cms_count_list[$count_date['chart_key']]           = $cms_count_group_list;
    $cms_count_total_list[$count_date['chart_key']]     = 0;
}

# 주문수 데이터 수집
$cms_count_sql = "
    SELECT 
        dp_c_no,
        COUNT(DISTINCT order_number) as ord_cnt,
        {$cms_count_column} as key_date
    FROM work_cms as w
    WHERE {$cms_count_where}
    GROUP BY dp_c_no, key_date 
";
$cms_count_query = mysqli_query($my_db, $cms_count_sql);
while($cms_count = mysqli_fetch_assoc($cms_count_query))
{
    $dp_type = (array_key_exists($cms_count['dp_c_no'], $cms_count_group_list) !== false) ? $cms_count['dp_c_no'] : "etc";
    $cms_count_list[$cms_count['key_date']][$dp_type]   += $cms_count['ord_cnt'];
    $cms_count_total_list[$cms_count['key_date']]       += $cms_count['ord_cnt'];
}

# 반품수 날짜 설정
$return_e_month = $prev_month;
$return_s_month = date('Y-m', strtotime("{$return_e_month} -4 months"));
$return_e_day   = date("t", strtotime($return_e_month));
$return_s_date  = $return_s_month."-01 00:00:00";
$return_e_date  = $return_e_month."-{$return_e_day} 23:59:59";

# 반품수 그룹 설정
$return_month_list          = [];
$return_count_list          = [];
$return_count_total_list    = [];
$return_count_where         = "`cr`.c_no IN($main_brand_text) AND `cr`.return_type IN(1) AND (`cr`.return_date BETWEEN '{$return_s_date}' AND '{$return_e_date}')";

$return_date_sql = "
	SELECT
 		DATE_FORMAT(`allday`.Date, '%Y%m') as chart_key,
	    DATE_FORMAT(`allday`.Date, '%Y-%m') as chart_name,
	    DATE_FORMAT(`allday`.Date, '%Y/%m') as chart_title
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$return_s_month}' AND '{$return_e_month}'
	GROUP BY chart_key
	ORDER BY chart_key
";
$return_date_query = mysqli_query($my_db, $return_date_sql);
while($return_date = mysqli_fetch_array($return_date_query))
{
    $return_month_list[$return_date['chart_key']]       = $return_date['chart_title'];
    $return_count_list[$return_date['chart_key']]       = $cms_count_group_list;
    $return_count_total_list[$return_date['chart_key']] = 0;
}

# 반품수 데이터 수집
$return_count_sql = "
    SELECT 
        dp_c_no,
        COUNT(DISTINCT order_number) as return_cnt,
        DATE_FORMAT(return_date, '%Y%m') as key_date
    FROM csm_return cr
    WHERE {$return_count_where}
    GROUP BY dp_c_no, key_date 
";
$return_count_query = mysqli_query($my_db, $return_count_sql);
while($return_count = mysqli_fetch_assoc($return_count_query))
{
    $dp_type = (array_key_exists($return_count['dp_c_no'], $cms_count_group_list) !== false) ? $return_count['dp_c_no'] : "etc";
    $return_count_list[$return_count['key_date']][$dp_type] += $return_count['return_cnt'];
    $return_count_total_list[$return_count['key_date']]     += $return_count['return_cnt'];
}

$smarty->assign("bm_type_name", $bm_type_name);
$smarty->assign("sch_brand_url", $sch_brand_url);
$smarty->assign("commerce_count_date_list", $commerce_count_date_list);
$smarty->assign("commerce_count_date_cnt", count($commerce_count_date_list)+1);
$smarty->assign("count_chart_name_list", json_encode($commerce_count_date_list));
$smarty->assign("cms_count_group_title_list", $cms_count_group_title_list);
$smarty->assign("cms_count_list", $cms_count_list);
$smarty->assign("cms_count_total_list", json_encode($cms_count_total_list));
$smarty->assign("return_s_month", $return_s_month);
$smarty->assign("return_e_month", $return_e_month);
$smarty->assign("return_month_list", $return_month_list);
$smarty->assign("return_count_list", $return_count_list);
$smarty->assign("return_chart_name_list", json_encode($return_month_list));
$smarty->assign("return_count_total_list", json_encode($return_count_total_list));

$smarty->display('wise_bm/brand_main_base_iframe_cms_count.html');
?>
