<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', 3000);

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/product_cms.php');
require('inc/helper/product_cms_stock.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/CommerceOrder.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');

# Navigation & My Quick
$nav_prd_no  = "192";
$nav_title   = "재고자산수불부(물류업체 검토 및 보정)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);


# 프로세스 처리
$process    = isset($_POST['process']) ? $_POST['process'] : "";

# 모델생성
$company_model  = Company::Factory();
$unit_model     = ProductCmsUnit::Factory();
$report_model   = ProductCmsStock::Factory();
$confirm_model  = ProductCmsStock::Factory();
$report_model->setMainInit("product_cms_stock_report", "no");
$confirm_model->setMainInit("product_cms_stock_confirm", "no");

if($process == "f_in_base_qty")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";
    $value  = str_replace(",","",trim($value));

    if (!$confirm_model->update(array("no" => $no, "qty" => $value))) {
        echo "기초재고 저장에 실패 하였습니다.";
    }else{
        echo "기초재고 저장에 성공했습니다.";
    }
    exit;
}
elseif($process == "apply_correct")
{
    $chk_correct_type   = isset($_POST['chk_correct_type']) ? $_POST['chk_correct_type'] : "";
    $correct_data       = isset($_POST['chk_correct_data']) ? $_POST['chk_correct_data'] : "";
    $search_url         = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(empty($correct_data)){
        exit("<script>alert('적용할 데이터가 없습니다.');location.href='product_cms_stock_receipt_list_copy.php?{$search_url}';</script>");
    }

    $type           = "3";
    $state          = "";
    $confirm_state  = "";
    $memo           = "";
    $tmp_log_c_no   = 2809;

    if($chk_correct_type == "return"){
        $state          = "반품입고";
        $confirm_state  = "5";
        $memo           = "오배송 반품입고 보정";
    }elseif($chk_correct_type == "unit"){
        $state          = "판매반출";
        $confirm_state  = "9";
        $memo           = "부속품 제공 판매반출 보정";
    }elseif($chk_correct_type == "sales"){
        $state          = "판매반출";
        $confirm_state  = "9";
        $memo           = "판매반출 보정";
    }elseif($chk_correct_type == "final"){
        $state          = "판매반출";
        $confirm_state  = "9";
        $memo           = "기말재고 최종 보정";
    }

    if(empty($state) || empty($confirm_state)){
        exit("<script>alert('적용할 데이터가 없습니다.');location.href='product_cms_stock_receipt_list_copy.php?{$search_url}';</script>");
    }

    $total_ins_list = [];
    $base_ins_data  = array(
        "report_type"   => "3",
        "type"          => "2",
        "is_order"      => "2",
        "state"         => $state,
        "confirm_state" => $confirm_state,
        "memo"          => $memo,
        "reg_s_no"      => $session_s_no,
        "report_date"   => date("Y-m-d H:i:s"),
    );

    $correct_data_list = json_decode($correct_data);

    foreach($correct_data_list as $confirm_no => $stock_qty)
    {
        $chk_confirm_sql    = "SELECT * FROM product_cms_stock_confirm WHERE `no`='{$confirm_no}'";
        $chk_confirm_query  = mysqli_query($my_db, $chk_confirm_sql);
        $chk_confirm_result = mysqli_fetch_assoc($chk_confirm_query);

        $chk_log_c_no       = $chk_confirm_result['log_c_no'];
        $chk_warehouse      = $chk_confirm_result['warehouse'];
        $chk_prd_unit       = $chk_confirm_result['prd_unit'];

        $unit_item  = $unit_model->getItem($chk_prd_unit, $chk_log_c_no);

        if(empty($unit_item)){
            $unit_item  = $unit_model->getItem($chk_prd_unit, $tmp_log_c_no);
        }
        $tmp_data   = $base_ins_data;

        $tmp_data['regdate']    = $chk_confirm_result['base_mon']."-".date("t", strtotime($chk_confirm_result['base_mon']));
        $tmp_data['brand']      = $unit_item["brand_name"];
        $tmp_data['prd_kind']   = $unit_item['type'] == "1" ? "상품" : "부속품";
        $tmp_data['prd_unit']   = $chk_prd_unit;
        $tmp_data['sku']        = $unit_item["sku"];
        $tmp_data['option_name']= $unit_item["option_name"];
        $tmp_data['prd_name']   = $unit_item["option_name"];
        $tmp_data['sup_c_no']   = $unit_item['sup_c_no'];
        $tmp_data['log_c_no']   = $chk_log_c_no;
        $tmp_data['stock_type'] = $chk_warehouse;
        $tmp_data['stock_qty']  = $stock_qty;

        $total_ins_list[] = $tmp_data;
    }

    if(!empty($total_ins_list)) {
        if($report_model->multiInsert($total_ins_list)){
            exit("<script>alert('데이터 적용했습니다');location.href='product_cms_stock_receipt_list_copy.php?{$search_url}';</script>");
        }else{
            exit("<script>alert('데이터 적용에 실패했습니다.');location.href='product_cms_stock_receipt_list_copy.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('적용할 데이터가 없습니다.');location.href='product_cms_stock_receipt_list_copy.php?{$search_url}';</script>");
    }
}

# 검색
$add_confirm_where      = "1=1 AND `base_type`='start'";
$add_report_where       = "1=1";
$add_confirm_rs_where   = "1=1";

$sch_cur_month      = date("Y-m");
$sch_staff_type     = isset($_GET['sch_staff_type']) ? $_GET['sch_staff_type'] : "1";
$sch_move_date      = isset($_GET['sch_move_date']) ? $_GET['sch_move_date'] : date('Y-m', strtotime("{$sch_cur_month} -1 months"));
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_option_name    = isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_unit_type      = isset($_GET['sch_unit_type']) ? $_GET['sch_unit_type'] : "";
$sch_log_c_no       = isset($_GET['sch_log_c_no']) && !empty($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "2809";
$sch_warehouse      = isset($_GET['sch_warehouse']) ? $_GET['sch_warehouse'] : "";
$sch_warehouse_all  = isset($_GET['sch_warehouse_all']) ? $_GET['sch_warehouse_all'] : "";
$sch_sku            = isset($_GET['sch_sku']) ? $_GET['sch_sku'] : "";
$sch_not_empty      = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : 1;
$sch_is_commerce    = isset($_GET['sch_is_commerce']) ? $_GET['sch_is_commerce'] : "";
$sch_only_qty       = isset($_GET['sch_only_qty']) ? $_GET['sch_only_qty'] : "";
$chk_process        = isset($_GET['chk_process']) ? $_GET['chk_process'] : "";
$chk_num            = isset($_GET['chk_num']) ? $_GET['chk_num'] : "";
$chk_num_return     = isset($_GET['chk_num_return']) ? $_GET['chk_num_return'] : 20;
$chk_num_unit       = isset($_GET['chk_num_unit']) ? $_GET['chk_num_unit'] : 20;
$chk_num_sales      = isset($_GET['chk_num_sales']) ? $_GET['chk_num_sales'] : 10;
$chk_num_discord    = isset($_GET['chk_num_discord']) ? $_GET['chk_num_discord'] : 20;
$search_url         = getenv("QUERY_STRING");
$with_log_c_no      = "2809";

if(empty($search_url))
{
    if($sch_staff_type == 1){
        $sch_only_qty       = 1;
    } elseif($sch_staff_type == 2){

    } elseif($sch_staff_type == 3){
        $sch_warehouse_all  = 1;
        $sch_log_c_no       = $with_log_c_no;
    }
}

$chk_correct_type = "";
$chk_correct_list = [];
if($chk_process)
{
    switch ($chk_process){
        case "correct_return": $chk_num_return = $chk_num; $chk_correct_type = "return"; break;
        case "correct_unit": $chk_num_unit = $chk_num; $chk_correct_type = "unit"; break;
        case "correct_sales": $chk_num_sales = $chk_num; $chk_correct_type = "sales"; break;
        case "correct_discord": $chk_num_discord = $chk_num; $chk_correct_type = "discord"; break;
        case "correct_review": $chk_num=1; $chk_correct_type = "review"; break;
    }

    $smarty->assign("chk_correct_type", $chk_correct_type);
}

$smarty->assign('sch_staff_type', $sch_staff_type);
$smarty->assign('sch_not_empty', $sch_not_empty);
$smarty->assign('sch_warehouse_all', $sch_warehouse_all);
$smarty->assign('sch_only_qty', $sch_only_qty);
$smarty->assign('chk_process', $chk_process);
$smarty->assign('chk_num_return', $chk_num_return);
$smarty->assign('chk_num_unit', $chk_num_unit);
$smarty->assign('chk_num_sales', $chk_num_sales);
$smarty->assign('chk_num_discord', $chk_num_discord);
$smarty->assign("search_url", $search_url);

if(!empty($sch_brand)){
    $add_confirm_where  .= " AND rs.brand = '{$sch_brand}'";
    $smarty->assign("sch_brand", $sch_brand);
}

if(!empty($sch_option_name)){
    $add_confirm_where  .= " AND rs.option_name LIKE '%{$sch_option_name}%'";
    $smarty->assign("sch_option_name", $sch_option_name);
}

if(!empty($sch_unit_type)){
    $add_report_where       .= " AND pcsr.prd_unit IN(SELECT pcu.`no` FROM product_cms_unit pcu WHERE pcu.`type`= '{$sch_unit_type}')";
    $add_confirm_rs_where   .= " AND unit_type='{$sch_unit_type}'";
    $smarty->assign("sch_unit_type", $sch_unit_type);
}

if(!empty($sch_log_c_no)){
    $add_confirm_where  .= " AND rs.log_c_no = '{$sch_log_c_no}'";
    $add_report_where   .= " AND pcsr.log_c_no = '{$sch_log_c_no}'";
    $smarty->assign("sch_log_c_no", $sch_log_c_no);
}

if(!empty($sch_warehouse)){
    $add_confirm_where  .= " AND rs.warehouse LIKE '%{$sch_warehouse}%'";
    $smarty->assign("sch_warehouse", $sch_warehouse);
}

if(!empty($sch_sku)){
    $add_confirm_where  .= " AND rs.sku = '{$sch_sku}'";
    $smarty->assign("sch_sku", $sch_sku);
}

if(!empty($sch_move_date)){
    $add_confirm_where  .=  " AND rs.base_mon='{$sch_move_date}'";
    $smarty->assign("sch_move_date", $sch_move_date);
}

if(!empty($sch_is_commerce)){
    if($sch_is_commerce == '1'){
        $add_confirm_where  .=  " AND rs.prd_unit IN(SELECT DISTINCT co.option_no FROM commerce_order co LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no` WHERE cos.display='1' AND DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1')";
    }else{
        $add_confirm_where  .=  " AND rs.prd_unit NOT IN(SELECT DISTINCT co.option_no FROM commerce_order co LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no` WHERE cos.display='1' AND DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1')";
    }

    $smarty->assign("sch_is_commerce", $sch_is_commerce);
}

# 재고자산수불부 total 변수
$total_in_base_qty = $total_in_base_price = $total_out_base_qty = $total_out_base_price = 0;
$total_in_point_qty = $total_in_point_price = $total_in_move_qty = $total_in_move_price = $total_in_return_qty = $total_in_return_price = $total_in_etc_qty = $total_in_etc_price = $total_in_qty = $total_in_price = 0;
$total_out_point_qty = $total_out_point_price = $total_out_move_qty = $total_out_move_price = $total_out_return_qty = $total_out_return_price = $total_out_etc_qty = $total_out_etc_price = $total_out_qty = $total_out_price = 0;

# 회수리스트 처리
$return_list      = [];
$return_prd_sql   = "SELECT `option`, SUM(quantity) as qty FROM work_cms_return_unit WHERE order_number IN(SELECT DISTINCT order_number FROM work_cms_return wcr WHERE wcr.return_state='6' AND DATE_FORMAT(wcr.return_date,'%Y-%m')='{$sch_move_date}') AND `type` IN(1,2,3,4) GROUP BY `option`";
$return_prd_query = mysqli_query($my_db, $return_prd_sql);
while($return_prd = mysqli_fetch_assoc($return_prd_query))
{
    $return_list[$return_prd['option']] += $return_prd['qty'];
}

# 쿼리용 날짜
$sch_move_s_date    = $sch_move_date."-01";
$end_day            = date('t', strtotime($sch_move_date));
$sch_move_e_date    = $sch_move_date."-".$end_day;

# 재고이동 리스트
$stock_move_list   = [];
$stock_move_sql     = "SELECT prd_unit, in_warehouse, out_warehouse, in_qty, out_qty FROM product_cms_stock_transfer pcst WHERE (pcst.move_date BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}')";
$stock_move_query   = mysqli_query($my_db, $stock_move_sql);
while($stock_move = mysqli_fetch_assoc($stock_move_query))
{
    $stock_move_list[$stock_move['prd_unit']][$stock_move['in_warehouse']]["in"]   += $stock_move['in_qty'];
    $stock_move_list[$stock_move['prd_unit']][$stock_move['out_warehouse']]["out"] += $stock_move['out_qty'];
}

# 입고/반출 리스트
$stock_list         = [];
$stock_dev_list     = [];
$stock_report_sql   = "SELECT * FROM product_cms_stock_report pcsr WHERE {$add_report_where} AND (pcsr.regdate BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}') AND pcsr.confirm_state > 0 AND move_no='0'";
$stock_report_query = mysqli_query($my_db, $stock_report_sql);
while($stock_report = mysqli_fetch_assoc($stock_report_query))
{
    $stock_kind     = "";
    $stock_dev_kind = "";
    switch($stock_report['confirm_state']){
        case "1":
        case "2":
            $stock_kind = "in_point";
            break;
        case "3":
            if($stock_report['state'] == "기간이동입고"){
                $stock_dev_kind = "in_move";
            }else{
                $stock_kind = "in_move";
            }
            break;
        case "4":
            $stock_kind = "in_etc";
            break;
        case "5":
            if($stock_report['state'] == "기간반품입고"){
                $stock_dev_kind = "in_return";
            }else{
                $stock_kind = "in_return";
            }
            break;
        case "6":
            $stock_kind = "out_move";
            break;
        case "8":
            $stock_kind = "out_etc";
            break;
        case "9":
            if($stock_report['state'] == "기간판매반출"){
                $stock_dev_kind = "out_point";
            }else{
                $stock_kind = "out_point";
            }
            break;
        case "10":
            $stock_kind = "out_return";
            break;
        case "11":
            $stock_dev_kind = "in_base";
            break;
        case "12":
            $stock_dev_kind = "out_base";
            break;
    }

    if(!empty($stock_kind))
    {
        if(!isset($stock_list[$stock_report['prd_unit']][$stock_report['stock_type']])){
            $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']] = array(
                "in_point"      => 0,
                "in_move"       => 0,
                "in_etc"        => 0,
                "in_return"     => 0,
                "out_point"     => 0,
                "out_move"      => 0,
                "out_return"    => 0,
                "out_etc"       => 0,
            );
        }

        $stock_list[$stock_report['prd_unit']][$stock_report['stock_type']][$stock_kind] += $stock_report['stock_qty'];
    }

    if(!empty($stock_dev_kind)){
        if(!isset($stock_dev_list[$stock_report['prd_unit']][$stock_report['stock_type']])){
            $stock_dev_list[$stock_report['prd_unit']][$stock_report['stock_type']] = array(
                "in_base"   => 0,
                "in_return" => 0,
                "out_point" => 0,
                "out_base"  => 0,
            );
        }
        $stock_dev_list[$stock_report['prd_unit']][$stock_report['stock_type']][$stock_dev_kind] += $stock_report['stock_qty'];
    }
}

# 실시간 확정원가
$commerce_order_list        = [];
$commerce_order_tmp_list    = [];
$commerce_order_sql         = "
    SELECT 
        co.set_no,
        co.option_no,
        co.unit_price, 
        co.quantity,
        co.supply_price,
        co.total_price,
        co.vat,
        co.group,
        (SELECT SUM(sub.supply_price) FROM commerce_order sub WHERE sub.set_no=co.set_no AND sub.`group`=co.`group`) as sum_sup_price,
        (SELECT SUM(sub.price) FROM commerce_order_etc sub WHERE sub.set_no=co.set_no AND sub.group_no=co.`group`) as sum_etc_price,
        (SELECT g.usd_rate FROM commerce_order_group g WHERE g.set_no=co.set_no AND g.group_no=co.`group`) as group_usd_rate,
        `cos`.sup_loc_type
    FROM commerce_order `co`
    LEFT JOIN commerce_order_set `cos` ON `cos`.no=`co`.set_no
    WHERE DATE_FORMAT(`co`.sup_date, '%Y-%m') = '{$sch_move_date}' AND `co`.`type`='supply' AND `cos`.display='1' AND (SELECT pcsr.confirm_state FROM product_cms_stock_report pcsr WHERE pcsr.ord_no=co.`no`) IN(1,2)
";
$commerce_order_query = mysqli_query($my_db, $commerce_order_sql);
while($commerce_order = mysqli_fetch_assoc($commerce_order_query))
{
    $total_price    = $commerce_order['total_price'];
    $quantity       = $commerce_order['quantity'];

    if(isset($commerce_order_tmp_list[$commerce_order['option_no']])){
        $commerce_order_tmp_list[$commerce_order['option_no']]['total_price'] += $total_price;
        $commerce_order_tmp_list[$commerce_order['option_no']]['total_qty']   += $quantity;
    }else{
        $commerce_order_tmp_list[$commerce_order['option_no']] = array(
            "total_price"   => $total_price,
            "total_qty"     => $quantity
        );
    }
}

if(!empty($commerce_order_tmp_list))
{
    foreach($commerce_order_tmp_list as $key => $comm_tmp)
    {
        $ord_qty = $comm_tmp['total_qty'] > 0 ? $comm_tmp['total_qty'] : 1;
        $commerce_order_list[$key] = round($comm_tmp['total_price']/$ord_qty, 1);
    }
}

# 재고자산수불부 쿼리
$unit_type_option    = getUnitTypeOption();
$product_receipt_sql = "
    SELECT
        *
    FROM 
    (
        SELECT
            *,
            (SELECT pcu.`type` FROM product_cms_unit pcu WHERE pcu.`no`=rs.prd_unit LIMIT 1) as unit_type
        FROM product_cms_stock_confirm as rs 
        WHERE {$add_confirm_where}
    ) as rs
    WHERE {$add_confirm_rs_where} 
    ORDER BY rs.option_name ASC, rs.warehouse ASC
";
$product_receipt_query  = mysqli_query($my_db, $product_receipt_sql);
$product_receipt_list   = [];
while($product_receipt = mysqli_fetch_assoc($product_receipt_query))
{
    $stock_list_data                        = isset($stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];
    $move_list_data                         = isset($stock_move_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_move_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];
    $stock_dev_data                         = isset($stock_dev_list[$product_receipt['prd_unit']][$product_receipt['warehouse']]) ? $stock_dev_list[$product_receipt['prd_unit']][$product_receipt['warehouse']] : [];
    $live_org_price                         = isset($commerce_order_list[$product_receipt['prd_unit']]) ? $commerce_order_list[$product_receipt['prd_unit']] : 0;

    $product_receipt['unit_type_name']      = $unit_type_option[$product_receipt['unit_type']];
    $product_receipt['in_base_qty']         = $product_receipt['qty'];
    $product_receipt['in_base_qty_dev']     = !empty($stock_dev_data) ? $stock_dev_data['in_base'] : 0;
    $product_receipt['in_base_qty_diff']    = $product_receipt['in_base_qty_dev']-$product_receipt['in_base_qty'];
    $product_receipt['in_point_qty']        = !empty($stock_list_data) ? $stock_list_data['in_point'] : 0;
    $product_receipt['in_move_sub_qty']     = !empty($stock_list_data) ? $stock_list_data['in_move'] : 0;
    $product_receipt['in_org_move_qty']     = !empty($move_list_data) && isset($move_list_data["in"]) ? $move_list_data["in"] : 0;
    $product_receipt['in_move_qty']         = $product_receipt['in_org_move_qty'] + $product_receipt['in_move_sub_qty'];
    $product_receipt['in_etc_qty']          = !empty($stock_list_data) ? $stock_list_data['in_etc'] : 0;
    $product_receipt['in_return_sub_qty']   = ($product_receipt['warehouse'] == "2-3 검수대기창고(반품건)" && isset($return_list[$product_receipt['prd_unit']])) ? $return_list[$product_receipt['prd_unit']] : 0;
    $product_receipt['in_return_org_qty']   = !empty($stock_list_data) ? $stock_list_data['in_return'] : 0;
    $product_receipt['in_return_qty']       = $product_receipt['in_return_org_qty'] + $product_receipt['in_return_sub_qty'];
    $product_receipt['in_return_qty_dev']   = !empty($stock_dev_data) ? $stock_dev_data['in_return'] : 0;
    $product_receipt['in_return_qty_diff']  = $product_receipt['in_return_qty_dev']-$product_receipt['in_return_qty'];

    $product_receipt['live_org_price']      = $live_org_price;
    $product_receipt['in_base_price']       = $product_receipt['base_price'] * $product_receipt['in_base_qty'];
    $product_receipt['in_point_price']      = $product_receipt['org_price'] * $product_receipt['in_point_qty'];
    $product_receipt['in_move_price']       = $product_receipt['org_price'] * $product_receipt['in_move_qty'];
    $product_receipt['in_return_price']     = $product_receipt['org_price'] * $product_receipt['in_return_qty'];
    $product_receipt['in_etc_price']        = $product_receipt['org_price'] * $product_receipt['in_etc_qty'];
    $product_receipt['in_qty']              = $product_receipt['in_point_qty'] + $product_receipt['in_move_qty'] + $product_receipt['in_return_qty'] + $product_receipt['in_etc_qty'];
    $product_receipt['in_qty_dev']          = $product_receipt['in_point_qty'] + $product_receipt['in_move_qty'] + $product_receipt['in_return_qty_dev'] + $product_receipt['in_etc_qty'];
    $product_receipt['in_qty_diff']         = $product_receipt['in_qty_dev']-$product_receipt['in_qty'];
    $product_receipt['in_price']            = $product_receipt['in_point_price'] + $product_receipt['in_move_price'] + $product_receipt['in_return_price'] + $product_receipt['in_etc_price'];

    $product_receipt['out_point_qty']       = !empty($stock_list_data) ? $stock_list_data['out_point']*-1 : 0;
    $product_receipt['out_point_qty_dev']   = !empty($stock_dev_data) ? $stock_dev_data['out_point']*-1 : 0;
    $product_receipt['out_point_qty_diff']  = ($product_receipt['out_point_qty_dev']-$product_receipt['out_point_qty'])*-1;
    $product_receipt['out_org_move_qty']    = !empty($move_list_data) && isset($move_list_data["out"]) ? $move_list_data["out"] : 0;
    $product_receipt['out_move_sub_qty']    = !empty($stock_list_data) ? $stock_list_data['out_move']*-1 : 0;
    $product_receipt['out_move_qty']        = $product_receipt['out_org_move_qty'] + $product_receipt['out_move_sub_qty'];
    $product_receipt['out_return_qty']      = !empty($stock_list_data) ? $stock_list_data['out_return']*-1 : 0;
    $product_receipt['out_etc_qty']         = !empty($stock_list_data) ? $stock_list_data['out_etc']*-1 : 0;
    $product_receipt['out_qty']             = $product_receipt['out_point_qty'] + $product_receipt['out_move_qty'] + $product_receipt['out_return_qty'] + $product_receipt['out_etc_qty'];
    $product_receipt['out_qty_dev']         = $product_receipt['out_point_qty_dev'] + $product_receipt['out_move_qty'] + $product_receipt['out_return_qty'] + $product_receipt['out_etc_qty'];
    $product_receipt['out_qty_diff']        = $product_receipt['out_qty_dev']-$product_receipt['out_qty'];
    $product_receipt['out_base_qty']        = $product_receipt['in_base_qty'] + $product_receipt['in_qty'] - $product_receipt['out_qty'];
    $product_receipt['out_base_qty_dev']    = !empty($stock_dev_data) ? $stock_dev_data['out_base'] : 0;
    $product_receipt['out_base_qty_diff']   = $product_receipt['out_base_qty_dev']-$product_receipt['out_base_qty'];

    # N개 이하 조회
    if(!empty($chk_process) && $chk_num > 0)
    {
        if($chk_process == 'correct_return')
        {
            if(strpos($product_receipt['warehouse'], '검수') === false){
                continue;
            } elseif($product_receipt['in_return_qty_diff'] == 0){
                continue;
            } elseif($product_receipt['in_return_qty_diff'] > $chk_num || $product_receipt['in_return_qty_diff'] < -$chk_num){
                continue;
            }

            $chk_correct_list[$product_receipt['no']] = $product_receipt['in_return_qty_diff'];
        }
        elseif($chk_process == 'correct_unit')
        {
            if($product_receipt['unit_type'] == '1'){
                continue;
            } elseif($product_receipt['out_point_qty_diff'] == 0){
                continue;
            } elseif($product_receipt['out_point_qty_diff'] > $chk_num || $product_receipt['out_point_qty_diff'] < -$chk_num){
                continue;
            }

            $chk_correct_list[$product_receipt['no']] = $product_receipt['out_point_qty_diff'];
        }
        elseif($chk_process == 'correct_sales')
        {
            if($product_receipt['unit_type'] == '2'){
                continue;
            } elseif($product_receipt['out_point_qty_diff'] == 0){
                continue;
            } elseif($product_receipt['out_point_qty_diff'] > $chk_num || $product_receipt['out_point_qty_diff'] < -$chk_num){
                continue;
            }

            $chk_correct_list[$product_receipt['no']] = $product_receipt['out_point_qty_diff'];
        }
        elseif($chk_process == 'correct_discord')
        {
            if($product_receipt['out_base_qty_diff'] == 0){
                continue;
            }elseif($product_receipt['out_base_qty_diff'] < $chk_num && $product_receipt['out_base_qty_diff'] > -$chk_num){
                continue;
            }
        }
        elseif($chk_process == 'correct_review')
        {
            if($product_receipt['out_base_qty_diff'] == 0){
                continue;
            }
        }
        elseif($chk_process == 'correct_final')
        {
            if($product_receipt['out_base_qty_diff'] == 0){
                continue;
            }elseif($product_receipt['out_base_qty_diff'] > $chk_num || $product_receipt['out_base_qty_diff'] < -$chk_num){
                continue;
            }

            $chk_correct_list[$product_receipt['no']] = $product_receipt['out_base_qty_diff'];
        }
    }

    # 판매반출 금액 계산
    $sales_price                            = ($product_receipt['in_base_qty']+$product_receipt['in_qty'] > 0) ? (($product_receipt['in_base_price']+$product_receipt['in_price']) / ($product_receipt['in_base_qty']+$product_receipt['in_qty'])) : 0;
    $product_receipt['out_point_price']     = $sales_price * $product_receipt['out_point_qty'];
    $product_receipt['out_move_price']      = $sales_price * $product_receipt['out_move_qty'];
    $product_receipt['out_return_price']    = $sales_price * $product_receipt['out_return_qty'];
    $product_receipt['out_etc_price']       = $sales_price * $product_receipt['out_etc_qty'];
    $product_receipt['out_price']           = $product_receipt['out_point_price'] + $product_receipt['out_move_price'] + $product_receipt['out_return_price'] + $product_receipt['out_etc_price'];
    $product_receipt['out_base_price']      = $product_receipt['in_base_price'] + $product_receipt['in_price'] - $product_receipt['out_price'];

    if(
        $sch_not_empty  == '1' && ($product_receipt['in_base_qty'] == 0  &&
            ($product_receipt['in_price'] == 0 && $product_receipt['in_qty']  == 0)
            && ($product_receipt['out_price'] == 0 && $product_receipt['out_qty']  == 0)
            && ($product_receipt['out_base_price'] == 0 && $product_receipt['out_base_qty']  == 0)
        )
    ){
        continue;
    }

    if($sch_warehouse_all == "1")
    {
        if(!isset($product_receipt_list[$product_receipt['prd_unit']])){
            $product_receipt_list[$product_receipt['prd_unit']] = array(
                "no"                => $product_receipt['no'],
                "prd_key"           => $product_receipt['prd_key'],
                "brand_name"        => $product_receipt['brand_name'],
                "option_name"       => $product_receipt["option_name"],
                "warehouse"         => "통합창고",
                "unit_type"         => $product_receipt['unit_type'],
                "unit_type_name"    => $unit_type_option[$product_receipt['unit_type']],
                "log_c_no"          => $product_receipt["log_c_no"],
                "log_company"       => $product_receipt["log_company"],
                "sku"               => $product_receipt["sku"],
                "sch_sku"           => urlencode($product_receipt["sku"]),
                "live_org_price"    => $live_org_price,
                "org_price"         => $product_receipt["org_price"],
                "in_base_qty"       => 0,
                "in_base_price"     => 0,
                "in_point_qty"      => 0,
                "in_point_price"    => 0,
                "in_move_qty"       => 0,
                "in_org_move_qty"   => 0,
                "in_move_sub_qty"   => 0,
                "in_move_price"     => 0,
                "in_return_qty"     => 0,
                "in_return_price"   => 0,
                "in_etc_qty"        => 0,
                "in_etc_price"      => 0,
                "in_qty"            => 0,
                "in_price"          => 0,
                "out_point_qty"     => 0,
                "out_point_price"   => 0,
                "out_move_qty"      => 0,
                "out_org_move_qty"  => 0,
                "out_move_sub_qty"  => 0,
                "out_move_price"    => 0,
                "out_return_qty"    => 0,
                "out_return_price"  => 0,
                "out_etc_qty"       => 0,
                "out_etc_price"     => 0,
                "out_qty"           => 0,
                "out_price"         => 0,
                "out_base_qty"      => 0,
                "out_base_price"    => 0,
            );
        }
        $product_receipt_list[$product_receipt['prd_unit']]["in_base_qty"]      += $product_receipt["in_base_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_base_price"]    += $product_receipt["in_base_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_point_qty"]     += $product_receipt["in_point_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_point_price"]   += $product_receipt["in_point_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_move_qty"]      += $product_receipt["in_move_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_org_move_qty"]  += $product_receipt["in_org_move_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_move_sub_qty"]  += $product_receipt["in_move_sub_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_move_price"]    += $product_receipt["in_move_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_return_qty"]    += $product_receipt["in_return_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_return_price"]  += $product_receipt["in_return_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_etc_qty"]       += $product_receipt["in_etc_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_etc_price"]     += $product_receipt["in_etc_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_qty"]           += $product_receipt["in_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["in_price"]         += $product_receipt["in_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_point_qty"]    += $product_receipt["out_point_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_point_price"]  += $product_receipt["out_point_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_move_qty"]     += $product_receipt["out_move_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_org_move_qty"] += $product_receipt["out_org_move_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_move_sub_qty"] += $product_receipt["out_move_sub_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_move_price"]   += $product_receipt["out_move_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_return_qty"]   += $product_receipt["out_return_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_return_price"] += $product_receipt["out_return_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_etc_qty"]      += $product_receipt["out_etc_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_etc_price"]    += $product_receipt["out_etc_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_qty"]          += $product_receipt["out_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_price"]        += $product_receipt["out_price"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_base_qty"]     += $product_receipt["out_base_qty"];
        $product_receipt_list[$product_receipt['prd_unit']]["out_base_price"]   += $product_receipt["out_base_price"];
    }
    else
    {
        $product_receipt["sch_sku"] = urlencode($product_receipt["sku"]);
        $product_receipt_list[] = $product_receipt;
    }

    $total_in_base_qty      += $product_receipt['in_base_qty'];
    $total_in_base_price    += $product_receipt['in_base_price'];
    $total_in_point_qty     += $product_receipt['in_point_qty'];
    $total_in_point_price   += $product_receipt['in_point_price'];
    $total_in_move_qty      += $product_receipt['in_move_qty'];
    $total_in_move_price    += $product_receipt['in_move_price'];
    $total_in_return_qty    += $product_receipt['in_return_qty'];
    $total_in_return_price  += $product_receipt['in_return_price'];
    $total_in_etc_qty       += $product_receipt['in_etc_qty'];
    $total_in_etc_price     += $product_receipt['in_etc_price'];
    $total_in_qty           += $product_receipt['in_qty'];
    $total_in_price         += $product_receipt['in_price'];
    $total_out_point_qty    += $product_receipt['out_point_qty'];
    $total_out_point_price  += $product_receipt['out_point_price'];
    $total_out_move_qty     += $product_receipt['out_move_qty'];
    $total_out_move_price   += $product_receipt['out_move_price'];
    $total_out_return_qty   += $product_receipt['out_return_qty'];
    $total_out_return_price += $product_receipt['out_return_price'];
    $total_out_etc_qty      += $product_receipt['out_etc_qty'];
    $total_out_etc_price    += $product_receipt['out_etc_price'];
    $total_out_qty          += $product_receipt['out_qty'];
    $total_out_price        += $product_receipt['out_price'];
}

if(!empty($chk_correct_list)){
    $chk_correct_data = json_encode($chk_correct_list);
    $smarty->assign("chk_correct_data", $chk_correct_data);
}


$total_out_base_qty     = $total_in_base_qty+$total_in_qty-$total_out_qty;
$total_out_base_price   = $total_in_base_price+$total_in_price-$total_out_price;

$smarty->assign("total_in_base_qty", $total_in_base_qty);
$smarty->assign("total_in_base_price", $total_in_base_price);
$smarty->assign("total_in_point_qty", $total_in_point_qty);
$smarty->assign("total_in_point_price", $total_in_point_price);
$smarty->assign("total_in_move_qty", $total_in_move_qty);
$smarty->assign("total_in_move_price", $total_in_move_price);
$smarty->assign("total_in_return_qty", $total_in_return_qty);
$smarty->assign("total_in_return_price", $total_in_return_price);
$smarty->assign("total_in_etc_qty", $total_in_etc_qty);
$smarty->assign("total_in_etc_price", $total_in_etc_price);
$smarty->assign("total_in_qty", $total_in_qty);
$smarty->assign("total_in_price", $total_in_price);
$smarty->assign("total_out_base_qty", $total_out_base_qty);
$smarty->assign("total_out_base_price", $total_out_base_price);
$smarty->assign("total_out_point_qty", $total_out_point_qty);
$smarty->assign("total_out_point_price", $total_out_point_price);
$smarty->assign("total_out_move_qty", $total_out_move_qty);
$smarty->assign("total_out_move_price", $total_out_move_price);
$smarty->assign("total_out_return_qty", $total_out_return_qty);
$smarty->assign("total_out_return_price", $total_out_return_price);
$smarty->assign("total_out_etc_qty", $total_out_etc_qty);
$smarty->assign("total_out_etc_price", $total_out_etc_price);
$smarty->assign("total_out_qty", $total_out_qty);
$smarty->assign("total_out_price", $total_out_price);
$smarty->assign("sch_reg_s_date", $sch_move_s_date);
$smarty->assign("sch_reg_e_date", $sch_move_e_date);
$smarty->assign("receipt_month", date("Y-m", strtotime("-1 months")));
$smarty->assign("logistics_company_list", array($with_log_c_no => "위드플레이스"));
$smarty->assign("brand_option", $unit_model->getBrandData());
$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("sch_unit_type_option", $unit_type_option);
$smarty->assign("product_receipt_list", $product_receipt_list);

$smarty->display('product_cms_stock_receipt_list_copy.html');
?>
