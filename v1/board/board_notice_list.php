<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/model/MyQuick.php');
require('../inc/model/Board.php');
require('../inc/model/Team.php');

# 기본 Model 설정
$board_model  = Board::Factory();
$board_model->setBoardNotice();

# 프로세스 처리
$process = (isset($_POST['process'])) ? $_POST['process'] : "";

if($process == "display_off")
{
	$b_no		= (isset($_POST['no']))?$_POST['no']:"";
    $search_url = isset($_POST['search_url'])?$_POST['search_url']:"";

    $board_model->update(array("no" => $b_no, "display" => 2));

	exit("<script>location.href='board_notice_list.php?{$search_url}';</script>");
}
elseif($process == "display_on")
{
	$b_no		= (isset($_POST['no']))?$_POST['no']:"";
	$search_url = isset($_POST['search_url'])?$_POST['search_url']:"";

	$board_model->update(array("no" => $b_no, "display" => 1));

	exit("<script>location.href='board_notice_list.php?{$search_url}';</script>");
}
elseif($process == "main_checkbox")
{
	$b_no 		= (isset($_POST['no'])) ? $_POST['no']:"";
	$board_item = $board_model->getItem($b_no);

    if($b_no && !empty($board_item))
    {
		$main_view = $board_item['main_view'] > 0 ? 0: 1;

		if (!$board_model->update(array("no" => $b_no, "main_view" => $main_view)))
			echo "Main 상태변경에 실패 했습니다.";
		else
			echo "Main 상태변경에 성공 했습니다.";
		exit;
	}
    else
    {
        echo "해당 게시글 정보가 없습니다";
        exit;
	}
}
elseif($process == "popup_checkbox")
{
	$b_no 		= (isset($_POST['no'])) ? $_POST['no']:"";
	$board_item = $board_model->getItem($b_no);

	if($b_no && !empty($board_item))
	{
		$popup_view = $board_item['popup_view'] > 0 ? 0: 1;

		if (!$board_model->update(array("no" => $b_no, "popup_view" => $popup_view)))
			echo "Popup 상태변경에 실패 했습니다.";
		else
			echo "Popup 상태변경에 성공 했습니다.";
		exit;
	}
	else
	{
		echo "해당 게시글 정보가 없습니다";
		exit;
	}
}
elseif($process=="main_slider")
{
	$b_no 		= (isset($_POST['no'])) ? $_POST['no']:"";
	$board_item = $board_model->getItem($b_no);

	if($b_no && !empty($board_item))
	{
		$main_slider = $board_item['main_slider'] > 0 ? 0: 1;

		if (!$board_model->update(array("no" => $b_no, "main_slider" => $main_slider)))
			echo "Main (image) 상태변경에 실패 했습니다.";
		else
			echo "Main (image) 상태변경에 성공 했습니다.";
		exit;
    }
	else
	{
        echo "해당 게시글 정보가 없습니다";
        exit;
    }
}

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# Board ID 체크
$b_id = isset($_GET['b_id']) ? $_GET['b_id'] : "";

if(empty($b_id)){
	echo("<script>alert('올바른 접근이 아닙니다.');location.href='/v1/main.php';</script>");
}

# 리스트 사용변수
$team_model 		 = Team::Factory();
$team_full_name_list = $team_model->getTeamFullNameList();

# 검색쿼리 시작
if(permissionNameCheck($session_permission,"마스터관리자") || permissionNameCheck($session_permission,"재무관리자"))
	$add_where = " 1=1";
else
	$add_where = " 1=1 AND (display = '1' OR (brd_n.display = '2' AND brd_n.s_no = {$session_s_no}))";

# 검색조건
$sch_title  = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_team 	= isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$sch_s_name = isset($_GET['sch_s_name'])?$_GET['sch_s_name']:"";
$sch_s_date = isset($_GET['sch_s_date'])?$_GET['sch_s_date']:"";
$sch_e_date = isset($_GET['sch_e_date'])?$_GET['sch_e_date']:"";
$ord_type 	= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";

if (!empty($b_id)) {
	$add_where .= " AND brd_n.b_id = '{$b_id}'";
	$smarty->assign("b_id", $b_id);
}

if (!empty($sch_title)) {
	$add_where .= " AND brd_n.title like '%{$sch_title}%'";
	$smarty->assign("sch_title", $sch_title);
}

if(!empty($sch_team))
{
	$sch_team_code_where = getTeamWhere($my_db, $sch_team);
	if($sch_team_code_where){
		$add_where .= " AND brd_n.team_code IN ({$sch_team_code_where})";
	}
	$smarty->assign("sch_team", $sch_team);
}

if(!empty($sch_s_name)) {
	$add_where .= " AND s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_s_name}%')";
	$smarty->assign("sch_s_name", $sch_s_name);
}

if(!empty($sch_s_date)){
	$sch_s_datetime  = "{$sch_s_date} 00:00:00";
	$add_where 		.= " AND brd_n.regdate >= '{$sch_s_datetime}'";
	$smarty->assign("sch_s_date", $sch_s_date);
}

if(!empty($sch_e_date)){
	$sch_e_datetime  = "{$sch_e_date} 23:59:59";
	$add_where 		.= " AND brd_n.regdate <= '{$sch_e_datetime}'";
	$smarty->assign("sch_e_date", $sch_e_date);
}

$add_orderby = "no DESC ";
if(!empty($ord_type))
{
	$ord_type_by = (isset($_GET['ord_type_by']) && !empty($_GET['ord_type_by'])) ? $_GET['ord_type_by'] : "";

	if($ord_type_by == '1'){
		$add_orderby = "team_name ASC";
	}elseif($ord_type_by == '2'){
		$add_orderby = "team_name DESC";
	}else{
		$add_orderby = "no DESC";
	}
}else{
	$ord_type_by = "";
}
$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);

#New & 댓글 체크
$new_tag = $comment_tag = 0;
$board_title = "";
if(!empty($b_id))
{
	$board_model->setBoardManager();
	$b_manager_item = $board_model->getWhereItem("b_id='{$b_id}'");

	$new_tag 		= isset($b_manager_item['new_display']) ? $b_manager_item['new_display'] : 0;
	$comment_tag 	= isset($b_manager_item['comment_display']) ? $b_manager_item['comment_display'] : 0;
	$board_title	= $b_manager_item['board_name'];
}
$smarty->assign("new_tag", $new_tag);
$smarty->assign("comment_tag", $comment_tag);
$smarty->assign("board_title", $board_title);

# 전체 게시물 수
$board_notice_total_sql		= "SELECT count(`no`) as cnt FROM board_notice brd_n WHERE {$add_where}";
$board_notice_total_query	= mysqli_query($my_db, $board_notice_total_sql);
$board_notice_total_result	= mysqli_fetch_array($board_notice_total_query);
$board_notice_total 		= $board_notice_total_result['cnt'];

# 페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
$pages 		= isset($_GET['page']) ? intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($board_notice_total/$num);

if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}

$search_url = getenv("QUERY_STRING");
if(!empty($search_url))
{
	$search_url_list = explode("&", $search_url);
	$chk_search_url = [];
	foreach($search_url_list as $sch_url){
		if(strpos($sch_url, "mode") === false && strpos($sch_url, "no") === false){
			$sch_url_val = explode("=", $sch_url, 2);
			if(!empty($sch_url_val[0])){
				$chk_search_url[$sch_url_val[0]] = "{$sch_url_val[0]}={$sch_url_val[1]}";
			}
		}
	}

	if(!empty($chk_search_url)){
		$search_url = implode("&", $chk_search_url);
	}
}
$page_list = pagelist($pages, "board_notice_list.php", $pagenum, $search_url);

$smarty->assign("total_num", $board_notice_total);
$smarty->assign("search_url", $search_url);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$board_notice_sql = "
	SELECT
		*,
		(SELECT t.team_name FROM team t WHERE t.team_code=brd_n.team_code) AS team_name,
		(SELECT s_name FROM staff s where s.s_no=brd_n.s_no) AS s_name,
		(SELECT count(br.r_no) FROM board_read br WHERE br.read_s_no='{$session_s_no}' AND br.b_no = brd_n.`no` AND br.b_id='{$b_id}') as read_cnt
	FROM board_notice brd_n
	WHERE {$add_where}
	ORDER BY {$add_orderby}
	LIMIT {$offset}, {$num}
";
$board_notice_query = mysqli_query($my_db, $board_notice_sql);
$board_notice_list	= [];
while($board_notice_array = mysqli_fetch_array($board_notice_query))
{
	if(!empty($board_notice_array['regdate'])) {
		$regdate_day_value 	= date("Y/m/d",strtotime($board_notice_array['regdate']));
		$regdate_time_value = date("H:i",strtotime($board_notice_array['regdate']));
	}else{
		$regdate_day_value	= "";
		$regdate_time_value	= "";
	}

	$file_origin = isset($board_notice_array['file_origin']) && !empty($board_notice_array['file_origin']) ? explode(',', $board_notice_array['file_origin']) : "";
	$file_read   = isset($board_notice_array['file_read']) && !empty($board_notice_array['file_read']) ? explode(',', $board_notice_array['file_read']) : "";

	$yet_comment_cnt = 0;
	if($comment_tag == '1')
	{
		$b_read_where = "";
		if($board_notice_array['read_cnt'] > 0){
			$board_read_sql 	= "SELECT * FROM board_read WHERE b_id='{$b_id}' AND b_no='{$board_notice_array['no']}' AND read_s_no='{$session_s_no}' LIMIT 1";
			$board_read_query 	= mysqli_query($my_db, $board_read_sql);
			$board_read_result 	= mysqli_fetch_assoc($board_read_query);

			if(isset($board_read_result['read_upd_date'])){
				$b_read_where = "AND regdate > '{$board_read_result['read_upd_date']}'";
			}
		}

		$comment_chk_sql 	= "SELECT count(`no`) as cnt FROM comment WHERE `table_name`='board_notice' AND b_id='{$b_id}' AND table_no='{$board_notice_array['no']}' AND display='1' {$b_read_where}";
		$comment_chk_query 	= mysqli_query($my_db, $comment_chk_sql);
		$comment_chk_result = mysqli_fetch_assoc($comment_chk_query);

		if(isset($comment_chk_result['cnt'])){
			$yet_comment_cnt = $comment_chk_result['cnt'];
		}
	}

	$read_cnt = $board_notice_array['read_cnt'];
	if($board_notice_array['regdate'] < '2022-05-19 00:00:00'){
		$read_cnt 		 = 1;
		$yet_comment_cnt = 0;
	}

	$is_editable = false;
	if (permissionNameCheck($session_permission,"마스터관리자") || permissionNameCheck($session_permission,"재무관리자") || $board_notice_array['s_no'] == $session_s_no){
		$is_editable = true;
	}

	$board_notice_array['regdate_day'] 		= $regdate_day_value;
	$board_notice_array['regdate_time'] 	= $regdate_time_value;
	$board_notice_array['file_origin'] 		= $file_origin;
	$board_notice_array['file_read'] 		= $file_read;
	$board_notice_array['read_cnt'] 		= $read_cnt;
	$board_notice_array['yet_comment_cnt'] 	= $yet_comment_cnt;
	$board_notice_array['is_editable'] 		= $is_editable;

	$board_notice_list[] = $board_notice_array;
}
$smarty->assign("board_notice_list", $board_notice_list);

# Navigation & My Quick
$nav_prd_no  = "";
$is_my_quick = "";
if($session_my_c_type != '3')
{
	$quick_model = MyQuick::Factory();
	switch($b_id){
		case "wp_down00":
			$nav_prd_no = "88";
			break;
	}

	if(!empty($nav_prd_no)){
		$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
	}

}
$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_prd_no", $nav_prd_no);

$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("sch_team_list", $team_full_name_list);


$smarty->display('board/board_notice_list.html');
?>
