<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_upload.php');
require('../inc/model/Kind.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

$editor_folder_name = folderCheck('board_image');
$smarty->assign('editor_path', 'board_image/'.$editor_folder_name);

# 가이드 등록
$b_no       = isset($_GET['b_no']) ? $_GET['b_no'] : "";
$mode       = isset($_GET['mode']) ? $_GET['mode'] : "";
$guide	    = [];
$keyword_list = [];
$guide_g_name = "";

if($mode != 'read' && $mode != 'write' && $mode != 'modify')
{
    $mode = 'read';
}

$process        = isset($_POST['process']) ? $_POST['process'] : "";
$regist_url     = "board_guide_regist.php";
$list_url       = "board_guide_list.php";

# 리스트 페이지 search_url 생성
$sch_page       = isset($_GET['page']) ? $_GET['page'] : "1";
$sch_page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$search_url     = "page={$sch_page}&ord_page_type={$sch_page_type}";

$sch_array_key  = array("sch_guide_g1","sch_guide_g2","sch_b_no","sch_question","sch_manager","sch_display","sch_keyword");
foreach($sch_array_key as $sch_key)
{
    $sch_val   = isset($_GET[$sch_key]) && !empty($_GET[$sch_key]) ? $_GET[$sch_key]: "";
    if(!!$sch_val)
    {
        $search_url .= "&{$sch_key}={$sch_val}";
    }
}
$smarty->assign("search_url", $search_url);

# Process
if($process == 'new_guide') # 가이드 생성
{
    // 필수정보
    $search_url     = isset($_POST['search_url']) ? $_POST['$search_url'] : "";
    $guide_g2       = isset($_POST['guide_g2']) ? $_POST['guide_g2'] : "";
    $question       = isset($_POST['question']) ? addslashes($_POST['question']) : "";
    $keyword        = isset($_POST['keyword']) && !empty($_POST['keyword']) ? implode(',', $_POST['keyword']) : "";
    $manager        = isset($_POST['manager']) ? $_POST['manager'] : "";
    $contents       = isset($_POST['contents']) ? addslashes($_POST['contents']) : "";
    $display        = isset($_POST['display']) ? $_POST['display'] : "1";
    $regdate        = date('Y-m-d H:i:s');

    # 필수정보 체크 및 저장
    if(empty($guide_g2) || empty($question) || empty($manager))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');location.href='{$regist_url}?{$search_url}';</script>");
    }

    $ins_sql = "
        INSERT INTO board_guide SET
            `k_name_code`   = '{$guide_g2}',
            `question`      = '{$question}',
            `keyword`       = '{$keyword}',
            `manager`       = '{$manager}',
            `contents`      = '{$contents}',
            `display`       = '{$display}',
            `regdate`       = '{$regdate}'
    ";

    // Dropbox 이미지 확인 및 저장
    $file_paths  = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_names  = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk    = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    if($file_chk)
    {
        if(!empty($file_paths) && !empty($file_names))
        {
            $file_read   = move_store_files($file_paths, "dropzone_tmp", "board_guide");
            $file_name   = implode(',', $file_names);
            $file_path   = implode(',', $file_read);

            $ins_sql .= ",file_name='{$file_name}', file_path='{$file_path}'";
        }
    }

    if(mysqli_query($my_db, $ins_sql))
    {
        exit("<script>alert('가이드 등록에 성공했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }else{
        exit("<script>alert('가이드 등록에 실패했습니다');history.back();</script>");
    }
}
elseif($process == 'modify_guide') # 가이드 수정
{
    $search_url     = isset($_POST['search_url']) ? $_POST['$search_url'] : "";
    $b_no           = isset($_POST['b_no']) ? $_POST['b_no'] : "";
    $guide_g2       = isset($_POST['guide_g2']) ? $_POST['guide_g2'] : "";
    $question       = isset($_POST['question']) ? addslashes($_POST['question']) : "";
    $keyword        = isset($_POST['keyword']) && !empty($_POST['keyword']) ? implode(',', $_POST['keyword']) : "";
    $manager        = isset($_POST['manager']) ? $_POST['manager'] : "";
    $contents       = isset($_POST['contents']) ? addslashes($_POST['contents']) : "";
    $display        = isset($_POST['display']) ? $_POST['display'] : "1";

    # 필수정보 체크 및 저장
    if(empty($b_no) || empty($guide_g2) || empty($question) || empty($manager))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 수정해 주세요.');history.back();</script>");
    }

    $upd_sql = "
        UPDATE board_guide SET
            `k_name_code`   = '{$guide_g2}',
            `question`      = '{$question}',
            `keyword`       = '{$keyword}',
            `manager`       = '{$manager}',
            `contents`      = '{$contents}',
            `display`       = '{$display}'
    ";

    // Dropbox 이미지 확인 및 저장
    $file_origin_path = isset($_POST['file_origin_path']) ? $_POST['file_origin_path'] : "";
    $file_origin_name = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_paths  = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_names  = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk    = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    if($file_chk)
    {
        if(!empty($file_paths) && !empty($file_names))
        {
            $file_read = move_store_files($file_paths, "dropzone_tmp", "board_guide");
            $file_name = implode(',', $file_names);
            $file_path = implode(',', $file_read);

            $upd_sql .= ",file_name='{$file_name}', file_path='{$file_path}'";
        }else{
            $upd_sql .= ",file_name='', file_path=''";
        }

        if(!empty($file_origin_path) && !empty($file_origin_name))
        {
            if(!empty($file_paths) && !empty($file_names))
            {
                $del_images = array_diff(explode(',', $file_origin_path), $file_paths);
            }else{
                $del_images = explode(',', $file_origin_path);
            }

            del_files($del_images);
        }
    }

    $upd_sql .= " WHERE b_no = '{$b_no}'";

    if(mysqli_query($my_db, $upd_sql)) {
        exit("<script>alert('가이드 수정에 성공했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }else{
        exit("<script>alert('가이드 수정에 실패했습니다');location.href='{$regist_url}?b_no={$b_no}&mode=modify&{$search_url}';</script>");
    }
}
elseif($process == 'duplicate_guide') # 가이드 복제
{
    $b_no       = isset($_POST['b_no']) ? $_POST['b_no'] : "";
    $mode       = "write";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(!$b_no) {
        exit("<script>alert('가이드 복제에 실패했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }

    $dup_guide_sql   = "SELECT *, (SELECT k.k_parent FROM kind k WHERE k.k_name_code = `bg`.k_name_code) as guide_g1 FROM board_guide `bg` WHERE `bg`.b_no = {$b_no} LIMIT 1";
    $dup_guide_query = mysqli_query($my_db, $dup_guide_sql);
    $guide 		     = mysqli_fetch_assoc($dup_guide_query);

    $guide['b_no'] = "";
    $guide['manager']       = $session_s_no;
    $guide['manager_name']  = $session_name;
    $guide_g1               = isset($guide['guide_g1']) ? sprintf('%05d', $guide['guide_g1']) : "";
}
elseif($process == 'delete_guide') # 가이드 삭제
{
    $b_no       = isset($_POST['b_no']) ? $_POST['b_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(!$b_no) {
        exit("<script>alert('가이드 삭제에 실패했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }

    $upd_sql = "UPDATE board_guide SET `display`='2' WHERE b_no='{$b_no}'";
    $del_sql = "DELETE FROM board_guide WHERE b_no='{$b_no}'";

    if(mysqli_query($my_db, $upd_sql)) {
        exit("<script>alert('가이드 삭제에 성공했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }else{
        exit("<script>alert('가이드 삭제에 실패했습니다');location.href='{$list_url}?{$search_url}';</script>");
    }
}
elseif ($process == "comment_add") # 댓글 등록
{
    $table_no 	 = (isset($_POST['table_no'])) ? $_POST['table_no'] : "";
    $comment_new = (isset($_POST['f_comment_new'])) ? $_POST['f_comment_new'] : "";
    $is_secret 	 = (isset($_POST['is_secret'])) ? 1 : 0;
    $is_unknown  = (isset($_POST['is_unknown'])) ? 1 : 0;
    $regdate     = date("Y-m-d H:i:s");
    $search_url  = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    # 코멘트 이미지 업로드
    $img_add_set = "";
    $f_comm_img     = $_FILES['f_comm_img'];
    if(isset($f_comm_img['name']) && !empty($f_comm_img['name'])){
        $img_path    = add_store_file($f_comm_img, "board_comment");
        $img_name    = $f_comm_img['name'];
        $img_add_set = ", `img_path`='{$img_path}', `img_name`='{$img_name}'";
    }

    $sql = "INSERT INTO `comment` SET
					`table_name` = 'board_guide',
					b_id = 'board_guid',
					table_no = '{$table_no}',
					`comment` = '".addslashes($comment_new)."',
					team = (SELECT s.team	FROM staff s WHERE s.s_no = '{$session_s_no}'),
					s_no = '{$session_s_no}',
					regdate = '{$regdate}',
					is_secret = {$is_secret},
					is_unknown = {$is_unknown}
					{$img_add_set}
		";

    if(!mysqli_query($my_db, $sql)) {
        echo("<script>alert('코멘트 추가에 실패 하였습니다');history.back();</script>");
    }
    exit ("<script>location.href='board_guide_regist.php?b_no={$table_no}&mode=read&{$search_url}';</script>");

}
elseif ($process == "comment_delete") # 댓글 삭제
{
    $cmt_no = (isset($_POST['cmt_no'])) ? $_POST['cmt_no'] : "";

    $sql = "UPDATE `comment` cmt SET cmt.display = '2' WHERE cmt.no = '{$cmt_no}'";

    if(mysqli_query($my_db, $sql)) {

        $comment_sub_del_sql = "UPDATE `comment` SET `display`='2' WHERE group_no='{$cmt_no}'";
        mysqli_query($my_db, $comment_sub_del_sql);

        echo("<script>alert('삭제 하였습니다');history.back();</script>");
    } else {
        echo("<script>alert('삭제에 실패 하였습니다');history.back();</script>");
    }

    exit;
}
elseif ($process == "comment_main_mod") # 댓글 수정
{
    $table_no 	  = (isset($_POST['table_no'])) ? $_POST['table_no'] : "";
    $main_comm_no = (isset($_POST['main_comm_no'])) ? $_POST['main_comm_no'] : "";
    $comment_val  = (isset($_POST['f_comment'])) ? addslashes($_POST['f_comment']) : "";
    $search_url  = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(empty($main_comm_no)){
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }

    $upd_sql = "UPDATE `comment` SET comment='{$comment_val}' WHERE `no`='{$main_comm_no}'";

    if(!mysqli_query($my_db, $upd_sql)) {
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }else{
        exit ("<script>location.href='board_guide_regist.php?b_no={$table_no}&mode=read&{$search_url}';</script>");
    }

}
elseif ($process == "comment_sub_add") # 답글 등록
{
    $table_no 	 = (isset($_POST['table_no'])) ? $_POST['table_no'] : "";
    $group_no 	 = (isset($_POST['group_no'])) ? $_POST['group_no'] : "";
    $parent_no 	 = (isset($_POST['parent_no'])) ? $_POST['parent_no'] : "";
    $depth 	     = (isset($_POST['depth'])) ? $_POST['depth']+1 : "1";
    $max_depth 	 = (isset($_POST['max_depth'])) ? $_POST['max_depth'] : "";
    $seq_val     = (isset($_POST['seq'])) ? $_POST['seq'] : "";

    $comment_new = (isset($_POST['f_comment_new'])) ? $_POST['f_comment_new'] : "";
    $is_secret 	 = (isset($_POST['is_secret'])) ? 1 : 0;
    $is_unknown  = (isset($_POST['is_unknown'])) ? 1 : 0;
    $regdate     = date("Y-m-d H:i:s");
    $search_url  = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(empty($seq_val)){
        $seq_max_sql    = "SELECT MAX(seq) as max_seq FROM `comment` WHERE display = '1' AND group_no='{$group_no}'";
        $seq_max_query  = mysqli_query($my_db, $seq_max_sql);
        $seq_max_result = mysqli_fetch_array($seq_max_query);
        $seq            = isset($seq_max_result['max_seq']) && !empty($seq_max_result['max_seq']) ? $seq_max_result['max_seq']+1 : 1;
    }else{

        if($max_depth == $depth){
            $max_chk_no = $parent_no;
        }else{
            $max_chk_no = $parent_no;
            for($i=$depth; $i<$max_depth; $i++)
            {
                if(empty($max_chk_no)){
                    break;
                }
                $max_chk_sql    = "SELECT `no`, seq FROM `comment` WHERE display = '1' AND group_no='{$group_no}' AND parent_no='{$max_chk_no}' ORDER BY seq DESC LIMIT 1";
                $max_chk_query  = mysqli_query($my_db, $max_chk_sql);
                $max_chk_result = mysqli_fetch_assoc($max_chk_query);
                $max_chk_no     = isset($max_chk_result['no']) && !empty($max_chk_result['no']) ? $max_chk_result['no'] : $parent_no;
                $seq_val        = isset($max_chk_result['seq']) && !empty($max_chk_result['seq']) ? $max_chk_result['seq'] : $seq_val;
            }
        }

        $seq_max_sql    = "SELECT MAX(seq) as max_seq FROM `comment` WHERE display = '1' AND group_no='{$group_no}' AND parent_no='{$max_chk_no}'";
        $seq_max_query  = mysqli_query($my_db, $seq_max_sql);
        $seq_max_result = mysqli_fetch_array($seq_max_query);
        $seq_max_value  = isset($seq_max_result['max_seq']) && !empty($seq_max_result['max_seq']) ? $seq_max_result['max_seq'] : $seq_val;
        $seq            = $seq_max_value+1;

        $seq_chk_sql    = "SELECT `no` FROM `comment` WHERE display = '1' AND group_no='{$group_no}' AND seq > '{$seq_max_value}' ORDER BY seq ASC";
        $seq_chk_query  = mysqli_query($my_db, $seq_chk_sql);
        $seq_chk_up     = $seq;
        while($seq_chk_result = mysqli_fetch_array($seq_chk_query))
        {
            if($seq_chk_result['no']){
                $seq_chk_up++;
                $seq_upd_sql = "UPDATE `comment` SET seq='{$seq_chk_up}' WHERE `no`='{$seq_chk_result['no']}'";
                mysqli_query($my_db, $seq_upd_sql);
            }
        }
    }

    $sql = "INSERT INTO `comment` SET
					`table_name` = 'board_guide',
					b_id       = 'board_guid',
					table_no   = '{$table_no}',
					group_no   = '{$group_no}',
					parent_no  = '{$parent_no}',
					`depth`    = '{$depth}',
					`seq`      = '{$seq}',
					`comment`  = '".addslashes($comment_new)."',
					team       = (SELECT s.team	FROM staff s WHERE s.s_no = '{$session_s_no}'),
					s_no       = '{$session_s_no}',
					regdate    = '{$regdate}',
					is_secret  = {$is_secret},
					is_unknown = {$is_unknown}
		";

    if(!mysqli_query($my_db, $sql)) {
        echo("<script>alert('코멘트 추가에 실패 하였습니다');history.back();</script>");
    }else{
        $sub_comm_no = mysqli_insert_id($my_db);
        exit ("<script>location.href='board_guide_regist.php?b_no={$table_no}&mode=read&{$search_url}';</script>");
    }

}
elseif ($process == "comment_sub_mod") # 답글 수정
{
    $table_no 	 = (isset($_POST['table_no'])) ? $_POST['table_no'] : "";
    $sub_comm_no = (isset($_POST['sub_comm_no'])) ? $_POST['sub_comm_no'] : "";
    $comment_val = (isset($_POST['f_comment'])) ? addslashes($_POST['f_comment']) : "";
    $search_url  = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(empty($sub_comm_no)){
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }

    $upd_sql = "UPDATE `comment` SET comment='{$comment_val}' WHERE `no`='{$sub_comm_no}'";

    if(!mysqli_query($my_db, $upd_sql)) {
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }else{
        exit ("<script>location.href='board_guide_regist.php?b_no={$table_no}&mode=read&{$search_url}';</script>");
    }

}
elseif ($process == "comment_sub_delete") # 답글 삭제
{
    $table_no    = (isset($_POST['table_no'])) ? $_POST['table_no'] : "";
    $group_no    = (isset($_POST['group_no'])) ? $_POST['group_no'] : "";
    $cmt_no      = (isset($_POST['parent_no'])) ? $_POST['parent_no'] : "";
    $depth       = (isset($_POST['depth'])) ? $_POST['depth'] : "";
    $max_depth   = (isset($_POST['max_depth'])) ? $_POST['max_depth'] : "";

    $sql = "UPDATE `comment` cmt SET cmt.display = '2' WHERE cmt.no = '{$cmt_no}'";

    if (mysqli_query($my_db, $sql)) {
        if ($max_depth > $depth) {
            $sub_del_no_list[] = $cmt_no;
            for ($i = $depth; $i < $max_depth; $i++) {
                $sub_de_no_chk = implode(",", $sub_del_no_list);
                $sub_de_no_chk_sql = "SELECT no FROM comment WHERE parent_no IN({$sub_de_no_chk}) AND display='1' AND group_no='{$group_no}'";
                $sub_de_no_chk_query = mysqli_query($my_db, $sub_de_no_chk_sql);
                while ($sub_de_no_chk_result = mysqli_fetch_assoc($sub_de_no_chk_query)) {
                    $sub_del_no_list[] = $sub_de_no_chk_result['no'];
                }
            }

            if (!empty($sub_del_no_list)) {
                $sub_del_no_list = array_unique($sub_del_no_list);
                $sub_de_no_chk = implode(",", $sub_del_no_list);
                $comment_sub_del_sql = "UPDATE `comment` SET `display`='2' WHERE `no` IN({$sub_de_no_chk})";
                mysqli_query($my_db, $comment_sub_del_sql);
            }

        }

        echo("<script>alert('삭제 하였습니다');history.back();</script>");
    } else {
        echo("<script>alert('삭제에 실패 하였습니다');history.back();</script>");
    }

    exit;
}
elseif($b_no)
{
    $guide_sql 	 = "
      SELECT *, 
        (SELECT s_name FROM staff WHERE s_no=manager) as manager_name, 
        (SELECT k.k_name FROM kind k WHERE k.k_name_code = `bg`.k_name_code) as guide_g2_name,
        (SELECT k.k_parent FROM kind k WHERE k.k_name_code = `bg`.k_name_code) as guide_g1,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code =(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code = `bg`.k_name_code LIMIT 1)) as guide_g1_name,
        (SELECT count(bgr.r_no) FROM board_guide_read bgr WHERE bgr.read_s_no='{$session_s_no}' AND bgr.b_no = bg.b_no) as read_cnt
      FROM board_guide `bg` 
      WHERE `bg`.b_no = {$b_no} LIMIT 1";
    $guide_query = mysqli_query($my_db, $guide_sql);
    $guide 		 = mysqli_fetch_assoc($guide_query);
    $b_no        = $guide['b_no'];
    $guide_g1    = isset($guide['guide_g1']) ? sprintf('%05d', $guide['guide_g1']) : "";

    # 파일 리스트 처리
    $file_paths = $guide['file_path'];
    $file_names = $guide['file_name'];
    $guide['file_count'] = 0;

    if(!empty($file_paths) && !empty($file_names))
    {
        $guide['file_origin_path'] = $file_paths;
        $guide['file_origin_name'] = $file_names;
        $guide['file_paths'] = explode(',', $file_paths);
        $guide['file_names'] = explode(',', $file_names);
        $guide['file_count'] = count($guide['file_paths']);
    }

    if($mode == 'read')
    {
        # 읽기 시 b_no 체크
        if(empty($b_no)) {
            exit("<script>alert('게시글이 없습니다.');location.href='{$list_url}?{$search_url}';</script>");
        }

        $hit = $guide['hit']+1;
        $hit_upd_sql = "UPDATE board_guide SET hit={$hit} WHERE b_no='{$b_no}'";
        mysqli_query($my_db, $hit_upd_sql);

        if($guide['read_cnt'] == '0'){
            $read_upd_sql = "INSERT INTO board_guide_read SET b_no='{$b_no}', read_s_no='{$session_s_no}', read_date=now()";
            mysqli_query($my_db, $read_upd_sql);
        }

        $keyword_list = !empty($guide['keyword']) ? explode(',', $guide['keyword']) : [];

        # 댓글
        $comment_sql = "
            SELECT
                cmt.no,
                cmt.table_name,
                cmt.b_id,
                cmt.table_no,
                cmt.parent_no,
                cmt.comment,
                cmt.team,
                cmt.s_no,
                (SELECT s_name FROM staff s where s.s_no=cmt.s_no) AS s_name,
                cmt.regdate,
                cmt.is_secret,
                cmt.is_unknown,
                cmt.img_path,
                cmt.img_name,
                (SELECT count(sub_cmt.`no`) FROM comment sub_cmt WHERE sub_cmt.group_no = cmt.no AND sub_cmt.`display`='1') as sub_comm_cnt,
                (SELECT MAX(sub_cmt.`depth`) FROM comment sub_cmt WHERE sub_cmt.group_no = cmt.no AND sub_cmt.`display`='1') as sub_max_depth
            FROM `comment` cmt
            WHERE cmt.table_no = '{$b_no}'
                AND cmt.b_id='board_guid'
                AND cmt.table_name='board_guide'
                AND cmt.display = '1'
                AND (cmt.parent_no is null OR cmt.parent_no = '0')
            ORDER BY regdate DESC
        ";
        $comment_query = mysqli_query($my_db, $comment_sql);
        $total_comment_cnt = 0;
        while($comment_array = mysqli_fetch_array($comment_query))
        {
            $sub_comm_list = [];
            if($comment_array['sub_comm_cnt'] > 0)
            {
                $sub_comm_where = "cmt.table_no IN ({$b_no}) AND cmt.b_id='board_guid' AND cmt.table_name='board_guide' AND cmt.display = '1' AND cmt.group_no = '{$comment_array['no']}'";
                $sub_comm_sql = "
                        SELECT
                            *,
                            (SELECT s_name FROM staff s where s.s_no=cmt.s_no) AS s_name
                        FROM comment cmt WHERE {$sub_comm_where} ORDER BY cmt.seq ASC, cmt.regdate DESC";

                $sub_comm_query = mysqli_query($my_db, $sub_comm_sql);
                while($sub_comm_result = mysqli_fetch_assoc($sub_comm_query))
                {
                    $sub_comm_result['comment_mod'] = $sub_comm_result['comment']; // 코멘트 수정용 처리
                    $sub_comm_result['comment']     = str_replace("\r\n","<br />",htmlspecialchars($sub_comm_result['comment'])); // 코멘트 개행문자 처리
                    $sub_comm_result['regdate_day'] = date("Y/m/d",strtotime($sub_comm_result['regdate']));
                    $sub_comm_result['regdate_time']   = date("H:i",strtotime($sub_comm_result['regdate']));
                    $sub_comm_result['depth_width'] = ($sub_comm_result['depth']*30)."px";
                    $sub_comm_list[] = $sub_comm_result;
                }
            }

            $comment_value = str_replace("\r\n","<br />",htmlspecialchars($comment_array['comment'])); // 코멘트 개행문자 처리
            $comment_regdate_day_value = date("Y/m/d",strtotime($comment_array['regdate']));
            $comment_regdate_time_value = date("H:i",strtotime($comment_array['regdate']));

            $comment_list[] = array(
                "no"            => $comment_array['no'],
                "table_name"    => $comment_array['table_name'],
                "b_id"          => $comment_array['b_id'],
                "table_no"      => $comment_array['table_no'],
                "comment"       => $comment_value,
                "comment_mod"   => $comment_array['comment'],
                "team"          => $comment_array['team'],
                "s_no"          => $comment_array['s_no'],
                "s_name"        => $comment_array['s_name'],
                "regdate_day"   => $comment_regdate_day_value,
                "regdate_time"  => $comment_regdate_time_value,
                "is_secret"     => $comment_array['is_secret'],
                "is_unknown"    => $comment_array['is_unknown'],
                "img_path"      => $comment_array['img_path'],
                "img_name"      => $comment_array['img_name'],
                "sub_comm_cnt"  => $comment_array['sub_comm_cnt'],
                "sub_max_depth" => $comment_array['sub_max_depth'],
                "sub_comm_list" => $sub_comm_list
            );

            $total_comment_cnt += ($comment_array['sub_comm_cnt']+1);
        }
    }
    $smarty->assign("comment_list", $comment_list);
}

# 읽기 시 b_no 체크
if($mode == 'read' && empty($b_no))
{
    exit("<script>alert('게시글이 없습니다.');location.href='{$list_url}?{$search_url}';</script>");
}

# 작성가능 여부 체크
$is_editable = false;
if(($mode == 'modify' && ($guide['manager'] == $session_s_no || permissionNameCheck($session_permission, "마스터관리자"))) || $mode == 'write')
{
    $is_editable = true;
}

if(empty($guide['b_no']) && $mode == 'write'){
    $max_b_no_sql = "SELECT MAX(b_no) as m_b_no FROM board_guide";
    $max_b_no_query = mysqli_query($my_db, $max_b_no_sql);
    $max_b_no_result = mysqli_fetch_assoc($max_b_no_query);
    $max_b_no = isset($max_b_no_result['m_b_no']) && !empty($max_b_no_result['m_b_no']) ? $max_b_no_result['m_b_no']+1 : 1;

    if($max_b_no) {
        $max_del_guide_sql = "DELETE FROM board_guide_url WHERE b_no='{$max_b_no}'";
        mysqli_query($my_db, $max_del_guide_sql);
    }
    $guide['b_no'] = $max_b_no;
}

if(empty($guide['manager'])){
    $guide['manager'] = $session_s_no;
}

if(empty($guide['manager_name'])){
    $guide['manager_name'] = $session_name;
}

# 가이드 그룹리스트 처리
$kind_model         = Kind::Factory();
$guide_group_list   = $kind_model->getKindGroupList("guide");
$guide_g1_list = $guide_g2_list = [];
foreach($guide_group_list as $key => $guide_data){
    if(!$key){
        $guide_g1_list = $guide_data;
    }else{
        $guide_g2_list[$key] = $guide_data;
    }
}
$sch_guide_g2_list = isset($guide_g2_list[$guide_g1]) ? $guide_g2_list[$guide_g1] : [];

$smarty->assign("guide_g1_list", $guide_g1_list);
$smarty->assign("guide_g2_list", $sch_guide_g2_list);

if(!isset($guide['file_paths']) || !isset($guide['file_names']))
{
    $guide['file_paths'] = [];
    $guide['file_names'] = [];
}

$file_count = !empty($guide['file_paths']) ? count($guide['file_paths']) : 0;
$smarty->assign("file_count", $file_count);
$smarty->assign($guide);
$smarty->assign("keyword_list", $keyword_list);
$smarty->assign("display_list", getDisplayOption());
$smarty->assign("mode", $mode);
$smarty->assign("is_editable", $is_editable);

$smarty->display('board/board_guide_regist.html');


?>

