<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_upload.php');
require('../inc/model/Board.php');

# 기본 Model 설정
$board_model = Board::Factory();
$board_model->setBoardNotice();

# Query String no 빼고 처리
$search_url = getenv("QUERY_STRING");
if(substr($search_url, 0, 2) == 'no'){ // 첫번째 값(no) 제거하기
    $search_url = substr($search_url, strpos($search_url,"&")+1);
}
$smarty->assign("search_url", $search_url);

# 프로세스 처리
$process    = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "del_board")
{
    $b_no  			 = $_POST['no'];
    $b_id 			 = $_POST['b_id'];
    $search_url_post = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(!$b_no || !$b_id){
        exit("<script>alert('삭제에 실패 하였습니다');location.href='board_notice_detail.php?no={$b_no}&{$search_url}';</script>");
    }

    if(!$board_model->delete($b_no)){
        exit("<script>alert('해당 게시글을 삭제에 실패 하였습니다');location.href='board_notice_detail.php?no={$b_no}&{$search_url_post}';</script>");
    }else{
        exit("<script>alert('해당 게시글을 삭제 하였습니다');location.href='board_notice_list.php?{$search_url_post}';</script>");
    }
}
elseif($process == "write")
{
    $add_set = "";

    if(!empty($_POST['b_id'])){
        $add_set    .= "b_id = '{$_POST['b_id']}', ";
        $b_id       = $_POST['b_id'];
    }

    if(!empty($_POST['search_url'])){
        $search_url = $_POST['search_url'];
    }

    if(!empty($_POST['f_title'])){
        $add_set    .= "title = '".addslashes(trim($_POST['f_title']))."', ";
    }

    if(!empty($_POST['f_contents'])){
        $add_set    .= "contents = '".addslashes(trim($_POST['f_contents']))."', ";
    }

    // Dropbox 이미지 확인 및 저장
    $file_path = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_name = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk  = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    if($file_chk)
    {
        if(!empty($file_path) && !empty($file_name))
        {
            $file_read   = move_store_files($file_path, "dropzone_tmp", "board_notice");
            $file_name_list = [];
            foreach($file_name as $file_name_val){
                $file_name_list[] = str_replace(',', '_', $file_name_val);
            }
            $file_origin = implode(',', $file_name_list);
            $file_read   = implode(',', $file_read);

            $add_set .= "file_origin='{$file_origin}', file_read='{$file_read}', ";
        }
    }

    if(!empty($_POST['f_s_no'])){
        $add_set .= "team_code = '{$_POST['f_team']}', ";
        $add_set .= "s_no = '{$_POST['f_s_no']}', ";
    }

    $add_set    .= "main_view = '{$_POST['f_main_view']}', ";
    $add_set    .= "popup_view = '{$_POST['f_popup_view']}', ";
    $add_set    .= "main_slider = '{$_POST['f_main_slider']}', ";

    $ins_sql    = "
        INSERT INTO board_notice SET
            {$add_set}
            regdate = now()
    ";

    if (mysqli_query($my_db, $ins_sql))
    {
        exit("<script>location.href='board_notice_list.php?b_id={$b_id}';</script>");
    } else {
        exit("<script>alert('등록에 실패 하였습니다');location.href='board_notice_list.php?{$search_url}';</script>");
    }
}
elseif($process == "modify")
{
    $add_set = "";

    if(!empty($_POST['b_id'])){
        $add_set    .= "b_id = '{$_POST['b_id']}', ";
    }

    if(!empty($_POST['search_url'])){
        $search_url = $_POST['search_url'];
    }

    if(!empty($_POST['f_title'])){
        $add_set    .= "title = '".addslashes(trim($_POST['f_title']))."', ";
    }

    if(!empty($_POST['f_contents'])){
        $add_set    .= "contents = '".addslashes(trim($_POST['f_contents']))."', ";
    }

    if(!empty($_POST['f_s_no'])){
        $add_set    .= "s_no = '{$_POST['f_s_no']}', ";
        $add_set    .= "team_code = '{$_POST['f_team']}', ";
    }

    $add_set    .= "main_view = '{$_POST['f_main_view']}', ";
    $add_set    .= "popup_view = '{$_POST['f_popup_view']}', ";
    $add_set    .= "main_slider = '{$_POST['f_main_slider']}', ";

    // Dropbox 이미지 확인 및 저장
    $file_origin_read = isset($_POST['file_origin_read']) ? $_POST['file_origin_read'] : "";
    $file_origin_name = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_path  = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_name  = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk   = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    if($file_chk)
    {
        if(!empty($file_path) && !empty($file_name))
        {
            $file_read   = move_store_files($file_path, "dropzone_tmp", "board_notice");
            $file_name_list = [];
            foreach($file_name as $file_name_val){
                $file_name_list[] = str_replace(',', '_', $file_name_val);
            }
            $file_origin = implode(',', $file_name_list);
            $file_read   = implode(',', $file_read);

            $add_set .= "file_origin='{$file_origin}', file_read='{$file_read}', ";
        }else{
            $add_set .= "file_origin='', file_read='', ";
        }

        if(!empty($file_origin_read) && !empty($file_origin_name))
        {
            if(!empty($file_path) && !empty($file_name))
            {
                $del_images = array_diff(explode(',', $file_origin_read), $file_path);
            }else{
                $del_images = explode(',', $file_origin_read);
            }

            del_files($del_images);
        }
    }

    $add_set = rtrim($add_set); // 뒤쪽 여백 제거
    if(substr($add_set, -1) == ","){ // 마지막 글자가 , 인지 확인
        $add_set = substr($add_set , 0, -1); // 마지막 한글자 제거
    }

    $upd_sql = "UPDATE board_notice SET {$add_set} WHERE `no` = '{$_POST['no']}'";

    if (mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('수정이 완료 되었습니다');location.href='board_notice_list.php?{$search_url}';</script>");
    } else {
        exit("<script>alert('수정에 실패 하였습니다');location.href='board_notice_list.php?{$search_url}';</script>");
    }
}

$dir_location       = "../";
$editor_folder_name = folderCheck('board_image');
$smarty->assign("dir_location", $dir_location);
$smarty->assign('editor_path', 'board_image/'.$editor_folder_name);

# 기본 게시판 no, b_id 체크 및 저장
$no     = isset($_GET['no']) ? $_GET['no'] : "";
$b_id   = isset($_GET['b_id']) ? $_GET['b_id'] : "";
$mode   = isset($_GET['mode']) ? $_GET['mode'] : "read";

$smarty->assign("no",  $no);
$smarty->assign("b_id", $b_id);
$smarty->assign("mode", $mode);

if($mode == "read" || $mode == "modify")
{
    $board_manager_sql      = "SELECT board_name FROM board_manager WHERE b_id='{$b_id}' AND display='1' LIMIT 1";
    $board_manager_query    = mysqli_query($my_db, $board_manager_sql);
    $board_manager_result   = mysqli_fetch_assoc($board_manager_query);
    $board_title            = $board_manager_result['board_name'];
    $smarty->assign("board_title", $board_title);

    $board_notice_sql = "
        SELECT
            brd_n.no,
            brd_n.b_id,
            brd_n.title,
            brd_n.contents,
            brd_n.s_no,
            (SELECT s_name FROM staff s where s.s_no=brd_n.s_no) AS s_name,
            brd_n.team_code,
            (SELECT t.depth FROM team t WHERE team_code=brd_n.team_code LIMIT 1) as t_depth,
            brd_n.hit,
            brd_n.regdate,
            brd_n.main_view,
            brd_n.main_slider,
            brd_n.popup_view,
            brd_n.file_origin,
            brd_n.file_read,
            (SELECT count(br.r_no) FROM board_read br WHERE br.read_s_no='{$session_s_no}' AND br.b_no = brd_n.`no` AND br.b_id='{$b_id}') as read_cnt
        FROM board_notice brd_n
        WHERE brd_n.no='{$no}'
    ";
    $board_notice_result = mysqli_query($my_db, $board_notice_sql);
    $board_notice = mysqli_fetch_array($board_notice_result);

    if(!empty($board_notice['regdate'])) {
        $regdate_day_value  = date("Y/m/d",strtotime($board_notice['regdate']));
        $regdate_time_value = date("H:i",strtotime($board_notice['regdate']));
    }else{
        $regdate_day_value  = "";
        $regdate_time_value = "";
    }

    $file_name   = isset($board_notice['file_origin']) && !empty($board_notice['file_origin']) ? explode(',', $board_notice['file_origin']) : [];
    $file_read   = isset($board_notice['file_read']) && !empty($board_notice['file_read']) ? explode(',', $board_notice['file_read']) : [];
    $file_count  = isset($board_notice['file_origin']) && !empty($file_name) ? count($file_name) : 0;

    $t_label = getTeamFullName($my_db, $board_notice['t_depth'], $board_notice['team_code']);

    $smarty->assign(
        array(
            "no"            => $board_notice['no'],
            "b_id"          => $board_notice['b_id'],
            "title"         => $board_notice['title'],
            "contents"      => $board_notice['contents'],
            "s_no"          => $board_notice['s_no'],
            "s_name"        => $board_notice['s_name']." ({$t_label})",
            "team"          => $board_notice['team_code'],
            "hit"           => number_format($board_notice['hit']),
            "regdate_day"   => $regdate_day_value,
            "regdate_time"  => $regdate_time_value,
            "main_view"     => $board_notice['main_view'],
            "main_slider"   => $board_notice['main_slider'],
            "popup_view"    => $board_notice['popup_view'],
            "file_origin_name" => $board_notice['file_origin'],
            "file_origin_read" => $board_notice['file_read'],
            "file_name"     => $file_name,
            "file_read"     => $file_read,
            "file_count"    => $file_count
        )
    );

    # hit count add
    if (!isset($_COOKIE["hit_{$b_id}_{$no}"])) {
        $board_notice_sql = "UPDATE board_notice brd_n SET brd_n.hit=brd_n.hit+1 WHERE brd_n.no='{$no}'";
        mysqli_query($my_db, $board_notice_sql);
        setcookie("hit_{$b_id}_{$no}", "HIT_OK", time() + 60 * 60); // 1시간을 쿠키값으로 설정함
    }

    if($mode == "read" && $no)
    {
        #읽기
        if($board_notice['read_cnt'] > 0) {
            $read_upd_sql = "UPDATE board_read SET read_upd_date=now() WHERE  b_id='{$b_id}' AND b_no='{$no}' AND read_s_no='{$session_s_no}'";
        }else{
            $read_upd_sql = "INSERT INTO board_read SET b_id='{$b_id}', b_no='{$no}', read_s_no='{$session_s_no}', read_date=now(), read_upd_date=now()";
        }
        mysqli_query($my_db, $read_upd_sql);

        if(!empty($search_url))
        {
            $sch_b_no       = $no;
            $sch_b_id       = isset($_GET['b_id']) ? $_GET['b_id'] : "";
            $sch_title_get  = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
            $sch_team_get   = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
            $sch_s_name_get = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
            $sch_s_date_get = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : "";
            $sch_e_date_get = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : "";
            $ord_type_by    = (isset($_GET['ord_type_by']) && !empty($_GET['ord_type_by'])) ? $_GET['ord_type_by'] : "";

            $sch_add_where = "display='1' AND b_id='{$sch_b_id}'";
            if (!empty($sch_title_get)) {
                $sch_add_where .= " AND title like '%{$sch_title_get}%'";
            }

            if (!empty($sch_team_get)) {
                $sch_add_where .= " AND team_code ='{$sch_team_get}'";
            }

            if (!empty($sch_s_name_get)) {
                $sch_add_where .= " AND s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_s_name_get}%')";
            }

            if (!empty($sch_s_date_get)) {
                $sch_s_date = $sch_s_date_get." 00:00:00";
                $sch_add_where .= " AND regdate >= '{$sch_s_date}'";
            }

            if (!empty($sch_e_date_get)) {
                $sch_e_date = $sch_e_date_get." 23:59:59";
                $sch_add_where .= " AND regdate <= '{$sch_e_date}'";
            }

            if($ord_type_by == '1'){
                $sch_prev_orderby = "team_name ASC";
                $sch_next_orderby = "team_name DESC";
            }elseif($ord_type_by == '2'){
                $sch_prev_orderby = "team_name DESC";
                $sch_next_orderby = "team_name ASC";
            }else{
                $sch_prev_orderby = "no DESC";
                $sch_next_orderby = "no ASC";
            }

            $sch_board_next_list = [];
            $sch_board_prev_list = [];

            $sch_board_next_sql = "SELECT `no`, title, (SELECT t.team_name FROM team t WHERE t.team_code=brd_nm.team_code) AS team_name, hit, (SELECT s_name FROM staff s where s.s_no=brd_nm.s_no) AS s_name, regdate FROM board_notice brd_nm WHERE {$sch_add_where} AND `no` > '{$sch_b_no}' ORDER BY {$sch_next_orderby} LIMIT 2";
            $sch_board_next_query = mysqli_query($my_db, $sch_board_next_sql);
            while($sch_board_next_result = mysqli_fetch_assoc($sch_board_next_query))
            {
                $sch_board_next_list[] = $sch_board_next_result;
            }

            $sch_board_prev_sql = "SELECT `no`, title, (SELECT t.team_name FROM team t WHERE t.team_code=brd_nm.team_code) AS team_name, hit, (SELECT s_name FROM staff s where s.s_no=brd_nm.s_no) AS s_name, regdate FROM board_notice brd_nm WHERE {$sch_add_where} AND `no` < '{$sch_b_no}' ORDER BY {$sch_prev_orderby} LIMIT 2";
            $sch_board_prev_query = mysqli_query($my_db, $sch_board_prev_sql);
            while($sch_board_prev_result = mysqli_fetch_assoc($sch_board_prev_query))
            {
                $sch_board_prev_list[$sch_board_prev_result['no']] = $sch_board_prev_result;
            }

            if($sch_board_prev_list){
                ksort($sch_board_prev_list);
            }
            $sch_next_cnt = !empty($sch_board_next_list) ? count($sch_board_next_list) : 0;
            $sch_prev_cnt = !empty($sch_board_prev_list) ? count($sch_board_prev_list) : 0;
            $sch_max_cnt  = $sch_next_cnt+$sch_prev_cnt;

            $smarty->assign('sch_max_cnt', $sch_max_cnt);
            $smarty->assign('board_next_list', $sch_board_next_list);
            $smarty->assign('board_prev_list', $sch_board_prev_list);
        }
    }

}else{ // 작성하기 모드
    $smarty->assign("s_no", $session_s_no);
    $smarty->assign("team", $session_team);
    $smarty->assign("s_name", $session_s_team_name);
}

$smarty->display('board/board_notice_detail.html');

?>
