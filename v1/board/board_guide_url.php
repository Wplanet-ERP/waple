<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_common.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

$b_no       = isset($_GET['b_no']) ? $_GET['b_no'] : "";
$mode       = isset($_GET['mode']) ? $_GET['mode'] : "";
$process    = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'save_url')
{
    $u_no   = isset($_POST['u_no']) ? $_POST['u_no'] : "";
    $value  = isset($_POST['value']) ? addslashes($_POST['value']) : "";

    if(!!$u_no) {
        $upd_sql = "UPDATE board_guide_url SET url='{$value}' WHERE u_no = '{$u_no}'";
    }else{
        echo "URL 저장에 실패 하였습니다.";
        exit;
    }

    if (!mysqli_query($my_db, $upd_sql))
        echo "URL 저장에 실패 하였습니다.";
    else
        echo "URL 저장 되었습니다.";
    exit;
}
elseif($process == 'save_param')
{
    $u_no   = isset($_POST['u_no']) ? $_POST['u_no'] : "";
    $value  = isset($_POST['value']) ? addslashes($_POST['value']) : "";

    if(!!$u_no) {
        $upd_sql = "UPDATE board_guide_url SET parameter='{$value}' WHERE u_no = '{$u_no}'";
    }else{
        echo "Parameter 저장에 실패 하였습니다.";
        exit;
    }

    if (!mysqli_query($my_db, $upd_sql))
        echo "Parameter 저장에 실패 하였습니다.";
    else
        echo "Parameter 저장 되었습니다.";
    exit;
}
elseif($process == 'save_memo')
{
    $u_no   = isset($_POST['u_no']) ? $_POST['u_no'] : "";
    $value  = isset($_POST['value']) ? addslashes($_POST['value']) : "";

    if(!!$u_no) {
        $upd_sql = "UPDATE board_guide_url SET memo='{$value}' WHERE u_no = '{$u_no}'";
    }else{
        echo "Memo 저장에 실패 하였습니다.";
        exit;
    }

    if (!mysqli_query($my_db, $upd_sql))
        echo "Memo 저장에 실패 하였습니다.";
    else
        echo "Memo 저장 되었습니다.";
    exit;
}
elseif($process == 'save_popup')
{
    $u_no   = isset($_POST['u_no']) ? $_POST['u_no'] : "";
    $value  = isset($_POST['value']) ? addslashes($_POST['value']) : 0;

    if(!!$u_no) {
        $upd_sql = "UPDATE board_guide_url SET new_popup='{$value}' WHERE u_no = '{$u_no}'";
    }else{
        echo "New Popup 저장에 실패 하였습니다.";
        exit;
    }

    if (!mysqli_query($my_db, $upd_sql))
        echo "New Popup 저장에 실패 하였습니다.";
    else
        echo "New Popup 저장 되었습니다.";
    exit;
}
elseif($process == 'del_guide_url') # 가이드 URL 삭제
{
    // 필수정보
    $b_no      = isset($_POST['b_no']) ? $_POST['b_no'] : "";
    $mode      = isset($_POST['mode']) ? $_POST['mode'] : "";
    $u_no      = isset($_POST['u_no']) ? $_POST['u_no'] : "";


    # 필수정보 체크 및 저장
    if(empty($b_no) || empty($mode) || empty($u_no))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 시도해 주세요.');location.href='board_guide_url.php?b_no={$b_no}&mode={$mode}';</script>");
    }

    $del_sql = "DELETE FROM board_guide_url WHERE u_no = '{$u_no}'";

    if(!mysqli_query($my_db, $del_sql)) {
        exit("<script>alert('URL 삭제에 실패했습니다');location.href='board_guide_url.php?b_no={$b_no}&mode={$mode}';</script>");
    }else{
        exit("<script>location.href='board_guide_url.php?b_no={$b_no}';</script>");
    }
}
elseif($process == 'new_guide_url') # 가이드 URL 생성
{
    // 필수정보
    $b_no           = isset($_POST['b_no']) ? $_POST['b_no'] : "";
    $mode           = isset($_POST['mode']) ? $_POST['mode'] : "";
    $new_url        = isset($_POST['new_url']) ? addslashes($_POST['new_url']) : "";
    $new_parameter  = isset($_POST['new_parameter']) ? addslashes($_POST['new_parameter']) : "";
    $new_memo       = isset($_POST['new_memo']) ? addslashes($_POST['new_memo']) : "";

    # 필수정보 체크 및 저장
    if(empty($b_no) || empty($mode) || empty($new_url))
    {
        exit("<script>alert('필수정보가 없습니다. 다시 등록해 주세요.');location.href='board_guide_url.php?b_no={$b_no}&mode={$mode}';</script>");
    }

    $ins_sql = "
        INSERT INTO `board_guide_url` SET
            `b_no`      = '{$b_no}',
            `url`       = '{$new_url}',
            `parameter` = '{$new_parameter}',
            `memo`      = '{$new_memo}'
    ";

    if(!mysqli_query($my_db, $ins_sql)) {
        exit("<script>alert('URL 등록에 실패했습니다');location.href='board_guide_url.php?b_no={$b_no}&mode={$mode};</script>");
    }else{
        exit("<script>location.href='board_guide_url.php?b_no={$b_no}&mode={$mode}';</script>");
    }
}
elseif($process == 'save_read')
{
    $b_no = isset($_POST['b_no']) ? $_POST['b_no'] : "";
    $s_no = isset($_POST['s_no']) ? $_POST['s_no'] : "";

    $board_chk_sql = "
        SELECT 
            *, 
            (SELECT count(bgr.r_no) FROM board_guide_read bgr WHERE bgr.read_s_no='{$s_no}' AND bgr.b_no = bg.b_no) as read_cnt
        FROM board_guide `bg` 
      WHERE `bg`.b_no = {$b_no} LIMIT 1
    ";

    $board_chk_query = mysqli_query($my_db, $board_chk_sql);
    $board_chk       = mysqli_fetch_assoc($board_chk_query);

    if(isset($board_chk['b_no']))
    {
        $hit = (int)$board_chk['hit']+1;
        $hit_upd_sql = "UPDATE board_guide SET hit={$hit} WHERE b_no='{$b_no}'";
        mysqli_query($my_db, $hit_upd_sql);

        if($board_chk['read_cnt'] == '0'){
            $read_upd_sql = "INSERT INTO board_guide_read SET b_no='{$b_no}', read_s_no='{$s_no}', read_date=now()";
            mysqli_query($my_db, $read_upd_sql);
        }
    }
}

$guide_url_list  = [];
if($b_no){
    $guide_url_sql 	 = "SELECT * FROM `board_guide_url` `bgu` WHERE `bgu`.b_no = {$b_no}";
    $guide_url_query = mysqli_query($my_db, $guide_url_sql);

    while($guide_url = mysqli_fetch_assoc($guide_url_query)){
        $guide_url_list[] = $guide_url;
    }
}


$smarty->assign('b_no', $b_no);
$smarty->assign('mode', $mode);
$smarty->assign('guide_url_list', $guide_url_list);
$smarty->display('board/board_guide_url.html');

?>

