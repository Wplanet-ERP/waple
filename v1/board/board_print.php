<?php
require('../inc/common.php');
require('../ckadmin.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

$board_no   = isset($_GET['b_no']) ? $_GET['b_no'] : "";
$board_name = isset($_GET['b_name']) ? $_GET['b_name'] : "";

$board_sql = "
    SELECT
        b.contents
    FROM {$board_name} b
    WHERE b.no = '{$board_no}'
";

$board_result = mysqli_query($my_db, $board_sql);
$board        = mysqli_fetch_array($board_result);

$smarty->assign($board);
$smarty->display('board/board_print.html');


?>