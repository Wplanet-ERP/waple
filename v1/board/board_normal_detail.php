<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/_upload.php');
require('../inc/helper/board.php');
require('../inc/model/Board.php');
require('../inc/model/Team.php');

# 기본 Model 설정
$board_model = Board::Factory();
$board_model->setBoardNormal();

# Query String no 빼고 처리
$search_url = getenv("QUERY_STRING");
if(substr($search_url, 0, 2) == 'no'){ // 첫번째 값(no) 제거하기
    if(strpos($search_url,"&"))
        $search_url = substr($search_url, strpos($search_url,"&")+1);
    else
        $search_url = "";
}
$smarty->assign("search_url",$search_url);

if(substr($search_url, 0, 13) == 'comm_ord_type'){ // 첫번째 값(no) 제거하기
    if(strpos($search_url,"&"))
        $search_url = substr($search_url, strpos($search_url,"&")+1);
    else
        $search_url = "";
}
$smarty->assign("list_search_url",$search_url);

# Process 처리
$proc   = isset($_POST['process']) ? $_POST['process'] : "";
$my_db->set_charset("utf8mb4");

if ($proc == "down_file")
{
    $b_no   = isset($_POST['no']) ? $_POST['no'] : "";
    $folder = isset($_POST['folder']) ? $_POST['folder'] : "";

    if ($folder == "board_normal"){
        $down_sql   = "SELECT file_origin, file_read FROM board_normal brd_nm WHERE no = '{$b_no}'";
        $down_query = mysqli_query($my_db, $down_sql);
        $file_info  = mysqli_fetch_array($down_query);

        $filename   = htmlspecialchars($file_info['file_origin']);
        $fileread   = $file_info['file_read'];
    }

    echo "File UP Name : {$fileread}<br>";
    echo "File Downloading...<br>File DN Name : {$filename}<br>";
    exit("<script>location.href='../popup/file_download.php?file_dn_name=".urlencode($filename)."&file_up_name={$fileread}';target='_blank';</script>");

}
elseif ($proc == "del_file")
{
    $b_no       = isset($_POST['no']) ? $_POST['no'] : "";
    $folder     = isset($_POST['folder']) ? $_POST['folder'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $fileread   = "";

    if ($folder == "board_normal") {
        $file_sql   = "SELECT file_origin, file_read FROM board_normal brd_nm WHERE no = '{$b_no}'";
        $file_query = mysqli_query($my_db, $file_sql);
        $file_info  = mysqli_fetch_array($file_query);

        $fileread   = $file_info['file_read'];
        $del_sql    = "UPDATE board_normal brd_nm SET file_origin = '', file_read = '' WHERE no = '{$b_no}'";
    }

    if(!empty($del_sql))
    {
        del_file($fileread);
        mysqli_query($my_db, $del_sql);
    }

    exit("<script>alert('해당 파일을 삭제 하였습니다.');location.href='board_normal_detail.php?{$search_url}';</script>");
}
elseif($proc=="del_board")
{
    $b_no  			 = $_POST['no'];
    $b_id 			 = $_POST['b_id'];
    $search_url_post = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(!$b_no || !$b_id){
        exit("<script>alert('삭제에 실패 하였습니다');location.href='board_normal_detail.php?no={$b_no}&{$search_url_post}';</script>");
    }

    $del_sql = "DELETE FROM board_normal WHERE no = {$b_no} AND b_id='{$b_id}'";

    if(!mysqli_query($my_db, $del_sql)){
        exit("<script>alert('해당 게시글을 삭제에 실패 하였습니다');location.href='board_normal_detail.php?no={$b_no}&{$search_url_post}';</script>");
    }else{
        exit("<script>alert('해당 게시글을 삭제 하였습니다');location.href='board_normal_list.php?{$search_url_post}';</script>");
    }
}
elseif($proc == "write")
{
    $add_set = "";

    if(!empty($_POST['b_id'])){
        $add_set    .= "b_id = '{$_POST['b_id']}', ";
        $b_id       = $_POST['b_id'];
    }

    if(!empty($_POST['search_url'])){
        $search_url = $_POST['search_url'];
    }

    if(!empty($_POST['f_category'])){
        $add_set    .= "category = '{$_POST['f_category']}', ";
    }

    if(!empty($_POST['f_brand'])){
        $add_set    .= "brand = '{$_POST['f_brand']}', ";
    }

    if(!empty($_POST['f_title'])){
        $add_set    .= "title = '".addslashes(trim($_POST['f_title']))."', ";
    }

    if(!empty($_POST['f_contents'])){
        $add_set    .= "contents = '".addslashes(trim($_POST['f_contents']))."', ";
    }

    // Dropbox 이미지 확인 및 저장
    $file_path  = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_name  = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk   = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    if($file_chk)
    {
        if(!empty($file_path) && !empty($file_name))
        {
            $file_read   = move_store_files($file_path, "dropzone_tmp", "board_normal");
            $file_name_list = [];
            foreach($file_name as $file_name_val){
                $file_name_list[] = str_replace(',', '_', $file_name_val);
            }
            $file_origin = implode(',', $file_name_list);
            $file_read   = implode(',', $file_read);

            $add_set .= "file_origin='{$file_origin}', file_read='{$file_read}', ";
        }
    }

    if(!empty($_POST['f_s_no'])){
        $add_set .= "team_code = '{$_POST['f_team']}', ";
        $add_set .= "s_no = '{$_POST['f_s_no']}', ";
    }

    $add_set    .= "top_view = '{$_POST['f_top_view']}', ";

    $ins_sql    = "INSERT INTO board_normal SET {$add_set} regdate = now()";

    if (mysqli_query($my_db, $ins_sql))
    {
        /* Slack API START */
        $last_sql_query  = mysqli_query($my_db, "SELECT LAST_INSERT_ID();");
        $last_sql_result = mysqli_fetch_array($last_sql_query);
        $last_board_no   = $last_sql_result[0];

        if($b_id == 'home_w_r')
        {
            $regdate         = date('Y-m-d H:i:s');
            $staff_state_ins = "INSERT INTO `working_state`(`s_no`, `team`, `state`, `regdate`) VALUES('{$session_s_no}', '{$session_team}', '2', '{$regdate}')";
            mysqli_query($my_db, $staff_state_ins);
        }

        if($last_board_no && $b_id == 'wp_inquiry')
        {
            $chk_ins_msg = "와이즈 1:1문의 게시물이 새로 등록 되었습니다.\r\nhttps://work.wplanet.co.kr/v1/board/board_normal_list.php?b_id=wp_inquiry";
            $chk_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='28', content='{$chk_ins_msg}', alert_type='21', regdate=now()";
            mysqli_query($my_db, $chk_ins_sql);
        }

        exit("<script>location.href='board_normal_list.php?b_id={$b_id}';</script>");
    } else {
        exit("<script>alert('등록에 실패 하였습니다');location.href='board_normal_list.php?{$search_url}';</script>");
    }
}
elseif ($proc == "modify")
{
    $add_set = "";

    if(!empty($_POST['b_id'])){
        $add_set    .= "b_id = '{$_POST['b_id']}', ";
    }

    if(!empty($_POST['search_url'])){
        $search_url = $_POST['search_url'];
    }

    if(!empty($_POST['f_category'])){
        $add_set    .= "category = '{$_POST['f_category']}', ";
    }

    if(!empty($_POST['f_brand'])){
        $add_set    .= "brand = '{$_POST['f_brand']}', ";
    }

    if(!empty($_POST['f_title'])){
        $add_set    .= "title = '".addslashes(trim($_POST['f_title']))."', ";
    }

    if(!empty($_POST['f_contents'])){
        $add_set    .= "contents = '".addslashes(trim($_POST['f_contents']))."', ";
    }

    if(!empty($_POST['f_s_no'])){
        $add_set .= "s_no = '{$_POST['f_s_no']}', ";
        $add_set .= "team_code = '{$_POST['f_team']}', ";
    }

    $add_set    .= "top_view = '".$_POST['f_top_view']."', ";

    // Dropbox 이미지 확인 및 저장
    $file_origin_read = isset($_POST['file_origin_read']) ? $_POST['file_origin_read'] : "";
    $file_origin_name = isset($_POST['file_origin_name']) ? $_POST['file_origin_name'] : "";
    $file_path  = isset($_POST['file_path']) ? $_POST['file_path'] : "";
    $file_name  = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_chk   = isset($_POST['file_chk']) ? $_POST['file_chk'] : "";

    if($file_chk)
    {
        if(!empty($file_path) && !empty($file_name))
        {
            $file_read   = move_store_files($file_path, "dropzone_tmp", "board_normal");
            $file_name_list = [];
            foreach($file_name as $file_name_val){
                $file_name_list[] = str_replace(',', '_', $file_name_val);
            }
            $file_origin = implode(',', $file_name_list);
            $file_read   = implode(',', $file_read);

            $add_set .= "file_origin='{$file_origin}', file_read='{$file_read}', ";
        }else{
            $add_set .= "file_origin='', file_read='', ";
        }

        if(!empty($file_origin_read) && !empty($file_origin_name))
        {
            if(!empty($file_path) && !empty($file_name))
            {
                $del_images = array_diff(explode(',', $file_origin_read), $file_path);
            }else{
                $del_images = explode(',', $file_origin_read);
            }

            del_files($del_images);
        }
    }

    $add_set = rtrim($add_set); // 뒤쪽 여백 제거
    if(substr($add_set, -1) == ","){ // 마지막 글자가 , 인지 확인
        $add_set = substr($add_set , 0, -1); // 마지막 한글자 제거
    }

    $sql="UPDATE board_normal SET {$add_set} WHERE `no` = '{$_POST['no']}'";

    if (mysqli_query($my_db, $sql)){
        exit("<script>alert('수정이 완료 되었습니다');location.href='board_normal_list.php?{$search_url}';</script>");
    } else {
        exit("<script>alert('수정에 실패 하였습니다');location.href='board_normal_list.php?{$search_url}';</script>");
    }
}
elseif ($proc == "comment_add")
{
    $table_no 	 = (isset($_POST['table_no'])) ? $_POST['table_no'] : "";
    $b_id 		 = (isset($_POST['b_id'])) ? $_POST['b_id'] : "";
    $comment_new = (isset($_POST['f_comment_new'])) ? $_POST['f_comment_new'] : "";
    $is_secret 	 = (isset($_POST['is_secret'])) ? 1 : 0;
    $is_unknown  = (isset($_POST['is_unknown'])) ? 1 : 0;
    $regdate     = date("Y-m-d H:i:s");
    $search_url  = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    # 코멘트 이미지 업로드
    $img_add_set = "";
    $f_comm_img     = $_FILES['f_comm_img'];
    if(isset($f_comm_img['name']) && !empty($f_comm_img['name'])){
        $img_path    = add_store_file($f_comm_img, "board_comment");
        $img_name    = $f_comm_img['name'];
        $img_add_set = ", `img_path`='{$img_path}', `img_name`='{$img_name}'";
    }

    $cmt_sql = "INSERT INTO `comment` SET
                `table_name` = 'board_normal',
                b_id = '{$b_id}',
                table_no = '{$table_no}',
                `comment` = '".addslashes($comment_new)."',
                team = (SELECT s.team	FROM staff s WHERE s.s_no = '{$session_s_no}'),
                s_no = '{$session_s_no}',
                regdate = '{$regdate}',
                is_secret = {$is_secret},
                is_unknown = {$is_unknown}
                {$img_add_set}
    ";

    if(!mysqli_query($my_db, $cmt_sql)) {
        echo("<script>alert('코멘트 추가에 실패 하였습니다');history.back();</script>");
    }else{
        if($b_id == 'wp_inquiry')
        {
            $board_chk_sql      = "SELECT * FROM board_normal WHERE `no`='{$table_no}'";
            $board_chk_query    = mysqli_query($my_db, $board_chk_sql);
            $board_chk_result   = mysqli_fetch_assoc($board_chk_query);
            if(isset($board_chk_result['no']) && !empty($board_chk_result['no']))
            {
                $chk_ins_msg = "와이즈 1:1문의 게시물 코멘트가 작성되었습니다.\r\nhttps://work.wplanet.co.kr/v1/board/board_normal_detail.php?no={$table_no}&b_id=wp_inquiry";
                $chk_ins_sql = "INSERT INTO waple_chat_content SET wc_no='2', s_no='{$board_chk_result['s_no']}', content='{$chk_ins_msg}', alert_type='22', regdate=now()";
                mysqli_query($my_db, $chk_ins_sql);
            }
        }
    }

    exit ("<script>location.href='board_normal_detail.php?no={$table_no}&{$search_url}';</script>");
}
elseif ($proc == "comment_delete")
{
    $cmt_no  = (isset($_POST['cmt_no'])) ? $_POST['cmt_no'] : "";
    $cmt_sql = "UPDATE `comment` cmt SET cmt.display = '2' WHERE cmt.no = '{$cmt_no}'";

    if(mysqli_query($my_db, $cmt_sql))
    {
        $comment_like_del_sql = "UPDATE `comment_like` SET `like`='2' WHERE com_no='{$cmt_no}'";
        mysqli_query($my_db, $comment_like_del_sql);

        $comment_sub_del_sql = "UPDATE `comment` SET `display`='2' WHERE group_no='{$cmt_no}'";
        mysqli_query($my_db, $comment_sub_del_sql);

        echo("<script>alert('삭제 하였습니다');history.back();</script>");
    } else {
        echo("<script>alert('삭제에 실패 하였습니다');history.back();</script>");
    }
    exit;
}
elseif ($proc == "comment_main_mod")
{
    $b_id 		  = (isset($_POST['b_id'])) ? $_POST['b_id'] : "";
    $table_no 	  = (isset($_POST['table_no'])) ? $_POST['table_no'] : "";
    $main_comm_no = (isset($_POST['main_comm_no'])) ? $_POST['main_comm_no'] : "";
    $comment_val  = (isset($_POST['f_comment'])) ? addslashes($_POST['f_comment']) : "";
    $search_url   = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(empty($main_comm_no)){
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }

    $upd_sql = "UPDATE `comment` SET comment='{$comment_val}' WHERE `no`='{$main_comm_no}'";

    if(!mysqli_query($my_db, $upd_sql)) {
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }else{
        exit ("<script>location.href='board_normal_detail.php?no={$table_no}&{$search_url}#sub-comment-wrapper-{$main_comm_no}';</script>");
    }

}
elseif ($proc == "comment_sub_add")
{
    $table_no 	 = (isset($_POST['table_no'])) ? $_POST['table_no'] : "";
    $group_no 	 = (isset($_POST['group_no'])) ? $_POST['group_no'] : "";
    $parent_no 	 = (isset($_POST['parent_no'])) ? $_POST['parent_no'] : "";
    $depth 	     = (isset($_POST['depth'])) ? $_POST['depth']+1 : "1";
    $max_depth 	 = (isset($_POST['max_depth'])) ? $_POST['max_depth'] : "";
    $seq_val     = (isset($_POST['seq'])) ? $_POST['seq'] : "";
    $b_id 		 = (isset($_POST['b_id'])) ? $_POST['b_id'] : "";
    $comment_new = (isset($_POST['f_comment_new'])) ? $_POST['f_comment_new'] : "";
    $is_secret 	 = (isset($_POST['is_secret'])) ? 1 : 0;
    $is_unknown  = (isset($_POST['is_unknown'])) ? 1 : 0;
    $regdate     = date("Y-m-d H:i:s");
    $search_url  = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(empty($seq_val)){
        $seq_max_sql    = "SELECT MAX(seq) as max_seq FROM `comment` WHERE display = '1' AND group_no='{$group_no}'";
        $seq_max_query  = mysqli_query($my_db, $seq_max_sql);
        $seq_max_result = mysqli_fetch_array($seq_max_query);
        $seq            = isset($seq_max_result['max_seq']) && !empty($seq_max_result['max_seq']) ? $seq_max_result['max_seq']+1 : 1;
    }else{

        if($max_depth == $depth){
            $max_chk_no = $parent_no;
        }else{
            $max_chk_no = $parent_no;
            for($i=$depth; $i<$max_depth; $i++)
            {
                if(empty($max_chk_no)){
                    break;
                }
                $max_chk_sql    = "SELECT `no`, seq FROM `comment` WHERE display = '1' AND group_no='{$group_no}' AND parent_no='{$max_chk_no}' ORDER BY seq DESC LIMIT 1";
                $max_chk_query  = mysqli_query($my_db, $max_chk_sql);
                $max_chk_result = mysqli_fetch_assoc($max_chk_query);
                $max_chk_no     = isset($max_chk_result['no']) && !empty($max_chk_result['no']) ? $max_chk_result['no'] : $parent_no;
                $seq_val        = isset($max_chk_result['seq']) && !empty($max_chk_result['seq']) ? $max_chk_result['seq'] : $seq_val;
            }
        }

        $seq_max_sql    = "SELECT MAX(seq) as max_seq FROM `comment` WHERE display = '1' AND group_no='{$group_no}' AND parent_no='{$max_chk_no}'";
        $seq_max_query  = mysqli_query($my_db, $seq_max_sql);
        $seq_max_result = mysqli_fetch_array($seq_max_query);
        $seq_max_value  = isset($seq_max_result['max_seq']) && !empty($seq_max_result['max_seq']) ? $seq_max_result['max_seq'] : $seq_val;
        $seq            = $seq_max_value+1;

        $seq_chk_sql    = "SELECT `no` FROM `comment` WHERE display = '1' AND group_no='{$group_no}' AND seq > '{$seq_max_value}' ORDER BY seq ASC";
        $seq_chk_query  = mysqli_query($my_db, $seq_chk_sql);
        $seq_chk_up     = $seq;
        while($seq_chk_result = mysqli_fetch_array($seq_chk_query))
        {
            if($seq_chk_result['no']){
                $seq_chk_up++;
                $seq_upd_sql = "UPDATE `comment` SET seq='{$seq_chk_up}' WHERE `no`='{$seq_chk_result['no']}'";
                mysqli_query($my_db, $seq_upd_sql);
            }
        }
    }

    $cmt_sql = "INSERT INTO `comment` SET
                `table_name` = 'board_normal',
                b_id       = '{$b_id}',
                table_no   = '{$table_no}',
                group_no   = '{$group_no}',
                parent_no  = '{$parent_no}',
                `depth`    = '{$depth}',
                `seq`      = '{$seq}',
                `comment`  = '".addslashes($comment_new)."',
                team       = (SELECT s.team	FROM staff s WHERE s.s_no = '{$session_s_no}'),
                s_no       = '{$session_s_no}',
                regdate    = '{$regdate}',
                is_secret  = {$is_secret},
                is_unknown = {$is_unknown}
    ";

    if(!mysqli_query($my_db, $cmt_sql)) {
        echo("<script>alert('코멘트 추가에 실패 하였습니다');history.back();</script>");
    }else{
        $sub_comm_no = mysqli_insert_id($my_db);
        exit ("<script>location.href='board_normal_detail.php?no={$table_no}&{$search_url}#sub-comment-wrapper-{$sub_comm_no}';</script>");
    }
}
elseif ($proc == "comment_sub_mod")
{
    $b_id 		 = (isset($_POST['b_id'])) ? $_POST['b_id'] : "";
    $table_no 	 = (isset($_POST['table_no'])) ? $_POST['table_no'] : "";
    $sub_comm_no = (isset($_POST['sub_comm_no'])) ? $_POST['sub_comm_no'] : "";
    $comment_val = (isset($_POST['f_comment'])) ? addslashes($_POST['f_comment']) : "";
    $search_url  = (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(empty($sub_comm_no)){
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }

    $upd_sql = "UPDATE `comment` SET comment='{$comment_val}' WHERE `no`='{$sub_comm_no}'";

    if(!mysqli_query($my_db, $upd_sql)) {
        echo("<script>alert('코멘트 수정에 실패 하였습니다');history.back();</script>");
    }else{
        exit ("<script>location.href='board_normal_detail.php?no={$table_no}&{$search_url}#sub-comment-wrapper-{$sub_comm_no}';</script>");
    }
}
elseif ($proc == "comment_sub_delete")
{
    $table_no    = (isset($_POST['table_no'])) ? $_POST['table_no'] : "";
    $group_no    = (isset($_POST['group_no'])) ? $_POST['group_no'] : "";
    $cmt_no      = (isset($_POST['parent_no'])) ? $_POST['parent_no'] : "";
    $depth       = (isset($_POST['depth'])) ? $_POST['depth'] : "";
    $max_depth   = (isset($_POST['max_depth'])) ? $_POST['max_depth'] : "";

    $sql = "UPDATE `comment` cmt SET cmt.display = '2' WHERE cmt.no = '" . $cmt_no . "'";

    if (mysqli_query($my_db, $sql)) {
        if ($max_depth > $depth) {
            $sub_del_no_list[] = $cmt_no;
            for ($i = $depth; $i < $max_depth; $i++) {
                $sub_de_no_chk = implode(",", $sub_del_no_list);
                $sub_de_no_chk_sql = "SELECT no FROM comment WHERE parent_no IN({$sub_de_no_chk}) AND display='1' AND group_no='{$group_no}'";
                $sub_de_no_chk_query = mysqli_query($my_db, $sub_de_no_chk_sql);
                while ($sub_de_no_chk_result = mysqli_fetch_assoc($sub_de_no_chk_query)) {
                    $sub_del_no_list[] = $sub_de_no_chk_result['no'];
                }
            }

            if (!empty($sub_del_no_list)) {
                $sub_del_no_list = array_unique($sub_del_no_list);
                $sub_de_no_chk = implode(",", $sub_del_no_list);
                $comment_sub_del_sql = "UPDATE `comment` SET `display`='2' WHERE `no` IN({$sub_de_no_chk})";
                mysqli_query($my_db, $comment_sub_del_sql);

                $comment_like_del_sql = "UPDATE `comment_like` SET `like`='2' WHERE com_no='{$cmt_no}'";
                mysqli_query($my_db, $comment_like_del_sql);
            }

        } else {
            $comment_like_del_sql = "UPDATE `comment_like` SET `like`='2' WHERE com_no='{$cmt_no}'";
            mysqli_query($my_db, $comment_like_del_sql);
        }

        echo("<script>alert('삭제 하였습니다');history.back();</script>");
    } else {
        echo("<script>alert('삭제에 실패 하였습니다');history.back();</script>");
    }
    exit;

}

$dir_location       = "../";
$editor_folder_name = folderCheck('board_image');
$smarty->assign("dir_location", $dir_location);
$smarty->assign('editor_path', 'board_image/'.$editor_folder_name);

# 기본 게시판 no, b_id 체크 및 저장
$no     = isset($_GET['no']) ? $_GET['no'] : "";
$b_id   = isset($_GET['b_id']) ? $_GET['b_id'] : "";
$mode   = isset($_GET['mode']) ? $_GET['mode'] : "read";

$smarty->assign("no",  $no);
$smarty->assign("b_id", $b_id);
$smarty->assign("mode", $mode);

if(empty($b_id))
{
    exit("<script>alert('올바른 접근방식이 아닙니다.');history.back();</script>");
}

if(($b_id != "cs_with" && $b_id != "mc_event") && $session_my_c_type == '3')
{
    exit("<script>alert('허용된 URL이 아닙니다.');location.href='/v1/board/board_normal_list.php?b_id=cs_with';</script>");
}

if($b_id != "mc_event" && $session_my_c_type == '2')
{
    exit("<script>alert('허용된 URL이 아닙니다.');location.href='/v1/board/board_normal_list.php?b_id=mc_event';</script>");
}

if(($b_id != "cs_with" && $b_id != "mc_event") && $session_my_c_type == '3')
{
    exit("<script>alert('허용된 URL이 아닙니다.');location.href='/v1/board/board_normal_list.php?b_id=cs_with';</script>");
}

$board_manager_sql      = "SELECT `table_name`, board_name, category, contents_format, comment_format FROM board_manager WHERE b_id='{$b_id}' AND display='1' LIMIT 1";
$board_manager_query    = mysqli_query($my_db, $board_manager_sql);
$board_manager_result   = mysqli_fetch_assoc($board_manager_query);
$board_title            = $board_manager_result['board_name'];
$contents_format        = $board_manager_result['contents_format'];

$sch_category_list = [];
if($b_id == 'cs_with'){
    $sch_category_list = array(
        "1" => "대기",
        "2" => "확인중",
        "3" => "확인완료",
    );
}else{
    if(isset($board_manager_result['category']) && !empty($board_manager_result['category']))
    {
        $sch_cate_val = explode("$", $board_manager_result['category']);
        $sch_category_list = $sch_cate_val;
    }
}

$sch_brand_list = [];
if($b_id == 'mc_event'){
    $sch_brand_list = getBrandCategoryOption();
}

$smarty->assign("board_title", $board_title);
$smarty->assign("board_table_name", $board_manager_result['table_name']);
$smarty->assign("board_comment_format", $board_manager_result['comment_format']);
$smarty->assign("sch_category_list", $sch_category_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

if($mode == "read" || $mode == "modify") // 읽기 또는 수정하기 모드
{
    $board_normal_sql = "
        SELECT
            brd_nm.no,
            brd_nm.b_id,
            brd_nm.title,
            brd_nm.category,
            brd_nm.brand,
            brd_nm.contents,
            brd_nm.s_no,
            (SELECT s_name FROM staff s where s.s_no=brd_nm.s_no) AS s_name,
            brd_nm.team_code,
            (SELECT t.depth FROM team t WHERE team_code=brd_nm.team_code LIMIT 1) as t_depth,
            brd_nm.hit,
            brd_nm.regdate,
            brd_nm.top_view,
            brd_nm.file_origin,
            brd_nm.file_read,
            (SELECT count(br.r_no) FROM board_read br WHERE br.read_s_no='{$session_s_no}' AND br.b_no = brd_nm.`no` AND br.b_id='{$b_id}') as read_cnt
        FROM board_normal brd_nm
        WHERE brd_nm.no='".$no."'
    ";

    $board_normal_result    = mysqli_query($my_db, $board_normal_sql);
    $board_normal           = mysqli_fetch_array($board_normal_result);

    if($b_id == "wp_inquiry" && $session_s_no != '28' && ($board_normal['s_no'] != '28' && $board_normal['top_view'] != '1'))
    {
        if($session_s_no != $board_normal['s_no']){
            exit("<script>alert('본인이 작성한 글만 열람할수있습니다.');location.href='board_normal_list.php?$search_url';</script>");
        }
    }

    $board_normal["is_editable"] = false;
    if($board_normal['s_no'] == $session_s_no || ($b_id == 'mc_event' && $session_s_no == '102') || ($b_id == 'new_product' && $session_team == '00249')){
        $board_normal["is_editable"] = true;
    }

    $board_normal["regdate_day"] = !empty($board_normal['regdate']) ? date("Y/m/d", strtotime($board_normal['regdate'])) : "";
    $board_normal["regdate_time"] = !empty($board_normal['regdate']) ? date("H:i", strtotime($board_normal['regdate'])) : "";

    $file_name  = isset($board_normal['file_origin']) && !empty($board_normal['file_origin']) ? explode(',', $board_normal['file_origin']) : "";
    $file_read  = isset($board_normal['file_read']) && !empty($board_normal['file_read']) ? explode(',', $board_normal['file_read']) : "";
    $file_count = isset($board_normal['file_origin']) && !empty($board_normal['file_origin']) ? count($file_name) : "0";
    $t_label    = getTeamFullName($my_db, $board_normal['t_depth'], $board_normal['team_code']);

    $board_normal["brand_name"] = isset($sch_brand_list[$board_normal['brand']]) ? $sch_brand_list[$board_normal['brand']] : "";
    $board_normal["s_name"]     = $board_normal['s_name']." ({$t_label})";
    $board_normal["team"]       = $board_normal['team_code'];
    $board_normal["hit"]        = number_format($board_normal['hit']);

    $board_normal["file_origin_name"]   = $board_normal['file_origin'];
    $board_normal["file_origin_read"]   = $board_normal['file_read'];
    $board_normal["file_read"]          = $file_read;
    $board_normal["file_name"]          = $file_name;
    $board_normal["file_count"]         = $file_count;

    $smarty->assign($board_normal);

    # hit count add
    if (!isset($_COOKIE["hit_{$b_id}_{$no}"]))
    {
        $board_normal_sql = "UPDATE board_normal brd_nm SET brd_nm.hit=brd_nm.hit+1 WHERE brd_nm.no='".$no."'";
        mysqli_query($my_db, $board_normal_sql);
        setcookie("hit_{$b_id}_{$no}", "HIT_OK", time() + 60 * 60);
    }

    if($mode == "read" && $no)
    {
        #읽기
        if($board_normal['read_cnt'] > 0) {
            $read_upd_sql = "UPDATE board_read SET read_upd_date=now() WHERE  b_id='{$b_id}' AND b_no='{$no}' AND read_s_no='{$session_s_no}'";
        }else{
            $read_upd_sql = "INSERT INTO board_read SET b_id='{$b_id}', b_no='{$no}', read_s_no='{$session_s_no}', read_date=now(), read_upd_date=now()";
        }
        mysqli_query($my_db, $read_upd_sql);

        //게시글 리스트
        if(!empty($search_url))
        {
            $sch_b_no           = $board_normal['no'];
            $sch_b_id           = isset($_GET['b_id']) ? $_GET['b_id'] : "";
            $sch_category_get 	= isset($_GET['sch_category']) ? $_GET['sch_category'] : "";
            $sch_brand_get 		= isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
            $sch_title_get      = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
            $sch_team_get       = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
            $sch_s_name_get     = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
            $sch_s_date_get     = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : "";
            $sch_e_date_get     = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : "";
            $ord_type_by        = (isset($_GET['ord_type_by']) && !empty($_GET['ord_type_by'])) ? $_GET['ord_type_by'] : "";

            $sch_add_where = "display='1' AND b_id='{$sch_b_id}'";
            if (!empty($sch_title_get)) {
                $sch_add_where .= " AND title like '%{$sch_title_get}%'";
            }

            if (!empty($sch_category_get)) {
                $sch_add_where .= " AND brd_nm.category ='{$sch_category_get}'";
            }

            if(!empty($sch_brand_get)){
                $sch_add_where .= " AND brd_nm.brand ='{$sch_brand_get}'";
            }

            if (!empty($sch_team_get)) {
                $sch_add_where .= " AND team_code ='{$sch_team_get}'";
            }

            if (!empty($sch_s_name_get)) {
                $sch_add_where .= " AND s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_s_name_get}%')";
            }

            if (!empty($sch_s_date_get)) {
                $sch_s_date = $sch_s_date_get." 00:00:00";
                $sch_add_where .= " AND regdate >= '{$sch_s_date}'";
            }

            if (!empty($sch_e_date_get)) {
                $sch_e_date = $sch_e_date_get." 23:59:59";
                $sch_add_where .= " AND regdate <= '{$sch_e_date}'";
            }

            if($ord_type_by == '1'){
                $sch_prev_orderby = "team_name ASC";
                $sch_next_orderby = "team_name DESC";
            }elseif($ord_type_by == '2'){
                $sch_prev_orderby = "team_name DESC";
                $sch_next_orderby = "team_name ASC";
            }else{
                $sch_prev_orderby = "no DESC";
                $sch_next_orderby = "no ASC";
            }

            $sch_board_next_list = [];
            $sch_board_prev_list = [];

            $sch_board_next_sql = "SELECT `no`, title, (SELECT t.team_name FROM team t WHERE t.team_code=brd_nm.team_code) AS team_name, hit, (SELECT s_name FROM staff s where s.s_no=brd_nm.s_no) AS s_name, regdate FROM board_normal brd_nm WHERE {$sch_add_where} AND `no` > '{$sch_b_no}' ORDER BY {$sch_next_orderby} LIMIT 2";
            $sch_board_next_query = mysqli_query($my_db, $sch_board_next_sql);
            while($sch_board_next_result = mysqli_fetch_assoc($sch_board_next_query))
            {
                $sch_board_next_list[] = $sch_board_next_result;
            }

            $sch_board_prev_sql = "SELECT `no`, title, (SELECT t.team_name FROM team t WHERE t.team_code=brd_nm.team_code) AS team_name, hit, (SELECT s_name FROM staff s where s.s_no=brd_nm.s_no) AS s_name, regdate FROM board_normal brd_nm WHERE {$sch_add_where} AND `no` < '{$sch_b_no}' ORDER BY {$sch_prev_orderby} LIMIT 2";
            $sch_board_prev_query = mysqli_query($my_db, $sch_board_prev_sql);
            while($sch_board_prev_result = mysqli_fetch_assoc($sch_board_prev_query))
            {
                $sch_board_prev_list[$sch_board_prev_result['no']] = $sch_board_prev_result;
            }

            if($sch_board_prev_list){
                ksort($sch_board_prev_list);
            }

            $sch_next_cnt = !empty($sch_board_next_list) ? count($sch_board_next_list) : 0;
            $sch_prev_cnt = !empty($sch_board_prev_list) ? count($sch_board_prev_list) : 0;
            $sch_max_cnt  = $sch_next_cnt+$sch_prev_cnt;

            $smarty->assign('sch_max_cnt', $sch_max_cnt);
            $smarty->assign('board_next_list', $sch_board_next_list);
            $smarty->assign('board_prev_list', $sch_board_prev_list);
        }

        $comm_ord_type = isset($_GET['comm_ord_type']) ? $_GET['comm_ord_type'] : "regdate";
        if($comm_ord_type == 'like_cnt'){
            $add_order = "like_cnt DESC, regdate DESC";
        }else{
            $add_order = "regdate DESC";
        }
        $smarty->assign("comm_ord_type", $comm_ord_type);

        $comment_sql = "
            SELECT
                cmt.no,
                cmt.table_name,
                cmt.b_id,
                cmt.table_no,
                cmt.parent_no,
                cmt.comment,
                cmt.team,
                cmt.s_no,
                (SELECT s_name FROM staff s where s.s_no=cmt.s_no) AS s_name,
                cmt.regdate,
                cmt.is_secret,
                cmt.is_unknown,
                cmt.img_path,
                cmt.img_name,
                (SELECT count(l_no) FROM comment_like cl WHERE cl.com_no = cmt.no AND cl.`like`='1') as like_cnt,
                (SELECT count(l_no) FROM comment_like cl WHERE cl.com_no = cmt.no AND cl.`like`='1' AND cl.s_no='{$session_s_no}') as self_like,
                (SELECT count(sub_cmt.`no`) FROM comment sub_cmt WHERE sub_cmt.group_no = cmt.no AND sub_cmt.`display`='1') as sub_comm_cnt,
                (SELECT MAX(sub_cmt.`depth`) FROM comment sub_cmt WHERE sub_cmt.group_no = cmt.no AND sub_cmt.`display`='1') as sub_max_depth
            FROM `comment` cmt
            WHERE cmt.table_no IN ({$board_normal['no']})
                AND cmt.b_id='{$b_id}'
                AND cmt.table_name='board_normal'
                AND cmt.display = '1'
                AND (cmt.parent_no is null OR cmt.parent_no = '0')
            ORDER BY {$add_order}
        ";

        $comment_query      = mysqli_query($my_db, $comment_sql);
        $total_comment_cnt  = 0;
        $comment_list       = [];
        while($comment_array = mysqli_fetch_array($comment_query))
        {
            $sub_comm_list = [];
            if($comment_array['sub_comm_cnt'] > 0)
            {
                $sub_comm_where = "cmt.table_no IN ({$board_normal['no']}) AND cmt.b_id='{$b_id}' AND cmt.table_name='board_normal' AND cmt.display = '1' AND cmt.group_no = '{$comment_array['no']}'";
                $sub_comm_sql = "
                SELECT
                    *,
                    (SELECT s_name FROM staff s where s.s_no=cmt.s_no) AS s_name,
                    (SELECT count(l_no) FROM comment_like cl WHERE cl.com_no = cmt.no AND cl.`like`='1') as like_cnt,
                    (SELECT count(l_no) FROM comment_like cl WHERE cl.com_no = cmt.no AND cl.`like`='1' AND cl.s_no='{$session_s_no}') as self_like
                FROM comment cmt WHERE {$sub_comm_where} ORDER BY cmt.seq ASC, cmt.regdate DESC";

                $sub_comm_query = mysqli_query($my_db, $sub_comm_sql);
                while($sub_comm_result = mysqli_fetch_assoc($sub_comm_query))
                {
                    $sub_comm_result['comment_mod'] = $sub_comm_result['comment']; // 코멘트 수정용 처리
                    $sub_comm_result['comment']     = str_replace("\r\n","<br />",htmlspecialchars($sub_comm_result['comment'])); // 코멘트 개행문자 처리
                    $sub_comm_result['regdate_day'] = date("Y/m/d",strtotime($sub_comm_result['regdate']));
                    $sub_comm_result['regdate_time']   = date("H:i",strtotime($sub_comm_result['regdate']));
                    $sub_comm_result['depth_width'] = ($sub_comm_result['depth']*30)."px";
                    $sub_comm_list[] = $sub_comm_result;
                }
            }

            $comment_value = str_replace("\r\n","<br />",htmlspecialchars($comment_array['comment'])); // 코멘트 개행문자 처리
            $comment_regdate_day_value = date("Y/m/d",strtotime($comment_array['regdate']));
            $comment_regdate_time_value = date("H:i",strtotime($comment_array['regdate']));

            $comment_list[] = array(
                "no"            => $comment_array['no'],
                "table_name"    => $comment_array['table_name'],
                "b_id"          => $comment_array['b_id'],
                "table_no"      => $comment_array['table_no'],
                "comment"       => $comment_value,
                "comment_mod"   => $comment_array['comment'],
                "team"          => $comment_array['team'],
                "s_no"          => $comment_array['s_no'],
                "s_name"        => $comment_array['s_name'],
                "regdate_day"   => $comment_regdate_day_value,
                "regdate_time"  => $comment_regdate_time_value,
                "is_secret"     => $comment_array['is_secret'],
                "is_unknown"    => $comment_array['is_unknown'],
                "like_cnt"      => $comment_array['like_cnt'],
                "self_like"     => $comment_array['self_like'],
                "img_path"      => $comment_array['img_path'],
                "img_name"      => $comment_array['img_name'],
                "sub_comm_cnt"  => $comment_array['sub_comm_cnt'],
                "sub_max_depth" => $comment_array['sub_max_depth'],
                "sub_comm_list" => $sub_comm_list
            );

            $total_comment_cnt += ($comment_array['sub_comm_cnt']+1);
        }

        $smarty->assign("total_comment_cnt", $total_comment_cnt);
        $smarty->assign("comment_list", $comment_list);
    }

}else{ // 작성하기 모드
    $b_id 	= (isset($_GET['b_id'])) ? $_GET['b_id'] : "";
    if($b_id == 'home_w_r')
    {
        $date_w = array('일', '월', '화', '수', '목', '금', '토');

        $date_md    = date('m/d');
        $date_preg	= preg_replace('/(0)(\d)/','$2', $date_md);;
        $title 		= "(" . $date_preg . ") 재택근무 보고";
        $date_ymd 	= date('Y.m.d') . " " . $date_w[date('w')];
        $contents 	=
            nl2br("1. 일시 : {$date_ymd}

                2. 근무시간 :

                3. 주요 업무 진행 사항
                -
                -
                -
                -

                4. 기타 특이사항
                 없음"
            );

        $smarty->assign('title', $title);
        $smarty->assign('contents', $contents);
    }elseif(!empty($contents_format)){
        $smarty->assign('contents_format', $contents_format);
    }

    if($b_id == 'mc_event' && !empty($sch_brand_list))
    {
        $write_brand_list = getBrandCategoryStaffOption();

        if(isset($write_brand_list[$session_s_no]))
        {
            $write_brand      = $write_brand_list[$session_s_no];
            $write_brand_name = $sch_brand_list[$write_brand];

            $smarty->assign("brand", $write_brand);
            $smarty->assign("brand_name", $write_brand_name);
        }
    }

    $smarty->assign("s_no", $session_s_no);
    $smarty->assign("team", $session_team);
    $smarty->assign("s_name", $session_s_team_name);
}

# Team 리스트
$team_model     = Team::Factory();
$team_all_list  = $team_model->getTeamAllList();
$team_name_list = $team_all_list['team_name_list'];

$smarty->assign("team_list", $team_name_list);

$smarty->display('board/board_normal_detail.html');
?>
