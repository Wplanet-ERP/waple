<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/model/MyQuick.php');
require('../inc/model/Kind.php');
require('../inc/model/Board.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# 프로세스 처리 & Model Init
$process = (isset($_POST['process'])) ? $_POST['process'] : "";
$board_model = Board::Factory();
$board_model->setBoardGuide();

if($process == 'display')
{
    $b_no       = isset($_POST['b_no']) ?$_POST['b_no'] : "";
    $type       = isset($_POST['type']) ?$_POST['type'] : "";
    $search_url = isset($_POST['search_url']) ?$_POST['search_url'] : "";

    if(empty($b_no) || empty($type)){
        exit("<script>alert('다시 시도해 주세요');location.href='board_guide_list.php?{$search_url}';</script>");
    }

    if(!$board_model->update(array("b_no" => $b_no, "display" => $type))) {
        exit("<script>alert('변경에 실패했습니다');location.href='board_guide_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('변경했습니다');location.href='board_guide_list.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no  = "91";
$nav_title   = "와플 GUIDE";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색 초기화 및 조건 생성
$add_where = "1=1";

# 가이드 그룹 검색
$sch_guide_g1   = isset($_GET['sch_guide_g1']) ? $_GET['sch_guide_g1'] : "";
$sch_guide_g2   = isset($_GET['sch_guide_g2']) ? $_GET['sch_guide_g2'] : "";

$smarty->assign("sch_guide_g1", $sch_guide_g1);
$smarty->assign("sch_guide_g2", $sch_guide_g2);

if($sch_guide_g2){
    $add_where .= " AND `bg`.k_name_code ='".$sch_guide_g2."'";
}elseif($sch_guide_g1){
    $add_where .= " AND (SELECT k_parent FROM kind k WHERE k.k_name_code=`bg`.k_name_code) ='".$sch_guide_g1."'";
}

$kind_model         = Kind::Factory();
$guide_group_list   = $kind_model->getKindGroupList("guide");
$guide_g1_list = $guide_g2_list = [];
foreach($guide_group_list as $key => $guide_data){
    if(!$key){
        $guide_g1_list = $guide_data;
    }else{
        $guide_g2_list[$key] = $guide_data;
    }
}
$sch_guide_g2_list = isset($guide_g2_list[$sch_guide_g1]) ? $guide_g2_list[$sch_guide_g1] : [];
$smarty->assign("guide_g1_list", $guide_g1_list);
$smarty->assign("guide_g2_list", $sch_guide_g2_list);

# 가이드 세부 검색
$sch_b_no     = isset($_GET['sch_b_no']) ? $_GET['sch_b_no'] : "";
$sch_question = isset($_GET['sch_question']) ? $_GET['sch_question'] : "";
$sch_manager  = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_display  = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$sch_keyword  = isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";

if(!empty($sch_b_no))
{
    $add_where .= " AND `bg`.b_no = '{$sch_b_no}'";
    $smarty->assign('sch_b_no', $sch_b_no);
}

if(!empty($sch_question))
{
    $add_where .= " AND `bg`.question LIKE '%{$sch_question}%'";
    $smarty->assign('sch_question', $sch_question);
}

if(!empty($sch_manager))
{
    $add_where .= " AND `bg`.manager IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_manager}%')";
    $smarty->assign('sch_manager', $sch_manager);
}

if(!empty($sch_display))
{
    $add_where .= " AND `bg`.display='{$sch_display}'";
    $smarty->assign('sch_display', $sch_display);
}

if(!empty($sch_keyword))
{
    $add_where .= " AND `bg`.keyword like '%{$sch_keyword}%'";
    $smarty->assign('sch_keyword', $sch_keyword);
}

# 전체 게시물 수
$guide_total_sql	= "SELECT count(`bg`.b_no) as cnt FROM board_guide `bg` WHERE {$add_where}";
$guide_total_query	= mysqli_query($my_db, $guide_total_sql);
$guide_total_result = mysqli_fetch_array($guide_total_query);
$guide_total        = $guide_total_result['cnt'];

# 페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($guide_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist	= pagelist($pages, "board_guide_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $guide_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

# 가이드 리스트 쿼리
$guide_sql  = "
    SELECT
        `bg`.b_no,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=`bg`.k_name_code)) AS k_g1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=`bg`.k_name_code) AS k_g2_name,
        `bg`.question,
        `bg`.keyword,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`bg`.manager) AS manager_name,
        DATE_FORMAT(`bg`.regdate, '%Y-%m-%d') AS reg_date,
        DATE_FORMAT(`bg`.regdate, '%H:%i') AS reg_time,
        `bg`.file_name,
        `bg`.file_path,
        `bg`.hit,
        `bg`.display,
        (SELECT count(bgr.r_no) FROM board_guide_read bgr WHERE bgr.read_s_no='{$session_s_no}' AND bgr.b_no = bg.b_no) as read_cnt
    FROM board_guide bg
    WHERE {$add_where}
    ORDER BY b_no DESC
    LIMIT {$offset}, {$num}
";
$guide_query    = mysqli_query($my_db, $guide_sql);
$guide_list     = [];
while($guide = mysqli_fetch_assoc($guide_query))
{
    $keyword     = isset($guide['keyword']) ? "#".implode(" #", explode(',', $guide['keyword'])) : "";
    $file_name   = isset($guide['file_name']) && !empty($guide['file_name']) ? explode(',', $guide['file_name']) : "";
    $file_path   = isset($guide['file_path']) && !empty($guide['file_path']) ? explode(',', $guide['file_path']) : "";
    $file_count  = !empty($file_path) && !empty($file_path) ? count($file_path) : 0;

    $guide['keyword']           = $keyword;
    $guide['file_count']        = $file_count;
    $guide['file_name_list']    = $file_name;
    $guide['file_path_list']    = $file_path;

    $guide_list[] = $guide;
}

$smarty->assign('guide_display_list', getDisplayOption());
$smarty->assign('page_type_list', getPageTypeOption(4));
$smarty->assign('guide_list', $guide_list);

$smarty->display('board/board_guide_list.html');

?>
