<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/board.php');
require('../inc/model/MyQuick.php');
require('../inc/model/Board.php');
require('../inc/model/Team.php');

# 기본 설정
$dir_location = "../";
$board_model  = Board::Factory();
$board_model->setBoardNormal();
$smarty->assign("dir_location",$dir_location);

# 프로세스 처리
$process = (isset($_POST['process'])) ? $_POST['process'] : "";

if($process == "display_off")
{
	$b_no		= (isset($_POST['no']))?$_POST['no']:"";
    $search_url	= isset($_POST['search_url'])?$_POST['search_url']:"";

	$board_model->update(array("no" => $b_no, "display" => 2));

	exit("<script>location.href='board_normal_list.php?{$search_url}';</script>");
}
elseif($process == "display_on")
{
	$b_no		= (isset($_POST['no']))?$_POST['no']:"";
    $search_url	= isset($_POST['search_url'])?$_POST['search_url']:"";

	$board_model->update(array("no" => $b_no, "display" => 1));

	exit("<script>location.href='board_normal_list.php?{$search_url}';</script>");
}
elseif ($process == "f_category")
{
	$b_no  	= (isset($_POST['no'])) ? $_POST['no'] : "";
	$value 	= (isset($_POST['val'])) ? $_POST['val'] : "";

	if (!$board_model->update(array("no" => $b_no, "category" => $value))) {
		echo "카테고리 변경에 실패 하였습니다.";
	}else{
		echo "카테고리가 변경되었습니다.";
	}
	exit;
}
elseif ($process == "duplicate")
{
	$b_no  		= (isset($_POST['no'])) ? $_POST['no'] : "";
	$board_item = $board_model->getItem($b_no);

	$duplicate_sql = "INSERT INTO board_normal(b_id, title, category, brand, contents, team_code, s_no, regdate, file_origin, file_read)
		(SELECT b_id, title, category, brand, contents, '{$session_team}', '{$session_s_no}', now(), file_origin, file_read FROM board_normal WHERE `no`='{$b_no}') 
	";

	if (!mysqli_query($my_db, $duplicate_sql)) {
		exit("<script>alert('복제에 실패했습니다.');location.href='board_normal_list.php?b_id={$board_item['b_id']}';</script>");
	}else{
		exit("<script>alert('복제했습니다.');location.href='board_normal_list.php?b_id={$board_item['b_id']}';</script>");
	}
}
else
{
	# Board ID 체크
	$b_id = isset($_GET['b_id']) ? $_GET['b_id'] : "";

    if(empty($b_id)){
        echo("<script>alert('올바른 접근이 아닙니다.');history.back();</script>");
    }

    if($b_id == 'piel_daily' || $b_id == 'piel_mark'){
        if (!permissionNameCheck($session_permission,"마스터관리자") && !permissionNameCheck($session_permission,"대표") && ($session_s_no != '42' && $session_s_no != '171')){
            $smarty->display('access_company_error.html');
            exit;
        }
    }

    if(($b_id != "cs_with" && $b_id != "mc_event") && $session_my_c_type == '3'){
        exit("<script>alert('허용된 URL이 아닙니다.');location.href='/v1/board/board_normal_list.php?b_id=cs_with';</script>");
	}

    if(($b_id != "mc_event") && $session_my_c_type == '2'){
        exit("<script>alert('허용된 URL이 아닙니다.');location.href='/v1/board/board_normal_list.php?b_id=mc_event';</script>");
    }

    # 리스트용 변수
	$team_model 		 	= Team::Factory();
	$team_full_name_list 	= $team_model->getTeamFullNameList();
    $sch_category_list 		= [];
	$sch_brand_list			= [];

    if($b_id == 'cs_with'){
        $sch_category_list = array(
        	"1" => "대기",
        	"2" => "확인중",
        	"3" => "확인완료",
		);
	}else{
        $sch_cate_sql    = "SELECT category FROM board_manager WHERE b_id='{$b_id}' AND display='1' LIMIT 1";
        $sch_cate_query  = mysqli_query($my_db, $sch_cate_sql);
        $sch_cate_result = mysqli_fetch_assoc($sch_cate_query);

        if(isset($sch_cate_result['category']) && !empty($sch_cate_result['category']))
        {
            $sch_cate_val = explode("$", $sch_cate_result['category']);
            $sch_category_list = $sch_cate_val;
        }
	}

	if($b_id == 'mc_event'){
		$sch_brand_list = getBrandCategoryOption();
	}

	# 검색쿼리 시작
	if(permissionNameCheck($session_permission,"마스터관리자") || permissionNameCheck($session_permission,"재무관리자")) {
		$add_where = " 1=1";
	}else {
		$add_where = " 1=1 AND (display = '1' OR (brd_nm.display = '2' AND brd_nm.s_no = {$session_s_no}))";
	}

	# 검색 쿼리
	$sch_title  	= isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
    $sch_category 	= isset($_GET['sch_category']) ? $_GET['sch_category'] : "";
    $sch_brand 		= isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
    $sch_team 		= isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
	$sch_s_name 	= isset($_GET['sch_s_name'])?$_GET['sch_s_name']:"";
	$sch_s_date 	= isset($_GET['sch_s_date'])?$_GET['sch_s_date']:"";
	$sch_e_date 	= isset($_GET['sch_e_date'])?$_GET['sch_e_date']:"";
	$page 	   		= isset($_GET['page']) ? $_GET['page'] : "";
	$ord_type 		= isset($_GET['ord_type']) ? $_GET['ord_type'] : "";

	if (!empty($b_id)) {
		$add_where .= " AND brd_nm.b_id = '{$b_id}'";
		$smarty->assign("b_id", $b_id);
	}

	if($b_id == 'wp_inquiry' && $session_s_no != '28'){
        $add_where .= " AND (brd_nm.s_no = '{$session_s_no}' OR (brd_nm.s_no = '28' AND brd_nm.top_view = '1'))";
	}

    if (!empty($sch_category)) {
        $add_where .= " AND brd_nm.category='{$sch_category}'";
        $smarty->assign("sch_category", $sch_category);
    }

    if($b_id == 'mc_event' && !isset($_GET['sch_brand'])) {
    	$brand_staff_list 	= getBrandCategoryStaffOption();
		$sch_brand 			= isset($brand_staff_list[$session_s_no]) ? $brand_staff_list[$session_s_no] : "";
	}

	if(!empty($sch_brand)){
		$add_where .= " AND brd_nm.brand='{$sch_brand}'";
		$smarty->assign("sch_brand", $sch_brand);
	}

	if (!empty($sch_title)) {
		$add_where .= " AND brd_nm.title like '%{$sch_title}%'";
		$smarty->assign("sch_title", $sch_title);
	}

	if(!empty($sch_team))
	{
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        if($sch_team_code_where){
            $add_where .= " AND brd_nm.team_code IN ({$sch_team_code_where})";
        }
		$smarty->assign("sch_team", $sch_team);
	}

	if(!empty($sch_s_name)) {
		$add_where .= " AND s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_s_name}%')";
		$smarty->assign("sch_s_name", $sch_s_name);
	}

	if(!empty($sch_s_date)){
		$sch_s_datetime	= $sch_s_date." 00:00:00";
		$add_where 	   .= " AND brd_nm.regdate >= '{$sch_s_datetime}'";
		$smarty->assign("sch_s_date", $sch_s_date);
	}

	if(!empty($sch_e_date)){
		$sch_e_datetime = $sch_e_date." 23:59:59";
		$add_where     .= " AND brd_nm.regdate <= '{$sch_e_datetime}'";
		$smarty->assign("sch_e_date", $sch_e_date);
	}

	$add_orderby = "top_view DESC, no DESC ";
    $ord_type_by = "";
    if(!empty($ord_type))
    {
        if($ord_type == 'team_priority'){
            $add_orderby = "team_priority ASC";
        }else{
            $ord_type_by = (isset($_GET['ord_type_by']) && !empty($_GET['ord_type_by'])) ? $_GET['ord_type_by'] : "";

            if($ord_type_by == '1'){
                $add_orderby = "team_name ASC";
            }elseif($ord_type_by == '2'){
                $add_orderby = "team_name DESC";
            }else{
                $add_orderby = "no DESC";
            }
        }
    }
    $smarty->assign('ord_type', $ord_type);
    $smarty->assign('ord_type_by', $ord_type_by);

    #날짜 계산
    $quick_s_date = date("Y-m-d", strtotime("-3 days"));
    $quick_e_date = date("Y-m-d");
    $smarty->assign("quick_s_date", $quick_s_date);
    $smarty->assign("quick_e_date", $quick_e_date);

    #New & 댓글 체크
    $new_tag = $comment_tag = 0;
    $board_title = "";
    if(!empty($b_id))
    {
        $b_manager_sql 	  = "SELECT * FROM board_manager WHERE b_id='{$b_id}' LIMIT 1";
        $b_manager_query  = mysqli_query($my_db, $b_manager_sql);
        $b_manager_result = mysqli_fetch_assoc($b_manager_query);

        $new_tag 		= isset($b_manager_result['new_display']) ? $b_manager_result['new_display'] : 0;
        $comment_tag 	= isset($b_manager_result['comment_display']) ? $b_manager_result['comment_display'] : 0;
		$board_title	= $b_manager_result['board_name'];
    }
    $smarty->assign("new_tag", $new_tag);
    $smarty->assign("comment_tag", $comment_tag);
    $smarty->assign("board_title", $board_title);

	# 전체 게시물 수
	$board_normal_total_sql		= "SELECT count(`no`) as cnt FROM board_normal brd_nm WHERE {$add_where}";
	$board_normal_total_query	= mysqli_query($my_db, $board_normal_total_sql);
	$board_normal_total_result	= mysqli_fetch_array($board_normal_total_query);
	$board_normal_total 		= $board_normal_total_result["cnt"];

	$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "10";
	$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num 		= $page_type;
	$offset 	= ($pages-1) * $num;
	$page_num	= ceil($board_normal_total/$num);

	if ($pages >= $page_num){$pages = $page_num;}
	if ($pages <= 0){$pages = 1;}

    $search_url = getenv("QUERY_STRING");
    if(!empty($search_url))
    {
        $search_url_list = explode("&", $search_url);
        $chk_search_url = [];
        foreach($search_url_list as $sch_url){
            if(strpos($sch_url, "mode") === false && strpos($sch_url, "no") === false){
                $sch_url_val = explode("=", $sch_url, 2);
                if(!empty($sch_url_val[0])){
                    $chk_search_url[$sch_url_val[0]] = "{$sch_url_val[0]}={$sch_url_val[1]}";
                }
            }
        }

        if(!empty($chk_search_url)){
            $search_url = implode("&", $chk_search_url);
        }
    }
	$page_list	= pagelist($pages, "board_normal_list.php", $page_num, $search_url);

	$smarty->assign("search_url", $search_url);
	$smarty->assign("total_num", $board_normal_total);
	$smarty->assign("page_list", $page_list);
	$smarty->assign("ord_page_type", $page_type);

	# 리스트 쿼리 결과(데이터)
	$board_normal_list 	= [];
	$board_normal_sql 	= "
		SELECT
			*,
			(SELECT count(*) FROM comment WHERE table_name='board_normal' AND b_id='{$b_id}' AND table_no=brd_nm.no AND display=1) AS comment_cnt,
			(SELECT t.team_name FROM team t WHERE t.team_code=brd_nm.team_code) AS team_name,
			(SELECT t.priority FROM team t WHERE t.team_code=brd_nm.team_code) AS team_priority,
			(SELECT s_name FROM staff s where s.s_no=brd_nm.s_no) AS s_name,
			(SELECT count(br.r_no) FROM board_read br WHERE br.read_s_no='{$session_s_no}' AND br.b_no = brd_nm.`no` AND br.b_id='{$b_id}') as read_cnt
		FROM board_normal brd_nm
		WHERE {$add_where}
		ORDER BY {$add_orderby}
		LIMIT {$offset}, {$num}
	";
	$board_normal_query	= mysqli_query($my_db, $board_normal_sql);
	while($board_normal_array = mysqli_fetch_array($board_normal_query))
	{
		if(!empty($board_normal_array['regdate'])) {
			$regdate_day_value 	= date("Y/m/d",strtotime($board_normal_array['regdate']));
			$regdate_time_value = date("H:i",strtotime($board_normal_array['regdate']));
		}else{
			$regdate_day_value	= "";
			$regdate_time_value	= "";
		}

		$file_origin = isset($board_normal_array['file_origin']) && !empty($board_normal_array['file_origin']) ? explode(',', $board_normal_array['file_origin']) : "";
		$file_read   = isset($board_normal_array['file_read']) && !empty($board_normal_array['file_read']) ? explode(',', $board_normal_array['file_read']) : "";

        $yet_comment_cnt = 0;
        if($comment_tag == '1')
        {
            $b_read_where = "";
            if($board_normal_array['read_cnt'] > 0){
                $board_read_sql 	= "SELECT * FROM board_read WHERE b_id='{$b_id}' AND b_no='{$board_normal_array['no']}' AND read_s_no='{$session_s_no}' LIMIT 1";
                $board_read_query 	= mysqli_query($my_db, $board_read_sql);
                $board_read_result 	= mysqli_fetch_assoc($board_read_query);

                if(isset($board_read_result['read_upd_date'])){
                    $b_read_where = "AND regdate > '{$board_read_result['read_upd_date']}'";
                }
            }

            $comment_chk_sql 	= "SELECT count(`no`) as cnt FROM comment WHERE `table_name`='board_normal' AND b_id='{$b_id}' AND table_no='{$board_normal_array['no']}' AND display='1' {$b_read_where}";
            $comment_chk_query 	= mysqli_query($my_db, $comment_chk_sql);
            $comment_chk_result = mysqli_fetch_assoc($comment_chk_query);

            if(isset($comment_chk_result['cnt'])){
                $yet_comment_cnt = $comment_chk_result['cnt'];
            }
        }

        $read_cnt = $board_normal_array['read_cnt'];
        if($board_normal_array['regdate'] < '2022-05-19 00:00:00'){
            $read_cnt = 1;
            $yet_comment_cnt = 0;
		}

		$board_normal_array['regdate_day'] 		= $regdate_day_value;
		$board_normal_array['regdate_time'] 	= $regdate_time_value;
		$board_normal_array['file_origin'] 		= $file_origin;
		$board_normal_array['file_read'] 		= $file_read;
		$board_normal_array['read_cnt'] 		= $read_cnt;
		$board_normal_array['yet_comment_cnt'] 	= $yet_comment_cnt;

		$board_normal_list[] = $board_normal_array;
	}

	# Navigation & My Quick
	$nav_prd_no  = "";
	$is_my_quick = "";
	if($session_my_c_type == '1')
	{
		$quick_model = MyQuick::Factory();
		switch($b_id){
			case "mc_event":
				$nav_prd_no = "25";
				break;
			case "wp_inquiry":
				$nav_prd_no = "89";
				break;
			case "event":
				$nav_prd_no = "90";
				break;
			case "qa":
				$nav_prd_no = "93";
				break;
			case "success":
				$nav_prd_no = "94";
				break;
			case "mkt_qa":
				$nav_prd_no = "95";
				break;
			case "lvup":
				$nav_prd_no = "97";
				break;
			case "wise_csm":
				$nav_prd_no = "99";
				break;
			case "cs_with":
				$nav_prd_no = "100";
				break;
			case "week_w_r":
				$nav_prd_no = "101";
				break;
			case "home_w_r":
				$nav_prd_no = "102";
				break;
			case "media_week":
				$nav_prd_no = "104";
				break;
			case "cms_report":
				$nav_prd_no = "218";
				break;
		}

		if(!empty($nav_prd_no)){
			$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
		}
	}
	$smarty->assign("is_my_quick", $is_my_quick);
	$smarty->assign("nav_prd_no", $nav_prd_no);

	$smarty->assign("page_type_option", getPageTypeOption(4));
	$smarty->assign("sch_category_list", $sch_category_list);
	$smarty->assign("sch_brand_list", $sch_brand_list);
	$smarty->assign("sch_team_list", $team_full_name_list);
	$smarty->assign("board_normal_list", $board_normal_list);

	$smarty->display('board/board_normal_list.html');
}
?>
