<?php
require('../inc/common.php');
require('../ckadmin.php');

/** Error reporting */
ini_set('error_reporting',E_ALL & ~E_NOTICE | E_STRICT);
date_Default_TimeZone_set("Asia/Seoul");	// 시간설정
define('ROOTPATH', dirname(__FILE__));


/** Include PHPExcel */
require_once '../Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
	->setLastModifiedBy("Maarten Balliauw")
	->setTitle("Office 2007 XLSX Test Document")
	->setSubject("Office 2007 XLSX Test Document")
	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	->setKeywords("office 2007 openxml php")
	->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);


//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "날짜")
	->setCellValue('B1', "시간")
	->setCellValue('C1', "이름")
;


// 리스트 내용
$add_where = 'brd_nm.display=1 ';
$b_id = "home_w_r";

$sch_title_get  	= isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_category_get 	= isset($_GET['sch_category']) ? $_GET['sch_category'] : "";
$sch_team_get 		= isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$sch_s_name_get 	= isset($_GET['sch_s_name'])?$_GET['sch_s_name']:"";
$sch_s_date_get 	= isset($_GET['sch_s_date'])?$_GET['sch_s_date']:"";
$sch_e_date_get 	= isset($_GET['sch_e_date'])?$_GET['sch_e_date']:"";

if (!empty($b_id)) {
    $add_where .= " AND brd_nm.b_id = '" . $b_id . "'";
}

if (!empty($sch_category_get)) {
    $add_where .= " AND brd_nm.category ='{$sch_category_get}'";
}

if (!empty($sch_title_get)) {
    $add_where .= " AND brd_nm.title like '%" . $sch_title_get . "%'";
}

if(!empty($sch_team_get))
{
    $sch_team_code_where = getTeamWhere($my_db, $sch_team_get);
    if($sch_team_code_where){
        $add_where .= " AND brd_nm.team_code IN ({$sch_team_code_where})";
    }
}

if(!empty($sch_s_name_get)) {
    $add_where.=" AND s_no IN (SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_s_name_get}%')";
}

if(!empty($sch_s_date_get)){
    $sch_s_date = $sch_s_date_get." 00:00:00";
    $add_where .= " AND brd_nm.regdate >= '{$sch_s_date}'";
    $smarty->assign("sch_s_date", $sch_s_date_get);
}

if(!empty($sch_e_date_get)){
    $sch_e_date = $sch_e_date_get." 23:59:59";
    $add_where .= " AND brd_nm.regdate <= '{$sch_e_date}'";
    $smarty->assign("sch_e_date", $sch_e_date_get);
}


// 리스트 쿼리
$board_normal_sql = "
		SELECT
			(SELECT s_name FROM staff s where s.s_no=brd_nm.s_no) AS s_name,
			brd_nm.regdate
		FROM board_normal brd_nm
		WHERE {$add_where}
		ORDER BY `no` DESC
";


$result= mysqli_query($my_db, $board_normal_sql);
for ($i = 2; $board_array = mysqli_fetch_array($result); $i++)
{
	$board_date = date('Y.m.d', strtotime($board_array['regdate']));
    $board_time = date('H:i', strtotime($board_array['regdate']));


	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue("A{$i}", $board_date)
		->setCellValue("B{$i}", $board_time)
		->setCellValue("C{$i}", $board_array['s_name']);
}

if($i > 1)
	$i = $i-1;


$objPHPExcel->getActiveSheet()->getStyle("A1:C{$i}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:C{$i}")->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A{$i}:C{$i}")->getFont()->setSize(10);

$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
	->getStartColor()->setARGB('00BFBFBF');
$objPHPExcel->getActiveSheet()->getStyle('A3:C'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->setTitle('재택근무 리스트');


# 2022년 통계
$objPHPExcel->createSheet(1);
$workSheet = $objPHPExcel->setActiveSheetIndex(1);
$workSheet->getDefaultStyle()->getFont()
    ->setName('나눔 고딕')
    ->setSize(10);
$objPHPExcel->getActiveSheet()->setTitle('팀별 주단위 데이터');
$objPHPExcel->getActiveSheet()->getStyle("A1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');

$eng_abc  = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
$eng_abcc = [];
$eng_list = $eng_abc;
foreach($eng_abc as $a){
    foreach($eng_abc as $b){
        $eng_list[] = $a.$b;
        $eng_abcc[] = $a.$b;
    }
}

foreach($eng_abcc as $aa){
    foreach($eng_abc as $b){
        $eng_list[] = $aa.$b;
    }
}

$eng_idx = 1;
$all_date_sql = "
	SELECT
		DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL (DAYOFWEEK(`allday`.Date)-2) DAY), '%Y/%m/%d') as start_date,
		DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL (DAYOFWEEK(`allday`.Date)-8) DAY), '%Y/%m/%d') as end_date,
		DATE_FORMAT(`allday`.Date, '%u') as w_set
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
		(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
		(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
		(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
		(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
		(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e
	) 
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '2022-01-03' AND '2022-12-31'
	GROUP BY w_set
	ORDER BY w_set
";

$all_date_query = mysqli_query($my_db, $all_date_sql);
$all_date_list  = [];
$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(20);
$last_alpha     = "A";
while($all_date_result = mysqli_fetch_assoc($all_date_query))
{
    $last_alpha = $eng_list[$eng_idx];
    $workSheet->setCellValue("{$eng_list[$eng_idx]}1", $all_date_result['start_date']."~".$all_date_result['end_date']);
    $objPHPExcel->getActiveSheet()->getColumnDimension("{$eng_list[$eng_idx]}")->setWidth(20);
    $objPHPExcel->getActiveSheet()->getStyle("{$eng_list[$eng_idx]}1")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("{$eng_list[$eng_idx]}1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
    $objPHPExcel->getActiveSheet()->getStyle("{$eng_list[$eng_idx]}1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("{$eng_list[$eng_idx]}1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $all_date_list[$all_date_result['w_set']] = $eng_list[$eng_idx];
    $eng_idx++;
}

$team_sql = "
	SELECT
		team_code,
		(SELECT team_name FROM team t where t.team_code=brd_nm.team_code) AS t_name
	FROM board_normal brd_nm
	WHERE b_id='home_w_r' AND display='1' AND regdate BETWEEN '2022-01-01' AND '2022-12-31'
	GROUP BY team_code
	ORDER BY t_name ASC;
";
$team_query = mysqli_query($my_db, $team_sql);
$team_list  = [];
$team_idx 	= 2;
while($team_result = mysqli_fetch_assoc($team_query))
{
    $team_list[$team_result['team_code']] = $team_idx;
    $workSheet->setCellValue("A{$team_idx}", $team_result['t_name']);
    $objPHPExcel->getActiveSheet()->getStyle("A{$team_idx}")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A{$team_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
    $team_idx++;
}

$board_normal_sql = "
	SELECT
		team_code,
		DATE_FORMAT(brd_nm.regdate, '%u') as w_set,
		count(no) as b_cnt
	FROM board_normal brd_nm
	WHERE b_id='home_w_r' AND display='1' AND regdate BETWEEN '2022-01-01' AND '2022-12-31'
	GROUP BY w_set, team_code
	ORDER BY regdate ASC;
";
$board_normal_query = mysqli_query($my_db, $board_normal_sql);
$board_normal_list  = [];
while($board_normal_result = mysqli_fetch_assoc($board_normal_query))
{
    $board_normal_list[$board_normal_result['team_code']][$board_normal_result['w_set']] = $board_normal_result['b_cnt'];
}

$board_idx = 2;
$last_idx  = 2;
foreach($team_list as $team_code => $team_idx)
{
	foreach ($all_date_list as $w_set => $alpha)
	{
		$b_cnt = isset($board_normal_list[$team_code][$w_set]) ? $board_normal_list[$team_code][$w_set] : 0;
        $workSheet->setCellValue("{$alpha}{$team_idx}", $b_cnt);
	}
    $last_idx = $team_idx;
}
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_alpha}{$last_idx}")->applyFromArray($styleArray);


# 사용자별 데이터
$objPHPExcel->createSheet(2);
$workSheet2 = $objPHPExcel->setActiveSheetIndex(2);
$workSheet2->getDefaultStyle()->getFont()
    ->setName('나눔 고딕')
    ->setSize(10);
$objPHPExcel->getActiveSheet()->setTitle('사용자별 주단위 데이터');
$objPHPExcel->getActiveSheet()->getStyle("A1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');

$eng_idx = 1;
$all_date_sql = "
	SELECT
		DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL (DAYOFWEEK(`allday`.Date)-2) DAY), '%Y/%m/%d') as start_date,
		DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL (DAYOFWEEK(`allday`.Date)-8) DAY), '%Y/%m/%d') as end_date,
		DATE_FORMAT(`allday`.Date, '%u') as w_set
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
		(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
		(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
		(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
		(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
		(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e
	) 
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '2022-01-03' AND '2022-12-31'
	GROUP BY w_set
	ORDER BY w_set
";

$all_date_query = mysqli_query($my_db, $all_date_sql);
$all_date_list  = [];
$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(20);
while($all_date_result = mysqli_fetch_assoc($all_date_query))
{
    $workSheet2->setCellValue("{$eng_list[$eng_idx]}1", $all_date_result['start_date']."~".$all_date_result['end_date']);
    $objPHPExcel->getActiveSheet()->getStyle("{$eng_list[$eng_idx]}1")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("{$eng_list[$eng_idx]}1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
    $objPHPExcel->getActiveSheet()->getStyle("{$eng_list[$eng_idx]}1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle("{$eng_list[$eng_idx]}1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getColumnDimension("{$eng_list[$eng_idx]}")->setWidth(20);
    $all_date_list[$all_date_result['w_set']] = $eng_list[$eng_idx];
    $eng_idx++;
}

$staff_sql = "
	SELECT
		s_no,
		(SELECT s_name FROM staff s where s.s_no=brd_nm.s_no) AS s_name
	FROM board_normal brd_nm
	WHERE b_id='home_w_r' AND display='1' AND regdate BETWEEN '2022-01-01' AND '2022-12-31'
	GROUP BY s_no
	ORDER BY s_name ASC;
";
$staff_query = mysqli_query($my_db, $staff_sql);
$staff_list  = [];
$staff_idx 	= 2;
while($staff_result = mysqli_fetch_assoc($staff_query))
{
    $staff_list[$staff_result['s_no']] = $staff_idx;
    $workSheet2->setCellValue("A{$staff_idx}", $staff_result['s_name']);
    $objPHPExcel->getActiveSheet()->getStyle("A{$staff_idx}")->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle("A{$staff_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
    $staff_idx++;
}

$board_normal_sql = "
	SELECT
		s_no,
		DATE_FORMAT(brd_nm.regdate, '%u') as w_set,
		count(no) as b_cnt
	FROM board_normal brd_nm
	WHERE b_id='home_w_r' AND display='1' AND regdate BETWEEN '2022-01-01' AND '2022-12-31'
	GROUP BY w_set, s_no
	ORDER BY regdate ASC;
";
$board_normal_query = mysqli_query($my_db, $board_normal_sql);
$board_normal_list  = [];
while($board_normal_result = mysqli_fetch_assoc($board_normal_query))
{
    $board_normal_list[$board_normal_result['s_no']][$board_normal_result['w_set']] = $board_normal_result['b_cnt'];
}

$board_idx = 2;
$last_idx  = 2;
foreach($staff_list as $s_no => $staff_idx)
{
    foreach ($all_date_list as $w_set => $alpha)
    {
        $b_cnt = isset($board_normal_list[$s_no][$w_set]) ? $board_normal_list[$s_no][$w_set] : 0;
        $workSheet2->setCellValue("{$alpha}{$staff_idx}", $b_cnt);
    }

    $last_idx = $staff_idx;
}
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_alpha}{$last_idx}")->applyFromArray($styleArray);


$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_"."재택근무 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
