<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/board.php');
require('../inc/model/MyQuick.php');
require('../inc/model/Team.php');
require('../inc/model/Board.php');

if (!permissionNameCheck($session_permission, "재무관리자") && !permissionNameCheck($session_permission, "마스터관리자")){
    $smarty->display('access_company_error.html');
    exit;
}

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# Navigation & My Quick
$nav_prd_no  = "98";
$nav_title   = "게시글 댓글 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 가이드 세부 검색
$add_where      = "1=1";
$sch_no         = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_b_id       = isset($_GET['sch_b_id']) ? $_GET['sch_b_id'] : "";
$sch_comment    = isset($_GET['sch_comment']) ? $_GET['sch_comment'] : "";
$sch_team 	    = isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$sch_s_name     = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
$sch_display    = isset($_GET['sch_display']) ? $_GET['sch_display'] : "";
$sch_secret     = isset($_GET['sch_secret']) ? $_GET['sch_secret'] : "";
$sch_unknown    = isset($_GET['sch_unknown']) ? $_GET['sch_unknown'] : "";

if(!empty($sch_no))
{
    $add_where .= " AND `co`.no = '{$sch_no}'";
    $smarty->assign('sch_no', $sch_no);
}

if(!empty($sch_b_id))
{
    if($sch_b_id == 'personal_expenses'){
        $add_where .= " AND `co`.table_name='{$sch_b_id}'";
    }else{
        $add_where .= " AND `co`.b_id='{$sch_b_id}'";
    }
    $smarty->assign('sch_b_id', $sch_b_id);
}

if(!empty($sch_comment))
{
    $add_where .= " AND `co`.comment like '%{$sch_comment}%'";
    $smarty->assign('sch_comment', $sch_comment);
}

#팀 체크
if (!empty($sch_team))
{
    $sch_team_code_where = getTeamWhere($my_db, $sch_team);
    if($sch_team_code_where){
        $add_where .= " AND `co`.team IN ({$sch_team_code_where})";
    }
    $smarty->assign("sch_team",$sch_team);
}

if(!empty($sch_s_name))
{
    $add_where .= " AND `co`.s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name LIKE '%{$sch_s_name}%')";
    $smarty->assign('sch_s_name', $sch_s_name);
}

if(!empty($sch_display))
{
    $add_where .= " AND `co`.display='{$sch_display}'";
    $smarty->assign('sch_display', $sch_display);
}

if(!empty($sch_secret))
{
    if($sch_secret == '1'){
        $add_where .= " AND `co`.is_secret = '1'";
    }else{
        $add_where .= " AND `co`.is_secret != '1'";
    }
    $smarty->assign('sch_secret', $sch_secret);
}

if(!empty($sch_unknown))
{
    if($sch_unknown == '1'){
        $add_where .= " AND `co`.is_unknown = '1'";
    }else{
        $add_where .= " AND `co`.is_unknown != '1'";
    }
    $smarty->assign('sch_unknown', $sch_unknown);
}

# 전체 게시물 수
$comment_total_sql		= "SELECT count(`co`.no) as cnt FROM comment `co` WHERE {$add_where}";
$comment_total_query	= mysqli_query($my_db, $comment_total_sql);
$comment_total_result   = mysqli_fetch_array($comment_total_query);
$comment_total          = $comment_total_result['cnt'];

# 페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($comment_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "board_comment_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $comment_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 가이드 리스트 쿼리
$comment_sql  = "
    SELECT
        `co`.no,
        `co`.table_name,
        `co`.b_id,
        `co`.table_no,
        `co`.team,
        (SELECT depth FROM team t WHERE t.team_code=`co`.team) AS t_depth,
        `co`.comment,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`co`.s_no) AS s_name,
        IF(`co`.`table_name`='personal_expenses', (SELECT `pe`.`team` FROM personal_expenses `pe` WHERE `pe`.pe_no=`co`.table_no LIMIT 1), `co`.team) as sel_team,
        IF(`co`.`table_name`='personal_expenses', (SELECT DATE_FORMAT(`pe`.`payment_date`,'%Y-%m') FROM personal_expenses `pe` WHERE `pe`.pe_no=`co`.table_no LIMIT 1), DATE_FORMAT(`co`.regdate, '%Y-%m')) AS reg_mon,
        DATE_FORMAT(`co`.regdate, '%Y-%m-%d') AS reg_date,
        DATE_FORMAT(`co`.regdate, '%H:%i') AS reg_time,
        `co`.display,
        `co`.is_secret,
        `co`.is_unknown
    FROM comment `co`
    WHERE {$add_where}
    ORDER BY no DESC
    LIMIT {$offset}, {$num}
";
$comment_query      = mysqli_query($my_db, $comment_sql);
$comment_list       = [];
$board_model        = Board::Factory();
$board_type_list    = $board_model->getBoardType();
$team_model         = Team::Factory();
$team_full_name_list= $team_model->getTeamFullNameList();
while($comment = mysqli_fetch_assoc($comment_query))
{
    if($comment['b_id'] == 'cs_with'){
        continue;
    }

    if(isset($comment['table_name']) && $comment['table_name'] == 'personal_expenses'){
        $comment['b_name'] = "개인경비";
        $comment['b_url']  = "https://work.wplanet.co.kr/v1/personal_expenses.php?sch_pe_no=&sch_state=&sch_team={$comment['sel_team']}&sch_s_no={$comment['s_no']}&sch_s_name=&sch_s_month={$comment['reg_mon']}&sch_e_month={$comment['reg_mon']}";
    }else{
        $comment['b_name'] = $board_type_list[$comment['b_id']];
        $comment['b_url']  = "https://work.wplanet.co.kr/v1/board/{$comment['table_name']}_detail.php?no={$comment['table_no']}&b_id={$comment['b_id']}";
    }

    $comment['team_name'] = getTeamFullName($my_db, $comment['t_depth'], $comment['team']);
    $comment_list[] = $comment;
}

$smarty->assign('board_type_list', $board_type_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign('secret_option', getSecretOption());
$smarty->assign('unknown_option', getUnknownOption());
$smarty->assign('display_option', getDisplayOption());
$smarty->assign('page_type_option', getPageTypeOption(3));
$smarty->assign('comment_list', $comment_list);

$smarty->display('board/board_comment_list.html');

?>
