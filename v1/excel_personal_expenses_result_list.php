<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/personal_expenses.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$fontbold = array(
    'font' => array(
        'bold' => true,
    ),
);

# 검색쿼리
$add_where          = " pe.display = '1' AND (pe.state = '4' OR pe.state = '5') AND pe.account_code != '0'"; // display ON, 승인완료 혹은 지급완료
$sch_c_no 		    = isset($_GET['sch_c_no']) ? $_GET['sch_c_no'] : "";
$sch_team           = isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team'] : "";
$sch_share_card     = isset($_GET['sch_share_card']) ? $_GET['sch_share_card'] : "";
$sch_req_s_name     = isset($_GET['sch_req_s_name']) ? $_GET['sch_req_s_name'] : "";
$sch_month 	        = isset($_GET['sch_month']) ? $_GET['sch_month'] : date("Y-m", strtotime("-1 month")); // 월간
$sch_account_code   = isset($_GET['sch_account_code']) ? $_GET['sch_account_code'] : "";

if (!empty($sch_c_no)) {
    if($sch_c_no == 'default'){
        $add_where .= " AND pe.my_c_no IN(1,2,5,7)";
    }elseif($sch_c_no == 'commerce'){
        $add_where .= " AND pe.my_c_no = 3";
    }
}

if(!empty($sch_team)) {
    if ($sch_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        $add_where          .= " AND pe.team IN ({$sch_team_code_where})";
    }
}

if(!empty($sch_share_card)) {
    $add_where .= " AND `cc`.share_card LIKE '%{$sch_share_card}%'";
}

if(!empty($sch_req_s_name)) {
    $add_where .= " AND `pe`.req_s_no IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_req_s_name}%')";
}

if (!empty($sch_month)) {
    $add_where .= " AND DATE_FORMAT(pe.payment_date, '%Y-%m') = '{$sch_month}'";
}

if (!empty($sch_account_code)) {
    $add_where .= " AND pe.account_code='{$sch_account_code}'";
}

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "개인/공용 사업자")
->setCellValue('B1', "부서")
->setCellValue('C1', "카드명")
->setCellValue('D1', "계정과목")
->setCellValue('E1', "계정별 금액")
->setCellValue('F1', "개별 합계")
->setCellValue('G1', "팀별 합계");

// 리스트 쿼리
$personal_expenses_sql = "
    SELECT
		pe.my_c_no,
		(SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=pe.my_c_no) as my_c_name,
		pe.team as share_group,
        (SELECT t.team_name FROM team t WHERE t.team_code=pe.team) as share_group_name,
		(SELECT t.priority FROM team t WHERE t.team_code=pe.team) AS team_priority,
		pe.s_no,
		(SELECT s_name FROM staff s where s.s_no=pe.s_no) AS s_name,
		(SELECT k_name_code FROM kind WHERE k_name_code = (SELECT k_parent FROM kind WHERE k_code = 'account_code' AND k_name_code=pe.account_code)) AS parent_account_code,
		(SELECT k_name FROM kind WHERE k_name_code = (SELECT k_parent FROM kind WHERE k_code = 'account_code' AND k_name_code=pe.account_code)) AS parent_account_code_name,
		pe.money,
		`cc`.card_type,		
		`cc`.share_card
	FROM personal_expenses pe
	LEFT JOIN corp_card `cc` ON `cc`.cc_no = pe.corp_card_no
	WHERE {$add_where}
	ORDER BY my_c_no ASC, team_priority ASC, parent_account_code ASC, s_name ASC
";
$personal_expenses_query = mysqli_query($my_db, $personal_expenses_sql);

# 공용
$corp_card_list     = [];
$corp_team_count    = "";
$corp_comp_count    = "";

#개인
$per_card_list      = [];

# 종합
$parent_account_total_list  = [];
$parent_account_total_sum   = 0;
$total_num                  = 0;
$total_pe_money             = 0;
while($pe_result = mysqli_fetch_assoc($personal_expenses_query))
{
    #개인, 공용 구분
    $card_type_chk = isset($pe_result['card_type']) && ($pe_result['card_type'] == '2') ? '2' : '1';

    if($card_type_chk == '1'){
        $share_group        = $pe_result['share_group'];
        $share_group_name   = $pe_result['share_group_name'];

        if(empty($pe_result['share_group'])){
            $chk_share_sql      = "SELECT `cc`.team, (SELECT t.team_name FROM team t WHERE t.team_code=`cc`.team) as team_name FROM corp_card `cc` WHERE card_type='3' AND manager='{$pe_result['s_no']}' AND display='1' LIMIT 1";
            $chk_share_query    = mysqli_query($my_db, $chk_share_sql);
            $chk_share_result   = mysqli_fetch_assoc($chk_share_query);

            $share_group        = isset($chk_share_result['team']) ? $chk_share_result['team'] : $pe_result['share_group'];
            $share_group_name   = isset($chk_share_result['team_name']) ? $chk_share_result['team_name'] : $pe_result['share_group_name'];
        }

        if(!isset($per_card_list[$pe_result['my_c_no']][$share_group][$pe_result['s_no']][$pe_result['parent_account_code']])){
            $per_card_list[$pe_result['my_c_no']][$share_group][$pe_result['s_no']][$pe_result['parent_account_code']] = array(
                'my_c_name' => $pe_result['my_c_name'],
                'team_name' => $share_group_name,
                's_name' 	=> $pe_result['share_card'],
                'code_name' => $pe_result['parent_account_code_name'],
                'money' 	=> 0,
            );
        }

        $per_card_list[$pe_result['my_c_no']][$share_group][$pe_result['s_no']][$pe_result['parent_account_code']]['money'] += $pe_result['money'];

    }
    elseif($card_type_chk == '2')
    {
        $share_group        = "public";
        $share_group_name   = "공용";
        if(!isset($corp_card_list[$pe_result['my_c_no']][$share_group][$pe_result['share_card']][$pe_result['parent_account_code']])){
            $corp_card_list[$pe_result['my_c_no']][$share_group][$pe_result['share_card']][$pe_result['parent_account_code']] = array(
                'my_c_name' 	=> $pe_result['my_c_name'],
                'share_group' 	=> $share_group_name,
                'share_card' 	=> $pe_result['share_card'],
                'code_name' 	=> $pe_result['parent_account_code_name'],
                'money' 		=> 0,
            );
        }

        $corp_card_list[$pe_result['my_c_no']][$share_group][$pe_result['share_card']][$pe_result['parent_account_code']]['money'] += $pe_result['money'];
    }

    # 계정 과목별 총계
    $parent_account_total_list[$pe_result['parent_account_code']]['label'] = $pe_result['parent_account_code_name'];
    $parent_account_total_list[$pe_result['parent_account_code']]['money'] += $pe_result['money'];

    $total_pe_money += $pe_result['money'];
}

#개인 카운트 및 합계
$per_team_data_list  = [];
$per_staff_data_list = [];
if($per_card_list)
{
    foreach ($per_card_list as $my_c_no => $per_card_comp_data){
        foreach ($per_card_comp_data as $team => $per_card_team_data){
            foreach ($per_card_team_data as $s_no => $per_card_staff_data){
                foreach ($per_card_staff_data as $per_card_acc_data){
                    $per_team_data_list[$my_c_no][$team]['total'] += $per_card_acc_data['money'];
                    $per_team_data_list[$my_c_no][$team]['count']++;

                    $per_staff_data_list[$my_c_no][$team][$s_no] += $per_card_acc_data['money'];
                    $total_num++;
                }
            }
        }
    }
}

#공용 카운트 및 합계
$corp_share_group_data_list = [];
$corp_share_card_data_list  = [];
if($corp_card_list)
{
    foreach ($corp_card_list as $my_c_no => $corp_card_comp_data){
        foreach ($corp_card_comp_data as $share_group => $corp_share_group_data){
            foreach ($corp_share_group_data as $share_card => $corp_share_card_data){
                foreach ($corp_share_card_data as $corp_card_acc_data){
                    $corp_share_group_data_list[$my_c_no][$share_group]['total'] += $corp_card_acc_data['money'];
                    $corp_share_group_data_list[$my_c_no][$share_group]['count']++;

                    $corp_share_card_data_list[$my_c_no][$share_group][$share_card] += $corp_card_acc_data['money'];
                    $total_num++;
                }
            }
        }
    }
}

#Excel 값 대입
$acc_sum    = 0;
$per_sum    = 0;
$teams_sum  = 0;
$excel_idx  = 2;
$workSheet  = $objPHPExcel->setActiveSheetIndex(0);
if($per_card_list)
{
    $workSheet->mergeCells("A{$excel_idx}:G{$excel_idx}");
    $workSheet->setCellValue("A{$excel_idx}", "#지정자및개인");
    $excel_idx++;

    foreach($per_card_list as $my_c_no => $per_card_comp_data)
    {
        foreach ($per_card_comp_data as $team => $per_card_team_data){
            $team_idx = 0;
            foreach($per_card_team_data as $s_no => $per_card_staff_data)
            {
                $staff_idx = 0;
                foreach($per_card_staff_data as $acc_code => $per_card_acc_data)
                {
                    $workSheet->setCellValue("A{$excel_idx}", $per_card_acc_data['my_c_name']);
                    $workSheet->setCellValue("B{$excel_idx}", $per_card_acc_data['team_name']);
                    $workSheet->setCellValue("C{$excel_idx}", $per_card_acc_data['s_name']);
                    $workSheet->setCellValue("D{$excel_idx}", $per_card_acc_data['code_name']);
                    $workSheet->setCellValue("E{$excel_idx}", number_format($per_card_acc_data['money']));

                    if($staff_idx == '0')
                    {
                        $staff_row = $excel_idx+count($per_card_staff_data)-1;
                        $staff_sum = $per_staff_data_list[$my_c_no][$team][$s_no];
                        $per_sum  += $staff_sum;

                        $workSheet->mergeCells("F{$excel_idx}:F{$staff_row}");
                        $workSheet->setCellValue("F{$excel_idx}", number_format($staff_sum));

                        $staff_idx++;
                    }

                    if($team_idx == '0')
                    {
                        $team_row  = $excel_idx+$per_team_data_list[$my_c_no][$team]['count']-1;
                        $team_sum  = $per_team_data_list[$my_c_no][$team]['total'];
                        $teams_sum += $team_sum;

                        $workSheet->mergeCells("G{$excel_idx}:G{$team_row}");
                        $workSheet->setCellValue("G{$excel_idx}", number_format($team_sum));

                        $team_idx++;
                    }

                    $acc_sum += $per_card_acc_data['money'];
                    $excel_idx++;
                }
            }
        }
    }
}

$corp_idx = $excel_idx;
if($corp_card_list)
{
    $workSheet->mergeCells("A{$excel_idx}:G{$excel_idx}");
    $workSheet->setCellValue("A{$excel_idx}", "#공용");
    $excel_idx++;

    foreach($corp_card_list as $my_c_no => $corp_card_comp_data)
    {
        foreach($corp_card_comp_data as $share_group => $corp_share_group_data)
        {
            $share_group_idx = 0;
            foreach ($corp_share_group_data as $share_card => $corp_share_card_data)
            {
                $share_card_idx = 0;
                foreach($corp_share_card_data as $acc_code => $corp_card_acc_data)
                {
                    $workSheet->setCellValue("A{$excel_idx}", $corp_card_acc_data['my_c_name']);
                    $workSheet->setCellValue("B{$excel_idx}", $corp_card_acc_data['share_group']);
                    $workSheet->setCellValue("C{$excel_idx}", $corp_card_acc_data['share_card']);
                    $workSheet->setCellValue("D{$excel_idx}", $corp_card_acc_data['code_name']);
                    $workSheet->setCellValue("E{$excel_idx}", number_format($corp_card_acc_data['money']));

                    if ($share_card_idx == '0')
                    {
                        $share_card_row = $excel_idx+count($corp_share_card_data)-1;
                        $share_card_sum = $corp_share_card_data_list[$my_c_no][$share_group][$share_card];
                        $per_sum  += $share_card_sum;

                        $workSheet->mergeCells("F{$excel_idx}:F{$share_card_row}");
                        $workSheet->setCellValue("F{$excel_idx}", number_format($share_card_sum));

                        $share_card_idx++;
                    }

                    if($share_group_idx == '0')
                    {
                        $share_group_row = $excel_idx+$corp_share_group_data_list[$my_c_no][$share_group]['count']-1;
                        $share_group_sum = $corp_share_group_data_list[$my_c_no][$share_group]['total'];
                        $teams_sum  += $share_group_sum;

                        $workSheet->mergeCells("G{$excel_idx}:G{$share_group_row}");
                        $workSheet->setCellValue("G{$excel_idx}", number_format($share_group_sum));

                        $share_group_idx++;
                    }

                    $acc_sum += $corp_card_acc_data['money'];
                    $excel_idx++;
                }
            }
        }
    }
}

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$workSheet->mergeCells("A{$excel_idx}:D{$excel_idx}");
$workSheet->setCellValue("A{$excel_idx}", "합계");
$workSheet->setCellValue("E{$excel_idx}", number_format($acc_sum));
$workSheet->setCellValue("F{$excel_idx}", number_format($per_sum));
$workSheet->setCellValue("G{$excel_idx}", number_format($teams_sum));

$objPHPExcel->getActiveSheet()->getStyle("A1:G{$excel_idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A2:G{$excel_idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00000000');
$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("A3:A{$excel_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("B3:B{$excel_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("C3:C{$excel_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("D3:D{$excel_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("E3:E{$excel_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("F3:F{$excel_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("G3:G{$excel_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("A{$excel_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00000000');
$objPHPExcel->getActiveSheet()->getStyle("A{$excel_idx}")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A{$excel_idx}")->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle("A{$corp_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("A{$corp_idx}")->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle("F3:F{$excel_idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("G3:G{$excel_idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(24);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
$objPHPExcel->getActiveSheet()->setTitle('개인경비(지출종합표)');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename=iconv('UTF-8','EUC-KR', date('Y-m-d').'_개인경비(지출종합표).xls');
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
