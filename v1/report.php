<?php
	session_start();
	require('inc/common.php');
	require('inc/data_state.php');


	// GET : registration.php?promotion_no= 로 넘어온 프로모션 고유번호(p_no)를 체크하고 있으면 code에 저장
	$code=isset($_GET['promotion_no']) ? $_GET['promotion_no'] : "";
	$blog_url_get=isset($_GET['blog_url']) ? $_GET['blog_url'] : "";
	$smarty->assign("blog_url",$blog_url_get);


	$nowdate = date("Y-m-d H:i:s");

	$sql="select * from promotion where p_no='".$code."'";
	//echo "$sql = ".$sql."<br><br>";

	$query=mysqli_query($my_db,$sql);
	$promotion_info=mysqli_fetch_array($query);

	// POST : form action 을 통해 넘겨받은 process 가 있는지 체크하고 있으면 proc에 저장
	$proc=isset($_POST['process']) ? $_POST['process'] : "";
	//echo "$proc = ".$proc;

	// form action 이 등록일 경우
	if($proc=="write")
	{

		//print_r($_POST);echo "<br><br><br>";
		$sql="select * from blog where blog_url = '".addslashes(trim($_POST['blog_url']))."'";
		$query=mysqli_query($my_db,$sql);
		$blog_info=mysqli_fetch_array($query);

		$application_sql="select * from application where blog_url = '".addslashes(trim($_POST['blog_url']))."' and p_no='".$code."' AND a_state='1'";
		$application_query=mysqli_query($my_db,$application_sql);
		$application_info=mysqli_fetch_array($application_query);

		$bk_value = "";
		if($promotion_info['kind'] == '2' || $promotion_info['kind'] == '4' || $promotion_info['kind'] == '5'){

			if(!empty($_POST['bk_title'])) {
				$bk_value.=" bk_title = '".addslashes($_POST['bk_title'])."',";
			}

			if($_POST['f_pres_reason_apply'] == 'N'){
					$bk_value.=" bk_num = '추가 보상 조건(미참여)',";
			}else{
				if(!empty($_POST['bk_num'])) {
					$bk_value.=" bk_num = '".addslashes(trim($_POST['bk_num']))."',";
				}
			}

			if(!empty($_POST['bk_name'])) {
				$bk_value.=" bk_name = '".addslashes(trim($_POST['bk_name']))."',";
			}

			if(!empty($_POST['bk_jumin'])) {
				//AES 인코딩
				$aes_hex = "HEX(AES_ENCRYPT('".addslashes(trim($_POST['bk_jumin']))."', '".$aeskey."'))";
				$bk_value.=" bk_jumin = ".$aes_hex.",";
			}
		}

		$query_org="insert into report set
					p_no = '".$code."',
					p_code = '".$promotion_info['promotion_code']."',
					c_no = '".$_POST['c_no']."',
					b_no = '".$blog_info['b_no']."',
					s_no = '".$_POST['s_no']."',
					a_no = '".$application_info['a_no']."',
					blog_url = '".addslashes(trim($_POST['blog_url']))."',
					memo = '".addslashes($_POST['memo'])."',
					".$bk_value."
					r_datetime = '".date("Y-m-d H:i:s")."',
					r_ip = '".ip2long($_SERVER['REMOTE_ADDR'])."'
				";


		if(addslashes(trim($_POST['post_title1'])) != "" || addslashes(trim($_POST['post_url1'])) != "")
		{
			$query_file_org = $query_org;

			# 첨부파일 작업
			$report_file = $_FILES['report_file'];
            $file_path 	 = "";
            $file_name 	 = "";
			if(isset($report_file['name']) && !empty($report_file['name'])){
				$r_file_path = store_image('report_file', "topblog");
				$file_path   = implode(',', $r_file_path);
				$file_name   = implode(',',$report_file['name']);
				$query_file_org  .= ", `file_path`='{$file_path}', `file_name`='{$file_name}'";
			}

            if($promotion_info['is_file'] == '1' && (empty($file_path) || empty($file_name)))
            {
                exit("<script>alert('파일처리에 실패했습니다.\\n다시 시도해주세요.\\n문제가 계속 될 경우 탑블로그로 문의바랍니다.\\n(wiseplanners@naver.com , 02-2675-6260)');history.back()</script>");
            }

			$query = $query_file_org.query_posting("post_title", addslashes(trim($_POST['post_title1'])), "post_url", addslashes(trim($_POST['post_url1'])));
			
			if(!$my_db->query($query))
				exit("<script>alert('완료보고를 처리하지 못하였습니다.\\n문제가 계속 될 경우 탑블로그로 문의바랍니다.\\n(wiseplanners@naver.com , 02-2675-6260)');</script>");
		}

		$input_count = isset($_POST['input_count']) && !empty($_POST['input_count']) ? $_POST['input_count'] : 1;
		if($input_count > 1)
		{
            for($i=2;$i<=$input_count;$i++)
            {
            	$po_title_key 	= "post_title{$i}";
            	$po_url_key 	= "post_url{$i}";
                if(addslashes(trim($_POST[$po_title_key])) != "" || addslashes(trim($_POST[$po_url_key])) != ""){
                    $query = $query_org.query_posting("post_title", addslashes(trim($_POST[$po_title_key])), "post_url", addslashes(trim($_POST[$po_url_key])));
                    if(!$my_db->query($query))
                        exit("<script>alert('완료보고를 처리하지 못하였습니다.\\n문제가 계속 될 경우 탑블로그로 문의바랍니다.\\n(wiseplanners@naver.com , 02-2675-6260)');</script>");
                }
			}
		}


		//기자단 의경우 application 에 보상지급 정보 저장하기
		if(($promotion_info['kind'] == '2' || $promotion_info['kind'] == '4' || $promotion_info['kind'] == '5') && !!$application_info['a_no'] && !!$code){

			$aes_hex = "''";
			if(!empty($_POST['bk_jumin'])) {
				//AES 인코딩
				$aes_hex = "HEX(AES_ENCRYPT('".addslashes(trim($_POST['bk_jumin']))."', '".$aeskey."'))";
			}

			if($application_info['a_no'] != "" && $code != ""){
				$sql="SELECT bk_title,bk_num,bk_name,bk_jumin
						FROM
							application
						WHERE
							p_no = '".$code."' AND
							a_no = '".$application_info['a_no']."'
					";
				$query=mysqli_query($my_db,$sql);
				$application_bk_info=mysqli_fetch_array($query);

				if($application_bk_info['bk_title'] == "" || $application_bk_info['bk_num'] == "" || $application_bk_info['bk_name'] == "" || $application_bk_info['bk_jumin'] == ""){
					$bk_value = substr($bk_value,0,-1); // 끝에 , 자르기
					$query="UPDATE application SET
							$bk_value
							WHERE
								p_no = '".$code."' AND
								a_no = '".$application_info['a_no']."'
							";
					//echo "계좌 저장하기 : ".$query;

					if(!$my_db->query($query))
						exit("<script>alert('계좌 및 주민번호를 저장하지 못하였습니다.\\n문제가 계속 될 경우 탑블로그로 문의바랍니다.\\n(wiseplanners@naver.com , 02-2675-6260)');</script>");
				}
			}
		}

		alert("보고를 완료하였습니다. ^^","report.php?promotion_no=".$code);

	}else{

		if(!$code)
			exit("<script>alert('해당 프로모션이 존재하지 않습니다(1)');</script>");


		// 포스팅보고 통합 및 주소 확인
		$channel_type  = "";
		$post_comment  = "";
		$post_title    = "";
		$init_blog_url = "http://blog.naver.com/";

		switch($promotion_info['channel'])
		{
			case '1':
				$channel_type = "blog";
				$post_comment = "포스팅 주소 예) http://blog.naver.com/블로그아이디/220384743094";
				break;
			case '2':
				$channel_type  = "instagram";
				$post_comment  = "포스팅 주소 예) http://www.instagram.com/p/AAAAAAAAAAA";
				$post_title	   = "인스타그램 입니다.";
                $init_blog_url = "http://www.instagram.com/";
				break;
			case '3':
				$post_comment = "포스팅 주소 예) https://www.facebook.com/hello/posts/12345678 (페이스북 컨텐츠 url은 게시글의 시간을 클릭하시면 됩니다.)";
				$post_title	  = "페이스북 입니다.";
				break;
			case '4':
				$channel_type = "cafe";
				$post_comment = "포스팅 주소 예) http://cafe.naver.com/220384743094";
				break;
			case '7':
				$channel_type  = "influencer";
				$post_comment  = "";
				$init_blog_url = "http://in.naver.com/";
				break;
			case '8':
				$channel_type  = "youtube";
				$post_comment  = "";
				$init_blog_url = "http://www.youtube.com/";
				break;
		}

		$promotion_title = $promotion_info['title'] ? $promotion_info['title'] : $promotion_info['company'];
		$smarty->assign(array(
			"no"			=> $code,
			"s_no"			=> $promotion_info['s_no'],
			"c_no"			=> $promotion_info['c_no'],
			"b_no"			=> "",
			"a_no"			=> $promotion_info['a_no'],
			"p_no"			=> $promotion['p_no'],
			"title"			=> $promotion_title,
			"company"		=> $promotion_info['company'],
			"kind"			=> $promotion_info['kind'],
			"channel"		=> $promotion_info['channel'],
			"pres_reason"	=> $promotion_info['pres_reason'],
			"is_file"		=> $promotion_info['is_file'],
			"channel_type" 	=> $channel_type, // type url 체크
			"post_title" 	=> $post_title, // post_title
			"post_comment" 	=> $post_comment, // post_comment
			"init_blog_url" => $init_blog_url
		));

		$sql="select reg_num, posting_num FROM `promotion` WHERE  p_no='".$code."'";

		$query=mysqli_query($my_db,$sql);
		$data=mysqli_fetch_array($query);

		$smarty->assign("reg_num",$data['reg_num']);
		$smarty->assign("posting_num",$data['posting_num']);

		$input_count = ($data['reg_num'] != 0 && $data['posting_num'] != 0) ? (int)($data['posting_num']/$data['reg_num']) : 1;
		$input_count_list = [];

        if($input_count < 1){
            $input_count = 1;
        }

		for($i=1;$i<=$input_count;$i++){
			$input_count_list[] = $i;
		}

		$smarty->assign("input_count_list", $input_count_list);
		$smarty->assign("input_count", $input_count);

		$smarty->display('report.html');
	}

function query_posting($post_title_f, $post_title_value_f, $post_url_f, $post_url_value_f){

	$query_f = ", $post_title_f = '$post_title_value_f', $post_url_f = '$post_url_value_f'";

	return $query_f;
}

// 포스팅보고 통합 및 주소 확인
function store_image($file_type, $folder)
{
	$sub_folder_name = folderCheck($folder);

	$saveNameList = $_FILES[$file_type]["name"];
	$tmpNameList  = $_FILES[$file_type]["tmp_name"];
	$r_saveName = [];

	$idx = 0;
	foreach($saveNameList as $o_saveName)
	{
		if($o_saveName){
			$tmp_filename = explode(".", $o_saveName);
			if ($tmp_filename[0] != '')
				$md5filename = md5(time().$tmp_filename[0]);
			else
				$md5filename = "";

            $ext = end($tmp_filename);
			$r_saveName[$idx] = $folder. "/" . $sub_folder_name . "/" .$md5filename.".".$ext;

			move_uploaded_file($tmpNameList[$idx], "uploads/". $r_saveName[$idx]);
			$idx++;
		}
	}

	return $r_saveName;
}

?>
