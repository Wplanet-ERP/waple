<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/setup.php');

// 접근 권한
if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자")) {
    $smarty->display('access_company_error.html');
    exit;
}

$sch_page_type = isset($_GET['sch_page_type']) ? $_GET['sch_page_type'] : "org";
$page_type_url = "";

switch($sch_page_type){
    case "org": $page_type_url = "org_setup.php"; break;
}

$smarty->assign("sch_page_type", $sch_page_type);
$smarty->assign("page_type_url", $page_type_url);
$smarty->assign("page_setup_option", getPageSetupOption());

$smarty->display('page_setup.html');
?>
