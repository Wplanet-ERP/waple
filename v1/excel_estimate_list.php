<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work.php');
require('Classes/PHPExcel.php');

# Create new PHPExcel object & Set document properties
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
	->setLastModifiedBy("Maarten Balliauw")
	->setTitle("Office 2007 XLSX Test Document")
	->setSubject("Office 2007 XLSX Test Document")
	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	->setKeywords("office 2007 openxml php")
	->setCategory("Test result file")
;

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where		= "1=1";
$sch_est_wp_no	= isset($_GET['sch_est_wp_no']) ? $_GET['sch_est_wp_no'] : "";

if(!empty($sch_est_wp_no)) { // est_wp_no
	$add_where .= " AND est_wp_no='{$sch_est_wp_no}'";
}

#  리스트 쿼리
$estimate_work_package_sql = "
	SELECT
		(SELECT dp.dp_date FROM deposit dp WHERE dp.dp_no=ewp.dp_no) AS dp_date,
		(SELECT dp.incentive_date FROM deposit dp WHERE dp.dp_no=ewp.dp_no) AS incentive_date,
		(SELECT dp.dp_money FROM deposit dp WHERE dp.dp_no=ewp.dp_no) AS dp_money,
		c_name,
		subject,
		(SELECT SUM(ew.price) FROM estimate_work ew WHERE ew.est_wp_no=ewp.est_wp_no) AS total_price
	FROM estimate_work_package ewp
	WHERE {$add_where}
";

$estimate_work_package_query = mysqli_query($my_db, $estimate_work_package_sql);
while($estimate_work_package_array = mysqli_fetch_array($estimate_work_package_query))
{
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('C1', "입금 완료일 (정산 년/월)")
	->setCellValue('C2', "입금액")
	->setCellValue('C3', "업체명")
	->setCellValue('C4', "견적서명")
	->setCellValue('C5', "총 판매가(VAT별도)");
	$objPHPExcel->getActiveSheet()->getStyle('C1:C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('D1', $estimate_work_package_array['dp_date'].' ('.$estimate_work_package_array['incentive_date'].')')
	->setCellValue('D2', number_format($estimate_work_package_array['dp_money']))
	->setCellValue('D3', $estimate_work_package_array['c_name'])
	->setCellValue('D4', $estimate_work_package_array['subject'])
	->setCellValue('D5', number_format($estimate_work_package_array['total_price']));
}

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A6', "w_no")
	->setCellValue('B6', "진행상태")
	->setCellValue('C6', "상품 그룹1")
	->setCellValue('D6', "상품 그룹2")
	->setCellValue('E6', "상품명")
	->setCellValue('F6', "타겟키워드 및 수량")
	->setCellValue('G6', "판매가(VAT별도)")
	->setCellValue('H6', "예상외주비(VAT별도)")
	->setCellValue('I6', "실제외주비(VAT별도)")
	->setCellValue('J6', "마케터메모")
;

# 견적서 업무 리스트 쿼리
$estimate_work_sql = "
	SELECT
		est_w_no,
		est_wp_no,
		w_no,
		(SELECT work_state FROM work w WHERE w.w_no=ew.w_no) AS work_state,
		(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=ew.prd_no))) AS k_prd1,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=ew.prd_no))) AS k_prd1_name,
		(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=ew.prd_no)) AS k_prd2,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=ew.prd_no)) AS k_prd2_name,
		prd_no,
		(SELECT title from product prd where prd.prd_no=ew.prd_no) as prd_no_name,
		t_keyword_or_cnt,
		(SELECT t_keyword FROM work w WHERE w.w_no=ew.w_no) AS wrok_t_keyword,
		price,
		out_price,
		(SELECT price FROM work w WHERE w.w_no=ew.w_no) AS work_price,
		memo
	FROM estimate_work ew
	WHERE {$add_where}
	ORDER BY k_prd1 ASC, k_prd2 ASC, prd_no ASC
";
$estimate_work_query = mysqli_query($my_db, $estimate_work_sql);
$work_state_option	 = getWorkStateOption();
$idx				 = 7;
while($estimate_work_array = mysqli_fetch_array($estimate_work_query))
{
	$price_value		= !empty($estimate_work_array['price']) ? number_format($estimate_work_array['price']) : "";
	$out_price_value	= !empty($estimate_work_array['out_price']) ? number_format($estimate_work_array['out_price']) : "";
	$work_price_value	= !empty($estimate_work_array['w_no']) ? number_format($estimate_work_array['work_price']) : "";


	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue("A{$idx}", $estimate_work_array['w_no'])
		->setCellValue("B{$idx}", $work_state_option[$estimate_work_array['work_state']])
		->setCellValue("C{$idx}", $estimate_work_array['k_prd1_name'])
		->setCellValue("D{$idx}", $estimate_work_array['k_prd2_name'])
		->setCellValue("E{$idx}", $estimate_work_array['prd_no_name'])
		->setCellValue("F{$idx}", $estimate_work_array['t_keyword_or_cnt'])
		->setCellValue("G{$idx}", $price_value)
		->setCellValue("H{$idx}", $out_price_value)
		->setCellValue("I{$idx}", $work_price_value)
		->setCellValue("J{$idx}", $estimate_work_array['memo']);


	$idx++;
}
$idx--;

$objPHPExcel->getActiveSheet()->getStyle("A6:J{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:J{$idx}")->getFont()->setName("맑은 고딕");
$objPHPExcel->getActiveSheet()->getStyle("A1:J6")->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle("A7:J{$idx}")->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle("A6:J6")->getFont()->setSize(11);
$objPHPExcel->getActiveSheet()->getStyle("A6:J6")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB("00c4bd97");
$objPHPExcel->getActiveSheet()->getStyle("A6:J6")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A6:J{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("A7:A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("B7:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("C7:C{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("D7:D{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("E7:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("F7:F{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("G7:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("H7:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("I7:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle("J7:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$objPHPExcel->getActiveSheet()->getStyle("F7:F{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("J7:J{$idx}")->getAlignment()->setWrapText(true);

$i2_length = $idx;
$i2=1;

while($i2_length){
	if($i2 > 2)
		$objPHPExcel->getActiveSheet()->getRowDimension($i2)->setRowHeight(20);

	if($i2 > 6 && $i2 % 2 == 1)
		$objPHPExcel->getActiveSheet()->getStyle("A{$i2}:J{$i2}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB("00ebf1de");

	$i2++;
	$i2_length--;
}

$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension("F")->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension("G")->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension("H")->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension("I")->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension("J")->setWidth(35);

$objPHPExcel->getActiveSheet()->setTitle("견적서 리스트");
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename = iconv('UTF-8','EUC-KR',date("Ymd")."_".$session_name."_견적서 리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>
