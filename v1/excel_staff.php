<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/staff.php');
require('Classes/PHPExcel.php');

# Create new PHPExcel object & SET document
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
	->setLastModifiedBy("Maarten Balliauw")
	->setTitle("Office 2007 XLSX Test Document")
	->setSubject("Office 2007 XLSX Test Document")
	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	->setKeywords("office 2007 openxml php")
	->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

# 상단타이틀 설정
$work_sheet			= $objPHPExcel->setActiveSheetIndex(0);
$header_title_list  = array("s_no", "활동상태", "ID", "계열사", "부서 > 팀", "이름", "직책", "이메일", "유선", "내선", "핸드폰", "생일", "입사일", "강점키워드");
$head_idx           = 0;
$row_idx           	= 3;
foreach(range('A', 'N') as $columnID){
	$work_sheet->setCellValue("{$columnID}{$row_idx}", $header_title_list[$head_idx]);
	$head_idx++;
}
$row_idx++;

# 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where			= "1=1";
$sch_staff_state	= isset($_GET['sch_staff_state'])?$_GET['sch_staff_state']:"1";
$sch_permission		= isset($_GET['sch_permission'])?$_GET['sch_permission']:"";
$sch_team		 	= isset($_GET['sch_team']) && ($_GET['sch_team'] != 'all') ? $_GET['sch_team']:"";
$sch_s_name		 	= isset($_GET['sch_s_name'])?$_GET['sch_s_name']:"";
$sch_s_keyword	 	= isset($_GET['sch_s_keyword'])?$_GET['sch_s_keyword']:"";
$sch_tel	 		= isset($_GET['sch_tel'])?$_GET['sch_tel']:"";

if(!empty($sch_staff_state)) {
    $add_where .= " AND s.staff_state='{$sch_staff_state}'";
}

if(!empty($sch_permission)) {
    $add_where_permission = str_replace("0", "_", $sch_permission);
    $add_where .= " AND s.permission like '{$add_where_permission}'";
}

if (!empty($sch_team))
{
    $sch_team_code_where = getTeamWhere($my_db, $sch_team);
    if($sch_team_code_where){
        $add_where .= " AND `s`.team IN ({$sch_team_code_where})";
    }
}

if(!empty($sch_s_name)) {
    $add_where .= " AND s.s_name LIKE '%{$sch_s_name}%'";
}

if(!empty($sch_s_keyword)) {
    $add_where .= " AND (SELECT GROUP_CONCAT(ss.keyword SEPARATOR ' ∙ ') FROM staff_strength AS ss WHERE ss.s_no = s.s_no GROUP BY ss.s_no) LIKE '%{$sch_s_keyword}%'";
}

if(!empty($sch_tel)) {
	$add_where .= " AND (tel LIKE '%{$sch_tel}%' OR hp LIKE '%{$sch_tel}%')";
}

# 리스트 쿼리
$staff_sql = "
	SELECT
		s.s_no,
		s.staff_state,
		s.id,
		(SELECT my_c.c_name FROM my_company my_c WHERE my_c.my_c_no=s.my_c_no) AS my_c_name,
		(SELECT t2.team_name FROM team t2 WHERE t2.team_code=(SELECT t.team_code_parent FROM team t WHERE t.team_code=team)) AS team_parent_name,
		(SELECT t.team_name FROM team t WHERE t.team_code=team) AS team_name,
		s.s_name,
		s.position,
		s.email,
		s.tel,
		s.hp,
		s.extension,
		s.permission,
		s.employment_date,
		s.birthday,
		(SELECT GROUP_CONCAT(ss.keyword SEPARATOR ' ∙ ') FROM staff_strength AS ss WHERE ss.s_no = s.s_no GROUP BY ss.s_no ORDER BY ss.priority ASC) as strength_keyword
	FROM staff s
	WHERE {$add_where}
	ORDER BY s_no desc
";
$staff_query 		= mysqli_query($my_db, $staff_sql);
$staff_state_option = getStaffStateOption();
while ($staff_array = mysqli_fetch_array($staff_query))
{
	$team_name = !empty($staff_array['team_parent_name']) ? $staff_array['team_parent_name'].' > '.$staff_array['team_name'] : $staff_array['team_name'];

	$work_sheet
		->setCellValue("A{$row_idx}", $staff_array['s_no'])
		->setCellValue("B{$row_idx}", $staff_state_option[$staff_array['staff_state']])
		->setCellValue("C{$row_idx}", $staff_array['id'])
		->setCellValue("D{$row_idx}", $staff_array['my_c_name'])
		->setCellValue("E{$row_idx}", $team_name)
		->setCellValue("F{$row_idx}", $staff_array['s_name'])
		->setCellValue("G{$row_idx}", $staff_array['position'])
		->setCellValue("H{$row_idx}", $staff_array['email'])
		->setCellValue("I{$row_idx}", $staff_array['tel'])
		->setCellValue("J{$row_idx}", $staff_array['extension'])
		->setCellValue("K{$row_idx}", $staff_array['hp'])
		->setCellValue("L{$row_idx}", $staff_array['birthday'])
		->setCellValue("M{$row_idx}", $staff_array['employment_date'])
		->setCellValue("N{$row_idx}", $staff_array['strength_keyword'])
	;

	$row_idx++;
}
$row_idx--;

$work_sheet->mergeCells("A1:N1");
$work_sheet->setCellValue("A1", "계정 리스트");
$work_sheet->getStyle("A1:N1")->getFont()->setSize(20);
$work_sheet->getStyle("A1:N1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$work_sheet->getStyle("A3:N{$row_idx}")->applyFromArray($styleArray);
$work_sheet->getStyle("A1:N{$row_idx}")->getFont()->setName('맑은 고딕');
$work_sheet->getStyle("A1:N3")->getFont()->setBold(true);
$work_sheet->getStyle("A3:N3")->getFont()->setSize(11);
$work_sheet->getStyle("A4:N{$row_idx}")->getFont()->setSize(9);
$work_sheet->getStyle('A3:N3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00c4bd97');

$work_sheet->getStyle("A3:N{$row_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$work_sheet->getStyle("A3:N{$row_idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$work_sheet->getStyle("C4:C{$row_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$work_sheet->getStyle("D4:D{$row_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$work_sheet->getStyle("E4:E{$row_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$work_sheet->getStyle("H4:H{$row_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$work_sheet->getStyle("N4:N{$row_idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$i2_length 	= $row_idx;
$i2			= 3;
while($i2 <= $i2_length)
{
	$work_sheet->getRowDimension($i2)->setRowHeight(20);
	if($i2 > 3 && $i2 % 2 == 1)
		$work_sheet->getStyle('A'.$i2.':L'.$i2)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ebf1de');

	$i2++;
}

# Rename worksheet  set the width autosize
$work_sheet->getColumnDimension('A')->setAutoSize(true);
$work_sheet->getColumnDimension('B')->setAutoSize(true);
$work_sheet->getColumnDimension('C')->setAutoSize(true);
$work_sheet->getColumnDimension('D')->setWidth(20);
$work_sheet->getColumnDimension('E')->setWidth(30);
$work_sheet->getColumnDimension('F')->setWidth(10);
$work_sheet->getColumnDimension('G')->setWidth(10);
$work_sheet->getColumnDimension('J')->setWidth(10);
$work_sheet->getColumnDimension('H')->setAutoSize(true);
$work_sheet->getColumnDimension('I')->setAutoSize(true);
$work_sheet->getColumnDimension('K')->setAutoSize(true);
$work_sheet->getColumnDimension('M')->setWidth(40);
$work_sheet->getColumnDimension('K')->setAutoSize(true);
$work_sheet->getColumnDimension('M')->setAutoSize(true);
$work_sheet->getColumnDimension('M')->setAutoSize(true);
$work_sheet->getColumnDimension('N')->setAutoSize(true);
$work_sheet->getColumnDimension('O')->setAutoSize(true);
$work_sheet->getColumnDimension('P')->setAutoSize(true);
$work_sheet->getColumnDimension('Q')->setAutoSize(true);
$work_sheet->setTitle('계정 리스트');

$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_".$session_name."_계정 리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
