<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
include_once('inc/model/Company.php');
include_once('inc/model/Agency.php');

# 파일 변수
$agency             = (isset($_POST['settle_type']))?$_POST['settle_type'] : "4004";
$settle_month       = (isset($_POST['settle_month']))?$_POST['settle_month'] : date('Y-m-d', strtotime('-1 months'));
$settle_month_val   = date("Ym",strtotime($settle_month));
$file_name          = $_FILES["settle_file"]["tmp_name"];

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$agency_model  = Agency::Factory();
$company_model = Company::Factory();
$ins_data   = [];
$regdate    = date('Y-m-d H:i:s');
$last_idx   = $agency_model->getLastGroupNo($settle_month);
$start_i    = ($agency == '4004') ? 2 : 4;

for ($i = $start_i; $i <= $totalRow; $i++)
{
    #변수 초기화
    $company = $media_val = $media = $prd_name = $prd_detail = $advertiser_name = $advertiser_id = $type = "";
    $supply_price = $supply_fee = $supply_fee_per = 0;
    $partner_val = $my_c_no = $partner = $partner_name = $manager = $manager_team = $run_s_no = $run_team = $run_memo = "";

    $agency_data     = [];

    $group_no = $settle_month_val."_agency_".sprintf('%04d', $last_idx);
    $last_idx++;

    if($agency == '4004')   //하나애드
    {
        $company        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 업체
        $media_val      = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 광고매체
        $prd_name       = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 상품명
        $advertiser_name= (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 광고주명
        $advertiser_id  = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 광고주 KEY
        $supply_price   = (double)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 매출(공급가액)
        $supply_fee     = (double)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 수수료(공급가액)
        $supply_fee_per = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 수수료(%)
        $partner_val    = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 업체들
    }
    elseif($agency == '1858')   //DMC미디어
    {
        $company        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 업체
        $advertiser_name= (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 광고주명
        $advertiser_id  = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 광고주 KEY
        $media_val      = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 광고매체
        $prd_name       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 상품명
        $prd_detail1    = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 상품상세1
        $prd_detail2    = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 상품상세2
        $prd_detail     = !empty($prd_detail1) ? $prd_detail1."<br/>".$prd_detail2 : $prd_detail2;  // 상품상세
        $supply_price   = (double)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 매출(공급가액)
        $supply_fee_per = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 수수료(%)
        $supply_fee     = (double)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   // 수수료(공급가액)
        $partner_val    = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   // 업체들
    }
    elseif($agency == '3735')   //모비데이즈
    {
        $company        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 업체
        $advertiser_name= (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 광고주명
        $advertiser_id  = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 광고주 KEY
        $media_val      = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 광고매체
        $prd_name       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 상품명
        $prd_detail1    = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 상품상세1
        $prd_detail2    = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 상품상세2
        $prd_detail3    = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 상품상세2
        $prd_detail     = !empty($prd_detail1) ? $prd_detail1."~".$prd_detail2 : $prd_detail2;    // 상품상세 작업 1
        $prd_detail     = !empty($prd_detail) ? $prd_detail."<br/>".$prd_detail3 : $prd_detail3;   // 상품상세 작업 2
        $supply_price   = (double)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 매출(공급가액)
        $supply_fee_per = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   // 수수료(%)
        $supply_fee     = (double)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   // 수수료(공급가액)
        $partner_val    = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));   // 업체들
    }
    elseif($agency == '4355')   //메조미디어
    {
        $company        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 업체
        $advertiser_name= (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 광고주명
        $advertiser_id  = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 광고주 KEY
        $media_val      = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 광고매체
        $prd_name       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 상품명
        $prd_detail1    = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 상품상세1
        $prd_detail2    = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 상품상세2
        $prd_detail     = !empty($prd_detail1) ? $prd_detail1."<br/>".$prd_detail2 : $prd_detail2;  // 상품상세
        $supply_price   = (double)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 매출(공급가액)
        $supply_fee_per = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 수수료(%)
        $supply_fee     = (double)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   // 수수료(공급가액)
        $partner_val    = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   // 업체들
    }
    elseif($agency == '528')   //페이스북
    {
        $company        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 업체
        $advertiser_name= (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 광고주명
        $advertiser_id  = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 광고주 KEY
        $media_val      = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 광고매체
        $prd_name       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 상품명
        $prd_detail1    = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 상품상세1
        $prd_detail2    = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 상품상세2
        $prd_detail3    = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 상품상세2
        $prd_detail     = !empty($prd_detail1) ? $prd_detail1."~".$prd_detail2 : $prd_detail2;    // 상품상세 작업 1
        $prd_detail     = !empty($prd_detail) ? $prd_detail."<br/>".$prd_detail3 : $prd_detail3;   // 상품상세 작업 2
        $supply_price   = (double)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 매출(공급가액)
        $supply_fee_per = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   // 수수료(%)
        $supply_fee     = (double)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   // 수수료(공급가액)
        $partner_val    = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));   // 업체들
    }
    elseif($agency == '4472')   //나스미디어
    {
        $company        = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 업체
        $advertiser_name= (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 광고주명
        $advertiser_id  = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 광고주 KEY
        $media_val      = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 광고매체
        $prd_name       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 상품명
        $prd_detail1    = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 상품상세1
        $prd_detail2    = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 상품상세2
        $prd_detail3    = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 상품상세2
        $prd_detail     = !empty($prd_detail1) ? $prd_detail1."~".$prd_detail2 : $prd_detail2;    // 상품상세 작업 1
        $prd_detail     = !empty($prd_detail) ? $prd_detail."<br/>".$prd_detail3 : $prd_detail3;   // 상품상세 작업 2
        $supply_price   = (double)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 매출(공급가액)
        $supply_fee_per = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   // 수수료(%)
        $supply_fee     = (double)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   // 수수료(공급가액)
        $partner_val    = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));   // 업체들
    }

    $supply_price   = round($supply_price,0);
    $supply_fee     = round($supply_fee,0);

    $prd_detail_val = str_replace("<br/>", "\r\n", $prd_detail);
    $run_memo       = "광고매체::{$media}\r\n상품명::{$prd_name}\r\n상품상세::{$prd_detail_val}";

    if(!empty($media_val))
    {
        switch($media_val)
        {
            case '네이버(검색광고)':
                $media  = 263;
                $type   = 'naver';
                break;
            case '네이버(NOSP)':
                $media  = 265;
                $type   = 'naver_nosp';
                break;
            case '네이버(GFA)':
                $media  = 266;
                $type   = 'naver_gfa';
                break;
            case '카카오':
                $media  = 267;
                $type   = 'kakao';
                break;
            case '구글':
                $media  = 268;
                $type   = 'google';
                break;
            case 'Facebook':
                $media = 269;
                break;
            case '토스':
                $media = 282;
                break;
        }
    }

    if(empty($company)){
        break;
    }

    if(empty($media)){
        exit("<script>alert('광고매체가 매칭되지 않습니다. Row : {$i}, 광고매체: {$media_val}');location.href='agency_settlement.php';</script>");
    }

    $agency_data = array(
        'status'            => '1',
        'agency'            => $agency,
        'settle_month'      => $settle_month,
        'group_no'          => $group_no,
        'company'           => $company,
        'media'             => $media,
        'prd_name'          => $prd_name,
        'prd_detail'        => $prd_detail,
        'advertiser_name'   => $advertiser_name,
        'advertiser_id'     => $advertiser_id,
        'supply_price'      => $supply_price,
        'supply_fee'        => $supply_fee,
        'supply_fee_per'    => $supply_fee_per,
        'wd_price'          => $supply_price,
        'dp_price'          => $supply_fee,
        'run_s_no'          => $session_s_no,
        'run_team'          => $session_team,
        'run_memo'          => $run_memo,
        'regdate'           => $regdate
    );

    if(!empty($partner_val))
    {
        $partner_list   = explode(',', $partner_val);
        $partner_count  = count($partner_list);

        $total_dp_price = $supply_fee;
        $total_wd_price = $supply_price;
        $unit_dp_price  = (int)($total_dp_price/$partner_count);
        $unit_wd_price  = (int)($total_wd_price/$partner_count);

        $cal_wd_price   = 0;
        $cal_dp_price   = 0;
        $unit_idx       = 1;

        foreach($partner_list as $partner)
        {
            $agency_data_tmp = $agency_data;
            $partner_no      = trim($partner);
            $comp_item       = $company_model->getItem($partner_no);

            $agency_data_tmp['my_c_no']         = $comp_item['my_c_no'];
            $agency_data_tmp['partner']         = $comp_item['c_no'];
            $agency_data_tmp['partner_name']    = $comp_item['c_name'];
            $agency_data_tmp['manager']         = $comp_item['s_no'];
            $agency_data_tmp['manager_team']    = $comp_item['team'];

            $cal_wd_price += $unit_wd_price;
            $cal_dp_price += $unit_dp_price;
            $chk_wd_price  = 0;
            $chk_dp_price  = 0;

            if($unit_idx == $partner_count)
            {
                if($cal_wd_price != $total_wd_price){
                    $chk_wd_price = $total_wd_price - $cal_wd_price;
                }

                if($cal_dp_price != $total_dp_price){
                    $chk_dp_price = $total_dp_price - $cal_dp_price;
                }
            }

            $agency_data_tmp['wd_price'] = $unit_wd_price+$chk_wd_price;
            $agency_data_tmp['dp_price'] = $unit_dp_price+$chk_dp_price;

            $ins_data[] = $agency_data_tmp;

            $unit_idx++;
        }
    }
    else
    {
        if(!empty($type))
        {
            $comp_item = $company_model->getAgencyTypeItem($type, $advertiser_id);

            if(!empty($comp_item))
            {
                $agency_data['my_c_no']         = $comp_item['my_c_no'];
                $agency_data['partner']         = $comp_item['c_no'];
                $agency_data['partner_name']    = $comp_item['c_name'];
                $agency_data['manager']         = $comp_item['s_no'];
                $agency_data['manager_team']    = $comp_item['team'];
            }
        }

        $ins_data[] = $agency_data;
    }
}

if (!$agency_model->multiInsert($ins_data)){
    echo "대행수수로 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    exit;
}else{
    exit("<script>alert('대행수수로가 반영 되었습니다.');location.href='agency_settlement.php';</script>");
}

?>
