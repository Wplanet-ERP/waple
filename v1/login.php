<?php
require('inc/common.php');
require('inc/model/MyCompany.php');

if (trim($_SESSION["ss_s_no"])) {
	goto_url("main.php");
}elseif (trim($_SESSION["ss_c_no"])) {
	goto_url("out_company/main.php");
}

if ($user_id && $pw)
{
	sleep(1);

	$convert_pwd	= substr(md5($pw), 8, 16);
	$staff_sql 		= "SELECT s_no, id, s_name, team, (SELECT team_code_parent FROM team WHERE team_code=team) AS team_parent, `position`, pos_permission, permission, staff_state, my_c_type, my_c_no FROM staff WHERE id = '{$user_id}' AND `pass` = '{$convert_pwd}' AND staff_state = '1' AND my_c_type='1'";
	$staff_query 	= mysqli_query($my_db, $staff_sql);
	$staff_info 	= mysqli_fetch_array($staff_query);

	if($staff_info['id'])
	{
		$_SESSION["ss_my_c_no"] 		= $staff_info['my_c_no'];
		$_SESSION["ss_s_no"] 			= $staff_info['s_no'];
		$_SESSION["ss_id"] 				= $staff_info['id'];
		$_SESSION["ss_name"] 			= $staff_info['s_name'];
		$_SESSION["ss_team"] 			= $staff_info['team'];
		$_SESSION["ss_team_parent"] 	= $staff_info['team_parent'];
		$_SESSION["ss_position"] 		= $staff_info['position'];
		$_SESSION["ss_pos_permission"] 	= $staff_info['pos_permission'];
		$_SESSION["ss_permission"] 		= $staff_info['permission'];
		$_SESSION["ss_staff_state"] 	= $staff_info['staff_state'];
		$_SESSION["ss_my_c_type"] 		= $staff_info['my_c_type'];
		$_SESSION["ss_first_login"]		= "1";

		if($pw == "!1234!"){
			alert("[보안경고] 비밀번호가 기본으로 설정되어 있습니다. 비밀번호를 변경 후 이용해주세요. (비밀번호 변경방법 : 조직관리 > 계정 리스트 페이지에서 본인 이름 클릭)", "staff_regist.php?s_no={$staff_info['s_no']}");
		}
		else
		{
			$chk_holiday	= date("Y-m-d");
			$chk_s_holiday  = $chk_holiday." 00:00:00";
			$chk_e_holiday	= $chk_holiday." 23:59:59";

			# 연차촉진체크
			$chk_holiday_sql	= "SELECT COUNT(*) as cal_cnt, (SELECT COUNT(*) FROM calendar_schedule_comment css WHERE css.cs_no=`cs`.cs_no) as comm_cnt FROM calendar_schedule `cs` WHERE `cs`.cs_s_date BETWEEN '{$chk_s_holiday}' AND '{$chk_e_holiday}' AND cal_id='hr_boost' ANd cal_no='7' AND `cs`.cs_s_no='{$staff_info['s_no']}'";
			$chk_holiday_query	= mysqli_query($my_db, $chk_holiday_sql);
			$chk_holiday_result	= mysqli_fetch_assoc($chk_holiday_query);

			if($chk_holiday_result['cal_cnt'] > 0 && $chk_holiday_result['comm_cnt'] == 0){
				goto_url("main.php");
			}

            $referer_url = isset($_POST['referer_url']) ? $_POST['referer_url'] : "";
            if(!!$referer_url){
                goto_url($referer_url);
            }else{
                goto_url("main.php");
            }
			goto_url("main.php");
		}
	}else{
		alert("정확한 정보를 입력하여주세요!","login.php");
	}
}

$my_company_model 		= MyCompany::Factory();
$my_company_name_list 	= $my_company_model->getList();

$smarty->assign('referer_url', $_SERVER['HTTP_REFERER']);
$smarty->assign('my_company_name_list', $my_company_name_list);
$smarty->assign("nobg","style='background:none'");

$smarty->display('login.html');
?>
