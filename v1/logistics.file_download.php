<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');

$lm_no    = isset($_GET['lm_no']) ? $_GET['lm_no'] : "";

// 업무번호 및 타입 확인
if(empty($lm_no)){
    $smarty->display('access_company_error.html');
    exit;
}


$logistics_sql 	 = "SELECT quick_req_file_name as file_name, quick_req_file_path as file_path FROM `logistics_management` WHERE lm_no = {$lm_no} LIMIT 1";
$logistics_query  = mysqli_query($my_db, $logistics_sql);
$logistics 		 = mysqli_fetch_assoc($logistics_query);

$zip_file_name  = "";
$zip_file_path  = "";
$logistics_file_name = $logistics['file_name'];
$logistics_file_path = $logistics['file_path'];
$logistics_file_list = [];
if(!empty($logistics_file_name) && !empty($logistics_file_path))
{
    $file_names = explode(',', $logistics_file_name);
    $file_paths = explode(',', $logistics_file_path);

    $zip_file_name   = "logistics_{$lm_no}_file.zip";
    $zip_file_path   = "zip_tmp/{$zip_file_name}";
    $zip_file_origin = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$zip_file_path}";

    if(file_exists($zip_file_origin)){
        del_file($zip_file_path);
    }

    $zip = new ZipArchive;
    $res = $zip->open($zip_file_origin, ZipArchive::CREATE);

    foreach($file_paths as $idx => $file_path)
    {
        $file_origin = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/{$file_path}";
        $file_size   = 0;
        $file_name   = isset($file_names[$idx]) ? $file_names[$idx] : "";
        if(file_exists($file_origin)){
            $file_size = filesize($file_origin)/1024;
            $file_size = floor($file_size);

             if ($res === TRUE) {
                 $zip->addFile($file_origin, $file_name);
             }

        }else{
            continue;
        }

        $logistics_file_list[] = array(
            "file_path" => $file_path,
            "file_name" => $file_name,
            "file_size" => $file_size." KB"
        );
    }

    if ($res === TRUE) {
        $zip->close();
    }

}else{
    $file_names = [];
    $file_paths = [];
}

$total_count = empty($logistics_file_list) ? 0 : count($logistics_file_list);

$smarty->assign("file_count", $total_count);
$smarty->assign("lm_no", $lm_no);
$smarty->assign("logistics_file_list", $logistics_file_list);
$smarty->assign("zip_file_name", $zip_file_name);
$smarty->assign("zip_file_path", $zip_file_path);

$smarty->display('logistics.file_download.html');
?>
