<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/product.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "prd_no")
	->setCellValue('B3', "display")
	->setCellValue('C3', "상품그룹1")
	->setCellValue('D3', "상품그룹2")
	->setCellValue('E3', "상품명")
	->setCellValue('F3', "우선순위")
	->setCellValue('G3', "상품소개")
	->setCellValue('H3', "작업요청(기본양식)")
	->setCellValue('I3', "작업대상")
	->setCellValue('J3', "work time(적용방법)")
	->setCellValue('K3', "work time(기본값)");


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where          = "1=1";
$sch_display_get    = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$sch_prd_g1_get     = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2_get     = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd_name_get   = isset($_GET['sch_prd_name'])?$_GET['sch_prd_name']:"";

if (!empty($sch_display_get)) {
    $add_where .= " AND display='" . $sch_display_get . "'";
}

if (!empty($sch_prd_g1_get)) {
    $add_where .= " AND k_prd1='" . $sch_prd_g1_get . "'";
}

if (!empty($sch_prd_g2_get)) {
    $add_where .= " AND k_prd2='" . $sch_prd_g2_get . "'";
}

if(!empty($sch_prd_name_get)) {
    $add_where.=" AND title like '%{$sch_prd_name_get}%'";
}

$add_orderby ="prd_no DESC";

// 페이에 따른 limit 설정
$pages 	= isset($_GET['page']) ?intval($_GET['page']) : 1;
$offset = ($pages-1) * $num;

// 리스트 쿼리
$product_sql = "
	SELECT * FROM
		(SELECT
			prd.prd_no,
			prd.display,
			(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code)) AS k_prd1,
			(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=prd.k_name_code)) AS k_prd1_name,
			(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=prd.k_name_code) AS k_prd2,
			(SELECT k_name FROM kind k WHERE k.k_name_code=prd.k_name_code) AS k_prd2_name,
			prd.title,
			prd.priority,
			prd.description,
			prd.work_format,
			prd.wd_dp_state,
			prd.work_time_method,
			prd.work_time_default
		FROM product prd
	) AS sql_result
	WHERE {$add_where}
	ORDER BY {$add_orderby}
";

$product_query = mysqli_query($my_db, $product_sql);
$sch_wd_dp_state_option = getWdDpStateOption();
for ($i = 4; $product_array = mysqli_fetch_array($product_query); $i++)
{
	$display_value = "";
	if(!!$product_array['display'])
		if($product_array['display'] == '1'){
			$display_value = "ON";
		}elseif($product_array['display'] == '2'){
			$display_value = "OFF";
		}

	$wd_dp_state_value = "";
	if(!!$product_array['wd_dp_state'])
		$wd_dp_state_value = $sch_wd_dp_state_option[$product_array['wd_dp_state']];


	$work_time_method_value = "";
	if(!!$product_array['work_time_method'])
		if($product_array['work_time_method'] == '0'){
			$work_time_method_value = "미사용";
		}elseif($product_array['work_time_method'] == '1'){
			$work_time_method_value = "사용(기본값 변경불가)";
		}elseif($product_array['work_time_method'] == '2'){
			$work_time_method_value = "사용(기본값 변경가능)";
		}

	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $product_array['prd_no'])
		->setCellValue('B'.$i, $display_value)
		->setCellValue('C'.$i, $product_array['k_prd1_name'])
		->setCellValue('D'.$i, $product_array['k_prd2_name'])
		->setCellValue('E'.$i, $product_array['title'])
		->setCellValue('F'.$i, $product_array['priority'])
		->setCellValue('G'.$i, $product_array['description'])
		->setCellValue('H'.$i, $product_array['work_format'])
		->setCellValue('I'.$i, $wd_dp_state_value)
		->setCellValue('J'.$i, $work_time_method_value)
		->setCellValue('K'.$i, $product_array['work_time_default']);
}

if($i > 1)
	$i = $i-1;

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:K1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "상품 리스트");
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFont()->setSize(20);
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


$objPHPExcel->getActiveSheet()->getStyle('A3:K'.$i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A1:K'.$i)->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:K3')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->getStyle('A4:K'.$i)->getFont()->setSize(9);
$objPHPExcel->getActiveSheet()->getStyle('A3:K3')->getFont()->setSize(11);

$objPHPExcel->getActiveSheet()->getStyle('A3:K3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
															->getStartColor()->setARGB('00c4bd97');
$objPHPExcel->getActiveSheet()->getStyle('A3:K3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:K'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('I4:I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


$i2_length = $i;
$i2=1;

while($i2_length){
	if($i2 > 2)
		$objPHPExcel->getActiveSheet()->getRowDimension($i2)->setRowHeight(20);

	if($i2 > 3 && $i2 % 2 == 1)
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i2.':K'.$i2)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ebf1de');

	$i2++;
	$i2_length--;
}

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);


$objPHPExcel->getActiveSheet()->setTitle('상품 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);



$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_".$session_name."_상품 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
