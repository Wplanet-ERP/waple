<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/work_cms.php');
require('inc/helper/wise_bm.php');
require('inc/model/Custom.php');

# 기본 변수
$url_model          = Custom::Factory();
$url_model->setMainInit("commerce_url", "url_no");

$dp_self_imweb_list     = getSelfDpImwebCompanyList();
$dp_self_imweb_text     = implode(",", $dp_self_imweb_list);

# 마지막 날짜
$max_convert_date_sql     = "SELECT convert_date FROM commerce_conversion WHERE dp_c_no > 0 AND brand > 0 ORDER BY convert_date DESC LIMIT 1";
$max_convert_date_query   = mysqli_query($my_db, $max_convert_date_sql);
$max_convert_date_result  = mysqli_fetch_assoc($max_convert_date_query);
$max_convert_date         = isset($max_convert_date_result['convert_date']) ? date("Y-m-d", strtotime($max_convert_date_result['convert_date'])) : date('Y-m-d', strtotime("-3 days"));

# 검색조건
$add_where          = "brand > 0 AND dp_c_no > 0";
$add_cms_where      = "delivery_state = '4' AND w.unit_price > 0 AND w.page_idx > 0 AND dp_c_no IN({$dp_self_imweb_text})";
$add_nosp_where     = "`am`.state IN(3,5) AND `am`.product IN(1,2)";
$prev_val           = date('Y-m-d', strtotime("{$max_convert_date} -13 days"));
$sch_traffic_type   = isset($_GET['sch_traffic_type']) ? $_GET['sch_traffic_type'] : "day";
$sch_convert_s_date = isset($_GET['sch_convert_s_date']) ? $_GET['sch_convert_s_date'] : $prev_val;
$sch_convert_e_date = isset($_GET['sch_convert_e_date']) ? $_GET['sch_convert_e_date'] : $max_convert_date;
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_dp_company     = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";
$sch_url            = isset($_GET['sch_url']) ? $_GET['sch_url'] : "";
$sch_brand_url      = "";
$chart_base_type    = getBaseChartType();
$chart_brand_type   = getTotalBrandChartType();
$chart_doc_type     = getDocBrandChartType();
$chart_empty_type   = getEmptyChartType();

if(empty($sch_convert_s_date) || empty($sch_convert_e_date)) {
    echo "날짜 검색 값이 없으면 차트 생성이 안됩니다.";
    exit;
}

if($sch_convert_e_date > $max_convert_date){
    $sch_convert_e_date = $max_convert_date;
}

$convert_total_chart_style_list = $chart_base_type;

if(!empty($sch_brand_g1)){
    $sch_brand_url = "sch_brand_g1={$sch_brand_g1}";

    switch ($sch_brand_g1){
        case '70001':   # 닥터피엘
            $convert_total_chart_style_list = $chart_brand_type["doc"];
            break;
        case '70002':   # 아이레놀
            $convert_total_chart_style_list = $chart_brand_type["ilenol"];
            break;
        case '70003':   # 누잠
            $convert_total_chart_style_list = $chart_brand_type["nuzam"];
            break;
        case '70004':   # 시티파이
            $convert_total_chart_style_list = $chart_brand_type["city"];
            break;
        default:
            $convert_total_chart_style_list = $chart_empty_type;
            break;
    }
}

if(!empty($sch_brand_g2)){
    $sch_brand_url .= "&sch_brand_g2={$sch_brand_g2}";

    switch ($sch_brand_g2) {
        case '70007':   # 닥터피엘 필터라인
            $convert_total_chart_style_list = $chart_doc_type["1314"];
            break;
        case '70008':   # 닥터피엘 큐빙
            $convert_total_chart_style_list = $chart_doc_type["3386"];
            break;
        case '70009':   # 닥터피엘 퓨어팟
            $convert_total_chart_style_list = $chart_doc_type["2863"];
            break;
        case '70010':   # 닥터피엘 에코호스
            $convert_total_chart_style_list = $chart_doc_type["3303"];
            break;
        case '70011':   # 닥터피엘 여행용
            $convert_total_chart_style_list = $chart_doc_type["5434"];
            break;
        case '70026':   # 닥터피엘 아토샤워헤드
            $convert_total_chart_style_list = $chart_doc_type["5812"];
            break;
        case '70014':   # 닥터피엘 피쳐정수기
            $convert_total_chart_style_list = $chart_doc_type["5434"];
            break;
        case '70016':   # 누잠 토퍼류
            $convert_total_chart_style_list = $chart_brand_type["nuzam"];
            break;
        default:
            $convert_total_chart_style_list = $chart_empty_type;
            break;
    }
}

if(!empty($sch_brand)) {
    $add_where         .= " AND `cc`.brand = '{$sch_brand}'";
    $add_cms_where     .= " AND `w`.c_no='{$sch_brand}'";
    $add_nosp_where    .= " AND `ar`.brand='{$sch_brand}'";
    $sch_brand_url     .= "&sch_brand={$sch_brand}";
}
elseif(!empty($sch_brand_g2)) {
    $add_where         .= " AND `cc`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_cms_where     .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_nosp_where    .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $add_where         .= " AND `cc`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_cms_where     .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_nosp_where    .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

if(!empty($sch_dp_company)) {
    $add_where      .= " AND `cc`.dp_c_no='{$sch_dp_company}'";
    $add_cms_where  .= " AND `w`.dp_c_no='{$sch_dp_company}'";
}

if(!empty($sch_url)) {
    $chk_url_item   = $url_model->getItem($sch_url);
    $sch_url_cms    = str_replace("idx=","", $chk_url_item['url']);

    $add_where      .= " AND `cc`.url_idx = '{$chk_url_item['url']}' AND `cc`.dp_c_no='{$chk_url_item['dp_c_no']}'";
    $add_cms_where  .= " AND `w`.page_idx='{$sch_url_cms}' AND `w`.dp_c_no='{$chk_url_item['dp_c_no']}'";
}

$sch_convert_s_datetime = $sch_convert_s_date." 00:00:00";
$sch_convert_e_datetime = $sch_convert_e_date." 23:59:59";

# 날짜 조건
$all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_convert_s_date}' AND '{$sch_convert_e_date}'";
$add_where         .= " AND convert_date BETWEEN '{$sch_convert_s_datetime}' AND '{$sch_convert_e_datetime}'";
$add_cms_where     .= " AND order_date BETWEEN '{$sch_convert_s_datetime}' AND '{$sch_convert_e_datetime}'";
$add_nosp_where    .= " AND `am`.adv_s_date BETWEEN '{$sch_convert_s_datetime}' AND '{$sch_convert_e_datetime}'";

$all_date_title     = "";
$all_date_key       = "";
$add_key_column     = "";
$add_cms_column     = "";
$add_nosp_column    = "";

if($sch_traffic_type == 'week') //주간
{
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_key_column  = "DATE_FORMAT(DATE_SUB(`cc`.convert_date, INTERVAL(IF(DAYOFWEEK(`cc`.convert_date)=1,8,DAYOFWEEK(`cc`.convert_date))-2) DAY), '%Y%m%d')";
    $add_cms_column  = "DATE_FORMAT(DATE_SUB(`w`.order_date, INTERVAL(IF(DAYOFWEEK(`w`.order_date)=1,8,DAYOFWEEK(`w`.order_date))-2) DAY), '%Y%m%d')";
    $add_nosp_column = "DATE_FORMAT(DATE_SUB(`am`.adv_s_date, INTERVAL(IF(DAYOFWEEK(`am`.adv_s_date)=1,8,DAYOFWEEK(`am`.adv_s_date))-2) DAY), '%Y%m%d')";
}
elseif($sch_traffic_type == 'hour') //시간
{
    $add_key_column  = "DATE_FORMAT(`cc`.convert_date, '%H')";
    $add_cms_column  = "DATE_FORMAT(`w`.order_date, '%H')";
}
else //일간
{
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";

    $add_key_column  = "DATE_FORMAT(`cc`.convert_date, '%Y%m%d')";
    $add_cms_column  = "DATE_FORMAT(`w`.order_date, '%Y%m%d')";
    $add_nosp_column = "DATE_FORMAT(`am`.adv_s_date, '%Y%m%d')";
}

# 사용 Array
$date_hour_option       = getHourOption();
$convert_date_list      = [];
$convert_link_date_list = [];
$convert_total_init     = array("traffic" => 0, "conversion" => 0, "conversion_rate" => 0, "total_price" => 0, "avg_price" => 0);
$nosp_result_init       = array("1" => array("count" => 0, "df_total" => 0, "n_total" => 0, "e_total" => 0, "total" => 0), "2" => array("count" => 0, "df_total" => 0, "n_total" => 0, "e_total" => 0, "total" => 0)); # 1: 타임보드, 2: 스페셜DA
$nosp_df_option         = array(1314);
$nosp_nuzam_option      = array(2827,4878);
$convert_total_list     = [];
$nosp_result_list       = [];

if($sch_traffic_type == 'week' || $sch_traffic_type == 'day')
{
    # 날짜 생성
    $all_date_sql = "
        SELECT
            {$all_date_title} as chart_title,
            {$all_date_key} as chart_key,
            DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
        FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
                (SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
        as allday
        WHERE {$all_date_where}
        ORDER BY chart_key, date_key
    ";
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    while($all_date = mysqli_fetch_assoc($all_date_query))
    {
        $convert_date_list[$all_date['chart_key']]  = $all_date['chart_title'];
        $convert_total_list[$all_date['chart_key']] = $convert_total_init;
        $nosp_result_list[$all_date['chart_key']]   = $nosp_result_init;

        if(!isset($convert_link_date_list[$all_date['chart_key']]))
        {
            $link_s_date = date("Y-m-d", strtotime($all_date['date_key']));
            if($sch_traffic_type == "week"){
                $link_s_w = date('w', strtotime($link_s_date));
                $link_e_w = 7-$link_s_w;
                $link_e_date = date("Y-m-d", strtotime("{$link_s_date} +{$link_e_w} days"));
                $convert_link_date_list[$all_date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_e_date);
            }else{
                $convert_link_date_list[$all_date['chart_key']] = array("s_date" => $link_s_date, "e_date" => $link_s_date);
            }
        }
    }
}else{
    foreach($date_hour_option as $hour){
        $hour = (int)$hour;
        $convert_date_list[$hour]   = $hour."시";
        $convert_total_list[$hour]  = $convert_total_init;
    }
}

$convert_sql = "
    SELECT
        {$add_key_column} as key_date,
        SUM(`cc`.traffic_count) as traffic_cnt,
        SUM(`cc`.conversion_count) as convert_cnt
    FROM commerce_conversion as `cc`
    WHERE {$add_where}
    GROUP BY key_date
    ORDER BY key_date
";
$convert_query = mysqli_query($my_db, $convert_sql);
while($convert_result = mysqli_fetch_assoc($convert_query)){
    $traffic_count  = $convert_result['traffic_cnt'];
    $convert_count  = $convert_result['convert_cnt'];
    $convert_rate   = ($traffic_count > 0) ? round((($convert_count/$traffic_count)*100),2) : 0;
    $key_date       = $convert_result['key_date'];

    if($sch_traffic_type == "hour"){
        $key_date = (int)$key_date;
    }

    $convert_total_list[$key_date]  = array("traffic" => $traffic_count, "conversion" => $convert_count, "conversion_rate" => $convert_rate);
}

$cms_sql = "
    SELECT
        key_date,
        SUM(ord_price) as total_price,
        AVG(ord_price) as avg_price
    FROM
    (
        SELECT
            {$add_cms_column} as key_date,
            SUM(`w`.unit_price) as ord_price
        FROM work_cms as `w`
        WHERE {$add_cms_where}
        GROUP BY order_number
    ) as w
    GROUP BY key_date
    ORDER BY key_date
";
$cms_query = mysqli_query($my_db, $cms_sql);
while($cms_result = mysqli_fetch_assoc($cms_query))
{
    $key_date = $cms_result['key_date'];
    if($sch_traffic_type == "hour"){
        $key_date = (int)$key_date;
    }

    $convert_total_list[$key_date]["total_price"]  = $cms_result['total_price'];
    $convert_total_list[$key_date]["avg_price"]    = round($cms_result['avg_price']);
}

$convert_total_chart_list   = array(
    array("title" => "유입", "type" => "line", "color" => "rgba(0,176,80)", "data" => array()),
    array("title" => "전환", "type" => "line", "color" => "rgba(255,192,0)", "data" => array()),
    array("title" => "전환율", "type" => "line", "color" => "rgba(0,176,240)", "data" => array()),
    array("title" => "아임웹 매출", "type" => "line", "color" => "rgba(128,100,162)", "data" => array()),
    array("title" => "주문건당 단가", "type" => "line", "color" => "rgba(255,51,204)", "data" => array())
);

foreach($convert_total_list as $key_date => $date_data)
{
    $convert_total_chart_list[0]['data'][]  = $date_data['traffic'];
    $convert_total_chart_list[1]['data'][]  = $date_data['conversion'];
    $convert_total_chart_list[2]['data'][]  = $date_data['conversion_rate'];
    $convert_total_chart_list[3]['data'][]  = !empty($date_data['total_price']) ? $date_data['total_price'] : 0;
    $convert_total_chart_list[4]['data'][]  = !empty($date_data['avg_price']) ? $date_data['avg_price'] : 0;
}

if($sch_traffic_type == 'week' || $sch_traffic_type == 'day')
{
    $nosp_result_tmp_list = [];
    $nosp_result_sql = "
        SELECT
            *,
            {$add_nosp_column} as key_date
        FROM advertising_result as `ar`
        LEFT JOIN advertising_management as `am` ON `am`.am_no=`ar`.am_no
        WHERE {$add_nosp_where}
        ORDER BY adv_s_date 
    ";
    $nosp_result_query = mysqli_query($my_db, $nosp_result_sql);
    while($nosp_result = mysqli_fetch_assoc($nosp_result_query))
    {
        $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["key_date"]         = $nosp_result['key_date'];
        $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["product"]          = $nosp_result['product'];
        $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["fee_per"]          = $nosp_result['fee_per'];
        $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_adv_price"] += $nosp_result['adv_price'];
        $nosp_result_tmp_list[$nosp_result['am_no']][$nosp_result['brand']]["total_cnt"]++;
    }

    $nosp_result_chk_list = [];
    foreach($nosp_result_tmp_list as $am_no => $nosp_result_tmp)
    {
        foreach($nosp_result_tmp as $brand => $adv_brand_data)
        {
            $fee_price      = $adv_brand_data['total_adv_price']*($adv_brand_data['fee_per']/100);
            $cal_price      = $adv_brand_data["total_adv_price"]-$fee_price;
            $cal_price_vat  = $cal_price*1.1;
            $chk_brand      = "e_total";

            if(array_search($brand, $nosp_df_option) !== false){
                $chk_brand = "df_total";
            }elseif(array_search($brand, $nosp_nuzam_option) !== false){
                $chk_brand = "n_total";
            }

            $nosp_result_list[$adv_brand_data['key_date']][$adv_brand_data['product']]['total']     += $cal_price_vat;
            $nosp_result_list[$adv_brand_data['key_date']][$adv_brand_data['product']][$chk_brand]  += $cal_price_vat;

            if(!isset($nosp_result_chk_list[$am_no])){
                $nosp_result_chk_list[$am_no] = 1;
                $nosp_result_list[$adv_brand_data['key_date']][$adv_brand_data['product']]['count']++;
            }
        }
    }
}

$smarty->assign("sch_brand_url", $sch_brand_url);
$smarty->assign("convert_date_list", $convert_date_list);
$smarty->assign("convert_link_date_list", $convert_link_date_list);
$smarty->assign("convert_total_list", $convert_total_list);
$smarty->assign("convert_total_cnt", count($convert_total_list));
$smarty->assign("nosp_result_list", $nosp_result_list);
$smarty->assign("convert_chart_name_list", json_encode($convert_date_list));
$smarty->assign("convert_total_chart_list", json_encode($convert_total_chart_list));
$smarty->assign("convert_total_chart_style_list", json_encode($convert_total_chart_style_list));

$smarty->display('work_cms_stats_traffic_conversion_iframe.html');
?>
