<?php
ini_set('display_errors', -1);
ini_set('max_execution_time', '3600');
ini_set('memory_limit', '5G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/model/Custom.php');
require('inc/model/WiseCsm.php');
require('Classes/PHPExcel.php');

$upload_type            = isset($_POST['new_excel_type']) ? $_POST['new_excel_type'] : "";
$return_file_type       = isset($_POST['return_file_type']) ? $_POST['return_file_type'] : "";
$return_file            = $_FILES["return_file"];
$regdate                = date("Y-m-d H:i:s");
$reg_s_no               = $session_s_no;
$csm_model              = WiseCsm::Factory();
$ins_sql_list           = [];
$ins_cr_order_no_list   = [];

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($return_file['name']) && !empty($return_file['name'])){
    $upload_file_path = add_store_file($return_file, "upload_management");
    $upload_file_name = $return_file['name'];
}

$upload_data  = array(
    "upload_type"   => $upload_type,
    "upload_kind"   => $return_file_type,
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $reg_s_no,
    "regdate"       => $regdate,
);
$upload_model->insert($upload_data);

$file_no         = $upload_model->getInsertId();
$excel_file_path = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($excel_file_path);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();


$start_idx = 2;
if($return_file_type == '5'){
    $start_idx = 3;
}
$upd_result = false;

for ($i = $start_idx; $i <= $totalRow; $i++)
{
    if($i == 2 && $return_file_type != '5')
    {
        switch($return_file_type){
            case '1':
                $chk_str = (string)trim(addslashes($objWorksheet->getCell("R1")->getValue()));
                if(strpos($chk_str, "반품") === false){
                    exit("<script>alert('아임웹(반품)용 엑셀이 아닙니다.');location.href='waple_upload_management.php';</script>");
                }
                break;
            case '2':
                $chk_str = (string)trim(addslashes($objWorksheet->getCell("U1")->getValue()));
                if(strpos($chk_str, "교환") === false){
                    exit("<script>alert('아임웹(교환)용 엑셀이 아닙니다.');location.href='waple_upload_management.php';</script>");
                }
                break;
            case '3':
                $chk_str = (string)trim(addslashes($objWorksheet->getCell("N1")->getValue()));
                if(strpos($chk_str, "반품") === false){
                    exit("<script>alert('스토어팜(반품)용 엑셀이 아닙니다.');location.href='waple_upload_management.php';</script>");
                }
                break;
            case '4':
                $chk_str = (string)trim(addslashes($objWorksheet->getCell("N1")->getValue()));
                if(strpos($chk_str, "교환") === false){
                    exit("<script>alert('스토어팜(교환)용 엑셀이 아닙니다.');location.href='waple_upload_management.php';</script>");
                }
                break;
        }
    }

    $cr_order_number = $shop_ord_no = $return_type = "";
    $order_number = $order_date = $c_no = $c_name = $prd_no = $quantity = $dp_c_no = $total_price = "";
    $return_state = $return_date = $return_progress = $return_reason = $customer_memo = "";
    $pickup_state = $pickup_date = $return_wait_state = $return_pay_type = "";
    $return_delivery_no = $return_delivery_type = $return_delivery_price = "";
    $buyer_addr = $buyer_postcode = $seller_addr = $seller_postcode = "";
    $re_pickup_date = $re_delivery_type = $re_delivery_no = "";
    $return_date_val = $pickup_date_val = $re_pickup_date_val = "";

    if($return_file_type == '1')  # 아임웹(반품)
    {
        $return_type            = '1';
        $order_number           = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $shop_ord_no            = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //상품주문번호
        $total_price            = (int)trim(addslashes($objWorksheet->getCell("AC{$i}")->getValue()));    //금액
        $return_state           = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //반품진행상태
        $return_progress        = (string)trim(addslashes($objWorksheet->getCell("AW{$i}")->getValue())); //반품진행방법
        $return_date_val        = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //반품요청일
        $return_reason_val      = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  //반품사유&고객메세지
        $return_reason_exp      = !empty($return_reason_val) ? explode('/', $return_reason_val) : [];
        $return_reason          = isset($return_reason_exp[0]) ? trim($return_reason_exp[0]) : "";
        $customer_memo          = isset($return_reason_exp[1]) ? trim($return_reason_exp[1]) : "";
        $pickup_date_val        = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //반품수거완료일
        $return_delivery_type   = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue())); //수거 택배사
        $return_delivery_no     = (string)trim(addslashes($objWorksheet->getCell("AY{$i}")->getValue())); //수거송장번호

        if($return_state != "반품완료"){
            continue;
        }
    }
    elseif($return_file_type == '2') # 아임웹(교환)
    {
        $return_type            = '2';
        $order_number           = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //주문번호
        $shop_ord_no            = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //상품주문번호
        $total_price            = (int)trim(addslashes($objWorksheet->getCell("AD{$i}")->getValue()));    //금액
        $return_state           = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //교환진행상태
        $return_progress        = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue())); //교환진행방법
        $return_date_val        = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //교환요청일
        $return_reason_val      = (string)trim(addslashes($objWorksheet->getCell("U{$i}")->getValue()));  //교환사유&고객메세지
        $return_reason_exp      = !empty($return_reason_val) ? explode('/', $return_reason_val) : [];
        $return_reason          = isset($return_reason_exp[0]) ? trim($return_reason_exp[0]) : "";
        $customer_memo          = isset($return_reason_exp[1]) ? trim($return_reason_exp[1]) : "";
        $pickup_date_val        = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  //교환수거완료일
        $re_pickup_date_val     = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //재발송완료일
        $return_delivery_type   = (string)trim(addslashes($objWorksheet->getCell("AY{$i}")->getValue())); //수거 택배사
        $return_delivery_no     = (string)trim(addslashes($objWorksheet->getCell("AZ{$i}")->getValue())); //수거송장번호
        $re_delivery_type       = (string)trim(addslashes($objWorksheet->getCell("BB{$i}")->getValue())); //재배송택배사
        $re_delivery_no         = (string)trim(addslashes($objWorksheet->getCell("BC{$i}")->getValue())); //재배송송장번호

        if($return_state != "교환완료"){
            continue;
        }
    }
    elseif($return_file_type == '3') # 스토어팜(반품)
    {
        $return_type            = '1';
        $shop_ord_no            = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //상품주문번호
        $order_number           = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //주문번호
        $total_price            = (int)trim(addslashes($objWorksheet->getCell("AL{$i}")->getValue()));    //금액
        $return_state           = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //반품진행상태
        $return_progress        = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //반품진행방법
        $pickup_state           = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //수거상태
        $return_date_val        = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //반품요청일
        $return_reason          = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //반품사유
        $return_wait_state      = (string)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  //환불보류상태
        $return_delivery_price  = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //반품비
        $return_pay_type        = (string)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  //반품배송비 결제방법
        $pickup_date_val        = (string)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue()));  //수거완료일
        $return_delivery_type   = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue())); //수거 택배사
        $return_delivery_no     = (string)trim(addslashes($objWorksheet->getCell("AY{$i}")->getValue())); //수거송장번호

        $buyer_addr_val         = (string)trim(addslashes($objWorksheet->getCell("BG{$i}")->getValue())); //구매자 주소
        $buyer_addr_exp         = !empty($buyer_addr_val) ? explode(')', $buyer_addr_val, 2) : [];
        $buyer_postcode         = isset($buyer_addr_exp[0]) ? trim(str_replace('(','',$buyer_addr_exp[0])) : "";
        $buyer_addr             = isset($buyer_addr_exp[1]) ? trim($buyer_addr_exp[1]) : "";
        $seller_addr_val        = (string)trim(addslashes($objWorksheet->getCell("BH{$i}")->getValue())); //판매자 주소
        $seller_addr_exp        = !empty($seller_addr_val) ? explode(')', $seller_addr_val, 2) : [];
        $seller_postcode        = isset($seller_addr_exp[0]) ? trim(str_replace('(','',$seller_addr_exp[0])) : "";
        $seller_addr            = isset($seller_addr_exp[1]) ? trim($seller_addr_exp[1]) : "";

        if($return_state != "반품완료"){
            continue;
        }
    }
    elseif($return_file_type == '4') # 스토어팜(교환)
    {
        $return_type            = '2';
        $shop_ord_no            = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //상품주문번호
        $order_number           = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //주문번호
        $total_price            = (int)trim(addslashes($objWorksheet->getCell("AJ{$i}")->getValue()));    //금액
        $return_state           = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));  //교환처리상태
        $return_progress        = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));  //교환진행방법
        $pickup_state           = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));  //수거상태
        $return_date_val        = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  //교환요청일
        $return_reason          = (string)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  //교환사유
        $return_wait_state      = (string)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  //교환보류상태
        $return_delivery_price  = (string)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  //교환배송비
        $return_pay_type        = (string)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  //교환배송비 결제방법
        $pickup_date_val        = (string)trim(addslashes($objWorksheet->getCell("W{$i}")->getValue()));  //수거완료일
        $re_pickup_date_val     = (string)trim(addslashes($objWorksheet->getCell("BE{$i}")->getValue()));  //재발송처리일
        $return_delivery_type   = (string)trim(addslashes($objWorksheet->getCell("AX{$i}")->getValue())); //수거 택배사
        $return_delivery_no     = (string)trim(addslashes($objWorksheet->getCell("AY{$i}")->getValue())); //수거송장번호
        $re_delivery_type       = (string)trim(addslashes($objWorksheet->getCell("BC{$i}")->getValue())); //재배송택배사
        $re_delivery_no         = (string)trim(addslashes($objWorksheet->getCell("BD{$i}")->getValue())); //재배송송장번호

        $buyer_addr_val  = (string)trim(addslashes($objWorksheet->getCell("BM{$i}")->getValue())); //구매자 주소
        $buyer_addr_exp  = !empty($buyer_addr_val) ? explode(')', $buyer_addr_val, 2) : [];
        $buyer_postcode  = isset($buyer_addr_exp[0]) ? trim(str_replace('(','',$buyer_addr_exp[0])) : "";
        $buyer_addr      = isset($buyer_addr_exp[1]) ? trim($buyer_addr_exp[1]) : "";
        $seller_addr_val = (string)trim(addslashes($objWorksheet->getCell("BN{$i}")->getValue())); //판매자 주소
        $seller_addr_exp = !empty($seller_addr_val) ? explode(')', $seller_addr_val, 2) : [];
        $seller_postcode = isset($seller_addr_exp[0]) ? trim(str_replace('(','',$seller_addr_exp[0])) : "";
        $seller_addr     = isset($seller_addr_exp[1]) ? trim($seller_addr_exp[1]) : "";

        if($return_state != "교환완료"){
            continue;
        }
    }
    elseif($return_file_type == '5')  # 사방넷(클레임확인)
    {
        $shop_ord_no            = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));  //상품주문번호
        $order_number           = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));  //주문번호
        $total_price            = (int)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));    //금액
        $return_state           = (string)trim(addslashes($objWorksheet->getCell("Z{$i}")->getValue()));  //반품진행상태
        $return_progress        = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue())); //반품진행방법
        $return_date_val        = (string)trim(addslashes($objWorksheet->getCell("AM{$i}")->getValue())); //반품요청일
        $return_reason          = (string)trim(addslashes($objWorksheet->getCell("AI{$i}")->getValue())); //반품사유
        $return_delivery_type   = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue())); //수거 택배사
        $return_delivery_no     = (string)trim(addslashes($objWorksheet->getCell("AC{$i}")->getValue())); //수거송장번호

        if(strpos($return_state, "반품") !== false){
            $return_type = "1";
        }elseif(strpos($return_state, "교환") !== false){
            $return_type = "2";
        }else{
            $return_type = "1";
        }
    }

    if(!$shop_ord_no){
        echo "ROW : {$i}<br/>";
        echo "반품/교환리스트 반영에 실패하였습니다.<br>상품주문번호가 없습니다.";
        exit;
    }

    # 날짜 계산
    if(!empty($return_date_val))
    {
        if(strpos($return_date_val, "/") !== false) {
            $return_date = date("Y-m-d H:i", strtotime("{$return_date_val}"));
        }elseif(strpos($return_date_val, "-") !== false) {
            $return_date = date("Y-m-d H:i", strtotime("{$return_date_val}"));
        }elseif($return_date_val > 20000000) {
            $return_date = date("Y-m-d H:i", strtotime("{$return_date_val}"));
        }else{
            //엑셀 시간 형식 변경
            $return_time_cal = ($return_date_val-25569)*86400;
            $return_time     = date("Y-m-d H:i", $return_time_cal);
            $return_date     = date("Y-m-d H:i", strtotime("{$return_time} -9 hour"));
        }
    }

    if(!empty($pickup_date_val))
    {
        if(strpos($pickup_date_val, "/") !== false) {
            $pickup_date = date("Y-m-d H:i", strtotime("{$pickup_date_val}"));
        }elseif(strpos($pickup_date_val, "-") !== false) {
            $pickup_date = date("Y-m-d H:i", strtotime("{$pickup_date_val}"));
        }elseif($pickup_date_val > 20000000) {
            $pickup_date = date("Y-m-d H:i", strtotime("{$pickup_date_val}"));
        }else{
            //엑셀 시간 형식 변경
            $pickup_time_cal = ($pickup_date_val-25569)*86400;
            $pickup_time     = date("Y-m-d H:i", $pickup_time_cal);
            $pickup_date     = date("Y-m-d H:i", strtotime("{$pickup_time} -9 hour"));
        }
    }

    if(!empty($re_pickup_date_val))
    {
        if(strpos($re_pickup_date_val, "/") !== false) {
            $re_pickup_date = date("Y-m-d H:i", strtotime("{$re_pickup_date_val}"));
        }elseif(strpos($re_pickup_date_val, "-") !== false) {
            $re_pickup_date = date("Y-m-d H:i", strtotime("{$re_pickup_date_val}"));
        }elseif($re_pickup_date_val > 20000000) {
            $re_pickup_date = date("Y-m-d H:i", strtotime("{$re_pickup_date_val}"));
        }else{
            //엑셀 시간 형식 변경
            $re_pickup_time_cal = ($re_pickup_date_val-25569)*86400;
            $re_pickup_time     = date("Y-m-d H:i", $re_pickup_time_cal);
            $re_pickup_date     = date("Y-m-d H:i", strtotime("{$re_pickup_time} -9 hour"));
        }
    }

    $order_sql   = "SELECT * FROM work_cms WHERE shop_ord_no = '{$shop_ord_no}'";
    $order_query = mysqli_query($my_db, $order_sql);
    $order       = mysqli_fetch_assoc($order_query);

    if(isset($order['order_number']))
    {
        if($return_file_type == '5'){
            $order_number = $order['order_number'];
        }

        $order_date   = $order['order_date'];
        $c_no         = $order['c_no'];
        $c_name       = $order['c_name'];
        $prd_no       = $order['prd_no'];
        $quantity     = $order['quantity'];
        $dp_c_no      = $order['dp_c_no'];
    }

    $chk_return_sql     = "SELECT count(cr_no) as cnt FROM csm_return WHERE shop_ord_no = '{$shop_ord_no}'";
    $chk_return_query   = mysqli_query($my_db, $chk_return_sql);
    $chk_return_result  = mysqli_fetch_assoc($chk_return_query);
    if($chk_return_result['cnt'] > 0){
        $chk_upd_data = array(
            "return_type"           => $return_type,
            "return_state"          => $return_state,
            "return_progress"       => $return_progress,
            "return_date"           => $return_date,
            "return_reason"         => addslashes($return_reason),
            "customer_memo"         => addslashes($customer_memo),
            "pickup_date"           => $pickup_date,
            "pickup_state"          => $pickup_state,
            "return_delivery_no"    => $return_delivery_no,
            "return_delivery_type"  => $return_delivery_type,
            "return_delivery_price" => $return_delivery_price,
            "return_wait_state"     => $return_wait_state,
            "return_pay_type"       => $return_pay_type,
            "re_pickup_date"        => $re_pickup_date,
            "re_delivery_type"      => $re_delivery_type,
            "re_delivery_no"        => $re_delivery_no,
            "re_delivery_date"      => $re_pickup_date,
            'file_no'               => $file_no,
        );

        if(!$csm_model->updateToArray($chk_upd_data, array("shop_ord_no" => $shop_ord_no))){
            echo "ROW : {$i}<br/>";
            echo "반품/교환리스트 반영에 실패하였습니다.<br>업데이트에 실패했습니다.<br/>";
            exit;
        }
        $upd_result = true;
    }else{
        $ins_data   = array(
            'return_type'           => $return_type,
            'return_state'          => $return_state,
            'return_date'           => $return_date,
            'order_number'          => $order_number,
            'order_date'            => $order_date,
            'shop_ord_no'           => $shop_ord_no,
            'c_no'                  => $c_no,
            'c_name'                => $c_name,
            'prd_no'                => $prd_no,
            'quantity'              => $quantity,
            'total_price'           => $total_price,
            'dp_c_no'               => $dp_c_no,
            'return_progress'       => $return_progress,
            'return_reason'         => addslashes($return_reason),
            'customer_memo'         => addslashes($customer_memo),
            'return_delivery_no'    => $return_delivery_no,
            'return_delivery_type'  => $return_delivery_type,
            'pickup_state'          => $pickup_state,
            'pickup_date'           => $pickup_date,
            'return_wait_state'     => $return_wait_state,
            'return_delivery_price' => $return_delivery_price,
            'return_pay_type'       => $return_pay_type,
            'buyer_addr'            => $buyer_addr,
            'buyer_postcode'        => $buyer_postcode,
            'seller_addr'           => $seller_addr,
            'seller_postcode'       => $seller_postcode,
            'regdate'               => $regdate,
            're_pickup_date'        => $re_pickup_date,
            're_delivery_type'      => $re_delivery_type,
            're_delivery_no'        => $re_delivery_no,
            're_delivery_date'      => $re_pickup_date,
            'file_no'               => $file_no,
        );

        $ins_sql_list[] = $ins_data;
    }
}

if(!empty($ins_sql_list))
{
    if($csm_model->multiInsert($ins_sql_list)){
        exit("<script>alert('반품/교환리스트 리스트 반영 되었습니다.');location.href='wise_csm_return_list.php?sch_s_date=&sch_e_date=&file_no={$file_no}';</script>");
    }else{
        exit("<script>alert('반품/교환리스트 리스트 반영에 실패했습니다.');location.href='waple_upload_management.php';</script>");
    }
}else if($upd_result){
    exit("<script>alert('반품/교환리스트 리스트 반영 되었습니다.');location.href='wise_csm_return_list.php?sch_s_date=&sch_e_date=&file_no={$file_no}';</script>");
}else{
    exit("<script>alert('반품/교환리스트 리스트 반영에 실패했습니다.');location.href='waple_upload_management.php';</script>");
}

?>
