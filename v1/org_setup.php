<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Team.php');
require('inc/model/MyCompany.php');

# Team Model 생성
$teamModel = Team::Factory();

# 프로세스 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "new_org_team")
{
    $team_name_val        = isset($_POST['team_name']) ? $_POST['team_name'] : "";
    $team_parent_code_val = isset($_POST['team_parent_code']) ? $_POST['team_parent_code'] : "";
    $team_parent_root_val = isset($_POST['team_parent_root']) ? $_POST['team_parent_root'] : "";
    $team_group           = isset($_POST['team_group']) ? $_POST['team_group'] : "";
    $priority_val         = isset($_POST['priority']) ? $_POST['priority'] : "";
    $my_c_no_val          = isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "";
    $display_val          = isset($_POST['display']) ? $_POST['display'] : "";

    $last_team_code_val = $teamModel->getLastTeamCode();
    $last_team_code     = "";
    if(!empty($last_team_code_val)){
        $last_team_code = sprintf('%05d', $last_team_code_val);
    }

    $depth_val  = ($team_parent_root_val) ? 1 : $teamModel->getTeamChildDepth($team_parent_code_val);
    $team_group = ($team_parent_root_val) ? $last_team_code : (empty($team_group) ? $teamModel->getTeamGroup($team_parent_code_val): $team_group);

    if($display_val == '2'){
        $priority_val = 999;
        $team_group   = '';
    }

    if(empty($priority_val)) {
        $priority_val = $teamModel->getMaxPriority();
    }else{
        $teamModel->updatePriority("", $priority_val);
    }

    $team_data = array(
        'team_code'     => $last_team_code,
        'team_name'     => $team_name_val,
        'team_group'    => $team_group,
        'priority'      => $priority_val,
        'my_c_no'       => $my_c_no_val,
        'depth'         => $depth_val,
        'display'       => $display_val
    );

    if($team_parent_root_val){
        $team_data['team_code_parent']="NULL";
    }else{
        $team_data['team_code_parent']=$team_parent_code_val;
    }

    if($teamModel->insert($team_data)){
        exit("<script>alert('부서추가에 성공했습니다');location.href='org_setup.php?sch_my_c_no={$my_c_no_val}';</script>");
    }else{
        exit("<script>alert('부서추가에 실패했습니다');location.href='org_setup.php?sch_my_c_no={$my_c_no_val}';</script>");
    }
}
elseif($process == "modify_org_team")
{
    $team_code_val        = isset($_POST['team_code']) ? $_POST['team_code'] : "";
    $team_name_val        = isset($_POST['team_name']) ? $_POST['team_name'] : "";
    $team_parent_code_val = isset($_POST['team_parent_code']) ? $_POST['team_parent_code'] : "";
    $team_parent_root_val = isset($_POST['team_parent_root']) ? $_POST['team_parent_root'] : "";
    $team_group           = isset($_POST['team_group']) ? $_POST['team_group'] : "";
    $my_c_no_val          = isset($_POST['my_c_no']) ? $_POST['my_c_no'] : "";
    $priority_val         = isset($_POST['priority']) ? $_POST['priority'] : "";
    $display_val          = isset($_POST['display']) ? $_POST['display'] : "";

    $depth_val  = ($team_parent_root_val) ? 1 : $teamModel->getTeamChildDepth($team_parent_code_val);
    $team_group = ($team_parent_root_val) ? $team_code_val : $team_group;

    if($display_val == '2'){
        $priority_val = 999;
    }

    if(empty($priority_val)){
        $priority_val        = $teamModel->getMaxPriority();
    }else{
        $teamModel->updatePriority($team_code_val, $priority_val);
    }

    $team_data = array(
        'team_code'     => $team_code_val,
        'team_name'     => $team_name_val,
        'team_group'    => $team_group,
        'priority'      => $priority_val,
        'my_c_no'       => $my_c_no_val,
        'depth'         => $depth_val,
        'display'       => $display_val
    );

    if($team_parent_root_val){
        $team_data['team_code_parent']="NULL";
    }else{
        $team_data['team_code_parent']=$team_parent_code_val;
    }

    if($teamModel->update($team_data)){
        exit("<script>alert('부서수정에 성공했습니다');location.href='org_setup.php?sch_my_c_no={$my_c_no_val}';</script>");
    }else{
        exit("<script>alert('부서수정에 실패했습니다');location.href='org_setup.php?sch_my_c_no={$my_c_no_val}';</script>");
    }
}

# 검색조건
$sch_my_c_no    = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "1";
$search_url     = getenv("QUERY_STRING");
$add_where      = "1=1";
if(!empty($sch_my_c_no))
{
    $add_where .= " AND t1.my_c_no='{$sch_my_c_no}'";
    $smarty->assign("sch_my_c_no", $sch_my_c_no);
}

$org_team_sql   = "SELECT * FROM team t1 WHERE {$add_where} ORDER BY priority, team_code_parent";
$org_team_query = mysqli_query($my_db, $org_team_sql);

#기본 그룹1 셋팅
$org_team_list = [];
while($org_team = mysqli_fetch_assoc($org_team_query))
{
    $org_team_list[$org_team['team_code']] = array("t_name" => $org_team['team_name'], "depth" => $org_team['depth'], "priority" => $org_team['priority'], "display" => $org_team['display']);
}

# 라벨정리
$org_team_final_list = [];
if(!empty($org_team_list))
{
    foreach($org_team_list as $t_key => $t_data)
    {
        $t_label      = "";
        $t_label_list = [];
        $t_code       = $t_key;
        $t_depth      = $t_data['depth'];
        for($i=0; $i<$t_depth; $i++)
        {
            if(empty($t_code)){
                break;
            }
            $t_label_result   = $teamModel->getItem($t_code);
            $t_code           = $t_label_result['team_code_parent'];
            $t_label_list[$i] = $t_label_result['team_name'];
        }

        if(!empty($t_label_list))
        {
            krsort($t_label_list);
            $t_label = implode(" > ", $t_label_list);

            $label_priority              = isset($t_data['priority']) ? sprintf('%02d', $t_data['priority']) : "01";
            $org_team_final_list[$t_key] = array("label" => $label_priority." : ".$t_label, "display" => $t_data['display']);
        }
    }
}

$my_company_model       = MyCompany::Factory();
$my_company_name_list   = $my_company_model->getNameList();

$smarty->assign("my_company_option", $my_company_name_list);
$smarty->assign("org_team_list", $org_team_final_list);

$smarty->display('org_setup.html');
?>
