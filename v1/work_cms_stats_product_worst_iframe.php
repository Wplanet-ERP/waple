<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');

# 검색조건
$add_where          = "1=1 AND `w`.delivery_state='4'";
$today_val          = date('Y-m-d');
$month_val          = date('Y-m', strtotime('-3 months'))."-01";
$sch_order_type     = isset($_GET['sch_order_type']) ? $_GET['sch_order_type'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_order_s_date   = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : $month_val;
$sch_order_e_date   = isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : $today_val;
$sch_price_type     = isset($_GET['sch_price_type']) ? $_GET['sch_price_type'] : "";
$sch_dp_company     = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";
$sch_in_date_type   = isset($_GET['sch_in_date_type']) ? $_GET['sch_in_date_type'] : 3;

if(empty($sch_order_s_date) || empty($sch_order_e_date))
{
    echo "날짜 검색 값이 없으면 차트 생성이 안됩니다.";
    exit;
}

if(!empty($sch_order_s_date)){
    $order_s_datetime   = $sch_order_s_date." 00:00:00";
    $add_where         .= " AND w.order_date >= '{$order_s_datetime}'";
    $smarty->assign("sch_order_s_date", $sch_order_s_date);
}

if(!empty($sch_order_e_date)){
    $order_e_datetime   = $sch_order_e_date." 23:59:59";
    $add_where         .= " AND w.order_date <= '{$order_e_datetime}'";
    $smarty->assign("sch_order_e_date", $sch_order_e_date);
}

if(!empty($sch_price_type)){

    if($sch_price_type == '1'){
        $add_where .= " AND w.unit_price > 0";
    }elseif($sch_price_type == '2'){
        $add_where .= " AND w.unit_price = 0";
    }
    $smarty->assign("sch_price_type", $sch_price_type);
}

if(!empty($sch_dp_company)){
    $add_where .= " AND w.dp_c_no = '{$sch_dp_company}'";
    $smarty->assign("sch_dp_company", $sch_dp_company);
}

# 브랜드 옵션
if(!empty($sch_brand)) {
    $add_where         .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);

$all_date_where     = "";
$all_date_title     = "";
$all_date_key       = "";
$add_key_column     = "";

if($sch_in_date_type == '4') //연간
{
    $sch_s_year      = date("Y", strtotime($sch_order_s_date));
    $sch_e_year      = date("Y", strtotime($sch_order_e_date));

    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y')";
    $add_key_column  = "DATE_FORMAT(`w`.order_date, '%Y')";
}
elseif($sch_in_date_type == '3') //월간
{
    $sch_s_month     = date("Y-m", strtotime($sch_order_s_date));
    $sch_e_month     = date("Y-m", strtotime($sch_order_e_date));
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m')";
    $add_key_column  = "DATE_FORMAT(`w`.order_date, '%Y%m')";
}
elseif($sch_in_date_type == '2') //주간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_order_s_date}' AND '{$sch_order_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";
    $add_key_column  = "DATE_FORMAT(DATE_SUB(`w`.order_date, INTERVAL(IF(DAYOFWEEK(`w`.order_date)=1,8,DAYOFWEEK(`w`.order_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_order_s_date}' AND '{$sch_order_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";
    $add_key_column  = "DATE_FORMAT(`w`.order_date, '%Y%m%d')";
}
$smarty->assign("sch_order_type", $sch_order_type);
$smarty->assign("sch_in_date_type", $sch_in_date_type);

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

# 날짜 생성
$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
	    DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
$total_all_date_list = [];
$cms_date_name_list  = [];
while($all_date = mysqli_fetch_assoc($all_date_query))
{
    $total_all_date_list[$all_date['date_key']] = array("s_date" => date("Y-m-d",strtotime($all_date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($all_date['date_key']))." 23:59:59");
    $cms_date_name_list[$all_date['chart_key']] = $all_date['chart_title'];
}

# 사용 Array
$chk_type                   = ($sch_order_type == "quantity") ? "quantity" : "unit_price";
$cms_worst_temp_list        = [];
$cms_worst_temp_name_list   = [];
$cms_worst_table_tmp_list   = [];
$cms_worst_table_list       = [];

foreach($total_all_date_list as $date_data)
{
    $chk_prd_rank_sql   = "SELECT {$add_key_column} as key_date, prd_no, (SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no) as prd_name, SUM({$chk_type}) as total_value FROM work_cms w WHERE {$add_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}') GROUP BY prd_no";
    $chk_prd_rank_query = mysqli_query($my_db, $chk_prd_rank_sql);
    while($chk_prd_rank = mysqli_fetch_assoc($chk_prd_rank_query))
    {
        $cms_worst_temp_list[$chk_prd_rank['prd_no']] += $chk_prd_rank['total_value'];
        $cms_worst_temp_name_list[$chk_prd_rank['prd_no']] = $chk_prd_rank['prd_name'];

        if(!isset($cms_worst_table_tmp_list[$chk_prd_rank['prd_no']])){
            foreach($cms_date_name_list as $key_date => $key_label){
                $cms_worst_table_tmp_list[$chk_prd_rank['prd_no']][$key_date] = 0;
            }
        }

        $cms_worst_table_tmp_list[$chk_prd_rank['prd_no']][$chk_prd_rank['key_date']] += $chk_prd_rank['total_value'];
    }
}
asort($cms_worst_temp_list);

$cms_worst_chart_name_list   = [];
$cms_worst_table_name_list   = [];
$chk_worst_idx = 0;
foreach($cms_worst_temp_list as $prd_no => $best_qty){

    if($chk_worst_idx < 5){
        $cms_worst_chart_name_list[$prd_no] = $cms_worst_temp_name_list[$prd_no];
    }

    if($chk_worst_idx > 19){
        break;
    }

    $cms_worst_table_name_list[$prd_no] = $cms_worst_temp_name_list[$prd_no];

    foreach($cms_date_name_list as $key_date => $key_label){
        $cms_worst_table_list[$prd_no][$key_date] = $cms_worst_table_tmp_list[$prd_no][$key_date];
    }

    $chk_worst_idx++;
}

$cms_worst_chart_list = [];
$chart_idx      = 0;
$colors_option  = array("#26619C","#00A86B","#FFBF00","#C30010","#1E90FF");
foreach($cms_worst_chart_name_list as $chart_key => $chart_label)
{
    $cms_worst_chart_list[$chart_idx] = array(
        "title" => $chart_label,
        "type"  => "line",
        "color" => $colors_option[$chart_idx],
        "data"  => array()
    );

    foreach($cms_date_name_list as $date_key => $date_name)
    {
        $total_value = isset($cms_worst_table_list[$chart_key][$date_key]) ? $cms_worst_table_list[$chart_key][$date_key] : 0;
        $cms_worst_chart_list[$chart_idx]['data'][] = $total_value;
    }
    $chart_idx++;
}

$smarty->assign("date_type_option", getDateTypeOption());
$smarty->assign("cms_date_name_list", $cms_date_name_list);
$smarty->assign("cms_date_chart_name_list", json_encode($cms_date_name_list));
$smarty->assign("cms_worst_chart_name_list", json_encode($cms_worst_table_name_list));
$smarty->assign("cms_worst_chart_list", json_encode($cms_worst_chart_list));
$smarty->assign("cms_date_table_cnt", count($cms_date_name_list)+2);
$smarty->assign("cms_worst_table_name_list", $cms_worst_table_name_list);
$smarty->assign("cms_worst_table_list", $cms_worst_table_list);

$smarty->display('work_cms_stats_product_worst_iframe.html');
?>
