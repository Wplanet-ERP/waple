<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/manufacturer.php');
require('inc/model/Manufacturer.php');

# 공급업체 체크
if($session_partner == "dewbell"){
    $add_c_no   = "2305";
    $add_c_name = "듀벨";
}elseif($session_partner == "seongill"){
    $add_c_no   = "2304";
    $add_c_name = "성일화학";
}elseif($session_partner == "pico"){
    $add_c_no   = "2772";
    $add_c_name = "주식회사 피코그램";
}elseif($session_partner == "paino"){
    $add_c_no   = "2472";
    $add_c_name = "(주)파이노";
}else{
    exit("접근할 수 없는 URL 입니다.");
}
$smarty->assign("add_c_no", $add_c_no);
$smarty->assign("add_c_name", $add_c_name);

$process        = isset($_POST['process']) ? $_POST['process'] : "";
$manu_model     = Manufacturer::Factory();

if($process == "f_work_state")
{
    $mi_no      = isset($_POST['mi_no']) ? $_POST['mi_no'] : "";
    $val        = isset($_POST['val']) ? $_POST['val'] : "";
    $manu_item  = $manu_model->getItem($mi_no);
    $upd_data   = array("mi_no" => $mi_no, "work_state" => $val);

    if($manu_item['run_state'] > 0){
        echo "진행상태를 변경할 수 없는 건입니다.";
    }else{
        if(!$manu_model->update($upd_data)){
            echo "진행상태 변경에 실패했습니다.";
        }else{
            echo "진행상태를 변경했습니다.";
        }
    }
    exit;
}
elseif($process == "f_is_as_in")
{
    $mi_no      = isset($_POST['mi_no']) ? $_POST['mi_no'] : "";
    $val        = isset($_POST['val']) ? $_POST['val'] : "";
    $manu_item  = $manu_model->getItem($mi_no);

    $upd_data   = array("mi_no" => $mi_no, "is_as_in" => $val);

    if(!$manu_model->update($upd_data)){
        echo "AS 입고상태 변경에 실패했습니다.";
    }else{
        echo "AS 입고상태를 변경했습니다.";
    }
    exit;
}
elseif($process == "f_is_as_out")
{
    $mi_no      = isset($_POST['mi_no']) ? $_POST['mi_no'] : "";
    $val        = isset($_POST['val']) ? $_POST['val'] : "";
    $manu_item  = $manu_model->getItem($mi_no);

    $upd_data   = array("mi_no" => $mi_no, "is_as_out" => $val);

    if(!$manu_model->update($upd_data)){
        echo "AS 출고상태 변경에 실패했습니다.";
    }else{
        echo "AS 출고상태를 변경했습니다.";
    }
    exit;
}
elseif ($process == "multi_is_as_in")
{
    $no_list 		= (isset($_POST['select_no_list'])) ? $_POST['select_no_list'] : "";
    $chk_value	    = (isset($_POST['select_value'])) ? $_POST['select_value'] : "";
    $search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(!empty($no_list) && !empty($chk_value))
    {
        $chk_mi_sql 	= "SELECT * FROM manufacturer_inspection WHERE `mi_no` IN({$no_list}) AND add_c_no='{$add_c_no}'";
        $chk_mi_query	= mysqli_query($my_db, $chk_mi_sql);
        $chk_mi_data 	= [];
        while($chk_mi = mysqli_fetch_array($chk_mi_query))
        {
            $chk_mi_data[] = array("mi_no" => $chk_mi['mi_no'], "is_as_in" => $chk_value);
        }

        if(!empty($chk_mi_data)){
            if($manu_model->multiUpdate($chk_mi_data)){
                exit ("<script>alert('AS 입고상태를 변경했습니다.');location.href='out_return_mi_list.php?{$search_url}';</script>");
            }else{
                exit ("<script>alert('AS 입고상태 변경에 실패했습니다.');location.href='out_return_mi_list.php?{$search_url}';</script>");
            }
        }
    }
    exit ("<script>alert('AS 입고상태 변경에 실패했습니다.');location.href='out_return_mi_list.php?{$search_url}';</script>");
}
elseif ($process == "multi_is_as_out")
{
    $no_list 		= (isset($_POST['select_no_list'])) ? $_POST['select_no_list'] : "";
    $chk_value	    = (isset($_POST['select_value'])) ? $_POST['select_value'] : "";
    $search_url 	= (isset($_POST['search_url'])) ? $_POST['search_url'] : "";

    if(!empty($no_list) && !empty($chk_value))
    {
        $chk_mi_sql 	= "SELECT * FROM manufacturer_inspection WHERE `mi_no` IN({$no_list}) AND add_c_no='{$add_c_no}'";
        $chk_mi_query	= mysqli_query($my_db, $chk_mi_sql);
        $chk_mi_data 	= [];
        while($chk_mi = mysqli_fetch_array($chk_mi_query))
        {
            $chk_mi_data[] = array("mi_no" => $chk_mi['mi_no'], "is_as_out" => $chk_value);
        }

        if(!empty($chk_mi_data)){
            if($manu_model->multiUpdate($chk_mi_data)){
                exit ("<script>alert('AS 출고상태를 변경했습니다.');location.href='out_return_mi_list.php?{$search_url}';</script>");
            }else{
                exit ("<script>alert('AS 출고상태 변경에 실패했습니다.');location.href='out_return_mi_list.php?{$search_url}';</script>");
            }
        }
    }
    exit ("<script>alert('AS 출고상태 변경에 실패했습니다.');location.href='out_return_mi_list.php?{$search_url}';</script>");
}

# 검색처리
$add_where          = "1=1 AND mi.add_c_no='{$add_c_no}'";
$sch_mi_no          = isset($_GET['sch_mi_no']) ? $_GET['sch_mi_no'] : "";
$sch_work_state     = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_parent_ord_no  = isset($_GET['sch_parent_ord_no']) ? $_GET['sch_parent_ord_no'] : "";
$sch_stock_date     = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : "";
$sch_recipient      = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp   = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_option         = isset($_GET['sch_option']) ? $_GET['sch_option'] : "";
$sch_title          = isset($_GET['sch_title']) ? $_GET['sch_title'] : "";
$sch_is_improve     = isset($_GET['sch_is_improve']) ? $_GET['sch_is_improve'] : "";
$sch_run_state      = isset($_GET['sch_run_state']) ? $_GET['sch_run_state'] : "";
$sch_is_as_in       = isset($_GET['sch_is_as_in']) ? $_GET['sch_is_as_in'] : "";
$sch_is_as_out      = isset($_GET['sch_is_as_out']) ? $_GET['sch_is_as_out'] : "";

if(!empty($sch_mi_no)) {
    $add_where .= " AND mi.mi_no='{$sch_mi_no}'";
    $smarty->assign("sch_mi_no", $sch_mi_no);
}

if(!empty($sch_work_state)) {
    $add_where .= " AND mi.work_state='{$sch_work_state}'";
    $smarty->assign("sch_work_state", $sch_work_state);
}

if(!empty($sch_parent_ord_no)) {
    $add_where .= " AND mi.parent_order_number LIKE '%{$sch_parent_ord_no}%'";
    $smarty->assign("sch_parent_ord_no", $sch_parent_ord_no);
}

if(!empty($sch_stock_date)) {
    $add_where .= " AND mi.stock_date = '{$sch_stock_date}'";
    $smarty->assign("sch_stock_date", $sch_stock_date);
}

if(!empty($sch_recipient)) {
    $add_where .= " AND mi.recipient LIKE '%{$sch_recipient}%'";
    $smarty->assign("sch_recipient", $sch_recipient);
}

if(!empty($sch_recipient_hp)) {
    $add_where .= " AND mi.recipient_hp LIKE '%{$sch_recipient_hp}%'";
    $smarty->assign("sch_recipient_hp", $sch_recipient_hp);
}

if(!empty($sch_option)) {
    $add_where .= " AND mi.mi_no IN(SELECT DISTINCT sub.mi_no FROM manufacturer_inspection_unit sub WHERE sub.sku LIKE '%{$sch_option}%')";
    $smarty->assign("sch_option", $sch_option);
}

if(!empty($sch_title)) {
    $add_where .= " AND mi.ins_title LIKE '%{$sch_title}%'";
    $smarty->assign("sch_title", $sch_title);
}

if(!empty($sch_is_improve)) {
    $add_where .= " AND mi.is_improve='{$sch_is_improve}'";
    $smarty->assign("sch_is_improve", $sch_is_improve);
}

if(!empty($sch_run_state)) {
    $add_where .= " AND mi.run_state='{$sch_run_state}'";
    $smarty->assign("sch_run_state", $sch_run_state);
}

if(!empty($sch_is_as_in)) {
    $add_where .= " AND mi.is_as_in='{$sch_is_as_in}'";
    $smarty->assign("sch_is_as_in", $sch_is_as_in);
}

if(!empty($sch_is_as_out)) {
    $add_where .= " AND mi.is_as_out='{$sch_is_as_out}'";
    $smarty->assign("sch_is_as_out", $sch_is_as_out);
}

# 페이징처리
$return_mi_total_sql    = "SELECT COUNT(mi_no) as cnt FROM manufacturer_inspection mi WHERE {$add_where}";
$return_mi_total_query  = mysqli_query($my_db, $return_mi_total_sql);
$return_mi_total_result = mysqli_fetch_assoc($return_mi_total_query);
$return_mi_total        = isset($return_mi_total_result['cnt']) ? $return_mi_total_result['cnt'] : 0;

$page       = isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 20;
$num 		= $page_type;
$offset 	= ($page-1) * $num;
$pagenum 	= ceil($return_mi_total/$num);

if ($page >= $pagenum){$page = $pagenum;}
if ($page <= 0){$page = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist   = pagelist($page, "out_return_mi_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $return_mi_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$return_mi_sql = "
    SELECT
        *,
        DATE_FORMAT(mi.regdate, '%Y/%m/%d') as reg_day,
        DATE_FORMAT(mi.regdate, '%H:%i') as reg_hour,
        (SELECT sub.c_name FROM company sub WHERE sub.c_no=mi.add_c_no) as add_c_name,
        (SELECT sub.s_name FROM staff sub WHERE sub.s_no=mi.reg_s_no) as reg_s_name,
        (SELECT sub.s_name FROM staff sub WHERE sub.s_no=mi.run_s_no) as run_s_name
    FROM manufacturer_inspection mi
    WHERE {$add_where}
    ORDER BY mi_no DESC
    LIMIT {$offset}, {$num}
";
$return_mi_query    = mysqli_query($my_db, $return_mi_sql);
$return_mi_list     = [];
$run_state_option   = getMiRunStateOption();
$is_improve_option  = getIsImproveOption();
while($return_mi = mysqli_fetch_assoc($return_mi_query))
{
    if (isset($return_mi['recipient_hp']) && !empty($return_mi['recipient_hp'])) {
        $f_hp = substr($return_mi['recipient_hp'], 0, 4);
        $e_hp = substr($return_mi['recipient_hp'], 7, 15);
        $return_mi['recipient_sc_hp'] = $f_hp . "***" . $e_hp;
    }

    $return_mi['is_improve_name']   = isset($is_improve_option[$return_mi['is_improve']]) ? $is_improve_option[$return_mi['is_improve']] : "";
    $return_mi['run_state_name']    = $run_state_option[$return_mi['run_state']];
    $return_mi['stock_date']        = ($return_mi['stock_date'] == '0000-00-00') ? "" : $return_mi['stock_date'];

    $return_mi_unit_sql    = "
        SELECT
            miu.sku,
            (SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=miu.sup_c_no) as sup_c_name,
            miu.quantity
        FROM manufacturer_inspection_unit miu
        WHERE miu.mi_no = '{$return_mi['mi_no']}'
    ";
    $return_mi_unit_query  = mysqli_query($my_db, $return_mi_unit_sql);
    $return_mi_unit_list   = [];
    while($mi_unit  = mysqli_fetch_assoc($return_mi_unit_query))
    {
        $return_mi_unit_list[] = array("option_info" => "{$mi_unit['sku']}::{$mi_unit['quantity']}", "sup_c_name" => $mi_unit['sup_c_name']);
    }
    $return_mi['return_mi_unit_list'] = $return_mi_unit_list;

    $return_mi_list[] = $return_mi;
}

$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("is_improve_option", $is_improve_option);
$smarty->assign("work_state_option", getWorkStateOption());
$smarty->assign("run_state_option", $run_state_option);
$smarty->assign("return_mi_list", $return_mi_list);

$smarty->display('out_return_mi_list.html');
?>
