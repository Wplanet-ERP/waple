<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_extra.php');

$wc_no = isset($_GET['wc_no']) ? $_GET['wc_no'] : "";

$certificate_view    = [];
$doc_type_option     = getCertDocTypeOption();
$company_seal_option = getCertCompanySealOption();

if(!empty($wc_no))
{
    $aes_unhex       = "(SELECT AES_DECRYPT(UNHEX(wc.cert_no), '{$aeskey}')) AS cert_no";
    $certificate_sql = "
        SELECT 
            *,
            (SELECT t.team_name FROM team t WHERE t.team_code=wc.req_team) as req_team_name,
            (SELECT s.my_c_no FROM staff s WHERE s.s_no=wc.req_s_no) as my_c_no,
            {$aes_unhex}
        FROM work_certificate as wc
        WHERE wc_no = '{$wc_no}'
    ";
    $certificate_query = mysqli_query($my_db, $certificate_sql);
    while($certificate_result = mysqli_fetch_assoc($certificate_query))
    {
        if($certificate_result['work_state'] != '6')
        {
            exit("<script>alert('인쇄 가능한 증명서가 아닙니다.');location.href='work_certificate_view.php?wc_no={$wc_no}'</script>");
        }

        if(!empty($certificate_result['cert_no']))
        {
            $cert_nos  = explode('-', $certificate_result['cert_no']);
            $cert_no_1 = $cert_nos[0];

            $cert_no_2_1 = $cert_no_2_2 = "";
            if(isset($cert_nos[1]))
            {
                $cert_no_2_1 = substr($cert_nos[1], 0, 1);
                $cert_no_2_2 = substr($cert_nos[1], 1);
            }

            if($certificate_result['cert_hide'] == '1'){
                $certificate_result['cert_no_text'] = $cert_no_1."-".$cert_no_2_1."******";
            }else{
                $certificate_result['cert_no_text'] = $certificate_result['cert_no'];
            }

        }

        $certificate_result['company_seal_name']    = $company_seal_option[$certificate_result['company_seal']]['seal_file'];
        $certificate_result['doc_type_name']        = $doc_type_option[$certificate_result['doc_type']];
        $certificate_view = $certificate_result;
    }
}
else
{
    exit("<script>alert('해당 증명서가 없습니다.');location.href='main.php'</script>");
}

$smarty->assign($certificate_view);
$smarty->display('work_certificate_print.html');
?>
