<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/evaluation.php');
require('inc/model/Staff.php');
require('inc/model/Evaluation.php');

# Model Init
$staff_model            = Staff::Factory();
$ev_system_model        = Evaluation::Factory();
$ev_relation_model      = Evaluation::Factory();
$ev_relation_model->setMainInit("evaluation_relation", "ev_r_no");
$ev_result_model        = Evaluation::Factory();
$ev_result_model->setMainInit("evaluation_system_result", "ev_result_no");

# Process 처리
$process = isset($_POST['process']) ? $_POST['process'] : "";

if($process == 'f_result_save')
{
    $ev_result_no   = isset($_POST['ev_result_no']) ? $_POST['ev_result_no'] : "";
    $ev_state       = isset($_POST['ev_state']) ? $_POST['ev_state'] : "";
    $value          = isset($_POST['value']) ? $_POST['value'] : "";
    $result_item    = $ev_result_model->getItem($ev_result_no);
    $ev_r_no        = $result_item['ev_r_no'];
    $ev_u_set_no    = $result_item['ev_u_set_no'];
    $hundred_points = 0;

    if($ev_state == '1')
        $hundred_points = $value*20;
    elseif($ev_state == '2')
        $hundred_points = $value*10;
    elseif($ev_state == '3')
        $hundred_points = $value;

    $upd_data = array("ev_result_no" => $ev_result_no, "evaluation_value" => $value, "hundred_points" => $hundred_points);
    if(!$ev_result_model->update($upd_data)){
        echo "자동 저장에 실패했습니다.";
    }
    $ev_relation_model->checkComplete($ev_r_no, $ev_u_set_no);
    exit;
}
elseif($process == 'f_result_check_save')
{
    $ev_result_no   = isset($_POST['ev_result_no']) ? $_POST['ev_result_no'] : "";
    $ev_state       = isset($_POST['ev_state']) ? $_POST['ev_state'] : "";
    $value          = isset($_POST['value']) ? $_POST['value'] : "";
    $result_item    = $ev_result_model->getItem($ev_result_no);
    $ev_r_no        = $result_item['ev_r_no'];
    $ev_u_set_no    = $result_item['ev_u_set_no'];

    $upd_data = array("ev_result_no" => $ev_result_no, "evaluation_value" => $value);
    if(!$ev_result_model->update($upd_data)){
        echo "자동 저장에 실패했습니다.";
    }
    $ev_relation_model->checkComplete($ev_r_no, $ev_u_set_no);
    exit;
}
elseif($process == 'f_result_text_save')
{
    $ev_result_no   = isset($_POST['ev_result_no']) ? $_POST['ev_result_no'] : "";
    $ev_state       = isset($_POST['ev_state']) ? $_POST['ev_state'] : "";
    $value          = isset($_POST['value']) ? addslashes(trim($_POST['value'])) : "";
    $result_item    = $ev_result_model->getItem($ev_result_no);
    $ev_r_no        = $result_item['ev_r_no'];
    $ev_u_set_no    = $result_item['ev_u_set_no'];

    $upd_data = array("ev_result_no" => $ev_result_no, "evaluation_value" => $value);
    if(!$ev_result_model->update($upd_data)){
        echo "자동 저장에 실패했습니다.";
    }
    $ev_relation_model->checkComplete($ev_r_no, $ev_u_set_no);
    exit;
}

# 평가지 및 평가자 정보
$ev_no      = isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$ev_r_no    = isset($_GET['ev_r_no']) ? $_GET['ev_r_no'] : "";
$page       = isset($_GET['page']) ? $_GET['page'] : "1";
# 기본 설정값
if(empty($ev_no)){
    echo "평가지 번호가 없습니다.";
    exit;
}
$evaluation_system  = $ev_system_model->getItem($ev_no);
$smarty->assign("evaluation_system", $evaluation_system);

$evaluation_relation = [];
if($ev_r_no == 0 && $evaluation_system['admin_s_no'] == $session_s_no)
{
    $smarty->assign("readonly", true);
}
else
{
    if(empty($ev_r_no)){
        echo "평가번호가 없습니다.";
        exit;
    }

    $evaluation_relation = $ev_relation_model->getItem($ev_r_no);
    $smarty->assign("evaluation_relation", $evaluation_relation);
    $ev_result_model->checkEvResultCreate($ev_no, $evaluation_system['ev_u_set_no'], $ev_r_no, $evaluation_relation['receiver_s_no'], $evaluation_relation['evaluator_s_no']);
}


# 페이지값
$eval_prev_page         = $page-1;
$eval_next_page         = $page+1;
$chk_max_page_sql       = "SELECT MAX(page) as page FROM evaluation_unit eu WHERE eu.ev_u_set_no='{$evaluation_system['ev_u_set_no']}' AND eu.active='1'";
$chk_max_page_query     = mysqli_query($my_db, $chk_max_page_sql);
$chk_max_page_result    = mysqli_fetch_assoc($chk_max_page_query);
$eval_max_page          = $chk_max_page_result['page'] > 0 ? $chk_max_page_result['page'] : 1;
$eval_page_img          = "";

if($page == '1'){
    $eval_page_img = "./images/evaluation/kpi/kpi_question1_subject.png";
}elseif($page == '2'){
    $eval_page_img = "./images/evaluation/kpi/kpi_question2_subject.png";
}elseif($page == '3'){
    $eval_page_img = "./images/evaluation/kpi/kpi_question3_subject.png";
}
$smarty->assign("eval_page_img", $eval_page_img);
$smarty->assign("eval_cur_page", $page);
$smarty->assign("eval_prev_page", $eval_prev_page);
$smarty->assign("eval_next_page", $eval_next_page);
$smarty->assign("eval_max_page", $eval_max_page);

$ev_self_text_list = [];
if($evaluation_relation)
{
    # 자기평가 기술서
    $evaluation_self_sql    = "SELECT * FROM evaluation_receiver_self WHERE ev_no='{$ev_no}' AND rec_s_no='{$evaluation_relation['receiver_s_no']}'";
    $evaluation_self_query  = mysqli_query($my_db, $evaluation_self_sql);
    while($evaluation_self=  mysqli_fetch_assoc($evaluation_self_query))
    {
        $ev_self_text_list[$evaluation_self['ev_u_no']] = array(
            "question"      => $evaluation_self['question'],
            "description"   => $evaluation_self['description'],
            "answer"        => $evaluation_self['answer'],
        );
    }
}

# 페이지 따른 질문 리스트
$evaluation_question_sql    = "
    SELECT 
        *,
        (SELECT esr.ev_result_no FROM evaluation_system_result esr WHERE esr.ev_no='{$ev_no}' AND esr.ev_u_no=eu.ev_u_no AND esr.receiver_s_no='{$evaluation_relation['receiver_s_no']}' AND esr.evaluator_s_no='{$evaluation_relation['evaluator_s_no']}') as result_no,
        (SELECT esr.evaluation_value FROM evaluation_system_result esr WHERE esr.ev_no='{$ev_no}' AND esr.ev_u_no=eu.ev_u_no AND esr.receiver_s_no='{$evaluation_relation['receiver_s_no']}' AND esr.evaluator_s_no='{$evaluation_relation['evaluator_s_no']}') as result_value 
    FROM evaluation_unit eu 
    WHERE eu.ev_u_set_no='{$evaluation_system['ev_u_set_no']}' AND eu.page='{$page}' AND eu.active='1'
    ORDER BY `order`
";
$evaluation_question_query  = mysqli_query($my_db, $evaluation_question_sql);
$evaluation_question_list   = [];
$evaluation_self_list       = [];
$evaluation_subject_list    = [];
$ev_unit_type_option        = getEvUnitTypeNumberOption();
$question_idx               = 1;

while($evaluation_question = mysqli_fetch_assoc($evaluation_question_query))
{
    if($evaluation_question['evaluation_state'] == '100')
    {
        $evaluation_self_list = array(
            "ev_u_no"       => $evaluation_question['ev_u_no'],
            "question"      => isset($ev_self_text_list[$evaluation_question['ev_u_no']]) ? $ev_self_text_list[$evaluation_question['ev_u_no']]['question'] : $evaluation_question['question'],
            "description"   => isset($ev_self_text_list[$evaluation_question['ev_u_no']]) ? $ev_self_text_list[$evaluation_question['ev_u_no']]['description'] : $evaluation_question['description'],
            "answer"        => isset($ev_self_text_list[$evaluation_question['ev_u_no']]) ? $ev_self_text_list[$evaluation_question['ev_u_no']]['answer'] : ""
        );
    }
    elseif($evaluation_question['evaluation_state'] == '99')
    {
        $evaluation_subject_list = array(
            "question"      => $evaluation_question['question'],
            "description"   => $evaluation_question['description'],
        );
    }
    else
    {
        $question_img = "";
        if($evaluation_system['ev_u_set_no'] == '9' || $evaluation_system['ev_u_set_no'] == '12')
        {
            $chk_file_name = "/v1/images/evaluation/kpi/kpi_question{$page}_{$question_idx}.png";
            $chk_file_path = $_SERVER['DOCUMENT_ROOT'].$chk_file_name;
            if(file_exists($chk_file_path)){
                $question_img = $chk_file_name;
            }
        }

        $ev_unit_base_option = $ev_unit_type_option;
        if(($evaluation_question['evaluation_state'] == 8 || $evaluation_question['evaluation_state'] == 9) && $evaluation_question['choice_items'])
        {
            $choice_items       = explode("||", $evaluation_question['choice_items']);
            $choice_item_list   = [];
            $choice_idx         = 1;
            foreach($choice_items as $choice_item){
                $choice_item_list[$choice_idx] = trim($choice_item);
                $choice_idx++;
            }
            $ev_unit_base_option[$evaluation_question['evaluation_state']]['option'] = $choice_item_list;
        }

        $evaluation_question_list[] = array(
            "ev_u_no"           => $evaluation_question['ev_u_no'],
            "ev_u_state"        => $evaluation_question['evaluation_state'],
            "question"          => $evaluation_question['question'],
            "question_idx"      => $question_idx,
            "question_img"      => $question_img,
            "question_option"   => isset($ev_unit_base_option[$evaluation_question['evaluation_state']]) ? $ev_unit_base_option[$evaluation_question['evaluation_state']] : "",
            "description"       => $evaluation_question['description'],
            "essential"         => $evaluation_question['essential'],
            "ev_result_no"      => $evaluation_question['result_no'],
            "ev_value"          => $evaluation_question['result_value'],
            "ev_value_list"     => explode("||",$evaluation_question['result_value']),
        );

        $question_idx++;
    }
}

$smarty->assign("evaluation_self_list", $evaluation_self_list);
$smarty->assign("evaluation_subject_list", $evaluation_subject_list);
$smarty->assign("evaluation_question_list", $evaluation_question_list);

$smarty->display('evaluation_page_iframe.html');

?>
