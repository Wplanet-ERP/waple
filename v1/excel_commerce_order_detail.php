<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

/** 검색조건 START */
$commerce_order_no        = isset($_GET['set_no']) ? $_GET['set_no'] : "";
$commerce_order_set_sql   = "SELECT * FROM commerce_order_set WHERE `no`='{$commerce_order_no}' LIMIT 1";
$commerce_order_set_query = mysqli_query($my_db, $commerce_order_set_sql);
$commerce_order_set       = mysqli_fetch_assoc($commerce_order_set_query);
$commerce_order_set_title = $commerce_order_set['sup_c_name']." [{$commerce_order_set['order_count']}]차";
$commerce_order_set_title = str_replace(".", " ", $commerce_order_set_title);
$commerce_order_set_title = str_replace(",", "", $commerce_order_set_title);


$com_rep_sql 	= "SELECT co.option_no, (SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.no=co.option_no) AS option_name, SUM(co.quantity) as qty FROM commerce_order co WHERE co.set_no='{$commerce_order_no}' AND co.type='request' GROUP BY co.option_no";
$com_rep_query 	= mysqli_query($my_db, $com_rep_sql);
$com_rep_list 	= [];
while($com_rep_result = mysqli_fetch_array($com_rep_query))
{
    $com_rep_list[$com_rep_result['option_no']] = array(
        'option_name' 	 => $com_rep_result['option_name'],
        'req_qty' 	  	 => $com_rep_result['qty'],
        'sup_qty' 	  	 => 0,
        'sub_qty' 	  	 => 0,
        'date_data_list' => []
    );
}

$commerce_order_date_sql   = "SELECT option_no, quantity, sup_date FROM commerce_order co WHERE co.set_no='{$commerce_order_no}' AND `type`='supply' ORDER BY sup_date ASC";
$commerce_order_date_query = mysqli_query($my_db, $commerce_order_date_sql);
$comm_date_list         = [];
while($commerce_order_data = mysqli_fetch_array($commerce_order_date_query))
{
    $com_rep_list[$commerce_order_data['option_no']]['sup_qty'] += $commerce_order_data['quantity'];
    $comm_date_list[] = $commerce_order_data['sup_date'];
}


#입고상세내역 계산
$date_count = 1;
$com_rep_detail_list = [];
$com_rep_total       = [];
$comm_date_total     = [];
if(!empty($com_rep_list))
{
    foreach($com_rep_list as $key => $com_rep)
    {
        $com_rep_detail_list[$key] = array(
            'option_name' 	 => $com_rep['option_name'],
            'req_qty' 	  	 => $com_rep['req_qty'],
            'sup_qty' 	  	 => $com_rep['sup_qty'],
            'sub_qty' 	  	 => ($com_rep['req_qty']-$com_rep['sup_qty']),
            'date_data_list' => []
        );

        $com_rep_total['req_total'] += $com_rep['req_qty'];
        $com_rep_total['sup_total'] += $com_rep['sup_qty'];
        $com_rep_total['sub_total'] += ($com_rep['req_qty']-$com_rep['sup_qty']);
    }

    if(!empty($comm_date_list) && !empty($com_rep_detail_list))
    {
        $comm_date_list = array_unique($comm_date_list);

        $comm_date_sql   = "SELECT option_no, sup_date, SUM(quantity) as qty FROM commerce_order WHERE set_no='{$commerce_order_no}' AND `type` = 'supply' GROUP BY option_no, sup_date";
        $comm_date_query = mysqli_query($my_db, $comm_date_sql);
        $comm_date_data  = [];
        while($comm_date_result = mysqli_fetch_assoc($comm_date_query))
        {
            $comm_date_data[$comm_date_result['option_no']][$comm_date_result['sup_date']] = $comm_date_result['qty'];
        }

        foreach($comm_date_list as $comm_date)
        {
            foreach($com_rep_detail_list as $option_key => $com_rep)
            {
                $com_date_val = isset($comm_date_data[$option_key]) && isset($comm_date_data[$option_key][$comm_date]) ? $comm_date_data[$option_key][$comm_date] : 0;
                $com_rep['date_data_list'][] = $com_date_val;

                $comm_date_total[$comm_date] += $com_date_val;
                $com_rep_detail_list[$option_key] = $com_rep;
            }
        }

        $date_count = count($comm_date_list);
    }
}

# 엑셀
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$workSheet = $objPHPExcel->setActiveSheetIndex(0);
$eng_abc  = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
$eng_abcc = [];
$eng_list = $eng_abc;
foreach($eng_abc as $a){
    foreach($eng_abc as $b){
        $eng_list[] = $a.$b;
        $eng_abcc[] = $a.$b;
    }
}

foreach($eng_abcc as $aa){
    foreach($eng_abc as $b){
        $eng_list[] = $aa.$b;
    }
}

$excel_column_list  = [];
$excel_column_count = 2 + $date_count + 2;
for($i=0;$i<$excel_column_count;$i++)
{
    $excel_column_list[] = $eng_list[$i];
}

$total_req_idx      = count($excel_column_list) - 2;
$total_req_column   = $excel_column_list[$total_req_idx];
$last_column_idx    = $total_req_idx+1;
$last_column        = $excel_column_list[$last_column_idx];

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');
$background_color = "00262626";

$workSheet->mergeCells("A1:A2");
$workSheet->mergeCells("B1:B2");
$workSheet->setCellValue("A1", "구성품목명");
$workSheet->setCellValue("B1", "발주수량\r\n(A)");

$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_column}2")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_column}2")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_column}2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_column}2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_column}2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$workSheet->getColumnDimension("A")->setWidth(40);
$workSheet->getColumnDimension("B")->setWidth(20);

sort($comm_date_list);

$excel_date_list = [];
if($com_rep_detail_list && $comm_date_list)
{
    $date_idx = 0;
    $last_date_column = "D";
    for($k=2;$k<$total_req_idx;$k++)
    {
        $workSheet->setCellValue("{$excel_column_list[$k]}2", $comm_date_list[$date_idx]);
        $workSheet->getColumnDimension("{$excel_column_list[$k]}")->setWidth(15);
        $last_date_column = $excel_column_list[$k];

        $excel_date_list[] = $excel_column_list[$k];
        $date_idx++;
    }
    $workSheet->mergeCells("C1:{$last_date_column}1");
    $workSheet->setCellValue("C1", "기간별 입고수량");
    $workSheet->mergeCells("{$total_req_column}1:{$total_req_column}2");
    $workSheet->mergeCells("{$last_column}1:{$last_column}2");
    $workSheet->setCellValue("{$total_req_column}1", "총입고수량\r\n(B)");
    $workSheet->setCellValue("{$last_column}1", "미 입고수량\r\n(A-B)");

    $workSheet->getColumnDimension("{$total_req_column}")->setWidth(20);
    $workSheet->getColumnDimension("{$last_column}")->setWidth(20);
}else{
    $workSheet->setCellValue("C1", "기간별 입고수량");
    $workSheet->setCellValue("C2", "-");
    $workSheet->mergeCells("D1:D2");
    $workSheet->setCellValue("D1", "총입고수량\r\n(B)");
    $workSheet->mergeCells("E1:E2");
    $workSheet->setCellValue("E1", "미 입고수량\r\n(A-B)");
    $workSheet->getColumnDimension("C")->setWidth(25);
    $workSheet->getColumnDimension("D")->setWidth(20);
    $workSheet->getColumnDimension("E")->setWidth(20);
}


$idx = 3;
$last_field_idx = count($com_rep_detail_list)+$idx;
$last_field     = $last_column.$last_field_idx;

if($com_rep_detail_list && $comm_date_list)
{
    foreach($com_rep_detail_list as $com_rep_detail)
    {
        $workSheet->setCellValue("A{$idx}", $com_rep_detail['option_name']);
        $workSheet->setCellValue("B{$idx}", number_format($com_rep_detail['req_qty'], 0));

        $date_dat_idx = 0;

        foreach($com_rep_detail['date_data_list'] as $date_data)
        {
            $workSheet->setCellValue("{$excel_date_list[$date_dat_idx]}{$idx}", $date_data);
            $date_dat_idx++;
        }
        $workSheet->setCellValue("{$total_req_column}{$idx}", number_format($com_rep_detail['sup_qty'], 0));
        $workSheet->setCellValue("{$last_column}{$idx}", number_format($com_rep_detail['sub_qty'], 0));

        $idx++;
    }

    $objPHPExcel->getActiveSheet()->getStyle("B3:{$last_field}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    $workSheet->setCellValue("A{$idx}", "합계");
    $workSheet->setCellValue("B{$idx}", number_format($com_rep_total['req_total'], 0));
    $date_dat_idx = 0;
    foreach ($comm_date_list as $comm_date)
    {
        $workSheet->setCellValue("{$excel_date_list[$date_dat_idx]}{$idx}", number_format($comm_date_total[$comm_date], 0));
        $date_dat_idx++;
    }
    $workSheet->setCellValue("{$total_req_column}{$idx}", number_format($com_rep_total['sup_total'], 0));
    $workSheet->setCellValue("{$last_column}{$idx}", number_format($com_rep_total['sub_total'], 0));

    $background_color = "00D9D9D9";
    $objPHPExcel->getActiveSheet()->getStyle("A{$idx}:{$last_column}{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
    $objPHPExcel->getActiveSheet()->getStyle("A1:{$last_column}{$idx}")->applyFromArray($styleArray);
}
else
{

    foreach($com_rep_detail_list as $com_rep_detail)
    {
        $workSheet->setCellValue("A{$idx}", $com_rep_detail['option_name']);
        $workSheet->setCellValue("B{$idx}", number_format($com_rep_detail['req_qty'], 0));
        $workSheet->setCellValue("C{$idx}", 0);
        $workSheet->setCellValue("D{$idx}", number_format($com_rep_detail['sup_qty'], 0));
        $workSheet->setCellValue("E{$idx}", number_format($com_rep_detail['sub_qty'], 0));

        $idx++;
    }

    $workSheet->setCellValue("A{$idx}", "합계");
    $workSheet->setCellValue("B{$idx}", number_format($com_rep_total['req_total'], 0));
    $workSheet->setCellValue("C{$idx}", 0);
    $workSheet->setCellValue("D{$idx}", number_format($com_rep_total['sup_total'], 0));
    $workSheet->setCellValue("E{$idx}", number_format($com_rep_total['sub_total'], 0));

    $background_color = "00D9D9D9";
    $objPHPExcel->getActiveSheet()->getStyle("A{$idx}:E{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB($background_color);
    $objPHPExcel->getActiveSheet()->getStyle("A1:E{$idx}")->applyFromArray($styleArray);

    $objPHPExcel->getActiveSheet()->getStyle("B3:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
}


$objPHPExcel->getActiveSheet()->setTitle("입고상세내역");

$excel_filename=iconv('UTF-8','EUC-KR',$commerce_order_set_title."_입고상세내역.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

?>
