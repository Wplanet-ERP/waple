<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_upload.php');
require('inc/helper/chat.php');
require('inc/model/Team.php');
require('inc/model/Staff.php');
require('inc/model/WapleChat.php');

# Init Model
$chat_model         = WapleChat::Factory();
$content_model      = WapleChat::Factory();
$content_model->setMainInit("waple_chat_content", "wcc_no");
$staff_model        = Staff::Factory();

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";

if($process == "add_chat")
{
    $send_staff     = isset($_POST['send_staff_no']) ? $_POST['send_staff_no'] : "";
    $send_staff_name= isset($_POST['send_staff']) ? $_POST['send_staff'] : "";
    $receiver_staff = isset($_POST['receiver_staff']) ? $_POST['receiver_staff'] : "";
    $chat_type      = isset($_POST['chat_type']) ? $_POST['chat_type'] : "";
    $content        = isset($_POST['chat_content']) ? addslashes(trim($_POST['chat_content'])) : "";
    $regdate        = date("Y-m-d H:i:s");
    $chk_chat_no    = "";
    $alert_type     = "";
    $content_staff  = "";

    if($chat_type == "1") {
        $chk_chat_no    = "1";
        $alert_type     = "1";
        $content_staff  = $send_staff;
    }
    elseif($chat_type == "2") {
        $chk_chat_no    = "2";
        $alert_type     = "99";
        $content_staff  = $receiver_staff;
    }
    elseif($chat_type == "3"){
        $chk_chat_sql       = "SELECT wc_no FROM waple_chat_user WHERE wc_no IN(SELECT wc_no FROM waple_chat_user WHERE `user`='{$send_staff}') AND `user` = '{$receiver_staff}'";
        $chk_chat_query     = mysqli_query($my_db, $chk_chat_sql);
        $chk_chat_result    = mysqli_fetch_assoc($chk_chat_query);
        $chk_chat_no        = isset($chk_chat_result['wc_no']) && !empty($chk_chat_result['wc_no']) ? $chk_chat_result['wc_no'] : "";
        $alert_type         = "100";
        $content_staff      = $send_staff;

        $receiver_staff_item = $staff_model->getStaff($receiver_staff);
        if(empty($chk_chat_no))
        {
            $chat_data  = array(
                "title"     => "1:1메세지",
                "type"      => "3",
                "info"      => "{$send_staff_name}, {$receiver_staff_item['s_name']} 1:1 메세지방",
                "regdate"   => $regdate
            );
            $chat_model->insert($chat_data);

            $chk_chat_no = $chat_model->getInsertId();

            $chat_user_ins_sql = "INSERT INTO waple_chat_user(`wc_no`, `user`) VALUES('{$chk_chat_no}','{$send_staff}'), ('{$chk_chat_no}','{$receiver_staff}')";
            mysqli_query($my_db, $chat_user_ins_sql);
        }
    }

    $content_data = array(
        "wc_no"         => $chk_chat_no,
        "s_no"          => $content_staff,
        "alert_type"    => $alert_type,
        "content"       => $content,
        "regdate"       => date("Y-m-d H:i:s")
    );

    # 파일첨부
    $file_names = isset($_POST['file_name']) ? $_POST['file_name'] : "";
    $file_paths = isset($_POST['file_path']) ? $_POST['file_path'] : "";

    $move_file_name = "";
    $move_file_path = "";
    if(!empty($file_names) && !empty($file_paths))
    {
        $move_file_paths = move_store_files($file_paths, "dropzone_tmp", "chat");
        $move_file_name  = implode(',', $file_names);
        $move_file_path  = implode(',', $move_file_paths);
    }

    if(!empty($move_file_path) && !empty($move_file_name)){
        $content_data['file_name'] = $move_file_name;
        $content_data['file_path'] = $move_file_path;
    }

    if($content_model->insert($content_data)){
        exit("<script>alert('알림톡 등록에 성공했습니다');parent.location.href='waple_chat.php';</script>");
    }else{
        exit("<script>alert('알림톡 등록에 실패패했습니');parent.location.href='waple_chat.php';</script>");
    }
}

$is_admin           = ($session_team == '00211') ? true : false;
$staff_model        = Staff::Factory();
$staff_name_list    = $staff_model->getStaffAndTeamNameList("s_name", "1");
$send_staff         = $staff_model->getStaff($session_s_no);

$smarty->assign("is_admin", $is_admin);
$smarty->assign("send_staff", $send_staff);
$smarty->assign("staff_name_list", $staff_name_list);
$smarty->assign("waple_chat_type_option", getChatTypeOption());

$smarty->display('waple_chat_send.html');
?>
