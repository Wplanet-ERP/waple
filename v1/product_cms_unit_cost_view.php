<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_price.php');
require('inc/model/ProductCmsUnit.php');

$process = (isset($_POST['process'])) ? $_POST['process'] : "";

if($process == "add_unit_cost")
{
	$prd_unit  		= $_POST['prd_unit'];
    $cost_date  	= $_POST['cost_date'];
    $sup_c_no  	    = isset($_POST['sup_c_no']) ? $_POST['sup_c_no'] : 0;
    $sup_price 		= str_replace(",","",trim($_POST['sup_price']));
    $sup_price_vat  = isset($_POST['sup_price_vat']) && !empty($_POST['sup_price_vat']) ? str_replace(",","",trim($_POST['sup_price_vat'])) : $sup_price;
    $memo  	        = isset($_POST['memo']) ? addslashes(trim($_POST['memo'])) : "";

    if(empty($prd_unit) || empty($cost_date) || empty($sup_price) || empty($sup_price_vat)){
        exit("<script>alert('등록할 정보가 없습니다.');location.href='product_cms_unit_cost_list.php';</script>");
	}

    $ins_sql = "
		INSERT INTO product_cms_unit_cost_control SET 
			prd_unit  		= '{$prd_unit}',
			cost_date 		= '{$cost_date}',
			sup_c_no 		= '{$sup_c_no}',
			sup_price 		= '{$sup_price}',
			sup_price_vat 	= '{$sup_price_vat}',
			memo 	        = '{$memo}',
			regdate 		= now()
	";

    if(mysqli_query($my_db, $ins_sql))
    {
        $chk_sql    = "SELECT COUNT(*) as cnt FROM product_cms_unit_cost_control WHERE cost_date > '{$cost_date}' AND prd_unit='{$prd_unit}'";
        $chk_query  = mysqli_query($my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        if($chk_result['cnt'] == 0)
        {
            $upd_sql    = "UPDATE product_cms_unit SET sup_price='{$sup_price}', sup_price_vat='{$sup_price_vat}' WHERE `no`='{$prd_unit}'";
            mysqli_query($my_db, $upd_sql);
        }

        exit("<script>alert('발주 원가관리에 추가 하였습니다');location.href='product_cms_unit_cost_view.php?prd_unit={$prd_unit}';</script>");
    } else {
        exit("<script>alert('발주 원가관리 추가에 실패 하였습니다');history.back();</script>");
    }
}
elseif($process == 'memo')
{
    $cc_no  = isset($_POST['cc_no']) ? $_POST['cc_no'] : "";
    $value  = isset($_POST['val']) ? $_POST['val'] : "";

    if(empty($cc_no))
    {
        echo "메모 저장에 실패 하였습니다.";
        exit;
    }

    $upd_sql = "UPDATE product_cms_unit_cost_control SET memo='".addslashes(trim($value))."' WHERE cc_no = '{$cc_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "메모 저장에 실패 하였습니다.";
    else
        echo "메모가 저장 되었습니다.";
    exit;
}
elseif($process == 'f_cost_date')
{
    $cc_no  = isset($_POST['cc_no']) ? $_POST['cc_no'] : "";
    $value  = isset($_POST['val']) ? $_POST['val'] : "";

    if(empty($cc_no))
    {
        echo "단가시작일 변경에 실패 하였습니다.";
        exit;
    }

    $upd_sql = "UPDATE product_cms_unit_cost_control SET cost_date='{$value}' WHERE cc_no = '{$cc_no}'";

    if (!mysqli_query($my_db, $upd_sql))
        echo "단가시작일 변경에 실패 하였습니다.";
    else
        echo "단가시작일이 변경되었습니다.";
    exit;
}
elseif($process == 'f_sup_price')
{
    $cc_no  = isset($_POST['cc_no']) ? $_POST['cc_no'] : "";

    if(empty($cc_no)) {
        echo "공급단가 변경에 실패 하였습니다.";
        exit;
    }

    $chk_sql        = "SELECT *, ,(SELECT c.license_type FROM company c WHERE c.c_no=pcu.sup_c_no) as license_type FROM product_cms_unit_cost_control pcu WHERE cc_no='{$cc_no}'";
    $chk_query      = mysqli_query($my_db, $chk_sql);
    $chk_result     = mysqli_fetch_assoc($chk_query);
    $prd_unit  		= $chk_result['prd_unit'];
    $cost_date  	= $chk_result['cost_date'];
    $sup_price_vat  = str_replace(",","",trim($_POST['val']));
    $sup_price      = $sup_price_vat;

    if($chk_result['license_type'] != "4"){
        $tax_price = $sup_price_vat / ((100 + 10) / 10);
        $sup_price = round($sup_price_vat - $tax_price);
    }

    $upd_sql = "UPDATE product_cms_unit_cost_control SET sup_price='{$sup_price}', sup_price_vat='{$sup_price_vat}' WHERE cc_no = '{$cc_no}'";

    if (!mysqli_query($my_db, $upd_sql)){
        echo "공급단가 변경에 실패 하였습니다.";
    }else{

        $chk_sql    = "SELECT COUNT(*) as cnt FROM product_cms_unit_cost_control WHERE cost_date > '{$cost_date}' AND prd_unit='{$prd_unit}'";
        $chk_query  = mysqli_query($my_db, $chk_sql);
        $chk_result = mysqli_fetch_assoc($chk_query);

        if($chk_result['cnt'] == 0)
        {
            $upd_sql    = "UPDATE product_cms_unit SET sup_price='{$sup_price}', sup_price_vat='{$sup_price_vat}' WHERE `no`='{$prd_unit}'";
            mysqli_query($my_db, $upd_sql);
        }

        echo "공급단가가 변경되었습니다.";
        exit;
    }
}

# 검색쿼리
$prd_unit       = isset($_GET['prd_unit']) ? $_GET['prd_unit'] : "";
$prd_unit_model = ProductCmsUnit::Factory();

if(empty($prd_unit)){
    exit("<script>alert('구성품 번호가 없습니다.');location.href='product_cms_unit_cost_list.php';</script>");
}

$product_cms_unit_sql   = "SELECT * ,(SELECT c.license_type FROM company c WHERE c.c_no=pcu.sup_c_no) as license_type FROM product_cms_unit pcu WHERE `no`='{$prd_unit}'";
$product_cms_unit_query = mysqli_query($my_db, $product_cms_unit_sql);
$product_cms_unit       = mysqli_fetch_assoc($product_cms_unit_query);

if(empty($product_cms_unit)){
    exit("<script>alert('발주원가 관리 구성품이 아닙니다.');location.href='product_cms_unit_cost_list.php';</script>");
}

$is_editable = false;
if($session_s_no == $product_cms_unit['ord_s_no'] || $session_s_no == $product_cms_unit['ord_sub_s_no'] || $session_s_no == $product_cms_unit['ord_third_s_no']){
    $is_editable = true;
}
$smarty->assign("is_editable", $is_editable);

$sch_sup_c_list 	= $prd_unit_model->getDistinctUnitCompanyData('sup_c_no');
$sch_brand_list 	= $prd_unit_model->getDistinctUnitCompanyData("brand");
$is_usd_price       = ($product_cms_unit['license_type'] == '4') ? true : false;

// 리스트 쿼리
$product_cms_unit_cost_sql = "
	SELECT
	   	pcucc.cc_no,
		pcucc.prd_unit,
		pcu.brand,
		pcu.ord_s_no,
		pcu.ord_sub_s_no,
		pcu.ord_third_s_no,
		pcucc.sup_c_no,
		(SELECT c.c_name FROM company c WHERE c.c_no=pcucc.sup_c_no) as sup_c_name,
		(SELECT c.license_type FROM company c WHERE c.c_no=pcucc.sup_c_no) as license_type,
		(SELECT s.s_name FROM staff s WHERE pcu.ord_s_no=s.s_no) as ord_s_name,
		pcu.option_name,
	    pcucc.cost_date,
	    pcucc.sup_price_vat,
	    pcucc.memo
	FROM product_cms_unit_cost_control as pcucc
	LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcucc.prd_unit
	WHERE pcucc.prd_unit='{$prd_unit}'
	ORDER BY pcucc.cost_date ASC
";
$product_cms_unit_cost_query = mysqli_query($my_db, $product_cms_unit_cost_sql);
$product_cms_unit_cost_list  = [];
$product_cms_unit_ins_data   = [];
$chk_idx        = 0;
$first_price    = 0;
while($product_cms_unit_cost = mysqli_fetch_assoc($product_cms_unit_cost_query))
{
    $sup_price = ($is_usd_price) ? getUsdFormatPrice($product_cms_unit_cost['sup_price_vat']) : getKrwFormatPrice($product_cms_unit_cost['sup_price_vat']);
	$product_cms_unit_cost_list[$product_cms_unit_cost['cc_no']] =
		array(
			"cc_no" 			=> $product_cms_unit_cost['cc_no'],
			"prd_unit" 			=> $product_cms_unit_cost['prd_unit'],
			"brand" 	  		=> $sch_brand_list[$product_cms_unit_cost['brand']],
			"sup_c_name"		=> $product_cms_unit_cost['sup_c_name'],
			"ord_s_no"			=> $product_cms_unit_cost['ord_s_no'],
			"ord_s_name"		=> $product_cms_unit_cost['ord_s_name'],
			"ord_sub_s_no"		=> $product_cms_unit_cost['ord_sub_s_no'],
			"ord_third_s_no"	=> $product_cms_unit_cost['ord_third_s_no'],
			"option_name" 		=> $product_cms_unit_cost['option_name'],
			"cost_date"			=> $product_cms_unit_cost['cost_date'],
			"memo"			    => $product_cms_unit_cost['memo'],
			"sup_price"			=> $sup_price,
			"prev_price"		=> "",
			"prev_price_text" 	=> "",
			"prev_per"		 	=> ""
		);

    if($chk_idx == 0)
    {
        $product_cms_unit_cost['brand_name']    = $sch_brand_list[$product_cms_unit_cost['brand']];
        $first_price = $product_cms_unit_cost['sup_price_vat'];
        $product_cms_unit_ins_data = $product_cms_unit_cost;
	}
	elseif($chk_idx > 0)
    {
        $prev_price = $product_cms_unit_cost['sup_price_vat']-$first_price;
        $product_cms_unit_cost_list[$product_cms_unit_cost['cc_no']]['prev_price'] 	    = $prev_price;
        $product_cms_unit_cost_list[$product_cms_unit_cost['cc_no']]['prev_price_text'] = ($product_cms_unit_cost['sup_price_vat']-$first_price)*-1;
        $product_cms_unit_cost_list[$product_cms_unit_cost['cc_no']]['prev_per']   	    = round((1-($first_price/$product_cms_unit_cost['sup_price_vat']))*100, 2);

        $first_price = $product_cms_unit_cost['sup_price_vat'];
        $product_cms_unit_ins_data['sup_c_no']      = $product_cms_unit_cost['sup_c_no'];
        $product_cms_unit_ins_data['sup_c_name']    = $product_cms_unit_cost['sup_c_name'];
    }
    $chk_idx++;
}

$product_cms_unit_cost_list = array_reverse($product_cms_unit_cost_list);

$smarty->assign("prd_unit", $prd_unit);
$smarty->assign("is_usd_price", $is_usd_price);
$smarty->assign("product_cms_unit", $product_cms_unit_ins_data);
$smarty->assign("product_cms_unit_cost_list", $product_cms_unit_cost_list);
$smarty->display('product_cms_unit_cost_view.html');

?>
