<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
),
),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
),
);


// 프로모션 구분 텍스트
$kind_name=array('','체험단','기자단','배송체험');

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="";

$search_kind = isset($_GET['skind'])?$_GET['skind']:"";
$f_location1_get=isset($_GET['f_location1'])?$_GET['f_location1']:"";
$f_location2_get=isset($_GET['f_location2'])?$_GET['f_location2']:"";
$search_stype=isset($_GET['stype'])?$_GET['stype']:"";
$search_ssend=isset($_GET['ssend'])?$_GET['ssend']:"";
$search_scompany = isset($_GET['scompany'])?$_GET['scompany']:"";

if(!empty($search_kind)) {
	$add_where .= " AND p.kind = '".$search_kind."'";
	$smarty->assign("skind", $search_kind);
}

if(!empty($f_location1_get)) {
	$add_where .= " AND p.c_no in (select c_no from company where location1='".$f_location1_get."')";
	$smarty->assign("f_location1", $f_location1_get);
}

if(!empty($f_location2_get)) {
	$add_where .= " AND p.c_no in (select c_no from company where location2='".$f_location2_get."')";
	$smarty->assign("f_location2", $f_location2_get);
}

if(!empty($search_stype)) {	
	if ($search_stype != "all")
		$add_where .= " AND r.{$search_stype} > ''";
	$smarty->assign("stype", $search_stype);
}

if(!empty($search_ssend)) {
	if ($search_ssend != "all")
		$add_where .= " AND r.send_yn = '{$search_ssend}'";
	$smarty->assign("ssend", $search_ssend);
}

if(!empty($search_scompany)) {
	$add_where.=" AND p.company like '%".$search_scompany."%'";
	$smarty->assign("scompany",$search_scompany);
}

// 2차 분류에 값이 있는 경우 2차 지역 분류 가져오기(2차)
if($f_location2_get != "" || $f_location1_get != ""){
	$sql="select k_name,k_name_code,k_parent from kind where k_code='location' and k_parent='".$f_location1_get."'";
	$sql=mysqli_query($my_db,$sql);
	while($result=mysqli_fetch_array($sql)) {
		$editlocation[]=array(
			"location_name"=>trim($result['k_name']),
			"location_code"=>trim($result['k_name_code']),
			"location_parent"=>trim($result['k_parent'])
		);
		$smarty->assign("editlocation",$editlocation);
	}
}

// 정렬순서 토글 & 필드 지정
$add_orderby.=" r.r_no DESC";

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "no")
->setCellValue('B1', "구분")
->setCellValue('C1', "CODE")
->setCellValue('D1', "지역1")
->setCellValue('E1', "지역2")
->setCellValue('F1', "업체명")
->setCellValue('G1', "네이버 쪽지	안내")
->setCellValue('H1', "문자	안내")
->setCellValue('I1', "안내	발송여부")
->setCellValue('J1', "요청 날짜");

// 리스트 내용
$i=2;
$ttamount=0;

$sql="
	SELECT p.p_state AS p_state, p.kind AS kind, p.promotion_code AS promotion_code, p.c_no AS c_no, p.name AS staff_name, p.pres_reward AS pres_reward,
		(select k_name from kind k where k.k_name_code=(select location1 from company c where c.c_no=p.c_no)) as location1,
		(select k_name from kind k where k.k_name_code=(select location2 from company c where c.c_no=p.c_no)) as location2,
		r.r_no,
		r.naver_id,
		r.hp,
		r.send_yn,
		r.regdate
	FROM promotion p, reservation r
	WHERE
		p.p_no=r.p_no
		{$add_where}
	ORDER BY
		$add_orderby
	";
	
//echo "<br><br><br>".$sql;
$idx_no=1;

$query=mysqli_query($my_db,$sql);
while($data=mysqli_fetch_array($query)) {
	
	$regdate = substr($data['regdate'], 0, 16);

	/* c_no로 업체명 가져오기 Start */
	$company_sql="select c_name from company where c_no='".$data['c_no']."'";
	$company_query=mysqli_query($my_db,$company_sql);
	$company_data=mysqli_fetch_array($company_query); //$company_data['c_name']
	/* c_no로 업체명 가져오기 End */


	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$i, $data['r_no'])
	->setCellValue('B'.$i, $kind_name[$data['kind']])
	->setCellValue('C'.$i, $data['promotion_code'])
	->setCellValue('D'.$i, $data['location1'])	
	->setCellValue('E'.$i, $data['location2'])
	->setCellValue('F'.$i, $company_data['c_name'])
	->setCellValue('G'.$i, $data['naver_id'])
	->setCellValue('H'.$i, $data['hp'])
	->setCellValue('I'.$i, $data['send_yn'] == 'Y' ? '발송' : '미발송')
	->setCellValue('J'.$i, $regdate);

	$i = $i+1;
	$idx_no++;
}


if($i > 1)
	$i = $i-1;
															
// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->setTitle('알림 요청 리스트');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="reservation_list_'.$today.'.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;