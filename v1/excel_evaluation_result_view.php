<?php
require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

$ev_no 	= isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$r_s_no = isset($_GET['r_s_no']) ? $_GET['r_s_no'] : "";

if($r_s_no == "" || $session_s_no == $r_s_no){ //대상이 없거나 본인일 경우
	$smarty->display('access_company_error.html');
	exit;
}

// 접근 권한
if (!(permissionNameCheck($session_permission,"재무관리자") || permissionNameCheck($session_permission,"대표")))
{
	$smarty->display('access_error.html');
	exit;
}

if (!(permissionNameCheck($session_permission,"대표")) && $session_s_no == $r_s_no)
{
	$smarty->display('access_error.html');
	exit;
}


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
	->setLastModifiedBy("Maarten Balliauw")
	->setTitle("Office 2007 XLSX Test Document")
	->setSubject("Office 2007 XLSX Test Document")
	->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	->setKeywords("office 2007 openxml php")
	->setCategory("Evaluation Result");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

function setBorder($sheet, $row)
{
	$sheet->getStyle($row)->applyFromArray(
		array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)
		)
	);
}

$sheet = $objPHPExcel->setActiveSheetIndex(0);
$sheet->getDefaultStyle()->getFont()
	->setName('맑은 고딕')
	->setSize(10);

// Gubun 값 및 Text Question
$ev_txt_gubun = array(
	'241' => '고객만족',
	'242' => '고객만족',
	'244' => '고객만족',
	'246' => '고객만족',
	'247' => '고객만족',
	'248' => '학습',
	'249' => '학습',
	'251' => '학습',
	'253' => '학습',
	'254' => '학습',
	'255' => '소통과배려',
	'256' => '소통과배려',
	'258' => '소통과배려',
	'260' => '소통과배려',
	'261' => '소통과배려',
    '476' => '고객만족',
    '477' => '고객만족',
    '478' => '고객만족',
    '479' => '고객만족',
    '481' => '학습',
    '482' => '학습',
    '483' => '학습',
    '485' => '소통과배려',
    '486' => '소통과배려',
    '487' => '소통과배려',
    '488' => '소통과배려',
);
$ev_num_question = [];
$ev_txt_question = [];


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$ev_self_result 	  = []; // 평가항목 및 평정표(피평가자)
$ev_result_sum_list   = []; // 점수 계산
$ev_result_txt_list   = []; // txt

// 기본정보
$ev_default_sql = "
	SELECT
		es.subject as ev_subject,
		(SELECT s_name FROM staff where s_no = esr.receiver_s_no) as ev_r_s_name, 
		CONCAT(es.ev_s_date, ' ~ ', es.ev_e_date) as ev_term
	FROM evaluation_system_result as esr
	LEFT JOIN evaluation_system as es ON es.ev_no = esr.ev_no
	WHERE 1=1 AND esr.ev_no = {$ev_no} AND esr.receiver_s_no = {$r_s_no} LIMIT 1
";

$ev_default_query 	= mysqli_query($my_db, $ev_default_sql);
$evaluation 		= mysqli_fetch_array($ev_default_query);

//질문 정보
$ev_question_sql = "
	SELECT
		eu.ev_u_no as question_no,
		IF(eu.evaluation_state > 4 , 'TEXT', 'NUM') as type,
		eu.question,
		esr.evaluation_value as score
	FROM evaluation_system_result as esr
	LEFT JOIN evaluation_system as es ON es.ev_no = esr.ev_no
	LEFT JOIN evaluation_unit as eu ON eu.ev_u_set_no = esr.ev_u_set_no AND eu.ev_u_no = esr.ev_u_no AND eu.evaluation_state != '99' 
	WHERE 1=1 AND esr.ev_no = {$ev_no} AND esr.receiver_s_no = {$r_s_no} AND esr.evaluator_s_no != '{$r_s_no}'
";

$ev_question_query = mysqli_query($my_db, $ev_question_sql);
while($ev_question = mysqli_fetch_array($ev_question_query))
{
	if($ev_question['type'] == "NUM")
	{
		$ev_num_question[$ev_question['question_no']] = $ev_question['question'];
		$ev_self_result[$ev_question['question_no']] = array('self_count' => 0, 'self_result' => 0);
	}elseif($ev_question['type'] == "TEXT")
	{
		$ev_txt_question[$ev_question['question_no']] 	   = $ev_question['question'];
		$ev_result_txt_list[$ev_question['question_no']][] = $ev_question['score'];
	}
}

//내 정보
$ev_self_sql = "
	SELECT
		esr.ev_u_no as question_no,
		esr.evaluation_value as score
	FROM evaluation_system_result as esr
	WHERE 1=1 AND esr.ev_no = {$ev_no} AND esr.receiver_s_no = {$r_s_no} AND esr.evaluator_s_no = {$r_s_no}
";

$ev_self_query = mysqli_query($my_db, $ev_self_sql);
while($ev_self = mysqli_fetch_array($ev_self_query))
{
	if(isset($ev_self_result[$ev_self['question_no']]))
	{
		$ev_self_result[$ev_self['question_no']]['self_count'] = $ev_self['score'];
	}
}


// 평가지 항목 쿼리
$evaluation_unit_sql = "
	SELECT
		ev_u.kind,
		ev_u.question,
		ev_u.description,
		ev_u.evaluation_state
	FROM evaluation_unit ev_u
	WHERE ev_u.ev_u_set_no=(SELECT ev_system.ev_u_set_no FROM evaluation_system ev_system WHERE ev_system.ev_no='{$ev_no}')
	AND ev_u.evaluation_state >= '1' AND ev_u.evaluation_state < '99'
	ORDER BY `order` ASC
";

$evaluation_unit_result = mysqli_query($my_db,$evaluation_unit_sql);
while($unit_array = mysqli_fetch_array($evaluation_unit_result)) {

	$evaluation_unit_list[]=array(
		"kind"		  		=> $unit_array['kind'],
		"question"	  		=> mb_substr($unit_array['question'], 0, 4, 'UTF-8'),
		"description"		=> $unit_array['description'],
		"evaluation_state"	=> $unit_array['evaluation_state']
	);
}

$result_list = [];
#평가자 리스트
$evaluator_sql="
	SELECT 
		DISTINCT evaluator_s_no,
		(SELECT s_name FROM staff s WHERE s.s_no=evaluator_s_no) AS evaluator_s_name
	FROM evaluation_system_result
	WHERE ev_no='{$ev_no}'
	ORDER BY evaluator_s_name ASC
";
$evaluator_result = mysqli_query($my_db,$evaluator_sql);
while($evaluator_array = mysqli_fetch_array($evaluator_result))
{
	#평가자의 평가지 항목별 리스트 evaluator_s_no
	$unit_sql = "
		SELECT 
			DISTINCT ev_u_no
		FROM evaluation_system_result
		WHERE ev_no='{$ev_no}' AND evaluator_s_no='{$evaluator_array['evaluator_s_no']}' AND (evaluation_state='1' OR evaluation_state='2' OR evaluation_state='3')
		ORDER BY ev_u_no ASC
  	";
	$unit_result=mysqli_query($my_db,$unit_sql);
	while($unit_array=mysqli_fetch_array($unit_result)){

		#평가자의 평가지 항목별 점수(자기평가 제외)
		$unit_value_sql="SELECT
						receiver_s_no,
                        (SELECT s_name FROM staff s WHERE s.s_no=receiver_s_no) AS receiver_s_name,
                        ev_r_no,
                        rate,
                        sqrt
                      FROM evaluation_system_result
                      WHERE ev_no='{$ev_no}' AND evaluator_s_no='{$evaluator_array['evaluator_s_no']}' AND ev_u_no='{$unit_array['ev_u_no']}' AND evaluator_s_no <> receiver_s_no
                      ORDER BY receiver_s_name ASC
                      ";

		$unit_value_result=mysqli_query($my_db,$unit_value_sql);
		$sqrt_array = [];
		while($unit_value_array=mysqli_fetch_array($unit_value_result)){

			$result_list[]=array(
				"ev_u_no"=>$unit_array['ev_u_no'],
				"receiver_s_no"=>$unit_value_array['receiver_s_no'],
				"evaluator_s_no"=>$evaluator_array['evaluator_s_no'],
				"ev_r_no"=>$unit_value_array['ev_r_no'],
				"rate"=>$unit_value_array['rate'],
				"sqrt_value"=>$unit_value_array['sqrt']
			);

		}
	}
}

foreach($result_list as $ev_u_result)
{
	if($ev_u_result['receiver_s_no'] == $r_s_no)
	{
		$ev_result_sum_list[$ev_u_result['ev_u_no']][] = $ev_u_result['sqrt_value'];
	}
}

foreach($ev_result_sum_list as $question => $ev_result_sum)
{
	$ev_result_sum_avg = array_sum($ev_result_sum) / count($ev_result_sum);
	$ev_self_result[$question]['self_result'] = round($ev_result_sum_avg, 2);
}


//셀크기
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(45);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

// 상단 타이틀
$sheet->setCellValue('A1', $evaluation['subject']);


// 기본 정보
$sheet->setCellValue('A3', '■ 기본정보');
$sheet->setCellValue('A4', '피평가자명');
$sheet->setCellValue('B4', $evaluation['ev_r_s_name']);
$sheet->setCellValue('A5', '평가기간');
$sheet->setCellValue('B5', $evaluation['ev_term']);

// 평가항목 및 평정표
$sheet->mergeCells("A7:B7");

$sheet->setCellValue('A7', '■ 평가항목 및 평정표');
$sheet->setCellValue('A8', '구분명');
$sheet->setCellValue('B8', '질문');
$sheet->setCellValue('C8', '자기평가');
$sheet->setCellValue('D8', '다면평가');
setBorder($sheet, "A8:D8");

$idx 	  		 	= 9;
$self_total 	 	= 0;
$self_result_total	= 0;

foreach($ev_self_result as $q_no => $self_result)
{
	$self_count  		= isset($self_result['self_count']) ? $self_result['self_count'] : 0;
	$self_result 		= isset($self_result['self_result']) ? $self_result['self_result'] : 0;
	$self_total 		+= $self_count;
	$self_result_total 	+= $self_result;

	$sheet->setCellValue("A{$idx}", $ev_txt_gubun[$q_no]);
	$sheet->setCellValue("B{$idx}", $ev_num_question[$q_no]);
	$sheet->setCellValue("C{$idx}", $self_count);
	$sheet->setCellValue("D{$idx}", $self_result);
	$sheet->getStyle("A{$idx}:B{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00F2F2F2');
	setBorder($sheet, "A{$idx}:D{$idx}");

	$idx++;
}

$self_avg 		 = $self_total / count($ev_self_result);
$self_result_avg = $self_result_total / count($ev_self_result);
$sheet->setCellValue("A{$idx}", "평균");
$sheet->mergeCells("A{$idx}:B{$idx}");
$sheet->setCellValue("C{$idx}", round($self_avg,2));
$sheet->setCellValue("D{$idx}", round($self_result_avg,2));
$sheet->getStyle("A{$idx}:B{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFC0C0C0');
setBorder($sheet, "A{$idx}:D{$idx}");
$idx += 2;

//평가의연
$sheet->setCellValue("A{$idx}", '■ 평가의견');

foreach ($ev_result_txt_list as $q_no => $ev_txt)
{
	$idx++;
	$first_idx = $idx;
	$sheet->mergeCells("B{$idx}:F{$idx}");
	$sheet->getStyle("A{$idx}:F{$idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00F2F2F2');
	setBorder($sheet, "A{$idx}:F{$idx}");
	$sheet->setCellValue("A{$idx}", $ev_txt_gubun[$q_no]);
	$sheet->setCellValue("B{$idx}", $ev_txt_question[$q_no]);
    $sheet->getRowDimension($idx)->setRowHeight(50);

	foreach($ev_txt as $txt)
	{
		if($txt != "")
		{
			$idx++;
			$sheet->mergeCells("B{$idx}:F{$idx}");
			setBorder($sheet, "A{$idx}:F{$idx}");
			$sheet->setCellValue("B{$idx}", $txt);
            $sheet->getRowDimension($idx)->setRowHeight(50);
            $sheet->getStyle("B{$idx}")->getAlignment()->setWrapText(true);
		}
	}

    $sheet->mergeCells("A{$first_idx}:A{$idx}");
}

// Style
$sheet->mergeCells('A1:F1');
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setSize(18);
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A1:A100')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B1:B100')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('C1:C100')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('D1:D100')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('A4:A5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFC0C0C0');

$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('A8:D8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A8:D8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFC0C0C0');
$objPHPExcel->getActiveSheet()->getStyle("A1:F{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(45);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(23);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(60);


// 출력
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
$objPHPExcel->getActiveSheet()->setTitle('평가결과');
$objPHPExcel->setActiveSheetIndex(0);
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="evaluation_result_'.$today.'.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;



?>
