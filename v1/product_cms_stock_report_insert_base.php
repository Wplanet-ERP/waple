<?php
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_upload.php');
require('inc/model/Custom.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/ProductCmsStock.php');
require('Classes/PHPExcel.php');

# 파일 변수
$report_month       = (isset($_POST['report_month'])) ? $_POST['report_month'] : "";
$report_month_day   = date("t", strtotime($report_month));
$report_date        = $report_month."-".$report_month_day;
$report_warehouse   = (isset($_POST['month_report_warehouse'])) ? $_POST['month_report_warehouse'] : "";
$report_file        = $_FILES["month_report_file"];
$cur_report_date    = date("Y-m-d H:i:s");
$report_memo        = "{$report_month} 기간별 수불현황";
$chk_except         = false;

# 업로드 관리 저장
$upload_model = Custom::Factory();
$upload_model->setMainInit("upload_management","file_no");

$upload_file_path   = "";
$upload_file_name   = "";
if(isset($report_file['name']) && !empty($report_file['name'])){
    $upload_file_path = add_store_file($report_file, "upload_management");
    $upload_file_name = $report_file['name'];
}

$upload_data  = array(
    "upload_type"   => 11,
    "upload_kind"   => $report_warehouse,
    "upload_date"   => $report_month,
    "file_path"     => $upload_file_path,
    "file_name"     => $upload_file_name,
    "reg_s_no"      => $session_s_no,
    "regdate"       => $cur_report_date,
);
$upload_model->insert($upload_data);

# 기초재고 처리
$stock_report       = ProductCmsStock::Factory();
$unit_model         = ProductCmsUnit::Factory();
$ins_report_list    = [];
$file_no            = $upload_model->getInsertId();
$excel_file_path    = $_SERVER['DOCUMENT_ROOT']."/v1/uploads/".$upload_file_path;
$sch_log_c_no       = ($report_warehouse == '1113') ? "2809" : $report_warehouse;

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($excel_file_path);
$excelReader->setReadDataOnly(true);

$excel = $excelReader->load($excel_file_path);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

if($report_warehouse  == "5484")
{
    $report_memo    = "{$report_month} 외부창고 재고수불부";
    $stock_type     = "파스토 창고";

    for ($i = 2; $i <= $totalRow; $i++)
    {
        $check              = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 체크
        $client             = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 고객사
        $product_name       = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 상품명
        $product_code       = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 상품코드
        $start_qty          = (int)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   // 기초재고
        $end_qty            = (int)trim(addslashes($objWorksheet->getCell("V{$i}")->getValue()));   // 기말재고
        $move_in_qty        = (int)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));  // 이동입고
        $ord_qty            = (int)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));  // 판매반출
        $move_out_qty       = (int)trim(addslashes($objWorksheet->getCell("N{$i}")->getValue()));  // 이동반출
        $return_qty         = (int)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));  // 반품입고
        $check_etc_qty      = (int)trim(addslashes($objWorksheet->getCell("P{$i}")->getValue()));  // 기타출고
        $check_in_qty       = (int)trim(addslashes($objWorksheet->getCell("Q{$i}")->getValue()));  // 유통가공(+)
        $check_out_qty      = (int)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));  // 유통가공(-)
        $check_use_in_qty   = (int)trim(addslashes($objWorksheet->getCell("S{$i}")->getValue()));  // 불용재고(+)
        $check_use_out_qty  = (int)trim(addslashes($objWorksheet->getCell("T{$i}")->getValue()));  // 불용재고(-)

        if(empty($check)){
            break;
        }

        $unit_item = $unit_model->getSkuItem($product_code, $sch_log_c_no);

        if(empty($unit_item)){
            exit("<script>alert('{$i}행의 상품코드 `{$product_code}`에 대한 값이\\n와플 구성품목(SKU)과 매칭 확인이 되지않아\\n업로드에 실패하였습니다.\\n설정 후 다시 시도해 주세요.');location.href='waple_upload_management.php';</script>");
        }

        if($check_etc_qty != 0 || $check_in_qty != 0 || $check_out_qty != 0 || $check_use_in_qty != 0 || $check_use_out_qty != 0){
            $chk_except = true;
        }

        $unit_type = ($unit_item['type'] == "1") ? "상품" : "부속품";

        if($move_in_qty != 0)
        {
            $ins_report_list[] = array(
                'file_no'       => $file_no,
                'regdate'       => $report_date,
                'report_type'   => "1",
                'type'          => "1",
                'state'         => "기간이동입고",
                'confirm_state' => "3",
                'client'        => $client,
                'brand'         => $unit_item['brand_name'],
                'prd_kind'      => $unit_type,
                'prd_name'      => $unit_item['option_name'],
                'prd_unit'      => $unit_item['prd_unit'],
                'sup_c_no'      => $unit_item['sup_c_no'],
                'log_c_no'      => $report_warehouse,
                'option_name'   => $unit_item['option_name'],
                'sku'           => $unit_item['sku'],
                'memo'          => $report_memo,
                'stock_type'    => $stock_type,
                'stock_qty'     => $move_in_qty,
                'reg_s_no'      => $session_s_no,
                'report_date'   => $cur_report_date,
            );
        }

        if($move_out_qty != 0)
        {
            $ins_report_list[] = array(
                'file_no'       => $file_no,
                'regdate'       => $report_date,
                'report_type'   => "1",
                'type'          => "2",
                'state'         => "이동반출",
                'confirm_state' => "6",
                'client'        => $client,
                'brand'         => $unit_item['brand_name'],
                'prd_kind'      => $unit_type,
                'prd_name'      => $unit_item['option_name'],
                'prd_unit'      => $unit_item['prd_unit'],
                'sup_c_no'      => $unit_item['sup_c_no'],
                'log_c_no'      => $report_warehouse,
                'option_name'   => $unit_item['option_name'],
                'sku'           => $unit_item['sku'],
                'memo'          => $report_memo,
                'stock_type'    => $stock_type,
                'stock_qty'     => -$move_out_qty,
                'reg_s_no'      => $session_s_no,
                'report_date'   => $cur_report_date,
            );
        }

        if($ord_qty != 0)
        {
            $ins_report_list[] = array(
                'file_no'       => $file_no,
                'regdate'       => $report_date,
                'report_type'   => "1",
                'type'          => "2",
                'state'         => "판매반출",
                'confirm_state' => "9",
                'client'        => $client,
                'brand'         => $unit_item['brand_name'],
                'prd_kind'      => $unit_type,
                'prd_name'      => $unit_item['option_name'],
                'prd_unit'      => $unit_item['prd_unit'],
                'sup_c_no'      => $unit_item['sup_c_no'],
                'log_c_no'      => $report_warehouse,
                'option_name'   => $unit_item['option_name'],
                'sku'           => $unit_item['sku'],
                'memo'          => $report_memo,
                'stock_type'    => $stock_type,
                'stock_qty'     => -$ord_qty,
                'reg_s_no'      => $session_s_no,
                'report_date'   => $cur_report_date,
            );
        }

        if($return_qty != 0)
        {
            $ins_report_list[] = array(
                'file_no'       => $file_no,
                'regdate'       => $report_date,
                'report_type'   => "1",
                'type'          => "1",
                'state'         => "반품입고",
                'confirm_state' => "5",
                'client'        => $client,
                'brand'         => $unit_item['brand_name'],
                'prd_kind'      => $unit_type,
                'prd_name'      => $unit_item['option_name'],
                'prd_unit'      => $unit_item['prd_unit'],
                'sup_c_no'      => $unit_item['sup_c_no'],
                'log_c_no'      => $report_warehouse,
                'option_name'   => $unit_item['option_name'],
                'sku'           => $unit_item['sku'],
                'memo'          => $report_memo,
                'stock_type'    => $stock_type,
                'stock_qty'     => $return_qty,
                'reg_s_no'      => $session_s_no,
                'report_date'   => $cur_report_date,
            );
        }

        $ins_report_list[] = array(
            'file_no'       => $file_no,
            'regdate'       => $report_date,
            'report_type'   => "1",
            'type'          => "0",
            'state'         => "기초재고",
            'confirm_state' => "11",
            'client'        => $client,
            'brand'         => $unit_item['brand_name'],
            'prd_kind'      => $unit_type,
            'prd_name'      => $unit_item['option_name'],
            'prd_unit'      => $unit_item['prd_unit'],
            'sup_c_no'      => $unit_item['sup_c_no'],
            'log_c_no'      => $report_warehouse,
            'option_name'   => $unit_item['option_name'],
            'sku'           => $unit_item['sku'],
            'memo'          => $report_memo,
            'stock_type'    => $stock_type,
            'stock_qty'     => $start_qty,
            'reg_s_no'      => $session_s_no,
            'report_date'   => $cur_report_date,
        );

        $ins_report_list[] = array(
            'file_no'       => $file_no,
            'regdate'       => $report_date,
            'report_type'   => "1",
            'type'          => "0",
            'state'         => "기말재고",
            'confirm_state' => "12",
            'client'        => $client,
            'brand'         => $unit_item['brand_name'],
            'prd_kind'      => $unit_type,
            'prd_name'      => $unit_item['option_name'],
            'prd_unit'      => $unit_item['prd_unit'],
            'sup_c_no'      => $unit_item['sup_c_no'],
            'log_c_no'      => $report_warehouse,
            'option_name'   => $unit_item['option_name'],
            'sku'           => $unit_item['sku'],
            'memo'          => $report_memo,
            'stock_type'    => $stock_type,
            'stock_qty'     => $end_qty,
            'reg_s_no'      => $session_s_no,
            'report_date'   => $cur_report_date,
        );
    }
}
elseif($report_warehouse  == "5659") # 스타트투데이_아이레놀 창고
{
    $stock_type     = "스타트투데이_아이레놀 창고";

    for ($i = 2; $i <= $totalRow; $i++)
    {
        $check              = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 체크
        $product_name       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 상품명
        $product_code       = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 상품코드
        $prd_report_type    = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 타입
        $quantity           = (int)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));      // 수량
        $reg_date_val       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 입출고날짜
        $memo               = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   // 메모
        $reg_date           = date("Y-m-d", strtotime($reg_date_val));
        $report_memo        = addslashes("{$report_month} 외부창고 재고수불부\r\n{$memo}");

        if(empty($check)){
            break;
        }

        $type           = "";
        $state          = "";
        $confirm_state  = "";

        if($prd_report_type == "입고"){
            $type           = "1";
            $state          = "기간이동입고";
            $confirm_state  = "3";
        }
        elseif($prd_report_type == "출고"){
            $type           = "2";
            $state          = "기간판매반출";
            $confirm_state  = "9";
            $quantity       = $quantity*-1;
        }
        elseif($prd_report_type == "재고조정"){

            if($quantity > 0){
                $type           = "1";
                $state          = "기간이동입고";
                $confirm_state  = "3";
            }else{
                $type           = "2";
                $state          = "기간이동반출";
                $confirm_state  = "6";
            }
        }
        elseif($prd_report_type == "출고취소"){
            $type           = "2";
            $state          = "기간판매반출";
            $confirm_state  = "9";
            $quantity       = $quantity*-1;
        }else{
            echo "타입(H)에 확인되지 않는 값({$prd_report_type})이 발견되어 업로드에 실패하였습니다. 운영팀에 문의해주세요.<br/>";
            exit;
        }

        $unit_item = $unit_model->getSkuItem($product_code, $sch_log_c_no);

        if(empty($unit_item)){
            exit("<script>alert('{$i}행의 상품코드 `{$product_code}`에 대한 값이\\n와플 구성품목(SKU)과 매칭 확인이 되지않아\\n업로드에 실패하였습니다.\\n설정 후 다시 시도해 주세요.');location.href='waple_upload_management.php';</script>");
        }

        $unit_type = ($unit_item['type'] == "1") ? "상품" : "부속품";

        $ins_report_list[] = array(
            'file_no'       => $file_no,
            'regdate'       => $reg_date,
            'report_type'   => "1",
            'type'          => $type,
            'state'         => $state,
            'confirm_state' => $confirm_state,
            'brand'         => $unit_item['brand_name'],
            'prd_kind'      => $unit_type,
            'prd_name'      => $unit_item['option_name'],
            'prd_unit'      => $unit_item['prd_unit'],
            'sup_c_no'      => $unit_item['sup_c_no'],
            'log_c_no'      => $report_warehouse,
            'option_name'   => $unit_item['option_name'],
            'sku'           => $unit_item['sku'],
            'memo'          => $report_memo,
            'stock_type'    => $stock_type,
            'stock_qty'     => $quantity,
            'reg_s_no'      => $session_s_no,
            'report_date'   => $cur_report_date,
        );
    }
}
elseif($report_warehouse  == "5956") # 스타트투데이_닥터피엘 창고
{
    $stock_type     = "스타트투데이_닥터피엘 창고";

    for ($i = 2; $i <= $totalRow; $i++)
    {
        $check              = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 체크
        $product_name       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 상품명
        $product_code       = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 상품코드
        $prd_report_type    = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 타입
        $quantity           = (int)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));      // 수량
        $reg_date_val       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 입출고날짜
        $memo               = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   // 메모
        $reg_date           = date("Y-m-d", strtotime($reg_date_val));
        $report_memo        = addslashes("{$report_month} 외부창고 재고수불부\r\n{$memo}");

        if(empty($check)){
            break;
        }

        $type           = "";
        $state          = "";
        $confirm_state  = "";

        if($prd_report_type == "입고"){
            $type           = "1";
            $state          = "기간이동입고";
            $confirm_state  = "3";
        }
        elseif($prd_report_type == "출고"){
            $type           = "2";
            $state          = "기간판매반출";
            $confirm_state  = "9";
            $quantity       = $quantity*-1;
        }
        elseif($prd_report_type == "재고조정"){

            if($quantity > 0){
                $type           = "1";
                $state          = "기간이동입고";
                $confirm_state  = "3";
            }else{
                $type           = "2";
                $state          = "기간이동반출";
                $confirm_state  = "6";
            }
        }
        elseif($prd_report_type == "출고취소"){
            $type           = "2";
            $state          = "기간판매반출";
            $confirm_state  = "9";
            $quantity       = $quantity*-1;
        }else{
            echo "타입(H)에 확인되지 않는 값({$prd_report_type})이 발견되어 업로드에 실패하였습니다. 운영팀에 문의해주세요.<br/>";
            exit;
        }

        $unit_item = $unit_model->getSkuItem($product_code, $sch_log_c_no);

        if(empty($unit_item)){
            exit("<script>alert('{$i}행의 상품코드 `{$product_code}`에 대한 값이\\n와플 구성품목(SKU)과 매칭 확인이 되지않아\\n업로드에 실패하였습니다.\\n설정 후 다시 시도해 주세요.');location.href='waple_upload_management.php';</script>");
        }

        $unit_type = ($unit_item['type'] == "1") ? "상품" : "부속품";

        $ins_report_list[] = array(
            'file_no'       => $file_no,
            'regdate'       => $reg_date,
            'report_type'   => "1",
            'type'          => $type,
            'state'         => $state,
            'confirm_state' => $confirm_state,
            'brand'         => $unit_item['brand_name'],
            'prd_kind'      => $unit_type,
            'prd_name'      => $unit_item['option_name'],
            'prd_unit'      => $unit_item['prd_unit'],
            'sup_c_no'      => $unit_item['sup_c_no'],
            'log_c_no'      => $report_warehouse,
            'option_name'   => $unit_item['option_name'],
            'sku'           => $unit_item['sku'],
            'memo'          => $report_memo,
            'stock_type'    => $stock_type,
            'stock_qty'     => $quantity,
            'reg_s_no'      => $session_s_no,
            'report_date'   => $cur_report_date,
        );
    }
}
elseif($report_warehouse  == "5777") # 씨엔제이커머스 창고
{
    $stock_type     = "씨엔제이커머스 창고";
    $sch_log_c_no   = 2809;

    for ($i = 2; $i <= $totalRow; $i++)
    {
        $product_code   = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 상품코드
        $ord_qty        = (int)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 판매반출 수량
        $move_in_qty    = (int)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 이동입고 수량
        $move_out_qty   = (int)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 이동반출 수량
        $start_qty      = (int)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 기초재고 수량
        $end_qty        = (int)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 기말재고 수량
        $report_memo        = addslashes("{$report_month} 외부창고 재고수불부");

        if(empty($product_code)){
            break;
        }

        $unit_item = $unit_model->getSkuItem($product_code, $sch_log_c_no);

        if(!empty($unit_item))
        {
            $unit_type = ($unit_item['type'] == "1") ? "상품" : "부속품";

            if($ord_qty != 0)
            {
                $ins_report_list[] = array(
                    'file_no'       => $file_no,
                    'regdate'       => $report_date,
                    'report_type'   => "1",
                    'type'          => "2",
                    'state'         => "기간판매반출",
                    'confirm_state' => "9",
                    'brand'         => $unit_item['brand_name'],
                    'prd_kind'      => $unit_type,
                    'prd_name'      => $unit_item['option_name'],
                    'prd_unit'      => $unit_item['prd_unit'],
                    'sup_c_no'      => $unit_item['sup_c_no'],
                    'log_c_no'      => $report_warehouse,
                    'option_name'   => $unit_item['option_name'],
                    'sku'           => $unit_item['sku'],
                    'memo'          => $report_memo,
                    'stock_type'    => $stock_type,
                    'stock_qty'     => -$ord_qty,
                    'reg_s_no'      => $session_s_no,
                    'report_date'   => $cur_report_date,
                );
            }

            if($move_in_qty != 0)
            {
                $ins_report_list[] = array(
                    'file_no'       => $file_no,
                    'regdate'       => $report_date,
                    'report_type'   => "1",
                    'type'          => "1",
                    'state'         => "기간이동입고",
                    'confirm_state' => "3",
                    'brand'         => $unit_item['brand_name'],
                    'prd_kind'      => $unit_type,
                    'prd_name'      => $unit_item['option_name'],
                    'prd_unit'      => $unit_item['prd_unit'],
                    'sup_c_no'      => $unit_item['sup_c_no'],
                    'log_c_no'      => $report_warehouse,
                    'option_name'   => $unit_item['option_name'],
                    'sku'           => $unit_item['sku'],
                    'memo'          => $report_memo,
                    'stock_type'    => $stock_type,
                    'stock_qty'     => $move_in_qty,
                    'reg_s_no'      => $session_s_no,
                    'report_date'   => $cur_report_date,
                );
            }

            if($move_out_qty != 0)
            {
                $ins_report_list[] = array(
                    'file_no'       => $file_no,
                    'regdate'       => $report_date,
                    'report_type'   => "1",
                    'type'          => "2",
                    'state'         => "기간이동반출",
                    'confirm_state' => "6",
                    'brand'         => $unit_item['brand_name'],
                    'prd_kind'      => $unit_type,
                    'prd_name'      => $unit_item['option_name'],
                    'prd_unit'      => $unit_item['prd_unit'],
                    'sup_c_no'      => $unit_item['sup_c_no'],
                    'log_c_no'      => $report_warehouse,
                    'option_name'   => $unit_item['option_name'],
                    'sku'           => $unit_item['sku'],
                    'memo'          => $report_memo,
                    'stock_type'    => $stock_type,
                    'stock_qty'     => -$move_out_qty,
                    'reg_s_no'      => $session_s_no,
                    'report_date'   => $cur_report_date,
                );
            }

            $ins_report_list[] = array(
                'file_no'       => $file_no,
                'regdate'       => $report_date,
                'report_type'   => "1",
                'type'          => "0",
                'state'         => "기초재고",
                'confirm_state' => "11",
                'brand'         => $unit_item['brand_name'],
                'prd_kind'      => $unit_type,
                'prd_name'      => $unit_item['option_name'],
                'prd_unit'      => $unit_item['prd_unit'],
                'sup_c_no'      => $unit_item['sup_c_no'],
                'log_c_no'      => $report_warehouse,
                'option_name'   => $unit_item['option_name'],
                'sku'           => $unit_item['sku'],
                'memo'          => $report_memo,
                'stock_type'    => $stock_type,
                'stock_qty'     => $start_qty,
                'reg_s_no'      => $session_s_no,
                'report_date'   => $cur_report_date,
            );

            $ins_report_list[] = array(
                'file_no'       => $file_no,
                'regdate'       => $report_date,
                'report_type'   => "1",
                'type'          => "0",
                'state'         => "기말재고",
                'confirm_state' => "12",
                'brand'         => $unit_item['brand_name'],
                'prd_kind'      => $unit_type,
                'prd_name'      => $unit_item['option_name'],
                'prd_unit'      => $unit_item['prd_unit'],
                'sup_c_no'      => $unit_item['sup_c_no'],
                'log_c_no'      => $report_warehouse,
                'option_name'   => $unit_item['option_name'],
                'sku'           => $unit_item['sku'],
                'memo'          => $report_memo,
                'stock_type'    => $stock_type,
                'stock_qty'     => $end_qty,
                'reg_s_no'      => $session_s_no,
                'report_date'   => $cur_report_date,
            );
        }
    }
}
else
{
    $prd_code_cell      = "";
    $stock_type_cell    = "";
    $ord_qty_cell       = "";
    $return_qty_cell    = "";
    $start_qty_cell     = "";
    $end_qty_cell       = "";
    $eng_abc            = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

    for ($i = 2; $i <= $totalRow; $i++)
    {
        if($i == "2")
        {
            $highestColumn  = $objWorksheet->getHighestColumn();
            $titleRowData   = $objWorksheet->rangeToArray("A{$i}:".$highestColumn.$i, NULL, TRUE, FALSE);

            foreach ($titleRowData[0] as $key => $title)
            {
                switch($title){
                    case "상품코드":
                        $prd_code_cell = $eng_abc[$key];
                        break;
                    case "창고":
                        $stock_type_cell = $eng_abc[$key];
                        break;
                    case "배송수량":
                        $ord_qty_cell = $eng_abc[$key];
                        break;
                    case "고객반품수량":
                        $return_qty_cell = $eng_abc[$key];
                        break;
                    case "기초재고수량":
                        $start_qty_cell = $eng_abc[$key];
                        break;
                    case "기말재고수량":
                        $end_qty_cell = $eng_abc[$key];
                        break;
                }
            }
            continue;
        }

        if(empty($prd_code_cell) || empty($stock_type_cell) || empty($ord_qty_cell) || empty($return_qty_cell) || empty($start_qty_cell) || empty($end_qty_cell)){
            echo "변환되지 않는 필드 값이 있습니다. 개발팀 임태형에게 문의해주세요!";
            exit;
        }

        $check          = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 체크
        $product_code   = (string)trim(addslashes($objWorksheet->getCell("{$prd_code_cell}{$i}")->getValue()));   // 상품명
        $stock_type     = (string)trim(addslashes($objWorksheet->getCell("{$stock_type_cell}{$i}")->getValue()));   // 물류창고
        $ord_qty        = (int)trim(addslashes($objWorksheet->getCell("{$ord_qty_cell}{$i}")->getValue()));   // 수량
        $return_qty     = (int)trim(addslashes($objWorksheet->getCell("{$return_qty_cell}{$i}")->getValue()));   // 수량
        $start_qty      = (int)trim(addslashes($objWorksheet->getCell("{$start_qty_cell}{$i}")->getValue()));   // 수량
        $end_qty        = (int)trim(addslashes($objWorksheet->getCell("{$end_qty_cell}{$i}")->getValue()));   // 수량

        if(empty($check)){
            break;
        }

        $unit_item = $unit_model->getSkuItem($product_code, $sch_log_c_no);

        if(!empty($unit_item))
        {
            $unit_type = ($unit_item['type'] == "1") ? "상품" : "부속품";

            if($ord_qty != 0)
            {
                $ins_report_list[] = array(
                    'file_no'       => $file_no,
                    'regdate'       => $report_date,
                    'report_type'   => "1",
                    'type'          => "2",
                    'state'         => "기간판매반출",
                    'confirm_state' => "9",
                    'brand'         => $unit_item['brand_name'],
                    'prd_kind'      => $unit_type,
                    'prd_name'      => $unit_item['option_name'],
                    'prd_unit'      => $unit_item['prd_unit'],
                    'sup_c_no'      => $unit_item['sup_c_no'],
                    'log_c_no'      => $report_warehouse,
                    'option_name'   => $unit_item['option_name'],
                    'sku'           => $unit_item['sku'],
                    'memo'          => $report_memo,
                    'stock_type'    => $stock_type,
                    'stock_qty'     => -$ord_qty,
                    'reg_s_no'      => $session_s_no,
                    'report_date'   => $cur_report_date,
                );
            }

            if($return_qty != 0)
            {
                $ins_report_list[] = array(
                    'file_no'       => $file_no,
                    'regdate'       => $report_date,
                    'report_type'   => "1",
                    'type'          => "1",
                    'state'         => "기간반품입고",
                    'confirm_state' => "5",
                    'brand'         => $unit_item['brand_name'],
                    'prd_kind'      => $unit_type,
                    'prd_name'      => $unit_item['option_name'],
                    'prd_unit'      => $unit_item['prd_unit'],
                    'sup_c_no'      => $unit_item['sup_c_no'],
                    'log_c_no'      => $report_warehouse,
                    'option_name'   => $unit_item['option_name'],
                    'sku'           => $unit_item['sku'],
                    'memo'          => $report_memo,
                    'stock_type'    => $stock_type,
                    'stock_qty'     => $return_qty,
                    'reg_s_no'      => $session_s_no,
                    'report_date'   => $cur_report_date,
                );
            }

            $ins_report_list[] = array(
                'file_no'       => $file_no,
                'regdate'       => $report_date,
                'report_type'   => "1",
                'type'          => "0",
                'state'         => "기초재고",
                'confirm_state' => "11",
                'brand'         => $unit_item['brand_name'],
                'prd_kind'      => $unit_type,
                'prd_name'      => $unit_item['option_name'],
                'prd_unit'      => $unit_item['prd_unit'],
                'sup_c_no'      => $unit_item['sup_c_no'],
                'log_c_no'      => $report_warehouse,
                'option_name'   => $unit_item['option_name'],
                'sku'           => $unit_item['sku'],
                'memo'          => $report_memo,
                'stock_type'    => $stock_type,
                'stock_qty'     => $start_qty,
                'reg_s_no'      => $session_s_no,
                'report_date'   => $cur_report_date,
            );

            $ins_report_list[] = array(
                'file_no'       => $file_no,
                'regdate'       => $report_date,
                'report_type'   => "1",
                'type'          => "0",
                'state'         => "기말재고",
                'confirm_state' => "12",
                'brand'         => $unit_item['brand_name'],
                'prd_kind'      => $unit_type,
                'prd_name'      => $unit_item['option_name'],
                'prd_unit'      => $unit_item['prd_unit'],
                'sup_c_no'      => $unit_item['sup_c_no'],
                'log_c_no'      => $report_warehouse,
                'option_name'   => $unit_item['option_name'],
                'sku'           => $unit_item['sku'],
                'memo'          => $report_memo,
                'stock_type'    => $stock_type,
                'stock_qty'     => $end_qty,
                'reg_s_no'      => $session_s_no,
                'report_date'   => $cur_report_date,
            );
        }
    }
}

$stock_report->setMainInit("product_cms_stock_report", 'no');

if (!$stock_report->multiInsert($ins_report_list)){
    echo "기초재고 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 임태형<br/>";
    exit;
}else{

    $alert_msg = "기간별 수불현황이 반영 되었습니다.";
    if($chk_except){
        $alert_msg .= "\\n※주의 : 기타 출고,유통가공,불용재고,변경재고에 0이 아닌 수량이 있습니다.\\n해당 값은 자세한 내용을 확인 후\\n입고/반출 리스트(product_cms_stock_report_list.php)에 직접 추가해야 합니다.";
    }

    exit("<script>alert('{$alert_msg}');location.href='product_cms_stock_report_list.php?sch_s_regdate=&sch_e_regdate=&sch_file_no={$file_no}';</script>");
}

?>
