<?php
session_start();
require('inc/common.php');

	// GET : registration.php?promotion_no=	로	넘어온	프로모션 고유번호(p_no)를 체크하고	있으면	code에	저장
	$code=isset($_GET['promotion_no']) ? $_GET['promotion_no'] : "";

	// POST	:	form action	을	통해 넘겨받은	process	가	있는지	체크하고 있으면 proc에 저장
	$proc=isset($_POST['process']) ? $_POST['process'] : "";

	// form	action 이 등록일 경우
	if ($proc	== "write")	{

		$user_ip = $_SERVER['REMOTE_ADDR'];

		$_POST = array_map("trim", $_POST);
		$arr_key = array("code", "q1", "q2", "q3", "q4", "q5", "q6", "q7", "q_opinion",	"q_contact");
		$arr_var = array_fill_keys($arr_key, "");
		foreach	($arr_var	as $key	=> $val) {
			$arr_var[$key] = $_POST[$key];
		}
		extract($arr_var);

		//exit;

		$select_sql	=	"select	count(*) from	evaluation where ip	=	'{$user_ip}' and p_no='{$code}' and regdate > DATE_SUB(now(), INTERVAL 1 MINUTE)";
		$select_query	=	mysqli_query($my_db,$select_sql);
		$select_data = mysqli_fetch_array($select_query);
		if ($select_data[0]	!= 0 )
			alert("이미 평가에 참여하셨습니다.","evaluation.php?promotion_no=".$_POST['code']);

		$sql = "
			insert into	evaluation set
						 p_no	=	'{$code}',
						 q1	=	'{$q1}',
						 q2	=	'{$q2}',
						 q3	=	'{$q3}',
						 q4	=	'{$q4}',
						 q5	=	'{$q5}',
						 q6	=	'{$q6}',
						 q7	=	'{$q7}',
						 q_opinion = '"	.	addslashes($q_opinion) ."',
						 q_contact = '{$q_contact}',
						 ip	=	'{$user_ip}',
						 regdate = now()
				";
		//echo $sql;exit;

		$my_db->query($sql);
		alert("이용 후 평가에 참여해 주셔서 감사합니다. ^^","evaluation.php?promotion_no=".$_POST['code']);
	}

	if(!$code)
		exit("<script>alert('해당	프로모션이	존재하지 않습니다(1)');</script>");

	$app_sql = "select * from	promotion	where	p_no='".$code."'";

	$app_query = mysqli_query($my_db,	$app_sql);
	$app_data	=	mysqli_fetch_array($app_query);

	if(!$app_data)
		exit("<script>alert('해당	프로모션이 존재하지 않습니다(2)');</script>");

	$promotion_title = $app_data['title'] ? $app_data['title'] : $app_data['company'];

	$smarty->assign(array(
		"is_p_close" =>	$is_p_close,
		"no"=>$app_data['p_no'],
		"s_no"=>$app_data['s_no'],
		"c_no"=>$app_data['c_no'],
		"title"=>$promotion_title,
		"company"=>$app_data['company'],
		"kind"=>$app_data['kind']
	));

	// 기타	세팅
	$kind=array(
		'name'=>array(
				'',
				'체험단',
				'기자단',
				'배송체험'
				),
			'url'=>array(
				'',
				'consumer',
				'press',
				'delivery'
			)
		);

	$smarty->assign(
		array(
			"kind_name"=>$kind['name'][$app_data['kind']]
			)
		);

	$smarty->display('evaluation.html');
?>
