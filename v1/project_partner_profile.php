<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/project.php');

$c_no           = isset($_GET['c_no']) ? $_GET['c_no'] : "";
$license_type   = isset($_GET['license_type']) ? $_GET['license_type'] : "";

$profile_sql        = "SELECT * FROM lector_profile WHERE c_no = '{$c_no}'";
$profile_query      = mysqli_query($my_db, $profile_sql);
$profile_result     = mysqli_fetch_assoc($profile_query);
$expertise_option   = getExpertiseOption();
if(isset($profile_result['lp_no']))
{
    #경력 데이터
    $career_list  = [];
    $career_sql   = "SELECT * FROM `lector_profile_career` WHERE lp_no='{$profile_result['lp_no']}' AND lpc_display='1'";
    $career_query = mysqli_query($my_db, $career_sql);
    while($career = mysqli_fetch_assoc($career_query))
    {
        $career_list[] = $career;
    }
    $profile_result['career_list'] = $career_list;

    #자격증 데이터
    $license_list  = [];
    $license_sql   = "SELECT * FROM `lector_profile_license` WHERE lp_no='{$profile_result['lp_no']}' AND lpl_display='1'";
    $license_query = mysqli_query($my_db, $license_sql);
    while($license = mysqli_fetch_assoc($license_query))
    {
        $license_list[] = $license;
    }
    $profile_result['license_list'] = $license_list;

    #멘토 및 기타데이터
    $mentor_list  = [];
    $etc_list     = [];
    $etc_sql   = "SELECT * FROM `lector_profile_etc` WHERE lp_no='{$profile_result['lp_no']}' AND lpe_display='1'";

    $etc_query = mysqli_query($my_db, $etc_sql);
    while($etc = mysqli_fetch_assoc($etc_query))
    {
        if($etc['lpe_type'] == '1'){
            $mentor_list[] = $etc;
        }elseif($etc['lpe_type'] == '2'){
            $etc_list[] = $etc;
        }
    }
    $profile_result['mentor_list'] = $mentor_list;
    $profile_result['etc_list'] = $etc_list;
    $profile_result['expertise_label'] =  "";

    if(!empty($profile_result['expertise']))
    {
        $expertise_val_list   = explode(',', $profile_result['expertise']);
        $expertise_label_list = [];

        foreach($expertise_val_list as $expertise){
            $expertise_label_list[] = $expertise_option[$expertise];
        }
        $profile_result['expertise_label'] = implode(',', $expertise_label_list);
    }

}

if($profile_result){
    foreach($profile_result as $key => $value){
        if(!empty($value) && !is_array($value)){
            $profile_result[$key] = htmlspecialchars($value);
        }
    }
}

$smarty->assign($profile_result);
$smarty->assign("license_type", $license_type);

$smarty->assign("email_list", getEmailOption());
$smarty->assign("hp_list", getHpOption());
$smarty->assign("tel_list", getTelOption());

$smarty->display('project_partner_profile.html');
?>
