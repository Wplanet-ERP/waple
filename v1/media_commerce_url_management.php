<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');
require('inc/model/Custom.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/Team.php');

# Process
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$company_model  = Company::Factory();
$kind_model     = Kind::Factory();
$team_model     = Team::Factory();
$url_model      = Custom::Factory();
$url_model->setMainInit("commerce_url", "url_no");

if($process == 'new_commerce_url')
{
    $new_brand      = isset($_POST['new_brand']) ? $_POST['new_brand'] : "";
    $new_dp_c_no    = isset($_POST['new_dp_c_no']) ? $_POST['new_dp_c_no'] : "";
    $new_page_title = isset($_POST['new_page_title']) ? trim(addslashes($_POST['new_page_title'])) : "";
    $new_url        = isset($_POST['new_url']) ? trim(addslashes($_POST['new_url'])) : "";
    $new_memo       = isset($_POST['new_memo']) ? trim(addslashes($_POST['new_memo'])) : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    if(empty($new_brand) || empty($new_dp_c_no) || empty($new_url))
    {
        exit("<script>alert('다시 입력해주세요');history.back();</script>");
    }

    $company_item   = $company_model->getItem($new_brand);
    $url_ins_data   = array(
        "brand"     => $new_brand,
        "dp_c_no"   => $new_dp_c_no,
        "s_no"      => $company_item['s_no'],
        "team"      => $company_item['team'],
        "page_title"=> $new_page_title,
        "url"       => $new_url,
        "memo"      => $new_memo,
        "reg_s_no"  => $session_s_no,
        "regdate"   => date("Y-m-d H:i:s"),
    );

    if($url_model->insert($url_ins_data)){
        exit("<script>alert('등록 성공했습니다');location.href='media_commerce_url_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('등록 실패했습니다');location.href='media_commerce_url_management.php?{$search_url}';</script>");
    }
}
elseif($process == 'modify_memo')
{
    $url_no     = (isset($_POST['url_no'])) ? $_POST['url_no'] : "";
    $memo       = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if(empty($url_no)){
        echo "url_no 값이 없습니다.";
        exit;
    }

    if (!$url_model->update(array("url_no" => $url_no, "memo" => $memo)))
        echo "메모 저장에 실패 하였습니다.";
    else
        echo "메모가 저장 되었습니다.";
    exit;
}
elseif($process == 'modify_display')
{
    $url_no     = (isset($_POST['url_no'])) ? $_POST['url_no'] : "";
    $display    = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($url_no)){
        echo "url_no 값이 없습니다.";
        exit;
    }

    if (!$url_model->update(array("url_no" => $url_no, "display" => $display)))
        echo "Display 변경에 실패 하였습니다.";
    else
        echo "Display를 변경했습니다.";
    exit;
}
elseif($process == 'modify_brand')
{
    $url_no     = (isset($_POST['url_no'])) ? $_POST['url_no'] : "";
    $brand_no   = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($url_no)){
        echo "url_no 값이 없습니다.";
        exit;
    }

    $company_item   = $company_model->getItem($brand_no);
    $url_upd_data   = array(
        "url_no"    => $url_no,
        "brand"     => $brand_no,
        "s_no"      => $company_item['s_no'],
        "team"      => $company_item['team']
    );

    if (!$url_model->update($url_upd_data))
        echo "브랜드 변경에 실패 하였습니다.";
    else
        echo "브랜드를 변경했습니다.";
    exit;
}
elseif($process == 'modify_dp_company')
{
    $url_no     = (isset($_POST['url_no'])) ? $_POST['url_no'] : "";
    $dp_c_no    = (isset($_POST['val'])) ? $_POST['val'] : "";

    if(empty($url_no)){
        echo "url_no 값이 없습니다.";
        exit;
    }

    if (!$url_model->update(array("url_no" => $url_no, "dp_c_no" => $dp_c_no)))
        echo "판매처 저장에 실패 하였습니다.";
    else
        echo "판매처를 변경했습니다.";
    exit;
}
elseif($process == 'modify_url')
{
    $url_no     = (isset($_POST['url_no'])) ? $_POST['url_no'] : "";
    $url        = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if(empty($url_no)){
        echo "url_no 값이 없습니다.";
        exit;
    }

    if (!$url_model->update(array("url_no" => $url_no, "url" => $url)))
        echo "URL 저장에 실패 하였습니다.";
    else
        echo "URL을 변경했습니다.";
    exit;
}
elseif($process == 'modify_page_title')
{
    $url_no     = (isset($_POST['url_no'])) ? $_POST['url_no'] : "";
    $url        = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if(empty($url_no)){
        echo "url_no 값이 없습니다.";
        exit;
    }

    if (!$url_model->update(array("url_no" => $url_no, "page_title" => $url)))
        echo "페이지 제목 저장에 실패 하였습니다.";
    else
        echo "페이지 제목을 변경했습니다.";
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "210";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
$nav_title   = "커머스 URL 리스트";

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 사용 변수 리스트
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_info_list            = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$brand_info_list[1314]['c_name']  = "닥터피엘(필터라인)";
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

$team_all_list              = $team_model->getTeamAllList();
$staff_team_list            = $team_all_list['staff_list'];
$sch_staff_list 	        = $staff_team_list['all'];
$team_full_name_list        = $team_model->getTeamFullNameList();
$team_name_list             = $team_all_list['team_name_list'];
$display_option             = getDisplayOption();

# 검색조건
$add_where          = "1=1";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_display        = isset($_GET['sch_display']) ? $_GET['sch_display'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_team           = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no           = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$sch_dp_c_no        = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_memo           = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
$sch_page_title     = isset($_GET['sch_page_title']) ? $_GET['sch_page_title'] : "";
$sch_url            = isset($_GET['sch_url']) ? $_GET['sch_url'] : "";
$search_url         = getenv("QUERY_STRING");

$url_check_str  = $_SERVER['QUERY_STRING'];
$url_chk_result = false;
if(empty($url_check_str)){
    $url_check_team_where   = getTeamWhere($my_db, $session_team);
    $url_check_sql    		=  "SELECT count(*) as cnt FROM commerce_url as `url` WHERE display='1' AND team IN({$url_check_team_where})";
    $url_check_query  		= mysqli_query($my_db, $url_check_sql);
    $url_check_result 		= mysqli_fetch_assoc($url_check_query);

    if($url_check_result['cnt'] > 0){
        $sch_team       = $session_team;
        $url_chk_result = true;
    }
}

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_no))
{
    $add_where .= " AND `url`.url_no='{$sch_no}'";
    $smarty->assign("sch_no", $sch_no);
}

if(!empty($sch_display))
{
    $add_where .= " AND `url`.display='{$sch_display}'";
    $smarty->assign("sch_display", $sch_display);
}

if(!empty($sch_brand)) {
    $sch_brand_g2_list = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list    = $brand_list[$sch_brand_g2];

    $add_where .= " AND `url`.brand='{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where .= " AND `url`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where  .= " AND `url`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

if (!empty($sch_team))
{
    if ($sch_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_team);
        $add_where 		 .= " AND `url`.team IN({$sch_team_code_where})";
        $sch_staff_list   = $staff_team_list[$sch_team];
    }
    $smarty->assign("sch_team", $sch_team);
}

if (!empty($sch_s_no))
{
    if ($sch_s_no != "all") {
        $add_where 		 .= " AND `url`.s_no = '{$sch_s_no}'";
    }
    $smarty->assign("sch_s_no",$sch_s_no);
}

if(!empty($sch_dp_c_no))
{
    $add_where .= " AND `url`.dp_c_no='{$sch_dp_c_no}'";
    $smarty->assign("sch_dp_c_no", $sch_dp_c_no);
}

if(!empty($sch_memo))
{
    $add_where .= " AND `url`.memo LIKE '%{$sch_memo}%'";
    $smarty->assign("sch_memo", $sch_memo);
}

if(!empty($sch_page_title))
{
    $add_where .= " AND `url`.page_title LIKE '%{$sch_page_title}%'";
    $smarty->assign("sch_page_title", $sch_page_title);
}

if(!empty($sch_url))
{
    $add_where .= " AND `url`.url LIKE '%{$sch_url}%'";
    $smarty->assign("sch_url", $sch_url);
}

# 전체 게시물 수
$comm_url_total_sql     = "SELECT COUNT(`url`.url_no) as cnt FROM commerce_url as `url` WHERE {$add_where}";
$comm_url_total_query	= mysqli_query($my_db, $comm_url_total_sql);
$comm_url_total_result  = mysqli_fetch_array($comm_url_total_query);
$comm_url_total         = !empty($comm_url_total_result['cnt']) ? $comm_url_total_result['cnt'] : 0;

# 페이징
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 10;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ($comm_url_total > 0) ? ceil($comm_url_total/$num) : 1;

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

if(empty($url_check_str) && $url_chk_result){
    $search_url = "sch_team={$sch_team}";
}else{
    $search_url = getenv("QUERY_STRING");
}
$pagelist   = pagelist($pages, "media_commerce_url_management.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $comm_url_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$comm_url_sql  = "
    SELECT 
        `url`.*,
        DATE_FORMAT(`url`.regdate, '%Y-%m-%d') as regdate,
        (SELECT t.team_name FROM team t WHERE t.team_code = `url`.team) as t_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no = `url`.s_no) as s_name,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=(SELECT `c`.brand FROM company `c` WHERE c.c_no=`url`.brand))) AS brand_g1_name,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT `c`.brand FROM company `c` WHERE c.c_no=`url`.brand)) AS brand_g2_name,
        (SELECT `c`.c_name FROM company `c` WHERE c.c_no=`url`.brand) as brand_name,
        (SELECT `c`.c_name FROM company `c` WHERE c.c_no=`url`.dp_c_no) as dp_c_name
    FROM commerce_url `url`
    WHERE {$add_where}
    ORDER BY `url`.url_no DESC
    LIMIT {$offset}, {$num}
";
$comm_url_query = mysqli_query($my_db, $comm_url_sql);
$comm_url_list  = [];
while($comm_url = mysqli_fetch_assoc($comm_url_query))
{
    $comm_url['display_name']       = $display_option[$comm_url['display']];
    $comm_url_list[] = $comm_url;
}

$smarty->assign("display_option", $display_option);
$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign('add_dp_company_option', $company_model->getDpList());
$smarty->assign('sch_dp_company_option', $company_model->getDpDisplayList());
$smarty->assign('brand_company_option', $brand_info_list);
$smarty->assign('comm_url_list', $comm_url_list);

$smarty->display('media_commerce_url_management.html');
?>
