<?php
require('inc/common.php');
require('ckadmin.php');

$type 		= isset($_GET['type']) ? $_GET['type'] : "";
$cur_date   	= date('Y-m-d');
$pre_date   	= date('Y-m-d', strtotime("{$cur_date} -6 months"));
$pre_month  	= date('Y-m', strtotime("{$pre_date}"));
$pre_month_val  = date('Ym', strtotime("{$pre_date}"));
$base_month 	= '2022-01';
$base_month_val	= '202201';
$return_url = ($type == 'sms') ? "sms_list.php" : "crm_log_list.php";

if($base_month > $pre_month_val){
	alert("이미 처리 되었습니다.", $return_url);
}

if($type == 'sms')
{
	$idx = 0;
    $uds_month = $pre_month;
	while(true)
	{
		$table_month = date('Ym',strtotime($uds_month));
        $table_year  = date('Y',strtotime($uds_month));

		if($idx == 0){
            $table_s_date = $uds_month."-01 00:00:00";
            $table_e_date = $pre_date." 23:59:59";
		}else{
            $table_s_date = $uds_month."-01 00:00:00";
            $table_e_date = $uds_month."-31 23:59:59";
		}

		$biz_sms_sql   = "SELECT CMID, DEST_PHONE FROM Z_UDS_LOG WHERE (REQUEST_TIME BETWEEN '{$table_s_date}' AND '{$table_e_date}') AND DEST_PHONE NOT LIKE '%#%'";
        $biz_sms_query = mysqli_query($my_db, $biz_sms_sql);
		while($biz_sms = mysqli_fetch_assoc($biz_sms_query))
		{
            $sms_org_hp = $biz_sms['DEST_PHONE'];
            $sms_hp_len = strlen($sms_org_hp);
            $sms_hp_val = substr($sms_org_hp, -5, 5);
            $sms_hp = str_pad($sms_hp_val, $sms_hp_len, "#", STR_PAD_LEFT);

            $sms_upd_sql = "UPDATE Z_UDS_LOG SET DEST_PHONE='{$sms_hp}' WHERE CMID='{$biz_sms['CMID']}'";
            mysqli_query($my_db, $sms_upd_sql);
        }

        $easy_sms_sql   = "SELECT MSG_ID, DEST_PHONE FROM EASY_SEND_LOG WHERE (REQUEST_TIME BETWEEN '{$table_s_date}' AND '{$table_e_date}') AND DEST_PHONE NOT LIKE '%#%' AND MST_TYPE IN('SMS','LMS')";
        $easy_sms_query = mysqli_query($my_db, $easy_sms_sql);
        while($easy_sms = mysqli_fetch_assoc($easy_sms_query))
        {
            $sms_org_hp = $easy_sms['DEST_PHONE'];
            $sms_hp_len = strlen($sms_org_hp);
            $sms_hp_val = substr($sms_org_hp, -5, 5);
            $sms_hp = str_pad($sms_hp_val, $sms_hp_len, "#", STR_PAD_LEFT);

            $sms_upd_sql = "UPDATE EASY_SEND_LOG SET DEST_PHONE='{$sms_hp}' WHERE MSG_ID='{$easy_sms['MSG_ID']}'";
            mysqli_query($my_db, $sms_upd_sql);
        }

        if($base_month == $uds_month){
            break;
        }

        $uds_month = date('Y-m', strtotime("{$uds_month} -1 months"));
        $idx++;
	}

    alert("6개월 이상 수신번호 삭제처리했습니다.", $return_url);
}
elseif($type == 'kakao')
{
    $idx = 0;
    $uds_month = $pre_month;

    while(true)
    {
        $table_month = date('Ym',strtotime($uds_month));
        $table_year  = date('Y',strtotime($uds_month));

        if($idx == 0){
            $table_s_date = $uds_month."-01 00:00:00";
            $table_e_date = $pre_date." 23:59:59";
        }else{
            $table_s_date = $uds_month."-01 00:00:00";
            $table_e_date = $uds_month."-31 23:59:59";
        }

        # 뿌리오 알림톡
        $biz_kakao_sql   = "SELECT CMID, DEST_NAME, DEST_PHONE FROM BIZ_KAKAO_LOG WHERE (REQUEST_TIME BETWEEN '{$table_s_date}' AND '{$table_e_date}') AND DEST_PHONE NOT LIKE '%#%'";
        $biz_kakao_query = mysqli_query($my_db, $biz_kakao_sql);
        while($biz_kakao = mysqli_fetch_assoc($biz_kakao_query))
        {
            $dest_org_phone = $biz_kakao['DEST_PHONE'];
            $dest_phone_len = strlen($dest_org_phone);
            $dest_hp_val 	= substr($dest_org_phone, -5, 5);
            $dest_hp 		= str_pad($dest_hp_val, $dest_phone_len, "#", STR_PAD_LEFT);

            $dest_org_name  = $biz_kakao['DEST_NAME'];

            if(preg_match("/[\xE0-\xFF][\x80-\xFF][\x80-\xFF]/", $dest_org_name))
            {
                $dest_name_len  	= mb_strlen($dest_org_name)-2;

                if(mb_strlen($dest_org_name) == 0){
                    $dest_name_first 	= "##";
                    $dest_name_end 		= "";
                    $dest_name_middle	= "";
                }elseif($dest_name_len <= 0){
                    $dest_name_first 	= mb_substr($dest_org_name, 0, 1)."#";
                    $dest_name_end 		= "";
                    $dest_name_middle	= "";
				}else{
                    $dest_name_first 	= mb_substr($dest_org_name, 0, 1);
                    $dest_name_end 		= mb_substr($dest_org_name, -1, 1);
                    $dest_name_middle	= str_pad("", $dest_name_len, "#", STR_PAD_RIGHT);
				}
			} else{
                $dest_name_len = strlen($dest_org_name)-2;

                if(strlen($dest_org_name) == 0) {
                    $dest_name_first 	= "##";
                    $dest_name_end 		= "";
                    $dest_name_middle	= "";
                }elseif($dest_name_len <= 0){
                    $dest_name_first 	= substr($dest_org_name, 0, 1)."#";
                    $dest_name_end 		= "";
                    $dest_name_middle	= "";
                }else{
                    $dest_name_first 	= substr($dest_org_name, 0, 1);
                    $dest_name_end 		= substr($dest_org_name, -1, 1);
                    $dest_name_middle	= str_pad("", $dest_name_len, "#", STR_PAD_RIGHT);
                }
			}

            $dest_name = $dest_name_first.$dest_name_middle.$dest_name_end;

            $kakao_upd_sql = "UPDATE BIZ_KAKAO_LOG SET DEST_PHONE='{$dest_hp}', DEST_NAME='{$dest_name}' WHERE CMID='{$biz_kakao['CMID']}'";
            mysqli_query($my_db, $kakao_upd_sql);
        }

        # 비즈엠 알림톡
        $easy_kakao_sql   = "SELECT MSG_ID, DEST_NAME, DEST_PHONE FROM EASY_SEND_LOG WHERE (REQUEST_TIME BETWEEN '{$table_s_date}' AND '{$table_e_date}') AND DEST_PHONE NOT LIKE '%#%' AND MSG_TYPE='AT'";
        $easy_kakao_query = mysqli_query($my_db, $easy_kakao_sql);
        while($easy_kakao = mysqli_fetch_assoc($biz_kakao_query))
        {
            $dest_org_phone = $easy_kakao['DEST_PHONE'];
            $dest_phone_len = strlen($dest_org_phone);
            $dest_hp_val 	= substr($dest_org_phone, -5, 5);
            $dest_hp 		= str_pad($dest_hp_val, $dest_phone_len, "#", STR_PAD_LEFT);

            $dest_org_name  = $easy_kakao['DEST_NAME'];

            if(preg_match("/[\xE0-\xFF][\x80-\xFF][\x80-\xFF]/", $dest_org_name))
            {
                $dest_name_len  	= mb_strlen($dest_org_name)-2;

                if(mb_strlen($dest_org_name) == 0){
                    $dest_name_first 	= "##";
                    $dest_name_end 		= "";
                    $dest_name_middle	= "";
                }elseif($dest_name_len <= 0){
                    $dest_name_first 	= mb_substr($dest_org_name, 0, 1)."#";
                    $dest_name_end 		= "";
                    $dest_name_middle	= "";
                }else{
                    $dest_name_first 	= mb_substr($dest_org_name, 0, 1);
                    $dest_name_end 		= mb_substr($dest_org_name, -1, 1);
                    $dest_name_middle	= str_pad("", $dest_name_len, "#", STR_PAD_RIGHT);
                }
            } else{
                $dest_name_len = strlen($dest_org_name)-2;

                if(strlen($dest_org_name) == 0) {
                    $dest_name_first 	= "##";
                    $dest_name_end 		= "";
                    $dest_name_middle	= "";
                }elseif($dest_name_len <= 0){
                    $dest_name_first 	= substr($dest_org_name, 0, 1)."#";
                    $dest_name_end 		= "";
                    $dest_name_middle	= "";
                }else{
                    $dest_name_first 	= substr($dest_org_name, 0, 1);
                    $dest_name_end 		= substr($dest_org_name, -1, 1);
                    $dest_name_middle	= str_pad("", $dest_name_len, "#", STR_PAD_RIGHT);
                }
            }

            $dest_name = $dest_name_first.$dest_name_middle.$dest_name_end;

            $kakao_upd_sql = "UPDATE EASY_SEND_LOG SET DEST_PHONE='{$dest_hp}', DEST_NAME='{$dest_name}' WHERE MSG_ID='{$easy_kakao['MSG_ID']}'";
            mysqli_query($my_db, $kakao_upd_sql);
        }

        if($base_month == $uds_month){
            break;
        }

        $uds_month = date('Y-m', strtotime("{$uds_month} -1 months"));
        $idx++;
    }

    alert("6개월 이상 수신번호 삭제처리했습니다.", $return_url);
}


?>
