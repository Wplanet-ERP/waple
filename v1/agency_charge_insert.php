<?php
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
include_once('inc/model/Company.php');
include_once('inc/model/Agency.php');

# 파일 변수
$charge_company     = (isset($_POST['charge_company']))?$_POST['charge_company'] : "4004";
$charge_month       = (isset($_POST['charge_month']))?$_POST['charge_month'] : date('Y-m-d', strtotime('-1 months'));
$file_name          = $_FILES["charge_file"]["tmp_name"];

$agency_model       = Agency::Factory();
$agency_model->setMainInit("agency_charge", "ac_no");
$upd_data           = [];
$regdate            = date('Y-m-d H:i:s');
$prev_month         = date('Y-m', strtotime("{$charge_month} -1 months"));

# 현재 소스 리스트(OFF 처리된 것들 메모)
$agency_charge_off_sql      = "SELECT * FROM agency_charge WHERE base_mon='{$charge_month}' AND display='2'";
$agency_charge_off_query    = mysqli_query($my_db, $agency_charge_off_sql);
$agency_charge_off_list     = [];
while($agency_charge_off = mysqli_fetch_assoc($agency_charge_off_query))
{
    if(!empty($agency_charge_off['task_run']) || !empty($agency_charge_off['run_price']))
    {
        $agency_charge_off_list[$agency_charge_off['media']][$agency_charge_off['partner']] = array(
            "task_run"  => $agency_charge_off['task_run'],
            "run_price" => $agency_charge_off['run_price'],
        );
    }
}

# 기본 셋팅
$chk_agency_sql     = "SELECT *, (SELECT s.team FROM staff s WHERE s.s_no=ac.manager) as team, (SELECT c.c_name FROM company c WHERE c.c_no=ac.partner) as c_name, (SELECT COUNT(*) FROM agency_charge sub WHERE sub.partner=ac.partner ANd sub.base_mon='{$charge_month}' AND sub.display='1') as next_cnt FROM agency_charge as ac WHERE `ac`.base_mon='{$prev_month}' AND `ac`.display='1'";
$chk_agency_query  = mysqli_query($my_db, $chk_agency_sql);
while ($chk_agency = mysqli_fetch_assoc($chk_agency_query))
{
    if($chk_agency['next_cnt'] == 0)
    {
        $charge_data = array(
            "base_mon"      => $charge_month,
            "agency"        => $chk_agency['agency'],
            "media"         => $chk_agency['media'],
            "advertiser_id" => $chk_agency['advertiser_id'],
            "partner"       => $chk_agency['partner'],
            "partner_name"  => $chk_agency['c_name'],
            "manager"       => $chk_agency['manager'],
            "manager_team"  => $chk_agency['team'],
            "prev_price"    => $chk_agency['total_price'],
            "total_price"   => 0,
            "regdate"       => $regdate,
        );

        if(isset($agency_charge_off_list[$chk_agency['media']][$chk_agency['partner']]))
        {
            $charge_off_data = $agency_charge_off_list[$chk_agency['media']][$chk_agency['partner']];
            $charge_data['task_run'] = !empty($charge_off_data['task_run']) ? addslashes($charge_off_data['task_run']) : "NULL";
            $charge_data['run_price'] = !empty($charge_off_data['run_price']) ? $charge_off_data['run_price'] : "NULL";
        }

        $agency_model->insert($charge_data);
    }
}

# 현재 소스 리스트
$agency_charge_sql      = "SELECT * FROM agency_charge WHERE base_mon='{$charge_month}' AND display='1'";
$agency_charge_query    = mysqli_query($my_db, $agency_charge_sql);
$charge_chk_list        = [];
while($agency_charge = mysqli_fetch_assoc($agency_charge_query)){
    $charge_chk_list[$agency_charge['ac_no']] = array(
        "ac_no"         => $agency_charge['ac_no'],
        "supply_fee"    => 0,
        "supply_vat"    => 0,
        "supply_price"  => 0,
    );
}

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();
for ($i = 11; $i <= $totalRow; $i++)
{
    $media_val = $advertiser_id = "";
    $supply_fee = $supply_vat = $supply_price = 0;

    $media_val      = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 광고매체
    $advertiser_id  = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 광고주 KEY
    $supply_fee     = (int)trim(addslashes(str_replace(",","",$objWorksheet->getCell("E{$i}")->getValue())));   // 수수료(공급가액)
    $supply_vat     = (int)trim(addslashes(str_replace(",","",$objWorksheet->getCell("F{$i}")->getValue())));   // 수수료(%)
    $supply_price   = $supply_fee+$supply_vat;
    $advertiser_id  = str_replace(":naver", "", $advertiser_id);
    $media_type     = ($media_val == "카카오") ? "267" : "263";
    $company_type   = ($media_val == "카카오") ? "kakao" : "naver";

    if($media_val != "네이버" && $media_val != "카카오"){
        continue;
    }

    $cur_check_sql     = "SELECT * FROM agency_charge WHERE base_mon='{$charge_month}' AND advertiser_id='{$advertiser_id}' AND media='{$media_type}' AND agency='{$charge_company}' AND display='1'";
    $cur_check_query   = mysqli_query($my_db, $cur_check_sql);
    $cur_check_item    = mysqli_fetch_assoc($cur_check_query);

    if(!isset($cur_check_item['ac_no']))
    {
        $company_sql    = "SELECT *, (SELECT s.team FROm staff s WHERE s.s_no=c.s_no) as team FROM company c WHERE {$company_type} LIKE '{$advertiser_id}%'";
        $company_query  = mysqli_query($my_db, $company_sql);
        $company_item   = mysqli_fetch_assoc($company_query);

        if(!isset($company_item['c_no']))
        {
            echo "등록되지 않은 광고주 아이디 {$advertiser_id} 입니다.";
            exit;
        }

        if(isset($agency_charge_off_list[$media_type][$company_item['c_no']])) {
            $charge_off_data = $agency_charge_off_list[$media_type][$company_item['c_no']];
            $ins_sql = "INSERT INTO agency_charge SET base_mon='{$charge_month}', agency='{$charge_company}', media='{$media_type}', advertiser_id='{$advertiser_id}', partner='{$company_item['c_no']}', partner_name='{$company_item['c_name']}', manager='{$company_item['s_no']}', manager_team='{$company_item['team']}', task_run='{$charge_off_data['task_run']}', run_price='{$charge_off_data['run_price']}', regdate=now()";
        }
        else {
            $ins_sql = "INSERT INTO agency_charge SET base_mon='{$charge_month}', agency='{$charge_company}', media='{$media_type}', advertiser_id='{$advertiser_id}', partner='{$company_item['c_no']}', partner_name='{$company_item['c_name']}', manager='{$company_item['s_no']}', manager_team='{$company_item['team']}', regdate=now()";
        }

        if(mysqli_query($my_db, $ins_sql))
        {
            $new_ac_no = mysqli_insert_id($my_db);
            $charge_chk_list[$new_ac_no] = array(
                "ac_no"         => $new_ac_no,
                "supply_fee"    => $supply_fee,
                "supply_vat"    => $supply_vat,
                "supply_price"  => $supply_price,
            );
        }else{
            echo "등록에 실패했습니다.";
            exit;
        }
    }
    else
    {
        $charge_chk_list[$cur_check_item['ac_no']]["supply_fee"]    += $supply_fee;
        $charge_chk_list[$cur_check_item['ac_no']]["supply_vat"]    += $supply_vat;
        $charge_chk_list[$cur_check_item['ac_no']]["supply_price"]  += $supply_price;
    }
}

if($charge_chk_list)
{
    foreach($charge_chk_list as $charge_data)
    {
        $charge_sql     = "
            SELECT 
                `ac`.ac_no,
                `ac`.ad_charge_price,
                `ac`.media,
                (SELECT SUM(wd.wd_money) AS total FROM withdraw wd LEFT JOIN `work` w ON w.w_no=wd.w_no WHERE wd.wd_state='3' AND wd.w_no > 0 AND w.prd_no='12' AND wd.c_no='814' AND DATE_FORMAT(wd.wd_date, '%Y-%m')='{$charge_month}' AND w.c_no=`ac`.partner) as naver_charge_price,
                (SELECT SUM(wd.wd_money) AS total FROM withdraw wd LEFT JOIN `work` w ON w.w_no=wd.w_no WHERE wd.wd_state='3' AND wd.w_no > 0 AND w.prd_no='12' AND wd.c_no='1079' AND DATE_FORMAT(wd.wd_date, '%Y-%m')='{$charge_month}' AND w.c_no=`ac`.partner) as kakao_charge_price,
                (SELECT sub.total_price FROM agency_charge sub WHERE sub.base_mon='{$prev_month}' AND `sub`.agency=`ac`.agency AND `sub`.media=`ac`.media AND `sub`.partner=`ac`.partner AND `sub`.display='1') as prev_total
            FROM agency_charge `ac`
            WHERE ac_no='{$charge_data['ac_no']}' AND `ac`.display='1'
        ";
        $charge_query   = mysqli_query($my_db, $charge_sql);
        $charge_item    = mysqli_fetch_assoc($charge_query);

        $charge_item['charge_price'] = ($charge_item['media'] == '263') ? $charge_item['naver_charge_price'] : $charge_item['kakao_charge_price'];
        $total_price    = $charge_item['prev_total']+(int)$charge_item['charge_price']-$charge_data['supply_price']+$charge_item['ad_charge_price'];

        $charge_data = array(
            "ac_no"          => $charge_item['ac_no'],
            "charge_price"   => (int)$charge_item['charge_price'],
            "prev_price"     => $charge_item['prev_total'],
            "ad_price"       => $charge_data['supply_fee'],
            "ad_price_vat"   => $charge_data['supply_vat'],
            "ad_total_price" => $charge_data['supply_price'],
            "total_price"    => $total_price,
        );

        $upd_data[] = $charge_data;
    }
}

if (!$agency_model->multiUpdate($upd_data)){
    echo "계산서 소진금 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    exit;
}else{
    $del_sql = "DELETE FROM agency_charge WHERE base_mon='{$charge_month}' AND display='2'";
    mysqli_query($my_db, $del_sql);
    exit("<script>alert('소진금이 반영 되었습니다.');location.href='agency_charge_management.php?sch_base_mon={$charge_month}';</script>");
}

?>
