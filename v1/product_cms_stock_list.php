<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');
require('inc/helper/product_cms.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/Company.php');

# Navigation & My Quick
$nav_prd_no  = "42";
$nav_title   = "재고 현황 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

$is_editable = false;
if(permissionNameCheck($session_permission,"마스터관리자") || ($session_s_no == '17' || $session_s_no == '9')){
    $is_editable = true;
}
$smarty->assign("is_editable", $is_editable);

# 데이터
$unit_model     = ProductCmsUnit::Factory();
$brand_option   = $unit_model->getDistinctUnitCompanyData('brand');
$sch_sup_c_list = $unit_model->getDistinctUnitCompanyData('sup_c_no');
$sup_ord_c_list = $unit_model->getDistinctUnitOrdCompanyData('sup_c_no');
$ord_s_list     = $unit_model->getOrdStaffData();

# 검색조건
$add_where          = "1=1";
$add_order          = "prd_unit ASC, stock_c_no ASC, warehouse ASC";

$last_stock_sql     = "SELECT * FROM (SELECT DISTINCT stock_c_no, stock_date FROM product_cms_stock ORDER BY stock_date DESC) as date_result GROUP BY stock_c_no";
$last_stock_query   = mysqli_query($my_db, $last_stock_sql);
$last_stock_list    = [];
while($last_stock_result  = mysqli_fetch_assoc($last_stock_query)){
    $last_stock_list[$last_stock_result['stock_c_no']] = $last_stock_result['stock_date'];
}
$last_stock_date = $last_stock_list['2809'];

$sch_no                 = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_stock_date         = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : $last_stock_date;
$sch_sup_c_no           = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_ord_s_no           = isset($_GET['sch_ord_s_no']) ? $_GET['sch_ord_s_no'] : "";
$sch_prd_name           = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_stock_c_name       = isset($_GET['sch_stock_c_name']) ? $_GET['sch_stock_c_name'] : "";
$sch_unit_type          = isset($_GET['sch_unit_type']) ? $_GET['sch_unit_type'] : "";
$sch_stock_prd_sku      = isset($_GET['sch_stock_prd_sku']) ? $_GET['sch_stock_prd_sku'] : "";
$sch_log_company        = isset($_GET['sch_log_company']) ? $_GET['sch_log_company'] : "";
$sch_warehouse          = isset($_GET['sch_warehouse']) ? $_GET['sch_warehouse'] : "";
$sch_is_good_warehouse  = isset($_GET['sch_is_good_warehouse']) ? $_GET['sch_is_good_warehouse'] : "";
$sch_file_no            = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";

if (!empty($sch_stock_date))
{
    if($sch_stock_date != $last_stock_date){
        foreach($last_stock_list as $stock_c_no => $last_date){
            $last_stock_list[$stock_c_no] = $sch_stock_date;
        }
    }
    $smarty->assign("sch_stock_date", $sch_stock_date);
}

if (!empty($sch_no)) {
    $add_where .= " AND pcs.`no`='{$sch_no}'";
    $smarty->assign("sch_no", $sch_no);
}

if (!empty($sch_sup_c_no)) {
    $add_where   .= " AND pcs.sup_c_no='{$sch_sup_c_no}'";
    $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
}

if (!empty($sch_ord_s_no)) {
    $add_where .= " AND pcs.ord_s_no='{$sch_ord_s_no}'";
    $sch_sup_c_list = $sup_ord_c_list[$sch_ord_s_no];
    $smarty->assign("sch_ord_s_no", $sch_ord_s_no);
}

if (!empty($sch_prd_name)) {
    $add_where .= " AND pcs.prd_name like '%{$sch_prd_name}%'";
    $smarty->assign("sch_prd_name", $sch_prd_name);
}

if (!empty($sch_stock_c_name)) {
    $add_where .= " AND pcs.stock_c_no IN (SELECT c.c_no FROM company c WHERE c.c_name like '%{$sch_stock_c_name}%')";
    $smarty->assign("sch_stock_c_name", $sch_stock_c_name);
}

if (!empty($sch_unit_type)) {
    $add_where .= " AND pcu.`type` = '{$sch_unit_type}'";
    $smarty->assign("sch_unit_type", $sch_unit_type);
}

if (!empty($sch_stock_prd_sku)) {
    $add_where .= " AND pcs.stock_prd_sku like '%{$sch_stock_prd_sku}%'";
    $smarty->assign("sch_stock_prd_sku", $sch_stock_prd_sku);
}

if (!empty($sch_log_company)) {
    $add_where .= " AND pcs.stock_c_no = '{$sch_log_company}'";
    $smarty->assign("sch_log_company", $sch_log_company);
}

if (!empty($sch_warehouse)) {
    $add_where .= " AND pcs.warehouse LIKE '%{$sch_warehouse}%'";
    $smarty->assign("sch_warehouse", $sch_warehouse);
}

if (!empty($sch_is_good_warehouse) && $sch_is_good_warehouse == '1') {
    $add_where .= " AND IF(pcs.stock_c_no=2809,(pcs.warehouse LIKE '%정상%' OR pcs.warehouse LIKE '%세이프인%'), (pcs.warehouse LIKE '%창고%'))";
    $smarty->assign("sch_is_good_warehouse", $sch_is_good_warehouse);
}

if (!empty($sch_file_no)) {
    $add_where .= " AND pcs.file_no = '{$sch_file_no}'";
    $smarty->assign("sch_file_no", $sch_file_no);
}

// 페이지에 따른 limit 설정
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
$smarty->assign("page",$pages);

$stock_date_sql = "";

$stock_idx = 1;
$stock_cnt = count($last_stock_list);
foreach($last_stock_list as $log_c_no => $stock_date)
{
    $stock_date_sql .= empty($stock_date_sql) ? "SELECT * FROM (" : "";

    $stock_date_sql .= "(SELECT pcs.*, pcu.`type` as unit_type, pcu.ord_s_no as org_ord_s_no, pcu.ord_sub_s_no, pcu.ord_third_s_no FROM product_cms_stock pcs LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcs.prd_unit WHERE {$add_where} AND stock_c_no='{$log_c_no}' AND stock_date='{$stock_date}')";

    if($stock_idx == $stock_cnt){
        $stock_date_sql .= ") AS result_search ";
    }else{
        $stock_date_sql .= " UNION ";
    }

    $stock_idx++;
}

# 전체 게시물 수
$stock_total_sql   = "
    SELECT
        COUNT(DISTINCT `prd_unit`) as cnt
    FROM
    (
        SELECT 
            pcs.no,
            pcs.prd_unit,
            pcs.stock_c_no,
            pcs.warehouse,
            pcs.qty,
            (SELECT SUM(co.quantity) FROM commerce_order co LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no` WHERE co.option_no=pcs.prd_unit AND `cos`.sup_c_no=pcs.sup_c_no AND `cos`.display='1' AND `cos`.state='2' AND `co`.`type` IN('request','supply') ) AS co_qty
        FROM ({$stock_date_sql}) as pcs
    ) AS result_total
    WHERE qty != 0 OR co_qty != 0
";
$stock_total_query  = mysqli_query($my_db, $stock_total_sql);
$stock_total_result = mysqli_fetch_assoc($stock_total_query);
$stock_total        = $stock_total_result['cnt'];

//페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($stock_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "product_cms_stock_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $stock_total);
$smarty->assign("pagelist", $page);
$smarty->assign("ord_page_type", $page_type);

# prd_unit 구분
$add_group_where    = "1=1";
$chk_group_sql      = "
    SELECT
        prd_unit
    FROM
    (
        SELECT 
            pcs.no,
            pcs.prd_unit,
            pcs.stock_c_no,
            pcs.warehouse,
            pcs.qty,
            (SELECT SUM(co.quantity) FROM commerce_order co LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no` WHERE co.option_no=pcs.prd_unit AND `cos`.sup_c_no=pcs.sup_c_no AND `cos`.display='1' AND `cos`.state='2' AND `co`.`type` IN('request','supply') ) AS co_qty
        FROM ({$stock_date_sql}) as pcs
    ) AS result_total WHERE qty != 0 OR co_qty != 0
    GROUP BY prd_unit
    ORDER BY {$add_order}
    LIMIT {$offset},{$num}             
";
$chk_group_query = mysqli_query($my_db, $chk_group_sql);
$add_group_unit_list = [];
while($chk_group_unit = mysqli_fetch_assoc($chk_group_query)){
    $add_group_unit_list[] = $chk_group_unit['prd_unit'];
}

if(!empty($add_group_unit_list)){
    $add_group_unit_text = implode(",", $add_group_unit_list);
    $add_group_where .= " AND prd_unit IN({$add_group_unit_text})";
}


$stock_sql = "
    SELECT 
       rs.*,
       (SELECT s.s_name FROM staff s WHERE s.s_no=rs.org_ord_s_no) as ord_s_name,
       (SELECT s.s_name FROM staff s WHERE s.s_no=rs.ord_sub_s_no) as ord_sub_s_name,
       (SELECT s.s_name FROM staff s WHERE s.s_no=rs.ord_third_s_no) as ord_third_s_name
    FROM 
    (
        SELECT
            *,
            (SELECT um.file_path FROM upload_management um WHERE um.file_no=pcs.file_no) as file_path,
            (SELECT um.file_name FROM upload_management um WHERE um.file_no=pcs.file_no) as file_name,
            (SELECT c.c_name FROM company c WHERE c.c_no=pcs.sup_c_no) as sup_c_name,
            (SELECT c.c_name FROM company c WHERE c.c_no=pcs.stock_c_no) as stock_c_name,
            (
                SELECT 
                    SUM(co.quantity) 
                FROM commerce_order co 
                LEFT JOIN commerce_order_set `cos` ON co.set_no=`cos`.`no`
                WHERE co.option_no=pcs.prd_unit AND `cos`.sup_c_no=pcs.sup_c_no AND `cos`.display='1' AND `cos`.state='2' AND `co`.`type` IN('request','supply')	
            ) AS co_qty
        FROM ({$stock_date_sql}) as pcs
        WHERE {$add_group_where}
    ) AS rs
    WHERE (co_qty != 0 OR qty != 0)
    ORDER BY {$add_order}
";
$stock_query        = mysqli_query($my_db, $stock_sql);
$stock_tmp_list     = [];
$stock_list         = [];
$unit_type_option   = getUnitTypeOption();
while($stock = mysqli_fetch_assoc($stock_query))
{
    $stock['unit_type_name'] = $unit_type_option[$stock['unit_type']];
    $stock_list[$stock['prd_unit']][] = $stock;
}

$company_model			= Company::Factory();
$log_company_list     	= $company_model->getLogisticsList();

$smarty->assign('page_type_list', getPageTypeOption('4'));
$smarty->assign('sch_sup_c_list', $sch_sup_c_list);
$smarty->assign('sch_ord_s_list', $ord_s_list);
$smarty->assign('unit_type_option', $unit_type_option);
$smarty->assign('log_company_list', $log_company_list);
$smarty->assign('stock_list', $stock_list);

$smarty->display('product_cms_stock_list.html');
?>
