<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_price.php');
require('inc/model/CommerceOrder.php');
require('inc/model/ProductCmsUnit.php');

# Model Init
$comm_set_model = CommerceOrder::Factory();
$comm_set_model->setMainInit("commerce_order_set", "no");

$commerce_model = CommerceOrder::Factory();
$commerce_model->setMainInit("commerce_order", "no");

$unit_model     = ProductCmsUnit::Factory();

# 프로세스 처리
$proc = isset($_POST['process']) ? $_POST['process'] : "";
if($proc == "f_reference")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$comm_set_model->update(array("no" => $no, "reference" => $value)))
        echo "참조내용 저장에 실패 하였습니다.";
    else
        echo "참조내용이 저장 되었습니다.";
    exit;
}
elseif($proc == "f_tel")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$comm_set_model->update(array("no" => $no, "tel" => $value)))
        echo "전화번호 저장에 실패 하였습니다.";
    else
        echo "전화번호가 저장 되었습니다.";
    exit;
}
elseif($proc == "f_fax")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$comm_set_model->update(array("no" => $no, "fax" => $value)))
        echo "FAX번호 저장에 실패 하였습니다.";
    else
        echo "FAX번호가 저장 되었습니다.";
    exit;
}
elseif($proc == "f_req_date")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$comm_set_model->update(array("no" => $no, "req_date" => $value)))
        echo "발주일자 저장에 실패 하였습니다.";
    else
        echo "발주일자가 저장 되었습니다.";
    exit;
}
elseif($proc == "f_option_no")
{
    $no             = (isset($_POST['no'])) ? $_POST['no'] : "";
    $option_no      = (isset($_POST['val'])) ? $_POST['val'] : "";

    $unit_item 	    = $unit_model->getItem($option_no);
    $unit_price     = $unit_item['sup_price_vat'];

    $commerce_item  = $commerce_model->getOrderItem($no);

    $set_no         = $commerce_item['set_no'];
    $quantity       = $commerce_item['quantity'];
    $total_price    = $unit_price*$quantity;
    $supply_price   = ($commerce_item['sup_loc_type'] == '2') ? $total_price : round($total_price/1.1, 0);
    $vat_price      = ($commerce_item['sup_loc_type'] == '2') ? 0 : ($total_price-$supply_price);

    $upd_data       = array(
        "no"            => $no,
        "option_no"     => $option_no,
        "unit_price"    => $unit_price,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price
    );

    if (!$commerce_model->update($upd_data))
        echo "구성품목 변경에 실패 하였습니다.";
    else
        exit("<script>location.href='commerce_order_iframe_request.php?no={$set_no}';</script>");
    exit;
}
elseif($proc == "f_priority")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";

    $commerce_item  = $commerce_model->getOrderItem($no);
    $set_no         = $commerce_item['set_no'];

    if (!$commerce_model->updatePriority($no, $set_no, $value))
        echo "순번 저장에 실패 하였습니다.";
    else {
        exit("<script>location.href='commerce_order_iframe_request.php?no={$set_no}';</script>");
    }
    exit;
}
elseif($proc == "f_quantity")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";
    $value  = str_replace(",","",trim($value));

    $commerce_item  = $commerce_model->getOrderItem($no);
    $set_no         = $commerce_item['set_no'];
    $total_price    = $value*$commerce_item['unit_price'];
    $supply_price   = ($commerce_item['sup_loc_type'] == '2') ? $total_price : round($total_price/1.1, 0);
    $vat_price      = ($commerce_item['sup_loc_type'] == '2') ? 0 : ($total_price-$supply_price);

    $upd_data       = array(
        "no"            => $no,
        "quantity"      => $value,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price
    );

    if (!$commerce_model->update($upd_data))
        echo "수량 저장에 실패 하였습니다.";
    else
        exit("<script>location.href='commerce_order_iframe_request.php?no={$set_no}';</script>");
    exit;
}
elseif($proc == "f_unit_price")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";
    $value  = str_replace(",","",trim($value));

    $commerce_item  = $commerce_model->getOrderItem($no);
    $set_no         = $commerce_item['set_no'];
    $total_price    = $value*$commerce_item['quantity'];
    $supply_price   = ($commerce_item['sup_loc_type'] == '2') ? $total_price : round($total_price/1.1, 0);
    $vat_price      = ($commerce_item['sup_loc_type'] == '2') ? 0 : ($total_price-$supply_price);

    $upd_data       = array(
        "no"            => $no,
        "unit_price"    => $value,
        "supply_price"  => $supply_price,
        "vat"           => $vat_price
    );

    if (!$commerce_model->update($upd_data))
        echo "단가(VAT 포함) 저장에 실패 하였습니다.";
    else
        exit("<script>location.href='commerce_order_iframe_request.php?no={$set_no}';</script>");
    exit;
}
elseif($proc == "f_supply_price")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";
    $value  = str_replace(",", "", trim($value));

    $commerce_item  = $commerce_model->getOrderItem($no);
    $set_no         = $commerce_item['set_no'];

    if (!$commerce_model->update(array("no" => $no, "supply_price" => $value)))
        echo "공급가 저장에 실패 하였습니다.";
    else
        exit("<script>location.href='commerce_order_iframe_request.php?no={$set_no}';</script>");
    exit;

}
elseif($proc == "f_vat")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? $_POST['val'] : "";
    $value  = str_replace(",", "", trim($value));

    $commerce_item  = $commerce_model->getOrderItem($no);
    $set_no         = $commerce_item['set_no'];

    if (!$commerce_model->update(array("no" => $no, "vat" => $value)))
        echo "부가세 저장에 실패 하였습니다.";
    else
        exit("<script>location.href='commerce_order_iframe_request.php?no={$set_no}';</script>");
    exit;
}
elseif($proc == "f_memo")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$commerce_model->update(array("no" => $no, "memo" => $value)))
        echo "비고 저장에 실패 하였습니다.";
    else
        echo "비고가 저장 되었습니다.";
    exit;
}
elseif($proc == "add_order_request") # 발주 직접추가
{
    $set_no         = $_POST['set_no'];
    $sup_loc_type   = $_POST['sup_loc_type'];
    $base_log_c_no  = "2809";

    if(empty($set_no)|| empty($_POST['req_option_no_new']) || empty($_POST['req_quantity_new'])){
        exit("<script>alert('필수 데이터가 없습니다. 발주 상세내역 추가에 실패했습니다.');history.back();</script>");
    }

    $comm_set  = $comm_set_model->getItem($set_no);
    $unit_item = $unit_model->getItem($_POST['req_option_no_new'], $comm_set['log_c_no']);

    if(!isset($unit_item['no']) || empty($unit_item['no'])){
        $unit_item = $unit_model->getItem($_POST['req_option_no_new'], $base_log_c_no);
    }
    
    if(!isset($unit_item['no']) || empty($unit_item['no']))
    {
        exit("<script>alert('해당 구성품목이 없습니다. 입고완료 상세내역 추가에 실패했습니다.');location.href='commerce_order_iframe_request.php?no={$set_no}';</script>");
    }

    $req_quantity_new = str_replace(",", "", trim($_POST['req_quantity_new'])); // 컴마 제거하기
    $unit_price       = !empty($unit_item['sup_price_vat']) ? $unit_item['sup_price_vat'] : 0;
    $total_price      = $unit_price*$req_quantity_new;
    $supply_price     = ($sup_loc_type == '2') ? $total_price : round($total_price/1.1,0);
    $vat_price        = ($sup_loc_type == '2') ? 0 : ($total_price-$supply_price);

    $f_priority_value   = !empty($_POST['req_priority_new']) ? $_POST['req_priority_new'] : 1;

    $ins_data = array(
            "set_no"        => $set_no,
            "type"          => 'request',
            "option_no"     => $_POST['req_option_no_new'],
            "unit_price"    => $unit_price,
            "quantity"      => $req_quantity_new,
            "supply_price"  => $supply_price,
            "vat"           => $vat_price,
            "regdate"       => date("Y-m-d H:i:s"),
            "priority"      => $f_priority_value,
            "memo"          => addslashes(trim($_POST['req_memo_new']))
    );

    if($commerce_model->insert($ins_data)) {
        $commerce_model->updatePriority($commerce_model->getInsertId(), $set_no, $f_priority_value);
        exit("<script>alert('발주요청 상세내역 추가에 성공했습니다');location.href='commerce_order_iframe_request.php?no={$set_no}';</script>");
    } else {
        exit("<script>alert('발주요청 상세내역 추가에 실패했습니다');location.href='commerce_order_iframe_request.php?no={$set_no}'</script>");
    }
}
elseif($proc == 'request_delete')
{
    $set_no = (isset($_POST['set_no'])) ? $_POST['set_no'] : "";
    $co_no 	= (isset($_POST['co_no'])) ? $_POST['co_no'] : "";

    if(empty($set_no) || empty($co_no)){
        echo("<script>alert('삭제에 실패 하였습니다.');</script>");
    }
    else
    {
        $commerce_item      = $commerce_model->getOrderItem($co_no);
        $f_priority_new		= $commerce_item['priority'];

        if (!$commerce_model->delete($co_no)) {
            echo("<script>alert('삭제에 실패 하였습니다.');</script>");
        } else {
            $commerce_model->deleteCommercePriority($set_no, $f_priority_new);
            echo("<script>alert('삭제했습니다.');</script>");
        }
    }
    exit ("<script>location.href='commerce_order_iframe_request.php?no={$set_no}';</script>");
}
elseif($proc == "f_delivery_date")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$comm_set_model->update(array("no" => $no, "delivery_date" => $value)))
        echo "납기일자 저장에 실패 하였습니다.";
    else
        echo "납기일자가 저장 되었습니다.";
    exit;
}
elseif($proc == "f_pay_method")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$comm_set_model->update(array("no" => $no, "pay_method" => $value)))
        echo "대금지불조건 저장에 실패 하였습니다.";
    else
        echo "대금지불조건이 저장 되었습니다.";
    exit;
}
elseif($proc == "f_sup_location")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$comm_set_model->update(array("no" => $no, "sup_location" => $value)))
        echo "납품장소 저장에 실패 하였습니다.";
    else
        echo "납품장소가가 장 되었습니다.";
    exit;
}
elseif($proc == "f_remark")
{
    $no     = (isset($_POST['no'])) ? $_POST['no'] : "";
    $value  = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";

    if (!$comm_set_model->update(array("no" => $no, "remark" => $value)))
        echo "특이사항 저장에 실패 하였습니다.";
    else
        echo "특이사항이 저장 되었습니다.";
    exit;
}

# 발주서 발주내역
$no = isset($_GET['no']) ? $_GET['no'] : "";

$commerce_order_set_sql="
		SELECT
			cos.no,
			cos.state,
		    cos.my_c_no,
            cos.sup_c_no,
		    cos.sup_c_name,
		    cos.order_count,
            cos.sup_loc_type,
		    cos.req_s_no,
			cos.req_date,
		    cos.tel,
            cos.fax,
		    cos.reference,
		    cos.delivery_date,
		    cos.pay_method,
		    cos.sup_location,
		    cos.remark
		FROM commerce_order_set cos
		WHERE cos.no='{$no}'
";
$commerce_order_set_query = mysqli_query($my_db, $commerce_order_set_sql);
$commerce_order_set       = mysqli_fetch_array($commerce_order_set_query);

$commerce_order_set['is_new'] = false;
if($commerce_order_set['req_date'] >= '2025-01-01'){
    $commerce_order_set['is_new'] = true;
}

$comm_my_company_sql    = "SELECT * FROM my_company WHERE my_c_no='{$commerce_order_set['my_c_no']}'";
$comm_my_company_query  = mysqli_query($my_db, $comm_my_company_sql);
$comm_my_company        = mysqli_fetch_assoc($comm_my_company_query);

$comm_company_sql       = "SELECT * FROM company WHERE c_no='{$commerce_order_set['sup_c_no']}'";
$comm_company_query     = mysqli_query($my_db, $comm_company_sql);
$comm_company           = mysqli_fetch_assoc($comm_company_query);
$comm_company['tel']    = !empty($commerce_order_set['tel']) ? $commerce_order_set['tel'] : str_replace("없음--","", $comm_company['tel']);
$comm_company['fax']    = !empty($commerce_order_set['fax']) ? $commerce_order_set['fax'] : str_replace("--","", $comm_company['fax']);

$comm_staff_sql         = "SELECT * FROM staff WHERE s_no='{$commerce_order_set['req_s_no']}'";
$comm_staff_query       = mysqli_query($my_db, $comm_staff_sql);
$comm_staff             = mysqli_fetch_assoc($comm_staff_query);

# 발주서 발주내역 쿼리
$commerce_order_sql = "
    SELECT
        co.no,
        co.priority,
        co.stock_no,
        co.option_no,
        (SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.`no`=co.option_no) AS option_name,
        co.quantity,
        co.unit_price,
        co.supply_price,
        co.vat,
        co.memo,
        co.sup_date,
        co.limit_date
    FROM commerce_order co
    WHERE co.set_no='{$no}' AND co.type='request'
    ORDER BY co.priority ASC, co.`no` ASC
";
$commerce_order_query   = mysqli_query($my_db, $commerce_order_sql);
$commerce_order_list    = [];
$commerce_is_usd        = ($commerce_order_set['sup_loc_type'] == '2') ? true : false;
$commerce_price_type    = ($commerce_is_usd) ? "$" : "₩";

$request_price_total        = 0;
$request_sup_price_total    = 0;
$request_vat_price_total    = 0;

$req_priority_new = 0;
while($commerce_order_result = mysqli_fetch_assoc($commerce_order_query))
{
    $request_price_total     += $commerce_order_result['supply_price'] + $commerce_order_result['vat'];
    $request_sup_price_total += $commerce_order_result['supply_price'];
    $request_vat_price_total += $commerce_order_result['vat'];

    $commerce_order_result['price']        = $commerce_order_result['supply_price'] + $commerce_order_result['vat'];
    $commerce_order_result['unit_price']   = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['unit_price']) : getKrwFormatPrice($commerce_order_result['unit_price']);
    $commerce_order_result['supply_price'] = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['supply_price']) : getNumberFormatPrice($commerce_order_result['supply_price']);
    $commerce_order_result['vat']          = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['vat']) : getNumberFormatPrice($commerce_order_result['vat']);
    $commerce_order_result['price']        = ($commerce_is_usd) ? getUsdFormatPrice($commerce_order_result['price']) : getNumberFormatPrice($commerce_order_result['price']);

    $req_priority_new         = $commerce_order_result['priority'];
    $commerce_order_list[]    = $commerce_order_result;
}

$request_price_total     = ($commerce_is_usd) ? getUsdFormatPrice($request_price_total) : getNumberFormatPrice($request_price_total);
$request_sup_price_total = ($commerce_is_usd) ? getUsdFormatPrice($request_sup_price_total) : getNumberFormatPrice($request_sup_price_total);
$request_vat_price_total = ($commerce_is_usd) ? getUsdFormatPrice($request_vat_price_total) : getNumberFormatPrice($request_vat_price_total);

$smarty->assign("comm_my_company", $comm_my_company);
$smarty->assign("comm_company", $comm_company);
$smarty->assign("comm_staff", $comm_staff);
$smarty->assign("commerce_is_usd", $commerce_is_usd);
$smarty->assign("commerce_price_type", $commerce_price_type);
$smarty->assign("request_price_total", $request_price_total);
$smarty->assign("request_sup_price_total", $request_sup_price_total);
$smarty->assign("request_vat_price_total", $request_vat_price_total);
$smarty->assign("req_priority_new", $req_priority_new+1);

$smarty->assign("commerce_order_set", $commerce_order_set);
$smarty->assign("commerce_order_list", $commerce_order_list);

$smarty->display('commerce_order_iframe_request.html');

?>
