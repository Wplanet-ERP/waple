<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/agency.php');
require('inc/model/MyQuick.php');
require('inc/model/Team.php');
require('inc/model/Agency.php');
require('inc/model/Company.php');
require('inc/model/Staff.php');

# Model Init
$charge_model   = Agency::Factory();
$charge_model->setChargeTable();

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
if($process == "f_ad_charge_price")
{
    $ac_no          = isset($_POST['ac_no']) ? $_POST['ac_no'] : "";
    $ad_charge_price= isset($_POST['val']) ? str_replace(",", "", $_POST['val']) : "";
    $charge_item    = $charge_model->getChargeItem($ac_no);
    $total_price    = $charge_item['prev_price']+$charge_item['charge_price']-$charge_item['ad_total_price']+$ad_charge_price;

    $upd_data       = array("ac_no" => $ac_no, "ad_charge_price" => $ad_charge_price, "total_price" => $total_price);

    if(!$charge_model->updateChargePrice($upd_data)){
        echo "광고주 충전금 변경에 실패했습니다.";
    }else{
        echo "광고주 충전금을 변경했습니다.";
    }
    exit;
}
elseif($process == 'f_task_req')
{
    $ac_no      = isset($_POST['ac_no']) ? $_POST['ac_no'] : "";
    $task_req   = isset($_POST['val']) ? addslashes(trim($_POST['val'])) : "";

    $upd_data   = array("ac_no" => $ac_no, "task_req" => $task_req);

    if(!$charge_model->update($upd_data)){
        echo "관리자 확인요청 저장에 실패했습니다.";
    }else{
        echo "관리자 확인요청 저장했습니다.";
    }
    exit;
}
elseif($process == 'f_task_run')
{
    $ac_no      = isset($_POST['ac_no']) ? $_POST['ac_no'] : "";
    $task_run   = isset($_POST['val']) ? addslashes(trim($_POST['val'])) : "";

    $upd_data   = array("ac_no" => $ac_no, "task_run" => $task_run);

    if(!$charge_model->update($upd_data)){
        echo "마케터 확인 저장에 실패했습니다.";
    }else{
        echo "마케터 확인 저장했습니다.";
    }
    exit;
}
elseif($process == 'f_memo')
{
    $ac_no      = isset($_POST['ac_no']) ? $_POST['ac_no'] : "";
    $memo       = isset($_POST['val']) ? addslashes(trim($_POST['val'])) : "";

    $upd_data   = array("ac_no" => $ac_no, "memo" => $memo);

    if(!$charge_model->update($upd_data)){
        echo "관리자 메모 저장에 실패했습니다.";
    }else{
        echo "관리자 메모 저장했습니다.";
    }
    exit;
}
elseif($process == 'f_is_inspection')
{
    $ac_no          = isset($_POST['ac_no']) ? $_POST['ac_no'] : "";
    $is_inspection  = isset($_POST['val']) ? $_POST['val'] : "";

    $upd_data   = array("ac_no" => $ac_no, "is_inspection" => $is_inspection);

    if(!$charge_model->update($upd_data)){
        echo "검수대상 변경에 실패했습니다.";
    }else{
        echo "검수대상을 변경했습니다.";
    }
    exit;
}
elseif($process == "f_run_price")
{
    $ac_no          = isset($_POST['ac_no']) ? $_POST['ac_no'] : "";
    $run_price      = isset($_POST['val']) ? str_replace(",", "", $_POST['val']) : "";

    $upd_data       = array("ac_no" => $ac_no, "run_price" => $run_price);

    if(!$charge_model->updateChargePrice($upd_data)){
        echo "잔액 변경에 실패했습니다.";
    }else{
        echo "잔액을 변경했습니다.";
    }
    exit;
}
elseif($process == "f_multi_is_inspection")
{
    $chk_ac_no_list = isset($_POST['chk_ac_no_list']) ? $_POST['chk_ac_no_list'] : "";
    $chk_status     = isset($_POST['chk_status']) ? $_POST['chk_status'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $upd_sql        = "UPDATE agency_charge SET is_inspection='{$chk_status}' WHERE ac_no IN({$chk_ac_no_list})";

    if(!mysqli_query($my_db, $upd_sql)){
        exit("<script>alert('검수상태 변경에 실패했습니다');location.href='agency_charge_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('검수상태 변경했습니다');location.href='agency_charge_management.php?{$search_url}';</script>");
    }
}
elseif($process == "charge_delete")
{
    $charge_month   = isset($_POST['charge_month']) ? $_POST['charge_month'] : "";
    $search_url     = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $del_sql        = "UPDATE agency_charge SET display='2' WHERE base_mon='{$charge_month}'";

    if(!mysqli_query($my_db, $del_sql)){
        exit("<script>alert('삭제에 실패했습니다');location.href='agency_charge_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제했습니다');location.href='agency_charge_management.php?{$search_url}';</script>");
    }
}

# Navigation & My Quick
$nav_prd_no     = "173";
$nav_title      = "충전금 잔액 관리";
$quick_model    = MyQuick::Factory();
$is_my_quick    = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);
$is_editable    = false;

if($session_team == "00211"){
    $is_editable = true;
}

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);
$smarty->assign("is_editable", $is_editable);

# 검색조건
$add_where          = "1=1 AND display='1'";
$search_url         = getenv("QUERY_STRING");
$prev_base_mon      = date('Y-m', strtotime("-1 months"));
$sch_base_mon       = isset($_GET['sch_base_mon']) ? $_GET['sch_base_mon'] : $prev_base_mon;
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "1";
$sch_media          = isset($_GET['sch_media']) ? $_GET['sch_media'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_partner_name   = isset($_GET['sch_partner_name']) ? $_GET['sch_partner_name'] : "";
$sch_manager_team   = isset($_GET['sch_manager_team']) ? $_GET['sch_manager_team'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_is_memo        = isset($_GET['sch_is_memo']) ? $_GET['sch_is_memo'] : "";
$sch_is_inspection  = isset($_GET['sch_is_inspection']) ? $_GET['sch_is_inspection'] : "";
$sch_task_req       = isset($_GET['sch_task_req']) ? $_GET['sch_task_req'] : "";
$sch_task_run       = isset($_GET['sch_task_run']) ? $_GET['sch_task_run'] : "";
$sch_memo           = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
$sch_advertiser_id  = isset($_GET['sch_advertiser_id']) ? $_GET['sch_advertiser_id'] : "";
$sch_quick_type     = isset($_GET['sch_quick_type']) ? $_GET['sch_quick_type'] : "";
$sch_quick_price    = isset($_GET['sch_quick_price']) ? $_GET['sch_quick_price'] : "";
$sch_quick_term     = isset($_GET['sch_quick_term']) ? $_GET['sch_quick_term'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "charge_price";

if(!empty($sch_quick_type))
{
    switch($sch_quick_type)
    {
        case '1':
            $add_where  .= " AND `ac`.total_price <= 0";
            break;
        case '2':
            $add_where  .= " AND `ac`.prev_price <= 0";
            break;
        case '3':
            $last_base_sql      = "SELECT MAX(base_mon) as last_mon FROM agency_charge";
            $last_base_query    = mysqli_query($my_db, $last_base_sql);
            $last_base_result   = mysqli_fetch_assoc($last_base_query);
            $sch_base_mon       = $last_base_result['last_mon'];
            
            $add_where  .= " AND `ac`.total_price <= {$sch_quick_price}";
            $smarty->assign("sch_quick_price", $sch_quick_price);
            break;
        case '4':
            $prev_quick_term    = date("Y-m", strtotime("{$sch_base_mon} -{$sch_quick_term} months"));
            $chk_quick_term     = $sch_quick_term+1;

            $check_term_sql     = "
                SELECT
                    partner,
                    COUNT(base_mon) AS `empty_cnt`
                FROM 
                (
                    SELECT 
                        base_mon,
                        partner
                    FROM agency_charge WHERE (base_mon BETWEEN '{$prev_quick_term}' AND '{$sch_base_mon}') AND ad_total_price = 0
                    GROUP BY partner, base_mon
                ) AS result_search
                GROUP BY partner
                HAVING empty_cnt = '{$chk_quick_term}'
                ORDER BY partner
            ";
            $check_term_query    = mysqli_query($my_db, $check_term_sql);
            $check_term_list     = [];
            while($check_term_result = mysqli_fetch_assoc($check_term_query)){
                $check_term_list[] = $check_term_result['partner'];
            }

            if(!empty($check_term_list)){
                $check_term_partner = implode(",", $check_term_list);
                $add_where .= " AND partner IN({$check_term_partner})";
            }

            $smarty->assign("sch_quick_term", $sch_quick_term);
            break;
    }
}
$smarty->assign("sch_quick_type", $sch_quick_type);

if(!empty($sch_base_mon)) {
    $add_where .= " AND `ac`.base_mon = '{$sch_base_mon}'";
}

$sch_base_s_date    = $sch_base_mon."-01";
$sch_base_e_day     = date("t", strtotime($sch_base_s_date));
$sch_base_e_date    = $sch_base_mon."-{$sch_base_e_day}";
$sch_prev_mon       = date('Y-m', strtotime("{$sch_base_mon} -1 months"));

$smarty->assign('sch_base_mon', $sch_base_mon);
$smarty->assign('prev_base_mon', $prev_base_mon);
$smarty->assign('sch_base_s_date', $sch_base_s_date);
$smarty->assign('sch_base_e_date', $sch_base_e_date);

if(empty($search_url) && !permissionNameCheck($session_permission, '재무관리자'))
{
    $sch_manager_team   = $session_team;
    $sch_manager        = $session_s_no;
    $search_url         = "sch_base_mon={$sch_base_mon}&sch_manager_team={$sch_manager_team}&sch_manager={$sch_manager}";
}

if(!empty($sch_my_c_no)){
    $add_where   .= " AND (SELECT c.my_c_no FROM company c WHERE c.c_no=`ac`.partner) = '{$sch_my_c_no}'";
    $smarty->assign('sch_my_c_no', $sch_my_c_no);
}

if(!empty($sch_media)){
    $add_where   .= " AND `ac`.media = '{$sch_media}'";
    $smarty->assign('sch_media', $sch_media);
}

if(!empty($sch_agency)){
    $add_where   .= " AND `ac`.agency = '{$sch_agency}'";
    $smarty->assign('sch_agency', $sch_agency);
}

if(!empty($sch_partner_name)){
    $add_where   .= " AND `ac`.partner_name LIKE '%{$sch_partner_name}%'";
    $smarty->assign('sch_partner_name', $sch_partner_name);
}

if(!empty($sch_is_memo)){
    if($sch_is_memo == '1') {
        $add_where .= " AND (`ac`.memo IS NOT NULL AND `ac`.memo != '')";
    }elseif($sch_is_memo == '2') {
        $add_where .= " AND (`ac`.memo IS NULL OR `ac`.memo = '')";
    }
    $smarty->assign('sch_is_memo', $sch_is_memo);
}

if(!empty($sch_w_no)){
    if($sch_w_no == '1'){
        $add_where   .= " AND `ac`.w_no IS NOT NULL";
    }elseif($sch_w_no == '2'){
        $add_where   .= " AND `ac`.w_no IS NULL";
    }
    $smarty->assign('sch_w_no', $sch_w_no);
}

if(!empty($sch_work_state)){
    $add_where   .= " AND `w`.work_state = '{$sch_work_state}'";
    $smarty->assign('sch_work_state', $sch_work_state);
}

if(!empty($sch_memo)){
    $add_where   .= " AND `ac`.memo LIKE '%{$sch_memo}%'";
    $smarty->assign('sch_memo', $sch_memo);
}

if(!empty($sch_advertiser_id)){
    $add_where   .= " AND `ac`.advertiser_id LIKE '%{$sch_advertiser_id}%'";
    $smarty->assign('sch_advertiser_id', $sch_advertiser_id);
}

if(!empty($sch_task_req)){
    $add_where   .= " AND `ac`.task_req LIKE '%{$sch_task_req}%'";
    $smarty->assign('sch_task_req', $sch_task_req);
}

if(!empty($sch_task_run)){
    $add_where   .= " AND `ac`.task_run LIKE '%{$sch_task_run}%'";
    $smarty->assign('sch_task_run', $sch_task_run);
}

# 팀 검색
$team_model         = Team::Factory();
$team_all_list      = $team_model->getTeamAllList();
$staff_team_list    = $team_all_list['staff_list'];
$team_full_name_list= $team_model->getTeamFullNameList();

$sch_staff_list      = [];
if (!empty($sch_manager_team))
{
    if ($sch_manager_team != "all") {
        $sch_team_code_where = getTeamWhere($my_db, $sch_manager_team);
        $add_where 		 .= " AND `ac`.manager_team IN({$sch_team_code_where})";
        $sch_staff_list   = $staff_team_list[$sch_manager_team];
    }
    $smarty->assign("sch_manager_team", $sch_manager_team);
}

if (!empty($sch_manager))
{
    if ($sch_manager != "all") {
        $add_where 		 .= " AND `ac`.manager='{$sch_manager}'";
    }
    $smarty->assign("sch_manager", $sch_manager);
}

$add_order_by = "";
$ord_type_by  = "";
$order_by_val = "";
if(!empty($ord_type))
{
    $ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

    if(!empty($ord_type_by))
    {
        if($ord_type_by == '1'){
            $order_by_val = "ASC";
        }elseif($ord_type_by == '2'){
            $order_by_val = "DESC";
        }
        $add_order_by .= "{$ord_type} {$order_by_val}";
    }
}

$smarty->assign('ord_type', $ord_type);
$smarty->assign('ord_type_by', $ord_type_by);

# 전체 게시물 수
$agency_total_sql	 = "
    SELECT
        COUNT(ac.ac_no) as cnt,
        SUM(`ac`.prev_price) as total_prev,
        SUM(`ac`.charge_price) as total_charge,
        SUM(`ac`.ad_total_price) as total_ad,
        SUM(`ac`.total_price) as total_sum
    FROM 
    (
        SELECT
            `ac`.ac_no,
            `ac`.prev_price,
            `ac`.charge_price,
            `ac`.ad_total_price,
            `ac`.total_price
        FROM agency_charge AS `ac`
        WHERE {$add_where}
    ) AS `ac`
";
$agency_total_query  = mysqli_query($my_db, $agency_total_sql);
$agency_total_result = mysqli_fetch_array($agency_total_query);
$agency_total 	     = $agency_total_result['cnt'];
$total_prev_price    = $agency_total_result['total_prev'];
$total_charge_price  = $agency_total_result['total_charge'];
$total_ad_price      = $agency_total_result['total_ad'];
$total_sum_price     = $agency_total_result['total_sum'];

$smarty->assign("total_prev_price", $total_prev_price);
$smarty->assign("total_charge_price", $total_charge_price);
$smarty->assign("total_ad_price", $total_ad_price);
$smarty->assign("total_sum_price", $total_sum_price);

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($agency_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$page_list	= pagelist($pages, "agency_charge_management.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $agency_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 충전금 리스트
$agency_charge_sql = "
    SELECT
        `ac`.*,
        (SELECT c.my_c_no FROM company c WHERE c.c_no=`ac`.partner) AS my_c_no,         
        (SELECT s.s_name FROM staff s WHERE s.s_no=`ac`.manager) AS s_name,         
        (SELECT t.team_name FROM team t WHERE t.team_code=`ac`.manager_team) AS t_name
    FROM agency_charge AS `ac` 
    WHERE {$add_where}
    ORDER BY {$add_order_by}
    LIMIT {$offset}, {$num}
";
$agency_charge_query = mysqli_query($my_db, $agency_charge_sql);
$agency_charge_list  = [];
$agency_option       = getSettleAgencyOption();
$media_option        = getAdvertiseMediaOption();
$sch_my_company_list = getAdMyCompanyOption();
while($agency_charge = mysqli_fetch_assoc($agency_charge_query))
{
    $agency_charge['agency_name'] = $agency_option[$agency_charge['agency']];
    $agency_charge['media_name']  = $media_option[$agency_charge['media']];
    $agency_charge['my_company']  = $sch_my_company_list[$agency_charge['my_c_no']];
    $agency_charge['diff_price']  = $agency_charge['total_price']-$agency_charge['run_price'];

    $agency_charge_list[] = $agency_charge;
}

$smarty->assign("agency_option", $agency_option);
$smarty->assign("media_option", $media_option);
$smarty->assign('page_type_list', getPageTypeOption(4));
$smarty->assign("sch_staff_list", $sch_staff_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("sch_my_company_list", $sch_my_company_list);
$smarty->assign("is_exist_option", getAdIsExistOption());
$smarty->assign("is_inspection_option", getAdIsInspectionOption());
$smarty->assign("quick_search_option", getAgencyQuickSearch());
$smarty->assign("agency_charge_list", $agency_charge_list);

$smarty->display('agency_charge_management.html');
?>
