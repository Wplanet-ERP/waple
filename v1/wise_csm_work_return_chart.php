<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/wise_csm.php');
require('inc/model/MyQuick.php');
require('inc/model/Staff.php');
require('inc/model/Kind.php');

# Navigation & My Quick
$nav_prd_no  = "230";
$nav_title   = "커머스 환불 현황";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 변수 리스트
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$sch_brand_total_list       = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$picker_list                = [];

# 검색조건
$add_where          = "w.prd_no='229' AND w.work_state='6'";
$sch_wd_s_month     = isset($_GET['sch_wd_s_month']) ? $_GET['sch_wd_s_month'] : date("Y-m", strtotime("-11 months"));
$sch_wd_e_month     = isset($_GET['sch_wd_e_month']) ? $_GET['sch_wd_e_month'] : date("Y-m");
$sch_wd_e_day       = date("t");
$sch_wd_s_date      = "{$sch_wd_s_month}-01";
$sch_wd_s_datetime  = "{$sch_wd_s_month}-01 00:00:00";
$sch_wd_e_date      = "{$sch_wd_e_month}-{$sch_wd_e_day}";
$sch_wd_e_datetime  = "{$sch_wd_e_month}-{$sch_wd_e_day} 23:59:59";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_brand_url      = "";

if(empty($sch_wd_s_month) || empty($sch_wd_e_month)){
    exit("<script>alert('출금완료일 모두 선택해주세요.');location.href='wise_csm_return_chart.php';</script>");
}

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

# 전체 기간 조회 및 누적데이터 조회
$all_date_where     = "(DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_wd_s_month}' AND '{$sch_wd_e_month}')";
$all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m')";
$all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y-%m')";

$add_where         .= " AND (wd.wd_date BETWEEN '{$sch_wd_s_date}' AND '{$sch_wd_e_date}')";
$add_date_column    = "DATE_FORMAT(wd.wd_date, '%Y%m')";
$add_brand_column   = "";

if(!empty($sch_brand_g1)){
    $sch_brand_url = "sch_brand_g1={$sch_brand_g1}";
}
if(!empty($sch_brand_g2)){
    $sch_brand_url .= "&sch_brand_g2={$sch_brand_g2}";
}

if(!empty($sch_brand)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $picker_list        = array($sch_brand => $sch_brand_list[$sch_brand]);
    $add_brand_column   = "w.c_no";
    $add_where         .= " AND `w`.c_no = '{$sch_brand}'";
    $sch_brand_url     .= "&sch_brand={$sch_brand}";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $picker_list        = $sch_brand_list;
    $add_brand_column   = "w.c_no";
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $picker_list        = $sch_brand_g2_list;
    $add_brand_column   = "(SELECT c.brand FROM company c WHERE c.c_no=w.c_no)";
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
else{
    $picker_list        = $brand_company_g1_list;
    $add_brand_column   = "(SELECT k.k_parent FROM kind k WHERE k.k_name_code=(SELECT c.brand FROM company c WHERE c.c_no=w.c_no))";
}
$smarty->assign("sch_wd_s_month", $sch_wd_s_month);
$smarty->assign("sch_wd_e_month", $sch_wd_e_month);
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);


# CHART 기본 리스트 Init 및 x key 및 타이틀 설정
$x_label_list       = [];
$return_date_list   = [];
$return_stats_list  = [];
$all_date_sql       = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
$date_w = getDateChartOption();
while($date = mysqli_fetch_array($all_date_query))
{
    $chart_title = $date['chart_title'];
    $x_label_list[$date['chart_key']]       = "'{$chart_title}'";
    $chart_e_day = date("t", strtotime($date['chart_key']));
    $return_date_list[$date['chart_key']]   = array("s_date" => $date['chart_title']."-01", "e_date" => $date['chart_title']."-".$chart_e_day, "title" => $chart_title);

    foreach($picker_list as $brand => $brand_name){
        $return_stats_list['qty'][$date['chart_key']][$brand]   = 0;
        $return_stats_list['price'][$date['chart_key']][$brand] = 0;
    }
}

# Y 컬럼 Label
$y_label_list       = [];
$y_label_title      = [];
$brand_label_list   = [];
foreach($picker_list as $brand => $picker_title)
{
    $y_label_list[$brand]   = $picker_title;
    $y_label_title[$brand]  = $picker_title;
    $y_label_title['all']   = "합산";

    $y_brand_url = $sch_brand_url;
    if(!empty($sch_brand)) {
        $y_brand_url = $sch_brand_url;
    }
    elseif(!empty($sch_brand_g2)) {
        $y_brand_url .= "&sch_brand={$brand}";
    }
    elseif(!empty($sch_brand_g1)) {
        $y_brand_url .= "&sch_brand_g2={$brand}";
    }
    else{
        $y_brand_url .= "sch_brand_g1={$brand}";
    }

    $brand_label_list[$brand] = array("brand_name" =>  $picker_title, "brand_url" => $y_brand_url);
}

$return_stats_sql = "
    SELECT
        {$add_date_column} as key_date,
        {$add_brand_column} as brand,
        COUNT(DISTINCT w.w_no) as total_qty,
        SUM(DISTINCT wd.wd_money) as total_price
    FROM work as w
    LEFT JOIN withdraw wd ON wd.wd_no=w.wd_no
    WHERE {$add_where} 
    GROUP BY key_date, brand
    ORDER BY key_date ASC
";
$return_stats_query = mysqli_query($my_db, $return_stats_sql);
while($return_stats = mysqli_fetch_assoc($return_stats_query))
{
    $brand_key = $return_stats['brand'];
    if(!empty($brand_key)){
        $return_stats_list['qty'][$return_stats['key_date']][$brand_key] +=  $return_stats['total_qty'];
        $return_stats_list['price'][$return_stats['key_date']][$brand_key] +=  $return_stats['total_price'];
    }
}

$full_data = [];
foreach($return_stats_list as $key_type => $return_stats_data)
{
    foreach($return_stats_data as $key_date => $return_stats)
    {
        $date_sum = 0;
        foreach ($return_stats as $key => $value)
        {
            $full_data[$key_type]["each"]["line"][$key]['title']   = $picker_list[$key];
            $full_data[$key_type]["each"]["line"][$key]['data'][]  = $value;

            $full_data[$key_type]["each"]["bar"][$key]['title']  = $picker_list[$key];
            $full_data[$key_type]["each"]["bar"][$key]['data'][] = $value;
            $date_sum += $value;
        }

        $full_data[$key_type]["sum"]["line"]["all"]['title']   = "합산";
        $full_data[$key_type]["sum"]["line"]["all"]['data'][]  = $date_sum;
        $full_data[$key_type]["sum"]["bar"]["all"]['title']    = "합산";
        $full_data[$key_type]["sum"]["bar"]["all"]['data'][]   = $date_sum;
    }
}

$smarty->assign('x_label_list', implode(',', $x_label_list));
$smarty->assign('legend_list', json_encode($y_label_list));
$smarty->assign('y_label_title', json_encode($y_label_title));
$smarty->assign('full_data', json_encode($full_data));
$smarty->assign('return_date_list', $return_date_list);
$smarty->assign('return_brand_list', $brand_label_list);
$smarty->assign('return_stats_list', $return_stats_list);

$smarty->display('wise_csm_work_return_chart.html');
?>
