<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/wise_wm.php');
require('inc/model/MyQuick.php');
require('inc/model/Board.php');

$is_admin = false;
if(permissionNameCheck($session_permission, "마스터관리자") || permissionNameCheck($session_permission, "대표") || permissionNameCheck($session_permission, "재무관리자") || $session_s_no == '102')
{
    $is_admin = true;
}
$smarty->assign("is_admin", $is_admin);

# Navigation & My Quick
$nav_prd_no  = "29";
$nav_title   = "WISE CSM";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

#복사용 텍스트
$online_txt = "고객님 베라베프 직원과의 상담은 어떠셨나요?\r\n고객님의 소중한 의견을 바탕으로 더욱 만족을 드리는 서비스를 제공 하도록 하겠습니다.\r\n바쁘시겠지만 상담 만족도 조사에 참여 부탁드립니다.\r\nhttps://cs.belabef.co.kr/ev_{$session_s_no}_o";
$ars_txt    = "고객님 베라베프 직원과의 상담은 어떠셨나요?\r\n고객님의 소중한 의견을 바탕으로 더욱 만족을 드리는 서비스를 제공 하도록 하겠습니다.\r\n바쁘시겠지만 상담 만족도 조사에 참여 부탁드립니다.\r\nhttps://cs.belabef.co.kr/ev_{$session_s_no}_a";

$smarty->assign("online_txt", $online_txt);
$smarty->assign("ars_txt", $ars_txt);

# 부서 게시판 최상위 5개
$board_model    = Board::Factory();
$board_cs_list  = $board_model->getBoardTopFiveList("wise_csm");
$total_num      = !empty($board_cs_list) ? count($board_cs_list) : 0;

$smarty->assign("total_num", $total_num);
$smarty->assign("board_cs_list", $board_cs_list);

# CS&위드 게시판 최상위 5개
$board_cs_with_list = $board_model->getBoardTopFiveList("cs_with");
$with_total_num     = !empty($board_cs_with_list) ? count($board_cs_with_list) : 0;
$cs_team_code       = "00244";
$is_cs_staff        = ($session_team == $cs_team_code) ? true : false;

$smarty->assign("with_total_num", $with_total_num);
$smarty->assign("board_cs_with_list", $board_cs_with_list);
$smarty->assign("is_cs_staff", $is_cs_staff);

# 출금 승인 현황
$approve_prev_date  = date("Y-m-d", strtotime("-4 days"));
$approve_prev_time  = $approve_prev_date." 00:00:00";
$approve_cur_date   = date("Y-m-d");
$date_type_option   = getDateChartOption();
$withdraw_prd_list  = array("229" => "커머스 환불");
$withdraw_init      = array("total" => 0, "wait" => 0);
$withdraw_date_list = [];
$withdraw_list      = [];

# 날짜 생성
$all_date_sql       = "
	SELECT
 		DATE_FORMAT(`allday`.Date, '%m/%d_%w') as chart_title,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as chart_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$approve_prev_date}' AND '$approve_cur_date'
	ORDER BY chart_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($all_date = mysqli_fetch_assoc($all_date_query))
{
    $date_convert   = explode('_', $all_date['chart_title']);
    $date_name      = isset($date_convert[1]) ? $date_type_option[$date_convert[1]] : "";
    $chart_title    = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];

    if(!isset($withdraw_date_list[$all_date['chart_key']]))
    {
        $withdraw_date_list[$all_date['chart_key']] = array("title" => $chart_title, "date" => date("Y-m-d", strtotime($all_date['chart_key'])));

        foreach($withdraw_prd_list as $prd_no => $prd_title){
            $withdraw_list[$prd_no][$all_date['chart_key']] = $withdraw_init;

            if(!isset($withdraw_list[$prd_no])){
                $withdraw_list[$prd_no]["total"] = $withdraw_init;
            }
        }
    }
}

# 출금 승인 현황 리스트 쿼리
$withdraw_approve_sql   = "
    SELECT 
        DATE_FORMAT(wd.regdate, '%Y%m%d') as wd_reg_day,
        w.prd_no,
        (SELECT p.prd_no FROM product p WHERE p.prd_no=w.prd_no) as prd_name,
        COUNT(wd.wd_no) as total_cnt, 
        SUM(IF(wd.is_approve='2',1,0)) as wait_cnt 
    FROM withdraw AS wd 
    LEFT JOIN `work` AS w ON w.w_no=wd.w_no
    WHERE wd.regdate >= '{$approve_prev_time}' AND wd.reg_s_no='146' AND wd.wd_state NOT IN(4) AND wd.wd_method='1' 
    GROUP BY wd_reg_day, prd_no
";
$withdraw_approve_query  = mysqli_query($my_db, $withdraw_approve_sql);
while($withdraw_approve = mysqli_fetch_assoc($withdraw_approve_query))
{
    $withdraw_list[$withdraw_approve['prd_no']][$withdraw_approve['wd_reg_day']]['total']   += $withdraw_approve['total_cnt'];
    $withdraw_list[$withdraw_approve['prd_no']][$withdraw_approve['wd_reg_day']]['wait']    += $withdraw_approve['wait_cnt'];
    $withdraw_list[$withdraw_approve['prd_no']]["total"]['total']   += $withdraw_approve['total_cnt'];
    $withdraw_list[$withdraw_approve['prd_no']]["total"]['wait']    += $withdraw_approve['wait_cnt'];
}

$smarty->assign("withdraw_date_list", $withdraw_date_list);
$smarty->assign("withdraw_prd_list", $withdraw_prd_list);
$smarty->assign("withdraw_list", $withdraw_list);

$work_quick_state   = getWorkStateQuickOption();
$sch_reg_s_date     = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : date("Y-m-d", strtotime("-1 weeks"));
$sch_reg_e_date     = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : date("Y-m-d");
$sch_reg_date_type  = isset($_GET['sch_reg_date_type']) ? $_GET['sch_reg_date_type'] : "week";
$sch_work_quick     = isset($_GET['sch_work_quick']) && !empty($_GET['sch_work_quick']) ? $_GET['sch_work_quick'] : "1";
$sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
$sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
$work_prev_date     = date("Y-m-d", strtotime("-10 days"));
$work_prev_url_date = date("Y-m-d", strtotime("{$work_prev_date} -1 days"));
$work_prev_datetime = "{$work_prev_date} 00:00:00";

$add_work_where     = "work_state IN(3,4,5,6)";
$add_work_yet_where = "1=1";
$add_prev_where     = "regdate < '{$work_prev_datetime}'";

if(!empty($sch_reg_s_date)){
    $add_work_where     .= " AND regdate >= '{$sch_reg_s_datetime}'";
    $add_work_yet_where .= " AND regdate >= '{$sch_reg_s_datetime}'";
    $smarty->assign("sch_reg_s_date", $sch_reg_s_date);
}

if(!empty($sch_reg_e_date)){
    $add_work_where     .= " AND regdate <= '{$sch_reg_e_datetime}'";
    $add_work_yet_where .= " AND regdate <= '{$sch_reg_e_datetime}'";
    $smarty->assign("sch_reg_e_date", $sch_reg_e_date);
}
$smarty->assign("sch_work_quick", $sch_work_quick);
$smarty->assign("sch_reg_date_type", $sch_reg_date_type);

# 업무 관리자 등록된 상품
$manager_prd_sql    = "SELECT * FROM product WHERE task_req_staff LIKE '%{$session_s_no}_{$session_team}%' AND display='1'";
$manager_prd_query  = mysqli_query($my_db, $manager_prd_sql);
$manager_prd_list   = [];
while($manager_prd  = mysqli_fetch_assoc($manager_prd_query)){
    $manager_prd_list[] = $manager_prd['prd_no'];
}

if(!empty($manager_prd_list)){
    $manager_prd_text       = implode(",", $manager_prd_list);
    $add_work_yet_where    .= "AND w.prd_no IN({$manager_prd_text})";
}else{
    $add_work_yet_where    .= "AND 1!=1";
}

$work_state_sql     = "SELECT * FROM `work` as w WHERE {$add_work_where} AND work_state != '6' AND (task_req_s_no='{$session_s_no}' OR task_run_s_no='{$session_s_no}')";
$work_state_query   = mysqli_query($my_db, $work_state_sql);
while($work_state_result  = mysqli_fetch_assoc($work_state_query))
{
    if($work_state_result['task_req_s_no'] == $session_s_no){
        $work_quick_state["1"]["cnt"]++;
    }

    if($work_state_result['task_run_s_no'] == $session_s_no){
        $work_quick_state["2"]["cnt"]++;
    }
}

$work_state_null_sql    = "SELECT * FROM `work` as w WHERE {$add_work_yet_where} AND (task_run_s_no=0 OR task_run_s_no IS NULL)";
$work_state_null_query  = mysqli_query($my_db, $work_state_null_sql);
while($work_state_null  = mysqli_fetch_assoc($work_state_null_query))
{
    $work_quick_state["3"]["cnt"]++;
}

# 업무 요청,처리별 현황
$main_staff     = "";
$check_staff    = "";
$table_staff    = "";
$work_prev_text = "";
$work_prev_url  = "";
if($sch_work_quick == "1"){
    $add_work_where .= " AND task_req_s_no='{$session_s_no}'";
    $add_prev_where .= " AND work_state IN(3,4,5) AND task_req_s_no='{$session_s_no}'";
    $check_staff     = "task_run_s_no";
    $table_staff     = "처리자";
    $work_prev_text  = "[주의] 10일 이상 지난 미완료 업무 : ";
    $work_prev_url   = "/v1/work_list.php?sch=Y&sch_req_s_name={$session_name}&sch_reg_e_date={$work_prev_url_date}&q_sch=6";
}elseif($sch_work_quick == "2"){
    $add_work_where .= " AND task_run_s_no='{$session_s_no}'";
    $add_prev_where .= " AND work_state IN(3,4,5) AND task_run_s_no='{$session_s_no}'";
    $check_staff     = "task_req_s_no";
    $table_staff     = "요청자";
    $work_prev_text  = "[주의] 10일 이상 지난 미완료 업무 : ";
    $work_prev_url   = "/v1/work_list.php?sch=Y&sch_run_s_name={$session_name}&sch_reg_e_date={$work_prev_url_date}&q_sch=6";
}elseif($sch_work_quick == "3"){
    $table_staff     = "요청자";
    $check_staff     = "task_req_s_no";
    $work_prev_text  = "[주의] 10일 이상 처리자 미설정 업무 : ";
    $work_prev_url   = "/v1/work_list.php?sch=Y&sch_reg_e_date={$work_prev_url_date}&sch_run_is_null=1";

    if(!empty($manager_prd_list)){
        $manager_prd_text = implode(",", $manager_prd_list);
        $add_work_where .= " AND w.prd_no IN({$manager_prd_text})";
        $add_prev_where .= " AND work_state IN(3,4,5,6) AND w.prd_no IN({$manager_prd_text})";
    }else{
        $add_work_where .= "AND 1!=1";
        $add_prev_where .= "AND 1!=1";
    }
    $add_work_where .= " AND (task_run_s_no=0 OR task_run_s_no IS NULL)";
    $add_prev_where .= " AND (task_run_s_no=0 OR task_run_s_no IS NULL)";
}

$work_parent_list       = [];
$work_parent_cnt_list   = [];
$work_parent_sql        = "
    SELECT
        (SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd1,
        (SELECT CONCAT(k_name_code) FROM product prd WHERE prd.prd_no=w.prd_no) AS k_prd2,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2_name
    FROM `work` as w 
    WHERE {$add_work_where} 
    GROUP BY k_prd2
";
$work_parent_query   = mysqli_query($my_db, $work_parent_sql);
while($work_parent = mysqli_fetch_assoc($work_parent_query)){
    $work_parent_list[$work_parent['k_prd2']] = array("k_prd1" => $work_parent['k_prd1'], "title" => $work_parent['k_prd2_name']);
}

$work_group_list    = [];
$work_group_sql     = "
    SELECT
        (SELECT CONCAT(k_name_code) FROM product prd WHERE prd.prd_no=w.prd_no) AS k_prd2,
        w.prd_no,
        (SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_name,
        w.{$check_staff} as staff_no,
        (SELECT s.s_name FROM staff s WHERE s.s_no=w.{$check_staff}) AS staff_name,
        COUNT(w.w_no) as total_qty
    FROM `work` as w 
    WHERE {$add_work_where} 
    GROUP BY prd_no, staff_no
    ORDER BY total_qty DESC, prd_name, staff_no ASC
";
$work_group_query   = mysqli_query($my_db, $work_group_sql);
while($work_group = mysqli_fetch_assoc($work_group_query)){
    $work_group_list[$work_group['k_prd2']][] = array(
        "prd_no"        => $work_group['prd_no'],
        "prd_title"     => $work_group['prd_name'],
        "staff_no"      => $work_group["staff_no"],
        "staff_title"   => $work_group['staff_name'],
        "search_url"    => (($sch_work_quick == "1") ? "&sch_run_s_name" : "&sch_req_s_name")."={$work_group['staff_name']}",
        "cnt"           => $work_group['total_qty']
    );
    $work_parent_cnt_list[$work_group['k_prd2']]++;
}

$work_all_list      = [];
$work_all_cnt_list  = [];
$work_count_list    = array("progress" => 0, "finish" => 0, "yet" => 0);
$work_all_sql       = "
    SELECT
        w.w_no,
        w.work_state,
        w.prd_no,
        (SELECT CONCAT(k_name_code) FROM product prd WHERE prd.prd_no=w.prd_no) AS k_prd2,   
        w.{$check_staff} as staff_no,
        (SELECT s.s_name FROM staff s WHERE s.s_no=w.{$check_staff}) AS req_name
    FROM `work` as w 
    WHERE {$add_work_where}
";
$work_all_query = mysqli_query($my_db, $work_all_sql);
while($work_all = mysqli_fetch_assoc($work_all_query))
{
    $work_state = ($sch_work_quick == "3") ? "yet" : $work_all['work_state'];

    $work_all_list[$work_all['prd_no']][$work_all['staff_no']][$work_state]++;
    $work_all_cnt_list[$work_all['work_state']]++;

    if($work_all['work_state'] != '6'){
        $work_all_cnt_list['progress']++;
    }else{
        $work_all_cnt_list['finish']++;
    }

    $work_all_cnt_list['total']++;
}

# 10일 이상 미완료건
$work_prev_sql      = "
    SELECT
        COUNT(w_no) as cnt
    FROM `work` as `w`
    WHERE {$add_prev_where}
";
$work_prev_query    = mysqli_query($my_db, $work_prev_sql);
$work_prev_result   = mysqli_fetch_assoc($work_prev_query);
$work_prev_cnt      = isset($work_prev_result['cnt']) ? $work_prev_result['cnt'] : 0;
if($work_prev_cnt > 0){
    $work_prev_text .= "{$work_prev_cnt}건";
}

$smarty->assign("table_staff", $table_staff);
$smarty->assign("work_prev_cnt", $work_prev_cnt);
$smarty->assign("work_prev_url", $work_prev_url);
$smarty->assign("work_prev_text", $work_prev_text);
$smarty->assign("work_quick_state", $work_quick_state);
$smarty->assign("work_parent_list", $work_parent_list);
$smarty->assign("work_parent_cnt_list", $work_parent_cnt_list);
$smarty->assign("work_group_list", $work_group_list);
$smarty->assign("work_all_list", $work_all_list);
$smarty->assign("work_all_cnt_list", $work_all_cnt_list);
$smarty->assign("manager_prd_list", $manager_prd_list);

$smarty->display('wise_csm.html');
?>
