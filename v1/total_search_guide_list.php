<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');

# 검색 초기화 및 조건 생성
$add_where = "1=1 AND display='1'";

# 가이드 세부 검색
$sch_keyword  = isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";

if(!empty($sch_keyword))
{
    $add_where .= " AND `bg`.keyword like '%{$sch_keyword}%'";
    $smarty->assign('sch_keyword', $sch_keyword);
}

# 가이드 리스트 쿼리
$guide_sql  = "
    SELECT
        `bg`.b_no,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=`bg`.k_name_code)) AS k_g1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=`bg`.k_name_code) AS k_g2_name,
        `bg`.question,
        `bg`.keyword,
        (SELECT s.s_name FROM staff s WHERE s.s_no=`bg`.manager) AS manager_name,
        DATE_FORMAT(`bg`.regdate, '%Y-%m-%d') AS reg_date,
        DATE_FORMAT(`bg`.regdate, '%H:%i') AS reg_time,
        `bg`.file_name,
        `bg`.file_path,
        `bg`.hit,
        (SELECT count(bgr.r_no) FROM board_guide_read bgr WHERE bgr.read_s_no='{$session_s_no}' AND bgr.b_no = bg.b_no) as read_cnt
    FROM board_guide bg
    WHERE {$add_where}
    ORDER BY b_no DESC
";

# 전체 게시물 수
$guide_total_sql		= "SELECT count(b_no) FROM (SELECT `bg`.b_no FROM board_guide `bg` WHERE {$add_where}) AS cnt";
$guide_total_query	= mysqli_query($my_db, $guide_total_sql);
if(!!$guide_total_query)
    $guide_total_result = mysqli_fetch_array($guide_total_query);

$guide_total = $guide_total_result[0];

# 페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($guide_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page		= pagelist($pages, "total_search_guide_list.php", $pagenum, $search_url);
$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $guide_total);
$smarty->assign("pagelist", $page);
$smarty->assign("ord_page_type", $page_type);

$guide_sql  .= " LIMIT {$offset}, {$num}";

$guide_list = [];
$guide_query = mysqli_query($my_db, $guide_sql);
while($guide = mysqli_fetch_assoc($guide_query))
{
    $keyword     = isset($guide['keyword']) ? "#".implode(" #", explode(',', $guide['keyword'])) : "";
    $file_name   = isset($guide['file_name']) && !empty($guide['file_name']) ? explode(',', $guide['file_name']) : [];
    $file_path   = isset($guide['file_path']) && !empty($guide['file_path']) ? explode(',', $guide['file_path']) : [];
    $file_count  = !empty($file_path) && !empty($file_path) ? count($file_path) : 0;


    $guide['keyword']           = !empty($sch_keyword) ? str_replace($sch_keyword, "<span style='color:red;'>{$sch_keyword}</span>", $keyword) : $keyword;
    $guide['file_count']        = $file_count;
    $guide['file_name_list']    = $file_name;
    $guide['file_path_list']    = $file_path;

    $guide_list[] = $guide;
}

$smarty->assign('guide_list', $guide_list);
$smarty->assign('page_type_list', getPageTypeOption('4'));

$smarty->display('total_search_guide_list.html');

?>
