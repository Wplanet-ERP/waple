<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('inc/helper/_navigation.php');

// 접근 권한
if (!permissionNameCheck($session_permission,"대표") && !permissionNameCheck($session_permission,"재무관리자") && !permissionNameCheck($session_permission,"마케터")){
	$smarty->display('access_error.html');
	exit;
}

$proc=(isset($_POST['process']))?$_POST['process']:"";

if($proc=="f_incentive_date") {
	$wp_no=(isset($_POST['wp_no']))?$_POST['wp_no']:"";
	$s_no=(isset($_POST['s_no']))?$_POST['s_no']:"";
	$value=(isset($_POST[$proc]))?addslashes($_POST[$proc]):"";
    $search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

	$incentive_sql="SELECT i.i_no FROM incentive i
					 WHERE i.incentive_date='".$value."' AND i.i_kind='1' AND i.s_no='".$s_no."'
				";
	mysqli_query($my_db,$incentive_sql);
	
	$query= mysqli_query($my_db, "$incentive_sql");
	$result=mysqli_fetch_array($query);
	$i_no = $result[0];

	$work_package_sql="UPDATE work_package wp SET 
							wp.incentive_date='".$value."',
							wp.i_no='".$i_no."'
						WHERE wp.wp_no='".$wp_no."'";
	//echo "$work_package_sql"; exit;
	if(!mysqli_query($my_db,$work_package_sql))
		exit("<script>alert('인센티브 정산 년/월을 저장에 실패하였습니다');location.href='incentive_set.php?$search_url';</script>");
   
}elseif($proc=="f_i_incentive_date") {
	$wp_no=(isset($_POST['wp_no']))?$_POST['wp_no']:"";
	$s_no=(isset($_POST['s_no']))?$_POST['s_no']:"";
	$value=(isset($_POST[$proc]))?addslashes($_POST[$proc]):"";
    $search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

	$incentive_sql="SELECT i.i_no FROM incentive i
					 WHERE i.incentive_date='".$value."' AND i.i_kind='1' AND i.s_no='".$s_no."'
				";
	mysqli_query($my_db,$incentive_sql);
	
	$query= mysqli_query($my_db, "$incentive_sql");
	$result=mysqli_fetch_array($query);
	$i_no = $result[0];

	if(!$i_no)
		exit("<script>alert('인센티브 생성이 되지 않았습니다.');location.href='incentive_set.php?$search_url';</script>");

	$work_package_sql="UPDATE work_package wp SET 
							wp.i_no='".$i_no."'
						WHERE wp.wp_no='".$wp_no."'";
	//echo "$work_package_sql"; exit;
	if(!mysqli_query($my_db,$work_package_sql))
		exit("<script>alert('인센티브 정산 년/월을 저장에 실패하였습니다');location.href='incentive_set.php?$search_url';</script>");

} elseif($proc=="selling_price") {
	$w_no=(isset($_POST['w_no']))?$_POST['w_no']:"";
	$value=(isset($_POST[$proc]))?addslashes($_POST[$proc]):"";
    $search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

	$value = preg_replace("/[^0-9]/", "",$value);

	$sql="UPDATE work w SET w.selling_price='".$value."' WHERE w.w_no='".$w_no."'";

	if(!mysqli_query($my_db,$sql))
        exit("<script>alert('판매가를 저장에 실패하였습니다');location.href='incentive_set.php?$search_url';</script>");
    
} elseif($proc=="wd_money") {
	$w_no=(isset($_POST['w_no']))?$_POST['w_no']:"";
	$value=(isset($_POST[$proc]))?addslashes($_POST[$proc]):"";
    $search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

	$value = preg_replace("/[^0-9]/", "",$value);

	$sql="UPDATE withdraw wd SET wd.wd_money='".$value."' WHERE wd.wd_no=(SELECT dw.wd_no FROM deposit_withdraw dw WHERE dw.f_no=(SELECT w.f_no FROM work w WHERE w_no='".$w_no."'))";
	
	if(!mysqli_query($my_db,$sql))
		exit("<script>alert('외주비 지급액을 저장에 실패 하였습니다');location.href='incentive_set.php?$search_url';</script>");

} elseif($proc=="cost_rate") {
	$w_no=(isset($_POST['w_no']))?$_POST['w_no']:"";
	$value=(isset($_POST[$proc]))?addslashes($_POST[$proc]):"";
    $search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

	$sql="UPDATE work w SET w.cost_rate='".$value."' WHERE w.w_no='".$w_no."'";
	
	if(!mysqli_query($my_db,$sql))
		exit("<script>alert('원가율을 저장에 실패하였습니다');location.href='incentive_set.php?$search_url';</script>");

} elseif($proc=="cost_price") {
	$w_no=(isset($_POST['w_no']))?$_POST['w_no']:"";
	$value=(isset($_POST[$proc]))?addslashes($_POST[$proc]):"";
    $search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

	$value = preg_replace("/[^0-9]/", "",$value);

	$sql="UPDATE work w SET w.cost_price='".$value."' WHERE w.w_no='".$w_no."'";
	//echo $sql; exit;
	
	if(!mysqli_query($my_db,$sql))
		exit("<script>alert('원가액 저장에 실패하였습니다');location.href='incentive_set.php?$search_url';</script>");

} elseif($proc=="incentive_sales_price") {
	$w_no=(isset($_POST['w_no']))?$_POST['w_no']:"";
	$value=(isset($_POST[$proc]))?addslashes($_POST[$proc]):"";
    $search_url_get=isset($_POST['search_url'])?$_POST['search_url']:"";

	$value = preg_replace("/[^0-9]/", "",$value);

	$sql="UPDATE work w SET w.incentive_sales_price='".$value."' WHERE w.w_no='".$w_no."'";
	
	if(!mysqli_query($my_db,$sql))
    	exit("<script>alert('인센기준 매출액을 저장에 실패하였습니다');location.href='incentive_set.php?$search_url';</script>");

} else {
	
	// 리스트 페이지 쿼리 저장
	$save_query=http_build_query($_GET);
	$smarty->assign("save_query",$save_query);

	// 직원가져오기
	$add_where_permission = str_replace("0", "_", $permission_name['마케터']);
	
	$staff_sql="select s_no,s_name from staff where staff_state < '3' AND permission like '".$add_where_permission."'";
	$staff_query=mysqli_query($my_db,$staff_sql);

	while($staff_data=mysqli_fetch_array($staff_query)) {
		$staff[]=array(
				'no'=>$staff_data['s_no'],
				'name'=>$staff_data['s_name']
		);
		$smarty->assign("staff",$staff);
	}

	// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
    $add_where="1=1";
	$sch_incentive_date_get=isset($_GET['sch_incentive_date'])?$_GET['sch_incentive_date']:"";
	$sch_i_incentive_date_get=isset($_GET['sch_i_incentive_date'])?$_GET['sch_i_incentive_date']:"";
    $sch_dp_state_get=isset($_GET['sch_dp_state'])?$_GET['sch_dp_state']:"";
	$sch_dp_tax_state_get=isset($_GET['sch_dp_tax_state'])?$_GET['sch_dp_tax_state']:"";
    $sch_package_kind_get=isset($_GET['sch_package_kind'])?$_GET['sch_package_kind']:"";
    $sch_package_state_get=isset($_GET['sch_package_state'])?$_GET['sch_package_state']:"";
    $sch_s_no_get=isset($_GET['sch_s_no'])?$_GET['sch_s_no']:"";
	$sch_c_name_get=isset($_GET['sch_c_name'])?$_GET['sch_c_name']:"";
	$sch_wp_subject_get=isset($_GET['sch_wp_subject'])?$_GET['sch_wp_subject']:"";
	
	
	if(!empty($sch_incentive_date_get)) {
	    $add_where.=" AND wp.incentive_date='".$sch_incentive_date_get."'";
	    $smarty->assign("sch_incentive_date",$sch_incentive_date_get);
    }
	if(!empty($sch_i_incentive_date_get)) {
	    $add_where.=" AND i.incentive_date='".$sch_i_incentive_date_get."'";
	    $smarty->assign("sch_i_incentive_date",$sch_i_incentive_date_get);
    }
	if(!empty($sch_dp_state_get)) {
	    $add_where.=" AND (SELECT dp_tax_state FROM deposit WHERE dp_no=(SELECT dp_no FROM deposit_withdraw WHERE f_no=(SELECT f_no FROM incentive WHERE i_no=wp.i_no)))='".$sch_dp_state_get."'";
	    $smarty->assign("sch_dp_state",$sch_dp_state_get);
    }
	if(!empty($sch_dp_tax_state_get)) {
	    $add_where.=" AND (SELECT dp_tax_state FROM deposit WHERE dp_no=(SELECT dp_no FROM deposit_withdraw WHERE f_no=(SELECT f_no FROM incentive WHERE i_no=wp.i_no)))='".$sch_dp_tax_state_get."'";
	    $smarty->assign("sch_dp_state",$sch_dp_tax_state_get);
    }
    if(!empty($sch_package_kind_get)) {
	    $add_where.=" AND wp.package_kind='".$sch_package_kind_get."'";
	    $smarty->assign("sch_package_kind",$sch_package_kind_get);
    }
    if(!empty($sch_package_state_get)) {
	    $add_where.=" AND wp.package_state='".$sch_package_state_get."'";
	    $smarty->assign("sch_package_state",$sch_package_state_get);
    }
    if(!empty($sch_s_no_get)) {
	    $add_where.=" AND wp.s_no='".$sch_s_no_get."'";
	    $smarty->assign("sch_s_no",$sch_s_no_get);
    }
	if(!empty($sch_c_name_get)) {
	    $add_where.=" AND wp.company like '%".$sch_c_name_get."%'";
	    $smarty->assign("sch_c_name",$sch_c_name_get);
    }
	if(!empty($sch_wp_subject_get)) {
	    $add_where.=" AND wp.subject like '%".$sch_wp_subject_get."%'";
	    $smarty->assign("sch_wp_subject",$sch_wp_subject_get);
    }

	if(permissionNameCheck($session_permission,"마케터")){
		if($sch_s_no_get != $session_s_no){
            $smarty->display('access_error.html');
	        exit;
        }
	}


	// 정렬순서 토글 & 필드 지정
	$add_orderby.=" wp_no asc";

	// 페이에 따른 limit 설정
	$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
	$num = 10;
	$offset = ($pages-1) * $num;

	// 리스트 쿼리
    $work_package_sql="
                SELECT
                    wp.wp_no, 
					wp.package_state, 
					wp.package_kind, 
					wp.incentive_date,
					(SELECT wd_state FROM withdraw WHERE wd_no=(SELECT wd_no FROM deposit_withdraw WHERE f_no=(SELECT f_no FROM incentive WHERE i_no=wp.i_no))) AS wd_state,
					(SELECT incentive_date FROM incentive WHERE i_no=wp.i_no) AS i_incentive_date,
					(SELECT confirm_state FROM incentive WHERE i_no=wp.i_no) AS confirm_state,
					(SELECT dp_state FROM deposit WHERE dp_no=(SELECT dp_no FROM deposit_withdraw WHERE f_no=(SELECT f_no FROM incentive WHERE i_no=wp.i_no))) AS dp_state,
					(SELECT dp_tax_state FROM deposit WHERE dp_no=(SELECT dp_no FROM deposit_withdraw WHERE f_no=(SELECT f_no FROM incentive WHERE i_no=wp.i_no))) AS dp_tax_state, 
					(SELECT dp_date FROM deposit WHERE dp_no=(SELECT dp_no FROM deposit_withdraw WHERE f_no=(SELECT f_no FROM incentive WHERE i_no=wp.i_no))) AS dp_date, 
					wp.c_no,
		            wp.company,
					wp.subject,
					wp.s_no,
		            (SELECT s_name FROM staff s WHERE s.s_no=wp.s_no) AS s_name,
					(SELECT SUM(`selling_price`) FROM `work` w WHERE w.wp_no=wp.wp_no) AS selling_price_total,
					(SELECT dp_money FROM deposit WHERE dp_no=(SELECT dp_no FROM deposit_withdraw WHERE f_no=wp.f_no)) AS dp_money,
					(SELECT COUNT(*) FROM `work` WHERE wp_no=wp.wp_no) AS work_count
                FROM work_package wp
					LEFT JOIN incentive i
					ON wp.i_no=i.i_no
                WHERE $add_where
                ORDER BY $add_orderby
			    ";					

    // 전체 게시물 수
    $cnt_sql = "SELECT count(*) FROM (".$work_package_sql.") AS cnt";
	//echo "$cnt_sql<br>"; //exit;
    $result= mysqli_query($my_db, $cnt_sql);
    $cnt_data=mysqli_fetch_array($result);
    $total_num = $cnt_data[0];

    $smarty->assign(array(
	    "total_num"=>number_format($total_num)
    ));

    $smarty->assign("total_num",$total_num);
    $pagenum = ceil($total_num/$num);


    // 페이징
    if ($pages>=$pagenum){$pages=$pagenum;}
    if ($pages<=0){$pages=1;}

    $search_url = "sch_incentive_date=".$sch_incentive_date_get."&sch_i_incentive_date=".$sch_i_incentive_date_get."&sch_dp_state=".$sch_dp_state_get."&sch_dp_state=".$sch_dp_state_get."&sch_package_kind=".$sch_package_kind_get."&sch_package_state=".$sch_package_state_get."&sch_tax_state=".$sch_tax_state_get."&sch_subject=".$sch_subject_get;
    $smarty->assign("search_url",$search_url);
    $page=pagelist($pages, "incentive_set.php", $pagenum, $search_url);
    $smarty->assign("pagelist",$page);


    // 리스트 쿼리 결과(데이터)
    $work_package_sql .= "LIMIT $offset,$num";
    //echo "$work_package_sql<br>"; //exit;

	$result= mysqli_query($my_db, $work_package_sql);
    while($work_package_array = mysqli_fetch_array($result)){

	    $work_package[] = array(
		    "wp_no"=>$work_package_array['wp_no'],
			"incentive_date"=>$work_package_array['incentive_date'],
			"i_incentive_date"=>$work_package_array['i_incentive_date'],
			"confirm_state"=>$work_package_array['confirm_state'],
			"wd_state_name"=>$wd_state[$work_package_array['wd_state']][0],
			"dp_tax_state_name"=>$dp_tax_state[$work_package_array['dp_tax_state']][0],
			"dp_state_name"=>$dp_state[$work_package_array['dp_state']][0],
			"dp_date"=>$work_package_array['dp_date'],
		    "package_state"=>$package_state[$work_package_array['package_state']][0],
			"package_kind_name"=>$package_kind[$work_package_array['package_kind']][0],
            "c_no"=>$work_package_array['c_no'],
            "company"=>$work_package_array['company'],
			"subject"=>$work_package_array['subject'],
			"s_no"=>$work_package_array['s_no'],
            "s_name"=>$work_package_array['s_name'],
			"selling_price_total"=>number_format($work_package_array['selling_price_total']),
			"dp_money"=>number_format($work_package_array['dp_money']),
			"work_count"=>$work_package_array['work_count'],
            /*현재입금액*/
	    );

		if($work_package_array['work_count'] > 0){
			$w_add_where="1=1";
			$w_add_where ="w.wp_no='".$work_package_array['wp_no']."'";

			// 업무 리스트 쿼리
			$work_sql="
					SELECT 
					w.w_no,
					w.wp_no,
					w.work_state,
					(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1,
					(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS k_prd1_name,
					(SELECT CONCAT(k.k_name_code) FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2,
					(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_prd2_name, 
					w.prd_no,
					(SELECT title FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_title,
					w.task_req,
					w.task_value,
					w.task_value2,
					w.out_c_no,
					w.selling_price,
					(SELECT wd.wd_money FROM withdraw wd WHERE wd.wd_no=(SELECT dw.wd_no FROM deposit_withdraw dw WHERE dw.f_no=w.f_no)) AS wd_money,
					w.cost_rate,
					w.cost_price,
					w.incentive_sales_price
				FROM 
					work w 
				WHERE 
					$w_add_where
				ORDER BY 
					w_no asc
				";
			//echo "$work_sql<br>"; //exit;

			$work_result= mysqli_query($my_db, $work_sql);
			while($work_array = mysqli_fetch_array($work_result)){
				$works[] = array(
					"w_no"=>$work_array['w_no'],
					"wp_no"=>$work_array['wp_no'],
					"work_state_name"=>$work_state[$work_array['work_state']][0],
					"k_prd1"=>$work_array['k_prd1'],
					"k_prd1_name"=>$work_array['k_prd1_name'],
					"k_prd2"=>$work_array['k_prd2'],
					"k_prd2_name"=>$work_array['k_prd2_name'],
					"prd_no"=>$work_array['prd_no'],
					"prd_title"=>$work_array['prd_title'],
					"task_req"=>$work_array['task_req'],
					"task_value"=>$work_array['task_value'],
					"task_value2"=>$work_array['task_value2'],
					"out_c_no"=>$work_array['out_c_no'],
					"selling_price"=>number_format($work_array['selling_price']),
					"wd_money"=>number_format($work_array['wd_money']),
					"cost_rate"=>$work_array['cost_rate'],
					"cost_price"=>number_format($work_array['cost_price']),
					"incentive_sales_price"=>number_format($work_array['incentive_sales_price'])
				);
			}
			$smarty->assign(array(
				"work"=>$works
			));
		}
    }
	$smarty->assign(array(
		"work_package"=>$work_package
	));
}

// 템플릿에 해당 입력값 던져주기
$smarty->assign(
	array(
		"proc"=>$proc,
	)
);
$smarty->display('incentive_set.html');

?>