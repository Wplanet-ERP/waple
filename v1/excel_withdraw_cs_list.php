<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');

require('inc/common.php');
require('ckadmin.php');
require('Classes/PHPExcel.php');

# Create new PHPExcel object & SET document
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

# 상단타이틀 설정
$work_sheet			= $objPHPExcel->setActiveSheetIndex(0);
$header_title_list  = array("출금완료일", "요청자(CS담당자)", "주문번호", "예금주명", "사유", "브랜드명", "금액");
$head_idx           = 0;
foreach(range('A', 'G') as $columnID){
	$work_sheet->setCellValue("{$columnID}1", $header_title_list[$head_idx]);
	$head_idx++;
}

# 검색
$add_where      	= "w.prd_no='229' AND w.work_state='6' AND wd.wd_state='3' AND wd.wd_date IS NOT NULL AND wd.display='1'";
$sch_wd_state 		= isset($_GET['sch_wd_state']) ? $_GET['sch_wd_state'] : "";
$sch_s_wd_date 		= isset($_GET['sch_s_wd_date']) ? $_GET['sch_s_wd_date'] : "";
$sch_e_wd_date 		= isset($_GET['sch_e_wd_date']) ? $_GET['sch_e_wd_date'] : "";
$sch_prd_no			= isset($_GET['sch_prd_no'])?$_GET['sch_prd_no']:"";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

if(!empty($sch_brand)) {
	$add_where      .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
	$add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
	$add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

if (!empty($sch_wd_state)) {
	$add_where .= " AND wd.wd_state='{$sch_wd_state}'";
}

if (!empty($sch_s_wd_date)) {
	$add_where .= " AND wd.wd_date >= '{$sch_s_wd_date}'";
}

if (!empty($sch_e_wd_date)) {
	$add_where .= " AND wd.wd_date <= '{$sch_e_wd_date}'";
}

if(!empty($sch_prd_no)) { // 상품
	$add_where.=" AND `w`.prd_no='{$sch_prd_no}'";
}

# 리스트 쿼리
$idx 			= 2;
$withdraw_sql 	= "
	SELECT
		wd.wd_date,
		(SELECT s.s_name FROM staff s WHERE s.s_no=w.task_req_s_no) AS req_s_name,
		(SELECT sub.order_number FROM work_cms sub WHERE sub.w_no=w.linked_no) as ord_no,
		wd.bk_name,
		w.task_req,
		w.c_name,
		wd.wd_money
	FROM withdraw wd
	LEFT JOIN `work` AS w ON w.wd_no=wd.wd_no
	WHERE {$add_where}
	ORDER BY wd.wd_date DESC
	LIMIT 2000
";
$withdraw_query = mysqli_query($my_db, $withdraw_sql);
while ($withdraw_array = mysqli_fetch_array($withdraw_query))
{
	$wd_money 		= number_format($withdraw_array['wd_money']);
	$task_req_tmp   = explode("사유 : ", $withdraw_array['task_req']);
	$reason 		= isset($task_req_tmp[1]) ? $task_req_tmp[1] : "";

	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue("A{$idx}", $withdraw_array['wd_date'])
		->setCellValue("B{$idx}", $withdraw_array['req_s_name'])
		->setCellValueExplicit("C{$idx}", $withdraw_array['ord_no'],PHPExcel_Cell_DataType::TYPE_STRING)
		->setCellValue("D{$idx}", $withdraw_array['bk_name'])
		->setCellValue("E{$idx}", $reason)
		->setCellValue("F{$idx}", $withdraw_array['c_name'])
		->setCellValue("G{$idx}", $wd_money)
	;
	$idx++;
}
$idx--;

$objPHPExcel->getActiveSheet()->getStyle("A1:G{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:G{$idx}")->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:G{$idx}")->getFont()->setSize(10);
$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00c4bd97');

$objPHPExcel->getActiveSheet()->getStyle("A1:G{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:G{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("E2:E{$idx}")->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);

$objPHPExcel->getActiveSheet()->setTitle('커머스 환불 리스트');
$objPHPExcel->setActiveSheetIndex(0);

$excel_filename = iconv('UTF-8','EUC-KR',date("Ymd")."_커머스 환불 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
