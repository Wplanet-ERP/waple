<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');
require('inc/model/Message.php');

# 프로세스 처리
$process 		= isset($_POST['process']) ? $_POST['process'] : "";
$message_model 	= Message::Factory();

if ($process == "send")
{
	$receiver_list	= isset($_POST['receiver_list']) ? $_POST['receiver_list'] : "";
	$sms_msg_val 	= isset($_POST['sms_msg']) ? $_POST['sms_msg'] : "";
	$send_phone		= isset($_POST['send_phone']) ? $_POST['send_phone'] : "";
	$send_data_list = [];

	$receiver_list		= str_replace(" ", "", $receiver_list);
	$receiver_list 		= preg_replace("/\r\n|\r|\n/", "||", $receiver_list);
	$receiver_hp_list 	= explode("||", $receiver_list);

	if (!$send_phone) {
		$send_phone = "02-2675-6260";
	}

	$sms_title		= "와이즈플래닛";
	$sms_msg		= addslashes($sms_msg_val);
	$sms_msg_len 	= mb_strlen(iconv('utf-8', 'euc-kr', $sms_msg), '8bit');
	$msg_type 		= ($sms_msg_len > 90) ? "L" : "S";
	$send_name 		= $session_name;

	$cur_datetime	= date("YmdHis");
	$cm_id 			= 1;
	$c_info 		= "3"; # 직원간 SMS

	foreach ($receiver_hp_list as $receiver_hp)
	{
		if(empty($receiver_hp)){
			continue;
		}

		$chk_hp 	 = str_replace("-","", $receiver_hp);
		$msg_id 	 = "W{$cur_datetime}".sprintf('%04d', $cm_id++);

		$chk_sql	 = "SELECT s_name FROM staff WHERE hp IS NOT NULL AND staff_state='1' AND REPLACE(hp,'-','')='{$chk_hp}' LIMIT 1";
		$chk_query	 = mysqli_query($my_db, $chk_sql);
		$chk_result  = mysqli_fetch_assoc($chk_query);
		$receiver  	 = isset($chk_result['s_name']) ? $chk_result['s_name'] : "";

		$send_data_list[$msg_id] = array(
			"msg_id"        => $msg_id,
			"msg_type"      => $msg_type,
			"sender"        => $send_name,
			"sender_hp"     => $send_phone,
			"receiver"      => $receiver,
			"receiver_hp"   => $receiver_hp,
			"title"         => $sms_title,
			"content"       => $sms_msg,
			"cinfo"         => $c_info,
		);
	}

	if(!empty($send_data_list)){
		$result_data = $message_model->sendMessage($send_data_list);

		if ($result_data['result']) {
			exit("<script>alert('문자 메시지를 발송하였습니다.');location.href='staff_sms.php';</script>");
		}
	}

	exit("<script>alert('문자 메세지 발송에 실패했습니다.');history.back();</script>");
}

# Navigation & My Quick
$nav_prd_no  = "82";
$nav_title   = "와이즈 플래닛 문자 보내기";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 리스트 쿼리
$staff_sql 	 = "SELECT * FROM staff WHERE staff_state='1' ORDER BY s_name";
$staff_query = mysqli_query($my_db, $staff_sql);
$staff_list	 = [];
while($staff_result = mysqli_fetch_array($staff_query)){
	$staff_list[] = $staff_result;
}

$smarty->assign("staff_list", $staff_list);
$smarty->display('staff_sms.html');
?>
