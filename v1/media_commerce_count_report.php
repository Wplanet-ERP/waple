<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/commerce_sales.php');
require('inc/helper/work_cms.php');
require('inc/helper/wise_bm.php');
require('inc/model/MyQuick.php');
require('inc/model/Kind.php');
require('inc/model/Company.php');

# Navigation & My Quick
$nav_prd_no  = "170";
$nav_title   = "커머스 주문수";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);


# Brand Group 검색
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$dp_except_list             = getNotApplyDpList();
$dp_except_text             = implode(",", $dp_except_list);
$global_dp_all_list         = getGlobalDpCompanyList();
$global_dp_ilenol_list      = $global_dp_all_list['ilenol'];
$doc_brand_list             = getTotalDocBrandList();
$doc_brand_text             = implode(",", $doc_brand_list);
$ilenol_brand_list          = getIlenolBrandList();
$ilenol_brand_text          = implode(",", $ilenol_brand_list);
$ilenol_global_dp_text      = implode(",", $global_dp_ilenol_list);
$global_brand_list          = getGlobalBrandList();
$global_brand_option        = array_keys($global_brand_list);
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

# 브랜드 검색 설정 및 수정여부
$add_cms_where      = "w.delivery_state='4' AND w.dp_c_no NOT IN({$dp_except_text}) AND w.unit_price > 0";
$add_quick_where    = "w.quick_state='4' AND w.prd_type='1' AND w.unit_price > 0";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$kind_column_list   = [];

if(!isset($_GET['sch_brand_g1']))
{
    switch($session_s_no){
        case '4':
            $sch_brand_g1   = '70003';
            break;
        case '6':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70008';
            break;
        case '7':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70014';
            $sch_brand      = '4446';
            break;
        case '10':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5368';
            break;
        case '15':
            $sch_brand_g1   = '70002';
            break;
        case '17':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '1314';
            break;
        case '42':
        case '177':
        case '265':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            break;
        case '59':
            $sch_brand_g1   = '70005';
            $sch_brand_g2   = '70020';
            $sch_brand      = '2402';
            break;
        case '67':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5434';
            break;
        case '145':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70010';
            break;
        case '147':
            $sch_brand_g1   = '70004';
            $sch_brand_g2   = '70019';
            break;
        case '43':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5812';
            break;
    }
}

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    if(in_array($sch_brand, $doc_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5956' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif(in_array($sch_brand, $ilenol_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5659' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "5513"){
        $add_cms_where      .= " AND `w`.c_no IN({$doc_brand_text}) AND (`w`.log_c_no = '5956' OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$doc_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5514"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND ((`w`.log_c_no = '5659' AND `w`.dp_c_no NOT IN({$ilenol_global_dp_text})) OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5979"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.c_no != '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "6044"){
        $add_cms_where      .= " AND `w`.c_no = '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    else{
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}'";
    }
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_cms_where     .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_quick_where   .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_cms_where     .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_quick_where   .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 날짜별 검색
$today_s_w	    = date('w')-1;
$sch_s_year     = isset($_GET['sch_s_year']) ? $_GET['sch_s_year'] : date('Y', strtotime("-5 years"));;
$sch_e_year     = isset($_GET['sch_e_year']) ? $_GET['sch_e_year'] : date('Y');
$sch_s_month    = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("-5 months"));
$sch_e_month    = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week     = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week     = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date     = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-8 day"));
$sch_e_date     = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');
$sch_not_empty  = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : "";
$sch_dp_c_no    = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_s_year', $sch_s_year);
$smarty->assign('sch_e_year', $sch_e_year);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);
$smarty->assign('sch_not_empty', $sch_not_empty);

$company_model = Company::Factory();
if(!empty($sch_dp_c_no)){
    $add_cms_where      .= " AND dp_c_no = '{$sch_dp_c_no}'";
    $add_quick_where    .= " AND run_c_no = '{$sch_dp_c_no}'";
    $smarty->assign("sch_dp_c_no", $sch_dp_c_no);
}
$smarty->assign('sch_dp_company_list', $company_model->getDpDisplayList());

//전체 기간 조회 및 누적데이터 조회
$all_date_where   = "";
$all_date_key	  = "";
$add_cms_column   = "";
$add_quick_column = "";

if($sch_date_type == '4') //연간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y')";

    $sch_s_datetime     = $sch_s_year."-01-01 00:00:00";
    $sch_e_datetime     = $sch_e_year."-12-31 23:59:59";

    $add_cms_where     .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column     = "DATE_FORMAT(order_date, '%Y')";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y')";
}
elseif($sch_date_type == '3') //월간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $sch_s_datetime     = $sch_s_month."-01 00:00:00";
    $sch_e_datetime     = $sch_e_month."-31 23:59:59";

    $add_cms_where     .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m')";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y%m')";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}'";
    $all_date_key 	    = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title     = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $sch_s_datetime     = $sch_s_week." 00:00:00";
    $sch_e_datetime     = $sch_e_week." 23:59:59";

    $add_cms_where     .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column     = "DATE_FORMAT(DATE_SUB(order_date, INTERVAL(IF(DAYOFWEEK(order_date)=1,8,DAYOFWEEK(order_date))-2) DAY), '%Y%m%d')";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(DATE_SUB(run_date, INTERVAL(IF(DAYOFWEEK(run_date)=1,8,DAYOFWEEK(run_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $sch_s_datetime     = $sch_s_date." 00:00:00";
    $sch_e_datetime     = $sch_e_date." 23:59:59";

    $add_cms_where      .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m%d')";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y%m%d')";
}

# 배송리스트 구매처
$cms_title_sql              = "SELECT DISTINCT dp_c_no, dp_c_name, (SELECT c.priority FROM company c WHERE c.c_no=w.dp_c_no) as priority FROM work_cms as w WHERE {$add_cms_where} ORDER BY priority ASC, dp_c_name ASC";
$cms_title_query            = mysqli_query($my_db, $cms_title_sql);
$cms_report_title_list      = [];
while($cms_title = mysqli_fetch_assoc($cms_title_query)) {
    $cms_report_title_list[$cms_title['dp_c_no']] = $cms_title['dp_c_name'];
}

$cms_report_prd_title_list      = [];
$cms_report_prd_best_title_list = [];
$cms_report_prd_best_list       = [];
if($sch_brand_g2 == '70015')
{
    # 배송리스트 상품
    $cms_prd_title_sql      = "SELECT prd_no, (SELECT p.title FROM product_cms p WHERE p.prd_no=w.prd_no) as prd_name, COUNT(DISTINCT order_number) as ord_cnt FROM work_cms as w WHERE {$add_cms_where} GROUP BY prd_no ORDER BY ord_cnt DESC, prd_name ASC";
    $cms_prd_title_query    = mysqli_query($my_db, $cms_prd_title_sql);
    $cms_prd_idx            = 0;
    while($cms_prd_title= mysqli_fetch_assoc($cms_prd_title_query)) {
        $cms_report_prd_title_list[$cms_prd_title['prd_no']] = $cms_prd_title['prd_name'];

        if($cms_prd_idx < 10){
            $cms_report_prd_best_title_list[$cms_prd_title['prd_no']] = $cms_prd_title['prd_name'];
        }

        $cms_prd_idx++;
    }
    $cms_report_prd_best_title_list["etc"] = "기타";
}

# 입/출고요청 구매처
$quick_title_sql            = "SELECT DISTINCT run_c_no, run_c_name FROM work_cms_quick as w WHERE {$add_quick_where} ORDER BY run_c_name ASC";
$quick_title_query          = mysqli_query($my_db, $quick_title_sql);
$quick_report_title_list    = [];
while($quick_title = mysqli_fetch_assoc($quick_title_query)) {
    $quick_report_title_list[$quick_title['run_c_no']] = $quick_title['run_c_name'];
}

$commerce_date_list             = [];
$cms_report_tmp_list            = [];
$cms_report_list                = [];
$cms_report_total_sum_list      = [];
$cms_report_prd_tmp_list        = [];
$cms_report_prd_list            = [];
$cms_report_prd_total_sum_list  = [];
$quick_report_list              = [];
$quick_report_tmp_list          = [];

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
if($all_date_where != '')
{
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        foreach($cms_report_title_list as $cms_key => $cms_title)
        {
            $cms_report_list[$cms_key][$date['chart_key']]['count']     = 0;
            $cms_report_list[$cms_key][$date['chart_key']]['combined']  = 0;
        }

        foreach($quick_report_title_list as $quick_key => $quick_title)
        {
            $quick_report_list[$quick_key][$date['chart_key']]['count']     = 0;
            $quick_report_list[$quick_key][$date['chart_key']]['combined']  = 0;
        }

        if(!isset($commerce_date_list[$date['chart_key']]))
        {
            $commerce_date_list[$date['chart_key']] = $chart_title;
        }

        $cms_report_total_sum_list[$date['chart_key']]['total_count']    = 0;
        $cms_report_total_sum_list[$date['chart_key']]['total_combined'] = 0;

        if($sch_brand_g2 == '70015')
        {
            foreach($cms_report_prd_title_list as $prd_key => $prd_title)
            {
                $cms_report_prd_list[$prd_key][$date['chart_key']] = 0;
            }
            $cms_report_prd_total_sum_list[$date['chart_key']] = 0;

            foreach($cms_report_prd_best_title_list as $best_prd_key => $best_prd_title)
            {
                $cms_report_prd_best_list[$best_prd_key][$date['chart_key']] = 0;
            }
        }
    }
}

# 배송리스트 매출관련
$cms_report_sql      = "
    SELECT 
        c_no,
        prd_no,
        dp_c_no,
        dp_c_name,
        order_number,
        {$add_cms_column} as key_date
    FROM work_cms as w
    WHERE {$add_cms_where}
";
$cms_report_query = mysqli_query($my_db, $cms_report_sql);
while($cms_report_result = mysqli_fetch_assoc($cms_report_query))
{
    if(in_array($sch_brand, $global_brand_option)){
        $cms_report_result['c_no'] = $sch_brand;
    }
    
    $cms_report_tmp_list[$cms_report_result['dp_c_no']][$cms_report_result['key_date']][$cms_report_result['order_number']][$cms_report_result['c_no']] = 1;
    $cms_report_prd_tmp_list[$cms_report_result['prd_no']][$cms_report_result['key_date']][$cms_report_result['order_number']] = 1;
}

if(!empty($cms_report_tmp_list))
{
    foreach($cms_report_tmp_list as $dp_c_no => $cms_report_tmp)
    {
        foreach($cms_report_tmp as $key_date => $tmp_key_data)
        {
            foreach($tmp_key_data as $ord_no => $tmp_ord_data)
            {
                $cms_report_list[$dp_c_no][$key_date]['count']++;
                $cms_report_total_sum_list[$key_date]['total_count']++;

                if(count($tmp_ord_data) > 1){
                    $cms_report_list[$dp_c_no][$key_date]['combined']++;
                    $cms_report_total_sum_list[$key_date]['total_combined']++;
                }
            }
        }
    }
}

# 배송리스트 매출관련
$quick_report_sql      = "
    SELECT 
        c_no,
        run_c_no,
        order_number,
        {$add_quick_column} as key_date
    FROM work_cms_quick as w
    WHERE {$add_quick_where}
";
$quick_report_query = mysqli_query($my_db, $quick_report_sql);
while($quick_report_result = mysqli_fetch_assoc($quick_report_query))
{
    if(in_array($sch_brand, $global_brand_option)){
        $quick_report_result['c_no'] = $sch_brand;
    }

    $quick_report_tmp_list[$quick_report_result['run_c_no']][$quick_report_result['key_date']][$quick_report_result['order_number']][$quick_report_result['c_no']] = 1;
}

if(!empty($quick_report_tmp_list))
{
    foreach($quick_report_tmp_list as $run_c_no => $quick_report_tmp)
    {
        foreach($quick_report_tmp as $run_key_date => $tmp_run_key_data)
        {
            foreach($tmp_run_key_data as $quick_no => $tmp_quick_data)
            {
                $quick_report_list[$run_c_no][$run_key_date]['count']++;
                $cms_report_total_sum_list[$run_key_date]['total_count']++;

                if(count($tmp_quick_data) > 1){
                    $quick_report_list[$run_c_no][$run_key_date]['combined']++;
                    $cms_report_total_sum_list[$run_key_date]['total_combined']++;
                }
            }
        }
    }
}

$cms_report_best_chart_list = [];
if(!empty($cms_report_prd_tmp_list) && $sch_brand_g2 == '70015')
{
    $best_prd_color_list = array("#5470C6","#EE6666","#3BA272","#FC8452","#9A60B4","#EA7CCC","#73C0DE","#91CC75","#FAC858","#ED9FB6","#DDDDDD");
    foreach($cms_report_prd_tmp_list as $prd_no => $cms_report_prd_tmp)
    {
        foreach($cms_report_prd_tmp as $prd_key_date => $tmp_prd_key_data)
        {
            foreach($tmp_prd_key_data as $prd_ord_no => $tmp_prd_ord_data)
            {
                if(isset($cms_report_prd_best_title_list[$prd_no])){
                    $cms_report_prd_best_list[$prd_no][$prd_key_date]++;
                }else{
                    $cms_report_prd_best_list["etc"][$prd_key_date]++;
                }

                $cms_report_prd_list[$prd_no][$prd_key_date]++;
                $cms_report_prd_total_sum_list[$prd_key_date]++;
            }
        }
    }

    if(!empty($cms_report_prd_best_list))
    {
        $best_prd_idx = 0;
        foreach($cms_report_prd_best_list as $best_prd_no => $best_prd_data)
        {
            $best_chart_init_data = array(
                "title" => $cms_report_prd_best_title_list[$best_prd_no],
                "color" => $best_prd_color_list[$best_prd_idx],
                "data"  => array(),
            );

            foreach($best_prd_data as $best_prd_date => $best_date_data)
            {
                $best_chart_init_data["data"][] = $best_date_data;
            }

            $cms_report_best_chart_list[] = $best_chart_init_data;
            $best_prd_idx++;
        }
    }
}

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);
$smarty->assign('commerce_date_list', $commerce_date_list);
$smarty->assign('commerce_date_count', count($commerce_date_list));
$smarty->assign('cms_report_list', $cms_report_list);
$smarty->assign('cms_report_title_list', $cms_report_title_list);
$smarty->assign('cms_report_total_sum_list', $cms_report_total_sum_list);
$smarty->assign('quick_report_list', $quick_report_list);
$smarty->assign('quick_report_title_list', $quick_report_title_list);
$smarty->assign('cms_report_prd_title_list', $cms_report_prd_title_list);
$smarty->assign('cms_report_prd_list', $cms_report_prd_list);
$smarty->assign('cms_report_prd_total_sum_list', $cms_report_prd_total_sum_list);
$smarty->assign('cms_report_best_chart_name_list', json_encode($commerce_date_list));
$smarty->assign('cms_report_best_total_list', json_encode($cms_report_prd_total_sum_list));
$smarty->assign('cms_report_best_chart_list', json_encode($cms_report_best_chart_list));

$smarty->display('media_commerce_count_report.html');
?>
