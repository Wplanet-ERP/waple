<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Team.php');

$s_no = isset($_GET['s_no']) ? $_GET['s_no'] : (isset($_POST['s_no']) ? $_POST['s_no'] : "");

if(empty($s_no)){
    exit("<script>alert('s_no 값이 없습니다.'); history.back();</script>");
}

$staff_sql    = "SELECT * FROM staff WHERE s_no='{$s_no}' AND staff_state='1' LIMIT 1";
$staff_query  = mysqli_query($my_db, $staff_sql);
$staff_result = mysqli_fetch_assoc($staff_query);

$mail_signature = [];
if(isset($staff_result['s_no']))
{
    $mail_signature['s_no']     = $staff_result['s_no'];
    $mail_signature['kor_name'] = $staff_result['s_name'];
    $mail_signature['team']     = $staff_result['team'];
    $mail_signature['tel']      = $staff_result['tel'];
    $mail_signature['hp']       = $staff_result['hp'];
    $mail_signature['email']    = $staff_result['email'];
    $mail_signature['position'] = $staff_result['position'];
    $mail_signature['com_tel']  = "02-830-1912";
    $mail_signature['fax']      = "02-830-1913";

    if($staff_result['my_c_no'] == '3'){
        $mail_signature['eng_c_name'] = "WISE MEDIA COMMERCE";
        $mail_signature['kor_c_name'] = "(주)와이즈미디어커머스";
        $mail_signature['url']        = "www.belabef.com";
        $mail_signature['c_no']       = "3";
    }else{
        $mail_signature['eng_c_name'] = "WISE PLANET COMPANY";
        $mail_signature['kor_c_name'] = "(주)와이즈플래닛컴퍼니";
        $mail_signature['url']        = "www.wplanet.co.kr";
        $mail_signature['c_no']       = "1";
    }
}

if(!empty($s_no))
{
    $mail_sign_sql    = "SELECT * FROM staff_mail_signature WHERE s_no='{$s_no}' LIMIT 1";
    $mail_sign_query  = mysqli_query($my_db, $mail_sign_sql);
    $mail_sign_result = mysqli_fetch_assoc($mail_sign_query);

    if(isset($mail_sign_result['sms_no'])){
        $mail_signature = $mail_sign_result;
    }
}

$team_model         = Team::Factory();
$mail_team_chk_list = $team_model->getTeamChkList();

$smarty->assign($mail_signature);
$smarty->assign("mail_team_chk_list", $mail_team_chk_list);
$smarty->display('staff_regist_mail_signature.html');
?>
