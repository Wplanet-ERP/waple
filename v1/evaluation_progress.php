<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/evaluation.php');
require('inc/model/Evaluation.php');
require('inc/model/Staff.php');

# Model Init
$staff_model        = Staff::Factory();
$ev_system_model 	= Evaluation::Factory();
$ev_relation_model  = Evaluation::Factory();
$ev_relation_model->setMainInit("evaluation_relation", "ev_r_no");

# 평가하기 시작
$ev_no		    = isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$ev_r_no        = isset($_GET['ev_r_no']) ? $_GET['ev_r_no'] : "";

$smarty->assign("ev_no", $ev_no);
$smarty->assign("ev_r_no", $ev_r_no);

if(empty($ev_no)){
    exit("<script type='text/javascript'>alert('선택된 평가지가 없습니다.');location.href='main.php';</script>");
}

$ev_system_item		= $ev_system_model->getItem($ev_no);
$cur_date           = date("Y-m-d");

if(empty($ev_system_item)){
    exit("<script type='text/javascript'>alert('없는 평가지입니다.');location.href='main.php';</script>");
}

$is_admin = false;
if($ev_system_item['admin_s_no'] == $session_s_no){
    $is_admin = true;
}
$smarty->assign("is_admin", $is_admin);

if(!($ev_system_item['admin_s_no'] == $session_s_no || permissionNameCheck($session_permission, "대표") || $session_s_no == '28'))
{
    if($ev_system_item['ev_state'] == '1'){
        echo "아직 평가일이 아닙니다.";
        exit;
    }elseif($ev_system_item['ev_state'] == '3' || $ev_system_item['ev_state'] == '4'){
        echo "평가기간이 종료되었습니다.";
        exit;
    }
    elseif($ev_system_item['ev_state'] != '2'){
        echo "평가 진행중이 아닙니다.";
        exit;
    }

    if(strtotime($ev_system_item['ev_s_date']) > strtotime($cur_date)){
        echo "아직 평가일이 아닙니다.";
        exit;
    }elseif(strtotime($ev_system_item['ev_e_date']) < strtotime($cur_date)){
        echo "평가기간이 종료되었습니다.";
        exit;
    }
}

$ev_system_item['description'] = nl2br($ev_system_item['description']);

if(!isset($_GET['ev_r_no']))
{
    $base_eval_sql      = "SELECT * FROM evaluation_relation er WHERE er.ev_no='{$ev_no}' AND er.evaluator_s_no = '{$session_s_no}' AND er.rec_is_review='1' AND er.eval_is_review='1' ORDER BY ev_r_no";
    $base_eval_query    = mysqli_query($my_db, $base_eval_sql);
    $base_eval_report   = 0;
    while($base_eval_result = mysqli_fetch_assoc($base_eval_query))
    {
        if($session_s_no == $base_eval_result['receiver_s_no']){
            $base_eval_report = $base_eval_result['ev_r_no'];
        }elseif(!empty($base_eval_report)){
            continue;
        }else{
            $base_eval_report = $base_eval_result['ev_r_no'];
        }
    }

    $ev_r_no = $base_eval_report;

    $smarty->assign("ev_r_no", $ev_r_no);
}

$is_guide_popup     = false;
$guide_modal_list   = [];
if(!empty($ev_system_item['guide_path'])){
    $is_guide_popup   = (empty($ev_r_no)) ? true : false;
    $guide_modal_list = explode(',', $ev_system_item['guide_path']);
}
$smarty->assign("is_guide_popup", $is_guide_popup);
$smarty->assign("guide_modal_list", $guide_modal_list);
$smarty->assign("evaluation_system", $ev_system_item);

# 평가지 문항
$evaluation_unit_sql    = "SELECT * FROM evaluation_unit WHERE ev_u_set_no='{$ev_system_item['ev_u_set_no']}' AND active='1' AND evaluation_state NOT IN(99,100) ORDER BY `order` ASC";
$evaluation_unit_query  = mysqli_query($my_db, $evaluation_unit_sql);
$ev_unit_question_cnt   = 0;
while($evaluation_unit = mysqli_fetch_array($evaluation_unit_query))
{
    $ev_unit_question_cnt++;
}

# 현재 평가받는 사람
$evaluation_receiver = [];
if(!empty($ev_r_no)) {
    $ev_relation_item    = $ev_relation_model->getItem($ev_r_no);
    $evaluation_receiver = $staff_model->getStaff($ev_relation_item['receiver_s_no']);

    if($ev_relation_item['evaluator_s_no'] != $session_s_no){
        echo "평가 대상자가 아닙니다.";
        exit;
    }
}
$smarty->assign("evaluation_receiver", $evaluation_receiver);

# 평가자
$evaluation_evaluator = $staff_model->getStaff($session_s_no);
$smarty->assign("evaluation_evaluator", $evaluation_evaluator);

# 평가 시스템 관계 쿼리
$evaluation_relation_sql = "
    SELECT 
        *, 
        (SELECT s_name FROM staff s where s.s_no=er.receiver_s_no) as rec_s_name,
        (SELECT COUNT(ev_result_no) as cnt FROM evaluation_system_result esr WHERE esr.ev_no='{$ev_no}' AND esr.receiver_s_no=er.receiver_s_no AND esr.evaluator_s_no='{$session_s_no}' AND ((evaluation_state IN(1,2,3) AND evaluation_value > 0) OR (evaluation_state NOT IN(1,2,3) AND evaluation_value IS NOT NULL AND evaluation_value != ''))) as result_cnt,
        (SELECT ROUND(AVG(evaluation_value),2) as cnt FROM evaluation_system_result esr WHERE esr.ev_no='{$ev_no}' AND esr.receiver_s_no=er.receiver_s_no AND esr.evaluator_s_no='{$session_s_no}' AND evaluation_state IN(1,2,3) AND evaluation_value IS NOT NULL AND evaluation_value != '') as result_avg
    FROM evaluation_relation er
    WHERE er.ev_no='{$ev_no}' AND er.evaluator_s_no = '{$session_s_no}' AND er.rec_is_review='1' AND er.eval_is_review='1'
    ORDER BY ev_r_no
";
$evaluation_relation_query      = mysqli_query($my_db, $evaluation_relation_sql);
$evaluation_relation_list       = array('self' => [], "wait" => [], "progress" => [], "complete" => []);
$evaluation_relation_sort_list  = [];
while($evaluation_relation = mysqli_fetch_assoc($evaluation_relation_query))
{
    $state      = "";
    $rec_s_name = $evaluation_relation['rec_s_name'];
    if($evaluation_relation['evaluator_s_no'] == $evaluation_relation['receiver_s_no']){
        $state = "self";
    }elseif($evaluation_relation['is_complete'] == '1'){
        $state = "complete";
        $evaluation_relation_sort_list[$evaluation_relation['ev_r_no']] = $evaluation_relation['result_avg'];
    }elseif($evaluation_relation['result_cnt'] > 0){
        $state = "progress";
    }else{
        $state = "wait";
    }

    $evaluation_relation_list[$state][$evaluation_relation['ev_r_no']] = array(
        "rec_s_name" => $rec_s_name,
    );
}

arsort($evaluation_relation_sort_list);
$smarty->assign("evaluation_relation_list", $evaluation_relation_list);
$smarty->assign("evaluation_relation_sort_list", $evaluation_relation_sort_list);

if($ev_system_item['ev_u_set_no'] == "13"){
    $smarty->display('evaluation_progress_invest.html');
}else{
    $smarty->display('evaluation_progress.html');
}
?>
