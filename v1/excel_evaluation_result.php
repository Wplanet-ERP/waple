<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/data_state.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
		),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);


$ev_no=isset($_GET['ev_no']) ? $_GET['ev_no'] : "";
$ev_r_no=isset($_GET['ev_r_no']) ? $_GET['ev_r_no'] : "";

function numToExcelAlpha($n) {
    $r = 'A';
    while ($n-- > 1) {
        $r++;
    }
    return $r;
}

$idx = 0;

$sheet = $objPHPExcel->setActiveSheetIndex(0);
$sheet->getDefaultStyle()->getFont()
->setName('맑은 고딕')
->setSize(9);


// 평가 시스템 쿼리
$evaluation_system_sql = "
	SELECT
		ev_system.subject,
		ev_system.ev_s_date,
		ev_system.ev_e_date,
		ev_system.ev_u_set_no,
		(SELECT subject FROM evaluation_unit_set WHERE ev_u_set_no = ev_system.ev_u_set_no) AS ev_u_set_subject,
		ev_system.admin_s_no
	FROM evaluation_system ev_system
	WHERE ev_system.ev_no='{$ev_no}'
";
$evaluation_system_result=mysqli_query($my_db,$evaluation_system_sql);
$system_result = mysqli_fetch_array($evaluation_system_result);

$sheet->setCellValue('A1', "[".$system_result['subject']."] 평가결과");
$sheet->setCellValue('A2', "평가기간 : ".$system_result['ev_s_date']." ~ ".$system_result['ev_e_date']);
$sheet->setCellValue('A3', "평가지 종류 : ".$system_result['ev_u_set_subject']);
$admin_s_no = $system_result['admin_s_no'];

if(!(permissionNameCheck($session_permission, "대표") || $session_s_no == '28' || $admin_s_no == $session_s_no)){ // 대표 및 김영경 외 접근불가
	$smarty->display('access_error.html');
	exit;
}

//상단타이틀
$sheet->setCellValue('A4', "피평가자");
$sheet->setCellValue('B4', "평가자");
$sheet->setCellValue('C4', "평가비중(%)");
$sheet->setCellValue('E4', "질문");
$sheet->setCellValue('D5', "구분명");
$sheet->setCellValue('D6', "질문");
$sheet->setCellValue('D7', "평가종류");

//셀크기
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);

//셀병합
$sheet->mergeCells('A4:A7');
$sheet->mergeCells('B4:B7');
$sheet->mergeCells('C4:C7');


// 평가지 항목 쿼리
$evaluation_unit_sql = "
	SELECT
		ev_u.kind,
		ev_u.question,
		ev_u.evaluation_state
	FROM evaluation_unit ev_u
	WHERE ev_u.ev_u_set_no=(SELECT ev_system.ev_u_set_no FROM evaluation_system ev_system WHERE ev_system.ev_no='{$ev_no}')
	   AND ev_u.evaluation_state >= '1' AND ev_u.evaluation_state < '99' AND ev_u.active='1'
	ORDER BY `order` ASC, ev_u_no ASC
";

$cnt_sql = "SELECT count(*) FROM (".$evaluation_unit_sql.") AS cnt";
$cnt_query = mysqli_query($my_db, $cnt_sql);
$cnt_data = mysqli_fetch_array($cnt_query);
$ev_unit_num = $cnt_data[0];

//셀병합
$sheet->mergeCells('E4:'.numToExcelAlpha($ev_unit_num+4).'4');

$ex_i = 5;
$evaluation_unit_result=mysqli_query($my_db,$evaluation_unit_sql);
while($unit_array=mysqli_fetch_array($evaluation_unit_result))
{
	// 첫번째 문자가 = 일경우 '을 삽입
	foreach($unit_array as $key => $value) {
		if($unit_array[$key][0] === '='){
			$unit_array[$key] = "'".$unit_array[$key];
		}
	}

	if($unit_array['evaluation_state'] == '5'){
		$objPHPExcel->getActiveSheet()->getColumnDimension(numToExcelAlpha($ex_i))->setWidth(20);
	}elseif($unit_array['evaluation_state'] == '9'){
		$objPHPExcel->getActiveSheet()->getColumnDimension(numToExcelAlpha($ex_i))->setWidth(10);
	}
	$objPHPExcel->getActiveSheet()->getStyle('E6:'.numToExcelAlpha($ex_i).'7')->getFont()->setSize(7);

	$sheet->setCellValue(numToExcelAlpha($ex_i).'5', $unit_array['kind']);
	$sheet->setCellValue(numToExcelAlpha($ex_i).'6', $unit_array['question']);
	$sheet->setCellValue(numToExcelAlpha($ex_i).'7', $evaluation_state[$unit_array['evaluation_state']][0]);

	$ex_i++;

	$evaluation_unit_list[]=array(
		"kind"=>$unit_array['kind'],
		"question"=>$unit_array['question'],
		"evaluation_state"=>$unit_array['evaluation_state']
	);
}

//STYLE
$objPHPExcel->getActiveSheet()->getStyle('A4:C4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A4:'.numToExcelAlpha($ex_i-1).'7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A4:'.numToExcelAlpha($ex_i-1).'7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFC0C0C0');
$objPHPExcel->getActiveSheet()->getStyle('A4:'.numToExcelAlpha($ex_i-1).'7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A4:'.numToExcelAlpha($ex_i-1).'7')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A4:'.numToExcelAlpha($ex_i-1).'7')->getBorders()->getInside()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

$sheet->getStyle('A4:'.numToExcelAlpha($ex_i-1).'7')->applyFromArray($styleArray); //테두리선

// 리스트 내용
$i			= 7;
$idx_no		= 1;
$ttamount	= 0;
$ex_ev_r_no = "";


// 평가 결과의 평가질문 답변 쿼리
$evaluation_unit_result_sql = "
	SELECT
		ev_result.ev_r_no,
		ev_result.receiver_s_no,
		(SELECT s_name FROM staff s WHERE s.s_no=receiver_s_no) AS receiver_s_name,
		(SELECT s_name FROM staff s WHERE s.s_no=evaluator_s_no) AS evaluator_s_name,
		ev_result.rate,
		ev_result.evaluation_state,
		ev_result.evaluation_value,
		ev_result.choice_items
	FROM evaluation_system_result ev_result
	WHERE ev_result.ev_no='{$ev_no}'
	ORDER BY receiver_s_name ASC, evaluator_s_name ASC, `order` ASC, ev_u_no ASC
";

$ex_i = 5;

$evaluation_unit_result=mysqli_query($my_db,$evaluation_unit_result_sql);
while($unit_result_array=mysqli_fetch_array($evaluation_unit_result))
{
	// 첫번째 문자가 = 일경우 '을 삽입
	foreach($unit_result_array as $key => $value) {
		if($unit_result_array[$key][0] === '='){
			$unit_result_array[$key] = "'".$unit_result_array[$key];
		}
	}
	
	$e_value = "";

	if($unit_result_array['evaluation_state'] == '9'){ // 객관형(복수)
		$e_value = str_replace("||",",",$unit_result_array['evaluation_value']);
	}else{
		$e_value = $unit_result_array['evaluation_value'];
	}

	if($ex_ev_r_no == $unit_result_array['ev_r_no'])
	{
		$sheet->getStyle(numToExcelAlpha($ex_i).$i)->applyFromArray($styleArray); //테두리선

		if($admin_s_no == $session_s_no && $unit_result_array['receiver_s_no'] == $session_s_no){ // 평가담당자의 경우 본인 평가 숨기기
			$sheet->setCellValue(numToExcelAlpha($ex_i++).$i, "-");
		}else{
			$sheet->setCellValue(numToExcelAlpha($ex_i++).$i, $e_value);
		}

	}else{
		$i = $i+1;
		$ex_i = 5;
		$ex_ev_r_no = $unit_result_array['ev_r_no'];

		$sheet->getStyle('A'.$i.':'.numToExcelAlpha($ex_i).$i)->applyFromArray($styleArray); //테두리선

		if($admin_s_no == $session_s_no && $unit_result_array['receiver_s_no'] == $session_s_no){ // 평가담당자의 경우 본인 평가 숨기기
			$sheet
			->setCellValue('A'.$i, $unit_result_array['receiver_s_name'])
			->setCellValue('B'.$i, $unit_result_array['evaluator_s_name'])
			->setCellValue('C'.$i, $unit_result_array['rate'])
			->setCellValue(numToExcelAlpha($ex_i++).$i, "-");
		}else{
			$sheet
			->setCellValue('A'.$i, $unit_result_array['receiver_s_name'])
			->setCellValue('B'.$i, $unit_result_array['evaluator_s_name'])
			->setCellValue('C'.$i, $unit_result_array['rate'])
			->setCellValue(numToExcelAlpha($ex_i++).$i, $e_value);
		}
	}

	$idx_no++;
}

$objPHPExcel->getActiveSheet()->setTitle('평가결과');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
//exit;
// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
$today = date("Ymd");
header('Content-Disposition: attachment;filename="evaluation_result_'.$today.'.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
