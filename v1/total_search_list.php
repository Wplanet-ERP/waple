<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');

$sch_keyword = isset($_GET['sch_keyword']) ? $_GET['sch_keyword'] : "";

$guide_total_sql		= "SELECT count(b_no) FROM (SELECT `bg`.b_no FROM board_guide `bg` WHERE bg.keyword like '%{$sch_keyword}%' AND bg.display='1') AS cnt";
$guide_total_query	= mysqli_query($my_db, $guide_total_sql);
if(!!$guide_total_query)
    $guide_total_result = mysqli_fetch_array($guide_total_query);
$guide_total = $guide_total_result[0];

$work_keyword_total_sql		= "SELECT count(prd_no) FROM (SELECT `p`.prd_no FROM product as p LEFT JOIN kind AS k ON k.k_name_code = p.k_name_code AND k.k_code='product' LEFT JOIN kind AS kp ON kp.k_name_code = k.k_parent AND kp.k_code='product' WHERE p.keyword LIKE '%{$sch_keyword}%' AND p.display='1') AS cnt";
$work_keyword_total_query	= mysqli_query($my_db, $work_keyword_total_sql);
if(!!$work_keyword_total_query)
    $work_keyword_total_result = mysqli_fetch_array($work_keyword_total_query);
$work_keyword_total = $work_keyword_total_result[0];

$asset_total_sql		= "SELECT count(as_no) FROM (SELECT `as`.as_no FROM asset `as` WHERE as.keyword like '%{$sch_keyword}%') AS cnt";
$asset_total_query	= mysqli_query($my_db, $asset_total_sql);
if(!!$asset_total_query)
    $asset_total_result = mysqli_fetch_array($asset_total_query);
$asset_total = $asset_total_result[0];

$nav_total_sql		= "SELECT count(`no`) FROM (SELECT `na`.no FROM navigation `na` WHERE na.nav_name like '%{$sch_keyword}%' AND na.nav_kind='waple' ANd na.display='1') AS cnt";
$nav_total_query	= mysqli_query($my_db, $nav_total_sql);
if(!!$nav_total_query)
    $nav_total_result = mysqli_fetch_array($nav_total_query);
$nav_total = $nav_total_result[0];

$smarty->assign('guide_total', $guide_total);
$smarty->assign('work_keyword_total', $work_keyword_total);
$smarty->assign('asset_total', $asset_total);
$smarty->assign('nav_total', $nav_total);
$smarty->assign('sch_keyword', $sch_keyword);

$smarty->display('total_search_list.html');
?>
