<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/company.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Team.php');

# 프로세스 처리
$process        = isset($_POST['process']) ? $_POST['process'] : "";
$company_model  = Company::Factory();
$company_model->setMainInit('company_influencer', 'ci_no');

if($process == "new_company_influencer")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $ins_data   = array(
        "category"  => (isset($_POST['new_category'])) ? $_POST['new_category'] : "",
        "channel"   => (isset($_POST['new_channel'])) ? $_POST['new_channel'] : "",
        "qty"       => (isset($_POST['new_qty'])) ? addslashes(trim(str_replace(",","",$_POST['new_qty']))) : "",
        "nickname"  => (isset($_POST['new_nickname'])) ? addslashes(trim($_POST['new_nickname'])) : "",
        "url"       => (isset($_POST['new_url'])) ? addslashes(trim($_POST['new_url'])) : "",
        "info"      => (isset($_POST['new_info'])) ? addslashes(trim($_POST['new_info'])) : "",
        "reg_s_no"  => $session_s_no,
        "reg_team"  => $session_team,
        "regdate"   => date("Y-m-d H:i:s"),
    );

    if(!$company_model->insert($ins_data)){
        exit("<script>alert('등록에 실패했습니다');location.href='company_influencer_management.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('등록했습니다');location.href='company_influencer_management.php?{$search_url}';</script>");
    }
}
elseif($process == "modify_data")
{
    $upd_type   = (isset($_POST['type'])) ? $_POST['type'] : "";
    $upd_value  = (isset($_POST['val'])) ? addslashes(trim($_POST['val'])) : "";
    $upd_data = array(
        "ci_no"     => (isset($_POST['no'])) ? $_POST['no'] : "",
        $upd_type   => ($upd_type == "qty") ? str_replace(",", "", $upd_value) : $upd_value,
    );

    if($upd_type == 'notice'){
        $upd_data['notice_date'] = date("Y-m-d H:i:s");
    }

    if ($company_model->update($upd_data)) {
        echo "변경됬습니다.";
    } else{
        echo "변경에 실패했습니다.";
    }
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "176";
$nav_title   = "인플루언서 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

#검색 조건
$add_where          = "1=1";
$sch_ci_no          = isset($_GET['sch_ci_no']) ? $_GET['sch_ci_no'] : "";
$sch_display        = isset($_GET['sch_display']) ? $_GET['sch_display'] : "1";
$sch_category       = isset($_GET['sch_category']) ? $_GET['sch_category'] : "";
$sch_channel        = isset($_GET['sch_channel']) ? $_GET['sch_channel'] : "";
$sch_s_qty          = isset($_GET['sch_s_qty']) ? $_GET['sch_s_qty'] : "";
$sch_e_qty          = isset($_GET['sch_e_qty']) ? $_GET['sch_e_qty'] : "";
$sch_nickname       = isset($_GET['sch_nickname']) ? $_GET['sch_nickname'] : "";
$sch_url            = isset($_GET['sch_url']) ? $_GET['sch_url'] : "";
$sch_info           = isset($_GET['sch_info']) ? $_GET['sch_info'] : "";
$sch_notice         = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";

if(!empty($sch_ci_no)) {
    $add_where  .= " AND ci.ci_no = '{$sch_ci_no}'";
    $smarty->assign("sch_ci_no", $sch_ci_no);
}

if(!empty($sch_display)) {
    $add_where  .= " AND ci.display = '{$sch_display}'";
    $smarty->assign("sch_display", $sch_display);
}

if(!empty($sch_category)) {
    $add_where  .= " AND ci.category = '{$sch_category}'";
    $smarty->assign("$sch_category", $sch_category);
}

if(!empty($sch_channel)) {
    $add_where  .= " AND ci.channel = '{$sch_channel}'";
    $smarty->assign("sch_channel", $sch_channel);
}

if(!empty($sch_s_qty)) {
    $add_where  .= " AND ci.qty >= '{$sch_s_qty}'";
    $smarty->assign("sch_s_qty", $sch_s_qty);
}

if(!empty($sch_e_qty)) {
    $add_where  .= " AND ci.qty <= '{$sch_e_qty}'";
    $smarty->assign("sch_e_qty", $sch_e_qty);
}

if(!empty($sch_nickname)) {
    $add_where  .= " AND ci.nickname LIKE '%{$sch_nickname}%'";
    $smarty->assign("sch_nickname", $sch_nickname);
}

if(!empty($sch_url)) {
    $add_where  .= " AND ci.url LIKE '%{$sch_url}%'";
    $smarty->assign("sch_url", $sch_url);
}

if(!empty($sch_info)) {
    if($sch_info == '1'){
        $add_where  .= " AND (ci.info != '' AND ci.info IS NOT NULL)";
    }elseif($sch_info == '2'){
        $add_where  .= " AND (ci.info = '' OR ci.info IS NULL)";
    }

    $smarty->assign("sch_info", $sch_info);
}

if(!empty($sch_notice)) {
    if($sch_notice == '1'){
        $add_where  .= " AND (ci.notice != '' AND ci.notice IS NOT NULL)";
    }elseif($sch_notice == '2'){
        $add_where  .= " AND (ci.notice = '' OR ci.notice IS NULL)";
    }

    $smarty->assign("sch_notice", $sch_notice);
}

# 전체 게시글 수
$influencer_total_sql     = "SELECT count(`ci_no`) as cnt FROM `company_influencer` as ci WHERE {$add_where}";
$influencer_total_query   = mysqli_query($my_db, $influencer_total_sql);
$influencer_total_result  = mysqli_fetch_assoc($influencer_total_query);
$influencer_total         = isset($influencer_total_result['cnt']) ? $influencer_total_result['cnt'] : 0;

$page 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$page_type	= isset($_GET['ord_page_type']) ?intval($_GET['ord_page_type']) : 20;
$num 		= $page_type;
$offset 	= ($page-1) * $num;
$page_num 	= ceil($influencer_total/$num);

if ($page >= $page_num){$page = $page_num;}
if ($page <= 0){$page = 1;}

$search_url = getenv("QUERY_STRING");
$page_list  = pagelist($page, "company_influencer_management.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $influencer_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 증명서 발급 리스트
$influencer_sql = "
    SELECT 
        *,
        (SELECT s.s_name FROM staff s WHERE s.s_no=ci.reg_s_no) as reg_name,
        (SELECT t.team_name FROM team t WHERE t.team_code=ci.reg_team) as reg_team_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no=ci.notice_reg_s_no) as notice_reg_name
    FROM `company_influencer` as ci
    WHERE {$add_where}
    ORDER BY `ci_no` DESC
    LIMIT {$offset}, {$num}
";
$influencer_query  = mysqli_query($my_db, $influencer_sql);
$influencer_list   = [];
while($influencer = mysqli_fetch_assoc($influencer_query))
{
    $work_list = [];

    if(!empty($influencer['w_no_list'])){
        $work_chk_sql       = "SELECT* FROM work WHERE w_no IN({$influencer['w_no_list']}) AND work_state IN(3,4,5,6)";
        $work_chk_query     = mysqli_query($my_db, $work_chk_sql);
        while($work_chk_result = mysqli_fetch_assoc($work_chk_query)){
            $work_list[] = $work_chk_result['w_no'];
        }
    }

    $influencer['work_list']    = $work_list;
    $influencer['reg_day']      = date("Y-m-d", strtotime($influencer['regdate']));

    $influencer_list[] = $influencer;
}

$smarty->assign("display_option", getDisplayOption());
$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("inf_category_option", getInfCategoryOption());
$smarty->assign("inf_channel_option", getInfChannelOption());
$smarty->assign("influencer_list", $influencer_list);

$smarty->display('company_influencer_management.html');
?>
