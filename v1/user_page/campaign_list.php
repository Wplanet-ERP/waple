<?php
require('../inc/common.php');
require('../inc/data_state.php');


$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

// 접근 권한
if (!$_SESSION["ss_user_b_no"]){
	goto_url("login.php");
	//$smarty->display('access_company_error.html');
	//exit;
}


$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

$proc=(isset($_POST['process']))?$_POST['process']:"";


// 리스트 페이지 쿼리 저장
$save_query=http_build_query($_GET);
$smarty->assign("save_query",$save_query);


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where="1=1";
$add_where_sql1="";
$sch_channel_get=isset($_GET['sch_channel'])?$_GET['sch_channel']:"";
$sch_kind_get=isset($_GET['sch_kind'])?$_GET['sch_kind']:"";
$sch_a_state_get=isset($_GET['sch_a_state'])?$_GET['sch_a_state']:"1";


if(!empty($_SESSION["ss_user_b_url"])) {
	if($_SESSION["ss_user_b_url2"]){
		$add_where.=" AND (a.blog_url LIKE '%{$_SESSION["ss_user_b_url"]}%' OR a.blog_url LIKE '%{$_SESSION["ss_user_b_url2"]}%')";

		$smarty->assign("ss_user_b_url",$_SESSION["ss_user_b_url"]);
		$smarty->assign("ss_user_b_url2",$_SESSION["ss_user_b_url2"]);
	}else{
		$add_where.=" AND a.blog_url LIKE '%{$_SESSION["ss_user_b_url"]}%'";
		$smarty->assign("ss_user_b_url",$_SESSION["ss_user_b_url"]);
	}
}

if(!empty($sch_channel_get)) {
	$add_where_sql1.=" AND channel = '".$sch_channel_get."'";
	$smarty->assign("sch_channel",$sch_channel_get);
}

if(!empty($sch_kind_get)) {
	$add_where_sql1.=" AND kind = '".$sch_kind_get."'";
	$smarty->assign("sch_kind",$sch_kind_get);
}

if(!empty($sch_a_state_get)) {
	$add_where_sql1.=" AND a_state = '".$sch_a_state_get."'";
	$smarty->assign("sch_a_state",$sch_a_state_get);
}


// 정렬순서 토글 & 필드 지정
$add_orderby="";
$order=isset($_GET['od'])?$_GET['od']:"";
$order_type=isset($_GET['by'])?$_GET['by']:"";
$toggle=$order_type?"DESC":"ASC";
$order_field=array('','awd_date','exp_edate','pres_reward');
if($order && $order<6) {
	$add_orderby.="$order_field[$order] $toggle,";
	$smarty->assign("order",$order);
	$smarty->assign("order_type",$order_type);
}

$add_orderby.=" a.p_no DESC";

// 페이에 따른 limit 설정
$pages = isset($_GET['page']) ?intval($_GET['page']) : 1;
$num = 10;
$offset = ($pages-1) * $num;

$application_sql="
			SELECT
				a.a_no,
				a.p_no,
				(SELECT p.p_state FROM promotion p WHERE p.p_no=a.p_no) AS p_state,
				(SELECT p.p_state1 FROM promotion p WHERE p.p_no=a.p_no) AS p_state1,
				(SELECT p.channel FROM promotion p WHERE p.p_no=a.p_no) AS channel,
				(SELECT p.kind FROM promotion p WHERE p.p_no=a.p_no) AS kind,
				(SELECT p.company FROM promotion p WHERE p.p_no=a.p_no) AS company,
				(SELECT p.reg_sdate FROM promotion p WHERE p.p_no=a.p_no) AS reg_sdate,
				(SELECT p.reg_edate FROM promotion p WHERE p.p_no=a.p_no) AS reg_edate,
				(SELECT p.awd_date FROM promotion p WHERE p.p_no=a.p_no) AS awd_date,
				a.a_state,
				a.blog_url,
				(SELECT p.exp_sdate FROM promotion p WHERE p.p_no=a.p_no) AS exp_sdate,
				(SELECT p.exp_edate FROM promotion p WHERE p.p_no=a.p_no) AS exp_edate,
				(SELECT p.pres_reward FROM promotion p WHERE p.p_no=a.p_no) AS pres_reward,
				(SELECT p.pres_money FROM promotion p WHERE p.p_no=a.p_no) AS pres_money,
				(SELECT p.posting_num FROM promotion p WHERE p.p_no=a.p_no) AS posting_num,
				(SELECT p.reg_num FROM promotion p WHERE p.p_no=a.p_no) AS reg_num,
				a.refund,
				a.refund_count,
				(SELECT p.cafe_url FROM promotion p WHERE p.p_no = a.p_no) AS cafe_url
			FROM application a
			WHERE
				$add_where
				$add_where_sql1
			ORDER BY
				$add_orderby
	";
//echo $application_sql; echo "<br>";

// 전체 게시물 수
$cnt_sql = "SELECT count(*) FROM (".$application_sql.") AS cnt";
$cnt_query= mysqli_query($my_db, $cnt_sql);
$cnt_data=mysqli_fetch_array($cnt_query);
$total_num = $cnt_data[0];

$smarty->assign(array(
	"total_num"=>number_format($total_num)
));

$smarty->assign("total_num",$total_num);
$pagenum = ceil($total_num/$num);

// 페이징
if ($pages>=$pagenum){$pages=$pagenum;}
if ($pages<=0){$pages=1;}


// 검색 조건
$search_url = "sch_channel=".$sch_channel_get.
							"&sch_kind=".$sch_kind_get.
							"&sch_a_state=".$sch_a_state_get.
							"&od=".$order.
							"&by=".$order_type;

$smarty->assign("search_url",$search_url);
$page=pagelist($pages, "campaign_list.php", $pagenum, $search_url);
$smarty->assign("pagelist",$page);



// 리스트 쿼리 결과(데이터)
$application_sql .= "LIMIT $offset,$num";

$no_i = $total_num - (($pages-1) * $num);

$application_query=mysqli_query($my_db,$application_sql);
while($application_data=mysqli_fetch_array($application_query)) {

	// channel name
	$channel_name="";
	for($i=0 ; $i < sizeof($promotion_channel) ; $i++){
		if($application_data['channel'] == $promotion_channel[$i][1]){
			$channel_name = $promotion_channel[$i][0];
		}
	}

	// kind name
	$kind_name="";
	for($i=0 ; $i < sizeof($promotion_kind) ; $i++){
		if($application_data['kind'] == $promotion_kind[$i][1]){
			$kind_name = $promotion_kind[$i][0];
		}
	}

	// 요일배열
	$datey=array('일','월','화','수','목','금','토');

	// 완료보고 체크
	$report_sql="
			SELECT
					r.a_no,
					r.post_title,
					r.post_url
			FROM report r
			WHERE
				r.a_no='{$application_data['a_no']}'
		";
	//echo $report_sql; echo "<br>";

	$report_query=mysqli_query($my_db,$report_sql);
	while($report_data=mysqli_fetch_array($report_query)) {
		$report_list[] = array(
			"a_no"=>$report_data['a_no'],
			"post_title"=>$report_data['post_title'],
			"post_url"=>$report_data['post_url']
		);
	}

	$application_list[] = array(
		"no"=>$no_i,
		"a_no"=>$application_data['a_no'],
		"p_no"=>$application_data['p_no'],
		"p_state"=>$application_data['p_state'],
		"p_state_name"=>$promotion_p_state[$application_data['p_state']][0],
		"p_state1"=>$application_data['p_state1'],
		"channel"=>$application_data['channel'],
		"channel_name"=>$channel_name,
		"kind"=>$application_data['kind'],
		"kind_name"=>$kind_name,
		"company"=>$application_data['company'],
		"reg_sdate"=>date("m/d(".$datey[date("w",strtotime($application_data['reg_sdate']))].")",strtotime($application_data['reg_sdate'])),
		"reg_edate"=>date("m/d(".$datey[date("w",strtotime($application_data['reg_edate']))].")",strtotime($application_data['reg_edate'])),
		"awd_date"=>date("m/d(".$datey[date("w",strtotime($application_data['awd_date']))].")",strtotime($application_data['awd_date'])),
		"a_state"=>$application_data['a_state'],
		"a_state_name"=>$application_a_state[$application_data['a_state']][0],
		"blog_url"=>$application_data['blog_url'],
		"exp_sdate"=>date("m/d(".$datey[date("w",strtotime($application_data['exp_sdate']))].")",strtotime($application_data['exp_sdate'])),
		"exp_edate"=>date("m/d(".$datey[date("w",strtotime($application_data['exp_edate']))].")",strtotime($application_data['exp_edate'])),
		"pres_reward"=>date("m/d(".$datey[date("w",strtotime($application_data['pres_reward']))].")",strtotime($application_data['pres_reward'])),
		"pres_money"=>number_format($application_data['pres_money']),
		"pres_num"=>$application_data['posting_num']/$application_data['reg_num'],
		"refund"=>$application_data['refund'],
		"refund_count"=>$application_data['refund_count'],
		"cafe_url"=>$application_data['cafe_url']
	);
	$no_i--;
}
$smarty->assign(array("report_list"=>$report_list));
$smarty->assign(array("application_list"=>$application_list));

$smarty->display('user_page/campaign_list.html');

?>
