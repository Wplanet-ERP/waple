<?php
require('../inc/common.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

if($_SESSION["ss_user_b_no"]) {
	goto_url("campaign_list.php");
}

$process_post=(isset($_POST['process']))?$_POST['process']:"";
if(!!$process_post){ // 로그인 처리 시 수행

	// POST 초기화 & 보안을 위해 다른 변수에 담기
	$add_where="1=1";
	$blog_kind_post=(isset($_POST['blog_kind']))?$_POST['blog_kind']:"";
	$blog_id_post=(isset($_POST['blog_id']))?$_POST['blog_id']:"";
	$name_post=(isset($_POST['name']))?$_POST['name']:"";
	$hp4_post=(isset($_POST['hp4']))?$_POST['hp4']:"";

	if(!empty($blog_kind_post) && !empty($blog_id_post)) {
		if($blog_kind_post == "naver"){
			$blog_url = "blog.naver.com/{$blog_id_post}";
			$blog_url2 = "{$blog_id_post}.blog.me";
			$add_where.=" AND (b.blog_url LIKE '%".$blog_url."%' OR b.blog_url LIKE '%".$blog_url2."%')";
		}elseif($blog_kind_post == "etc"){
		}
	}
	if(!empty($name_post)) {
		$add_where.=" AND b.username = '".$name_post."'";
	}
	if(!empty($hp4_post)) {
		$add_where.=" AND SUBSTRING_INDEX(b.hp,'-',-1) = '".$hp4_post."'";
	}

	sleep(1);
	$blog_sql="
			SELECT b.b_no,
					b.blog_url
			FROM blog b
			WHERE
				$add_where
		";
	//echo $blog_sql; echo "<br>";
	//exit;

	$blog_sql_query= mysqli_query($my_db, $blog_sql);
	$blog_sql_data=mysqli_fetch_array($blog_sql_query);
	$b_no = $blog_sql_data['b_no'];
	$blog_url = trim($blog_sql_data['blog_url']);

	$blog_url = str_replace("http://", "", $blog_url);
	$blog_url = str_replace("https://", "", $blog_url);

	if(substr($blog_url,-1) == '/'){ // 마지막 글자가 / 일경우
		$blog_url = substr($blog_url , 0, -1); //마지막글자 제거
	}

	if($blog_sql_data['b_no']){
		$_SESSION["ss_user_b_no"] = $blog_sql_data['b_no'];
		$_SESSION["ss_user_b_url"] = $blog_url;

		if($blog_sql_data=mysqli_fetch_array($blog_sql_query)){ // blog.me 추가 확인

			$blog_url2 = trim($blog_sql_data['blog_url']);

			$blog_url2 = str_replace("http://", "", $blog_url2);
			$blog_url2 = str_replace("https://", "", $blog_url2);

			if(substr($blog_url2,-1) == '/'){ // 마지막 글자가 / 일경우
				$blog_url2 = substr($blog_url2 , 0, -1); //마지막글자 제거
			}

			$_SESSION["ss_user_b_url2"] = $blog_url2;
		}

		goto_url("campaign_list.php");
	}else{
		alert("신청한 내역을 찾을 수 없습니다. 정확한 정보를 입력해주세요!","login.php");
	}

}
$smarty->assign("nobg","style='background:none'");
$smarty->display('user_page/login.html');
?>
