<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/data_state.php');
require('../inc/helper/_navigation.php');
require('../inc/model/MyQuick.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);


// 접근 제한
if (!$session_s_no){ // 외주업체 로그인 시
	$smarty->display('access_company_error.html');
	exit;
}

//Freelancer로 로그인 한 경우
if($session_staff_state == '2'){
    $smarty->display('access_error.html');
    exit;
}

# Navigation & My Quick
$nav_prd_no  = "121";
$nav_title   = "매출 통계";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기

// [사업자]
$sch_my_c_no_get = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "1";
$add_my_c_no_where="";
if (!empty($sch_my_c_no_get)) {
	$smarty->assign("sch_my_c_no", $sch_my_c_no_get);

	if($sch_my_c_no_get == "wise"){
		$add_my_c_no_where .= " AND (dp.my_c_no = '1' OR dp.my_c_no = '2' OR dp.my_c_no = '5' OR dp.my_c_no = '6')";
		$smarty->assign("sch_my_c_name","전체(미디어커머스 외)");
	}else{
		$add_my_c_no_where .= " AND dp.my_c_no = '" . $sch_my_c_no_get . "'";

		for($i = 0 ; $i < count($my_company_list) ; $i++) {
			if($my_company_list[$i]['my_c_no'] == $sch_my_c_no_get){
				$smarty->assign("sch_my_c_name", $my_company_list[$i]['c_name']);
				break;
			}
		}
	}
}


// [부서 > 담당자]
$sch_s_name   = "";
$sch_team_get = isset($_GET['sch_team'])?$_GET['sch_team']:"";
$sch_s_no_get = isset($_GET['sch_s_no'])?$_GET['sch_s_no']:"";

$sch_team_code_where = "";
if(!empty($sch_team_get) && $sch_team_get != "all"){
    $sch_team_code_where = getTeamWhere($my_db, $sch_team_get);
    $sch_s_name .= isset($sch_team_name_list[$sch_team_get]) ? $sch_team_name_list[$sch_team_get] : "";
}else{
    $sch_s_name .= " 전체";
}

$staff_list = [];
$staff_name = "";
if($sch_team_code_where)
{
    $staff_team_where 		= [];
    $staff_team_where_list 	= [];

    $sch_team_code_list_val = explode(",", $sch_team_code_where);

    foreach($sch_team_code_list_val as $sch_team_code){
        $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
    }

    if($staff_team_where_list)
    {
        $staff_team_where = implode(" OR ", $staff_team_where_list);
        $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
        $staff_result=mysqli_query($my_db,$staff_sql);
        while($staff=mysqli_fetch_array($staff_result)) {
            $staff_list[]=array(
                "s_no"=>trim($staff['s_no']),
                "s_name"=>trim($staff['s_name'])
            );

            if(!!$sch_s_no_get && $sch_s_no_get == $staff['s_no']){
                $staff_name = $staff['s_name'];
            }
        }
    }
}

if(!!$sch_s_no_get && !empty($staff_name)){
    $sch_s_name .= " > ".$staff_name;
}else{
    $sch_s_name .= " > 전체";
}

$smarty->assign("sch_s_name", $sch_s_name);
$smarty->assign("sch_team", $sch_team_get);
$smarty->assign("sch_s_no", $sch_s_no_get);
$smarty->assign("staff_list", $staff_list);
$smarty->assign("sch_team_list", $sch_team_name_list);


// [조회기간]
$sch_s_date_get = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date("Y-m", strtotime("-6 month")); // 월간
$sch_s_date2_get = isset($_GET['sch_s_date2']) ? $_GET['sch_s_date2'] : date("Y-m-d", strtotime("-7 day")); // 일간

$smarty->assign("sch_s_date", $sch_s_date_get);
$smarty->assign("sch_s_date2", $sch_s_date2_get);

$sch_e_date_get = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date("Y-m"); // 월간
$sch_e_date2_get = isset($_GET['sch_e_date2']) ? $_GET['sch_e_date2'] : date("Y-m-d"); // 일간
$smarty->assign("sch_e_date", $sch_e_date_get);
$smarty->assign("sch_e_date2", $sch_e_date2_get);


// 부서별 직원 목록 가져오기
if($sch_team_get){ // 상품 선택 없이 상품 그룹2로 검색시 상품 가져오기
	$staff_sql="
			SELECT
					s.s_no, s.s_name
			FROM staff s
			WHERE
					team='".$sch_team_get."'
			";

	$staff_result=mysqli_query($my_db,$staff_sql);
	while($staff=mysqli_fetch_array($staff_result)) {
		$staff_list[]=array(
			"s_no"=>trim($staff['s_no']),
			"s_name"=>trim($staff['s_name'])
		);
	}
	$smarty->assign("staff_list",$staff_list);
}


// 상태 조건
$add_method_where = " dp.display = '1'";

// 조회 기간
$add_date_where = " AND DATE_FORMAT(dp.dp_date,'%Y-%m') BETWEEN '{$sch_s_date_get}' AND '{$sch_e_date_get}' ";

// 매출 상태 조건
$add_where = " AND ((dp.dp_method = '2' AND (dp.dp_state = '2' OR dp.dp_state = '3' OR dp.dp_state = '4')) "; // 현금일 경우 입금완료 or 부분입금
$add_where .= " OR (dp.dp_method = '1' AND (dp.dp_state = '1' OR dp.dp_state = '2' OR dp.dp_state = '4')) "; // 카드일 경우 입금대기 or 입금완료
$add_where .= " OR (dp.dp_method = '3' AND dp.dp_state = '2')) "; // 월정산 경우 입금완료


// 담당자 기준
$add_team_select = "";
$add_s_no_select = "";
$add_team_where = "";

$add_team_select .= " dp.team AS select_team, (SELECT t.display FROM team t WHERE t.team_code=dp.team) AS t_display,";
$add_s_no_select .= " dp.s_no AS select_s_no,";
$add_s_no_select .= " (SELECT s.s_name FROM staff s WHERE s.s_no = dp.s_no) AS select_s_name, (SELECT s.staff_state FROM staff s WHERE s.s_no=dp.s_no) AS s_display,";
if (!!$sch_s_no_get) { // 담당자 설정된 경우
	$add_team_where .= " AND dp.s_no = '$sch_s_no_get'";
}else{
    if(!!$sch_team_code_where){ // 부서 설정, 담당자 전체의 경우
        $add_team_where.=" AND dp.team IN ({$sch_team_code_where})";
    }
}

// x축 이름 (data가 0인 경우 대비)
$x_ym 		 = date("Y-m", strtotime($sch_s_date_get));
$x_ym_end 	 = date("Y-m", strtotime($sch_e_date_get));
$x_name_list = [];
while( $x_ym <= $x_ym_end ){
	$x_name_list[] = $x_ym;
	$x_ym = date("Y-m", strtotime("+1 month", strtotime($x_ym)));
}
$smarty->assign("x_name_list", $x_name_list);


/* ---------- chart_01 매출 통계 [Start] ---------- */
$chart_01_sql = "
	SELECT
		DATE_FORMAT(dp.dp_date,'%Y-%m') AS result_ym,
		SUM(dp_money) AS dp_money
	FROM deposit dp
	WHERE
	 	{$add_method_where}
		{$add_date_where}
		{$add_where}
		{$add_my_c_no_where}
		{$add_team_where}
	GROUP BY result_ym
	ORDER BY result_ym ASC
";
$chart_01_sql_sum = "
	SELECT
		SUM(result_sql.dp_money) AS dp_money_sum
	FROM ({$chart_01_sql}) AS	result_sql
	WHERE 1=1
";
$chart_01_sum_result = mysqli_query($my_db, $chart_01_sql_sum);
$chart_01_sum_array  = mysqli_fetch_array($chart_01_sum_result);

$dp_money_sum = "";
$dp_money_sum = $chart_01_sum_array['dp_money_sum'];

$chart_01_result = mysqli_query($my_db, $chart_01_sql);
$chart_01		 = [];
while($chart_01_array = mysqli_fetch_array($chart_01_result)){
	$chart_01[] = array(
		"result_ym"			 => $chart_01_array['result_ym'],
		"result_data_dp"	 => floor($chart_01_array['dp_money']/1000),
		"result_data_dp_sum" => floor($dp_money_sum/1000)
	);
}

$smarty->assign("chart_01", $chart_01);
/* ---------- chart_01 매출 통계[End] ---------- */


/* ---------- chart_02 부서별 매출 통계 [Start] ---------- */
$chart_02_sql = "
	SELECT
		{$add_team_select}
		DATE_FORMAT(dp.dp_date,'%Y-%m') AS result_ym,
		SUM(dp_money) AS dp_money
	FROM deposit dp
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_my_c_no_where}
		{$add_team_where}
	GROUP BY select_team, result_ym
	ORDER BY result_ym ASC
";
$chart_02_result 	= mysqli_query($my_db, $chart_02_sql);
$chart_02		 	= [];
$chart_02_display 	= [];
while($chart_02_array = mysqli_fetch_array($chart_02_result)){
	$chart_02[] = array(
		"team"			=> $chart_02_array['select_team'],
		"result_ym"		=> $chart_02_array['result_ym'],
		"result_data_dp"=> floor($chart_02_array['dp_money']/1000)
	);

    $chart_02_display[$chart_02_array['select_team']] = ($chart_02_array['t_display'] == '1') ? "Y" : "N";
}
$smarty->assign("chart_02", $chart_02);
$smarty->assign("chart_02_display", $chart_02_display);


$chart_02_sql_sum = "
	SELECT
		result_sql.select_team,
		(SELECT team_name FROM team WHERE team_code = result_sql.select_team) AS team_name,
		SUM(result_sql.dp_money) AS dp_money_sum
	FROM ({$chart_02_sql}) AS	result_sql
	WHERE 1=1
	GROUP BY result_sql.select_team
";
$chart_02_sum_result = mysqli_query($my_db, $chart_02_sql_sum);
while($chart_02_sum_array = mysqli_fetch_array($chart_02_sum_result)){
    $team_name = empty($chart_02_sum_array['select_team']) ? "(알수없음)" : $chart_02_sum_array['team_name'];
	$chart_02_sum[] = array(
		"team"=>$chart_02_sum_array['select_team'],
		"team_name"=>$team_name,
		"result_data_dp_sum"=>floor($chart_02_sum_array['dp_money_sum']/1000)
	);
}

$chart_02_sum = array_sort($chart_02_sum, "result_data_dp_sum"); // result_data_dp_sum 순으로 변경 (오름차순)
$chart_02_title_list = "";
if(!empty($chart_02_sum))
	$chart_02_title_list = array_reverse($chart_02_sum); // result_data_dp_sum 순으로 변경 (내림차순)

$chart_02_title_list = super_unique($chart_02_title_list,"team"); // s_no 중복 제거 (오름차순)
$smarty->assign("chart_02_title_list",$chart_02_title_list);


if(!!$chart_02){
	// x좌표 항목에 관한 합계 구하기
	$chart_02_result_ym_dp_sum = "";
	$chart_02_result_ym_wd_sum = "";
	for($i=0 ; $i < sizeof($x_name_list) ; $i++){
		$dp_sum_tmp = 0;
		$wd_sum_tmp = 0;
		foreach($chart_02 as $key => $value){
			if($value['result_ym'] == $x_name_list[$i]){
				$dp_sum_tmp += $value['result_data_dp'];
				$wd_sum_tmp += $value['result_data_wd'];
			}
		}

		$chart_02_result_ym_sum[] = array(
			"result_ym"=>$value['result_ym'],
			"result_ym_dp_sum"=>$dp_sum_tmp,
			"result_ym_wd_sum"=>$wd_sum_tmp
		);
	}

	$smarty->assign("chart_02_result_ym_sum", $chart_02_result_ym_sum);
}
/* ---------- chart_02 부서별 매출 통계 [End] ---------- */


/* ---------- chart_03 담당자별 매출 통계 [Start] ---------- */
$chart_03_sql = "
	SELECT
		{$add_s_no_select}
		DATE_FORMAT(dp.dp_date,'%Y-%m') AS result_ym,
		SUM(dp_money) AS dp_money
	FROM deposit dp
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_my_c_no_where}
		{$add_team_where}
	GROUP BY select_s_no, result_ym
	ORDER BY result_ym ASC
";

$chart_03_result  = mysqli_query($my_db, $chart_03_sql);
$chart_03_display = [];
while($chart_03_array = mysqli_fetch_array($chart_03_result)){
	$chart_03[] = array(
		"s_no"=>$chart_03_array['select_s_no'],
		"s_name"=>$chart_03_array['select_s_name'],
		"result_ym"=>$chart_03_array['result_ym'],
		"result_data_dp"=>floor($chart_03_array['dp_money']/1000)
	);

    $chart_03_display[$chart_03_array['select_s_no']]   = (empty($chart_03_array['s_display']) || $chart_03_array['s_display'] == '3') ? "N" : "Y";
}

$smarty->assign("chart_03", $chart_03);
$smarty->assign("chart_03_display", $chart_03_display);


$chart_03_sql_sum = "
	SELECT
		result_sql.select_s_no,
		result_sql.select_s_name,
		SUM(result_sql.dp_money) AS dp_money_sum
	FROM ({$chart_03_sql}) AS	result_sql
	WHERE 1=1
	GROUP BY result_sql.select_s_no
";
$chart_03_sum_result = mysqli_query($my_db, $chart_03_sql_sum);
while($chart_03_sum_array = mysqli_fetch_array($chart_03_sum_result)){
    $s_name = empty($chart_03_sum_array['select_s_name']) ? "(알수없음)" : $chart_03_sum_array['select_s_name'];
	$chart_03_sum[] = array(
		"s_no"=>$chart_03_sum_array['select_s_no'],
		"s_name"=>$s_name,
		"result_data_dp_sum"=>floor($chart_03_sum_array['dp_money_sum']/1000)
	);
}

$chart_03_sum = array_sort($chart_03_sum, "result_data_dp_sum"); // result_data_dp_sum 순으로 변경 (오름차순)
$chart_03_title_list = [];
if(!empty($chart_03_sum))
	$chart_03_title_list = array_reverse($chart_03_sum); // result_data_dp_sum 순으로 변경 (내림차순)

$chart_03_title_list = super_unique($chart_03_title_list,"s_no"); // s_no 중복 제거 (오름차순)
$smarty->assign("chart_03_title_list",$chart_03_title_list);


if(!!$chart_03){
	// x좌표 항목에 관한 합계 구하기
	$chart_03_result_ym_dp_sum = "";
	$chart_03_result_ym_wd_sum = "";
	for($i=0 ; $i < sizeof($x_name_list) ; $i++){
		$dp_sum_tmp = 0;
		$wd_sum_tmp = 0;
		foreach($chart_03 as $key => $value){
			if($value['result_ym'] == $x_name_list[$i]){
				$dp_sum_tmp += $value['result_data_dp'];
				$wd_sum_tmp += $value['result_data_wd'];
			}
		}

		$chart_03_result_ym_sum[] = array(
			"result_ym"=>$value['result_ym'],
			"result_ym_dp_sum"=>$dp_sum_tmp,
			"result_ym_wd_sum"=>$wd_sum_tmp
		);
	}

	$smarty->assign("chart_03_result_ym_sum",$chart_03_result_ym_sum);
}
/* ---------- chart_03 담당자별 매출 통계 [End] ---------- */


$smarty->display('stats/wise_sales.html');

?>
