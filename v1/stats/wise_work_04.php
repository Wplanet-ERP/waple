<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/stats.php');
require('../inc/model/MyQuick.php');

$dir_location = "../";
$smarty->assign("dir_location", $dir_location);

# Navigation & My Quick
$nav_prd_no  = "132";
$nav_title   = "업무 기여도(업무처리평가)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
// [상품(업무) 종류]
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);

$prd_g1_list = $prd_g2_list = $prd_g3_list = [];

foreach($work_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}

$prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
$sch_prd_list = isset($work_prd_list[$sch_prd_g2]) ? $work_prd_list[$sch_prd_g2] : [];
$sch_prd_name = isset($work_name_list[$sch_prd_g1]) ? $work_name_list[$sch_prd_g1] : "전체";
$sch_prd_name .= isset($work_name_list[$sch_prd_g2]) ? " > ".$work_name_list[$sch_prd_g2] : " > 전체";
$sch_prd_name .= isset($work_prd_name_list[$sch_prd]) ? " > ".$work_prd_name_list[$sch_prd] : " > 전체";

$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);
$smarty->assign("sch_prd_name", $sch_prd_name);


// [조회 단위] & [조회 기간]
$sch_kind		    = isset($_GET['sch_kind']) ? $_GET['sch_kind'] : "2";
$sch_kind_type      = "month";
$today_s_w		    = date('w')-1;
$sch_year_s_date 	= isset($_GET['sch_year_s_date']) ? $_GET['sch_year_s_date'] : date("Y", strtotime("-3 years")); // 연간
$sch_year_e_date 	= isset($_GET['sch_year_e_date']) ? $_GET['sch_year_e_date'] : date("Y"); // 연간
$sch_mon_s_date 	= isset($_GET['sch_mon_s_date']) ? $_GET['sch_mon_s_date'] : date("Y-m", strtotime("-6 months")); // 월간
$sch_mon_e_date 	= isset($_GET['sch_mon_e_date']) ? $_GET['sch_mon_e_date'] : date("Y-m"); // 월간
$sch_week_s_date 	= isset($_GET['sch_week_s_date']) ? $_GET['sch_week_s_date'] : date("Y-m-d",strtotime("-{$today_s_w} day")); // 주간
$sch_week_e_date 	= isset($_GET['sch_week_e_date']) ? $_GET['sch_week_e_date'] : date("Y-m-d", strtotime("{$sch_week_s_date} +6 day")); // 주간
$sch_s_date 	    = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date("Y-m-d", strtotime("-6 day")); // 일간
$sch_e_date 	    = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date("Y-m-d"); // 일간

switch ($sch_kind){
    case "1":
        $sch_kind_type = "year";
        break;
    case "2":
        $sch_kind_type = "month";
        break;
    case "3":
        $sch_kind_type = "week";
        break;
    case "4":
        $sch_kind_type = "day";
        break;
}

$smarty->assign("sch_kind", $sch_kind);
$smarty->assign("sch_kind_type", $sch_kind_type);
$smarty->assign("sch_year_s_date", $sch_year_s_date);
$smarty->assign("sch_year_e_date", $sch_year_e_date);
$smarty->assign("sch_mon_s_date", $sch_mon_s_date);
$smarty->assign("sch_mon_e_date", $sch_mon_e_date);
$smarty->assign("sch_week_s_date", $sch_week_s_date);
$smarty->assign("sch_week_e_date", $sch_week_e_date);
$smarty->assign("sch_s_date", $sch_s_date);
$smarty->assign("sch_e_date", $sch_e_date);

// [담당자 기준] : 팀, 담당자
$sch_team	= isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no	= isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";

$sch_team_code_where = "";
if(!empty($sch_team) && $sch_team != "all"){
    $sch_team_code_where = getTeamWhere($my_db, $sch_team);
    $sch_s_name .= isset($sch_team_name_list[$sch_team]) ? $sch_team_name_list[$sch_team] : "";
}else{
    $sch_s_name .= " 전체";
}

$staff_list = [];
$staff_name = "";
if($sch_team_code_where)
{
    $staff_team_where 		= "";
    $staff_team_where_list 	= [];

    $sch_team_code_list_val = explode(",", $sch_team_code_where);

    foreach($sch_team_code_list_val as $sch_team_code){
        $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
    }

    if($staff_team_where_list)
    {
        $staff_team_where = implode(" OR ", $staff_team_where_list);
        $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
        $staff_result=mysqli_query($my_db,$staff_sql);
        while($staff=mysqli_fetch_array($staff_result)) {
            $staff_list[]=array(
                "s_no"=>trim($staff['s_no']),
                "s_name"=>trim($staff['s_name'])
            );

            if(!!$sch_s_no && $sch_s_no == $staff['s_no']){
                $staff_name = $staff['s_name'];
            }
        }
    }
}

if(!!$sch_s_no && !empty($staff_name)){
    $sch_s_name .= " > ".$staff_name;
}else{
    $sch_s_name .= " > 전체";
}

$smarty->assign("sch_s_name", $sch_s_name);
$smarty->assign("sch_s_name_table", str_replace(">", "", $sch_s_name));
$smarty->assign("sch_team", $sch_team);
$smarty->assign("sch_s_no", $sch_s_no);
$smarty->assign("staff_list", $staff_list);
$smarty->assign("sch_team_list", $sch_team_name_list);


// 상태 조건
$add_method_where = " w.work_state = '6' AND (w.evaluation IS NOT NULL OR w.evaluation <> '0')"; // 진행완료

// 상품(업무) 종류
$add_prd_where = "";
if (!empty($sch_prd) && $sch_prd != "0") { // 상품
	$add_prd_where .= " AND w.prd_no='".$sch_prd."'";
}else{
	if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_method_where .= " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.k_name_code='{$sch_prd_g2}')";
	}elseif($sch_prd_g1){ // 상품 선택 없이 번째 그룹까지 설정한 경우
        $add_method_where .= " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
	}
}

// 업무처리 담당자
$add_team_where = "";
if (!!$sch_s_no) { // 업무처리 담당자 설정된 경우
	if($sch_s_no == 'null'){
		$add_team_where.=" AND (w.task_run_s_no IS NULL OR w.task_run_s_no = '0')";
	}else{
		$add_team_where.=" AND (w.task_run_s_no = '$sch_s_no')";
	}
}else{
	if(!!$sch_team_code_where){ // 부서 설정, 업무처리 담당자 전체의 경우
		$add_team_where.=" AND (w.task_run_team IN ({$sch_team_code_where}))";
	}
}

// 기본 베이스 Array 만들기($x_name_list: 그래프 X 부분 타이틀, $default_chart_list: 기본 차트 날짜별, $default_chart_sum_list: 기본 차트 합)
$x_name_list 			= [];
$default_chart_list 	= [];
$default_chart_sum_list = [];

// 조회 기간 : add_date_where, date_column
$all_date_where 		= "";
$all_date_key			= "";
$all_date_title 		= "";
$add_date_where 		= "";
$add_date_column 		= "";

if($sch_kind == '1') //연간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_year_s_date}' AND '{$sch_year_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y')";

    $add_date_where  = " AND DATE_FORMAT(task_run_regdate, '%Y') BETWEEN '{$sch_year_s_date}' AND '{$sch_year_e_date}' ";
    $add_date_column = "DATE_FORMAT(task_run_regdate, '%Y')";
}
elseif($sch_kind == '2') //월간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_mon_s_date}' AND '{$sch_mon_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m')";

    $add_date_where  = " AND DATE_FORMAT(task_run_regdate, '%Y-%m') BETWEEN '{$sch_mon_s_date}' AND '{$sch_mon_e_date}' ";
    $add_date_column = "DATE_FORMAT(task_run_regdate, '%Y%m')";
}
elseif($sch_kind == '3') //주간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_week_s_date}' AND '{$sch_week_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where  = " AND DATE_FORMAT(task_run_regdate, '%Y-%m-%d') BETWEEN '{$sch_week_s_date}' AND '{$sch_week_e_date}' ";
    $add_date_column = "DATE_FORMAT(DATE_SUB(w.task_run_regdate, INTERVAL(IF(DAYOFWEEK(w.task_run_regdate)=1,8,DAYOFWEEK(w.task_run_regdate))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";

    $add_date_where  = " AND DATE_FORMAT(task_run_regdate, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_date_column = "DATE_FORMAT(task_run_regdate, '%Y%m%d')";
}

$all_date_sql = "
	SELECT
 		$all_date_key as chart_key,
		$all_date_title as chart_title
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	GROUP BY chart_key
	ORDER BY chart_key
";

if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    while($date = mysqli_fetch_array($all_date_query))
    {
        $default_chart_list[$date['chart_key']] = [];
        $default_chart_sum_list[$date['chart_key']] = array(
            'count'		=> 0,
            'result'	=> 0
        );
        $x_name_list[$date['chart_key']] = $date['chart_title'];
    }
}
ksort($x_name_list);
$smarty->assign('x_name_title', $x_name_list);
$smarty->assign('x_name_list', "'".implode("','", $x_name_list)."'");


/* ---------- chart_01 상품별 평가점수 [Start] ---------- */
$column_select 		= ""; // 차트 데이터
$group_by 			= ""; // 차트 데이터 DB GROUP
$order_by 			= ""; // 차트 데이터 정렬
$prd_chart_title 	= $sch_prd ? "별점" : ($sch_prd_g2 ? "상품명" : ($sch_prd_g1 ? "그룹2" : "그룹1"));

if(!$sch_prd_g1){ // 상품 그룹1가 전체인 경우
	$column_select = "
			(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS result_name_id,
			(SELECT k_name FROM kind WHERE k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no))) AS result_name,
			ROUND(AVG(w.evaluation),2) AS result_data,
	";

	$group_by = "result_name_id,";
	$order_by = "result_ym ASC, result_data DESC";
}elseif(!!$sch_prd_g1 && !$sch_prd_g2){ // 상품 그룹1 설정, 상품 그룹2가 전체인 경우
	$column_select = "
			(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no) AS result_name_id,
			(SELECT k_name FROM kind WHERE k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS result_name,
			ROUND(AVG(w.evaluation),2) AS result_data,
	";

	$group_by = "result_name_id,";
	$order_by = "result_ym ASC, result_data DESC";
}elseif(!!$sch_prd_g2 && !$sch_prd){ // 상품 그룹2 설정, 상품 전체인 경우
	$column_select = "
			w.prd_no AS result_name_id,
			(SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no) AS result_name,
			ROUND(AVG(w.evaluation),2) AS result_data,
	";

	$group_by = "result_name_id,";
	$order_by = "result_ym ASC, result_data DESC";
}elseif(!!$sch_prd_g2 && !!$sch_prd){ // 상품 그룹2 설정, 상품 설정인 경우
	$column_select = "
			w.evaluation AS result_name_id,
			(SELECT IF(w.evaluation = 2,'☆☆☆☆★',IF(w.evaluation = 4,'☆☆☆★★',IF(w.evaluation = 6,'☆☆★★★',IF(w.evaluation = 8,'☆★★★★',IF(w.evaluation = 10,'★★★★★','')))))) AS result_name,
			COUNT(w.evaluation) AS result_data,
	";

	$group_by = "w.evaluation,";
	$order_by = "result_ym ASC, result_name_id DESC";
}

$chart_01_sql = "
	SELECT
		{$column_select}
		{$add_date_column} AS result_ym,
		COUNT(w.evaluation) AS evaluation_count,
		COUNT(*) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_prd_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY
		{$group_by}
		result_ym
	ORDER BY
		{$order_by}
";

$chart_01			  = $default_chart_list;
$chart_01_day_sum	  = $default_chart_sum_list;
$chart_01_day_avg	  = $default_chart_sum_list;
$chart_01_row_sum	  = [];
$chart_01_row_avg	  = [];
$chart_01_row_name 	  = [];

$chart_01_query = mysqli_query($my_db, $chart_01_sql);
while($chart_01_array = mysqli_fetch_array($chart_01_query))
{
    $chart_01[$chart_01_array['result_ym']][$chart_01_array['result_name_id']] = array(
    	'result_data' 	=> $chart_01_array['result_data'],
		'work_count' 	=> $chart_01_array['work_count']
	);
    $chart_01_day_sum[$chart_01_array['result_ym']]['count'] 		+= $chart_01_array['work_count'];
    $chart_01_day_sum[$chart_01_array['result_ym']]['result'] 		+= ($chart_01_array['result_data'] * $chart_01_array['work_count']);
    $chart_01_row_sum[$chart_01_array['result_name_id']]['count'] 	+= $chart_01_array['work_count'];
    $chart_01_row_sum[$chart_01_array['result_name_id']]['result'] 	+= ($chart_01_array['result_data'] * $chart_01_array['work_count']);
    $chart_01_row_name[$chart_01_array['result_name_id']] 			 = $chart_01_array['result_name'] ? $chart_01_array['result_name'] : "(NULL)";
}

foreach($chart_01_day_sum as $day => $day_sum) {
    $chart_01_day_avg[$day]["result"] = (isset($day_sum['count']) && !empty($day_sum['count'])) ? round($day_sum['result']/$day_sum['count'], 2) : '-';
    $chart_01_day_avg[$day]["count"] = (isset($day_sum['count']) && !empty($day_sum['count'])) ? $day_sum['count'] : '-';
}

foreach($chart_01_row_sum as $row => $row_sum) {
    $chart_01_row_avg[$row]["result"] = (isset($row_sum['count']) && !empty($row_sum['count'])) ? round($row_sum['result'] / $row_sum['count'], 2) : '-';
    $chart_01_row_avg[$row]["count"] = (isset($row_sum['count']) && !empty($row_sum['count'])) ? $row_sum['count'] : '-';
}


$chart_01_t_sql = "
    SELECT
        {$column_select}
        {$add_date_column} AS result_ym,
        COUNT(w.evaluation) AS evaluation_count,
        COUNT(*) AS work_count
    FROM `work` w
    WHERE
        {$add_method_where}
        {$add_prd_where}
        {$add_date_where}
    GROUP BY
        result_ym
    ORDER BY
        {$order_by}
";

$chart_01_t_day_avg = $default_chart_sum_list;
$chart_01_t_query = mysqli_query($my_db, $chart_01_t_sql);
while($chart_01_t_array = mysqli_fetch_array($chart_01_t_query))
{
    $chart_01_t_day_avg[$chart_01_t_array['result_ym']]['count']  += $chart_01_t_array['work_count'];
    $chart_01_t_day_avg[$chart_01_t_array['result_ym']]['result'] += ($chart_01_t_array['result_data'] * $chart_01_t_array['work_count']);
}

arsort($chart_01_row_avg);
ksort($chart_01);
ksort($chart_01_t_day_avg);

$smarty->assign("chart_01", $chart_01);
$smarty->assign("chart_01_day_avg", $chart_01_day_avg);
$smarty->assign("chart_01_row_avg", $chart_01_row_avg);
$smarty->assign("chart_01_row_name", $chart_01_row_name);
$smarty->assign("chart_01_t_day_avg", $chart_01_t_day_avg);
/* ---------- chart_01 상품별 평가점수 [End] ---------- */



/* ---------- chart_02 부서/담당자별 평가점수 [Start] ---------- */
$column_select 		= ""; // 차트 데이터
$group_by 			= ""; // 차트 데이터 DB GROUP
$order_by 			= ""; // 차트 데이터 정렬
$staff_chart_title 	= $sch_s_no ? "별점" : ($sch_team ? "담당자명" : "부서명"); // 차트 종류 [1:부서 전체(부서별 차트), 2:담당자 전체(담당자별 차트), 3: 담당자 설정(담당자 세부 차트)]

if(!$sch_team){ // 부서 전체인 경우
	$column_select = "
			w.task_run_team AS result_name_id,
			(SELECT t.team_name FROM team t WHERE t.team_code=w.task_run_team) AS result_name,
			(SELECT t.display FROM team t WHERE t.team_code=w.task_run_team) AS result_display,
			ROUND(AVG(w.evaluation),2) AS result_data,
	";

	$group_by = "result_name_id,";
	$order_by = "result_ym ASC, result_data DESC";
}elseif(!!$sch_team && !$sch_s_no){ // 부서 설정, 담당자 전체인 경우
	$column_select = "
			w.task_run_s_no AS result_name_id,
			(SELECT s.s_name FROM staff s WHERE s.s_no=w.task_run_s_no) AS result_name,
			(SELECT s.staff_state FROM staff s WHERE s.s_no=w.task_run_s_no) AS result_display,
			ROUND(AVG(w.evaluation),2) AS result_data,
	";

	$group_by = "result_name_id,";
	$order_by = "result_ym ASC, result_data DESC";
}elseif(!!$sch_team && !!$sch_s_no){ // 부서 설정, 담당자 설정인 경우
	$column_select = "
			w.evaluation AS result_name_id,
			(SELECT IF(w.evaluation = 2,'☆☆☆☆★',IF(w.evaluation = 4,'☆☆☆★★',IF(w.evaluation = 6,'☆☆★★★',IF(w.evaluation = 8,'☆★★★★',IF(w.evaluation = 10,'★★★★★','')))))) AS result_name,
			COUNT(w.evaluation) AS result_data,
	";

	$group_by = "w.evaluation,";
	$order_by = "result_ym ASC, result_name_id DESC";
}

$chart_02_sql = "
	SELECT
		{$column_select}
		{$add_date_column} AS result_ym,
		COUNT(w.evaluation) AS evaluation_count,
		COUNT(*) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_prd_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY
		{$group_by}
		result_ym
	ORDER BY
		{$order_by}
";

$chart_02			  = $default_chart_list;
$chart_02_day_sum	  = $default_chart_sum_list;
$chart_02_day_avg	  = $default_chart_sum_list;
$chart_02_row_sum	  = [];
$chart_02_row_avg	  = [];
$chart_02_row_name 	  = [];
$chart_02_display 	  = [];
$chart_02_title 	  = $sch_prd ? "별점" : ($sch_prd_g2 ? "상품명" : ($sch_prd_g1 ? "그룹2" : "그룹1"));

$chart_02_query = mysqli_query($my_db, $chart_02_sql);
while($chart_02_array = mysqli_fetch_array($chart_02_query))
{
    $chart_02[$chart_02_array['result_ym']][$chart_02_array['result_name_id']] = array(
        'result_data' 	=> $chart_02_array['result_data'],
        'work_count' 	=> $chart_02_array['work_count']
    );
    $chart_02_day_sum[$chart_02_array['result_ym']]['count'] 		+= $chart_02_array['work_count'];
    $chart_02_day_sum[$chart_02_array['result_ym']]['result'] 		+= ($chart_02_array['result_data'] * $chart_02_array['work_count']);
    $chart_02_row_sum[$chart_02_array['result_name_id']]['count'] 	+= $chart_02_array['work_count'];
    $chart_02_row_sum[$chart_02_array['result_name_id']]['result'] 	+= ($chart_02_array['result_data'] * $chart_02_array['work_count']);
    $chart_02_row_name[$chart_02_array['result_name_id']] 			 = $chart_02_array['result_name'] ? $chart_02_array['result_name'] : "(NULL)";

    if(!$sch_team){
        $chart_02_display[$chart_02_array['result_name_id']] = ($chart_02_array['result_display'] == '1') ? "Y" : "N";
    }elseif(!!$sch_team && !$sch_s_no){
        $chart_02_display[$chart_02_array['result_name_id']] = ($chart_02_array['result_display'] == '3') ? "N" : "Y";
    }else{
        $chart_02_display[$chart_02_array['result_name_id']] = "Y";
    }
}

foreach($chart_02_day_sum as $day => $day_sum) {
    $chart_02_day_avg[$day]["result"] = (isset($day_sum['count']) && !empty($day_sum['count'])) ? round($day_sum['result']/$day_sum['count'], 2) : '0';
    $chart_02_day_avg[$day]["count"] = (isset($row_sum['count']) && !empty($row_sum['count'])) ? $day_sum['count'] : '0';
}

foreach($chart_02_row_sum as $row => $row_sum) {
    $chart_02_row_avg[$row]["result"] = (isset($row_sum['count']) && !empty($row_sum['count'])) ? round($row_sum['result'] / $row_sum['count'], 2) : '0';
    $chart_02_row_avg[$row]["count"] = (isset($row_sum['count']) && !empty($row_sum['count'])) ? $row_sum['count'] : '0';
}

$chart_02_t_sql = "
    SELECT
		{$add_date_column} AS result_ym,
		ROUND(AVG(w.evaluation),2) AS result_data,
        COUNT(*) AS work_count
    FROM `work` w
    WHERE
        {$add_method_where}
        {$add_prd_where}
        {$add_date_where}
    GROUP BY
        result_ym
    ORDER BY
        result_ym ASC, result_data DESC
";

$chart_02_t_day_avg = $default_chart_sum_list;
$chart_02_t_query = mysqli_query($my_db, $chart_02_t_sql);
while($chart_02_t_array = mysqli_fetch_array($chart_02_t_query))
{
    $chart_02_t_day_avg[$chart_02_t_array['result_ym']]['count']  += $chart_02_t_array['work_count'];
    $chart_02_t_day_avg[$chart_02_t_array['result_ym']]['result'] += ($chart_02_t_array['result_data'] * $chart_02_t_array['work_count']);
}

arsort($chart_02_row_avg);
ksort($chart_02);
ksort($chart_02_t_day_avg);

$smarty->assign("chart_02", $chart_02);
$smarty->assign("chart_02_display", $chart_02_display);
$smarty->assign('chart_02_title', $chart_02_title);
$smarty->assign("chart_02_day_avg", $chart_02_day_avg);
$smarty->assign("chart_02_row_avg", $chart_02_row_avg);
$smarty->assign("chart_02_row_name", $chart_02_row_name);
$smarty->assign("chart_02_t_day_avg", $chart_02_t_day_avg);
$smarty->assign("staff_chart_title", $staff_chart_title);
/* ---------- chart_02 부서/담당자별 평가점수 [End] ---------- */


$smarty->display('stats/wise_work_04.html');

?>
