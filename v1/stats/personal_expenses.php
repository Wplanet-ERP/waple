<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/data_state.php');
require('../inc/helper/_navigation.php');
require('../inc/model/MyQuick.php');

$dir_location = "../";
$smarty->assign("dir_location",$dir_location);

# Navigation & My Quick
$nav_prd_no  = "122";
$nav_title   = "개인경비 통계";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

// 접근 제한
if (!$session_s_no){ // 외주업체 로그인 시
	$smarty->display('access_company_error.html');
	exit;
}elseif (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "재무관리자") && !permissionNameCheck($session_permission, "마스터관리자")){
	$smarty->display('access_error.html');
	exit;
}

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기

// [사업자]
$sch_my_c_no_get = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "1";
$add_my_c_no_where="";
if (!empty($sch_my_c_no_get)) {
	$smarty->assign("sch_my_c_no", $sch_my_c_no_get);

	if($sch_my_c_no_get == "wise"){
		$add_my_c_no_where .= " AND (pe.my_c_no = '1' OR pe.my_c_no = '2' OR pe.my_c_no = '5' OR pe.my_c_no = '6')";
		$smarty->assign("sch_my_c_name","전체(미디어커머스 외)");
	}else{
		$add_my_c_no_where .= " AND pe.my_c_no = '" . $sch_my_c_no_get . "'";

		for($i = 0 ; $i < count($my_company_list) ; $i++) {
			if($my_company_list[$i]['my_c_no'] == $sch_my_c_no_get){
				$smarty->assign("sch_my_c_name", $my_company_list[$i]['c_name']);
				break;
			}
		}
	}
}

// [부서 > 담당자]
$sch_s_name   = "";
$sch_team_get = isset($_GET['sch_team'])?$_GET['sch_team']:"";
$sch_s_no_get = isset($_GET['sch_s_no'])?$_GET['sch_s_no']:"";

$sch_team_code_where = "";
if(!empty($sch_team_get) && $sch_team_get != "all"){
	$sch_team_code_where = getTeamWhere($my_db, $sch_team_get);
    $sch_s_name .= isset($sch_team_name_list[$sch_team_get]) ? $sch_team_name_list[$sch_team_get] : "";
}else{
    $sch_s_name .= " 전체";
}

$staff_list = [];
$staff_name = "";
if($sch_team_code_where)
{
    $staff_team_where 		= [];
    $staff_team_where_list 	= [];

    $sch_team_code_list_val = explode(",", $sch_team_code_where);

    foreach($sch_team_code_list_val as $sch_team_code){
        $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
    }

    if($staff_team_where_list)
    {
        $staff_team_where = implode(" OR ", $staff_team_where_list);
        $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
        $staff_result=mysqli_query($my_db,$staff_sql);
        while($staff=mysqli_fetch_array($staff_result)) {
            $staff_list[]=array(
                "s_no"=>trim($staff['s_no']),
                "s_name"=>trim($staff['s_name'])
            );

            if(!!$sch_s_no_get && $sch_s_no_get == $staff['s_no']){
                $staff_name = $staff['s_name'];
			}
        }
    }
}

if(!!$sch_s_no_get && !empty($staff_name)){
    $sch_s_name .= " > ".$staff_name;
}else{
    $sch_s_name .= " > 전체";
}

$smarty->assign("sch_s_name",$sch_s_name);
$smarty->assign("sch_team",$sch_team_get);
$smarty->assign("sch_s_no",$sch_s_no_get);
$smarty->assign("staff_list", $staff_list);
$smarty->assign("sch_team_list", $sch_team_name_list);


// [계정과목]
$sch_a_code_name = "";
$sch_a_code_1_get=isset($_GET['sch_a_code_1'])?$_GET['sch_a_code_1']:"";
$sch_a_code_2_get=isset($_GET['sch_a_code_2'])?$_GET['sch_a_code_2']:"";

$smarty->assign("sch_a_code_1", $sch_a_code_1_get);
$smarty->assign("sch_a_code_2", $sch_a_code_2_get);

if(!!$sch_a_code_1_get){
	for($i = 0 ; $i < count($kind_account_1_list) ; $i++) {
		if($kind_account_1_list[$i]['k_name_code'] == $sch_a_code_1_get){
			$sch_a_code_name .= $kind_account_1_list[$i]['k_name'];
			break;
		}
	}
}else{
	$sch_a_code_name .= "전체";
}

if(!!$sch_a_code_2_get){
	for($i = 0 ; $i < count($kind_account_2_list) ; $i++) {
		if($kind_account_2_list[$i]['k_name_code'] == $sch_a_code_2_get){
			$sch_a_code_name .= " > ".$kind_account_2_list[$i]['k_name'];
			break;
		}
	}
}else{
	$sch_a_code_name .= " > 전체";
}

$smarty->assign("sch_a_code_name", $sch_a_code_name);


// [조회기간]
$sch_s_date_get = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date("Y-m", strtotime("-6 month")); // 월간
$sch_s_date2_get = isset($_GET['sch_s_date2']) ? $_GET['sch_s_date2'] : date("Y-m-d", strtotime("-7 day")); // 일간

$smarty->assign("sch_s_date", $sch_s_date_get);
$smarty->assign("sch_s_date2", $sch_s_date2_get);

$sch_e_date_get = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date("Y-m"); // 월간
$sch_e_date2_get = isset($_GET['sch_e_date2']) ? $_GET['sch_e_date2'] : date("Y-m-d"); // 일간
$smarty->assign("sch_e_date", $sch_e_date_get);
$smarty->assign("sch_e_date2", $sch_e_date2_get);


// 계정과목 목록 가져오기
$kind_sql="SELECT k.k_name, k.k_name_code
							FROM kind k
							WHERE k.display='1' AND k.k_code='account_code' AND k.k_parent IS NULL
							ORDER BY k.priority ASC
						";
$kind_query = mysqli_query($my_db,$kind_sql);

while($kind_data = mysqli_fetch_array($kind_query)){
	$account_code_1[]=array(
		"k_name"=>$kind_data['k_name'],
		"k_name_code"=>$kind_data['k_name_code']
	);
}
$smarty->assign("account_code_1", $account_code_1);

// 계정과목에 따른 2차 계정과목 가져오기
if($sch_a_code_1_get != ""){
	$kind_sql="SELECT k.k_name, k.k_name_code
							FROM kind k
							WHERE k.display='1' AND k.k_code='account_code' AND k.k_parent='".$sch_a_code_1_get."'
							ORDER BY k.priority ASC
		        ";
	$kind_query=mysqli_query($my_db,$kind_sql);

	while($kind_data=mysqli_fetch_array($kind_query)) {
		$account_code_2[]=array(
			"k_name"=>trim($kind_data['k_name']),
			"k_name_code"=>trim($kind_data['k_name_code']),
			"k_parent"=>trim($kind_data['k_parent'])
		);
		$smarty->assign("account_code_2",$account_code_2);
	}
}

// 상태 조건
$add_method_where = " pe.display = '1'";

// 조회 기간
$add_date_where = "";
$add_date_where .= " AND DATE_FORMAT(pe.payment_date, '%Y-%m') BETWEEN '$sch_s_date_get' AND '$sch_e_date_get' ";

// 진행 상태 조건
$add_where = "";
$add_where .= " AND (pe.state = '4' OR pe.state = '5') "; // display ON, 승인완료 혹은 지급완료

// 담당자 기준
$add_team_select = "";
$add_s_no_select = "";
$add_team_where = "";

$add_team_select .= " pe.team AS select_team, (SELECT t.display FROM team t WHERE t.team_code=pe.team) as t_display,";
$add_s_no_select .= " pe.s_no AS select_s_no, (SELECT s.staff_state FROM staff s WHERE s.s_no=pe.s_no) as s_display,";
$add_s_no_select .= " (SELECT s.s_name FROM staff s WHERE s.s_no = pe.s_no) AS select_s_name,";
if (!!$sch_s_no_get) { // 담당자 설정된 경우
	$add_team_where .= " AND pe.s_no = '$sch_s_no_get'";
}else{
	if(!!$sch_team_code_where){ // 부서 설정, 담당자 전체의 경우
		$add_team_where.=" AND pe.team IN ({$sch_team_code_where})";
	}
}

// 계정과목 기준
$add_account_1_select = "";
$add_account_2_select = "";
$add_account_code_where = "";

$add_account_1_select .= " (SELECT k_name_code FROM kind WHERE k_name_code = (SELECT k_parent FROM kind WHERE k_code = 'account_code' AND k_name_code=pe.account_code)) AS select_account_1_code,";
$add_account_1_select .= " (SELECT k_name FROM kind WHERE k_name_code = (SELECT k_parent FROM kind WHERE k_code = 'account_code' AND k_name_code=pe.account_code)) AS select_account_1_name,";
$add_account_2_select .= " pe.account_code AS select_account_2_code,";
$add_account_2_select .= " (SELECT k.k_name FROM kind k WHERE k.k_name_code = pe.account_code) AS select_account_2_name,";

if (!!$sch_a_code_2_get) { // 계정과목 2가 설정된 경우
	$add_account_code_where .= " AND pe.account_code = '$sch_a_code_2_get'";
}else{
	if(!!$sch_a_code_1_get){ // 계정과목 1이 설정된 경우
		$add_account_code_where .= " AND pe.account_code IN (SELECT k_name_code FROM kind WHERE k_parent = '$sch_a_code_1_get')";
	}
}


// x축 이름 (data가 0인 경우 대비)
$x_ym = date("Y-m", strtotime($sch_s_date_get));
$x_ym_end = date("Y-m", strtotime($sch_e_date_get));

while( $x_ym <= $x_ym_end ){
	$x_name_list[] = $x_ym;
	$x_ym = date("Y-m", strtotime("+1 month", strtotime($x_ym)));
}

$smarty->assign("x_name_list",$x_name_list);


/* ---------- chart_01 개인경비 통계 [Start] ---------- */
$chart_01_sql = "
	SELECT
		DATE_FORMAT(pe.payment_date, '%Y-%m') AS result_ym,
		SUM(money) AS money
	FROM personal_expenses pe
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_where}
		{$add_my_c_no_where}
		{$add_team_where}
		{$add_account_code_where}
	GROUP BY result_ym
	ORDER BY result_ym ASC
";

$chart_01_sql_sum = "
	SELECT
		SUM(result_sql.money) AS money_sum
	FROM ({$chart_01_sql}) AS	result_sql
	WHERE 1=1
";
$chart_01_sum_result 	= mysqli_query($my_db, $chart_01_sql_sum);
$chart_01_sum_array 	= mysqli_fetch_array($chart_01_sum_result);

$money_sum = "";
$money_sum = $chart_01_sum_array['money_sum'];

$chart_01_result = mysqli_query($my_db, $chart_01_sql);
$chart_01		 = [];
while($chart_01_array = mysqli_fetch_array($chart_01_result)){
	$chart_01[] = array(
		"result_ym"		  	=> $chart_01_array['result_ym'],
		"result_data"	  	=> floor($chart_01_array['money']/1000),
		"result_data_sum" 	=> floor($money_sum/1000)
	);
}
$smarty->assign("chart_01", $chart_01);
/* ---------- chart_01 개인경비 통계[End] ---------- */


/* ---------- chart_02 부서별 개인경비 통계 [Start] ---------- */
$chart_02_sql = "
	SELECT
		{$add_team_select}
		DATE_FORMAT(pe.payment_date, '%Y-%m') AS result_ym,
		SUM(money) AS money
	FROM personal_expenses pe
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_my_c_no_where}
		{$add_team_where}
		{$add_account_code_where}
	GROUP BY select_team, result_ym
	ORDER BY result_ym ASC
";
$chart_02_result 	= mysqli_query($my_db, $chart_02_sql);
$chart_02		 	= [];
$chart_02_display 	= [];
while($chart_02_array = mysqli_fetch_array($chart_02_result)){
	$chart_02[] = array(
		"team"			=> $chart_02_array['select_team'],
		"result_ym"		=> $chart_02_array['result_ym'],
		"result_data"	=> floor($chart_02_array['money']/1000)
	);
    $chart_02_display[$chart_02_array['select_team']] = ($chart_02_array['t_display'] == '1') ? "Y" : "N";
}
$smarty->assign("chart_02", $chart_02);
$smarty->assign("chart_02_display", $chart_02_display);


$chart_02_sql_sum = "
	SELECT
		result_sql.select_team,
		(SELECT team_name FROM team WHERE team_code = result_sql.select_team) AS team_name,
		SUM(result_sql.money) AS money_sum
	FROM ({$chart_02_sql}) AS	result_sql
	WHERE
		1=1
	GROUP BY result_sql.select_team
";
//echo $chart_02_sql_sum; echo "<br>";//exit;

$chart_02_sum_result = mysqli_query($my_db, $chart_02_sql_sum);
while($chart_02_sum_array = mysqli_fetch_array($chart_02_sum_result)){
	$chart_02_sum[] = array(
		"team"=>$chart_02_sum_array['select_team'],
		"team_name"=>$chart_02_sum_array['team_name'],
		"result_data_sum"=>floor($chart_02_sum_array['money_sum']/1000)
	);
}

$chart_02_sum = array_sort($chart_02_sum, "result_data_sum"); // result_data_sum 순으로 변경 (오름차순)
$chart_02_title_list = "";
if(!empty($chart_02_sum))
	$chart_02_title_list = array_reverse($chart_02_sum); // result_data_sum 순으로 변경 (내림차순)

$chart_02_title_list = super_unique($chart_02_title_list,"team"); // s_no 중복 제거 (오름차순)
$smarty->assign("chart_02_title_list",$chart_02_title_list);


if(!!$chart_02){
	// x좌표 항목에 관한 합계 구하기
	$chart_02_result_ym_dp_sum = "";
	$chart_02_result_ym_wd_sum = "";
	for($i=0 ; $i < sizeof($x_name_list) ; $i++){
		$dp_sum_tmp = 0;
		$wd_sum_tmp = 0;
		foreach($chart_02 as $key => $value){
			if($value['result_ym'] == $x_name_list[$i]){
				$dp_sum_tmp += $value['result_data'];
				$wd_sum_tmp += $value['result_data_wd'];
			}
		}

		$chart_02_result_ym_sum[] = array(
			"result_ym"=>$value['result_ym'],
			"result_ym_dp_sum"=>$dp_sum_tmp,
			"result_ym_wd_sum"=>$wd_sum_tmp
		);
	}

	$smarty->assign("chart_02_result_ym_sum",$chart_02_result_ym_sum);
}

/* ---------- chart_02 부서별 개인경비 통계 [End] ---------- */



/* ---------- chart_03 담당자별 개인경비 통계 [Start] ---------- */

/* chart_03 [Start] */
// 리스트 쿼리
$chart_03_sql = "
	SELECT
		$add_s_no_select
		DATE_FORMAT(pe.payment_date, '%Y-%m') AS result_ym,
		SUM(money) AS money
	FROM personal_expenses pe
	WHERE
		$add_method_where
		$add_date_where
		$add_my_c_no_where
		$add_team_where
		$add_account_code_where
	GROUP BY select_s_no, result_ym
	ORDER BY result_ym ASC
";
$chart_03_result 	= mysqli_query($my_db, $chart_03_sql);
$chart_03_display	= [];
while($chart_03_array = mysqli_fetch_array($chart_03_result)){
	$chart_03[] = array(
		"s_no"			=> $chart_03_array['select_s_no'],
		"s_name"		=> $chart_03_array['select_s_name'],
		"result_ym"		=> $chart_03_array['result_ym'],
		"result_data"	=> floor($chart_03_array['money']/1000)
	);

    $chart_03_display[$chart_03_array['select_s_no']]   = (empty($chart_03_array['s_display']) || $chart_03_array['s_display'] == '3') ? "N" : "Y";
}
$smarty->assign("chart_03", $chart_03);
$smarty->assign("chart_03_display", $chart_03_display);


$chart_03_sql_sum = "
	SELECT
		result_sql.select_s_no,
		result_sql.select_s_name,
		SUM(result_sql.money) AS money_sum
	FROM ({$chart_03_sql}) AS	result_sql
	WHERE
		1=1
	GROUP BY result_sql.select_s_no
";
//echo $chart_03_sql_sum; echo "<br>";//exit;

$chart_03_sum_result = mysqli_query($my_db, $chart_03_sql_sum);
while($chart_03_sum_array = mysqli_fetch_array($chart_03_sum_result)){
	$chart_03_sum[] = array(
		"s_no"=>$chart_03_sum_array['select_s_no'],
		"s_name"=>$chart_03_sum_array['select_s_name'],
		"result_data_sum"=>floor($chart_03_sum_array['money_sum']/1000)
	);
}

$chart_03_sum = array_sort($chart_03_sum, "result_data_sum"); // result_data_sum 순으로 변경 (오름차순)
$chart_03_title_list = "";
if(!empty($chart_03_sum))
	$chart_03_title_list = array_reverse($chart_03_sum); // result_data_sum 순으로 변경 (내림차순)

$chart_03_title_list = super_unique($chart_03_title_list,"s_no"); // s_no 중복 제거 (오름차순)
$smarty->assign("chart_03_title_list",$chart_03_title_list);


if(!!$chart_03){
	// x좌표 항목에 관한 합계 구하기
	$chart_03_result_ym_dp_sum = "";
	$chart_03_result_ym_wd_sum = "";
	for($i=0 ; $i < sizeof($x_name_list) ; $i++){
		$dp_sum_tmp = 0;
		$wd_sum_tmp = 0;
		foreach($chart_03 as $key => $value){
			if($value['result_ym'] == $x_name_list[$i]){
				$dp_sum_tmp += $value['result_data'];
				$wd_sum_tmp += $value['result_data_wd'];
			}
		}

		$chart_03_result_ym_sum[] = array(
			"result_ym"=>$value['result_ym'],
			"result_ym_dp_sum"=>$dp_sum_tmp,
			"result_ym_wd_sum"=>$wd_sum_tmp
		);
	}

	$smarty->assign("chart_03_result_ym_sum",$chart_03_result_ym_sum);
}
/* ---------- chart_03 담당자별 개인경비 통계 [End] ---------- */



/* ---------- chart_04 계정과목1별 개인경비 통계 [Start] ---------- */

/* chart_04 [Start] */
// 리스트 쿼리
$chart_04_sql = "
	SELECT
		$add_account_1_select
		DATE_FORMAT(pe.payment_date, '%Y-%m') AS result_ym,
		SUM(money) AS money
	FROM personal_expenses pe
	WHERE
	 		$add_method_where
			$add_date_where
			$add_my_c_no_where
			$add_team_where
			$add_account_code_where
	GROUP BY select_account_1_code, result_ym
	ORDER BY result_ym ASC
";
//echo $chart_04_sql; echo "<br>";//exit;

$chart_04_result = mysqli_query($my_db, $chart_04_sql);
while($chart_04_array = mysqli_fetch_array($chart_04_result)){
	$chart_04[] = array(
		"account_1_code"=>$chart_04_array['select_account_1_code'],
		"account_1_name"=>$chart_04_array['select_account_1_name'],
		"result_ym"=>$chart_04_array['result_ym'],
		"result_data"=>floor($chart_04_array['money']/1000)
	);
}

$smarty->assign("chart_04",$chart_04);


$chart_04_sql_sum = "
	SELECT
		result_sql.select_account_1_code,
		result_sql.select_account_1_name,
		SUM(result_sql.money) AS money_sum
	FROM ({$chart_04_sql}) AS	result_sql
	WHERE
		1=1
	GROUP BY result_sql.select_account_1_code
";
//echo $chart_04_sql_sum; echo "<br>";//exit;

$chart_04_sum_result = mysqli_query($my_db, $chart_04_sql_sum);
while($chart_04_sum_array = mysqli_fetch_array($chart_04_sum_result)){
	$chart_04_sum[] = array(
		"account_1_code"=>$chart_04_sum_array['select_account_1_code'],
		"account_1_name"=>$chart_04_sum_array['select_account_1_name'],
		"result_data_sum"=>floor($chart_04_sum_array['money_sum']/1000)
	);
}

$chart_04_sum = array_sort($chart_04_sum, "result_data_sum"); // result_data_sum 순으로 변경 (오름차순)
$chart_04_title_list = "";
if(!empty($chart_04_sum))
	$chart_04_title_list = array_reverse($chart_04_sum); // result_data_sum 순으로 변경 (내림차순)

$chart_04_title_list = super_unique($chart_04_title_list,"account_1_code"); // account_1_code 중복 제거 (오름차순)
$smarty->assign("chart_04_title_list",$chart_04_title_list);


if(!!$chart_04){
	// x좌표 항목에 관한 합계 구하기
	$chart_04_result_ym_dp_sum = "";
	$chart_04_result_ym_wd_sum = "";
	for($i=0 ; $i < sizeof($x_name_list) ; $i++){
		$dp_sum_tmp = 0;
		$wd_sum_tmp = 0;
		foreach($chart_04 as $key => $value){
			if($value['result_ym'] == $x_name_list[$i]){
				$dp_sum_tmp += $value['result_data'];
				$wd_sum_tmp += $value['result_data_wd'];
			}
		}

		$chart_04_result_ym_sum[] = array(
			"result_ym"=>$value['result_ym'],
			"result_ym_dp_sum"=>$dp_sum_tmp,
			"result_ym_wd_sum"=>$wd_sum_tmp
		);
	}

	$smarty->assign("chart_04_result_ym_sum",$chart_04_result_ym_sum);
}
/* ---------- chart_04 계정과목1별 개인경비 통계 [End] ---------- */



/* ---------- chart_05 계정과목2별 개인경비 통계 [Start] ---------- */

/* chart_05 [Start] */
// 리스트 쿼리
$chart_05_sql = "
	SELECT
		$add_account_2_select
		DATE_FORMAT(pe.payment_date, '%Y-%m') AS result_ym,
		SUM(money) AS money
	FROM personal_expenses pe
	WHERE
	 		$add_method_where
			$add_date_where
			$add_my_c_no_where
			$add_team_where
			$add_account_code_where
	GROUP BY select_account_2_code, result_ym
	ORDER BY result_ym ASC
";
//echo $chart_05_sql; echo "<br>";//exit;

$chart_05_result = mysqli_query($my_db, $chart_05_sql);
while($chart_05_array = mysqli_fetch_array($chart_05_result)){
	$chart_05[] = array(
		"account_2_code"=>$chart_05_array['select_account_2_code'],
		"account_2_name"=>$chart_05_array['select_account_2_name'],
		"result_ym"=>$chart_05_array['result_ym'],
		"result_data"=>floor($chart_05_array['money']/1000)
	);
}

$smarty->assign("chart_05",$chart_05);


$chart_05_sql_sum = "
	SELECT
		result_sql.select_account_2_code,
		result_sql.select_account_2_name,
		SUM(result_sql.money) AS money_sum
	FROM ({$chart_05_sql}) AS	result_sql
	WHERE
		1=1
	GROUP BY result_sql.select_account_2_code
";
//echo $chart_05_sql_sum; echo "<br>";//exit;

$chart_05_sum_result = mysqli_query($my_db, $chart_05_sql_sum);
while($chart_05_sum_array = mysqli_fetch_array($chart_05_sum_result)){
	$chart_05_sum[] = array(
		"account_2_code"=>$chart_05_sum_array['select_account_2_code'],
		"account_2_name"=>$chart_05_sum_array['select_account_2_name'],
		"result_data_sum"=>floor($chart_05_sum_array['money_sum']/1000)
	);
}

$chart_05_sum = array_sort($chart_05_sum, "result_data_sum"); // result_data_sum 순으로 변경 (오름차순)
$chart_05_title_list = "";
if(!empty($chart_05_sum))
	$chart_05_title_list = array_reverse($chart_05_sum); // result_data_sum 순으로 변경 (내림차순)

$chart_05_title_list = super_unique($chart_05_title_list,"account_2_code"); // account_2_code 중복 제거 (오름차순)
$smarty->assign("chart_05_title_list",$chart_05_title_list);


if(!!$chart_05){
	// x좌표 항목에 관한 합계 구하기
	$chart_05_result_ym_dp_sum = "";
	$chart_05_result_ym_wd_sum = "";
	for($i=0 ; $i < sizeof($x_name_list) ; $i++){
		$dp_sum_tmp = 0;
		$wd_sum_tmp = 0;
		foreach($chart_05 as $key => $value){
			if($value['result_ym'] == $x_name_list[$i]){
				$dp_sum_tmp += $value['result_data'];
				$wd_sum_tmp += $value['result_data_wd'];
			}
		}

		$chart_05_result_ym_sum[] = array(
			"result_ym"=>$value['result_ym'],
			"result_ym_dp_sum"=>$dp_sum_tmp,
			"result_ym_wd_sum"=>$wd_sum_tmp
		);
	}

	$smarty->assign("chart_05_result_ym_sum",$chart_05_result_ym_sum);
}
/* ---------- chart_05 계정과목2별 개인경비 통계 [End] ---------- */


$smarty->display('stats/personal_expenses.html');

?>
