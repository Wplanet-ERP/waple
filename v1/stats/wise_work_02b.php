<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/wise_stats.php');
require('../inc/model/MyQuick.php');
require('../inc/model/Kind.php');
require('../inc/model/Staff.php');
require('../inc/model/Team.php');
require('../inc/model/ProductCms.php');
require('../inc/model/Company.php');

$dir_location = "../";
$smarty->assign("dir_location", $dir_location);

# Navigation & My Quick
$nav_prd_no  = "128";
$nav_title   = "커머스 상품 판매금액 통계";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 모델 생성
$kind_model 	= Kind::Factory();
$staff_model 	= Staff::Factory();
$team_model 	= Team::Factory();
$product_model 	= ProductCms::Factory();
$company_model 	= Company::Factory();
$kind_code		= "product_cms";

# 상품 관련 검색쿼리
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

$kind_chart_data_list 	= $kind_model->getKindChartData($kind_code);
$prd_group_list 		= $kind_chart_data_list['kind_group_list'];
$prd_group_name_list	= $kind_chart_data_list['kind_group_name'];
$prd_group_code_list	= $kind_chart_data_list['kind_group_code'];
$prd_group_code 		= implode(",", $prd_group_code_list);
$prd_data_list			= $product_model->getPrdGroupChartData($prd_group_code);
$prd_total_list			= $prd_data_list['prd_total'];

$prd_g1_list = $prd_g2_list = $prd_g3_list = [];

foreach($prd_group_list as $key => $prd_data)
{
	if(!$key){
		$prd_g1_list = $prd_data;
	}else{
		$prd_g2_list[$key] = $prd_data;
	}
}

$prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
$sch_prd_list = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];
$sch_prd_name = isset($prd_group_name_list[$sch_prd_g1]) ? $prd_group_name_list[$sch_prd_g1] : "전체";
$sch_prd_name .= isset($prd_group_name_list[$sch_prd_g2]) ? " > ".$prd_group_name_list[$sch_prd_g2] : " > 전체";
$sch_prd_name .= isset($prd_name_list[$sch_prd]) ? " > ".$prd_name_list[$sch_prd] : " > 전체";

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);
$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);
$smarty->assign("sch_prd_name", $sch_prd_name);


# [조회 단위] & [조회 기간]
$kind_type_option		= getKindTypeOption();
$kind_type_name_option	= getKindTypeNameOption();

$sch_kind			= isset($_GET['sch_kind']) ? $_GET['sch_kind'] : "4";
$sch_kind_type      = isset($kind_type_option[$sch_kind]) ? $kind_type_option[$sch_kind] : "day";
$today_s_w		    = date('w')-1;
$sch_year_s_date 	= isset($_GET['sch_year_s_date']) ? $_GET['sch_year_s_date'] : date("Y", strtotime("-3 years")); // 연간
$sch_year_e_date 	= isset($_GET['sch_year_e_date']) ? $_GET['sch_year_e_date'] : date("Y"); // 연간
$sch_mon_s_date 	= isset($_GET['sch_mon_s_date']) ? $_GET['sch_mon_s_date'] : date("Y-m", strtotime("-6 months")); // 월간
$sch_mon_e_date 	= isset($_GET['sch_mon_e_date']) ? $_GET['sch_mon_e_date'] : date("Y-m"); // 월간
$sch_week_s_date 	= isset($_GET['sch_week_s_date']) ? $_GET['sch_week_s_date'] : date("Y-m-d",strtotime("-{$today_s_w} day")); // 주간
$sch_week_e_date 	= isset($_GET['sch_week_e_date']) ? $_GET['sch_week_e_date'] : date("Y-m-d", strtotime("{$sch_week_s_date} +6 day")); // 주간
$sch_s_date 	    = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date("Y-m-d", strtotime("-6 day")); // 일간
$sch_e_date 	    = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date("Y-m-d"); // 일간

$smarty->assign("sch_kind", $sch_kind);
$smarty->assign("sch_kind_type", $sch_kind_type);
$smarty->assign("sch_year_s_date", $sch_year_s_date);
$smarty->assign("sch_year_e_date", $sch_year_e_date);
$smarty->assign("sch_mon_s_date", $sch_mon_s_date);
$smarty->assign("sch_mon_e_date", $sch_mon_e_date);
$smarty->assign("sch_week_s_date", $sch_week_s_date);
$smarty->assign("sch_week_e_date", $sch_week_e_date);
$smarty->assign("sch_s_date", $sch_s_date);
$smarty->assign("sch_e_date", $sch_e_date);
$smarty->assign("sch_kind_type_option", $kind_type_option);
$smarty->assign("sch_kind_type_name_option", $kind_type_name_option);


# [담당자 기준] : 업체, 팀, 담당자
$staff_kind_option	= getStaffTypeOption();
$team_kind_option	= getTeamTypeOption();
$sch_team_name_list	= $team_model->getTeamFullNameList();
$sch_s_no_kind		= isset($_GET['sch_s_no_kind']) ? $_GET['sch_s_no_kind'] : "s_no";
$sch_team_kind		= isset($team_kind_option[$sch_s_no_kind]) ? $team_kind_option[$sch_s_no_kind] : "team";
$sch_s_no_kind_name = isset($staff_kind_option[$sch_s_no_kind]) ? $staff_kind_option[$sch_s_no_kind] : "담당자";
$sch_team			= isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no			= isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$sch_s_name 		= "";
$staff_list 		= [];
$staff_name 		= "";

$sch_team_code_where = "";
if(!empty($sch_team) && $sch_team != "all"){
	$sch_team_code_where = $team_model->getTeamCodeWhere($sch_team);
	$sch_s_name .= isset($sch_team_name_list[$sch_team]) ? $sch_team_name_list[$sch_team] : "";
}else{
	$sch_s_name .= " 전체";
}

if($sch_team_code_where)
{
	$staff_team_where 		= "";
	$staff_team_where_list 	= [];

	$sch_team_code_list_val = explode(",", $sch_team_code_where);

	foreach($sch_team_code_list_val as $sch_team_code){
		$staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
	}

	if($staff_team_where_list)
	{
		$staff_team_where = implode(" OR ", $staff_team_where_list);
		$staff_list  	  = $staff_model->getTeamWhereStaff($staff_team_where);
		if(!!$sch_s_no){
			$staff_name = $staff_model->getStaffName($sch_s_no);
		}
	}
}

if(!!$sch_s_no && !empty($staff_name)){
	$sch_s_name .= " > ".$staff_name;
}else{
	$sch_s_name .= " > 전체";
}

$smarty->assign("sch_s_no_kind", $sch_s_no_kind);
$smarty->assign("sch_s_no_kind_name", $sch_s_no_kind_name);
$smarty->assign("sch_s_name", $sch_s_name);
$smarty->assign("sch_team", $sch_team);
$smarty->assign("sch_s_no", $sch_s_no);
$smarty->assign("sch_staff_list", $staff_list);
$smarty->assign("sch_team_list", $sch_team_name_list);
$smarty->assign("sch_staff_kind_option", $staff_kind_option);


# 검색 조건 처리
$add_method_where = " w.delivery_state='4' AND w.dp_c_no NOT IN(2007, 2008) AND w.unit_price > 0";

# 구매처 검색
$sch_dp_c_no 		= isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$dp_company_list	= $company_model->getDpList();

if(!empty($sch_dp_c_no))
{
	$add_method_where .= " AND w.dp_c_no='{$sch_dp_c_no}'";
	$smarty->assign("sch_dp_c_no", $sch_dp_c_no);
}
$smarty->assign("dp_company_list", $dp_company_list);

# 상품(업무) 종류
if (!empty($sch_prd) && $sch_prd != "0") { // 상품
	$add_method_where .= " AND w.prd_no='{$sch_prd}'";
}else{
	if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
		$add_method_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
	}elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
		$add_method_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
	}
}

# 담당자 기준
$add_team_select = " w.{$sch_team_kind} AS select_team, (SELECT t.team_name FROM team as t WHERE t.team_code = w.{$sch_team_kind}) as team_name, (SELECT t.display FROM team as t WHERE t.team_code = w.{$sch_team_kind}) as t_display,";
$add_s_no_select = " w.{$sch_s_no_kind} AS select_s_no, (SELECT s.s_name FROM staff s WHERE s.s_no = w.{$sch_s_no_kind}) AS select_s_name, (SELECT s.staff_state FROM staff s WHERE s.s_no=w.{$sch_s_no_kind}) AS s_display,";
$add_team_where  = "";

if(!!$sch_s_no_kind)
{
	if (!!$sch_s_no) {
		$add_team_where .= " AND w.{$sch_s_no_kind} = '{$sch_s_no}'";
	}else{
		if(!!$sch_team_code_where){ // 부서 설정, 담당자 전체의 경우
			$add_team_where .= " AND w.{$sch_team_kind} IN ({$sch_team_code_where})";
		}
	}
}

// 기본 베이스 Array 만들기($x_name_list: 그래프 X 부분 타이틀, $default_chart_list: 기본 차트 날짜별, $default_chart_sum_list: 기본 차트 합)
$x_name_list 					= [];
$default_chart_one_type_list 	= [];
$default_chart_two_type_list 	= [];
$default_chart_one_type_sum_list = [];
$default_chart_two_type_sum_list = [];
$work_02_chart_type              = array('deposit' => '매출');

// 조회 기간 : add_date_where, date_column
$all_date_where 		= "";
$all_date_key			= "";
$all_date_title 		= "";
$add_date_where 		= "";
$add_date_column 		= "";

if($sch_kind == '1') //연간
{
	$all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_year_s_date}' AND '{$sch_year_e_date}'";
	$all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y')";
	$all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y')";

	$sch_year_s_date = $sch_year_s_date."-01-01 00:00:00";
	$sch_year_e_date = $sch_year_e_date."-12-31 23:59:59";
	$add_date_where  = " AND (w.order_date >= '{$sch_year_s_date}' AND w.order_date <= '{$sch_year_e_date}') ";
	$add_date_column = "DATE_FORMAT(w.order_date, '%Y')";
}
elseif($sch_kind == '2') //월간
{
	$all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_mon_s_date}' AND '{$sch_mon_e_date}'";
	$all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
	$all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m')";

	$sch_mon_s_date  = $sch_mon_s_date."-01 00:00:00";
	$sch_mon_e_date  = $sch_mon_e_date."-31 23:59:59";
	$add_date_where  = " AND (w.order_date >= '{$sch_mon_s_date}' AND w.order_date <= '{$sch_mon_e_date}') ";
	$add_date_column = "DATE_FORMAT(w.order_date, '%Y%m')";
}
elseif($sch_kind == '3') //주간
{
	$all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_week_s_date}' AND '{$sch_week_e_date}'";
	$all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
	$all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

	$sch_week_s_date = $sch_week_s_date." 00:00:00";
	$sch_week_e_date = $sch_week_e_date." 23:59:59";
	$add_date_where  = " AND (w.order_date >= '{$sch_week_s_date}' AND w.order_date <= '{$sch_week_e_date}') ";
	$add_date_column = "DATE_FORMAT(DATE_SUB(w.order_date, INTERVAL(IF(DAYOFWEEK(w.order_date)=1,8,DAYOFWEEK(w.order_date))-2) DAY), '%Y%m%d')";
}
else //일간
{
	$all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
	$all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
	$all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";

	$sch_s_date 	 = $sch_s_date." 00:00:00";
	$sch_e_date 	 = $sch_e_date." 23:59:59";
	$add_date_where  = " AND (w.order_date >= '{$sch_s_date}' AND w.order_date <= '{$sch_e_date}') ";
	$add_date_column = "DATE_FORMAT(w.order_date, '%Y%m%d')";
}

$all_date_sql = "
	SELECT
 		$all_date_key as chart_key,
		$all_date_title as chart_title
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	GROUP BY chart_key
	ORDER BY chart_key
";

if($all_date_where != ''){
	$all_date_query = mysqli_query($my_db, $all_date_sql);
	while($date = mysqli_fetch_array($all_date_query))
	{
		$default_chart_one_type_list[$date['chart_key']] = [];
		$default_chart_one_type_sum_list[$date['chart_key']] = 0;

		$default_chart_two_type_list[$date['chart_key']] = array(
			"deposit" 	=> []
		);

		$default_chart_two_type_sum_list[$date['chart_key']] = array(
			"deposit" 	=> 0
		);

		$x_name_list[$date['chart_key']] = $date['chart_title'];
	}
}

ksort($x_name_list);
$smarty->assign('x_name_title', $x_name_list);
$smarty->assign('x_name_list', "'".implode("','", $x_name_list)."'");
$smarty->assign("work_02_chart_type", $work_02_chart_type);


/* ---------- chart_01 상품 판매금액 통계 [Start] ---------- */
$chart_01_sql = "
	SELECT
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price
	FROM `work_cms` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY result_ym
	ORDER BY result_ym ASC
";

$chart_01 = [];
$chart_01_result = mysqli_query($my_db, $chart_01_sql);
while($chart_01_array = mysqli_fetch_array($chart_01_result))
{
	$chart_01["deposit"][$chart_01_array['result_ym']] = floor($chart_01_array['dp_price']/1000);
}

$chart_01["deposit"]["color"] 	= "window.chartColors.blue";
$chart_01["deposit"]["total"] 	= array_sum($chart_01["deposit"]);

$smarty->assign("chart_01", $chart_01);
/* ---------- chart_01 상품 판매금액 통계[End] ---------- */


/* ---------- chart_02 부서별 상품 판매금액 통계 [Start] ---------- */
$chart_02_sql = "
	SELECT
		{$add_team_select}
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price
	FROM `work_cms` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY select_team, result_ym
	ORDER BY result_ym ASC
";

$chart_02		   	= $default_chart_two_type_list;
$chart_02_day_sum  	= $default_chart_two_type_sum_list;
$chart_02_row_sum  	= [];
$chart_02_row_name 	= [];
$chart_02_display 	= [];

$chart_02_query = mysqli_query($my_db, $chart_02_sql);
while($chart_02_array = mysqli_fetch_array($chart_02_query))
{
	$chart_02[$chart_02_array['result_ym']][$chart_02_array['select_team']] = array(
		"deposit" 	=> floor($chart_02_array['dp_price']/1000)
	);
	$chart_02_day_sum[$chart_02_array['result_ym']]["deposit"]		+= floor($chart_02_array['dp_price']/1000);
	$chart_02_row_sum[$chart_02_array['select_team']]["deposit"]	+= floor($chart_02_array['dp_price']/1000);

	$chart_02_row_name[$chart_02_array['select_team']] 	= $chart_02_array['team_name'] ? $chart_02_array['team_name'] : "(NULL)";
    $chart_02_display[$chart_02_array['select_team']] 	= $chart_02_array['t_display'] == "1" ? "Y" : "N";
}

arsort($chart_02_row_sum);
ksort($chart_02);

$smarty->assign("chart_02", $chart_02);
$smarty->assign("chart_02_display", $chart_02_display);
$smarty->assign("chart_02_day_sum", $chart_02_day_sum);
$smarty->assign("chart_02_row_sum", $chart_02_row_sum);
$smarty->assign("chart_02_row_name", $chart_02_row_name);
/* ---------- chart_02 부서별 상품 판매금액 통계 [End] ---------- */


/* ---------- chart_03 <{$sch_s_no_kind_name}>별 상품 판매금액 통계 [Start] ---------- */
$chart_03_sql = "
	SELECT
		{$add_s_no_select}
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price
	FROM `work_cms` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY select_s_no, result_ym
	ORDER BY result_ym ASC
";

$chart_03		   = $default_chart_two_type_list;
$chart_03_day_sum  = $default_chart_two_type_sum_list;
$chart_03_row_sum  = [];
$chart_03_row_name = [];
$chart_03_display  = [];

$chart_03_query = mysqli_query($my_db, $chart_03_sql);
while($chart_03_array = mysqli_fetch_array($chart_03_query))
{
	$chart_03[$chart_03_array['result_ym']][$chart_03_array['select_s_no']] = array(
		"deposit" 	=> floor($chart_03_array['dp_price']/1000)
	);
	$chart_03_day_sum[$chart_03_array['result_ym']]["deposit"]		+= floor($chart_03_array['dp_price']/1000);
	$chart_03_row_sum[$chart_03_array['select_s_no']]["deposit"]	+= floor($chart_03_array['dp_price']/1000);

	$chart_03_row_name[$chart_03_array['select_s_no']] 	= $chart_03_array['select_s_name'] ? $chart_03_array['select_s_name'] : "(NULL)";
    $chart_03_display[$chart_03_array['select_s_no']]   = (empty($chart_03_array['s_display']) || $chart_03_array['s_display'] == '3') ? "N" : "Y";
}

arsort($chart_03_row_sum);
ksort($chart_03);

$smarty->assign("chart_03", $chart_03);
$smarty->assign("chart_03_day_sum", $chart_03_day_sum);
$smarty->assign("chart_03_row_sum", $chart_03_row_sum);
$smarty->assign("chart_03_row_name", $chart_03_row_name);
$smarty->assign("chart_03_display", $chart_03_display);
/* ---------- chart_03 <{$sch_s_no_kind_name}>별 상품 판매금액 통계 [End] ---------- */


/* ---------- chart_04 상품그룹2별 판매금액 통계 [Start] ---------- */
$chart_04_sql = "
	SELECT
		(SELECT prd_cms.k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no) AS k_name_code,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT prd_cms.k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_name,
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price
	FROM `work_cms` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY k_name, result_ym
	ORDER BY result_ym ASC
";

$chart_04		   = $default_chart_two_type_list;
$chart_04_day_sum  = $default_chart_two_type_sum_list;
$chart_04_row_sum  = [];
$chart_04_row_name = [];

$chart_04_query = mysqli_query($my_db, $chart_04_sql);
while($chart_04_array = mysqli_fetch_array($chart_04_query))
{
	$chart_04[$chart_04_array['result_ym']][$chart_04_array['k_name_code']] = array(
		"deposit" 	=> floor($chart_04_array['dp_price']/1000)
	);
	$chart_04_day_sum[$chart_04_array['result_ym']]["deposit"]		+= floor($chart_04_array['dp_price']/1000);
	$chart_04_row_sum[$chart_04_array['k_name_code']]["deposit"]	+= floor($chart_04_array['dp_price']/1000);

	$chart_04_row_name[$chart_04_array['k_name_code']] 	= $chart_04_array['k_name'] ? $chart_04_array['k_name'] : "(NULL)";
}

arsort($chart_04_row_sum);
ksort($chart_04);

$smarty->assign("chart_04", $chart_04);
$smarty->assign("chart_04_day_sum", $chart_04_day_sum);
$smarty->assign("chart_04_row_sum", $chart_04_row_sum);
$smarty->assign("chart_04_row_name", $chart_04_row_name);
/* ---------- chart_04 상품그룹2별 판매금액 통계 [End] ---------- */



/* ---------- chart_05 상품별 판매금액 통계 [Start] ---------- */
$chart_05_sql = "
	SELECT
		w.prd_no,
		(SELECT prd.title FROM product_cms prd WHERE prd.prd_no=w.prd_no) AS prd_name,
		(SELECT prd.display FROM product_cms prd WHERE prd.prd_no=w.prd_no) AS prd_display,
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price
	FROM `work_cms` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY w.prd_no, result_ym
	ORDER BY result_ym ASC
";

$chart_05		   = $default_chart_two_type_list;
$chart_05_day_sum  = $default_chart_two_type_sum_list;
$chart_05_row_sum  = [];
$chart_05_row_name = [];
$chart_05_display  = [];

$chart_05_query = mysqli_query($my_db, $chart_05_sql);
while($chart_05_array = mysqli_fetch_array($chart_05_query))
{
	$chart_05[$chart_05_array['result_ym']][$chart_05_array['prd_no']] = array(
		"deposit" 	=> floor($chart_05_array['dp_price']/1000)
	);
	$chart_05_day_sum[$chart_05_array['result_ym']]["deposit"]	+= floor($chart_05_array['dp_price']/1000);
	$chart_05_row_sum[$chart_05_array['prd_no']]["deposit"]		+= floor($chart_05_array['dp_price']/1000);

	$chart_05_row_name[$chart_05_array['prd_no']] 	= $chart_05_array['prd_name'] ? $chart_05_array['prd_name'] : "(NULL)";
    $chart_05_display[$chart_05_array['prd_no']]    = (empty($chart_05_array['prd_display']) || $chart_05_array['prd_display'] == '2') ? "N" : "Y";
}

arsort($chart_05_row_sum);
ksort($chart_05);

$smarty->assign("chart_05", $chart_05);
$smarty->assign("chart_05_day_sum", $chart_05_day_sum);
$smarty->assign("chart_05_row_sum", $chart_05_row_sum);
$smarty->assign("chart_05_row_name", $chart_05_row_name);
$smarty->assign("chart_05_display", $chart_05_display);
/* ---------- chart_05 상품별 판매금액 통계 [End] ---------- */


/* ---------- chart_06 상품그룹2별 판매금액 평균 통계 [Start] ---------- */
$chart_06_sql = "
	SELECT
		(SELECT prd_cms.k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no) AS k_name_code,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT prd_cms.k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_name,
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price,
		SUM(w.quantity) AS work_count
	FROM `work_cms` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY k_name, result_ym
	ORDER BY result_ym ASC
";

$chart_06		   		= $default_chart_two_type_list;
$chart_06_day_sum  		= $default_chart_two_type_sum_list;
$chart_06_day_avg  		= $default_chart_two_type_sum_list;
$chart_06_day_avg_chk  	= $default_chart_two_type_sum_list;
$chart_06_row_sum  		= [];
$chart_06_row_avg  		= [];
$chart_06_row_avg_chk	= [];
$chart_06_row_name 		= [];

$chart_06_query = mysqli_query($my_db, $chart_06_sql);
while($chart_06_array = mysqli_fetch_array($chart_06_query))
{
	$dp_price = ($chart_06_array['work_count'] > 0) ? ($chart_06_array['dp_price']/$chart_06_array['work_count']) : 0;

	$dp_price_round = round($dp_price/1000, 1);

	$chart_06[$chart_06_array['result_ym']][$chart_06_array['k_name_code']] = array(
		"deposit" 	=> $dp_price_round
	);
	$chart_06_day_sum[$chart_06_array['result_ym']]["deposit"] 		+= $dp_price_round;
	$chart_06_row_sum[$chart_06_array['k_name_code']]["deposit"]	+= $dp_price_round;

	$chart_06_row_name[$chart_06_array['k_name_code']] 	= $chart_06_array['k_name'] ? $chart_06_array['k_name'] : "(NULL)";

	if($chart_06_array['dp_price'] > 0) {
		$chart_06_day_avg_chk[$chart_06_array['result_ym']]["deposit"]++;
		$chart_06_row_avg_chk[$chart_06_array['k_name_code']]["deposit"]++;
	}
}

foreach($chart_06_day_sum as $key => $value)
{
	$chart_06_day_avg[$key]['deposit']  = ($chart_06_day_avg_chk[$key]['deposit'] > 0) ? round(($value['deposit']/$chart_06_day_avg_chk[$key]['deposit']), 1) : '0';
}
foreach($chart_06_row_sum as $key => $value)
{
	$chart_06_row_avg[$key]['deposit']  = ($chart_06_row_avg_chk[$key]['deposit'] > 0) ? round(($value['deposit']/$chart_06_row_avg_chk[$key]['deposit']), 1) : '0';
}

arsort($chart_06_row_avg);
ksort($chart_06);

$smarty->assign("chart_06", $chart_06);
$smarty->assign("chart_06_day_avg", $chart_06_day_avg);
$smarty->assign("chart_06_row_avg", $chart_06_row_avg);
$smarty->assign("chart_06_row_name", $chart_06_row_name);
/* ---------- chart_06 상품그룹2별 판매금액 평균 통계 [End] ---------- */


/* ---------- chart_07 상품별 판매금액 평균 통계 [Start] ---------- */
$chart_07_sql = "
	SELECT
		w.prd_no,
		(SELECT prd.title FROM product_cms prd WHERE prd.prd_no=w.prd_no) AS prd_name,
		(SELECT prd.display FROM product_cms prd WHERE prd.prd_no=w.prd_no) AS prd_display,
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price,
		SUM(w.quantity) AS work_count
	FROM `work_cms` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY w.prd_no, result_ym
	ORDER BY result_ym ASC
";

$chart_07		   		= $default_chart_two_type_list;
$chart_07_day_sum  		= $default_chart_two_type_sum_list;
$chart_07_day_avg  		= $default_chart_two_type_sum_list;
$chart_07_day_avg_chk  	= $default_chart_two_type_sum_list;
$chart_07_row_sum  		= [];
$chart_07_row_avg  		= [];
$chart_07_row_avg_chk	= [];
$chart_07_row_name 		= [];
$chart_07_display 		= [];

$chart_07_query = mysqli_query($my_db, $chart_07_sql);
while($chart_07_array = mysqli_fetch_array($chart_07_query))
{
	$dp_price = ($chart_07_array['work_count'] > 0) ? ($chart_07_array['dp_price']/$chart_07_array['work_count']) : 0;

	$dp_price_round = round($dp_price/1000, 1);

	$chart_07[$chart_07_array['result_ym']][$chart_07_array['prd_no']] = array(
		"deposit" 	=> $dp_price_round
	);
	$chart_07_day_sum[$chart_07_array['result_ym']]["deposit"]	+= $dp_price_round;
	$chart_07_row_sum[$chart_07_array['prd_no']]["deposit"]		+= $dp_price_round;

	$chart_07_row_name[$chart_07_array['prd_no']] 	= $chart_07_array['prd_name'] ? $chart_07_array['prd_name'] : "(NULL)";

	if($chart_07_array['dp_price'] > 0) {
		$chart_07_day_avg_chk[$chart_07_array['result_ym']]["deposit"]++;
		$chart_07_row_avg_chk[$chart_07_array['prd_no']]["deposit"]++;
	}

    $chart_07_display[$chart_07_array['prd_no']] = (empty($chart_07_array['prd_display']) || $chart_07_array['prd_display'] == '2') ? "N" : "Y";
}

foreach($chart_07_day_sum as $key => $value)
{
	$chart_07_day_avg[$key]['deposit']  = ($chart_07_day_avg_chk[$key]['deposit'] > 0) ? round(($value['deposit']/$chart_07_day_avg_chk[$key]['deposit']), 1) : '0';
}
foreach($chart_07_row_sum as $key => $value)
{
	$chart_07_row_avg[$key]['deposit']  = ($chart_07_row_avg_chk[$key]['deposit'] > 0) ? round(($value['deposit']/$chart_07_row_avg_chk[$key]['deposit']), 1) : '0';
}

arsort($chart_07_row_avg);
ksort($chart_07);

$smarty->assign("chart_07", $chart_07);
$smarty->assign("chart_07_day_avg", $chart_07_day_avg);
$smarty->assign("chart_07_row_avg", $chart_07_row_avg);
$smarty->assign("chart_07_row_name", $chart_07_row_name);
$smarty->assign("chart_07_display", $chart_07_display);
/* ---------- chart_07 상품별 판매금액 평균 통계 [End] ---------- */


/* ---------- chart_08 구매처별 상품 판매금액 통계 [Start] ---------- */
$chart_08_sql = "
	SELECT
		w.dp_c_no,
		w.dp_c_name,
		(SELECT c.display FROM company c WHERE c.c_no=w.dp_c_no) as dp_c_display,
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price
	FROM `work_cms` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY w.dp_c_no, result_ym
	ORDER BY result_ym ASC
";

$chart_08		   = $default_chart_one_type_list;
$chart_08_day_sum  = $default_chart_one_type_sum_list;
$chart_08_row_sum  = [];
$chart_08_row_name = [];
$chart_08_display  = [];

$chart_08_query = mysqli_query($my_db, $chart_08_sql);
while($chart_08_array = mysqli_fetch_array($chart_08_query))
{
	$chart_08[$chart_08_array['result_ym']][$chart_08_array['dp_c_no']] = floor($chart_08_array['dp_price']/1000);
	$chart_08_day_sum[$chart_08_array['result_ym']]	+= floor($chart_08_array['dp_price']/1000);
	$chart_08_row_sum[$chart_08_array['dp_c_no']]	+= floor($chart_08_array['dp_price']/1000);
	$chart_08_row_name[$chart_08_array['dp_c_no']] 	= $chart_08_array['dp_c_name'] ? $chart_08_array['dp_c_name'] : "(NULL)";
    $chart_08_display[$chart_08_array['dp_c_no']] 	= (empty($chart_08_array['dp_c_display']) || $chart_08_array['dp_c_display'] != '1') ? "N" : "Y";
}

arsort($chart_08_row_sum);
ksort($chart_08);

$smarty->assign("chart_08", $chart_08);
$smarty->assign("chart_08_day_sum", $chart_08_day_sum);
$smarty->assign("chart_08_row_sum", $chart_08_row_sum);
$smarty->assign("chart_08_row_name", $chart_08_row_name);
$smarty->assign("chart_08_display", $chart_08_display);
/* ---------- chart_08 구매처별 상품 판매금액 통계 [End] ---------- */

/* ---------- chart_10 요일별 상품 판매금액 통계 [Start] ---------- */
$chart_10_sql = "
	SELECT
		DAYOFWEEK(w.order_date) AS date_n,
		SUBSTR('일월화수목금토', DAYOFWEEK(w.order_date), 1) AS date_name,
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price
	FROM `work_cms` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY date_n, result_ym
	ORDER BY result_ym ASC
";

$chart_10		   = $default_chart_two_type_list;
$chart_10_day_sum  = $default_chart_two_type_sum_list;
$chart_10_row_sum  = [];
$chart_10_row_name = [];

$chart_10_query = mysqli_query($my_db, $chart_10_sql);
while($chart_10_array = mysqli_fetch_array($chart_10_query))
{
	$chart_10[$chart_10_array['result_ym']][$chart_10_array['date_n']] = array(
		"deposit" 	=> floor($chart_10_array['dp_price']/1000)
	);
	$chart_10_day_sum[$chart_10_array['result_ym']]["deposit"]	+= floor($chart_10_array['dp_price']/1000);
	$chart_10_row_sum[$chart_10_array['date_n']]["deposit"]		+= floor($chart_10_array['dp_price']/1000);

	$chart_10_row_name[$chart_10_array['date_n']] = $chart_10_array['date_name'] ? $chart_10_array['date_name'] : "(NULL)";
}

ksort($chart_10_row_sum);
ksort($chart_10);

$smarty->assign("chart_10", $chart_10);
$smarty->assign("chart_10_day_sum", $chart_10_day_sum);
$smarty->assign("chart_10_row_sum", $chart_10_row_sum);
$smarty->assign("chart_10_row_name", $chart_10_row_name);
/* ---------- chart_10 요일별 상품 판매금액 통계 [End] ---------- */

/* ---------- chart_11 시간대별 상품 판매금액 통계 [Start] ---------- */
$chart_11_sql = "
	SELECT
		LPAD(HOUR(w.order_date),2,'0') AS `hour`,
		CONCAT(LPAD(HOUR(w.order_date),2,'0'),' ~ ',LPAD(HOUR(w.order_date)+1,2,'0'),'시') AS hour_name,
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price
	FROM `work_cms` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY `hour`, result_ym
	ORDER BY result_ym ASC
";

$chart_11		   = $default_chart_two_type_list;
$chart_11_day_sum  = $default_chart_two_type_sum_list;
$chart_11_row_sum  = [];
$chart_11_row_name = [];

$chart_11_query = mysqli_query($my_db, $chart_11_sql);
while($chart_11_array = mysqli_fetch_array($chart_11_query))
{
	$chart_11[$chart_11_array['result_ym']][$chart_11_array['hour']] = array(
		"deposit" 	=> floor($chart_11_array['dp_price']/1000)
	);
	$chart_11_day_sum[$chart_11_array['result_ym']]["deposit"]	+= floor($chart_11_array['dp_price']/1000);
	$chart_11_row_sum[$chart_11_array['hour']]["deposit"]		+= floor($chart_11_array['dp_price']/1000);

	$chart_11_row_name[$chart_11_array['hour']] = $chart_11_array['hour_name'] ? $chart_11_array['hour_name'] : "(NULL)";
}

ksort($chart_11_row_sum);
ksort($chart_11);

$smarty->assign("chart_11", $chart_11);
$smarty->assign("chart_11_day_sum", $chart_11_day_sum);
$smarty->assign("chart_11_row_sum", $chart_11_row_sum);
$smarty->assign("chart_11_row_name", $chart_11_row_name);
/* ---------- chart_11 시간대별 상품 판매금액 통계 [End] ---------- */


/* ---------- chart_12 지역별 상품 판매금액 통계 [Start] ---------- */
$chart_12_sql = "
	SELECT
		(SELECT LOWER(zip.state_eng) as state FROM state_postcode zip WHERE zip.postcode = REPLACE(w.zip_code,'-','') LIMIT 1) as `state`,
		(SELECT zip.state FROM state_postcode zip WHERE zip.postcode = REPLACE(w.zip_code,'-','') LIMIT 1) as state_name,
		{$add_date_column} AS result_ym,
		SUM(w.unit_price-w.coupon_price) AS dp_price
	FROM `work_cms` w 
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY `state`, result_ym
	ORDER BY result_ym ASC
";

$chart_12		   = $default_chart_two_type_list;
$chart_12_day_sum  = $default_chart_two_type_sum_list;
$chart_12_row_sum  = [];
$chart_12_row_name = [];

$chart_12_query = mysqli_query($my_db, $chart_12_sql);
while($chart_12_array = mysqli_fetch_array($chart_12_query))
{

	$chart_12[$chart_12_array['result_ym']][$chart_12_array['state']] = array(
		"deposit" => floor($chart_12_array['dp_price'] / 1000)
	);
	$chart_12_day_sum[$chart_12_array['result_ym']]["deposit"] += floor($chart_12_array['dp_price'] / 1000);
	$chart_12_row_sum[$chart_12_array['state']]["deposit"] += floor($chart_12_array['dp_price'] / 1000);

	$chart_12_row_name[$chart_12_array['state']] = $chart_12_array['state_name'] ? $chart_12_array['state_name'] : "(NULL)";

}

arsort($chart_12_row_sum);
ksort($chart_12);

$smarty->assign("chart_12", $chart_12);
$smarty->assign("chart_12_day_sum", $chart_12_day_sum);
$smarty->assign("chart_12_row_sum", $chart_12_row_sum);
$smarty->assign("chart_12_row_name", $chart_12_row_name);
/* ---------- chart_12 지역별 상품 판매금액 통계 [End] ---------- */

$smarty->display('stats/wise_work_02b.html');

?>
