<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/stats.php');
require('../inc/model/MyQuick.php');

$dir_location = "../";
$smarty->assign("dir_location", $dir_location);

# Navigation & My Quick
$nav_prd_no  = "125";
$nav_title   = "업무 커스텀 통계";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 조회기간
$sch_date_type  = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "set";
$sch_s_date     = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d', strtotime("-1 month"));
$sch_e_date     = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');

$one_week_day  = date('Y-m-d', strtotime("-1 week"));
$two_week_day  = date('Y-m-d', strtotime("-2 week"));
$one_month_day = date('Y-m-d', strtotime("-1 month"));

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);
$smarty->assign('one_week_day', $one_week_day);
$smarty->assign('two_week_day', $two_week_day);
$smarty->assign('one_month_day', $one_month_day);

# 날짜조회
$add_date_where = "";
if($sch_date_type == 'all')
{
    $sch_s_date     = date('Y-m-d H:i:s', strtotime('2019-03-01 00:00:00'));
    $sch_e_date     = date('Y-m-d')." 23:59:59";
    $add_date_where = " AND (w.task_run_regdate >= '{$sch_s_date}' AND w.task_run_regdate <= '{$sch_e_date}')";
}
else
{
    $add_date_where = " AND (w.task_run_regdate >= '$sch_s_date' AND w.task_run_regdate <= '$sch_e_date') ";
}

# X축(day: 날짜, work: 업무, task_req: 업무요청자, task_run: 업무처리자, dp_comp: 입금업체, wd_comp: 입금업체, task_req_eval: 업무요청자 평가, task_run_eval: 업무처리자 평가)
$sch_x_type       = isset($_GET['sch_x_type']) ? $_GET['sch_x_type'] : "day";
$sch_x_type_main  = isset($_GET['sch_x_type_main']) ? $_GET['sch_x_type_main'] : "";
$sch_x_type_sub   = isset($_GET['sch_x_type_sub']) ? $_GET['sch_x_type_sub'] : "";
$sch_x_type_sub2  = isset($_GET['sch_x_type_sub2']) ? $_GET['sch_x_type_sub2'] : "";

if($sch_x_type == 'day' && $sch_x_type_main == ''){
    $sch_x_type_main = '1';
}

$sch_x_type_list  = getSchType($my_db, $sch_s_date, $sch_e_date, $sch_x_type, $sch_x_type_main, $sch_x_type_sub, $sch_x_type_sub2, $type_day, $work_list, $work_prd_list, $sch_team_name_list, $type_dp_comp, $type_wd_comp, $type_task_eval);

$x_type_title      = $sch_x_type_list['type_title'];
$sch_x_depth       = $sch_x_type_list['sch_depth'];
$x_type_main_list  = $sch_x_type_list['type_main_list'];
$x_type_sub_list   = $sch_x_type_list['type_sub_list'];
$x_type_sub2_list  = $sch_x_type_list['type_sub2_list'];
$add_x_type_where  = $sch_x_type_list['add_type_where'];
$add_x_type_column = "{$sch_x_type_list['add_type_column']} as x_type, {$sch_x_type_list['add_type_name_column']} as x_type_name";
$add_x_type_name   = "DISTINCT {$sch_x_type_list['add_type_column']} as x_type, {$sch_x_type_list['add_type_name_column']} as x_type_name";
$add_x_type_empty  = $sch_x_type_list['add_type_empty'];

if($sch_x_type == 'day'){
    $add_date_where = "";
}

$smarty->assign('x_type_list', $work_type_list);
$smarty->assign('x_type_main_list', $x_type_main_list);
$smarty->assign('x_type_sub_list', $x_type_sub_list);
$smarty->assign('x_type_sub2_list', $x_type_sub2_list);
$smarty->assign('sch_x_type', $sch_x_type);
$smarty->assign('sch_x_type_main', $sch_x_type_main);
$smarty->assign('sch_x_type_sub', $sch_x_type_sub);
$smarty->assign('sch_x_type_sub2', $sch_x_type_sub2);
$smarty->assign('sch_x_depth', $sch_x_depth);


# Y축(day: 날짜, work: 업무, task_req: 업무요청자, task_run: 업무처리자, dp_comp: 입금업체, wd_comp: 입금업체, task_req_eval: 업무요청자 평가, task_run_eval: 업무처리자 평가)
$sch_y_type       = isset($_GET['sch_y_type']) ? $_GET['sch_y_type'] : "work";
$sch_y_type_main  = isset($_GET['sch_y_type_main']) ? $_GET['sch_y_type_main'] : "";
$sch_y_type_sub   = isset($_GET['sch_y_type_sub']) ? $_GET['sch_y_type_sub'] : "";
$sch_y_type_sub2  = isset($_GET['sch_y_type_sub2']) ? $_GET['sch_y_type_sub2'] : "";

if($sch_y_type == 'day' && $sch_y_type_main == ''){
    $sch_y_type_main = '1';
}

$sch_y_type_list  = getSchType($my_db, $sch_s_date, $sch_e_date, $sch_y_type, $sch_y_type_main, $sch_y_type_sub, $sch_y_type_sub2, $type_day, $work_list, $work_prd_list, $sch_team_name_list, $type_dp_comp, $type_wd_comp, $type_task_eval);

$y_type_title      = $sch_y_type_list['type_title'];
$sch_y_depth       = $sch_y_type_list['sch_depth'];
$y_type_main_list  = $sch_y_type_list['type_main_list'];
$y_type_sub_list   = $sch_y_type_list['type_sub_list'];
$y_type_sub2_list  = $sch_y_type_list['type_sub2_list'];
$add_y_type_where  = $sch_y_type_list['add_type_where'];
$add_y_type_column = "{$sch_y_type_list['add_type_column']} as y_type, {$sch_y_type_list['add_type_name_column']} as y_type_name";
$add_y_type_name   = "DISTINCT {$sch_y_type_list['add_type_column']} as y_type, {$sch_y_type_list['add_type_name_column']} as y_type_name";
$add_y_type_empty  = $sch_y_type_list['add_type_empty'];

if($sch_y_type == 'day'){
    $add_date_where = "";
}

$smarty->assign('y_type_list', $work_type_list);
$smarty->assign('y_type_main_list', $y_type_main_list);
$smarty->assign('y_type_sub_list', $y_type_sub_list);
$smarty->assign('y_type_sub2_list', $y_type_sub2_list);
$smarty->assign('sch_y_type', $sch_y_type);
$smarty->assign('sch_y_type_main', $sch_y_type_main);
$smarty->assign('sch_y_type_sub', $sch_y_type_sub);
$smarty->assign('sch_y_type_sub2', $sch_y_type_sub2);
$smarty->assign('sch_y_depth', $sch_y_depth);


# Z축 비교, 비율
$sch_chart_type = isset($_GET['sch_chart_type']) ? $_GET['sch_chart_type'] : "compare";
$smarty->assign('sch_chart_type', $sch_chart_type);

# Z축(1.건(업무번호 w_no), 2.건(수량), 3.시간(work_time), 4: 금액(기여매출액), 5: 금액(판매가)
$sch_z_type         = isset($_GET['sch_z_type']) ? $_GET['sch_z_type'] : "1";
$add_z_type_column  = "SUM(w.w_no)";
$z_type_title       = "건(업무번호)";
$z_type_value_title = "건";


switch($sch_z_type){
    case '1':
        $add_z_type_column  = "count(w.w_no)";
        $z_type_title       = "건(업무번호)";
        $z_type_value_title = "건";
        break;
    case '2':
        $add_z_type_column  = "SUM(w.quantity)";
        $z_type_title       = "건(수량)";
        $z_type_value_title = "건";
        break;
    case '3':
        $add_z_type_column  = "SUM(w.work_time)";
        $z_type_title       = "시간(work_time)";
        $z_type_value_title = "시간";
        break;
    case '4':
        $add_z_type_column  = "SUM(IF(w.service_apply=2,w.selling_price,0))";
        $z_type_title       = "금액(기여매출액)";
        $z_type_value_title = "원";
        break;
    case '5':
        $add_z_type_column  = "SUM(IF(w.service_apply=1,w.selling_price,0))";
        $z_type_title       = "금액(판매가)";
        $z_type_value_title = "원";
        break;

}

$smarty->assign('z_type_list', $work_z_type_list);
$smarty->assign('sch_z_type', $sch_z_type);


# 정렬 : 날짜, 요일, 시간 일때는 type || 상품, 업체담당자, 구매처, 지역 일때는 type_name
$add_order_by = "data_sum DESC";
$sch_ord_type = isset($_GET['sch_ord_type']) ? $_GET['sch_ord_type'] : "0";
$smarty->assign('ord_type_list', $ord_type_list);
$smarty->assign('sch_ord_type', $sch_ord_type);

$add_x_order_by = "x_type ASC";
$add_y_order_by = "data_sum DESC";

switch($sch_ord_type){
    case '0':
        $add_order_by = $add_y_order_by = "data_sum DESC";
        break;
    case '1':
        $add_order_by = $add_y_order_by = "data_sum ASC";
        break;
    case '2':
        if($sch_x_type == 'day')
        {
            $add_x_order_by = "x_type DESC";
            $add_order_by = "x_type DESC, data_sum DESC";
        }else{
            $add_x_order_by = "x_type_name DESC";
            $add_order_by = "x_type_name DESC, data_sum DESC";
        }
        break;
    case '3':
        if($sch_x_type == 'day')
        {
            $add_x_order_by = "x_type ASC";
            $add_order_by = "x_type ASC, data_sum DESC";
        }else{
            $add_x_order_by = "x_type_name ASC";
            $add_order_by = "x_type_name ASC, data_sum DESC";
        }
        break;
    case '4':
        if($sch_y_type == 'day'){
            $add_y_order_by = "y_type DESC";
            $add_order_by = "y_type DESC, data_sum DESC";
        }else{
            $add_y_order_by = "y_type_name DESC";
            $add_order_by = "y_type_name DESC, data_sum DESC";
        }
        break;
    case '5':
        if($sch_y_type == 'day'){
            $add_y_order_by = "y_type ASC";
            $add_order_by = "y_type ASC, data_sum DESC";
        }else{
            $add_y_order_by = "y_type_name ASC";
            $add_order_by = "y_type_name ASC, data_sum DESC";
        }
        break;
    default: $add_order_by = "data_sum DESC"; break;
}

# Commerce List 초기화
$commerce_list      = [];
$commerce_sum_list  = [];

# X축 Name List
$x_name_list        = [];
$x_name_legend_list = [];
$commerce_x_sql     = "
    SELECT
        {$add_x_type_name}
    FROM work w
    WHERE 1=1 AND w.work_state = '6'
        {$add_date_where}
        {$add_x_type_where}
        {$add_y_type_where}
    ORDER BY {$add_x_order_by}
";

$commerce_x_query = mysqli_query($my_db, $commerce_x_sql);
while($commerce_x = mysqli_fetch_assoc($commerce_x_query))
{
    if($commerce_x['x_type'] == '' || $commerce_x['x_type'] == '0')
    {
        $commerce_x['x_type']      = '0';
        $commerce_x['x_type_name'] = $add_x_type_empty;
    }

    $commerce_sum_list[$commerce_x['x_type']] = 0;
    $x_name_list[$commerce_x['x_type']] = $commerce_x['x_type_name'];
}

# Y축 Name List
$y_name_list    = [];
$y_legend_list  = [];
$commerce_y_sql = "
    SELECT
        {$add_y_type_name},
        {$add_z_type_column} as data_sum
    FROM work w
    WHERE 1=1
        {$add_date_where}
        {$add_x_type_where}
        {$add_y_type_where}
    GROUP BY y_type
    ORDER BY {$add_y_order_by}
";

$commerce_y_query = mysqli_query($my_db, $commerce_y_sql);
while($commerce_y = mysqli_fetch_assoc($commerce_y_query))
{
    if($commerce_y['y_type'] == '' || $commerce_y['y_type'] == '0')
    {
        $commerce_y['y_type'] = '0';
        $commerce_y['y_type_name'] = $add_y_type_empty;
    }

    if($x_name_list) {
        foreach ($x_name_list as $x_type => $x_type_name) {
            $commerce_list[$commerce_y['y_type']][$x_type] = 0;
        }
    }

    $y_name_list[$commerce_y['y_type']] = $commerce_y['y_type_name'];
    $y_legend_list[] = array('type' => $commerce_y['y_type'], 'name' => $commerce_y['y_type_name']);
}

$commerce_sql = "
    SELECT
        {$add_x_type_column},
        {$add_y_type_column},
        {$add_z_type_column} as data_sum
    FROM work w
    WHERE 1=1
        {$add_date_where}
        {$add_x_type_where}
        {$add_y_type_where}
    GROUP BY y_type, x_type
    ORDER BY {$add_order_by}
";
$commerce_query = mysqli_query($my_db, $commerce_sql);

while($commerce = mysqli_fetch_assoc($commerce_query))
{
    if($commerce['y_type'] == '0' OR $commerce['y_type'] == ''){
        $commerce['y_type'] = '0';
    }

    $commerce_list[$commerce['y_type']][$commerce['x_type']] += $commerce['data_sum'];
    $commerce_sum_list[$commerce['x_type']] += $commerce['data_sum'];
}

$x_idx = 0;
$max   = 0;
$commerce_compare_chart_list = [];
$commerce_percent_chart_list = [];
$commerce_percent_chart_sort_list = [];

if($x_name_list)
{
    foreach($x_name_list as $x_type => $x_item)
    {
        $y_idx = 0;
        foreach($y_legend_list as $y_item)
        {
            $y_type = $y_item['type'];
            $value  = $commerce_list[$y_type][$x_type];
            if($max < $value){
                $max = $value;
            }
            $commerce_compare_chart_list[] = array($x_idx, $y_idx, $value);
            $commerce_percent_chart_list[$y_type]['title']  = $y_item['name'];
            $commerce_percent_chart_list[$y_type]['data'][] = $value;
            $y_idx++;
        }
        $x_idx++;

        $x_legend_list[] = $x_item;
    }
}

if($commerce_percent_chart_list)
{
    foreach ($commerce_percent_chart_list as $chart_list) {
        $commerce_percent_chart_sort_list[] = $chart_list;
    }
}

$commerce_total = empty($commerce_list) ? 0 : count($commerce_list);

$smarty->assign('x_name_list', $x_name_list);
$smarty->assign('y_name_list', $y_name_list);
$smarty->assign('x_legend_list', json_encode($x_legend_list));
$smarty->assign('y_legend_list', json_encode($y_legend_list));
$smarty->assign('commerce_total', $commerce_total);
$smarty->assign('commerce_list', $commerce_list);
$smarty->assign('commerce_sum_list', $commerce_sum_list);
$smarty->assign('commerce_compare_chart_list', json_encode($commerce_compare_chart_list));
$smarty->assign('commerce_percent_chart_list', json_encode($commerce_percent_chart_sort_list));
$smarty->assign('max_value', $max);
$smarty->assign('x_type_title', $x_type_title);
$smarty->assign('y_type_title', $y_type_title);
$smarty->assign('z_type_title',$z_type_title);
$smarty->assign('z_type_value_title',$z_type_value_title);
$smarty->assign('curdate', date('Y-m-d'));

$smarty->display('stats/wise_work_custom.html');

function getSchType($my_db, $sch_s_date, $sch_e_date, $sch_type, $sch_type_main, $sch_type_sub, $sch_type_sub2, $type_day, $work_list, $work_prd_list, $team_list, $type_dp, $type_wd, $type_task_eval)
{
    $type_main_list = $type_sub_list = $type_sub2_list = [];
    $sch_depth      = 0;
    $type_title     = "";
    $add_type_where = $add_type_column = $add_type_name_column = $add_type_empty = "";

    switch($sch_type)
    {
        case 'day':
            $type_title  = "날짜";
            $type_main_list = $type_day;
            $sch_depth = 1;

            if ($sch_type_main == '1') //월간
            {
                $sch_s_date     = date('Y-m', strtotime($sch_s_date)) . "-01 00:00:00"; //월간
                $sch_e_date     = date('Y-m', strtotime($sch_e_date)) . "-31 23:59:59"; //월간

                $add_type_where       = " AND (w.task_run_regdate >= '$sch_s_date' AND w.task_run_regdate <= '$sch_e_date') ";
                $add_type_column      = "DATE_FORMAT(w.task_run_regdate, '%Y%m')";
                $add_type_name_column = "DATE_FORMAT(w.task_run_regdate, '%Y-%m')";
            } elseif ($sch_type_main == '2') //주간
            {
                $s_w_date_no = date('w', strtotime($sch_s_date));
                $e_w_date_no = 7 - (date('w', strtotime($sch_e_date)) + 1);
                $sch_s_date = date('Y-m-d', strtotime("$sch_s_date -{$s_w_date_no} day")) . " 00:00:00";
                $sch_e_date = date('Y-m-d', strtotime("{$sch_e_date} +{$e_w_date_no} day")) . " 23:59:59";

                $add_type_where        = " AND (w.task_run_regdate >= '$sch_s_date' AND w.task_run_regdate <= '$sch_e_date') ";
                $add_type_column       = "DATE_FORMAT(DATE_SUB(w.task_run_regdate, INTERVAL (IF(DAYOFWEEK(w.task_run_regdate)=1,8,DAYOFWEEK(w.task_run_regdate))-2) DAY), '%Y%m%d')";
                $add_type_name_column  = "CONCAT(DATE_FORMAT(DATE_SUB(w.task_run_regdate, INTERVAL (IF(DAYOFWEEK(w.task_run_regdate)=1,8,DAYOFWEEK(w.task_run_regdate))-2) DAY), '%Y-%m-%d'),' ~ ', DATE_FORMAT(DATE_SUB(w.task_run_regdate, INTERVAL(IF(DAYOFWEEK(w.task_run_regdate)=1,8,DAYOFWEEK(w.task_run_regdate))-8) DAY), '%Y-%m-%d'))";
            } else //일간
            {
                $sch_s_date = $sch_s_date . " 00:00:00";
                $sch_e_date = $sch_e_date . " 23:59:59";

                $add_type_where        = " AND (w.task_run_regdate >= '$sch_s_date' AND w.task_run_regdate <= '$sch_e_date') ";
                $add_type_column       = "DATE_FORMAT(w.task_run_regdate, '%Y%m%d')";
                $add_type_name_column  = "DATE_FORMAT(w.task_run_regdate, '%Y-%m-%d')";
            }
            break;

        case 'work':
            $type_title  = "업무";
            $prd_g1_list = $prd_g2_list = $prd_g3_list = [];
            foreach($work_list as $key => $prd_data)
            {
                if(!$key){
                    foreach($prd_data as $prd){
                        $prd_g1_list[$prd['k_name_code']] = $prd['k_name'];
                    }
                }else{
                    foreach($prd_data as $prd){
                        $prd_g2_list[$key][$prd['k_name_code']] = $prd['k_name'];
                    }
                }
            }

            if(!empty($sch_type_sub) && isset($work_prd_list[$sch_type_sub]))
            {
                foreach($work_prd_list[$sch_type_sub] as $prd){
                    $prd_g3_list[$prd['prd_no']] = $prd['title'];
                }
            }

            $type_main_list = $prd_g1_list;
            $type_sub_list  = isset($prd_g2_list[$sch_type_main]) ? $prd_g2_list[$sch_type_main] : [];
            $type_sub2_list = $prd_g3_list;

            $sch_depth = 3;

            if(!empty($sch_type_sub2)){
                $add_type_where         = " AND w.prd_no='{$sch_type_sub2}'";
                $add_type_column        = "w.prd_no";
                $add_type_name_column   = "(SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no LIMIT 1)";
            }elseif(!empty($sch_type_sub)){
                $add_type_where         = " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.k_name_code='{$sch_type_sub}')";
                $add_type_column        = "w.prd_no";
                $add_type_name_column   = "(SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no LIMIT 1)";

            }elseif(!empty($sch_type_main)){
                $add_type_where         = " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_type_main}'))";
                $add_type_column        = "(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no LIMIT 1)";
                $add_type_name_column   = "(SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no) LIMIT 1)";
            }else{
                $add_type_column        = "(SELECT k.k_parent FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no) LIMIT 1)";
                $add_type_name_column   = "(SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) LIMIT 1)";
            }
            $add_type_empty = "상품없음";

            break;

        case 'staff':
            $type_title    = "업무담당자";
            $sch_team_list = $sch_staff_list = [];
            foreach($team_list as $team_code => $team_name)
            {
                $sch_team_list[$team_code] = $team_name;
            }

            if(!empty($sch_type_main) && $sch_type_main != 'all')
            {
                $sch_team_code_where = getTeamWhere($my_db, $sch_type_main);
                $add_type_where = " AND w.team IN ({$sch_team_code_where})";

                $sch_team_code_list_val = explode(",", $sch_team_code_where);

                $staff_team_where_list = [];
                foreach($sch_team_code_list_val as $sch_team_code){
                    $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
                }

                $staff_team_where = implode(" OR ", $staff_team_where_list);
                $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
                $staff_result=mysqli_query($my_db,$staff_sql);
                while($staff=mysqli_fetch_array($staff_result)) {
                    $sch_staff_list[$staff['s_no']] = trim($staff['s_name']);
                }
            }

            $type_main_list = $sch_team_list;
            $type_sub_list  = $sch_staff_list;
            $sch_depth = 2;

            if(!empty($sch_type_sub)){
                $add_type_where .= " AND w.s_no = '{$sch_type_sub}'";
            }
            $add_type_column        = "w.s_no";
            $add_type_name_column   = "(SELECT s.s_name FROM staff s WHERE s.s_no = w.s_no LIMIT 1)";
            $add_type_empty         = "직원없음";

            break;
            
        case 'task_req':
            $type_title    = "업무요청자";
            $sch_team_list = $sch_staff_list = [];
            foreach($team_list as $team_code => $team_name)
            {
                $sch_team_list[$team_code] = $team_name;
            }

            if(!empty($sch_type_main) && $sch_type_main != 'all')
            {
                $sch_team_code_where = getTeamWhere($my_db, $sch_type_main);
                $add_type_where = " AND w.task_req_team IN ({$sch_team_code_where})";

                $sch_team_code_list_val = explode(",", $sch_team_code_where);

                $staff_team_where_list = [];
                foreach($sch_team_code_list_val as $sch_team_code){
                    $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
                }

                $staff_team_where = implode(" OR ", $staff_team_where_list);
                $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
                $staff_result=mysqli_query($my_db,$staff_sql);
                while($staff=mysqli_fetch_array($staff_result)) {
                    $sch_staff_list[$staff['s_no']] = trim($staff['s_name']);
                }
            }

            $type_main_list = $sch_team_list;
            $type_sub_list  = $sch_staff_list;
            $sch_depth = 2;

            if(!empty($sch_type_sub)){
                $add_type_where .= " AND w.task_req_s_no = '{$sch_type_sub}'";
            }
            $add_type_column        = "w.task_req_s_no";
            $add_type_name_column   = "(SELECT s.s_name FROM staff s WHERE s.s_no = w.task_req_s_no LIMIT 1)";
            $add_type_empty         = "직원없음";

            break;

        case 'task_run':
            $type_title    = "업무처리자";
            $sch_team_list = $sch_staff_list = [];
            foreach($team_list as $team_code => $team_name)
            {
                $sch_team_list[$team_code] = $team_name;
            }

            if(!empty($sch_type_main) && $sch_type_main != 'all')
            {
                $sch_team_code_where = getTeamWhere($my_db, $sch_type_main);
                $add_type_where = " AND w.task_run_team IN ({$sch_team_code_where})";

                $sch_team_code_list_val = explode(",", $sch_team_code_where);

                $staff_team_where_list = [];
                foreach($sch_team_code_list_val as $sch_team_code){
                    $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
                }

                $staff_team_where = implode(" OR ", $staff_team_where_list);
                $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
                $staff_result=mysqli_query($my_db,$staff_sql);
                while($staff=mysqli_fetch_array($staff_result)) {
                    $sch_staff_list[$staff['s_no']] = trim($staff['s_name']);
                }
            }

            $type_main_list = $sch_team_list;
            $type_sub_list  = $sch_staff_list;
            $sch_depth = 2;

            if(!empty($sch_type_sub)){
                $add_type_where .= " AND w.task_run_s_no = '{$sch_type_sub}'";
            }
            $add_type_column        = "w.task_run_s_no";
            $add_type_name_column   = "(SELECT s.s_name FROM staff s WHERE s.s_no = w.task_run_s_no LIMIT 1)";
            $add_type_empty         = "직원없음";

            break;

        case 'wd_comp':
            $type_title     = "출금업체";
            $type_main_list = $type_wd;
            $sch_depth      = 1;

            if(!empty($sch_type_main)){
                $add_type_where = " AND w.wd_c_no = '{$sch_type_main}'";
            }

            $add_type_column      = "w.wd_c_no";
            $add_type_name_column = "(SELECT c.c_name FROM company c WHERE c.c_no = w.wd_c_no LIMIT 1)";
            $add_type_empty       = "출금업체없음";

            break;

        case 'dp_comp':
            $type_title     = "입금업체";
            $type_main_list = $type_dp;
            $sch_depth      = 1;

            if(!empty($sch_type_main)){
                $add_type_where = " AND w.dp_c_no = '{$sch_type_main}'";
            }

            $add_type_column      = "w.dp_c_no";
            $add_type_name_column = "(SELECT c.c_name FROM company c WHERE c.c_no = w.dp_c_no LIMIT 1)";
            $add_type_empty       = "입금업체없음";

            break;

        case 'task_req_eval':
            $type_title     = "업무요청자 평가";
            $type_main_list = $type_task_eval;
            $sch_depth      = 1;

            if(!empty($sch_type_main)){
                $add_type_where = " AND w.req_evaluation = '{$sch_type_main}'";
            }

            $add_type_column      = "w.req_evaluation";
            $add_type_name_column = "w.req_evaluation";
            $add_type_empty       = "평가없음";

            break;

        case 'task_run_eval':
            $type_title     = "업무처리자 평가";
            $type_main_list = $type_task_eval;
            $sch_depth      = 1;

            if(!empty($sch_type_main)){
                $add_type_where = " AND w.evaluation = '{$sch_type_main}'";
            }

            $add_type_column      = "w.evaluation";
            $add_type_name_column = "w.evaluation";
            $add_type_empty       = "평가없음";

            break;
    }

    return array(
        "type_title"             => $type_title,
        "type_main_list"         => $type_main_list,
        "type_sub_list"          => $type_sub_list,
        "type_sub2_list"         => $type_sub2_list,
        "sch_depth"              => $sch_depth,
        "add_type_where"         => $add_type_where,
        "add_type_column"        => $add_type_column,
        "add_type_name_column"   => $add_type_name_column,
        "add_type_empty"         => $add_type_empty
    );
}
?>
