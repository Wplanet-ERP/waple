<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/stats.php');
require('../inc/model/MyQuick.php');

// 접근 제한
if (!$session_s_no){ // 외주업체 로그인 시
    $smarty->display('access_company_error.html');
    exit;
}

//Freelancer로 로그인 한 경우
if($session_staff_state == '2'){
    $smarty->display('access_error.html');
    exit;
}

$dir_location = "../";
$smarty->assign("dir_location", $dir_location);

# Navigation & My Quick
$nav_prd_no  = "123";
$nav_title   = "업무 통계";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
// [상품(업무) 종류]
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);

$prd_g1_list = $prd_g2_list = $prd_g3_list = [];

foreach($work_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}

$prd_g2_list  = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
$sch_prd_list = isset($work_prd_list[$sch_prd_g2]) ? $work_prd_list[$sch_prd_g2] : [];
$sch_prd_name = isset($work_name_list[$sch_prd_g1]) ? $work_name_list[$sch_prd_g1] : "전체";
$sch_prd_name .= isset($work_name_list[$sch_prd_g2]) ? " > ".$work_name_list[$sch_prd_g2] : " > 전체";
$sch_prd_name .= isset($work_prd_name_list[$sch_prd]) ? " > ".$work_prd_name_list[$sch_prd] : " > 전체";

$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);
$smarty->assign("sch_prd_name", $sch_prd_name);


// [조회 단위] & [조회 기간]
$sch_kind		    = isset($_GET['sch_kind']) ? $_GET['sch_kind'] : "2";
$sch_kind_type      = "month";
$today_s_w		    = date('w')-1;
$sch_year_s_date 	= isset($_GET['sch_year_s_date']) ? $_GET['sch_year_s_date'] : date("Y", strtotime("-3 years")); // 연간
$sch_year_e_date 	= isset($_GET['sch_year_e_date']) ? $_GET['sch_year_e_date'] : date("Y"); // 연간
$sch_mon_s_date 	= isset($_GET['sch_mon_s_date']) ? $_GET['sch_mon_s_date'] : date("Y-m", strtotime("-6 months")); // 월간
$sch_mon_e_date 	= isset($_GET['sch_mon_e_date']) ? $_GET['sch_mon_e_date'] : date("Y-m"); // 월간
$sch_week_s_date 	= isset($_GET['sch_week_s_date']) ? $_GET['sch_week_s_date'] : date("Y-m-d",strtotime("-{$today_s_w} day")); // 주간
$sch_week_e_date 	= isset($_GET['sch_week_e_date']) ? $_GET['sch_week_e_date'] : date("Y-m-d", strtotime("{$sch_week_s_date} +6 day")); // 주간
$sch_s_date 	    = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date("Y-m-d", strtotime("-6 day")); // 일간
$sch_e_date 	    = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date("Y-m-d"); // 일간

switch ($sch_kind){
    case "1":
        $sch_kind_type = "year";
        break;
    case "2":
        $sch_kind_type = "month";
        break;
    case "3":
        $sch_kind_type = "week";
        break;
    case "4":
        $sch_kind_type = "day";
        break;
}

$smarty->assign("sch_kind", $sch_kind);
$smarty->assign("sch_kind_type", $sch_kind_type);
$smarty->assign("sch_year_s_date", $sch_year_s_date);
$smarty->assign("sch_year_e_date", $sch_year_e_date);
$smarty->assign("sch_mon_s_date", $sch_mon_s_date);
$smarty->assign("sch_mon_e_date", $sch_mon_e_date);
$smarty->assign("sch_week_s_date", $sch_week_s_date);
$smarty->assign("sch_week_e_date", $sch_week_e_date);
$smarty->assign("sch_s_date", $sch_s_date);
$smarty->assign("sch_e_date", $sch_e_date);


// [담당자 기준] : 업체, 팀, 담당자
$sch_s_no_kind		= isset($_GET['sch_s_no_kind']) ? $_GET['sch_s_no_kind'] : "c_s_no";
$sch_s_no_kind_name = "업체 담당자";

if($sch_s_no_kind == "req_s_no"){
    $sch_s_no_kind_name = "업무 요청자";
}elseif($sch_s_no_kind == "run_s_no"){
    $sch_s_no_kind_name = "업무 처리자";
}

// [부서 > 담당자]
$sch_team	= isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no	= isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";

$sch_team_code_where = "";
if(!empty($sch_team) && $sch_team != "all"){
    $sch_team_code_where = getTeamWhere($my_db, $sch_team);
    $sch_s_name .= isset($sch_team_name_list[$sch_team]) ? $sch_team_name_list[$sch_team] : "";
}else{
    $sch_s_name .= " 전체";
}

$staff_list = [];
$staff_name = "";
if($sch_team_code_where)
{
    $staff_team_where 		= "";
    $staff_team_where_list 	= [];

    $sch_team_code_list_val = explode(",", $sch_team_code_where);

    foreach($sch_team_code_list_val as $sch_team_code){
        $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
    }

    if($staff_team_where_list)
    {
        $staff_team_where = implode(" OR ", $staff_team_where_list);
        $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
        $staff_result=mysqli_query($my_db,$staff_sql);
        while($staff=mysqli_fetch_array($staff_result)) {
            $staff_list[]=array(
                "s_no"=>trim($staff['s_no']),
                "s_name"=>trim($staff['s_name'])
            );

            if(!!$sch_s_no && $sch_s_no == $staff['s_no']){
                $staff_name = $staff['s_name'];
            }
        }
    }
}

if(!!$sch_s_no && !empty($staff_name)){
    $sch_s_name .= " > ".$staff_name;
}else{
    $sch_s_name .= " > 전체";
}

$smarty->assign("sch_s_no_kind", $sch_s_no_kind);
$smarty->assign("sch_s_no_kind_name", $sch_s_no_kind_name);
$smarty->assign("sch_s_name", $sch_s_name);
$smarty->assign("sch_team", $sch_team);
$smarty->assign("sch_s_no", $sch_s_no);
$smarty->assign("staff_list", $staff_list);
$smarty->assign("sch_team_list", $sch_team_name_list);

// 상태 조건
$add_method_where = " w.work_state = '6'"; // 진행완료

if($sch_prd_g1 == "01014" && $sch_prd_g2 == "01016" && empty($sch_prd)){
    $add_method_where .= " AND w.prd_no != '174'"; //마케팅컨텐츠 검색시 재택 자료전달 제외
}

// 상품(업무) 종류
if (!empty($sch_prd) && $sch_prd != "0") { // 상품
	$add_method_where .= " AND w.prd_no='".$sch_prd."'";
}else{
	if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
        $add_method_where .= " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.k_name_code='{$sch_prd_g2}')";
	}elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
        $add_method_where .= " AND w.prd_no IN(SELECT prd.prd_no FROM product prd WHERE prd.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
	}
}

// 담당자 기준
$add_team_where = "";
if(!!$sch_s_no_kind){
	if($sch_s_no_kind == 'c_s_no'){ // 업체 담당자
		if (!!$sch_s_no) { // 담당자 설정된 경우
			$add_team_where .= " AND w.s_no = '$sch_s_no'";
		}else{
			if(!!$sch_team_code_where){ // 부서 설정, 담당자 전체의 경우
				$add_team_where.=" AND w.team IN ({$sch_team_code_where})";
			}
		}
	}elseif($sch_s_no_kind == 'req_s_no'){ // 업무 요청자
		if (!!$sch_s_no) { // 담당자 설정된 경우
			$add_team_where.=" AND w.task_req_s_no = '$sch_s_no'";
		}else{
			if(!!$sch_team_code_where){ // 부서 설정, 담당자 전체의 경우
				$add_team_where.=" AND w.task_req_team IN ({$sch_team_code_where})";
			}
		}
	}elseif($sch_s_no_kind == 'run_s_no'){ // 업무 처리자
		if (!!$sch_s_no) { // 담당자 설정된 경우
			$add_team_where.=" AND w.task_run_s_no = '$sch_s_no'";
		}else{
			if(!!$sch_team_code_where){ // 부서 설정, 담당자 전체의 경우
				$add_team_where.=" AND w.task_run_team IN ({$sch_team_code_where})";
			}
		}
	}
}

// 기본 베이스 Array 만들기($x_name_list: 그래프 X 부분 타이틀, $default_chart_list: 기본 차트 날짜별, $default_chart_sum_list: 기본 차트 합)
$x_name_list 			= [];
$default_chart_list 	= [];
$default_chart_sum_list = [];

// 조회 기간 : add_date_where, date_column
$all_date_where 		= "";
$all_date_key			= "";
$all_date_title 		= "";
$add_date_where 		= "";
$add_date_column 		= "";

if($sch_kind == '1') //연간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_year_s_date}' AND '{$sch_year_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y')";

    $sch_year_s_date = $sch_year_s_date."-01-01 00:00:00"; //연간
    $sch_year_e_date = $sch_year_e_date."-12-31 23:59:59"; //연간
    $add_date_where  = " AND (w.task_run_regdate >= '{$sch_year_s_date}' AND w.task_run_regdate <= '{$sch_year_e_date}') ";
    $add_date_column = "DATE_FORMAT(task_run_regdate, '%Y')";
}
elseif($sch_kind == '2') //월간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_mon_s_date}' AND '{$sch_mon_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m')";

    $sch_mon_s_date  = $sch_mon_s_date."-01 00:00:00"; //월간
    $sch_mon_e_date  = $sch_mon_e_date."-31 23:59:59"; //월간
    $add_date_where  = " AND (w.task_run_regdate >= '{$sch_mon_s_date}' AND w.task_run_regdate <= '{$sch_mon_e_date}') ";
    $add_date_column = "DATE_FORMAT(task_run_regdate, '%Y%m')";
}
elseif($sch_kind == '3') //주간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_week_s_date}' AND '{$sch_week_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $sch_week_s_date = $sch_week_s_date." 00:00:00";  //주간
    $sch_week_e_date = $sch_week_e_date." 23:59:59";  //주간
    $add_date_where  = " AND (w.task_run_regdate >= '{$sch_week_s_date}' AND w.task_run_regdate <= '{$sch_week_e_date}') ";
    $add_date_column = "DATE_FORMAT(DATE_SUB(task_run_regdate, INTERVAL(IF(DAYOFWEEK(task_run_regdate)=1,8,DAYOFWEEK(task_run_regdate))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";

    $sch_s_date 	 = $sch_s_date." 00:00:00"; //일간
    $sch_e_date 	 = $sch_e_date." 23:59:59";	//일간
    $add_date_where  = " AND (w.task_run_regdate >= '{$sch_s_date}' AND w.task_run_regdate <= '{$sch_e_date}') ";
    $add_date_column = "DATE_FORMAT(task_run_regdate, '%Y%m%d')";
}

$all_date_sql = "
	SELECT
 		{$all_date_key} as chart_key,
		{$all_date_title} as chart_title
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	GROUP BY chart_key
	ORDER BY chart_key
";

if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    while($date = mysqli_fetch_array($all_date_query))
	{
		$default_chart_list[$date['chart_key']] = [];
		$default_chart_sum_list[$date['chart_key']] = 0;
        $x_name_list[$date['chart_key']] = $date['chart_title'];
	}
}
ksort($x_name_list);
$smarty->assign('x_name_title', $x_name_list);
$smarty->assign('x_name_list', "'".implode("','", $x_name_list)."'");

/* ---------- chart_01 업무 통계 (업무 건수) [Start] ---------- */
$chart_01_sql = "
	SELECT
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY result_ym
	ORDER BY result_ym ASC
";

$chart_01 = $default_chart_list;

$chart_01_result = mysqli_query($my_db, $chart_01_sql);
while($chart_01_array = mysqli_fetch_array($chart_01_result))
{
    $chart_01[$chart_01_array['result_ym']] = $chart_01_array['work_count'];
}

$smarty->assign("chart_01", $chart_01);
$smarty->assign("chart_01_result_data_sum", array_sum($chart_01));
/* ---------- chart_01 업무 통계 (업무 건수)[End] ---------- */


/* ---------- chart_02 부서별 업무(요청자) 통계 [Start] ---------- */
$chart_02_sql = "
	SELECT
		IF(TRIM(w.task_req_team)='','00000',TRIM(w.task_req_team)) AS select_team,
		(SELECT t.team_name FROM team as t WHERE t.team_code=w.task_req_team) as team_name,
		(SELECT t.display FROM team as t WHERE t.team_code=w.task_req_team) AS t_display,
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY select_team, result_ym
	ORDER BY result_ym ASC
";

$chart_02		   = $default_chart_list;
$chart_02_day_sum  = $default_chart_sum_list;
$chart_02_row_sum  = [];
$chart_02_row_name = [];
$chart_02_display  = [];

$chart_02_query = mysqli_query($my_db, $chart_02_sql);
while($chart_02_array = mysqli_fetch_array($chart_02_query))
{
	$chart_02[$chart_02_array['result_ym']][$chart_02_array['select_team']] = $chart_02_array['work_count'];
    $chart_02_day_sum[$chart_02_array['result_ym']]		+= $chart_02_array['work_count'];
    $chart_02_row_sum[$chart_02_array['select_team']] 	+= $chart_02_array['work_count'];
    $chart_02_row_name[$chart_02_array['select_team']] 	= $chart_02_array['team_name'] ? $chart_02_array['team_name'] : "(NULL)";
    $chart_02_display[$chart_02_array['select_team']]   = ($chart_02_array['t_display'] == '1') ? "Y" : "N";
}

arsort($chart_02_row_sum);
ksort($chart_02);

$smarty->assign("chart_02", $chart_02);
$smarty->assign("chart_02_display", $chart_02_display);
$smarty->assign("chart_02_day_sum", $chart_02_day_sum);
$smarty->assign("chart_02_row_sum", $chart_02_row_sum);
$smarty->assign("chart_02_row_name", $chart_02_row_name);
/* ---------- chart_02 부서별 업무(요청자) 통계 [End] ---------- */


/* ---------- chart_03 업무(요청자) 통계 [Start] ---------- */
$chart_03_sql = "
	SELECT
		w.task_req_s_no AS select_s_no, (SELECT s.s_name FROM staff s WHERE s.s_no = w.task_req_s_no) AS select_s_name, 
		(SELECT s.staff_state FROM staff s WHERE s.s_no=w.task_req_s_no) AS s_display,
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY select_s_no, result_ym
	ORDER BY result_ym ASC
";

$chart_03			  = $default_chart_list;
$chart_03_day_sum	  = $default_chart_sum_list;
$chart_03_row_sum	  = [];
$chart_03_row_name 	  = [];
$chart_03_display 	  = [];

$chart_03_query = mysqli_query($my_db, $chart_03_sql);
while($chart_03_array = mysqli_fetch_array($chart_03_query))
{
    $chart_03[$chart_03_array['result_ym']][$chart_03_array['select_s_no']] = $chart_03_array['work_count'];
    $chart_03_day_sum[$chart_03_array['result_ym']] 	+= $chart_03_array['work_count'];
    $chart_03_row_sum[$chart_03_array['select_s_no']] 	+= $chart_03_array['work_count'];
    $chart_03_row_name[$chart_03_array['select_s_no']] 	= $chart_03_array['select_s_name'] ? $chart_03_array['select_s_name'] : "(NULL)";
    $chart_03_display[$chart_03_array['select_s_no']]   = (empty($chart_03_array['s_display']) || $chart_03_array['s_display'] == '3') ? "N" : "Y";
}

arsort($chart_03_row_sum);
ksort($chart_03);

$smarty->assign("chart_03", $chart_03);
$smarty->assign("chart_03_day_sum", $chart_03_day_sum);
$smarty->assign("chart_03_row_sum", $chart_03_row_sum);
$smarty->assign("chart_03_row_name", $chart_03_row_name);
$smarty->assign("chart_03_display", $chart_03_display);
/* ---------- chart_03 업무(요청자) 통계 [End] ---------- */


/* ---------- chart_09 부서별 업무(처리자) 통계 [Start] ---------- */
$chart_09_sql = "
	SELECT
	    IF(TRIM(w.task_run_team)='','00000',TRIM(w.task_run_team)) AS select_team,
	    (SELECT team_name FROM team as t WHERE t.team_code = w.task_run_team) as team_name,
	    (SELECT t.display FROM team as t WHERE t.team_code=w.task_run_team) AS t_display,
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY select_team, result_ym
	ORDER BY result_ym ASC
";

$chart_09		   = $default_chart_list;
$chart_09_day_sum  = $default_chart_sum_list;
$chart_09_row_sum  = [];
$chart_09_row_name = [];
$chart_09_display  = [];

$chart_09_query = mysqli_query($my_db, $chart_09_sql);
while($chart_09_array = mysqli_fetch_array($chart_09_query))
{
    $chart_09[$chart_09_array['result_ym']][$chart_09_array['select_team']] = $chart_09_array['work_count'];
    $chart_09_day_sum[$chart_09_array['result_ym']]		+= $chart_09_array['work_count'];
    $chart_09_row_sum[$chart_09_array['select_team']] 	+= $chart_09_array['work_count'];
    $chart_09_row_name[$chart_09_array['select_team']] 	= $chart_09_array['team_name'] ? $chart_09_array['team_name'] : "(NULL)";
    $chart_09_display[$chart_09_array['select_team']] 	= ($chart_09_array['t_display'] == '1') ? "Y" : "N";
}

arsort($chart_09_row_sum);
ksort($chart_09);

$smarty->assign("chart_09", $chart_09);
$smarty->assign("chart_09_display", $chart_09_display);
$smarty->assign("chart_09_day_sum", $chart_09_day_sum);
$smarty->assign("chart_09_row_sum", $chart_09_row_sum);
$smarty->assign("chart_09_row_name", $chart_09_row_name);
/* ---------- chart_09 부서별 업무(처리자) 통계 [End] ---------- */


/* ---------- chart_10 업무(처리자) 통계 [Start] ---------- */
$chart_10_sql = "
	SELECT
	    w.task_run_s_no AS select_s_no, (SELECT s.s_name FROM staff s WHERE s.s_no = w.task_run_s_no) AS select_s_name, 
	    (SELECT s.staff_state FROM staff s WHERE s.s_no=w.task_run_s_no) AS s_display,
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY select_s_no, result_ym
	ORDER BY result_ym ASC
";

$chart_10			  = $default_chart_list;
$chart_10_day_sum	  = $default_chart_sum_list;
$chart_10_row_sum	  = [];
$chart_10_row_name 	  = [];
$chart_10_display 	  = [];

$chart_10_query = mysqli_query($my_db, $chart_10_sql);
while($chart_10_array = mysqli_fetch_array($chart_10_query))
{
    $chart_10[$chart_10_array['result_ym']][$chart_10_array['select_s_no']] = $chart_10_array['work_count'];
    $chart_10_day_sum[$chart_10_array['result_ym']] 	+= $chart_10_array['work_count'];
    $chart_10_row_sum[$chart_10_array['select_s_no']] 	+= $chart_10_array['work_count'];
    $chart_10_row_name[$chart_10_array['select_s_no']] 	= $chart_10_array['select_s_name'] ? $chart_10_array['select_s_name'] : "(NULL)";
    $chart_10_display[$chart_10_array['select_s_no']]   = (empty($chart_10_array['s_display']) || $chart_10_array['s_display'] == '3') ? "N" : "Y";
}

arsort($chart_10_row_sum);
ksort($chart_10);

$smarty->assign("chart_10", $chart_10);
$smarty->assign("chart_10_day_sum", $chart_10_day_sum);
$smarty->assign("chart_10_row_sum", $chart_10_row_sum);
$smarty->assign("chart_10_row_name", $chart_10_row_name);
$smarty->assign("chart_10_display", $chart_10_display);
/* ---------- chart_10 업무(처리자) 통계 [End] ---------- */


/* ---------- chart_04 상품그룹2별 업무 통계 [Start] ---------- */
$chart_04_sql = "
	SELECT
		(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no) AS k_name_code,
		(SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT prd.k_name_code FROM product prd WHERE prd.prd_no=w.prd_no)) AS k_name,
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY k_name, result_ym
	ORDER BY result_ym ASC
";

$chart_04			  = $default_chart_list;
$chart_04_day_sum	  = $default_chart_sum_list;
$chart_04_row_sum	  = [];
$chart_04_row_name 	  = [];

$chart_04_query = mysqli_query($my_db, $chart_04_sql);
while($chart_04_array = mysqli_fetch_array($chart_04_query))
{
    $chart_04[$chart_04_array['result_ym']][$chart_04_array['k_name_code']] = $chart_04_array['work_count'];
    $chart_04_day_sum[$chart_04_array['result_ym']] 	+= $chart_04_array['work_count'];
    $chart_04_row_sum[$chart_04_array['k_name_code']] 	+= $chart_04_array['work_count'];
    $chart_04_row_name[$chart_04_array['k_name_code']] 	= $chart_04_array['k_name'] ? $chart_04_array['k_name'] : "(NULL)";
}

arsort($chart_04_row_sum);
ksort($chart_04);

$smarty->assign("chart_04", $chart_04);
$smarty->assign("chart_04_day_sum", $chart_04_day_sum);
$smarty->assign("chart_04_row_sum", $chart_04_row_sum);
$smarty->assign("chart_04_row_name", $chart_04_row_name);
/* ---------- chart_04 상품그룹2별 업무 통계 [End] ---------- */


/* ---------- chart_05 상품별 업무 통계 [Start] ---------- */
$chart_05_sql = "
	SELECT
		w.prd_no,
		(SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_name,
		(SELECT prd.display FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_display,
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY w.prd_no, result_ym
	ORDER BY result_ym ASC
";

$chart_05			  = $default_chart_list;
$chart_05_day_sum	  = $default_chart_sum_list;
$chart_05_row_sum	  = [];
$chart_05_row_name 	  = [];
$chart_05_display 	  = [];

$chart_05_query = mysqli_query($my_db, $chart_05_sql);
while($chart_05_array = mysqli_fetch_array($chart_05_query))
{
    $chart_05[$chart_05_array['result_ym']][$chart_05_array['prd_no']] = $chart_05_array['work_count'];
    $chart_05_day_sum[$chart_05_array['result_ym']] += $chart_05_array['work_count'];
    $chart_05_row_sum[$chart_05_array['prd_no']] 	+= $chart_05_array['work_count'];
    $chart_05_row_name[$chart_05_array['prd_no']] 	= $chart_05_array['prd_name'] ? $chart_05_array['prd_name'] : "(NULL)";
    $chart_05_display[$chart_05_array['prd_no']]    = (empty($chart_05_array['prd_display']) || $chart_05_array['prd_display'] == '2') ? "N" : "Y";
}

arsort($chart_05_row_sum);
ksort($chart_05);

$smarty->assign("chart_05", $chart_05);
$smarty->assign("chart_05_day_sum", $chart_05_day_sum);
$smarty->assign("chart_05_row_sum", $chart_05_row_sum);
$smarty->assign("chart_05_row_name", $chart_05_row_name);
$smarty->assign("chart_05_display", $chart_05_display);
/* ---------- chart_05 상품별 업무 통계 [End] ---------- */

if(!!$sch_prd) {
    /* ---------- chart_06 #담당자명 업무 기여도(건) [End] ---------- */
    $chart_06_sql = "
        SELECT
            w.prd_no,
            (SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_name,
            w.k_name_code,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=w.k_name_code) AS k_name,
            {$add_date_column} AS result_ym,
            SUM(quantity) AS work_count
        FROM `work` w LEFT JOIN staff s ON w.task_run_s_no = s.s_no
        WHERE
            {$add_method_where}
            {$add_date_where}
            {$add_team_where}
            AND w.task_run_s_no IS NOT NULL
        GROUP BY w.task_run_s_no, w.prd_no, w.k_name_code, result_ym
        ORDER BY prd_no ASC, k_name_code ASC, result_ym ASC, work_count DESC
    ";
    $chart_06 = $default_chart_list;
    $chart_06_day_sum = $default_chart_sum_list;
    $chart_06_row_sum = [];
    $chart_06_row_name = [];
    $chart_06_table_row = [];
    $chart_06_table_name = [];

    $chart_06_query = mysqli_query($my_db, $chart_06_sql);
    while ($chart_06_array = mysqli_fetch_array($chart_06_query)) {
        $chart_06[$chart_06_array['result_ym']][$chart_06_array['prd_no']][$chart_06_array['k_name_code']] += $chart_06_array['work_count'];
        $chart_06[$chart_06_array['result_ym']][$chart_06_array['prd_no']]['total'] += $chart_06_array['work_count'];
        $chart_06_day_sum[$chart_06_array['result_ym']] += $chart_06_array['work_count'];
        $chart_06_row_sum[$chart_06_array['prd_no']] += $chart_06_array['work_count'];
        $chart_06_row_name[$chart_06_array['prd_no']] = $chart_06_array['prd_name'] ? $chart_06_array['prd_name'] : "(NULL)";

        $chart_06_table_row[$chart_06_array['prd_no']][$chart_06_array['k_name_code']] += $chart_06_array['work_count'];
        $chart_06_table_name[$chart_06_array['prd_no']][$chart_06_array['k_name_code']] = $chart_06_array['k_name'];
    }

    arsort($chart_06_table_row);
    arsort($chart_06_row_sum);
    ksort($chart_06);

    $smarty->assign("chart_06", $chart_06);
    $smarty->assign("chart_06_day_sum", $chart_06_day_sum);
    $smarty->assign("chart_06_row_sum", $chart_06_row_sum);
    $smarty->assign("chart_06_row_name", $chart_06_row_name);
    $smarty->assign("chart_06_table_row", $chart_06_table_row);
    $smarty->assign("chart_06_table_name", $chart_06_table_name);
    /* ---------- chart_06 #담당자명 업무 기여도(건) [End] ---------- */
}

/* ---------- chart_07 출금업체별 업무 통계 [Start] ---------- */
$chart_07_sql = "
	SELECT
		w.wd_c_no,
		w.wd_c_name,
		(SELECT c.display FROM company c WHERE c.c_no=w.wd_c_no) as wd_c_display,
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY w.wd_c_no, result_ym
	ORDER BY result_ym ASC
";

$chart_07			  = $default_chart_list;
$chart_07_day_sum	  = $default_chart_sum_list;
$chart_07_row_sum	  = [];
$chart_07_row_name 	  = [];
$chart_07_display 	  = [];

$chart_07_query = mysqli_query($my_db, $chart_07_sql);
while($chart_07_array = mysqli_fetch_array($chart_07_query))
{
    $chart_07[$chart_07_array['result_ym']][$chart_07_array['wd_c_no']] = $chart_07_array['work_count'];
    $chart_07_day_sum[$chart_07_array['result_ym']] += $chart_07_array['work_count'];
    $chart_07_row_sum[$chart_07_array['wd_c_no']] 	+= $chart_07_array['work_count'];
    $chart_07_row_name[$chart_07_array['wd_c_no']] 	= $chart_07_array['wd_c_name'] ? $chart_07_array['wd_c_name'] : "(NULL)";
    $chart_07_display[$chart_07_array['wd_c_no']] 	= (empty($chart_07_array['wd_c_display']) || $chart_07_array['wd_c_display'] != '1') ? "N" : "Y";
}

arsort($chart_07_row_sum);
ksort($chart_07);

$smarty->assign("chart_07", $chart_07);
$smarty->assign("chart_07_day_sum", $chart_07_day_sum);
$smarty->assign("chart_07_row_sum", $chart_07_row_sum);
$smarty->assign("chart_07_row_name", $chart_07_row_name);
$smarty->assign("chart_07_display", $chart_07_display);
/* ---------- chart_07 출금업체별 업무 통계 [End] ---------- */

/* ---------- chart_08 업체별 업무 통계 [Start] ---------- */
$chart_08_sql = "
	SELECT
		w.c_no,
		w.c_name,
		(SELECT c.display FROM company c WHERE c.c_no=w.c_no) as c_display,
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY w.c_no, result_ym
	ORDER BY result_ym ASC
";

$chart_08			  = $default_chart_list;
$chart_08_day_sum	  = $default_chart_sum_list;
$chart_08_row_sum	  = [];
$chart_08_row_name 	  = [];
$chart_08_display 	  = [];

$chart_08_query = mysqli_query($my_db, $chart_08_sql);
while($chart_08_array = mysqli_fetch_array($chart_08_query))
{
    $chart_08[$chart_08_array['result_ym']][$chart_08_array['c_no']] = $chart_08_array['work_count'];
    $chart_08_day_sum[$chart_08_array['result_ym']] += $chart_08_array['work_count'];
    $chart_08_row_sum[$chart_08_array['c_no']] 	    += $chart_08_array['work_count'];
    $chart_08_row_name[$chart_08_array['c_no']] 	= $chart_08_array['c_name'] ? $chart_08_array['c_name'] : "(NULL)";
    $chart_08_display[$chart_08_array['c_no']]  	= (empty($chart_08_array['c_display']) || $chart_08_array['c_display'] != '1') ? "N" : "Y";
}

arsort($chart_08_row_sum);
ksort($chart_08);

$smarty->assign("chart_08", $chart_08);
$smarty->assign("chart_08_day_sum", $chart_08_day_sum);
$smarty->assign("chart_08_row_sum", $chart_08_row_sum);
$smarty->assign("chart_08_row_name", $chart_08_row_name);
$smarty->assign("chart_08_display", $chart_08_display);
/* ---------- chart_08 출금업체별 업무 통계 [End] ---------- */

$smarty->display('stats/wise_work_01.html');

?>
