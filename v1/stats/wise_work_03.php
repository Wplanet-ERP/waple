<?php
require('../inc/common.php');
require('../ckadmin.php');
require('../inc/helper/_navigation.php');
require('../inc/helper/_common.php');
require('../inc/helper/stats.php');
require('../inc/model/MyQuick.php');

$dir_location = "../";
$smarty->assign("dir_location", $dir_location);

# Navigation & My Quick
$nav_prd_no  = "131";
$nav_title   = "업무 기여도(업무량)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

// [조회 단위] & [조회 기간]
$sch_kind		    = isset($_GET['sch_kind']) ? $_GET['sch_kind'] : "2";
$sch_kind_type      = "month";
$today_s_w		    = date('w')-1;
$sch_year_s_date 	= isset($_GET['sch_year_s_date']) ? $_GET['sch_year_s_date'] : date("Y", strtotime("-3 years")); // 연간
$sch_year_e_date 	= isset($_GET['sch_year_e_date']) ? $_GET['sch_year_e_date'] : date("Y"); // 연간
$sch_mon_s_date 	= isset($_GET['sch_mon_s_date']) ? $_GET['sch_mon_s_date'] : date("Y-m", strtotime("-6 months")); // 월간
$sch_mon_e_date 	= isset($_GET['sch_mon_e_date']) ? $_GET['sch_mon_e_date'] : date("Y-m"); // 월간
$sch_week_s_date 	= isset($_GET['sch_week_s_date']) ? $_GET['sch_week_s_date'] : date("Y-m-d",strtotime("-{$today_s_w} day")); // 주간
$sch_week_e_date 	= isset($_GET['sch_week_e_date']) ? $_GET['sch_week_e_date'] : date("Y-m-d", strtotime("{$sch_week_s_date} +6 day")); // 주간
$sch_s_date 	    = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date("Y-m-d", strtotime("-6 day")); // 일간
$sch_e_date 	    = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date("Y-m-d"); // 일간

switch ($sch_kind){
    case "1":
        $sch_kind_type = "year";
        break;
    case "2":
        $sch_kind_type = "month";
        break;
    case "3":
        $sch_kind_type = "week";
        break;
    case "4":
        $sch_kind_type = "day";
        break;
}

$smarty->assign("sch_kind", $sch_kind);
$smarty->assign("sch_kind_type", $sch_kind_type);
$smarty->assign("sch_year_s_date", $sch_year_s_date);
$smarty->assign("sch_year_e_date", $sch_year_e_date);
$smarty->assign("sch_mon_s_date", $sch_mon_s_date);
$smarty->assign("sch_mon_e_date", $sch_mon_e_date);
$smarty->assign("sch_week_s_date", $sch_week_s_date);
$smarty->assign("sch_week_e_date", $sch_week_e_date);
$smarty->assign("sch_s_date", $sch_s_date);
$smarty->assign("sch_e_date", $sch_e_date);


// [담당자 기준] : 팀, 담당자
$sch_team	= isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_s_no	= isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";

$sch_team_code_where = "";
if(!empty($sch_team) && $sch_team != "all"){
    $sch_team_code_where = getTeamWhere($my_db, $sch_team);
    $sch_s_name .= isset($sch_team_name_list[$sch_team]) ? $sch_team_name_list[$sch_team] : "";
}else{
    $sch_s_name .= " 전체";
}

$staff_list = [];
$staff_name = "";
if($sch_team_code_where)
{
    $staff_team_where 		= "";
    $staff_team_where_list 	= [];

    $sch_team_code_list_val = explode(",", $sch_team_code_where);

    foreach($sch_team_code_list_val as $sch_team_code){
        $staff_team_where_list[] = "team_list like '%{$sch_team_code}%'";
    }

    if($staff_team_where_list)
    {
        $staff_team_where = implode(" OR ", $staff_team_where_list);
        $staff_sql="SELECT s.s_no, s.s_name FROM staff s WHERE ({$staff_team_where}) AND s.staff_state<>'3' ORDER BY s_name ASC";
        $staff_result=mysqli_query($my_db,$staff_sql);
        while($staff=mysqli_fetch_array($staff_result)) {
            $staff_list[]=array(
                "s_no"=>trim($staff['s_no']),
                "s_name"=>trim($staff['s_name'])
            );

            if(!!$sch_s_no && $sch_s_no == $staff['s_no']){
                $staff_name = $staff['s_name'];
            }
        }
    }
}

if(!!$sch_s_no && !empty($staff_name)){
    $sch_s_name .= " > ".$staff_name;
}else{
    $sch_s_name .= " > 전체";
}

$smarty->assign("sch_s_name", $sch_s_name);
$smarty->assign("sch_team", $sch_team);
$smarty->assign("sch_s_no", $sch_s_no);
$smarty->assign("staff_list", $staff_list);
$smarty->assign("sch_team_list", $sch_team_name_list);


// 상태 조건
$add_method_where = " w.work_state = '6'"; // 진행완료

// 업무처리 담당자
if (!!$sch_s_no) { // 업무처리 담당자 설정된 경우
	if($sch_s_no == 'null'){
		$add_team_where.=" AND (w.task_run_s_no IS NULL OR w.task_run_s_no = '0')";
	}else{
		$add_team_where.=" AND (w.task_run_s_no = '$sch_s_no')";
	}
}else{
	if(!!$sch_team_code_where){ // 부서 설정, 업무처리 담당자 전체의 경우
		$add_team_where.=" AND w.task_run_team IN ({$sch_team_code_where})";
	}
}

// 기본 베이스 Array 만들기($x_name_list: 그래프 X 부분 타이틀, $default_chart_list: 기본 차트 날짜별, $default_chart_sum_list: 기본 차트 합)
$x_name_list 			= [];
$default_chart_list 	= [];
$default_chart_sum_list = [];

// 조회 기간 : add_date_where, date_column
$all_date_where 		= "";
$all_date_key			= "";
$all_date_title 		= "";
$add_date_where 		= "";
$add_date_column 		= "";

if($sch_kind == '1') //연간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_year_s_date}' AND '{$sch_year_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y')";

    $add_date_where  = " AND DATE_FORMAT(task_run_regdate, '%Y') BETWEEN '{$sch_year_s_date}' AND '{$sch_year_e_date}' ";
    $add_date_column = "DATE_FORMAT(task_run_regdate, '%Y')";
}
elseif($sch_kind == '2') //월간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_mon_s_date}' AND '{$sch_mon_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m')";

    $add_date_where  = " AND DATE_FORMAT(task_run_regdate, '%Y-%m') BETWEEN '{$sch_mon_s_date}' AND '{$sch_mon_e_date}' ";
    $add_date_column = "DATE_FORMAT(task_run_regdate, '%Y%m')";
}
elseif($sch_kind == '3') //주간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_week_s_date}' AND '{$sch_week_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where  = " AND DATE_FORMAT(task_run_regdate, '%Y-%m-%d') BETWEEN '{$sch_week_s_date}' AND '{$sch_week_e_date}' ";
    $add_date_column = "DATE_FORMAT(DATE_SUB(w.task_run_regdate, INTERVAL(IF(DAYOFWEEK(w.task_run_regdate)=1,8,DAYOFWEEK(w.task_run_regdate))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d')";

    $add_date_where  = " AND DATE_FORMAT(task_run_regdate, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '{$sch_e_date}' ";
    $add_date_column = "DATE_FORMAT(task_run_regdate, '%Y%m%d')";
}

$all_date_sql = "
	SELECT
 		$all_date_key as chart_key,
		$all_date_title as chart_title
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	GROUP BY chart_key
	ORDER BY chart_key
";

if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    while($date = mysqli_fetch_array($all_date_query))
    {
        $default_chart_list[$date['chart_key']] = [];
        $default_chart_sum_list[$date['chart_key']] = 0;
        $x_name_list[$date['chart_key']] = $date['chart_title'];
    }
}
ksort($x_name_list);
$smarty->assign('x_name_title', $x_name_list);
$smarty->assign('x_name_list', "'".implode("','", $x_name_list)."'");


/* ---------- chart_01 총 업무 기여도(건) [Start] ---------- */
$chart_01_sql = "
	SELECT
		w.task_run_s_no,
		(SELECT s.s_name FROM staff s WHERE s.s_no=w.task_run_s_no) AS task_run_s_name,
		(SELECT s.staff_state FROM staff s WHERE s.s_no=w.task_run_s_no) AS s_display,
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY w.task_run_s_no, result_ym
	ORDER BY result_ym ASC, work_count DESC
";

$chart_01		   = $default_chart_list;
$chart_01_day_sum  = $default_chart_sum_list;
$chart_01_row_sum  = [];
$chart_01_row_name = [];
$chart_01_display  = [];

$chart_01_query = mysqli_query($my_db, $chart_01_sql);
while($chart_01_array = mysqli_fetch_array($chart_01_query))
{
    $chart_01[$chart_01_array['result_ym']][$chart_01_array['task_run_s_no']] = $chart_01_array['work_count'];
    $chart_01_day_sum[$chart_01_array['result_ym']]		+= $chart_01_array['work_count'];
    $chart_01_row_sum[$chart_01_array['task_run_s_no']] += $chart_01_array['work_count'];
    $chart_01_row_name[$chart_01_array['task_run_s_no']] = $chart_01_array['task_run_s_name'] ? $chart_01_array['task_run_s_name'] : "(NULL)";
    $chart_01_display[$chart_01_array['task_run_s_no']]  = (empty($chart_01_array['s_display']) || $chart_01_array['s_display'] == '3') ? "N" : "Y";
}

arsort($chart_01_row_sum);
ksort($chart_01);

$smarty->assign("chart_01", $chart_01);
$smarty->assign("chart_01_day_sum", $chart_01_day_sum);
$smarty->assign("chart_01_row_sum", $chart_01_row_sum);
$smarty->assign("chart_01_row_name", $chart_01_row_name);
$smarty->assign("chart_01_display", $chart_01_display);
/* ---------- chart_01 총 업무 기여도(건)[End] ---------- */

/* ---------- chart_02 총 업무 기여도(work-time) [Start] ---------- */
$chart_02_sql = "
	SELECT
			w.task_run_s_no,
			(SELECT s.s_name FROM staff s WHERE s.s_no=w.task_run_s_no) AS task_run_s_name,
			(SELECT s.staff_state FROM staff s WHERE s.s_no=w.task_run_s_no) AS s_display,
			{$add_date_column} AS result_ym,
			SUM(TRUNCATE(w.work_time, 2)) AS work_time_sum,
			COUNT(CASE WHEN (w.work_time = '0' OR w.work_time IS NULL) THEN 1 END) AS work_empty_time
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY w.task_run_s_no, result_ym
	ORDER BY result_ym ASC, work_time_sum DESC
";

$chart_02		     = $default_chart_list;
$chart_02_day_sum    = $default_chart_sum_list;
$chart_02_row_sum    = [];
$chart_02_row_name   = [];
$chart_02_empty_time = [];
$chart_02_display    = [];

$chart_02_query = mysqli_query($my_db, $chart_02_sql);
while($chart_02_array = mysqli_fetch_array($chart_02_query))
{
    $chart_02[$chart_02_array['result_ym']][$chart_02_array['task_run_s_no']] = $chart_02_array['work_time_sum'];
    $chart_02_day_sum[$chart_02_array['result_ym']]		+= $chart_02_array['work_time_sum'];
    $chart_02_row_sum[$chart_02_array['task_run_s_no']] += $chart_02_array['work_time_sum'];
    $chart_02_row_name[$chart_02_array['task_run_s_no']] = $chart_02_array['task_run_s_name'] ? $chart_02_array['task_run_s_name'] : "(NULL)";
    $chart_02_empty_time[$chart_02_array['result_ym']]  += $chart_02_array['work_empty_time'];
    $chart_02_display[$chart_02_array['task_run_s_no']]  = (empty($chart_02_array['s_display']) || $chart_02_array['s_display'] == '3') ? "N" : "Y";
}

arsort($chart_02_row_sum);
ksort($chart_02);

$smarty->assign("chart_02", $chart_02);
$smarty->assign("chart_02_day_sum", $chart_02_day_sum);
$smarty->assign("chart_02_row_sum", $chart_02_row_sum);
$smarty->assign("chart_02_row_name", $chart_02_row_name);
$smarty->assign("chart_02_empty_time", $chart_02_empty_time);
$smarty->assign("chart_02_display", $chart_02_display);
/* ---------- chart_02 총 업무 기여도(work-time)[End] ---------- */


if(!!$sch_s_no)
{
    /* ---------- chart_03 #담당자명 업무 기여도(건) [Start] ---------- */
    $chart_03_sql = "
		SELECT
			w.prd_no,
			(SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_name,
			(SELECT prd.display FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_display,
			w.k_name_code,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=w.k_name_code) AS k_name,
			{$add_date_column} AS result_ym,
			SUM(quantity) AS work_count
		FROM `work` w LEFT JOIN staff s ON w.task_run_s_no = s.s_no
		WHERE
			{$add_method_where}
			{$add_date_where}
			{$add_team_where}
			AND w.task_run_s_no IS NOT NULL
		GROUP BY w.task_run_s_no, w.prd_no, w.k_name_code, result_ym
		ORDER BY prd_no ASC, k_name_code ASC, result_ym ASC, work_count DESC
	";

    $chart_03			  = $default_chart_list;
    $chart_03_day_sum	  = $default_chart_sum_list;
    $chart_03_row_sum	  = [];
    $chart_03_row_name 	  = [];
    $chart_03_table_row   = [];
    $chart_03_table_name  = [];

    $chart_03_query = mysqli_query($my_db, $chart_03_sql);
    while($chart_03_array = mysqli_fetch_array($chart_03_query))
    {
        $chart_03[$chart_03_array['result_ym']][$chart_03_array['prd_no']][$chart_03_array['k_name_code']] = $chart_03_array['work_count'];
        $chart_03[$chart_03_array['result_ym']][$chart_03_array['prd_no']]['total'] +=  $chart_03_array['work_count'];
        $chart_03_day_sum[$chart_03_array['result_ym']] += $chart_03_array['work_count'];
        $chart_03_row_sum[$chart_03_array['prd_no']] 	+= $chart_03_array['work_count'];
        $chart_03_row_name[$chart_03_array['prd_no']]    = $chart_03_array['prd_name'] ? $chart_03_array['prd_name'] : "(NULL)";

        $chart_03_table_row[$chart_03_array['prd_no']][$chart_03_array['k_name_code']]  += $chart_03_array['work_count'];
        $chart_03_table_name[$chart_03_array['prd_no']][$chart_03_array['k_name_code']] = $chart_03_array['k_name'];

        $chart_03_display[$chart_03_array['prd_no']] = (empty($chart_03_array['prd_display']) || $chart_03_array['prd_display'] == '2') ? "N" : "Y";
    }

    arsort($chart_03_table_row);
    arsort($chart_03_row_sum);
    ksort($chart_03);

    $smarty->assign("chart_03", $chart_03);
    $smarty->assign("chart_03_day_sum", $chart_03_day_sum);
    $smarty->assign("chart_03_row_sum", $chart_03_row_sum);
    $smarty->assign("chart_03_row_name", $chart_03_row_name);
    $smarty->assign("chart_03_table_row", $chart_03_table_row);
    $smarty->assign("chart_03_table_name", $chart_03_table_name);
    $smarty->assign("chart_03_display", $chart_03_display);
    /* ---------- chart_03 #담당자명 업무 기여도(건) [End] ---------- */


    /* ---------- chart_04 #담당자명 업무 기여도(work-time) [Start] ---------- */
    $chart_04_sql = "
        SELECT
			w.prd_no,
            (SELECT prd.title FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_name,
            (SELECT prd.display FROM product prd WHERE prd.prd_no=w.prd_no) AS prd_display,
			w.k_name_code,
            (SELECT k.k_name FROM kind k WHERE k.k_name_code=w.k_name_code) AS k_name,
			{$add_date_column} AS result_ym,
			SUM(TRUNCATE(w.work_time,2)) AS work_time_sum
		FROM `work` w LEFT JOIN staff s ON w.task_run_s_no = s.s_no
		WHERE
			{$add_method_where}
			{$add_date_where}
			{$add_team_where}
			AND w.task_run_s_no IS NOT NULL
		GROUP BY w.task_run_s_no, w.prd_no, w.k_name_code, result_ym
		ORDER BY prd_no ASC, k_name_code ASC, result_ym ASC, work_time_sum DESC
	";

    $chart_04			  = $default_chart_list;
    $chart_04_day_sum	  = $default_chart_sum_list;
    $chart_04_row_sum	  = [];
    $chart_04_row_name 	  = [];
    $chart_04_table_row   = [];
    $chart_04_table_name  = [];
    $chart_04_display     = [];

    $chart_04_query = mysqli_query($my_db, $chart_04_sql);
    while($chart_04_array = mysqli_fetch_array($chart_04_query))
    {
        $chart_04[$chart_04_array['result_ym']][$chart_04_array['prd_no']][$chart_04_array['k_name_code']] = $chart_04_array['work_time_sum'];
        $chart_04[$chart_04_array['result_ym']][$chart_04_array['prd_no']]['total'] +=  $chart_04_array['work_time_sum'];
        $chart_04_day_sum[$chart_04_array['result_ym']] += $chart_04_array['work_time_sum'];
        $chart_04_row_sum[$chart_04_array['prd_no']] 	+= $chart_04_array['work_time_sum'];
        $chart_04_row_name[$chart_04_array['prd_no']]    = $chart_04_array['prd_name'] ? $chart_04_array['prd_name'] : "(NULL)";

        $chart_04_table_row[$chart_04_array['prd_no']][$chart_04_array['k_name_code']]  += $chart_04_array['work_time_sum'];
        $chart_04_table_name[$chart_04_array['prd_no']][$chart_04_array['k_name_code']] = $chart_04_array['k_name'];

        $chart_04_display[$chart_04_array['prd_no']] = (empty($chart_04_array['prd_display']) || $chart_04_array['prd_display'] == '2') ? "N" : "Y";
    }

    arsort($chart_04_table_row);
    arsort($chart_04_row_sum);
    ksort($chart_04);

    $smarty->assign("chart_04", $chart_04);
    $smarty->assign("chart_04_day_sum", $chart_04_day_sum);
    $smarty->assign("chart_04_row_sum", $chart_04_row_sum);
    $smarty->assign("chart_04_row_name", $chart_04_row_name);
    $smarty->assign("chart_04_table_row", $chart_04_table_row);
    $smarty->assign("chart_04_table_name", $chart_04_table_name);
    $smarty->assign("chart_04_display", $chart_04_display);
    /* ---------- chart_04 #담당자명 업무 기여도(work-time) [End] ---------- */
}


/* ---------- chart_05 업무요청자별 기여도(건) [Start] ---------- */
$chart_05_sql = "
	SELECT
		w.task_req_s_no,
		(SELECT s.s_name FROM staff s WHERE s.s_no=w.task_req_s_no) AS task_req_s_name,
		(SELECT s.staff_state FROM staff s WHERE s.s_no=w.task_req_s_no) AS s_display,
		{$add_date_column} AS result_ym,
		SUM(quantity) AS work_count
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY w.task_req_s_no, result_ym
	ORDER BY result_ym ASC, work_count DESC
";

$chart_05		   = $default_chart_list;
$chart_05_day_sum  = $default_chart_sum_list;
$chart_05_row_sum  = [];
$chart_05_row_name = [];
$chart_05_display  = [];

$chart_05_query = mysqli_query($my_db, $chart_05_sql);
while($chart_05_array = mysqli_fetch_array($chart_05_query))
{
    $chart_05[$chart_05_array['result_ym']][$chart_05_array['task_req_s_no']] = $chart_05_array['work_count'];
    $chart_05_day_sum[$chart_05_array['result_ym']]		+= $chart_05_array['work_count'];
    $chart_05_row_sum[$chart_05_array['task_req_s_no']] += $chart_05_array['work_count'];
    $chart_05_row_name[$chart_05_array['task_req_s_no']] = $chart_05_array['task_req_s_name'] ? $chart_05_array['task_req_s_name'] : "(NULL)";
    $chart_05_display[$chart_05_array['task_req_s_no']]  = (empty($chart_05_array['s_display']) || $chart_05_array['s_display'] == '3') ? "N" : "Y";
}

arsort($chart_05_row_sum);
ksort($chart_05);

$smarty->assign("chart_05", $chart_05);
$smarty->assign("chart_05_day_sum", $chart_05_day_sum);
$smarty->assign("chart_05_row_sum", $chart_05_row_sum);
$smarty->assign("chart_05_row_name", $chart_05_row_name);
$smarty->assign("chart_05_display", $chart_05_display);
/* ---------- chart_05 총 업무 기여도(건)[End] ---------- */

/* ---------- chart_06 업무요청자별 기여도(work-time) [Start] ---------- */
$chart_06_sql = "
	SELECT
			w.task_req_s_no,
			(SELECT s.s_name FROM staff s WHERE s.s_no=w.task_req_s_no) AS task_req_s_name,
			(SELECT s.staff_state FROM staff s WHERE s.s_no=w.task_req_s_no) AS s_display,
			{$add_date_column} AS result_ym,
			SUM(TRUNCATE(w.work_time, 2)) AS work_time_sum,
			COUNT(CASE WHEN (w.work_time = '0' OR w.work_time IS NULL) THEN 1 END) AS work_empty_time
	FROM `work` w
	WHERE
		{$add_method_where}
		{$add_date_where}
		{$add_team_where}
	GROUP BY w.task_req_s_no, result_ym
	ORDER BY result_ym ASC, work_time_sum DESC
";

$chart_06		     = $default_chart_list;
$chart_06_day_sum    = $default_chart_sum_list;
$chart_06_row_sum    = [];
$chart_06_row_name   = [];
$chart_06_empty_time = [];
$chart_06_display    = [];

$chart_06_query = mysqli_query($my_db, $chart_06_sql);
while($chart_06_array = mysqli_fetch_array($chart_06_query))
{
    $chart_06[$chart_06_array['result_ym']][$chart_06_array['task_req_s_no']] = $chart_06_array['work_time_sum'];
    $chart_06_day_sum[$chart_06_array['result_ym']]		+= $chart_06_array['work_time_sum'];
    $chart_06_row_sum[$chart_06_array['task_req_s_no']] += $chart_06_array['work_time_sum'];
    $chart_06_row_name[$chart_06_array['task_req_s_no']] = $chart_06_array['task_req_s_name'] ? $chart_06_array['task_req_s_name'] : "(NULL)";
    $chart_06_empty_time[$chart_06_array['result_ym']]  += $chart_06_array['work_empty_time'];
    $chart_06_display[$chart_06_array['task_req_s_no']]  = (empty($chart_06_array['s_display']) || $chart_06_array['s_display'] == '3') ? "N" : "Y";
}

arsort($chart_06_row_sum);
ksort($chart_06);

$smarty->assign("chart_06", $chart_06);
$smarty->assign("chart_06_day_sum", $chart_06_day_sum);
$smarty->assign("chart_06_row_sum", $chart_06_row_sum);
$smarty->assign("chart_06_row_name", $chart_06_row_name);
$smarty->assign("chart_06_empty_time", $chart_06_empty_time);
$smarty->assign("chart_06_display", $chart_06_display);
/* ---------- chart_06 총 업무 기여도(work-time)[End] ---------- */

$smarty->display('stats/wise_work_03.html');

?>
