<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_cms.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);
$nowdate = date("Y-m-d H:i:s");
$smarty->assign("nowdate",$nowdate);

//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', "주문일시")
	->setCellValue('B3', "주문번호")
	->setCellValue('C3', "수령자명")
	->setCellValue('D3', "수령자전화")
	->setCellValue('E3', "우편번호")
	->setCellValue('F3', "수령지")
	->setCellValue('G3', "배송메모")
	->setCellValue('H3', "발송처리일")
	->setCellValue('I3', "택배사")
	->setCellValue('J3', "운송자번호")
	->setCellValue('K3', "업체명/브랜드명")
	->setCellValue('L3', "커머스 상품명")
	->setCellValue('M3', "수량")
	->setCellValue('N3', "특이사항")
	->setCellValue('O3', "결제금액")
	->setCellValue('P3', "구분")
	->setCellValue('Q3', "진행상태")
	->setCellValue('R3', "이슈")
	->setCellValue('S3', "결제일시")
	->setCellValue('T3', "구매처");


// 검색쿼리 & GET 초기화 & 보안을 위해 다른 변수에 담기
$add_where = "1=1";

// [상품(업무) 종류]
$sch_prd_g1	= isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	= isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	= isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$sch_get	= isset($_GET['sch'])?$_GET['sch']:"";

// 상품(업무) 종류
if (!empty($sch_prd) && $sch_prd != "0") { // 상품
	$add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
	if($sch_prd_g2){ // 상품 선택 없이 두번째 그룹까지 설정한 경우
		$add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code='{$sch_prd_g2}')";
	}elseif($sch_prd_g1){ // 상품 선택 없이 찻번째 그룹까지 설정한 경우
		$add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no FROM product_cms prd_cms WHERE prd_cms.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
	}
}

$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));
$years_val  = date('Y-m-d', strtotime('-2 years'));
$return_val = date('Y-m-d', strtotime('-15 days'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);
$smarty->assign("years_val", $years_val);
$smarty->assign("return_val", $return_val);

//검색 처리
$sch_w_no 			    = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
$sch_stock_date 	    = isset($_GET['sch_stock_date']) ? $_GET['sch_stock_date'] : "";
$sch_ord_s_date         = isset($_GET['sch_ord_s_date']) ? $_GET['sch_ord_s_date'] : $year_val;
$sch_ord_e_date         = isset($_GET['sch_ord_e_date']) ? $_GET['sch_ord_e_date'] : $today_val;
$sch_ord_date_type      = isset($_GET['sch_ord_date_type']) ? $_GET['sch_ord_date_type'] : "year";
$sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_recipient 		    = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp 	    = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_recipient_addr 	= isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
$sch_delivery_no 	    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
$sch_dp_c_no 		    = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_c_name 		    = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
$sch_prd_name 		    = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_notice             = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$sch_is_refund 		    = isset($_GET['sch_is_refund']) ? $_GET['sch_is_refund'] : "";
$sch_is_deposit 		= isset($_GET['sch_is_deposit']) ? $_GET['sch_is_deposit'] : "";
$sch_cs_type 		    = isset($_GET['sch_cs_type']) ? $_GET['sch_cs_type'] : "";
$sch_cs_issue 		    = isset($_GET['sch_cs_issue']) ? $_GET['sch_cs_issue'] : "";
$sch_cs_manager 		= isset($_GET['sch_cs_manager']) ? $_GET['sch_cs_manager'] : "";
$sch_cs_manager_null	= isset($_GET['sch_cs_manager_null']) ? $_GET['sch_cs_manager_null'] : "";
$sch_cs_status 		    = isset($_GET['sch_cs_status']) ? $_GET['sch_cs_status'] : "";
$sch_cs_comment 		= isset($_GET['sch_cs_comment']) ? $_GET['sch_cs_comment'] : "";
$sch_req_name 		    = isset($_GET['sch_req_name']) ? $_GET['sch_req_name'] : "";
$sch_coupon_price       = isset($_GET['sch_coupon_price']) ? $_GET['sch_coupon_price'] : "";
$sch_coupon_type        = isset($_GET['sch_coupon_type']) ? $_GET['sch_coupon_type'] : "";

if(!empty($sch_w_no)){
	$add_where .= " AND w.w_no='{$sch_w_no}'";
}

if(!empty($sch_stock_date)){
	$add_where .= " AND w.stock_date = '{$sch_stock_date}'";
}

if(!empty($sch_ord_s_date) || !empty($sch_ord_e_date))
{
	if(!empty($sch_ord_s_date) && empty($sch_ord_e_date)){
		$sch_ord_s_datetime = $sch_ord_s_date." 00:00:00";
		$add_where .= " AND w.regdate >= '{$sch_ord_s_datetime}'";
	}elseif(!empty($sch_ord_e_date) && empty($sch_ord_s_date)){
		$sch_ord_e_datetime = $sch_ord_e_date." 23:59:59";
		$add_where .= " AND w.regdate <= '{$sch_ord_e_datetime}'";
	}else{
		$sch_ord_s_datetime = $sch_ord_s_date." 00:00:00";
		$sch_ord_e_datetime = $sch_ord_e_date." 23:59:59";

		$add_where .= " AND (w.regdate BETWEEN '{$sch_ord_s_datetime}' AND '{$sch_ord_e_datetime}')";
	}
}

if(!empty($sch_order_number)){
	$add_where .= " AND w.order_number = '{$sch_order_number}'";
}

if(!empty($sch_recipient)){
	$add_where .= " AND w.recipient = '{$sch_recipient}'";
}

if(!empty($sch_recipient_hp)){
	$add_where .= " AND w.recipient_hp like '%{$sch_recipient_hp}%'";
}

if(!empty($sch_recipient_addr)){
	$add_where .= " AND w.recipient_addr like '%{$sch_recipient_addr}%'";
}

if(!empty($sch_delivery_no)){
	$add_where .= " AND w.order_number IN(SELECT DISTINCT order_number FROM work_cms_delivery wcd WHERE wcd.delivery_no='{$sch_delivery_no}')";
}

if(!empty($sch_dp_c_no)){
	$add_where .= " AND w.dp_c_no='{$sch_dp_c_no}'";
}

if(!empty($sch_c_name)){
	$add_where .= " AND w.c_name like '%{$sch_c_name}%'";
}

if(!empty($sch_prd_name)){
	$add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no from product_cms prd_cms where prd_cms.title like '%{$sch_prd_name}%')";
}

if(!empty($sch_notice)){
	$add_where .= " AND w.notice like '%{$sch_notice}%'";
}

if(!empty($sch_cs_type)){
	if($sch_cs_type == '1'){
		$add_where .= " AND w.shop_ord_no IN(SELECT wcr.parent_shop_ord_no FROM work_cms_return wcr WHERE wcr.return_purpose='1')";
	}elseif($sch_cs_type == '2'){
		$add_where .= " AND w.shop_ord_no IN(SELECT wcr.parent_shop_ord_no FROM work_cms_return wcr WHERE wcr.return_purpose='2')";
	}elseif($sch_cs_type == '3'){
		$add_where .= " AND w.shop_ord_no IN(SELECT wcr.parent_shop_ord_no FROM work_cms_return wcr WHERE wcr.return_purpose='3')";
	}
}

if(!empty($sch_is_refund)) {
	$add_where .= " AND is_refund='{$sch_is_refund}'";
}

if(!empty($sch_is_deposit)) {
	$add_where .= " AND is_deposit='{$sch_is_deposit}'";
}

if(!empty($sch_cs_issue)){
	$add_where .= " AND w.cs_issue like '%{$sch_cs_issue}%'";
}

if(!empty($sch_cs_manager_null)){
	$add_where .= " AND w.order_number NOT IN(SELECT wcm.order_number FROM work_cs_management as wcm WHERE wcm.cs_s_no > 0)";
}else{
	if(!empty($sch_cs_manager)){
		$add_where .= " AND w.order_number IN(SELECT wcm.order_number FROM work_cs_management as wcm WHERE wcm.cs_s_no='{$sch_cs_manager}') ";
	}
}

if(!empty($sch_cs_status)){
	$add_where .= " AND (SELECT dp.dp_state FROM deposit dp WHERE dp_no IN(SELECT sub.dp_no FROM `work` as sub WHERE sub.prd_no='260' AND sub.work_state='6' AND sub.linked_table='work_cms' AND sub.linked_shop_no=w.order_number))='{$sch_cs_status}' ";
}

if(!empty($sch_cs_comment)){
	$add_where .= " AND w.order_number IN(SELECT DISTINCT ord_no FROM work_cms_comment wcc WHERE wcc.comment LIKE '%{$sch_cs_comment}%')";
}

if(!empty($sch_req_name)){
	$add_where .= " AND w.task_req_s_no IN(SELECT s.s_no FROM staff s WHERE s_name LIKE '%{$sch_req_name}%')";
}

if(!empty($sch_coupon_price))
{
	if($sch_coupon_price == '1'){
		$add_where .= " AND (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) > 0";
	}else{
		$add_where .= " AND (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) IS NULL";
	}
}

if(!empty($sch_coupon_type))
{
	if($sch_coupon_type == '1') {
		$add_where .= " AND w.dp_c_no = '1372'";
	}elseif($sch_coupon_type == '2') {
		$add_where .= " AND w.dp_c_no IN(3295,4629,5427,5588)";
	}elseif($sch_coupon_type == '3') {
		$add_where .= " AND w.dp_c_no = '5800'";
	}elseif($sch_coupon_type == '4'){
		$add_where .= " AND w.dp_c_no = '5958'";
	}elseif($sch_coupon_type == '5'){
		$add_where .= " AND w.dp_c_no = '6012'";
	}
}

$work_cs_ord_sql    = "
	SELECT 
	    DISTINCT w.order_number 
	FROM work_cms w 
	LEFT JOIN work_cs_management as wcm ON wcm.order_number=w.order_number 
	WHERE {$add_where} AND w.order_number is not null 
	ORDER BY w.w_no DESC LIMIT 1000";
$work_cs_ord_query  = mysqli_query($my_db, $work_cs_ord_sql);
$order_number_list  = [];
while($order_number = mysqli_fetch_assoc($work_cs_ord_query)){
    $order_number_list[] =  "'".$order_number['order_number']."'";
}

$order_numbers = implode(',', $order_number_list);

$delivery_sql   = "SELECT order_number, delivery_no, delivery_type FROM work_cms_delivery w WHERE w.order_number IN({$order_numbers}) GROUP BY order_number ORDER BY `no` ASC";
$delivery_query = mysqli_query($my_db, $delivery_sql);
$delivery_list  = [];
while($delivery = mysqli_fetch_assoc($delivery_query))
{
	$delivery_list[$delivery['order_number']] = $delivery;
}

// 리스트 쿼리
$work_cs_sql = "
	SELECT
		w.w_no,
		w.order_date,
		w.order_number,
		w.recipient,
		w.recipient_hp,
		w.recipient_addr,
		w.zip_code,
		w.stock_date,
		w.delivery_memo,
		w.payment_date,
		w.dp_c_name,
		w.w_no,
		w.c_name,
		(SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
		w.quantity,
		w.notice,
		w.dp_price_vat,
		w.cs_issue,
	   	wcm.cs_s_no as cs_manager,
		wcm.cs_status as cs_status
	FROM work_cms w
	LEFT JOIN work_cs_management as wcm ON wcm.order_number=w.order_number 
	WHERE w.order_number IN({$order_numbers})
	ORDER BY w.w_no DESC, w.prd_no ASC
";

$result	= mysqli_query($my_db, $work_cs_sql);
$idx 	= 4;
$cs_issue_option = getCsIssueOption();
if(!!$work_cs_query)
{
    while($work_cs = mysqli_fetch_array($work_cs_query))
    {
        $dp_price_vat = number_format($work_cs['dp_price_vat']);
        $cs_issues    = !empty($work_cs['cs_issue']) ? explode(',',$work_cs['cs_issue']) : "";
        $cs_issue     = "";
        if($cs_issues){
        	foreach($cs_issue_option as $key => $label){
        		if(in_array($key, $cs_issues)){
					$cs_issue .= $label;
				}
			}
		}

		$delivery_type  = isset($delivery_list[$work_cs['order_number']]) ? $delivery_list[$work_cs['order_number']]['delivery_type'] : "";
		$delivery_no    = isset($delivery_list[$work_cs['order_number']]) ? $delivery_list[$work_cs['order_number']]['delivery_no'] : "";

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$idx, $work_cs['order_date'])
            ->setCellValueExplicit('B'.$idx, $work_cs['order_number'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('C'.$idx, $work_cs['recipient'])
            ->setCellValue('D'.$idx, $work_cs['recipient_hp'])
            ->setCellValue('E'.$idx, $work_cs['zip_code'])
            ->setCellValue('F'.$idx, $work_cs['recipient_addr'])
			->setCellValueExplicit('G'.$idx, $work_cs['delivery_memo'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('H'.$idx, $work_cs['stock_date'])
            ->setCellValue('I'.$idx, $delivery_type)
            ->setCellValueExplicit('J'.$idx, $delivery_no, PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValue('K'.$idx, $work_cs['c_name'])
            ->setCellValue('L'.$idx, $work_cs['prd_name'])
            ->setCellValue('M'.$idx, $work_cs['quantity'])
            ->setCellValue('N'.$idx, $work_cs['notice'])
            ->setCellValue('O'.$idx, $dp_price_vat)
            ->setCellValue('P'.$idx, "")
            ->setCellValue('Q'.$idx, "")
            ->setCellValue('R'.$idx, $cs_issue)
            ->setCellValue('S'.$idx, $work_cs['payment_date'])
            ->setCellValue('T'.$idx, $work_cs['dp_c_name'])
		;

        $idx++;
    }
}
$idx--;

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "커머스 C/S 리스트");
$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setSize(18);
$objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00BFBFBF');
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("A4:T{$idx}")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle('A4:A'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('I4:I'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('O4:O'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('P4:P'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('Q4:Q'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('R4:R'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('S4:S'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('T4:T'.$idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A3:R'.$idx)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$idx)->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$idx)->getAlignment()->setWrapText(true);


// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);

$objPHPExcel->getActiveSheet()->setTitle('커머스 C&S 리스트');
$objPHPExcel->getActiveSheet()->getStyle('A3:T'.$idx)->applyFromArray($styleArray);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_커머스 C/S 리스트.xls");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>
