<?php
ini_set('max_execution_time', '3600');
ini_set('memory_limit', '5G');

require('Classes/PHPExcel.php');
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/work_sales.php');
require('inc/model/Company.php');
require('inc/model/WorkCmsSettle.php');

# 파일 변수
$company_model      = Company::Factory();
$dp_company_option  = $company_model->getDpTotalList();

$vat_type           = isset($_POST['vat_type']) ? $_POST['vat_type'] : "";
$vat_dp_c_no        = isset($_POST['vat_dp_c_no']) ? $_POST['vat_dp_c_no'] : "";
$vat_month          = isset($_POST['vat_month']) ? $_POST['vat_month'] : date('Y-m', strtotime("-1 months"));
$cur_date           = date("Y-m-d H:i:s");
$file_name          = $_FILES["vat_file"]["tmp_name"];

if(empty($vat_type)){
    exit("<script>alert('부가세자료가 없습니다. 다시 시도해 주세요');location.href='work_cms_sales_vat_list.php';</script>");
}

# 기본 엑셀 소스
$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);

$start_idx = 2;
if($vat_type == '12'){
    $start_idx    = 7;
}
elseif($vat_type == '18'){
    $start_idx    = 4;
}
$objWorksheet = $excel->getActiveSheet();
$totalRow     = $objWorksheet->getHighestRow();

$ins_data_list  = [];
$chk_data_list  = [];
$vat_model      = WorkCmsSettle::Factory();
$vat_model->setMainInit("work_cms_vat", "no");

for ($i = $start_idx; $i <= $totalRow; $i++)
{
    #변수 초기화
    $vat_date   = $cancel_date = $ord_no = $ord_date = $notice = "";
    $card_price = $cash_price = $etc_price = $tax_price = 0;

    $vat_data   = array(
        "vat_type"      => $vat_type,
        "vat_dp_c_no"   => $vat_dp_c_no,
        "vat_dp_c_name" => !empty($vat_dp_c_no) ? $dp_company_option[$vat_dp_c_no] : "",
        "vat_month"     => $vat_month,
        "is_return"     => 2,
        'reg_s_no'      => $session_s_no,
        'reg_team'      => $session_team,
        'regdate'       => $cur_date
    );

    if($vat_type == '1') # 이니시스 신용카드
    {
        $check_idx      = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 번호
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 승인일
        $cancel_date    = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 취소일
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 주문번호
        $price          = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));   // 금액
        $card_text      = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 카드종류
        $status         = (string)trim(addslashes($objWorksheet->getCell("Y{$i}")->getValue()));   // 상태

        if($check_idx == '합계'){
            continue;
        }

        if($card_text == '카카오머니' || $card_text == '토스머니'){
            $cash_price = $price;
        }elseif($card_text == 'SSG머니' || $card_text == '티머니' || $card_text == '페이코포인트'){
            $etc_price  = $price;
        }else{
            $card_price = $price;
        }

        if($status != "승인"){
            $notice = "#{$status}";
        }
    }
    elseif($vat_type == '2' || $vat_type == '3') # 이니시스 계좌이체 || 가상계좌
    {
        $check_idx      = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));   // 번호
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 승인일
        $cancel_date    = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 취소일
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 주문번호
        $cash_price     = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));   // 금액
        $status         = (string)trim(addslashes($objWorksheet->getCell("R{$i}")->getValue()));   // 상태

        if($check_idx == '합계'){
            continue;
        }

        if($status != "승인"){
            $notice = "#{$status}";
        }
    }
    elseif($vat_type == '4' || $vat_type == '5') # 네이버 N페이 || 스토어팜
    {
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 기준일
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 주문번호
        $card_price     = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));    // 신용카드매출전표
        $cash_price_1   = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));    // 현금(소득공제)
        $cash_price_2   = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));    // 현금(지출증빙)
        $cash_price     = (int)$cash_price_1+(int)$cash_price_2;
        $etc_price      = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));    // 기타
        $status         = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 상태

        if($status != "원주문 매출"){
            $notice = "#{$status}";
        }
    }
    elseif($vat_type == '6') # 쿠팡
    {
        $vat_data['vat_dp_c_no']   = "1812";
        $vat_data['vat_dp_c_name'] = "쿠팡";
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 매출인식일
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 주문번호
        $card_price_1   = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));    // 신용카드(판매)
        $card_price_2   = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));    // 신용카드(환불)
        $card_price     = (int)$card_price_1+(int)$card_price_2;
        $cash_price_1   = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));    // 현금(판매)
        $cash_price_2   = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));    // 현금(환불)
        $cash_price     = (int)$cash_price_1+(int)$cash_price_2;
        $etc_price_1    = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    // 기타(판매)
        $etc_price_2    = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));    // 기타(환불)
        $etc_price      = (int)$etc_price_1+(int)$etc_price_2;
        $status         = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 항목유형

        if($status != "상품비"){
            $notice = "#{$status}";
        }
    }
    elseif($vat_type == '7') # 통합양식
    {
        $vat_type_name  = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 부가세자료
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 승인일
        $cancel_date    = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 취소일
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));    // 주문번호
        $card_price     = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));    // 카드
        $cash_price     = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));    // 현금
        $etc_price      = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));    // 기타
        $tax_price      = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));    // 세금계산서
        $notice         = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));    // 비고

        $vat_data['vat_dp_c_no']   = array_search($vat_type_name, $dp_company_option);
        $vat_data['vat_dp_c_name'] = $vat_type_name;
    }
    elseif($vat_type == '8') # 카카오페이(건별통합)
    {
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 구매결정일
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 주문번호
        $card_price     = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));    // 카드
        $card_price     = str_replace(',','', $card_price);
        $cash_price_1   = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   // 휴대폰
        $cash_price_1   = str_replace(',','', $cash_price_1);
        $cash_price_2   = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));   // 현금영수증
        $cash_price_2   = str_replace(',','', $cash_price_2);
        $cash_price_3   = (string)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));   // 기타
        $cash_price_3   = str_replace(',','', $cash_price_3);
        $cash_price     = (int)$cash_price_1+(int)$cash_price_2+(int)$cash_price_3;
        $status         = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 구분

        if($status != "상품"){
            $notice = "#{$status}";
        }
    }
    elseif($vat_type == '10') # 지마켓 상품
    {
        $vat_data['vat_dp_c_no']   = "1834";
        $vat_data['vat_dp_c_name'] = "지마켓";
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 주문번호
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));    // 입금확인일
        $tmp_price      = (string)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));    // 결제금액
        $tmp_price      = str_replace(',','', $tmp_price);
        $type_check     = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 증빙종류

        if($type_check == "카드"){
            $card_price = $tmp_price;
        }elseif($type_check == "현금영수증"){
            $cash_price = $tmp_price;
        }else{
            $etc_price = $tmp_price;
        }
    }
    elseif($vat_type == '12') # 11번가
    {
        $vat_data['vat_dp_c_no']   = "1818";
        $vat_data['vat_dp_c_name'] = "11번가";
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));    // 주문번호
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));    // 정산확정처리일
        $card_price     = (string)trim(addslashes($objWorksheet->getCell("AB{$i}")->getValue()));   // 신용카드
        $cash_price_1   = (string)trim(addslashes($objWorksheet->getCell("AC{$i}")->getValue()));   // 현금영수증(소득공제)
        $cash_price_2   = (string)trim(addslashes($objWorksheet->getCell("AD{$i}")->getValue()));   // 현금영수증(지출증빙용)
        $cash_price     = (int)$cash_price_1+(int)$cash_price_2;
        $etc_price_1    = (string)trim(addslashes($objWorksheet->getCell("AE{$i}")->getValue()));    // 휴대폰결제금액
        $etc_price_2    = (string)trim(addslashes($objWorksheet->getCell("AF{$i}")->getValue()));    // 기타결제금액
        $etc_price      = (int)$etc_price_1+(int)$etc_price_2;
    }
    elseif($vat_type == '13') # 오늘의집
    {
        $vat_data['vat_dp_c_no']   = "3337";
        $vat_data['vat_dp_c_name'] = "오늘의집";
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));    // 주문번호
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 구매확정일
        $etc_price      = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));    // 기타금액
        $type_check     = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 정산구분

        if($type_check != "구매확정" && $type_check != "구매확정후취소" && $type_check != "클레임완료" && $type_check != "구매확정주문 취소"){
            continue;
        }
    }
    elseif($vat_type == '14') # 오늘의집(세부할인내역)
    {
        $vat_data['vat_dp_c_no']   = "3337";
        $vat_data['vat_dp_c_name'] = "오늘의집";
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 주문번호
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 구매확정/취소일
        $etc_price_1    = (string)trim(addslashes(str_replace(",","",$objWorksheet->getCell("I{$i}")->getValue())));    // 포인트 할인금액
        $etc_price_2    = (string)trim(addslashes(str_replace(",","",$objWorksheet->getCell("J{$i}")->getValue())));    // 장바구니 쿠폰 할인금액
        $etc_price_3    = (string)trim(addslashes(str_replace(",","",$objWorksheet->getCell("K{$i}")->getValue())));    // 상품쿠폰 파트너 할인금액
        $etc_price_4    = (string)trim(addslashes(str_replace(",","",$objWorksheet->getCell("L{$i}")->getValue())));    // 상품쿠폰 오늘의집 할인금액
        $etc_price      = (int)$etc_price_1+(int)$etc_price_2+(int)$etc_price_3+(int)$etc_price_4;
    }
    elseif($vat_type == '15') # 지그재그
    {
        $vat_data['vat_dp_c_no']   = "4737";
        $vat_data['vat_dp_c_name'] = "지그재그";
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));    // 주문번호
        $tmp_date       = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));    // 날짜
        $type_check     = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));    // 구분
        $etc_price_1    = (string)trim(addslashes(str_replace(",","",$objWorksheet->getCell("E{$i}")->getValue())));    // 상품주문액
        $etc_price_2    = (string)trim(addslashes(str_replace(",","",$objWorksheet->getCell("G{$i}")->getValue())));    // 스토어 쿠폰 부담 금액
        $etc_price_3    = (string)trim(addslashes(str_replace(",","",$objWorksheet->getCell("H{$i}")->getValue())));    // 프로모션 부담금액
        $etc_price      = (int)$etc_price_1+(int)$etc_price_2+(int)$etc_price_3;

        if($type_check == '취소'){
            $cancel_date = $tmp_date;
            $notice      = "#취소";
        }else{
            $vat_date = $tmp_date;
        }
    }
    elseif($vat_type == '16') # 다날_신용카드
    {
        $payment_type   = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 결제수단
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 거래일시
        $ord_no         = $objWorksheet->getCell("I{$i}")->getValue();   // 주문번호
        $price          = (string)trim(addslashes($objWorksheet->getCell("K{$i}")->getValue()));   // 금액

        if(strpos($ord_no, "E+") !== false){
            $ord_no = sprintf("%.0f", $ord_no);
        }

        if($payment_type == '신용카드'){
            $card_price = $price;
        }else{
            echo "결제수단이 신용카드가 아닙니다.<br/>ROW: {$i}, 결제수단:{$payment_type}";
            exit;
        }
    }
    elseif($vat_type == '17') # 다날_핸드폰결제
    {
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 거래일시
        $ord_no         = $objWorksheet->getCell("H{$i}")->getValue();   // 주문번호
        $etc_price      = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));   // 금액

        if(strpos($ord_no, "E+") !== false){
            $ord_no = sprintf("%.0f", $ord_no);
        }
    }
    elseif($vat_type == '18') # 현대이지웰
    {
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("AA{$i}")->getValue()));   // 거래일시
        $ord_no         = $objWorksheet->getCell("E{$i}")->getValue();   // 주문번호
        $tax_price      = (string)trim(addslashes($objWorksheet->getCell("X{$i}")->getValue()));   // 금액

        $vat_data['vat_dp_c_no']   = "5641";
        $vat_data['vat_dp_c_name'] = "현대이지웰";
    }
    elseif($vat_type == '19') # 토스페이먼츠 신용카드
    {
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("B{$i}")->getValue()));   // 결제일시
        $cancel_date    = (string)trim(addslashes($objWorksheet->getCell("C{$i}")->getValue()));   // 취소일
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 주문번호
        $card_text      = (string)trim(addslashes($objWorksheet->getCell("L{$i}")->getValue()));   // 결제기관
        $payment_price  = (int)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 결제액
        $cancel_price   = (int)trim(addslashes($objWorksheet->getCell("J{$i}")->getValue()));   // 취소액
        $status         = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 결제상태

        if($card_text == 'PAYCO' || $card_text == '토스페이'){
            $etc_price = $payment_price + $cancel_price;
        }else{
            $card_price = $payment_price + $cancel_price;
        }

        if($status == "취소" || $status == "부분취소"){
            $notice = "#{$status}";
        }
    }
    elseif($vat_type == '20') # 토스페이먼츠 가상계좌
    {
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 정산완료일시
        $cancel_date    = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 취소완료일시
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("H{$i}")->getValue()));   // 주문번호
        $cash_price     = (int)trim(addslashes($objWorksheet->getCell("O{$i}")->getValue()));      // 결제취소액
        $status         = (string)trim(addslashes($objWorksheet->getCell("I{$i}")->getValue()));   // 결제상태

        if($status == "취소" || $status == "부분취소"){
            $notice = "#{$status}";
        }
    }
    elseif($vat_type == '21') # 토스페이먼츠 계좌이체
    {
        $vat_date       = (string)trim(addslashes($objWorksheet->getCell("E{$i}")->getValue()));   // 정산완료일시
        $cancel_date    = (string)trim(addslashes($objWorksheet->getCell("D{$i}")->getValue()));   // 취소완료일시
        $ord_no         = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));   // 주문번호
        $cash_price     = (int)trim(addslashes($objWorksheet->getCell("M{$i}")->getValue()));      // 결제취소액
        $status         = (string)trim(addslashes($objWorksheet->getCell("G{$i}")->getValue()));   // 결제상태

        if($status == "취소" || $status == "부분취소"){
            $notice = "#{$status}";
        }
    }

    $ord_no = str_replace("-P1", "", $ord_no);
    if($vat_data['vat_dp_c_no'] == 0 || empty($vat_data['vat_dp_c_no']))
    {
        echo "판매처를 찾을 수 없습니다.<br/>ROW: {$i}, 판매처:{$vat_data['vat_dp_c_name']}, 승인일/매출인식일/전산확정일: {$vat_date}, 주문번호: {$ord_no}, 카드: {$card_price}, 현금: {$cash_price}, 기타: {$etc_price}, 세금계산서: {$tax_price}";
        exit;
    }

    if(!empty($vat_date))
    {
        if(strpos($vat_date, "T") !== false){
            $vat_date = str_replace("T"," ", $vat_date);
            $vat_date = date("Y-m-d", strtotime($vat_date));
        }elseif(strpos($vat_date, "-") !== false){
            $vat_date = $vat_date;
        }elseif(strpos($vat_date, ".") !== false){
            $vat_date = str_replace(".","-", $vat_date);
            $vat_date = date("Y-m-d", strtotime($vat_date));
        }elseif(strpos($vat_date, "/") !== false){
            $vat_date = str_replace("/","-", $vat_date);
            $vat_date = date("Y-m-d", strtotime($vat_date));
        }else{
            $vat_time_cal = ($vat_date-25569)*86400;
            $vat_date     = date("Y-m-d", $vat_time_cal);
        }

        if(!empty($vat_date) && $vat_date != "0000-00-00"){
            $vat_data['vat_date'] = $vat_date;
        }
    }

    if(!empty($cancel_date))
    {
        if(strpos($cancel_date, "T") !== false){
            $cancel_date = str_replace("T"," ", $cancel_date);
            $cancel_date = date("Y-m-d", strtotime($cancel_date));
        }elseif(strpos($cancel_date, "-") !== false){
            $cancel_date = $cancel_date;
        }elseif(strpos($cancel_date, ".") !== false){
            $cancel_date = str_replace(".","-", $cancel_date);
            $cancel_date = date("Y-m-d", strtotime($cancel_date));
        }elseif(strpos($cancel_date, "/") !== false){
            $cancel_date = str_replace("/","-", $cancel_date);
            $cancel_date = date("Y-m-d", strtotime($cancel_date));
        }else{
            $cancel_time_cal = ($cancel_date-25569)*86400;
            $cancel_date     = date("Y-m-d", $cancel_time_cal);
        }

        if($cancel_date != "0000-00-00"){
            $vat_data['cancel_date'] = $cancel_date;
        }
    }

    $vat_data['card_price'] = $card_price;
    $vat_data['cash_price'] = $cash_price;
    $vat_data['etc_price']  = $etc_price;
    $vat_data['tax_price']  = $tax_price;
    $vat_data['notice']     = $notice;

    # 중복 체크
    $check_sql      = "SELECT count(*) as cnt FROM work_cms_vat WHERE order_number='{$ord_no}' AND vat_date='{$vat_date}' AND card_price='{$card_price}' AND cash_price='{$cash_price}' AND etc_price='{$etc_price}' AND tax_price='{$tax_price}'";
    $check_query    = mysqli_query($my_db, $check_sql);
    $check_result   = mysqli_fetch_assoc($check_query);

    if($check_result['cnt'] > 0){
        echo "[중복 확인] 동일한 내용이 이미 업로드 된것 같습니다.<br/>ROW: {$i}, 승인일/매출인식일/전산확정일: {$vat_date}, 주문번호: {$ord_no}, 카드: {$card_price}, 현금: {$cash_price}, 기타: {$etc_price}, 세금계산서: {$tax_price}";
        exit;
    }

    # 브랜드 체크
    if(!empty($ord_no))
    {
        $is_order        = true;
        $vat_total_price = $vat_data['card_price'] + $vat_data['cash_price'] + $vat_data['etc_price'] + $vat_data['tax_price'];

        $vat_data['order_number'] = $ord_no;
        $vat_data['total_ord_no'] = $ord_no;

        if($vat_total_price < 0)
        {
            $return_sql      = "SELECT parent_order_number, c_no, c_name, COUNT(DISTINCT c_no) as c_cnt FROM work_cms_return WHERE parent_order_number='{$ord_no}'";
            $return_query    = mysqli_query($my_db, $return_sql);
            $return_item     = mysqli_fetch_assoc($return_query);

            if(!isset($return_item['c_no']))
            {
                $return_sql      = "SELECT parent_order_number, c_no, c_name, COUNT(DISTINCT c_no) as c_cnt FROM work_cms_return WHERE parent_order_number=(SELECT order_number FROM work_cms WHERE origin_ord_no='{$ord_no}' AND unit_price > 0 LIMIT 1)";
                $return_query    = mysqli_query($my_db, $return_sql);
                $return_item     = mysqli_fetch_assoc($return_query);
            }

            if($return_item['c_cnt'] > 0)
            {
                $is_order                   = false;
                $vat_data["is_return"]      = 1;
                $vat_data['c_no']           = ($return_item['c_cnt'] > 1) ? 9999 : $return_item['c_no'];
                $vat_data['c_name']         = ($return_item['c_cnt'] > 1) ? "혼합" : $return_item['c_name'];
                $vat_data['total_ord_no']   = $return_item['parent_order_number'];

                if($vat_data['c_no'] == 9999){
                    $vat_data['chk_type']   = "return";
                    $vat_data['total_price']= $return_item['c_cnt'];
                    $chk_data_list[]        = $vat_data;
                }else{
                    $ins_data_list[] = $vat_data;
                }
            }
        }

        if($is_order)
        {
            $order_sql      = "SELECT order_number, c_no, c_name, COUNT(DISTINCT c_no) as c_cnt, SUM(unit_price) as total_price FROM work_cms WHERE order_number='{$ord_no}' AND unit_price > 0";
            $order_query    = mysqli_query($my_db, $order_sql);
            $ord_item       = mysqli_fetch_assoc($order_query);

            if(!isset($ord_item['c_no']))
            {
                $order_sql      = "SELECT order_number, c_no, c_name, COUNT(DISTINCT c_no) as c_cnt, SUM(unit_price) as total_price FROM work_cms WHERE origin_ord_no='{$ord_no}' AND unit_price > 0";
                $order_query    = mysqli_query($my_db, $order_sql);
                $ord_item       = mysqli_fetch_assoc($order_query);
            }

            if($ord_item['c_cnt'] > 0) {
                $vat_data['c_no']         = ($ord_item['c_cnt'] > 1) ? 9999 : $ord_item['c_no'];
                $vat_data['c_name']       = ($ord_item['c_cnt'] > 1) ? "혼합" : $ord_item['c_name'];
                $vat_data['total_ord_no'] = $ord_item['order_number'];
            }
            else {
                $vat_data['c_no']   = 0;
                $vat_data['c_name'] = "NULL";
            }

            if($vat_data['c_no'] == 9999){
                $vat_data['chk_type']   = "order";
                $vat_data['total_price']= $ord_item['total_price'];
                $chk_data_list[] = $vat_data;
            }else{
                $ins_data_list[] = $vat_data;
            }
        }
    }
    else
    {
        $vat_data['c_no']   = 0;
        $vat_data['c_name'] = "NULL";
        $ins_data_list[]    = $vat_data;
    }
}

if(!empty($chk_data_list))
{
    foreach($chk_data_list as $chk_data)
    {
        $chk_type           = $chk_data['chk_type'];
        $order_number       = $chk_data['total_ord_no'];
        $ord_total_price    = $chk_data['total_price'];
        $total_card_price   = $chk_data['card_price'];
        $total_cash_price   = $chk_data['cash_price'];
        $total_etc_price    = $chk_data['etc_price'];
        $total_tax_price    = $chk_data['tax_price'];
        $chk_card_price     = $total_card_price;
        $chk_cash_price     = $total_cash_price;
        $chk_etc_price      = $total_etc_price;
        $chk_tax_price      = $total_tax_price;
        $chk_ord_list       = [];

        unset($chk_data['total_price']);
        unset($chk_data['chk_type']);

        if($chk_type == 'order')
        {
            $chk_ord_sql    = "SELECT c_no, c_name, SUM(unit_price) as unit_total FROM work_cms WHERE unit_price > 0 AND order_number='{$order_number}' GROUP BY c_no";
            $chk_ord_query  = mysqli_query($my_db, $chk_ord_sql);
            while($chk_ord = mysqli_fetch_assoc($chk_ord_query))
            {
                $chk_ord_list[] = $chk_ord;
            }
        }
        elseif($chk_type == 'return')
        {
            $chk_ord_sql    = "SELECT c_no, c_name FROM work_cms_return WHERE parent_order_number='{$order_number}' GROUP BY c_no";
            $chk_ord_query  = mysqli_query($my_db, $chk_ord_sql);
            while($chk_ord = mysqli_fetch_assoc($chk_ord_query))
            {
                $chk_ord['unit_total'] = 1;
                $chk_ord_list[] = $chk_ord;
            }
        }

        $chk_ord_cnt = count($chk_ord_list);
        $chk_ord_idx = 1;
        foreach($chk_ord_list as $chk_ord)
        {
            $chk_vat_data            = $chk_data;
            $chk_vat_data['c_no']    = $chk_ord['c_no'];
            $chk_vat_data['c_name']  = $chk_ord['c_name'];

            $cal_ord_per        = $chk_ord['unit_total']/$ord_total_price;
            $cal_card_price     = round($total_card_price*$cal_ord_per);
            $cal_cash_price     = round($total_cash_price*$cal_ord_per);
            $cal_etc_price      = round($total_etc_price*$cal_ord_per);
            $cal_tax_price      = round($total_tax_price*$cal_ord_per);

            $chk_card_price     -= $cal_card_price;
            $chk_cash_price     -= $cal_cash_price;
            $chk_etc_price      -= $cal_etc_price;
            $chk_tax_price      -= $cal_tax_price;

            if($chk_ord_cnt == $chk_ord_idx){
                $cal_card_price += $chk_card_price;
                $cal_cash_price += $chk_cash_price;
                $cal_etc_price  += $chk_etc_price;
                $cal_tax_price  += $chk_tax_price;
            }

            $chk_vat_data['card_price']  = $cal_card_price;
            $chk_vat_data['cash_price']  = $cal_cash_price;
            $chk_vat_data['etc_price']   = $cal_etc_price;
            $chk_vat_data['tax_price']   = $cal_tax_price;

            $chk_ord_idx++;
            $ins_data_list[] = $chk_vat_data;
        }
    }
}

$result = true;
$result_cnt = 0;
foreach($ins_data_list as $ins_data)
{
    if(!$vat_model->insert($ins_data)){
        $result = false;
        $result_cnt++;
    }
}

if (!$result){
    echo "부가세관리 반영에 {$result_cnt}건 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    exit;
}else{
    exit("<script>alert('부가세 데이터가 반영 되었습니다.');location.href='work_cms_sales_vat_list.php';</script>");
}

?>
