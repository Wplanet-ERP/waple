<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/company.php');
require('inc/helper/project.php');

// 접근 권한
if (!$session_s_no){
    $smarty->display('access_company_error.html');
    exit;
}

$proc = (isset($_POST['process']))?$_POST['process']:"";

/////////////////////////* 자동 저장 처리 부분 *//////////////////////
if($proc == "add_external_partner")
{
    $pj_no      = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_c_no    = isset($_POST['pj_c_no']) ? $_POST['pj_c_no'] : "";
    $pj_c_name  = isset($_POST['pj_c_name']) ? $_POST['pj_c_name'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";

    $chk_sql = "SELECT count(*) as cnt FROM project_external_report WHERE pj_no='{$pj_no}' AND active='1' AND pj_c_no='{$pj_c_no}'";
    $chk_query = mysqli_query($my_db, $chk_sql);
    $chk_result = mysqli_fetch_assoc($chk_query);

    if($chk_result['cnt'] < 2){
        $ins_sql = "INSERT INTO project_external_report SET pj_no='{$pj_no}', pj_c_no ='{$pj_c_no}', pj_c_name='{$pj_c_name}', regdate=now(), active='1'";
        if(mysqli_query($my_db, $ins_sql)){
            exit("<script>alert('추가했습니다');window.close();opener.reloadExtenerPartner()</script>");
        }else{
            exit("<script>alert('실패했습니다');location.href='project_regist_external_partner_list.php?{$search_url}';</script>");
        }
    }else{
        exit("<script>alert('이미 등록되어 있습니다');location.href='project_regist_external_partner_list.php?{$search_url}';</script>");
    }

}else{

    $pj_no = isset($_GET['pj_no']) ? $_GET['pj_no'] : "";
    $pj_no = (empty($pj_no) && isset($_POST['pj_no'])) ? $_POST['pj_no'] : $pj_no;

    if(!$pj_no)
    {
        exit("<script>alert('프로젝트 번호가 없습니다');</script>");
    }
    $smarty->assign('pj_no', $pj_no);

    # 검색 초기화 및 조건 생성
    $corp_kind_option       = getCorpKindOption();
    $license_type_option    = getLicenseTypeOption();
    $resource_type_option   = getResourceTypeOption();

    $add_where          = "1=1 AND c.partner_state='2' AND c.participation='1'";
    $add_res_type_column= "";
    $sch_c_no           = isset($_GET['sch_c_no']) ? $_GET['sch_c_no'] : "";
    $sch_license_type   = isset($_GET['sch_license_type']) ? $_GET['sch_license_type'] : "";
    $sch_corp_kind      = isset($_GET['sch_corp_kind']) ? $_GET['sch_corp_kind'] : "3";
    $sch_c_name         = isset($_GET['sch_c_name']) ? $_GET['sch_c_name'] : "";
    $sch_resource_type  = isset($_GET['sch_resource_type']) ? $_GET['sch_resource_type'] : "";
    $sch_memo           = isset($_GET['sch_memo']) ? $_GET['sch_memo'] : "";
    $sch_s_avg          = isset($_GET['sch_s_avg']) ? $_GET['sch_s_avg'] : "";
    $sch_e_avg          = isset($_GET['sch_e_avg']) ? $_GET['sch_e_avg'] : "";
    $sch_s_cnt          = isset($_GET['sch_s_cnt']) ? $_GET['sch_s_cnt'] : "";
    $sch_e_cnt          = isset($_GET['sch_e_cnt']) ? $_GET['sch_e_cnt'] : "";

    if(!empty($sch_c_no))
    {
        $add_where .= " AND `c`.c_no = '{$sch_c_no}'";
        $smarty->assign('sch_c_no', $sch_c_no);
    }

    if(!empty($sch_license_type))
    {
        $add_where .= " AND `c`.license_type = '{$sch_license_type}'";
        $smarty->assign('sch_license_type', $sch_license_type);
    }

    if(!empty($sch_corp_kind))
    {
        $add_where .= " AND `c`.corp_kind = '{$sch_corp_kind}'";
        $smarty->assign('sch_corp_kind', $sch_corp_kind);
    }

    if(!empty($sch_c_name))
    {
        $add_where .= " AND `c`.c_name like '%{$sch_c_name}%'";
        $smarty->assign('sch_c_name', $sch_c_name);
    }

    if(!empty($sch_resource_type))
    {
        $add_where .= " AND `c`.resource_type like '%{$sch_resource_type}%'";
        $add_res_type_column = "
            ,(SELECT COUNT(pere.pj_ere_no) FROM project_external_report_evaluation as pere WHERE pere.pj_er_no IN(SELECT sub.pj_er_no FROM project_external_report sub WHERE sub.pj_c_no=c.c_no AND sub.pj_c_type='{$sch_resource_type}' AND sub.active='1' AND sub.pj_participation='2')) as part_cnt
            ,(SELECT ROUND(AVG(pere.score),2) FROM project_external_report_evaluation as pere WHERE pere.pj_er_no IN(SELECT sub.pj_er_no FROM project_external_report sub WHERE sub.pj_c_no=c.c_no AND sub.pj_c_type='{$sch_resource_type}' AND sub.active='1' AND sub.pj_participation='2')) as part_avg
        ";
        $sch_resource_type_name = $resource_type_option[$sch_resource_type];
        $smarty->assign('sch_resource_type_name', $sch_resource_type_name);
        $smarty->assign('sch_resource_type', $sch_resource_type);
    }

    if(!empty($sch_s_avg))
    {
        $add_where .= " AND (SELECT ROUND(AVG(pere.score),2) FROM project_external_report_evaluation as pere WHERE pere.pj_c_no = c.c_no) >= '{$sch_s_avg}'";
        $smarty->assign('sch_s_avg', $sch_s_avg);
    }

    if(!empty($sch_e_avg))
    {
        $add_where .= " AND (SELECT ROUND(AVG(pere.score),2) FROM project_external_report_evaluation as pere WHERE pere.pj_c_no = c.c_no) <= '{$sch_e_avg}'";
        $smarty->assign('sch_e_avg', $sch_e_avg);
    }

    if(!empty($sch_s_cnt))
    {
        $add_where .= " AND (SELECT COUNT(pere.pj_ere_no) FROM project_external_report_evaluation as pere WHERE pere.pj_c_no = c.c_no) >= '{$sch_s_cnt}'";
        $smarty->assign('sch_s_cnt', $sch_s_cnt);
    }

    if(!empty($sch_e_cnt))
    {
        $add_where .= " AND (SELECT COUNT(pere.pj_ere_no) FROM project_external_report_evaluation as pere WHERE pere.pj_c_no = c.c_no) <= '{$sch_e_cnt}'";
        $smarty->assign('sch_e_cnt', $sch_e_cnt);
    }

    if(!empty($sch_memo))
    {
        $add_where .= " AND `c`.c_memo like '%{$sch_memo}%'";
        $smarty->assign('sch_memo', $sch_memo);
    }

    // 전체 게시물 수
    $partner_total_sql		= "SELECT count(c_no) FROM (SELECT `c`.c_no FROM company c WHERE {$add_where}) AS cnt";
    $partner_total_query	= mysqli_query($my_db, $partner_total_sql);
    if(!!$partner_total_query)
        $partner_total_result = mysqli_fetch_array($partner_total_query);

    $partner_total = $partner_total_result[0];

    $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= 10;
    $offset 	= ($pages-1) * $num;
    $pagenum 	= ceil($partner_total/$num);

    if ($pages >= $pagenum){$pages = $pagenum;}
    if ($pages <= 0){$pages = 1;}

    $search_url = getenv("QUERY_STRING");
    $page		= pagelist($pages, "project_regist_external_partner_list.php", $pagenum, $search_url);
    $smarty->assign("search_url", $search_url);
    $smarty->assign("total_num", $partner_total);
    $smarty->assign("pagelist", $page);

    $partner_sql = "
        SELECT
            c.c_no,
            c.c_name,
            c.corp_kind,
            c.license_type,
            c.resource_type,
            c.c_memo,
            (SELECT COUNT(pere.pj_ere_no) FROM project_external_report_evaluation as pere WHERE pere.pj_c_no = c.c_no) as total_cnt,
            (SELECT ROUND(AVG(pere.score),2) FROM project_external_report_evaluation as pere WHERE pere.pj_c_no = c.c_no) as total_avg
            {$add_res_type_column}
		FROM
			company c
		WHERE {$add_where}
		ORDER BY `c`.c_no DESC
        LIMIT {$offset}, {$num} 
	";
    $partner_query	= mysqli_query($my_db, $partner_sql);
    $partner_list   = [];
    if(!!$partner_query) {
        while ($partner = mysqli_fetch_assoc($partner_query))
        {
            $partner['corp_kind_name']      = isset($corp_kind_option[$partner['corp_kind']]) ? $corp_kind_option[$partner['corp_kind']] : "";
            $partner['license_type_name']   = isset($license_type_option[$partner['license_type']]) ? $license_type_option[$partner['license_type']] : "";

            $resource_type_name = [];
            if(!empty($partner['resource_type']))
            {
                $resource_type_val_list = explode(',', $partner['resource_type']);
                foreach($resource_type_val_list as $resource_type){
                    $resource_type_name[] = $resource_type_option[$resource_type];
                }
            }
            $partner['resource_type_name'] = !empty($resource_type_name) ? implode('  ', $resource_type_name) : "";

            if(empty($partner['total_avg'])){
                $partner['total_avg'] = 0;
            }

            $partner_list[] = $partner;
        }
    }

    $smarty->assign("corp_kind_option", $corp_kind_option);
    $smarty->assign("license_type_option", $license_type_option);
    $smarty->assign("resource_type_option", $resource_type_option);
    $smarty->assign("partner_list", $partner_list);

    $smarty->display('project_regist_external_partner_list.html');
}

?>
