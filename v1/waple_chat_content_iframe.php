<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/model/WapleChat.php');

$chat_model = WapleChat::Factory();
$wc_no      = $_GET['wc_no'];

$chk_per_sql        = "SELECT COUNT(*) as cnt FROM waple_chat_user wcu LEFT JOIN waple_chat as wc ON wc.wc_no=wcu.wc_no WHERE wc.display='1' AND wc.`type`='3' AND wcu.`wc_no` ='{$wc_no}' AND wcu.`user` = '{$session_s_no}'";
$chk_per_query      = mysqli_query($my_db, $chk_per_sql);
$chk_per_result     = mysqli_fetch_assoc($chk_per_query);
$chk_per_cnt        = isset($chk_per_result['cnt']) ? $chk_per_result['cnt'] : 0;
$waple_chat_list    = [];

if($chk_per_cnt > 0)
{
    # 와플 알림톡 쿼리
    $waple_chat_sql = "
        SELECT
            wcc.wcc_no,
            wcc.file_path,
            wcc.file_name,
            wcc.content,
            wcc.s_no,
            s.s_name as s_name,
            s.profile_img as profile,
            DATE_FORMAT(wcc.regdate, '%Y-%m-%d %H:%i') AS regdate,
            (SELECT COUNT(wcr.wcr_no) FROM waple_chat_read wcr WHERE wcr.wcc_no=wcc.wcc_no AND wcr.read_s_no='{$session_s_no}') AS read_cnt
        FROM waple_chat_content wcc
        LEFT JOIN staff s ON s.s_no=wcc.s_no
        WHERE wcc.wc_no='{$wc_no}'
        ORDER BY wcc.regdate ASC, wcc.wcc_no ASC
    ";
    $waple_chat_query       = mysqli_query($my_db, $waple_chat_sql);
    $waple_chat_first_new   = false;
    while($waple_chat = mysqli_fetch_assoc($waple_chat_query))
    {
        $file_list = [];
        if(!empty($waple_chat['file_path']))
        {
            $chat_file_paths = explode(",", $waple_chat['file_path']);
            $chat_file_names = explode(",", $waple_chat['file_name']);
            foreach($chat_file_paths as $key => $file_path)
            {
                $file_name   = $chat_file_names[$key];
                $file_list[] = array("path" => $file_path, "name" => $file_name);
            }
        }

        if(!empty($waple_chat['profile']) ) {
            $waple_chat['profile'] = ($waple_chat['type'] != 2) ? "uploads/{$waple_chat['profile']}" : $waple_chat['profile'];
        }else{
            $waple_chat['profile'] = "images/staff_profile_base.png";
        }

        if(strpos($waple_chat['content'], "http") !== false)
        {
            $content_val  = explode("<br/>", $waple_chat['content']);
            $content_list = [];

            foreach($content_val as $content_tmp){
                if(strpos($content_tmp, "http") !== false){
                    $content_list[] = "<a href='{$content_tmp}' target='_blank' style='font-size: 10pt;' class='chat-content-link'>{$content_tmp}</a>";
                }else{
                    $content_list[] = $content_tmp;
                }
            }

            $waple_chat['content'] = implode("<br/>", $content_list);
        }

        $waple_chat['file_list'] = $file_list;
        $waple_chat['is_new']    = !$waple_chat_first_new && ($waple_chat['s_no'] != $session_s_no && $waple_chat['read_cnt'] == 0) ? true : false;
        $waple_chat_list[]       = $waple_chat;

        if($waple_chat['s_no'] != $session_s_no && $waple_chat['read_cnt'] == 0) {
            $waple_chat_first_new = true;
            $chat_model->readWapleChat($waple_chat['wcc_no'], $session_s_no);
        }
    }
}

$smarty->assign("wc_no", $wc_no);
$smarty->assign("waple_chat_list", $waple_chat_list);
$smarty->display('waple_chat_content_iframe.html');
?>
