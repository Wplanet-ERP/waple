<?php
ini_set('max_execution_time', '360');
ini_set('memory_limit', '5G');

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');

function get_time() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$file_name = $_FILES["ba_file"]["tmp_name"];

$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$worksheet = $excel->getActiveSheet();
$totalRow  = $worksheet->getHighestRow()+1;

$total_time = 0;
$comma = '';
$sql = " INSERT INTO `broadcast_advertisement`
            (media, adv_no, advertiser, state, `type`, program_title, s_time, e_time, `date_w`, hourly_wage, sub_s_date, sub_e_date, second_cnt, application_rate, unit_price, base_count, total_price, billing_price, notice, regdate, req_name, req_date, detail_media, subs_gubun, delivery_gubun)
          VALUES ";

$adv_no     = (isset($_POST['ba_adv_no']))?$_POST['ba_adv_no']:"";
$advertiser = (isset($_POST['ba_advertiser']))?$_POST['ba_advertiser']:"";
$excel_type = (isset($_POST['ba_excel']))?$_POST['ba_excel']:"";
$search_url = (isset($_POST['search_url']))?$_POST['search_url']:"";

$total_cnt   = 0;

for ($i = 2; $i < $totalRow; $i++)
{
    $start = get_time(); //한줄당 시간

    //변수 초기화

    $ba_no = $media = $state = $ba_no = $type = $program_title = $notice = $req_name = "";
    $s_time = $e_time = $sub_s_date = $sub_e_date = $req_date = $date_w = $hourly_wage = "";
    $second_cnt = $application_rate = $unit_price = $base_count = $billing_price = $total_price = 0;
    $detail_media = $delivery_gubun = $subs_gubun = "";
    $regdate = date("Y-m-d H:i:s");

    if($excel_type == '1')
    {
        $ba_no          = (string)trim(addslashes($worksheet->getCell("A{$i}")->getValue()));  //고유번호
        $media          = (string)trim(addslashes($worksheet->getCell("B{$i}")->getValue()));  //매체
        $detail_media   = (string)trim(addslashes($worksheet->getCell("C{$i}")->getValue()));  //세부매체
        $state_val      = (string)trim(addslashes($worksheet->getCell("F{$i}")->getValue()));  //진행상태
        $type           = (string)trim(addslashes($worksheet->getCell("G{$i}")->getValue()));  //유형
        $program_title  = (string)trim(addslashes($worksheet->getCell("H{$i}")->getValue()));  //프로그램명
        $s_time         = PHPExcel_Style_NumberFormat::toFormattedString((string)trim(addslashes($worksheet->getCell("I{$i}")->getValue())), PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME3);  //시작시간
        $e_time         = PHPExcel_Style_NumberFormat::toFormattedString((string)trim(addslashes($worksheet->getCell("J{$i}")->getValue())), PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME3);  //종료시간
        $second_cnt     = (string)trim(addslashes($worksheet->getCell("K{$i}")->getValue()));  //초수
        $date_w         = (string)trim(addslashes($worksheet->getCell("L{$i}")->getValue()));  //요일
        $hourly_wage    = (string)trim(addslashes($worksheet->getCell("M{$i}")->getValue()));  //시급
        $sub_s_date     = PHPExcel_Style_NumberFormat::toFormattedString((string)trim(addslashes($worksheet->getCell("N{$i}")->getValue())), PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);  //청약시작
        $sub_e_date     = PHPExcel_Style_NumberFormat::toFormattedString((string)trim(addslashes($worksheet->getCell("O{$i}")->getValue())), PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);  //청약종료
        $subs_gubun     = (string)trim(addslashes($worksheet->getCell("P{$i}")->getValue()));  //청약구분
        $application_rate  = (string)trim(addslashes($worksheet->getCell("Q{$i}")->getValue()));  //적용율
        $unit_price        = (string)trim(addslashes($worksheet->getCell("R{$i}")->getValue()));  //단가
        $base_count        = (string)trim(addslashes($worksheet->getCell("S{$i}")->getValue()));  //총횟수
        $total_price       = (string)trim(addslashes($worksheet->getCell("T{$i}")->getValue()));  //금액
        $billing_price     = (string)trim(addslashes($worksheet->getCell("U{$i}")->getValue()));  //청구금액
        $notice            = (string)trim(addslashes($worksheet->getCell("V{$i}")->getValue()));  //비고
        $delivery_gubun    = (string)trim(addslashes($worksheet->getCell("X{$i}")->getValue())); //운행구분
        $req_name          = (string)trim(addslashes($worksheet->getCell("Y{$i}")->getValue())); //신청자
        $req_date          = PHPExcel_Style_NumberFormat::toFormattedString((string)trim(addslashes($worksheet->getCell("Z{$i}")->getValue())), PHPExcel_Style_NumberFormat::FORMAT_DATE_YYYYMMDD2);  //신청일

        switch($state_val){
            case '신청': $state='1';break;
            case '확인중': $state='2';break;
            case '확정': $state='3';break;
            case '제외': $state='4';break;
        }

        if(empty($media) && empty($state)){
            break;
        }
    }

    if(!empty($date_w) && mb_strlen($date_w, 'UTF-8') >1)
    {
        if(empty($sub_s_date) && empty($sub_s_date))
        {
            echo '시작일과 종료일이 없습니다';
            exit;
        }

        $date_w_name = array('0' => '일', '1' => '월', '2' => '화','3' => '수','4' => '목','5' => '금','6' => '토');
        $date_w_list = [];

        for($j=0; $j<mb_strlen($date_w, 'UTF-8'); $j++)
        {
            $key        = "";
            $date_w_han = mb_substr($date_w, $j, 1,'UTF-8');

            switch($date_w_han)
            {
                case '월': $key = 1; break;
                case '화': $key = 2; break;
                case '수': $key = 3; break;
                case '목': $key = 4; break;
                case '금': $key = 5; break;
                case '토': $key = 6; break;
                case '일': $key = 0; break;
            }
            $date_w_list[$key]['cnt']  = 0;
            $date_w_list[$key]['bonus_cnt']  = 0;
            $date_w_list[$key]['name'] = $date_w_han;
        }

        if($date_w_list)
        {
            $s_date_val = strtotime($sub_s_date);
            $e_date_val = strtotime($sub_e_date);
            $sub_day    = (int)(($e_date_val-$s_date_val)/86400);

            for($k=0; $k<=$sub_day; $k++){
                $date_key = date('w', strtotime("{$sub_s_date} +{$k}day"));

                if(isset($date_w_list[$date_key])){
                    if($base_count > 0){
                        $date_w_list[$date_key]['cnt']++;
                    }
                }
            }

            foreach($date_w_list as $date_w_data)
            {
                $date_w_val         = $date_w_data['name'];
                $base_count_val     = $date_w_data['cnt'];
                $billing_price_val  = ($billing_price>0) ? ($unit_price*$base_count_val) : 0;
                $total_price_val    = ($total_price>0) ? ($unit_price*$base_count_val) : 0;

                if(!empty($ba_no))
                {
                    $update_sql = "UPDATE `broadcast_advertisement` SET media='{$media}', adv_no='{$adv_no}', advertiser='{$advertiser}', state='{$state}', `type`='{$type}', program_title='{$program_title}', s_time='{$s_time}', e_time='{$e_time}', `date_w`='{$date_w}', hourly_wage='{$hourly_wage}', sub_s_date='{$sub_s_date}', sub_e_date='{$sub_e_date}', second_cnt='{$second_cnt}', application_rate='{$application_rate}', unit_price='{$unit_price}', base_count='{$base_count_val}', billing_price='{$billing_price_val}', total_price='{$total_price_val}', notice='{$notice}', regdate='{$regdate}', req_name='{$req_name}', req_date='{$req_date}', detail_media='{$detail_media}', subs_gubun='{$subs_gubun}', delivery_gubun='{$delivery_gubun}' WHERE ba_no='{$ba_no}'";
                    mysqli_query($my_db, $update_sql);
                }
                else
                {
                    $sql .= $comma." ('{$media}', '{$adv_no}', '{$advertiser}', '{$state}', '{$type}', '{$program_title}', '{$s_time}', '{$e_time}', '{$date_w_val}', '{$hourly_wage}', '{$sub_s_date}', '{$sub_e_date}', '{$second_cnt}', '{$application_rate}', '{$unit_price}', '{$base_count_val}','{$total_price_val}','{$billing_price_val}','{$notice}', '{$regdate}', '{$req_name}', '{$req_date}', '{$detail_media}', '{$subs_gubun}', '{$delivery_gubun}')";
                    $comma = " , ";
                }
            }
        }else{
            echo "요일정보가 이상합니다";
            exit;
        }

    }else{

        if(!empty($ba_no))
        {
            $update_sql = "UPDATE `broadcast_advertisement` SET media='{$media}', adv_no='{$adv_no}', advertiser='{$advertiser}', state='{$state}', `type`='{$type}', program_title='{$program_title}', s_time='{$s_time}', e_time='{$e_time}', `date_w`='{$date_w}', hourly_wage='{$hourly_wage}', sub_s_date='{$sub_s_date}', sub_e_date='{$sub_e_date}', second_cnt='{$second_cnt}', application_rate='{$application_rate}', unit_price='{$unit_price}', base_count='{$base_count}', billing_price='{$billing_price}', total_price='{$total_price}', notice='{$notice}', regdate='{$regdate}', req_name='{$req_name}', req_date='{$req_date}', detail_media='{$detail_media}', subs_gubun='{$subs_gubun}', delivery_gubun='{$delivery_gubun}' WHERE ba_no='{$ba_no}'";
            mysqli_query($my_db, $update_sql);
        }
        else
        {
            $sql .= $comma." ('{$media}', '{$adv_no}', '{$advertiser}', '{$state}', '{$type}', '{$program_title}', '{$s_time}', '{$e_time}', '{$date_w}', '{$hourly_wage}', '{$sub_s_date}', '{$sub_e_date}', '{$second_cnt}', '{$application_rate}', '{$unit_price}', '{$base_count}', '{$total_price}','{$billing_price}', '{$notice}', '{$regdate}', '{$req_name}', '{$req_date}', '{$detail_media}', '{$subs_gubun}', '{$delivery_gubun}')";
            $comma = " , ";
        }
    }

    $end = get_time();
    $time = $end - $start;
    $total_time += $time;

    $total_cnt++;
}


echo "-----------------------------------------------------------------------------------------------<br/><br/>";
echo "총개수 {$total_cnt}, 엑셀 정리 시간 : ".round($total_time,2)."초 <br/>";

$start = get_time(); //한줄당 시간
if (!mysqli_query($my_db, $sql)){
    echo "방송광고리스트 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    echo "SQL : ".$sql."<br/>";
    exit;
}else{
    exit("<script>alert('방송광고리스트 반영 되었습니다.');location.href='broadcast_ad_list.php?{$search_url}';</script>");
}
$end = get_time();
$time = $end - $start;
echo "<br/>-----------------------------------------------------------------------------------------------<br/><br/>";
echo "DB 총 걸린시간 : ".round($time,2)."초";


?>
