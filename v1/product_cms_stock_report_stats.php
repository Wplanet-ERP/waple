<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/ProductCmsUnit.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "43";
$nav_title   = "입고/반출 현황";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색조건
$sch_sup_c_no   = isset($_GET['sch_sup_c_no']) ? $_GET['sch_sup_c_no'] : "";
$sch_ord_s_no   = isset($_GET['sch_ord_s_no']) ? $_GET['sch_ord_s_no'] : "";
$sch_option_name= isset($_GET['sch_option_name']) ? $_GET['sch_option_name'] : "";
$sch_kind		= isset($_GET['sch_kind']) ? $_GET['sch_kind'] : "1";
$today_s_w		= date('w') + 55;
$sch_s_m_date 	= isset($_GET['sch_s_m_date']) ? $_GET['sch_s_m_date'] : date("Y-m", strtotime("-5 month")); // 월간
$sch_e_m_date 	= isset($_GET['sch_e_m_date']) ? $_GET['sch_e_m_date'] : date("Y-m"); // 월간
$sch_s_w_date 	= isset($_GET['sch_s_w_date']) ? $_GET['sch_s_w_date'] : date("Y-m-d",strtotime("-{$today_s_w} day")); // 주간
$sch_e_w_date 	= isset($_GET['sch_e_w_date']) ? $_GET['sch_e_w_date'] : date("Y-m-d", strtotime("{$sch_s_w_date} +62 day")); // 주간
$sch_s_date 	= isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date("Y-m-d", strtotime("-10 day")); // 일간
$sch_e_date 	= isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date("Y-m-d"); // 일간

$smarty->assign("sch_kind", $sch_kind);
$smarty->assign("sch_s_m_date", $sch_s_m_date);
$smarty->assign("sch_e_m_date", $sch_e_m_date);
$smarty->assign("sch_s_w_date", $sch_s_w_date);
$smarty->assign("sch_e_w_date", $sch_e_w_date);
$smarty->assign("sch_s_date", $sch_s_date);
$smarty->assign("sch_e_date", $sch_e_date);

$prd_init        = [];
$prd_title_list  = [];
$prd_legend_list = [];

# 구성품 차트용 데이터
$unit_model         = ProductCmsUnit::Factory();
$chart_total_list   = $unit_model->getChartUnitData();
$sup_c_list         = $unit_model->getDistinctUnitCompanyData("sup_c_no");
$sup_c_init         = $chart_total_list['sup_c_init'];
$sup_ord_c_list     = $chart_total_list['sup_ord_c_list'];
$sch_ord_s_list     = $unit_model->getOrdStaffData();

$sch_sup_c_list = $sup_c_list;
$add_where = "1=1";

if(!empty($sch_sup_c_no))
{
    $add_where .= " AND pcsr.sup_c_no = '{$sch_sup_c_no}'";
    $smarty->assign("sch_sup_c_no", $sch_sup_c_no);
    $smarty->assign('sch_c_name', $sup_c_list[$sch_sup_c_no]);

    $prd_sql = "SELECT DISTINCT no, option_name FROM product_cms_unit WHERE sup_c_no = {$sch_sup_c_no} and display=1";
    $prd_query = mysqli_query($my_db, $prd_sql);
    while($prd_unit = mysqli_fetch_assoc($prd_query))
    {
        $prd_init[$prd_unit['no']]        = array('title' => $prd_unit['option_name'], 'imp_qty' => 0, 'exp_qty' => 0, 'deli_qty' => 0, 'total_qty' => 0);
        $prd_title_list[$prd_unit['no']]  = $prd_unit['option_name'];
    }
}

if (!empty($sch_ord_s_no)) {
    $add_where .= " AND pcsr.prd_unit IN(SELECT pcu.`no` FROM product_cms_unit pcu WHERE (pcu.ord_s_no='{$sch_ord_s_no}' OR pcu.ord_sub_s_no='{$sch_ord_s_no}' OR pcu.ord_third_s_no='{$sch_ord_s_no}') AND display='1')";
    $sch_sup_c_list = $sup_ord_c_list[$sch_ord_s_no];
    $smarty->assign("sch_ord_s_no", $sch_ord_s_no);
}

if (!empty($sch_option_name)) {
    $add_where .= " AND pcsr.option_name like '%{$sch_option_name}%'";
    if(!empty($prd_title_list))
    {
        $prd_title_list_val = [];
        foreach($prd_title_list as $key => $label){
            if(strpos($label, $sch_option_name) !== false){
                $prd_title_list_val[$key] = $label;
            }
        }

        if(!empty($prd_title_list_val)){
            $prd_title_list = $prd_title_list_val;
        }
    }

    $smarty->assign("sch_option_name", $sch_option_name);
}


// 조회 기간 : add_date_where, date_column
$all_date_where   = "";
$all_date_key	  = "";
$add_date_where   = "";
$add_date_column  = "";
$total_date_where = "";
$stats_list       = [];
if($sch_kind == '1') //월간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '$sch_s_m_date' AND '$sch_e_m_date'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where  = " AND DATE_FORMAT(regdate, '%Y-%m') BETWEEN '$sch_s_m_date' AND '$sch_e_m_date' ";
    $add_date_column = "DATE_FORMAT(regdate, '%Y%m')";

    $total_date_where = "DATE_FORMAT(regdate, '%Y-%m') < '{$sch_s_m_date}'";
}
elseif($sch_kind == '2') //주간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '$sch_s_w_date' AND '$sch_e_w_date'";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where  = " AND DATE_FORMAT(regdate, '%Y-%m-%d') BETWEEN '$sch_s_w_date' AND '$sch_e_w_date' ";
    $add_date_column = "DATE_FORMAT(DATE_SUB(regdate, INTERVAL(IF(DAYOFWEEK(regdate)=1,8,DAYOFWEEK(regdate))-2) DAY), '%Y%m%d')";

    $total_date_where = "DATE_FORMAT(regdate, '%Y-%m-%d') < '{$sch_s_w_date}'";
}
else //일간
{
    $all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '$sch_s_date' AND '$sch_e_date'";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_date_where  = " AND DATE_FORMAT(regdate, '%Y-%m-%d') BETWEEN '$sch_s_date' AND '$sch_e_date' ";
    $add_date_column = "DATE_FORMAT(regdate, '%Y%m%d')";

    $total_date_where = "DATE_FORMAT(regdate, '%Y-%m-%d') < '{$sch_s_date}'";
}

$total_group_by = empty($sch_sup_c_no) ?  "pcsr.sup_c_no" : "pcsr.prd_unit";
$stats_total_sql   = "SELECT {$total_group_by} as total_key, SUM(pcsr.stock_qty) as total FROM product_cms_stock_report as pcsr WHERE {$total_date_where} GROUP BY {$total_group_by}";
$stats_total_query = mysqli_query($my_db, $stats_total_sql);
$stats_total_list  = [];

while($stats_total = mysqli_fetch_assoc($stats_total_query))
{
    $stats_total_list[$stats_total['total_key']] = $stats_total['total'];
}


$all_date_sql = "
	SELECT
 		$all_date_title as chart_title,
 		$all_date_key as chart_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	GROUP BY chart_key
	ORDER BY chart_key
";

$x_label_list = [];
$x_table_th_list = [];
if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $stats_list[$date['chart_key']]  = ($sch_sup_c_no == "") ? $sup_c_init : $prd_init;
        $chart_title = $date['chart_title'];

        if($sch_kind == '3'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        $x_label_list[$date['chart_key']]    = "'".$chart_title."'";
        $x_table_th_list[$date['chart_key']] = $chart_title;
    }
}

$stats_sql = "
    SELECT 
        {$add_date_column} as regdate,
        pcsr.sku,
        pcsr.sup_c_no,
        pcsr.prd_unit,
        (SELECT sub_c.c_name FROM company sub_c WHERE sub_c.c_no = pcsr.sup_c_no) as sup_c_name,
        (SELECT pcu.display FROM product_cms_unit pcu WHERE pcu.`no`=pcsr.prd_unit) as pcu_display,
        pcsr.option_name,
        pcsr.`type`,
        pcsr.state,
        pcsr.stock_qty
    FROM product_cms_stock_report as pcsr
    WHERE {$add_where} 
    {$add_date_where}
    ORDER BY regdate ASC
";
$stats_query = mysqli_query($my_db, $stats_sql);

$imp_qty_list = $exp_qty_list = $total_qty_list = $deli_qty_list = [];
while($stats = mysqli_fetch_assoc($stats_query))
{
    if($stats['pcu_display'] == '1')
    {
        $key      = empty($sch_sup_c_no) ? $stats['sup_c_no'] : $stats['prd_unit'];

        $imp_qty  = ($stats['type'] == '1') ? $stats['stock_qty'] : 0;
        $exp_qty  = ($stats['type'] == '2') ? $stats['stock_qty'] : 0;
        $deli_qty = ($stats['type'] == '3') ? $stats['stock_qty'] : 0;

        $stats_list[$stats['regdate']][$key]['imp_qty']   += $imp_qty;
        $stats_list[$stats['regdate']][$key]['exp_qty']   += $exp_qty;
        $stats_list[$stats['regdate']][$key]['deli_qty']  += $deli_qty;
        $stats_list[$stats['regdate']][$key]['total_qty'] += $stats['stock_qty'];

        $imp_qty_list[$key]   = 0;
        $deli_qty_list[$key]  = 0;
        $exp_qty_list[$key]   = 0;
        $total_qty_list[$key] = 0;
    }
}

$full_data = [];
$title_list = empty($sch_sup_c_no) ? $sch_sup_c_list : $prd_title_list;

$imp_acc_qty_sum = 0;
$exp_acc_qty_sum = 0;
$deli_acc_qty_sum = 0;
$total_acc_qty_sum = 0;
$stats_total_sum = array_sum($stats_total_list);

if(!empty($stats_list))
{
    foreach($stats_list as $regdate => $stats_reg_data)
    {
        if($stats_reg_data)
        {
            $imp_qty_sum    = 0;
            $deli_qty_sum   = 0;
            $exp_qty_sum    = 0;
            $total_qty_sum  = 0;
            $stats_stock    = 0;

            foreach ($stats_reg_data as $key => $stats_data) {
                $stats_stock = isset($stats_total_list[$key]) ? $stats_total_list[$key] : 0;

                $full_data["each"]["daily"]["import"][$key]['title']    = $title_list[$key];
                $full_data["each"]["daily"]["import"][$key]['data'][]   = $stats_data['imp_qty'];
                $full_data["each"]["daily"]["delivery"][$key]['title']  = $title_list[$key];
                $full_data["each"]["daily"]["delivery"][$key]['data'][] = $stats_data['deli_qty'];
                $full_data["each"]["daily"]["export"][$key]['title']    = $title_list[$key];
                $full_data["each"]["daily"]["export"][$key]['data'][]   = $stats_data['exp_qty'];
                $full_data["each"]["daily"]["stock"][$key]['title']     = $title_list[$key];
                $full_data["each"]["daily"]["stock"][$key]['data'][]   = $stats_stock + $stats_data['total_qty'];

                $imp_qty_list[$key]   += $stats_data['imp_qty'];
                $exp_qty_list[$key]   += $stats_data['exp_qty'];
                $deli_qty_list[$key]  += $stats_data['deli_qty'];
                $total_qty_list[$key] += $stats_data['total_qty'];

                $full_data["each"]["acc"]["import"][$key]['title']    = $title_list[$key];
                $full_data["each"]["acc"]["import"][$key]['data'][]   = $imp_qty_list[$key];
                $full_data["each"]["acc"]["export"][$key]['title']    = $title_list[$key];
                $full_data["each"]["acc"]["export"][$key]['data'][]   = $exp_qty_list[$key];
                $full_data["each"]["acc"]["delivery"][$key]['title']  = $title_list[$key];
                $full_data["each"]["acc"]["delivery"][$key]['data'][] = $deli_qty_list[$key];
                $full_data["each"]["acc"]["stock"][$key]['title']     = $title_list[$key];
                $full_data["each"]["acc"]["stock"][$key]['data'][]    = $stats_stock + $total_qty_list[$key];

                $imp_qty_sum   += $stats_data['imp_qty'];
                $exp_qty_sum   += $stats_data['exp_qty'];
                $deli_qty_sum  += $stats_data['deli_qty'];
                $total_qty_sum += $stats_data['total_qty'];
            }

            $imp_acc_qty_sum    += $imp_qty_sum;
            $exp_acc_qty_sum    += $exp_qty_sum;
            $deli_acc_qty_sum   += $deli_qty_sum;
            $total_acc_qty_sum  += $total_qty_sum;

            $full_data["sum"]["daily"]['import']['sum']['title']    = "합산";
            $full_data["sum"]["daily"]['import']['sum']['data'][]   = $imp_qty_sum;
            $full_data["sum"]["daily"]['export']['sum']['title']    = "합산";
            $full_data["sum"]["daily"]['export']['sum']['data'][]   = $exp_qty_sum;
            $full_data["sum"]["daily"]['delivery']['sum']['title']  = "합산";
            $full_data["sum"]["daily"]['delivery']['sum']['data'][] = $deli_qty_sum;
            $full_data["sum"]["daily"]['stock']['sum']['title']     = "합산";
            $full_data["sum"]["daily"]['stock']['sum']['data'][]    = $stats_total_sum + $total_qty_sum;

            $full_data["sum"]["acc"]['import']['sum']['title']   = "합산";
            $full_data["sum"]["acc"]['import']['sum']['data'][]   = $imp_acc_qty_sum;
            $full_data["sum"]["acc"]['export']['sum']['title']    = "합산";
            $full_data["sum"]["acc"]['export']['sum']['data'][]   = $exp_acc_qty_sum;
            $full_data["sum"]["acc"]['delivery']['sum']['title']  = "합산";
            $full_data["sum"]["acc"]['delivery']['sum']['data'][] = $deli_acc_qty_sum;
            $full_data["sum"]["acc"]['stock']['sum']['title']     = "합산";
            $full_data["sum"]["acc"]['stock']['sum']['data'][]    = $stats_total_sum + $total_acc_qty_sum;
        }else{
            $full_data = [];
        }
    }
}

$legend_list = empty($full_data) ? [] : (empty($sch_sup_c_no) ? $sch_sup_c_list : $prd_title_list);

$smarty->assign('x_label_list', implode(',', $x_label_list));
$smarty->assign('x_table_th_list', json_encode($x_table_th_list));
$smarty->assign('legend_list', json_encode($legend_list));
$smarty->assign('full_data', json_encode($full_data));
$smarty->assign('picker_list', $legend_list);
$smarty->assign('sch_ord_s_list', $sch_ord_s_list);
$smarty->assign('sch_sup_c_list', $sch_sup_c_list);

$smarty->display('product_cms_stock_report_stats.html');
?>
