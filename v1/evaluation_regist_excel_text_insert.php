<?php
ini_set('max_execution_time', '300');
ini_set('memory_limit', '2G');
error_reporting(E_ALL ^ E_NOTICE);

include_once('Classes/PHPExcel.php');
include_once('inc/common.php');
require('ckadmin.php');
include_once('inc/model/Evaluation.php');


$ev_no      = isset($_POST['ev_no']) ? $_POST['ev_no'] : "";
$file_name  = $_FILES["text_file"]["tmp_name"];

$excelReader = PHPExcel_IOFactory::createReaderForFile($file_name);
$excelReader->setReadDataOnly(true);
$excel = $excelReader->load($file_name);
$excel->setActiveSheetIndex(0);
$objWorksheet   = $excel->getActiveSheet();
$totalRow       = $objWorksheet->getHighestRow();
$ev_model       = Evaluation::Factory();
$ev_model->setMainInit("evaluation_system_result", "ev_result_no");
$upd_data       = [];
$chk_list = [];

for ($i = 2; $i <= $totalRow; $i++)
{
    //변수 초기화
    $ev_result_no   = (string)trim(addslashes($objWorksheet->getCell("A{$i}")->getValue()));
    $text_value     = (string)trim(addslashes($objWorksheet->getCell("F{$i}")->getValue()));

    if (empty($ev_result_no)) {
        break;
    }

    if(!empty($text_value)){
        $upd_data[] = array(
            "ev_result_no"      => $ev_result_no,
            "evaluation_value"  => $text_value
        );
    }
}

if(empty($upd_data))
{
    echo "반영할 내역이 없습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    exit;
}

if (!$ev_model->multiUpdate($upd_data)){
    echo "평가 의견 보정 적용에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    exit;
}else{
    exit("<script>alert('평가 의견 보정이 적용 되었습니다.');location.href='evaluation_regist.php?ev_no={$ev_no}';</script>");
}


?>
