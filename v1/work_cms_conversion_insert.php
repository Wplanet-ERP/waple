<?php
ini_set('max_execution_time', '7200');
ini_set('memory_limit', '5G');

require('Classes/PHPExcel.php');
require('inc/common.php');
require('ckadmin.php');
require('inc/model/Custom.php');

# 파일 변수
$conversion_model   = Custom::Factory();
$conversion_model->setMainInit("commerce_conversion", "cc_no");
$conversion_file    = $_FILES["conversion_file"]['tmp_name'];
$dp_c_no            = isset($_POST['conversion_site']) ? $_POST['conversion_site'] : 0;
$search_url         = isset($_POST['search_url']) ? $_POST['$search_url'] : "";
$ins_data_list      = [];

$csv_file_path  = fopen($conversion_file, "r");
$file_idx       = 0;
$regdate        = date("Y-m-d H:i:s");

while ($csv_file_data = fgetcsv($csv_file_path, 2048, ","))
{
    $file_idx++;

    if($file_idx < 9){
        continue;
    }

    $ga_datetime            = $csv_file_data[0];
    $ga_connect_type        = $csv_file_data[1];
    $ga_url_val             = $csv_file_data[2];
    $ga_traffic_count       = $csv_file_data[3];
    $ga_conversion_count    = $csv_file_data[4];
    $ga_conversion_price    = round($csv_file_data[5]);

    if(empty($ga_datetime)){
        break;
    }

    if(empty($ga_url_val)){
        continue;
    }
    $ga_url_tmp             = "";
    $ga_datetime_conv       = substr($ga_datetime,0,4)."-".substr($ga_datetime,4,2)."-".substr($ga_datetime,6,2)." ".substr($ga_datetime,8,2).":00:00";
    if($dp_c_no == '5800'){
        $ga_url_tmp             = str_replace("https://dr-piel.com/", "", $ga_url_val);
        $ga_url_tmp             = str_replace("http://dr-piel.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("https://www.dr-piel.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("http://www.dr-piel.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("https://drbodylab.imweb.me/", "", $ga_url_tmp);
    }
    elseif($dp_c_no == '5958'){
        $ga_url_tmp             = str_replace("https://ilenol.com/", "", $ga_url_val);
        $ga_url_tmp             = str_replace("http://ilenol.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("https://www.ilenol.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("http://www.ilenol.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("https://i-lenol.imweb.me/", "", $ga_url_tmp);
    }
    elseif($dp_c_no == '6012'){
        $ga_url_tmp             = str_replace("https://nuzam.com/", "", $ga_url_val);
        $ga_url_tmp             = str_replace("http://nuzam.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("https://www.nuzam.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("http://www.nuzam.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("https://nuzam.imweb.me/", "", $ga_url_tmp);
    }
    else{
        $ga_url_tmp             = str_replace("https://belabef.com/", "", $ga_url_val);
        $ga_url_tmp             = str_replace("http://belabef.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("https://www.belabef.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("http://www.belabef.com/", "", $ga_url_tmp);
        $ga_url_tmp             = str_replace("https://drbodylab.imweb.me/", "", $ga_url_tmp);
    }

    $ga_url_chk_val         = "";
    $brand                  = 0;
    $url_idx                = "";

    if($ga_url_val == "/"){
        $ga_url_chk_val = "/";
    }
    elseif(strpos($ga_url_tmp,"one_event") !== false){
        $ga_url_chk_val = "one_event";
    }
    elseif(strpos($ga_url_tmp,"event_drpiel") !== false){
        $ga_url_chk_val = "event_drpiel";
    }
    elseif(strpos($ga_url_tmp,"filter_event") !== false){
        $ga_url_chk_val = "filter_event";
    }
    elseif(strpos($ga_url_tmp,"birthday_event") !== false){
        $ga_url_chk_val = "birthday_event";
    }
    elseif(strpos($ga_url_tmp,"drpiel_total") !== false){
        $ga_url_chk_val = "drpiel_total";
    }
    else{
        if(strpos($ga_url_tmp,"?idx=") !== false)
        {
            $ga_url_tmp_list = explode("?", $ga_url_tmp);
            if(strpos($ga_url_tmp_list[0],"shop_payment_complete") === false){
                if(isset($ga_url_tmp_list[1])){
                    $ga_url_tmp_idx_list = explode("&", $ga_url_tmp_list[1]);
                    $ga_url_chk_tmp_val  = str_replace("idx=", "", $ga_url_tmp_idx_list[0]);
                    $ga_url_chk_val      = (is_numeric($ga_url_chk_tmp_val)) ? str_replace(".","",$ga_url_tmp_idx_list[0]) : "";
                }
            }
        }else{
            $ga_url_tmp_list = explode("/", $ga_url_tmp);
            $ga_url_chk_val  = (is_numeric($ga_url_tmp_list[0])) ? $ga_url_tmp_list[0] : "";

            if(empty($ga_url_chk_val)){
                $ga_url_tmp_list = explode("?", $ga_url_tmp);
                $ga_url_chk_val  = (is_numeric($ga_url_tmp_list[0])) ? $ga_url_tmp_list[0] : "";
            }
        }
    }

    if(!empty($ga_url_chk_val)){
        $chk_url_sql    = "SELECT * FROM commerce_url WHERE dp_c_no='{$dp_c_no}' AND url='{$ga_url_chk_val}' LIMIT 1";
        $chk_url_query  = mysqli_query($my_db, $chk_url_sql);
        $chk_url_result = mysqli_fetch_assoc($chk_url_query);
        $url_idx        = $ga_url_chk_val;

        if(!empty($chk_url_result)){
            $brand      = $chk_url_result['brand'];
        }
    }

    $ins_data = array(
        "convert_date"      => $ga_datetime_conv,
        "brand"             => $brand,
        "dp_c_no"           => $dp_c_no,
        "url_idx"           => $url_idx,
        "url"               => addslashes(trim($ga_url_val)),
        "connect_type"      => $ga_connect_type,
        "traffic_count"     => $ga_traffic_count,
        "conversion_count"  => $ga_conversion_count,
        "conversion_price"  => $ga_conversion_price,
        "reg_s_no"          => $session_s_no,
        "regdate"           => $regdate,
    );

//    $conversion_model->insert($ins_data);
    $ins_data_list[] = $ins_data;
}

//exit("<script>alert('데이터가 반영 되었습니다.');location.href='work_cms_stats_traffic_conversion.php?{$search_url}';</script>");

if (!$conversion_model->multiInsert($ins_data_list)){
    echo "데이터 반영에 실패했습니다.<br>담당자에게 바로 문의 해주세요.<br>담당자 : 김윤영<br/>";
    exit;
}else{
    exit("<script>alert('데이터가 반영 되었습니다.');location.href='work_cms_stats_traffic_conversion.php?{$search_url}';</script>");
}

?>
