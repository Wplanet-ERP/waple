<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_cms.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/Kind.php');
require('inc/model/ProductCms.php');
require('inc/model/WorkCms.php');

$is_freelancer  = false;
if($session_staff_state == '2'){ // 프리랜서의 경우 기본 접근제한으로 설정
    $is_freelancer = true;
}

$is_cms_staff   = false;
if($session_team == "00244" || permissionNameCheck($session_permission, "마스터관리자")){
    $is_cms_staff = true;
}
$smarty->assign("is_cms_staff", $is_cms_staff);

# Model Init
$company_model      = Company::Factory();
$cms_model          = WorkCms::Factory();
$reservation_model  = WorkCms::Factory();
$reservation_model->setMainInit("work_cms_reservation", "order_number");
$with_log_c_no      = '2809';

# Process Start
$process = (isset($_POST['process']))?$_POST['process'] : "";

if($process == "f_delivery_state")
{
    $ord_no     = (isset($_POST['ord_no'])) ? $_POST['ord_no'] : "";
    $value 		= (isset($_POST['val'])) ? $_POST['val'] : "";

    if (!$reservation_model->update(array("order_number" => $ord_no, "delivery_state" => $value)))
        echo "발송 진행상태 변경에 실패 했습니다.";
    else
        echo "발송 진행상태 변경에 성공 했습니다.";
    exit;
}
elseif($process == "move_delivery")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $option_no  = isset($_POST['add_option']) ? $_POST['add_option'] : "";
    $ord_s_date = isset($_POST['add_ord_s_date']) ? $_POST['add_ord_s_date'] : "";
    $ord_e_date = isset($_POST['add_ord_e_date']) ? $_POST['add_ord_e_date'] : "";
    $out_date   = isset($_POST['add_out_date']) ? $_POST['add_out_date'] : "";

    if(empty($option_no)){
        exit("<script>alert('선택된 구성품이 없습니다. 택배리스트 추가에 실패했습니다');location.href='work_cms_reservation_list.php?{$search_url}';</script>");
    }

    $option_no_where = "";
    $add_out_where   = "1=1";
    $chk_prd_list    = [];
    if($option_no == "238"){
        $option_no_where = "pcr.`option_no` IN (238,239)";
    }elseif($option_no == "240"){
        $option_no_where = "pcr.`option_no` IN (240,241)";
    }elseif($option_no == "475"){
        $option_no_where = "pcr.`option_no` IN (473,475)";
    }elseif($option_no == "275"){
        $option_no_where = "pcr.`option_no` IN (275,276,277)";
    }elseif($option_no == "100000"){
        $chk_prd_list = array(1732,1733,1735,1736,1737,1738,1740,1742,1745,1734,1739,1741,1743,1744);
    }elseif($option_no == "100001"){
        $chk_prd_list = array(1733,1735,1738,1742,1745);
    }elseif($option_no == "100002"){
        $chk_prd_list = array(1732,1736,1737,1734,1739,1740,1741,1743,1744);
    }else{
        $option_no_where = "pcr.`option_no` IN ({$option_no})";
    }

    if(!empty($chk_prd_list)){
        $chk_prd_text   = implode(",", $chk_prd_list);
        $add_where      = "w.delivery_state IN(1,2) AND w.prd_no IN({$chk_prd_text})";
    }else{
        $add_where      = "w.delivery_state IN(1,2) AND w.prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE {$option_no_where} AND pcr.display='1')";
    }

    if(!empty($ord_s_date)){
        $ord_s_datetime  = "{$ord_s_date} 00:00:00";
        $add_where      .= " AND w.order_date >= '{$ord_s_date}'";
    }

    if(!empty($ord_e_date)){
        $ord_s_datetime  = "{$ord_e_date} 23:59:59";
        $add_where      .= " AND w.order_date >= '{$ord_e_date}'";
    }

    if(!empty($out_date)){
        $add_where      .= " AND w.out_date = '{$out_date}'";
        $add_out_where  .= " AND w.out_date = '{$out_date}'";
    }

    $move_order_list        = [];
    $insert_order_list      = [];
    $chk_reservation_sql    = "SELECT * FROM work_cms_reservation as w WHERE {$add_where} GROUP BY order_number";
    $chk_reservation_query  = mysqli_query($my_db, $chk_reservation_sql);
    while($chk_reservation = mysqli_fetch_assoc($chk_reservation_query))
    {
        $move_order_list[$chk_reservation['order_number']] = "'{$chk_reservation['order_number']}'";
    }

    $mod_reservation_list = [];
    $move_stock_date      = date("Y-m-d");
    $move_reg_date        = date("Y-m-d H:i:s");
    if(!empty($move_order_list))
    {
        $move_order_text        = implode(",", $move_order_list);
        $move_reservation_sql   = "SELECT * FROM work_cms_reservation as w WHERE w.order_number IN({$move_order_text}) AND w.delivery_state IN(1,2) AND {$add_out_where} ORDER BY order_date ASC, order_number ASC, unit_price DESC, prd_no ASC";
        $move_reservation_query = mysqli_query($my_db, $move_reservation_sql);
        while($move_reservation = mysqli_fetch_assoc($move_reservation_query))
        {
            $order_init_data = $move_reservation;
            $order_init_data['delivery_state']  = "1";
            $order_init_data['stock_date']      = $move_stock_date;
            $order_init_data['regdate']         = $move_reg_date;
            $order_init_data['notice']          = addslashes("예판상품 택배리스트로 이동. 이동날짜 : {$move_stock_date}");
            $order_init_data['recipient']       = addslashes($move_reservation['recipient']);
            $order_init_data['recipient_addr']  = addslashes($move_reservation['recipient_addr']);
            $order_init_data['delivery_memo']   = addslashes($move_reservation['delivery_memo']);

            unset($order_init_data["w_no"]);
            unset($order_init_data["move_date"]);
            unset($order_init_data["out_date"]);

            $mod_reservation_list[] = $move_reservation["w_no"];
            $insert_order_list[]    = $order_init_data;
        }
    }

    if(!empty($insert_order_list) && $cms_model->multiInsert($insert_order_list))
    {
        $mod_reservation_text   = implode(",", $mod_reservation_list);
        $upd_reservation_sql    = "UPDATE work_cms_reservation SET delivery_state = '4', move_date='{$move_reg_date}' WHERE w_no IN({$mod_reservation_text})";
        mysqli_query($my_db, $upd_reservation_sql);
        exit("<script>alert('택배리스트에 추가했습니다');location.href='work_list_cms.php';</script>");
    }else{
        exit("<script>alert('택배리스트 추가에 실패했습니다');location.href='work_cms_reservation_list.php?{$search_url}';</script>");
    }
}
elseif($process == "del_reservation")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $ord_no     = isset($_POST['chk_order_number']) ? $_POST['chk_order_number'] : "";

    if(!$reservation_model->delete($ord_no)){
        exit("<script>alert('삭제 처리에 실패했습니다');location.href='work_cms_reservation_list.php?{$search_url}';</script>");
    }else{
        exit("<script>alert('삭제 처리했습니다');location.href='work_cms_reservation_list.php?{$search_url}';</script>");
    }
}
elseif($process == "move_one_delivery")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $ord_no     = isset($_POST['chk_order_number']) ? $_POST['chk_order_number'] : "";

    if(empty($ord_no)){
        exit("<script>alert('선택된 주문이 없습니다. 택배리스트 추가에 실패했습니다');location.href='work_cms_reservation_list.php?{$search_url}';</script>");
    }

    $move_stock_date        = date("Y-m-d");
    $move_reg_date          = date("Y-m-d H:i:s");
    $mod_reservation_list   = [];
    $insert_order_list      = [];

    $move_reservation_sql   = "SELECT * FROM work_cms_reservation as w WHERE w.order_number = '{$ord_no}' AND w.delivery_state IN(1,2) ORDER BY order_date ASC, order_number ASC, unit_price DESC, prd_no ASC";
    $move_reservation_query = mysqli_query($my_db, $move_reservation_sql);
    while($move_reservation = mysqli_fetch_assoc($move_reservation_query))
    {
        $order_init_data = $move_reservation;
        $order_init_data['delivery_state']  = "1";
        $order_init_data['stock_date']      = $move_stock_date;
        $order_init_data['regdate']         = $move_reg_date;
        $order_init_data['notice']          = addslashes("예판상품 택배리스트로 이동. 이동날짜 : {$move_stock_date}");
        $order_init_data['recipient']       = addslashes($move_reservation['recipient']);
        $order_init_data['recipient_addr']  = addslashes($move_reservation['recipient_addr']);
        $order_init_data['delivery_memo']   = addslashes($move_reservation['delivery_memo']);

        unset($order_init_data["w_no"]);
        unset($order_init_data["move_date"]);
        unset($order_init_data["out_date"]);

        $mod_reservation_list[] = $move_reservation["w_no"];
        $insert_order_list[]    = $order_init_data;
    }

    if(!empty($insert_order_list) && $cms_model->multiInsert($insert_order_list))
    {
        $mod_reservation_text   = implode(",", $mod_reservation_list);
        $upd_reservation_sql    = "UPDATE work_cms_reservation SET delivery_state = '4', move_date='{$move_reg_date}' WHERE w_no IN({$mod_reservation_text})";
        mysqli_query($my_db, $upd_reservation_sql);
        exit("<script>alert('택배리스트에 추가했습니다');location.href='work_list_cms.php';</script>");
    }else{
        exit("<script>alert('택배리스트 추가에 실패했습니다');location.href='work_cms_reservation_list.php?{$search_url}';</script>");
    }
}
elseif($process == "combined_order")
{
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $option_no  = isset($_POST['add_option']) ? $_POST['add_option'] : "";
    $ord_s_date = isset($_POST['add_ord_s_date']) ? $_POST['add_ord_s_date'] : "";
    $ord_e_date = isset($_POST['add_ord_e_date']) ? $_POST['add_ord_e_date'] : "";

    if(empty($option_no)){
        exit("<script>alert('선택된 구성품이 없습니다. 택배리스트 추가에 실패했습니다'); location.href='work_cms_reservation_list.php?{$search_url}';</script>");
    }

    $add_where = "w.delivery_state IN(1,2) AND w.prd_no IN(SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE pcr.`option_no` ='{$option_no}' AND pcr.display='1')";
    if(!empty($ord_s_date)){
        $ord_s_datetime  = $ord_s_date." 00:00:00";
        $add_where      .= " AND w.order_date >= '{$ord_s_date}'";
    }

    if(!empty($ord_e_date)){
        $ord_s_datetime  = $ord_e_date." 23:59:59";
        $add_where      .= " AND w.order_date >= '{$ord_e_date}'";
    }

    $move_order_list        = [];
    $insert_order_list      = [];
    $chk_reservation_sql    = "SELECT * FROM work_cms_reservation as w WHERE {$add_where} GROUP BY order_number";
    $chk_reservation_query  = mysqli_query($my_db, $chk_reservation_sql);
    while($chk_reservation = mysqli_fetch_assoc($chk_reservation_query))
    {
        $move_order_list[$chk_reservation['order_number']] = "'{$chk_reservation['order_number']}'";
    }

    # 합배송 상품정보
    $combined_prd_sql        = "SELECT * FROM product_cms WHERE combined_pack='2' AND combined_except != '' AND combined_product != '' AND display='1'";
    $combined_prd_query      = mysqli_query($my_db, $combined_prd_sql);
    $combined_except_list    = [];
    $combined_product_list   = [];
    $combined_prd_where_list = [];
    while($combined_prd = mysqli_fetch_assoc($combined_prd_query))
    {
        $combined_product_list[$combined_prd['prd_no']] = array(
            'except'    => explode(',', $combined_prd['combined_except']),
            'combined'  => $combined_prd['combined_product']
        );

        $combined_prd_where_list[$combined_prd['prd_no']] = $combined_prd['prd_no'];
    }

    # 합배송 처리
    if(!empty($combined_prd_where_list))
    {
        # 1 : 합배송 관련 상품 주문번호 추출
        $move_order_text    = implode(",", $move_order_list);
        $combined_prd_where = implode(',', $combined_prd_where_list);
        $combined_chk_sql   = "SELECT * FROM work_cms_reservation as main WHERE main.order_number IN(SELECT DISTINCT order_number FROM work_cms_reservation wct WHERE wct.order_number IN({$move_order_text}) AND wct.prd_no IN({$combined_prd_where})) AND main.order_number NOT IN(SELECT DISTINCT order_number FROM work_cms_reservation wct WHERE wct.order_number IN({$move_order_text}) AND wct.prd_no IN(SELECT combined_product FROM product_cms WHERE combined_pack='2' AND combined_product !='' AND display='1')) ORDER BY main.w_no ASC";
        $combined_chk_query = mysqli_query($my_db, $combined_chk_sql);
        $combined_ord_list  = [];
        $combined_chg_list  = [];
        while($combined_chk = mysqli_fetch_assoc($combined_chk_query))
        {
            $combined_ord_list[$combined_chk['order_number']][] = array(
                'w_no'                  => $combined_chk['w_no'],
                'prd_no'                => $combined_chk['prd_no'],
                'qty'                   => $combined_chk['quantity'],
                'price'                 => $combined_chk['dp_price'],
                'price_vat'             => $combined_chk['dp_price_vat'],
                'unit_delivery_price'   => $combined_chk['unit_delivery_price'],
                'coupon_price'          => $combined_chk['coupon_price'],
            );
        }

        # 2 : 합배송 부자재 여부 확인
        if(!empty($combined_ord_list))
        {
            foreach($combined_ord_list as $ord_no => $combined_products)
            {
                $combined_products = array_reverse($combined_products);
                foreach($combined_products as $combined_product)
                {
                    $comb_except  = isset($combined_product_list[$combined_product['prd_no']]) ? $combined_product_list[$combined_product['prd_no']]['except'] : "";
                    $comb_pro_no  = isset($combined_product_list[$combined_product['prd_no']]) ? $combined_product_list[$combined_product['prd_no']]['combined'] : "";

                    if((!empty($comb_except) && !empty($comb_pro_no)) && combinedExceptCheck($combined_products, $comb_except))
                    {
                        $combined_chg_list[$ord_no] = array('w_no' => $combined_product['w_no'], 'prd_no' => $combined_product['prd_no'], "comb_prd_no" => $comb_pro_no, 'org_prd' => $combined_product);
                        break;
                    }
                }
            }
        }

        # 3 : 합배송 해당 상품 교체
        if(!empty($combined_chg_list))
        {
            foreach($combined_chg_list as $combined_ord_no => $combined_chg_data)
            {
                $combined_org_prd = $combined_chg_data['org_prd'];
                if($combined_org_prd['qty'] > 1)
                {
                    $combine_price_val          = $combined_org_prd['price']/$combined_org_prd['qty'];
                    $combine_price_vat_val      = $combined_org_prd['price_vat']/$combined_org_prd['qty'];
                    $combine_unit_delivery_val  = ($combined_org_prd['unit_delivery_price'] > 0) ? $combined_org_prd['unit_delivery_price']/$combined_org_prd['qty'] : 0;
                    $combine_coupon_val         = ($combined_org_prd['coupon_price'] > 0) ? $combined_org_prd['coupon_price']/$combined_org_prd['qty'] : 0;

                    $combine_price          = (int)$combine_price_val;
                    $combine_price_vat      = (int)$combine_price_vat_val;
                    $combine_unit_delivery  = (int)$combine_unit_delivery_val;
                    $combine_coupon_price   = (int)$combine_coupon_val;

                    $org_upd_qty            = $combined_org_prd['qty'] - 1;
                    $org_upd_price          = $combined_org_prd['price']-$combine_price;
                    $org_upd_price_vat      = $combined_org_prd['price_vat']-$combine_price_vat;
                    $ord_upd_unit_delivery  = $combined_org_prd['unit_delivery_price'] - $combine_unit_delivery_val;
                    $ord_upd_coupon_price   = $combined_org_prd['coupon_price'] - $combine_coupon_price;
                    $combined_shop_ord_no   = $combined_ord_no."_".$combined_chg_data['comb_prd_no'];

                    $ins_sql = "
                        INSERT INTO work_cms_reservation(c_no, c_name, log_c_no, s_no, `team`, prd_no, stock_date, quantity, order_type, order_number, parent_order_number, order_date, payment_date, recipient, recipient_addr, recipient_hp, recipient_hp2, zip_code, dp_price, dp_price_vat, dp_c_name, dp_c_no, manager_memo, regdate, task_req, task_req_s_no, task_req_team, task_run_regdate, task_run_s_no, task_run_team, notice, delivery_memo, payment_type, `account`, coupon_price, final_price, unit_price, unit_delivery_price)
                        (SELECT c_no, c_name, log_c_no, s_no, `team`, '{$combined_chg_data['comb_prd_no']}', stock_date, '1', order_type, order_number, parent_order_number, order_date, payment_date, recipient, recipient_addr, recipient_hp, recipient_hp2, zip_code, '{$combine_price}', '{$combine_price_vat}', dp_c_name, dp_c_no, '예판 합포장수정', regdate, task_req, task_req_s_no, task_req_team, task_run_regdate, task_run_s_no, task_run_team, notice, delivery_memo, payment_type, `account`, '{$combine_coupon_price}', final_price, '{$combine_price_vat}', '{$ord_upd_unit_delivery}'
                        FROM work_cms_reservation WHERE w_no='{$combined_chg_data['w_no']}')";
                    mysqli_query($my_db, $ins_sql);

                    $upd_sql = "UPDATE work_cms_reservation SET quantity='{$org_upd_qty}', dp_price='{$org_upd_price}', dp_price_vat='{$org_upd_price_vat}', unit_price='{$org_upd_price_vat}', unit_delivery_price='{$ord_upd_unit_delivery}', coupon_price='{$ord_upd_coupon_price}' WHERE w_no='{$combined_chg_data['w_no']}'";
                    mysqli_query($my_db, $upd_sql);
                }else{
                    $upd_sql = "UPDATE work_cms_reservation SET prd_no='{$combined_chg_data['comb_prd_no']}', manager_memo='예판 합포장수정' WHERE w_no='{$combined_chg_data['w_no']}'";
                    mysqli_query($my_db, $upd_sql);
                }
            }
        }
    }

    exit("<script>alert('합배송처리 했습니다');location.href='work_cms_reservation_list.php?{$search_url}';</script>");
}

# 검색용 변수, 리스트
$kind_model             = Kind::Factory();
$product_model 	        = ProductCms::Factory();
$kind_code		        = "product_cms";
$log_company_list       = $company_model->getLogisticsList();
$kind_chart_data_list 	= $kind_model->getKindChartData($kind_code);
$prd_group_list 		= $kind_chart_data_list['kind_group_list'];
$prd_group_name_list	= $kind_chart_data_list['kind_group_name'];
$prd_group_code_list	= $kind_chart_data_list['kind_group_code'];
$prd_group_code 		= implode(",", $prd_group_code_list);
$prd_data_list			= $product_model->getPrdGroupChartData($prd_group_code);
$prd_total_list			= $prd_data_list['prd_total'];

$prd_g1_list = $prd_g2_list = $prd_g3_list = [];
foreach($prd_group_list as $key => $prd_data)
{
    if(!$key){
        $prd_g1_list = $prd_data;
    }else{
        $prd_g2_list[$key] = $prd_data;
    }
}

# 검색 조건
$add_where          = "1=1";
$add_group_where    = " AND delivery_state != '4'";
$sch_prd_g1	        = isset($_GET['sch_prd_g1']) ? $_GET['sch_prd_g1'] : "";
$sch_prd_g2	        = isset($_GET['sch_prd_g2']) ? $_GET['sch_prd_g2'] : "";
$sch_prd	        = isset($_GET['sch_prd']) ? $_GET['sch_prd'] : "";
$prd_g2_list        = isset($prd_g2_list[$sch_prd_g1]) ? $prd_g2_list[$sch_prd_g1] : [];
$sch_prd_list       = isset($prd_total_list[$sch_prd_g2]) ? $prd_total_list[$sch_prd_g2] : [];

$smarty->assign("sch_prd_g1", $sch_prd_g1);
$smarty->assign("sch_prd_g2", $sch_prd_g2);
$smarty->assign("sch_prd", $sch_prd);
$smarty->assign("prd_g1_list", $prd_g1_list);
$smarty->assign("prd_g2_list", $prd_g2_list);
$smarty->assign("sch_prd_list", $sch_prd_list);

if (!empty($sch_prd) && $sch_prd != "0") {
    $add_where .= " AND w.prd_no='{$sch_prd}'";
}else{
    if($sch_prd_g2){
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code='{$sch_prd_g2}')";
    }elseif($sch_prd_g1){
        $add_where .= " AND w.prd_no IN(SELECT p.prd_no FROM product_cms p WHERE p.k_name_code IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_prd_g1}'))";
    }
}

# 브랜드 검색
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$sch_brand_g1               = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2               = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand                  = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where      .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];

    $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

    $add_where      .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

$sch_order_s_date 	    = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : "";
$sch_order_e_date 	    = isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : "";
$sch_out_date 	        = isset($_GET['sch_out_date']) ? $_GET['sch_out_date'] : "";
$sch_order_number 	    = isset($_GET['sch_order_number']) ? $_GET['sch_order_number'] : "";
$sch_recipient 		    = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp 	    = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_recipient_addr 	= isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
$sch_dp_c_no 		    = isset($_GET['sch_dp_c_no']) ? $_GET['sch_dp_c_no'] : "";
$sch_prd_name 		    = isset($_GET['sch_prd_name']) ? $_GET['sch_prd_name'] : "";
$sch_option 		    = isset($_GET['sch_option']) ? $_GET['sch_option'] : "";
$sch_quick_type	        = isset($_GET['sch_quick_type']) ? $_GET['sch_quick_type'] : "";

if(!empty($sch_order_s_date) || !empty($sch_order_e_date))
{
    if(!empty($sch_order_s_date)){
        $sch_ord_s_datetime = $sch_order_s_date." 00:00:00";
        $add_where .= " AND w.order_date >= '{$sch_ord_s_datetime}'";
        $smarty->assign('sch_order_s_date', $sch_order_s_date);
    }

    if(!empty($sch_order_e_date)){
        $sch_ord_e_datetime = $sch_order_e_date." 23:59:59";
        $add_where .= " AND w.order_date <= '{$sch_ord_e_datetime}'";
        $smarty->assign('sch_order_e_date', $sch_order_e_date);
    }
}

if(!empty($sch_out_date)){
    $add_where .= " AND w.out_date = '{$sch_out_date}'";
    $smarty->assign('sch_out_date', $sch_out_date);
}

if(!empty($sch_order_number)){
    $add_where .= " AND w.order_number = '{$sch_order_number}'";
    $smarty->assign('sch_order_number', $sch_order_number);
}

if(!empty($sch_delivery_state)){
    $add_where      .= " AND w.delivery_state = '{$sch_delivery_state}'";
    $add_group_where = " AND w.delivery_state = '{$sch_delivery_state}'";
    $smarty->assign('sch_delivery_state', $sch_delivery_state);
}else{
    $add_where .= " AND w.delivery_state != '4'";
}

if(!empty($sch_recipient)){
    $add_where .= " AND w.recipient like '%{$sch_recipient}%'";
    $smarty->assign('sch_recipient', $sch_recipient);
}

if(!empty($sch_recipient_hp)){
    $add_where .= " AND w.recipient_hp like '%{$sch_recipient_hp}%'";
    $smarty->assign('sch_recipient_hp', $sch_recipient_hp);
}

if(!empty($sch_recipient_addr)){
    $add_where .= " AND w.recipient_addr like '%{$sch_recipient_addr}%'";
    $smarty->assign('sch_recipient_addr', $sch_recipient_addr);
}

if(!empty($sch_dp_c_no)){
    $add_where .= " AND w.dp_c_no = '{$sch_dp_c_no}'";
    $smarty->assign('sch_dp_c_no', $sch_dp_c_no);
}

if(!empty($sch_prd_name)){
    $add_where .= " AND w.prd_no IN(SELECT prd_cms.prd_no from product_cms prd_cms where prd_cms.title like '%{$sch_prd_name}%')";
    $smarty->assign('sch_prd_name', $sch_prd_name);
}

if(!empty($sch_option))
{
    $chk_option_where = "";
    $chk_option_list  = [];

    if($sch_option == "240"){
        $chk_option_where = "option_no IN(240,241)";
    }elseif($sch_option == "238"){
        $chk_option_where = "option_no IN(238,239)";
    }elseif($sch_option == "475"){
        $chk_option_where = "option_no IN(473,475)";
    }elseif($sch_option == "275"){
        $chk_option_where = "option_no IN(275,276,277)";
    }elseif($sch_option == "100000"){
        $chk_option_list = array(1732,1733,1735,1736,1737,1738,1740,1742,1745,1734,1739,1741,1743,1744);
    }elseif($sch_option == "100001"){
        $chk_option_list = array(1733,1735,1738,1742,1745);
    }elseif($sch_option == "100002"){
        $chk_option_list = array(1732,1736,1737,1734,1739,1740,1741,1743,1744);
    }else{
        $chk_option_where = "option_no = '{$sch_option}'";
    }

    if(!empty($chk_option_where))
    {
        $chk_option_sql   = "SELECT DISTINCT pcr.prd_no FROM product_cms_relation pcr WHERE {$chk_option_where} AND pcr.display='1'";
        $chk_option_query = mysqli_query($my_db, $chk_option_sql);
        while($chk_option_result = mysqli_fetch_assoc($chk_option_query)){
            $chk_option_list[] = "'{$chk_option_result['prd_no']}'";
        }
    }

    if(!empty($chk_option_list)){
        $chk_option_text = implode(",", $chk_option_list);
        $add_where .= " AND w.prd_no IN({$chk_option_text})";
    }
    $smarty->assign('sch_option', $sch_option);
}
$smarty->assign('sch_quick_type', $sch_quick_type);

# 정렬기능
$add_orderby    = "w.order_date DESC, w.order_number ASC, w.prd_no ASC";

# 예판 리스트
$cms_reservation_total_sql		= "SELECT count(order_number) as cnt FROM (SELECT DISTINCT w.order_number FROM work_cms_reservation w WHERE {$add_where}) AS cnt";
$cms_reservation_total_query	= mysqli_query($my_db, $cms_reservation_total_sql);
$cms_reservation_total_result   = mysqli_fetch_array($cms_reservation_total_query);
$cms_reservation_total          = $cms_reservation_total_result['cnt'];

# 페이징처리
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "100";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$pagenum 	= ceil($cms_reservation_total/$num);

if ($pages >= $pagenum){$pages = $pagenum;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$pagelist	= pagelist($pages, "work_cms_reservation_list.php", $pagenum, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $cms_reservation_total);
$smarty->assign("pagelist", $pagelist);
$smarty->assign("ord_page_type", $page_type);

# 주문번호 뽑아내기
$cms_ord_sql    = "SELECT DISTINCT w.order_number FROM work_cms_reservation w WHERE {$add_where} AND w.order_number is not null ORDER BY {$add_orderby} LIMIT {$offset},{$num}";
$cms_ord_query  = mysqli_query($my_db, $cms_ord_sql);
$order_number_list  = [];
while($order_number = mysqli_fetch_assoc($cms_ord_query)){
    $order_number_list[] =  "'".$order_number['order_number']."'";
}
$order_numbers   = implode(',', $order_number_list);
$add_where_group = !empty($order_numbers) ? "w.order_number IN({$order_numbers})" : "1!=1";

$cms_reservation_sql = "
    SELECT
        *,
        DATE_FORMAT(w.regdate, '%Y-%m-%d') as reg_date,
        DATE_FORMAT(w.regdate, '%H:%i') as reg_time,
        DATE_FORMAT(w.order_date, '%Y-%m-%d') as ord_date,
        DATE_FORMAT(w.order_date, '%H:%i') as ord_time,
        (SELECT s.s_name FROM staff s WHERE s.s_no=w.s_no) as s_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_parent FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no))) AS k_prd1_name,
        (SELECT k_name FROM kind k WHERE k.k_name_code=(SELECT k_name_code FROM product_cms prd_cms WHERE prd_cms.prd_no=w.prd_no)) AS k_prd2_name,
        (SELECT title from product_cms prd_cms where prd_cms.prd_no=w.prd_no) as prd_name,
        (SELECT s.s_name FROM staff s WHERE s.s_no=w.task_req_s_no) as task_req_s_name,
        (SELECT s_name FROM staff s where s.s_no=w.task_run_s_no) as task_run_s_name
    FROM
    (
        SELECT
            w.w_no,
            w.delivery_state,
            w.regdate,
            w.stock_date,
            w.out_date,
            w.order_type,
            w.order_date,
            w.order_number,
            w.parent_order_number,
            w.recipient,
            w.recipient_hp,
            w.recipient_hp2,
            w.recipient_addr,
            IF(w.zip_code, CONCAT('[',w.zip_code,']'), '') as postcode,
            w.delivery_memo,
            w.s_no,
            w.log_c_no,
            w.c_name,
            w.prd_no,
            w.task_req,
            w.task_req_s_no,
            w.task_run_s_no,
            w.quantity,
            w.unit_price,
            w.coupon_price,
            w.dp_c_no,
            w.dp_c_name,
            w.manager_memo,
            w.notice,
            w.shop_ord_no,
            w.unit_delivery_price,
            w.final_price
        FROM work_cms_reservation w
        WHERE {$add_where_group} {$add_group_where}
    ) as w
    ORDER BY {$add_orderby}
";
$cms_reservation_query  = mysqli_query($my_db, $cms_reservation_sql);
$cms_reservation_list   = [];
$address_list           = [];
$final_price_list       = [];
while ($cms_reservation = mysqli_fetch_array($cms_reservation_query))
{
    if(!isset($final_price_list[$cms_reservation['order_number']])){
        $final_price_list[$cms_reservation['order_number']] = $cms_reservation['final_price'];
    }elseif($final_price_list[$cms_reservation['order_number']] <= 0 && $cms_reservation['final_price'] > 0){
        $final_price_list[$cms_reservation['order_number']] = $cms_reservation['final_price'];
    }

    $unit_sql   = "SELECT (SELECT pcum.sku FROM product_cms_unit_management pcum WHERE pcum.prd_unit=pcr.option_no AND pcum.log_c_no='{$with_log_c_no}') as sku, pcr.quantity as qty FROM product_cms_relation as pcr WHERE pcr.prd_no ='{$cms_reservation['prd_no']}' AND pcr.display='1'";
    $unit_query = mysqli_query($my_db, $unit_sql);
    $unit_list  = [];
    while($unit_result = mysqli_fetch_assoc($unit_query)){
        $unit_result['qty'] = $cms_reservation['quantity']*$unit_result['qty'];
        $unit_list[] = $unit_result;
    }
    $cms_reservation["unit_list"] = $unit_list;

    if(isset($cms_reservation['recipient_hp']) && !empty($cms_reservation['recipient_hp'])){
        $f_hp  = substr($cms_reservation['recipient_hp'],0,4);
        $e_hp  = substr($cms_reservation['recipient_hp'],7,15);
        $cms_reservation['recipient_sc_hp'] = $f_hp."***".$e_hp;
    }

    if(isset($cms_reservation['recipient_hp2']) && !empty($cms_reservation['recipient_hp2'])){
        $f_hp2  = substr($cms_reservation['recipient_hp2'],0,4);
        $e_hp2  = substr($cms_reservation['recipient_hp2'],7,15);
        $cms_reservation['recipient_sc_hp2'] = $f_hp2."***".$e_hp2;
    }

    if(!isset($address_list[$cms_reservation['order_number']]))
    {
        $address_list[$cms_reservation['order_number']] = array(
            'recipient'         => $cms_reservation['recipient'],
            'recipient_hp'      => $cms_reservation['recipient_hp'],
            'recipient_hp2'     => $cms_reservation['recipient_hp2'],
            'recipient_sc_hp'   => $cms_reservation['recipient_sc_hp'],
            'recipient_sc_hp2'  => $cms_reservation['recipient_sc_hp2'],
            'recipient_addr'    => $cms_reservation['recipient_addr'],
            'zip_code'          => $cms_reservation['postcode'],
        );
    }else{
        if(empty($address_list[$cms_reservation['order_number']]['recipient_hp']) && !empty($cms_reservation['recipient_hp']))
        {
            $address_list[$cms_reservation['order_number']]['recipient_hp'] = $cms_reservation['recipient_hp'];
        }

        if(empty($address_list[$cms_reservation['order_number']]['recipient_hp2']) && !empty($cms_reservation['recipient_hp2']))
        {
            $address_list[$cms_reservation['order_number']]['recipient_hp2'] = $cms_reservation['recipient_hp2'];
        }

        if(empty($address_list[$cms_reservation['order_number']]['recipient_sc_hp']) && !empty($cms_reservation['recipient_sc_hp']))
        {
            $address_list[$cms_reservation['order_number']]['recipient_sc_hp'] = $cms_reservation['recipient_sc_hp'];
        }

        if(empty($address_list[$cms_reservation['order_number']]['recipient_sc_hp2']) && !empty($cms_reservation['recipient_sc_hp2']))
        {
            $address_list[$cms_reservation['order_number']]['recipient_sc_hp2'] = $cms_reservation['recipient_sc_hp2'];
        }

        if(empty($address_list[$cms_reservation['order_number']]['recipient_addr']) && !empty($cms_reservation['recipient_addr']))
        {
            $address_list[$cms_reservation['order_number']]['recipient_addr'] = $cms_reservation['recipient_addr'];
        }

        if(empty($address_list[$cms_reservation['order_number']]['zip_code']) && !empty($cms_reservation['zip_code']))
        {
            $address_list[$cms_reservation['order_number']]['zip_code'] = $cms_reservation['zip_code'];
        }
    }

    $cms_reservation_list[$cms_reservation['order_number']][] = $cms_reservation;
}

$cms_reservation_unit_sql = "
    SELECT 
        pcr.option_no,
        (SELECT pcu.option_name FROM product_cms_unit pcu WHERE pcu.`no`=pcr.option_no) AS option_name,
        SUM(w.quantity*pcr.quantity) AS total_qty
    FROM work_cms_reservation AS w
    LEFT JOIN product_cms_relation pcr ON pcr.prd_no=w.prd_no AND pcr.display='1'
    WHERE move_date IS NULL
    GROUP BY option_no
";
$cms_reservation_unit_query = mysqli_query($my_db, $cms_reservation_unit_sql);
$cms_reservation_unit_list  = [];
while($cms_reservation_unit = mysqli_fetch_assoc($cms_reservation_unit_query)){
    $cms_reservation_unit_list[] = $cms_reservation_unit;
}

$wait_option_list = array(
//    "481" => "닥터피엘 미세버블 세면대필터 본품"
//    "100000"    => "겨울이불 전체",
//    "100001"    => "겨울이불(다크그레이 제외)",
//    "100002"    => "겨울이불(다크그레이 포함)",
//    "240" => "누잠_더블업토퍼_매트리스_Q_그레이 (단품)",
//    "238" => "누잠_더블업토퍼_매트리스_SS_그레이 (단품)",
//    "142" => "시티파이 스타일에어 (블랙)",
//    "441" => "닥터피엘 아토샤워헤드 기본세트",
//    "475" => "누잠_더블업토퍼_매트리스_Q_그레이 (단품)",
//    "275" => "닥터피엘 올뉴 큐빙",
    "459" => "누잠 경추심베개 본품",
);

$smarty->assign('page_type_option', getPageTypeOption(4));
$smarty->assign('quick_search_option', getReservationQuickOption());
$smarty->assign('reservation_state_option', getReservationStateOption());
$smarty->assign('sch_dp_company_option', $company_model->getDpDisplayList());
$smarty->assign('wait_option_list', $wait_option_list);
$smarty->assign("address_list", $address_list);
$smarty->assign("final_price_list", $final_price_list);
$smarty->assign("cms_reservation_unit_list", $cms_reservation_unit_list);
$smarty->assign("cms_reservation_list", $cms_reservation_list);

$smarty->display('work_cms_reservation_list.html');
?>
