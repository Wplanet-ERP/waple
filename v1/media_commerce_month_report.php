<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/commerce_sales.php');
require('inc/helper/work_cms.php');
require('inc/helper/wise_bm.php');
require('inc/model/MyQuick.php');
require('inc/model/CommerceSales.php');
require('inc/model/ProductCms.php');
require('inc/model/Kind.php');

# Navigation & My Quick
$nav_prd_no  = "156";
$nav_title   = "커머스 매출 및 비용관리(월정산)";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# Kind(cost,sales), Brand Group 검색
$comm_sales_model           = CommerceSales::Factory();
$comm_chart_total_init      = $comm_sales_model->getChartAutoReportData();
$comm_g1_title_list         = $comm_chart_total_init['comm_g1_title'];
$comm_g2_title_list         = $comm_chart_total_init['comm_g2_title'];

$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$dp_except_list             = getNotApplyDpList();
$dp_except_text             = implode(",", $dp_except_list);
$global_dp_all_list         = getGlobalDpCompanyList();
$global_dp_ilenol_list      = $global_dp_all_list['ilenol'];
$global_brand_list          = getGlobalBrandList();
$global_brand_option        = array_keys($global_brand_list);
$doc_brand_list             = getTotalDocBrandList();
$doc_brand_text             = implode(",", $doc_brand_list);
$ilenol_brand_list          = getIlenolBrandList();
$ilenol_brand_text          = implode(",", $ilenol_brand_list);
$ilenol_global_dp_text      = implode(",", $global_dp_ilenol_list);
$sch_brand_g2_list          = [];
$sch_brand_list             = [];
$sch_brand_name             = "";
$kind_column_list           = [];
$brand_column_list          = [];
$brand_company_list         = [];

/** 검색조건 START */
$total_brand_option     = array("1314","2863","3303","3386","4140","4445","4446","5812","5283","5434","2827","4878","5201","5642","5415","5810","6026","6060","2388","4440","4809","5759","4333","5445","5493");
$total_brand_where      = implode(",", $total_brand_option);
$add_where              = "display='1'";
$add_kind_where         = "";
$add_cms_where          = "w.delivery_state='4' AND dp_c_no NOT IN({$dp_except_text})";
$add_quick_where        = "w.prd_type='1' AND w.unit_price > 0 AND w.quick_state='4'";
$add_work_where         = "w.work_state='6' AND w.prd_no IN(SELECT p.prd_no FROM product p WHERE p.k_name_code IN('01041','01053') AND display='1') AND w.c_no IN({$total_brand_where})";

# 브랜드 검색
$sch_toggle         = isset($_GET['sch_toggle']) ? $_GET['sch_toggle'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

$smarty->assign("sch_toggle", $sch_toggle);

# 브랜드 검색 설정 및 수정여부
if(!isset($_GET['sch_brand_g1']))
{
    switch($session_s_no){
        case '4':
            $sch_brand_g1   = '70003';
            break;
        case '6':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70008';
            break;
        case '7':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70014';
            $sch_brand      = '4446';
            break;
        case '10':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5368';
            break;
        case '15':
            $sch_brand_g1   = '70002';
            break;
        case '17':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '1314';
            break;
        case '42':
        case '177':
        case '265':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            break;
        case '59':
            $sch_brand_g1   = '70005';
            $sch_brand_g2   = '70020';
            $sch_brand      = '2402';
            break;
        case '67':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5434';
            break;
        case '145':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70010';
            break;
        case '147':
            $sch_brand_g1   = '70004';
            $sch_brand_g2   = '70019';
            break;
        case '43':
            $sch_brand_g1   = '70001';
            $sch_brand_g2   = '70007';
            $sch_brand      = '5812';
            break;
    }
}

$sch_cms_brand_url    = "sch=Y";
$sch_work_brand_url   = "sch=Y";
$sch_agency_brand_url = "sch_manager_team=all&sch_manager=all&sch_my_c_no=3";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_brand))
{
    $sch_brand_g2_list              = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list                 = $brand_list[$sch_brand_g2];
    $brand_column_list[$sch_brand]  = $brand_list[$sch_brand_g2][$sch_brand];
    $brand_company_list[$sch_brand] = $sch_brand;
    $sch_brand_name                 = $brand_list[$sch_brand_g2][$sch_brand];

    if(in_array($sch_brand, $global_brand_option)) {
        if($sch_brand == '5979' || $sch_brand == '6044'){
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','09056'))";
            $add_kind_where .= " AND k_parent IN('08027','09056')";
        }elseif($sch_brand == '5514'){
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08033','08034','08035','08036','08042','08046','09060','09061','09062','09063','09081','09092'))";
            $add_kind_where .= " AND k_parent IN('08033','08034','08035','08036','08042','08046','09060','09061','09062','09063','09081','09092')";
        }else{
            $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092'))";
            $add_kind_where .= " AND k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092')";
        }
        $is_world = true;
    }
    else {
        $add_where      .= " AND `cr`.brand = '{$sch_brand}' AND cr.k_name_code NOT IN(SELECT sub.k_name_code FROM kind sub WHERE sub.k_parent IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092'))";
        $add_kind_where .= " AND k_parent NOT IN('08027','08033','08034','08035','08036','08042','08046','09056','09060','09061','09062','09063','09081','09092')";
    }

    if(in_array($sch_brand, $doc_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5956' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif(in_array($sch_brand, $ilenol_brand_list)){
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}' AND `w`.log_c_no != '5659' AND `w`.dp_c_no != '6003'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "5513"){
        $add_cms_where      .= " AND `w`.c_no IN({$doc_brand_text}) AND (`w`.log_c_no = '5956' OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$doc_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5514"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND ((`w`.log_c_no = '5659' AND `w`.dp_c_no NOT IN({$ilenol_global_dp_text})) OR `w`.dp_c_no='6003')";
        $add_quick_where    .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.run_c_no = '6003'";
    }
    elseif($sch_brand == "5979"){
        $add_cms_where      .= " AND `w`.c_no IN({$ilenol_brand_text}) AND `w`.c_no != '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    elseif($sch_brand == "6044"){
        $add_cms_where      .= " AND `w`.c_no = '5759' AND `w`.log_c_no = '5659' AND `w`.dp_c_no IN({$ilenol_global_dp_text})";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}' AND `w`.run_c_no != '6003'";
    }
    else{
        $add_cms_where      .= " AND `w`.c_no = '{$sch_brand}'";
        $add_quick_where    .= " AND `w`.c_no = '{$sch_brand}'";
    }

    $add_work_where       .= " AND `w`.c_no = '{$sch_brand}'";
    $sch_cms_brand_url    .= "&sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}&sch_brand={$sch_brand}";
    $sch_work_brand_url   .= "&sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}&sch_brand={$sch_brand}";
    $sch_agency_brand_url .= "&sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}&sch_brand={$sch_brand}";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $brand_column_list  = $sch_brand_list;
    $sch_brand_name     = $brand_company_g2_list[$sch_brand_g1][$sch_brand_g2];

    foreach($sch_brand_list as $c_no => $c_name) {
        $brand_company_list[$c_no] = $c_no;
    }

    $add_where           .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_cms_where       .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_quick_where     .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    $add_work_where      .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";

    $sch_cms_brand_url    .= "&sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}";
    $sch_work_brand_url   .= "&sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}";
    $sch_agency_brand_url .= "&sch_brand_g1={$sch_brand_g1}&sch_brand_g2={$sch_brand_g2}";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $brand_column_list  = $sch_brand_g2_list;
    $sch_brand_name     = $brand_company_g1_list[$sch_brand_g1];

    foreach($sch_brand_g2_list as $brand_g2 => $brand_g2_name)
    {
        $init_company_list = $brand_list[$brand_g2];
        if(!empty($init_company_list))
        {
            foreach($init_company_list as $c_no => $c_name) {
                $brand_company_list[$c_no] = $c_no;
            }
        }
    }

    $add_where           .= " AND cr.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_cms_where       .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_quick_where     .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    $add_work_where      .= " AND w.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";

    $sch_cms_brand_url    .= "&sch_brand_g1={$sch_brand_g1}";
    $sch_work_brand_url   .= "&sch_brand_g1={$sch_brand_g1}";
    $sch_agency_brand_url .= "&sch_brand_g1={$sch_brand_g1}";
}
else
{
    $brand_column_list  = $brand_company_g1_list;

    foreach($brand_company_g1_list as $brand_g1 => $brand_g1_name)
    {
        $init_g2_list = $brand_company_g2_list[$brand_g1];
        if(!empty($init_g2_list))
        {
            foreach($init_g2_list as $brand_g2 => $brand_g2_name)
            {
                $init_company_list = $brand_list[$brand_g2];
                if(!empty($init_company_list))
                {
                    foreach($init_company_list as $c_no => $c_name) {
                        $brand_company_list[$c_no] = $c_no;
                    }
                }
            }
        }
    }
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_name", $sch_brand_name);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 날짜별 검색
$sch_cur_month      = date('Y-m');
$sch_date_type      = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "3";
$sch_s_year         = isset($_GET['sch_s_year']) ? $_GET['sch_s_year'] : date('Y', strtotime("-5 years"));;
$sch_e_year         = isset($_GET['sch_e_year']) ? $_GET['sch_e_year'] : date('Y');
$sch_s_month        = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m', strtotime("{$sch_cur_month} -2 months"));
$sch_e_month        = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m', strtotime("{$sch_cur_month} -1 months"));
$sch_not_empty      = isset($_GET['sch_not_empty']) ? $_GET['sch_not_empty'] : "";

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_s_year', $sch_s_year);
$smarty->assign('sch_e_year', $sch_e_year);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_not_empty', $sch_not_empty);

//전체 기간 조회 및 누적데이터 조회
$all_date_where     = "";
$all_date_key	    = "";
$add_date_where     = "";
$add_date_column    = "";

$add_cms_column     = "";
$add_quick_column   = "";
$add_work_column    = "";

if($sch_date_type == '4') //연간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y') BETWEEN '{$sch_s_year}' AND '{$sch_e_year}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y')";

    $sch_s_datetime     = $sch_s_year."-01-01 00:00:00";
    $sch_e_datetime     = $sch_e_year."-12-31 23:59:59";

    $add_cms_where     .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column     = "DATE_FORMAT(order_date, '%Y')";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y')";

    $add_work_where    .= " AND w.task_run_regdate BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_work_column    = "DATE_FORMAT(w.task_run_regdate, '%Y')";
}
elseif($sch_date_type == '3') //월간
{
    $all_date_where     = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
    $all_date_key 	    = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title     = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where     = " AND DATE_FORMAT(sales_date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}' ";
    $add_date_column    = "DATE_FORMAT(sales_date, '%Y%m')";

    $sch_s_datetime     = $sch_s_month."-01 00:00:00";
    $sch_e_datetime     = $sch_e_month."-31 23:59:59";

    $add_cms_where     .= " AND order_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_cms_column     = "DATE_FORMAT(order_date, '%Y%m')";

    $add_quick_where   .= " AND run_date BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_quick_column   = "DATE_FORMAT(run_date, '%Y%m')";

    $add_work_where    .= " AND w.task_run_regdate BETWEEN '{$sch_s_datetime}' AND '{$sch_e_datetime}' ";
    $add_work_column    = "DATE_FORMAT(w.task_run_regdate, '%Y%m')";
}

# 기본 Report 리스트 Init 및 x key 및 타이틀 설정
$kind_column_list  = [];
$kind_column_sql   = "
    SELECT 
        k_parent, 
        (SELECT sub_k.k_name FROM kind sub_k WHERE sub_k.k_name_code=k.k_parent) as k_parent_name, 
        (SELECT sub_k.priority FROM kind sub_k WHERE sub_k.k_name_code=k.k_parent) as k_parent_priority, 
        k_code, 
        k_name_code, 
        k_name 
    FROM kind k 
    WHERE (k_code='cost' OR k_code='sales')
      AND k_parent is not null {$add_kind_where} 
    ORDER BY k_parent_priority ASC, priority ASC
";
$kind_column_query = mysqli_query($my_db, $kind_column_sql);
while($kind_column_data = mysqli_fetch_assoc($kind_column_query))
{
    $kind_column_list[$kind_column_data['k_parent']][$kind_column_data['k_name_code']] = array(
        'k_name'        => $kind_column_data['k_name'],
        'k_name_code'   => $kind_column_data['k_name_code'],
        'type'          => $kind_column_data['k_code'],
        'k_parent'      => $kind_column_data['k_parent'],
        'k_parent_name' => $kind_column_data['k_parent_name'],
        'date'          => "",
        'price'         => 0
    );
}

# 배송리스트 구매처
$cms_title_sql          = "SELECT DISTINCT dp_c_no, dp_c_name, (SELECT c.priority FROM company c WHERE c.c_no=w.dp_c_no) as priority FROM work_cms as w WHERE {$add_cms_where} ORDER BY priority ASC, dp_c_name ASC";
$cms_title_query        = mysqli_query($my_db, $cms_title_sql);
$cms_report_title_list  = [];
while($cms_title = mysqli_fetch_assoc($cms_title_query)) {
    $cms_report_title_list[$cms_title['dp_c_no']] = array('title' => $cms_title['dp_c_name'], 'total' => 0);
}

# 입/출고요청 구매처
$quick_title_sql            = "SELECT DISTINCT run_c_no, run_c_name FROM work_cms_quick as w WHERE {$add_quick_where} ORDER BY run_c_name ASC";
$quick_title_query          = mysqli_query($my_db, $quick_title_sql);
$quick_report_title_list    = [];
while($quick_title = mysqli_fetch_assoc($quick_title_query)) {
    $quick_report_title_list[$quick_title['run_c_no']] = $quick_title['run_c_name'];
}

# 업체 관련업무명
$work_title_sql     = "SELECT  p.prd_no, p.k_name_code, (SELECT k.k_name FROM kind k WHERE k.k_name_code=p.k_name_code) as prd_g1_name, p.title as prd_name FROM product p WHERE p.k_name_code IN('01041','01053') AND display='1' ORDER BY p.priority ASC";
$work_title_query   = mysqli_query($my_db, $work_title_sql);
$work_title_list    = [];
$work_g1_title_list = [];
$work_g2_title_list = [];
while($work_title = mysqli_fetch_assoc($work_title_query)) {
    $work_title_list[$work_title['k_name_code']][$work_title['prd_no']] = $work_title['prd_name'];

    $work_g1_title_list['cost'][$work_title['k_name_code']]  = ($work_title['k_name_code'] == "01053") ? "마케팅비용" : $work_title['prd_g1_name'];
    $work_g1_title_list['sales'][$work_title['k_name_code']] = ($work_title['k_name_code'] == "01053") ? "대행수수료" : $work_title['prd_g1_name'];
    $work_g2_title_list[$work_title['prd_no']]      = $work_title['prd_name'];
}

# 기본 리스트
$commerce_date_list     = [];
$coupon_report_list     = [];
$track_report_list      = [];
$cost_report_list       = [];
$cost_work_list         = [];
$cms_report_list        = [];
$quick_report_list      = [];
$sales_report_list      = [];
$sales_work_list        = [];

# 합계 리스트
$cms_month_total_list           = [];
$cms_report_total_list          = [];
$cost_work_total_list           = [];
$sales_work_total_list          = [];
$report_empty_check_list        = [];
$report_empty_work_check_list   = [];

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";
if($all_date_where != '')
{
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        # 기본 비용/매출 관련 Init
        foreach($kind_column_list as $comm_g1 => $y_columns)
        {
            $report_empty_check_list[$comm_g1] = 0;
            foreach($y_columns as $comm_g2 => $y_column){
                if($y_column['type'] == 'sales'){
                    $sales_report_list[$comm_g1][$comm_g2][$date['chart_key']] = $y_column;
                    $sales_report_list[$comm_g1][$comm_g2][$date['chart_key']]['date'] = $date['chart_key'];
                }elseif($y_column['type'] == 'cost'){
                    $cost_report_list[$comm_g1][$comm_g2][$date['chart_key']] = $y_column;
                    $cost_report_list[$comm_g1][$comm_g2][$date['chart_key']]['date'] = $date['chart_key'];
                }

                $report_empty_check_list[$comm_g2] = 0;
            }
        }

        # CMS 매출 관련 Init
        foreach($cms_report_title_list as $cms_key => $cms_title)
        {
            $cms_report_list[$cms_key][$date['chart_key']]['subtotal'] = 0;
            $cms_report_list[$cms_key][$date['chart_key']]['delivery'] = 0;
        }

        foreach($quick_report_title_list as $quick_key => $quick_title)
        {
            $quick_report_list[$quick_key][$date['chart_key']] = 0;
        }

        $cms_report_total_list[$date['chart_key']]  = array("subtotal" => 0, "delivery" => 0, "total" => 0);
        $coupon_report_list[$date['chart_key']]     = array("imweb" => 0, "doc" => 0, "ilenol", "nuzam" => 0);
        $track_report_list[$date['chart_key']]      = 0;
        $cms_month_total_list[$date['chart_key']]   = array(
            "sales_auto"    => 0,
            "sales_base"    => 0,
            "sales_month"   => 0,
            "sales_deposit" => 0,
            "sales_total"   => 0,
            "cost_auto"     => 0,
            "cost_base"     => 0,
            "cost_month"    => 0,
            "cost_total"    => 0,
            "month_total"   => 0,
        );

        # 입/출금 매출 관련 Init
        foreach($work_title_list as $work_g1 => $work_data)
        {
            $report_empty_work_check_list['cost'][$work_g1]  = 0;
            $report_empty_work_check_list['sales'][$work_g1] = 0;

            $cost_work_total_list[$work_g1][$date['chart_key']]  = 0;
            $sales_work_total_list[$work_g1][$date['chart_key']] = 0;

            foreach($work_data as $prd_no => $prd_title){
                $cost_work_list[$work_g1][$prd_no][$date['chart_key']]  = 0;
                $sales_work_list[$work_g1][$prd_no][$date['chart_key']] = 0;
                $report_empty_work_check_list['cost'][$prd_no]  = 0;
                $report_empty_work_check_list['sales'][$prd_no] = 0;
            }
        }

        # 날짜 Init 및 Link용 작업
        if(!isset($commerce_date_list[$date['chart_key']]))
        {
            $chart_s_date       = "";
            $chart_e_date       = "";
            $chart_next_s_date  = "";
            $chart_next_e_date  = "";
            $chart_mon          = "";
            $agency_mon         = "";

            if($sch_date_type == '4'){
                $chart_s_date       = $chart_title."-01-01";
                $chart_e_date       = $chart_title."-12-31";
                $chart_next_s_date  = ($chart_title+1)."-01-01";
                $chart_next_e_date  = ($chart_title+1)."-12-31";
                $chart_mon          = date("Y-m", strtotime($chart_s_date));
            }elseif($sch_date_type == '3'){
                $chart_s_date_val   = $date['chart_key']."01";
                $chart_s_date       = date("Y-m-d", strtotime("{$chart_s_date_val}"));
                $chart_e_day        = DATE('t', strtotime($chart_s_date));
                $chart_e_date_val   = $date['chart_key'].$chart_e_day;
                $chart_e_date       = date("Y-m-d", strtotime("{$chart_e_date_val}"));
                $chart_mon          = date("Y-m", strtotime($chart_s_date));
                $agency_mon         = date("Y-m", strtotime("{$chart_mon} -1 months"));

                $chart_next_s_mon   = date("Y-m", strtotime("{$chart_s_date} +1 months"));
                $chart_next_s_date  = $chart_next_s_mon."-01";
                $chart_next_e_day   = DATE('t', strtotime($chart_next_s_date));
                $chart_next_e_date  = "{$chart_next_s_mon}-{$chart_next_e_day}";
                
            }
            $commerce_date_list[$date['chart_key']] = array('title' => $chart_title, 's_date' => $chart_s_date, 'e_date' => $chart_e_date, 'next_s_date' => $chart_next_s_date,'next_e_date' => $chart_next_e_date, 'mon_date' => $chart_mon, "agency_mon" => $agency_mon);
        }
    }
}

# 기본 비용, 매출 값
$commerce_sql  = "
    SELECT
        cr.`type`,
        cr.price as price,
        cr.k_name_code as comm_g2,
        {$add_date_column} as key_date,
        (SELECT k.k_code FROM kind k WHERE k.k_name_code=`cr`.k_name_code) as comm_type,
        (SELECT k.k_name_code FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cr`.k_name_code)) as comm_g1,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code=(SELECT sub_k.k_parent FROM kind sub_k WHERE sub_k.k_name_code=`cr`.k_name_code)) as comm_g1_name,
        (SELECT k.k_name FROM kind k WHERE k.k_name_code=`cr`.k_name_code) as comm_g2_name
    FROM commerce_report `cr`
    WHERE {$add_where}
    {$add_date_where}
    ORDER BY key_date ASC
";
$commerce_query = mysqli_query($my_db, $commerce_sql);
while($commerce = mysqli_fetch_assoc($commerce_query))
{
    $comm_g1 = str_pad($commerce['comm_g1'], 5, "0", STR_PAD_LEFT);
    if($commerce['type'] == 'sales'){
        $sales_report_list[$comm_g1][$commerce['comm_g2']][$commerce['key_date']]['price'] += $commerce['price'];
        $cms_month_total_list[$commerce['key_date']]['sales_base']  += $commerce['price'];
        $cms_month_total_list[$commerce['key_date']]['sales_total'] += $commerce['price'];
    }elseif($commerce['type'] == 'cost'){
        $cost_report_list[$comm_g1][$commerce['comm_g2']][$commerce['key_date']]['price'] += $commerce['price'];
        $cms_month_total_list[$commerce['key_date']]['cost_base']  += $commerce['price'];
        $cms_month_total_list[$commerce['key_date']]['cost_total'] += $commerce['price'];
    }

    $report_empty_check_list[$comm_g1] += $commerce['price'];
    $report_empty_check_list[$commerce['comm_g2']] += $commerce['price'];
}

# 배송리스트 매출관련
$cms_report_sql   = "
    SELECT 
        dp_c_no, 
        dp_c_name,
        order_number,
        {$add_cms_column} as key_date, 
        unit_price,
        (SELECT SUM(wcc.coupon_price) FROM work_cms_coupon wcc WHERE wcc.order_number=w.order_number AND wcc.shop_ord_no=w.shop_ord_no) as coupon_price,
        unit_delivery_price,
        (SELECT COUNT(*) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) AS track_cnt,
        (SELECT SUM(delivery_fee) FROM work_cms_delivery wcd WHERE wcd.order_number=w.order_number) AS track_fee
    FROM work_cms as w
    WHERE {$add_cms_where}
    ORDER BY key_date ASC
";
$cms_report_query = mysqli_query($my_db, $cms_report_sql);
while($cms_report_data = mysqli_fetch_assoc($cms_report_query))
{
    $cms_report_list[$cms_report_data['dp_c_no']][$cms_report_data['key_date']]['subtotal'] += $cms_report_data['unit_price'];
    $cms_report_list[$cms_report_data['dp_c_no']][$cms_report_data['key_date']]['delivery'] += $cms_report_data['unit_delivery_price'];

    $cms_total_price = $cms_report_data['unit_price']+$cms_report_data['unit_delivery_price'];
    $cms_month_total_list[$cms_report_data['key_date']]['sales_auto']  += $cms_total_price;
    $cms_month_total_list[$cms_report_data['key_date']]['sales_total'] += $cms_total_price;

    $cms_report_total_list[$cms_report_data['key_date']]['total']    += $cms_total_price;
    $cms_report_total_list[$cms_report_data['key_date']]['subtotal'] += $cms_report_data['unit_price'];
    $cms_report_total_list[$cms_report_data['key_date']]['delivery'] += $cms_report_data['unit_delivery_price'];

    if($cms_report_data['dp_c_no'] == '1372'){
        $coupon_report_list[$cms_report_data['key_date']]['imweb'] += $cms_report_data['coupon_price'];
        $cms_month_total_list[$cms_report_data['key_date']]['cost_auto']  += $cms_report_data['coupon_price'];
        $cms_month_total_list[$cms_report_data['key_date']]['cost_total'] += $cms_report_data['coupon_price'];
    }

    if($cms_report_data['dp_c_no'] == '5800'){
        $coupon_report_list[$cms_report_data['key_date']]['doc'] += $cms_report_data['coupon_price'];
        $cms_month_total_list[$cms_report_data['key_date']]['cost_auto']  += $cms_report_data['coupon_price'];
        $cms_month_total_list[$cms_report_data['key_date']]['cost_total'] += $cms_report_data['coupon_price'];
    }

    if($cms_report_data['dp_c_no'] == '5958'){
        $coupon_report_list[$cms_report_data['key_date']]['ilenol'] += $cms_report_data['coupon_price'];
        $cms_month_total_list[$cms_report_data['key_date']]['cost_auto']  += $cms_report_data['coupon_price'];
        $cms_month_total_list[$cms_report_data['key_date']]['cost_total'] += $cms_report_data['coupon_price'];
    }

    if($cms_report_data['dp_c_no'] == '6012'){
        $coupon_report_list[$cms_report_data['key_date']]['nuzam'] += $cms_report_data['coupon_price'];
        $cms_month_total_list[$cms_report_data['key_date']]['cost_auto']  += $cms_report_data['coupon_price'];
        $cms_month_total_list[$cms_report_data['key_date']]['cost_total'] += $cms_report_data['coupon_price'];
    }

    $cms_report_title_list[$cms_report_data['dp_c_no']]['total'] += $cms_total_price;

    if($cms_report_data['track_fee'] > 0){
        $track_report_list[$cms_report_data['key_date']] += $cms_report_data['track_fee']/$cms_report_data['track_cnt'];
    }
}

foreach($cms_report_title_list as $dp_c_no => $cms_title_chk)
{
    if($cms_title_chk['total'] == 0){
        unset($cms_report_title_list[$dp_c_no]);
        unset($cms_report_list[$dp_c_no]);
    }
}

# 입/출고요청 리스트
$quick_report_sql      = "
    SELECT 
        c_no,
        run_c_no,
        run_c_name,
        order_number,
        DATE_FORMAT(run_date, '%Y%m%d') as sales_date,
        {$add_quick_column} as key_date, 
        unit_price
    FROM work_cms_quick as w
    WHERE {$add_quick_where}
";
$quick_report_query = mysqli_query($my_db, $quick_report_sql);
while($quick_report_result = mysqli_fetch_assoc($quick_report_query))
{
    $quick_report_list[$quick_report_result['run_c_no']][$quick_report_result['key_date']] += $quick_report_result['unit_price'];

    $cms_report_total_list[$quick_report_result['key_date']]['subtotal']    += $quick_report_result['unit_price'];
    $cms_report_total_list[$quick_report_result['key_date']]['total']       += $quick_report_result['unit_price'];
    $cms_month_total_list[$quick_report_result['key_date']]['sales_auto']   += $quick_report_result['unit_price'];
    $cms_month_total_list[$quick_report_result['key_date']]['sales_total']  += $quick_report_result['unit_price'];
}

# 출금리스트 비용(자동)
$work_sql = "
    SELECT
        *,
        SUM(work_wd_price) as work_wd_total,
        SUM(work_dp_price) as work_dp_total,
        SUM(agency_dp_price) as agency_dp_total,
        SUM(agency_wd_price) as agency_wd_total
    FROM
    (
        SELECT
            w.prd_no, 
            (SELECT sub.k_name_code FROM product sub WHERE sub.prd_no=w.prd_no) as prd_g1,
            {$add_work_column} as key_date, 
            w.wd_price as work_wd_price, 
            w.dp_price as work_dp_price,
            (SELECT `as`.dp_price FROM agency_settlement as `as` WHERE `as`.w_no=w.w_no) as agency_dp_price,
            (SELECT `as`.wd_price FROM agency_settlement as `as` WHERE `as`.w_no=w.w_no) as agency_wd_price
        FROM `work` w
        WHERE {$add_work_where}
    ) AS rs
    GROUP BY prd_no, key_date
";
$work_query = mysqli_query($my_db, $work_sql);
while($work_result = mysqli_fetch_assoc($work_query))
{
    $prd_g1 = str_pad($work_result['prd_g1'], 5, "0", STR_PAD_LEFT);

    $dp_price = ($prd_g1 == "01053") ? $work_result['agency_dp_total'] : $work_result['work_dp_total'];
    $wd_price = ($prd_g1 == "01053") ? $work_result['agency_wd_total'] : $work_result['work_wd_total'];

    $cost_work_list[$prd_g1][$work_result['prd_no']][$work_result['key_date']]  += $wd_price;
    $cost_work_total_list[$prd_g1][$work_result['key_date']]  += $wd_price;
    $cms_month_total_list[$work_result['key_date']]['cost_month'] += $wd_price;
    $cms_month_total_list[$work_result['key_date']]['cost_total'] += $wd_price;

    $sales_work_list[$prd_g1][$work_result['prd_no']][$work_result['key_date']] += $dp_price;
    $sales_work_total_list[$prd_g1][$work_result['key_date']]  += $dp_price;
    $cms_month_total_list[$work_result['key_date']]['sales_month'] += $dp_price;
    $cms_month_total_list[$work_result['key_date']]['sales_total'] += $dp_price;

    $report_empty_work_check_list['cost'][$prd_g1]  += $wd_price;
    $report_empty_work_check_list['sales'][$prd_g1] += $dp_price;
    $report_empty_work_check_list['cost'][$work_result['prd_no']]   += $wd_price;
    $report_empty_work_check_list['sales'][$work_result['prd_no']]  += $dp_price;
}

# 지출 빈값 구하기
$cost_report_total_list = [];
if(!empty($cost_report_list))
{
    foreach($cost_report_list as $comm_g1 => $cost_parent_report)
    {
        if($sch_not_empty && $report_empty_check_list[$comm_g1] == 0)
        {
            unset($cost_report_list[$comm_g1]);
            continue;
        }

        foreach($cost_parent_report as $comm_g2 => $cost_report)
        {
            if($sch_not_empty && $report_empty_check_list[$comm_g2] == 0) {
                unset($cost_report_list[$comm_g1][$comm_g2]);
            }

            foreach($cost_report as $key_date => $cost_data){
                if(!isset($cost_report_total_list[$comm_g1])){
                    $cost_report_total_list[$comm_g1][$key_date] = 0;
                }
                $cost_report_total_list[$comm_g1][$key_date] += $cost_data['price'];
            }
        }
    }
}

# 매출 빈값 숨기기
$sales_report_total_list = [];
if(!empty($sales_report_list))
{
    foreach($sales_report_list as $comm_g1 => $sales_parent_report)
    {
        if($sch_not_empty && $report_empty_check_list[$comm_g1] == 0)
        {
            unset($sales_report_list[$comm_g1]);
            continue;
        }

        foreach($sales_parent_report as $comm_g2 => $sales_report)
        {
            if($sch_not_empty && $report_empty_check_list[$comm_g2] == 0) {
                unset($sales_report_list[$comm_g1][$comm_g2]);
            }

            foreach($sales_report as $key_date => $sales_data){
                if(!isset($sales_report_total_list[$comm_g1])){
                    $sales_report_total_list[$comm_g1][$key_date] = 0;
                }
                $sales_report_total_list[$comm_g1][$key_date] += $sales_data['price'];
            }
        }
    }
}

if(!empty($cost_work_list))
{
    $chk_work_list = $report_empty_work_check_list['cost'];
    foreach($cost_work_list as $comm_g1 => $cost_parent_report)
    {
        if($sch_not_empty && $chk_work_list[$comm_g1] == 0)
        {
            unset($cost_work_list[$comm_g1]);
            continue;
        }

        foreach($cost_parent_report as $comm_g2 => $cost_report){
            if($sch_not_empty && $chk_work_list[$comm_g2] == 0) {
                unset($cost_work_list[$comm_g1][$comm_g2]);
            }
        }
    }
}

if(!empty($sales_work_list))
{
    $chk_work_list = $report_empty_work_check_list['sales'];
    foreach($sales_work_list as $comm_g1 => $sales_parent_report)
    {
        if($sch_not_empty && $chk_work_list[$comm_g1] == 0)
        {
            unset($sales_work_list[$comm_g1]);
            continue;
        }

        foreach($sales_parent_report as $comm_g2 => $sales_report){
            if($sch_not_empty && $chk_work_list[$comm_g2] == 0) {
                unset($sales_work_list[$comm_g1][$comm_g2]);
            }
        }
    }
}

if(!empty($cms_month_total_list))
{
    foreach($cms_month_total_list as $key_date => $cms_month_total){
        $cms_month_total_list[$key_date]['month_total'] =  $cms_month_total['sales_total'] - $cms_month_total['cost_total'];
    }
}

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);
$smarty->assign('sch_cms_brand_url', $sch_cms_brand_url);
$smarty->assign('sch_agency_brand_url', $sch_agency_brand_url);
$smarty->assign('sch_agency_brand_url', $sch_agency_brand_url);

$smarty->assign('commerce_date_list', $commerce_date_list);
$smarty->assign('commerce_date_count', count($commerce_date_list));
$smarty->assign('coupon_report_list', $coupon_report_list);
$smarty->assign('track_report_list', $track_report_list);
$smarty->assign('cost_report_list', $cost_report_list);
$smarty->assign('cost_report_total_list', $cost_report_total_list);
$smarty->assign('cms_report_list', $cms_report_list);
$smarty->assign('cms_report_title_list', $cms_report_title_list);
$smarty->assign('cms_report_total_list', $cms_report_total_list);
$smarty->assign('quick_report_list', $quick_report_list);
$smarty->assign('quick_report_title_list', $quick_report_title_list);
$smarty->assign('sales_report_list', $sales_report_list);
$smarty->assign('sales_report_total_list', $sales_report_total_list);
$smarty->assign('cms_month_total_list', $cms_month_total_list);
$smarty->assign('cost_work_list', $cost_work_list);
$smarty->assign('cost_work_total_list', $cost_work_total_list);
$smarty->assign('sales_work_list', $sales_work_list);
$smarty->assign('sales_work_total_list', $sales_work_total_list);
$smarty->assign('comm_g1_title_list', $comm_g1_title_list);
$smarty->assign('comm_g2_title_list', $comm_g2_title_list);
$smarty->assign('work_g1_title_list', $work_g1_title_list);
$smarty->assign('work_g2_title_list', $work_g2_title_list);
$smarty->assign('report_empty_check_list', $report_empty_check_list);

$smarty->display('media_commerce_month_report.html');
?>
