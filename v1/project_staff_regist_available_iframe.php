<?php
require('inc/common.php');
require('ckadmin.php');

# 접근 권한
$permission_team_val = getTeamWhere($my_db, "00251");
$permission_team_list = !empty($permission_team_val) ? explode(',', $permission_team_val) : [];

if (!permissionNameCheck($session_permission, "대표") && !permissionNameCheck($session_permission, "마스터관리자") && !permissionNameCheck($session_permission, "재무관리자") && !in_array($session_team, $permission_team_list)) {
    $smarty->display('access_company_error.html');
    exit;
}

# 변수 설정
$process            = isset($_POST['process']) ? $_POST['process'] : "";
$list_url           = "project_staff_regist_available_iframe.php";
$team_staff_list    = [];
$project_staff_list = [];

# 프로세스 처리
if($process == 'project_input_staff')
{
    $pj_no      = isset($_POST['pj_no']) ? $_POST['pj_no'] : "";
    $pj_s_date  = isset($_POST['pj_s_date']) ? $_POST['pj_s_date'] : "";
    $pj_e_date  = isset($_POST['pj_e_date']) ? $_POST['pj_e_date'] : "";
    $pj_s_no    = isset($_POST['pj_s_no']) ? $_POST['pj_s_no'] : "";
    $search_url = isset($_POST['search_url']) ? $_POST['search_url'] : "";
    $my_c_no    = "";
    $regdate    = date('Y-m-d H:i:s');

    if(empty($pj_no)){
        alert("투입인력 처리에 실패했습니다. 프로젝트 번호가 없습니다.", "{$list_url}?{$search_url}");
    }

    if($pj_s_no){
        $pj_staff_sql   = "SELECT * FROM project_staff WHERE pj_s_no='{$pj_s_no}' LIMIT 1";
        $pj_staff_query = mysqli_query($my_db, $pj_staff_sql);
        $pj_staff       = mysqli_fetch_assoc($pj_staff_query);

        if(isset($pj_staff['pj_s_no'])){
            $my_c_no = $pj_staff['my_c_no'];
        }
    }

    if(!empty($my_c_no) &&!empty($pj_s_no)){
        $ins_sql = "INSERT INTO `project_input_report` SET active='1', pj_no='{$pj_no}', pj_s_no='{$pj_s_no}', my_c_no='{$my_c_no}', regdate='{$regdate}', pir_s_date=(SELECT p.pj_s_date FROM project p WHERE p.pj_no='{$pj_no}' LIMIT 1), pir_e_date=(SELECT p.pj_e_date FROM project p WHERE p.pj_no='{$pj_no}' LIMIT 1)";

        if(!mysqli_query($my_db, $ins_sql)){
            alert("투입인력에 처리에 실패했습니다", "{$list_url}?{$search_url}");
        }else{
            goto_url("{$list_url}?{$search_url}");
        }
    }else{
        alert("투입인력 처리에 실패했습니다", "{$list_url}?{$search_url}");
    }
}
else
{
    $pj_no          = isset($_GET['pj_no']) ? $_GET['pj_no'] : "";
    $pj_s_date      = isset($_GET['pj_s_date']) ? $_GET['pj_s_date'] : "";
    $pj_e_date      = isset($_GET['pj_e_date']) ? $_GET['pj_e_date'] : "";
    $sch_s_name     = isset($_GET['sch_s_name']) ? $_GET['sch_s_name'] : "";
    $sch_info       = isset($_GET['sch_info']) ? $_GET['sch_info'] : "";

    $add_pir_where = "";

    if (!empty($pj_s_date) && !empty($pj_e_date)) {
        $add_pir_where .= " AND ((pir_s_date BETWEEN '{$pj_s_date}' AND '{$pj_e_date}' OR pir_e_date BETWEEN '{$pj_s_date}' AND '{$pj_e_date}') OR (pir_s_date <= '{$pj_s_date}' AND pir_e_date >= '{$pj_e_date}'))";
        $smarty->assign('pj_s_date', $pj_s_date);
        $smarty->assign('pj_e_date', $pj_e_date);
    }

    $add_where = "ps.active='1' AND ps.pj_s_no NOT IN (SELECT pir.pj_s_no FROM project_input_report pir WHERE pir.pj_no = '{$pj_no}' AND pir.active='1')";

    if (!empty($sch_s_name)) {
        $add_where .= " AND ps.s_name like '%{$sch_s_name}%'";
        $smarty->assign('sch_s_name', $sch_s_name);
    }

    if (!empty($sch_info)) {
        $add_where .= " AND ps.info like '%{$sch_info}%'";
        $smarty->assign('sch_info', $sch_info);
    }

    // 전체 게시물 수
    $pj_total_sql		= "SELECT count(pj_s_no) FROM (SELECT `ps`.pj_s_no FROM project_staff `ps` WHERE {$add_where}) AS cnt";
    $pj_total_query	= mysqli_query($my_db, $pj_total_sql);
    if(!!$pj_total_query)
        $pj_total_result = mysqli_fetch_array($pj_total_query);

    $pj_staff_total = $pj_total_result[0];

    $pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
    $num 		= 10;
    $offset 	= ($pages-1) * $num;
    $pagenum 	= ceil($pj_staff_total/$num);

    if ($pages >= $pagenum){$pages = $pagenum;}
    if ($pages <= 0){$pages = 1;}

    $search_url = getenv("QUERY_STRING");
    $page		= pagelist($pages, "project_staff_regist_available_iframe.php", $pagenum, $search_url);
    $smarty->assign("search_url", $search_url);
    $smarty->assign("total_num", $pj_staff_total);
    $smarty->assign("pagelist", $page);

    $project_staff_sql = "
        SELECT 
            *,
            (SELECT mc.c_name FROM my_company mc WHERE mc.my_c_no=ps.my_c_no) as my_c_name,
            (SELECT SUM(pir.resource) as resource FROM project_input_report pir WHERE pir.pj_no IN (SELECT p.pj_no FROM project p WHERE p.display='1' AND p.state='2') AND pir.pj_s_no = ps.pj_s_no AND pir.active='1' {$add_pir_where}) as confirm_use_resource,
            (SELECT SUM(pir.resource) as resource FROM project_input_report pir WHERE pir.pj_no IN (SELECT p.pj_no FROM project p WHERE p.display='1' AND p.state IN ('1','2')) AND pir.pj_s_no = ps.pj_s_no AND pir.active='1' {$add_pir_where}) as total_use_resource
        FROM project_staff `ps` 
        WHERE {$add_where}
        ORDER BY s_name ASC
        LIMIT {$offset}, {$num}
    ";

    $project_staff_query = mysqli_query($my_db, $project_staff_sql);
    $project_staff_list = [];

    while ($project_staff = mysqli_fetch_assoc($project_staff_query)) {
        $confirm_use_resource   = isset($project_staff['confirm_use_resource']) && !empty($project_staff['confirm_use_resource']) ? $project_staff['confirm_use_resource'] : 0;
        $total_use_resource     = isset($project_staff['total_use_resource']) && !empty($project_staff['total_use_resource']) ? $project_staff['total_use_resource'] : 0;
        $project_staff['confirm_resource'] = ($confirm_use_resource > 0) ? (100-$confirm_use_resource) : 100;
        $project_staff['total_resource'] = ($total_use_resource > 0) ? (100-$total_use_resource) : 100;

        $project_staff_list[] = $project_staff;
    }
}

$smarty->assign('pj_no', $pj_no);
$smarty->assign('pj_s_date', $pj_s_date);
$smarty->assign('pj_e_date', $pj_e_date);
$smarty->assign("project_staff_list", $project_staff_list);

$smarty->display('project_staff_regist_available_iframe.html');
?>
