<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/deposit.php');
require('inc/helper/work.php');
require('inc/model/Corporation.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

$lfcr = chr(10) ;
//상단타이틀
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A1', "no")
	->setCellValue('B1', "진행상태")
	->setCellValue('C1', "업로드일{$lfcr}업로드 담당자")
	->setCellValue('D1', "계좌명")
	->setCellValue('E1', "입금 수집처")
	->setCellValue('F1', "입금일자")
	->setCellValue('G1', "구분")
    ->setCellValue('H1', "계좌명")
	->setCellValue('I1', "상대은행")
	->setCellValue('J1', "입급처(입금자명)")
	->setCellValue('K1', "적요")
	->setCellValue('L1', "금액")
    ->setCellValue('M1', "작성자{$lfcr}(입금자명/입금완료액)")
	->setCellValue('N1', "dp_no")
	->setCellValue('O1', "업체명/브랜드명")
	->setCellValue('P1', "비고")
	->setCellValue('Q1', "입금 확인일{$lfcr}처리자")
;


# 검색조건
$today_val  = date('Y-m-d');
$days_val   = date('Y-m-d', strtotime('-3 days'));
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));

$add_where          = "1=1";
$sch_dp_s_date      = isset($_GET['sch_dp_s_date']) ? $_GET['sch_dp_s_date'] : $today_val;
$sch_dp_e_date      = isset($_GET['sch_dp_e_date']) ? $_GET['sch_dp_e_date'] : $today_val;
$sch_dp_date_type   = isset($_GET['sch_dp_date_type']) ? $_GET['sch_dp_date_type'] : "today";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_work_state     = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "";
$sch_dp_account     = isset($_GET['sch_dp_account']) ? $_GET['sch_dp_account'] : "";
$sch_bk_name        = isset($_GET['sch_bk_name']) ? $_GET['sch_bk_name'] : "";
$sch_price          = isset($_GET['sch_price']) ? $_GET['sch_price'] : "";
$sch_dp_no          = isset($_GET['sch_dp_no']) ? $_GET['sch_dp_no'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "";
$ori_ord_type       = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "";

if(!empty($sch_dp_s_date) || !empty($sch_dp_e_date))
{
	if(!empty($sch_dp_s_date)){
		$add_where .= " AND `dpc`.dp_date >= '{$sch_dp_s_date}'";
	}

	if(!empty($sch_dp_e_date)){
		$add_where .= " AND `dpc`.dp_date <= '{$sch_dp_e_date}'";
	}
}

if(!empty($sch_no)){
	$add_where   .= " AND `dpc`.dpc_no = '{$sch_no}'";
}

if(!empty($sch_work_state)){
	$add_where   .= " AND `dpc`.work_state = '{$sch_work_state}'";
}

if(!empty($sch_dp_account)){
	$add_where   .= " AND `dpc`.dp_account = '{$sch_dp_account}'";
}

if(!empty($sch_bk_name)){
	$add_where   .= " AND `dpc`.bk_name LIKE '%{$sch_bk_name}%'";
}

if(!empty($sch_price)){
	$add_where   .= " AND `dpc`.price = '{$sch_price}'";
}

if(!empty($sch_dp_no)) {
	if ($sch_dp_no == '1') {
		$add_where .= " AND `dpc`.dp_no > 0";
	} elseif ($sch_dp_no == '2') {
		$add_where .= " AND `dpc`.dp_no=0";
	}
}


$add_order_by = "";
$ord_type_by  = "";
$order_by_val = "";
if(!empty($ord_type))
{
	$ord_type_by = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

	if(!empty($ord_type_by))
	{
		$ord_type_list = [];
		if($ord_type == 'dp_date'){
			$ord_type_list[] = "dp_date";
		}

		if($ori_ord_type == $ord_type)
		{
			if($ord_type_by == '1'){
				$order_by_val = "ASC";
			}elseif($ord_type_by == '2'){
				$order_by_val = "DESC";
			}
		}else{
			$ord_type_by  = '2';
			$order_by_val = "DESC";
		}

		foreach($ord_type_list as $ord_type_val){
			$add_order_by .= "{$ord_type_val} {$order_by_val}, ";
		}
	}
}
$add_order_by .= "dpc_no DESC";

# 쿼리
$deposit_confirm_sql = "
    SELECT 
        *,
        DATE_FORMAT(`dpc`.regdate, '%Y-%m-%d') as reg_day,
        (SELECT name FROM corp_account s WHERE s.co_no=`dpc`.dp_account) AS dp_account_name,
        (SELECT s_name FROM staff s WHERE s.s_no=`dpc`.reg_s_no) AS reg_name,
        (SELECT s_name FROM staff s WHERE s.s_no=`dpc`.run_s_no) AS run_name
    FROM deposit_confirm `dpc`
    WHERE {$add_where}
    ORDER BY {$add_order_by}
";
$deposit_confirm_query 	= mysqli_query($my_db, $deposit_confirm_sql);
$corp_model 			= Corporation::Factory();
$work_state_option		= getWorkStateOption();
$dp_account_option		= $corp_model->getDpAccountList();
$dpc_company_option		= getDpcCompanyOption();
$is_deposit_option		= getIsDepositOption();

$idx = 2;
if(!!$deposit_confirm_query)
{
    while($deposit_confirm = mysqli_fetch_array($deposit_confirm_query))
    {
		$deposit_list = "";
		if($deposit_confirm['work_state'] == '3' && $deposit_confirm['dp_no'] == 0)
		{
			$deposit_sql    = "SELECT dp_no, reg_s_no, (SELECT s.s_name FROM staff s WHERE s.s_no=dp.reg_s_no) as reg_name, bk_name, cost FROM deposit dp WHERE (confirm_no IS NULL OR confirm_no = '' OR confirm_no = '0') AND display='1' AND dp_state='1' AND bk_name='{$deposit_confirm['bk_name']}' AND cost='{$deposit_confirm['price']}'";
			$deposit_query  = mysqli_query($my_db, $deposit_sql);
			while($deposit_result = mysqli_fetch_array($deposit_query)){
				$dp_text  = $deposit_result['reg_name']."({$deposit_result['bk_name']} / ".number_format($deposit_result['cost']).")";
				$deposit_list .= !empty($deposit_list) ? $lfcr.$dp_text : $dp_text;
			}
		}

		$deposit_confirm['work_state_name'] = $work_state_option[$deposit_confirm['work_state']];
		$deposit_confirm['run_text']		= !empty($deposit_confirm['run_date']) ? $deposit_confirm['run_date'].$lfcr."({$deposit_confirm['run_name']})" : "";
		$deposit_confirm['dp_text']			= $deposit_list;

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$idx}", $deposit_confirm['dpc_no'])
            ->setCellValue("B{$idx}", $deposit_confirm['work_state_name'])
            ->setCellValue("C{$idx}", $deposit_confirm['reg_day'].$lfcr.$deposit_confirm['reg_name'])
            ->setCellValue("D{$idx}", $deposit_confirm['dp_account_name'])
            ->setCellValue("E{$idx}", $deposit_confirm['dpc_company'])
            ->setCellValue("F{$idx}", $deposit_confirm['dp_date'])
            ->setCellValue("G{$idx}", $deposit_confirm['dpc_type'])
            ->setCellValue("H{$idx}", $deposit_confirm['acc_name'])
            ->setCellValue("I{$idx}", $deposit_confirm['bk_title'])
            ->setCellValue("J{$idx}", $deposit_confirm['bk_name'])
			->setCellValue("K{$idx}", $deposit_confirm['juck_yo'])
			->setCellValue("L{$idx}", $deposit_confirm['price'])
			->setCellValue("M{$idx}", $deposit_confirm['dp_text'])
			->setCellValue("N{$idx}", $deposit_confirm['dp_no'])
			->setCellValue("O{$idx}", $deposit_confirm['c_name'])
			->setCellValue("P{$idx}", $deposit_confirm['notice'])
			->setCellValue("Q{$idx}", $deposit_confirm['run_text'])
		;
		$idx++;
    }
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00343a40');
$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A2:Q{$idx}")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("A1:Q{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:Q{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("H2:H{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("I2:I{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("J2:J{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


$objPHPExcel->getActiveSheet()->getStyle("L2:L{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("C1:C{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("M1:M{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("N2:N{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("O2:O{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("P2:P{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("Q1:Q{$idx}")->getAlignment()->setWrapText(true);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);

$objPHPExcel->getActiveSheet()->getStyle("A2:Q{$idx}")->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->setTitle('입금확인 리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_입금확인 리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

//header('Content-Disposition: attachment;filename="popup_posting_list.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
