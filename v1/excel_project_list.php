<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/project.php');
require('Classes/PHPExcel.php');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
->setLastModifiedBy("Maarten Balliauw")
->setTitle("Office 2007 XLSX Test Document")
->setSubject("Office 2007 XLSX Test Document")
->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
->setKeywords("office 2007 openxml php")
->setCategory("Test result file");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => '00000000'),
			),
	),
);

$fontbold = array(
	'font' => array(
		'bold' => true,
	),
);

$lfcr = chr(10) ;

$work_sheet = $objPHPExcel->setActiveSheetIndex(0);
$work_sheet->mergeCells("A1:A2");
$work_sheet->mergeCells("B1:B2");
$work_sheet->mergeCells("C1:C2");
$work_sheet->mergeCells("D1:D2");
$work_sheet->mergeCells("E1:E2");
$work_sheet->mergeCells("F1:G1");
$work_sheet->mergeCells("H1:H2");
$work_sheet->mergeCells("I1:I2");
$work_sheet->mergeCells("J1:J2");
$work_sheet->mergeCells("K1:K2");

$work_sheet
	->setCellValue('A1', "내부통용사업명")
	->setCellValue('B1', "수요기관")
	->setCellValue('C1', "사업개요")
	->setCellValue('D1', "계약형태")
	->setCellValue('E1', "예상매출액")
	->setCellValue('F1', "사업기간")
	->setCellValue('F2', "시작")
	->setCellValue('G2', "종료")
    ->setCellValue('H1', "담당자")
	->setCellValue('I1', "참가자 만족도")
	->setCellValue('J1', "참여 인원")
	->setCellValue('K1', "주요성과")
;

# 검색조건
$add_where          = "`p`.display = '1'";
$sch_pj_no          = isset($_GET['sch_pj_no']) ? $_GET['sch_pj_no'] : "";
$sch_work_state     = isset($_GET['sch_work_state']) ? $_GET['sch_work_state'] : "5";
$sch_my_c_no        = isset($_GET['sch_my_c_no']) ? $_GET['sch_my_c_no'] : "";
$sch_pj_name        = isset($_GET['sch_pj_name']) ? $_GET['sch_pj_name'] : "";
$sch_contract_type  = isset($_GET['sch_contract_type']) ? $_GET['sch_contract_type'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_team           = isset($_GET['sch_team']) ? $_GET['sch_team'] : "";
$sch_manager        = isset($_GET['sch_manager']) ? $_GET['sch_manager'] : "";
$sch_resource_type  = isset($_GET['sch_resource_type']) ? $_GET['sch_resource_type'] : "";
$sch_cal_state      = isset($_GET['sch_cal_state']) ? $_GET['sch_cal_state'] : "";
$sch_achievements   = isset($_GET['sch_achievements']) ? $_GET['sch_achievements'] : "";
$ord_type 		    = isset($_GET['ord_type']) ? $_GET['ord_type'] : "pj_e_date";
$ori_ord_type       = isset($_GET['ori_ord_type']) ? $_GET['ori_ord_type'] : "pj_e_date";
$ord_type_by        = isset($_GET['ord_type_by']) ? $_GET['ord_type_by'] : "2";

#검색 조건 없을때 & session_team 검색결과 없을 시 전체결과 노출
$url_check_str  = $_SERVER['QUERY_STRING'];
$url_chk_result = false;
if(empty($url_check_str)){
	$url_check_team_where   = getTeamWhere($my_db, $session_team);
	$url_check_sql          = "SELECT count(pj_no) as cnt FROM project WHERE `team` IN ({$url_check_team_where})";
	$url_check_query  		= mysqli_query($my_db, $url_check_sql);
	$url_check_result 		= mysqli_fetch_assoc($url_check_query);

	if($url_check_result['cnt'] == 0){
		$sch_team       = "all";
		$url_chk_result = true;
	}
}

if(!empty($sch_pj_no)) {
	$add_where .= " AND p.pj_no = '{$sch_pj_no}'";
}

if(!empty($sch_work_state)) {
	$add_where .= " AND p.work_state = '{$sch_work_state}'";
}

if(!empty($sch_my_c_no)) {
	$add_where .= " AND p.my_c_no = '{$sch_my_c_no}'";
}

if(!empty($sch_pj_name)) {
	$add_where .= " AND p.pj_name like '%{$sch_pj_name}%'";
}

if(!empty($sch_contract_type)) {
	$add_where .= " AND p.contract_type = '{$sch_contract_type}'";
}

if(!empty($sch_state)) {
	$add_where .= " AND p.state = '{$sch_state}'";
}

if (!empty($sch_team))
{
	if($sch_team != 'all')
	{
		$sch_team_code_where = getTeamWhere($my_db, $sch_team);
		if($sch_team_code_where){
			$add_where .= " AND `p`.team IN ({$sch_team_code_where})";
		}
	}
}else{

	$sch_team_code_where = getTeamWhere($my_db, $session_team);
	if($sch_team_code_where){
		$add_where .= " AND `p`.team IN ({$sch_team_code_where})";
	}
}

if(!empty($sch_manager))
{
	$add_where .= " AND p.manager IN(SELECT s.s_no FROM staff s WHERE s.s_name like '%{$sch_manager}%')";
}

if(!empty($sch_resource_type))
{

	$resource_where_list = [];
	foreach($sch_resource_type as $sch_resource){
		$resource_where_list[] = "p.resource_type LIKE '%{$sch_resource}%'";
	}

	$resource_where = implode(" OR ", $resource_where_list);
	$add_where .= " AND ({$resource_where})";
}

if(!empty($sch_cal_state))
{
	$add_where .= " AND p.cal_state = '{$sch_cal_state}'";
}

if(!empty($sch_achievements))
{
	$add_where .= " AND p.achievements LIKE '%{$sch_achievements}%'";
}

$add_orderby = "";

if($ori_ord_type != $ord_type && empty($ord_type_by)){
	$ord_type_by = "2";
}

if(!empty($ord_type_by))
{
	$ord_type_list      = [];
	$ord_type_list[]    = $ord_type;
	$orderby_val        = "";

	if($ord_type_by == '1'){
		$orderby_val = "ASC";
	}elseif($ord_type_by == '2'){
		$orderby_val = "DESC";
	}

	foreach($ord_type_list as $ord_type_val){
		$add_orderby .= "{$ord_type_val} {$orderby_val}, ";
	}
}
$add_orderby .= "pj_no DESC";

# 프로젝트 쿼리
$project_sql = "
    SELECT 
        *,
        (SELECT s.s_name FROM staff s WHERE s.s_no = p.manager) as manager_name
    FROM project `p` 
    WHERE {$add_where} 
    ORDER BY {$add_orderby}
 ";
$project_query 			= mysqli_query($my_db, $project_sql);
$idx 					= 3;
$contract_type_option   = getContractTypeOption();
while($project = mysqli_fetch_assoc($project_query))
{
	$contract_type_name = isset($contract_type_option[$project['contract_type']]) ? $contract_type_option[$project['contract_type']] : "";
	$work_sheet
		->setCellValue("A{$idx}", $project["pj_name"])
		->setCellValue("B{$idx}", $project["agency"])
		->setCellValue("C{$idx}", $project["description"])
		->setCellValue("D{$idx}", $contract_type_name)
		->setCellValue("E{$idx}", $project["expected_sales"])
		->setCellValue("F{$idx}", $project["pj_s_date"])
		->setCellValue("G{$idx}", $project["pj_e_date"])
		->setCellValue("H{$idx}", $project["manager_name"])
		->setCellValue("I{$idx}", $project["satisfaction"])
		->setCellValue("J{$idx}", $project["participants"])
		->setCellValue("K{$idx}", $project["achievements"])
	;
	$idx++;
}
$idx--;

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle('A1:K2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00343a40');
$objPHPExcel->getActiveSheet()->getStyle("A1:K2")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle('A1:K2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:K2')->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A3:K{$idx}")->getFont()->setSize(8);
$objPHPExcel->getActiveSheet()->getStyle("A1:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:K{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle("A3:A{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("B3:B{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("C3:C{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("K3:K{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objPHPExcel->getActiveSheet()->getStyle("E3:E{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

$objPHPExcel->getActiveSheet()->getStyle("C3:C{$idx}")->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle("K3:K{$idx}")->getAlignment()->setWrapText(true);

// Rename worksheet  set the width autosize
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);

$objPHPExcel->getActiveSheet()->getStyle("A2:K{$idx}")->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->setTitle('사업리스트');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$excel_filename=iconv('UTF-8','EUC-KR',date("Ymd")."_사업리스트.xlsx");
header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.$excel_filename);

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

?>
