<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/product_cms_stock.php');
require('inc/model/Kind.php');

# 브랜드 리스트
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

# 검색
$add_where          = "1=1 AND log_subject IN(1,2,3,4,5,9) AND confirm_state='10'";
$sch_move_mon       = isset($_GET['sch_move_mon']) ? $_GET['sch_move_mon'] : date('Y-m', strtotime('-1 months'));
$sch_move_s_date    = $sch_move_mon."-01";
$sch_move_end_day   = date("t", strtotime($sch_move_mon));
$sch_move_e_date    = $sch_move_mon."-{$sch_move_end_day}";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_move_mon))
{
    $add_where  .=  " AND pcsr.regdate BETWEEN '{$sch_move_s_date}' AND '{$sch_move_e_date}'";
    $smarty->assign("sch_move_mon", $sch_move_mon);
}

# 브랜드 옵션
if(!empty($sch_brand))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `pcu`.brand = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `pcu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where         .= " AND `pcu`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 입고/반출 리스트
$stock_report_list        = [];
$stock_report_total_list  = array("1" => 0, "2" => 0, "3" => 0, "4" => 0, "5" => 0, "9" => 0, "1_price" => 0, "2_price" => 0, "3_price" => 0, "4_price" => 0, "5_price" => 0, "9_price" => 0,"total" => 0);
$stock_report_sql   = "
    SELECT 
        pcsr.option_name,
        pcu.brand,
        pcsr.sku,
        pcsr.prd_unit,
        (SELECT org_price FROM product_cms_stock_confirm pcsc WHERE base_type='start' AND pcsc.prd_unit=pcsr.prd_unit AND base_mon='{$sch_move_mon}' LIMIT 1) as unit_price,
        pcsr.log_subject,
        SUM(pcsr.stock_qty) as total_qty
    FROM product_cms_stock_report pcsr
    LEFT JOIN product_cms_unit pcu ON pcu.`no`=pcsr.prd_unit
    WHERE {$add_where}
    GROUP BY pcsr.prd_unit, pcsr.log_subject
    ORDER BY pcsr.sku
";
$stock_report_query = mysqli_query($my_db, $stock_report_sql);
while($stock_report = mysqli_fetch_assoc($stock_report_query))
{
    if(!isset($stock_report_list[$stock_report['prd_unit']])){
        $stock_report_list[$stock_report['prd_unit']] = array(
            "name"          => $stock_report['option_name'],
            "sku"           => $stock_report['sku'],
            "prd_unit"      => $stock_report['prd_unit'],
            "price"         => $stock_report['unit_price'],
            "1"             => 0,
            "1_price"       => 0,
            "2"             => 0,
            "2_price"       => 0,
            "3"             => 0,
            "3_price"       => 0,
            "4"             => 0,
            "4_price"       => 0,
            "5"             => 0,
            "5_price"       => 0,
            "9"             => 0,
            "9_price"       => 0,
            "total"         => 0,
            "total_price"   => 0
        );
    }

    $price_name     = $stock_report['log_subject']."_price";
    $total_qty      = $stock_report['total_qty']*-1;
    $unit_price     = $stock_report_list[$stock_report['prd_unit']]['price'];
    $total_price    = $total_qty*$unit_price;


    $stock_report_list[$stock_report['prd_unit']][$stock_report['log_subject']] += $total_qty;
    $stock_report_list[$stock_report['prd_unit']][$price_name] += $total_price;
    $stock_report_list[$stock_report['prd_unit']]["total"] += $total_qty;
    $stock_report_list[$stock_report['prd_unit']]["total_price"] += $total_price;

    $stock_report_total_list[$stock_report['log_subject']] += $total_qty;
    $stock_report_total_list[$price_name]   += $total_price;
    $stock_report_total_list["total"]       += $total_qty;
    $stock_report_total_list["total_price"] += $total_price;
}

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

$smarty->assign("sch_move_s_date", $sch_move_s_date);
$smarty->assign("sch_move_e_date", $sch_move_e_date);
$smarty->assign("stock_report_list", $stock_report_list);
$smarty->assign("stock_report_total_list", $stock_report_total_list);

$smarty->display('product_cms_stock_other_list.html');
?>
