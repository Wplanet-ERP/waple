<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/wise_csm.php');
require('inc/model/Staff.php');

# 날짜별 검색
$today_s_w		 = date('w')-1;
$sch_date_type   = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "1";
$sch_date        = isset($_GET['sch_date']) ? $_GET['sch_date'] : date('Y-m');
$sch_s_month     = isset($_GET['sch_s_month']) ? $_GET['sch_s_month'] : date('Y-m');
$sch_e_month     = isset($_GET['sch_e_month']) ? $_GET['sch_e_month'] : date('Y-m');
$sch_s_week      = isset($_GET['sch_s_week']) ? $_GET['sch_s_week'] : date('Y-m-d',strtotime("-{$today_s_w} day"));
$sch_e_week      = isset($_GET['sch_e_week']) ? $_GET['sch_e_week'] : date("Y-m-d",strtotime("{$sch_s_week} +6 day"));
$sch_s_date      = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d',strtotime("-10 day"));
$sch_e_date      = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_date', $sch_date);
$smarty->assign('sch_s_month', $sch_s_month);
$smarty->assign('sch_e_month', $sch_e_month);
$smarty->assign('sch_s_week', $sch_s_week);
$smarty->assign('sch_e_week', $sch_e_week);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);

# 조건
$add_where          = "1=1 AND display='1' AND s_no > 0";
$sch_ev_type        = isset($_GET['sch_ev_type']) ? $_GET['sch_ev_type'] : "1";
$sch_question_type  = isset($_GET['sch_question_type']) ? $_GET['sch_question_type'] : "1";
$sch_s_no           = isset($_GET['sch_s_no']) ? $_GET['sch_s_no'] : "";
$staff_model        = Staff::Factory();
$cs_staff_list      = $staff_model->getActiveTeamStaff("00244");
$cs_staff_list['309'] = "이채하";
$cs_staff_list[10000] = "100번";
$cs_staff_list[10001] = "101번";
$cs_staff_list[10002] = "102번";

if(!empty($sch_s_no))
{
    $add_where  .= " AND s_no='{$sch_s_no}'";
    $picker_list[$sch_s_no] = $cs_staff_list[$sch_s_no];
    $smarty->assign('sch_s_no', $sch_s_no);
}else{
    $picker_list = $cs_staff_list;
}

if(!empty($sch_ev_type))
{
    if($sch_ev_type == '2'){
        $add_where .= " AND type='1'";
    }elseif($sch_ev_type == '3'){
        $add_where .= " AND type='2'";
    }

    $smarty->assign('sch_ev_type', $sch_ev_type);
}

$add_score_column = "ROUND(AVG((ce.q1_score+ce.q2_score+ce.q3_score+ce.q4_score)/4),2)";
if(!empty($sch_question_type))
{
    switch($sch_question_type)
    {
        case 1:
            $add_score_column = "ROUND(AVG((ce.q1_score+ce.q2_score+ce.q3_score+ce.q4_score)/4),2)";
            break;
        case 2:
            $add_score_column = "ROUND(AVG(ce.q4_score),2)";
            break;
        case 3:
            $add_score_column = "ROUND(AVG(ce.q4_score),2)";
            break;
        case 4:
            $add_score_column = "ROUND(AVG(ce.q4_score),2)";
            break;
        case 5:
            $add_score_column = "ROUND(AVG(ce.q4_score),2)";
            break;
        default:

            break;
    }
    $smarty->assign('sch_question_type', $sch_question_type);
}

# CHART
# 전체 기간 조회 및 누적데이터 조회
$all_date_where   = "";
$all_date_key	  = "";
$add_date_where   = "";
$add_date_column  = "";
$stats_list       = [];

if($sch_date_type == '3') //월간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";

    $add_date_where  = " AND (DATE_FORMAT(ce.regdate, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}') ";
    $add_date_column = "DATE_FORMAT(ce.regdate, '%Y%m')";
}
elseif($sch_date_type == '2') //주간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}')";
    $all_date_key 	 = "DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%Y%m%d')";
    $all_date_title  = "CONCAT(DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-2) DAY), '%m/%d'),'~', DATE_FORMAT(DATE_SUB(`allday`.Date, INTERVAL(IF(DAYOFWEEK(`allday`.Date)=1,8,DAYOFWEEK(`allday`.Date))-8) DAY), '%m/%d'))";

    $add_date_where  = " AND (DATE_FORMAT(ce.regdate, '%Y-%m-%d') BETWEEN '{$sch_s_week}' AND '{$sch_e_week}') ";
    $add_date_column = "DATE_FORMAT(DATE_SUB(ce.regdate, INTERVAL(IF(DAYOFWEEK(ce.regdate)=1,8,DAYOFWEEK(ce.regdate))-2) DAY), '%Y%m%d')";
}
else //일간
{
    $all_date_where  = "(DATE_FORMAT(`allday`.Date, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '$sch_e_date')";
    $all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m%d')";
    $all_date_title  = "DATE_FORMAT(`allday`.Date, '%m/%d_%w')";

    $add_date_where  = " AND (DATE_FORMAT(ce.regdate, '%Y-%m-%d') BETWEEN '{$sch_s_date}' AND '$sch_e_date') ";
    $add_date_column = "DATE_FORMAT(ce.regdate, '%Y%m%d')";
}

$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
 		DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e) 
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key, date_key
";

# 기본 STAT 리스트 Init 및 x key 및 타이틀 설정
$x_label_list = [];
$x_table_th_list = [];
$stats_list = [];
$stats_date_avg_list = [];
$stats_staff_avg_list = [];
if($all_date_where != ''){
    $all_date_query = mysqli_query($my_db, $all_date_sql);
    $date_w = array('일','월','화','수','목','금','토');
    while($date = mysqli_fetch_array($all_date_query))
    {
        $chart_title = $date['chart_title'];

        if($sch_date_type == '1'){
            $date_convert = explode('_', $chart_title);
            $date_name = isset($date_convert[1]) ? $date_w[$date_convert[1]] : "";
            $chart_title = ($date_name) ? $date_convert[0]."({$date_name})" : $date_convert[0];
        }

        $x_label_list[$date['chart_key']]    = "'".$chart_title."'";
        $x_table_th_list[$date['chart_key']] = $chart_title;

        foreach($picker_list as $s_no => $label){
            $stats_list[$date['chart_key']][$s_no] = array(
                "score" => 0,
                "count" => 0
            );
            $stats_date_avg_list[$date['chart_key']] = array('result' => 0, 'cnt' => 0);
            $stats_staff_avg_list[$s_no] = array('result' => 0, 'cnt' => 0);
        }
    }
}

# Y 컬럼 Label
$y_label_list   = [];
$y_label_title  = [];
$question_type_option = getQuestionTypeOption();
foreach($picker_list as $s_no => $s_name)
{
    $question_label = $question_type_option[$sch_question_type];

    $y_label_list[$s_no]  = $s_name;
    $y_label_title[$s_no] = $question_label;
    $y_label_title['all'] = "합산";
}

$stats_sql = "
    SELECT
        {$add_date_column} as key_date,
        ce.s_no,
        {$add_score_column} as result_data,
        count(ce.csm_e_no) as result_cnt
    FROM `csm_evaluation` as ce
    WHERE {$add_where} 
      {$add_date_where}
    GROUP BY key_date, ce.s_no
    ORDER BY key_date ASC
";
$stats_query = mysqli_query($my_db, $stats_sql);
while($stats = mysqli_fetch_assoc($stats_query))
{
    $stats_list[$stats['key_date']][$stats['s_no']] = array(
        "score" => $stats['result_data'],
        "count" => $stats['result_cnt']
    );

    $stats_date_avg_list[$stats['key_date']]['result']  += ($stats['result_data']*$stats['result_cnt']);
    $stats_date_avg_list[$stats['key_date']]['cnt']     += $stats['result_cnt'];
    $stats_staff_avg_list[$stats['s_no']]['result']     += ($stats['result_data']*$stats['result_cnt']);
    $stats_staff_avg_list[$stats['s_no']]['cnt']        += $stats['result_cnt'];
}

$full_data      = [];
$full_avg_data  = [];
foreach($stats_list as $sales_date => $stats_ev_data)
{
    $date_avg = 0;
    if($stats_ev_data)
    {
        foreach ($stats_ev_data as $key => $ev_data)
        {
            $full_data["each"]["line"][$key]['title']  = $picker_list[$key];
            $full_data["each"]["line"][$key]['data'][] = $ev_data['score'];
            $full_data["each"]["line"][$key]['cnt'][]  = $ev_data['count'];

            $full_data["each"]["bar"][$key]['title']  = $picker_list[$key];
            $full_data["each"]["bar"][$key]['data'][] = $ev_data['score'];
            $full_data["each"]["bar"][$key]['cnt'][]  = $ev_data['count'];

            $staff_avg = 0;
            if(isset($stats_staff_avg_list[$key]['result']) && $stats_staff_avg_list[$key]['result'] > 0)
            {
                $staff_avg = ROUND(($stats_staff_avg_list[$key]['result']/$stats_staff_avg_list[$key]['cnt']),2);
            }
            $full_avg_data["staff"][$key] = $staff_avg;
        }
    }

    if(isset($stats_date_avg_list[$sales_date]['result']) && $stats_date_avg_list[$sales_date]['result'] > 0)
    {
        $date_avg = ROUND(($stats_date_avg_list[$sales_date]['result']/$stats_date_avg_list[$sales_date]['cnt']),2);
    }

    $full_data["sum"]["line"]["all"]['title']  = "합산";
    $full_data["sum"]["line"]["all"]['data'][] = $date_avg;
    $full_data["sum"]["line"]["all"]['cnt'][]  = $stats_date_avg_list[$sales_date]['cnt'];
    $full_data["sum"]["bar"]["all"]['title']   = "합산";
    $full_data["sum"]["bar"]["all"]['data'][]  = $date_avg;
    $full_data["sum"]["bar"]["all"]['cnt'][]   = $stats_date_avg_list[$sales_date]['cnt'];
    $full_avg_data["date"][$sales_date] = $date_avg;
}

$smarty->assign('cs_staff_list', $cs_staff_list);
$smarty->assign('evaluation_type_option', getEvaluationTypeOption());
$smarty->assign('question_type_option', $question_type_option);
$smarty->assign('x_label_list', implode(',', $x_label_list));
$smarty->assign('x_table_th_list', json_encode($x_table_th_list));
$smarty->assign('legend_list', json_encode($y_label_list));
$smarty->assign('y_label_title', json_encode($y_label_title));
$smarty->assign('picker_list', $picker_list);
$smarty->assign('full_data', json_encode($full_data));
$smarty->assign('full_avg_data', json_encode($full_avg_data));

$smarty->display('wise_csm_evaluation_chart.html');
?>
