<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_date.php');
require('inc/helper/logistics.php');
require('inc/model/ProductCmsUnit.php');
require('Classes/PHPExcel.php');

ini_set('error_reporting',E_ALL & ~E_NOTICE | E_STRICT);
date_Default_TimeZone_set("Asia/Seoul");	// 시간설정
define('ROOTPATH', dirname(__FILE__));

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '00000000'),
        ),
    ),
);

$lfcr       = chr(10) ;
$eng_abc    = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
$eng_abcc   = [];
$eng_list   = $eng_abc;
foreach($eng_abc as $a){
    foreach($eng_abc as $b){
        $eng_list[] = $a.$b;
        $eng_abcc[] = $a.$b;
    }
}

foreach($eng_abcc as $aa){
    foreach($eng_abc as $b){
        $eng_list[] = $aa.$b;
    }
}

# 검색조건
$add_where      = "1=1 AND lm.work_state='6'";
$main_option    = getStatsMainOption();

# 필터검색
$sch_stock_type     = isset($_GET['sch_stock_type']) ? $_GET['sch_stock_type'] : "all";
$sch_addr_filter    = isset($_GET['sch_addr_filter']) ? $_GET['sch_addr_filter'] : "";
$sch_addr_type      = isset($_GET['sch_addr_type']) ? $_GET['sch_addr_type'] : "";

if(!empty($sch_stock_type) && $sch_stock_type != "all")
{
    $add_where .= " AND lm.stock_type='{$sch_stock_type}'";
}

if(!empty($sch_addr_filter))
{
    if(!empty($sch_addr_type))
    {
        $add_where .= " AND lm.{$sch_addr_filter}_type='{$sch_addr_type}'";
    }
}else{
    $sch_addr_type = "";
}

# 조회기간
$sch_date_type  = isset($_GET['sch_date_type']) ? $_GET['sch_date_type'] : "set";
$sch_s_date     = isset($_GET['sch_s_date']) ? $_GET['sch_s_date'] : date('Y-m-d', strtotime("-1 month"));
$sch_e_date     = isset($_GET['sch_e_date']) ? $_GET['sch_e_date'] : date('Y-m-d');

$one_week_day  = date('Y-m-d', strtotime("-1 week"));
$two_week_day  = date('Y-m-d', strtotime("-2 week"));
$one_month_day = date('Y-m-d', strtotime("-1 month"));

$smarty->assign('sch_date_type', $sch_date_type);
$smarty->assign('sch_s_date', $sch_s_date);
$smarty->assign('sch_e_date', $sch_e_date);
$smarty->assign('one_week_day', $one_week_day);
$smarty->assign('two_week_day', $two_week_day);
$smarty->assign('one_month_day', $one_month_day);
$smarty->assign('main_option', $main_option);

if($sch_date_type == 'set'){
    $add_where .= " AND (lm.run_date BETWEEN '{$sch_s_date}' AND '{$sch_e_date}')";
}

# X축(day: 날짜, subject: 계정과목, unit: 구성품)
$sch_x_type         = isset($_GET['sch_x_type']) ? $_GET['sch_x_type'] : "day";
$sch_x_type_main    = isset($_GET['sch_x_type_main']) ? $_GET['sch_x_type_main'] : "";

if($sch_x_type == 'day' && $sch_x_type_main == ''){
    $sch_x_type_main = '1';
}

$type_day           = getDayChartOption();
$type_subject       = getSubjectNameOption();
$product_model      = ProductCmsUnit::Factory();
$type_unit          = $product_model->getLogisticsUnitList();

$sch_x_type_list    = getSchType($sch_x_type, $sch_x_type_main, $type_day, $type_subject, $type_unit);
$x_type_title       = $main_option[$sch_x_type];
$x_type_main_list   = $sch_x_type_list['type_main_list'];
$add_x_type_where   = $sch_x_type_list['add_type_where'];
$x_type_column      = $sch_x_type_list['add_type_column'];
$x_type_name_column = $sch_x_type_list['add_type_column_name'];

$add_x_type_column  = "{$x_type_column} as x_type";
if(!empty($x_type_name_column)){
    $add_x_type_name = "DISTINCT {$x_type_column} as x_type, {$x_type_name_column} as x_type_name";
}else{
    $add_x_type_name = "DISTINCT {$x_type_column} as x_type";
}


# Y축(day: 날짜, subject: 계정과목, unit: 구성품)
$sch_y_type         = isset($_GET['sch_y_type']) ? $_GET['sch_y_type'] : "subject";
$sch_y_type_main    = isset($_GET['sch_y_type_main']) ? $_GET['sch_y_type_main'] : "";

$sch_y_type_list    = getSchType($sch_y_type, $sch_y_type_main, $type_day, $type_subject, $type_unit);
$y_type_title       = $main_option[$sch_y_type];
$y_type_main_list   = $sch_y_type_list['type_main_list'];
$add_y_type_where   = $sch_y_type_list['add_type_where'];
$y_type_column      = $sch_y_type_list['add_type_column'];
$y_type_name_column = $sch_y_type_list['add_type_column_name'];

$add_y_type_column  = "{$y_type_column} as y_type";
if(!empty($y_type_name_column)){
    $add_y_type_name = "DISTINCT {$y_type_column} as y_type, {$y_type_name_column} as y_type_name";
}else{
    $add_y_type_name = "DISTINCT {$y_type_column} as y_type";
}


# Commerce List 초기화
$commerce_list      = [];
$commerce_sum_list  = [];

# X축 Name List
$x_name_list        = [];
$commerce_x_sql     = "
    SELECT
        {$add_x_type_name}
    FROM logistics_management lm
    LEFT JOIN (
        (SELECT lp.lp_no,lp.lm_no,pcr.option_no AS prd_no,lp.quantity*pcr.quantity AS quantity FROM logistics_product lp LEFT JOIN product_cms_relation pcr ON pcr.prd_no=lp.prd_no WHERE lp.prd_type = '1' AND pcr.display='1')
	    UNION
	    (SELECT lp.lp_no,lp.lm_no,lp.prd_no,lp.quantity FROM logistics_product lp WHERE lp.prd_type='2')
    ) lp ON lp.lm_no=lm.lm_no
    WHERE {$add_where}
        {$add_x_type_where}
        {$add_y_type_where}
    ORDER BY x_type ASC
";
$commerce_x_query = mysqli_query($my_db, $commerce_x_sql);
while($commerce_x = mysqli_fetch_assoc($commerce_x_query))
{
    $commerce_sum_list[$commerce_x['x_type']] = 0;
    if(!empty($x_type_name_column)){
        $x_name_list[$commerce_x['x_type']] = $commerce_x['x_type_name'];
    }else{
        $x_name_list[$commerce_x['x_type']] = $x_type_main_list[$commerce_x['x_type']];
    }
}

$y_name_list    = [];
$commerce_y_sql = "
    SELECT
        {$add_y_type_name}
    FROM logistics_management lm
    LEFT JOIN (
        (SELECT lp.lp_no,lp.lm_no,pcr.option_no AS prd_no,lp.quantity*pcr.quantity AS quantity FROM logistics_product lp LEFT JOIN product_cms_relation pcr ON pcr.prd_no=lp.prd_no WHERE lp.prd_type = '1' AND pcr.display='1')
	    UNION
	    (SELECT lp.lp_no,lp.lm_no,lp.prd_no,lp.quantity FROM logistics_product lp WHERE lp.prd_type='2')
    ) lp ON lp.lm_no=lm.lm_no
    WHERE {$add_where}
        {$add_x_type_where}
        {$add_y_type_where}
    GROUP BY y_type
";
$commerce_y_query = mysqli_query($my_db, $commerce_y_sql);
while($commerce_y = mysqli_fetch_assoc($commerce_y_query))
{
    foreach ($x_name_list as $x_type => $x_type_name) {
        $commerce_list[$commerce_y['y_type']][$x_type] = 0;
    }

    if(!empty($y_type_name_column)){
        $y_name_list[$commerce_y['y_type']] = $commerce_y['y_type_name'];
    }else{
        $y_name_list[$commerce_y['y_type']] = $y_type_main_list[$commerce_y['y_type']];
    }
}

$commerce_sql = "
    SELECT
        {$add_x_type_column},
        {$add_y_type_column},
	    lp.quantity as data_sum
    FROM logistics_management lm
    LEFT JOIN (
        (SELECT lp.lp_no,lp.lm_no,pcr.option_no AS prd_no,lp.quantity*pcr.quantity AS quantity FROM logistics_product lp LEFT JOIN product_cms_relation pcr ON pcr.prd_no=lp.prd_no WHERE lp.prd_type = '1' AND pcr.display='1')
	    UNION
	    (SELECT lp.lp_no,lp.lm_no,lp.prd_no,lp.quantity FROM logistics_product lp WHERE lp.prd_type='2')
    ) lp ON lp.lm_no=lm.lm_no
    WHERE {$add_where}
        {$add_x_type_where}
        {$add_y_type_where}
    ORDER BY x_type ASC, lm.lm_no ASC
";
$commerce_query = mysqli_query($my_db, $commerce_sql);
while($commerce = mysqli_fetch_assoc($commerce_query))
{
    $data_sum = $commerce['data_sum'];
    $commerce_list[$commerce['y_type']][$commerce['x_type']] += $data_sum;
    $commerce_sum_list[$commerce['x_type']] += $data_sum;
}


$alpha_idx  = 1;
$last_alpha = "";
$last_idx   = count($commerce_list)+2;

$work_sheet = $objPHPExcel->setActiveSheetIndex(0);
$work_sheet->setCellValue('A1', "");
$work_sheet->setCellValue("A{$last_idx}", "합계");
$work_sheet->getColumnDimension('A')->setWidth(20);

foreach($x_name_list as $x_type => $label)
{
    $alpha = $eng_list[$alpha_idx];
    $work_sheet->setCellValue("{$alpha}1", $label);
    $work_sheet->setCellValue("{$alpha}{$last_idx}", number_format($commerce_sum_list[$x_type]));
    $work_sheet->getColumnDimension("{$alpha}")->setWidth(10);
    $alpha_idx++;
}
$last_alpha = $eng_list[$alpha_idx];
$work_sheet->setCellValue("{$last_alpha}1", "합계");
$work_sheet->getColumnDimension("{$last_alpha}")->setWidth(10);

$idx = 2;
foreach($commerce_list as $y_type => $commerce_data)
{
    $alpha_idx    = 1;
    $work_sheet->setCellValue("A{$idx}", $y_name_list[$y_type]);
    $total_x_data = 0;
    foreach($x_name_list as $x_type => $x_label)
    {
        $alpha = $eng_list[$alpha_idx];
        $total_x_data += $commerce_data[$x_type];
        $work_sheet->setCellValue("{$alpha}{$idx}", number_format($commerce_data[$x_type]));
        $alpha_idx++;
    }

    $alpha = $eng_list[$alpha_idx];
    $work_sheet->setCellValue("{$alpha}{$idx}", number_format($total_x_data));

    $idx++;
}

$fontColor = new PHPExcel_Style_Color();
$fontColor->setRGB('FFFFFF');

$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('맑은 고딕');
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_alpha}1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF343A40');
$objPHPExcel->getActiveSheet()->getStyle("A{$last_idx}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF343A40');
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_alpha}1")->getFont()->setColor($fontColor);
$objPHPExcel->getActiveSheet()->getStyle("A{$last_idx}")->getFont()->setColor($fontColor);

$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_alpha}{$idx}")->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_alpha}1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:A{$last_idx}")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_alpha}{$idx}")->getFont()->setSize(9);

$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_alpha}{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("A1:{$last_alpha}{$idx}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle("B2:{$last_alpha}{$idx}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);


$excel_filename = "물류관리 통계";
$objPHPExcel->getActiveSheet()->setTitle($excel_filename);
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: text/html; charset=UTF-8');
header("Content-type: application/octetstream");
header('Content-Disposition: attachment;filename='.date("Ymd")."_".$excel_filename.".xlsx");

header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

$objWriter->save('php://output');
?>
