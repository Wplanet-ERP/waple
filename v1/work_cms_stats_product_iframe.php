<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');

# 검색조건
$today_val  = date('Y-m-d');
$month_val  = date('Y-m', strtotime('-3 months'))."-01";

$add_where          = "1=1 AND `w`.delivery_state='4'";
$sch_order_type     = isset($_GET['sch_order_type']) ? $_GET['sch_order_type'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_order_s_date   = isset($_GET['sch_order_s_date']) ? $_GET['sch_order_s_date'] : $month_val;
$sch_order_e_date   = isset($_GET['sch_order_e_date']) ? $_GET['sch_order_e_date'] : $today_val;
$sch_price_type     = isset($_GET['sch_price_type']) ? $_GET['sch_price_type'] : "";
$sch_dp_company     = isset($_GET['sch_dp_company']) ? $_GET['sch_dp_company'] : "";

if(empty($sch_order_s_date) || empty($sch_order_e_date))
{
    echo "날짜 검색 값이 없으면 차트 생성이 안됩니다.";
    exit;
}

if(!empty($sch_price_type)){

    if($sch_price_type == '1'){
        $add_where .= " AND w.unit_price > 0";
    }elseif($sch_price_type == '2'){
        $add_where .= " AND w.unit_price = 0";
    }
}

if(!empty($sch_dp_company)){
    $add_where .= " AND w.dp_c_no = '{$sch_dp_company}'";
}

# 브랜드 옵션
if(!empty($sch_brand))
{
    $add_where         .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2))
{
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1))
{
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

# 날짜 계산
$sch_s_month     = date("Y-m", strtotime($sch_order_s_date));
$sch_e_month     = date("Y-m", strtotime($sch_order_e_date));
$all_date_where  = "DATE_FORMAT(`allday`.Date, '%Y-%m') BETWEEN '{$sch_s_month}' AND '{$sch_e_month}'";
$all_date_key 	 = "DATE_FORMAT(`allday`.Date, '%Y%m')";
$all_date_title  = "DATE_FORMAT(`allday`.Date, '%Y.%m')";
$add_key_column  = "DATE_FORMAT(`w`.order_date, '%Y%m')";

# 사용 Array
$cms_total_name_list    = [];
$cms_total_list         = [];
$total_all_date_list    = [];

# 날짜 생성
$all_date_sql = "
	SELECT
 		{$all_date_title} as chart_title,
 		{$all_date_key} as chart_key,
        DATE_FORMAT(`allday`.Date, '%Y%m%d') as date_key
	FROM (SELECT adddate('2010-01-01',e.a*10000 + d.a*1000 + c.a*100 + b.a*10 + a.a) as Date FROM
			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) a,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) b,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) c,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) d,
 			(SELECT 0 a UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) e)
	as allday
	WHERE {$all_date_where}
	ORDER BY chart_key
";
$all_date_query = mysqli_query($my_db, $all_date_sql);
while($all_date = mysqli_fetch_assoc($all_date_query))
{
    $total_all_date_list[$all_date['date_key']]  = array("s_date" => date("Y-m-d",strtotime($all_date['date_key']))." 00:00:00", "e_date" => date("Y-m-d",strtotime($all_date['date_key']))." 23:59:59");
    $cms_total_name_list[$all_date['chart_key']] = $all_date['chart_title'];
    $cms_total_list[$all_date['chart_key']]      = 0;
}

$search_url = getenv("QUERY_STRING");
$smarty->assign("search_url", $search_url);

foreach($total_all_date_list as $date_data)
{
    $cms_total_sql = "
        SELECT
            DATE_FORMAT(w.order_date, '%Y%m') as ord_mon,
            SUM(w.{$sch_order_type}) AS total_value
        FROM work_cms w
        WHERE {$add_where} AND (order_date BETWEEN '{$date_data['s_date']}' AND '{$date_data['e_date']}')
        GROUP BY ord_mon
    ";
    $cms_total_query = mysqli_query($my_db, $cms_total_sql);
    while($cms_total_result = mysqli_fetch_assoc($cms_total_query)){
        $cms_total_list[$cms_total_result['ord_mon']] += $cms_total_result['total_value'];
    }
}

$smarty->assign("date_type_option", getDateTypeOption());
$smarty->assign("cms_total_row_cnt", count($cms_total_name_list)+1);
$smarty->assign("cms_total_list", $cms_total_list);
$smarty->assign("cms_total_name_list", $cms_total_name_list);
$smarty->assign("cms_total_chart_list", json_encode($cms_total_list));
$smarty->assign("cms_total_chart_name_list", json_encode($cms_total_name_list));

if($sch_order_type == "quantity"){
    $smarty->display('work_cms_stats_product_quantity_iframe.html');
}else{
    $smarty->display('work_cms_stats_product_price_iframe.html');
}
?>
