<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/evaluation.php');
require('inc/model/Staff.php');
require('inc/model/Team.php');
require('inc/model/Evaluation.php');

$ev_system_model 	= Evaluation::Factory();
$staff_model		= Staff::Factory();
$team_model			= Team::Factory();

$is_super_admin = false;
if($session_s_no == '1' || $session_s_no == '28'){
	$is_super_admin = true;
}
$smarty->assign("is_super_admin", $is_super_admin);


# 검색조건
$add_where          	= "1=1 AND er.is_complete='1' AND (er.rec_is_review='1' AND er.eval_is_review='1') AND esr.evaluation_state IN(1,2,3)";
$add_self_where       	= "1=1 AND sub.is_complete='1' AND (sub.rec_is_review='1' AND sub.eval_is_review='1') AND sub.receiver_s_no <> sub.evaluator_s_no AND sub.receiver_s_no=er.receiver_s_no";
$add_text_where       	= "1=1 AND er.is_complete='1' AND (er.rec_is_review='1' AND er.eval_is_review='1') AND esr.evaluation_state IN(4,5)";
$sch_ev_no              = isset($_GET['sch_ev_no']) ? $_GET['sch_ev_no'] : "";
$sch_receiver_team  	= isset($_GET['sch_receiver_team']) ? $_GET['sch_receiver_team'] : "";
$sch_receiver_staff 	= isset($_GET['sch_receiver_staff']) ? $_GET['sch_receiver_staff'] : "";
$search_url 			= getenv("QUERY_STRING");

$ev_system_list			= $ev_system_model->getEvSystemMultiRaterList($session_s_no);
$team_full_name_list    = $team_model->getTeamFullNameList();
$team_all_list          = $team_model->getTeamAllList();
$all_staff_list    		= $team_all_list['staff_list'];
$sch_rec_staff_list     = [];
$ev_unit_question_list  = [];
$evaluation_system		= [];
$ev_result_list			= [];
$ev_result_self_list	= [];
$ev_result_self_text	= [];
$ev_result_text_list	= [];
$ev_result_total_list	= [];
$ev_result_total_data   = [];
$ev_prev_total_list		= [];
$ev_rader_name_list		= [];
$ev_result_rec_total_list	= [];
$ev_simple_question_list	= [];

if(!empty($sch_ev_no))
{
	# 해당 평가지 체크
	$evaluation_system 	= $ev_system_model->getItem($sch_ev_no);
	$eval_year			= date("Y", strtotime($evaluation_system['ev_s_date']));

	if(empty($evaluation_system['ev_no'])) {
		exit("<script>alert('없는 평가지 입니다.'); location.href='evaluation_result_list.php';</script>");
	}

	if(!$is_super_admin && $evaluation_system['admin_s_no'] != $session_s_no){
		exit("<script>alert('이용 권한이 없습니다.'); location.href='evaluation_result_list.php';</script>");
	}

	$smarty->assign("eval_year", $eval_year);
	$smarty->assign("evaluation_system", $evaluation_system);

	$add_where 			.= " AND esr.ev_no='{$sch_ev_no}'";
	$add_self_where 	.= " AND sub.ev_no='{$sch_ev_no}'";
	$add_text_where 	.= " AND esr.ev_no='{$sch_ev_no}'";
	$smarty->assign("sch_ev_no", $sch_ev_no);

	if(!empty($sch_receiver_team))
	{
		if($sch_receiver_team != 'all')
		{
			$sch_rec_staff_list  = $all_staff_list[$sch_receiver_team];
			$sch_team_code_where = getTeamWhere($my_db, $sch_receiver_team);
			if($sch_team_code_where){
				$add_where 		.= " AND er.receiver_team IN ({$sch_team_code_where})";
				$add_self_where	.= " AND sub.receiver_team IN ({$sch_team_code_where})";
				$add_text_where .= " AND er.receiver_team IN ({$sch_team_code_where})";
			}
		}
		$smarty->assign("sch_receiver_team", $sch_receiver_team);
	}

	if(!empty($sch_receiver_staff)) {
		$add_where 		.= " AND esr.receiver_s_no='{$sch_receiver_staff}'";
		$add_self_where .= " AND sub.receiver_s_no='{$sch_receiver_staff}'";
		$add_text_where .= " AND esr.receiver_s_no='{$sch_receiver_staff}'";
		$smarty->assign("sch_receiver_staff", $sch_receiver_staff);
	}

	$evaluation_unit_sql    = "SELECT * FROM evaluation_unit WHERE ev_u_set_no='9' AND active='1' AND evaluation_state NOT IN(99,100) ORDER BY `order`, page ASC";
	$evaluation_unit_query  = mysqli_query($my_db, $evaluation_unit_sql);
	$page_idx               = 0;
	$page_chk               = 1;
	while($evaluation_unit = mysqli_fetch_array($evaluation_unit_query))
	{
		if($page_chk == $evaluation_unit['page']){
			$page_idx++;
		}else{
			$page_chk = $evaluation_unit['page'];
			$page_idx = 1;
		}

		$ev_question_subject = $evaluation_unit['page']."-".$page_idx.". ".$evaluation_unit['question'];
		$ev_unit_question_list[$evaluation_unit['ev_u_no']] = array(
			"kind"   	=> $evaluation_unit['kind'],
			"subject"   => $ev_question_subject,
			"state"    	=> $evaluation_unit['evaluation_state'],
			"question"  => $evaluation_unit['question']
		);

		if($evaluation_unit['evaluation_state'] == '1' || $evaluation_unit['evaluation_state'] == '2' || $evaluation_unit['evaluation_state'] == '3'){
			$ev_rader_name_list[] = array('name' => $ev_question_subject, 'min' => 55, 'max' => 85);
			$ev_simple_question_list[] = $ev_question_subject;
		}
	}

	# 자기평가 점수 리스트
	$ev_self_sql 	= "
		SELECT 
			esr.ev_u_no,
			esr.evaluation_value,
		   	er.receiver_s_no,
			(SELECT t.team_name FROM team t WHERE t.team_code=er.receiver_team) AS t_name,
			(SELECT s.s_name FROM staff s WHERE s.s_no=er.receiver_s_no) AS s_name,
			(SELECT s.`position` FROM staff s WHERE s.s_no=er.receiver_s_no) AS s_position,
			(SELECT COUNT(sub.ev_r_no) FROM evaluation_relation sub WHERE {$add_self_where}) AS eval_cnt 
		FROM evaluation_system_result esr 
		LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no 
		WHERE {$add_where} AND esr.receiver_s_no=esr.evaluator_s_no
		ORDER BY receiver_s_no
	";
	$ev_self_query		= mysqli_query($my_db, $ev_self_sql);
	$ev_self_total_tmp	= [];
	while($ev_self = mysqli_fetch_assoc($ev_self_query)){
		if(!isset($ev_result_self_list[$ev_self['receiver_s_no']])){
			$ev_result_self_list[$ev_self['receiver_s_no']] = array(
				"t_name" 		=> $ev_self['t_name'],
				"s_name" 		=> $ev_self['s_name'],
				"s_position"	=> $ev_self['s_position'],
				"eval_cnt"		=> $ev_self['eval_cnt']
			);
		}
		$ev_result_self_list[$ev_self['receiver_s_no']][$ev_self['ev_u_no']] = $ev_self['evaluation_value'];

		$ev_self_total_tmp[$ev_self['receiver_s_no']]['total'] += $ev_self['evaluation_value'];
		$ev_self_total_tmp[$ev_self['receiver_s_no']]['cnt']++;
	}

	if(!empty($ev_self_total_tmp)) {
		foreach($ev_self_total_tmp as $self_s_no => $self_data) {
			$ev_result_self_list[$self_s_no]['avg'] = round($self_data['total']/$self_data['cnt'], 2);
		}
	}

	# 평가의견 리스트
	$ev_text_sql 	= "SELECT * FROM evaluation_system_result esr LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no WHERE {$add_text_where} ORDER BY esr.ev_u_no";
	$ev_text_query	= mysqli_query($my_db, $ev_text_sql);
	while($ev_text = mysqli_fetch_assoc($ev_text_query)){
		if($ev_text['receiver_s_no'] == $ev_text['evaluator_s_no']){
			$ev_result_self_text[$ev_text['receiver_s_no']][$ev_text['ev_u_no']]   = $ev_text['evaluation_value'];
		}
		else{
			$ev_result_text_list[$ev_text['receiver_s_no']][$ev_text['ev_u_no']][] = $ev_text['evaluation_value'];
		}
	}

	# 현평가 전체평균 리스트
	$ev_result_total_sql 	= "
		SELECT
			ev_no,
			ev_u_no,
			AVG(avg_sqrt) as avg_sqrt
		FROM
		(
			SELECT 
				esr.ev_no,
				esr.ev_u_no, 
				esr.receiver_s_no,
				AVG(esr.`sqrt`) AS avg_sqrt
			FROM evaluation_system_result as esr
			LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
			WHERE esr.ev_no='{$sch_ev_no}' AND er.is_complete='1' AND (er.rec_is_review='1' AND er.eval_is_review='1') AND esr.evaluation_state IN(1,2,3) AND esr.evaluator_s_no <> esr.receiver_s_no
			GROUP BY esr.receiver_s_no, esr.ev_u_no
			ORDER BY esr.receiver_s_no, esr.ev_u_no
		) as ev_result
		GROUP BY ev_u_no
		ORDER BY ev_u_no
	";

	$ev_result_total_query 	= mysqli_query($my_db, $ev_result_total_sql);
	$ev_result_total_avg 	= 0;
	$ev_result_total_cnt 	= 0;
	while($ev_result_total = mysqli_fetch_assoc($ev_result_total_query)){
		$ev_total_avg = round($ev_result_total['avg_sqrt'], 2);

		$ev_result_total_list[$sch_ev_no]["subject"] = $eval_year." 전체평균";
		$ev_result_total_list[$sch_ev_no][$ev_result_total['ev_u_no']] = $ev_total_avg;
		$ev_result_total_data[$sch_ev_no][] = $ev_total_avg;
		$ev_result_total_avg 	+= $ev_total_avg;
		$ev_result_total_cnt++;
	}

	$ev_result_total_list[$sch_ev_no]['avg'] = 0;
	if($ev_result_total_avg > 0 && $ev_result_total_cnt > 0){
		$ev_result_total_list[$sch_ev_no]['avg'] = round($ev_result_total_avg/$ev_result_total_cnt, 2);
	}

	# 평가 평균
	$ev_result_sql = "
		SELECT 
			esr.ev_u_no, 
			esr.receiver_s_no,
			AVG(esr.`sqrt`) AS avg_sqrt
		FROM evaluation_system_result as esr
		LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
		WHERE {$add_where} AND esr.evaluator_s_no <> esr.receiver_s_no
		GROUP BY esr.receiver_s_no, esr.ev_u_no
		ORDER BY esr.receiver_s_no, esr.ev_u_no
	";
	$ev_result_query 	= mysqli_query($my_db, $ev_result_sql);
	$ev_result_tmp_list	= [];
	$ev_result_tmp_value= [];
	while($ev_result = mysqli_fetch_assoc($ev_result_query))
	{
		$ev_avg_sqrt = round($ev_result['avg_sqrt'],2);

		$ev_result_list[$ev_result['receiver_s_no']][$ev_result['ev_u_no']] = $ev_avg_sqrt;
		$ev_result_tmp_value[$ev_result['receiver_s_no']][] = $ev_avg_sqrt;
		$ev_result_tmp_list[$ev_result['receiver_s_no']]['total'] += $ev_avg_sqrt;
		$ev_result_tmp_list[$ev_result['receiver_s_no']]['cnt']++;
	}

	foreach($ev_result_self_list as $rec_s_no => $self_data)
	{
		$ev_self_result_data = $ev_result_tmp_value[$rec_s_no];

		$ev_rader_tmp_list 	 = [];
		$ev_rader_tmp_list[] = array('name' => "{$eval_year} 평가결과", 'value' => $ev_self_result_data);
		$ev_rader_tmp_list[] = array('name' => "{$eval_year} 전체평균", 'value' => $ev_result_total_data[$sch_ev_no]);
		$ev_result_list[$rec_s_no]['rader_list']  = json_encode($ev_rader_tmp_list);

		$rec_total_data 			= [];
		$ev_simple_tmp_type_list	= [];
		$ev_simple_tmp_name_list	= array("result_type");

		if(!empty($ev_self_result_data))
		{
			$ev_simple_tmp_type_list[] = array("type" => 'bar');
			$ev_simple_tmp_name_list[] = "{$eval_year} 평가결과";
		}

		foreach($ev_system_list as $ev_no => $ev_subject)
		{
			if($ev_no < $sch_ev_no)
			{
				$rec_ev_system 	= $ev_system_model->getItem($ev_no);
				$rec_cur_year	= date("Y", strtotime($rec_ev_system['ev_s_date']));
				$rec_total_sql  = "
					SELECT 
						esr.ev_no,
						esr.ev_u_no, 
						AVG(esr.`sqrt`) AS avg_sqrt
					FROM evaluation_system_result as esr
					LEFT JOIN evaluation_relation er ON er.ev_r_no=esr.ev_r_no
					WHERE esr.ev_no='{$ev_no}' AND er.receiver_s_no='{$rec_s_no}' AND er.is_complete='1' AND (er.rec_is_review='1' AND er.eval_is_review='1') AND esr.evaluation_state IN(1,2,3) AND esr.evaluator_s_no <> esr.receiver_s_no
					GROUP BY esr.ev_u_no
					ORDER BY esr.ev_u_no
				";
				$rec_total_query 	= mysqli_query($my_db, $rec_total_sql);
				$rec_total_list 	= [];
				$rec_total_avg		= 0;
				$rec_total_cnt		= 0;
				while($rec_total = mysqli_fetch_assoc($rec_total_query))
				{
					if($rec_total_cnt == 0 ){
						$ev_simple_tmp_name_list[] = $rec_cur_year." 평가결과";
						$rec_total_list["subject"] = $rec_cur_year." 평가결과";
						$ev_simple_tmp_type_list[] = array("type" => 'bar');
					}

					$rec_u_avg = round($rec_total['avg_sqrt'], 2);
					$rec_total_list[$rec_total['ev_u_no']] 	= $rec_u_avg;
					$rec_total_data[$ev_no][] 				= $rec_u_avg;

					$rec_total_avg += $rec_u_avg;
					$rec_total_cnt++;
				}

				if(!empty($rec_total_list))
				{
					if($rec_total_avg > 0 && $rec_total_cnt > 0){
						$rec_total_list['avg'] = round($rec_total_avg/$rec_total_cnt, 2);
					}

					$ev_result_rec_total_list[$rec_s_no][$ev_no] = $rec_total_list;
				}
			}
		}

		$ev_simple_tmp_list 		= [];
		$ev_simple_tmp_list[] 		= $ev_simple_tmp_name_list;

		if($ev_self_result_data)
		{
			$idx = 0;
			foreach($ev_simple_question_list as $simple_question)
			{
				$ev_question_name 	  	= $ev_rader_name_list[$idx];
				$ev_simple_tmp_data		= [];
				$ev_simple_tmp_data[] 	= $ev_question_name['name'];
				$ev_simple_tmp_data[] 	= $ev_self_result_data[$idx];
				foreach($rec_total_data as $ev_no => $total_data){
					$ev_simple_tmp_data[] = $total_data[$idx];
				}
				$ev_simple_tmp_list[] = $ev_simple_tmp_data;
				$idx++;
			}
		}

		$ev_result_list[$rec_s_no]['simple_list'] 		= json_encode($ev_simple_tmp_list);
		$ev_result_list[$rec_s_no]['simple_type_list'] 	= json_encode($ev_simple_tmp_type_list);
	}

	if($ev_result_tmp_list){
		foreach ($ev_result_tmp_list as $rec_s_no => $result_data){
			$ev_result_list[$rec_s_no]['avg'] = round($result_data['total']/$result_data['cnt'], 2);
		}
	}
}

$smarty->assign("search_url", $search_url);
$smarty->assign("ev_system_list", $ev_system_list);
$smarty->assign("sch_team_list", $team_full_name_list);
$smarty->assign("sch_rec_staff_list", $sch_rec_staff_list);
$smarty->assign("ev_unit_question_list", $ev_unit_question_list);
$smarty->assign("evaluation_system", $evaluation_system);
$smarty->assign("ev_result_list", $ev_result_list);
$smarty->assign("ev_result_self_list", $ev_result_self_list);
$smarty->assign("ev_result_self_text", $ev_result_self_text);
$smarty->assign("ev_result_text_list", $ev_result_text_list);
$smarty->assign("ev_result_total_list", $ev_result_total_list);
$smarty->assign("ev_result_rec_total_list", $ev_result_rec_total_list);
$smarty->assign("ev_rader_name_list", json_encode($ev_rader_name_list));

$smarty->display('evaluation_multi_rater.html');

?>
