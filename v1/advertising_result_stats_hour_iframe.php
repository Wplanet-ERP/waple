<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_common.php');
require('inc/helper/_date.php');
require('inc/helper/advertising.php');

# 검색조건
$today_val  = date('Y-m-d');
$week_val   = date('Y-m-d', strtotime('-1 weeks'));
$month_val  = date('Y-m-d', strtotime('-1 months'));
$months_val = date('Y-m-d', strtotime('-3 months'));
$year_val   = date('Y-m-d', strtotime('-1 years'));

$smarty->assign("today_val", $today_val);
$smarty->assign("week_val", $week_val);
$smarty->assign("month_val", $month_val);
$smarty->assign("months_val", $months_val);
$smarty->assign("year_val", $year_val);

$add_where          = "1=1 AND `am`.state IN(3,5)";
$add_date_where     = "1=1";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_adv_s_date     = isset($_GET['sch_adv_s_date']) ? $_GET['sch_adv_s_date'] : $month_val;
$sch_adv_e_date     = isset($_GET['sch_adv_e_date']) ? $_GET['sch_adv_e_date'] : $today_val;
$sch_adv_date_type  = isset($_GET['sch_adv_date_type']) ? $_GET['sch_adv_date_type'] : "month";
$sch_date_kind      = isset($_GET['sch_date_kind']) ? $_GET['sch_date_kind'] : "";
$sch_date_s_time    = isset($_GET['sch_date_s_time']) ? $_GET['sch_date_s_time'] : "";
$sch_date_e_time    = isset($_GET['sch_date_e_time']) ? $_GET['sch_date_e_time'] : "";
$sch_no             = isset($_GET['sch_no']) ? $_GET['sch_no'] : "";
$sch_media          = 265;
$sch_product        = isset($_GET['sch_product']) ? $_GET['sch_product'] : "";
$sch_state          = isset($_GET['sch_state']) ? $_GET['sch_state'] : "";
$sch_agency         = isset($_GET['sch_agency']) ? $_GET['sch_agency'] : "";
$sch_is_complete    = isset($_GET['sch_is_complete']) ? $_GET['sch_is_complete'] : "";
$sch_in_date_type   = isset($_GET['sch_in_date_type']) ? $_GET['sch_in_date_type'] : 3;

if(empty($sch_adv_s_date) || empty($sch_adv_e_date)){
    echo "날짜 검색 값이 없으면 차트 생성이 안됩니다.";
    exit;
}

if(!empty($sch_adv_s_date)){
    $adv_s_datetime  = $sch_adv_s_date." 00:00:00";
    $add_where      .= " AND am.adv_s_date >= '{$adv_s_datetime}'";
}

if(!empty($sch_adv_e_date)){
    $adv_e_datetime  = $sch_adv_e_date." 23:59:59";
    $add_where      .= " AND am.adv_s_date <= '{$adv_e_datetime}'";
}

if($sch_date_kind != ""){
    $add_date_where .= " AND chk_s_day = '{$sch_date_kind}'";
    $smarty->assign("sch_date_kind", $sch_date_kind);
}

if($sch_date_s_time != ""){
    $add_date_where .= " AND chk_s_time = '{$sch_date_s_time}'";
    $smarty->assign("sch_date_s_time", $sch_date_s_time);
}

if($sch_date_e_time != ""){
    $add_date_where .= " AND chk_e_time = '{$sch_date_e_time}'";
    $smarty->assign("sch_date_e_time", $sch_date_e_time);
}

if(!empty($sch_no)){
    $add_where .= " AND am.am_no = '{$sch_no}'";
}

if(!empty($sch_media)){
    $add_where .= " AND am.media = '{$sch_media}'";
}

if(!empty($sch_product)){
    $add_where .= " AND am.product = '{$sch_product}'";
}

if(!empty($sch_state)){
    $add_where .= " AND am.state = '{$sch_state}'";
}

if(!empty($sch_agency)){
    $add_where .= " AND am.agency = '{$sch_agency}'";
}

if(!empty($sch_is_complete)){
    if($sch_is_complete == '1'){
        $add_where .= " AND (`am`.price - (SELECT SUM(`ar`.adv_price) FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no) != 0)";
    }elseif($sch_is_complete == '2'){
        $add_where .= " AND (`am`.price - (SELECT SUM(`ar`.adv_price) FROM advertising_result `ar` WHERE `ar`.am_no=`am`.am_no) = 0)";
    }
}

# 브랜드 옵션
if(!empty($sch_brand)) {
    $add_where         .= " AND `ar`.brand = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $add_where         .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $add_where         .= " AND `ar`.brand IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}

$adv_result_time_hour_list   = array("0" => "00~04", "1" => "04~08", "2" => "08~09", "3" => "09~10", "4" => "10~11", "5" => "11~12", "6" => "12~13", "7" => "13~14", "8" => "14~15", "9" => "15~16", "10" => "16~17", "11" => "17~18", "12" => "18~19", "13" => "19~20", "14" => "20~21", "15" => "21~22", "16" => "22~23", "17" => "23~24");
$adv_result_time_table_list  = [];
$adv_result_time_chart_list  = array(
    array(
        "title" => "광고비",
        "type"  => "bar",
        "color" => "#BFBFFF",
        "data"  => array(),
    ),
    array(
        "title" => "클릭수",
        "type"  => "bar",
        "color" => "#BDBEBD",
        "data"  => array(),
    ),
    array(
        "title" => "CPC",
        "type"  => "line",
        "color" => "#FF0000",
        "data"  => array(),
    )
);

foreach($adv_result_time_hour_list as $chart_key => $chart_title){
    $adv_result_time_table_list[$chart_key]  = array("adv_price" => 0, "click_cnt" => 0, "adv_cpc" => 0, "impressions" => 0, "click_per" => 0);
}

$adv_result_special_hour_list   = array("0" => "00~02", "1" => "02~04", "2" => "04~06", "3" => "06~08", "4" => "08~10", "5" => "10~12", "6" => "12~14", "7" => "14~16", "8" => "16~18", "9" => "18~20", "10" => "20~22", "11" => "22~24");
$adv_result_special_table_list  = [];
$adv_result_special_chart_list  = array(
    array(
        "title" => "광고비",
        "type"  => "bar",
        "color" => "#BFBFFF",
        "data"  => array(),
    ),
    array(
        "title" => "클릭수",
        "type"  => "bar",
        "color" => "#BDBEBD",
        "data"  => array(),
    ),
    array(
        "title" => "CPC",
        "type"  => "line",
        "color" => "#FF0000",
        "data"  => array(),
    )
);

foreach($adv_result_special_hour_list as $chart_key => $chart_title){
    $adv_result_special_table_list[$chart_key]  = array("adv_price" => 0, "click_cnt" => 0, "adv_cpc" => 0, "impressions" => 0, "click_per" => 0);
}

$adv_result_sql = "
    SELECT
        *
    FROM
    (
        SELECT
            `ar`.am_no,
            `ar`.brand,
            `am`.product,
            `am`.fee_per,
            `am`.price,
            `ar`.impressions,
            `ar`.click_cnt,
            `ar`.adv_price,
            DATE_FORMAT(`am`.adv_s_date, '%H') as key_date,
            DATE_FORMAT(`am`.adv_s_date, '%w') as chk_s_day,
            DATE_FORMAT(`am`.adv_s_date, '%H') as chk_s_time,
            DATE_FORMAT(`am`.adv_e_date, '%H') as chk_e_time
        FROM advertising_result `ar`
        LEFT JOIN advertising_management `am` ON `am`.am_no=`ar`.am_no
        WHERE {$add_where}
    ) AS `am`
    WHERE {$add_date_where}
    ORDER BY key_date ASC
";
$adv_result_query       = mysqli_query($my_db, $adv_result_sql);
$adv_result_tmp_list    = [];
while($adv_result = mysqli_fetch_assoc($adv_result_query))
{
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["product"]             = $adv_result['product'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["key_date"]            = $adv_result['key_date'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["fee_per"]             = $adv_result['fee_per'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_price"]         = $adv_result['price'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_impressions"]  += $adv_result['impressions'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_click_cnt"]    += $adv_result['click_cnt'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_adv_price"]    += $adv_result['adv_price'];
    $adv_result_tmp_list[$adv_result['am_no']][$adv_result['brand']]["total_cnt"]++;
}
foreach($adv_result_tmp_list as $am_no => $adv_result_tmp)
{
    foreach($adv_result_tmp as $brand => $adv_brand_data)
    {
        $fee_price      = $adv_brand_data['total_adv_price']*($adv_brand_data['fee_per']/100);
        $cal_price      = $adv_brand_data["total_adv_price"]-$fee_price;

        if($adv_brand_data['product'] == '1')
        {
            $chk_key_date = "";

            switch($adv_brand_data['key_date']){
                case "0":
                case "1":
                case "2":
                case "3":
                    $chk_key_date = 0;
                    break;
                case "4":
                case "5":
                case "6":
                case "7":
                    $chk_key_date = 1;
                    break;
                case "8":
                    $chk_key_date = 2;
                    break;
                case "9":
                    $chk_key_date = 3;
                    break;
                case "10":
                    $chk_key_date = 4;
                    break;
                case "11":
                    $chk_key_date = 5;
                    break;
                case "12":
                    $chk_key_date = 6;
                    break;
                case "13":
                    $chk_key_date = 7;
                    break;
                case "14":
                    $chk_key_date = 8;
                    break;
                case "15":
                    $chk_key_date = 9;
                    break;
                case "16":
                    $chk_key_date = 10;
                    break;
                case "17":
                    $chk_key_date = 11;
                    break;
                case "18":
                    $chk_key_date = 12;
                    break;
                case "19":
                    $chk_key_date = 13;
                    break;
                case "20":
                    $chk_key_date = 14;
                    break;
                case "21":
                    $chk_key_date = 15;
                    break;
                case "22":
                    $chk_key_date = 16;
                    break;
                case "23":
                    $chk_key_date = 17;
                    break;
                default:
                    $chk_key_date = 0;
            }

            $adv_result_time_table_list[$chk_key_date]['adv_price']    += $cal_price;
            $adv_result_time_table_list[$chk_key_date]['click_cnt']    += $adv_brand_data["total_click_cnt"];
            $adv_result_time_table_list[$chk_key_date]['impressions']  += $adv_brand_data["total_impressions"];
        }
        elseif($adv_brand_data['product'] == '2')
        {
            $chk_key_date = (int)($adv_brand_data['key_date']/2);

            $adv_result_special_table_list[$chk_key_date]['adv_price']    += $cal_price;
            $adv_result_special_table_list[$chk_key_date]['click_cnt']    += $adv_brand_data["total_click_cnt"];
            $adv_result_special_table_list[$chk_key_date]['impressions']  += $adv_brand_data["total_impressions"];
        }
    }
}

foreach($adv_result_time_table_list as $key_date => $key_data){
    $adv_result_time_table_list[$key_date]["adv_cpc"]    = ($key_data['click_cnt'] == 0) ? 0 : $key_data['adv_price']/$key_data['click_cnt'];
    $adv_result_time_table_list[$key_date]["click_per"]  = ($key_data['impressions'] == 0) ? $key_data['impressions'] : $key_data['click_cnt']/$key_data['impressions']*100;

    $adv_result_time_chart_list[0]['data'][] = (int)$key_data['adv_price'];
    $adv_result_time_chart_list[1]['data'][] = (int)$key_data['click_cnt'];
    $adv_result_time_chart_list[2]['data'][] = ($key_data['click_cnt'] == 0) ? 0 : round($key_data['adv_price']/$key_data['click_cnt'], 1);
}

foreach($adv_result_special_table_list as $key_date => $key_data){
    $adv_result_special_table_list[$key_date]["adv_cpc"]    = ($key_data['click_cnt'] == 0) ? 0 : $key_data['adv_price']/$key_data['click_cnt'];
    $adv_result_special_table_list[$key_date]["click_per"]  = ($key_data['impressions'] == 0) ? $key_data['impressions'] : $key_data['click_cnt']/$key_data['impressions']*100;

    $adv_result_special_chart_list[0]['data'][] = (int)$key_data['adv_price'];
    $adv_result_special_chart_list[1]['data'][] = (int)$key_data['click_cnt'];
    $adv_result_special_chart_list[2]['data'][] = ($key_data['click_cnt'] == 0) ? 0 : round($key_data['adv_price']/$key_data['click_cnt'], 1);
}

$smarty->assign("adv_result_time_hour_list", $adv_result_time_hour_list);
$smarty->assign("adv_result_time_hour_cnt", count($adv_result_time_hour_list));
$smarty->assign("adv_result_time_chart_name_list", json_encode($adv_result_time_hour_list));
$smarty->assign("adv_result_time_chart_list", json_encode($adv_result_time_chart_list));
$smarty->assign("adv_result_time_table_list", $adv_result_time_table_list);
$smarty->assign("adv_result_special_hour_list", $adv_result_special_hour_list);
$smarty->assign("adv_result_special_hour_cnt", count($adv_result_special_hour_list));
$smarty->assign("adv_result_special_chart_name_list", json_encode($adv_result_special_hour_list));
$smarty->assign("adv_result_special_chart_list", json_encode($adv_result_special_chart_list));
$smarty->assign("adv_result_special_table_list", $adv_result_special_table_list);

$smarty->display('advertising_result_stats_hour_iframe.html');
?>
