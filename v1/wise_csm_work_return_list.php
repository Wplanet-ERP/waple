<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/wise_csm.php');
require('inc/model/MyQuick.php');
require('inc/model/Staff.php');
require('inc/model/Kind.php');

# Navigation & My Quick
$nav_prd_no  = "229";
$nav_title   = "커머스 환불 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 변수 리스트
$staff_model                = Staff::Factory();
$cs_staff_list              = $staff_model->getActiveTeamStaff("00244");
$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$sch_brand_total_list       = $brand_company_total_list['brand_info_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

# 검색 조건
$add_where          = "w.prd_no='229' AND w.work_state='6'";
$sch_reg_s_date     = isset($_GET['sch_reg_s_date']) ? $_GET['sch_reg_s_date'] : date("Y-m")."-01";
$sch_reg_e_date     = isset($_GET['sch_reg_e_date']) ? $_GET['sch_reg_e_date'] : date("Y-m-d");
$sch_reg_s_datetime = $sch_reg_s_date." 00:00:00";
$sch_reg_e_datetime = $sch_reg_e_date." 23:59:59";
$sch_cs_staff       = isset($_GET['sch_cs_staff']) ? $_GET['sch_cs_staff'] : "";
$sch_brand_g1       = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2       = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand          = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_task_req       = isset($_GET['sch_task_req']) ? $_GET['sch_task_req'] : "";
$sch_wd_s_date      = isset($_GET['sch_wd_s_date']) ? $_GET['sch_wd_s_date'] : "";
$sch_wd_e_date      = isset($_GET['sch_wd_e_date']) ? $_GET['sch_wd_e_date'] : "";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if(!empty($sch_reg_s_date))
{
    $add_where .= " AND `w`.regdate >= '{$sch_reg_s_datetime}'";
    $smarty->assign('sch_reg_s_date', $sch_reg_s_date);
}

if(!empty($sch_reg_e_date))
{
    $add_where .= " AND `w`.regdate <= '{$sch_reg_e_datetime}'";
    $smarty->assign('sch_reg_e_date', $sch_reg_e_date);
}

if(!empty($sch_cs_staff))
{
    $add_where .= " AND `w`.task_req_s_no = '{$sch_cs_staff}'";
    $smarty->assign('sch_cs_staff', $sch_cs_staff);
}

if(!empty($sch_brand)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `w`.c_no = '{$sch_brand}'";
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $sch_brand_list     = $brand_list[$sch_brand_g2];
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
}
elseif(!empty($sch_brand_g1)) {
    $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
    $add_where         .= " AND `w`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
}
$smarty->assign("sch_brand_g1", $sch_brand_g1);
$smarty->assign("sch_brand_g2", $sch_brand_g2);
$smarty->assign("sch_brand", $sch_brand);
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

if(!empty($sch_task_req))
{
    $add_where .= " AND `w`.task_req LIKE '%사유 : {$sch_task_req}%'";
    $smarty->assign('sch_task_req', $sch_task_req);
}

if(!empty($sch_wd_s_date))
{
    $add_where .= " AND `wd`.wd_date >= '{$sch_wd_s_date}'";
    $smarty->assign('sch_wd_s_date', $sch_wd_s_date);
}

if(!empty($sch_wd_e_date))
{
    $add_where .= " AND `wd`.wd_date <= '{$sch_wd_e_date}'";
    $smarty->assign('sch_wd_e_date', $sch_wd_e_date);
}

# 전체 게시물 수
$csm_work_return_total_sql       = "SELECT COUNT(w.w_no) as cnt FROM `work` w LEFT JOIN withdraw wd ON wd.wd_no=w.wd_no WHERE {$add_where}";
$csm_work_return_total_query	 = mysqli_query($my_db, $csm_work_return_total_sql);
$csm_work_return_total_result    = mysqli_fetch_array($csm_work_return_total_query);
$csm_work_return_total           = $csm_work_return_total_result['cnt'];

# 페이징
$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num	= ceil($csm_work_return_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "wise_csm_work_return_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $csm_work_return_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$csm_work_return_sql  = "
    SELECT
        wd.wd_no,
        w.c_no,
        w.regdate,
        DATE_FORMAT(w.regdate,'%Y-%m-%d') as reg_day,
        w.task_req_s_no,
        (SELECT s.s_name FROM staff s WHERE s.s_no=w.task_req_s_no) AS req_s_name,
        (SELECT c.c_name FROM company c WHERE c.c_no=w.c_no) AS brand_name,
        w.w_no,
        wd.bk_name,
        w.task_req,
        wd.wd_money,
        wd.wd_date,
        (SELECT w.order_number FROM work_cms w WHERE w.w_no=w.linked_no) as ord_no
    FROM `work` w
    LEFT JOIN withdraw wd ON wd.wd_no=w.wd_no
    WHERE {$add_where}
    ORDER BY wd.wd_no DESC
    LIMIT {$offset}, {$num}
";
$csm_work_return_query      = mysqli_query($my_db, $csm_work_return_sql);
$csm_work_return_list       = [];
while($csm_work_return = mysqli_fetch_assoc($csm_work_return_query))
{
    $task_req_tmp               = explode("사유 : ", $csm_work_return['task_req']);
    $csm_work_return['reason']  = isset($task_req_tmp[1]) ? $task_req_tmp[1] : "";
    $csm_work_return_list[]     = $csm_work_return;
}

$smarty->assign("page_type_option", getPageTypeOption('4'));
$smarty->assign("cs_staff_list", $cs_staff_list);
$smarty->assign("csm_work_return_list", $csm_work_return_list);

$smarty->display('wise_csm_work_return_list.html');
?>