<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/helper/work_sales.php');
require('inc/model/MyQuick.php');
require('inc/model/Company.php');
require('inc/model/ProductCms.php');
require('inc/model/Kind.php');
require('inc/model/WorkCmsSettle.php');

# Navigation & My Quick
$nav_prd_no  = "159";
$nav_title   = "정산 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

$company_model          = Company::Factory();
$sch_dp_company_list    = $company_model->getDpDisplayList();
$settle_company_list    = $company_model->getSettleCompanyList();
$logistics_company_list = $company_model->getLogisticsList();
$product_model          = ProductCms::Factory();

$kind_model                 = Kind::Factory();
$brand_company_total_list   = $kind_model->getBrandCompanyList();
$brand_company_g1_list      = $brand_company_total_list['brand_g1_list'];
$brand_company_g2_list      = $brand_company_total_list['brand_g2_list'];
$brand_list                 = $brand_company_total_list['brand_dom_list'];
$brand_total_list           = $brand_company_total_list['brand_total_list'];
$brand_g2_parent_list       = $brand_company_total_list['brand_g2_parent_list'];
$brand_parent_list          = $brand_company_total_list['brand_parent_list'];
$sch_brand_g2_list          = [];
$sch_brand_list             = [];

# 검색조건
$add_where              = "1=1";
$current_month          = date('Y-m');
$sch_settle_month       = isset($_GET['sch_settle_month']) ? $_GET['sch_settle_month'] : $current_month;
$sch_settle_c_no        = isset($_GET['sch_settle_c_no']) ? $_GET['sch_settle_c_no'] : "";
$sch_settle_type        = isset($_GET['sch_settle_type']) ? $_GET['sch_settle_type'] : "";
$sch_settle_dp_c_no     = isset($_GET['sch_settle_dp_c_no']) ? $_GET['sch_settle_dp_c_no'] : "";
$sch_settle_s_date      = isset($_GET['sch_settle_s_date']) ? $_GET['sch_settle_s_date'] : "";
$sch_settle_e_date      = isset($_GET['sch_settle_e_date']) ? $_GET['sch_settle_e_date'] : "";
$sch_ord_no             = isset($_GET['sch_ord_no']) ? $_GET['sch_ord_no'] : "";
$sch_is_return          = isset($_GET['sch_is_return']) ? $_GET['sch_is_return'] : "";
$sch_is_order           = isset($_GET['sch_is_order']) ? $_GET['sch_is_order'] : "";
$sch_log_c_no           = isset($_GET['sch_log_c_no']) ? $_GET['sch_log_c_no'] : "";
$sch_sku                = isset($_GET['sch_sku']) ? $_GET['sch_sku'] : "";
$sch_settle_gubun       = isset($_GET['sch_settle_gubun']) ? $_GET['sch_settle_gubun'] : "";
$sch_settle_cs          = isset($_GET['sch_settle_cs']) ? $_GET['sch_settle_cs'] : "";
$sch_file_no            = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";
$sch_notice             = isset($_GET['sch_notice']) ? $_GET['sch_notice'] : "";
$sch_brand_g1           = isset($_GET['sch_brand_g1']) ? $_GET['sch_brand_g1'] : "";
$sch_brand_g2           = isset($_GET['sch_brand_g2']) ? $_GET['sch_brand_g2'] : "";
$sch_brand              = isset($_GET['sch_brand']) ? $_GET['sch_brand'] : "";
$sch_single_settle      = isset($_GET['sch_single_settle']) ? $_GET['sch_single_settle'] : "";
$sch_ord_s_date         = isset($_GET['sch_ord_s_date']) ? $_GET['sch_ord_s_date'] : "";
$sch_ord_e_date         = isset($_GET['sch_ord_e_date']) ? $_GET['sch_ord_e_date'] : "";
$sch_prd_no             = isset($_GET['sch_prd_no']) ? $_GET['sch_prd_no'] : "";
$sch_is_single          = false;
$add_order_by           = "`no` DESC";

# 브랜드 parent 매칭
if(!empty($sch_brand)) {
    $sch_brand_g1 = $brand_parent_list[$sch_brand]["brand_g1"];
    $sch_brand_g2 = $brand_parent_list[$sch_brand]["brand_g2"];
}
elseif(!empty($sch_brand_g2)) {
    $sch_brand_g1 = $brand_g2_parent_list[$sch_brand_g2]["brand_g1"];
}

if($sch_single_settle && (!empty($sch_ord_s_date) && !empty($sch_ord_e_date)))
{
    $sch_ord_s_datetime = "{$sch_ord_s_date} 00:00:00";
    $sch_ord_e_datetime = "{$sch_ord_e_date} 23:59:59";
    $add_where         .= " AND `wcs`.settle_dp_c_no='{$sch_settle_dp_c_no}' AND `wcs`.settle_price > 0 AND `wcs`.order_number IN(SELECT DISTINCT w.order_number FROM work_cms w WHERE w.delivery_state='4' AND w.unit_price > 0 AND w.prd_no='{$sch_prd_no}' AND w.order_number IN(SELECT DISTINCT sub.order_number FROM work_cms sub WHERE sub.dp_c_no='{$sch_settle_dp_c_no}' AND sub.unit_price > 0 AND sub.delivery_state='4' AND (sub.order_date BETWEEN '{$sch_ord_s_datetime}' AND '{$sch_ord_e_datetime}') GROUP BY sub.order_number HAVING COUNT(*) = 1) AND w.order_number NOT IN(SELECT DISTINCT sub.order_number FROM work_cms_settlement sub WHERE sub.order_number=w.order_number AND sub.settle_price_vat < 0))";
    $sch_is_single      = true;
    $add_order_by       = "`settle_price` DESC";
}
else
{
    if(!empty($sch_settle_month))
    {
        $add_where .= " AND `wcs`.settle_month = '{$sch_settle_month}'";
        $smarty->assign('sch_settle_month', $sch_settle_month);
    }

    if(!empty($sch_settle_c_no))
    {
        $add_where .= " AND `wcs`.settle_c_no = '{$sch_settle_c_no}'";
        $smarty->assign('sch_settle_c_no', $sch_settle_c_no);
    }

    if(!empty($sch_settle_type))
    {
        $add_where .= " AND `wcs`.settle_type = '{$sch_settle_type}'";
        $smarty->assign('sch_settle_type', $sch_settle_type);
    }

    if(!empty($sch_settle_dp_c_no))
    {
        $add_where .= " AND `wcs`.settle_dp_c_no = '{$sch_settle_dp_c_no}'";
        $smarty->assign('sch_settle_dp_c_no', $sch_settle_dp_c_no);
    }

    if(!empty($sch_settle_s_date)){
        $add_where   .= " AND `wcs`.settle_date >= '{$sch_settle_s_date}'";
        $smarty->assign('sch_settle_s_date', $sch_settle_s_date);
    }

    if(!empty($sch_settle_e_date)){
        $add_where   .= " AND `wcs`.settle_date <= '{$sch_settle_e_date}'";
        $smarty->assign('sch_settle_e_date', $sch_settle_e_date);
    }

    if(!empty($sch_ord_no)){
        $add_where   .= " AND (`wcs`.order_number = '{$sch_ord_no}' OR `wcs`.total_ord_no = '{$sch_ord_no}')";
        $smarty->assign('sch_ord_no', $sch_ord_no);
    }

    if(!empty($sch_is_return)){
        $add_where   .= " AND `wcs`.is_return = '{$sch_is_return}'";
        $smarty->assign('sch_is_return', $sch_is_return);
    }

    if(!empty($sch_is_order)){
        if($sch_is_order == '1'){
            $add_where   .= " AND wcs.c_no > 0";
        }elseif($sch_is_order == '2'){
            $add_where   .= " AND (wcs.c_no = 0 OR wcs.c_no IS null)";
        }
        $smarty->assign('sch_is_order', $sch_is_order);
    }

    if(!empty($sch_log_c_no)){
        $add_where   .= " AND `wcs`.log_c_no = '{$sch_log_c_no}'";
        $smarty->assign('sch_log_c_no', $sch_log_c_no);
    }

    if(!empty($sch_sku)){
        $add_where   .= " AND `wcs`.sku LIKE '%{$sch_sku}%'";
        $smarty->assign('sch_sku', $sch_sku);
    }

    if(!empty($sch_settle_gubun)){
        $add_where   .= " AND `wcs`.settle_gubun = '{$sch_settle_gubun}'";
        $smarty->assign('sch_settle_gubun', $sch_settle_gubun);
    }

    if(!empty($sch_settle_cs)){
        $add_where   .= " AND `wcs`.settle_cs = '{$sch_settle_cs}'";
        $smarty->assign('sch_settle_cs', $sch_settle_cs);
    }

    if(!empty($sch_file_no)){
        $add_where   .= " AND FIND_IN_SET('{$sch_file_no}', `wcs`.file_no) > 0";
        $smarty->assign('sch_file_no', $sch_file_no);
    }

    if(!empty($sch_notice)){
        $add_where   .= " AND `wcs`.notice LIKE '%{$sch_notice}%'";
        $smarty->assign('sch_notice', $sch_notice);
    }

    if(!empty($sch_brand))
    {
        $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
        $sch_brand_list     = $brand_list[$sch_brand_g2];

        $add_where .= " AND `wcs`.c_no = '{$sch_brand}'";
    }
    elseif(!empty($sch_brand_g2))
    {
        $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];
        $sch_brand_list     = $brand_list[$sch_brand_g2];

        $add_where .= " AND `wcs`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand='{$sch_brand_g2}')";
    }
    elseif(!empty($sch_brand_g1))
    {
        $sch_brand_g2_list  = $brand_company_g2_list[$sch_brand_g1];

        $add_where .= " AND `wcs`.c_no IN(SELECT c.c_no FROM company c WHERE c.brand IN(SELECT k.k_name_code FROM kind k WHERE k.k_parent='{$sch_brand_g1}'))";
    }

    $smarty->assign("sch_brand_g1", $sch_brand_g1);
    $smarty->assign("sch_brand_g2", $sch_brand_g2);
    $smarty->assign("sch_brand", $sch_brand);
}
$smarty->assign("sch_brand_g1_list", $brand_company_g1_list);
$smarty->assign("sch_brand_g2_list", $sch_brand_g2_list);
$smarty->assign("sch_brand_list", $sch_brand_list);

# 페이징 처리
$sales_settle_total_sql	    = "SELECT count(`no`) as cnt FROM work_cms_settlement `wcs` WHERE {$add_where}";
$sales_settle_total_query   = mysqli_query($my_db, $sales_settle_total_sql);
$sales_settle_total_result  = mysqli_fetch_array($sales_settle_total_query);
$sales_settle_total         = $sales_settle_total_result['cnt'];

$sales_settle_total_type_sql    = "SELECT settle_gubun, SUM(sales_price) as total_sales_price, SUM(settle_price) as total_settle_price, SUM(settle_price_vat) as total_settle_price_vat FROM work_cms_settlement `wcs` WHERE {$add_where} GROUP BY settle_gubun";
$sales_settle_total_type_query  = mysqli_query($my_db, $sales_settle_total_type_sql);
$settle_gubun_option            = getSettleGubunOption();
$sales_settle_total_list        = array(
    "sales"     => [],
    "settle"    => []
);
foreach($settle_gubun_option as $gubun_key => $gubun_title){
    $sales_settle_total_list['sales'][$gubun_key]   = array('title' => $gubun_title, 'price' => 0);
    $sales_settle_total_list['settle'][$gubun_key]  = array('title' => $gubun_title, 'price' => 0, 'price_vat' => 0);
}
while($sales_settle_total_type = mysqli_fetch_assoc($sales_settle_total_type_query))
{
    $sales_settle_total_list['sales'][$sales_settle_total_type['settle_gubun']]['price']        +=  $sales_settle_total_type['total_sales_price'];
    $sales_settle_total_list['settle'][$sales_settle_total_type['settle_gubun']]['price']       +=  $sales_settle_total_type['total_settle_price'];
    $sales_settle_total_list['settle'][$sales_settle_total_type['settle_gubun']]['price_vat']   +=  $sales_settle_total_type['total_settle_price_vat'];
}

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ?intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($sales_settle_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = getenv("QUERY_STRING");
$page_list	= pagelist($pages, "work_cms_sales_settlement.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $sales_settle_total);
$smarty->assign("pagelist", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$sales_settle_sql = "
    SELECT 
        *,
        DATE_FORMAT(`wcs`.regdate, '%Y-%m-%d') as reg_day,
        (SELECT `c`.c_name FROM company `c` WHERE `c`.c_no=`wcs`.log_c_no) as log_c_name,
        (SELECT um.file_path FROM upload_management um WHERE `um`.file_no=`wcs`.file_no) as file_path,
        (SELECT um.file_name FROM upload_management um WHERE um.file_no=`wcs`.file_no) as file_name,
        (SELECT COUNT(w_no) FROM work_cms w WHERE w.order_number=`wcs`.order_number) as ord_cnt,
        (SELECT SUM(quantity) FROM work_cms w WHERE w.order_number=`wcs`.order_number AND w.unit_price > 0) as ord_qty,
        (SELECT COUNT(w_no) FROM work_cms w WHERE w.origin_ord_no=`wcs`.order_number) as org_cnt
    FROM work_cms_settlement `wcs`
    WHERE {$add_where}
    ORDER BY {$add_order_by} 
    LIMIT {$offset}, {$num}
";
$sales_settle_query     = mysqli_query($my_db, $sales_settle_sql);
$sales_settle_list      = [];
$sales_settle_sum_list  = [];
$sales_settle_qty_list  = [];
$settle_cs_option       = getSettleCsOption();
while($sales_settle = mysqli_fetch_assoc($sales_settle_query))
{
    $sales_settle['settle_cs_name']     = isset($settle_cs_option[$sales_settle['settle_cs']]) ? $settle_cs_option[$sales_settle['settle_cs']] : "";
    $sales_settle['settle_gubun_name']  = isset($settle_gubun_option[$sales_settle['settle_gubun']]) ? $settle_gubun_option[$sales_settle['settle_gubun']] : "";

    $upload_file_list = [];
    if(!empty($sales_settle['file_no'])){
        $chk_file_sql   = "SELECT file_no, file_path, file_name FROM upload_management um WHERE `um`.file_no IN({$sales_settle['file_no']})";
        $chk_file_query = mysqli_query($my_db, $chk_file_sql);
        while($chk_file_result = mysqli_fetch_assoc($chk_file_query)){
            $upload_file_list[] = $chk_file_result;
        }
    }
    $sales_settle['upload_file_list'] = $upload_file_list;

    if($sch_is_single){
        $sales_settle_sum_list[$sales_settle['order_number']] += $sales_settle['settle_price'];
        $sales_settle_qty_list[$sales_settle['order_number']]  = $sales_settle['ord_qty'];
    }

    $sales_settle_list[] = $sales_settle;
}

$max_order_number = "";
$min_order_number = "";
if(!empty($sales_settle_sum_list))
{
    $max_settle_price = 0;
    $min_settle_price = 0;
    $chk_settle_idx   = 0;

    foreach($sales_settle_sum_list as $ord_no => $settle_price)
    {
        $ord_qty = $sales_settle_qty_list[$ord_no];
        if($chk_settle_idx == 0)
        {
            $max_settle_price = $settle_price/$ord_qty;
            $min_settle_price = $settle_price/$ord_qty;
            $max_order_number = $ord_no;
            $min_order_number = $ord_no;
        }

        $cal_settle_price = $settle_price/$ord_qty;

        if($max_settle_price < $cal_settle_price){
            $max_settle_price = $cal_settle_price;
            $max_order_number = $ord_no;
        }

        if($min_settle_price > $cal_settle_price){
            $min_settle_price = $cal_settle_price;
            $min_order_number = $ord_no;
        }

        $chk_settle_idx++;
    }
}
$smarty->assign("sch_is_single", $sch_is_single);
$smarty->assign("max_order_number", $max_order_number);
$smarty->assign("min_order_number", $min_order_number);

$smarty->assign('current_month', $current_month);
$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("settle_type_option", getSettleTypeOption());
$smarty->assign("sch_dp_company_list", $sch_dp_company_list);
$smarty->assign("settle_company_list", $settle_company_list);
$smarty->assign("logistics_company_list", $logistics_company_list);
$smarty->assign("is_exist_option", getIsExistOption());
$smarty->assign("is_order_option", getIsOrderOption());
$smarty->assign("settle_gubun_option", $settle_gubun_option);
$smarty->assign("settle_cs_option", $settle_cs_option);
$smarty->assign("sales_settle_total_list", $sales_settle_total_list);
$smarty->assign("sales_settle_list", $sales_settle_list);

$smarty->display('work_cms_sales_settlement.html');
?>
