<?php
require('inc/common.php');
require('ckadmin.php');

$promotion_kind=array(
	array('::전체::',''),
	array('체험단','1'),
	array('기자단','2'),
	array('배송체험단','3'),
	array('체험단(+보상)','4'),
	array('배송체험단(+보상)','5')
);
$smarty->assign("promotion_kind",$promotion_kind);

// 직원가져오기
$staff_sql="SELECT s_no,s_name FROM staff WHERE staff_state < '3' AND permission LIKE '1______'";
$staff_query=mysqli_query($my_db,$staff_sql);

while($staff_data=mysqli_fetch_array($staff_query)) {
	$staffs[]=array(
		'no'=>$staff_data['s_no'],
		'name'=>$staff_data['s_name']
	);
	$smarty->assign("staff",$staffs);
}

$search_kind		= isset($_GET['skind'])?$_GET['skind']:"";
$search_start_month	= isset($_GET['ssmonth'])?$_GET['ssmonth']:date("Y/m", strtotime("-12 month"));
$search_end_month	= isset($_GET['semonth'])?$_GET['semonth']:date("Y/m");
$sch_s_no		= isset($_GET['sch_s_no'])?$_GET['sch_s_no']:"";

$smarty->assign("skind", $search_kind);
$smarty->assign("ssmonth",$search_start_month);
$smarty->assign("semonth",$search_end_month);

$add_where = "1=1";
if(!empty($sch_s_no))
{
	$add_where .= " AND s_no='{$sch_s_no}'";
	$smarty->assign("sch_s_no", $sch_s_no);
}


$chart_date = $search_start_month."/01";

$result_month_array = [];
$result_exp_array = [];
$result_rep_array = [];
$result_dlv_array = [];
$result_exp_rep_array = [];
$result_dlv_rep_array = [];

/* 날짜와 테이블 where 조건을 토대로 chart01의 쿼리값을 불러옴 */
function chart01_sql($chart_date_f, $table_name_f, $add_where_f){
	$sql_f="
		SELECT
			DATE_FORMAT('".$chart_date_f."', '%Y-%m') as result_month,
			COUNT(*) as result_promotion_num
		FROM
			$table_name_f
		WHERE
			$add_where_f
			DATE_FORMAT(reg_sdate, '%Y-%m') BETWEEN DATE_FORMAT('".$chart_date_f."', '%Y-%m') AND DATE_FORMAT(DATE_ADD(DATE_ADD('".$chart_date_f."',INTERVAL 1 MONTH),INTERVAL -1 DAY), '%Y-%m')
	";

	return $sql_f;
}


$x_name_list = [];
for ($i=0 ; $chart_date <= $search_end_month."/01" ; $i++)
{
	$x_name_list[] = date('Y-m', strtotime($chart_date));
	if($chart_date < date("2016/02/01")){
		$table_name = "promotion_old";
	}else{
		$table_name = "promotion";
	}

	// 체험단 수 가져오기...
	$sql = chart01_sql($chart_date, $table_name, "{$add_where} AND kind = '1' and p_state1 = '0' and");

	if($search_kind !=2 && $search_kind != 3 && $search_kind != 4 && $search_kind != 5){
		$query=mysqli_query($my_db,$sql);
		$data=mysqli_fetch_array($query);

		$result_exp_array[$data['result_month']] = $data['result_promotion_num'];

		if($chart_date == date("2016/02/01")){
			$sql = chart01_sql($chart_date, "promotion_old", "{$add_where} AND kind = '1' and p_state1 = '0' and");
			$query=mysqli_query($my_db,$sql);
			$data=mysqli_fetch_array($query);

			$result_exp_array[$data['result_month']] += $data['result_promotion_num']; // 2016년 2월 지난 체험단 더하기(promotion_old)
		}
	}

	// 기자단 수 가져오기...
	$sql = chart01_sql($chart_date, $table_name, "{$add_where} AND kind = '2' and p_state1 = '0' and");

	if($search_kind !=1 && $search_kind != 3 && $search_kind != 4 && $search_kind != 5){
		$query=mysqli_query($my_db,$sql);
		$data=mysqli_fetch_array($query);

		$result_rep_array[$data['result_month']] = $data['result_promotion_num'];

		if($chart_date == date("2016/02/01")){
			$sql = chart01_sql($chart_date, "promotion_old", "{$add_where} AND kind = '2' and p_state1 = '0' and");
			$query=mysqli_query($my_db,$sql);
			$data=mysqli_fetch_array($query);

			$result_rep_array[$data['result_month']] += $data['result_promotion_num']; // 2016년 2월 지난 기자단 더하기(promotion_old)
		}
	}

	// 배송체험단 수 가져오기...
	$sql = chart01_sql($chart_date, $table_name, "{$add_where} AND kind = '3' and p_state1 = '0' and");

	if($search_kind !=1 && $search_kind != 2 && $search_kind != 4 && $search_kind != 5){
		$query=mysqli_query($my_db,$sql);
		$data=mysqli_fetch_array($query);

		$result_dlv_array[$data['result_month']] = $data['result_promotion_num'];

		if($chart_date == date("2016/02/01")){
			$sql = chart01_sql($chart_date, "promotion_old", "{$add_where} AND kind = '3' and p_state1 = '0' and");
			$query=mysqli_query($my_db,$sql);
			$data=mysqli_fetch_array($query);

			$result_dlv_array[$data['result_month']] += $data['result_promotion_num']; // 2016년 2월 지난 배송체험단 더하기(promotion_old)
		}
	}

	// 체험단(+보상) 수 가져오기...
	$sql = chart01_sql($chart_date, $table_name, "{$add_where} AND kind = '4' and p_state1 = '0' and");

	if($search_kind !=1 && $search_kind != 2 && $search_kind != 3 && $search_kind != 5){
		$query=mysqli_query($my_db,$sql);
		$data=mysqli_fetch_array($query);

		$result_exp_rep_array[$data['result_month']] = $data['result_promotion_num'];
	}

	// 배송체험단(+보상) 수 가져오기...
	$sql = chart01_sql($chart_date, $table_name, "{$add_where} AND kind = '5' and p_state1 = '0' and");

	if($search_kind !=1 && $search_kind != 2 && $search_kind != 3 && $search_kind != 4){
		$query=mysqli_query($my_db,$sql);
		$data=mysqli_fetch_array($query);

		$result_dlv_rep_array[$data['result_month']] = $data['result_promotion_num'];
	}
	$chart_date =  date("Y/m/01", strtotime("+1 month", strtotime($chart_date)));

	if($i >= 50){
		echo "조회기간이 너무 많습니다. 조회기간을 줄여주세요.";
		return;
	}
}

$chart_title = "";
$chart_title_list = [];
$chart_data_list  = [];
$chart_color_list = [];
switch($search_kind){
	case 1 :
        $chart_title 		= "TOPBLOG '체험단' Promotion Count";
		$chart_title_list 	= ["체험단"];
        $chart_data_list	= [$result_exp_array];
		$chart_color_list	= ["#f77b18"];
        break;
	case 2 :
        $chart_title 		= "TOPBLOG '기자단' Promotion Count";
		$chart_title_list 	= ["기자단"];
		$chart_data_list	= [$result_rep_array];
		$chart_color_list 	= ["#5175fe"];
		break;
	case 3 :
        $chart_title 		= "TOPBLOG '배송체험' Promotion Count";
		$chart_title_list 	= ["배송체험"];
		$chart_data_list  	= [$result_dlv_array];
		$chart_color_list 	= ["#63F"];
		break;
	case 4 :
        $chart_title 		= "TOPBLOG '체험단(+보상)' Promotion Count";
		$chart_title_list 	= ["체험단(+보상)"];
		$chart_data_list  	= [$result_exp_rep_array];
		$chart_color_list 	= ["#a64c00"];
		break;
	case 5 :
        $chart_title 		= "TOPBLOG '배송체험(+보상)' Promotion Count";
		$chart_title_list 	= ["배송체험(+보상)"];
		$chart_data_list  	= [$result_dlv_rep_array];
		$chart_color_list 	= ["#27009b"];
		break;
	default :
        $chart_title 		= "TOPBLOG 'ALL' Promotion Count";
		$chart_title_list 	= ["체험단","기자단","배송체험","체험단(+보상)","배송체험(+보상)"];
        $chart_data_list	= [$result_exp_array, $result_rep_array, $result_dlv_array, $result_exp_rep_array, $result_dlv_rep_array];
        $chart_color_list	= ['#f77b18', '#5175fe', '#63F', '#a64c00', '#27009b'];
		break;
}

$smarty->assign("x_name_list", $x_name_list);
$smarty->assign("chart_title_list", $chart_title_list);
$smarty->assign("chart_data_list", $chart_data_list);
$smarty->assign("chart_color_list", $chart_color_list);

$smarty->display('topblog_stats_chart01.html');
?>
