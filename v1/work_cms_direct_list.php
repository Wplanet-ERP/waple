<?php
require('inc/common.php');
require('ckadmin.php');
require('inc/helper/_navigation.php');
require('inc/helper/_common.php');
require('inc/model/MyQuick.php');

# Navigation & My Quick
$nav_prd_no  = "241";
$nav_title   = "커머스 직택배 리스트";
$quick_model = MyQuick::Factory();
$is_my_quick = $quick_model->isMyQuick("navigation", $nav_prd_no, $session_s_no);

$smarty->assign("is_my_quick", $is_my_quick);
$smarty->assign("nav_title", $nav_title);
$smarty->assign("nav_prd_no", $nav_prd_no);

# 검색쿼리
$add_where          = "1=1";
$sch_w_no           = isset($_GET['sch_w_no']) ? $_GET['sch_w_no'] : "";
$sch_log_company    = isset($_GET['sch_log_company']) ? $_GET['sch_log_company'] : "";
$sch_delivery_date  = isset($_GET['sch_delivery_date']) ? $_GET['sch_delivery_date'] : "";
$sch_direct_type    = isset($_GET['sch_direct_type']) ? $_GET['sch_direct_type'] : "";
$sch_delivery_type  = isset($_GET['sch_delivery_type']) ? $_GET['sch_delivery_type'] : "";
$sch_delivery_no    = isset($_GET['sch_delivery_no']) ? $_GET['sch_delivery_no'] : "";
$sch_recipient      = isset($_GET['sch_recipient']) ? $_GET['sch_recipient'] : "";
$sch_recipient_hp   = isset($_GET['sch_recipient_hp']) ? $_GET['sch_recipient_hp'] : "";
$sch_recipient_addr = isset($_GET['sch_recipient_addr']) ? $_GET['sch_recipient_addr'] : "";
$sch_file_no        = isset($_GET['sch_file_no']) ? $_GET['sch_file_no'] : "";

if(!empty($sch_w_no)){
    $add_where .= " AND `w`.`w_no`='{$sch_w_no}'";
    $smarty->assign("sch_w_no", $sch_w_no);
}

if(!empty($sch_log_company)){
    $add_where .= " AND `w`.`log_c_no`='{$sch_log_company}'";
    $smarty->assign("sch_log_company", $sch_log_company);
}

if(!empty($sch_delivery_date)){
    $add_where .= " AND `w`.`delivery_date`='{$sch_delivery_date}'";
    $smarty->assign("sch_delivery_date", $sch_delivery_date);
}

if(!empty($sch_direct_type)){
    $add_where .= " AND `w`.`direct_type`='{$sch_direct_type}'";
    $smarty->assign("sch_direct_type", $sch_direct_type);
}

if(!empty($sch_delivery_type)){
    $add_where .= " AND `w`.`delivery_type` LIKE '%{$sch_delivery_type}%'";
    $smarty->assign("sch_delivery_type", $sch_delivery_type);
}

if(!empty($sch_delivery_no)){
    $add_where .= " AND `w`.`delivery_no` LIKE '%{$sch_delivery_no}%'";
    $smarty->assign("sch_delivery_no", $sch_delivery_no);
}

if(!empty($sch_recipient)){
    $add_where .= " AND `w`.`recipient` LIKE '%{$sch_recipient}%'";
    $smarty->assign("sch_recipient", $sch_recipient);
}

if(!empty($sch_recipient_hp)){
    $add_where .= " AND `w`.`recipient_hp` LIKE '%{$sch_recipient_hp}%'";
    $smarty->assign("sch_recipient_hp", $sch_recipient_hp);
}

if(!empty($sch_recipient_addr)){
    $add_where .= " AND `w`.`recipient_addr` LIKE '%{$sch_recipient_addr}%'";
    $smarty->assign("sch_recipient_addr", $sch_recipient_addr);
}

if(!empty($sch_file_no)){
    $add_where .= " AND `w`.`file_no` = '{$sch_file_no}'";
    $smarty->assign("sch_file_no", $sch_file_no);
}

# 전체 게시물 수
$cms_direct_cnt_sql     = "SELECT COUNT(DISTINCT `w`.w_no) as cnt FROM work_cms_direct AS `w` WHERE {$add_where}";
$cms_direct_cnt_query   = mysqli_query($my_db, $cms_direct_cnt_sql);
$cms_direct_cnt_result  = mysqli_fetch_array($cms_direct_cnt_query);
$cms_direct_total 	    = $cms_direct_cnt_result['cnt'];

$page_type  = (isset($_GET['ord_page_type']) && intval($_GET['ord_page_type']) > 0) ? intval($_GET['ord_page_type']) : "20";
$pages 		= isset($_GET['page']) ? intval($_GET['page']) : 1;
$num 		= $page_type;
$offset 	= ($pages-1) * $num;
$page_num   = ceil($cms_direct_total/$num);

if ($pages >= $page_num){$pages = $page_num;}
if ($pages <= 0){$pages = 1;}

$search_url = (!empty($new_am_no)) ? "" : getenv("QUERY_STRING");
$page_list	= pagelist($pages, "work_cms_direct_list.php", $page_num, $search_url);

$smarty->assign("search_url", $search_url);
$smarty->assign("total_num", $cms_direct_total);
$smarty->assign("page_list", $page_list);
$smarty->assign("ord_page_type", $page_type);

# 리스트 쿼리
$work_cms_direct_sql = "
    SELECT
        `w`.*,
        (SELECT um.file_path FROM upload_management um WHERE um.file_no=w.file_no) as file_path,
        (SELECT um.file_name FROM upload_management um WHERE um.file_no=w.file_no) as file_name
    FROM work_cms_direct AS `w`
    WHERE {$add_where}
    ORDER BY `w`.`w_no` DESC
    LIMIT {$offset}, {$num}
";
$work_cms_direct_query  = mysqli_query($my_db, $work_cms_direct_sql);
$work_cms_direct_list   = [];
$log_company_option     = array("2809" => "위드플레이스");
while($work_cms_direct = mysqli_fetch_assoc($work_cms_direct_query))
{
    $prd_info_conv  = $work_cms_direct['prd_info'];
    $prd_info_conv  = str_replace("EA)", "EA_", $prd_info_conv);
    $prd_info_conv  = str_replace(")", ")<br/>", $prd_info_conv);
    $prd_info_conv  = str_replace("EA_", "EA)", $prd_info_conv);
    $work_cms_direct['prd_info_conv']   = $prd_info_conv;
    $work_cms_direct['log_c_name']      = $log_company_option[$work_cms_direct['log_c_no']];

    $work_cms_direct_list[] = $work_cms_direct;
}

$smarty->assign("page_type_option", getPageTypeOption(4));
$smarty->assign("log_company_option", $log_company_option);
$smarty->assign("work_cms_direct_list", $work_cms_direct_list);

$smarty->display('work_cms_direct_list.html');
?>