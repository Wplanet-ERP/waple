<?php
ini_set("display_errors", -1);
ini_set("max_execution_time", 2000);

require('inc/common.php');
require('ckadmin.php');

$corp_info_sql = "SELECT * FROM corp_info group by corp_code";
$corp_info_query = mysqli_query($my_db, $corp_info_sql);
$corp_key = "96726cd83941fb82f1c191891c7167e06c590206";


while($corp_info = mysqli_fetch_assoc($corp_info_query))
{
    $corp_code   = $corp_info['corp_code'];
    $corp_header = array(
        "crtfc_key" => $corp_key,
        "corp_code" => $corp_code,
    );

    $url = "https://opendart.fss.or.kr/api/company.json?".http_build_query($corp_header, ''); //주소셋팅
    $headers = [
        'Accept: ' . $_SERVER['HTTP_ACCEPT'],
        'Accept-Encoding: gzip, deflate',
        'Accept-Language: ' . $_SERVER['HTTP_ACCEPT_LANGUAGE'],
        'Cache-Control: no-cache',
        'User-Agent: ' . $_SERVER['HTTP_USER_AGENT']
    ];

    $ch = curl_init(); //curl 로딩
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $result = curl_exec($ch);
    curl_close ($ch);

    $corp_data = json_decode($result, true);

    if(isset($corp_data['corp_code']) && !empty($corp_data['corp_code']))
    {
        $induty_code = $corp_data['induty_code'];
        $corp_addr  = $corp_data['adres'];
        $home_url   = (empty($corp_data['hm_url']) || $corp_data['hm_url'] == '없음') ? "" : $corp_data['hm_url'];
        $tel        = $corp_data['phn_no'];
        $est_date   = date('Y-m-d', strtotime($corp_data['est_dt']));

        $upd_sql = "UPDATE corp_info SET induty_code='{$induty_code}', corp_addr='{$corp_addr}', home_url='{$home_url}', tel='{$tel}', est_date='{$est_date}' WHERE corp_code='{$corp_code}'";

        mysqli_query($my_db, $upd_sql);
    }else{
        echo $corp_code."<br/>";
    }
}
?>
